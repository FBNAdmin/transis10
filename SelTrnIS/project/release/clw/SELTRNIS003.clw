

   MEMBER('SELTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SELTRNIS003.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_Floors PROCEDURE 

CurrentTab           STRING(80)                            ! 
BRW1::View:Browse    VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FBNFloor)
                       PROJECT(FLO:FID)
                       PROJECT(FLO:AID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:FBNFloor           LIKE(FLO:FBNFloor)             !List box control field - type derived from field
FLO:FBNFloor_Icon      LONG                           !Entry's icon ID
FLO:FID                LIKE(FLO:FID)                  !Primary key field - type derived from field
FLO:AID                LIKE(FLO:AID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Floors Record'),AT(,,158,198),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('SelectFloors'),SYSTEM
                       LIST,AT(8,30,142,124),USE(?Browse:1),HVSCROLL,FORMAT('92L(2)|M~Floor~@s35@40R(2)|MI~FBN' & |
  ' Floor~C(0)@p p@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Floors file')
                       BUTTON('&Select'),AT(101,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,150,172),USE(?CurrentTab),FONT('Tahoma')
                         TAB('&1) By Floor'),USE(?Tab:2)
                         END
                         TAB('&2) By Address'),USE(?Tab:3)
                         END
                       END
                       BUTTON('&Close'),AT(105,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Floors')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Floors.Open                                       ! File Floors used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Floors,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon FLO:AID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,FLO:FKey_AID)    ! Add the sort order for FLO:FKey_AID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,FLO:AID,1,BRW1)                ! Initialize the browse locator using  using key: FLO:FKey_AID , FLO:AID
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon FLO:Floor for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,FLO:Key_Floor)   ! Add the sort order for FLO:Key_Floor for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,FLO:Floor,1,BRW1)              ! Initialize the browse locator using  using key: FLO:Key_Floor , FLO:Floor
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(FLO:Floor,BRW1.Q.FLO:Floor)                ! Field FLO:Floor is a hot field or requires assignment from browse
  BRW1.AddField(FLO:FBNFloor,BRW1.Q.FLO:FBNFloor)          ! Field FLO:FBNFloor is a hot field or requires assignment from browse
  BRW1.AddField(FLO:FID,BRW1.Q.FLO:FID)                    ! Field FLO:FID is a hot field or requires assignment from browse
  BRW1.AddField(FLO:AID,BRW1.Q.FLO:AID)                    ! Field FLO:AID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_Floors',QuickWindow)                ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Floors.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_Floors',QuickWindow)             ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (FLO:FBNFloor = 1)
    SELF.Q.FLO:FBNFloor_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.FLO:FBNFloor_Icon = 1                           ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_Transporter PROCEDURE 

CurrentTab           STRING(80)                            ! 
LOC:FilterStatusIncludeDoNotUse BYTE                       ! 
LOC:Status           STRING(14)                            ! 
BRW1::View:Browse    VIEW(Transporter)
                       PROJECT(TRA:TransporterName)
                       PROJECT(TRA:OpsManager)
                       PROJECT(TRA:VATNo)
                       PROJECT(TRA:Broking)
                       PROJECT(TRA:ChargesVAT)
                       PROJECT(TRA:ACID)
                       PROJECT(TRA:BID)
                       PROJECT(TRA:Status)
                       PROJECT(TRA:TID)
                       PROJECT(TRA:AID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
TRA:TransporterName_NormalFG LONG                     !Normal forground color
TRA:TransporterName_NormalBG LONG                     !Normal background color
TRA:TransporterName_SelectedFG LONG                   !Selected forground color
TRA:TransporterName_SelectedBG LONG                   !Selected background color
TRA:OpsManager         LIKE(TRA:OpsManager)           !List box control field - type derived from field
TRA:VATNo              LIKE(TRA:VATNo)                !List box control field - type derived from field
TRA:Broking            LIKE(TRA:Broking)              !List box control field - type derived from field
TRA:Broking_Icon       LONG                           !Entry's icon ID
TRA:ChargesVAT         LIKE(TRA:ChargesVAT)           !List box control field - type derived from field
TRA:ChargesVAT_Icon    LONG                           !Entry's icon ID
LOC:Status             LIKE(LOC:Status)               !List box control field - type derived from local data
TRA:ACID               LIKE(TRA:ACID)                 !List box control field - type derived from field
TRA:BID                LIKE(TRA:BID)                  !List box control field - type derived from field
TRA:Status             LIKE(TRA:Status)               !Browse hot field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !Primary key field - type derived from field
TRA:AID                LIKE(TRA:AID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Transporter Record'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('SelectTransporter'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M*~Transporter Name~@s35@8' & |
  '0L(2)|M~Ops Manager~@s35@50L(2)|M~VAT No.~@s20@28L(2)|MI~Broking~C(0)@p p@46L(2)|MI~' & |
  'Charges VAT~C(0)@p p@56L(2)|M~Status~@s14@30R(2)|M~ACID~C(0)@n_10@30R(2)|M~BID~C(0)@n_10@'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing the Transporter file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Transporter Name'),USE(?Tab:2)
                         END
                         TAB('&2) By Branch ID'),USE(?Tab:3)
                         END
                         TAB('&3) By Address ID'),USE(?Tab:4)
                         END
                         TAB('&4) By Accountant ID'),USE(?Tab:5)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       CHECK(' Include Do Not Use'),AT(4,184),USE(LOC:FilterStatusIncludeDoNotUse)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 4
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW1::Sort2:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 3
BRW1::Sort3:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 4
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Transporter')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:FilterStatusIncludeDoNotUse',LOC:FilterStatusIncludeDoNotUse) ! Added by: BrowseBox(ABC)
  BIND('LOC:Status',LOC:Status)                            ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                                  ! File Transporter used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Transporter,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon TRA:BID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,TRA:FKey_BID)    ! Add the sort order for TRA:FKey_BID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,TRA:BID,1,BRW1)                ! Initialize the browse locator using  using key: TRA:FKey_BID , TRA:BID
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon TRA:AID for sort order 2
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,TRA:FKey_AID)    ! Add the sort order for TRA:FKey_AID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,TRA:AID,1,BRW1)                ! Initialize the browse locator using  using key: TRA:FKey_AID , TRA:AID
  BRW1::Sort3:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon TRA:ACID for sort order 3
  BRW1.AddSortOrder(BRW1::Sort3:StepClass,TRA:FKey_ACID)   ! Add the sort order for TRA:FKey_ACID for sort order 3
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort3:Locator.Init(,TRA:ACID,1,BRW1)               ! Initialize the browse locator using  using key: TRA:FKey_ACID , TRA:ACID
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon TRA:TransporterName for sort order 4
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,TRA:Key_TransporterName) ! Add the sort order for TRA:Key_TransporterName for sort order 4
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 4
  BRW1::Sort0:Locator.Init(,TRA:TransporterName,1,BRW1)    ! Initialize the browse locator using  using key: TRA:Key_TransporterName , TRA:TransporterName
  BRW1.SetFilter('(LOC:FilterStatusIncludeDoNotUse = 1 OR (TRA:Status << 2))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:FilterStatusIncludeDoNotUse)      ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(TRA:TransporterName,BRW1.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW1.AddField(TRA:OpsManager,BRW1.Q.TRA:OpsManager)      ! Field TRA:OpsManager is a hot field or requires assignment from browse
  BRW1.AddField(TRA:VATNo,BRW1.Q.TRA:VATNo)                ! Field TRA:VATNo is a hot field or requires assignment from browse
  BRW1.AddField(TRA:Broking,BRW1.Q.TRA:Broking)            ! Field TRA:Broking is a hot field or requires assignment from browse
  BRW1.AddField(TRA:ChargesVAT,BRW1.Q.TRA:ChargesVAT)      ! Field TRA:ChargesVAT is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Status,BRW1.Q.LOC:Status)              ! Field LOC:Status is a hot field or requires assignment from browse
  BRW1.AddField(TRA:ACID,BRW1.Q.TRA:ACID)                  ! Field TRA:ACID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:BID,BRW1.Q.TRA:BID)                    ! Field TRA:BID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:Status,BRW1.Q.TRA:Status)              ! Field TRA:Status is a hot field or requires assignment from browse
  BRW1.AddField(TRA:TID,BRW1.Q.TRA:TID)                    ! Field TRA:TID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:AID,BRW1.Q.TRA:AID)                    ! Field TRA:AID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_Transporter',QuickWindow)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Transporter.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_Transporter',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
     LOC:Status = 'Normal'
     IF TRA:Status = 1
        LOC:Status = 'Pending'
     ELSIF TRA:Status = 2
        LOC:Status = 'Do Not Use'
     .
  
  IF (TRA:Status=1)
    SELF.Q.TRA:TransporterName_NormalFG = -1               ! Set conditional color values for TRA:TransporterName
    SELF.Q.TRA:TransporterName_NormalBG = 33023
    SELF.Q.TRA:TransporterName_SelectedFG = 33023
    SELF.Q.TRA:TransporterName_SelectedBG = 16777215
  ELSIF (TRA:Status=2)
    SELF.Q.TRA:TransporterName_NormalFG = -1               ! Set conditional color values for TRA:TransporterName
    SELF.Q.TRA:TransporterName_NormalBG = 255
    SELF.Q.TRA:TransporterName_SelectedFG = 255
    SELF.Q.TRA:TransporterName_SelectedBG = 16777215
  ELSE
    SELF.Q.TRA:TransporterName_NormalFG = -1               ! Set color values for TRA:TransporterName
    SELF.Q.TRA:TransporterName_NormalBG = -1
    SELF.Q.TRA:TransporterName_SelectedFG = -1
    SELF.Q.TRA:TransporterName_SelectedBG = -1
  END
  IF (TRA:Broking = 1)
    SELF.Q.TRA:Broking_Icon = 2                            ! Set icon from icon list
  ELSE
    SELF.Q.TRA:Broking_Icon = 1                            ! Set icon from icon list
  END
  IF (TRA:ChargesVAT = 1)
    SELF.Q.TRA:ChargesVAT_Icon = 2                         ! Set icon from icon list
  ELSE
    SELF.Q.TRA:ChargesVAT_Icon = 1                         ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_AddressAlias PROCEDURE 

CurrentTab           STRING(80)                            ! 
BRW1::View:Browse    VIEW(AddressAlias)
                       PROJECT(A_ADD:AddressName)
                       PROJECT(A_ADD:Line1)
                       PROJECT(A_ADD:Line2)
                       PROJECT(A_ADD:PhoneNo)
                       PROJECT(A_ADD:PhoneNo2)
                       PROJECT(A_ADD:Fax)
                       PROJECT(A_ADD:Branch)
                       PROJECT(A_ADD:Client)
                       PROJECT(A_ADD:AID)
                       PROJECT(A_ADD:SUID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
A_ADD:AddressName      LIKE(A_ADD:AddressName)        !List box control field - type derived from field
A_ADD:Line1            LIKE(A_ADD:Line1)              !List box control field - type derived from field
A_ADD:Line2            LIKE(A_ADD:Line2)              !List box control field - type derived from field
A_ADD:PhoneNo          LIKE(A_ADD:PhoneNo)            !List box control field - type derived from field
A_ADD:PhoneNo2         LIKE(A_ADD:PhoneNo2)           !List box control field - type derived from field
A_ADD:Fax              LIKE(A_ADD:Fax)                !List box control field - type derived from field
A_ADD:Branch           LIKE(A_ADD:Branch)             !List box control field - type derived from field
A_ADD:Client           LIKE(A_ADD:Client)             !List box control field - type derived from field
A_ADD:AID              LIKE(A_ADD:AID)                !Primary key field - type derived from field
A_ADD:SUID             LIKE(A_ADD:SUID)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Address Record (a)'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('SelectAddressAlias'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Address Name~@s35@80L(2)' & |
  '|M~Line 1~@s35@80L(2)|M~Line 2~@s35@80L(2)|M~Phone No~@s20@80L(2)|M~Phone No 2~@s20@' & |
  '80L(2)|M~Fax~@s20@28R(2)|M~Branch~C(0)@n3@28R(2)|M~Client~C(0)@n3@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the AddressAlias file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Name'),USE(?Tab:2)
                         END
                         TAB('&2) By Suburb'),USE(?Tab:3)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_AddressAlias')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:AddressAlias.Open                                 ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:AddressAlias,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon A_ADD:SUID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,A_ADD:FKey_SUID) ! Add the sort order for A_ADD:FKey_SUID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,A_ADD:SUID,1,BRW1)             ! Initialize the browse locator using  using key: A_ADD:FKey_SUID , A_ADD:SUID
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon A_ADD:AddressName for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,A_ADD:Key_Name)  ! Add the sort order for A_ADD:Key_Name for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,A_ADD:AddressName,1,BRW1)      ! Initialize the browse locator using  using key: A_ADD:Key_Name , A_ADD:AddressName
  BRW1.AddField(A_ADD:AddressName,BRW1.Q.A_ADD:AddressName) ! Field A_ADD:AddressName is a hot field or requires assignment from browse
  BRW1.AddField(A_ADD:Line1,BRW1.Q.A_ADD:Line1)            ! Field A_ADD:Line1 is a hot field or requires assignment from browse
  BRW1.AddField(A_ADD:Line2,BRW1.Q.A_ADD:Line2)            ! Field A_ADD:Line2 is a hot field or requires assignment from browse
  BRW1.AddField(A_ADD:PhoneNo,BRW1.Q.A_ADD:PhoneNo)        ! Field A_ADD:PhoneNo is a hot field or requires assignment from browse
  BRW1.AddField(A_ADD:PhoneNo2,BRW1.Q.A_ADD:PhoneNo2)      ! Field A_ADD:PhoneNo2 is a hot field or requires assignment from browse
  BRW1.AddField(A_ADD:Fax,BRW1.Q.A_ADD:Fax)                ! Field A_ADD:Fax is a hot field or requires assignment from browse
  BRW1.AddField(A_ADD:Branch,BRW1.Q.A_ADD:Branch)          ! Field A_ADD:Branch is a hot field or requires assignment from browse
  BRW1.AddField(A_ADD:Client,BRW1.Q.A_ADD:Client)          ! Field A_ADD:Client is a hot field or requires assignment from browse
  BRW1.AddField(A_ADD:AID,BRW1.Q.A_ADD:AID)                ! Field A_ADD:AID is a hot field or requires assignment from browse
  BRW1.AddField(A_ADD:SUID,BRW1.Q.A_ADD:SUID)              ! Field A_ADD:SUID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_AddressAlias',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_AddressAlias',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_StatementRuns_Alias PROCEDURE 

CurrentTab           STRING(80)                            ! 
LOC:Type             STRING(20)                            ! Client Monthly, Client Adhoc, Internal Adhoc (not on web)
LOC:Statement_No     ULONG                                 ! 
BRW1::View:Browse    VIEW(Statement_Runs_Alias)
                       PROJECT(A_STAR:STRID)
                       PROJECT(A_STAR:RunDescription)
                       PROJECT(A_STAR:RunDate)
                       PROJECT(A_STAR:RunTime)
                       PROJECT(A_STAR:EntryDate)
                       PROJECT(A_STAR:EntryTime)
                       PROJECT(A_STAR:RunDateTime)
                       PROJECT(A_STAR:UID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
A_STAR:STRID           LIKE(A_STAR:STRID)             !List box control field - type derived from field
A_STAR:RunDescription  LIKE(A_STAR:RunDescription)    !List box control field - type derived from field
LOC:Type               LIKE(LOC:Type)                 !List box control field - type derived from local data
A_STAR:RunDate         LIKE(A_STAR:RunDate)           !List box control field - type derived from field
A_STAR:RunTime         LIKE(A_STAR:RunTime)           !List box control field - type derived from field
LOC:Statement_No       LIKE(LOC:Statement_No)         !List box control field - type derived from local data
A_STAR:EntryDate       LIKE(A_STAR:EntryDate)         !List box control field - type derived from field
A_STAR:EntryTime       LIKE(A_STAR:EntryTime)         !List box control field - type derived from field
A_STAR:RunDateTime     LIKE(A_STAR:RunDateTime)       !Browse key field - type derived from field
A_STAR:UID             LIKE(A_STAR:UID)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Statement Runs File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Select_StatementRuns_Alias'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~Run ID~C(0)@n_10@80L(2)|' & |
  'M~Run Description~@s35@50L(2)|M~Type~C(0)@s20@[48R(2)|M~Date~C(0)@d6@40R(2)|M~Time~C' & |
  '(0)@t7@]|M~Run~52R(2)|M~Statement No.~C(0)@n13@[48R(2)|M~Date~C(0)@d6@40R(2)|M~Entry' & |
  ' Time~C(0)@t7@]|M~Entry~'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Statement_Runs' & |
  '_Alias file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Statement Run ID'),USE(?Tab:2)
                         END
                         TAB('&2) By Run Date && Time'),USE(?Tab:3)
                         END
                         TAB('&3) By User'),USE(?Tab:4)
                         END
                         TAB('&4) By Run Description'),USE(?Tab:5)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 4
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_StatementRuns_Alias')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Type',LOC:Type)                                ! Added by: BrowseBox(ABC)
  BIND('LOC:Statement_No',LOC:Statement_No)                ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Statement_Runs_Alias.Open                         ! File Statement_Runs_Alias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Statement_Runs_Alias,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,A_STAR:Key_DateTime)                  ! Add the sort order for A_STAR:Key_DateTime for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,A_STAR:RunDateTime,1,BRW1)     ! Initialize the browse locator using  using key: A_STAR:Key_DateTime , A_STAR:RunDateTime
  BRW1.AddSortOrder(,A_STAR:FKey_UID)                      ! Add the sort order for A_STAR:FKey_UID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,A_STAR:UID,1,BRW1)             ! Initialize the browse locator using  using key: A_STAR:FKey_UID , A_STAR:UID
  BRW1.AddSortOrder(,A_STAR:FKey_RunDesc)                  ! Add the sort order for A_STAR:FKey_RunDesc for sort order 3
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort3:Locator.Init(,A_STAR:RunDescription,1,BRW1)  ! Initialize the browse locator using  using key: A_STAR:FKey_RunDesc , A_STAR:RunDescription
  BRW1.AddSortOrder(,A_STAR:PKey_STRID)                    ! Add the sort order for A_STAR:PKey_STRID for sort order 4
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 4
  BRW1::Sort0:Locator.Init(,A_STAR:STRID,1,BRW1)           ! Initialize the browse locator using  using key: A_STAR:PKey_STRID , A_STAR:STRID
  BRW1.AddField(A_STAR:STRID,BRW1.Q.A_STAR:STRID)          ! Field A_STAR:STRID is a hot field or requires assignment from browse
  BRW1.AddField(A_STAR:RunDescription,BRW1.Q.A_STAR:RunDescription) ! Field A_STAR:RunDescription is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Type,BRW1.Q.LOC:Type)                  ! Field LOC:Type is a hot field or requires assignment from browse
  BRW1.AddField(A_STAR:RunDate,BRW1.Q.A_STAR:RunDate)      ! Field A_STAR:RunDate is a hot field or requires assignment from browse
  BRW1.AddField(A_STAR:RunTime,BRW1.Q.A_STAR:RunTime)      ! Field A_STAR:RunTime is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Statement_No,BRW1.Q.LOC:Statement_No)  ! Field LOC:Statement_No is a hot field or requires assignment from browse
  BRW1.AddField(A_STAR:EntryDate,BRW1.Q.A_STAR:EntryDate)  ! Field A_STAR:EntryDate is a hot field or requires assignment from browse
  BRW1.AddField(A_STAR:EntryTime,BRW1.Q.A_STAR:EntryTime)  ! Field A_STAR:EntryTime is a hot field or requires assignment from browse
  BRW1.AddField(A_STAR:RunDateTime,BRW1.Q.A_STAR:RunDateTime) ! Field A_STAR:RunDateTime is a hot field or requires assignment from browse
  BRW1.AddField(A_STAR:UID,BRW1.Q.A_STAR:UID)              ! Field A_STAR:UID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_StatementRuns_Alias',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Statement_Runs_Alias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_StatementRuns_Alias',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! Client Monthly|Client Adhoc|Internal Adhoc (not on web)
      EXECUTE STAR:Type + 1
         LOC:Type     = 'Client Monthly'
         LOC:Type     = 'Client Ad Hoc'
         LOC:Type     = 'Internal Adhoc'
      ELSE
         LOC:Type     = ''
      .
  
  
      LOC:Statement_No    = Get_Statement_Run_Info(STAR:STRID)
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_Users_NonMDI PROCEDURE 

CurrentTab           STRING(80)                            ! 
BRW1::View:Browse    VIEW(Users)
                       PROJECT(USE:Login)
                       PROJECT(USE:Name)
                       PROJECT(USE:Surname)
                       PROJECT(USE:AccessLevel)
                       PROJECT(USE:Password)
                       PROJECT(USE:UID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
USE:Name               LIKE(USE:Name)                 !List box control field - type derived from field
USE:Surname            LIKE(USE:Surname)              !List box control field - type derived from field
USE:AccessLevel        LIKE(USE:AccessLevel)          !List box control field - type derived from field
USE:Password           LIKE(USE:Password)             !List box control field - type derived from field
USE:UID                LIKE(USE:UID)                  !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Users File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,HLP('Browse_Users'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Login~@s20@80L(2)|M~Name' & |
  '~@s35@80L(2)|M~Surname~@s35@52R(2)|M~Access Level~C(0)@n3b@80L(2)|M~Password~@s35@30' & |
  'R(2)|M~UID~L@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Users file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab),FONT('Tahoma',,,,CHARSET:DEFAULT)
                         TAB('&1) By Login'),USE(?Tab:2)
                         END
                         TAB('&2) By Name && Surname'),USE(?Tab:3)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Users_NonMDI')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Users.SetOpenRelated()
  Relate:Users.Open                                        ! File Users used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Users,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,USE:SKey_Name_Surname)                ! Add the sort order for USE:SKey_Name_Surname for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,USE:Name,1,BRW1)               ! Initialize the browse locator using  using key: USE:SKey_Name_Surname , USE:Name
  BRW1.AddSortOrder(,USE:Key_Login)                        ! Add the sort order for USE:Key_Login for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,USE:Login,1,BRW1)              ! Initialize the browse locator using  using key: USE:Key_Login , USE:Login
  BRW1.AddField(USE:Login,BRW1.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW1.AddField(USE:Name,BRW1.Q.USE:Name)                  ! Field USE:Name is a hot field or requires assignment from browse
  BRW1.AddField(USE:Surname,BRW1.Q.USE:Surname)            ! Field USE:Surname is a hot field or requires assignment from browse
  BRW1.AddField(USE:AccessLevel,BRW1.Q.USE:AccessLevel)    ! Field USE:AccessLevel is a hot field or requires assignment from browse
  BRW1.AddField(USE:Password,BRW1.Q.USE:Password)          ! Field USE:Password is a hot field or requires assignment from browse
  BRW1.AddField(USE:UID,BRW1.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_Users_NonMDI',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Users.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_Users_NonMDI',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      IF GLO:AccessLevel < 10
         USE:Password     = '**********'
      .
      IF GLO:AccessLevel < 5
         USE:AccessLevel  = ''
      .
  
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

