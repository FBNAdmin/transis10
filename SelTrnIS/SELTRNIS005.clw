

   MEMBER('SELTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SELTRNIS005.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_ReplicationIDControl PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::REP:Record  LIKE(REP:RECORD),THREAD
QuickWindow          WINDOW('Form Replication ID Control'),AT(,,273,177),FONT('Tahoma',8,,FONT:regular,CHARSET:DEFAULT), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('UpdateReplicationIDControl'),SYSTEM
                       SHEET,AT(4,4,266,153),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           STRING(@n_10),AT(166,6,104,10),USE(REP:RepCID),RIGHT(1),TRN
                           PROMPT('Replicated Database ID:'),AT(9,22),USE(?REP:ReplicatedDatabaseID:Prompt),TRN
                           ENTRY(@n_10),AT(105,22,104,10),USE(REP:ReplicatedDatabaseID),RIGHT(1),MSG('The ID of th' & |
  'e Replicated Database - used to decide auto numbering'),TIP('The ID of the Replicate' & |
  'd Database - used to decide auto numbering')
                           PROMPT('Site Description:'),AT(9,36),USE(?REP:SiteDescription:Prompt),TRN
                           TEXT,AT(105,36,161,71),USE(REP:SiteDescription),VSCROLL
                           PROMPT('Range From:'),AT(9,116),USE(?REP:RangeFrom:Prompt),TRN
                           SPIN(@n12),AT(105,116,104,10),USE(REP:RangeFrom),RIGHT(1)
                           PROMPT('Range To:'),AT(9,130),USE(?REP:RangeTo:Prompt),TRN
                           SPIN(@n12),AT(105,130,104,10),USE(REP:RangeTo),RIGHT(1)
                         END
                       END
                       BUTTON('&OK'),AT(166,160,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(220,160,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,160,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Rep. ID Control Record'
  OF InsertRecord
    ActionMessage = 'Rep. ID Control Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Rep. ID Control Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ReplicationIDControl')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REP:RepCID
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:ReplicationIDControl)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(REP:Record,History::REP:Record)
  SELF.AddHistoryField(?REP:RepCID,1)
  SELF.AddHistoryField(?REP:ReplicatedDatabaseID,2)
  SELF.AddHistoryField(?REP:SiteDescription,3)
  SELF.AddHistoryField(?REP:RangeFrom,4)
  SELF.AddHistoryField(?REP:RangeTo,5)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ReplicationIDControl.Open                         ! File ReplicationIDControl used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ReplicationIDControl
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?REP:ReplicatedDatabaseID{PROP:ReadOnly} = True
    ?REP:RangeFrom{PROP:ReadOnly} = True
    ?REP:RangeTo{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_ReplicationIDControl',QuickWindow)  ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ReplicationIDControl.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_ReplicationIDControl',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_ReplicationTableIDs PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(ReplicationTableIDs)
                       PROJECT(REPT:ReplicatedDatabaseID)
                       PROJECT(REPT:TableName)
                       PROJECT(REPT:LastID)
                       PROJECT(REPT:RepTID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
REPT:ReplicatedDatabaseID LIKE(REPT:ReplicatedDatabaseID) !List box control field - type derived from field
REPT:TableName         LIKE(REPT:TableName)           !List box control field - type derived from field
REPT:LastID            LIKE(REPT:LastID)              !List box control field - type derived from field
REPT:RepTID            LIKE(REPT:RepTID)              !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Replication Table IDs File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_ReplicationTableIDs'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('80R(1)|M~Replicated Database ID~C' & |
  '(0)@n_10@80L(2)|M~Table Name~@s255@80R(2)|M~Last ID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM, |
  MSG('Browsing the ReplicationTableIDs file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Replication DB ID && Table Name'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_ReplicationTableIDs')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:ReplicationTableIDs.Open                          ! File ReplicationTableIDs used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ReplicationTableIDs,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,REPT:Key_ReplicationDatabaseIDTableName) ! Add the sort order for REPT:Key_ReplicationDatabaseIDTableName for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,REPT:ReplicatedDatabaseID,1,BRW1) ! Initialize the browse locator using  using key: REPT:Key_ReplicationDatabaseIDTableName , REPT:ReplicatedDatabaseID
  BRW1.AddField(REPT:ReplicatedDatabaseID,BRW1.Q.REPT:ReplicatedDatabaseID) ! Field REPT:ReplicatedDatabaseID is a hot field or requires assignment from browse
  BRW1.AddField(REPT:TableName,BRW1.Q.REPT:TableName)      ! Field REPT:TableName is a hot field or requires assignment from browse
  BRW1.AddField(REPT:LastID,BRW1.Q.REPT:LastID)            ! Field REPT:LastID is a hot field or requires assignment from browse
  BRW1.AddField(REPT:RepTID,BRW1.Q.REPT:RepTID)            ! Field REPT:RepTID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_ReplicationTableIDs',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ReplicationTableIDs.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_ReplicationTableIDs',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_ReplicationTableIDs
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_ReplicationTableIDs PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::REPT:Record LIKE(REPT:RECORD),THREAD
QuickWindow          WINDOW('Form Replication Table IDs'),AT(,,358,84),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateReplicationTableIDs'),SYSTEM
                       SHEET,AT(4,4,350,58),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Replicated Database ID:'),AT(9,20),USE(?REPT:ReplicatedDatabaseID:Prompt),TRN
                           ENTRY(@n_10),AT(105,20,104,10),USE(REPT:ReplicatedDatabaseID),RIGHT(1),MSG('The ID of t' & |
  'he Replicated Database - used to decide auto numbering'),TIP('The ID of the Replicat' & |
  'ed Database - used to decide auto numbering')
                           PROMPT('Table Name:'),AT(9,34),USE(?REPT:TableName:Prompt),TRN
                           ENTRY(@s255),AT(105,34,246,10),USE(REPT:TableName),REQ
                           PROMPT('Last ID:'),AT(9,48),USE(?REPT:LastID:Prompt),TRN
                           STRING(@n_10),AT(105,48,104,10),USE(REPT:LastID),RIGHT(1),TRN
                         END
                       END
                       BUTTON('&OK'),AT(250,66,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(304,66,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,66,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Fuel Surcharge Record'
  OF InsertRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ReplicationTableIDs')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REPT:ReplicatedDatabaseID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:ReplicationTableIDs)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(REPT:Record,History::REPT:Record)
  SELF.AddHistoryField(?REPT:ReplicatedDatabaseID,2)
  SELF.AddHistoryField(?REPT:TableName,3)
  SELF.AddHistoryField(?REPT:LastID,4)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ReplicationTableIDs.Open                          ! File ReplicationTableIDs used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ReplicationTableIDs
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?REPT:ReplicatedDatabaseID{PROP:ReadOnly} = True
    ?REPT:TableName{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_ReplicationTableIDs',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ReplicationTableIDs.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_ReplicationTableIDs',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_UserGroups PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(UserGroups)
                       PROJECT(USEG:GroupName)
                       PROJECT(USEG:UGID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the User Groups File'),AT(,,330,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Browse_UserGroups'),SYSTEM
                       LIST,AT(8,30,314,124),USE(?Browse:1),HVSCROLL,FORMAT('144L(2)|M~Group Name~@s35@42R(2)|' & |
  'M~UG ID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the UserGroups file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,322,172),USE(?CurrentTab)
                         TAB('&1) By Group Name'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(277,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_UserGroups')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:UserGroups.Open                                   ! File UserGroups used by this procedure, so make sure it's RelationManager is open
  Access:UsersGroups.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:UserGroups,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,USEG:PKey_UGID)                       ! Add the sort order for USEG:PKey_UGID for sort order 1
  BRW1.AddRange(USEG:UGID,Relate:UserGroups,Relate:UsersGroups) ! Add file relationship range limit for sort order 1
  BRW1.AddSortOrder(,USEG:Key_GroupName)                   ! Add the sort order for USEG:Key_GroupName for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,USEG:GroupName,1,BRW1)         ! Initialize the browse locator using  using key: USEG:Key_GroupName , USEG:GroupName
  BRW1.AddField(USEG:GroupName,BRW1.Q.USEG:GroupName)      ! Field USEG:GroupName is a hot field or requires assignment from browse
  BRW1.AddField(USEG:UGID,BRW1.Q.USEG:UGID)                ! Field USEG:UGID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_UserGroups',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:UserGroups.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_UserGroups',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_DeliveryItems PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(DeliveryItems)
                       PROJECT(DELI:ItemNo)
                       PROJECT(DELI:Type)
                       PROJECT(DELI:ByContainer)
                       PROJECT(DELI:ShowOnInvoice)
                       PROJECT(DELI:ContainerNo)
                       PROJECT(DELI:ContainerVessel)
                       PROJECT(DELI:SealNo)
                       PROJECT(DELI:ETA)
                       PROJECT(DELI:DIID)
                       PROJECT(DELI:DID)
                       PROJECT(DELI:PTID)
                       PROJECT(DELI:CTID)
                       PROJECT(DELI:CMID)
                       JOIN(PACK:PKey_PTID,DELI:PTID)
                         PROJECT(PACK:Packaging)
                         PROJECT(PACK:PTID)
                       END
                       JOIN(CTYP:PKey_CTID,DELI:CTID)
                         PROJECT(CTYP:ContainerType)
                         PROJECT(CTYP:CTID)
                       END
                       JOIN(COM:PKey_CMID,DELI:CMID)
                         PROJECT(COM:Commodity)
                         PROJECT(COM:CMID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Type              LIKE(DELI:Type)                !List box control field - type derived from field
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
PACK:Packaging         LIKE(PACK:Packaging)           !List box control field - type derived from field
DELI:ByContainer       LIKE(DELI:ByContainer)         !List box control field - type derived from field
DELI:ByContainer_Icon  LONG                           !Entry's icon ID
DELI:ShowOnInvoice     LIKE(DELI:ShowOnInvoice)       !List box control field - type derived from field
DELI:ShowOnInvoice_Icon LONG                          !Entry's icon ID
CTYP:ContainerType     LIKE(CTYP:ContainerType)       !List box control field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !List box control field - type derived from field
DELI:ContainerVessel   LIKE(DELI:ContainerVessel)     !List box control field - type derived from field
DELI:SealNo            LIKE(DELI:SealNo)              !List box control field - type derived from field
DELI:ETA               LIKE(DELI:ETA)                 !List box control field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Primary key field - type derived from field
DELI:DID               LIKE(DELI:DID)                 !Browse key field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Related join file key field - type derived from field
CTYP:CTID              LIKE(CTYP:CTID)                !Related join file key field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Delivery Items file'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MAX,MDI,HLP('Select_DeliveryItems'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('32R(2)|M~Item No.~C(0)@n_5@22R(2)' & |
  '|M~Type~C(0)@n3@60L(2)|M~Commodity~C(0)@s35@60L(2)|M~Packaging~C(0)@s35@[25R(2)|MI~C' & |
  'ontainer~L(1)@p p@25R(2)|MI~Show On Invoice~L(1)@p p@60L(2)|M~Container Type~C(0)@s3' & |
  '5@60L(2)|M~Container No.~@s35@60L(2)|M~Container Vessel~@s35@60L(2)|M~Seal No.~@s35@' & |
  '40R(2)|M~ETA~C(0)@d5@]|M~Container~'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Del' & |
  'iveryItems file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Delivery ID && Item No.'),USE(?Tab:2)
                           BUTTON('Select Delivery'),AT(9,158,118,14),USE(?SelectDeliveries),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_DeliveryItems')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:DeliveryItems,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,DELI:FKey_DID_ItemNo)                 ! Add the sort order for DELI:FKey_DID_ItemNo for sort order 1
  BRW1.AddRange(DELI:DID,Relate:DeliveryItems,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,DELI:ItemNo,1,BRW1)            ! Initialize the browse locator using  using key: DELI:FKey_DID_ItemNo , DELI:ItemNo
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(DELI:ItemNo,BRW1.Q.DELI:ItemNo)            ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW1.AddField(DELI:Type,BRW1.Q.DELI:Type)                ! Field DELI:Type is a hot field or requires assignment from browse
  BRW1.AddField(COM:Commodity,BRW1.Q.COM:Commodity)        ! Field COM:Commodity is a hot field or requires assignment from browse
  BRW1.AddField(PACK:Packaging,BRW1.Q.PACK:Packaging)      ! Field PACK:Packaging is a hot field or requires assignment from browse
  BRW1.AddField(DELI:ByContainer,BRW1.Q.DELI:ByContainer)  ! Field DELI:ByContainer is a hot field or requires assignment from browse
  BRW1.AddField(DELI:ShowOnInvoice,BRW1.Q.DELI:ShowOnInvoice) ! Field DELI:ShowOnInvoice is a hot field or requires assignment from browse
  BRW1.AddField(CTYP:ContainerType,BRW1.Q.CTYP:ContainerType) ! Field CTYP:ContainerType is a hot field or requires assignment from browse
  BRW1.AddField(DELI:ContainerNo,BRW1.Q.DELI:ContainerNo)  ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW1.AddField(DELI:ContainerVessel,BRW1.Q.DELI:ContainerVessel) ! Field DELI:ContainerVessel is a hot field or requires assignment from browse
  BRW1.AddField(DELI:SealNo,BRW1.Q.DELI:SealNo)            ! Field DELI:SealNo is a hot field or requires assignment from browse
  BRW1.AddField(DELI:ETA,BRW1.Q.DELI:ETA)                  ! Field DELI:ETA is a hot field or requires assignment from browse
  BRW1.AddField(DELI:DIID,BRW1.Q.DELI:DIID)                ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW1.AddField(DELI:DID,BRW1.Q.DELI:DID)                  ! Field DELI:DID is a hot field or requires assignment from browse
  BRW1.AddField(PACK:PTID,BRW1.Q.PACK:PTID)                ! Field PACK:PTID is a hot field or requires assignment from browse
  BRW1.AddField(CTYP:CTID,BRW1.Q.CTYP:CTID)                ! Field CTYP:CTID is a hot field or requires assignment from browse
  BRW1.AddField(COM:CMID,BRW1.Q.COM:CMID)                  ! Field COM:CMID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_DeliveryItems',QuickWindow)         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_DeliveryItems',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectDeliveries
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Deliveries()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (DELI:ByContainer = 1)
    SELF.Q.DELI:ByContainer_Icon = 2                       ! Set icon from icon list
  ELSE
    SELF.Q.DELI:ByContainer_Icon = 1                       ! Set icon from icon list
  END
  IF (DELI:ShowOnInvoice = 1)
    SELF.Q.DELI:ShowOnInvoice_Icon = 2                     ! Set icon from icon list
  ELSE
    SELF.Q.DELI:ShowOnInvoice_Icon = 1                     ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

