

   MEMBER('SELTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SELTRNIS002.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_Branches PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                       PROJECT(BRA:AID)
                       PROJECT(BRA:FID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !List box control field - type derived from field
BRA:AID                LIKE(BRA:AID)                  !Browse key field - type derived from field
BRA:FID                LIKE(BRA:FID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Branches File'),AT(,,168,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Select_Branches'),SYSTEM
                       LIST,AT(8,30,152,124),USE(?Browse:1),HVSCROLL,FORMAT('100L(2)|M~Branch Name~@s35@40R(2)' & |
  '|M~BID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Branches file')
                       BUTTON('&Select'),AT(111,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,160,172),USE(?CurrentTab),JOIN
                         TAB('&1) By Branch Name'),USE(?Tab:2)
                         END
                         TAB('&3) By Floor'),USE(?Tab:4)
                         END
                       END
                       BUTTON('&Close'),AT(115,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW1::Sort2:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Branches')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Branches,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon BRA:AID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,BRA:FKey_AID)    ! Add the sort order for BRA:FKey_AID for sort order 1
  BRW1.AddRange(BRA:AID,Relate:Branches,Relate:Addresses)  ! Add file relationship range limit for sort order 1
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon BRA:FID for sort order 2
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,BRA:FKey_FID)    ! Add the sort order for BRA:FKey_FID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,BRA:FID,1,BRW1)                ! Initialize the browse locator using  using key: BRA:FKey_FID , BRA:FID
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon BRA:BranchName for sort order 3
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,BRA:Key_BranchName) ! Add the sort order for BRA:Key_BranchName for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(,BRA:BranchName,1,BRW1)         ! Initialize the browse locator using  using key: BRA:Key_BranchName , BRA:BranchName
  BRW1.AddField(BRA:BranchName,BRW1.Q.BRA:BranchName)      ! Field BRA:BranchName is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BID,BRW1.Q.BRA:BID)                    ! Field BRA:BID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:AID,BRW1.Q.BRA:AID)                    ! Field BRA:AID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:FID,BRW1.Q.BRA:FID)                    ! Field BRA:FID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_Branches',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_Branches',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_Deliveries PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:Locator          STRING(20)                            !
BRW1::View:Browse    VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:DocumentCharge)
                       PROJECT(DEL:FuelSurcharge)
                       PROJECT(DEL:Charge)
                       PROJECT(DEL:AdditionalCharge)
                       PROJECT(DEL:VATRate)
                       PROJECT(DEL:Rate)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:BID)
                       PROJECT(DEL:CID)
                       PROJECT(DEL:JID)
                       PROJECT(DEL:CollectionAID)
                       PROJECT(DEL:DeliveryAID)
                       PROJECT(DEL:LTID)
                       PROJECT(DEL:SID)
                       PROJECT(DEL:TripSheetInTID)
                       PROJECT(DEL:TripSheetOutTID)
                       PROJECT(DEL:FID)
                       PROJECT(DEL:DC_ID)
                       PROJECT(DEL:DELCID)
                       PROJECT(DEL:CRTID)
                       PROJECT(DEL:UID)
                       JOIN(JOU:PKey_JID,DEL:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                       JOIN(FLO:PKey_FID,DEL:FID)
                         PROJECT(FLO:Floor)
                         PROJECT(FLO:FID)
                       END
                       JOIN(BRA:PKey_BID,DEL:BID)
                         PROJECT(BRA:BranchName)
                         PROJECT(BRA:BID)
                       END
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
DEL:DIDate             LIKE(DEL:DIDate)               !List box control field - type derived from field
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
DEL:DIDate             LIKE(DEL:DIDate)               !List box control field - type derived from field - type derived from field
DEL:DocumentCharge     LIKE(DEL:DocumentCharge)       !List box control field - type derived from field
DEL:FuelSurcharge      LIKE(DEL:FuelSurcharge)        !List box control field - type derived from field
DEL:Charge             LIKE(DEL:Charge)               !List box control field - type derived from field
DEL:AdditionalCharge   LIKE(DEL:AdditionalCharge)     !List box control field - type derived from field
DEL:VATRate            LIKE(DEL:VATRate)              !List box control field - type derived from field
DEL:Rate               LIKE(DEL:Rate)                 !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Primary key field - type derived from field
DEL:BID                LIKE(DEL:BID)                  !Browse key field - type derived from field
DEL:CID                LIKE(DEL:CID)                  !Browse key field - type derived from field
DEL:JID                LIKE(DEL:JID)                  !Browse key field - type derived from field
DEL:CollectionAID      LIKE(DEL:CollectionAID)        !Browse key field - type derived from field
DEL:DeliveryAID        LIKE(DEL:DeliveryAID)          !Browse key field - type derived from field
DEL:LTID               LIKE(DEL:LTID)                 !Browse key field - type derived from field
DEL:SID                LIKE(DEL:SID)                  !Browse key field - type derived from field
DEL:TripSheetInTID     LIKE(DEL:TripSheetInTID)       !Browse key field - type derived from field
DEL:TripSheetOutTID    LIKE(DEL:TripSheetOutTID)      !Browse key field - type derived from field
DEL:FID                LIKE(DEL:FID)                  !Browse key field - type derived from field
DEL:DC_ID              LIKE(DEL:DC_ID)                !Browse key field - type derived from field
DEL:DELCID             LIKE(DEL:DELCID)               !Browse key field - type derived from field
DEL:CRTID              LIKE(DEL:CRTID)                !Browse key field - type derived from field
DEL:UID                LIKE(DEL:UID)                  !Browse key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Related join file key field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Related join file key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Deliveries File'),AT(,,460,218),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('Select_Deliveries'),SYSTEM
                       LIST,AT(8,32,443,142),USE(?Browse:1),HVSCROLL,FORMAT('36R(2)|FM~DI No.~C(0)@n_10@69L(2)' & |
  '|M~Client~C(0)@s35@50L(2)|M~Client Ref.~C(0)@s30@38R(2)|M~DI Date~C(0)@d5b@40L(2)|M~' & |
  'Branch~C(0)@s35@36L(2)|M~Floor~C(0)@s35@40R(2)|M~DI Date~C(0)@d5b@[44R(2)|M~Docs~C(0' & |
  ')@n-11.2@44R(2)|M~Fuel~C(0)@n-11.2@60R(2)|M~Freight~C(0)@n-15.2@60R(2)|M~Additional~' & |
  'C(0)@n-15.2@38R(2)|M~VAT Rate~C(0)@n7.2@38R(2)|M~Rate~C(0)@n-13.4@]|M~Charges~70L(2)' & |
  '|M~Journey~C(0)@s70@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Deliveries file')
                       BUTTON('&Select'),AT(402,178,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       GROUP,AT(8,20,97,10),USE(?Group_Locator)
                         PROMPT('Locator:'),AT(8,20),USE(?LOC:Locator:Prompt),TRN
                         ENTRY(@s20),AT(42,20,60,10),USE(LOC:Locator),TRN
                       END
                       SHEET,AT(4,4,452,192),USE(?CurrentTab)
                         TAB('&1) By DI No.'),USE(?Tab:2)
                         END
                         TAB('&2) By Client Ref.'),USE(?Tab:3)
                         END
                         TAB('&3) By Branch'),USE(?Tab:4)
                           BUTTON('Select Branches'),AT(9,178,,14),USE(?SelectBranches),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&4) By Client'),USE(?Tab:5)
                           BUTTON('Select Clients'),AT(9,178,,14),USE(?SelectClients),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&5) By Journey'),USE(?Tab:6)
                           BUTTON('Select Journeys'),AT(9,178,,14),USE(?SelectJourneys),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&6) By Load Type'),USE(?Tab:9)
                         END
                         TAB('&7) By Service'),USE(?Tab:10)
                           BUTTON('Select Service Requirements'),AT(9,178,118,14),USE(?SelectServiceRequirements),LEFT, |
  ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&8) By Floor'),USE(?Tab:13)
                           BUTTON('Select Floors'),AT(9,178,,14),USE(?SelectFloors),LEFT,ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                         END
                         TAB('&9) By User'),USE(?Tab:17)
                           BUTTON('Select Users'),AT(9,178,,14),USE(?SelectUsers),LEFT,ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(407,200,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,200,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort7:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 8
BRW1::Sort9:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 10
BRW1::Sort10:Locator StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 11
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Deliveries')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:AddressAlias.Open                                 ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Branches.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ClientsRateTypes.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryComposition.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Delivery_CODAddresses.UseFile                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Floors.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ServiceRequirements.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,DEL:SKey_ClientReference)             ! Add the sort order for DEL:SKey_ClientReference for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(?LOC:Locator,DEL:ClientReference,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: DEL:SKey_ClientReference , DEL:ClientReference
  BRW1.AddSortOrder(,DEL:FKey_BID)                         ! Add the sort order for DEL:FKey_BID for sort order 2
  BRW1.AddRange(DEL:BID,Relate:Deliveries,Relate:Branches) ! Add file relationship range limit for sort order 2
  BRW1.AddSortOrder(,DEL:FKey_CID)                         ! Add the sort order for DEL:FKey_CID for sort order 3
  BRW1.AddRange(DEL:CID,Relate:Deliveries,Relate:Clients)  ! Add file relationship range limit for sort order 3
  BRW1.AddSortOrder(,DEL:FKey_JID)                         ! Add the sort order for DEL:FKey_JID for sort order 4
  BRW1.AddRange(DEL:JID,Relate:Deliveries,Relate:Journeys) ! Add file relationship range limit for sort order 4
  BRW1.AddSortOrder(,DEL:FKey_CollectionAID)               ! Add the sort order for DEL:FKey_CollectionAID for sort order 5
  BRW1.AddRange(DEL:CollectionAID,Relate:Deliveries,Relate:Addresses) ! Add file relationship range limit for sort order 5
  BRW1.AddSortOrder(,DEL:FKey_DeliveryAID)                 ! Add the sort order for DEL:FKey_DeliveryAID for sort order 6
  BRW1.AddRange(DEL:DeliveryAID,Relate:Deliveries,Relate:AddressAlias) ! Add file relationship range limit for sort order 6
  BRW1.AddSortOrder(,DEL:FKey_LTID)                        ! Add the sort order for DEL:FKey_LTID for sort order 7
  BRW1.AddLocator(BRW1::Sort7:Locator)                     ! Browse has a locator for sort order 7
  BRW1::Sort7:Locator.Init(,DEL:LTID,1,BRW1)               ! Initialize the browse locator using  using key: DEL:FKey_LTID , DEL:LTID
  BRW1.AddSortOrder(,DEL:FKey_SID)                         ! Add the sort order for DEL:FKey_SID for sort order 8
  BRW1.AddRange(DEL:SID,Relate:Deliveries,Relate:ServiceRequirements) ! Add file relationship range limit for sort order 8
  BRW1.AddSortOrder(,DEL:FKey_TripSheetInTID)              ! Add the sort order for DEL:FKey_TripSheetInTID for sort order 9
  BRW1.AddLocator(BRW1::Sort9:Locator)                     ! Browse has a locator for sort order 9
  BRW1::Sort9:Locator.Init(,DEL:TripSheetInTID,1,BRW1)     ! Initialize the browse locator using  using key: DEL:FKey_TripSheetInTID , DEL:TripSheetInTID
  BRW1.AddSortOrder(,DEL:FKey_TripSheetOutTID)             ! Add the sort order for DEL:FKey_TripSheetOutTID for sort order 10
  BRW1.AddLocator(BRW1::Sort10:Locator)                    ! Browse has a locator for sort order 10
  BRW1::Sort10:Locator.Init(,DEL:TripSheetOutTID,1,BRW1)   ! Initialize the browse locator using  using key: DEL:FKey_TripSheetOutTID , DEL:TripSheetOutTID
  BRW1.AddSortOrder(,DEL:FKey_FID)                         ! Add the sort order for DEL:FKey_FID for sort order 11
  BRW1.AddRange(DEL:FID,Relate:Deliveries,Relate:Floors)   ! Add file relationship range limit for sort order 11
  BRW1.AddSortOrder(,DEL:FKey_DCID)                        ! Add the sort order for DEL:FKey_DCID for sort order 12
  BRW1.AddRange(DEL:DC_ID,Relate:Deliveries,Relate:Delivery_CODAddresses) ! Add file relationship range limit for sort order 12
  BRW1.AddSortOrder(,DEL:FKey_DELCID)                      ! Add the sort order for DEL:FKey_DELCID for sort order 13
  BRW1.AddRange(DEL:DELCID,Relate:Deliveries,Relate:DeliveryComposition) ! Add file relationship range limit for sort order 13
  BRW1.AddSortOrder(,DEL:FKey_CRTID)                       ! Add the sort order for DEL:FKey_CRTID for sort order 14
  BRW1.AddRange(DEL:CRTID,Relate:Deliveries,Relate:ClientsRateTypes) ! Add file relationship range limit for sort order 14
  BRW1.AddSortOrder(,DEL:FKey_UID)                         ! Add the sort order for DEL:FKey_UID for sort order 15
  BRW1.AddRange(DEL:UID,Relate:Deliveries,Relate:Users)    ! Add file relationship range limit for sort order 15
  BRW1.AddSortOrder(,DEL:Key_DINo)                         ! Add the sort order for DEL:Key_DINo for sort order 16
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 16
  BRW1::Sort0:Locator.Init(?LOC:Locator,DEL:DINo,1,BRW1)   ! Initialize the browse locator using ?LOC:Locator using key: DEL:Key_DINo , DEL:DINo
  BRW1.AddField(DEL:DINo,BRW1.Q.DEL:DINo)                  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientName,BRW1.Q.CLI:ClientName)      ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW1.AddField(DEL:ClientReference,BRW1.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DIDate,BRW1.Q.DEL:DIDate)              ! Field DEL:DIDate is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BranchName,BRW1.Q.BRA:BranchName)      ! Field BRA:BranchName is a hot field or requires assignment from browse
  BRW1.AddField(FLO:Floor,BRW1.Q.FLO:Floor)                ! Field FLO:Floor is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DIDate,BRW1.Q.DEL:DIDate)              ! Field DEL:DIDate is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DocumentCharge,BRW1.Q.DEL:DocumentCharge) ! Field DEL:DocumentCharge is a hot field or requires assignment from browse
  BRW1.AddField(DEL:FuelSurcharge,BRW1.Q.DEL:FuelSurcharge) ! Field DEL:FuelSurcharge is a hot field or requires assignment from browse
  BRW1.AddField(DEL:Charge,BRW1.Q.DEL:Charge)              ! Field DEL:Charge is a hot field or requires assignment from browse
  BRW1.AddField(DEL:AdditionalCharge,BRW1.Q.DEL:AdditionalCharge) ! Field DEL:AdditionalCharge is a hot field or requires assignment from browse
  BRW1.AddField(DEL:VATRate,BRW1.Q.DEL:VATRate)            ! Field DEL:VATRate is a hot field or requires assignment from browse
  BRW1.AddField(DEL:Rate,BRW1.Q.DEL:Rate)                  ! Field DEL:Rate is a hot field or requires assignment from browse
  BRW1.AddField(JOU:Journey,BRW1.Q.JOU:Journey)            ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DID,BRW1.Q.DEL:DID)                    ! Field DEL:DID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:BID,BRW1.Q.DEL:BID)                    ! Field DEL:BID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:CID,BRW1.Q.DEL:CID)                    ! Field DEL:CID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:JID,BRW1.Q.DEL:JID)                    ! Field DEL:JID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:CollectionAID,BRW1.Q.DEL:CollectionAID) ! Field DEL:CollectionAID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DeliveryAID,BRW1.Q.DEL:DeliveryAID)    ! Field DEL:DeliveryAID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:LTID,BRW1.Q.DEL:LTID)                  ! Field DEL:LTID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:SID,BRW1.Q.DEL:SID)                    ! Field DEL:SID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:TripSheetInTID,BRW1.Q.DEL:TripSheetInTID) ! Field DEL:TripSheetInTID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:TripSheetOutTID,BRW1.Q.DEL:TripSheetOutTID) ! Field DEL:TripSheetOutTID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:FID,BRW1.Q.DEL:FID)                    ! Field DEL:FID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DC_ID,BRW1.Q.DEL:DC_ID)                ! Field DEL:DC_ID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DELCID,BRW1.Q.DEL:DELCID)              ! Field DEL:DELCID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:CRTID,BRW1.Q.DEL:CRTID)                ! Field DEL:CRTID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:UID,BRW1.Q.DEL:UID)                    ! Field DEL:UID is a hot field or requires assignment from browse
  BRW1.AddField(JOU:JID,BRW1.Q.JOU:JID)                    ! Field JOU:JID is a hot field or requires assignment from browse
  BRW1.AddField(FLO:FID,BRW1.Q.FLO:FID)                    ! Field FLO:FID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BID,BRW1.Q.BRA:BID)                    ! Field BRA:BID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:CID,BRW1.Q.CLI:CID)                    ! Field CLI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_Deliveries',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_Deliveries',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Branches()
      ThisWindow.Reset
    OF ?SelectClients
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Clients()
      ThisWindow.Reset
    OF ?SelectJourneys
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Journey()
      ThisWindow.Reset
    OF ?SelectServiceRequirements
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_ServiceRequirements()
      ThisWindow.Reset
    OF ?SelectFloors
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Floors()
      ThisWindow.Reset
    OF ?SelectUsers
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Users()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?CurrentTab) = 6
    RETURN SELF.SetSort(5,Force)
  ELSIF CHOICE(?CurrentTab) = 7
    RETURN SELF.SetSort(6,Force)
  ELSIF CHOICE(?CurrentTab) = 8
    RETURN SELF.SetSort(7,Force)
  ELSIF CHOICE(?CurrentTab) = 9
    RETURN SELF.SetSort(8,Force)
  ELSIF CHOICE(?CurrentTab) = 10
    RETURN SELF.SetSort(9,Force)
  ELSIF CHOICE(?CurrentTab) = 11
    RETURN SELF.SetSort(10,Force)
  ELSIF CHOICE(?CurrentTab) = 12
    RETURN SELF.SetSort(11,Force)
  ELSIF CHOICE(?CurrentTab) = 13
    RETURN SELF.SetSort(12,Force)
  ELSIF CHOICE(?CurrentTab) = 14
    RETURN SELF.SetSort(13,Force)
  ELSIF CHOICE(?CurrentTab) = 15
    RETURN SELF.SetSort(14,Force)
  ELSIF CHOICE(?CurrentTab) = 16
    RETURN SELF.SetSort(15,Force)
  ELSE
    RETURN SELF.SetSort(16,Force)
  END
    CASE CHOICE(?CurrentTab)
    OF 1 OROF 2
       UNHIDE(?Group_Locator)
    ELSE
       HIDE(?Group_Locator)
    .
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_DeliveryStatuses PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(DeliveryStatuses)
                       PROJECT(DELS:DeliveryStatus)
                       PROJECT(DELS:DSID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DELS:DeliveryStatus    LIKE(DELS:DeliveryStatus)      !List box control field - type derived from field
DELS:DSID              LIKE(DELS:DSID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Delivery Status Record'),AT(,,158,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('SelectDeliveryStatuses'),SYSTEM
                       LIST,AT(8,30,142,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Delivery Status~L(2)@s35@'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing the DeliveryStatuses file')
                       BUTTON('&Select'),AT(101,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,150,172),USE(?CurrentTab)
                         TAB('&1) By Status'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(105,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_DeliveryStatuses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:DeliveryStatuses.Open                             ! File DeliveryStatuses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:DeliveryStatuses,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,DELS:Key_Status)                      ! Add the sort order for DELS:Key_Status for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,DELS:DeliveryStatus,1,BRW1)    ! Initialize the browse locator using  using key: DELS:Key_Status , DELS:DeliveryStatus
  BRW1.AddField(DELS:DeliveryStatus,BRW1.Q.DELS:DeliveryStatus) ! Field DELS:DeliveryStatus is a hot field or requires assignment from browse
  BRW1.AddField(DELS:DSID,BRW1.Q.DELS:DSID)                ! Field DELS:DSID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_DeliveryStatuses',QuickWindow)      ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryStatuses.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_DeliveryStatuses',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! --- filtering done on client - slow - make temp table?  whenever rates changed for client delete temp records?
!!! </summary>
Select_Journey PROCEDURE (p:CID, p:LTID, p:FID)

CurrentTab           STRING(80)                            !
LOC:Locator          STRING(35)                            !
LOC:CID              ULONG                                 !Client ID
LOC:ContainerParkSpecial BYTE                              !
LOC:This_Client      BYTE                                  !This Client, Default Client, All Clients
LOC:Setup_CID        ULONG                                 !Client ID
BRW1::View:Browse    VIEW(Journeys)
                       PROJECT(JOU:Journey)
                       PROJECT(JOU:Description)
                       PROJECT(JOU:JID)
                       PROJECT(JOU:BID)
                       JOIN(_View_Rates_Client_Journeys,'V_RCJ:JID = JOU:JID AND LOC:CID = V_RCJ:CID AND LOC:CID <<> 0')
                       END
                       JOIN(BRA:PKey_BID,JOU:BID)
                         PROJECT(BRA:BranchName)
                         PROJECT(BRA:BID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
JOU:Description        LIKE(JOU:Description)          !List box control field - type derived from field
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !List box control field - type derived from field
JOU:BID                LIKE(JOU:BID)                  !Browse key field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Journeys File'),AT(,,298,206),FONT('Tahoma',8),RESIZE,GRAY,IMM,MDI,HLP('BrowseJourneys'), |
  SYSTEM
                       LIST,AT(8,28,282,135),USE(?Browse:1),HVSCROLL,FORMAT('200L(2)|M~Journey~200L(2)|M~Descr' & |
  'iption~60L(2)|M~Branch Name~C(0)@s35@30R(2)|M~JID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM, |
  MSG('Browsing Records')
                       BUTTON('&Select'),AT(237,166,,14),USE(?Select:2),LEFT,ICON('WAselect.ICO'),FLAT
                       SHEET,AT(4,4,290,180),USE(?CurrentTab)
                         TAB('By Journey'),USE(?Tab:2)
                           PROMPT('Locator:'),AT(9,18),USE(?LOC:Locator:Prompt),TRN
                           STRING(@s35),AT(38,18),USE(LOC:Locator),TRN
                           GROUP,AT(5,193,157,10),USE(?Group_CLient)
                             PROMPT('This Client:'),AT(5,193),USE(?LOC:This_Client:Prompt)
                             LIST,AT(46,193,117,9),USE(LOC:This_Client),DROP(5),FROM('This Client|#1|Default Client|' & |
  '#2|All Clients|#0'),MSG('This Client, Default Client, All Clients'),TIP('This Client' & |
  ', Default Client, All Clients')
                           END
                         END
                         TAB('By Branch'),USE(?Tab:3),HIDE
                           BUTTON('Branch'),AT(9,167,,14),USE(?SelectBranches),LEFT,ICON('WAPARENT.ICO'),FLAT
                         END
                       END
                       BUTTON('Close'),AT(241,188,,14),USE(?Close),LEFT,ICON('WAclose.ico'),FLAT
                       BUTTON('Help'),AT(240,2,,14),USE(?Help),LEFT,ICON('WAhelp.ico'),FLAT,HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Journey')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:CID',LOC:CID)                                  ! Added by: BrowseBox(ABC)
  BIND('LOC:ContainerParkSpecial',LOC:ContainerParkSpecial) ! Added by: BrowseBox(ABC)
  BIND('GLO:BranchID',GLO:BranchID)                        ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:ClientsRateTypesAlias.Open                        ! File ClientsRateTypesAlias used by this procedure, so make sure it's RelationManager is open
  Relate:LoadTypes2Alias.Open                              ! File LoadTypes2Alias used by this procedure, so make sure it's RelationManager is open
  Relate:_View_ContainerParkRates_Client.Open              ! File _View_ContainerParkRates_Client used by this procedure, so make sure it's RelationManager is open
  Relate:_View_Rates_Client.Open                           ! File _View_Rates_Client used by this procedure, so make sure it's RelationManager is open
  Relate:_View_Rates_Client_Journeys.Open                  ! File _View_Rates_Client_Journeys used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Journeys,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon JOU:BID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,JOU:FKey_BID)    ! Add the sort order for JOU:FKey_BID for sort order 1
  BRW1.AddRange(JOU:BID,Relate:Journeys,Relate:Branches)   ! Add file relationship range limit for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(?LOC:Locator,JOU:BID,1,BRW1)    ! Initialize the browse locator using ?LOC:Locator using key: JOU:FKey_BID , JOU:BID
  BRW1.AppendOrder('+JOU:Journey,+JOU:JID')                ! Append an additional sort order
  BRW1.AddResetField(LOC:This_Client)                      ! Apply the reset field
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon JOU:Journey for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,JOU:Key_Journey) ! Add the sort order for JOU:Key_Journey for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(?LOC:Locator,JOU:Journey,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: JOU:Key_Journey , JOU:Journey
  BRW1.AppendOrder('+JOU:JID')                             ! Append an additional sort order
  BRW1.SetFilter('(LOC:CID = 0 OR V_RCJ:JID = JOU:JID OR LOC:ContainerParkSpecial = 1)') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:CID)                              ! Apply the reset field
  BRW1.AddResetField(LOC:This_Client)                      ! Apply the reset field
  BRW1.AddField(JOU:Journey,BRW1.Q.JOU:Journey)            ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW1.AddField(JOU:Description,BRW1.Q.JOU:Description)    ! Field JOU:Description is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BranchName,BRW1.Q.BRA:BranchName)      ! Field BRA:BranchName is a hot field or requires assignment from browse
  BRW1.AddField(JOU:JID,BRW1.Q.JOU:JID)                    ! Field JOU:JID is a hot field or requires assignment from browse
  BRW1.AddField(JOU:BID,BRW1.Q.JOU:BID)                    ! Field JOU:BID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BID,BRW1.Q.BRA:BID)                    ! Field BRA:BID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_Journey',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      BRA:BID     = GLO:BranchID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
      .
      IF p:CID = 0
         HIDE(?LOC:This_Client)
      ELSE
         LOC:This_Client    = TRUE
         UNHIDE(?LOC:This_Client)
         LOC:CID            = p:CID
      .
      CLEAR(A_LOAD2:Record)
      IF p:LTID ~= 0     ! (p:FID, p:JID, p:Eff_Date, p:Mass, p:Rate)
         A_LOAD2:LTID          = p:LTID
         IF Access:LoadTypes2Alias.TryFetch(A_LOAD2:PKey_LTID) = LEVEL:Benign
            IF A_LOAD2:ContainerParkStandard = TRUE
               LOC:ContainerParkSpecial   = TRUE
      .  .  .
      LOC:Setup_CID   = Get_Branch_Info(,4)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:ClientsRateTypesAlias.Close
    Relate:LoadTypes2Alias.Close
    Relate:_View_ContainerParkRates_Client.Close
    Relate:_View_Rates_Client.Close
    Relate:_View_Rates_Client_Journeys.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_Journey',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
    IF SELF.Request = SelectRecord
       IF Request = InsertRecord
          IF ReturnValue = RequestCompleted
             ! Then assume the user has added a Client they want to Select
             IF JOU:JID ~= 0
                POST(EVENT:Accepted, ?Select:2)
    .  .  .  .
  
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LOC:This_Client
      !    ! Report this as a bug to SV
      !    IF RECORDS(Queue:Browse:1) = 0
      !       BRW1.ResetQueue(1)
      !    .
      
          CASE LOC:This_Client
          OF 0    ! All
             LOC:CID  = 0
          OF 1    ! Passed
             LOC:CID  = p:CID
          OF 2    ! Default
             LOC:CID  = LOC:Setup_CID
          .
      
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Branches()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LOC:This_Client
      !    ! Report this as a bug to SV
      !    IF RECORDS(Queue:Browse:1) = 0
      !       BRW1.ResetQueue(1)
      !    .
      
          CASE LOC:This_Client
          OF 0    ! All
             LOC:CID  = 0
          OF 1    ! Passed
             LOC:CID  = p:CID
          OF 2    ! Default
             LOC:CID  = LOC:Setup_CID
          .
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  !           old
  !!    Start_#     = CLOCK()
  !
  !    IF FALSE
  !    IF LOC:This_Client = TRUE AND p:CID ~= 0
  !       IF Get_Clients_Related(p:CID, JOU:JID, 0, 1) = 0
  !          IF Get_Clients_CP_Related(p:CID, JOU:JID, 1) = 0
  !             RETURN Record:Filtered
  !    .  .  .
  !    .
  !
  !!    db.debugout('Browse_Journeys - Time: ' & CLOCK() - Start_#)
  ReturnValue = PARENT.ValidateRecord()
      IF LOC:This_Client > 0 AND LOC:CID ~= 0
  !    IF LOC:This_Client = TRUE AND p:CID ~= 0
         IF ReturnValue ~= Record:Filtered
            ! Check Normal rates
            V_RATC:JID        = JOU:JID
            V_RATC:CID        = LOC:CID
            IF Access:_View_Rates_Client.TryFetch(V_RATC:CKey_JID_CID) = LEVEL:Benign
            ELSE
               ! Check Park rates
               V_CPRC:JID     = JOU:JID
               V_CPRC:CID     = LOC:CID
               IF Access:_View_ContainerParkRates_Client.TryFetch(V_CPRC:CKey_JID_CID) = LEVEL:Benign
               ELSE
                  ! Check Park rates - specified
                  ! Need to check if any Container Park Rates exist for Specified load types....
                  ! Only place p:LTID, p:FID is used
                  ! (p:CID, p:LTID, p:FID)
                  IF LOC:ContainerParkSpecial = TRUE              ! (p:FID, p:JID, p:Eff_Date, p:Mass, p:Rate, p:Cache_Type)
  !                   db.debugout('[Select_Journey]  A_LOAD:FullLoad_ContainerPark: ' & A_LOAD:FullLoad_ContainerPark & ', p:FID: ' & p:FID & ', JOU:JID: ' & JOU:JID)
                     IF Get_Floor_Rate(p:FID, JOU:JID, TODAY(),,, 2) > 0
                        ! Ok then
                     ELSE
                        ReturnValue = Record:Filtered
                     .
                  ELSE
                     ReturnValue    = Record:Filtered
      .  .  .  .  .
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_CLient, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_CLient

!!! <summary>
!!! Generated from procedure template - Window
!!! Select Clients / Mod Rates for Clients
!!! </summary>
Select_RateMod_Clients PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:Locator          STRING(100)                           !
LOC:InsuranceType    STRING(20)                            !
LOC:BID              ULONG                                 !Branch ID
LOC:Selection_Option BYTE                                  !Selection of All clients option
L_BG:Client_Status   STRING(20)                            !Normal, On Hold, Closed
BRW1::View:Browse    VIEW(Clients)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:MinimiumCharge)
                       PROJECT(CLI:DocumentCharge)
                       PROJECT(CLI:OpsManager)
                       PROJECT(CLI:InsuranceRequired)
                       PROJECT(CLI:InsurancePercent)
                       PROJECT(CLI:VolumetricRatio)
                       PROJECT(CLI:GenerateInvoice)
                       PROJECT(CLI:ACID)
                       PROJECT(CLI:CID)
                       PROJECT(CLI:BID)
                       PROJECT(CLI:Status)
                       PROJECT(CLI:ClientSearch)
                       JOIN(BRA:PKey_BID,CLI:BID)
                         PROJECT(BRA:BID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:ClientName_Style   LONG                           !Field style
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
CLI:ClientNo_Style     LONG                           !Field style
CLI:MinimiumCharge     LIKE(CLI:MinimiumCharge)       !List box control field - type derived from field
CLI:MinimiumCharge_Style LONG                         !Field style
CLI:DocumentCharge     LIKE(CLI:DocumentCharge)       !List box control field - type derived from field
CLI:DocumentCharge_Style LONG                         !Field style
CLI:OpsManager         LIKE(CLI:OpsManager)           !List box control field - type derived from field
CLI:OpsManager_Style   LONG                           !Field style
L_BG:Client_Status     LIKE(L_BG:Client_Status)       !List box control field - type derived from local data
L_BG:Client_Status_Style LONG                         !Field style
CLI:InsuranceRequired  LIKE(CLI:InsuranceRequired)    !List box control field - type derived from field
CLI:InsuranceRequired_Icon LONG                       !Entry's icon ID
CLI:InsuranceRequired_Style LONG                      !Field style
LOC:InsuranceType      LIKE(LOC:InsuranceType)        !List box control field - type derived from local data
LOC:InsuranceType_Style LONG                          !Field style
CLI:InsurancePercent   LIKE(CLI:InsurancePercent)     !List box control field - type derived from field
CLI:InsurancePercent_Style LONG                       !Field style
CLI:VolumetricRatio    LIKE(CLI:VolumetricRatio)      !List box control field - type derived from field
CLI:VolumetricRatio_Style LONG                        !Field style
CLI:GenerateInvoice    LIKE(CLI:GenerateInvoice)      !List box control field - type derived from field
CLI:GenerateInvoice_Icon LONG                         !Entry's icon ID
CLI:GenerateInvoice_Style LONG                        !Field style
CLI:ACID               LIKE(CLI:ACID)                 !List box control field - type derived from field
CLI:ACID_Style         LONG                           !Field style
CLI:CID                LIKE(CLI:CID)                  !List box control field - type derived from field
CLI:CID_Style          LONG                           !Field style
CLI:BID                LIKE(CLI:BID)                  !List box control field - type derived from field
CLI:BID_Style          LONG                           !Field style
CLI:Status             LIKE(CLI:Status)               !Browse hot field - type derived from field
CLI:ClientSearch       LIKE(CLI:ClientSearch)         !Browse key field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Clients Record'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('SelectClients'),SYSTEM
                       LIST,AT(8,32,342,122),USE(?Browse:1),HVSCROLL,ALRT(CtrlA),ALRT(CtrlU),FORMAT('80L(2)|MY~' & |
  'Client Name~@s100@38R(2)|MY~Client No.~L@n_10b@44D(12)|MY~Min. Charge~C(0)@n-11.2@42' & |
  'R(1)|MY~Docs. Charge~L@n-11.2@60L(1)|MY~Ops. Manager~C(0)@s35@50L(1)|MY~Status~C(0)@' & |
  's20@26L(1)|MIY~Insurance Required~@p p@56L(1)|MY~Insurance Type~C(0)@s20@32R(1)|MY~%' & |
  ' Insurance~L@n-7.2@58R(2)|MY~Volumetric Ratio~C(0)@n-11.2@42R(2)|MIY~Generate Invoic' & |
  'e~L(1)@p p@40R(2)|MY~ACID~C(0)@n_10@40R(2)|MY~CID~C(0)@n_10@40R(2)|MY~BID~C(0)@n_10@'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing the Clients file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       GROUP,AT(8,19,331,10),USE(?Group_Locator)
                         PROMPT('Locator:'),AT(8,19),USE(?LOC:Locator:Prompt),TRN
                         STRING(@s100),AT(38,19,301,10),USE(LOC:Locator),FONT('Tahoma'),TRN
                       END
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Client Name'),USE(?Tab:2)
                         END
                         TAB('&2) By Client Name - By Branch'),USE(?Tab:4)
                           BUTTON('&Branches'),AT(9,158,,14),USE(?Button_Branch),LEFT,ICON('WAPARENT.ICO'),FLAT
                         END
                         TAB('&3) By Client Search'),USE(?Tab:3),HIDE
                         END
                       END
                       BUTTON('&Help'),AT(246,182,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('Modify Rates'),AT(4,182,,14),USE(?Button_ModifyRates),LEFT,ICON('WIZDITTO.ICO'),FLAT
                       GROUP,AT(81,186,119,10),USE(?Group_SelectionOpt)
                         PROMPT('Selection Option:'),AT(81,186),USE(?LOC:Selection_Option:Prompt)
                         LIST,AT(140,186,77,10),USE(LOC:Selection_Option),DROP(5),FROM('All|#0|Exclude Closed & ' & |
  'Dormant|#1|Exclude Closed|#2'),MSG('Selection of All clients option'),TIP('Selection ' & |
  'of All clients option')
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort2:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort1:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort2:StepClass StepStringClass                      ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW1::Sort1:StepClass StepStringClass                      ! Conditional Step Manager - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Client_Tags              shpTagClass
View_Cleint     VIEW(Clients)
    PROJECT(CLI:CID, CLI:Status)
    .

Client_View     ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(CLI:CID)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
  !------------------------------------
  !Style for ?Browse:1
  !------------------------------------
!---------------------------------------------------------------------------
Style_Setup                     ROUTINE
!    IF ~OMITTED(1)
       ! Style:Normal
   !    ?list1{PROPSTYLE:FontStyle, 1}     = FONT:Regular
   !    ?list1{PROPSTYLE:TextColor, 1}     = COLOR:NONE
   !    ?list1{PROPSTYLE:BackColor, 1}     = COLOR:NONE
   !    ?list1{PROPSTYLE:TextSelected, 1}  = COLOR:NONE
   !    ?list1{PROPSTYLE:BackSelected, 1}  = COLOR:NONE

       ! Style:Header
       !?list1{PROPSTYLE:FontStyle, 1}     = FONT:Bold
       ?Browse:1{PROPSTYLE:TextColor, 1}     = COLOR:HIGHLIGHTTEXT
       ?Browse:1{PROPSTYLE:BackColor, 1}     = COLOR:HIGHLIGHT
       ?Browse:1{PROPSTYLE:TextSelected, 1}  = COLOR:White
       ?Browse:1{PROPSTYLE:BackSelected, 1}  = COLOR:Blue

!       SELF.Q.LOC:Day_NormalFG = -2147483634
!       SELF.Q.LOC:Day_NormalBG = -2147483635
!       SELF.Q.LOC:Day_SelectedFG = -1
!       SELF.Q.LOC:Day_SelectedBG = -1

        !?LOC:Selected{PROP:Text}    = 'Show Selected (' & RECORDS(p_Addresses) & ')'
!    ELSE
       !LOC:Omitted_1    = TRUE

       !HIDE(?LOC:Selected)
!    .
    EXIT
Style_Entry                       ROUTINE
    !IF LOC:Omitted_1 = FALSE
       IF Client_Tags.IsTagged(Queue:Browse:1.CLI:CID) = TRUE
!    db.debugout('[Select_RateMod_Clients]  Check - tagged on: ' & Queue:Browse:1.CLI:CID)
          Queue:Browse:1.CLI:ClientName_Style             = 1
          Queue:Browse:1.CLI:ClientNo_Style               = 1
          Queue:Browse:1.CLI:MinimiumCharge_Style         = 1
          Queue:Browse:1.CLI:DocumentCharge_Style         = 1
          !Queue:Browse:1.CLI:FuelSurcharge_Style          = 1
          Queue:Browse:1.CLI:OpsManager_Style             = 1
          Queue:Browse:1.CLI:InsuranceRequired_Style      = 1
          Queue:Browse:1.LOC:InsuranceType_Style          = 1
          Queue:Browse:1.CLI:InsurancePercent_Style       = 1
          Queue:Browse:1.CLI:GenerateInvoice_Style        = 1
          Queue:Browse:1.CLI:VolumetricRatio_Style        = 1
          Queue:Browse:1.L_BG:Client_Status_Style         = 1
          Queue:Browse:1.CLI:ACID_Style                   = 1
          Queue:Browse:1.CLI:CID_Style                    = 1
          Queue:Browse:1.CLI:BID_Style                    = 1
       ELSE
!    db.debugout('[Select_RateMod_Clients]  Check - NOT tagged on: ' & Queue:Browse:1.CLI:CID)
          Queue:Browse:1.CLI:ClientName_Style             = 0
          Queue:Browse:1.CLI:ClientNo_Style               = 0
          Queue:Browse:1.CLI:MinimiumCharge_Style         = 0
          Queue:Browse:1.CLI:DocumentCharge_Style         = 0
          !Queue:Browse:1.CLI:FuelSurcharge_Style          = 0
          Queue:Browse:1.CLI:OpsManager_Style             = 0
          Queue:Browse:1.CLI:InsuranceRequired_Style      = 0
          Queue:Browse:1.LOC:InsuranceType_Style          = 0
          Queue:Browse:1.CLI:InsurancePercent_Style       = 0
          Queue:Browse:1.CLI:GenerateInvoice_Style        = 0
          Queue:Browse:1.CLI:VolumetricRatio_Style        = 0
          Queue:Browse:1.L_BG:Client_Status_Style         = 0
          Queue:Browse:1.CLI:ACID_Style                   = 0
          Queue:Browse:1.CLI:CID_Style                    = 0
          Queue:Browse:1.CLI:BID_Style                    = 0
    .!  .

    EXIT
Tag_Toggle_this_rec       ROUTINE
    !LOC:Last_Tagged_Rec_Id       = CLI:CID

    IF Client_Tags.IsTagged(CLI:CID) = TRUE
       Client_Tags.ClearTag(CLI:CID)
!       db.debugout('[Select_RateMod_Clients]  UN Tagging - CLI:CID: ' & CLI:CID)
    ELSE
       Client_Tags.MakeTag(CLI:CID)
!       db.debugout('[Select_RateMod_Clients]  Tagging - CLI:CID: ' & CLI:CID)
    .

    BRW1.ResetFromBuffer()
!    BRW1.Reset(1)
    EXIT
Tag_From_To                  ROUTINE
    DATA
R:Forward_Backward              BYTE

    CODE
!    R:Current_Selected_Rec_ID       = REC:Rec_ID
!
!!    MESSAGE('Selected Rec ID: ' & R:Current_Selected_Rec_ID & '|REC:TA_Date: ' & FORMAT(REC:TA_Date,@d5) & '|REC:TA_Time: ' & FORMAT(REC:TA_Time,@t4) & |
!!            '||LOC:Last_Tagged_Rec_Id: ' & LOC:Last_Tagged_Rec_Id)
!
!    ! If there is no Last Tagged then this is the only record to tag
!    IF LOC:Last_Clicked_Rec_ID ~= 0
!       LOC:Last_Tagged_Rec_Id   = LOC:Last_Clicked_Rec_ID
!    .
!
!    IF LOC:Last_Tagged_Rec_Id = 0
!       InBox_Tags.MakeTag(R:Current_Selected_Rec_ID)
!
!       LOC:Last_Tagged_Rec_Id       = R:Current_Selected_Rec_ID
!       LOC:Last_Tagged_Date         = REC:TA_Date
!       LOC:Last_Tagged_Time         = REC:TA_Time
!    ELSE
!       ! The beginning position of the loop depends on the last tagged record being before or after the
!       ! currently selected one.
!       R:Forward_Backward           = 0
!       IF REC:TA_Date >= LOC:Last_Tagged_Date
!          IF REC:TA_Date = LOC:Last_Tagged_Date
!             IF REC:TA_Time >= LOC:Last_Tagged_Time
!                IF REC:TA_Time = LOC:Last_Tagged_Time
!                   IF R:Current_Selected_Rec_ID < LOC:Last_Tagged_Rec_Id
!                      R:Forward_Backward  = 1
!                .  .
!             ELSE
!                R:Forward_Backward  = 1
!          .  .
!       ELSE
!          R:Forward_Backward        = 1
!       .
!
!
!       IF R:Forward_Backward = 0
!          CLEAR(A_REC:Record,-1)
!
!          ! Forward meaning from earliest Last Tagged Date / Time - file is in descending date / time order
!          ! So we set these to the later date / time here
!          A_REC:User_ID_Private_To      = LOC:User_ID
!          A_REC:TA_Date                 = REC:TA_Date
!          A_REC:TA_Time                 = REC:TA_Time
!
!          R:User_ID_Private_To          = LOC:User_ID
!          R:TA_Date                     = LOC:Last_Tagged_Date
!          R:TA_Time                     = LOC:Last_Tagged_Time
!
!          R:To_Rec_ID                   = LOC:Last_Tagged_Rec_Id
!       ELSE
!          CLEAR(A_REC:Record,+1)
!
!          A_REC:User_ID_Private_To      = LOC:User_ID
!          A_REC:TA_Date                 = LOC:Last_Tagged_Date
!          A_REC:TA_Time                 = LOC:Last_Tagged_Time
!
!          R:User_ID_Private_To          = LOC:User_ID
!          R:TA_Date                     = REC:TA_Date
!          R:TA_Time                     = REC:TA_Time
!          R:To_Rec_ID                   = REC:Rec_ID
!       .
!
!       ! The Key used does not have a field below the Time, so if the date and time in the From and To are the
!       ! same then the Records will be in record no. order or received order!
!       R:From_Rec_ID    = 0
!       IF (LOC:Last_Tagged_Date = REC:TA_Date) AND (LOC:Last_Tagged_Time = REC:TA_Time)
!          IF R:Current_Selected_Rec_ID ~= LOC:Last_Tagged_Rec_Id
!             ! Then the greater Rec_ID must be the To Rec_ID
!             IF LOC:Last_Tagged_Rec_Id > R:Current_Selected_Rec_ID
!                R:To_Rec_ID     = LOC:Last_Tagged_Rec_Id
!                R:From_Rec_ID   = R:Current_Selected_Rec_ID
!             ELSE
!                R:To_Rec_ID     = R:Current_Selected_Rec_ID
!                R:From_Rec_ID   = LOC:Last_Tagged_Rec_Id
!       .  .  .
!
!       SET(A_REC:SKEY_Adjust_Date_Time, A_REC:SKEY_Adjust_Date_Time)
!       LOOP
!          IF Access:Received_Msgs_Alias.TryNext() ~= LEVEL:Benign
!             BREAK
!          .
!
!!       IF R:Forward_Backward = 0
!!          MESSAGE('Set Date / Time: ' & FORMAT(REC:TA_Date,@d5) & ' / ' & FORMAT(REC:TA_Time, @t4) & '||' |
!!                   & 'Found Date / Time: ' & FORMAT(A_REC:TA_Date,@d5) & ' / ' & FORMAT(A_REC:TA_Time, @t4) |
!!                   & '||A_REC:Rec_ID: ' & REC:Rec_ID & ' - to Rec_ID: ' & R:To_Rec_ID,'Forward')
!!       ELSE
!!          MESSAGE('Set Date / Time: ' & FORMAT(LOC:Last_Tagged_Date,@d5) & ' / ' & FORMAT(LOC:Last_Tagged_Time, @t4) & '||' |
!!                   & 'Found Date / Time: ' & FORMAT(A_REC:TA_Date,@d5) & ' / ' & FORMAT(A_REC:TA_Time, @t4) |
!!                   & '||A_REC:Rec_ID: ' & REC:Rec_ID & ' - to Rec_ID: ' & R:To_Rec_ID,'Backwards')
!!       .
!
!          IF A_REC:User_ID_Private_To ~= LOC:User_ID      |
!                OR A_REC:TA_Date < R:TA_Date OR (A_REC:TA_Date = R:TA_Date AND A_REC:TA_Time < R:TA_Time)
!             BREAK
!          .
!
!          IF R:From_Rec_ID ~= 0
!             !MESSAGE('cycles R:From_Rec_ID < A_REC:Rec_ID||' & R:From_Rec_ID & ' < ' & A_REC:Rec_ID,'To Rec_ID: ' & R:To_Rec_ID)
!             IF R:From_Rec_ID > A_REC:Rec_ID
!                CYCLE
!          .  .
!
!          InBox_Tags.MakeTag(A_REC:Rec_ID)
!
!          IF R:To_Rec_ID = A_REC:Rec_ID
!             BREAK
!    .  .  .
!
!    BRW1.ResetFromBuffer()
    EXIT
Tag_All                             ROUTINE
    ! Tag All in the current sort order...
    PUSHBIND()
    BIND('CLI:Status',CLI:Status)

    Client_View.Init(View_Cleint, Relate:Clients)

    IF CHOICE(?CurrentTab) = 2
       ! Branch CLients
       Client_View.AddSortOrder(CLI:FKey_BID)
       Client_View.AppendOrder('CLI:CID')
       Client_View.AddRange(CLI:BID, LOC:BID)
    ELSIF CHOICE(?CurrentTab) = 3
    ELSE
       ! All Clients
       Client_View.AddSortOrder(CLI:PKey_CID)
    .


    ! All|Exclude Closed & Dormant|Exclude Closed
    IF LOC:Selection_Option = 1
       ! Normal|On Hold|Closed|Dormant
       Client_View.SetFilter('CLI:Status <= 1')         ! Not closed or dormant
    ELSIF LOC:Selection_Option = 2
       Client_View.SetFilter('CLI:Status <> 2')         ! Not closed
    .

    Client_View.Reset()
    LOOP
       IF Client_View.Next() ~= LEVEL:Benign
          BREAK
       .

       Client_Tags.MakeTag(CLI:CID)
    .

    Client_View.Kill()
    POPBIND()
    BRW1.ResetFromBuffer()
    EXIT
Un_Tag_All                      ROUTINE
    Client_Tags.ClearAllTags()
    BRW1.ResetFromBuffer()
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_RateMod_Clients')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:BID',LOC:BID)                                  ! Added by: BrowseBox(ABC)
  BIND('L_BG:Client_Status',L_BG:Client_Status)            ! Added by: BrowseBox(ABC)
  BIND('LOC:InsuranceType',LOC:InsuranceType)              ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Clients,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:CaseSensitive,ScrollBy:Runtime) ! Moveable thumb based upon CLI:ClientName for sort order 1
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,CLI:Key_ClientName) ! Add the sort order for CLI:Key_ClientName for sort order 1
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort2:Locator.Init(?LOC:Locator,CLI:ClientName,,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: CLI:Key_ClientName , CLI:ClientName
  BRW1.AppendOrder('+CLI:ClientNo')                        ! Append an additional sort order
  BRW1.SetFilter('(CLI:BID = LOC:BID)')                    ! Apply filter expression to browse
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:CaseSensitive,ScrollBy:Runtime) ! Moveable thumb based upon CLI:ClientSearch for sort order 2
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,CLI:Key_ClientSearch) ! Add the sort order for CLI:Key_ClientSearch for sort order 2
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort1:Locator.Init(?LOC:Locator,CLI:ClientSearch,,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: CLI:Key_ClientSearch , CLI:ClientSearch
  BRW1.AppendOrder('+CLI:ClientNo')                        ! Append an additional sort order
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:CaseSensitive,ScrollBy:Runtime) ! Moveable thumb based upon CLI:ClientName for sort order 3
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,CLI:Key_ClientName) ! Add the sort order for CLI:Key_ClientName for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(?LOC:Locator,CLI:ClientName,,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: CLI:Key_ClientName , CLI:ClientName
  BRW1.AppendOrder('+CLI:ClientNo')                        ! Append an additional sort order
  BRW1.AddResetField(LOC:BID)                              ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(CLI:ClientName,BRW1.Q.CLI:ClientName)      ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientNo,BRW1.Q.CLI:ClientNo)          ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW1.AddField(CLI:MinimiumCharge,BRW1.Q.CLI:MinimiumCharge) ! Field CLI:MinimiumCharge is a hot field or requires assignment from browse
  BRW1.AddField(CLI:DocumentCharge,BRW1.Q.CLI:DocumentCharge) ! Field CLI:DocumentCharge is a hot field or requires assignment from browse
  BRW1.AddField(CLI:OpsManager,BRW1.Q.CLI:OpsManager)      ! Field CLI:OpsManager is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Client_Status,BRW1.Q.L_BG:Client_Status) ! Field L_BG:Client_Status is a hot field or requires assignment from browse
  BRW1.AddField(CLI:InsuranceRequired,BRW1.Q.CLI:InsuranceRequired) ! Field CLI:InsuranceRequired is a hot field or requires assignment from browse
  BRW1.AddField(LOC:InsuranceType,BRW1.Q.LOC:InsuranceType) ! Field LOC:InsuranceType is a hot field or requires assignment from browse
  BRW1.AddField(CLI:InsurancePercent,BRW1.Q.CLI:InsurancePercent) ! Field CLI:InsurancePercent is a hot field or requires assignment from browse
  BRW1.AddField(CLI:VolumetricRatio,BRW1.Q.CLI:VolumetricRatio) ! Field CLI:VolumetricRatio is a hot field or requires assignment from browse
  BRW1.AddField(CLI:GenerateInvoice,BRW1.Q.CLI:GenerateInvoice) ! Field CLI:GenerateInvoice is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ACID,BRW1.Q.CLI:ACID)                  ! Field CLI:ACID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:CID,BRW1.Q.CLI:CID)                    ! Field CLI:CID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:BID,BRW1.Q.CLI:BID)                    ! Field CLI:BID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:Status,BRW1.Q.CLI:Status)              ! Field CLI:Status is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientSearch,BRW1.Q.CLI:ClientSearch)  ! Field CLI:ClientSearch is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BID,BRW1.Q.BRA:BID)                    ! Field BRA:BID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_RateMod_Clients',QuickWindow)       ! Restore window settings from non-volatile store
      DO Style_Setup
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      BRA:BID     = GLO:BranchID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         LOC:BID  = GLO:BranchID
      .
      IF SELF.Request = SelectRecord
         HIDE(?Button_ModifyRates)
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_RateMod_Clients',QuickWindow)    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_ModifyRates
          IF Client_Tags.NumberTagged() <= 0
             MESSAGE('There are no Clients selected to modify rates.||To select clients hold down the CTRL ' & |
                      'button and click on the Client|To select all hold down CTRL and press A|To de-select all ' & |
                      'hold down CTRL and press U', 'Select Clients for Rate Mod.', ICON:Exclamation)
             SELECT(?Browse:1)
             CYCLE
          .
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Select:2
      ThisWindow.Update()
      BRW1.UpdateViewRecord()
    OF ?Button_Branch
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Branches()
      ThisWindow.Reset
          LOC:BID = BRA:BID
    OF ?Button_ModifyRates
      ThisWindow.Update()
      Rates_Modify(Client_Tags)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
        CASE EVENT()
        OF EVENT:Accepted
           !EVENT:MouseDown
    !       BRW1.UpdateViewRecord()
    
           IF KeyCode() = ShiftMouseLeft
              ! Tag all records between this one and last tagged record
              IF CHOICE(?CurrentTab) = 1
                 DO Tag_From_To
              .
           ELSIF KeyCode() = CtrlMouseLeft
              DO Tag_Toggle_this_rec
           ELSIF KeyCode() ~= MouseRight AND KeyCode() ~= CtrlMouseRight !AND LOC:Last_Tagged_Rec_Id ~= 0
    !          InBox_Tags.ClearAllTags()                ! Clear all tags when no shift left mouse
    !          LOC:Last_Tagged_Rec_Id        = 0
    
    !          BRW1.ResetFromBuffer()
           .
    
    !       LOC:Last_Clicked_Rec_ID      = REC:Rec_ID
    !       LOC:Last_Tagged_Date         = REC:TA_Date
    !       LOC:Last_Tagged_Time         = REC:TA_Time
        .
    CASE EVENT()
    OF EVENT:AlertKey
          CASE Keycode()
          OF CtrlA
             DO Tag_All
          OF CtrlU
             DO Un_Tag_All
          .
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
        ! Normal|On Hold|Closed|Dormant
        EXECUTE CLI:Status + 1
           L_BG:Client_Status   = 'Normal'
           L_BG:Client_Status   = 'On Hold'
           L_BG:Client_Status   = 'Closed'
           L_BG:Client_Status   = 'Dormant'
        .
  PARENT.SetQueueRecord
  
  SELF.Q.CLI:ClientName_Style = 0 ! 
  SELF.Q.CLI:ClientNo_Style = 0 ! 
  SELF.Q.CLI:MinimiumCharge_Style = 0 ! 
  SELF.Q.CLI:DocumentCharge_Style = 0 ! 
  SELF.Q.CLI:OpsManager_Style = 0 ! 
  SELF.Q.L_BG:Client_Status_Style = 0 ! 
  IF (CLI:InsuranceRequired = 1)
    SELF.Q.CLI:InsuranceRequired_Icon = 2                  ! Set icon from icon list
  ELSE
    SELF.Q.CLI:InsuranceRequired_Icon = 1                  ! Set icon from icon list
  END
  SELF.Q.CLI:InsuranceRequired_Style = 0 ! 
  SELF.Q.LOC:InsuranceType_Style = 0 ! 
  SELF.Q.CLI:InsurancePercent_Style = 0 ! 
  SELF.Q.CLI:VolumetricRatio_Style = 0 ! 
  IF (CLI:GenerateInvoice = 1)
    SELF.Q.CLI:GenerateInvoice_Icon = 2                    ! Set icon from icon list
  ELSE
    SELF.Q.CLI:GenerateInvoice_Icon = 1                    ! Set icon from icon list
  END
  SELF.Q.CLI:GenerateInvoice_Style = 0 ! 
  SELF.Q.CLI:ACID_Style = 0 ! 
  SELF.Q.CLI:CID_Style = 0 ! 
  SELF.Q.CLI:BID_Style = 0 ! 
      DO Style_Entry
  
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Locator, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:LockHeight) ! Override strategy for ?Group_Locator
  SELF.SetStrategy(?Group_SelectionOpt, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_SelectionOpt

