

   MEMBER('SELTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SELTRNIS004.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_TransporterInvoice PROCEDURE (p:Option)

CurrentTab           STRING(80)                            !
LOC:Total            DECIMAL(12,2)                         !
LOC:Option           BYTE                                  !
LOC:Locator          STRING(20)                            !
BRW1::View:Browse    VIEW(InvoiceTransporterAlias)
                       PROJECT(A_INT:TIN)
                       PROJECT(A_INT:MID)
                       PROJECT(A_INT:Cost)
                       PROJECT(A_INT:VAT)
                       PROJECT(A_INT:VATRate)
                       PROJECT(A_INT:Rate)
                       PROJECT(A_INT:DINo)
                       PROJECT(A_INT:Leg)
                       PROJECT(A_INT:TID)
                       PROJECT(A_INT:BID)
                       PROJECT(A_INT:CR_TIN)
                       JOIN(Transporter,'A_INT:TID = TRA:TID')
                         PROJECT(TRA:TransporterName)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
A_INT:TIN              LIKE(A_INT:TIN)                !List box control field - type derived from field
A_INT:MID              LIKE(A_INT:MID)                !List box control field - type derived from field
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
LOC:Total              LIKE(LOC:Total)                !List box control field - type derived from local data
A_INT:Cost             LIKE(A_INT:Cost)               !List box control field - type derived from field
A_INT:VAT              LIKE(A_INT:VAT)                !List box control field - type derived from field
A_INT:VATRate          LIKE(A_INT:VATRate)            !List box control field - type derived from field
A_INT:Rate             LIKE(A_INT:Rate)               !List box control field - type derived from field
A_INT:DINo             LIKE(A_INT:DINo)               !List box control field - type derived from field
A_INT:Leg              LIKE(A_INT:Leg)                !List box control field - type derived from field
A_INT:TID              LIKE(A_INT:TID)                !List box control field - type derived from field
A_INT:BID              LIKE(A_INT:BID)                !List box control field - type derived from field
A_INT:CR_TIN           LIKE(A_INT:CR_TIN)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Invoice Transporter File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MAX,MDI,HLP('Select_TransporterInvoice'),SYSTEM
                       LIST,AT(8,32,342,122),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~TIN~C(0)@n_10@40R(2)|M~M' & |
  'ID~C(0)@n_10@70L(2)|M~Transporter~C(0)@s35@50R(2)|M~Total~C(0)@n-17.2@50R(1)|M~Cost~' & |
  'C(0)@n-14.2@50R(1)|M~VAT~C(0)@n-14.2@36R(1)|M~VAT Rate~C(0)@n-7.2@50R(1)|M~Rate~C(0)' & |
  '@n-13.4@[40R(2)|M~DI No.~C(0)@n_10@24R(2)|M~Leg~C(0)@n5@]|M~Extra Leg~40R(2)|M~TID~C' & |
  '(0)@n_10@40R(2)|M~BID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Invoic' & |
  'eTransporterAlias file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Invoice No.'),USE(?Tab6)
                           PROMPT('Locator:'),AT(9,20),USE(?LOC:Locator:Prompt),TRN
                           ENTRY(@s20),AT(38,20,60,10),USE(LOC:Locator)
                         END
                         TAB('&2) By Transporter'),USE(?Tab:3)
                         END
                         TAB('&3) By Branch'),USE(?Tab:4)
                         END
                         TAB('&4) By Manifest'),USE(?Tab:5)
                         END
                         TAB('&5) By Credited Invoice'),USE(?Tab:9)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                     ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 4
BRW1::Sort7:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 5
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_TransporterInvoice')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Option',LOC:Option)                            ! Added by: BrowseBox(ABC)
  BIND('LOC:Total',LOC:Total)                              ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:InvoiceTransporterAlias.Open                      ! File InvoiceTransporterAlias used by this procedure, so make sure it's RelationManager is open
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                                  ! File Transporter used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:InvoiceTransporterAlias,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,A_INT:FKey_TID)                       ! Add the sort order for A_INT:FKey_TID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,A_INT:TID,1,BRW1)              ! Initialize the browse locator using  using key: A_INT:FKey_TID , A_INT:TID
  BRW1.SetFilter('(LOC:Option = 0 OR A_INT:Cost >= 0.0)')  ! Apply filter expression to browse
  BRW1.AddSortOrder(,A_INT:FKey_BID)                       ! Add the sort order for A_INT:FKey_BID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,A_INT:BID,1,BRW1)              ! Initialize the browse locator using  using key: A_INT:FKey_BID , A_INT:BID
  BRW1.SetFilter('(LOC:Option = 0 OR A_INT:Cost >= 0.0)')  ! Apply filter expression to browse
  BRW1.AddSortOrder(,A_INT:Fkey_MID)                       ! Add the sort order for A_INT:Fkey_MID for sort order 3
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort3:Locator.Init(,A_INT:MID,1,BRW1)              ! Initialize the browse locator using  using key: A_INT:Fkey_MID , A_INT:MID
  BRW1.SetFilter('(LOC:Option = 0 OR A_INT:Cost >= 0.0)')  ! Apply filter expression to browse
  BRW1.AddSortOrder(,A_INT:Key_CR_TIN)                     ! Add the sort order for A_INT:Key_CR_TIN for sort order 4
  BRW1.AddLocator(BRW1::Sort7:Locator)                     ! Browse has a locator for sort order 4
  BRW1::Sort7:Locator.Init(,A_INT:CR_TIN,1,BRW1)           ! Initialize the browse locator using  using key: A_INT:Key_CR_TIN , A_INT:CR_TIN
  BRW1.SetFilter('(LOC:Option = 0 OR A_INT:Cost >= 0.0)')  ! Apply filter expression to browse
  BRW1.AddSortOrder(,A_INT:PKey_TIN)                       ! Add the sort order for A_INT:PKey_TIN for sort order 5
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 5
  BRW1::Sort0:Locator.Init(?LOC:Locator,A_INT:TIN,1,BRW1)  ! Initialize the browse locator using ?LOC:Locator using key: A_INT:PKey_TIN , A_INT:TIN
  BRW1.SetFilter('(LOC:Option = 0 OR A_INT:Cost >= 0.0)')  ! Apply filter expression to browse
  BRW1.AddField(A_INT:TIN,BRW1.Q.A_INT:TIN)                ! Field A_INT:TIN is a hot field or requires assignment from browse
  BRW1.AddField(A_INT:MID,BRW1.Q.A_INT:MID)                ! Field A_INT:MID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:TransporterName,BRW1.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Total,BRW1.Q.LOC:Total)                ! Field LOC:Total is a hot field or requires assignment from browse
  BRW1.AddField(A_INT:Cost,BRW1.Q.A_INT:Cost)              ! Field A_INT:Cost is a hot field or requires assignment from browse
  BRW1.AddField(A_INT:VAT,BRW1.Q.A_INT:VAT)                ! Field A_INT:VAT is a hot field or requires assignment from browse
  BRW1.AddField(A_INT:VATRate,BRW1.Q.A_INT:VATRate)        ! Field A_INT:VATRate is a hot field or requires assignment from browse
  BRW1.AddField(A_INT:Rate,BRW1.Q.A_INT:Rate)              ! Field A_INT:Rate is a hot field or requires assignment from browse
  BRW1.AddField(A_INT:DINo,BRW1.Q.A_INT:DINo)              ! Field A_INT:DINo is a hot field or requires assignment from browse
  BRW1.AddField(A_INT:Leg,BRW1.Q.A_INT:Leg)                ! Field A_INT:Leg is a hot field or requires assignment from browse
  BRW1.AddField(A_INT:TID,BRW1.Q.A_INT:TID)                ! Field A_INT:TID is a hot field or requires assignment from browse
  BRW1.AddField(A_INT:BID,BRW1.Q.A_INT:BID)                ! Field A_INT:BID is a hot field or requires assignment from browse
  BRW1.AddField(A_INT:CR_TIN,BRW1.Q.A_INT:CR_TIN)          ! Field A_INT:CR_TIN is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_TransporterInvoice',QuickWindow)    ! Restore window settings from non-volatile store
      LOC:Option      = p:Option
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:InvoiceTransporterAlias.Close
    Relate:Transporter.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_TransporterInvoice',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      LOC:Total   = A_INT:Cost + A_INT:VAT
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_Clients PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Clients)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:GenerateInvoice)
                       PROJECT(CLI:MinimiumCharge)
                       PROJECT(CLI:DocumentCharge)
                       PROJECT(CLI:BID)
                       PROJECT(CLI:CID)
                       PROJECT(CLI:ClientSearch)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:GenerateInvoice    LIKE(CLI:GenerateInvoice)      !List box control field - type derived from field
CLI:GenerateInvoice_Icon LONG                         !Entry's icon ID
CLI:MinimiumCharge     LIKE(CLI:MinimiumCharge)       !List box control field - type derived from field
CLI:DocumentCharge     LIKE(CLI:DocumentCharge)       !List box control field - type derived from field
CLI:BID                LIKE(CLI:BID)                  !List box control field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !List box control field - type derived from field
CLI:ClientSearch       LIKE(CLI:ClientSearch)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Clients File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Select_Clients'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('36R(2)|M~Client No.~C(0)@n_10b@15' & |
  '0L(2)|M~Client Name~@s100@60R(2)|MI~Generate Invoice~C(0)@p p@64R(1)|M~Minimium Char' & |
  'ge~C(0)@n-11.2@64R(1)|M~Document Charge~C(0)@n-11.2@30R(2)|M~BID~C(0)@n_10@30R(2)|M~' & |
  'CID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Clients file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Client No.'),USE(?Tab:2)
                         END
                         TAB('&2) By Client Name'),USE(?Tab:3)
                         END
                         TAB('&3) By Branch'),USE(?Tab:5)
                           BUTTON('Select Branches'),AT(9,158,118,14),USE(?SelectBranches),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Clients')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Clients,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,CLI:Key_ClientSearch)                 ! Add the sort order for CLI:Key_ClientSearch for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,CLI:ClientSearch,,BRW1)        ! Initialize the browse locator using  using key: CLI:Key_ClientSearch , CLI:ClientSearch
  BRW1.AddSortOrder(,CLI:FKey_BID)                         ! Add the sort order for CLI:FKey_BID for sort order 2
  BRW1.AddRange(CLI:BID,Relate:Clients,Relate:Branches)    ! Add file relationship range limit for sort order 2
  BRW1.AddSortOrder(,CLI:Key_ClientNo)                     ! Add the sort order for CLI:Key_ClientNo for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(,CLI:ClientNo,1,BRW1)           ! Initialize the browse locator using  using key: CLI:Key_ClientNo , CLI:ClientNo
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(CLI:ClientNo,BRW1.Q.CLI:ClientNo)          ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientName,BRW1.Q.CLI:ClientName)      ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW1.AddField(CLI:GenerateInvoice,BRW1.Q.CLI:GenerateInvoice) ! Field CLI:GenerateInvoice is a hot field or requires assignment from browse
  BRW1.AddField(CLI:MinimiumCharge,BRW1.Q.CLI:MinimiumCharge) ! Field CLI:MinimiumCharge is a hot field or requires assignment from browse
  BRW1.AddField(CLI:DocumentCharge,BRW1.Q.CLI:DocumentCharge) ! Field CLI:DocumentCharge is a hot field or requires assignment from browse
  BRW1.AddField(CLI:BID,BRW1.Q.CLI:BID)                    ! Field CLI:BID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:CID,BRW1.Q.CLI:CID)                    ! Field CLI:CID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientSearch,BRW1.Q.CLI:ClientSearch)  ! Field CLI:ClientSearch is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_Clients',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_Clients',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Branches()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (CLI:GenerateInvoice = 1)
    SELF.Q.CLI:GenerateInvoice_Icon = 2                    ! Set icon from icon list
  ELSE
    SELF.Q.CLI:GenerateInvoice_Icon = 1                    ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_Users PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Users)
                       PROJECT(USE:Login)
                       PROJECT(USE:Name)
                       PROJECT(USE:Surname)
                       PROJECT(USE:AccessLevel)
                       PROJECT(USE:UID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
USE:Name               LIKE(USE:Name)                 !List box control field - type derived from field
USE:Surname            LIKE(USE:Surname)              !List box control field - type derived from field
USE:AccessLevel        LIKE(USE:AccessLevel)          !List box control field - type derived from field
USE:UID                LIKE(USE:UID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Users File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Select_Users'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Login~@s20@80L(2)|M~Name' & |
  '~@s35@80L(2)|M~Surname~@s35@52R(2)|M~Access Level~C(0)@n3@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he Users file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Login'),USE(?Tab:2)
                         END
                         TAB('&2) By Name && Surname'),USE(?Tab:3)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Users')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Users.SetOpenRelated()
  Relate:Users.Open                                        ! File Users used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Users,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,USE:SKey_Name_Surname)                ! Add the sort order for USE:SKey_Name_Surname for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,USE:Name,1,BRW1)               ! Initialize the browse locator using  using key: USE:SKey_Name_Surname , USE:Name
  BRW1.AddSortOrder(,USE:Key_Login)                        ! Add the sort order for USE:Key_Login for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,USE:Login,1,BRW1)              ! Initialize the browse locator using  using key: USE:Key_Login , USE:Login
  BRW1.AddField(USE:Login,BRW1.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW1.AddField(USE:Name,BRW1.Q.USE:Name)                  ! Field USE:Name is a hot field or requires assignment from browse
  BRW1.AddField(USE:Surname,BRW1.Q.USE:Surname)            ! Field USE:Surname is a hot field or requires assignment from browse
  BRW1.AddField(USE:AccessLevel,BRW1.Q.USE:AccessLevel)    ! Field USE:AccessLevel is a hot field or requires assignment from browse
  BRW1.AddField(USE:UID,BRW1.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_Users',QuickWindow)                 ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Users.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_Users',QuickWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_ServiceRequirements PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(ServiceRequirements)
                       PROJECT(SERI:ServiceRequirement)
                       PROJECT(SERI:Broking)
                       PROJECT(SERI:SID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
SERI:ServiceRequirement LIKE(SERI:ServiceRequirement) !List box control field - type derived from field
SERI:Broking           LIKE(SERI:Broking)             !List box control field - type derived from field
SERI:SID               LIKE(SERI:SID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Service Requirements File'),AT(,,158,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Select_ServiceRequirements'),SYSTEM
                       LIST,AT(8,30,142,124),USE(?Browse:1),HVSCROLL,FORMAT('100L(2)|M~Service Requirement~@s3' & |
  '5@32R(2)|M~Broking~C(0)@n3@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the ServiceRequ' & |
  'irements file')
                       BUTTON('&Select'),AT(101,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,150,172),USE(?CurrentTab)
                         TAB('&1) By Service Requirment'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(105,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_ServiceRequirements')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:ServiceRequirements.Open                          ! File ServiceRequirements used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ServiceRequirements,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,SERI:Key_ServiceRequirement)          ! Add the sort order for SERI:Key_ServiceRequirement for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,SERI:ServiceRequirement,1,BRW1) ! Initialize the browse locator using  using key: SERI:Key_ServiceRequirement , SERI:ServiceRequirement
  BRW1.AddField(SERI:ServiceRequirement,BRW1.Q.SERI:ServiceRequirement) ! Field SERI:ServiceRequirement is a hot field or requires assignment from browse
  BRW1.AddField(SERI:Broking,BRW1.Q.SERI:Broking)          ! Field SERI:Broking is a hot field or requires assignment from browse
  BRW1.AddField(SERI:SID,BRW1.Q.SERI:SID)                  ! Field SERI:SID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_ServiceRequirements',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ServiceRequirements.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_ServiceRequirements',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_ReplicationIDControl PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(ReplicationIDControl)
                       PROJECT(REP:ReplicatedDatabaseID)
                       PROJECT(REP:SiteDescription)
                       PROJECT(REP:RangeFrom)
                       PROJECT(REP:RangeTo)
                       PROJECT(REP:RepCID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
REP:ReplicatedDatabaseID LIKE(REP:ReplicatedDatabaseID) !List box control field - type derived from field
REP:SiteDescription    LIKE(REP:SiteDescription)      !List box control field - type derived from field
REP:RangeFrom          LIKE(REP:RangeFrom)            !List box control field - type derived from field
REP:RangeTo            LIKE(REP:RangeTo)              !List box control field - type derived from field
REP:RepCID             LIKE(REP:RepCID)               !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Replication ID Control File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_ReplicationIDControl'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('60R(2)|M~Replicated DB ID~C(0)@n_' & |
  '10@130L(2)|M~Site Description~@s255@46R(2)|M~Range From~C(0)@n12@46R(2)|M~Range To~C' & |
  '(0)@n12@40R(2)|M~Rep CID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Rep' & |
  'licationIDControl file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Replicated Database ID'),USE(?Tab:3)
                         END
                         TAB('&2) By Rep. CID'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_ReplicationIDControl')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:ReplicationIDControl.Open                         ! File ReplicationIDControl used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ReplicationIDControl,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,REP:PKey_RepCID)                      ! Add the sort order for REP:PKey_RepCID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,REP:RepCID,1,BRW1)             ! Initialize the browse locator using  using key: REP:PKey_RepCID , REP:RepCID
  BRW1.AddSortOrder(,REP:SKEY_ReplicatedDatabaseID)        ! Add the sort order for REP:SKEY_ReplicatedDatabaseID for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,REP:ReplicatedDatabaseID,1,BRW1) ! Initialize the browse locator using  using key: REP:SKEY_ReplicatedDatabaseID , REP:ReplicatedDatabaseID
  BRW1.AddField(REP:ReplicatedDatabaseID,BRW1.Q.REP:ReplicatedDatabaseID) ! Field REP:ReplicatedDatabaseID is a hot field or requires assignment from browse
  BRW1.AddField(REP:SiteDescription,BRW1.Q.REP:SiteDescription) ! Field REP:SiteDescription is a hot field or requires assignment from browse
  BRW1.AddField(REP:RangeFrom,BRW1.Q.REP:RangeFrom)        ! Field REP:RangeFrom is a hot field or requires assignment from browse
  BRW1.AddField(REP:RangeTo,BRW1.Q.REP:RangeTo)            ! Field REP:RangeTo is a hot field or requires assignment from browse
  BRW1.AddField(REP:RepCID,BRW1.Q.REP:RepCID)              ! Field REP:RepCID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_ReplicationIDControl',QuickWindow)  ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ReplicationIDControl.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_ReplicationIDControl',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_ReplicationIDControl
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

