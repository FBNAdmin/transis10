!ABCIncludeFile(nclsmsmest)

OMIT('_EndOfInclude_',_SMSMeNClst_)
_SMSMeNClst_ EQUATE(1)


GL_Cl_Msg_Exp_Class             CLASS,TYPE,MODULE('NClSMSSt.clw'),LINK('nclsmsst.clw',_ABCLinkMode_),DLL(_ABCDLLMode_)
Success                 BYTE
ID                      ULONG
Date_In                 LONG
Type                    BYTE
                                .


GL_Cl_MID_Class                 CLASS,TYPE,TYPE,MODULE('NClSMSSt.clw'),LINK('nclsmsst.clw',_ABCLinkMode_),DLL(_ABCDLLMode_)
Success                 BYTE                            ! This means?  Think it was meant to mean that we had got an item
Client_Sent_ID          ULONG
Sent_Reference          ULONG
Status                  BYTE
Delivered_Date          LONG
Delivered_Time          LONG
Date                    LONG
Time                    LONG
Last_Error_Code         SHORT
                                .


GL_Cl_RCM_Class                 CLASS,TYPE,TYPE,MODULE('NClSMSSt.clw'),LINK('nclsmsst.clw',_ABCLinkMode_),DLL(_ABCDLLMode_)
Success                 BYTE                                            ! See MID Success above
Client_CID              STRING(10)                      ! RJA1xxxx
User_Login              STRING(15)      !LIKE(USE:User_Login)            !    OF 'RMU'
Server_Rec_ID           ULONG                           !    OF 'RID'
Read_Status             BYTE                            !    OF 'RRS'
Originator_Address      STRING(30)                      !    OF 'ROA'
Data                    STRING(255)                     !    OF 'DAT'
Service_Time_Stamp      STRING(20)                      !    OF 'RSS'
Date                    LONG                            !    OF 'RDA'
Time                    LONG                            !    OF 'RTI'
Time_Zone               SHORT                           !    OF 'RTZ'
Message                 STRING(160)                     !    OF 'RME'
Address_No              ULONG                           !    OF 'RAN'
SMSC_Address            STRING(30)                      !    OF 'RSA'
Msg_No                  SHORT                           !    OF 'RMN'
Total_Msgs              SHORT                           !    OF 'RTM'
Type                    BYTE                            !    OF 'RTY'
TA_Date                 LONG                            !    OF 'RTD'
TA_Time                 LONG                            !    OF 'RTT'
Received_Date           LONG                            !    OF 'RRD'
Received_Time           LONG                            !    OF 'RRT'
Read                    BYTE                            !    OF 'RED'  ! These will never be assigned from the server for SMS Me clients.  See above.
User_ID_Received_For    ULONG                           !    OF 'RRF'
User_ID_Private_To      ULONG                           !    OF 'RPT'
Group_ID                ULONG                           !    OF 'RGI'
RP_Status               BYTE                            !    OF 'RST'
                                .

_EndOfInclude_