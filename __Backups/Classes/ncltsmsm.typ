!               Class Data Type declarations
EQU:Cl_Net_Packet_Size      EQUATE(2000)
EQU:Cl_Packet_Size          EQUATE(2000)
EQU:Cl_Sent_Msg_Size        EQUATE(1000)                ! Previously 500 - made 1000 for HTTP

! Msg Errors
EQU:Err_Client_Has_Msg      EQUATE(2)                   ! ERR:2|ERT:Client has msg for this Server ID already - Accepted

!EQU:Err_      EQUATE(513)
EQU:Err_Msg_Failed_Exists   EQUATE(34)                  ! ERR:34|ERT:Message Sending Failed (Send Client Msg. Ref. exists for Client Message ID: xx)
EQU:Err_User_Not_Found      EQUATE(12)                  ! ERR:12|ERT:No User found with that login (Login: xx)

! Object Errors
EQU:Err_Not_Initilised      EQUATE(-101)                ! Above 100 is EQU error range
EQU:Err_No_NetSimple_Obj    EQUATE(-102)
EQU:Err_RSR_Send_Failed     EQUATE(-103)
EQU:Err_NS_Obj_Send_Error   EQUATE(-104)
EQU:Err_NS_Obj_Not_Inited   EQUATE(-105)
EQU:Err_No_Server_Address   EQUATE(-106)
EQU:Err_No_Server_Port      EQUATE(-107)
EQU:Err_Q_Add_Read_Out_Sent EQUATE(-108)
EQU:Err_Q_Add_Read_Recd     EQUATE(-109)
EQU:Err_Q_Add_Read_RMIDs    EQUATE(-110)
EQU:Err_Add_Dump_Recd       EQUATE(-111)
EQU:Err_Add_Dump_RMIDs      EQUATE(-112)
EQU:Err_Add_Dump_Packets    EQUATE(-113)

EQU:Err_General_Add_Queue   EQUATE(-114)
EQU:Err_General_Bad_Param   EQUATE(-115)

EQU:Err_HTTP_Get            EQUATE(-116)


GL_Cl_Push_Users_Q_Type             QUEUE,TYPE
User_ID                     SHORT
User_Login                  STRING(15)
!Access_Level                LIKE(USE:Access_Level)
Password                    STRING(15)
!Default_Transport           LIKE(USE:Default_Transport)
!Default_Queuing             LIKE(USE:Default_Queuing)
!TCP_IP                      LIKE(USE:TCP_IP)
!TCP_IP_Login                LIKE(USE:TCP_IP_Login)
!TCP_IP_Password             LIKE(USE:TCP_IP_Password)
                                    .

GL_Cl_Servers_Q_Type            QUEUE,TYPE
TCP_IP_ID               ULONG                           ! Internal ID of this record
Server_Address          STRING(100)                     ! Address of the server (name or IP)
Server_Port             USHORT(9042)                    ! Port to connect to on the server
Default_User            SHORT                           ! Default user to send messages as
Send_User_Option        BYTE
Order                   SHORT   !LIKE(TCP:Order)
Use_Fixed_Order_Only    BYTE    !LIKE(TCP:Use_Fixed_Order_Only)
Connections             LONG    !LIKE(TCP:Connections)
                                .

GL_Cl_Messages_Q_Type           QUEUE,TYPE
Q_ID                    ULONG
Message                 STRING(EQU:Cl_Sent_Msg_Size)
Date_In_Q               LONG
Time_In_Q               LONG
Type                    BYTE                            ! 0 normal comm, 1 send message, 2 license request (RLI)
Ack_ID                  ULONG                           ! This clients ID for the message - as sent with the msg
Date_Sent               LONG
Time_Sent               LONG
Response                BYTE                            ! Not used in Out, used in Sent - added 30 May 2004
                                .

GL_Cl_Msg_Exp_Q_Type            QUEUE,TYPE
ID                      ULONG
Date_In                 LONG
Type                    BYTE
                                .
! --- see nclsmsme_st.inc
!GL_Cl_Msg_Exp_Class             CLASS,TYPE,MODULE('NClSMSMe_St.clw'),LINK('nclsmsme_st.clw',_ABCLinkMode_),DLL(_ABCDLLMode_)
!Success                 BYTE
!ID                      ULONG
!Date_In                 LONG
!Type                    BYTE
!                                .

GL_Cl_Packets_Q_Type            QUEUE,TYPE
Q_ID                    ULONG
Packet                  STRING(EQU:Cl_Packet_Size)
Len                     LONG
                                .

GL_Cl_Fields_Q                  QUEUE,TYPE
Field                   STRING(3)
Element                 STRING(500)
                                .


! -----------------------   User Q's (to be processed by the user)   -----------------------
GL_Cl_Trigger_Events_Q          QUEUE,TYPE
Event_ID            ULONG                           ! This entries ID
Event_Type          BYTE                            ! 0 = Ack Event, 1 = New Packet
Event_Trigger       LONG                            ! Event to trigger on receiving user info - make a Q??
Event_Thread        LONG                            ! Thread to trigger
                                .


GL_Cl_Licenses_Q                QUEUE,TYPE
Client_Reg_ID           STRING(50)
License_No              STRING(50)
Received_Date           LONG
Received_Time           LONG
                                .

GL_Cl_Audit_Q                   QUEUE,TYPE
Type                    CSTRING(30)
Level                   BYTE
Data1                   CSTRING(2500)
Data2                   CSTRING(2500)
Data3                   CSTRING(2500)
Date                    LONG
Time                    LONG
                                .

GL_Cl_MID_Q                     QUEUE,TYPE
Success                 BYTE                            ! This means?  Think it was meant to mean that we had got an item
Client_Sent_ID          ULONG
Sent_Reference          ULONG
Status                  BYTE
Delivered_Date          LONG
Delivered_Time          LONG
Date                    LONG
Time                    LONG
Last_Error_Code         SHORT
Date_In_Q               LONG
Time_In_Q               LONG
                                .

! --- see nclsmsme_st.inc
!GL_Cl_MID_Class                 CLASS,TYPE,TYPE,MODULE('NClSMSMe_St.clw'),LINK('nclsmsme_st.clw',_ABCLinkMode_),DLL(_ABCDLLMode_)
!Success                 BYTE                            ! This means?  Think it was meant to mean that we had got an item
!Client_Sent_ID          ULONG
!Sent_Reference          ULONG
!Status                  BYTE
!Delivered_Date          LONG
!Delivered_Time          LONG
!Date                    LONG
!Time                    LONG
!Last_Error_Code         SHORT
!                                .

GL_Cl_RCM_Q                     QUEUE,TYPE
Success                 BYTE                                            ! See MID Success above
User_Login              STRING(15)      !LIKE(USE:User_Login)            !    OF 'RMU'
Server_Rec_ID           ULONG                           !    OF 'RID'
Read_Status             BYTE                            !    OF 'RRS'
Originator_Address      STRING(30)                      !    OF 'ROA'
Data                    STRING(255)                     !    OF 'DAT'
Service_Time_Stamp      STRING(20)                      !    OF 'RSS'
Date                    LONG                            !    OF 'RDA'
Time                    LONG                            !    OF 'RTI'
Time_Zone               SHORT                           !    OF 'RTZ'
Message                 STRING(160)                     !    OF 'RME'
Address_No              ULONG                           !    OF 'RAN'
SMSC_Address            STRING(30)                      !    OF 'RSA'
Msg_No                  SHORT                           !    OF 'RMN'
Total_Msgs              SHORT                           !    OF 'RTM'
Type                    BYTE                            !    OF 'RTY'
TA_Date                 LONG                            !    OF 'RTD'
TA_Time                 LONG                            !    OF 'RTT'
Received_Date           LONG                            !    OF 'RRD'
Received_Time           LONG                            !    OF 'RRT'
Read                    BYTE                            !    OF 'RED'  ! These will never be assigned from the server for SMS Me clients.  See above.
User_ID_Received_For    ULONG                           !    OF 'RRF'
User_ID_Private_To      ULONG                           !    OF 'RPT'
Group_ID                ULONG                           !    OF 'RGI'
RP_Status               BYTE                            !    OF 'RST'
Date_In_Q               LONG
Time_In_Q               LONG
                                .


! --- see nclsmsme_st.inc
!GL_Cl_RCM_Class                 CLASS,TYPE,TYPE,MODULE('NClSMSMe_St.clw'),LINK('nclsmsme_st.clw',_ABCLinkMode_),DLL(_ABCDLLMode_)
!Success                 BYTE                                            ! See MID Success above
!Client_CID              STRING(10)                      ! RJA1xxxx
!User_Login              STRING(15)      !LIKE(USE:User_Login)            !    OF 'RMU'
!Server_Rec_ID           ULONG                           !    OF 'RID'
!Read_Status             BYTE                            !    OF 'RRS'
!Originator_Address      STRING(30)                      !    OF 'ROA'
!Data                    STRING(255)                     !    OF 'DAT'
!Service_Time_Stamp      STRING(20)                      !    OF 'RSS'
!Date                    LONG                            !    OF 'RDA'
!Time                    LONG                            !    OF 'RTI'
!Time_Zone               SHORT                           !    OF 'RTZ'
!Message                 STRING(160)                     !    OF 'RME'
!Address_No              ULONG                           !    OF 'RAN'
!SMSC_Address            STRING(30)                      !    OF 'RSA'
!Msg_No                  SHORT                           !    OF 'RMN'
!Total_Msgs              SHORT                           !    OF 'RTM'
!Type                    BYTE                            !    OF 'RTY'
!TA_Date                 LONG                            !    OF 'RTD'
!TA_Time                 LONG                            !    OF 'RTT'
!Received_Date           LONG                            !    OF 'RRD'
!Received_Time           LONG                            !    OF 'RRT'
!Read                    BYTE                            !    OF 'RED'  ! These will never be assigned from the server for SMS Me clients.  See above.
!User_ID_Received_For    ULONG                           !    OF 'RRF'
!User_ID_Private_To      ULONG                           !    OF 'RPT'
!Group_ID                ULONG                           !    OF 'RGI'
!RP_Status               BYTE                            !    OF 'RST'
!                                .

