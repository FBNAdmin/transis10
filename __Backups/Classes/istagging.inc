!ABCIncludeFile
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)


OMIT('_EndOfInclude_',_IStagging_)
_IStagging_ EQUATE(1)

    INCLUDE('shptagging.inc')
                    
ISTagClass    CLASS(shpTagClass),TYPE,MODULE('ISTagging.clw'),LINK('ISTagging.clw',_ABCLinkMode_),DLL(_ABCDllMode_)

Inited          BYTE

Access_Tags     &FileManager
Access_Passed   BYTE

Tags_Tbl        &FILE
R_Tags_Rec      &GROUP
R_Key           &KEY
R_TH_ID         ANY
R_Ptr           ANY
R_KEY_T_ID      &KEY
R_T_ID          ANY


Construct       PROCEDURE
Destruct        PROCEDURE

Init            PROCEDURE(FILE p_Tag_File, <*FileManager p_Access_Tags>),BYTE

IS_Save_Tags    PROCEDURE(ULONG p_TH_ID)
              End

_EndOfInclude_






