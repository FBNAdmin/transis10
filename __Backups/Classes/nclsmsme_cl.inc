
!ABCIncludeFile

OMIT('_EndOfInclude_',_SMSMeNCl_)
_SMSMeNCl_ EQUATE(1)

    INCLUDE('NClSMSMe_Cl.TYP'),ONCE                                 ! Type declarations


!MyNetSimple          CLASS(NetSimple),TYPE
!                     .


SMSMeNet_Class                  CLASS,TYPE,MODULE('NClSMSMe_Cl.clw'),LINK('NClSMSMe_Cl.clw',_ABCLinkMode_),DLL(_ABCDLLMode_)
          ! ---------------------------   Properties   ---------------------------
!Dump_File               FILE,DRIVER('BASIC'),CREATE,PRE(SCL_DUM)
!Rec                     RECORD
!Field                       STRING(500),DIM(30)
!                        . .

!Dump_Out_Q              PROCEDURE(ULONG p:Recs, *FILE p_File, STRING p:File_Name, BYTE p:Delete, BYTE p:Expire_Mode, Call_our_proc_type Call_our_proc_in, ULONG p:Persist_Q_Trans, LONG p:Persist_Q_Days, QUEUE p_Q),LONG,PROC
!Call_our_Proc           PROCEDURE(BYTE p:Type, GL_Cl_Msg_Exp_Q_Type p_Q, BYTE p:Opt),LONG,PROC

Cl_NetSimple            &NetSimple
Set_Cl_NetSimple        PROCEDURE(<NetSimple p_NetS>),LONG,PROC

Event_ID                ULONG                           ! Incremented every take event after init

Start_Delim             CSTRING(20)
Field_Delim             CSTRING(20)
Data_Delim              CSTRING(20)
End_Delim               CSTRING(20)

Server_Q_ID             ULONG                           ! Server connected ID - if using server Q
Server_G                GROUP,PRE(L_Svr)
Address                     STRING(150)       ! ( M )   ! Connected on
Port                        LONG                        ! Port Connected on
Send_User_Option            BYTE                        ! Send all as default user option
Default_User                SHORT                       ! Default user
TCP_IP_ID                   ULONG
                        .

Out_Msgs_Q              &GL_Cl_Messages_Q_Type
Sent_Msgs_Q             &GL_Cl_Messages_Q_Type
Send_Msgs_Exp_Q         &GL_Cl_Msg_Exp_Q_Type           ! Msgs that have expired and will not be attempted again

In_Packets_Q            &GL_Cl_Packets_Q_Type
Recd_MID_Q              &GL_Cl_MID_Q
Recd_RCM_Q              &GL_Cl_RCM_Q

Push_Users_Q            &GL_Cl_Push_Users_Q_Type
Net_Servers_Q           &GL_Cl_Servers_Q_Type
Audit_Q                 &GL_Cl_Audit_Q

Licenses_Q              &GL_Cl_Licenses_Q               ! Queue of received licenses
Trigger_Events_Q        &GL_Cl_Trigger_Events_Q

Recd_Str                STRING(2000)                    ! Received, unprocessed text (incomplete message)
Recd_S_Len              LONG                            ! Length of data in Recd_Str

          ! ---------------------------   Config Options   ---------------------------
Ini_File                STRING(150)
Ini_Section             STRING(150)

CID_Group               GROUP,PRE(L_CID)
Prefix                      STRING(4)
CID                         ULONG                       ! Client ID
                        .

Cl_Login                STRING(50)  !,PRIVATE    ! ( M )   ! The default user login to use
Cl_Password             STRING(50)  !,PRIVATE    ! ( M )   ! The default user password

Re_Send_Time_Out        LONG                            ! Max 24 hours
No_Re_Send_Hours        LONG                            ! No resend after this many hours
Wait_Ack_Days           LONG                            ! How many days to wait for an ack

Rec_Msg_Mode            BYTE                            ! 0 - Pull, 1 - Push (def.)
Send_Per_Event          ULONG                           ! def 5
Receive_Per_Event       ULONG                           ! def 5

Persist_Q_Days          LONG                            ! days entries in the persistant Q's last - def 7
Persist_Q_Trans         LONG                            ! trans persist Q entries saved up to this limit - def 100

RSR_State_Check         BYTE                            ! Do we want to check for RSR on new connection - default on
Send_RMM_CID            BYTE                            ! Send RMM & CID                                - default on

User_Connect_Req        BYTE                            ! 0 - no connect, 1 - connect when required send, 2 - connect when required awaiting acks (or send) (def), 3 - always try connect
Dump_Data_Opt           BYTE,PROTECTED                  ! 0 - none, 1 - incoming, 2 - outgoing, 3 - both

          ! ---------------------------   State Indicators / Requests   ---------------------------
Init_ed                 BYTE                            ! Class initilised
Connect_Req             BYTE    !,PRIVATE                    ! 0 - No req. (def - depends on User_Connect_Req - set in init), 1 - Open, 2 - Close
Connect_State           BYTE    !,PRIVATE                    ! 0 - closed, 1 - open, 2 - openning, 3 - Open & Setup, 4 - closing
RSR_State               BYTE                            ! 0 - not sent, 1 - sent, 2 - confirmed
Connection_Attemps      SHORT

Server_Msgs_No          ULONG                           ! Msgs waiting on server for this client - RMW
Server_Sys_Cmd_Last     STRING(150) !,PRIVATE             ! Last command from server
Server_RSR_Resp         STRING(150)                     ! Servers RSR response

Last_Error_Code         LONG                            ! Last error code
Error_Sent_ID           ULONG                           ! Error was for this msg. ID

Sent_ID                 ULONG                           ! Persistant Sent ID - used if user does not provide
Last_Sent_ID            ULONG                           ! 

          ! ---------------------------   User - Posting Methods   ---------------------------
Check_Sent_Msg          PROCEDURE(ULONG p:Sent_ID=0, <*LONG p:Date_In>, <*BYTE p:Type>, BYTE p:Remove=0),LONG   ! p:Sent_ID - Opt Remove
Add_Trigger_Event       PROCEDURE(LONG p:Event, LONG p:Thread=0, BYTE p:Event_Type=0, BYTE p:Trigger_Now=0), ULONG, PROC
Trigger_Events          PROCEDURE(BYTE p:Event_Type=0)                     ! In Posting window

          ! ---------------------------   SMS Methods   ---------------------------
Take_Event              PROCEDURE(),VIRTUAL
Send                    PROCEDURE(STRING p:Send_Str),LONG

          ! ---------------------------   Received Methods - User   ---------------------------
Read_Received_Msg       PROCEDURE(<ULONG p:Pos>, BYTE=1),*GL_Cl_RCM_Class       ! opt Delete
Read_Received_Msg_G     PROCEDURE(<ULONG p:Pos>, BYTE=1),STRING                 ! opt Delete
Read_Recd_MIDs          PROCEDURE(<ULONG p:Pos>, BYTE=1),*GL_Cl_MID_Class       ! opt Delete
Read_Recd_MIDs_G        PROCEDURE(<ULONG p:Pos>, BYTE=1),STRING                 ! opt Delete

Recd_Msg_Ack_Send       PROCEDURE(ULONG p:Server_Rec_ID, <ULONG p:Clients_ID>, LONG p:Result=0), LONG, PROC  ! Used internally also
Req_Recd_Msg_Usr        PROCEDURE(STRING p:Login, STRING p:Password, BYTE p:Mode = 0, LONG p:Req_Number=0), LONG

Read_License            PROCEDURE(<STRING p:Reg_ID>, BYTE p:Delete = 0), STRING

          ! ---------------------------   Received Methods   ---------------------------
Receive_Msg             PROCEDURE(<ULONG p:No_to_Rec>), ULONG, PROC, PROTECTED
Recd_Add_Packet_Q       PROCEDURE(STRING p:Packet, LONG p:Packet_Len),LONG,PROC

Recd_Process_Recd_Str   PROCEDURE(STRING p:Packet, LONG p:Packet_Len),LONG,VIRTUAL  ! HTTP subclassed - calls Recd_Process_Packet
Recd_Process_Packet     PROCEDURE(STRING p:Packet),LONG,PROC,VIRTUAL                ! Returns failure to find proc. Type (-1)

Process_RCM             PROCEDURE(GL_Cl_Fields_Q p_Field_Q),LONG,PROC       ! Recd. message
Process_MID             PROCEDURE(GL_Cl_Fields_Q p_Field_Q),LONG,PROC       ! Sent msg. server ID
Process_RPU             PROCEDURE(<GL_Cl_Fields_Q p_Field_Q>),LONG,PROC     ! Send Push users - returns count of users sent

Dump_In_Packets         PROCEDURE(<STRING p:File_Name>, BYTE=1, ULONG=0, BYTE=1),LONG,PROC  ! Delete, Pos

Dump_Received_Msgs      PROCEDURE(<STRING p:File_Name>, BYTE=1, BYTE=0),LONG,PROC   ! Dump all recd Q, Expire mode (0,1,2,3)
Dump_Recd_MIDs          PROCEDURE(<STRING p:File_Name>, BYTE=1, BYTE=0),LONG,PROC   ! Dump all recd MIDs, Expire mode (0,1,2,3)
ReadIn_Received_Msgs    PROCEDURE(<STRING p:File_Name>, BYTE=0),LONG,PROC   ! Read In dumped items - Delete
ReadIn_Recd_MIDs        PROCEDURE(<STRING p:File_Name>, BYTE=0),LONG,PROC   !

Dump_Licenses_Q         PROCEDURE(<STRING p:File_Name>, <GL_Cl_Licenses_Q p_D_Q>, BYTE p:Delete=1),LONG,PROC
ReadIn_Licenses_Q       PROCEDURE(<STRING p:File_Name>, <GL_Cl_Licenses_Q p_D_Q>, BYTE p:Delete=1),LONG,PROC

          ! ---------------------------   Send Methods - User   ---------------------------
Add_Send_Msg            PROCEDURE(<STRING p:Login>, <STRING p:Password>, STRING p:Cell_No, STRING p:Msg, <*LONG p:Msg_ID>, |
                                  <*BYTE p:Q_Opt>, <*BYTE p:Delivery_Confirm>, <*LONG p:Feature_ID>, |
                                  <*STRING p_Deliver_Date_Time_Group>, <*STRING p_Concat_M_Group>),LONG,PROC,VIRTUAL ! HTTP subclassed
Request_License         PROCEDURE(STRING p:Reg_ID, <STRING p:Type>, <STRING p:Desc>), LONG, PROC

          ! ---------------------------   Send Methods   ---------------------------
Send_Msg                PROCEDURE(ULONG = 0, <ULONG p:No_to_Send>),ULONG,PROC,PROTECTED,VIRTUAL
Re_Send_Msg             PROCEDURE(LONG p:No_To_Send=0),LONG,PROC,PROTECTED

Add_Send_Msg_to_Q       PROCEDURE(STRING p:Msg, LONG=0, BYTE=1, BYTE=0, ULONG=0, LONG=0, LONG=0),ULONG,PROC  ! Q Pos, Make packet, type, ack_ID
Resend_Sent_Msg         PROCEDURE(STRING p:Msg, LONG p:Position=0, BYTE p:Make_Packet=1, BYTE p:Type=0, ULONG p:Ack_ID=0, LONG p:Date_Sent_Prev=0, LONG p:Time_Sent_Prev=0),ULONG,PROC,VIRTUAL

Make_Packet             PROCEDURE(STRING p:Packet),STRING                               ! Dynamic string needed!???
Add_Field_Packet        PROCEDURE(STRING p:Send_Field, <STRING p:Send_Data>, <*STRING p:Packet>, BYTE=0),STRING,PROC    ! Opt. add to back (0) or front (1)

Dump_Out_Sent_Msgs      PROCEDURE(<STRING p:File_Name>, BYTE=1, BYTE=0, <GL_Cl_Messages_Q_Type p_D_Q>, <ULONG p:Persist_Q_Trans>, <LONG p:Persist_Q_Days>),LONG,PROC   ! Delete, Expire mode (0,1,2,3)
ReadIn_Out_Sent_Msgs    PROCEDURE(<STRING p:File_Name>, BYTE=0, <GL_Cl_Messages_Q_Type p_D_Q>),LONG,PROC   ! Delete

Sent_Msgs_Add           PROCEDURE()
Sent_Msgs_Set_Time      PROCEDURE()
Sent_Msg_Ack            PROCEDURE(ULONG p:Client_Sent_ID, ULONG p:Sent_Reference, BYTE L:Delete_Sent=0),LONG,PROC  ! Called in Process_MID
Send_Push_Users         PROCEDURE(), ULONG, PROC

          ! ---------------------------   Msg Sent / Msg Ack Methods   ---------------------------
Dump_Out_Msgs_Expired   PROCEDURE(<STRING p:File_Name>, BYTE=1, BYTE=0, <GL_Cl_Msg_Exp_Q_Type p_D_Q>, <ULONG p:Persist_Q_Trans>, <LONG p:Persist_Q_Days>),LONG,PROC
Add_Msg_Expired         PROCEDURE(ULONG p:ID, BYTE p:Type)
Read_Expired_Msg        PROCEDURE(<ULONG p:Pos>, BYTE=0),*GL_Cl_Msg_Exp_Class   ! opt Delete

          ! ---------------------------   Audit Methods   ---------------------------
Add_Audit               PROCEDURE(<STRING p:Data1>, <STRING p:Data2>, <STRING p:Data3>, <BYTE p:Level>, <STRING p:Type>, BYTE p:Date_Time_Stamp=1)
Dump_Audit              PROCEDURE(<STRING p:Loc_Name>, BYTE=1),LONG,PROC    ! Opt1, Delete audit q after dump
Read_Audit              PROCEDURE(<ULONG p:Pos>, BYTE=0),STRING             ! Opt1, Delete entry after read



          ! ---------------------------   General Methods - User  ---------------------------
Set_RSR_State_Check     PROCEDURE(BYTE p:RSR_State_Check)
Set_Send_RMM_CID        PROCEDURE(BYTE p:Set_Send)

Set_Last_Error          PROCEDURE(LONG p:Err_Code, ULONG p:Sent_ID=0)

!nclPosting              PROCEDURE(BYTE p:Init_NetClass=1, BYTE p:Init_Connect_Req=0, BYTE p:Hide_Window=1)

          ! ---------------------------   General Methods   ---------------------------
Error_Text              PROCEDURE(<LONG p:Error_Code>),STRING

Add_Net_Server          PROCEDURE(STRING p:Server, STRING p:Port, BYTE p:Free_Q = 0, |      ! Adds to Net_Servers_Q
                                  ULONG p:ID=0, <STRING p:Def_User>, BYTE p:Send_User_Opt=0, LONG p:Order=0, |
                                  BYTE p:Fixed_Ord=0, LONG p:Connections=0),LONG,PROC

Open_Dump_File          PROCEDURE(*FILE, STRING, BYTE=1),LONG
Dump_Data               PROCEDURE(STRING p:FileName, STRING p:Data, LONG=0, BYTE p:Date_Time_Stamp = 1),LONG,PROC   ! Dump text to file

Send_RSR                PROCEDURE(), LONG, PROTECTED           ! Check status of the server - is it a SMS Me Net server
Save_INIs               PROCEDURE(STRING p:Type),LONG,PROC

Check_State             PROCEDURE(BYTE = 0),LONG,PROC           ! Checks and updates Connect_State
Set_State               PROCEDURE(BYTE p:State)                 ! Sets state

Connect                 PROCEDURE(<STRING p:IP>, <STRING p:Port>),LONG
Disconnect              PROCEDURE(), LONG, PROC                ! Returns -2 if there was no connection present at all
Reconnect               PROCEDURE()
Connected               PROCEDURE()

Check_User_Connect_Req_Opt  PROCEDURE()

Connect_Failed          PROCEDURE()                     ! Async or other connection has failed
Connection_Closed       PROCEDURE(),VIRTUAL             ! Must always be called if a connection is closed

Add_Push_User           PROCEDURE(<STRING p:User_Login>, <STRING p:User_Password>, BYTE p:Free = 0, BYTE p:Send_Option = 0),LONG

Set_Def_User            PROCEDURE(STRING p:Login, STRING p:Password)

!Save_Servers           PROCEDURE()
!Add_Server             PROCEDURE()

Load_Servers_Ini        PROCEDURE(BYTE p:Free_Servers_Q = 1),LONG,PROC

Init                    PROCEDURE(<STRING p:Ini_File>, <STRING p:Ini_Section>, BYTE p:Connect_Req=0, BYTE p:Free_Servers=0, BYTE p:Load_Servers=0)
Init                    PROCEDURE(*FileManager p_SMSMeNet_Servers, <STRING p:Ini_File>, <STRING p:Ini_Section>, BYTE=0)
Init_Final              PROCEDURE(STRING p:Ini_File, STRING p:Ini_Section, BYTE p:Connect_Req),PRIVATE

Setup                   PROCEDURE()

Dump_Queues             PROCEDURE()
Read_In_Qs              PROCEDURE()

Construct               PROCEDURE                       ! Allocate / deallocate newed variables
Destruct                PROCEDURE

Replace_Chars           PROCEDURE(*STRING p:String, STRING p:To_Replace, *CSTRING p:Replacement)
                                .


_EndOfInclude_

