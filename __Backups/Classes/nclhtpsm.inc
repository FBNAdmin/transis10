
!ABCIncludeFile(nclsmsme)

OMIT('_EndOfInclude_',_SMSMeNClH_)
_SMSMeNClH_ EQUATE(1)

!    INCLUDE('NClSMSMe_Cl.TYP'),ONCE                                 ! Type declarations
    INCLUDE('nclSMSMe.INC'),ONCE                                 ! Type declarations

EQU:CRLF            EQUATE('<13,10>')


!MyNetSimple          CLASS(NetSimple),TYPE
!                     .


SMSMeHTTP_Class                  CLASS(SMSMeNet_Class),TYPE,MODULE('nclHTPSM.clw'),LINK('nclHTPSM.clw',_ABCLinkMode_),DLL(_ABCDLLMode_)

          ! ---------------------------   Properties - Config   ---------------------------
!SELF.Init_ed
!SELF.Cl_Login
!SELF.Cl_Password

!User_Name           STRING(100)
!Password            STRING(100)

Path                STRING(100)         ! /cgi-bin
Page                STRING(100)         ! sendsms

From                STRING(100)         ! send from string

Response_Req        STRING(100)         ! dlrmask=24 + 3 for delivery notice
Post_Back_To        STRING(100)         ! dlrurl=http://www.infosolutions.co.za:9046/Page.asp?SMSCref=%25c%26DeliveryNotice=%25d

User_Agent          STRING(100)         ! SMSMeHTTP - www.infosolutions.co.za
Connection          STRING(100)         ! Connection: Keep-Alive

Client_Thread       SIGNED              ! Client Windows THREAD - to post stuff to

          ! ---------------------------   Properties - State   ---------------------------
Send_Complete       BYTE
Cur_Sent_ID         ULONG
Last_Send_At_Date   LONG
Last_Send_At_Time   LONG

HTTP_Send_TimeOut   LONG

          ! ---------------------------   Methods   ---------------------------
Set_Def_User            PROCEDURE(STRING p:Login, STRING p:Password)

Recd_Process_Packet     PROCEDURE(STRING p:Packet),LONG,VIRTUAL,PROC
Recd_Process_Recd_Str   PROCEDURE(STRING p:New_Packet, LONG p:Packet_Len),LONG,VIRTUAL

Check_Send_Complete     PROCEDURE(),BYTE
Set_Send_Complete       PROCEDURE(BYTE p:State=0),BYTE,PROC

Send_Failed             PROCEDURE(BYTE p:Type)

          ! ---------------------------   Send Methods - User   ---------------------------
Send_Msg                PROCEDURE(),ULONG,VIRTUAL

Add_Send_Msg            PROCEDURE(<STRING p:Login>, <STRING p:Password>, STRING p:Cell_No, STRING p:Msg, <*LONG p:Msg_ID>, |
                                  <*BYTE p:Q_Opt>, <*BYTE p:Delivery_Confirm>, <*LONG p:Feature_ID>, |
                                  <*STRING p_Deliver_Date_Time_Group>, <*STRING p_Concat_M_Group>),LONG,PROC,VIRTUAL      ! HTTP subclassed

Resend_Sent_Msg         PROCEDURE(STRING p:Msg, LONG p:Position=0, BYTE p:Make_Packet=1, BYTE p:Type=0, ULONG p:Ack_ID=0, |
                                  LONG p:Date_Sent_Prev=0, LONG p:Time_Sent_Prev=0),ULONG,PROC,VIRTUAL                    ! HTTP subclassed

Set_From_Str            PROCEDURE(<STRING p:From>),STRING,PROC              ! Default from to INI value

Take_Event              PROCEDURE(LONG p:No_To_Send=5),VIRTUAL

Connection_Closed       PROCEDURE(),VIRTUAL                                 ! Alway called when connection is closed??


Construct               PROCEDURE 
!Destruct                PROCEDURE

EncodeWebString         PROCEDURE  (STRING p_PostString),STRING
DecodeWebString         PROCEDURE  (STRING p_PostString),STRING
                    .

_EndOfInclude_