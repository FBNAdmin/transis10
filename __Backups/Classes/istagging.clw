
  MEMBER

  omit('***',_c55_)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
  ***

   Include('ISTagging.inc'),ONCE
   Include('ABFILE.INC'),ONCE
   Include('ABERROR.INC'),ONCE

  Map
  End


ISTagClass.Init             PROCEDURE(FILE p_Tag_File, <*FileManager p_Access_Tags>)

R:Idx       ULONG
R:Result    BYTE,DIM(5)
l:Result    BYTE

    CODE
    IF ~OMITTED(2)
       SELF.Access_Tags    &= p_Access_Tags
       SELF.Access_Tags.Open()
       SELF.Access_Tags.UseFile()
       SELF.Access_Passed   = TRUE
    .

    l:Result         = TRUE
    SELF.Tags_Tbl   &= p_Tag_File

    SELF.R_Tags_Rec &= SELF.Tags_Tbl{PROP:Record}

    LOOP R:Idx = 1 TO SELF.Tags_Tbl{PROP:Keys}
        SELF.R_Key &= SELF.Tags_Tbl{PROP:Key,R:Idx}
        IF INSTRING('FKEY_THID_TID',SELF.R_Key{PROP:Label},1)
           R:Result[1]  = TRUE
           BREAK
    .   .

    LOOP R:Idx = 1 TO SELF.Tags_Tbl{PROP:Keys}
        SELF.R_KEY_T_ID &= SELF.Tags_Tbl{PROP:Key,R:Idx}
        IF INSTRING('PKEY_T_ID',SELF.R_KEY_T_ID{PROP:Label},1)
           R:Result[2]  = TRUE
           BREAK
    .   .

    LOOP R:Idx = 1 TO SELF.Tags_Tbl{PROP:Fields}
        IF INSTRING('TH_ID',SELF.Tags_Tbl{PROP:Label,R:Idx},1)
           SELF.R_TH_ID &= WHAT(SELF.R_Tags_Rec, R:Idx)         ! = R:Idx
           R:Result[3]  = TRUE
           BREAK
    .   .

    LOOP R:Idx = 1 TO SELF.Tags_Tbl{PROP:Fields}
        IF INSTRING('PTR',SELF.Tags_Tbl{PROP:Label,R:Idx},1)
           SELF.R_Ptr  &= WHAT(SELF.R_Tags_Rec, R:Idx)
           R:Result[4]  = TRUE
           BREAK
    .   .

    LOOP R:Idx = 1 TO SELF.Tags_Tbl{PROP:Fields}
        IF INSTRING('T_ID',SELF.Tags_Tbl{PROP:Label,R:Idx},1)
           SELF.R_T_ID  &= WHAT(SELF.R_Tags_Rec, R:Idx)
           R:Result[5]  = TRUE
           BREAK
    .   .

    SELF.Inited = TRUE
    LOOP R:Idx = 1 TO 5
       IF R:Result[R:Idx] ~= TRUE
          l:Result    = FALSE
          SELF.Inited = FALSE
          MESSAGE('File does not contain field: ' & R:Idx)
    .  .
    RETURN(l:Result)


ISTagClass.IS_Save_Tags        PROCEDURE(ULONG p_TH_ID)

Prog_Window WINDOW,AT(,,133,43),FONT('MS Sans Serif',,,,CHARSET:ANSI),CENTER,GRAY,MDI
       PROMPT('Please wait one moment.....'),AT(14,12),USE(?Prompt1),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
       PROMPT(''),AT(5,28,123,10),USE(?Prompt_Prog),CENTER
     END

R:Idx       ULONG
R:Dots      STRING(50)
R:Orig      CSTRING(100)
R:Logout    BYTE
R:Next_ID   ULONG

!Tags_Tbl        &FILE
!R_Tags_Rec      &GROUP
!R_Key           &KEY
!R_TH_ID         &ANY
!R_Ptr           &ANY
!R_KEY_T_ID      &KEY
!R_T_ID          &ANY

    CODE
    IF SELF.Inited = FALSE
       MESSAGE('The IS Tagging class has not been initialised.','Object Not Ready',ICON:Hand)
    .

    Open(Prog_Window)
    DISPLAY
    SETCURSOR(CURSOR:Wait)

    LOGOUT(1,SELF.Tags_Tbl)
    IF ~ERRORCODE()
       R:Logout = TRUE
    .

    R:Orig                  = 'Removing old tags..'
    ?Prompt_Prog{PROP:Text} = R:Orig
    CLEAR(SELF.R_Tags_Rec,-1)
    SELF.R_TH_ID = p_TH_ID
    SET(SELF.R_Key, SELF.R_Key)
    LOOP
       NEXT(SELF.Tags_Tbl)
       IF ERRORCODE() OR SELF.R_TH_ID ~= p_TH_ID
          BREAK
       .
       Delete(SELF.Tags_Tbl)
       R:Dots                   = CLIP(R:Dots) & '.'
       IF LEN(CLIP(R:Dots)) > 10
          R:Dots    = ''
       .
       ?Prompt_Prog{PROP:Text}  = R:Orig & R:Dots
    .

    R:Orig                  = 'Adding new tags..'
    ?Prompt_Prog{PROP:Text} = R:Orig
    R:Idx   = 0
    LOOP
       R:Idx    += 1
       GET(SELF.TagQueue, R:Idx)
       IF ERRORCODE()
          BREAK
       .

       R:Dots   = CLIP(R:Dots) & '.'
       IF LEN(CLIP(R:Dots)) > 10
          R:Dots    = ''
       .
       ?Prompt_Prog{PROP:Text}  = R:Orig & R:Dots

       DO Prime_Tags_Rec
       IF R:Next_ID ~= 0
          SELF.R_TH_ID           = p_TH_ID
          SELF.R_Ptr             = SELF.TagQueue.Ptr

          PUT(SELF.Tags_Tbl)
    .  .

    IF R:Logout = TRUE
       COMMIT
    .

    SETCURSOR()
    CLOSE(Prog_Window)


!    R:TH_ID = p_TH_ID
!    CLEAR(TAGH:Record)
!    TAGH:TH_ID  = R:TH_ID
!    IF Access:Tags_Header.TryFetch(TAGH:PKEY_THID) = LEVEL:Benign
!       TAGH:Entries    = SELF.NumberTagged()
!       IF Access:Tags_Header.TryUpdate() = LEVEL:Benign
!    .  .
!    IF LOC:Quiet = FALSE
!       MESSAGE('All the Tags have been saved. (' & SELF.NumberTagged() & ')||Purpose Name "' & CLIP(TAGH:Purpose) & '"','Notice',ICON:Exclamation)
!    .
    RETURN


Prime_Tags_Rec      ROUTINE
    DATA
R:Tries     SHORT

    CODE
    LOOP
       R:Tries  += 1
       IF R:Tries > 10
          R:Next_ID = 0
          BREAK
       .
       CLEAR(SELF.Tags_Tbl)
       SET(SELF.R_KEY_T_ID)
       PREVIOUS(SELF.Tags_Tbl)
       IF ERRORCODE()
          R:Next_ID    = 1
       ELSE
          R:Next_ID    = SELF.R_T_ID + 1
       .
       CLEAR(SELF.Tags_Tbl)
       SELF.R_T_ID          = R:Next_ID
       ADD(SELF.Tags_Tbl)
       IF ~ERRORCODE()
          BREAK
    .  .
    EXIT




ISTagClass.Construct          Procedure
   CODE
   RETURN


ISTagClass.Destruct           Procedure
   CODE
   IF SELF.Access_Passed   = TRUE
      SELF.Access_Tags.Close()
   .
   RETURN


