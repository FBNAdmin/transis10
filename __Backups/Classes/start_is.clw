    SECTION('Map')
Start_IS_PT1                PROCEDURE(STRING),TYPE
Start_IS_PT2                PROCEDURE(STRING, STRING),TYPE
Start_IS_PT3                PROCEDURE(STRING, STRING, STRING),TYPE

Start_IS                    PROCEDURE(Start_IS_PT1, LONG=10000, <STRING>),SIGNED,PROC
Start_IS                    PROCEDURE(Start_IS_PT2, LONG=10000, <STRING>, <STRING>),SIGNED,PROC
Start_IS                    PROCEDURE(Start_IS_PT3, LONG=10000, <STRING>, <STRING>, <STRING>),SIGNED,PROC

    SECTION('Procs')
Start_IS                    PROCEDURE(Start_IS_PT1 p_Proc, LONG p_Stack, <STRING p1>)
LOC:Start       SIGNED
    CODE
    IF OMITTED(1)
       p1       = ''
    .

    LOOP 5 TIMES
       LOC:Start    = START(p_Proc, p_Stack, p1)
       IF LOC:Start ~= 0
          BREAK
    .  .

    IF LOC:Start = 0                    ! Still not started - then do it on this thread
       p_Proc(p1)
    .
    RETURN(LOC:Start)

Start_IS                    PROCEDURE(Start_IS_PT2 p_Proc, LONG p_Stack, <STRING p1>, <STRING p2>)
LOC:Start       SIGNED
    CODE
    IF OMITTED(1)
       p1       = ''
    .
    IF OMITTED(2)
       p2       = ''
    .

    LOOP 5 TIMES
       LOC:Start    = START(p_Proc, p_Stack, p1, p2)
       IF LOC:Start ~= 0
          BREAK
    .  .

    IF LOC:Start = 0                    ! Still not started - then do it on this thread
       p_Proc(p1,p2)
    .
    RETURN(LOC:Start)


Start_IS                    PROCEDURE(Start_IS_PT3 p_Proc, LONG p_Stack, <STRING p1>, <STRING p2>, <STRING p3>)
LOC:Start       SIGNED
    CODE
    ! (Add_Message_Log_Entry, <STRING>, <STRING>, <STRING>)
    ! (p_Proc, p1, p2, p3)

    IF OMITTED(1)
       p1       = ''
    .
    IF OMITTED(2)
       p2       = ''
    .
    IF OMITTED(3)
       p3       = ''
    .

    LOOP 5 TIMES
       LOC:Start    = START(p_Proc, p_Stack, p1, p2, p3)
       IF LOC:Start ~= 0
          BREAK
    .  .

    IF LOC:Start = 0                    ! Still not started - then do it on this thread
       p_Proc(p1, p2, p3)
    .

    RETURN(LOC:Start)
