
!ABCIncludeFile

OMIT('_EndOfInclude_',_ISFiler_)
_ISFiler_ EQUATE(1)

Dir_List_Q_Type         QUEUE,TYPE
Name                        STRING(FILE:MAXFILENAME)        ! FILE:MAXFILENAME is an EQUATE
Shortname                   STRING(13)
Date                        LONG
Time                        LONG
Size                        LONG
Attrib                      BYTE                            ! A bitmap, the same as the attributes EQUATEs
Location                    STRING(256)
                        .


IS_Files_Find                   CLASS(),TYPE,MODULE('C:\Projects\_Classes\is_filer.clw'),LINK('C:\Projects\_Classes\is_filer.clw')!,_ABCLinkMode_),DLL(_ABCDllMode_)

L:Search_All_Drives     BYTE
L:Include_Sub_Dirs      BYTE
L:Extension_List        STRING(256)
L:Where_To_Search       STRING(256)
L:Minimum_File_Size     ULONG
L:Drives                STRING(26)

L:Current_Path          STRING(256)

L:Done_Dirs_Q           &Dir_List_Q_Type
L:All_Files_Q           &Dir_List_Q_Type

LS_Progress             LONG                                 ! Screen controls
LS_Dirs_ToDo            ANY
LS_Total_Files          ANY
LS_Total_Dirs           ANY

L:Files_Q_Next_Pos      ULONG


Construct               PROCEDURE()
Destruct                PROCEDURE()
Init                    PROCEDURE(<LONG p_Progress>, <*? p_Tot_Files>, <*? p_Tot_Dirs>, <*? p_Dirs_Todo>)
Search_Dirs             PROCEDURE()
Get_Next_Files_Q        PROCEDURE(<ULONG p_Position>, <ULONG p_Cur_Entry>),STRING
Set_Next_Files_Q_Pos    PROCEDURE(<ULONG p_Position>),ULONG,PROC


Check_Extensions        PROCEDURE(STRING p_FileName), LONG
Enumerate_Drives        PROCEDURE(BYTE p:Show_Drives_List = 0, <STRING p:Drive_List_To_Check>),STRING

Sort_Q                  PROCEDURE(LONG p_Type)          ! ???????????
                                .       ! End Class

!       LOC:IS_Files            IS_Files_Find

_EndOfInclude_
