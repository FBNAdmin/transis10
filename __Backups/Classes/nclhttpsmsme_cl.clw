  MEMBER

   Include('ABFILE.INC'),ONCE
!   Include('ABError.INC'),ONCE
!   Include('NetTalk.INC'),ONCE

  Map
    MODULE('NClSMSMe_Cl')
Get_1st_Element_From_Delim_Str          PROCEDURE(*STRING, STRING, BYTE =0),STRING
    End
  End

  omit('***',_c55_)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
  ***

   Include('NClHTTPSMSMe_Cl.inc'),ONCE


! ---------   Add_Send_Msg (M)   ---------
SMSMeHTTP_Class.Add_Send_Msg            PROCEDURE(<STRING p:User_Login>, <STRING p:User_Password>, STRING p:Cell_No, |
                                                  STRING p:Msg, <*LONG p:Msg_ID>, <*BYTE p:Q_Opt>, <*BYTE p:Delivery_Confirm>, |
                                                  <*LONG p:Feature_ID>, <*STRING p_Deliver_Date_Time_Group>, |
                                                  <*STRING p_Concat_M_Group>)

L:Result            LONG
L:Msg_Str           STRING(5000)

!L_Deliver_Date_Time_Group   GROUP,PRE(L_DTG)
!Before_Date         LONG
!Before_Time         LONG
!After_Date          LONG
!After_Time          LONG
!                            .

!L_Concat_M_Group            GROUP,PRE(L_CMG)
!Concat_Msg          BYTE
!Msg_No              BYTE
!Total_Msgs          SHORT
!Concat_ID           LONG                        ! New field - Feb 19
!                            .

!LOC:Q_Opt           BYTE

L:User_Login        STRING(100)
L:Password          STRING(100)

    CODE
 SELF.Dump_Data('__Speed_Dump.TXT', 'Add_Send_Msg    ..Msg_ID: ' & p:Msg_ID)

    IF SELF.Init_ed = TRUE
       IF OMITTED(2) OR CLIP(p:User_Login) = ''
          L:User_Login      = SELF.Cl_Login
          IF OMITTED(3) OR CLIP(p:User_Password) = ''
             L:Password     = SELF.Cl_Password
          .
       ELSE
          L:User_Login      = p:User_Login
          IF OMITTED(3)
             L:Password     = ''
          ELSE
             L:Password     = p:User_Password
       .  .


       !    GET /cgi-bin/sendsms?username=infosol&password=7dsz2k&from=27834471572&to=27834471572&text=test+from+mobility+methods HTTP/1.1
       !    Accept-Language: en-us
       !    User-Agent:
       !    Host: 127.0.0.1:9055
       !    Connection: Keep-Alive

       L:Msg_Str        = 'GET /'
       IF CLIP(SELF.Path) ~= ''
          L:Msg_Str     = CLIP(L:Msg_Str) & CLIP(SELF.Path)
       .

       L:Msg_Str        = CLIP(L:Msg_Str) & '/' & CLIP(SELF.Page)

       L:Msg_Str        = CLIP(L:Msg_Str) & '?username=' & CLIP(L:User_Login)

       L:Msg_Str        = CLIP(L:Msg_Str) & '&password=' & CLIP(L:Password)

       L:Msg_Str        = CLIP(L:Msg_Str) & '&from=' & SELF.EncodeWebString( CLIP(SELF.From) )

       IF SUB(p:Cell_No,1,1) = '+'
          p:Cell_No     = SUB(p:Cell_No,2,LEN(CLIP(p:Cell_No)))
       .
       L:Msg_Str        = CLIP(L:Msg_Str) & '&to=' & CLIP(p:Cell_No)

       L:Msg_Str        = CLIP(L:Msg_Str) & '&text=' & SELF.EncodeWebString( CLIP(p:Msg) )


!   Not implementing Flash or Conact at this stage - ??
!
!       IF ~OMITTED(9)
!          IF p:Feature_ID ~= 0
!             SELF.Add_Field_Packet('FEA', p:Feature_ID, L:Msg_Str)
!       .  .
!
!       IF ~OMITTED(11)
!          L_Concat_M_Group = p_Concat_M_Group
!          IF L_CMG:Concat_Msg ~= 0
!             SELF.Add_Field_Packet('CON', L_CMG:Concat_Msg, L:Msg_Str)
!             SELF.Add_Field_Packet('MNO', L_CMG:Msg_No, L:Msg_Str)
!             SELF.Add_Field_Packet('MTO', L_CMG:Total_Msgs, L:Msg_Str)
!             SELF.Add_Field_Packet('CMI', L_CMG:Concat_ID, L:Msg_Str)           ! New field - Feb 19
!       .  .
!
!   Delayed will be controlled by the SMS Me server and not at the SMSC at this stage ??    (p_Deliver_Date_Time_Group)
!       IF ~OMITTED(10)
!          IF LOC:Q_Opt = 0
!             L_Deliver_Date_Time_Group    = p_Deliver_Date_Time_Group
!             IF L_DTG:Before_Date ~= 0         ! (p:Date_From, p:Time_From, p:Date_To, p:Time_To, p:Option)
!                L:Work_String     = Date_Time_Difference(TODAY(), CLOCK(), L_DTG:Before_Date, L_DTG:Before_Time)
!
!                SELF.Add_Field_Packet('OBD', Get_1st_Element_From_Delim_Str(L:Work_String,',',TRUE), L:Msg_Str)
!                SELF.Add_Field_Packet('OBT', Get_1st_Element_From_Delim_Str(L:Work_String,',',TRUE), L:Msg_Str)
!             .
!             IF L_DTG:After_Date ~= 0
!                L:Work_String     = Date_Time_Difference(TODAY(), CLOCK(), L_DTG:After_Date, L_DTG:After_Time)
!
!                SELF.Add_Field_Packet('SOD', Get_1st_Element_From_Delim_Str(L:Work_String,',',TRUE), L:Msg_Str)
!                SELF.Add_Field_Packet('SOT', Get_1st_Element_From_Delim_Str(L:Work_String,',',TRUE), L:Msg_Str)
!             .
!          ELSE                                                      ! Local server -
!             ! Then server has decided to send now so we do not want to send any delay times
!       .  .

       IF CLIP(SELF.Response_Req) ~= ''
          IF ~OMITTED(8)
             IF p:Delivery_Confirm ~= 0
                SELF.Response_Req   += 3
          .  .
          L:Msg_Str     = CLIP(L:Msg_Str) & '&dlrmask=' & CLIP(SELF.Response_Req)
       .


       IF ~OMITTED(6)
          SELF.Sent_ID  = p:Msg_ID
       ELSE
          SELF.Sent_ID += 1
       .

       IF CLIP(SELF.Post_Back_To) ~= ''
          L:Msg_Str     = CLIP(L:Msg_Str) & '&dlrurl=' & CLIP(SELF.Post_Back_To) & '%26SMSMeRef=' & SELF.Sent_ID
       .


       L:Msg_Str        = CLIP(L:Msg_Str) & ' HTTP/1.1'
       L:Msg_Str        = CLIP(L:Msg_Str) & EQU:CRLF & 'Accept-Language: en-us'

       IF CLIP(SELF.User_Agent) ~= ''
          L:Msg_Str     = CLIP(L:Msg_Str) & EQU:CRLF & 'User-Agent: ' & CLIP(SELF.User_Agent)
       ELSE
          L:Msg_Str     = CLIP(L:Msg_Str) & EQU:CRLF & 'User-Agent: SMSMeHTTP - www.infosolutions.co.za'
       .

       L:Msg_Str        = CLIP(L:Msg_Str) & EQU:CRLF & 'Host: ' & CLIP(SELF.Server_G.Address) & ':' & SELF.Server_G.Port

       IF CLIP(SELF.Connection) ~= ''
          L:Msg_Str     = CLIP(L:Msg_Str) & EQU:CRLF & CLIP(SELF.Connection)
       .

       L:Msg_Str        = CLIP(L:Msg_Str) & EQU:CRLF & EQU:CRLF


!       IF ~OMITTED(7)              ???  not relavent here in HTTP
!          LOC:Q_Opt = p:Q_Opt      ???  not relavent here in HTTP



       ! ----------------------------------   Send the Message   ----------------------------------
       L:Result             = SELF.Add_Send_Msg_to_Q(L:Msg_Str,,0, 1, SELF.Sent_ID)
       IF L:Result > 0
          ! Result would be 0 if the Q record was not added.  If Sent_ID was passed as 0
          ! then we would be setting this here to 0.  The meaning would be unclear.
          ! Therefore we are going to change the returned value on zero
          L:Result          = SELF.Sent_ID
       ELSIF L:Result = 0
          L:Result          = EQU:Err_General_Add_Queue      ! = -114
       .
    ELSE
       L:Result             = EQU:Err_Not_Initilised         ! = -101
       SELF.Last_Error_Code = L:Result
    .

    IF SELF.Client_Thread ~= 0
       POST(EVENT:Timer, , SELF.Client_Thread, 1)            ! Imediately do something then

 SELF.Dump_Data('__Speed_Dump.TXT', 'Add_Send_Msg    ..Posted Timer event')
    .

    RETURN(L:Result)


    ! Are we interested in what profile they used to send the message?
    !IF LO_TI:Send_Profile_ID ~= 0
    !   LOC:TCP_IP_Msg  = LOC:TCP_IP_Msg & L_TID:F_Delim &   'PRO'  & L_TID:D_Delim &  CLIP(LO_TI:Send_Profile_ID)
    !.

    ! LO_TI:Message_ID
    ! LO_TI:User_ID

    ! It is possible that the user would want the messages open on their network but private on ours.....???
    ! LO_TI:Sent_Private_User





! ---------   Take_Event / Check_Send_Complete / Send_Msg   ---------
SMSMeHTTP_Class.Take_Event               PROCEDURE(LONG p:No_To_Send=5)
    CODE
    ! Nothing happens until the object has been initilised.
    IF SELF.Init_ed = TRUE
       SELF.Event_ID        += 1

       IF SELF.Check_State() <= 0
          IF SELF.Check_Send_Complete() ~= TRUE            ! As we are closed - make sure we don't have an incomplete sending state
             SELF.Set_Send_Complete(TRUE)
          .
          SELF.Setup()                                     ! Does Connection open
       .

       LOOP p:No_To_Send TIMES
          ! If we have a msg. pending completion break
          IF SELF.Check_Send_Complete()  = FALSE           ! Last send not complete
             SELF.Receive_Msg()                            ! Receive any waiting messages - from the Q
             BREAK
          .

          IF SELF.Check_State() = 3                        ! Open & Setup - meaning is slightly different in HTTP - means user set
             IF SELF.Send_Msg() > 0                        ! Send 1 waiting messages - from the Q
                ! We sent one then
             ELSE
                IF SELF.Re_Send_Msg(1) > 0                 ! Send 1 expired msgs with no acks - from the Q - adds back to Out Q
             .  .
          ELSIF SELF.Check_State() = 0                     ! Currently closed - then check def. user state
             SELF.Check_User_Connect_Req_Opt()
          .

          ! If we have not received any messages - break - because?  Because we only send 1 before we wait in HTTP
          IF SELF.Receive_Msg() <= 0                       ! Receive any waiting messages - from the Q
             BREAK
    .  .  .
    RETURN



SMSMeHTTP_Class.Send_Msg                 PROCEDURE()
L:Count         ULONG
    CODE
    IF PARENT.Send_Msg(,1) > 0
 SELF.Dump_Data('__Speed_Dump.TXT', 'Send_Msg    ..Last_Sent_ID: ' & SELF.Last_Sent_ID & ',  Cur_Sent_ID: ' & SELF.Cur_Sent_ID & ',  Error_Sent_ID: ' & SELF.Error_Sent_ID & '  - Event: ' & SELF.Event_ID)

       IF SELF.Error_Sent_ID ~= 0 AND SELF.Error_Sent_ID = SELF.Cur_Sent_ID            ! Check for an error - if found for this Message ID
          SELF.Set_Send_Complete(TRUE)
       ELSE
          L:Count               = 1
          SELF.Cur_Sent_ID      = SELF.Last_Sent_ID
          
          SELF.Set_Send_Complete(FALSE)
    .  .
    RETURN( L:Count )


SMSMeHTTP_Class.Send_Failed              PROCEDURE(BYTE p:Type)
L_Cl_Fields_Q                   QUEUE,PRE(L)
Field                   STRING(3)
Element                 STRING(500)
                                .
    CODE
    ! Add audit
    ! Add MID failed record

    FREE(L_Cl_Fields_Q)
    L:Field         = 'MID'
    L:Element       = SELF.Cur_Sent_ID
    ADD(L_Cl_Fields_Q)

    L:Field         = 'SID'
    L:Element       = 0
    ADD(L_Cl_Fields_Q)

    L:Field         = 'MSS'
    L:Element       = 172                               ! HTTP send not acknowledged
    ADD(L_Cl_Fields_Q)
             
    L:Field         = 'MSD'
    L:Element       = TODAY()
    ADD(L_Cl_Fields_Q)
             
    L:Field         = 'MST'
    L:Element       = CLOCK()
    ADD(L_Cl_Fields_Q)

    EXECUTE p:Type
       SELF.Add_Audit('Send_Failed - HTTP', 'Added MID Q failed 172 rec. - Cur Sent ID: ' & SELF.Cur_Sent_ID, 'Send timed out - Send Date: ' & FORMAT(SELF.Last_Send_At_Date,@d5) &' @ ' &   FORMAT(SELF.Last_Send_At_Time,@t4), 1)
       SELF.Add_Audit('Send_Failed - HTTP', 'Added MID Q failed 172 rec. - Cur Sent ID: ' & SELF.Cur_Sent_ID, 'Connection closed before result rec.  - Send Date: ' & FORMAT(SELF.Last_Send_At_Date,@d5) &' @ ' &   FORMAT(SELF.Last_Send_At_Time,@t4), 1)
    .

    SELF.Process_MID( L_Cl_Fields_Q )

    SELF.Trigger_Events()                               ! Why?

    SELF.Set_Send_Complete(TRUE)
    RETURN


SMSMeHTTP_Class.Check_Send_Complete      PROCEDURE()
    CODE
    IF SELF.Send_Complete = FALSE
       IF (TODAY() - SELF.Last_Send_At_Date) * 8640000 + (CLOCK() - SELF.Last_Send_At_Time) > SELF.HTTP_Send_TimeOut
          ! Send already cancelled if connection closed!
          IF SELF.Check_State() ~= 0                ! Not Closed
             ! But check if connection open anyway - if still open then what? - Disconnect connection
             SELF.Disconnect()
          .

          SELF.Send_Failed(1)
    .  .
    RETURN( SELF.Send_Complete )


SMSMeHTTP_Class.Set_Send_Complete        PROCEDURE(BYTE p:State=0)
    CODE
    ! We are setting to sent in-progress - so check if complete has been called before we got here ??
    ! Shouldn't be nessisary - this is all on EVENT:Timer - next EVENT will happen after all this

    SELF.Send_Complete      = p:State

    IF SELF.Send_Complete = FALSE
       SELF.Last_Send_At_Date  = TODAY()
       SELF.Last_Send_At_Time  = CLOCK()
    .

 SELF.Dump_Data('__Speed_Dump.TXT', 'Set_Send_Complete    Send_Complete: ' & SELF.Send_Complete & ',  Last_Sent_ID: ' & SELF.Last_Sent_ID & ',  Cur_Sent_ID: ' & SELF.Cur_Sent_ID & ',  Error_Sent_ID: ' & SELF.Error_Sent_ID & '  - Event: ' & SELF.Event_ID)

    RETURN( SELF.Send_Complete )


SMSMeHTTP_Class.Connection_Closed       PROCEDURE()
    CODE
    IF SELF.Check_Send_Complete() = FALSE
       SELF.Send_Failed(2)
    .

    PARENT.Connection_Closed()
    RETURN



! ---------   Recd_Process_Packet   ---------
SMSMeHTTP_Class.Recd_Process_Packet      PROCEDURE(STRING p:Packet)
L:Result                        LONG

L_Cl_Fields_Q                   QUEUE,PRE(L)
Field                   STRING(3)
Element                 STRING(500)
                                .
    CODE
 SELF.Dump_Data('__Speed_Dump.TXT', 'Recd_Process_Packet    ..Sent_ID: ' & SELF.Cur_Sent_ID & '  - Event: ' & SELF.Event_ID)

    FREE(L_Cl_Fields_Q)
    L:Field         = 'MID'
    L:Element       = SELF.Cur_Sent_ID
    ADD(L_Cl_Fields_Q)

    L:Field         = 'SID'
    L:Element       = 0
    ADD(L_Cl_Fields_Q)

    ! "Sent."
    ! "Authorization failed for sendsms"

    ! Regardless - we can now send another SMS - so set that

    L:Field         = 'MSS'
    IF CLIP(UPPER(p:Packet)) = 'SENT.'
       L:Element    = 171                               ! HTTP submitted - in process
    ELSE
       L:Element    = 173                               ! Failed HTTP submission
    .
    ADD(L_Cl_Fields_Q)
             
    L:Field         = 'MSD'
    L:Element       = TODAY()
    ADD(L_Cl_Fields_Q)
             
    L:Field         = 'MST'
    L:Element       = CLOCK()
    ADD(L_Cl_Fields_Q)


    IF CLIP(UPPER(p:Packet)) ~= 'SENT.'
       L:Field      = 'ERR'
       L:Element    = EQU:Err_HTTP_Get                  ! -116
       ADD(L_Cl_Fields_Q)

       L:Field      = 'ERT'
       L:Element    = 'Send Error on HTTP GET command. Data: ' & CLIP(p:Packet)
       ADD(L_Cl_Fields_Q)

       SELF.Add_Audit('Recd_Process_Packet - HTTP - Error', 'Failed to find SENT.  Adding error no.: ' & EQU:Err_HTTP_Get, 'Packet: ' & CLIP(p:Packet) & ' / Out Q: ' & RECORDS(SELF.Out_Msgs_Q) & ' / Sent Q: ' & RECORDS(SELF.Sent_Msgs_Q) & ' / MID_Q: ' & RECORDS(SELF.Recd_MID_Q), 1)
    ELSE
       SELF.Add_Audit('Recd_Process_Packet - HTTP - Sent', 'SENT.  Added MID Q rec. - Cur Sent ID: ' & SELF.Cur_Sent_ID, 'Packet: ' & CLIP(p:Packet) & ' / Out Q: ' & RECORDS(SELF.Out_Msgs_Q) & ' / Sent Q: ' & RECORDS(SELF.Sent_Msgs_Q) & ' / MID_Q: ' & RECORDS(SELF.Recd_MID_Q), 2)
    .


    SELF.Process_MID( L_Cl_Fields_Q )

    SELF.Trigger_Events()

    SELF.Set_Send_Complete( TRUE )

    L:Result            = 1
    RETURN( L:Result )


!       SID:xxxxxxxxx                   - Server Sent Message ID
!       MSS:xxx                         - Message status
!       DNO:xxxxxxxxx                   - Server delivery notification info - dd/MM/yy,hh:MM:ss
!       MSD:xxx                         - Message sent date
!       MST:xxx                         - Message sent time
!       ERR:xxx                         - Last message error
!       ERT:xxx                         - Last message error - text



! ---------   Recd_Process_Recd_Str   ---------
SMSMeHTTP_Class.Recd_Process_Recd_Str    PROCEDURE(STRING p:New_Packet, LONG p:Packet_Len)
L:Result            LONG

L:Data              LIKE(SELF.Recd_Str)
L:Start_Pos         LONG

L:Hdr_Lines         LONG
L:Line              LIKE(SELF.Recd_Str)
L:Content_Length    LONG
L:Blank             BYTE                ! Blank line also found - signified end of header
L:Process           BYTE

L:Count             LONG

L:Start_Delim       CSTRING(9)

L:Content           STRING(1000)

!    HTTP/1.1 202 Foo
!    Server: WirelessWindow/3.0
!    Content-Length: 5
!    Content-type: text/html
!    Pragma: no-cache
!    Cache-Control: no-cache
!
!    Sent.


    CODE
    L:Start_Delim       = 'HTTP/1.1'

    ! ---------------   Add our new packet to our string   ---------------
    IF p:Packet_Len > 0
       IF SELF.Recd_S_Len > 0
          SELF.Recd_Str     = SELF.Recd_Str[1 : SELF.Recd_S_Len] & p:New_Packet[1 : p:Packet_Len]
       ELSE
          SELF.Recd_Str     = p:New_Packet[1 : p:Packet_Len]
       .
       SELF.Recd_S_Len     += p:Packet_Len
    .

    ! ---------------   Strip completed messages out of the string   ---------------
    LOOP
       L:Process            = FALSE
       L:Start_Pos          = INSTRING(L:Start_Delim, SELF.Recd_Str, 1, 1)
       IF L:Start_Pos > 0
          ! We must make sure that we have the last Start Pos before the 1st end pos.
          ! Otherwise we may have junk strings.
          DO Count_Hdr_Lines
          IF L:Blank = TRUE
             IF LEN(CLIP(L:Data)) >= L:Content_Length
                ! End of our page then - process it!
                L:Process   = TRUE
          .  .

          IF L:Process = TRUE
             IF BAND(SELF.Dump_Data_Opt, 00000100b) = 4
                SELF.Dump_Data('HTTP_Recd_Proc_Dump.TXT', L:Data)
             .

             ! -----------------   Reduce our received string   -----------------
             DO Reduce_Rec_Str

             ! -----------------   Process data   -----------------
             SELF.Recd_Process_Packet( L:Content )
             
             L:Result      += 1
          ELSE
             BREAK
          .
       ELSE
          ! Throw away everything up to the end of the string less L:Start_Delim length -1 (e.g. end may be "HTTP/1.")
          BREAK
    .  .

    RETURN( L:Result )                                      ! L:Result is no of packets processed



Reduce_Rec_Str      ROUTINE
    L:Count = 0
    LOOP (L:Hdr_Lines + 1) TIMES
       IF CLIP(SELF.Recd_Str) = ''
          BREAK
       .

       L:Line           = Get_1st_Element_From_Delim_Str(SELF.Recd_Str, '<13,10>', TRUE)

       SELF.Recd_S_Len -= LEN(CLIP(L:Line)) + 2
       L:Count         += 1
    .

    IF L:Count = L:Hdr_Lines + 1
       L:Content        = SELF.Recd_Str[1 : L:Content_Length]

       IF LEN(CLIP(SELF.Recd_Str)) > L:Content_Length
          SELF.Recd_Str     = SELF.Recd_Str[L:Content_Length + 1 : LEN(CLIP(SELF.Recd_Str))]
          SELF.Recd_S_Len  -= L:Content_Length
       ELSE
          SELF.Recd_Str     = ''
          SELF.Recd_S_Len   = 0
    .  .
    EXIT


Count_Hdr_Lines      ROUTINE
    L:Data              = SELF.Recd_Str
    L:Line              = ''
    L:Hdr_Lines         = 0
    L:Content_Length    = 0
    L:Blank             = FALSE

    LOOP
       IF CLIP(L:Data) = ''
          BREAK
       .

       ! Look at last extracted line
       IF INSTRING('CONTENT-LENGTH', UPPER(L:Line), 1, 1) > 0
          ! Previous line was complete and had Content length
          junk_"            = Get_1st_Element_From_Delim_Str(L:Line, ':', TRUE)
          L:Content_Length  = DEFORMAT(L:Line)
       .

       L:Line   = Get_1st_Element_From_Delim_Str(L:Data, '<13,10>', TRUE)
       IF CLIP(L:Line) = ''
          L:Blank   = TRUE
          BREAK
       .

       IF L:Blank = FALSE
          L:Hdr_Lines += 1
    .  .
    EXIT


! ---------   Set_Def_User   ---------
SMSMeHTTP_Class.Set_Def_User    PROCEDURE(STRING p:Login, STRING p:Password)
    CODE
    SELF.Cl_Login       = p:Login
    SELF.Cl_Password    = p:Password

    SELF.Set_State(3)
    RETURN


! ---------   Construct   ---------
SMSMeHTTP_Class.Construct    PROCEDURE()
    CODE
    SELF.Send_Complete  = TRUE              ! We are available to send a message from startup

    SELF.Path           = GETINI('Default_Setup','Path', 'cgi-bin', '.\SMSMeHTTP_Client.ini')
    SELF.Page           = GETINI('Default_Setup','Page', 'sendsms', '.\SMSMeHTTP_Client.ini')

    SELF.From           = GETINI('Default_Setup','From', 'SMSMe', '.\SMSMeHTTP_Client.ini')

    SELF.Response_Req   = GETINI('Default_Setup','Response_Path', 24, '.\SMSMeHTTP_Client.ini')       ! + 3 for delivery notice - added by proc.
    SELF.Post_Back_To   = GETINI('Default_Setup','Post_Back_To', 'http://www.infosolutions.co.za:9046/Page.asp?SMSCref=%25c%26DeliveryNotice=%25d', '.\SMSMeHTTP_Client.ini')

    SELF.User_Agent     = GETINI('Default_Setup','User_Agent', 'SMSMeHTTP - www.infosolutions.co.za', '.\SMSMeHTTP_Client.ini')
    SELF.Connection     = GETINI('Default_Setup','Connection', 'Connection: Keep-Alive', '.\SMSMeHTTP_Client.ini')

    SELF.HTTP_Send_TimeOut = GETINI('Default_Setup','HTTP_Send_TimeOut', 100 * 60 * 120, '.\SMSMeHTTP_Client.ini')  ! 2 hours!
    RETURN



SMSMeHTTP_Class.Resend_Sent_Msg     PROCEDURE(STRING p:Msg, LONG p:Position=0, BYTE p:Make_Packet=1, BYTE p:Type=0, |
                                              ULONG p:Ack_ID=0, LONG p:Date_Sent_Prev=0, LONG p:Time_Sent_Prev=0)
L:Result        ULONG
    CODE
    ! We don't want to re-send HTTP messages.  But in fact we should never get in here as we have an immediate
    ! result when we do the submittal.
    SELF.Add_Audit('Resend_Sent_Msg - HTTP - should not occur', 'Resend_Sent_Msg called. Ack_ID: ' & p:Ack_ID & |
                   ',  Sent @: ' & FORMAT(p:Date_Sent_Prev,@d5) & ', ' & FORMAT(p:Time_Sent_Prev,@t4)   |
                    , 'Msg: ' & CLIP(p:Msg), 3)

    RETURN( L:Result )



SMSMeHTTP_Class.Set_From_Str        PROCEDURE(<STRING p:From>)
    CODE
    IF OMITTED(2)                                                   ! Default from to INI value if omitted
       SELF.From    = GETINI('Default_Setup','From', 'SMSMe', '.\SMSMeHTTP_Client.ini')
    ELSE
       SELF.From    = p:From
    .
    RETURN( SELF.From )



SMSMeHTTP_Class.EncodeWebString     PROCEDURE  (STRING p_PostString)
local   group, pre (loc)
x         long
y         long
t1        byte
t2        byte
c1        string(1)
c2        string(1)
l         long
v         byte
        .

loc:table    string('0123456789ABCDEF')

loc:ans      string(16384)


  CODE
! >> I also found that the (*) asterisk, (_) underscore, (@) at symbol, (-) dash, and
!    (.) period do not need to be converted to ascii hex along with 0-9, A-Z, and a-z.

  loc:ans = p_PostString
  ! turn single chars into %ab
  loc:l = len (clip (loc:ans))
  loc:x = 0
  loop until loc:x >= loc:l
    loc:x += 1
    case loc:ans[loc:x]
    of 'a' to 'z'
    orof 'A' to 'Z'
    orof '0' to '9'
    orof '*'
    orof '_'
    orof '@'
    orof '-'
    orof '.'
      ! do nothing
    else
      loc:v = val (loc:ans[loc:x])
      loc:t1 = (loc:v / 16)
      loc:c1 = loc:table[loc:t1 + 1]
      loc:t2 = loc:v - (loc:t1 * 16)
      loc:c2 = loc:table[loc:t2 + 1]
      loc:ans = sub (loc:ans,1,loc:x-1) & '%' & loc:c1 & loc:c2 & sub (loc:ans,loc:x+1,(loc:l - loc:x))
      !stop (clip (loc:ans) & ' | ' & sub (loc:ans,loc:x+1,(loc:l - loc:x)))
      loc:x += 2
      loc:l += 2
    end
  end
  return (loc:ans)

!------------------------------------------------------------------------------

SMSMeHTTP_Class.DecodeWebString PROCEDURE  (string p_PostString)
local   group, pre (loc)
ans       string(8196)
x         long
y         long
        end

  CODE
!ExtractValueFromPost PROCEDURE  (string v, string s1) ! Declare Procedure
!  s = s1
!  ans = ''
!  x = instring(clip(lower(v))&'=',lower(s),1,1)
!  if x > 0
!    y = instring('&',s,1,x)                                 
!    if y = 0 then y = instring('<13>',s,1,x).
!    if y = 0 then y = instring('<10>',s,1,x).
!    if y = 0 then y = len(clip(s)) + 1.
!    x = x + len(clip(v)) + 1
!    ans = sub(s,x,y-x)
!    do InterpretAns
!  end
!!  if instring('date',lower(v),1,1)
!!    stop(clip(v) & '='&clip(ans))
!!  end
!  return(ans)
 
!InterpretAns  Routine

  ! turn + into space
  loc:ans = p_PostString
  loc:x = instring('+',loc:ans,1,1)
  loop until loc:x = 0
    loc:ans[loc:x] = ' '
    loc:x = instring('+',loc:ans,1,1)
  end
  ! turn %ab into a single char.
  loc:x = instring('%',loc:ans,1,1)
  loop until loc:x = 0
    case loc:ans[loc:x+1]
    of '0' to '9'
      loc:y = loc:ans[loc:x+1] * 16
    of 'A' to 'F'
      loc:y = val(loc:ans[loc:x+1])-55 * 16
    end
    case loc:ans[loc:x+2]
    of '0' to '9'
      loc:y += loc:ans[loc:x+2]
    of 'A' to 'F'
      loc:y += val(loc:ans[loc:x+2])-55
    end
    loc:ans = sub(loc:ans,1,loc:x-1) & chr(loc:y) & sub(loc:ans,loc:x+3,255)
    loc:x = instring('%',loc:ans,1,(loc:x+1))
  end
  return (loc:ans)
 
! >> I also found that the (*) asterisk, (_) underscore, @ symbol, (-) dash, and
!    (.) period do not need to be converted to ascii hex along with 0-9, A-Z, and a-z.
