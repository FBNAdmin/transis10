
  MEMBER

  omit('***',_c55_)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
  ***

   Include('ISSQLCls.inc'),ONCE
!   Include('ABFILE.INC'),ONCE
!   Include('ABERROR.INC'),ONCE

  Map
  End


! ============  SQL_Class Class  ============
SQLQueryClass.Get_El                PROCEDURE   (LONG p_No)       !,STRING
p_El        STRING(255)
    CODE
    EXECUTE p_No
       p_El     = SELF.Data_G.F1
       p_El     = SELF.Data_G.F2
       p_El     = SELF.Data_G.F3
       p_El     = SELF.Data_G.F4
       p_El     = SELF.Data_G.F5
       p_El     = SELF.Data_G.F6
       p_El     = SELF.Data_G.F7
       p_El     = SELF.Data_G.F8
       p_El     = SELF.Data_G.F9
       p_El     = SELF.Data_G.F10
       p_El     = SELF.Data_G.F11
       p_El     = SELF.Data_G.F12
       p_El     = SELF.Data_G.F13
       p_El     = SELF.Data_G.F14
       p_El     = SELF.Data_G.F15
       p_El     = SELF.Data_G.F16
       p_El     = SELF.Data_G.F17
       p_El     = SELF.Data_G.F18
    .

    RETURN(p_El)


SQLQueryClass.Next_Q                PROCEDURE   (<STRING p_Rec>, <ULONG p:Pos>)
L:Result        LONG
L:Pos           ULONG
    CODE
    IF OMITTED(3)
       L:Pos    = SELF.Cur_Q_Pos + 1
    ELSE
       L:Pos    = p:Pos
    .
    IF L:Pos    = 0
       L:Pos    = 1
    .

    GET(SELF.q, L:Pos)
    SELF.LastError      = ERRORCODE()
    IF ~SELF.LastError
       SELF.Cur_Q_Pos   = L:Pos
       L:Result         = SELF.Cur_Q_Pos

       SELF.Data_G      = SELF.q

       IF ~OMITTED(2)
          p_Rec         = SELF.q
    .  .

    RETURN( L:Result )



SQLQueryClass.Free_Q                PROCEDURE   ()
    CODE
    FREE(SELF.q)
    SELF.Cur_Q_Pos  = 0
    RETURN



SQLQueryClass.PropSQL               PROCEDURE   (STRING p_sql, LONG p_LC=0)
L:Time          LONG
L:Query_Time    LONG
L:Result        LONG
L:EndPos        LONG
L:Pos           LONG
    CODE
    ! Notes:
    !   L:Result
    !        x  = Number of records 0 to n
    !       -1  = Failed prop SQL
    !       -2  = Failed Q add

    FREE(SELF.q)

    L:Time      = CLOCK()
    L:Result    = SELF._Open()
    IF L:Result >= 0
       BUFFER(cl_Table, 500)
       SELF.Cur_Q_Pos  = 0

       IF SELF.log > 0
          L:EndPos  = LEN(CLIP(p_SQL))
          L:Pos     = 1
          LOOP
             IF (L:EndPos - L:Pos + 1) <= 220
                IF L:Pos > 1
                   SELF.AddLog('[SQL Query] (' & L:Pos & ')  ' & p_SQL[L:Pos : L:EndPos])
                ELSE
                   SELF.AddLog('[SQL Query] ' & p_SQL[L:Pos : L:EndPos])
                .
                BREAK
             ELSE
                SELF.AddLog('[SQL Query] (' & L:Pos & ')  ' & p_SQL[L:Pos : L:Pos + 219])
                L:Pos   += 220
       .  .  .

       cl_Table{PROP:SQL}   = p_Sql
       SELF.LastError       = ERRORCODE()
       IF ERRORCODE() = 0
          L:Query_Time      = CLOCK() - L:Time

          LOOP
             NEXT(cl_Table)
             SELF.LastError = ERRORCODE()
             IF ERRORCODE()
                IF ERRORCODE() ~= 33
                   L:Result         = -3
                   SELF.LastError   = ERRORCODE()
                   SELF.ErrorTrap('NextTable', 'Next Error is ' & SELF.LastError)
                .
                BREAK
             .

             SELF.q         = cl_Table.RECORD
             IF SELF.log > 2
                SELF.AddLog('[SQL Result] ' & CLIP(SELF.q.field1) & '| ' & CLIP(SELF.q.field2) & '| ' & CLIP(SELF.q.field3) & |
                            '| ' & CLIP(SELF.q.field4)  & '| ' & CLIP(SELF.q.field5)  & '| ' & CLIP(SELF.q.field6)  & |
                            '| ' & CLIP(SELF.q.field7)  & '| ' & CLIP(SELF.q.field8)  & '| ' & CLIP(SELF.q.field9)  & |
                            '| ' & CLIP(SELF.q.field10) & '| ' & CLIP(SELF.q.field11) & '| ' & CLIP(SELF.q.field12) & |
                            '| ' & CLIP(SELF.q.field13) & '| ' & CLIP(SELF.q.field14) & '| ' & CLIP(SELF.q.field15) & |
                            '| ' & CLIP(SELF.q.field16) & '| ' & CLIP(SELF.q.field17) & '| ' & CLIP(SELF.q.field18))
             .

             ADD(SELF.q)

             SELF.LastError = ERRORCODE()
             IF ERRORCODE()
                L:Result    = -2

                SELF.ErrorTrap('Assert','')
                BREAK
         .  .

         IF L:Result >=0
            L:Result        = RECORDS(SELF.q)
            IF SELF.log > 1
               SELF.AddLog('[SQL Query]  Run OK - Records: ' & L:Result)
         .  .

         IF p_LC <> 0 THEN SELF._FormatListBox(p_LC)
         ELSIF SELF.ListControl <> 0 THEN SELF._FormatListBox(SELF.listcontrol)
         .
       ELSE
          L:Result          = -1
          SELF.ErrorTrap('PropSQL', p_Sql)
       .

       IF SELF.log > 10
          SELF.AddLog('[SQL Query]  Connect: ' & cl_Table{PROP:ConnectString})
       .

       SELF.profile         = CLOCK() - L:Time
       IF SELF.log > 0
          IF (SELF.log = 1 AND (L:Query_Time / 100) > 0.25) OR SELF.log > 2
             SELF.AddLog('[SQL Time Taken]  Query Time: ' & (L:Query_Time / 100) & '  -  Incl. Q Add: ' & SELF.Profile / 100)
    .  .  .
    RETURN( L:Result )



SQLQueryClass._Open                 PROCEDURE  ()
L:Result    LONG
    CODE
    IF cl_TableOpen = 0
       cl_DBOwner       = SELF.DBOwner
       cl_TableName     = SELF.Table_Name

       OPEN(cl_Table, 42h)
       SELF.LastError       = ERRORCODE()
       IF ERRORCODE() = 2
          CREATE(cl_Table)
          SELF.LastError    = ERRORCODE()
          IF ERRORCODE()
             L:Result       = -2
             SELF.ErrorTrap('Create','Table: ' & cl_Table{PROP:Name} & '|Owner: ' & cl_Table{PROP:Owner})
          ELSE
             OPEN(cl_Table, 42h)
       .  .
       IF ERRORCODE()
          L:Result          = -1
          SELF.ErrorTrap('Open','Table: ' & cl_Table{PROP:Name} & '|Owner: ' & cl_Table{PROP:Owner})
       .
       cl_TableOpen         = 1
    .
    RETURN( L:Result )



SQLQueryClass.ErrorTrap             PROCEDURE  (STRING p_where, STRING p_Sql) ! Declare Procedure
m   STRING(400)
    CODE
    m = 'SQL - There''s a Problem at - ' & CLIP(p_Where) & '||Error (' & SELF.LastError & '): ' & CLIP(ERROR()) & '|File Err (' & FILEERRORCODE() & '): ' & CLIP(FILEERROR())
    SELF.AddLog('[SQL Error] ' & m)

    SELF.AddLog('[SQL Error] with ' & CLIP(p_SQL))
    IF SELF.SuppressMessages = 0
       MESSAGE(CLIP(m) & '|with ' & CLIP(p_SQL))
    .
    RETURN



SQLQueryClass._FormatListBox        PROCEDURE  (LONG p_LC)        ! Declare Procedure
x LONG
    CODE
    IF SELF.List_On = TRUE
       SELF._CountColumns()
       p_LC{PROP:FROM} = SELF.q

       LOOP x = 1 to SELF._HighColumn
          p_LC{proplist:exists,x}          = 1
          p_LC{proplist:width,x}           = 50
          p_LC{proplist:resize,x}          = 1
          p_LC{proplist:rightborder,x}     = 1
          p_LC{proplist:fieldno,x}         = x + 1
          p_LC{proplist:LeftOffset,x}      = 2

          p_LC{PROPLIST:Header,x}          = SELF.List_Headers[x]
       .

       LOOP x = SELF._HighColumn + 1 to cl_Columns
          p_LC{proplist:exists,x}          = 0
          p_LC{proplist:width,x}           = 0
    .  .
    RETURN



SQLQueryClass._CountColumns         PROCEDURE                      ! Declare Procedure
L:QStr      STRING(255 * cl_Columns)
L:ColStr    STRING(255)
L:Col       LONG
L:ChrPos    LONG
cs      LONG
ce      LONG

L:Idx   LONG


!MaxInColumn       byte,dim(cl_Columns)
    CODE
    !
    SELF._HighColumn = 1

    LOOP L:Idx = 1 TO RECORDS(SELF.q)
       IF SELF._HighColumn = cl_Columns THEN BREAK.
       GET(SELF.q, L:Idx)
       L:QStr               = SELF.q

       LOOP L:Col = 1 TO cl_Columns
          ce                        = L:Col * 255
          cs                        = ce - 254
          L:ColStr                  = L:QStr[cs :ce]
          SELF._HighInColumn[L:Col] = 0

          LOOP L:ChrPos = 255 TO SELF._HighInColumn[L:Col] + 1 BY -1
             IF VAL(L:ColStr[L:ChrPos]) = 0 OR VAL(L:ColStr[L:ChrPos]) = 32 THEN CYCLE.
             SELF._HighInColumn[L:Col] = L:ChrPos
             BREAK
    .  .  .

    LOOP L:Col = cl_Columns TO 1 BY -1
       IF SELF._HighInColumn[L:Col] = 0 THEN CYCLE.
       SELF._HighColumn = L:Col
       BREAK
    .

  !db.debugout('[SQL]  High column: ' & SELF._HighColumn)
    RETURN



SQLQueryClass.AddLog                PROCEDURE  (STRING p_q)          ! Declare Procedure
cs   CSTRING(2048)
    CODE
    IF SELF.log > 0
       cs = CLIP(p_q)
       !SQLWriteDebug(cs)
       !db.debugout(cs)
    .
    RETURN


SQLQueryClass.Init                  PROCEDURE   (STRING p_DBOwner, STRING p_TableName)
    CODE
    cl_DBOwner              = p_DBOwner
    cl_TableName            = p_TableName

    SELF.DBOwner            = cl_DBOwner
    SELF.Table_Name         = cl_TableName
    RETURN

SQLQueryClass.Construct             PROCEDURE                        ! Declare Procedure
    CODE
    SELF.List_On            = FALSE

    IF SELF.q &= NULL
       SELF.q &= NEW (SQLQueryQueueType)
    .

    IF COMMAND('/LogSQL')
       SELF.log = 10                ! Full
    ELSIF COMMAND('/LogSQL1')
       SELF.log = 1
    ELSIF COMMAND('/LogSQL2')
       SELF.log = 2
    .

    SELF.Cur_Q_Pos          = 0
    RETURN



SQLQueryClass.Destruct              PROCEDURE                        ! Declare Procedure
    CODE
    IF NOT SELF.q &= NULL
       FREE(SELF.q)
       DISPOSE(SELF.q)
    .
    IF cl_TableOpen = 1
       CLOSE(cl_Table)
       cl_TableOpen = 0
    .
    RETURN
