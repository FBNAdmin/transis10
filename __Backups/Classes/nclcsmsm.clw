  omit('***',_c55_)                         ! Temp comment, remove if compiles ok - moved from below Map
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
  ***

  MEMBER

   Include('ABFILE.INC'),ONCE
   Include('ABError.INC'),ONCE
   !Include('NetTalk.INC'),ONCE
   Include('NetSimp.inc'),ONCE

  Map
Get_1st_Element_From_Delim_Str          PROCEDURE(*STRING, STRING, BYTE =0),STRING
Date_Time_Difference                    PROCEDURE(LONG,LONG,LONG,LONG, BYTE=0),STRING
  End

!   Include('NClSMSMe_St.inc'),ONCE
!   Include('NClSMSMe_Cl.inc'),ONCE
   Include('NClSMSSt.inc'),ONCE
   Include('NClSMSme.inc'),ONCE


!    INCLUDE('PostIt.clw')

Date_Time_Difference                    PROCEDURE(LONG p:Date_From, LONG p:Time_From, LONG p:Date_To, LONG p:Time_To, BYTE p:Option)
LOC:Difference           STRING(50)
LOC:From_Date            LONG
LOC:From_Time            LONG
LOC:To_Date              LONG
LOC:To_Time              LONG
LOC:TDY_Group            GROUP,PRE(L_TDY)
Time                       LONG
Days                       LONG
Years                      LONG
                         END
    CODE
    ! (p:Date_From, p:Time_From, p:Date_To, p:Time_To, p:Option)
    ! Returns:  (years,) days, time (hours:minutes:seconds) t4

    DO From_To_Alloc

    L_TDY:Time  = LOC:To_Time - LOC:From_Time + 1

    L_TDY:Days  = LOC:To_Date - LOC:From_Date
     
    IF L_TDY:Time < 0                                               ! To time less than from time
       L_TDY:Days  -= 1                                             ! Reduce days by 1 for less than full day
       L_TDY:Time   = 8640000 + L_TDY:Time                              ! L_TDY:Time = 100 000 - 150 000 = - 50 000
    .


    IF p:Option = 1                                             ! We want years also
       L_TDY:Years      = L_TDY:Days / 365
       L_TDY:Days       = L_TDY:Days - (L_TDY:Years * 365)

       LOC:Difference   = L_TDY:Years & ','
    .

    LOC:Difference  = CLIP(LOC:Difference) & L_TDY:Days & ',' & FORMAT(L_TDY:Time,@t4)

    RETURN(LOC:Difference)

From_To_Alloc           ROUTINE
    IF p:Date_From < p:Date_To
       LOC:From_Date    = p:Date_From
       LOC:From_Time    = p:Time_From
       LOC:To_Date      = p:Date_To
       LOC:To_Time      = p:Time_To
    ELSE
       IF p:Date_From = p:Date_To                               ! Same day or no day - assumed same day
          IF p:Time_From < p:Time_To                            ! Order time entries
             LOC:From_Time    = p:Time_From
             LOC:To_Time      = p:Time_To
          ELSE
             LOC:From_Time    = p:Time_To
             LOC:To_Time      = p:Time_From
          .
       ELSE
          LOC:From_Date    = p:Date_To
          LOC:From_Time    = p:Time_To
          LOC:To_Date      = p:Date_From
          LOC:To_Time      = p:Time_From
    .  .

    EXIT





Get_1st_Element_From_Delim_Str          PROCEDURE(*STRING p:Delim_List, STRING p:Delim, BYTE p:Chop_Out)
LOC_Ret_Str         &STRING
LOC_Use_Str         &STRING
Str_Cls     CLASS
Str_1           &STRING
Init            PROCEDURE(STRING)
Destruct        PROCEDURE
            .

LOC_My_Str_Ele      Str_Cls

LOC:Delim_Pos            ULONG !Position of deliminator
LOC:Delim_Len            ULONG !Length of the deliminator
LOC:Delim                CSTRING(50)
LOC:Len                  SHORT

    CODE
    ! (p:Delim_List, p:Delim, p:Chop_Out - optional defaulted)
    !  String, Delim, Chop
    LOC:Len         = LEN(p:Delim_List)

    LOC_Ret_Str    &= NEW(String(LOC:Len))
    LOC_Use_Str    &= NEW(String(LOC:Len))

    LOC_Ret_Str     = ''
    LOC_Use_Str     = p:Delim_List
    !LOC:Return_Str  = ''
    !LOC:Delim_List  = LEFT(p:Delim_List)

    ! We are going to assume that if the p:Delim is empty the user wants to split on space!
    IF CLIP(p:Delim) = ''
       LOC:Delim        = ' '
       LOC:Delim_Len    = 1
    ELSE
       LOC:Delim        = LEFT(p:Delim)
       LOC:Delim_Len    = LEN(CLIP(p:Delim))
    .

    IF CLIP(LOC_Use_Str) ~= ''
       LOC:Delim_Pos = INSTRING(LOC:Delim[1 : LOC:Delim_Len], CLIP(LOC_Use_Str), 1)
       IF LOC:Delim_Pos > 0                                 ! We have a comma
          IF LOC:Delim_Pos > 1                              ! If we dont have ,something
             LOC_Ret_Str    = LOC_Use_Str[1 : (LOC:Delim_Pos - 1)]
          ELSE              ! > 0 but NOT > 1    =   1  - found at pos 1 so chop off - done below
          .
          IF p:Chop_Out = TRUE
             p:Delim_List   = LOC_Use_Str[(LOC:Delim_Pos + LOC:Delim_Len) : LEN(CLIP(LOC_Use_Str))]
          .
       ELSE                                                 ! No comma in the string
          LOC_Ret_Str       = CLIP(LOC_Use_Str)
          IF p:Chop_Out = TRUE
             p:Delim_List   = ''
    .  .  .

    DISPOSE(LOC_Use_Str)

    LOC_My_Str_Ele.Init(LOC_Ret_Str)

    DISPOSE(LOC_Ret_Str)

    RETURN(CLIP(LOC_My_Str_Ele.Str_1))


! ----------- Class Procs -----------------
Str_Cls.Init        PROCEDURE(STRING Our_Str)
    CODE
    Len_SCls_#      = LEN(CLIP(Our_Str))

    SELF.Str_1     &= NEW(STRING(Len_SCls_#))

    SELF.Str_1      = Our_Str

    !MESSAGE('Our_Str: ' & CLIP(Our_Str) & '||Str_Cls.Str_1: ' & CLIP(Str_Cls.Str_1))

    RETURN



Str_Cls.Destruct    PROCEDURE()
    CODE
    DISPOSE(SELF.Str_1)
    RETURN



! ---------   Add_Push_User (M) / Make_Packet / Add_Send_Msg_to_Q * 2 / Add_Field_Packet   ---------
SMSMeNet_Class.Resend_Sent_Msg       PROCEDURE(STRING p:Msg, LONG p:Position=0, BYTE p:Make_Packet=1, BYTE p:Type=0, ULONG p:Ack_ID=0, LONG p:Date_Sent_Prev, LONG p:Time_Sent_Prev)
    CODE
    RETURN(SELF.Add_Send_Msg_to_Q(p:Msg, p:Position, p:Make_Packet, p:Type, p:Ack_ID, p:Date_Sent_Prev, p:Time_Sent_Prev))



SMSMeNet_Class.Add_Send_Msg_to_Q        PROCEDURE(STRING p:Msg, LONG p:Position=0, BYTE p:Make_Packet=1, BYTE p:Type=0, ULONG p:Ack_ID=0, LONG p:Date_Sent_Prev, LONG p:Time_Sent_Prev)
    CODE
    CLEAR(SELF.Out_Msgs_Q)

    IF RECORDS(SELF.Out_Msgs_Q) > 0
       GET(SELF.Out_Msgs_Q, RECORDS(SELF.Out_Msgs_Q))
    .

    SELF.Out_Msgs_Q.Q_ID       += 1
    SELF.Out_Msgs_Q.Date_In_Q   = TODAY()
    SELF.Out_Msgs_Q.Time_In_Q   = CLOCK()
    SELF.Out_Msgs_Q.Type        = p:Type                    ! Send Msg type - from .typ file : ! 0 normal comm, 1 send message, 2 license request (RLI)
    SELF.Out_Msgs_Q.Ack_ID      = p:Ack_ID

    IF p:Make_Packet = TRUE
       SELF.Out_Msgs_Q.Message  = SELF.Make_Packet(p:Msg)
    ELSE
       SELF.Out_Msgs_Q.Message  = p:Msg
    .

    IF p:Position ~= 0
       ADD(SELF.Out_Msgs_Q, p:Position)
    ELSE
       ADD(SELF.Out_Msgs_Q)
    .

    IF ERRORCODE()
       SELF.Set_Last_Error(EQU:Err_General_Add_Queue, p:Ack_ID)
       CLEAR(SELF.Out_Msgs_Q)
    ELSE
       SELF.Check_User_Connect_Req_Opt()                    ! Check connection default actions (check connected and cur req.)
    .

    RETURN(SELF.Out_Msgs_Q.Q_ID)



SMSMeNet_Class.Set_Last_Error           PROCEDURE(LONG p:Err_Code, ULONG p:Sent_ID=0)
    CODE
    SELF.Last_Error_Code    = p:Err_Code                ! Last error code
    SELF.Error_Sent_ID      = p:Sent_ID                 ! Error was for this msg. ID
    RETURN



SMSMeNet_Class.Make_Packet             PROCEDURE(STRING p:Packet)
L:Packet            STRING(1000)
    CODE
    L:Packet        = SELF.Start_Delim   & CLIP(p:Packet)   & SELF.End_Delim
    RETURN(L:Packet)



SMSMeNet_Class.Add_Field_Packet        PROCEDURE(STRING p:Send_Field, <STRING p:Send_Data>, <*STRING p:Packet>, BYTE p:Pos)
L:Packet            STRING(1000)
L:Empty_Pack        BYTE
    CODE
    IF OMITTED(4) OR CLIP(p:Packet) = ''
       L:Empty_Pack = TRUE
    .

    IF OMITTED(3)                               ! If Data omitted
       L:Packet  = CLIP(p:Send_Field)
    ELSE
       L:Packet  = CLIP(p:Send_Field)   & SELF.Data_Delim   & CLIP(p:Send_Data)
    .

    ! We don't want p:Packet limited by L:Packet string size
    IF ~OMITTED(4)
       IF L:Empty_Pack = TRUE
          p:Packet     = CLIP(L:Packet)
       ELSE
          IF p:Pos = 0                                                             ! Add new bit to the end
             p:Packet  = CLIP(p:Packet) &  SELF.Field_Delim  & CLIP(L:Packet)
          ELSE                                                                     ! Add new bit to the front
             p:Packet  = CLIP(L:Packet) &  SELF.Field_Delim  & CLIP(p:Packet)
    .  .  .

    IF L:Empty_Pack = FALSE
       IF p:Pos = 0                                                             ! Add new bit to the end
          L:Packet  = CLIP(p:Packet) &  SELF.Field_Delim  & CLIP(L:Packet)
       ELSE                                                                     ! Add new bit to the front
          L:Packet  = CLIP(L:Packet) &  SELF.Field_Delim  & CLIP(p:Packet)
    .  .

    RETURN(L:Packet)



SMSMeNet_Class.Send_Push_Users          PROCEDURE()
L:Idx               LONG
L:Msg               STRING(1000)
L:Count_Users       LONG
L:Count_Users_Tot   LONG
    CODE
    L:Idx   = 0
    LOOP
       L:Idx    += 1
       GET(SELF.Push_Users_Q, L:Idx)
       IF ERRORCODE()
          BREAK
       .

       IF LEN(SELF.Start_Delim) + LEN(SELF.Field_Delim) + LEN(SELF.Data_Delim) + LEN(SELF.End_Delim) + |
                    LEN(CLIP(SELF.Push_Users_Q.User_Login)) + LEN(CLIP(SELF.Push_Users_Q.Password)) + LEN(CLIP(L:Msg)) > 800
          SELF.Add_Field_Packet('RPU', L:Count_Users, L:Msg, 1)
          SELF.Add_Send_Msg_to_Q(L:Msg)

          CLEAR(L:Msg)
          L:Count_Users = 0
       .

       SELF.Add_Field_Packet('RMU', SELF.Push_Users_Q.User_Login, L:Msg)
       SELF.Add_Field_Packet('RMP', SELF.Push_Users_Q.Password, L:Msg)

       L:Count_Users        += 1
       L:Count_Users_Tot    += 1
    .

    IF LEN(CLIP(L:Msg)) > 0
       SELF.Add_Field_Packet('RPU', L:Count_Users, L:Msg, 1)
       SELF.Add_Send_Msg_to_Q(L:Msg)
    .
    RETURN(L:Count_Users_Tot)




SMSMeNet_Class.Add_Push_User           PROCEDURE(<STRING p:User_Login>, <STRING p:User_Password>, BYTE p:Free = 0, BYTE p:Send_Option = 0)
L:Result            LONG
    CODE
    IF p:Free = TRUE
       FREE(SELF.Push_Users_Q)
    .

    CLEAR(SELF.Push_Users_Q)

!    SELF.Push_Users_Q.User_ID
!    SELF.Push_Users_Q.User_Login
!    SELF.Push_Users_Q.Access_Level
!    SELF.Push_Users_Q.Password
!    SELF.Push_Users_Q.Default_Transport
!    SELF.Push_Users_Q.Default_Queuing
!    SELF.Push_Users_Q.TCP_IP


    ! NOT Adding the default user anymore!
    ! If no user provided then add the default user as a push user
    IF OMITTED(2)   ! OR CLIP(p:User_Login) = ''                                ! No user
       p:User_Login         = ''
       p:User_Password      = ''

!       p:User_Login         = SELF.Cl_Login
!       IF OMITTED(3) OR CLIP(SELF.Cl_Password) = ''                         ! No password
!          p:User_Password   = SELF.Cl_Password
!       .
!    ELSIF OMITTED(3)
!       p:User_Password      = ''
    .

    IF CLIP(p:User_Login) ~= ''
       ! Check if we have this user yet or not
       SELF.Push_Users_Q.User_Login        = p:User_Login
       GET(SELF.Push_Users_Q, +SELF.Push_Users_Q.User_Login)
       IF ~ERRORCODE()
          L:Result                         = -1
       ELSE
          SELF.Push_Users_Q.User_Login     = p:User_Login
          SELF.Push_Users_Q.Password       = p:User_Password
          ADD(SELF.Push_Users_Q)

          IF ERRORCODE()
             SELF.Set_Last_Error(EQU:Err_General_Add_Queue)
             L:Result                      = EQU:Err_General_Add_Queue
    .  .  .

    IF L:Result = 0
       IF p:Send_Option > 0
          SELF.Send_Push_Users()
    .  .

    RETURN( L:Result )


! ---------   Helper Functions - Add_Audit / Dump_Audit / Read_Audit / Save_INIs / Dump_Data / Open_Dump_File   ---------
SMSMeNet_Class.Open_Dump_File  PROCEDURE(*FILE p_File, STRING p_File_Name, BYTE p:Create_File)
L:Result    LONG
    CODE
    p_File{PROP:Name}   = p_File_Name

    SHARE(p_File)
    IF ERRORCODE() < 4 AND ERRORCODE() > 0
       IF p:Create_File = 1
          CREATE(p_File)
          SHARE(p_File)
          IF ERRORCODE()
             SELF.Add_Audit('Open_Dump_File','Created failed - File: ' & CLIP(p_File_Name),'Error (' & ERRORCODE() & '): ' & ERROR(),1,'File Errors')
             L:Result  = -1
          ELSE
             L:Result  = 2
       .  .
    ELSIF ERRORCODE()
       SELF.Add_Audit('Open_Dump_File','File: ' & CLIP(p_File_Name),'Error (' & ERRORCODE() & '): ' & ERROR(),1,'File Errors')
       L:Result  = -2
    ELSE
       L:Result = TRUE
    .
    RETURN(L:Result)



SMSMeNet_Class.Dump_Data        PROCEDURE(STRING p:FileName, STRING p:Data, LONG p:Length, BYTE p:Date_Time_Stamp=1)
L:Result        LONG
Audit_File      FILE,DRIVER('ASCII'),CREATE,PRE(SCL_AUD)
Rec               RECORD
Line                STRING(10000)
                . .
L:File_Open     BYTE

    CODE
    IF SELF.Open_Dump_File(Audit_File, p:FileName) > 0
       ! 01/01/2004 1 11:11:11 - = 10 + 1 + 8 + 3 = 22

       IF p:Length > 0
          IF p:Date_Time_Stamp = TRUE
             SCL_AUD:Line  = FORMAT(TODAY(),@D06) & ' ' & FORMAT(CLOCK(),@t4) & '.' & CLOCK() % 100 & ' - ' &  CLIP(p:Data[1 : p:Length])
          ELSE
             SCL_AUD:Line  = CLIP(p:Data[1 : p:Length])
          .
       ELSE
          IF p:Date_Time_Stamp = TRUE
             SCL_AUD:Line  = FORMAT(TODAY(),@D06) & ' ' & FORMAT(CLOCK(),@t4) & '.' & CLOCK() % 100 & ' - ' &  CLIP(p:Data)
          ELSE
             SCL_AUD:Line  = CLIP(p:Data)
       .  .
       ADD(Audit_File)
       IF ~ERRORCODE()
          L:Result     += 1
       .

       CLOSE(Audit_File)
    ELSE
       L:Result         = -1
    .
    RETURN(L:Result)



SMSMeNet_Class.Save_INIs        PROCEDURE(STRING p:Type)
L:Result            LONG
    CODE
    CASE p:Type
    OF 'CID'
       PUTINI(CLIP(SELF.Ini_Section), 'ClientID_Prefix', SELF.CID_Group.Prefix, CLIP(SELF.Ini_File))
       PUTINI(CLIP(SELF.Ini_Section), 'ClientID_CID', SELF.CID_Group.CID, CLIP(SELF.Ini_File))
    OF 'Send_Recd_Per'
       PUTINI(CLIP(SELF.Ini_Section), 'Send_Per_Event', SELF.Send_Per_Event, CLIP(SELF.Ini_File))
       PUTINI(CLIP(SELF.Ini_Section), 'Receive_Per_Event', SELF.Receive_Per_Event, CLIP(SELF.Ini_File))
    OF 'Delims'
       PUTINI(CLIP(SELF.Ini_Section), 'Start_Delim', SELF.Start_Delim, CLIP(SELF.Ini_File))
       PUTINI(CLIP(SELF.Ini_Section), 'Field_Delim', SELF.Field_Delim, CLIP(SELF.Ini_File))
       PUTINI(CLIP(SELF.Ini_Section), 'Data_Delim', SELF.Data_Delim, CLIP(SELF.Ini_File))  
       PUTINI(CLIP(SELF.Ini_Section), 'End_Delim', SELF.End_Delim, CLIP(SELF.Ini_File))
    OF 'Current_Server'
       ! This should loop through the servers?
       PUTINI(CLIP(SELF.Ini_Section), 'SMN_Server_ID_1', SELF.Net_Servers_Q.TCP_IP_ID, CLIP(SELF.Ini_File))
       PUTINI(CLIP(SELF.Ini_Section), 'SMN_Server_Address_' & 1,SELF.Net_Servers_Q.Server_Address,CLIP(SELF.Ini_File))
       PUTINI(CLIP(SELF.Ini_Section), 'SMN_Server_Port_' & 1,SELF.Net_Servers_Q.Server_Port,CLIP(SELF.Ini_File))
    ELSE
       L:Result = -1
    .
    RETURN(L:Result)



SMSMeNet_Class.Replace_Chars    PROCEDURE(*STRING p:String, STRING p:To_Replace, *CSTRING p:Replacement)
L:End           LONG
L:To_Rep_Len    LONG
L:Found         LONG
    CODE
    L:To_Rep_Len        = LEN(CLIP(p:To_Replace))
    LOOP
       L:Found  = INSTRING(CLIP(p:To_Replace), CLIP(p:String), 1, 1)
       IF L:Found <= 0
          BREAK
       .

       L:End            = LEN(CLIP(p:String))

       IF L:Found = 1
          IF L:End > L:To_Rep_Len                       ! aaaX
             p:String   = p:Replacement & p:String[L:To_Rep_Len + 1 : L:End]
          ELSE                                          ! aaa
             p:String   = p:Replacement
          .
       ELSE
          IF L:End > (L:Found + L:To_Rep_Len - 1)         ! YYYaaaX   - 4 + 3 - 1 = 6
             p:String   = p:String[1 : L:Found - 1] & p:Replacement & p:String[L:Found + L:To_Rep_Len : L:End]
          ELSE
             p:String   = p:String[1 : L:Found - 1] & p:Replacement
    .  .  .
    RETURN



SMSMeNet_Class.Dump_Audit       PROCEDURE(<STRING p:Loc_Name>, BYTE p:Delete)
L:Line          STRING(2000)

Audit_File      FILE,DRIVER('ASCII'),CREATE,PRE(SCL_AUD)
Rec               RECORD
Line                STRING(2000)
                . .

L:File_Name     STRING(255)
L:IDx           LONG
L:Result        LONG

    CODE
    IF RECORDS(SELF.Audit_Q) > 0
       L:File_Name      = p:Loc_Name
       IF OMITTED(2)
          L:File_Name   = 'SMSMeNet_Audit.csv'
       .
       L:Result     = SELF.Open_Dump_File(Audit_File, L:File_Name)
       IF L:Result > 0
          IF L:Result = 2                               ! File was created
             SCL_AUD:Line  = 'HDR  --------  Level, Type, Data 1, Data 2, Data 3'
             ADD(Audit_File)
          .
          L:Result  = 0

          SCL_AUD:Line  = 'HDR  --------  Dump Audit @ ' & FORMAT(TODAY(),@d6) & ' - ' & FORMAT(CLOCK(), @t4) & '  --------'
          ADD(Audit_File)

          LOOP
             L:IDx   += 1
             L:Line   = SELF.Read_Audit(L:IDx)
             IF L:Line = '<No Record>'
                BREAK
             .

             SCL_AUD:Line  = L:Line

             ADD(Audit_File)

             IF ~ERRORCODE()
                L:Result     += 1
          .  .
          CLOSE(Audit_File)
       .

       IF p:Delete = TRUE
          FREE(SELF.Audit_Q)
    .  .

    RETURN(L:Result)



SMSMeNet_Class.Read_Audit       PROCEDURE(<ULONG p:Pos>, BYTE p:Delete)
L:Return        STRING(2000)
L:Replacement   CSTRING(2)
    CODE
    IF OMITTED(2) OR p:Pos = 0
       p:Pos    = 1
    .

    L:Replacement = '||'

    GET(SELF.Audit_Q, p:Pos)
    IF ~ERRORCODE()
       L:Return             = SELF.Audit_Q.Data1
       SELF.Replace_Chars(L:Return, '<13,10>', L:Replacement)
       SELF.Audit_Q.Data1   = L:Return

       L:Return             = SELF.Audit_Q.Data2
       SELF.Replace_Chars(L:Return, '<13,10>', L:Replacement)
       SELF.Audit_Q.Data2   = L:Return

       L:Return             = SELF.Audit_Q.Data3
       SELF.Replace_Chars(L:Return, '<13,10>', L:Replacement)
       SELF.Audit_Q.Data3   = L:Return

       L:Return     = SELF.Audit_Q.Level & ', ' & CLIP(SELF.Audit_Q.Type) & ', ' & CLIP(SELF.Audit_Q.Data1) & ', ' & |
                        CLIP(SELF.Audit_Q.Data2) & ', ' & CLIP(SELF.Audit_Q.Data3)
       IF SELF.Audit_Q.Date ~= 0
          L:Return  = CLIP(L:Return) & ', ' & FORMAT(SELF.Audit_Q.Date,@d5) & ', ' & FORMAT(SELF.Audit_Q.Time,@t4)
       .
       IF p:Delete = TRUE
          DELETE(SELF.Audit_Q)
       .
    ELSE
       L:Return    = '<No Record>'
    .
    RETURN(L:Return)



SMSMeNet_Class.Add_Audit        PROCEDURE(<STRING p:Data1>, <STRING p:Data2>, <STRING p:Data3>, <BYTE p:Level>, <STRING p:Type>, BYTE p:Date_Time_Stamp=1)
    CODE
    IF OMITTED(2)
       p:Data1  = ''
    .
    IF OMITTED(3)
       p:Data1  = ''
    .
    IF OMITTED(4)
       p:Data1  = ''
    .
    IF OMITTED(5)
       p:Level  = 0
    .
    IF OMITTED(6)
       p:Type   = 'General'
    .

    CLEAR(SELF.Audit_Q)

    SELF.Audit_Q.Type       = CLIP(p:Type)
    SELF.Audit_Q.Level      = p:Level
    SELF.Audit_Q.Data1      = CLIP(p:Data1)
    SELF.Audit_Q.Data2      = CLIP(p:Data2)
    SELF.Audit_Q.Data3      = CLIP(p:Data3)

    IF p:Date_Time_Stamp = TRUE
       SELF.Audit_Q.Date    = TODAY()
       SELF.Audit_Q.Time    = CLOCK()
    .

    ADD(SELF.Audit_Q)

    IF RECORDS(SELF.Audit_Q) > 49               ! Dump every 50 records
       SELF.Dump_Audit()
    .
    RETURN

! ---------   Set_Def_User   ---------
SMSMeNet_Class.Set_Def_User             PROCEDURE(STRING p:Login, STRING p:Password)
    CODE
    SELF.Cl_Login       = p:Login
    SELF.Cl_Password    = p:Password
    RETURN


! ---------   Inits (M) and Construct / Destruct / Set_Cl_NetSimple   ---------
SMSMeNet_Class.Set_Cl_NetSimple             PROCEDURE(<NetSimple p_NetS>)
LOC:Result          LONG
    CODE
    IF SELF.Cl_NetSimple &= NULL
       SELF.Cl_NetSimple        &= p_NetS
       SELF.Check_User_Connect_Req_Opt()                ! Check default connect action
    ELSE
       IF OMITTED(2) = TRUE
          SELF.Cl_NetSimple              &= NULL

          ! why did we have the event being reset here??
          !SELF.Ack_Event_Trigger    = 0           ! Reset the trigger event
       ELSE
          LOC:Result   = -1
    .  .
    RETURN(LOC:Result)


SMSMeNet_Class.Construct               PROCEDURE
    CODE
    SELF.Push_Users_Q       &= NEW(GL_Cl_Push_Users_Q_Type)
    SELF.Net_Servers_Q      &= NEW(GL_Cl_Servers_Q_Type)
    SELF.Out_Msgs_Q         &= NEW(GL_Cl_Messages_Q_Type)
    SELF.Sent_Msgs_Q        &= NEW(GL_Cl_Messages_Q_Type)
    SELF.In_Packets_Q       &= NEW(GL_Cl_Packets_Q_Type)

    SELF.Audit_Q            &= NEW(GL_Cl_Audit_Q)

    SELF.Recd_MID_Q         &= NEW(GL_Cl_MID_Q)
    SELF.Recd_RCM_Q         &= NEW(GL_Cl_RCM_Q)
    SELF.Send_Msgs_Exp_Q    &= NEW(GL_Cl_Msg_Exp_Q_Type)

    SELF.Licenses_Q         &= NEW(GL_Cl_Licenses_Q)
    SELF.Trigger_Events_Q   &= NEW(GL_Cl_Trigger_Events_Q)

    SELF.Read_In_Qs()

    SELF.ReadIn_Licenses_Q()

    SELF.Ini_File                       = '.\SMSMeNet_Cl.INI'
    SELF.Ini_Section                    = 'SMSMeNet_Servers'
    SELF.Init_ed                        = FALSE

    SELF.Rec_Msg_Mode                   = 1
    SELF.Send_Per_Event                 = 5
    SELF.Receive_Per_Event              = 5

    SELF.Persist_Q_Days                 = 7
    SELF.Persist_Q_Trans                = 100

    SELF.Re_Send_Time_Out               = 100 * 60 * 5          ! 5 mins
    SELF.No_Re_Send_Hours               = 24                    ! No resend after this many hours
    SELF.Wait_Ack_Days                  = 30                    ! How many days to wait for an ack

    SELF.Net_Servers_Q.TCP_IP_ID        = 1
    SELF.Net_Servers_Q.Server_Address   = 'infosolutions.co.za'
    SELF.Net_Servers_Q.Server_Port      = 9042

    SELF.Cl_Login                       = ''
    SELF.Cl_Password                    = ''

    SELF.RSR_State_Check                = TRUE
    SELF.Send_RMM_CID                   = TRUE

    SELF.Last_Sent_ID                   = 0
    SELF.Sent_ID                        = 0
    RETURN


SMSMeNet_Class.Read_In_Qs               PROCEDURE()
    CODE
    SELF.ReadIn_Received_Msgs(,1)
    SELF.ReadIn_Recd_MIDs(,1)
    SELF.ReadIn_Out_Sent_Msgs(,1)
    SELF.ReadIn_Out_Sent_Msgs('SMSMeNet_Sent_Uprocessed.csv',1,SELF.Sent_Msgs_Q)
    RETURN


SMSMeNet_Class.Destruct                PROCEDURE
    CODE
    SELF.Dump_Queues()

    PUTINI(CLIP(SELF.Ini_Section), 'Sent_ID', SELF.Sent_ID, CLIP(SELF.Ini_File))

    DISPOSE(SELF.Push_Users_Q)
    DISPOSE(SELF.Net_Servers_Q)
    DISPOSE(SELF.Out_Msgs_Q)
    DISPOSE(SELF.Sent_Msgs_Q)
    DISPOSE(SELF.In_Packets_Q)

    DISPOSE(SELF.Audit_Q)

    DISPOSE(SELF.Recd_MID_Q)
    DISPOSE(SELF.Recd_RCM_Q)
    DISPOSE(SELF.Send_Msgs_Exp_Q)

    DISPOSE(SELF.Licenses_Q)
    DISPOSE(SELF.Trigger_Events_Q)
    RETURN


SMSMeNet_Class.Dump_Queues             PROCEDURE()
    CODE
    SELF.Dump_Audit()
    SELF.Dump_In_Packets()

    SELF.Dump_Received_Msgs(,,1)            ! Dump un-expired
    SELF.Dump_Received_Msgs(,,2)            ! Dump all left with expired file name
    SELF.Dump_Recd_MIDs(,,1)
    SELF.Dump_Recd_MIDs(,,2)

    SELF.Dump_Out_Sent_Msgs(,,1)
    SELF.Dump_Out_Sent_Msgs(,,2)

    SELF.Dump_Licenses_Q()

    ! 1000 Q entries and Wait_Ack_Days days before expiry
    SELF.Dump_Out_Sent_Msgs('SMSMeNet_Sent_Uprocessed.csv',,1,SELF.Sent_Msgs_Q, 1000, SELF.Wait_Ack_Days)
    SELF.Dump_Out_Sent_Msgs('SMSMeNet_Sent_Uprocessed-Exp ' & FORMAT(TODAY(),@d8) & ' - ' & FORMAT(CLOCK(),@t4-) & '.csv',,2,SELF.Sent_Msgs_Q)

    ! 1000 Q entries and 100 days before expiry
    SELF.Dump_Out_Msgs_Expired(,,1,,1000,100)
    SELF.Dump_Out_Msgs_Expired(,,2)
    RETURN


SMSMeNet_Class.Load_Servers_Ini        PROCEDURE(BYTE p:Free_Servers_Q = 1)
L:Idx           LONG
    CODE
    ! Load the servers into the Q from ini
    IF p:Free_Servers_Q = TRUE
       FREE(SELF.Net_Servers_Q)
    .

    L:Idx   = 0
    LOOP
       L:Idx    += 1

       SELF.Net_Servers_Q.TCP_IP_ID             = GETINI(CLIP(SELF.Ini_Section), 'SMN_Server_ID_' & L:Idx, 0, CLIP(SELF.Ini_File))
       IF SELF.Net_Servers_Q.TCP_IP_ID = 0
          BREAK
       .

       SELF.Net_Servers_Q.Server_Address        = GETINI(CLIP(SELF.Ini_Section), 'SMN_Server_Address_' & L:Idx,'',CLIP(SELF.Ini_File))
       SELF.Net_Servers_Q.Server_Port           = GETINI(CLIP(SELF.Ini_Section), 'SMN_Server_Port_' & L:Idx,'',CLIP(SELF.Ini_File))
       SELF.Net_Servers_Q.Default_User          = GETINI(CLIP(SELF.Ini_Section), 'SMN_Server_DefUser_' & L:Idx,'',CLIP(SELF.Ini_File))
       SELF.Net_Servers_Q.Send_User_Option      = GETINI(CLIP(SELF.Ini_Section), 'SMN_Server_SendUserOpt_' & L:Idx,'',CLIP(SELF.Ini_File))

       SELF.Net_Servers_Q.Order                 = GETINI(CLIP(SELF.Ini_Section), 'SMN_Server_Order_' & L:Idx,'',CLIP(SELF.Ini_File))
       SELF.Net_Servers_Q.Use_Fixed_Order_Only  = GETINI(CLIP(SELF.Ini_Section), 'SMN_Server_UseFOrderOnly_' & L:Idx,'',CLIP(SELF.Ini_File))
       SELF.Net_Servers_Q.Connections           = GETINI(CLIP(SELF.Ini_Section), 'SMN_Server_Connections_' & L:Idx,'',CLIP(SELF.Ini_File))

       ADD(SELF.Net_Servers_Q)
    .
    IF RECORDS(SELF.Net_Servers_Q) = 0
       SELF.Net_Servers_Q.Server_Address        = 'infosolutions.co.za'
       SELF.Net_Servers_Q.Server_Port           = '9042'
       SELF.Net_Servers_Q.TCP_IP_ID             = 1
       ADD(SELF.Net_Servers_Q)
    .
    RETURN( RECORDS(SELF.Net_Servers_Q) )


SMSMeNet_Class.Init                    PROCEDURE(<STRING p:Ini_File>, <STRING p:Ini_Section>, BYTE p:Connect_Req=0, |
                                                 BYTE p:Free_Servers=0, BYTE p:Load_Servers=0)
L:Ini_File      STRING(255)
L:Ini_Section   STRING(255)

    CODE
    L:Ini_File              = p:Ini_File
    IF OMITTED(2)
       L:Ini_File           = '.\SMSMeNet_Cl.INI'
    .
    L:Ini_Section           = p:Ini_Section
    IF OMITTED(3)
       L:Ini_Section        = 'SMSMeNet_Servers'
    .

    SELF.Ini_File           = L:Ini_File
    SELF.Ini_Section        = L:Ini_Section

    IF p:Load_Servers = TRUE
       SELF.Load_Servers_Ini()
    .

    SELF.Init_Final(SELF.Ini_File, SELF.Ini_Section, p:Connect_Req)
    RETURN


SMSMeNet_Class.Init                    PROCEDURE(*FileManager p_SMSMeNet_Servers, <STRING p:Ini_File>, <STRING p:Ini_Section>,|
                                                 BYTE p:Connect_Req)
L:Ini_File      STRING(255)
L:Ini_Section   STRING(255)

    CODE
    L:Ini_File      = p:Ini_File
    IF OMITTED(2)
       L:Ini_File           = '.\SMSMeNet_Cl.INI'
    .
    L:Ini_Section   = p:Ini_Section
    IF OMITTED(3)
       L:Ini_Section        = 'SMSMeNet_Servers'
    .

    SELF.Ini_File           = L:Ini_File
    SELF.Ini_Section        = L:Ini_Section

    ! Load the servers into the Q
    FREE(SELF.Net_Servers_Q)

    p_SMSMeNet_Servers.Open()
    p_SMSMeNet_Servers.UseFile()

!    CLEAR(TCP:Record)
    SET(p_SMSMeNet_Servers.File)
    LOOP
       IF p_SMSMeNet_Servers.TryNext() ~= LEVEL:Benign
          BREAK
       .

       ! Could use What to load these fields?  Make it file independant?

!       SELF.Net_Servers_Q.TCP_IP_ID             = TCP:TCP_IP_ID          ! p_SMSMeNet_Servers:TCP_IP_ID
!       SELF.Net_Servers_Q.Server_Address        = TCP:Server_Address     ! p_SMSMeNet_Servers.Server_Address
!       SELF.Net_Servers_Q.Server_Port           = TCP:Server_Port        ! p_SMSMeNet_Servers.Server_Port
!       SELF.Net_Servers_Q.Default_User          = TCP:Default_User       ! p_SMSMeNet_Servers.Default_User
!       SELF.Net_Servers_Q.Send_User_Option      = TCP:Send_User_Option   ! p_SMSMeNet_Servers.Send_User_Option
!
!       SELF.Net_Servers_Q.Order                 = TCP:Order              ! p_SMSMeNet_Servers.Order
!       SELF.Net_Servers_Q.Use_Fixed_Order_Only  = TCP:Use_Fixed_Order_Only ! p_SMSMeNet_Servers.Use_Fixed_Order_Only
!       SELF.Net_Servers_Q.Connections           = TCP:Connections        ! p_SMSMeNet_Servers.Connections

       ADD(SELF.Net_Servers_Q)
    .

    p_SMSMeNet_Servers.Close()

    SELF.Init_Final(SELF.Ini_File, SELF.Ini_Section, p:Connect_Req)
    RETURN


SMSMeNet_Class.Init_Final           PROCEDURE(STRING p:Ini_File, STRING p:Ini_Section, BYTE p:Connect_Req)      ! ( M )
L:Idx       LONG
L:Byte      BYTE

    CODE
    ! Sort server Q
    SORT(SELF.Net_Servers_Q, -SELF.Net_Servers_Q.Connections)

    L:Idx   = 0
    LOOP
       L:Idx    += 1
       GET(SELF.Net_Servers_Q, L:Idx)
       IF ERRORCODE()
          BREAK
       .

       IF SELF.Net_Servers_Q.Use_Fixed_Order_Only
          PUT(SELF.Net_Servers_Q, SELF.Net_Servers_Q.Order)
    .  .

    ! Get Login and password
    SELF.Cl_Login           = GETINI(CLIP(p:Ini_Section), 'SMSMeNet_Login', 'SMSMeNetClient', CLIP(p:Ini_File))
    SELF.Cl_Password        = GETINI(CLIP(p:Ini_Section), 'SMSMeNet_Password', 'smsmenetclient', CLIP(p:Ini_File))

    ! Get CID - Prefix and CID
    SELF.CID_Group.Prefix   = GETINI(CLIP(p:Ini_Section), 'ClientID_Prefix', '', CLIP(p:Ini_File))
    SELF.CID_Group.CID      = GETINI(CLIP(p:Ini_Section), 'ClientID_CID', '', CLIP(p:Ini_File))

    SELF.Send_Per_Event     = GETINI(CLIP(p:Ini_Section), 'Send_Per_Event', 5, CLIP(p:Ini_File))
    SELF.Receive_Per_Event  = GETINI(CLIP(p:Ini_Section), 'Receive_Per_Event', 5, CLIP(p:Ini_File))

    SELF.Start_Delim        = CLIP( GETINI(CLIP(p:Ini_Section), 'Start_Delim', '|+|', CLIP(p:Ini_File)) )
    SELF.Field_Delim        = CLIP( GETINI(CLIP(p:Ini_Section), 'Field_Delim', '|', CLIP(p:Ini_File))   )
    SELF.Data_Delim         = CLIP( GETINI(CLIP(p:Ini_Section), 'Data_Delim', ':', CLIP(p:Ini_File))    )
    SELF.End_Delim          = CLIP( GETINI(CLIP(p:Ini_Section), 'End_Delim', '|-|', CLIP(p:Ini_File))   )

    SELF.Dump_Data_Opt      = GETINI(CLIP(p:Ini_Section), 'Dump_Data_Opt', 3, CLIP(p:Ini_File))
    L:Byte                  = GETINI(CLIP(p:Ini_Section), 'Dump_Data_Opt_RP', 1, CLIP(p:Ini_File))

    IF L:Byte = 1
       SELF.Dump_Data_Opt   = BOR(SELF.Dump_Data_Opt, 00000100b)          ! Set Recd_Proc_Dump ON
    .

    SELF.Sent_ID            = GETINI(CLIP(p:Ini_Section), 'Sent_ID', 0, CLIP(p:Ini_File))

    SELF.User_Connect_Req   = GETINI(CLIP(p:Ini_Section), 'User_Connect_Req', 2, CLIP(p:Ini_File))  ! Def - connect if req send / ack

    CLEAR(SELF.Recd_Str)
    SELF.Recd_S_Len         = 0

    SELF.Connect_Req        = p:Connect_Req
    SELF.Connection_Attemps = 3

    SELF.Check_User_Connect_Req_Opt()                   ! Check default connect action

    ! Keeping original details if no ini entry found
    SELF.RSR_State_Check    = GETINI(CLIP(p:Ini_Section), 'RSR_State_Check', SELF.RSR_State_Check, CLIP(p:Ini_File))
    SELF.Send_RMM_CID       = GETINI(CLIP(p:Ini_Section), 'Send_RMM_CID', SELF.Send_RMM_CID, CLIP(p:Ini_File))

    SELF.Init_ed            = TRUE
    RETURN


SMSMeNet_Class.Check_User_Connect_Req_Opt       PROCEDURE()
    CODE
    !IF SELF.Connect_State = 0                           ! 0 - closed, 1 - open, 2 - openning, 3 - Open & Setup, 4 - closing
    IF SELF.Check_State() = 0
       IF SELF.Connect_Req = 0                          ! Nothing requested
          CASE SELF.User_Connect_Req
          OF 1                                          ! Do we have Out bound msgs waiting?
             IF RECORDS(SELF.Out_Msgs_Q) > 0
                SELF.Connect_Req          = TRUE
                SELF.Connection_Attemps   = 3
             .
          OF 2                                          ! Do we have Out bound or Sent msgs waiting acks?
             IF RECORDS(SELF.Out_Msgs_Q) > 0 OR RECORDS(SELF.Sent_Msgs_Q) > 0
                SELF.Connect_Req          = TRUE
                SELF.Connection_Attemps   = 3
             .
          OF 3                                          ! Allways connect on startup
             SELF.Connect_Req             = TRUE
             SELF.Connection_Attemps      = 3
    .  .  .
    RETURN



! ---------   SMS Me Receiving - Recd_Add_Packet_Q / Recd_Process_Recd_Str / Recd_Process_Packet   ---------
SMSMeNet_Class.Recd_Process_Packet      PROCEDURE(STRING p:Packet)
L_Field_Q           QUEUE,PRE(L_FQ)
Field                   STRING(3)
Element                 STRING(500)
                    .
L:Result            LONG
L:Idx               LONG
L:Recd_Msg_Type     BYTE
L:Recd_Type_Result  LONG

L:Audit_Text        CSTRING(151)

    CODE
    ! -----------------   Process fields to Q   -----------------
    DO Process_Fields_to_Q

    ! -----------------   Process Q  -----------------
    IF RECORDS(L_Field_Q)
       ! We had some fields then possibly - first we want to make sure we have the MID loaded if it exists
       ! We should only have 1 per message that reaches this stage.
       L:Recd_Msg_Type  = 0
       L:Idx            = 0

       LOOP     ! L:Idx = 1 TO x                                  ! Look for field
          L:Idx += 1
          CLEAR(L_Field_Q)

          EXECUTE L:Idx
             L_FQ:Field       = 'MID'                             ! Msg. ID from serevr for Sent Msg
             L_FQ:Field       = 'RCM'                             ! Recd. msg from server for this client
             L_FQ:Field       = 'RPU'                             ! Server request push users
             L_FQ:Field       = 'CID'                             ! Client ID from server to assign
             L_FQ:Field       = 'SCO'                             ! System command from server (not implemented)
             L_FQ:Field       = 'RSR'                             ! Server responding to our RSR req.
             L_FQ:Field       = 'RMW'                             ! No. msgs waiting
             L_FQ:Field       = 'RLI'                             ! Received license
             BREAK
          .

          GET(L_Field_Q, +L_FQ:Field)
          IF ~ERRORCODE()                                           ! Found field
             EXECUTE L:Idx
                L:Recd_Type_Result  = SELF.Process_MID(L_Field_Q)   ! 'MID'     - store for user
                L:Recd_Type_Result  = SELF.Process_RCM(L_Field_Q)   ! 'RCM'     - store for user
                L:Recd_Type_Result  = SELF.Process_RPU(L_Field_Q)   ! 'RPU'     - send server push users list
                DO Process_CID                                      ! 'CID'     - update persistant CID
                DO Process_Sys_Cmd                                  ! 'SCO'     - do system command
                DO Process_RSR                                      ! 'RSR'     - update RSR
                SELF.Server_Msgs_No           = L_FQ:Element        ! 'RMW'     - set class variable
                DO Recd_License
             .

             L:Recd_Msg_Type = L:Idx
             BREAK
       .  .

       DO General_Msg_Proc

       IF RECORDS(L_Field_Q)
          IF L:Recd_Msg_Type = 0                                    ! We have no known type. - Dump Q to audit file
             L:Result       = -1
             L:Audit_Text   = 'No Recd. Type identified to process'
          ELSIF RECORDS(L_Field_Q)                                  ! Un processed fields??
             L:Audit_Text   = 'Recd. Type IDed as ' & L:Recd_Msg_Type & ' - Unprocessed fields'
          .

          LOOP L:Idx = 1 TO RECORDS(L_Field_Q)
             GET(L_Field_Q, L:Idx)
             IF ERRORCODE()
                BREAK
             .

             SELF.Add_Audit('Recd_Process_Packet - Event: ' & SELF.Event_ID, L:Audit_Text & ' - Field (' & |
                             L:Idx & '): ' & L_FQ:Field, 'Element: ' & L_FQ:Element, 2, 'Recd. Unknown Type')
    .  .  .

    RETURN(L:Result)


Recd_License            ROUTINE
    SELF.Licenses_Q.Client_Reg_ID       = L_FQ:Element        ! RLI field record loaded
    GET(SELF.Licenses_Q, +SELF.Licenses_Q.Client_Reg_ID)
    IF ERRORCODE()
       CLEAR(SELF.Licenses_Q)
       SELF.Licenses_Q.Client_Reg_ID    = L_FQ:Element
       ADD(SELF.Licenses_Q)
    .
    SELF.Licenses_Q.Received_Date       = TODAY()
    SELF.Licenses_Q.Received_Time       = CLOCK()

    LOOP L:Idx = RECORDS(L_Field_Q) TO 1 By -1      ! Keep a list of all received licenses
       GET(L_Field_Q, L:Idx)
       IF ERRORCODE()
          BREAK
       .
       CASE L_FQ:Field
       OF 'RLI'                                     ! RLI:<Clients ID>
          ! Already updated above - where previous license record is got if exists
          DELETE(L_Field_Q)
       OF 'RLN'                                     ! RLN:<License No. / Reg. No>
          SELF.Licenses_Q.License_No    = L_FQ:Element
          DELETE(L_Field_Q)
    .  .
    PUT(SELF.Licenses_Q)

    SELF.Trigger_Events()
    EXIT


General_Msg_Proc        ROUTINE
    LOOP L:Idx = RECORDS(L_Field_Q) TO 1 By -1
       GET(L_Field_Q, L:Idx)
       IF ERRORCODE()
          BREAK
       .
       CASE L_FQ:Field
       OF 'ERR'
          SELF.Add_Audit('R General_Msg_Proc','Error code for recd msg type: ' & L:Recd_Msg_Type & ' result: ' & L:Recd_Type_Result,'Information: ' & CLIP(L_FQ:Element), 3, 'Recd_Process_Packet')
          DELETE(L_Field_Q)
       OF 'INF'                             ! Information in text format - to help the clients human interface
          IF L:Recd_Msg_Type = 1            ! MID recd.
             SELF.Add_Audit('R General_Msg_Proc','Info recd for msg Sent ID: ' & L:Recd_Type_Result,'Information: ' & CLIP(L_FQ:Element), 6, 'Recd_Process_Packet')
          ELSE
             SELF.Add_Audit('R General_Msg_Proc','Info. received - Message type: ' & L:Recd_Msg_Type & ' result: ' & L:Recd_Type_Result,'Information: ' & CLIP(L_FQ:Element), 6, 'Recd_Process_Packet')
          .                                                                          
          DELETE(L_Field_Q)
       OF 'ERT'                             ! Information in text format about errors - to help the clients human interface
          IF L:Recd_Msg_Type = 1            ! MID recd.
             SELF.Add_Audit('R General_Msg_Proc','Error text received for msg Sent ID: ' & L:Recd_Type_Result,'Text: ' & CLIP(L_FQ:Element),3,'Recd_Process_Packet')
          ELSE
             SELF.Add_Audit('R General_Msg_Proc','Error text - Message type: ' & L:Recd_Msg_Type & ' result: ' & L:Recd_Type_Result,'Text: ' & CLIP(L_FQ:Element),3,'Recd_Process_Packet')
          .
          DELETE(L_Field_Q)
       ELSE
          SELF.Add_Audit('R General_Msg_Proc','Unknown Field (' & CLIP(L_FQ:Field) & ') - Message type: ' & L:Recd_Msg_Type & ' result: ' & L:Recd_Type_Result,'Information: ' & CLIP(L_FQ:Element),3,'Recd_Process_Packet')
          DELETE(L_Field_Q)
    .  .
    EXIT


Process_Sys_Cmd         ROUTINE                     ! 'SCO'     - do system command
    SELF.Server_Sys_Cmd_Last    = L_FQ:Element

    ! Do command now ???

    DELETE(L_Field_Q)
    EXIT


Process_RSR             ROUTINE                     ! 'RSR'     - update RSR
    SELF.Server_RSR_Resp        = L_FQ:Element
    SELF.RSR_State              = 2                 ! Confirmed

    DELETE(L_Field_Q)
    EXIT


Process_CID             ROUTINE
    IF DEFORMAT(L_FQ:Element) ~= 0
       SELF.CID_Group.Prefix    = L_FQ:Element[1 : 4]
       SELF.CID_Group.CID       = L_FQ:Element[5 : LEN(CLIP(L_FQ:Element))]

       SELF.Save_INIs('CID')
    ELSE
       SELF.Add_Audit('Recd_Process_Packet - Process_CID','Client ID missing ?!!  Field: ' & CLIP(L_FQ:Field), 'No Data: ' & CLIP(L_FQ:Element), 1)
    .

    DELETE(L_Field_Q)
    EXIT


Process_Fields_to_Q                 ROUTINE
    FREE(L_Field_Q)
    LOOP
       IF CLIP(p:Packet) = ''
          BREAK
       .

       CLEAR(L_Field_Q)

       L_FQ:Element     = Get_1st_Element_From_Delim_Str(p:Packet, SELF.Field_Delim, TRUE)
       L_FQ:Field       = UPPER(Get_1st_Element_From_Delim_Str(L_FQ:Element, SELF.Data_Delim, TRUE))

       ADD(L_Field_Q, +L_FQ:Field)
    .
    EXIT




SMSMeNet_Class.Recd_Process_Recd_Str    PROCEDURE(STRING p:New_Packet, LONG p:Packet_Len)
L:Result            LONG
L:Start_Pos         LONG
L:Start_Pos_Other   LONG
L:End_Pos           LONG
L:Data              LIKE(SELF.Recd_Str)


    CODE
    ! ---------------   Add our new packet to our string   ---------------
    IF SELF.Recd_S_Len > 0
       SELF.Recd_Str    = SELF.Recd_Str[1 : SELF.Recd_S_Len] & p:New_Packet[1 : p:Packet_Len]
    ELSE
       SELF.Recd_Str    = p:New_Packet[1 : p:Packet_Len]
    .
    SELF.Recd_S_Len    += p:Packet_Len


    ! ---------------   Strip completed messages out of the string   ---------------
    LOOP
       L:Start_Pos          = INSTRING(SELF.Start_Delim, SELF.Recd_Str, 1, 1)
       L:End_Pos            = INSTRING(SELF.End_Delim, SELF.Recd_Str, 1, 1)

       ! We must make sure that we have the last Start Pos before the 1st end pos.
       ! Otherwise we may have junk strings.
       L:Start_Pos_Other    = L:Start_Pos
       LOOP
          L:Start_Pos_Other = INSTRING(SELF.Start_Delim, SELF.Recd_Str, 1, L:Start_Pos_Other + 1)
          IF L:Start_Pos_Other > L:End_Pos OR L:Start_Pos_Other <= 0
             BREAK
          .

          ! We have found a later start pos.  We have an incomplete msg. - audit
          SELF.Add_Audit('Recd_Process_Recd_Str','Incomplete Msg - missing End Delim (' & CLIP(SELF.End_Delim) & |
                         '),  Start Pos 1 @ ' & L:Start_Pos & ', Pos 2 @ ' & L:Start_Pos_Other, |
                         SELF.Recd_Str[L:Start_Pos : L:Start_Pos_Other + 2], |
                          1, 'Recd Packet Error')
          L:Start_Pos       = L:Start_Pos_Other             ! Adjust Start to last Start Pos
       .

       IF L:End_Pos < L:Start_Pos AND L:End_Pos ~= 0
          ! This means that we had a string without a start pos - throw away - and report ???
          SELF.Recd_Str     = SELF.Recd_Str[L:Start_Pos : SELF.Recd_S_Len]
          SELF.Recd_S_Len  -= L:Start_Pos - 1

          L:Start_Pos       = 1                             ! new - untested - 17 01 2004

          ! Try get another end pos - which now cannot be before the start pos
          L:End_Pos         = INSTRING(SELF.End_Delim, SELF.Recd_Str, 1, L:End_Pos+1)
       .

       IF L:End_Pos <= 0 OR L:Start_Pos <= 0
          BREAK                                             ! No full packet to process
       ELSE
          IF BAND(SELF.Dump_Data_Opt, 00000100b) = 4
             SELF.Dump_Data('Recd_Proc_Dump.TXT', SELF.Recd_Str[L:Start_Pos : L:End_Pos + LEN(SELF.End_Delim) - 1])
          .

          ! -----------------   Strip Start and End delims   -----------------
          IF (L:Start_Pos + LEN(SELF.Start_Delim)) < (L:End_Pos - 1)
             L:Data         = SELF.Recd_Str[L:Start_Pos + LEN(SELF.Start_Delim) : L:End_Pos - 1]
             SELF.Recd_Process_Packet(L:Data)
          .

          ! -----------------   Reduce our received string   -----------------
          IF (L:End_Pos + LEN(SELF.End_Delim)) < SELF.Recd_S_Len
             SELF.Recd_Str     = SELF.Recd_Str[L:End_Pos + LEN(SELF.End_Delim) : SELF.Recd_S_Len]
             SELF.Recd_S_Len  -= L:End_Pos + LEN(SELF.End_Delim) - 1
          ELSE
             SELF.Recd_Str     = ''
             SELF.Recd_S_Len   = 0
          .

          L:Result         += 1
    .  .

    RETURN( L:Result )                                      ! L:Result is no of packets processed




SMSMeNet_Class.Recd_Add_Packet_Q        PROCEDURE(STRING p:Packet, LONG p:Packet_Len)
L:Result        LONG
L:Add_Len       LONG
L:Start         LONG(1)
L:End           LONG(0)
L:Q_ID          ULONG
    CODE
    GET(SELF.In_Packets_Q, RECORDS(SELF.In_Packets_Q))

    IF ERRORCODE()
       CLEAR(SELF.In_Packets_Q)
    .
    L:Q_ID  = SELF.In_Packets_Q.Q_ID

    ! We need to add in chunks of 2000 chars no more
    LOOP
       IF p:Packet_Len <= 0
          BREAK
       .
       L:Q_ID   += 1

       IF p:Packet_Len > EQU:Cl_Net_Packet_Size
          L:Add_Len     = EQU:Cl_Net_Packet_Size
       ELSE
          L:Add_Len     = p:Packet_Len
       .
       L:End    += L:Add_Len

       SELF.In_Packets_Q.Q_ID      = L:Q_ID
       SELF.In_Packets_Q.Packet    = p:Packet[L:Start : L:End]
       SELF.In_Packets_Q.Len       = L:Add_Len

       ADD(SELF.In_Packets_Q)
       IF ~ERRORCODE()
          L:Result      += 1
          p:Packet_Len  -= L:Add_Len
          L:Start       += L:Add_Len

          IF BAND(SELF.Dump_Data_Opt, 00000011b) = 1 OR BAND(SELF.Dump_Data_Opt, 00000011b) = 3
          !IF SELF.Dump_Data_Opt = 1 OR SELF.Dump_Data_Opt = 3             ! Incoming or Both
             SELF.Dump_In_Packets('Recd_Packet_Dump.TXT', FALSE, RECORDS(SELF.In_Packets_Q))     ! Last record
          .
       ELSE
          L:Result       = -1
          BREAK
    .  .

    SELF.Trigger_Events(1)                              ! New Packet Event = 1

    RETURN(L:Result)



! ---------   SMS Me Sending - Send_RSR / Add_Send_Msg (M) / Setup   ---------
SMSMeNet_Class.Setup                   PROCEDURE()
L:Result            LONG
L:RSR_Sent_Time     LONG,STATIC
    CODE
    CASE SELF.Check_State()
    OF 0                                        ! Closed
       IF SELF.Connect_Req = 1                  ! Open please
          IF SELF.Connection_Attemps <= 0
             SELF.Connect_Req   = 0
!             MESSAGE('Connection attemps exhausted.', 'Connection Problem', ICON:Hand)
          ELSE               
             L:Result   = SELF.Connect()        ! Try to connect
             IF L:Result < 0
!                MESSAGE('Could not connect.||Error ' & SELF.Error_Text(L:Result) & ' (' & L:Result & ')', 'Connection Problem', ICON:Hand)
                SELF.Connection_Attemps  = 0    ! This is a permanent error
       .  .  .
    OF 1                                        ! Open
       ! We need to Setup the connection before sending any messages.
       IF SELF.RSR_State_Check = TRUE AND SELF.RSR_State = 0
          ! First we need to send an RSR and wait for a response establishing that there is a SMS Me Net service running.
          IF SELF.Send_RSR() < 0
             SELF.Reconnect()                                               ! Re-connect
          ELSE
             L:RSR_Sent_Time   = CLOCK()
          .
       ELSIF SELF.RSR_State_Check = FALSE OR SELF.RSR_State = 2             ! Confirmed or No Check
          SELF.Set_State(3)                                                 ! Set Open & Setup

          SELF.Sent_Msgs_Set_Time()                                         ! Set Time_In_Q to now - used by Re_Send_Msg

          IF SELF.Send_RMM_CID = TRUE
             ! Then we need to send our message receiving request option (request message mode RMM) and our client ID (CID).
             ! Add RMM and CID at top of send Q
             L:Result  = SELF.Add_Send_Msg_to_Q(SELF.Add_Field_Packet('RMM', SELF.Rec_Msg_Mode), 1)

             L:Result  = SELF.Add_Send_Msg_to_Q(SELF.Add_Field_Packet('CID', CLIP(SELF.CID_Group.Prefix) & SELF.CID_Group.CID), 1)
          .
       ELSIF SELF.RSR_State = 1
          ! Need to check for timeout of RSR Sent state = 1
          IF ABS(CLOCK() - L:RSR_Sent_Time) > (100 * 60 * 5)                ! 5 mins
             SELF.Reconnect()                                               ! Re-connect
    .  .  .
    RETURN



SMSMeNet_Class.Send_RSR                 PROCEDURE()       ! Check status of the server - is it a SMS Me Net server
L:Result        LONG
    CODE
    SELF.RSR_State  = 0

    IF SELF.Send(SELF.Make_Packet(SELF.Add_Field_Packet('RSR','0'))) < 0             ! Sending failed
       ! Haven't been able to send the RSR message and should revert back to finding another server?
!       L_TIG:Server_Status  = 0
       L:Result             = EQU:Err_RSR_Send_Failed    ! = -103
       SELF.Set_Last_Error(L:Result)
    ELSE
       SELF.RSR_State       = 1                          ! Sent
    .
    RETURN( L:Result )



SMSMeNet_Class.Add_Send_Msg            PROCEDURE(<STRING p:User_Login>, <STRING p:User_Password>, STRING p:Cell_No, |
                                                STRING p:Msg, <*LONG p:Msg_ID>, <*BYTE p:Q_Opt>, <*BYTE p:Delivery_Confirm>, |
                                                <*LONG p:Feature_ID>, <*STRING p_Deliver_Date_Time_Group>, |
                                                <*STRING p_Concat_M_Group>)

! Changed p:Feature_ID from USHORT to LONG - for vb
! Changed p:Msg_ID from ULONG to LONG      - for vb

L:Result            LONG
L:Msg_Str           STRING(1500)
L:Work_String       STRING(100)

L_Deliver_Date_Time_Group   GROUP,PRE(L_DTG)
Before_Date         LONG
Before_Time         LONG
After_Date          LONG
After_Time          LONG
                            .

L_Concat_M_Group            GROUP,PRE(L_CMG)
Concat_Msg          BYTE
Msg_No              BYTE
Total_Msgs          SHORT
Concat_ID           LONG                        ! New field - Feb 19
                            .

LOC:Q_Opt           BYTE

    CODE
    IF SELF.Init_ed = TRUE
       IF OMITTED(2) OR CLIP(p:User_Login) = ''
          p:User_Login           = SELF.Cl_Login
          IF OMITTED(3) OR CLIP(p:User_Password) = ''
             p:User_Password     = SELF.Cl_Password
          .
       ELSE
          IF OMITTED(3)
             p:User_Password     = ''
       .  .

       SELF.Add_Field_Packet('LOG', p:User_Login, L:Msg_Str)
       SELF.Add_Field_Packet('PWD', p:User_Password, L:Msg_Str)
       SELF.Add_Field_Packet('CEL', p:Cell_No, L:Msg_Str)
       SELF.Add_Field_Packet('MSG', p:Msg, L:Msg_Str)

       IF ~OMITTED(6)
          SELF.Add_Field_Packet('MID', p:Msg_ID, L:Msg_Str)
          SELF.Sent_ID  = p:Msg_ID
       ELSE
          SELF.Sent_ID += 1
          SELF.Add_Field_Packet('MID', SELF.Sent_ID, L:Msg_Str)
       .

       IF ~OMITTED(7)
          LOC:Q_Opt = p:Q_Opt
          SELF.Add_Field_Packet('MQO', p:Q_Opt, L:Msg_Str)
       ELSE
          LOC:Q_Opt = 0                                        ! Last server assumed if not provided
       .

       IF ~OMITTED(8)
          IF p:Delivery_Confirm ~= 0
             SELF.Add_Field_Packet('DEL', p:Delivery_Confirm, L:Msg_Str)
       .  .
       IF ~OMITTED(9)
          IF p:Feature_ID ~= 0
             SELF.Add_Field_Packet('FEA', p:Feature_ID, L:Msg_Str)
       .  .

       IF ~OMITTED(10)
          IF LOC:Q_Opt = 0
             L_Deliver_Date_Time_Group    = p_Deliver_Date_Time_Group
             IF L_DTG:Before_Date ~= 0         ! (p:Date_From, p:Time_From, p:Date_To, p:Time_To, p:Option)
                L:Work_String     = Date_Time_Difference(TODAY(), CLOCK(), L_DTG:Before_Date, L_DTG:Before_Time)

                SELF.Add_Field_Packet('OBD', Get_1st_Element_From_Delim_Str(L:Work_String,',',TRUE), L:Msg_Str)
                SELF.Add_Field_Packet('OBT', Get_1st_Element_From_Delim_Str(L:Work_String,',',TRUE), L:Msg_Str)
             .
             IF L_DTG:After_Date ~= 0
                L:Work_String     = Date_Time_Difference(TODAY(), CLOCK(), L_DTG:After_Date, L_DTG:After_Time)

                SELF.Add_Field_Packet('SOD', Get_1st_Element_From_Delim_Str(L:Work_String,',',TRUE), L:Msg_Str)
                SELF.Add_Field_Packet('SOT', Get_1st_Element_From_Delim_Str(L:Work_String,',',TRUE), L:Msg_Str)
             .
          ELSE                                                      ! Local server -
             ! Then server has decided to send now so we do not want to send any delay times
       .  .

       IF ~OMITTED(11)
          L_Concat_M_Group = p_Concat_M_Group
          IF L_CMG:Concat_Msg ~= 0 AND L_CMG:Total_Msgs > 1                     
             SELF.Add_Field_Packet('CON', L_CMG:Concat_Msg, L:Msg_Str)
             SELF.Add_Field_Packet('MNO', L_CMG:Msg_No, L:Msg_Str)
             SELF.Add_Field_Packet('MTO', L_CMG:Total_Msgs, L:Msg_Str)
             SELF.Add_Field_Packet('CMI', L_CMG:Concat_ID, L:Msg_Str)           ! New field - Feb 19
       .  .

       ! ----------------------------------   Send the Message   ----------------------------------
       L:Result             = SELF.Add_Send_Msg_to_Q(L:Msg_Str,,, 1, SELF.Sent_ID)
       IF L:Result > 0
          ! Result would be 0 if the Q record was not added.  If Sent_ID was passed as 0
          ! then we would be setting this here to 0.  The meaning would be unclear.
          ! Therefore we are going to change the returned value on zero
          L:Result          = SELF.Sent_ID
       ELSIF L:Result = 0
          L:Result          = EQU:Err_General_Add_Queue      ! = -114
       .
    ELSE
       L:Result             = EQU:Err_Not_Initilised         ! = -101

       IF ~OMITTED(6)
          SELF.Set_Last_Error(L:Result, p:Msg_ID)
       ELSE
          SELF.Set_Last_Error(L:Result)
    .  .

    RETURN(L:Result)


    ! Are we interested in what profile they used to senf the message?
    !IF LO_TI:Send_Profile_ID ~= 0
    !   LOC:TCP_IP_Msg  = LOC:TCP_IP_Msg & L_TID:F_Delim &   'PRO'  & L_TID:D_Delim &  CLIP(LO_TI:Send_Profile_ID)
    !.

    ! LO_TI:Message_ID
    ! LO_TI:User_ID

    ! It is possible that the user would want the messages open on their network but private on ours.....???
    ! LO_TI:Sent_Private_User

! ---------   Communication - Send / Connect / Disconnect / Check_State   --------- (M) (all use Cl_NetSimple)
SMSMeNet_Class.Send                PROCEDURE(STRING p:Send_Str)
L:Result        LONG

    CODE
!    SETCURSOR (CURSOR:Wait)
    IF SELF.Cl_NetSimple &= NULL
       L:Result             = EQU:Err_No_NetSimple_Obj                          ! = -102
       SELF.Set_Last_Error(L:Result)
    ELSE
       CLEAR (SELF.Cl_NetSimple.Packet)

       SELF.Cl_NetSimple.Packet.BinData     = CLIP(p:Send_Str) & '<13,10>'
       SELF.Cl_NetSimple.Packet.BinDataLen  = LEN(CLIP(p:Send_Str))+2

       SELF.Cl_NetSimple.Send()

       IF SELF.Cl_NetSimple.Error ~= 0
          L:Result              = EQU:Err_NS_Obj_Send_Error              ! = -104

          SELF.Set_Last_Error(L:Result)
          SELF.Add_Audit('Send', 'NetSimple Error (' & SELF.Cl_NetSimple.Error & '): ' & SELF.Cl_NetSimple.InterpretError(), 'Send str: ' & p:Send_Str, 1, '[SMSMeNetClient]')

          IF SELF.Cl_NetSimple.Error = ERROR:ClientNotConnected
             SELF.Connection_Closed()
          ELSE
             SELF.Disconnect()                                           ! Close our connection on any error!
          .
       ELSE
          IF BAND(SELF.Dump_Data_Opt, 00000011b) = 2 OR BAND(SELF.Dump_Data_Opt, 00000011b) = 3
             SELF.Dump_Data('Send_Dump.TXT', SELF.Cl_NetSimple.Packet.BinData, SELF.Cl_NetSimple.Packet.BinDataLen)
    .  .  .

!    SETCURSOR
    RETURN(L:Result)



SMSMeNet_Class.Check_State             PROCEDURE(BYTE p:Report)
L_State         LONG
    CODE
    IF SELF.Init_ed = FALSE
       L_State              = EQU:Err_NS_Obj_Not_Inited                     ! = -105
       SELF.Set_Last_Error(L_State)
    ELSE
       IF SELF.Cl_NetSimple &= NULL
          L_State               = EQU:Err_No_NetSimple_Obj                  ! = -102
          SELF.Set_Last_Error(L_State)
       ELSE
          CASE SELF.Connect_State
          OF 0                                                              ! Was closed
             IF SELF.Cl_NetSimple.OpenFlag = TRUE                           ! Connection is open
                SELF.Connect_State    = 1
             .
          OF 1                                                              ! Was open
             IF SELF.Cl_NetSimple.OpenFlag = FALSE                          ! Connection is closed
                SELF.Connect_State    = 0
             .
          OF 2                                                              ! Was openning
             IF SELF.Cl_NetSimple.OpenFlag = TRUE                           ! Connection is open
                SELF.Connect_State    = 1
             .
          OF 3                                                              ! Was open and setup
             IF SELF.Cl_NetSimple.OpenFlag = FALSE                          ! Connection has closed
                SELF.Connect_State    = 0
             .
          OF 4                                                              ! Was closing
             IF SELF.Cl_NetSimple.OpenFlag = FALSE                          ! Connection has closed
                SELF.Connect_State    = 0
          .  .

          L_State  = SELF.Connect_State
    .  .

    IF p:Report = TRUE
       CASE L_State
       OF 1 TO 4
          EXECUTE L_State
             MESSAGE('Connection open.','SMS Me Net Object',ICON:Hand)               ! 1
             MESSAGE('Connection openning.','SMS Me Net Object',ICON:Hand)           ! 2
             MESSAGE('Connection open & setup.','SMS Me Net Object',ICON:Hand)       ! 3
             MESSAGE('Connection closing.','SMS Me Net Object',ICON:Hand)            ! 4
          ELSE
             MESSAGE('Connection closed.','SMS Me Net Object',ICON:Hand)             ! 0 and ?
          .
       OF -105
          MESSAGE('The SMS Me Net object is not initilised.','SMS Me Net Object',ICON:Hand)
       OF -102
          MESSAGE('The SMS Me Net object does not have a NetSimply object set.','SMS Me Net Object',ICON:Hand)
    .  .

    RETURN(L_State)



SMSMeNet_Class.Connect                 PROCEDURE(<STRING p:Address>, <STRING p:Port>)
L:Result        LONG
    CODE
    CLEAR(SELF.Server_G)

    IF SELF.Cl_NetSimple &= NULL
       L:Result             = EQU:Err_No_NetSimple_Obj          ! = -102
       SELF.Set_Last_Error(L:Result)
    ELSE
       IF SELF.Cl_NetSimple.OpenFlag = TRUE                     ! Connection is open
          L:Result     = 1                                      ! 0 connected, 1 already connected
       ELSE
          IF OMITTED(2)
             ! We must get a server from our Q
             SELF.Server_Q_ID    += 1

             LOOP
                GET(SELF.Net_Servers_Q, SELF.Server_Q_ID)
                IF ERRORCODE()
                   IF SELF.Server_Q_ID > 1 AND RECORDS(SELF.Net_Servers_Q) > 0
                      SELF.Server_Q_ID    = 1                         ! Go back to server 1 in the Q - and try again
                      CYCLE
                   .
                   BREAK
                .

                SELF.Server_G.Address           = SELF.Net_Servers_Q.Server_Address
                SELF.Server_G.Port              = SELF.Net_Servers_Q.Server_Port
                SELF.Server_G.Send_User_Option  = SELF.Net_Servers_Q.Send_User_Option
                SELF.Server_G.Default_User      = SELF.Net_Servers_Q.Default_User
                SELF.Server_G.TCP_IP_ID         = SELF.Net_Servers_Q.TCP_IP_ID

                !SELF.Address   = SELF.Net_Servers_Q.Default_User
                !SELF.Net_Servers_Q.Send_User_Option      
                !SELF.Net_Servers_Q.Order                 
                !SELF.Net_Servers_Q.Use_Fixed_Order_Only  
                !SELF.Net_Servers_Q.Connections
                BREAK
             .
          ELSE
             SELF.Server_G.Address  = p:Address
             SELF.Server_Q_ID       = 0
          .
          IF ~OMITTED(3)
             SELF.Server_G.Port     = p:Port
          .

          IF CLIP(SELF.Server_G.Address) = ''
             L:Result               = EQU:Err_No_Server_Address             ! = -106
          .
          IF CLIP(SELF.Server_G.Port) = ''
             L:Result               = EQU:Err_No_Server_Port                ! = -107
          .
          SELF.Set_Last_Error(L:Result)

          IF L:Result >= 0                                                  ! Try to connect this server
             SELF.Cl_NetSimple.AsyncOpenUse     = 1
             SELF.Cl_NetSimple.AsyncOpenTimeout = 5000                      ! 50 seconds

             SELF.Cl_NetSimple.Open(SELF.Server_G.Address, SELF.Server_G.Port)
             SELF.Set_State(2)                                              ! Openning
    .  .  .

    RETURN(L:Result)



SMSMeNet_Class.Disconnect              PROCEDURE()
L:Result        LONG
    CODE
    IF SELF.Cl_NetSimple &= NULL
       L:Result             = EQU:Err_No_NetSimple_Obj          ! = -102
       SELF.Set_Last_Error(L:Result)
    ELSE
       SELF.Cl_NetSimple.Close()
    .
    SELF.Set_State(4)                                           ! Closing
    RETURN( L:Result )



SMSMeNet_Class.Reconnect               PROCEDURE()
    CODE
    !IF SELF.Check_State() ~= 0
       SELF.Disconnect()
    !.

    ! Re-set connection req. - and re-connect
    SELF.Connect_Req            = TRUE
    SELF.Connection_Attemps     = 3
    SELF.RSR_State              = 0
    RETURN



! ---------   Set_State / Connection_Closed / Connect_Failed / Connected  --------- (M)
SMSMeNet_Class.Connected               PROCEDURE()
    CODE
    SELF.Set_State(1)
    SELF.Connect_Req  = 0
    RETURN


SMSMeNet_Class.Set_State               PROCEDURE(BYTE p:State)
    CODE
    ! Need to call this in .ErrorTrap, self.Packet.PacketType = NET:SimpleAsyncOpenFailed
    !               and in .Process(), self.Packet.PacketType = NET:SimpleAsyncOpenSuccessful

    SELF.Connect_State  = p:State

    SELF.Check_State()
    RETURN


SMSMeNet_Class.Connection_Closed       PROCEDURE()      ! Alway called when connection is closed??
    CODE
    SELF.Set_State(0)                                   ! Closed

    SELF.RSR_State  = 0                                 ! None
    RETURN


SMSMeNet_Class.Connect_Failed          PROCEDURE()      ! Async or other connection has failed
    CODE
    SELF.Connection_Closed()
    SELF.Connection_Attemps     -= 1
    SELF.Set_State(0)
    !My_SMSMeNet.Set_State(0)
    RETURN



! ---------   SMS Me Recd. - Recd_Msg_Ack_Send   ---------
SMSMeNet_Class.Recd_Msg_Ack_Send        PROCEDURE(ULONG p:Server_Rec_ID, <ULONG p:Clients_ID>, LONG p:Result=0)
L:Send_Str  STRING(250)

    CODE
    ! We want to respond with an acknowledgement and our Rec ID.
    ! Initial response will be from the object itself - method Process_RCM - without the Client ID
    !
    ! Subsequent update could be from the client - with the clients ID
    !      RCM:xxx     Message to Server intended - RESPONSE to msg. from server for Inbox on Client
    !                  - xxx is the servers EXR:Ex_RM_ID (Ex_Received_Msg_Refs) for this message
    !      RCI:xxx     Client Side Rec_ID for msg. received from the server being sent to the server
    !
    !      ERR:xxx     Now we may have a error result (p:Result)
    !      ERT:$$$$$
    !
    ! This was our original line - but now we have found that we previously were sending this with the following code
    ! L:Result  = SELF.Add_Send_Msg_to_Q( SELF.Add_Field_Packet('RID', p:Server_Rec_ID, L:Send_Str) )

    SELF.Add_Field_Packet('RCM', p:Server_Rec_ID, L:Send_Str)
    IF ~OMITTED(3)
       SELF.Add_Field_Packet('RCI', p:Clients_ID, L:Send_Str)
    .
    IF p:Result ~= 0
       SELF.Add_Field_Packet('ERR', p:Result, L:Send_Str)
       EXECUTE p:Result
          SELF.Add_Field_Packet('ERT', 'Client has msg for this Server ID already - Rejected', L:Send_Str)
          SELF.Add_Field_Packet('ERT', 'Client has msg for this Server ID already - Accepted', L:Send_Str)
          SELF.Add_Field_Packet('ERT', 'Client has msg for this Server ID already, msg. content differs - Accepted', L:Send_Str)
       ELSE
          SELF.Add_Field_Packet('ERT', '<Unknown Error Code>', L:Send_Str)
    .  .

    RETURN( SELF.Add_Send_Msg_to_Q( L:Send_Str ) )



! ---------   SMS Me Recd. - ReadIn_Received_Msgs / ReadIn_Recd_MIDs / ReadIn_Out_Sent_Msgs   ---------
SMSMeNet_Class.ReadIn_Licenses_Q       PROCEDURE(<STRING p:File_Name>, <GL_Cl_Licenses_Q p_D_Q>, BYTE p:Delete = 1)
Audit_File          FILE,DRIVER('BASIC'),CREATE,PRE(SCL_AUD)
Rec                 RECORD
Q_ID                    LONG
Reg_ID                  STRING(50)
Reg_No                  STRING(50)
Date                    LONG
Time                    LONG
                    . .

L:Result        LONG
L:File_Name     STRING(255)

    CODE
    IF OMITTED(3)
       p_D_Q     &= SELF.Licenses_Q
    .

    L:File_Name   = p:File_Name
    IF OMITTED(2)
       L:File_Name   = 'SMSMeNet_Licenses.csv'
    .
    IF SELF.Open_Dump_File(Audit_File, L:File_Name, 0) = 1              ! 0 = No create
       SET(Audit_File)
       LOOP
          NEXT(Audit_File)
          IF ERRORCODE()
             BREAK
          .
          IF SCL_AUD:Q_ID = 0
             CYCLE
          .

          !p_D_Q.Q_ID            = SCL_AUD:Q_ID
          p_D_Q.Client_Reg_ID   = SCL_AUD:Reg_ID
          p_D_Q.License_No      = SCL_AUD:Reg_No
          p_D_Q.Received_Date   = SCL_AUD:Date
          p_D_Q.Received_Time   = SCL_AUD:Time

          ADD(p_D_Q)
          IF ERRORCODE()
             L:Result               = EQU:Err_General_Add_Queue
             SELF.Set_Last_Error(L:Result)
             BREAK
          ELSE
             L:Result     += 1
       .  .
       CLOSE(Audit_File)

       IF p:Delete = TRUE
          REMOVE(Audit_File)
    .  .

    RETURN(L:Result)



SMSMeNet_Class.ReadIn_Out_Sent_Msgs         PROCEDURE(<STRING p:File_Name>, BYTE p:Delete,  <GL_Cl_Messages_Q_Type p_D_Q>)
Audit_File      FILE,DRIVER('BASIC'),CREATE,PRE(SCL_AUD)
Rec               RECORD
Q_ID                ULONG
Msg                 STRING(EQU:Cl_Sent_Msg_Size)
Date_In_Q           LONG
Time_In_Q           LONG
Type                BYTE
Ack_ID              ULONG
Date_Sent           LONG
Time_Sent           LONG
Response            BYTE                    ! Sent Q only
                . .
L:IDx           LONG
L:Result        LONG
L:File_Name     STRING(255)

    CODE
    IF OMITTED(4)
       p_D_Q     &= SELF.Out_Msgs_Q
    .

    L:File_Name   = p:File_Name
    IF OMITTED(2)
       L:File_Name   = 'SMSMeNet_Out_Going_Uprocessed.csv'
    .
    IF SELF.Open_Dump_File(Audit_File, L:File_Name, 0) = 1              ! 0 = No create
       SET(Audit_File)
       LOOP
          L:IDx        += 1
          NEXT(Audit_File)
          IF ERRORCODE()
             BREAK
          .
          IF SCL_AUD:Q_ID = 0
             CYCLE
          .

          p_D_Q.Q_ID            = SCL_AUD:Q_ID
          p_D_Q.Message         = SCL_AUD:Msg
          p_D_Q.Date_In_Q       = SCL_AUD:Date_In_Q
          p_D_Q.Time_In_Q       = SCL_AUD:Time_In_Q
          p_D_Q.Type            = SCL_AUD:Type                          ! Send Msg type
          p_D_Q.Ack_ID          = SCL_AUD:Ack_ID
          p_D_Q.Date_Sent       = SCL_AUD:Date_Sent
          p_D_Q.Time_Sent       = SCL_AUD:Time_Sent
          p_D_Q.Response        = SCL_AUD:Response

          ADD(p_D_Q)
          IF ERRORCODE()
             L:Result               = EQU:Err_Q_Add_Read_Out_Sent       ! = -108
             SELF.Set_Last_Error(L:Result)
             BREAK
          ELSE
             L:Result     += 1
       .  .
       CLOSE(Audit_File)

       IF p:Delete = TRUE
          REMOVE(Audit_File)
    .  .

    RETURN(L:Result)



SMSMeNet_Class.ReadIn_Received_Msgs    PROCEDURE(<STRING p:File_Name>, BYTE p:Delete)
Audit_File      FILE,DRIVER('BASIC'),CREATE,PRE(SCL_AUD)
Rec               RECORD
Line                 STRING(500),DIM(27)
                . .
L:IDx           LONG
L:Result        LONG
L:File_Name     STRING(255)

    CODE
    L:File_Name   = p:File_Name
    IF OMITTED(2)
       L:File_Name  = 'SMSMeNet_Received_Msgs_Unprocessed.csv'
    .
    IF SELF.Open_Dump_File(Audit_File, L:File_Name, 0) = 1              ! 0 = No create
       SET(Audit_File)
       LOOP
          L:IDx        += 1
          NEXT(Audit_File)
          IF ERRORCODE()
             BREAK
          .

          !SELF.Recd_RCM_Q.Client_CID         = SCL_AUD:Line[1]         - taken out of Q
          IF SCL_AUD:Line[1][1 : 13] = 'HDR  --------'
             CYCLE
          .

          CLEAR(SELF.Recd_RCM_Q)

          SELF.Recd_RCM_Q.User_Login         = SCL_AUD:Line[1]
          SELF.Recd_RCM_Q.Server_Rec_ID      = SCL_AUD:Line[2]
          SELF.Recd_RCM_Q.Read_Status        = SCL_AUD:Line[3]
          SELF.Recd_RCM_Q.Originator_Address = SCL_AUD:Line[4]
          SELF.Recd_RCM_Q.Data               = SCL_AUD:Line[5]
          SELF.Recd_RCM_Q.Service_Time_Stamp = SCL_AUD:Line[6]
          SELF.Recd_RCM_Q.Date               = SCL_AUD:Line[7]
          SELF.Recd_RCM_Q.Time               = SCL_AUD:Line[8]
          SELF.Recd_RCM_Q.Time_Zone          = SCL_AUD:Line[9]
          SELF.Recd_RCM_Q.Message            = SCL_AUD:Line[10]
          SELF.Recd_RCM_Q.Address_No         = SCL_AUD:Line[11]
          SELF.Recd_RCM_Q.SMSC_Address       = SCL_AUD:Line[12]
          SELF.Recd_RCM_Q.Msg_No             = SCL_AUD:Line[13]
          SELF.Recd_RCM_Q.Total_Msgs         = SCL_AUD:Line[14]
          SELF.Recd_RCM_Q.Type               = SCL_AUD:Line[15]
          SELF.Recd_RCM_Q.TA_Date            = SCL_AUD:Line[16]
          SELF.Recd_RCM_Q.TA_Time            = SCL_AUD:Line[17]
          SELF.Recd_RCM_Q.Received_Date      = SCL_AUD:Line[18]
          SELF.Recd_RCM_Q.Received_Time      = SCL_AUD:Line[19]
          SELF.Recd_RCM_Q.Read               = SCL_AUD:Line[20]
          SELF.Recd_RCM_Q.User_ID_Received_For = SCL_AUD:Line[21]
          SELF.Recd_RCM_Q.User_ID_Private_To = SCL_AUD:Line[22]
          SELF.Recd_RCM_Q.Group_ID           = SCL_AUD:Line[23]
          SELF.Recd_RCM_Q.RP_Status          = SCL_AUD:Line[24]
          SELF.Recd_RCM_Q.Date_In_Q          = SCL_AUD:Line[25]
          SELF.Recd_RCM_Q.Time_In_Q          = SCL_AUD:Line[26]

          ADD(SELF.Recd_RCM_Q)
          IF ERRORCODE()
             L:Result               = EQU:Err_Q_Add_Read_Recd       ! = -109
             SELF.Set_Last_Error(L:Result)
             BREAK
          ELSE
             L:Result     += 1
       .  .
       CLOSE(Audit_File)

       IF p:Delete = TRUE
          REMOVE(Audit_File)
    .  .

    RETURN(L:Result)



SMSMeNet_Class.ReadIn_Recd_MIDs        PROCEDURE(<STRING p:File_Name>, BYTE p:Delete)
Audit_File      FILE,DRIVER('BASIC'),CREATE,PRE(SCL_AUD)
Rec               RECORD
Line                STRING(500),DIM(10)
                . .

L:IDx           LONG
L:Result        LONG
L:File_Name     STRING(255)

    CODE
    L:File_Name   = p:File_Name
    IF OMITTED(2)
       L:File_Name   = 'SMSMeNet_Recd_MIDs_Unprocessed.csv'
    .
    IF SELF.Open_Dump_File(Audit_File, L:File_Name, 0) = 1              ! 0 = No create
       SET(Audit_File)
       LOOP
          L:IDx        += 1
          NEXT(Audit_File)
          IF ERRORCODE()
             BREAK
          .
          IF SCL_AUD:Line[1][1 : 13] = 'HDR  --------'
             CYCLE
          .

          CLEAR(SELF.Recd_MID_Q)

          SELF.Recd_MID_Q.Client_Sent_ID     = SCL_AUD:Line[1]
          SELF.Recd_MID_Q.Sent_Reference     = SCL_AUD:Line[2]
          SELF.Recd_MID_Q.Status             = SCL_AUD:Line[3]
          SELF.Recd_MID_Q.Delivered_Date     = SCL_AUD:Line[4]
          SELF.Recd_MID_Q.Delivered_Time     = SCL_AUD:Line[5]
          SELF.Recd_MID_Q.Date               = SCL_AUD:Line[6]
          SELF.Recd_MID_Q.Time               = SCL_AUD:Line[7]
          SELF.Recd_MID_Q.Last_Error_Code    = SCL_AUD:Line[8]
          SELF.Recd_MID_Q.Date_In_Q          = SCL_AUD:Line[9]
          SELF.Recd_MID_Q.Time_In_Q          = SCL_AUD:Line[10]

          ADD(SELF.Recd_MID_Q)
          IF ERRORCODE()
             L:Result               = EQU:Err_Q_Add_Read_RMIDs       ! = -110
             SELF.Set_Last_Error(L:Result)
             BREAK
          ELSE
             L:Result   += 1
       .  .
       CLOSE(Audit_File)
       IF p:Delete = TRUE
          REMOVE(Audit_File)
    .  .

    RETURN(L:Result)



! ---------   SMS Me Recd. - Dump_Received_Msgs / Dump_Recd_MIDs / Read_Received_Msg / Read_Recd_MIDs   ---------
SMSMeNet_Class.Read_Received_Msg_G      PROCEDURE(<ULONG p:Pos>, BYTE p:Delete)             ! Delete is default on
L_Recd_Fields       GROUP(GL_Cl_RCM_Q)
                    .
L_ErrorCode         LONG

    CODE
    IF OMITTED(2) OR p:Pos = 0
       p:Pos    = 1
    .

    L_Recd_Fields.Success       = FALSE
    !L_Recd_Fields.Client_CID    = 0                ! taken out of Q
    L_Recd_Fields.Server_Rec_ID = 0                ! Init this fields - can check for no message using these

    GET(SELF.Recd_RCM_Q, p:Pos)
    L_ErrorCode                 = ERRORCODE()

!    db.debugout('[SMSMeNet_Class]  Recd Q Recs: ' & RECORDS(SELF.Recd_RCM_Q) & ',  Pos: ' & p:Pos & ',  Error: ' & ERROR())
    IF RECORDS(SELF.Recd_RCM_Q) >= p:Pos AND L_ErrorCode
       SELF.Add_Audit('Read_Received_Msg_G', '<> Error (' & ERRORCODE() & '): ' & ERROR(), 'Recd Q Recs: ' & RECORDS(SELF.Recd_RCM_Q) & ',  Pos: ' & p:Pos, 1, '[SMSMeNetClient]')
       SELF.Add_Audit('Read_Received_Msg_G', 'SELF.Recd_RCM_Q.User_Login: ' & SELF.Recd_RCM_Q.User_Login, '', 1, '[SMSMeNetClient]')
    .

    IF ~L_ErrorCode          !~ERRORCODE()
       L_Recd_Fields.Success                = TRUE

       !L_Recd_Fields.Client_CID             = SELF.Recd_RCM_Q.Client_CID       ! taken out of Q
       L_Recd_Fields.User_Login             = SELF.Recd_RCM_Q.User_Login
       L_Recd_Fields.Server_Rec_ID          = SELF.Recd_RCM_Q.Server_Rec_ID
       L_Recd_Fields.Read_Status            = SELF.Recd_RCM_Q.Read_Status
       L_Recd_Fields.Originator_Address     = SELF.Recd_RCM_Q.Originator_Address
       L_Recd_Fields.Data                   = SELF.Recd_RCM_Q.Data
       L_Recd_Fields.Service_Time_Stamp     = SELF.Recd_RCM_Q.Service_Time_Stamp
       L_Recd_Fields.Date                   = SELF.Recd_RCM_Q.Date
       L_Recd_Fields.Time                   = SELF.Recd_RCM_Q.Time
       L_Recd_Fields.Time_Zone              = SELF.Recd_RCM_Q.Time_Zone
       L_Recd_Fields.Message                = SELF.Recd_RCM_Q.Message
       L_Recd_Fields.Address_No             = SELF.Recd_RCM_Q.Address_No
       L_Recd_Fields.SMSC_Address           = SELF.Recd_RCM_Q.SMSC_Address
       L_Recd_Fields.Msg_No                 = SELF.Recd_RCM_Q.Msg_No
       L_Recd_Fields.Total_Msgs             = SELF.Recd_RCM_Q.Total_Msgs
       L_Recd_Fields.Type                   = SELF.Recd_RCM_Q.Type
       L_Recd_Fields.TA_Date                = SELF.Recd_RCM_Q.TA_Date
       L_Recd_Fields.TA_Time                = SELF.Recd_RCM_Q.TA_Time
       L_Recd_Fields.Received_Date          = SELF.Recd_RCM_Q.Received_Date
       L_Recd_Fields.Received_Time          = SELF.Recd_RCM_Q.Received_Time
       L_Recd_Fields.Read                   = SELF.Recd_RCM_Q.Read
       L_Recd_Fields.User_ID_Received_For   = SELF.Recd_RCM_Q.User_ID_Received_For
       L_Recd_Fields.User_ID_Private_To     = SELF.Recd_RCM_Q.User_ID_Private_To
       L_Recd_Fields.Group_ID               = SELF.Recd_RCM_Q.Group_ID
       L_Recd_Fields.RP_Status              = SELF.Recd_RCM_Q.RP_Status

       IF p:Delete = TRUE
          DELETE(SELF.Recd_RCM_Q)
    .  .

    RETURN( L_Recd_Fields )


SMSMeNet_Class.Read_Received_Msg        PROCEDURE(<ULONG p:Pos>, BYTE p:Delete)             ! Delete is default on
L_Recd_Fields       GL_Cl_RCM_Class

    CODE
    IF OMITTED(2) OR p:Pos = 0
       p:Pos    = 1
    .

    L_Recd_Fields.Success       = FALSE
    !L_Recd_Fields.Client_CID    = 0                 ! Init these 2 fields - can check for no message using these
    L_Recd_Fields.Server_Rec_ID = 0

    GET(SELF.Recd_RCM_Q, p:Pos)
    IF ~ERRORCODE()
       L_Recd_Fields.Success                = TRUE
       !L_Recd_Fields.Client_CID             = SELF.Recd_RCM_Q.Client_CID       ! taken out of Q
       L_Recd_Fields.User_Login             = SELF.Recd_RCM_Q.User_Login
       L_Recd_Fields.Server_Rec_ID          = SELF.Recd_RCM_Q.Server_Rec_ID
       L_Recd_Fields.Read_Status            = SELF.Recd_RCM_Q.Read_Status
       L_Recd_Fields.Originator_Address     = SELF.Recd_RCM_Q.Originator_Address
       L_Recd_Fields.Data                   = SELF.Recd_RCM_Q.Data
       L_Recd_Fields.Service_Time_Stamp     = SELF.Recd_RCM_Q.Service_Time_Stamp
       L_Recd_Fields.Date                   = SELF.Recd_RCM_Q.Date
       L_Recd_Fields.Time                   = SELF.Recd_RCM_Q.Time
       L_Recd_Fields.Time_Zone              = SELF.Recd_RCM_Q.Time_Zone
       L_Recd_Fields.Message                = SELF.Recd_RCM_Q.Message
       L_Recd_Fields.Address_No             = SELF.Recd_RCM_Q.Address_No
       L_Recd_Fields.SMSC_Address           = SELF.Recd_RCM_Q.SMSC_Address
       L_Recd_Fields.Msg_No                 = SELF.Recd_RCM_Q.Msg_No
       L_Recd_Fields.Total_Msgs             = SELF.Recd_RCM_Q.Total_Msgs
       L_Recd_Fields.Type                   = SELF.Recd_RCM_Q.Type
       L_Recd_Fields.TA_Date                = SELF.Recd_RCM_Q.TA_Date
       L_Recd_Fields.TA_Time                = SELF.Recd_RCM_Q.TA_Time
       L_Recd_Fields.Received_Date          = SELF.Recd_RCM_Q.Received_Date
       L_Recd_Fields.Received_Time          = SELF.Recd_RCM_Q.Received_Time
       L_Recd_Fields.Read                   = SELF.Recd_RCM_Q.Read
       L_Recd_Fields.User_ID_Received_For   = SELF.Recd_RCM_Q.User_ID_Received_For
       L_Recd_Fields.User_ID_Private_To     = SELF.Recd_RCM_Q.User_ID_Private_To
       L_Recd_Fields.Group_ID               = SELF.Recd_RCM_Q.Group_ID
       L_Recd_Fields.RP_Status              = SELF.Recd_RCM_Q.RP_Status

       IF p:Delete = TRUE
          DELETE(SELF.Recd_RCM_Q)
    .  .

    !message('in 4 - L_Recd_Fields.Success: ' & L_Recd_Fields.Success)

    RETURN( L_Recd_Fields )



SMSMeNet_Class.Read_Recd_MIDs_G      PROCEDURE(<ULONG p:Pos>, BYTE p:Delete)
L_Recd_MID_Fields       GROUP(GL_Cl_MID_Q)
                        .

    CODE
    IF OMITTED(2) OR p:Pos = 0
       p:Pos    = 1
    .

    GET(SELF.Recd_MID_Q, p:Pos)
    IF ~ERRORCODE()
       L_Recd_MID_Fields.Success            = TRUE
       L_Recd_MID_Fields.Client_Sent_ID     = SELF.Recd_MID_Q.Client_Sent_ID
       L_Recd_MID_Fields.Sent_Reference     = SELF.Recd_MID_Q.Sent_Reference
       L_Recd_MID_Fields.Status             = SELF.Recd_MID_Q.Status
       L_Recd_MID_Fields.Delivered_Date     = SELF.Recd_MID_Q.Delivered_Date
       L_Recd_MID_Fields.Delivered_Time     = SELF.Recd_MID_Q.Delivered_Time
       L_Recd_MID_Fields.Date               = SELF.Recd_MID_Q.Date
       L_Recd_MID_Fields.Time               = SELF.Recd_MID_Q.Time
       L_Recd_MID_Fields.Last_Error_Code    = SELF.Recd_MID_Q.Last_Error_Code

       IF p:Delete = TRUE
          DELETE(SELF.Recd_MID_Q)
    .  .

    RETURN( L_Recd_MID_Fields )


SMSMeNet_Class.Read_Recd_MIDs        PROCEDURE(<ULONG p:Pos>, BYTE p:Delete)
L_Recd_MID_Fields       GL_Cl_MID_Class

    CODE
    IF OMITTED(2) OR p:Pos = 0
       p:Pos    = 1
    .

    GET(SELF.Recd_MID_Q, p:Pos)
    IF ~ERRORCODE()
       L_Recd_MID_Fields.Success            = TRUE
       L_Recd_MID_Fields.Client_Sent_ID     = SELF.Recd_MID_Q.Client_Sent_ID
       L_Recd_MID_Fields.Sent_Reference     = SELF.Recd_MID_Q.Sent_Reference
       L_Recd_MID_Fields.Status             = SELF.Recd_MID_Q.Status
       L_Recd_MID_Fields.Delivered_Date     = SELF.Recd_MID_Q.Delivered_Date
       L_Recd_MID_Fields.Delivered_Time     = SELF.Recd_MID_Q.Delivered_Time
       L_Recd_MID_Fields.Date               = SELF.Recd_MID_Q.Date
       L_Recd_MID_Fields.Time               = SELF.Recd_MID_Q.Time
       L_Recd_MID_Fields.Last_Error_Code    = SELF.Recd_MID_Q.Last_Error_Code

       IF p:Delete = TRUE
          DELETE(SELF.Recd_MID_Q)
    .  .

    RETURN(L_Recd_MID_Fields)



SMSMeNet_Class.Dump_Received_Msgs       PROCEDURE(<STRING p:File_Name>, BYTE p:Delete, BYTE p:Expire_Mode)
Audit_File      FILE,DRIVER('BASIC'),CREATE,PRE(SCL_AUD)
Rec               RECORD
Line                STRING(500),DIM(27)
                . .

L:IDx           LONG
L:File_Name     STRING(255)
L:Result        LONG

    CODE
    IF RECORDS(SELF.Recd_RCM_Q) > 0
       L:File_Name     = p:File_Name
       IF OMITTED(2)
          L:File_Name  = 'SMSMeNet_Received_Msgs_Unprocessed.csv'

          IF p:Expire_Mode >= 2             ! Then we have no name and are processing for expired only
             L:File_Name  = 'SMSMeNet_Received_Msgs_Unprocessed-Exp ' & FORMAT(TODAY(),@d8) & ' - ' & FORMAT(CLOCK(),@t4-) & '.csv'
       .  .


       L:Result     = SELF.Open_Dump_File(Audit_File, L:File_Name)
       IF L:Result > 0
          IF L:Result = 2                               ! File was created
             DO Table_Header
          .
          L:Result  = 0

          SCL_AUD:Line[1]  = 'HDR  --------  Dump Received Unprocessed @ ' & FORMAT(TODAY(),@d6) & ', ' & FORMAT(CLOCK(), @t4) & '  --------'
          ADD(Audit_File)

          L:IDx     = 1
          IF p:Expire_Mode = 1                    ! Processing for expired - check number
             IF RECORDS(SELF.Recd_RCM_Q) > SELF.Persist_Q_Trans
                L:IDx   = RECORDS(SELF.Recd_RCM_Q) - SELF.Persist_Q_Trans + 1       ! 120 - 100 + 1 = 21
          .  .
          LOOP
             !L:IDx += 1
             GET(SELF.Recd_RCM_Q, L:IDx)
             IF ERRORCODE()
                BREAK
             .

             IF p:Expire_Mode > 0 AND p:Expire_Mode ~= 2                            ! 2 is dump all as expired
                IF TODAY() - SELF.Recd_RCM_Q.Date_In_Q > SELF.Persist_Q_Days          ! Expired 
                   IF p:Expire_Mode = 1           ! Then exclude expired from the dump - leaving in Q
                      L:IDx    += 1
                      CYCLE
                   .
                ELSIF p:Expire_Mode = 3           ! Include only expired in dump - delete from Q
                   ! Not expired but processing only for expired - so cycle
                   CYCLE
             .  .

             !SCL_AUD:Line[1]    = CLIP(SELF.Recd_RCM_Q.Client_CID)     ! taken out of Q
             SCL_AUD:Line[1]    = CLIP(SELF.Recd_RCM_Q.User_Login)
             SCL_AUD:Line[2]    = CLIP(SELF.Recd_RCM_Q.Server_Rec_ID)
             SCL_AUD:Line[3]    = CLIP(SELF.Recd_RCM_Q.Read_Status)
             SCL_AUD:Line[4]    = CLIP(SELF.Recd_RCM_Q.Originator_Address)
             SCL_AUD:Line[5]    = CLIP(SELF.Recd_RCM_Q.Data)
             SCL_AUD:Line[6]    = CLIP(SELF.Recd_RCM_Q.Service_Time_Stamp)
             SCL_AUD:Line[7]    = CLIP(SELF.Recd_RCM_Q.Date)
             SCL_AUD:Line[8]    = CLIP(SELF.Recd_RCM_Q.Time)
             SCL_AUD:Line[9]    = CLIP(SELF.Recd_RCM_Q.Time_Zone)
             SCL_AUD:Line[10]   = CLIP(SELF.Recd_RCM_Q.Message)
             SCL_AUD:Line[11]   = CLIP(SELF.Recd_RCM_Q.Address_No)
             SCL_AUD:Line[12]   = CLIP(SELF.Recd_RCM_Q.SMSC_Address)
             SCL_AUD:Line[13]   = CLIP(SELF.Recd_RCM_Q.Msg_No)
             SCL_AUD:Line[14]   = CLIP(SELF.Recd_RCM_Q.Total_Msgs)
             SCL_AUD:Line[15]   = CLIP(SELF.Recd_RCM_Q.Type)
             SCL_AUD:Line[16]   = CLIP(SELF.Recd_RCM_Q.TA_Date)
             SCL_AUD:Line[17]   = CLIP(SELF.Recd_RCM_Q.TA_Time)
             SCL_AUD:Line[18]   = CLIP(SELF.Recd_RCM_Q.Received_Date)
             SCL_AUD:Line[19]   = CLIP(SELF.Recd_RCM_Q.Received_Time)
             SCL_AUD:Line[20]   = CLIP(SELF.Recd_RCM_Q.Read)
             SCL_AUD:Line[21]   = CLIP(SELF.Recd_RCM_Q.User_ID_Received_For)
             SCL_AUD:Line[22]   = CLIP(SELF.Recd_RCM_Q.User_ID_Private_To)
             SCL_AUD:Line[23]   = CLIP(SELF.Recd_RCM_Q.Group_ID)
             SCL_AUD:Line[24]   = CLIP(SELF.Recd_RCM_Q.RP_Status)
             SCL_AUD:Line[25]   = CLIP(SELF.Recd_RCM_Q.Date_In_Q)
             SCL_AUD:Line[26]   = CLIP(SELF.Recd_RCM_Q.Time_In_Q)

             ADD(Audit_File)

             IF ERRORCODE()
                L:Result               = EQU:Err_Add_Dump_Recd          ! = -111
                SELF.Set_Last_Error(L:Result)
                BREAK
             ELSE
                IF p:Delete = TRUE
                   DELETE(SELF.Recd_RCM_Q)
                .
                L:Result     += 1
          .  .
          CLOSE(Audit_File)
          IF L:Result = 0
             REMOVE(Audit_File)
    .  .  .

    RETURN(L:Result)

Table_Header            ROUTINE
    SCL_AUD:Line[1]    = 'HDR  --------  Client ID'
    SCL_AUD:Line[2]    = 'User_Login'
    SCL_AUD:Line[3]    = 'Server_Rec_ID'
    SCL_AUD:Line[4]    = 'Read_Status'
    SCL_AUD:Line[5]    = 'Originator_Address'
    SCL_AUD:Line[6]    = 'Data'
    SCL_AUD:Line[7]    = 'Service_Time_Stamp'
    SCL_AUD:Line[8]    = 'Date'
    SCL_AUD:Line[9]    = 'Time'
    SCL_AUD:Line[10]   = 'Time_Zone'
    SCL_AUD:Line[11]   = 'Message'
    SCL_AUD:Line[12]   = 'Address_No'
    SCL_AUD:Line[13]   = 'SMSC_Address'
    SCL_AUD:Line[14]   = 'Msg_No'
    SCL_AUD:Line[15]   = 'Total_Msgs'
    SCL_AUD:Line[16]   = 'Type'
    SCL_AUD:Line[17]   = 'TA_Date'
    SCL_AUD:Line[18]   = 'TA_Time'
    SCL_AUD:Line[19]   = 'Received_Date'
    SCL_AUD:Line[20]   = 'Received_Time'
    SCL_AUD:Line[21]   = 'Read'
    SCL_AUD:Line[22]   = 'User_ID_Received_For'
    SCL_AUD:Line[23]   = 'User_ID_Private_To'
    SCL_AUD:Line[24]   = 'Group_ID'
    SCL_AUD:Line[25]   = 'RP_Status'
    SCL_AUD:Line[26]   = 'Date_In_Q'
    SCL_AUD:Line[27]   = 'Time_In_Q'
    ADD(Audit_File)
    EXIT



SMSMeNet_Class.Dump_Recd_MIDs          PROCEDURE(<STRING p:File_Name>, BYTE p:Delete, BYTE p:Expire_Mode)
Audit_File      FILE,DRIVER('BASIC'),CREATE,PRE(SCL_AUD)
Rec               RECORD
Line                STRING(500),DIM(10)
                . .

L:IDx           LONG
L:Result        LONG
L:File_Name     STRING(255)

    CODE
    IF RECORDS(SELF.Recd_MID_Q) > 0
       L:File_Name      = p:File_Name
       IF OMITTED(2)
          L:File_Name   = 'SMSMeNet_Recd_MIDs_Unprocessed.csv'

          IF p:Expire_Mode >= 2             ! Then we have no name and are processing for expired only
             L:File_Name  = 'SMSMeNet_Recd_MIDs_Unprocessed-Exp ' & FORMAT(TODAY(),@d8) & ' - ' & FORMAT(CLOCK(),@t4-) & '.csv'
       .  .
       L:Result     = SELF.Open_Dump_File(Audit_File, L:File_Name)
       IF L:Result > 0
          IF L:Result = 2                               ! File was created
             DO Table_Header
          .
          L:Result  = 0

          SCL_AUD:Line[1]  = 'HDR  --------  Dump Recd. MIDs Unprocessed @ ' & FORMAT(TODAY(),@d6) & ', ' & FORMAT(CLOCK(), @t4) & '  --------'
          ADD(Audit_File)

          L:IDx     = 1
          IF p:Expire_Mode = 1                    ! Processing for expired - check number
             IF RECORDS(SELF.Recd_MID_Q) > SELF.Persist_Q_Trans
                L:IDx   = RECORDS(SELF.Recd_MID_Q) - SELF.Persist_Q_Trans + 1       ! 120 - 100 + 1 = 21
          .  .
          LOOP
             GET(SELF.Recd_MID_Q, L:IDx)
             IF ERRORCODE()
                BREAK
             .

             IF p:Expire_Mode > 0 AND p:Expire_Mode ~= 2                            ! 2 is dump all as expired
                IF TODAY() - SELF.Recd_MID_Q.Date_In_Q > SELF.Persist_Q_Days          ! Expired
                   IF p:Expire_Mode = 1           ! Then exclude expired from the dump - leaving in Q
                      L:IDx    += 1
                      CYCLE
                   .
                ELSIF p:Expire_Mode = 3           ! Include only expired in dump - delete from Q
                   CYCLE                          ! Not expired but processing only for expired - so cycle
             .  .

             SCL_AUD:Line[1]    = CLIP(SELF.Recd_MID_Q.Client_Sent_ID)
             SCL_AUD:Line[2]    = CLIP(SELF.Recd_MID_Q.Sent_Reference)
             SCL_AUD:Line[3]    = CLIP(SELF.Recd_MID_Q.Status)
             SCL_AUD:Line[4]    = CLIP(SELF.Recd_MID_Q.Delivered_Date)
             SCL_AUD:Line[5]    = CLIP(SELF.Recd_MID_Q.Delivered_Time)
             SCL_AUD:Line[6]    = CLIP(SELF.Recd_MID_Q.Date)
             SCL_AUD:Line[7]    = CLIP(SELF.Recd_MID_Q.Time)
             SCL_AUD:Line[8]    = CLIP(SELF.Recd_MID_Q.Last_Error_Code)
             SCL_AUD:Line[9]    = CLIP(SELF.Recd_MID_Q.Date_In_Q)
             SCL_AUD:Line[10]   = CLIP(SELF.Recd_MID_Q.Time_In_Q)

             ADD(Audit_File)
             IF ERRORCODE()
                L:Result               = EQU:Err_Add_Dump_RMIDs          ! = -112
                SELF.Set_Last_Error(L:Result)
                BREAK
             ELSE
                IF p:Delete = TRUE
                   DELETE(SELF.Recd_MID_Q)
                .
                L:Result     += 1
          .  .
          CLOSE(Audit_File)
          IF L:Result = 0
             REMOVE(Audit_File)
    .  .  .

    RETURN(L:Result)

Table_Header            ROUTINE
    SCL_AUD:Line[1]    = 'HDR  --------  Client_Sent_ID'
    SCL_AUD:Line[2]    = 'Sent_Reference'
    SCL_AUD:Line[3]    = 'Status'
    SCL_AUD:Line[4]    = 'Delivered_Date'
    SCL_AUD:Line[5]    = 'Delivered_Time'
    SCL_AUD:Line[6]    = 'Date'
    SCL_AUD:Line[7]    = 'Time'
    SCL_AUD:Line[8]    = 'Last_Error_Code'
    SCL_AUD:Line[9]    = 'Date_In_Q'
    SCL_AUD:Line[10]   = 'Time_In_Q'
    ADD(Audit_File)
    EXIT

! ---------   SMS Me Recd. Process - Process_RCM / Process_MID / Process_RPU   ---------
SMSMeNet_Class.Process_RCM             PROCEDURE(GL_Cl_Fields_Q p_Field_Q)
L:Idx               LONG
L:Not_Processed     BYTE
L:Result            LONG

    CODE
    p_Field_Q.Field     = 'RCM'             ! Recd. msg from server for this client, element is CID if not blank
    GET(p_Field_Q, +p_Field_Q.Field)
    IF ~ERRORCODE()
       CLEAR(SELF.Recd_RCM_Q)
       !SELF.Recd_RCM_Q.Client_CID       = p_Field_Q.Element             ! taken out of Q

       DELETE(p_Field_Q)                    ! Remove field entry

       LOOP L:Idx = RECORDS(p_Field_Q) TO 1 BY -1
          GET(p_Field_Q, L:Idx)
          IF ERRORCODE()
             BREAK
          .

          L:Not_Processed = FALSE

          CASE p_Field_Q.Field
          OF 'RMU'                                                !     - User Login
             SELF.Recd_RCM_Q.User_Login               = p_Field_Q.Element
             ! If we have been proivided with a User Login, Check if we know of a General send as User ID??
          OF 'RID'                                                ! - Server Rec. Message ID
             SELF.Recd_RCM_Q.Server_Rec_ID            = p_Field_Q.Element

             IF SELF.Recd_RCM_Q.Server_Rec_ID = 0
                SELF.Recd_RCM_Q.Server_Rec_ID   = -1              ! Default - max ULONG value
             .
          OF 'RRS'                                                !     - REC:Read_Status
             SELF.Recd_RCM_Q.Read_Status              = p_Field_Q.Element
          OF 'ROA'                                                ! - REC:Originator_Address
             SELF.Recd_RCM_Q.Originator_Address       = p_Field_Q.Element
          OF 'DAT'                                                ! - REC:Data
             SELF.Recd_RCM_Q.Data                     = p_Field_Q.Element
          OF 'RSS'                                                ! - REC:Service_Time_Stamp
             SELF.Recd_RCM_Q.Service_Time_Stamp       = p_Field_Q.Element
          OF 'RDA'                                                ! - REC:Date
             SELF.Recd_RCM_Q.Date                     = p_Field_Q.Element
          OF 'RTI'                                                ! - REC:Time
             SELF.Recd_RCM_Q.Time                     = p_Field_Q.Element
          OF 'RTZ'                                                ! - REC:Time_Zone
             SELF.Recd_RCM_Q.Time_Zone                = p_Field_Q.Element
          OF 'RME'                                                ! - REC:Message
             SELF.Recd_RCM_Q.Message                  = p_Field_Q.Element
          OF 'RAN'                                                ! - REC:Address_No
             SELF.Recd_RCM_Q.Address_No               = p_Field_Q.Element
          OF 'RSA'                                                ! - REC:SMSC_Address
             SELF.Recd_RCM_Q.SMSC_Address             = p_Field_Q.Element
          OF 'RMN'                                                ! - REC:Msg_No
             SELF.Recd_RCM_Q.Msg_No                   = p_Field_Q.Element
          OF 'RTM'                                                ! - REC:Total_Msgs
             SELF.Recd_RCM_Q.Total_Msgs               = p_Field_Q.Element
          OF 'RTY'                                                !     - REC:Type
             SELF.Recd_RCM_Q.Type                     = p_Field_Q.Element
          OF 'RTD'                                                ! - REC:TA_Date
             SELF.Recd_RCM_Q.TA_Date                  = p_Field_Q.Element
          OF 'RTT'                                                ! - REC:TA_Time
             SELF.Recd_RCM_Q.TA_Time                  = p_Field_Q.Element
          OF 'RRD'                                                ! - REC:Received_Date
             SELF.Recd_RCM_Q.Received_Date            = p_Field_Q.Element
          OF 'RRT'                                                ! - REC:Received_Time
             SELF.Recd_RCM_Q.Received_Time            = p_Field_Q.Element

          ! These will never be assigned from the server for SMS Me clients.  See above.
          OF 'RED'                                                !     - REC:Read
             SELF.Recd_RCM_Q.Read                     = p_Field_Q.Element
          OF 'RRF'                                                !     - REC:User_ID_Received_For
             SELF.Recd_RCM_Q.User_ID_Received_For     = p_Field_Q.Element
          OF 'RPT'                                                !     - REC:User_ID_Private_To
             SELF.Recd_RCM_Q.User_ID_Private_To       = p_Field_Q.Element
          OF 'RGI'                                                !     - REC:Group_ID
             SELF.Recd_RCM_Q.Group_ID                 = p_Field_Q.Element
          OF 'RST'                                                !     - REC:RP_Status
             SELF.Recd_RCM_Q.RP_Status                = p_Field_Q.Element
          ELSE
             L:Not_Processed   = TRUE
          .

          IF L:Not_Processed = FALSE                               ! Keep all un-processed fields - will dump later
             DELETE(p_Field_Q)
       .  .

       L:Result                     = SELF.Recd_RCM_Q.Server_Rec_ID

       SELF.Recd_RCM_Q.Date_In_Q    = TODAY()
       SELF.Recd_RCM_Q.Time_In_Q    = CLOCK()

       ADD(SELF.Recd_RCM_Q)
            
       SELF.Recd_Msg_Ack_Send(SELF.Recd_RCM_Q.Server_Rec_ID)

       SELF.Trigger_Events()
    .
    RETURN(L:Result)



SMSMeNet_Class.Process_MID             PROCEDURE(GL_Cl_Fields_Q p_Field_Q)            ! Sent msg. server ID
L:Idx               LONG
L:Not_Processed     BYTE
L:Result            LONG
L:Delete_Out        BYTE

    CODE
    ! This will never be sent by itself - always followed by 1 of next 3
    p_Field_Q.Field       = 'MID'
    GET(p_Field_Q, +p_Field_Q.Field)
    IF ~ERRORCODE()
       CLEAR(SELF.Recd_MID_Q)

       SELF.Recd_MID_Q.Client_Sent_ID   = DEFORMAT(p_Field_Q.Element)
       L:Result                         = SELF.Recd_MID_Q.Client_Sent_ID

       DELETE(p_Field_Q)

       LOOP L:Idx = RECORDS(p_Field_Q) TO 1 BY -1
          GET(p_Field_Q, L:Idx)
          IF ERRORCODE()
             BREAK
          .

          L:Not_Processed = FALSE

          CASE p_Field_Q.Field
          OF 'SID'                                              ! This tells us message added to server database
             SELF.Recd_MID_Q.Sent_Reference     = DEFORMAT(p_Field_Q.Element)
          OF 'MSS'
             SELF.Recd_MID_Q.Status             = DEFORMAT(p_Field_Q.Element)
          OF 'DNO'                                              ! Here we are to expect: dd/MM/yy,hh:MM:ss
             SELF.Recd_MID_Q.Delivered_Date     = DEFORMAT(Get_1st_Element_From_Delim_Str(p_Field_Q.Element, ',', TRUE), @d5)
             SELF.Recd_MID_Q.Delivered_Time     = DEFORMAT(Get_1st_Element_From_Delim_Str(p_Field_Q.Element, ',', TRUE), @t4)
          OF 'MSD'                                              ! Message sent Date
             SELF.Recd_MID_Q.Date               = DEFORMAT(p_Field_Q.Element)
          OF 'MST'                                              ! Message sent Time
             SELF.Recd_MID_Q.Time               = DEFORMAT(p_Field_Q.Element)
          OF 'ERR'
             SELF.Recd_MID_Q.Last_Error_Code    = DEFORMAT(p_Field_Q.Element)
          ELSE
             L:Not_Processed        = TRUE
          .

          IF L:Not_Processed = FALSE                            ! Keep all un-processed fields - will dump later
             DELETE(p_Field_Q)
       .  .

       SELF.Recd_MID_Q.Date_In_Q    = TODAY()
       SELF.Recd_MID_Q.Time_In_Q    = CLOCK()

       ADD(SELF.Recd_MID_Q)

       IF SELF.Recd_MID_Q.Last_Error_Code = 34                  ! 34 = Msg send failed - msg. ref. exists for client ID
          ! We should not try send this msg again - remove it from out out Q?
          SELF.Add_Audit('Sent_Msg_Ack', 'Received MID response with error code 34 - msg. ref. exists on server.', 'Sent ID: ' & SELF.Recd_MID_Q.Client_Sent_ID & ',  will be removed from OutBox', 1, 'Sent_Msg_Ack')

          L:Delete_Out  = TRUE
       .

       ! This method will delete the outbox item if there is a Sent_Ref  OR  Delete_Out is set.
       SELF.Sent_Msg_Ack(SELF.Recd_MID_Q.Client_Sent_ID, SELF.Recd_MID_Q.Sent_Reference, L:Delete_Out)
       SELF.Trigger_Events()
    .

    RETURN(L:Result)



SMSMeNet_Class.Process_RPU             PROCEDURE(GL_Cl_Fields_Q p_Field_Q)            ! Sent Push users
L:Count_Users_Tot   LONG

    CODE
    IF ~OMITTED(2)
       p_Field_Q.Field       = 'RPU'
       GET(p_Field_Q, +p_Field_Q.Field)
       IF ~ERRORCODE()
          DELETE(p_Field_Q)                    ! Remove field entry
    .  .

    L:Count_Users_Tot   = SELF.Send_Push_Users()

    RETURN(L:Count_Users_Tot)


! ---------   User Functions - Check_Sent_Msg (M) / Trigger_Ack_Events / Add_Ack_Trigger_Events / Read_Expired_Msg   ---------
SMSMeNet_Class.Read_Expired_Msg         PROCEDURE(<ULONG p:Pos>, BYTE p:Delete)
L:Expired_Msg     GL_Cl_Msg_Exp_Class
    CODE
    IF OMITTED(2) OR p:Pos = 0
       p:Pos    = 1
    .

    GET(SELF.Send_Msgs_Exp_Q, p:Pos)
    IF ~ERRORCODE()
       L:Expired_Msg.ID         = SELF.Send_Msgs_Exp_Q.ID
       L:Expired_Msg.Date_In    = SELF.Send_Msgs_Exp_Q.Date_In
       L:Expired_Msg.Type       = SELF.Send_Msgs_Exp_Q.Type

       IF p:Delete = TRUE
          DELETE(SELF.Send_Msgs_Exp_Q)
    .  .

    RETURN(L:Expired_Msg)



SMSMeNet_Class.Add_Trigger_Event    PROCEDURE(LONG p:Event, LONG p:Thread=0, BYTE p:Event_Type=0, BYTE p:Trigger_Now=0)
    CODE
    SELF.Trigger_Events_Q.Event_Trigger     = p:Event
    SELF.Trigger_Events_Q.Event_Thread      = p:Thread
    SELF.Trigger_Events_Q.Event_Type        = p:Event_Type
    GET(SELF.Trigger_Events_Q, SELF.Trigger_Events_Q.Event_Trigger, SELF.Trigger_Events_Q.Event_Thread, SELF.Trigger_Events_Q.Event_Type)
    IF ERRORCODE()                                                          ! We don't have this event then
       GET(SELF.Trigger_Events_Q, RECORDS(SELF.Trigger_Events_Q))
       IF ERRORCODE()
          CLEAR(SELF.Trigger_Events_Q)
       .

       SELF.Trigger_Events_Q.Event_ID      += 1
       SELF.Trigger_Events_Q.Event_Trigger  = p:Event
       SELF.Trigger_Events_Q.Event_Thread   = p:Thread
       SELF.Trigger_Events_Q.Event_Type     = p:Event_Type
       ADD(SELF.Trigger_Events_Q)
       IF ERRORCODE()
          CLEAR(SELF.Trigger_Events_Q)
    .  .

    IF p:Trigger_Now > 0
       IF RECORDS(SELF.Send_Msgs_Exp_Q) OR RECORDS(SELF.Recd_MID_Q) OR RECORDS(SELF.Recd_RCM_Q)
          ! If we have Expired, Received or Received Acknowledged
          SELF.Trigger_Events(0)
       .
       IF RECORDS(SELF.In_Packets_Q)
          SELF.Trigger_Events(1)
    .  .
    RETURN( SELF.Trigger_Events_Q.Event_ID )



SMSMeNet_Class.Trigger_Events           PROCEDURE(BYTE p:Event_Type=0)
L:Idx   ULONG
    CODE
    LOOP
       L:Idx    += 1
       GET(SELF.Trigger_Events_Q, L:Idx)
       IF ERRORCODE()
          BREAK
       .
       IF SELF.Trigger_Events_Q.Event_Type ~= p:Event_Type
          CYCLE
       .

       IF SELF.Trigger_Events_Q.Event_Trigger > 0
          IF SELF.Trigger_Events_Q.Event_Thread = 0
             POST(SELF.Trigger_Events_Q.Event_Trigger)
          ELSE
             POST(SELF.Trigger_Events_Q.Event_Trigger,,SELF.Trigger_Events_Q.Event_Thread)
    .  .  .
    RETURN



SMSMeNet_Class.Check_Sent_Msg           PROCEDURE(ULONG p:Sent_ID, <*LONG p:Ret_Date_In>, <*BYTE p:Ret_Type>, BYTE p:Remove)
L:Result        LONG
    CODE
    ! Check Out Msgs Q
    IF p:Sent_ID = 0
       IF SELF.Sent_ID = 0
          L:Result  = -2
       ELSE
          p:Sent_ID = SELF.Sent_ID
    .  .

    IF L:Result = 0
       SELF.Out_Msgs_Q.Ack_ID      = p:Sent_ID
       GET(SELF.Out_Msgs_Q, +SELF.Out_Msgs_Q.Ack_ID)
       IF ~ERRORCODE()
          L:Result     = 1
          IF p:Remove = TRUE
             DELETE(SELF.Out_Msgs_Q)
          .
       ELSE
          SELF.Sent_Msgs_Q.Ack_ID   = p:Sent_ID
          GET(SELF.Sent_Msgs_Q, +SELF.Sent_Msgs_Q.Ack_ID)
          IF ~ERRORCODE()
             L:Result  = 2
             IF p:Remove = TRUE
                DELETE(SELF.Sent_Msgs_Q)
             .
          ELSE
             SELF.Send_Msgs_Exp_Q.ID   = p:Sent_ID
             GET(SELF.Send_Msgs_Exp_Q, +SELF.Send_Msgs_Exp_Q.ID)
             IF ~ERRORCODE()
                L:Result   = -1

                p:Ret_Date_In  = SELF.Send_Msgs_Exp_Q.Date_In
                p:Ret_Type     = SELF.Send_Msgs_Exp_Q.Type
                IF p:Remove = TRUE
                   DELETE(SELF.Send_Msgs_Exp_Q)
    .  .  .  .  .

    ! Returns:  1 = out box, 2 = Sent, -1 = expired, 0 = not found, -2 = didn't provide sent ID and no last one found
    RETURN(L:Result)


! ---------   Send Msgs Expired - Add_Msg_Expired / Dump_Out_Msgs_Expired   ---------
SMSMeNet_Class.Dump_Out_Msgs_Expired       PROCEDURE(<STRING p:File_Name>, BYTE p:Delete, BYTE p:Expire_Mode, <GL_Cl_Msg_Exp_Q_Type p_D_Q>, <ULONG p:Persist_Q_Trans>, <LONG p:Persist_Q_Days>)
Audit_File          FILE,DRIVER('BASIC'),CREATE,PRE(SCL_AUD)
Rec                 RECORD
ID                      ULONG
Date_In                 LONG
Type                    BYTE
                    . .

L:IDx               LONG
L:Result            LONG
L:File_Name         STRING(255)
L:Persist_Q_Trans   ULONG
L:Persist_Q_Days    LONG

    CODE
    IF OMITTED(5)
       p_D_Q               &= SELF.Send_Msgs_Exp_Q
    .

    L:Persist_Q_Days        = SELF.Persist_Q_Days
    IF ~OMITTED(7)
       L:Persist_Q_Days     = p:Persist_Q_Days
    .

    IF p:Expire_Mode = 1                          ! Processing for expired - check number
       L:Persist_Q_Trans       = SELF.Persist_Q_Trans
       IF ~OMITTED(6)
          L:Persist_Q_Trans    = p:Persist_Q_Trans
    .  .

    IF RECORDS(p_D_Q) > 0
       L:File_Name   = p:File_Name
       IF OMITTED(2)
          L:File_Name   = 'SMSMeNet_Msgs_Expired_Unprocessed.csv'
          IF p:Expire_Mode >= 2             ! Then we have no name and are processing for expired only
             L:File_Name  = 'SMSMeNet_Msgs_Expired_Unprocessed-Exp ' & FORMAT(TODAY(),@d8) & ' - ' & FORMAT(CLOCK(),@t4-) & '.csv'
       .  .
       L:Result     = SELF.Open_Dump_File(Audit_File, L:File_Name)
       IF L:Result > 0
          IF L:Result = 2                               ! File was created
             !SCL_AUD:ID       = 0
             !SCL_AUD:Msg        = 'HDR  --------  ID, Date_In, Type'
             !ADD(Audit_File)
          .
          L:Result  = 0

          !SCL_AUD:Q_ID  = 0
          !SCL_AUD:Msg   = 'HDR  --------  Dump Msgs Expired Unprocessed @ ' & FORMAT(TODAY(),@d6) & ', ' & FORMAT(CLOCK(), @t4) & '  --------'
          !ADD(Audit_File)

          L:IDx     = 1
          IF p:Expire_Mode = 1                          ! Processing for expired - check number
             IF RECORDS(p_D_Q) > L:Persist_Q_Trans
                L:IDx   = RECORDS(p_D_Q) - L:Persist_Q_Trans + 1        ! 120 - 100 + 1 = 21
          .  .
          LOOP
             GET(p_D_Q, L:IDx)
             IF ERRORCODE()
                BREAK
             .

             IF p:Expire_Mode > 0 AND p:Expire_Mode ~= 2                ! 2 is dump all as expired
                IF TODAY() - p_D_Q.Date_In > L:Persist_Q_Days           ! Expired
                   IF p:Expire_Mode = 1                 ! Then exclude expired from the dump - leaving in Q
                      L:IDx    += 1
                      CYCLE
                   .
                ELSIF p:Expire_Mode = 3                 ! Include only expired in dump - delete from Q
                   CYCLE                                ! Not expired but processing only for expired - so cycle
             .  .

             SCL_AUD:ID   = p_D_Q.ID
             IF SCL_AUD:ID = 0
                SCL_AUD:ID    = 1                       ! 0 means header record
             .
             SCL_AUD:Date_In    = CLIP(p_D_Q.Date_In)
             SCL_AUD:Type       = p_D_Q.Type

             ADD(Audit_File)
             IF ~ERRORCODE()
                IF p:Delete = TRUE
                   DELETE(p_D_Q)
                .
                L:Result     += 1
          .  .
          CLOSE(Audit_File)
          IF L:Result = 0
             REMOVE(Audit_File)
    .  .  .

    RETURN(L:Result)



SMSMeNet_Class.Add_Msg_Expired              PROCEDURE(ULONG p:ID, BYTE p:Type)
    CODE
    SELF.Send_Msgs_Exp_Q.ID         = p:ID
    SELF.Send_Msgs_Exp_Q.Date_In    = TODAY()
    SELF.Send_Msgs_Exp_Q.Type       = p:Type

    ADD(SELF.Send_Msgs_Exp_Q)
    RETURN


! ---------   SMS Me Sent Msgs Ack - Sent_Msgs_Set_Time / Sent_Msgs_Add / Sent_Msg_Ack / Re_Send_Msg   ---------
SMSMeNet_Class.Re_Send_Msg                  PROCEDURE(LONG p:No_To_Send=0, BYTE p:All=0)
L:Idx           ULONG
L:Count         ULONG
    CODE
    CLEAR(SELF.Sent_Msgs_Q)
    L:Idx   = RECORDS(SELF.Sent_Msgs_Q) + 1
    LOOP
       IF p:No_To_Send ~= 0
          IF L:Count >= p:No_To_Send
             BREAK
       .  .

       L:Idx    -= 1
       GET(SELF.Sent_Msgs_Q, L:Idx)
       IF ERRORCODE()
          L:Idx = 0
          BREAK
       .

       IF SELF.Sent_Msgs_Q.Response ~= 0                ! We have had a Response then so don't check for resending
          CYCLE
       .

       ! Check we are not later than our resend window
       IF p:All = TRUE OR (((TODAY() - SELF.Sent_Msgs_Q.Date_Sent) * 8640000) + (CLOCK() - SELF.Sent_Msgs_Q.Time_Sent)) / (100 * 60 * 60)    |
            < SELF.No_Re_Send_Hours

          ! Check we have reached our resend after time
          IF p:All = TRUE OR ((TODAY() - SELF.Sent_Msgs_Q.Date_In_Q) * 8640000 + (CLOCK() - SELF.Sent_Msgs_Q.Time_In_Q)) > SELF.Re_Send_Time_Out
             L:Count   += 1
                                                                                 ! Resend
             SELF.Resend_Sent_Msg(SELF.Sent_Msgs_Q.Message,,FALSE, 1, SELF.Sent_Msgs_Q.Ack_ID, SELF.Sent_Msgs_Q.Date_Sent, SELF.Sent_Msgs_Q.Time_Sent)
             !SELF.Add_Send_Msg_to_Q(SELF.Sent_Msgs_Q.Message,,FALSE, 1, SELF.Sent_Msgs_Q.Ack_ID, SELF.Sent_Msgs_Q.Date_Sent, SELF.Sent_Msgs_Q.Time_Sent)

             SELF.Add_Audit('Re_Send_Msg', 'Message expired in Sent Msgs Q without ack. - resending. Orig Date: ' & FORMAT(SELF.Sent_Msgs_Q.Date_Sent,@d5) & ', ' & FORMAT(SELF.Sent_Msgs_Q.Time_Sent,@t4), SELF.Sent_Msgs_Q.Message, 1, 'Re_Send_Msg')

             DELETE(SELF.Sent_Msgs_Q)
    .  .  .
    RETURN( L:Count )



SMSMeNet_Class.Sent_Msg_Ack                 PROCEDURE(ULONG p:Client_Sent_ID, ULONG p:Sent_Reference, BYTE L:Delete_Sent=0)
L:Idx           ULONG
L:Ret           LONG
    CODE
    ! Note: We are deleting the Sent_Msgs_Q item here if we have been passed a Sent_Reference.
    !       Even though there may be further updates.
    !       Failed final error code msgs remain in the Sent Q.  Must be removed by clients.
    !
    ! Aug 30, 04 - p:Sent_Reference should not be delt with in here?  Or maybe the other code here needs a new proc??
    !            - Also Type needs attention?  Should it be a parameter?

    CLEAR(SELF.Sent_Msgs_Q)
    SELF.Sent_Msgs_Q.Type   = 1
    SELF.Sent_Msgs_Q.Ack_ID = p:Client_Sent_ID

    GET(SELF.Sent_Msgs_Q, +SELF.Sent_Msgs_Q.Type, +SELF.Sent_Msgs_Q.Ack_ID)
    IF ~ERRORCODE() AND SELF.Sent_Msgs_Q.Ack_ID = p:Client_Sent_ID AND SELF.Sent_Msgs_Q.Type = 1
       IF p:Sent_Reference ~= 0 OR L:Delete_Sent = TRUE          ! We have a Reference from the server OR delete requested
          DELETE(SELF.Sent_Msgs_Q)                              ! Sent so Delete
          L:Ret             = 0
       ELSE
          IF SELF.Sent_Msgs_Q.Response < 255                    ! Dont want it looping
             SELF.Sent_Msgs_Q.Response += 1
             PUT(SELF.Sent_Msgs_Q)
             L:Ret                      = 1
       .  .
    ELSE
       ! If we have a Sent_Reference this msg - assume it should be in the sent Q!
       ! In SMSMeNet do all msgs. following initial one have Sent_References?
       ! Otherwise assume that it is ok for it not to be.  Maybe improve this later with an ack field
       ! in the sent Q and relavent expirey code changes and others if required.
       L:Ret                            = -1

       IF p:Sent_Reference ~= 0
          SELF.Add_Audit('Sent_Msg_Ack - Sent Q recs: ' & RECORDS(SELF.Sent_Msgs_Q),  |
                         'Failed to find Sent Msg for acknowledgement.',            |
                         'p:Client_Sent_ID: ' & p:Client_Sent_ID & ',   p:Sent_Reference: ' & p:Sent_Reference,2,'Sent_Msg_Ack')
    .  .


!    ! Not sure why this is a Loop and not a Get
!    CLEAR(SELF.Sent_Msgs_Q)
!    LOOP
!       L:Idx    += 1
!       GET(SELF.Sent_Msgs_Q, L:Idx)
!       IF ERRORCODE()
!          L:Idx = 0
!          BREAK
!       .
!       
!       IF SELF.Sent_Msgs_Q.Type = 1
!          IF SELF.Sent_Msgs_Q.Ack_ID = p:Client_Sent_ID
!             DELETE(SELF.Sent_Msgs_Q)
!             BREAK
!    .  .  .
!
!    IF L:Idx = 0                                ! Nothing found to acknowledge?
    RETURN( L:Ret )



SMSMeNet_Class.Sent_Msgs_Set_Time           PROCEDURE()
L:Idx           ULONG
    CODE
    CLEAR(SELF.Sent_Msgs_Q)
    LOOP
       L:Idx    += 1
       GET(SELF.Sent_Msgs_Q, L:Idx)
       IF ERRORCODE()
          BREAK
       .
               
       SELF.Sent_Msgs_Q.Date_In_Q   = TODAY()
       SELF.Sent_Msgs_Q.Time_In_Q   = CLOCK()
       PUT(SELF.Sent_Msgs_Q)
    .
    RETURN



SMSMeNet_Class.Sent_Msgs_Add                PROCEDURE()
    CODE
    CLEAR(SELF.Sent_Msgs_Q)
    SELF.Sent_Msgs_Q            :=: SELF.Out_Msgs_Q

    SELF.Sent_Msgs_Q.Date_In_Q   = TODAY()
    SELF.Sent_Msgs_Q.Time_In_Q   = CLOCK()

    SELF.Sent_Msgs_Q.Date_Sent   = TODAY()
    SELF.Sent_Msgs_Q.Time_Sent   = CLOCK()
    ADD(SELF.Sent_Msgs_Q)
    RETURN


! ---------   SMS Me Pre-Process - Dump_Licenses_Q / Dump_In_Packets / Dump_Out_Sent_Msgs (M)   ---------
SMSMeNet_Class.Dump_Licenses_Q       PROCEDURE(<STRING p:File_Name>, <GL_Cl_Licenses_Q p_D_Q>, BYTE p:Delete=1)
Audit_File          FILE,DRIVER('BASIC'),CREATE,PRE(SCL_AUD)
Rec                 RECORD
Q_ID                    LONG
Reg_ID                  STRING(50)
Reg_No                  STRING(50)
Date                    LONG
Time                    LONG
                    . .

L:File_Name         STRING(255)
L:Result            LONG
L:IDx               LONG

L:Date_Compare      LONG

    CODE
    ! Licenses_Q - constructor and destructor - load and save

    IF OMITTED(3)
       p_D_Q               &= SELF.Licenses_Q
    .

    IF RECORDS(p_D_Q) > 0
       L:File_Name      = p:File_Name
       IF OMITTED(2)
          L:File_Name   = 'SMSMeNet_Licenses.csv'
       .
       L:Result         = SELF.Open_Dump_File(Audit_File, L:File_Name)
       IF L:Result > 0
          IF L:Result = 2                         ! File was created
             SCL_AUD:Q_ID       = 0
             SCL_AUD:Reg_ID     = 'HDR  --------  Reg ID, Reg No., Date Recd, Time Recd'
             ADD(Audit_File)
          .
          L:Result          = 0

          SCL_AUD:Q_ID      = 0
          SCL_AUD:Reg_ID    = 'HDR  --------  Dump Licensing Recs Unprocessed @ ' & FORMAT(TODAY(),@d6) & ', ' & FORMAT(CLOCK(), @t4) & '  --------'
          ADD(Audit_File)

          L:IDx      = 1
          LOOP
             GET(p_D_Q, L:IDx)
             IF ERRORCODE()
                BREAK
             .

             SCL_AUD:Q_ID       = L:IDx
             SCL_AUD:Reg_ID     = p_D_Q.Client_Reg_ID
             SCL_AUD:Reg_No     = p_D_Q.License_No
             SCL_AUD:Date       = p_D_Q.Received_Date
             SCL_AUD:Time       = p_D_Q.Received_Time

             ADD(Audit_File)
             IF ~ERRORCODE()
                IF p:Delete = TRUE
                   DELETE(p_D_Q)
                ELSE
                   L:IDx    += 1
                .
                L:Result    += 1
             ELSE
                L:IDx       += 1
          .  .
          CLOSE(Audit_File)
          IF L:Result = 0
             REMOVE(Audit_File)
    .  .  .

    RETURN(L:Result)



SMSMeNet_Class.Dump_Out_Sent_Msgs       PROCEDURE(<STRING p:File_Name>, BYTE p:Delete, BYTE p:Expire_Mode,  <GL_Cl_Messages_Q_Type p_D_Q>, <ULONG p:Persist_Q_Trans>, <LONG p:Persist_Q_Days>)
Audit_File          FILE,DRIVER('BASIC'),CREATE,PRE(SCL_AUD)
Rec                 RECORD
Q_ID                    ULONG
Msg                     STRING(EQU:Cl_Sent_Msg_Size)
Date_In_Q               LONG
Time_In_Q               LONG
Type                    BYTE
Ack_ID                  ULONG
Date_Sent               LONG
Time_Sent               LONG
Response                BYTE                            ! Relavent for Sent Q
                    . .

L:IDx               LONG
L:Result            LONG
L:File_Name         STRING(255)
L:Persist_Q_Trans   ULONG
L:Persist_Q_Days    LONG
L:Out_Msgs          BYTE
L:Date_Compare      LONG

    CODE
    ! This is called for Out Going msgs and Sent messages by the destructor
    ! The Expire_Mode is set to 1 when calling to dump un-expired msgs - it leaves expired in the Q.
    !   - Mode 1 also sets the starting point based on the Persist_Q_Trans limits.
    ! Then Expire_Mode is set to 2 to dump all the remaining messages as expired.
    ! Expire_Mode 3 is not used by destruct - it would only dump expired msgs.

    IF OMITTED(5)
       p_D_Q               &= SELF.Out_Msgs_Q
       L:Out_Msgs           = TRUE
    .
    L:Persist_Q_Days        = SELF.Persist_Q_Days
    IF ~OMITTED(7)
       L:Persist_Q_Days     = p:Persist_Q_Days
    .
    IF p:Expire_Mode = 1                          ! Processing for expired - check number
       L:Persist_Q_Trans    = SELF.Persist_Q_Trans
       IF ~OMITTED(6)
          L:Persist_Q_Trans = p:Persist_Q_Trans
    .  .

    IF RECORDS(p_D_Q) > 0
       L:File_Name   = p:File_Name
       IF OMITTED(2)
          L:File_Name   = 'SMSMeNet_Out_Going_Uprocessed.csv'
          IF p:Expire_Mode >= 2                   ! Then we have no name and are processing for expired only
             L:File_Name  = 'SMSMeNet_Out_Going_Uprocessed-Exp ' & FORMAT(TODAY(),@d8) & ' - ' & FORMAT(CLOCK(),@t4-) & '.csv'
       .  .
       L:Result     = SELF.Open_Dump_File(Audit_File, L:File_Name)
       IF L:Result > 0
          IF L:Result = 2                         ! File was created
             SCL_AUD:Q_ID       = 0
             SCL_AUD:Msg        = 'HDR  --------  Q_ID, Msg, Date_In_Q, Time_In_Q, Type, Ack ID, Date Sent, Time Sent'
             ADD(Audit_File)
          .
          L:Result  = 0

          SCL_AUD:Q_ID  = 0
          SCL_AUD:Msg   = 'HDR  --------  Dump Out Going Unprocessed @ ' & FORMAT(TODAY(),@d6) & ', ' & FORMAT(CLOCK(), @t4) & '  --------'
          ADD(Audit_File)

          L:IDx     = 1
          IF p:Expire_Mode = 1                    ! Processing for expired - check number
             IF RECORDS(p_D_Q) > L:Persist_Q_Trans
                L:IDx   = RECORDS(p_D_Q) - L:Persist_Q_Trans + 1       ! 120 - 100 + 1 = 21
          .  .
          LOOP
             GET(p_D_Q, L:IDx)
             IF ERRORCODE()
                BREAK
             .

             IF p:Expire_Mode > 0 AND p:Expire_Mode ~= 2               ! 2 is dump all as expired
                IF L:Out_Msgs = TRUE
                   L:Date_Compare   = p_D_Q.Date_In_Q
                ELSE                                                   ! For Sent Msgs use Sent Date (original date)
                   L:Date_Compare   = p_D_Q.Date_Sent
                .
                IF (TODAY() - L:Date_Compare) > L:Persist_Q_Days         ! Expired
                   IF p:Expire_Mode = 1           ! Then exclude expired from the dump - leaving in Q
                      L:IDx    += 1
                      CYCLE
                   .
                ELSIF p:Expire_Mode = 3           ! Include only expired in dump - delete from Q
                   L:IDx    += 1
                   CYCLE                          ! Not expired but processing only for expired - so cycle
             .  .

             ! Note: If Expire_Mode is 1 and msg has expired then code will not reach here.
             !       If msg hasn't expired yet, code will reach here if Expire_Mode is 1.


             ! Record expired message ID's in a expired Q
             IF L:Out_Msgs = FALSE
                IF p:Expire_Mode >= 2
                   SELF.Add_Msg_Expired(p_D_Q.Ack_ID, 0)
             .  .

             SCL_AUD:Q_ID   = p_D_Q.Q_ID
             IF SCL_AUD:Q_ID = 0
                SCL_AUD:Q_ID    = 1               ! 0 means header record
             .
             SCL_AUD:Msg        = CLIP(p_D_Q.Message)
             SCL_AUD:Date_In_Q  = p_D_Q.Date_In_Q               ! were clipped - why?
             SCL_AUD:Time_In_Q  = p_D_Q.Time_In_Q
             SCL_AUD:Type       = p_D_Q.Type
             SCL_AUD:Ack_ID     = p_D_Q.Ack_ID
             SCL_AUD:Date_Sent  = p_D_Q.Date_Sent
             SCL_AUD:Time_Sent  = p_D_Q.Time_Sent
             SCL_AUD:Response   = p_D_Q.Response

             ADD(Audit_File)
             IF ~ERRORCODE()
                IF p:Delete = TRUE
                   DELETE(p_D_Q)
                ELSE
                   L:IDx    += 1
                .
                L:Result    += 1
             ELSE
                L:IDx       += 1
          .  .
          CLOSE(Audit_File)
          IF L:Result = 0
             REMOVE(Audit_File)
    .  .  .

    RETURN(L:Result)



SMSMeNet_Class.Dump_In_Packets         PROCEDURE(<STRING p:File_Name>, BYTE p:Delete, ULONG p:Pos, BYTE p:Date_Stamp=1)
Audit_File      FILE,DRIVER('BASIC'),CREATE,PRE(SCL_AUD)
Rec               RECORD
Q_ID                ULONG
Length              LONG
Date_Time           STRING(20)
Packet              STRING(EQU:Cl_Packet_Size)
                . .

L:IDx           LONG
L:Result        LONG
L:File_Name     STRING(255)

    CODE
    IF RECORDS(SELF.In_Packets_Q) > 0
       L:File_Name   = p:File_Name
       IF OMITTED(2)
          L:File_Name   = 'SMSMeNet_Recd_Uprocessed_Packets.csv'
       .
       L:Result     = SELF.Open_Dump_File(Audit_File, L:File_Name)
       IF L:Result > 0
          IF L:Result = 2                               ! File was created
             SCL_AUD:Q_ID       = 0
             SCL_AUD:Length     = 0
             SCL_AUD:Packet     = 'HDR  --------  Q_ID, Length, Packet'
             ADD(Audit_File)
          .
          L:Result  = 0

          IF p:Pos <= 0
             SCL_AUD:Q_ID   = 0
             SCL_AUD:Length = 0
             SCL_AUD:Packet = 'HDR  --------  Dump Recd. Unprocessed Packets @ ' & FORMAT(TODAY(),@d6) & ', ' & FORMAT(CLOCK(), @t4) & '  --------'
             ADD(Audit_File)
          ELSE
             L:IDx          = p:Pos - 1
          .

          LOOP
             L:IDx     += 1
             GET(SELF.In_Packets_Q, L:IDx)
             IF ERRORCODE()
                BREAK
             .

             SCL_AUD:Q_ID   = SELF.In_Packets_Q.Q_ID
             SCL_AUD:Length = SELF.In_Packets_Q.Len

             IF p:Date_Stamp = TRUE
                SCL_AUD:Date_Time   = FORMAT(TODAY(),@d5) & ' - ' & FORMAT(CLOCK(),@t4)
             .

             SCL_AUD:Packet = SELF.In_Packets_Q.Packet

             ADD(Audit_File)
             IF ERRORCODE()
                L:Result               = EQU:Err_Add_Dump_Packets          ! = -113
                SELF.Set_Last_Error(L:Result)
                BREAK
             ELSE
                L:Result     += 1
             .

             IF p:Pos > 0
                BREAK
          .  .
          CLOSE(Audit_File)

          IF L:Result > 0 AND p:Delete = TRUE
             FREE(SELF.In_Packets_Q)
    .  .  .

    RETURN(L:Result)


! ---------   Timer - Take_Event / Send_Msg / Receive_Msg   ---------
SMSMeNet_Class.Take_Event               PROCEDURE()
    CODE
    ! Nothing happens until the object has been initilised.
    IF SELF.Init_ed = TRUE
       SELF.Event_ID        += 1

       SELF.Setup()                                     ! Does RSR, adds CID and RMM, sets Setup state

       CASE SELF.Check_State()
       OF 0
          SELF.Check_User_Connect_Req_Opt()
       OF 3
          SELF.Send_Msg()                               ! Send any waiting messages - from the Q

          SELF.Re_Send_Msg()                            ! Send any expired msgs with no acks - from the Q
       .

       SELF.Receive_Msg()                               ! Receive any waiting messages - from the Q
    .
    RETURN



SMSMeNet_Class.Receive_Msg              PROCEDURE(<ULONG p:No_to_Rec>)
L:Loops                 ULONG
L:Result                LONG
L:Packets_Processed     ULONG
    CODE
    IF SELF.Init_ed = FALSE
       ! Do Nothing
    ELSE
       L:Loops      = SELF.Receive_Per_Event
       IF ~OMITTED(2)
          L:Loops   = p:No_to_Rec
       .

       ! Check the Q for message - send 1st one
       LOOP L:Loops TIMES
          GET(SELF.In_Packets_Q, 1)
          IF ERRORCODE()
             BREAK
          .
          L:Result      = SELF.Recd_Process_Recd_Str(SELF.In_Packets_Q.Packet, SELF.In_Packets_Q.Len)
          IF L:Result < 0
             ! Error receiving message... do what?    leave in the Q    Error handled by Recd method?
             BREAK
          ELSE
             L:Packets_Processed    += L:Result
             DELETE(SELF.In_Packets_Q)                           ! Remove Q item
    .  .  .

    RETURN( L:Packets_Processed )



SMSMeNet_Class.Send_Msg                 PROCEDURE(ULONG p:Q_ID = 0, <ULONG p:No_to_Send>)
L:Loops             ULONG,AUTO
L:Count             ULONG
    CODE
    IF SELF.Init_ed = FALSE
       ! Do Nothing
    ELSIF p:Q_ID ~= 0
       GET(SELF.Out_Msgs_Q, p:Q_ID)
       IF ~ERRORCODE()
          IF SELF.Send(SELF.Out_Msgs_Q.Message) < 0
             ! Error sending message... do what?    leave in the Q    Error handled by Send method?
             SELF.Set_Last_Error(SELF.Last_Error_Code, SELF.Out_Msgs_Q.Ack_ID)      ! Keep same error - add msg ID
          ELSE
             ! If we have no Ack ID then we are not going to be able to match msg
             ! to servers responses so throw it away??
             SELF.Last_Sent_ID  = SELF.Out_Msgs_Q.Ack_ID
             IF SELF.Out_Msgs_Q.Ack_ID ~= 0
                SELF.Sent_Msgs_Add()                        ! Expects loaded Out_Msg_Q
             .
             DELETE(SELF.Out_Msgs_Q)                        ! Remove Q item
             L:Count    += 1
       .  .
    ELSE
       L:Loops      = SELF.Send_Per_Event
       IF ~OMITTED(3)
          L:Loops   = p:No_to_Send
       .

       ! Check the Q for message - send 1st one
       LOOP L:Loops TIMES
          GET(SELF.Out_Msgs_Q, 1)
          IF ERRORCODE()
             BREAK
          .
          !SELF.Out_Msgs_Q.Q_ID
          !SELF.Out_Msgs_Q.Message

          IF SELF.Send(SELF.Out_Msgs_Q.Message) < 0
             ! Error sending message... do what?    leave in the Q    Error handled by Send method?
             SELF.Set_Last_Error(SELF.Last_Error_Code, SELF.Out_Msgs_Q.Ack_ID)      ! Keep same error - add msg ID
             BREAK
          ELSE
             SELF.Last_Sent_ID  = SELF.Out_Msgs_Q.Ack_ID
             IF SELF.Out_Msgs_Q.Ack_ID ~= 0
                SELF.Sent_Msgs_Add()                        ! Expects loaded Out_Msg_Q
             .
             DELETE(SELF.Out_Msgs_Q)                         ! Remove Q item
             L:Count    += 1
    .  .  .

    RETURN( L:Count )



! ---------   Errors - Error_Text   ---------
SMSMeNet_Class.Error_Text               PROCEDURE(<LONG p:Error_Code>)
L:Err_Code          LONG
L:Error_Text        STRING(255)
    CODE
    IF OMITTED(2) = FALSE
       L:Err_Code   = p:Error_Code
    ELSE
       L:Err_Code   = SELF.Last_Error_Code
    .

    CASE L:Err_Code
    OF EQU:Err_Not_Initilised
       L:Error_Text                 = 'SMS Me Net client object not initilised'
    OF EQU:Err_No_NetSimple_Obj
       L:Error_Text                 = 'No Net Simple object has been specified'
    OF EQU:Err_RSR_Send_Failed
       L:Error_Text                 = 'Request for Reponse send failed'
    OF EQU:Err_NS_Obj_Send_Error
       L:Error_Text                 = 'Net Simple object send error'
    OF EQU:Err_NS_Obj_Not_Inited
       L:Error_Text                 = 'Net Simple object not initilised in SMS Me Net client object'
    OF EQU:Err_No_Server_Address
       L:Error_Text                 = 'No server address specified'
    OF EQU:Err_No_Server_Port
       L:Error_Text                 = 'No server port specified'
    OF EQU:Err_Q_Add_Read_Out_Sent
       L:Error_Text                 = 'Error adding to Out / Sent Q on reading'
    OF EQU:Err_Q_Add_Read_Recd
       L:Error_Text                 = 'Error adding to Received Q on reading'
    OF EQU:Err_Q_Add_Read_RMIDs
       L:Error_Text                 = 'Error adding to Recd. MIDs Q on reading'
    OF EQU:Err_Add_Dump_Recd
       L:Error_Text                 = 'Error adding to dump file for received'
    OF EQU:Err_Add_Dump_RMIDs
       L:Error_Text                 = 'Error adding to dump file for received MIDs'
    OF EQU:Err_Add_Dump_Packets
       L:Error_Text                 = 'Error adding to dump file for in coming packets'
    OF EQU:Err_General_Add_Queue
       L:Error_Text                 = 'Error adding to queue'
    OF EQU:Err_General_Bad_Param
       L:Error_Text                 = 'Error bad parameters passed'

    ! Non object error - messaging errors
    OF EQU:Err_Client_Has_Msg
       L:Error_Text                 = 'Client has msg for this Server ID already - Accepted'
    OF EQU:Err_Msg_Failed_Exists
       L:Error_Text                 = 'Message Sending Failed (Send Client Msg. Ref. exists for Client Message ID).'
    OF EQU:Err_User_Not_Found
       L:Error_Text                 = 'No User found with that login.'
    .

    RETURN(CLIP(L:Error_Text))


SMSMeNet_Class.Add_Net_Server          PROCEDURE(STRING p:Server, STRING p:Port, BYTE p:Free_Q, |
                                            ULONG p:ID=0 , <STRING p:Def_User>, BYTE p:Send_User_Opt=0, LONG p:Order=0, |
                                            BYTE p:Fixed_Ord=0, LONG p:Connections=0)
LOC:Result      LONG
    CODE
    IF p:Free_Q = TRUE
       FREE(SELF.Net_Servers_Q)
    .

    IF OMITTED(6)
       p:Def_User       = ''
    .

    IF CLIP(p:Server) ~= ''
       SELF.Net_Servers_Q.Server_Address        = p:Server
       SELF.Net_Servers_Q.Server_Port           = p:Port

       SELF.Net_Servers_Q.TCP_IP_ID             = p:ID
       SELF.Net_Servers_Q.Default_User          = p:Def_User
       SELF.Net_Servers_Q.Send_User_Option      = p:Send_User_Opt

       SELF.Net_Servers_Q.Order                 = p:Order
       SELF.Net_Servers_Q.Use_Fixed_Order_Only  = p:Fixed_Ord
       SELF.Net_Servers_Q.Connections           = p:Connections

       ADD(SELF.Net_Servers_Q)
       IF ERRORCODE()
          LOC:Result    = EQU:Err_General_Add_Queue
       .
    ELSE
       LOC:Result   = EQU:Err_General_Bad_Param
    .
    RETURN(LOC:Result)



SMSMeNet_Class.Req_Recd_Msg_Usr         PROCEDURE(STRING p:Login, STRING p:Password, BYTE p:Mode = 0, LONG p:Req_Number=0)
L:Send_Str      STRING(100)
LOC:Mode_No     STRING(10)
L:Result        LONG

    CODE
    ! p:Mode    - None, All Un-Read, Last X Not Received Yet, Last X Messages, Clear Server History
    EXECUTE p:Mode
       LOC:Mode_No  = '0'
       LOC:Mode_No  = p:Req_Number
       LOC:Mode_No  = -p:Req_Number
       LOC:Mode_No  = 'CL'
    ELSE
       LOC:Mode_No  = '0'
    .

    SELF.Add_Field_Packet('RMR', LOC:Mode_No, L:Send_Str)
    SELF.Add_Field_Packet('RMU', CLIP(p:Login), L:Send_Str)
    SELF.Add_Field_Packet('RMP', CLIP(p:Password), L:Send_Str)

    L:Result  = SELF.Add_Send_Msg_to_Q( L:Send_Str )

    IF L:Result = 0
       L:Result  = SELF.Last_Error_Code
    .

    ! Taken out - we want the correct response, which is?
!    IF L:Result <= 0
       ! Failed to add to Q - memory?
!       L:Result     = -1                                            ! Failed to add
!    .

    RETURN(L:Result)


! ==============================================================================================================
!
!  Receiving messages - Request Options
!
!  *  RMW:xxxxxxxxxx                 - Used by Client to request No. of pending messages waiting on the Server
!                                      for this Client (Server will need to know Client ID)
!                                    - Also sent by the Server after connection established if required (if some exist)
!           xxxx = 0    for request no. server side un-read messages
!           xxxx = 1    for request no. un-sent this client messages (lots of processing)
!
!  *  RMR:xxxxxxxxxx                 - Request messages for this Client from the Server
!
!           xxxx = 0    all un-read
!           xxxx = XX   last XX number of un-sent this client messages
!           xxxx = -XX  last XX number of this client messages regardless of previously sent
!           xxxx = CL   clear Server history of previously sent this client messages
!
!  *  RMU:xxxxxxxxxx                - User Login       - these should be combined with sending ones later
!  *  RMP:xxxxxxxxxx                - User Password
!
!  *  RMM:X                         - Set request message Mode; 0 = Pull, 1 = Push
!
!
! ( p:Type                    , p:Code, p:Msg_ID, p:Data_Delim, p:Field_Delim, p:Text )
! ( CID or DNO-SID-MSS-ERR-ERT, xxxxxx, yyyyyyyy, ':', '|', Error text when p:Type = ERT)



SMSMeNet_Class.Read_License            PROCEDURE(<STRING p:Reg_ID>, BYTE p:Delete = 0)
l_Return_Group      GROUP,PRE(l)
Reg_ID                  STRING(50)
Reg_No                  STRING(50)
Date                    LONG
Time                    LONG
                    .

    CODE
    CLEAR(SELF.Licenses_Q)

    IF OMITTED(2) = TRUE
       GET(SELF.Licenses_Q, 1)
    ELSE
       SELF.Licenses_Q.Client_Reg_ID   = p:Reg_ID
       GET(SELF.Licenses_Q, +SELF.Licenses_Q.Client_Reg_ID)
    .

    IF ERRORCODE()
       CLEAR(SELF.Licenses_Q)
    ELSE
       l:Reg_ID    = SELF.Licenses_Q.Client_Reg_ID
       l:Reg_No    = SELF.Licenses_Q.License_No
       l:Date      = SELF.Licenses_Q.Received_Date
       l:Time      = SELF.Licenses_Q.Received_Time

       IF p:Delete = TRUE
          DELETE(SELF.Licenses_Q)
    .  .
    RETURN(l_Return_Group)



SMSMeNet_Class.Request_License          PROCEDURE(STRING p:Reg_ID, <STRING p:Type>, <STRING p:Desc>)
L:Msg       STRING(300)
L:Idx       LONG
L:Q_ID      LONG
    CODE
    ! Need to check for a previous request in the to be sent Q....

    LOOP
       L:Idx    += 1
       GET(SELF.Out_Msgs_Q, L:Idx)
       IF ERRORCODE()
          BREAK
       .
       IF SELF.Out_Msgs_Q.Type ~= 2                 ! previously 1 - which is for send messages no comms messages?
          CYCLE
       .

       IF INSTRING(CLIP(p:Reg_ID), CLIP(SELF.Out_Msgs_Q.Message), 1) > 0
          L:Q_ID    = SELF.Out_Msgs_Q.Q_ID
          BREAK
    .  .

    IF L:Q_ID <= 0
       ! RLI - Request license - when sent from a SMS Me Net client
       ! RLI - Received license information - when sent from a SMS Me Net server

                           ! (STRING p:Send_Field, <STRING p:Send_Data>, <*STRING p:Packet>, BYTE p:Pos)
       SELF.Add_Field_Packet('RLI', p:Reg_ID, L:Msg)

       IF ~OMITTED(3)
          SELF.Add_Field_Packet('RLT', p:Type, L:Msg)
       .

       IF ~OMITTED(4)
          SELF.Add_Field_Packet('RLD', p:Desc, L:Msg)
       .
                           ! (STRING p:Msg, LONG p:Position=0, BYTE p:Make_Packet=1, BYTE p:Type=0, ULONG p:Ack_ID=0,
                           !  LONG p:Date_Sent_Prev=0, LONG p:Time_Sent_Prev=0)

       ! Returns message Q ID or 0 if failed to add.
       L:Q_ID   = SELF.Add_Send_Msg_to_Q(L:Msg,,,2)                 !,,,1)  shouldn't have type 1? 2 is now lic. req.
    .


    RETURN( L:Q_ID )



SMSMeNet_Class.Set_RSR_State_Check          PROCEDURE(BYTE p:RSR_State_Check)
    CODE
    SELF.RSR_State_Check    = p:RSR_State_Check
    RETURN


SMSMeNet_Class.Set_Send_RMM_CID             PROCEDURE(BYTE p:State)
    CODE
    SELF.Send_RMM_CID       = p:State
    RETURN


SMSMeNet_Class.Sent_Msgs_Q_ReSend           PROCEDURE(LONG p:Q_Pos=0, BYTE p:All=0)         !, LONG, PROC
L:Return    LONG
    CODE
    IF p:All = TRUE
       L:Return         = SELF.Re_Send_Msg(,TRUE)      ! p:No_To_Send, p:All
    ELSE
       GET(SELF.Sent_Msgs_Q, p:Q_Pos)
       IF ERRORCODE()
          L:Return      = -(ERRORCODE())       !-1     ! Does this need an Error_Text no?
       ELSE
                             ! p:Msg, p:Position=0, p:Make_Packet=1, p:Type=0, p:Ack_ID=0, p:Date_Sent_Prev, p:Time_Sent_Prev
          SELF.Resend_Sent_Msg(SELF.Sent_Msgs_Q.Message,,FALSE, 1, SELF.Sent_Msgs_Q.Ack_ID, SELF.Sent_Msgs_Q.Date_Sent, SELF.Sent_Msgs_Q.Time_Sent)

          SELF.Add_Audit('Sent_Msgs_Q_ReSend', 'Resending. Orig Date: ' & FORMAT(SELF.Sent_Msgs_Q.Date_Sent,@d5) & ', ' & FORMAT(SELF.Sent_Msgs_Q.Time_Sent,@t4), SELF.Sent_Msgs_Q.Message, 1, 'Sent_Msgs_Q_ReSend')
          DELETE(SELF.Sent_Msgs_Q)
    .  .
    RETURN(L:Return)

