
!ABCIncludeFile

OMIT('_EndOfInclude_',_ISQClass_)
_ISQClass_ EQUATE(1)


! A queue of queues
! Q_Class uses Q_Rows_Type Q, which defines 1 field, Q_Cols_Class, which uses Q_Cols_Type Q
!


Q_Cols_Type     QUEUE,TYPE
Str                 STRING(1024)
                .

Q_Rows_Type     QUEUE,TYPE
Node                &Q_Cols_Class
                .

Q_Cols_Class    CLASS(),TYPE,MODULE('isqclass.clw'),LINK('isqclass.clw',_ABCLinkMode_),DLL(_ABCDllMode_)
Q_Cols              &Q_Cols_Type

Cols                LONG

Find_Col            PROCEDURE(STRING p_Element, *LONG p_ColNo),LONG

Get_Col             PROCEDURE(LONG p_ColNo, *STRING p_Element),LONG

Set_Col             PROCEDURE(LONG p_ColNo, STRING p_Element)

Construct           PROCEDURE()
Destruct            PROCEDURE()
                .

Q_Class    CLASS(),TYPE,MODULE('isqclass.clw'),LINK('isqclass.clw',_ABCLinkMode_),DLL(_ABCDllMode_)

!Q_Class         CLASS,TYPE
Q_Nodes             &Q_Rows_Type

Rows                LONG
Cur_Row             LONG

Find_Row            PROCEDURE(STRING p_Element, *LONG p_ColNo),LONG,PROC

Get_Row_Item        PROCEDURE(LONG p_RowNo=0, LONG p_ColNo, *STRING p_Element),LONG,PROC

Update_Row_Item     PROCEDURE(STRING p_Element, LONG p_ColNo=0),LONG,PROC

Set_Row_Item        PROCEDURE(LONG p_ColNo, STRING p_Element, LONG p_RowNo=0),LONG,PROC
Add_Row_Item        PROCEDURE(LONG p_ColNo, STRING p_Element),LONG,PROC

Delete_Row          PROCEDURE(LONG p_RowNo),LONG,PROC
Clear_Q             PROCEDURE()

Construct           PROCEDURE()
Destruct            PROCEDURE()
                .



_EndOfInclude_
