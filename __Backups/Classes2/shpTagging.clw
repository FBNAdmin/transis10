
  MEMBER

  omit('***',_c55_) 
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
  ***

   Include('D:\Apps\_Classes2\shpTagging.inc'),ONCE


  Map
  End

shpTagClass.Construct          Procedure

   Code
   SELF.No_Tagged_Ctrl  = 0
   Self.TagQueue       &= New(shpTagQueue)
   RETURN


shpTagClass.Destruct           Procedure

   Code
   Free(Self.Tagqueue)
   Dispose(Self.TagQueue)
   RETURN


shpTagClass.IsTagged           Procedure(String pPoint)!,Byte

    CODE
    SELF.TagQueue.Ptr = pPoint
    GET(SELF.TagQueue, SELF.TagQueue.Ptr)
    IF ~ERRORCODE()
       RETURN(True)
    ELSE
       RETURN(False)
    END
    !RETURN

shpTagClass.MakeTag            Procedure(String pPoint)

    CODE
    Self.TagQueue.Ptr = pPoint
    Get(Self.TagQueue,Self.TagQueue.Ptr)
    If ErrorCode() then Add(Self.TagQueue,Self.TagQueue.Ptr).

    IF SELF.No_Tagged_Ctrl ~= 0
       SELF.No_Tagged_Ctrl{PROP:Text}  = 'Tagged: ' & SELF.NumberTagged()
    .
    RETURN


shpTagClass.ClearTag           Procedure(String pPoint)

    CODE
    SELF.TagQueue.Ptr = pPoint
    GET(SELF.TagQueue, SELF.TagQueue.Ptr)
    IF ~ERRORCODE() THEN DELETE(SELF.TagQueue).

    IF SELF.No_Tagged_Ctrl ~= 0
       SELF.No_Tagged_Ctrl{PROP:Text}  = 'Tagged: ' & SELF.NumberTagged()
    .
    RETURN


shpTagClass.ClearAllTags       Procedure

    CODE
    FREE(SELF.TagQueue)

    IF SELF.No_Tagged_Ctrl ~= 0
       SELF.No_Tagged_Ctrl{PROP:Text}  = 'Tagged: ' & SELF.NumberTagged()
    .
    RETURN


shpTagClass.NumberTagged       Procedure()      !,Long
L_No        LONG
    CODE
    L_No    = RECORDS(SELF.TagQueue)
    RETURN L_No

	
shpTagClass.ToggleTag          Procedure(String pPoint)

    CODE
	IF SELF.IsTagged(pPoint) = TRUE
	   SELF.ClearTag(pPoint)
	ELSE
	   SELF.MakeTag(pPoint)
	.
    RETURN


shpTagClass.Init                        PROCEDURE(<LONG p_Tag_Ctrl>)
    CODE
    IF ~OMITTED(2)
       SELF.No_Tagged_Ctrl  = p_Tag_Ctrl
    .
    RETURN