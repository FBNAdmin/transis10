
!ABCIncludeFile

OMIT('_EndOfInclude_',_tagging_)
_tagging_ EQUATE(1)

shpTagQueue   Queue,Type
Ptr             String(1024)
              End


shpTagClass   CLASS,TYPE,MODULE('D:\Apps\_Classes2\shpTagging.clw'),LINK('D:\Apps\_Classes2\shpTagging.clw',_ABCLinkMode_),DLL(_ABCDllMode_)
!shpTagClass   CLASS,TYPE,MODULE('D:\Apps\_Classes2\shpTagging.clw'),LINK('D:\Apps\_Classes2\shpTagging.clw',1),DLL(0)
TagQueue        &shpTagQueue

No_Tagged_Ctrl  LONG                ! Screen Control

Init            PROCEDURE(<LONG p_Tag_Ctrl>)

Construct       Procedure
Destruct        Procedure

IsTagged        Procedure(String pPoint),Byte
MakeTag         Procedure(String pPoint)
ClearTag        Procedure(String pPoint)
ClearAllTags    Procedure

ToggleTag       Procedure(String pPoint)				! added 14/06/12

NumberTagged    Procedure(),Long
              End

_EndOfInclude_
