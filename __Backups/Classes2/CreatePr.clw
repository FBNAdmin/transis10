
 Section('Data')
STARTF_USESHOWWINDOW    Equate(1)
CREATE_NEW_CONSOLE       Equate(010h)
CREATE_NEW_PROCESS_GROUP Equate(0200h)
STARTF_FORCEOFFFEEDBACK  Equate(080h)
INFINITE                 Equate(0FFFFFFFFh)

!SECURITY_ATTRIBUTES        GROUP,TYPE
!nLength                      Long
!lpSecurityDescriptor         ULong
!bInheritUnsigned               BOOL
!                           END
STARTUP_INFO               GROUP,TYPE
cb                           Long
lpReserved                   ULONG
lpDesktop                    ULONG
lpTitle                      ULONG
dwX                          Long
dwY                          Long
dwXSize                      Long
dwYSize                      Long
dwXCountChars                Long
dwYCountChars                Long
dwFillAttribute              Long
dwFlags                      Long
wShowWindow                  Signed
cbReserved2                  Signed
lpReserved2                  ULONG
hStdInput                    Unsigned
hStdOutput                   Unsigned
hStdError                    Unsigned
                           END
PROCESS_INFORMATION        GROUP,TYPE
hProcess                     Unsigned
hThread                      Unsigned
dwProcessId                  Long
dwThreadId                   Long
                           END

 Section('Prototype')
 Module('Win API')
!        GetLastError(),Long,Pascal
        CreateProcess(ULong  lpApplicationName,   |
                     *CString  lpCommandLine,       |
                     ULONG  lpProcessAttributes, |
                     ULONG  lpThreadAttributes,  |
                     BOOL   bInheritHandles,     |
                     Long  dwCreationFlags,     |
                     ULong lpEnvironment,       |
                     ULONG  lpCurrentDirectory,  |
                     ULONG  lpStartupInfo,       |
                     ULONG  lpProcessInformation |
                    ),BOOL,RAW,PASCAL,NAME('CreateProcessA')
        CloseHandle(Unsigned),BOOL,PASCAL,PROC
        WaitForSingleObject(Unsigned,Long),Long,PASCAL
        WaitForInputIdle(Unsigned,Long),Long,PASCAL
        FindWindow(Long,Long),Unsigned,Pascal,NAME('FindWindowA')
        ShowWindow(Long,Long),Unsigned,Pascal
 End