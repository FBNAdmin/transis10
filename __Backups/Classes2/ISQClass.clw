
  MEMBER

  omit('***',_c55_)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
  ***

   Include('ISQClass.inc'),ONCE
!   Include('ABFILE.INC'),ONCE
!   Include('ABERROR.INC'),ONCE

  Map
  End


! ============  Q_Class Class  ============
Q_Class.Update_Row_Item     PROCEDURE(STRING p_Element, LONG p_ColNo=0)     !,LONG,PROC
Retrn   LONG
RowNo   LONG
    CODE
    ! Look for Element in Q, if found this will be updated to what it already is .... ??
    !   If found it will be updated
    !   If not found the current row will be updated (SELF.Cur_Row) if it exists
    !   Otherwise a new row will be added
    !
    !   Current class does not allow for RowNo < 0 to be returned but this has no effect because the
    !   RowNo if not passed to Set_Row_Item is defaulted to 0, so if not found this will be zero
    !   but in all cases ELSE will execute... ??

    RowNo       = SELF.Find_Row(p_Element, p_ColNo)
    IF RowNo < 0
       Retrn    = SELF.Set_Row_Item(p_ColNo, p_Element)
    ELSE
       Retrn    = SELF.Set_Row_Item(p_ColNo, p_Element, RowNo)
    .
    RETURN(Retrn)       ! Row no.


Q_Class.Find_Row            PROCEDURE(STRING p_Element, *LONG p_ColNo)      !,LONG,PROC
Idx     LONG
Retrn   LONG(0)
ColNo   LONG
    CODE
    ! Loop through all Rows in our rows Q
    !   Look for the passed element, if found return Row No (and Col No)

    Idx     = 0
    LOOP
       Idx += 1
       GET(SELF.Q_Nodes, Idx)
       IF ERRORCODE()
          BREAK
       .
       ColNo        = SELF.Q_Nodes.Node.Find_Col(p_Element, p_ColNo)        !,LONG
       IF ColNo > 0
          Retrn     = Idx
          p_ColNo   = ColNo
          BREAK
    .  .

    RETURN(Retrn)


Q_Class.Get_Row_Item        PROCEDURE(LONG p_RowNo=0, LONG p_ColNo, *STRING p_Element)         !,LONG,PROC
Retrn   LONG
    CODE
    IF p_RowNo = 0
       p_RowNo      = SELF.Cur_Row
    .

    GET(SELF.Q_Nodes, p_RowNo)
    IF ERRORCODE()
       Retrn    = -1
    ELSE
       SELF.Cur_Row = p_RowNo
       Retrn    = SELF.Q_Nodes.Node.Get_Col(p_ColNo, p_Element)
    .
    RETURN(Retrn)


Q_Class.Add_Row_Item        PROCEDURE(LONG p_ColNo, STRING p_Element)       !,LONG,PROC
Retrn   LONG
    CODE
    ADD(SELF.Q_Nodes)
    IF ~ERRORCODE()
       SELF.Q_Nodes.Node   &= NEW(Q_Cols_Class)
       SELF.Q_Nodes.Node.Set_Col(p_ColNo, p_Element)
       PUT(SELF.Q_Nodes)

       SELF.Rows   = RECORDS(SELF.Q_Nodes)
       Retrn       = 1
    .
    RETURN(Retrn)


Q_Class.Set_Row_Item        PROCEDURE(LONG p_ColNo, STRING p_Element, LONG p_RowNo=0)
Retrn   LONG
    CODE
    ! Note: p_RowNo when not supplied set to Cur_Row
    !       If the row exists then the item will be updated in that row
    !       If the row does no exist then a new row, the next one in order, will be added
    !       and the item set.

    IF p_RowNo = 0
       p_RowNo      = SELF.Cur_Row
    .

    GET(SELF.Q_Nodes, p_RowNo)
    IF ERRORCODE()
       IF SELF.Add_Row_Item(p_ColNo, p_Element) = 1
          Retrn     = RECORDS(SELF.Q_Nodes)
       .
    ELSE
       SELF.Q_Nodes.Node.Set_Col(p_ColNo, p_Element)
       Retrn        = p_RowNo
    .
    SELF.Rows   = RECORDS(SELF.Q_Nodes)
    RETURN(Retrn)


Q_Class.Delete_Row          PROCEDURE(LONG p_RowNo)     !,LONG,PROC
Retrn   LONG
    CODE
    GET(SELF.Q_Nodes, p_RowNo)
    IF ERRORCODE()
       Retrn    = -1
    ELSE
       DISPOSE(SELF.Q_Nodes.Node)
       DELETE(SELF.Q_Nodes)

       Retrn    = 1
    .
    SELF.Rows   = RECORDS(SELF.Q_Nodes)
    RETURN(Retrn)

Q_Class.Clear_Q            PROCEDURE()
    CODE
    LOOP
       GET(SELF.Q_Nodes,1)
       IF ERRORCODE()
          BREAK
       .
       DISPOSE(SELF.Q_Nodes.Node)
       DELETE(SELF.Q_Nodes)
    .
    RETURN

Q_Class.Construct   PROCEDURE()
    CODE
    SELF.Q_Nodes    &= NEW(Q_Rows_Type)
    RETURN

Q_Class.Destruct    PROCEDURE()
    CODE
    SELF.Clear_Q()
    DISPOSE(SELF.Q_Nodes)
    RETURN


! ============  Q_Cols Class  ============

Q_Cols_Class.Find_Col            PROCEDURE(STRING p_Element, *LONG p_ColNo)    !,LONG
Retrn       LONG
Idx         LONG
Elem        STRING(1024)
    CODE
    ! Note: Returns the 1st matched column no.
    !       If column passed then only that will be checked
    Idx = 0
    LOOP
       IF p_ColNo ~= 0
          Idx   = p_ColNo
       ELSE
          Idx  += 1
       .
       IF SELF.Get_Col(Idx, Elem) < 0
          BREAK
       .
       IF CLIP(Elem) = CLIP(p_Element)
          Retrn     = Idx
          BREAK
       .

       IF p_ColNo ~= 0
          BREAK
    .  .
    RETURN(Retrn)

Q_Cols_Class.Get_Col             PROCEDURE(LONG p_ColNo, *STRING p_Element)     !,LONG
Retrn       LONG
    CODE
    GET(SELF.Q_Cols, p_ColNo)
    IF ERRORCODE()
       Retrn        = -1
    ELSE
       p_Element    = SELF.Q_Cols.Str
    .
    RETURN(Retrn)

Q_Cols_Class.Set_Col             PROCEDURE(LONG p_ColNo, STRING p_Element)
Idx     LONG
    CODE
    IF p_ColNo > SELF.Cols       ! Create any empty cols up to..
       Idx  = SELF.Cols
       LOOP UNTIL Idx >= p_ColNo
          Idx             += 1
          SELF.Q_Cols.Str  = ''
          ADD(SELF.Q_Cols)
       .
       SELF.Cols    = RECORDS(SELF.Q_Cols)
    .

    GET(SELF.Q_Cols, p_ColNo)
    SELF.Q_Cols.Str  = p_Element
    PUT(SELF.Q_Cols)
    RETURN

Q_Cols_Class.Construct           PROCEDURE()
    CODE
    SELF.Cols       = 0
    SELF.Q_Cols     &= NEW(Q_Cols_Type)
    RETURN

Q_Cols_Class.Destruct            PROCEDURE()
    CODE
    DISPOSE(SELF.Q_Cols)
    RETURN
