
!!! Periods !!!!!
Period:All                   EQUATE(1B)
!!!! Current  !!!!!
Period:Today                 EQUATE(10B)
Period:Current2              EQUATE(100B)
Period:Current3              EQUATE(1000B)
Period:Yesterday             EQUATE(10000B)
Period:Current5              EQUATE(100000B)
Period:ThisWeek              EQUATE(1000000B)
Period:Current7              EQUATE(10000000B)
Period:ThisMonth             EQUATE(100000000B)
Period:Current9              EQUATE(1000000000B)
Period:Current10             EQUATE(10000000000B)
!!!! Past Periods !!!!!
Period:LastWeek              EQUATE(100000000000B)
Period:Past2                 EQUATE(1000000000000B)
Period:Past3                 EQUATE(10000000000000B)
Period:Past4                 EQUATE(100000000000000B)
Period:Past5                 EQUATE(1000000000000000B)
Period:LastMonth             EQUATE(10000000000000000B)
Period:Past7                 EQUATE(100000000000000000B)
Period:Past8                 EQUATE(1000000000000000000B)
Period:Past9                 EQUATE(10000000000000000000B)
Period:Past10                EQUATE(100000000000000000000B)
!!!! Future Periods !!!!!
Period:Future1               EQUATE(1000000000000000000000B)
Period:Future2               EQUATE(10000000000000000000000B)
Period:Future3               EQUATE(100000000000000000000000B)
Period:Future4               EQUATE(1000000000000000000000000B)
Period:Future5               EQUATE(10000000000000000000000000B)
Period:Future6               EQUATE(100000000000000000000000000B)
Period:Future7               EQUATE(1000000000000000000000000000B)
Period:Future8               EQUATE(10000000000000000000000000000B)
Period:Future9               EQUATE(100000000000000000000000000000B)
!!!!!!!!!!!!!!!!!!!!!!!!!
Period:DateRange             EQUATE(1000000000000000000000000000000B)

!Override method FillQPeriod and GetPeriodName !!!
!!!!!!!!!!!!!!!!!


tgDatePeriodsParams  GROUP,TYPE
PeriodEquates         ULONG
DefaultPeriod         ULONG
ListControl           ULONG
DateFromControl       ULONG
DateToControl         ULONG
CurDateFrom           &DATE
CurDateTo             &DATE
PrevControl           ULONG
NextControl           ULONG
 END

tgPeriod    GROUP,TYPE
perName      CSTRING(100)
DateFrom     DATE
DateTo       DATE
PeriodID     ULONG
            END



tqPeriod    QUEUE(tgPeriod),TYPE.

DatePeriodClass   CLASS,TYPE,MODULE('DatePeriods.CLW'),LINK('DatePeriods.clw',_ABCLinkMode_),DLL(_ABCDLLMode_)
gLastPeriod    GROUP(tgPeriod),PRIVATE.
gCurPeriod     GROUP(tgPeriod),PRIVATE.

ListControl  ULONG,PROTECTED
DateFromControl  ULONG,PROTECTED
DateToControl    ULONG,PROTECTED

PrevControl      ULONG,PROTECTED
NextControl      ULONG,PROTECTED

qPeriod          &tqPeriod,PROTECTED

PeriodId      &ULONG
DateFrom      &DATE
DateTo        &DATE

Init         PROCEDURE(*tgDatePeriodsParams pParams)
Kill         PROCEDURE
TakeEvent    PROCEDURE()

SetTips          PROCEDURE(),PRIVATE
MovePeriod       PROCEDURE(BYTE Direction),VIRTUAL  !,PRIVATE
GetPeriodType    PROCEDURE(),SHORT,VIRTUAL  !,PRIVATE

SelectPeriod     PROCEDURE(ULONG pPeriodEquate=0),Private !!! BY List CHOICE IF 0
Synchronize      PROCEDURE(),PRIVATE
ChangeDateRange  PROCEDURE(),PRIVATE

FillqPeriod  PROCEDURE(ULONG pPeriodEquates),PROTECTED,VIRTUAL
GetPeriodName    PROCEDURE(ULONG pPeriodEquate),STRING,PROTECTED,VIRTUAL
 END
