

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS040.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Drivers PROCEDURE (p:DI_Drivers, p:D_Type)

CurrentTab           STRING(80)                            !
L_G:Category         STRING(15)                            !Driver Category
L_G:Type             STRING(10)                            !Driver or Assistant
LOC:Group_Options    GROUP,PRE(L_OG)                       !
BID                  ULONG                                 !Branch ID
BranchName           STRING(35)                            !Branch Name
Selecting            BYTE                                  !
Type                 LONG                                  !Driver or Assistant
Category             LONG                                  !
                     END                                   !
LOC:Browse_Fields    GROUP,PRE()                           !
L_BG:BranchName      STRING(35)                            !Branch Name
                     END                                   !
LOC:Show_Archived    BYTE                                  !
LOC:Archive_Warning_Show BYTE                              !
BRW1::View:Browse    VIEW(Drivers)
                       PROJECT(DRI:FirstName)
                       PROJECT(DRI:Surname)
                       PROJECT(DRI:EmployeeNo)
                       PROJECT(DRI:Archived)
                       PROJECT(DRI:DRID)
                       PROJECT(DRI:FirstNameSurname)
                       PROJECT(DRI:Type)
                       PROJECT(DRI:Category)
                       PROJECT(DRI:BID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DRI:FirstName          LIKE(DRI:FirstName)            !List box control field - type derived from field
DRI:Surname            LIKE(DRI:Surname)              !List box control field - type derived from field
L_G:Category           LIKE(L_G:Category)             !List box control field - type derived from local data
L_G:Type               LIKE(L_G:Type)                 !List box control field - type derived from local data
DRI:EmployeeNo         LIKE(DRI:EmployeeNo)           !List box control field - type derived from field
L_BG:BranchName        LIKE(L_BG:BranchName)          !List box control field - type derived from local data
DRI:Archived           LIKE(DRI:Archived)             !List box control field - type derived from field
DRI:Archived_Icon      LONG                           !Entry's icon ID
DRI:DRID               LIKE(DRI:DRID)                 !List box control field - type derived from field
DRI:FirstNameSurname   LIKE(DRI:FirstNameSurname)     !Browse hot field - type derived from field
DRI:Type               LIKE(DRI:Type)                 !Browse hot field - type derived from field
DRI:Category           LIKE(DRI:Category)             !Browse hot field - type derived from field
DRI:BID                LIKE(DRI:BID)                  !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB8::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?L_OG:BranchName
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Drivers File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Browse_Drivers'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,ALRT(F2Key),FORMAT('80L(2)|M~First Name~@' & |
  's35@80L(2)|M~Surname~@s35@40L(2)|M~Category~@s15@40L(2)|M~Type~@s10@50L(2)|M~Employe' & |
  'e No.~@s15@60L(2)|M~Branch Name~@s35@35L(2)|MI~Archived~@p p@40R(2)|M~DrID~L(2)@n_10@'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing the Drivers file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By First Name && Surname'),USE(?Tab:2)
                           GROUP,AT(5,184,189,10),USE(?Group_Branch)
                             PROMPT('Branch:'),AT(5,184),USE(?Prompt1)
                             LIST,AT(34,184,81,10),USE(L_OG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                             CHECK('Show Archived'),AT(131,184),USE(LOC:Show_Archived),TIP('Include Archived in the ' & |
  'list<0DH,0AH,0DH,0AH>To toggle Archived press F2')
                           END
                         END
                         TAB('&2) By Employee No.'),USE(?Tab2)
                         END
                       END
                       BUTTON('Cl&ose'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(210,180,17,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB8                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Archive_Toggle                  ROUTINE
DATA
R:Yes   BYTE

CODE  
  BRW1.UpdateViewRecord()
  BRW1.UpdateBuffer()
  
  ! Toggle archived on highlighted...
  IF LOC:Archive_Warning_Show = 0
     IF DRI:Archived = FALSE
        CASE MESSAGE('Archive this Driver?','Archive Drivers',ICON:Question, 'Yes|Yes (don''t warn again)|No', 3)
        OF 1
          R:Yes = TRUE
        OF 2    
          R:Yes = TRUE          
          LOC:Archive_Warning_Show  = 1
        .
      ELSE
        CASE MESSAGE('Un-Archive this Driver?','Archive Drivers',ICON:Question, 'Yes|Yes (don''t warn again)|No', 3)
        OF 1
          R:Yes = TRUE
        OF 2    
          R:Yes = TRUE          
          LOC:Archive_Warning_Show  = 1
        .        
      .
  ELSE
    R:Yes = TRUE
  .
  
  IF R:Yes = TRUE    
    BRW1.UpdateViewRecord()
    
    BRW1.UpdateBuffer()
    IF Access:Drivers.TryFetch(DRI:PKey_DRID) = Level:Benign
      IF DRI:Archived  = 1
        DRI:Archived = 0
      ELSE
        DRI:Archived = 1
        DRI:Archived_Date = TODAY()
        DRI:Archived_Time = CLOCK()
      .          
      IF Access:Drivers.TryUpdate() = Level:Benign
        BRW1.ResetFromBuffer()  
  . . .    

  EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Drivers')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:BID',L_OG:BID)                                ! Added by: BrowseBox(ABC)
  BIND('L_OG:Category',L_OG:Category)                      ! Added by: BrowseBox(ABC)
  BIND('L_OG:Type',L_OG:Type)                              ! Added by: BrowseBox(ABC)
  BIND('LOC:Show_Archived',LOC:Show_Archived)              ! Added by: BrowseBox(ABC)
  BIND('L_G:Category',L_G:Category)                        ! Added by: BrowseBox(ABC)
  BIND('L_G:Type',L_G:Type)                                ! Added by: BrowseBox(ABC)
  BIND('L_BG:BranchName',L_BG:BranchName)                  ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Drivers,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,DRI:SKey_FirstNameSurname)            ! Add the sort order for DRI:SKey_FirstNameSurname for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,DRI:FirstNameSurname,1,BRW1)   ! Initialize the browse locator using  using key: DRI:SKey_FirstNameSurname , DRI:FirstNameSurname
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon DRI:FirstName for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,DRI:Key_FirstNameSurname) ! Add the sort order for DRI:Key_FirstNameSurname for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,DRI:FirstName,1,BRW1)          ! Initialize the browse locator using  using key: DRI:Key_FirstNameSurname , DRI:FirstName
  BRW1.SetFilter('((L_OG:BID = 0 OR L_OG:BID = DRI:BID) AND ((L_OG:Category = -1 OR DRI:Category = L_OG:Category) AND (L_OG:Type = -1 OR L_OG:Type = DRI:Type)) AND (LOC:Show_Archived = 1 OR DRI:Archived = 0))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Show_Archived)                    ! Apply the reset field
  BRW1.AddResetField(L_OG:BID)                             ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(DRI:FirstName,BRW1.Q.DRI:FirstName)        ! Field DRI:FirstName is a hot field or requires assignment from browse
  BRW1.AddField(DRI:Surname,BRW1.Q.DRI:Surname)            ! Field DRI:Surname is a hot field or requires assignment from browse
  BRW1.AddField(L_G:Category,BRW1.Q.L_G:Category)          ! Field L_G:Category is a hot field or requires assignment from browse
  BRW1.AddField(L_G:Type,BRW1.Q.L_G:Type)                  ! Field L_G:Type is a hot field or requires assignment from browse
  BRW1.AddField(DRI:EmployeeNo,BRW1.Q.DRI:EmployeeNo)      ! Field DRI:EmployeeNo is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:BranchName,BRW1.Q.L_BG:BranchName)    ! Field L_BG:BranchName is a hot field or requires assignment from browse
  BRW1.AddField(DRI:Archived,BRW1.Q.DRI:Archived)          ! Field DRI:Archived is a hot field or requires assignment from browse
  BRW1.AddField(DRI:DRID,BRW1.Q.DRI:DRID)                  ! Field DRI:DRID is a hot field or requires assignment from browse
  BRW1.AddField(DRI:FirstNameSurname,BRW1.Q.DRI:FirstNameSurname) ! Field DRI:FirstNameSurname is a hot field or requires assignment from browse
  BRW1.AddField(DRI:Type,BRW1.Q.DRI:Type)                  ! Field DRI:Type is a hot field or requires assignment from browse
  BRW1.AddField(DRI:Category,BRW1.Q.DRI:Category)          ! Field DRI:Category is a hot field or requires assignment from browse
  BRW1.AddField(DRI:BID,BRW1.Q.DRI:BID)                    ! Field DRI:BID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Drivers',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  FDB8.Init(?L_OG:BranchName,Queue:FileDrop.ViewPosition,FDB8::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB8.Q &= Queue:FileDrop
  FDB8.AddSortOrder(BRA:Key_BranchName)
  FDB8.AddField(BRA:BranchName,FDB8.Q.BRA:BranchName) !List box control field - type derived from field
  FDB8.AddField(BRA:BID,FDB8.Q.BRA:BID) !Primary key field - type derived from field
  FDB8.AddUpdateField(BRA:BID,L_OG:BID)
  ThisWindow.AddItem(FDB8.WindowComponent)
  FDB8.DefaultFill = 0
      ! (p:DI_Drivers, p:D_Type)
  
      IF CLIP(p:DI_Drivers) = ''
         L_OG:Category    = -1
      ELSE
         L_OG:Category    = DEFORMAT(p:DI_Drivers)
      .
  
  !    IF DEFORMAT(p:DI_Drivers) = 1
  !       L_OG:Selecting    = 1                ! Not sure what the purpose of this was meant to be
  !       HIDE(?Group_Branch)
  !    .
  
      IF CLIP(p:D_Type) = ''
         L_OG:Type        = -1
      ELSE
         L_OG:Type        = DEFORMAT(p:D_Type)
      .
      ! Load the Branch record
      BRA:BID             = GLO:BranchID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         L_OG:BID         = GLO:BranchID
         L_OG:BranchName  = BRA:BranchName
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Drivers',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Drivers(p:DI_Drivers, p:D_Type)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      IF KEYCODE() = F2Key
        DO Archive_Toggle
      .
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      CLEAR(L_G:Category)
      CLEAR(L_G:Type)
      CLEAR(L_BG:BranchName)
  
      EXECUTE DRI:Category + 1
         L_G:Category     = 'Branch'
         L_G:Category     = 'Long Distance'
      .
  
  
      EXECUTE DRI:Type + 1
         L_G:Type         = 'Driver'
         L_G:Type         = 'Assistant'
      .
  
  
  
  
      BRA:BID     = DRI:BID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         L_BG:BranchName  = BRA:BranchName
      .
  PARENT.SetQueueRecord
  
  IF (DRI:Archived)
    SELF.Q.DRI:Archived_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.DRI:Archived_Icon = 1                           ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Branch, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Branch


FDB8.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      ! Add an All
      Queue:FileDrop.BRA:BranchName      = 'All'
      GET(Queue:FileDrop, Queue:FileDrop.BRA:BranchName)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop)
         Queue:FileDrop.BRA:BranchName      = 'All'
         Queue:FileDrop.BRA:BID             = 0
         ADD(Queue:FileDrop)
      .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Window
!!! Browse the Vessels file
!!! </summary>
Browse_Vessels PROCEDURE 

CurrentTab           STRING(80)                            !
Locator              STRING(30)                            !
BRW1::View:Browse    VIEW(Vessels)
                       PROJECT(VES:Vessel)
                       PROJECT(VES:VEID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
VES:Vessel             LIKE(VES:Vessel)               !List box control field - type derived from field
VES:VEID               LIKE(VES:VEID)                 !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Vessels file'),AT(,,277,198),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_Vessels'),SYSTEM
                       LIST,AT(8,36,261,118),USE(?Browse:1),HVSCROLL,FORMAT('200L(2)|M~Vessel~@s50@40L(2)|M~VE' & |
  'ID~L(1)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Vessels file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Vessel'),USE(?Tab:2)
                           STRING(@s30),AT(9,22,121),USE(Locator)
                         END
                       END
                       BUTTON('&Close'),AT(222,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(8,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Vessels')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Vessels.Open                                      ! File Vessels used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Vessels,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,VES:Key_Vessel)                       ! Add the sort order for VES:Key_Vessel for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(?Locator,VES:Vessel,1,BRW1)     ! Initialize the browse locator using ?Locator using key: VES:Key_Vessel , VES:Vessel
  BRW1.AddField(VES:Vessel,BRW1.Q.VES:Vessel)              ! Field VES:Vessel is a hot field or requires assignment from browse
  BRW1.AddField(VES:VEID,BRW1.Q.VES:VEID)                  ! Field VES:VEID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Vessels',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Vessels.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Vessels',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    UpdateVessels
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Form Vessels
!!! </summary>
UpdateVessels PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::VES:Record  LIKE(VES:RECORD),THREAD
QuickWindow          WINDOW('Form Vessels'),AT(,,273,56),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('UpdateVessels'),SYSTEM
                       SHEET,AT(4,4,265,30),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Vessel:'),AT(8,20),USE(?VES:Vessel:Prompt),TRN
                           ENTRY(@s50),AT(61,20,204,10),USE(VES:Vessel),MSG('Vessel Name'),TIP('Vessel Name')
                         END
                       END
                       BUTTON('&OK'),AT(114,38,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(167,38,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(220,38,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateVessels')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?VES:Vessel:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(VES:Record,History::VES:Record)
  SELF.AddHistoryField(?VES:Vessel,2)
  SELF.AddUpdateFile(Access:Vessels)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Vessels.Open                                      ! File Vessels used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Vessels
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?VES:Vessel{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('UpdateVessels',QuickWindow)                ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Vessels.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateVessels',QuickWindow)             ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! (p:Option)
!!! </summary>
Browse_EmailAddresses PROCEDURE (p:Option)

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(EmailAddresses)
                       PROJECT(EMAI:EmailName)
                       PROJECT(EMAI:EmailAddress)
                       PROJECT(EMAI:RateLetter)
                       PROJECT(EMAI:DefaultAddress)
                       PROJECT(EMAI:Operations)
                       PROJECT(EMAI:OperationsReference)
                       PROJECT(EMAI:EAID)
                       PROJECT(EMAI:CID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
EMAI:EmailName         LIKE(EMAI:EmailName)           !List box control field - type derived from field
EMAI:EmailAddress      LIKE(EMAI:EmailAddress)        !List box control field - type derived from field
EMAI:RateLetter        LIKE(EMAI:RateLetter)          !List box control field - type derived from field
EMAI:DefaultAddress    LIKE(EMAI:DefaultAddress)      !List box control field - type derived from field
EMAI:Operations        LIKE(EMAI:Operations)          !List box control field - type derived from field
EMAI:OperationsReference LIKE(EMAI:OperationsReference) !List box control field - type derived from field
EMAI:EAID              LIKE(EMAI:EAID)                !Primary key field - type derived from field
EMAI:CID               LIKE(EMAI:CID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Email Addresses'),AT(,,358,198),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_EmailAddresses'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('70L(2)|M~Email Name~@s35@150L(2)|' & |
  'M~Email Address~@s255@48R(2)|M~Rate Letter~C(0)@n3@36R(2)|M~ Default~C(0)@n3@44R(2)|' & |
  'M~Operations~C(0)@n3@80L(2)|M~Operations Reference~C(0)@s35@'),FROM(Queue:Browse:1),IMM, |
  MSG('Browsing the EmailAddresses file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Client'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(252,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_EmailAddresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EmailAddresses,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,EMAI:FKey_CID)                        ! Add the sort order for EMAI:FKey_CID for sort order 1
  BRW1.AddRange(EMAI:CID,Relate:EmailAddresses,Relate:Clients) ! Add file relationship range limit for sort order 1
  BRW1.AddField(EMAI:EmailName,BRW1.Q.EMAI:EmailName)      ! Field EMAI:EmailName is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:EmailAddress,BRW1.Q.EMAI:EmailAddress) ! Field EMAI:EmailAddress is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:RateLetter,BRW1.Q.EMAI:RateLetter)    ! Field EMAI:RateLetter is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:DefaultAddress,BRW1.Q.EMAI:DefaultAddress) ! Field EMAI:DefaultAddress is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:Operations,BRW1.Q.EMAI:Operations)    ! Field EMAI:Operations is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:OperationsReference,BRW1.Q.EMAI:OperationsReference) ! Field EMAI:OperationsReference is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:EAID,BRW1.Q.EMAI:EAID)                ! Field EMAI:EAID is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:CID,BRW1.Q.EMAI:CID)                  ! Field EMAI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_EmailAddresses',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_EmailAddresses',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Email_Addresses(p:Option)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Form EmailAddresses
!!! </summary>
UpdateEmailAddresses PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::EMAI:Record LIKE(EMAI:RECORD),THREAD
QuickWindow          WINDOW('Form EmailAddresses'),AT(,,358,122),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('UpdateEmailAddresses'),SYSTEM
                       SHEET,AT(4,4,350,96),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Email Name:'),AT(8,20),USE(?EMAI:EmailName:Prompt),TRN
                           ENTRY(@s35),AT(96,20,144,10),USE(EMAI:EmailName),MSG('Name of contact'),REQ,TIP('Name of contact')
                           PROMPT('Email Address:'),AT(8,34),USE(?EMAI:EmailAddress:Prompt),TRN
                           ENTRY(@s255),AT(96,34,254,10),USE(EMAI:EmailAddress),MSG('Email Address'),REQ,TIP('Email Address')
                           CHECK('Rate Letter'),AT(96,48,70,8),USE(EMAI:RateLetter),MSG('This email address gets s' & |
  'ent rate letters'),TIP('This email address gets sent rate letters')
                           PROMPT('CID:'),AT(8,60),USE(?EMAI:CID:Prompt),TRN
                           STRING(@n_10),AT(96,60,104,10),USE(EMAI:CID),RIGHT(1),TRN
                           CHECK(' Default'),AT(96,74,70,8),USE(EMAI:DefaultAddress),MSG('Default Email Address'),TIP('Default Em' & |
  'ail Address')
                           CHECK(' Operations'),AT(170,74,70,8),USE(EMAI:Operations),MSG('This email address is fo' & |
  'r the client Operations team'),TIP('This email address is for the client Operations team')
                           PROMPT('Operations Reference:'),AT(8,86),USE(?EMAI:OperationsReference:Prompt),TRN
                           ENTRY(@s35),AT(96,86,144,10),USE(EMAI:OperationsReference),MSG('A reference for the Ope' & |
  'rations team'),TIP('A reference for the Operations team')
                         END
                       END
                       BUTTON('&OK'),AT(199,104,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(252,104,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(305,104,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateEmailAddresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?EMAI:EmailName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(EMAI:Record,History::EMAI:Record)
  SELF.AddHistoryField(?EMAI:EmailName,3)
  SELF.AddHistoryField(?EMAI:EmailAddress,4)
  SELF.AddHistoryField(?EMAI:RateLetter,5)
  SELF.AddHistoryField(?EMAI:CID,6)
  SELF.AddHistoryField(?EMAI:DefaultAddress,7)
  SELF.AddHistoryField(?EMAI:Operations,8)
  SELF.AddHistoryField(?EMAI:OperationsReference,9)
  SELF.AddUpdateFile(Access:EmailAddresses)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:EmailAddresses.Open                               ! File EmailAddresses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:EmailAddresses
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?EMAI:EmailName{PROP:ReadOnly} = True
    ?EMAI:EmailAddress{PROP:ReadOnly} = True
    ?EMAI:OperationsReference{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('UpdateEmailAddresses',QuickWindow)         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EmailAddresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateEmailAddresses',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

