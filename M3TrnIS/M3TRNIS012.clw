

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS012.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_PackagingTypes PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(DeliveryItems)
                       PROJECT(DELI:ItemNo)
                       PROJECT(DELI:Type)
                       PROJECT(DELI:ContainerNo)
                       PROJECT(DELI:ContainerReturnAID)
                       PROJECT(DELI:ContainerVessel)
                       PROJECT(DELI:ETA)
                       PROJECT(DELI:ByContainer)
                       PROJECT(DELI:Length)
                       PROJECT(DELI:Breadth)
                       PROJECT(DELI:DIID)
                       PROJECT(DELI:PTID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Type              LIKE(DELI:Type)                !List box control field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !List box control field - type derived from field
DELI:ContainerReturnAID LIKE(DELI:ContainerReturnAID) !List box control field - type derived from field
DELI:ContainerVessel   LIKE(DELI:ContainerVessel)     !List box control field - type derived from field
DELI:ETA               LIKE(DELI:ETA)                 !List box control field - type derived from field
DELI:ByContainer       LIKE(DELI:ByContainer)         !List box control field - type derived from field
DELI:Length            LIKE(DELI:Length)              !List box control field - type derived from field
DELI:Breadth           LIKE(DELI:Breadth)             !List box control field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Primary key field - type derived from field
DELI:PTID              LIKE(DELI:PTID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::PACK:Record LIKE(PACK:RECORD),THREAD
QuickWindow          WINDOW('Form Packaging Types'),AT(,,213,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdatePackagingTypes'),SYSTEM
                       SHEET,AT(4,4,205,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Packaging:'),AT(9,22),USE(?PACK:Packaging:Prompt),TRN
                           ENTRY(@s35),AT(62,22,144,10),USE(PACK:Packaging),REQ
                           CHECK(' Archived'),AT(61,54),USE(PACK:Archived),MSG('Mark Packaging Type as Archived'),TIP('Mark Packa' & |
  'ging Type as Archived')
                           ENTRY(@d17),AT(62,76,60,10),USE(PACK:Archived_Date),RIGHT(1),READONLY,SKIP
                           PROMPT('Archived Date:'),AT(9,76),USE(?PACK:Archived_Date:Prompt)
                           PROMPT('Archived Time:'),AT(9,98),USE(?PACK:Archived_Time:Prompt)
                           ENTRY(@t7),AT(62,97,60,10),USE(PACK:Archived_Time),RIGHT(1),READONLY,SKIP
                         END
                         TAB('&2) Delivery Items'),USE(?Tab:2)
                           LIST,AT(9,20,197,93),USE(?Browse:2),HVSCROLL,FORMAT('32R(2)|M~Item No.~C(0)@n6@20R(2)|M' & |
  '~Type~C(0)@n3@80L(2)|M~Container No~@s35@80D(12)|M~Container Return Address~C(0)@s20' & |
  '@80L(2)|M~Container Vessel~@s35@80R(2)|M~ETA~C(0)@d6@52R(2)|M~By Container~C(0)@n3@2' & |
  '8R(2)|M~Length~C(0)@n6@32R(2)|M~Breadth~C(0)@n6@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he DeliveryItems file')
                         END
                       END
                       BUTTON('&OK'),AT(108,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(160,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Packaging Types Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Packaging Types Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_PackagingTypes')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PACK:Packaging:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(PACK:Record,History::PACK:Record)
  SELF.AddHistoryField(?PACK:Packaging,2)
  SELF.AddHistoryField(?PACK:Archived,3)
  SELF.AddHistoryField(?PACK:Archived_Date,6)
  SELF.AddHistoryField(?PACK:Archived_Time,7)
  SELF.AddUpdateFile(Access:PackagingTypes)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:DeliveryItems.SetOpenRelated()
  Relate:DeliveryItems.Open                                ! File DeliveryItems used by this procedure, so make sure it's RelationManager is open
  Access:PackagingTypes.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PackagingTypes
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:DeliveryItems,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?PACK:Packaging{PROP:ReadOnly} = True
    ?PACK:Archived_Date{PROP:ReadOnly} = True
    ?PACK:Archived_Time{PROP:ReadOnly} = True
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon DELI:PTID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,DELI:FKey_PTID)  ! Add the sort order for DELI:FKey_PTID for sort order 1
  BRW2.AddRange(DELI:PTID,Relate:DeliveryItems,Relate:PackagingTypes) ! Add file relationship range limit for sort order 1
  BRW2.AddField(DELI:ItemNo,BRW2.Q.DELI:ItemNo)            ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW2.AddField(DELI:Type,BRW2.Q.DELI:Type)                ! Field DELI:Type is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ContainerNo,BRW2.Q.DELI:ContainerNo)  ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ContainerReturnAID,BRW2.Q.DELI:ContainerReturnAID) ! Field DELI:ContainerReturnAID is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ContainerVessel,BRW2.Q.DELI:ContainerVessel) ! Field DELI:ContainerVessel is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ETA,BRW2.Q.DELI:ETA)                  ! Field DELI:ETA is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ByContainer,BRW2.Q.DELI:ByContainer)  ! Field DELI:ByContainer is a hot field or requires assignment from browse
  BRW2.AddField(DELI:Length,BRW2.Q.DELI:Length)            ! Field DELI:Length is a hot field or requires assignment from browse
  BRW2.AddField(DELI:Breadth,BRW2.Q.DELI:Breadth)          ! Field DELI:Breadth is a hot field or requires assignment from browse
  BRW2.AddField(DELI:DIID,BRW2.Q.DELI:DIID)                ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW2.AddField(DELI:PTID,BRW2.Q.DELI:PTID)                ! Field DELI:PTID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_PackagingTypes',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryItems.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_PackagingTypes',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Vessels PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::VES:Record  LIKE(VES:RECORD),THREAD
QuickWindow          WINDOW('Form Vessels'),AT(,,273,56),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('Update_Vessels'),SYSTEM
                       SHEET,AT(4,4,265,30),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Vessel:'),AT(9,20),USE(?VES:Vessel:Prompt),TRN
                           ENTRY(@s50),AT(61,20,204,10),USE(VES:Vessel),MSG('Vessel Name'),TIP('Vessel Name')
                         END
                       END
                       BUTTON('&OK'),AT(168,38,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(220,38,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,38,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Vessel Record'
  OF InsertRecord
    ActionMessage = 'Vessel Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Vessel Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  CASE SELF.Request
  OF ChangeRecord OROF DeleteRecord
    QuickWindow{PROP:Text} = QuickWindow{PROP:Text} & '  (' & VES:Vessel & ')' ! Append status message to window title text
  OF InsertRecord
    QuickWindow{PROP:Text} = QuickWindow{PROP:Text} & '  (New)'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Vessels')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?VES:Vessel:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(VES:Record,History::VES:Record)
  SELF.AddHistoryField(?VES:Vessel,2)
  SELF.AddUpdateFile(Access:Vessels)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Vessels.Open                                      ! File Vessels used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Vessels
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?VES:Vessel{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Vessels',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Vessels.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Vessels',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_PackagingTypes PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:Locator          STRING(50)                            !
LOC:Show_Archived    BYTE                                  !
LOC:Archive_Warning_Show BYTE                              !
BRW1::View:Browse    VIEW(PackagingTypes)
                       PROJECT(PACK:Packaging)
                       PROJECT(PACK:Archived)
                       PROJECT(PACK:PTID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
PACK:Packaging         LIKE(PACK:Packaging)           !List box control field - type derived from field
PACK:Archived          LIKE(PACK:Archived)            !List box control field - type derived from field
PACK:Archived_Icon     LONG                           !Entry's icon ID
LOC:Show_Archived      LIKE(LOC:Show_Archived)        !Browse hot field - type derived from local data
PACK:PTID              LIKE(PACK:PTID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Packaging Types File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('BrowsePackagingTypes'),SYSTEM
                       LIST,AT(8,36,261,118),USE(?Browse:1),HVSCROLL,ALRT(F2Key),FORMAT('150L(2)|M~Packaging~@' & |
  's35@12L(2)|MI~Archived~L(0)@p p@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Packag' & |
  'ingTypes file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       BUTTON('Merge'),AT(4,180,,14),USE(?Button_Merge),LEFT,ICON('WIZDITTO.ICO'),FLAT
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Packaging'),USE(?Tab:2)
                           PROMPT('Locator:'),AT(9,22),USE(?LOC:Locator:Prompt),TRN
                           STRING(@s50),AT(38,22),USE(LOC:Locator),TRN
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       CHECK(' &Show Archived'),AT(114,184),USE(LOC:Show_Archived)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Archive_Toggle                  ROUTINE
DATA
R:Yes   BYTE

CODE  
  BRW1.UpdateViewRecord()
  BRW1.UpdateBuffer()
  
  ! Toggle archived on highlighted...
  IF LOC:Archive_Warning_Show = 0
     IF PACK:Archived = FALSE
        CASE MESSAGE('Archive this Packaging Type?','Archive Packaging Types',ICON:Question, 'Yes|Yes (don''t warn again)|No', 3)
        OF 1
          R:Yes = TRUE
        OF 2    
          R:Yes = TRUE          
          LOC:Archive_Warning_Show  = 1
        .
      ELSE
        CASE MESSAGE('Un-Archive this Packaging Type?','Archive Packaging Types',ICON:Question, 'Yes|Yes (don''t warn again)|No', 3)
        OF 1
          R:Yes = TRUE
        OF 2    
          R:Yes = TRUE          
          LOC:Archive_Warning_Show  = 1
        .        
      .
  ELSE
    R:Yes = TRUE
  .
  
  IF R:Yes = TRUE    
    BRW1.UpdateViewRecord()
    
    BRW1.UpdateBuffer()
    IF Access:PackagingTypes.TryFetch(PACK:PKey_PTID) = Level:Benign
      IF PACK:Archived  = 1
        PACK:Archived = 0
      ELSE
        PACK:Archived = 1
        PACK:Archived_Date = TODAY()
        PACK:Archived_Time = CLOCK()
      .          
      IF Access:PackagingTypes.TryUpdate() = Level:Benign
        BRW1.ResetFromBuffer()  
  . . .    

  EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_PackagingTypes')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Show_Archived',LOC:Show_Archived)              ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:PackagingTypes.Open                               ! File PackagingTypes used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:PackagingTypes,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon PACK:Packaging for sort order 1
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,PACK:Key_Packaging) ! Add the sort order for PACK:Key_Packaging for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(?LOC:Locator,PACK:Packaging,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: PACK:Key_Packaging , PACK:Packaging
  BRW1.SetFilter('(LOC:Show_Archived = 1 OR PACK:Archived = 0)') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Show_Archived)                    ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoff.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(PACK:Packaging,BRW1.Q.PACK:Packaging)      ! Field PACK:Packaging is a hot field or requires assignment from browse
  BRW1.AddField(PACK:Archived,BRW1.Q.PACK:Archived)        ! Field PACK:Archived is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Show_Archived,BRW1.Q.LOC:Show_Archived) ! Field LOC:Show_Archived is a hot field or requires assignment from browse
  BRW1.AddField(PACK:PTID,BRW1.Q.PACK:PTID)                ! Field PACK:PTID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_PackagingTypes',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PackagingTypes.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_PackagingTypes',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_PackagingTypes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Merge
      BRW1.UpdateViewRecord()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Merge
      ThisWindow.Update()
      START(Window_Merge_Commodities, 25000, '1',PACK:PTID)
      ThisWindow.Reset
          POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      IF KEYCODE() = F2Key
        DO Archive_Toggle
      .
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF SELF.Request = SelectRecord
             DISABLE(?Button_Merge)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (PACK:Archived = 1)
    SELF.Q.PACK:Archived_Icon = 2                          ! Set icon from icon list
  ELSE
    SELF.Q.PACK:Archived_Icon = 1                          ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_StatementItems PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::STAI:Record LIKE(STAI:RECORD),THREAD
QuickWindow          WINDOW('Form Statement Items'),AT(,,184,140),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_StatementItems'),SYSTEM
                       SHEET,AT(4,4,176,114),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('STID:'),AT(9,20),USE(?STAI:STID:Prompt),TRN
                           STRING(@n_10),AT(121,20,56,10),USE(STAI:STID),RIGHT(1),TRN
                           PROMPT('Invoice Date:'),AT(9,34),USE(?STAI:InvoiceDate:Prompt),TRN
                           ENTRY(@d17),AT(121,34,56,10),USE(STAI:InvoiceDate)
                           PROMPT('Invoice Number:'),AT(9,48),USE(?STAI:IID:Prompt),TRN
                           STRING(@n_10),AT(121,48,56,10),USE(STAI:IID),RIGHT(1),TRN
                           PROMPT('DI No.:'),AT(9,62),USE(?STAI:DINo:Prompt),TRN
                           ENTRY(@n_10),AT(121,62,56,10),USE(STAI:DINo),RIGHT(1),MSG('Delivery Instruction Number'),REQ, |
  TIP('Delivery Instruction Number')
                           PROMPT('Debit:'),AT(9,76),USE(?STAI:Debit:Prompt),TRN
                           ENTRY(@n-14.2),AT(121,76,56,10),USE(STAI:Debit),RIGHT(1)
                           PROMPT('Credit:'),AT(9,90),USE(?STAI:Credit:Prompt),TRN
                           ENTRY(@n-14.2),AT(121,90,56,10),USE(STAI:Credit),RIGHT(1)
                           PROMPT('Amount:'),AT(9,104),USE(?STAI:Amount:Prompt),TRN
                           ENTRY(@n-14.2),AT(121,104,56,10),USE(STAI:Amount),RIGHT(1)
                         END
                       END
                       BUTTON('&OK'),AT(76,122,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(130,122,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,122,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    GlobalErrors.Throw(Msg:InsertIllegal)
    RETURN
  OF ChangeRecord
    GlobalErrors.Throw(Msg:UpdateIllegal)
    RETURN
  OF DeleteRecord
    GlobalErrors.Throw(Msg:DeleteIllegal)
    RETURN
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_StatementItems')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STAI:STID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(STAI:Record,History::STAI:Record)
  SELF.AddHistoryField(?STAI:STID,2)
  SELF.AddHistoryField(?STAI:InvoiceDate,5)
  SELF.AddHistoryField(?STAI:IID,7)
  SELF.AddHistoryField(?STAI:DINo,9)
  SELF.AddHistoryField(?STAI:Debit,10)
  SELF.AddHistoryField(?STAI:Credit,11)
  SELF.AddHistoryField(?STAI:Amount,12)
  SELF.AddUpdateFile(Access:_StatementItems)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:_StatementItems.Open                              ! File _StatementItems used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_StatementItems
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.InsertAction = Insert:None                        ! Inserts not allowed
    SELF.DeleteAction = Delete:None                        ! Deletes not allowed
    SELF.ChangeAction = Change:None                        ! Changes not allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?STAI:InvoiceDate{PROP:ReadOnly} = True
    ?STAI:DINo{PROP:ReadOnly} = True
    ?STAI:Debit{PROP:ReadOnly} = True
    ?STAI:Credit{PROP:ReadOnly} = True
    ?STAI:Amount{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_StatementItems',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_StatementItems.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_StatementItems',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Statement_Run_Desc PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(_Statement_Runs)
                       PROJECT(STAR:STRID)
                       PROJECT(STAR:RunDescription)
                       PROJECT(STAR:RunDate)
                       PROJECT(STAR:RunTime)
                       PROJECT(STAR:EntryDate)
                       PROJECT(STAR:EntryTime)
                       PROJECT(STAR:Complete)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
STAR:STRID             LIKE(STAR:STRID)               !List box control field - type derived from field
STAR:RunDescription    LIKE(STAR:RunDescription)      !List box control field - type derived from field
STAR:RunDate           LIKE(STAR:RunDate)             !List box control field - type derived from field
STAR:RunTime           LIKE(STAR:RunTime)             !List box control field - type derived from field
STAR:EntryDate         LIKE(STAR:EntryDate)           !List box control field - type derived from field
STAR:EntryTime         LIKE(STAR:EntryTime)           !List box control field - type derived from field
STAR:Complete          LIKE(STAR:Complete)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::STDES:Record LIKE(STDES:RECORD),THREAD
QuickWindow          WINDOW('Form Statement Run Desc.'),AT(,,228,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_Statement_Run_Desc'),SYSTEM
                       SHEET,AT(4,4,220,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Run Description:'),AT(9,25),USE(?STDES:RunDescription:Prompt),TRN
                           ENTRY(@s35),AT(68,25,144,10),USE(STDES:RunDescription),MSG('Run Description'),TIP('Run Description')
                         END
                         TAB('&2) Statement Runs'),USE(?Tab:2)
                           LIST,AT(9,20,212,74),USE(?Browse:2),HVSCROLL,FORMAT('40R(2)|M~STRID~C(0)@n_10@60L(2)|M~' & |
  'Run Description~@s35@48R(2)|M~Run Date~C(0)@d17@40R(2)|M~Run Time~C(0)@t8@48R(2)|M~E' & |
  'ntry Date~C(0)@d17@40R(2)|M~Entry Time~C(0)@t8@36R(2)|M~Complete~C(0)@n3@'),FROM(Queue:Browse:2), |
  IMM,MSG('Browsing the _Statement_Runs file')
                           BUTTON('&Insert'),AT(9,98,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),DISABLE,FLAT,MSG('Insert a Record'), |
  SKIP,TIP('Insert a Record')
                           BUTTON('&Change'),AT(61,98,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),DISABLE,FLAT,HIDE, |
  MSG('Change the Record'),TIP('Change the Record')
                           BUTTON('&Delete'),AT(115,98,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),DISABLE,FLAT,HIDE, |
  MSG('Delete the Record'),TIP('Delete the Record')
                           BUTTON('&View'),AT(167,98,53,14),USE(?View),LEFT,ICON('waview.ico'),FLAT
                         END
                       END
                       BUTTON('&OK'),AT(122,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(176,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetAlerts              PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Statement Run Desc. Record'
  OF InsertRecord
    ActionMessage = 'Adding a Statement Run Desc. Record'
  OF ChangeRecord
    ActionMessage = 'Changing a Statement Run Desc. Record'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Statement_Run_Desc')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STDES:RunDescription:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(STDES:Record,History::STDES:Record)
  SELF.AddHistoryField(?STDES:RunDescription,2)
  SELF.AddUpdateFile(Access:_Statement_Run_Desc)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:_Statement_Run_Desc.SetOpenRelated()
  Relate:_Statement_Run_Desc.Open                          ! File _Statement_Run_Desc used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_Statement_Run_Desc
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:_Statement_Runs,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?STDES:RunDescription{PROP:ReadOnly} = True
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    DISABLE(?View)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,STAR:FKey_RunDesc)                    ! Add the sort order for STAR:FKey_RunDesc for sort order 1
  BRW2.AddRange(STAR:RunDescription,Relate:_Statement_Runs,Relate:_Statement_Run_Desc) ! Add file relationship range limit for sort order 1
  BRW2.AddField(STAR:STRID,BRW2.Q.STAR:STRID)              ! Field STAR:STRID is a hot field or requires assignment from browse
  BRW2.AddField(STAR:RunDescription,BRW2.Q.STAR:RunDescription) ! Field STAR:RunDescription is a hot field or requires assignment from browse
  BRW2.AddField(STAR:RunDate,BRW2.Q.STAR:RunDate)          ! Field STAR:RunDate is a hot field or requires assignment from browse
  BRW2.AddField(STAR:RunTime,BRW2.Q.STAR:RunTime)          ! Field STAR:RunTime is a hot field or requires assignment from browse
  BRW2.AddField(STAR:EntryDate,BRW2.Q.STAR:EntryDate)      ! Field STAR:EntryDate is a hot field or requires assignment from browse
  BRW2.AddField(STAR:EntryTime,BRW2.Q.STAR:EntryTime)      ! Field STAR:EntryTime is a hot field or requires assignment from browse
  BRW2.AddField(STAR:Complete,BRW2.Q.STAR:Complete)        ! Field STAR:Complete is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Statement_Run_Desc',QuickWindow)    ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Statement_Run_Desc.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Statement_Run_Desc',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.ViewControl = ?View                                 ! Setup the control used to initiate view only mode


BRW2.SetAlerts PROCEDURE

  CODE
  SELF.EditViaPopup = False
  PARENT.SetAlerts


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_RemittanceItems PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::REMIT:Record LIKE(REMIT:RECORD),THREAD
QuickWindow          WINDOW('Form Remittance Items'),AT(,,176,112),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_RemittanceItems'),SYSTEM
                       SHEET,AT(4,4,168,86),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('REMID:'),AT(9,20),USE(?REMIT:REMID:Prompt),TRN
                           STRING(@n_10),AT(65,20,64,10),USE(REMIT:REMID),RIGHT(1),TRN
                           PROMPT('Invoice Date:'),AT(9,34),USE(?REMIT:InvoiceDate:Prompt),TRN
                           ENTRY(@d17),AT(65,34,64,10),USE(REMIT:InvoiceDate)
                           PROMPT('Invoice Time:'),AT(9,48),USE(?REMIT:InvoiceTime:Prompt),TRN
                           ENTRY(@t7),AT(65,48,64,10),USE(REMIT:InvoiceTime)
                           PROMPT('MID:'),AT(9,62),USE(?REMIT:MID:Prompt),TRN
                           STRING(@n_10),AT(65,62,64,10),USE(REMIT:MID),RIGHT(1),TRN
                           PROMPT('Amount Paid:'),AT(9,76),USE(?REMIT:AmountPaid:Prompt),TRN
                           ENTRY(@n-14.2),AT(65,76,64,10),USE(REMIT:AmountPaid),RIGHT(1)
                         END
                       END
                       BUTTON('&OK'),AT(68,94,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(122,94,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(123,4,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Remittance Itemsl Record'
  OF InsertRecord
    ActionMessage = 'Remittance Items Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Remittance Items Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_RemittanceItems')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REMIT:REMID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(REMIT:Record,History::REMIT:Record)
  SELF.AddHistoryField(?REMIT:REMID,2)
  SELF.AddHistoryField(?REMIT:InvoiceDate,5)
  SELF.AddHistoryField(?REMIT:InvoiceTime,6)
  SELF.AddHistoryField(?REMIT:MID,8)
  SELF.AddHistoryField(?REMIT:AmountPaid,9)
  SELF.AddUpdateFile(Access:_RemittanceItems)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:_RemittanceItems.SetOpenRelated()
  Relate:_RemittanceItems.Open                             ! File _RemittanceItems used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_RemittanceItems
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?REMIT:InvoiceDate{PROP:ReadOnly} = True
    ?REMIT:InvoiceTime{PROP:ReadOnly} = True
    ?REMIT:AmountPaid{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_RemittanceItems',QuickWindow)       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_RemittanceItems.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_RemittanceItems',QuickWindow)    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Remittance PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(_RemittanceItems)
                       PROJECT(REMIT:REMID)
                       PROJECT(REMIT:InvoiceDate)
                       PROJECT(REMIT:InvoiceTime)
                       PROJECT(REMIT:MID)
                       PROJECT(REMIT:AmountPaid)
                       PROJECT(REMIT:REMIID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
REMIT:REMID            LIKE(REMIT:REMID)              !List box control field - type derived from field
REMIT:InvoiceDate      LIKE(REMIT:InvoiceDate)        !List box control field - type derived from field
REMIT:InvoiceTime      LIKE(REMIT:InvoiceTime)        !List box control field - type derived from field
REMIT:MID              LIKE(REMIT:MID)                !List box control field - type derived from field
REMIT:AmountPaid       LIKE(REMIT:AmountPaid)         !List box control field - type derived from field
REMIT:REMIID           LIKE(REMIT:REMIID)             !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::REMI:Record LIKE(REMI:RECORD),THREAD
QuickWindow          WINDOW('Form Remittance'),AT(,,287,243),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('Update_Remittance'),SYSTEM
                       SHEET,AT(4,4,279,217),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           GROUP,AT(145,132,132,52),USE(?Group1),HIDE
                             PROMPT('90 Days:'),AT(145,132),USE(?REMI:Days90:Prompt),TRN
                             ENTRY(@n-14.2),AT(213,132,64,10),USE(REMI:Days90),RIGHT(1),MSG('90 Days'),TIP('90 Days')
                             PROMPT('60 Days:'),AT(145,146),USE(?REMI:Days60:Prompt),TRN
                             ENTRY(@n-14.2),AT(213,146,64,10),USE(REMI:Days60),RIGHT(1),MSG('60 Days'),TIP('60 Days')
                             PROMPT('30 Days:'),AT(145,161),USE(?REMI:Days30:Prompt),TRN
                             ENTRY(@n-14.2),AT(213,161,64,10),USE(REMI:Days30),RIGHT(1),MSG('30 Das'),TIP('30 Das')
                             PROMPT('Current:'),AT(145,174),USE(?REMI:Current:Prompt),TRN
                             ENTRY(@n-14.2),AT(213,174,64,10),USE(REMI:Current),RIGHT(1),MSG('Current'),TIP('Current')
                           END
                           PROMPT('REMID:'),AT(9,20),USE(?REMI:REMID:Prompt),TRN
                           STRING(@n_10),AT(77,20,64,10),USE(REMI:REMID),RIGHT(1),TRN
                           PROMPT('RERID:'),AT(9,34),USE(?REMI:RERID:Prompt),TRN
                           STRING(@n_10),AT(77,34,64,10),USE(REMI:RERID),RIGHT(1),TRN
                           PROMPT('Remittance Date:'),AT(9,48),USE(?REMI:RemittanceDate:Prompt),TRN
                           ENTRY(@d17),AT(77,48,64,10),USE(REMI:RemittanceDate),MSG('Statement Date'),TIP('Statement Date')
                           PROMPT('Remittance Time:'),AT(9,62),USE(?REMI:RemittanceTime:Prompt),TRN
                           ENTRY(@t7),AT(77,62,64,10),USE(REMI:RemittanceTime)
                           PROMPT('TID:'),AT(9,76),USE(?REMI:TID:Prompt),TRN
                           STRING(@n_10),AT(77,76,64,10),USE(REMI:TID),RIGHT(1),TRN
                           PROMPT('BID:'),AT(9,90),USE(?REMI:BID:Prompt),TRN
                           STRING(@n_10),AT(77,90,64,10),USE(REMI:BID),RIGHT(1),TRN
                           PROMPT('Total:'),AT(9,202),USE(?REMI:Total:Prompt),TRN
                           ENTRY(@n-14.2),AT(77,202,64,10),USE(REMI:Total),RIGHT(1),MSG('Total'),TIP('Total')
                         END
                         TAB('&2) Remittance Items'),USE(?Tab:3)
                           LIST,AT(9,20,270,179),USE(?Browse:2),HVSCROLL,FORMAT('30R(2)|M~REM ID~C(0)@n_10@[38R(2)' & |
  '|M~Date~C(0)@d5b@36R(2)|M~Time~C(0)@t7@]|M~Invoice~30R(2)|M~MID~C(0)@n_10@64R(1)|M~A' & |
  'mount Paid~C(0)@n-14.2@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the _RemittanceItems file')
                           BUTTON('&Insert'),AT(125,204,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(177,204,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(231,204,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(182,226,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(234,226,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(234,2,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Remittance Record'
  OF InsertRecord
    ActionMessage = 'Remittance Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Remittance Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Remittance')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REMI:Days90:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(REMI:Record,History::REMI:Record)
  SELF.AddHistoryField(?REMI:Days90,9)
  SELF.AddHistoryField(?REMI:Days60,10)
  SELF.AddHistoryField(?REMI:Days30,11)
  SELF.AddHistoryField(?REMI:Current,12)
  SELF.AddHistoryField(?REMI:REMID,1)
  SELF.AddHistoryField(?REMI:RERID,2)
  SELF.AddHistoryField(?REMI:RemittanceDate,5)
  SELF.AddHistoryField(?REMI:RemittanceTime,6)
  SELF.AddHistoryField(?REMI:TID,7)
  SELF.AddHistoryField(?REMI:BID,8)
  SELF.AddHistoryField(?REMI:Total,13)
  SELF.AddUpdateFile(Access:_Remittance)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:_Remittance.SetOpenRelated()
  Relate:_Remittance.Open                                  ! File _Remittance used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_Remittance
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:_RemittanceItems,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?REMI:Days90{PROP:ReadOnly} = True
    ?REMI:Days60{PROP:ReadOnly} = True
    ?REMI:Days30{PROP:ReadOnly} = True
    ?REMI:Current{PROP:ReadOnly} = True
    ?REMI:RemittanceDate{PROP:ReadOnly} = True
    ?REMI:RemittanceTime{PROP:ReadOnly} = True
    ?REMI:Total{PROP:ReadOnly} = True
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,REMIT:FKey_REMID)                     ! Add the sort order for REMIT:FKey_REMID for sort order 1
  BRW2.AddRange(REMIT:REMID,Relate:_RemittanceItems,Relate:_Remittance) ! Add file relationship range limit for sort order 1
  BRW2.AddField(REMIT:REMID,BRW2.Q.REMIT:REMID)            ! Field REMIT:REMID is a hot field or requires assignment from browse
  BRW2.AddField(REMIT:InvoiceDate,BRW2.Q.REMIT:InvoiceDate) ! Field REMIT:InvoiceDate is a hot field or requires assignment from browse
  BRW2.AddField(REMIT:InvoiceTime,BRW2.Q.REMIT:InvoiceTime) ! Field REMIT:InvoiceTime is a hot field or requires assignment from browse
  BRW2.AddField(REMIT:MID,BRW2.Q.REMIT:MID)                ! Field REMIT:MID is a hot field or requires assignment from browse
  BRW2.AddField(REMIT:AmountPaid,BRW2.Q.REMIT:AmountPaid)  ! Field REMIT:AmountPaid is a hot field or requires assignment from browse
  BRW2.AddField(REMIT:REMIID,BRW2.Q.REMIT:REMIID)          ! Field REMIT:REMIID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Remittance',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Remittance.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Remittance',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_RemittanceItems
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Remittance_Runs PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(_Remittance)
                       PROJECT(REMI:REMID)
                       PROJECT(REMI:RERID)
                       PROJECT(REMI:RemittanceDate)
                       PROJECT(REMI:RemittanceTime)
                       PROJECT(REMI:TID)
                       PROJECT(REMI:BID)
                       PROJECT(REMI:Days90)
                       PROJECT(REMI:Days60)
                       PROJECT(REMI:Days30)
                       PROJECT(REMI:Current)
                       PROJECT(REMI:Total)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
REMI:REMID             LIKE(REMI:REMID)               !List box control field - type derived from field
REMI:RERID             LIKE(REMI:RERID)               !List box control field - type derived from field
REMI:RemittanceDate    LIKE(REMI:RemittanceDate)      !List box control field - type derived from field
REMI:RemittanceTime    LIKE(REMI:RemittanceTime)      !List box control field - type derived from field
REMI:TID               LIKE(REMI:TID)                 !List box control field - type derived from field
REMI:BID               LIKE(REMI:BID)                 !List box control field - type derived from field
REMI:Days90            LIKE(REMI:Days90)              !List box control field - type derived from field
REMI:Days60            LIKE(REMI:Days60)              !List box control field - type derived from field
REMI:Days30            LIKE(REMI:Days30)              !List box control field - type derived from field
REMI:Current           LIKE(REMI:Current)             !List box control field - type derived from field
REMI:Total             LIKE(REMI:Total)               !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::REMR:Record LIKE(REMR:RECORD),THREAD
QuickWindow          WINDOW('Form Remittance Runs'),AT(,,240,209),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Update_Remittance_Runs'),SYSTEM
                       SHEET,AT(4,4,232,188),USE(?CurrentTab),FONT('Tahoma')
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('RERID:'),AT(8,20),USE(?REMR:RERID:Prompt),TRN
                           STRING(@n_10),AT(88,20,104,10),USE(REMR:RERID),TRN
                           PROMPT('Run Description:'),AT(8,34),USE(?REMR:RunDescription:Prompt),TRN
                           ENTRY(@s35),AT(88,34,144,10),USE(REMR:RunDescription),MSG('Run Description'),TIP('Run Description')
                           PROMPT('Run Date:'),AT(8,48),USE(?REMR:RunDate:Prompt),TRN
                           ENTRY(@d17),AT(88,48,104,10),USE(REMR:RunDate),MSG('Statement Run Date'),TIP('Statement Run Date')
                           PROMPT('Run Time:'),AT(8,62),USE(?REMR:RunTime:Prompt),TRN
                           ENTRY(@t7),AT(88,62,104,10),USE(REMR:RunTime)
                           PROMPT('Entry Date:'),AT(8,76),USE(?REMR:EntryDate:Prompt),TRN
                           ENTRY(@d17),AT(88,76,104,10),USE(REMR:EntryDate),MSG('Run done at'),TIP('Run done at')
                           PROMPT('Entry Time:'),AT(8,90),USE(?REMR:EntryTime:Prompt),TRN
                           ENTRY(@t7),AT(88,90,104,10),USE(REMR:EntryTime)
                           CHECK(' Complete'),AT(88,104,70,8),USE(REMR:Complete),MSG('Complete'),TIP('Complete')
                           PROMPT('Type:'),AT(8,116),USE(?REMR:Type:Prompt),TRN
                           LIST,AT(88,116,104,10),USE(REMR:Type),DROP(5),FROM('Transporter Monthly|#0|Transporter ' & |
  'Adhoc|#1|Internal Adhoc (not on web)|#20'),MSG('Transporter, Adhoc'),TIP('Transporter, Adhoc')
                           PROMPT('Payments From Date:'),AT(8,136),USE(?REMR:PaymentsFromDate:Prompt),TRN
                           ENTRY(@d6b),AT(88,136,104,10),USE(REMR:PaymentsFromDate)
                           PROMPT('Payments From Time:'),AT(8,150),USE(?REMR:PaymentsFromTime:Prompt),TRN
                           ENTRY(@t7),AT(88,150,104,10),USE(REMR:PaymentsFromTime)
                           PROMPT('Payments To Date:'),AT(8,164),USE(?REMR:PaymentsToDate:Prompt),TRN
                           ENTRY(@d6b),AT(88,164,104,10),USE(REMR:PaymentsToDate)
                           PROMPT('Payments To Time:'),AT(8,178),USE(?REMR:PaymentsToTime:Prompt),TRN
                           ENTRY(@t7),AT(88,178,104,10),USE(REMR:PaymentsToTime)
                         END
                         TAB('&2) Remittance'),USE(?Tab:3)
                           LIST,AT(8,20,224,116),USE(?Browse:2),HVSCROLL,FORMAT('40R(2)|M~REM ID~C(0)@n_10@40R(2)|' & |
  'M~RER ID~C(0)@n_10@[48R(2)|M~Date~C(0)@d17@36R(2)|M~Time~C(0)@t8@]|M~Remittance~40R(' & |
  '2)|M~TID~C(0)@n_10@40R(2)|M~BID~C(0)@n_10@50R(1)|M~90 Days~C(0)@n-14.2@50R(1)|M~60 D' & |
  'ays~C(0)@n-14.2@50R(1)|M~30 Days~C(0)@n-14.2@50R(1)|M~Current~C(0)@n-14.2@50R(1)|M~T' & |
  'otal~C(0)@n-14.2@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the _Remittance file')
                           BUTTON('&Insert'),AT(77,140,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(130,140,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(183,140,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(132,194,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(186,194,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,194,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Vessel Record'
  OF InsertRecord
    ActionMessage = 'Vessel Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Vessel Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Remittance_Runs')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REMR:RERID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(REMR:Record,History::REMR:Record)
  SELF.AddHistoryField(?REMR:RERID,1)
  SELF.AddHistoryField(?REMR:RunDescription,2)
  SELF.AddHistoryField(?REMR:RunDate,5)
  SELF.AddHistoryField(?REMR:RunTime,6)
  SELF.AddHistoryField(?REMR:EntryDate,9)
  SELF.AddHistoryField(?REMR:EntryTime,10)
  SELF.AddHistoryField(?REMR:Complete,11)
  SELF.AddHistoryField(?REMR:Type,12)
  SELF.AddHistoryField(?REMR:PaymentsFromDate,16)
  SELF.AddHistoryField(?REMR:PaymentsFromTime,17)
  SELF.AddHistoryField(?REMR:PaymentsToDate,20)
  SELF.AddHistoryField(?REMR:PaymentsToTime,21)
  SELF.AddUpdateFile(Access:_Remittance_Runs)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:_Remittance.SetOpenRelated()
  Relate:_Remittance.Open                                  ! File _Remittance used by this procedure, so make sure it's RelationManager is open
  Access:_Remittance_Runs.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_Remittance_Runs
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:_Remittance,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?REMR:RunDescription{PROP:ReadOnly} = True
    ?REMR:RunDate{PROP:ReadOnly} = True
    ?REMR:RunTime{PROP:ReadOnly} = True
    ?REMR:EntryDate{PROP:ReadOnly} = True
    ?REMR:EntryTime{PROP:ReadOnly} = True
    DISABLE(?REMR:Type)
    ?REMR:PaymentsFromDate{PROP:ReadOnly} = True
    ?REMR:PaymentsFromTime{PROP:ReadOnly} = True
    ?REMR:PaymentsToDate{PROP:ReadOnly} = True
    ?REMR:PaymentsToTime{PROP:ReadOnly} = True
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,REMI:FKey_RERID)                      ! Add the sort order for REMI:FKey_RERID for sort order 1
  BRW2.AddRange(REMI:RERID,Relate:_Remittance,Relate:_Remittance_Runs) ! Add file relationship range limit for sort order 1
  BRW2.AddField(REMI:REMID,BRW2.Q.REMI:REMID)              ! Field REMI:REMID is a hot field or requires assignment from browse
  BRW2.AddField(REMI:RERID,BRW2.Q.REMI:RERID)              ! Field REMI:RERID is a hot field or requires assignment from browse
  BRW2.AddField(REMI:RemittanceDate,BRW2.Q.REMI:RemittanceDate) ! Field REMI:RemittanceDate is a hot field or requires assignment from browse
  BRW2.AddField(REMI:RemittanceTime,BRW2.Q.REMI:RemittanceTime) ! Field REMI:RemittanceTime is a hot field or requires assignment from browse
  BRW2.AddField(REMI:TID,BRW2.Q.REMI:TID)                  ! Field REMI:TID is a hot field or requires assignment from browse
  BRW2.AddField(REMI:BID,BRW2.Q.REMI:BID)                  ! Field REMI:BID is a hot field or requires assignment from browse
  BRW2.AddField(REMI:Days90,BRW2.Q.REMI:Days90)            ! Field REMI:Days90 is a hot field or requires assignment from browse
  BRW2.AddField(REMI:Days60,BRW2.Q.REMI:Days60)            ! Field REMI:Days60 is a hot field or requires assignment from browse
  BRW2.AddField(REMI:Days30,BRW2.Q.REMI:Days30)            ! Field REMI:Days30 is a hot field or requires assignment from browse
  BRW2.AddField(REMI:Current,BRW2.Q.REMI:Current)          ! Field REMI:Current is a hot field or requires assignment from browse
  BRW2.AddField(REMI:Total,BRW2.Q.REMI:Total)              ! Field REMI:Total is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Remittance_Runs',QuickWindow)       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Remittance.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Remittance_Runs',QuickWindow)    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Remittance
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

