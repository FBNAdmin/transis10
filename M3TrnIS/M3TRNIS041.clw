

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS041.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Clients_OverTime_old PROCEDURE 

LOC:Criteria_Group   GROUP,PRE(L_CG)                       !
Periods              BYTE                                  !Years, Quarters, Months, Weeks, Days
Trend_Periods        BYTE                                  !
FromDate             DATE                                  !
ToDate               DATE                                  !
                     END                                   !
Data_Q               QUEUE,PRE(L_DQ)                       !
ClientNo             ULONG                                 !Client No.
ClientName           STRING(100)                           !
Period1Value         ULONG                                 !
Period2Value         DECIMAL(14,2)                         !
CID                  ULONG                                 !Client ID
                     END                                   !
Window               WINDOW('Clients Over Time'),AT(,,649,379),FONT('MS Sans Serif',8,,FONT:regular),RESIZE,GRAY, |
  MAX,MDI,SYSTEM,IMM
                       BUTTON('&Close'),AT(577,349,63,24),USE(?CancelButton),LEFT,ICON('WACancel.ico'),STD(STD:Close)
                       SHEET,AT(6,5,635,340),USE(?SHEET1)
                         TAB('Criteria'),USE(?TAB1)
                           LIST,AT(381,87,,9),USE(L_CG:Periods),DISABLE,DROP(5),FROM('Years|#1|Quarters|#2|Months|' & |
  '#3|Weeks|#4|Days|#5'),MSG('Years, Quarters, Months, Weeks, Days'),TIP('Years, Quarte' & |
  'rs, Months, Weeks, Days')
                           PROMPT('Periods:'),AT(336,87),USE(?L_CG:Periods:Prompt),DISABLE,TRN
                           PROMPT('From Date:'),AT(28,34),USE(?L_CG:FromDate:Prompt)
                           SPIN(@d6b),AT(73,33,60,10),USE(L_CG:FromDate),RIGHT(1)
                           PROMPT('To Date:'),AT(28,54),USE(?L_CG:ToDate:Prompt)
                           SPIN(@d6b),AT(73,52,60,10),USE(L_CG:ToDate),RIGHT(1)
                           OPTION('Trend Periods:'),AT(381,111),USE(L_CG:Trend_Periods),BOXED,DISABLE
                             RADIO('Half'),AT(389,126),USE(?L_CG:Trend_Periods:Radio1),VALUE('0')
                             RADIO('?'),AT(389,139),USE(?L_CG:Trend_Periods:Radio2),VALUE('1')
                           END
                           BUTTON('&Run'),AT(73,87,63,24),USE(?BUTTON_Run),LEFT,ICON('WAOk.ico')
                           STRING(''),AT(149,51,119,11),USE(?STRING1),FONT('Microsoft Sans Serif',,,FONT:regular)
                         END
                         TAB('Data'),USE(?TAB2),LAYOUT(0)
                           LIST,AT(16,29,611,306),USE(?LIST1),VSCROLL,FORMAT('50R(2)|M~Client No.~C(0)@s255@250L(2' & |
  ')|M~Client Name~C(0)@s255@100R(2)|M~Period 1 Value~C(0)@n-14.2@100R(2)|M~Period 2 Va' & |
  'lue~C(0)@n-14.2@'),FROM(Data_Q)
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
                    MAP
Get_Data              PROCEDURE()
                    .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Clients_OverTime_old')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CancelButton
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:_Invoice.SetOpenRelated()
  Relate:_Invoice.Open                                     ! File _Invoice used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Clients_OverTime_old',Window)              ! Restore window settings from non-volatile store
    L_CG:FromDate = GETINI('Clients_OverTime', 'FromDate', TODAY()-365, GLO:Global_INI)
    L_CG:ToDate   = GETINI('Clients_OverTime', 'ToDate', TODAY(), GLO:Global_INI)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Invoice.Close
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('Clients_OverTime_old',Window)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_CG:FromDate
        ?String1{PROP:Text} = 'Mid: ' & FORMAT(L_CG:ToDate - ((L_CG:ToDate - L_CG:FromDate) /2), @d6)
    OF ?L_CG:ToDate
        ?String1{PROP:Text} = 'Mid: ' & FORMAT(L_CG:ToDate - ((L_CG:ToDate - L_CG:FromDate) /2), @d6)
    OF ?BUTTON_Run
      ThisWindow.Update
        PUTINI('Clients_OverTime', 'FromDate', L_CG:FromDate, GLO:Global_INI)
        PUTINI('Clients_OverTime', 'ToDate', L_CG:ToDate, GLO:Global_INI)
        Get_Data()
        SELECT(?TAB2)
    OF ?LIST1
      GET(Data_Q, CHOICE(?LIST1)
      IF ~ERRORCODE()
        ! drill down to client
        CLEAR(CLI:Record)
        CLI:CID = Data_Q.CID
        IF Access:Clients.TryFetch(CLI:PKey_CID) = Level:Benign
          ! cant call update client from here as is in parent
          GlobalRequest = ChangeRecord    
          Update_Client()
        .
      .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_CG:FromDate
        ?String1{PROP:Text} = 'Mid: ' & FORMAT(L_CG:ToDate - ((L_CG:ToDate - L_CG:FromDate) /2), @d6)
    OF ?L_CG:ToDate
        ?String1{PROP:Text} = 'Mid: ' & FORMAT(L_CG:ToDate - ((L_CG:ToDate - L_CG:FromDate) /2), @d6)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Get_Data            PROCEDURE

__SQLTemp        FILE,DRIVER('MSSQL'),OWNER(GLO:DBOwner),NAME('dbo.__SQLTemp'),PRE(__SQ),CREATE
record            RECORD
S1                  STRING(255)
S2                  STRING(255)
S3                  STRING(255)
S4                  STRING(255)
S5                  STRING(255)
                  END
                END


L:DaysMid       LONG
CODE
  FREE(Data_Q)
  
  !Periods         BYTE !Years, Quarters, Months, Weeks, Days
  L:DaysMid     = (L_CG:ToDate - L_CG:FromDate) / 2
    
  CREATE(__SQLTemp)
  OPEN(__SQLTemp)
  
  
  CLEAR(__SQ:Record)
  __SQLTemp{PROP:SQL}  = 'SELECT c.ClientNo, c.ClientName, a.CID1, a.Tot1, b.Tot2 FROM ' & |
    '(SELECT CID as CID1, SUM(Total) as Tot1 FROM _Invoice WHERE InvoiceDateAndTime >= ' & | 
    SQL_Get_DateT_G(L_CG:FromDate) & |         
  ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(L_CG:ToDate - L:DaysMid) & |    
  ' GROUP BY CID) as a' & |
  ' JOIN ' & | 
   '(SELECT CID as CID2, SUM(Total) as Tot2 FROM _Invoice WHERE InvoiceDateAndTime >= ' & SQL_Get_DateT_G(L_CG:ToDate - L:DaysMid) & | 
  ' AND InvoiceDateAndTime <= ' & SQL_Get_DateT_G(L_CG:ToDate) & |    
  ' GROUP BY CID) as b ' & |
    'ON a.CID1 = b.CID2 ' & |
    'join Clients as c on a.CID1 = c.CID'
  
  SETCLIPBOARD(CLIP(__SQLTemp{PROP:SQL}))
  
  IF ERRORCODE()
    SETCLIPBOARD(CLIP(__SQLTemp{PROP:SQL}))
    MESSAGE('SQL error?  ' & ERROR() & '||SQL: ' & CLIP(__SQLTemp{PROP:SQL}))
  ELSE 
    LOOP
      NEXT(__SQLTemp)
      IF ERRORCODE()
        BREAK 
      . 
        
      L_DQ:ClientNo       = DEFORMAT(__SQ:S1)
      L_DQ:ClientName     = __SQ:S2
      L_DQ:ValuePeriod1   = DEFORMAT(__SQ:S4)
      L_DQ:ValuePeriod2   = DEFORMAT(__SQ:S5)
      L_DQ:CID            = DEFORMAT(__SQ:S3)
      ADD(Data_Q)  
  .  .
    
  CLOSE(__SQLTemp)
  RETURN
  
  
