

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS015.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Rate_Updates PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW5::View:Browse    VIEW(__Rates)
                       PROJECT(RAT:ToMass)
                       PROJECT(RAT:RatePerKg)
                       PROJECT(RAT:MinimiumCharge)
                       PROJECT(RAT:Effective_Date)
                       PROJECT(RAT:Added_Date)
                       PROJECT(RAT:AdHoc)
                       PROJECT(RAT:RID)
                       PROJECT(RAT:RUBID)
                       PROJECT(RAT:CTID)
                       PROJECT(RAT:CRTID)
                       PROJECT(RAT:JID)
                       PROJECT(RAT:CID)
                       JOIN(CTYP:PKey_CTID,RAT:CTID)
                         PROJECT(CTYP:ContainerType)
                         PROJECT(CTYP:CTID)
                       END
                       JOIN(CRT:PKey_CRTID,RAT:CRTID)
                         PROJECT(CRT:ClientRateType)
                         PROJECT(CRT:CRTID)
                       END
                       JOIN(JOU:PKey_JID,RAT:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                       JOIN(CLI:PKey_CID,RAT:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
CRT:ClientRateType     LIKE(CRT:ClientRateType)       !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
RAT:ToMass             LIKE(RAT:ToMass)               !List box control field - type derived from field
RAT:RatePerKg          LIKE(RAT:RatePerKg)            !List box control field - type derived from field
RAT:MinimiumCharge     LIKE(RAT:MinimiumCharge)       !List box control field - type derived from field
RAT:Effective_Date     LIKE(RAT:Effective_Date)       !List box control field - type derived from field
RAT:Added_Date         LIKE(RAT:Added_Date)           !List box control field - type derived from field
RAT:AdHoc              LIKE(RAT:AdHoc)                !List box control field - type derived from field
CTYP:ContainerType     LIKE(CTYP:ContainerType)       !List box control field - type derived from field
RAT:RID                LIKE(RAT:RID)                  !Primary key field - type derived from field
RAT:RUBID              LIKE(RAT:RUBID)                !Browse key field - type derived from field
CTYP:CTID              LIKE(CTYP:CTID)                !Related join file key field - type derived from field
CRT:CRTID              LIKE(CRT:CRTID)                !Related join file key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(__RatesFuelSurcharge)
                       PROJECT(FSRA:Effective_Date)
                       PROJECT(FSRA:FuelSurcharge)
                       PROJECT(FSRA:FCID)
                       PROJECT(FSRA:RUBID)
                       PROJECT(FSRA:CID)
                       JOIN(CLI:PKey_CID,FSRA:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
FSRA:Effective_Date    LIKE(FSRA:Effective_Date)      !List box control field - type derived from field
FSRA:FuelSurcharge     LIKE(FSRA:FuelSurcharge)       !List box control field - type derived from field
FSRA:FCID              LIKE(FSRA:FCID)                !Primary key field - type derived from field
FSRA:RUBID             LIKE(FSRA:RUBID)               !Browse key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(Clients_ContainerParkDiscounts)
                       PROJECT(CLI_CP:Effective_Date)
                       PROJECT(CLI_CP:ContainerParkRateDiscount)
                       PROJECT(CLI_CP:ContainerParkMinimium)
                       PROJECT(CLI_CP:CPDID)
                       PROJECT(CLI_CP:RUBID)
                       PROJECT(CLI_CP:FID)
                       PROJECT(CLI_CP:CID)
                       JOIN(FLO:PKey_FID,CLI_CP:FID)
                         PROJECT(FLO:Floor)
                         PROJECT(FLO:FID)
                       END
                       JOIN(CLI:PKey_CID,CLI_CP:CID)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI_CP:Effective_Date  LIKE(CLI_CP:Effective_Date)    !List box control field - type derived from field
CLI_CP:ContainerParkRateDiscount LIKE(CLI_CP:ContainerParkRateDiscount) !List box control field - type derived from field
CLI_CP:ContainerParkMinimium LIKE(CLI_CP:ContainerParkMinimium) !List box control field - type derived from field
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
CLI_CP:CPDID           LIKE(CLI_CP:CPDID)             !Primary key field - type derived from field
CLI_CP:RUBID           LIKE(CLI_CP:RUBID)             !Browse key field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Related join file key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(__RatesContainerPark)
                       PROJECT(CPRA:ToMass)
                       PROJECT(CPRA:RatePerKg)
                       PROJECT(CPRA:CPID)
                       PROJECT(CPRA:RUBID)
                       PROJECT(CPRA:JID)
                       PROJECT(CPRA:FID)
                       JOIN(JOU:PKey_JID,CPRA:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                       JOIN(FLO:PKey_FID,CPRA:FID)
                         PROJECT(FLO:Floor)
                         PROJECT(FLO:FID)
                       END
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
CPRA:ToMass            LIKE(CPRA:ToMass)              !List box control field - type derived from field
CPRA:RatePerKg         LIKE(CPRA:RatePerKg)           !List box control field - type derived from field
CPRA:CPID              LIKE(CPRA:CPID)                !Primary key field - type derived from field
CPRA:RUBID             LIKE(CPRA:RUBID)               !Browse key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(__RatesAdditionalCharges)
                       PROJECT(CARA:Charge)
                       PROJECT(CARA:ACID)
                       PROJECT(CARA:RUBID)
                       PROJECT(CARA:ACCID)
                       PROJECT(CARA:CID)
                       JOIN(ACCA:PKey_ACCID,CARA:ACCID)
                         PROJECT(ACCA:Description)
                         PROJECT(ACCA:ACCID)
                       END
                       JOIN(CLI:PKey_CID,CARA:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?List:5
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
ACCA:Description       LIKE(ACCA:Description)         !List box control field - type derived from field
CARA:Charge            LIKE(CARA:Charge)              !List box control field - type derived from field
CARA:ACID              LIKE(CARA:ACID)                !Primary key field - type derived from field
CARA:RUBID             LIKE(CARA:RUBID)               !Browse key field - type derived from field
ACCA:ACCID             LIKE(ACCA:ACCID)               !Related join file key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::RATU:Record LIKE(RATU:RECORD),THREAD
QuickWindow          WINDOW('Form Rate Batch Update'),AT(,,371,295),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('Update__RateUpdates'),SYSTEM
                       SHEET,AT(4,4,364,272),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           GROUP,AT(9,22,300,214),USE(?Group_General)
                             PROMPT('Effective Date:'),AT(9,22),USE(?RATU:Effective_Date:Prompt),TRN
                             ENTRY(@d6),AT(85,22,56,10),USE(RATU:Effective_Date),RIGHT(1)
                             PROMPT('RUB ID:'),AT(221,22),USE(?RATU:RUBID:Prompt),TRN
                             STRING(@n_10),AT(253,22,56,10),USE(RATU:RUBID),RIGHT(1),TRN
                             PROMPT('Run Date:'),AT(9,42),USE(?RATU:RunDate:Prompt),TRN
                             ENTRY(@d6),AT(85,42,56,10),USE(RATU:RunDate),RIGHT(1)
                             PROMPT('Run Time:'),AT(9,56),USE(?RATU:RunTime:Prompt),TRN
                             ENTRY(@t7),AT(85,56,56,10),USE(RATU:RunTime),RIGHT(1)
                             PROMPT('Rates Option:'),AT(9,78),USE(?RATU:Rates_Option:Prompt),TRN
                             LIST,AT(85,78,100,10),USE(RATU:Rates_Option),DROP(5),FROM('Rates|#1|Additional Rates|#2' & |
  '|Container Park Rates|#3|Container Park Discount Minimiums|#4|Fuel Surcharges|#5|(legacy)|#0')
                             PROMPT('Increase Decrease:'),AT(9,92),USE(?RATU:IncreaseDecrease:Prompt),TRN
                             LIST,AT(85,92,100,10),USE(RATU:IncreaseDecrease),DROP(5),FROM('Increase|#0|Decrease|#1'), |
  MSG('Increase or Decrease'),TIP('Increase or Decrease')
                             CHECK(' Set Percent'),AT(85,110),USE(RATU:Set_Percent),TRN
                             PROMPT('Percentage:'),AT(9,124),USE(?RATU:Percentage:Prompt),TRN
                             ENTRY(@n7.2),AT(85,124,56,10),USE(RATU:Percentage),DECIMAL(12)
                             PROMPT('Minimium Charge:'),AT(9,138),USE(?RATU:MinimiumCharge:Prompt),TRN
                             LIST,AT(85,138,100,10),USE(RATU:MinimiumCharge),DROP(5),FROM('Percentage|#0|No Change|#1'), |
  TIP('Percentage, No Change'),MSG('Percentage, No Change')
                             PROMPT('Ad Hoc:'),AT(9,152),USE(?RATU:AdHoc:Prompt),TRN
                             LIST,AT(85,152,100,10),USE(RATU:AdHoc),DROP(5),FROM('Don''t Adjust|#0|Adjust (leave Ad ' & |
  'Hoc)|#1|Adjust (change to non Ad Hoc)|#2'),MSG('Ad Hoc option'),TIP('Ad Hoc option -' & |
  ' Don''t Adjust, Adjust (leave Ad Hoc), Adjust (change to non Ad Hoc)')
                             CHECK(' Round To Cents'),AT(85,166),USE(RATU:Round_To_Cents),TRN
                             PROMPT('Round To Rands:'),AT(9,182),USE(?RATU:Round_To_Rands:Prompt),TRN
                             LIST,AT(85,182,100,10),USE(RATU:Round_To_Rands),DROP(5),FROM('None|#0|Rands|#1|10 Rands' & |
  '|#2|100 Rands|#3|5 Rands|#4')
                             PROMPT('Clients Updated:'),AT(9,198),USE(?RATU:ClientsUpdated:Prompt),TRN
                             ENTRY(@n13),AT(85,198,56,10),USE(RATU:ClientsUpdated),RIGHT(1)
                             PROMPT('Rates Added:'),AT(9,212),USE(?RATU:RatesAdded:Prompt),TRN
                             ENTRY(@n13),AT(85,212,56,10),USE(RATU:RatesAdded),RIGHT(1)
                             PROMPT('Completed Status:'),AT(9,226),USE(?RATU:CompletedStatus:Prompt),TRN
                             LIST,AT(85,226,100,10),USE(RATU:CompletedStatus),DROP(5),FROM('Started|#0|Completed Suc' & |
  'cessfully|#1|Failed 2|#2|Failed 3|#3'),MSG('Completed Successfully'),TIP('Completed ' & |
  'Successfully')
                           END
                         END
                         TAB('&2) Rates Added'),USE(?Tab3)
                           SHEET,AT(9,22,355,250),USE(?Sheet_RateLists)
                             TAB('&a) Rates'),USE(?Tab6)
                               LIST,AT(14,39,346,228),USE(?List),HVSCROLL,FORMAT('70L(1)|M~Client~L(2)@s100@38R(1)|M~C' & |
  'lient No.~L(2)@n_10b@70L(1)|M~Load Type~L(2)@s80@70L(1)|M~Journey~L(2)@s70@48R(1)|M~' & |
  'To Mass~L(2)@n-12.0@52R(1)|M~Rate Per Kg~L(2)@n-13.4@56R(1)|M~Minimium Charge~L(2)@n' & |
  '-14.2@40R(1)|M~Effective Date~L(2)@d5@40R(1)|M~Added Date~L(2)@d5@30R(1)|M~Ad Hoc~L(' & |
  '2)@n3@70L(1)|M~Container Type~L(2)@s35@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                             END
                             TAB('&b) Fuel Surcharge'),USE(?Tab7)
                               LIST,AT(14,39,346,228),USE(?List:2),HVSCROLL,FORMAT('150L(1)|M~Client Name~L(2)@s100@40' & |
  'R(1)|M~Client No.~L(2)@n_10b@44R(1)|M~Effective Date~L(2)@d5@28R(1)|M~Fuel Surcharge' & |
  '~L(2)@n-7.2@'),FROM(Queue:Browse:1),IMM,MSG('Browsing Records')
                             END
                             TAB('&c) Container Park Dis.'),USE(?Tab8)
                               LIST,AT(14,39,346,228),USE(?List:3),HVSCROLL,FORMAT('40R(1)|M~Client No.~L(2)@n_10b@120' & |
  'L(1)|M~Client Name~L(2)@s100@38R(1)|M~Effective Date~L(2)@d5@32R(1)|M~Discount (Cont' & |
  'ainer Park Rate)~L(2)@n7.2@52R(1)|M~Container Park Minimium~L(2)@n-13.2@50L(1)|M~Flo' & |
  'or~L(2)@s35@'),FROM(Queue:Browse:2),IMM,MSG('Browsing Records')
                             END
                             TAB('&d) Container Park Mins.'),USE(?Tab10)
                               LIST,AT(14,39,346,228),USE(?List:4),HVSCROLL,FORMAT('110L(2)|M~Floor~@s35@110L(2)|M~Jou' & |
  'rney~@s70@48R(2)|M~To Mass~L@n-12.0@52R(2)|M~Rate Per Kg~L@n-13.4@'),FROM(Queue:Browse:3), |
  IMM,MSG('Browsing Records')
                             END
                             TAB('&e) Additional Rates'),USE(?Tab9)
                               LIST,AT(14,39,346,228),USE(?List:5),HVSCROLL,FORMAT('100L(2)|M~Client Name~@s100@40R(2)' & |
  '|M~Client No.~L(2)@n_10b@120L(2)|M~Add. Charge Description~@s35@60R(2)|M~Charge~@n-15.2@'), |
  FROM(Queue:Browse:4),IMM,MSG('Browsing Records')
                             END
                           END
                         END
                       END
                       BUTTON('&OK'),AT(266,278,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(318,278,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,278,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW5                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
                     END

BRW5::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW8                 CLASS(BrowseClass)                    ! Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
                     END

BRW8::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW9                 CLASS(BrowseClass)                    ! Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW9::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW10                CLASS(BrowseClass)                    ! Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
                     END

BRW10::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW11                CLASS(BrowseClass)                    ! Browse using ?List:5
Q                      &Queue:Browse:4                !Reference to browse queue
                     END

BRW11::Sort0:Locator StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Rate Batch Update Record'
  OF InsertRecord
    ActionMessage = 'Rate Batch Update Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Rate Batch Update Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Rate_Updates')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RATU:Effective_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(RATU:Record,History::RATU:Record)
  SELF.AddHistoryField(?RATU:Effective_Date,5)
  SELF.AddHistoryField(?RATU:RUBID,1)
  SELF.AddHistoryField(?RATU:RunDate,9)
  SELF.AddHistoryField(?RATU:RunTime,10)
  SELF.AddHistoryField(?RATU:Rates_Option,11)
  SELF.AddHistoryField(?RATU:IncreaseDecrease,12)
  SELF.AddHistoryField(?RATU:Set_Percent,13)
  SELF.AddHistoryField(?RATU:Percentage,14)
  SELF.AddHistoryField(?RATU:MinimiumCharge,15)
  SELF.AddHistoryField(?RATU:AdHoc,16)
  SELF.AddHistoryField(?RATU:Round_To_Cents,17)
  SELF.AddHistoryField(?RATU:Round_To_Rands,18)
  SELF.AddHistoryField(?RATU:ClientsUpdated,20)
  SELF.AddHistoryField(?RATU:RatesAdded,21)
  SELF.AddHistoryField(?RATU:CompletedStatus,22)
  SELF.AddUpdateFile(Access:__RateUpdates)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients_ContainerParkDiscounts.SetOpenRelated()
  Relate:Clients_ContainerParkDiscounts.Open               ! File Clients_ContainerParkDiscounts used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:__RateUpdates
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:__Rates,SELF) ! Initialize the browse manager
  BRW8.Init(?List:2,Queue:Browse:1.ViewPosition,BRW8::View:Browse,Queue:Browse:1,Relate:__RatesFuelSurcharge,SELF) ! Initialize the browse manager
  BRW9.Init(?List:3,Queue:Browse:2.ViewPosition,BRW9::View:Browse,Queue:Browse:2,Relate:Clients_ContainerParkDiscounts,SELF) ! Initialize the browse manager
  BRW10.Init(?List:4,Queue:Browse:3.ViewPosition,BRW10::View:Browse,Queue:Browse:3,Relate:__RatesContainerPark,SELF) ! Initialize the browse manager
  BRW11.Init(?List:5,Queue:Browse:4.ViewPosition,BRW11::View:Browse,Queue:Browse:4,Relate:__RatesAdditionalCharges,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?RATU:Effective_Date{PROP:ReadOnly} = True
    ?RATU:RunDate{PROP:ReadOnly} = True
    ?RATU:RunTime{PROP:ReadOnly} = True
    DISABLE(?RATU:Rates_Option)
    DISABLE(?RATU:IncreaseDecrease)
    ?RATU:Percentage{PROP:ReadOnly} = True
    DISABLE(?RATU:MinimiumCharge)
    DISABLE(?RATU:AdHoc)
    DISABLE(?RATU:Round_To_Rands)
    ?RATU:ClientsUpdated{PROP:ReadOnly} = True
    ?RATU:RatesAdded{PROP:ReadOnly} = True
    DISABLE(?RATU:CompletedStatus)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,RAT:FKey_RUBID)                       ! Add the sort order for RAT:FKey_RUBID for sort order 1
  BRW5.AddRange(RAT:RUBID,RATU:RUBID)                      ! Add single value range limit for sort order 1
  BRW5.AddLocator(BRW5::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW5::Sort0:Locator.Init(,RAT:RUBID,1,BRW5)              ! Initialize the browse locator using  using key: RAT:FKey_RUBID , RAT:RUBID
  BRW5.AppendOrder('+CLI:ClientName,+CRT:ClientRateType,+JOU:Journey,+CTYP:ContainerType,+RAT:ToMass,+RAT:RatePerKg') ! Append an additional sort order
  BRW5.AddField(CLI:ClientName,BRW5.Q.CLI:ClientName)      ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW5.AddField(CLI:ClientNo,BRW5.Q.CLI:ClientNo)          ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW5.AddField(CRT:ClientRateType,BRW5.Q.CRT:ClientRateType) ! Field CRT:ClientRateType is a hot field or requires assignment from browse
  BRW5.AddField(JOU:Journey,BRW5.Q.JOU:Journey)            ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW5.AddField(RAT:ToMass,BRW5.Q.RAT:ToMass)              ! Field RAT:ToMass is a hot field or requires assignment from browse
  BRW5.AddField(RAT:RatePerKg,BRW5.Q.RAT:RatePerKg)        ! Field RAT:RatePerKg is a hot field or requires assignment from browse
  BRW5.AddField(RAT:MinimiumCharge,BRW5.Q.RAT:MinimiumCharge) ! Field RAT:MinimiumCharge is a hot field or requires assignment from browse
  BRW5.AddField(RAT:Effective_Date,BRW5.Q.RAT:Effective_Date) ! Field RAT:Effective_Date is a hot field or requires assignment from browse
  BRW5.AddField(RAT:Added_Date,BRW5.Q.RAT:Added_Date)      ! Field RAT:Added_Date is a hot field or requires assignment from browse
  BRW5.AddField(RAT:AdHoc,BRW5.Q.RAT:AdHoc)                ! Field RAT:AdHoc is a hot field or requires assignment from browse
  BRW5.AddField(CTYP:ContainerType,BRW5.Q.CTYP:ContainerType) ! Field CTYP:ContainerType is a hot field or requires assignment from browse
  BRW5.AddField(RAT:RID,BRW5.Q.RAT:RID)                    ! Field RAT:RID is a hot field or requires assignment from browse
  BRW5.AddField(RAT:RUBID,BRW5.Q.RAT:RUBID)                ! Field RAT:RUBID is a hot field or requires assignment from browse
  BRW5.AddField(CTYP:CTID,BRW5.Q.CTYP:CTID)                ! Field CTYP:CTID is a hot field or requires assignment from browse
  BRW5.AddField(CRT:CRTID,BRW5.Q.CRT:CRTID)                ! Field CRT:CRTID is a hot field or requires assignment from browse
  BRW5.AddField(JOU:JID,BRW5.Q.JOU:JID)                    ! Field JOU:JID is a hot field or requires assignment from browse
  BRW5.AddField(CLI:CID,BRW5.Q.CLI:CID)                    ! Field CLI:CID is a hot field or requires assignment from browse
  BRW8.Q &= Queue:Browse:1
  BRW8.AddSortOrder(,FSRA:FKey_RUBID)                      ! Add the sort order for FSRA:FKey_RUBID for sort order 1
  BRW8.AddRange(FSRA:RUBID,RATU:RUBID)                     ! Add single value range limit for sort order 1
  BRW8.AddLocator(BRW8::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW8::Sort0:Locator.Init(,FSRA:RUBID,1,BRW8)             ! Initialize the browse locator using  using key: FSRA:FKey_RUBID , FSRA:RUBID
  BRW8.AppendOrder('+CLI:ClientName,+CLI:ClientNo')        ! Append an additional sort order
  BRW8.AddField(CLI:ClientName,BRW8.Q.CLI:ClientName)      ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW8.AddField(CLI:ClientNo,BRW8.Q.CLI:ClientNo)          ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW8.AddField(FSRA:Effective_Date,BRW8.Q.FSRA:Effective_Date) ! Field FSRA:Effective_Date is a hot field or requires assignment from browse
  BRW8.AddField(FSRA:FuelSurcharge,BRW8.Q.FSRA:FuelSurcharge) ! Field FSRA:FuelSurcharge is a hot field or requires assignment from browse
  BRW8.AddField(FSRA:FCID,BRW8.Q.FSRA:FCID)                ! Field FSRA:FCID is a hot field or requires assignment from browse
  BRW8.AddField(FSRA:RUBID,BRW8.Q.FSRA:RUBID)              ! Field FSRA:RUBID is a hot field or requires assignment from browse
  BRW8.AddField(CLI:CID,BRW8.Q.CLI:CID)                    ! Field CLI:CID is a hot field or requires assignment from browse
  BRW9.Q &= Queue:Browse:2
  BRW9.AddSortOrder(,CLI_CP:FKey_RUBID)                    ! Add the sort order for CLI_CP:FKey_RUBID for sort order 1
  BRW9.AddRange(CLI_CP:RUBID,RATU:RUBID)                   ! Add single value range limit for sort order 1
  BRW9.AddLocator(BRW9::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW9::Sort0:Locator.Init(,CLI_CP:RUBID,1,BRW9)           ! Initialize the browse locator using  using key: CLI_CP:FKey_RUBID , CLI_CP:RUBID
  BRW9.AppendOrder('+CLI:ClientName,+CLI:ClientNo,+FLO:Floor') ! Append an additional sort order
  BRW9.AddField(CLI:ClientNo,BRW9.Q.CLI:ClientNo)          ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW9.AddField(CLI:ClientName,BRW9.Q.CLI:ClientName)      ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW9.AddField(CLI_CP:Effective_Date,BRW9.Q.CLI_CP:Effective_Date) ! Field CLI_CP:Effective_Date is a hot field or requires assignment from browse
  BRW9.AddField(CLI_CP:ContainerParkRateDiscount,BRW9.Q.CLI_CP:ContainerParkRateDiscount) ! Field CLI_CP:ContainerParkRateDiscount is a hot field or requires assignment from browse
  BRW9.AddField(CLI_CP:ContainerParkMinimium,BRW9.Q.CLI_CP:ContainerParkMinimium) ! Field CLI_CP:ContainerParkMinimium is a hot field or requires assignment from browse
  BRW9.AddField(FLO:Floor,BRW9.Q.FLO:Floor)                ! Field FLO:Floor is a hot field or requires assignment from browse
  BRW9.AddField(CLI_CP:CPDID,BRW9.Q.CLI_CP:CPDID)          ! Field CLI_CP:CPDID is a hot field or requires assignment from browse
  BRW9.AddField(CLI_CP:RUBID,BRW9.Q.CLI_CP:RUBID)          ! Field CLI_CP:RUBID is a hot field or requires assignment from browse
  BRW9.AddField(FLO:FID,BRW9.Q.FLO:FID)                    ! Field FLO:FID is a hot field or requires assignment from browse
  BRW9.AddField(CLI:CID,BRW9.Q.CLI:CID)                    ! Field CLI:CID is a hot field or requires assignment from browse
  BRW10.Q &= Queue:Browse:3
  BRW10.AddSortOrder(,CPRA:FKey_RUBID)                     ! Add the sort order for CPRA:FKey_RUBID for sort order 1
  BRW10.AddRange(CPRA:RUBID,RATU:RUBID)                    ! Add single value range limit for sort order 1
  BRW10.AddLocator(BRW10::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW10::Sort0:Locator.Init(,CPRA:RUBID,1,BRW10)           ! Initialize the browse locator using  using key: CPRA:FKey_RUBID , CPRA:RUBID
  BRW10.AppendOrder('+FLO:Floor,+CPRA:FID,+JOU:Journey,+CPRA:ToMass,+CPRA:RatePerKg') ! Append an additional sort order
  BRW10.AddField(FLO:Floor,BRW10.Q.FLO:Floor)              ! Field FLO:Floor is a hot field or requires assignment from browse
  BRW10.AddField(JOU:Journey,BRW10.Q.JOU:Journey)          ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW10.AddField(CPRA:ToMass,BRW10.Q.CPRA:ToMass)          ! Field CPRA:ToMass is a hot field or requires assignment from browse
  BRW10.AddField(CPRA:RatePerKg,BRW10.Q.CPRA:RatePerKg)    ! Field CPRA:RatePerKg is a hot field or requires assignment from browse
  BRW10.AddField(CPRA:CPID,BRW10.Q.CPRA:CPID)              ! Field CPRA:CPID is a hot field or requires assignment from browse
  BRW10.AddField(CPRA:RUBID,BRW10.Q.CPRA:RUBID)            ! Field CPRA:RUBID is a hot field or requires assignment from browse
  BRW10.AddField(JOU:JID,BRW10.Q.JOU:JID)                  ! Field JOU:JID is a hot field or requires assignment from browse
  BRW10.AddField(FLO:FID,BRW10.Q.FLO:FID)                  ! Field FLO:FID is a hot field or requires assignment from browse
  BRW11.Q &= Queue:Browse:4
  BRW11.AddSortOrder(,CARA:FKey_RUBID)                     ! Add the sort order for CARA:FKey_RUBID for sort order 1
  BRW11.AddRange(CARA:RUBID,RATU:RUBID)                    ! Add single value range limit for sort order 1
  BRW11.AddLocator(BRW11::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW11::Sort0:Locator.Init(,CARA:RUBID,1,BRW11)           ! Initialize the browse locator using  using key: CARA:FKey_RUBID , CARA:RUBID
  BRW11.AppendOrder('+CLI:ClientName,+CLI:ClientNo,+ACCA:Description,+CARA:ACID') ! Append an additional sort order
  BRW11.AddField(CLI:ClientName,BRW11.Q.CLI:ClientName)    ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW11.AddField(CLI:ClientNo,BRW11.Q.CLI:ClientNo)        ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW11.AddField(ACCA:Description,BRW11.Q.ACCA:Description) ! Field ACCA:Description is a hot field or requires assignment from browse
  BRW11.AddField(CARA:Charge,BRW11.Q.CARA:Charge)          ! Field CARA:Charge is a hot field or requires assignment from browse
  BRW11.AddField(CARA:ACID,BRW11.Q.CARA:ACID)              ! Field CARA:ACID is a hot field or requires assignment from browse
  BRW11.AddField(CARA:RUBID,BRW11.Q.CARA:RUBID)            ! Field CARA:RUBID is a hot field or requires assignment from browse
  BRW11.AddField(ACCA:ACCID,BRW11.Q.ACCA:ACCID)            ! Field ACCA:ACCID is a hot field or requires assignment from browse
  BRW11.AddField(CLI:CID,BRW11.Q.CLI:CID)                  ! Field CLI:CID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_Rate_Updates',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW5.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW5.ToolbarItem.HelpButton = ?Help
  BRW8.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW8.ToolbarItem.HelpButton = ?Help
  BRW9.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW9.ToolbarItem.HelpButton = ?Help
  BRW10.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW10.ToolbarItem.HelpButton = ?Help
  BRW11.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW11.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients_ContainerParkDiscounts.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Rate_Updates',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_General, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_General

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Setup PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::SET:Record  LIKE(SET:RECORD),THREAD
QuickWindow          WINDOW('Form Setup'),AT(,,295,256),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY,IMM,MDI, |
  HLP('Setup'),SYSTEM
                       SHEET,AT(4,4,287,234),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Note these settings are for ALL BRANCHES'),AT(78,22),USE(?Prompt2),FONT(,,COLOR:Red, |
  ,CHARSET:ANSI),TRN
                           PROMPT('VAT Rate:'),AT(9,42),USE(?SET:VATRate:Prompt),TRN
                           ENTRY(@n7.2),AT(121,42,40,10),USE(SET:VATRate),RIGHT(1),MSG('VAT Rate'),TIP('VAT Rate')
                           CHECK(' Test Database'),AT(121,64),USE(SET:TestDatabase),MSG('This is a Test Database '),TIP('This is a ' & |
  'Test Database '),TRN
                         END
                       END
                       BUTTON('&OK'),AT(188,240,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(242,240,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,240,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Setup Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Setup Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Setup')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(SET:Record,History::SET:Record)
  SELF.AddHistoryField(?SET:VATRate,4)
  SELF.AddHistoryField(?SET:TestDatabase,5)
  SELF.AddUpdateFile(Access:Setup)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Relate:Setup.Open                                        ! File Setup used by this procedure, so make sure it's RelationManager is open
  Access:Branches.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ServiceRequirements.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Setup
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?SET:VATRate{PROP:ReadOnly} = True
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
    Relate:Setup.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  SET:VATRate = 14
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
          IF SET:TestDatabase = TRUE
             CASE MESSAGE('You have specified that this Database is a Test database.||Are you absolutely sure?','Test DB Confirm',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No)
             OF BUTTON:No
                QuickWindow{PROP:AcceptAll}   = FALSE
                CYCLE
          .  .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Journeys PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
From_Address         STRING(35)                            !Name of this address
To_Address           STRING(35)                            !Name of this address
Floor                STRING(35)                            !Floor Name
BRW14::View:Browse   VIEW(__Rates)
                       PROJECT(RAT:ToMass)
                       PROJECT(RAT:RatePerKg)
                       PROJECT(RAT:MinimiumCharge)
                       PROJECT(RAT:Effective_Date)
                       PROJECT(RAT:Added_Date)
                       PROJECT(RAT:AdHoc)
                       PROJECT(RAT:RID)
                       PROJECT(RAT:JID)
                       PROJECT(RAT:CID)
                       JOIN(CLI:PKey_CID,RAT:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
RAT:ToMass             LIKE(RAT:ToMass)               !List box control field - type derived from field
RAT:RatePerKg          LIKE(RAT:RatePerKg)            !List box control field - type derived from field
RAT:MinimiumCharge     LIKE(RAT:MinimiumCharge)       !List box control field - type derived from field
RAT:Effective_Date     LIKE(RAT:Effective_Date)       !List box control field - type derived from field
RAT:Added_Date         LIKE(RAT:Added_Date)           !List box control field - type derived from field
RAT:AdHoc              LIKE(RAT:AdHoc)                !List box control field - type derived from field
RAT:AdHoc_Icon         LONG                           !Entry's icon ID
RAT:RID                LIKE(RAT:RID)                  !Primary key field - type derived from field
RAT:JID                LIKE(RAT:JID)                  !Browse key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(Clients_ContainerParkDiscounts)
                       PROJECT(CLI_CP:ContainerParkRateDiscount)
                       PROJECT(CLI_CP:CPDID)
                       PROJECT(CLI_CP:CID)
                       PROJECT(CLI_CP:FID)
                       JOIN(CLI:PKey_CID,CLI_CP:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                       JOIN(FLO:PKey_FID,CLI_CP:FID)
                         PROJECT(FLO:Floor)
                         PROJECT(FLO:FID)
                         JOIN(CPRA:FKey_FID,FLO:FID)
                           PROJECT(CPRA:RatePerKg)
                           PROJECT(CPRA:ToMass)
                           PROJECT(CPRA:Effective_Date)
                         END
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
CLI_CP:ContainerParkRateDiscount LIKE(CLI_CP:ContainerParkRateDiscount) !List box control field - type derived from field
CPRA:RatePerKg         LIKE(CPRA:RatePerKg)           !List box control field - type derived from field
CPRA:ToMass            LIKE(CPRA:ToMass)              !List box control field - type derived from field
CPRA:Effective_Date    LIKE(CPRA:Effective_Date)      !List box control field - type derived from field
CLI_CP:CPDID           LIKE(CLI_CP:CPDID)             !Primary key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(__RatesTransporter)
                       PROJECT(TRRA:MinimiumLoad)
                       PROJECT(TRRA:BaseCharge)
                       PROJECT(TRRA:PerRate)
                       PROJECT(TRRA:TID)
                       PROJECT(TRRA:TRID)
                       PROJECT(TRRA:JID)
                       JOIN(A_JOU:PKey_JID,TRRA:JID)
                         PROJECT(A_JOU:Journey)
                         PROJECT(A_JOU:JID)
                       END
                       JOIN(TRA:PKey_TID,TRRA:TID)
                         PROJECT(TRA:TransporterName)
                         PROJECT(TRA:TID)
                       END
                     END
Queue:Browse:8       QUEUE                            !Queue declaration for browse/combo box using ?Browse:8
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
A_JOU:Journey          LIKE(A_JOU:Journey)            !List box control field - type derived from field
TRRA:MinimiumLoad      LIKE(TRRA:MinimiumLoad)        !List box control field - type derived from field
TRRA:BaseCharge        LIKE(TRRA:BaseCharge)          !List box control field - type derived from field
TRRA:PerRate           LIKE(TRRA:PerRate)             !List box control field - type derived from field
TRRA:TID               LIKE(TRRA:TID)                 !List box control field - type derived from field
TRRA:TRID              LIKE(TRRA:TRID)                !Primary key field - type derived from field
TRRA:JID               LIKE(TRRA:JID)                 !Browse key field - type derived from field
A_JOU:JID              LIKE(A_JOU:JID)                !Related join file key field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:Rate)
                       PROJECT(DEL:DocumentCharge)
                       PROJECT(DEL:FuelSurcharge)
                       PROJECT(DEL:Charge)
                       PROJECT(DEL:BID)
                       PROJECT(DEL:CID)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:JID)
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:10      QUEUE                            !Queue declaration for browse/combo box using ?Browse:10
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
DEL:Rate               LIKE(DEL:Rate)                 !List box control field - type derived from field
DEL:DocumentCharge     LIKE(DEL:DocumentCharge)       !List box control field - type derived from field
DEL:FuelSurcharge      LIKE(DEL:FuelSurcharge)        !List box control field - type derived from field
DEL:Charge             LIKE(DEL:Charge)               !List box control field - type derived from field
DEL:BID                LIKE(DEL:BID)                  !List box control field - type derived from field
DEL:CID                LIKE(DEL:CID)                  !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Primary key field - type derived from field
DEL:JID                LIKE(DEL:JID)                  !Browse key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB4::View:FileDrop  VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?Floor
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::JOU:Record  LIKE(JOU:RECORD),THREAD
QuickWindow          WINDOW('Update the Journeys File'),AT(,,395,247),FONT('MS Sans Serif',8),DOUBLE,GRAY,IMM,MAX, |
  MDI,HLP('UpdateJourneys'),SYSTEM
                       GROUP,AT(4,233,166,10),USE(?Group_BottomLeft)
                         PROMPT('BID:'),AT(4,230),USE(?JOU:BID:Prompt)
                         STRING(@n_10),AT(23,230,46,10),USE(JOU:BID),RIGHT(1)
                         PROMPT('JID:'),AT(85,230),USE(?JOU:JID:Prompt),HIDE
                         STRING(@n_10),AT(104,230),USE(JOU:JID),RIGHT(1),HIDE
                       END
                       SHEET,AT(4,4,387,222),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           GROUP,AT(9,22,381,155),USE(?Group_General)
                             PROMPT('Journey:'),AT(8,22),USE(?JOU:Journey:Prompt),TRN
                             TEXT,AT(64,23,321,82),USE(JOU:Journey),VSCROLL,MSG('Description')
                             PROMPT('Description:'),AT(9,110),USE(?JOU:Description:Prompt),TRN
                             TEXT,AT(64,110,321,60),USE(JOU:Description),HVSCROLL,BOXED
                             PROMPT('Transfer to Floor'),AT(9,181),USE(?Prompt5),TRN
                             LIST,AT(64,181,139,10),USE(Floor),VSCROLL,DROP(15),FORMAT('140L(2)|M~Floor~@s35@'),FROM(Queue:FileDrop), |
  TIP('When Manifests use this Journey the DI''s will be <0DH,0AH>moved to this floor o' & |
  'nce the Manifest is Transferred')
                           END
                         END
                         TAB('&2) Client Rates'),USE(?Tab4)
                           LIST,AT(8,23,378,174),USE(?List),VSCROLL,FORMAT('80L(2)|M~Client Name~@s35@48R(2)|M~To ' & |
  'Mass~L@n-12.0@46R(2)|M~Rate Per Kg~L@n-13.4@40R(2)|M~Minimium Charge~L@n-14.2@40R(2)' & |
  '|M~Effective Date~L@d5@40R(2)|M~Added Date~L@d5@30R(2)|MI~Ad Hoc~L@p p@'),FROM(Queue:Browse), |
  IMM,MSG('Browsing Records')
                         END
                         TAB('&3) Client Container Park Rates'),USE(?Tab5)
                           LIST,AT(8,22,378,175),USE(?List:2),VSCROLL,FORMAT('120L(2)|M~Client Name~@s35@80L(2)|M~' & |
  'Floor~@s35@34R(2)|M~Discount~L@n7.2@46R(2)|M~Rate Per Kg~L@n-13.4@48R(2)|M~To Mass~L' & |
  '@n-12.0@32R(2)|M~Effective Date~L@d5@'),FROM(Queue:Browse:1),IMM,MSG('Browsing Records')
                         END
                         TAB('&4) Deliveries'),USE(?Tab:6)
                           LIST,AT(8,22,378,174),USE(?Browse:10),HVSCROLL,FORMAT('40R(2)|M~DI No.~C(0)@n_10@80R(2)' & |
  '|M~Client Name~C(0)@s100@80L(2)|M~Client Reference~@s30@42R(1)|M~Rate~C(0)@n-11.2@50' & |
  'R(1)|M~Docs. Charge~C(0)@n-11.2@54R(1)|M~Fuel Surcharge~C(0)@n-11.2@48R(1)|M~Charge~' & |
  'C(0)@n-13.2@30R(2)|M~BID~C(0)@n_10@30R(2)|M~CID~C(0)@n_10@'),FROM(Queue:Browse:10),IMM, |
  MSG('Browsing Records')
                         END
                         TAB('&5) Transporter Rates'),USE(?Tab:5)
                           LIST,AT(8,22,378,174),USE(?Browse:8),HVSCROLL,FORMAT('70L(2)|M~Transporter Name~C(0)@s3' & |
  '5@70L(2)|M~Journey~C(0)@s70@56R(2)|M~Minimium Load~C(0)@n-11.0@60D(14)|M~Base Charge' & |
  '~C(0)@n-13.2@48D(14)|M~Per Rate~C(0)@n-10.2@40R(2)|M~TID~C(0)@n_10@'),FROM(Queue:Browse:8), |
  IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('&OK'),AT(293,230,45,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT
                       BUTTON('Cancel'),AT(341,230,45,14),USE(?Cancel),LEFT,ICON('WAcancel.ICO'),FLAT
                       BUTTON('Help'),AT(243,230,45,14),USE(?Help),LEFT,ICON('WAhELP.ICO'),FLAT,HIDE,STD(STD:Help)
                       CHECK(' &E-Toll applies'),AT(63,201),USE(JOU:EToll),MSG('Is an E-Toll normally incurred' & |
  ' on this Journey?'),TIP('Is an E-Toll normally incurred on this Journey?')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW14                CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW2                 CLASS(BrowseClass)                    ! Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
                     END

BRW8                 CLASS(BrowseClass)                    ! Browse using ?Browse:8
Q                      &Queue:Browse:8                !Reference to browse queue
                     END

BRW8::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW10                CLASS(BrowseClass)                    ! Browse using ?Browse:10
Q                      &Queue:Browse:10               !Reference to browse queue
                     END

BRW10::Sort0:StepClass StepRealClass                       ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a Journeys Record'
  OF ChangeRecord
    ActionMessage = 'Changing a Journeys Record'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Journeys')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?JOU:BID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(JOU:Record,History::JOU:Record)
  SELF.AddHistoryField(?JOU:BID,4)
  SELF.AddHistoryField(?JOU:JID,1)
  SELF.AddHistoryField(?JOU:Journey,2)
  SELF.AddHistoryField(?JOU:Description,3)
  SELF.AddHistoryField(?JOU:EToll,6)
  SELF.AddUpdateFile(Access:Journeys)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:AddressAlias.Open                                 ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  Relate:JourneysAlias.Open                                ! File JourneysAlias used by this procedure, so make sure it's RelationManager is open
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesContainerPark.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Journeys
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW14.Init(?List,Queue:Browse.ViewPosition,BRW14::View:Browse,Queue:Browse,Relate:__Rates,SELF) ! Initialize the browse manager
  BRW2.Init(?List:2,Queue:Browse:1.ViewPosition,BRW2::View:Browse,Queue:Browse:1,Relate:Clients_ContainerParkDiscounts,SELF) ! Initialize the browse manager
  BRW8.Init(?Browse:8,Queue:Browse:8.ViewPosition,BRW8::View:Browse,Queue:Browse:8,Relate:__RatesTransporter,SELF) ! Initialize the browse manager
  BRW10.Init(?Browse:10,Queue:Browse:10.ViewPosition,BRW10::View:Browse,Queue:Browse:10,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW14.Q &= Queue:Browse
  BRW14.AddSortOrder(,RAT:FKey_JID)                        ! Add the sort order for RAT:FKey_JID for sort order 1
  BRW14.AddRange(RAT:JID,JOU:JID)                          ! Add single value range limit for sort order 1
  BRW14.AddLocator(BRW14::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW14::Sort0:Locator.Init(,RAT:JID,1,BRW14)              ! Initialize the browse locator using  using key: RAT:FKey_JID , RAT:JID
  BRW14.AppendOrder('+CLI:ClientName,+RAT:ToMass,+RAT:Effective_Date,+RAT:RID') ! Append an additional sort order
  ?List{PROP:IconList,1} = '~checkoffdim.ico'
  ?List{PROP:IconList,2} = '~checkon.ico'
  BRW14.AddField(CLI:ClientName,BRW14.Q.CLI:ClientName)    ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW14.AddField(RAT:ToMass,BRW14.Q.RAT:ToMass)            ! Field RAT:ToMass is a hot field or requires assignment from browse
  BRW14.AddField(RAT:RatePerKg,BRW14.Q.RAT:RatePerKg)      ! Field RAT:RatePerKg is a hot field or requires assignment from browse
  BRW14.AddField(RAT:MinimiumCharge,BRW14.Q.RAT:MinimiumCharge) ! Field RAT:MinimiumCharge is a hot field or requires assignment from browse
  BRW14.AddField(RAT:Effective_Date,BRW14.Q.RAT:Effective_Date) ! Field RAT:Effective_Date is a hot field or requires assignment from browse
  BRW14.AddField(RAT:Added_Date,BRW14.Q.RAT:Added_Date)    ! Field RAT:Added_Date is a hot field or requires assignment from browse
  BRW14.AddField(RAT:AdHoc,BRW14.Q.RAT:AdHoc)              ! Field RAT:AdHoc is a hot field or requires assignment from browse
  BRW14.AddField(RAT:RID,BRW14.Q.RAT:RID)                  ! Field RAT:RID is a hot field or requires assignment from browse
  BRW14.AddField(RAT:JID,BRW14.Q.RAT:JID)                  ! Field RAT:JID is a hot field or requires assignment from browse
  BRW14.AddField(CLI:CID,BRW14.Q.CLI:CID)                  ! Field CLI:CID is a hot field or requires assignment from browse
  BRW2.Q &= Queue:Browse:1
  BRW2.AddSortOrder(,)                                     ! Add the sort order for  for sort order 1
  BRW2.AppendOrder('+CLI:ClientName,+FLO:Floor,+CPRA:ToMass,+CPRA:Effective_Date,+CLI_CP:CPDID') ! Append an additional sort order
  BRW2.SetFilter('(CPRA:JID = JOU:JID)')                   ! Apply filter expression to browse
  BRW2.AddField(CLI:ClientName,BRW2.Q.CLI:ClientName)      ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW2.AddField(FLO:Floor,BRW2.Q.FLO:Floor)                ! Field FLO:Floor is a hot field or requires assignment from browse
  BRW2.AddField(CLI_CP:ContainerParkRateDiscount,BRW2.Q.CLI_CP:ContainerParkRateDiscount) ! Field CLI_CP:ContainerParkRateDiscount is a hot field or requires assignment from browse
  BRW2.AddField(CPRA:RatePerKg,BRW2.Q.CPRA:RatePerKg)      ! Field CPRA:RatePerKg is a hot field or requires assignment from browse
  BRW2.AddField(CPRA:ToMass,BRW2.Q.CPRA:ToMass)            ! Field CPRA:ToMass is a hot field or requires assignment from browse
  BRW2.AddField(CPRA:Effective_Date,BRW2.Q.CPRA:Effective_Date) ! Field CPRA:Effective_Date is a hot field or requires assignment from browse
  BRW2.AddField(CLI_CP:CPDID,BRW2.Q.CLI_CP:CPDID)          ! Field CLI_CP:CPDID is a hot field or requires assignment from browse
  BRW2.AddField(CLI:CID,BRW2.Q.CLI:CID)                    ! Field CLI:CID is a hot field or requires assignment from browse
  BRW2.AddField(FLO:FID,BRW2.Q.FLO:FID)                    ! Field FLO:FID is a hot field or requires assignment from browse
  BRW8.Q &= Queue:Browse:8
  BRW8::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon TRRA:JID for sort order 1
  BRW8.AddSortOrder(BRW8::Sort0:StepClass,TRRA:FKey_JID)   ! Add the sort order for TRRA:FKey_JID for sort order 1
  BRW8.AddRange(TRRA:JID,Relate:__RatesTransporter,Relate:Journeys) ! Add file relationship range limit for sort order 1
  BRW8.AppendOrder('+TRRA:MinimiumLoad,+TRRA:Effective_Date') ! Append an additional sort order
  BRW8.AddField(TRA:TransporterName,BRW8.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW8.AddField(A_JOU:Journey,BRW8.Q.A_JOU:Journey)        ! Field A_JOU:Journey is a hot field or requires assignment from browse
  BRW8.AddField(TRRA:MinimiumLoad,BRW8.Q.TRRA:MinimiumLoad) ! Field TRRA:MinimiumLoad is a hot field or requires assignment from browse
  BRW8.AddField(TRRA:BaseCharge,BRW8.Q.TRRA:BaseCharge)    ! Field TRRA:BaseCharge is a hot field or requires assignment from browse
  BRW8.AddField(TRRA:PerRate,BRW8.Q.TRRA:PerRate)          ! Field TRRA:PerRate is a hot field or requires assignment from browse
  BRW8.AddField(TRRA:TID,BRW8.Q.TRRA:TID)                  ! Field TRRA:TID is a hot field or requires assignment from browse
  BRW8.AddField(TRRA:TRID,BRW8.Q.TRRA:TRID)                ! Field TRRA:TRID is a hot field or requires assignment from browse
  BRW8.AddField(TRRA:JID,BRW8.Q.TRRA:JID)                  ! Field TRRA:JID is a hot field or requires assignment from browse
  BRW8.AddField(A_JOU:JID,BRW8.Q.A_JOU:JID)                ! Field A_JOU:JID is a hot field or requires assignment from browse
  BRW8.AddField(TRA:TID,BRW8.Q.TRA:TID)                    ! Field TRA:TID is a hot field or requires assignment from browse
  BRW10.Q &= Queue:Browse:10
  BRW10::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)      ! Moveable thumb based upon DEL:JID for sort order 1
  BRW10.AddSortOrder(BRW10::Sort0:StepClass,DEL:FKey_JID)  ! Add the sort order for DEL:FKey_JID for sort order 1
  BRW10.AddRange(DEL:JID,Relate:Deliveries,Relate:Journeys) ! Add file relationship range limit for sort order 1
  BRW10.AppendOrder('+DEL:DINo')                           ! Append an additional sort order
  BRW10.AddField(DEL:DINo,BRW10.Q.DEL:DINo)                ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW10.AddField(CLI:ClientName,BRW10.Q.CLI:ClientName)    ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW10.AddField(DEL:ClientReference,BRW10.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW10.AddField(DEL:Rate,BRW10.Q.DEL:Rate)                ! Field DEL:Rate is a hot field or requires assignment from browse
  BRW10.AddField(DEL:DocumentCharge,BRW10.Q.DEL:DocumentCharge) ! Field DEL:DocumentCharge is a hot field or requires assignment from browse
  BRW10.AddField(DEL:FuelSurcharge,BRW10.Q.DEL:FuelSurcharge) ! Field DEL:FuelSurcharge is a hot field or requires assignment from browse
  BRW10.AddField(DEL:Charge,BRW10.Q.DEL:Charge)            ! Field DEL:Charge is a hot field or requires assignment from browse
  BRW10.AddField(DEL:BID,BRW10.Q.DEL:BID)                  ! Field DEL:BID is a hot field or requires assignment from browse
  BRW10.AddField(DEL:CID,BRW10.Q.DEL:CID)                  ! Field DEL:CID is a hot field or requires assignment from browse
  BRW10.AddField(DEL:DID,BRW10.Q.DEL:DID)                  ! Field DEL:DID is a hot field or requires assignment from browse
  BRW10.AddField(DEL:JID,BRW10.Q.DEL:JID)                  ! Field DEL:JID is a hot field or requires assignment from browse
  BRW10.AddField(CLI:CID,BRW10.Q.CLI:CID)                  ! Field CLI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Journeys',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  FDB4.Init(?Floor,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:Floors,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(FLO:Key_Floor)
  FDB4.AddField(FLO:Floor,FDB4.Q.FLO:Floor) !List box control field - type derived from field
  FDB4.AddField(FLO:FID,FDB4.Q.FLO:FID) !Primary key field - type derived from field
  FDB4.AddUpdateField(FLO:FID,JOU:FID)
  ThisWindow.AddItem(FDB4.WindowComponent)
  FDB4.DefaultFill = 0
  BRW14.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW14.ToolbarItem.HelpButton = ?Help
  BRW2.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW2.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressAlias.Close
    Relate:JourneysAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Journeys',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  JOU:BID = GLO:BranchID
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
          IF JOU:BID = 0
             MESSAGE('There is no Branch.||Please check your setup.', 'Journey Validation', ICON:Hand)
             SELECT(?)
             QuickWindow{PROP:AcceptAll}   = FALSE
             CYCLE
          .
      
      
      
      
          IF SELF.Request = InsertRecord AND JOU:EToll = FALSE
             CASE MESSAGE('Charge E Tolls on this Journey?','E Tolls check', ICON:Question, BUTTON:Yes+BUTTON:No)
             OF BUTTON:YES
                JOU:EToll  = TRUE
             OF BUTTON:NO
                JOU:EToll  = FALSE
             .
          .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW14.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (RAT:AdHoc = 1)
    SELF.Q.RAT:AdHoc_Icon = 2                              ! Set icon from icon list
  ELSE
    SELF.Q.RAT:AdHoc_Icon = 1                              ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_BottomLeft, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_BottomLeft
  SELF.SetStrategy(?Group_General, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_General

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_LoadTypes PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Original_LoadOption BYTE                               !Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
BRW2::View:Browse    VIEW(__Rates)
                       PROJECT(RAT:ToMass)
                       PROJECT(RAT:RatePerKg)
                       PROJECT(RAT:MinimiumCharge)
                       PROJECT(RAT:Effective_Date)
                       PROJECT(RAT:Added_Date)
                       PROJECT(RAT:Added_Time)
                       PROJECT(RAT:AdHoc)
                       PROJECT(RAT:CID)
                       PROJECT(RAT:RID)
                       PROJECT(RAT:LTID)
                       JOIN(CLI:PKey_CID,RAT:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
RAT:ToMass             LIKE(RAT:ToMass)               !List box control field - type derived from field
RAT:RatePerKg          LIKE(RAT:RatePerKg)            !List box control field - type derived from field
RAT:MinimiumCharge     LIKE(RAT:MinimiumCharge)       !List box control field - type derived from field
RAT:Effective_Date     LIKE(RAT:Effective_Date)       !List box control field - type derived from field
RAT:Added_Date         LIKE(RAT:Added_Date)           !List box control field - type derived from field
RAT:Added_Time         LIKE(RAT:Added_Time)           !List box control field - type derived from field
RAT:AdHoc              LIKE(RAT:AdHoc)                !List box control field - type derived from field
RAT:AdHoc_Icon         LONG                           !Entry's icon ID
RAT:CID                LIKE(RAT:CID)                  !List box control field - type derived from field
RAT:RID                LIKE(RAT:RID)                  !Primary key field - type derived from field
RAT:LTID               LIKE(RAT:LTID)                 !Browse key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(ClientsRateTypes)
                       PROJECT(CRT:ClientRateType)
                       PROJECT(CRT:CRTID)
                       PROJECT(CRT:LTID)
                       PROJECT(CRT:CID)
                       JOIN(CLI:PKey_CID,CRT:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
CRT:ClientRateType     LIKE(CRT:ClientRateType)       !List box control field - type derived from field
CRT:CRTID              LIKE(CRT:CRTID)                !Primary key field - type derived from field
CRT:LTID               LIKE(CRT:LTID)                 !Browse key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::LOAD2:Record LIKE(LOAD2:RECORD),THREAD
QuickWindow          WINDOW('Form Load Types'),AT(,,358,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdateLoadTypes2'),SYSTEM
                       SHEET,AT(4,4,350,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Load Type:'),AT(9,22),USE(?LOAD2:LoadType:Prompt),TRN
                           ENTRY(@s100),AT(61,22,289,10),USE(LOAD2:LoadType),MSG('Load Type'),REQ,TIP('Load Type')
                           PROMPT('Load Option:'),AT(9,40),USE(?LOAD2:LoadOption:Prompt),TRN
                           LIST,AT(61,40,100,10),USE(LOAD2:LoadOption),DROP(15),FROM('Consolidated|#0|Container Pa' & |
  'rk|#1|Container|#2|Full Load|#3|Empty Container|#4|Local Delivery|#5'),MSG('Option - C' & |
  'onsolidated, Container Park, Container, Full Load, Empty Container, Local Delivery'), |
  TIP('Option<0DH,0AH>Consolidated, Container Park, Container, Full Load, Empty Contain' & |
  'er, Local Delivery')
                           CHECK(' &Hazchem'),AT(61,64),USE(LOAD2:Hazchem),TRN
                           CHECK(' &Turn In / Container Details'),AT(61,82),USE(LOAD2:TurnIn),MSG('Container Turn ' & |
  'In required for this Load Type'),TIP('Container Turn In required for this Load Type<0DH>' & |
  '<0AH>Container Details will be requested'),TRN
                           CHECK(' &Container Park Standard'),AT(61,98,112),USE(LOAD2:ContainerParkStandard),DISABLE, |
  MSG('None, Full Load, Container Park'),TIP('None - Breakbulk or Container Rates will ' & |
  'apply<0DH,0AH>Full Load - Full Load rates will apply<0DH,0AH>Container Park - Contai' & |
  'ner Park rates will apply'),TRN
                         END
                         TAB('&2) Rates'),USE(?Tab:2)
                           LIST,AT(7,21,342,92),USE(?Browse:2),HVSCROLL,FORMAT('60L(2)|M~Client Name~C(0)@s100@36R' & |
  '(2)|M~Client No.~C(0)@n_10b@46R(2)|M~To Mass~C(0)@n-12.0@50R(1)|M~Rate Per Kg~C(0)@n' & |
  '-13.4@58R(1)|M~Minimium Charge~C(0)@n-14.2@50R(2)|M~Effective Date~C(0)@d5@44R(2)|M~' & |
  'Added Date~C(0)@d5@44R(2)|M~Added Time~C(0)@t7@28R(2)|MI~Ad Hoc~C(0)@p p@40R(2)|M~CI' & |
  'D~C(0)@n_10@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the ClientsRateTypes file')
                         END
                         TAB('&3) Clients Rate Types'),USE(?Tab:3)
                           LIST,AT(7,21,342,92),USE(?Browse:4),HVSCROLL,FORMAT('60L(2)|M~Client Name~C(0)@s100@36R' & |
  '(2)|M~Client No.~C(0)@n_10b@80L(2)|M~Client Rate Type~C(0)@s100@'),FROM(Queue:Browse:4), |
  IMM,MSG('Browsing the ClientsRateTypes file')
                         END
                       END
                       BUTTON('&OK'),AT(252,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(306,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Load Type Record'
  OF InsertRecord
    ActionMessage = 'Load Type Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Load Type Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_LoadTypes')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOAD2:LoadType:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(LOAD2:Record,History::LOAD2:Record)
  SELF.AddHistoryField(?LOAD2:LoadType,2)
  SELF.AddHistoryField(?LOAD2:LoadOption,3)
  SELF.AddHistoryField(?LOAD2:Hazchem,5)
  SELF.AddHistoryField(?LOAD2:TurnIn,4)
  SELF.AddHistoryField(?LOAD2:ContainerParkStandard,6)
  SELF.AddUpdateFile(Access:LoadTypes2)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ClientsRateTypes.Open                             ! File ClientsRateTypes used by this procedure, so make sure it's RelationManager is open
  Access:LoadTypes2.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LoadTypes2
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:__Rates,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:ClientsRateTypes,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?LOAD2:LoadType{PROP:ReadOnly} = True
    DISABLE(?LOAD2:LoadOption)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,RAT:FKey_LTID)                        ! Add the sort order for RAT:FKey_LTID for sort order 1
  BRW2.AddRange(RAT:LTID,Relate:__Rates,Relate:LoadTypes2) ! Add file relationship range limit for sort order 1
  BRW2.AddField(CLI:ClientName,BRW2.Q.CLI:ClientName)      ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW2.AddField(CLI:ClientNo,BRW2.Q.CLI:ClientNo)          ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW2.AddField(RAT:ToMass,BRW2.Q.RAT:ToMass)              ! Field RAT:ToMass is a hot field or requires assignment from browse
  BRW2.AddField(RAT:RatePerKg,BRW2.Q.RAT:RatePerKg)        ! Field RAT:RatePerKg is a hot field or requires assignment from browse
  BRW2.AddField(RAT:MinimiumCharge,BRW2.Q.RAT:MinimiumCharge) ! Field RAT:MinimiumCharge is a hot field or requires assignment from browse
  BRW2.AddField(RAT:Effective_Date,BRW2.Q.RAT:Effective_Date) ! Field RAT:Effective_Date is a hot field or requires assignment from browse
  BRW2.AddField(RAT:Added_Date,BRW2.Q.RAT:Added_Date)      ! Field RAT:Added_Date is a hot field or requires assignment from browse
  BRW2.AddField(RAT:Added_Time,BRW2.Q.RAT:Added_Time)      ! Field RAT:Added_Time is a hot field or requires assignment from browse
  BRW2.AddField(RAT:AdHoc,BRW2.Q.RAT:AdHoc)                ! Field RAT:AdHoc is a hot field or requires assignment from browse
  BRW2.AddField(RAT:CID,BRW2.Q.RAT:CID)                    ! Field RAT:CID is a hot field or requires assignment from browse
  BRW2.AddField(RAT:RID,BRW2.Q.RAT:RID)                    ! Field RAT:RID is a hot field or requires assignment from browse
  BRW2.AddField(RAT:LTID,BRW2.Q.RAT:LTID)                  ! Field RAT:LTID is a hot field or requires assignment from browse
  BRW2.AddField(CLI:CID,BRW2.Q.CLI:CID)                    ! Field CLI:CID is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4.AddSortOrder(,CRT:FKey_LTID)                        ! Add the sort order for CRT:FKey_LTID for sort order 1
  BRW4.AddRange(CRT:LTID,Relate:ClientsRateTypes,Relate:LoadTypes2) ! Add file relationship range limit for sort order 1
  BRW4.AddField(CLI:ClientName,BRW4.Q.CLI:ClientName)      ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW4.AddField(CLI:ClientNo,BRW4.Q.CLI:ClientNo)          ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW4.AddField(CRT:ClientRateType,BRW4.Q.CRT:ClientRateType) ! Field CRT:ClientRateType is a hot field or requires assignment from browse
  BRW4.AddField(CRT:CRTID,BRW4.Q.CRT:CRTID)                ! Field CRT:CRTID is a hot field or requires assignment from browse
  BRW4.AddField(CRT:LTID,BRW4.Q.CRT:LTID)                  ! Field CRT:LTID is a hot field or requires assignment from browse
  BRW4.AddField(CLI:CID,BRW4.Q.CLI:CID)                    ! Field CLI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_LoadTypes',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      LOC:Original_LoadOption = LOAD2:LoadOption
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ClientsRateTypes.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_LoadTypes',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
          IF SELF.Request = ChangeRecord AND LOC:Original_LoadOption = 2 AND LOC:Original_LoadOption ~= LOAD2:LoadOption
             ! Change record, load option was container, no longer is...
             CASE MESSAGE('You are changing the Load Option from Container.||The Container Load Option has special functionality ' & |
                      'associated with it.||The Rates using this Loadtype need to have any associated container info ' & |
                      'removed from them.|This will also affect historical rates but will not affect any existing DIs or Invoics, only DIs that are edited after this stage.||This will be done now if you select Yes to continue.', 'Container Load Option', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:No
                QuickWindow{PROP:AcceptAll}   = FALSE
                CYCLE
             OF BUTTON:Yes
                Update_DBStructure('UPDATE    __Rates ' & |
                          'SET              CTID = 0 ' & |
                          'WHERE     (CTID <> 0) AND (__Rates.LTID NOT IN ' & |
                          '                          (SELECT     loadtypes2.LTID ' & |
                          '                            FROM          loadtypes2 ' & |
                          '                            WHERE      loadoption = 2))')
          .  .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  SELF.Q.RAT:AdHoc_Icon = 0


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

