

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS039.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Drivers PROCEDURE (p:DI_Drivers, p:D_Type)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Group            GROUP,PRE(L_G)                        !
BranchName           STRING(35)                            !Branch Name
                     END                                   !
LOC:Archived         BYTE                                  !
BRW2::View:Browse    VIEW(Manifest)
                       PROJECT(MAN:MID)
                       PROJECT(MAN:DepartDate)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:Rate)
                       PROJECT(MAN:VATRate)
                       PROJECT(MAN:State)
                       PROJECT(MAN:CreatedDate)
                       PROJECT(MAN:DepartTime)
                       PROJECT(MAN:TID)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:DRID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
MAN:MID                LIKE(MAN:MID)                  !List box control field - type derived from field
MAN:DepartDate         LIKE(MAN:DepartDate)           !List box control field - type derived from field
MAN:Cost               LIKE(MAN:Cost)                 !List box control field - type derived from field
MAN:Rate               LIKE(MAN:Rate)                 !List box control field - type derived from field
MAN:VATRate            LIKE(MAN:VATRate)              !List box control field - type derived from field
MAN:State              LIKE(MAN:State)                !List box control field - type derived from field
MAN:CreatedDate        LIKE(MAN:CreatedDate)          !List box control field - type derived from field
MAN:DepartTime         LIKE(MAN:DepartTime)           !List box control field - type derived from field
MAN:TID                LIKE(MAN:TID)                  !List box control field - type derived from field
MAN:BID                LIKE(MAN:BID)                  !List box control field - type derived from field
MAN:DRID               LIKE(MAN:DRID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(TripSheets)
                       PROJECT(TRI:TRID)
                       PROJECT(TRI:DepartDate)
                       PROJECT(TRI:DepartTime)
                       PROJECT(TRI:ReturnedDate)
                       PROJECT(TRI:ReturnedTime)
                       PROJECT(TRI:Notes)
                       PROJECT(TRI:BID)
                       PROJECT(TRI:State)
                       PROJECT(TRI:DRID)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
TRI:TRID               LIKE(TRI:TRID)                 !List box control field - type derived from field
TRI:DepartDate         LIKE(TRI:DepartDate)           !List box control field - type derived from field
TRI:DepartTime         LIKE(TRI:DepartTime)           !List box control field - type derived from field
TRI:ReturnedDate       LIKE(TRI:ReturnedDate)         !List box control field - type derived from field
TRI:ReturnedTime       LIKE(TRI:ReturnedTime)         !List box control field - type derived from field
TRI:Notes              LIKE(TRI:Notes)                !List box control field - type derived from field
TRI:BID                LIKE(TRI:BID)                  !List box control field - type derived from field
TRI:State              LIKE(TRI:State)                !List box control field - type derived from field
TRI:DRID               LIKE(TRI:DRID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB3::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?L_G:BranchName
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DRI:Record  LIKE(DRI:RECORD),THREAD
QuickWindow          WINDOW('Form Drivers'),AT(,,281,234),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('UpdateDrivers'),SYSTEM
                       PROMPT('DrID:'),AT(191,5),USE(?DRI:DRID:Prompt)
                       STRING(@n_10),AT(215,5),USE(DRI:DRID),RIGHT(1)
                       SHEET,AT(4,4,273,208),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('First Name:'),AT(9,25),USE(?DRI:FirstName:Prompt),TRN
                           ENTRY(@s35),AT(65,25,144,10),USE(DRI:FirstName),MSG('First Name'),REQ,TIP('First Name')
                           PROMPT('Surname:'),AT(9,39),USE(?DRI:Surname:Prompt),TRN
                           ENTRY(@s35),AT(65,39,144,10),USE(DRI:Surname),MSG('Surname'),TIP('Surname')
                           PROMPT('Employee No.:'),AT(9,54),USE(?DRI:EmployeeNo:Prompt),TRN
                           ENTRY(@s20),AT(65,54,84,10),USE(DRI:EmployeeNo),MSG('Employee No.'),TIP('Employee No.')
                           PROMPT('Type:'),AT(9,73),USE(?DRI:Type:Prompt),TRN
                           LIST,AT(65,73,84,10),USE(DRI:Type),DROP(5),FROM('Driver|#0|Assistant|#1'),MSG('Driver or Assistant'), |
  TIP('Driver or Assistant')
                           PROMPT('Category:'),AT(9,92),USE(?DRI:Category:Prompt),TRN
                           LIST,AT(65,92,84,10),USE(DRI:Category),DROP(5),FROM('Branch|#0|Long Distance|#1'),MSG('Driver Category'), |
  TIP('Driver Category')
                           PROMPT('Branch:'),AT(9,118),USE(?Prompt6),TRN
                           LIST,AT(65,118,84,10),USE(L_G:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                           CHECK(' &Archived'),AT(65,134),USE(DRI:Archived),MSG('Mark driver as not active'),TIP('Mark drive' & |
  'r as not active'),TRN
                           ENTRY(@d17),AT(113,134,56,10),USE(DRI:Archived_Date),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP
                         END
                         TAB('&2) Manifest'),USE(?Tab:2)
                           LIST,AT(11,23,257,183),USE(?Browse:2),HVSCROLL,FORMAT('40R(1)|M~MID~C(0)@n_10@44R(2)|M~' & |
  'Depart Date~C(0)@d5b@52R(1)|M~Cost~C(0)@n-14.2@38R(1)|M~Rate~C(0)@n-10.2@36R(1)|M~VA' & |
  'T Rate~C(0)@n-7.2@24R(2)|M~State~C(0)@n3@48R(2)|M~Created Date~C(0)@d6@46R(2)|M~Depa' & |
  'rt Time~C(0)@t7@30R(2)|M~TID~C(0)@n_10@30R(2)|M~BID~C(0)@n_10@'),FROM(Queue:Browse:2),IMM, |
  MSG('Browsing the TripSheets file')
                         END
                         TAB('&3) TripSheets'),USE(?Tab:3)
                           LIST,AT(11,23,257,183),USE(?Browse:4),HVSCROLL,FORMAT('40R(2)|M~TRID~C(0)@N_10@48R(2)|M' & |
  '~Depart Date~C(0)@d6@48R(2)|M~Depart Time~C(0)@t7@52R(2)|M~Returned Date~C(0)@d6@52R' & |
  '(2)|M~Returned Time~C(0)@t7@80L(2)|M~Notes~@s255@30R(2)|M~BID~C(0)@n_10@24R(2)|M~Sta' & |
  'te~C(0)@n3@'),FROM(Queue:Browse:4),IMM,MSG('Browsing the TripSheets file')
                         END
                       END
                       BUTTON('&OK'),AT(173,215,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(227,215,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,215,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
                     END

BRW4::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB3                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Passed                ROUTINE
    ! (p:DI_Drivers, p:D_Type)
    IF ~OMITTED(1)
       IF p:DI_Drivers = TRUE
          DRI:Category     = 1  ! Branch|Long Distance
    .  .
    IF ~OMITTED(2)
       IF p:D_Type ~= 0
          DRI:Type     = p:D_Type
    .  .

    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Drivers Record'
  OF InsertRecord
    ActionMessage = 'Drivers Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Drivers Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Drivers')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DRI:DRID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DRI:Record,History::DRI:Record)
  SELF.AddHistoryField(?DRI:DRID,1)
  SELF.AddHistoryField(?DRI:FirstName,2)
  SELF.AddHistoryField(?DRI:Surname,3)
  SELF.AddHistoryField(?DRI:EmployeeNo,4)
  SELF.AddHistoryField(?DRI:Type,5)
  SELF.AddHistoryField(?DRI:Category,7)
  SELF.AddHistoryField(?DRI:Archived,9)
  SELF.AddHistoryField(?DRI:Archived_Date,12)
  SELF.AddUpdateFile(Access:Drivers)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Drivers
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:Manifest,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:TripSheets,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?DRI:FirstName{PROP:ReadOnly} = True
    ?DRI:Surname{PROP:ReadOnly} = True
    ?DRI:EmployeeNo{PROP:ReadOnly} = True
    DISABLE(?DRI:Type)
    DISABLE(?DRI:Category)
    DISABLE(?L_G:BranchName)
    ?DRI:Archived_Date{PROP:ReadOnly} = True
  END
      LOC:Archived    = DRI:Archived
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon MAN:DRID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,MAN:FKey_DRID)   ! Add the sort order for MAN:FKey_DRID for sort order 1
  BRW2.AddRange(MAN:DRID,Relate:Manifest,Relate:Drivers)   ! Add file relationship range limit for sort order 1
  BRW2.AddField(MAN:MID,BRW2.Q.MAN:MID)                    ! Field MAN:MID is a hot field or requires assignment from browse
  BRW2.AddField(MAN:DepartDate,BRW2.Q.MAN:DepartDate)      ! Field MAN:DepartDate is a hot field or requires assignment from browse
  BRW2.AddField(MAN:Cost,BRW2.Q.MAN:Cost)                  ! Field MAN:Cost is a hot field or requires assignment from browse
  BRW2.AddField(MAN:Rate,BRW2.Q.MAN:Rate)                  ! Field MAN:Rate is a hot field or requires assignment from browse
  BRW2.AddField(MAN:VATRate,BRW2.Q.MAN:VATRate)            ! Field MAN:VATRate is a hot field or requires assignment from browse
  BRW2.AddField(MAN:State,BRW2.Q.MAN:State)                ! Field MAN:State is a hot field or requires assignment from browse
  BRW2.AddField(MAN:CreatedDate,BRW2.Q.MAN:CreatedDate)    ! Field MAN:CreatedDate is a hot field or requires assignment from browse
  BRW2.AddField(MAN:DepartTime,BRW2.Q.MAN:DepartTime)      ! Field MAN:DepartTime is a hot field or requires assignment from browse
  BRW2.AddField(MAN:TID,BRW2.Q.MAN:TID)                    ! Field MAN:TID is a hot field or requires assignment from browse
  BRW2.AddField(MAN:BID,BRW2.Q.MAN:BID)                    ! Field MAN:BID is a hot field or requires assignment from browse
  BRW2.AddField(MAN:DRID,BRW2.Q.MAN:DRID)                  ! Field MAN:DRID is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon TRI:DRID for sort order 1
  BRW4.AddSortOrder(BRW4::Sort0:StepClass,TRI:FKey_DRID)   ! Add the sort order for TRI:FKey_DRID for sort order 1
  BRW4.AddRange(TRI:DRID,Relate:TripSheets,Relate:Drivers) ! Add file relationship range limit for sort order 1
  BRW4.AddField(TRI:TRID,BRW4.Q.TRI:TRID)                  ! Field TRI:TRID is a hot field or requires assignment from browse
  BRW4.AddField(TRI:DepartDate,BRW4.Q.TRI:DepartDate)      ! Field TRI:DepartDate is a hot field or requires assignment from browse
  BRW4.AddField(TRI:DepartTime,BRW4.Q.TRI:DepartTime)      ! Field TRI:DepartTime is a hot field or requires assignment from browse
  BRW4.AddField(TRI:ReturnedDate,BRW4.Q.TRI:ReturnedDate)  ! Field TRI:ReturnedDate is a hot field or requires assignment from browse
  BRW4.AddField(TRI:ReturnedTime,BRW4.Q.TRI:ReturnedTime)  ! Field TRI:ReturnedTime is a hot field or requires assignment from browse
  BRW4.AddField(TRI:Notes,BRW4.Q.TRI:Notes)                ! Field TRI:Notes is a hot field or requires assignment from browse
  BRW4.AddField(TRI:BID,BRW4.Q.TRI:BID)                    ! Field TRI:BID is a hot field or requires assignment from browse
  BRW4.AddField(TRI:State,BRW4.Q.TRI:State)                ! Field TRI:State is a hot field or requires assignment from browse
  BRW4.AddField(TRI:DRID,BRW4.Q.TRI:DRID)                  ! Field TRI:DRID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Drivers',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  IF ?DRI:Archived{Prop:Checked}
    UNHIDE(?DRI:Archived_Date)
  END
  IF NOT ?DRI:Archived{PROP:Checked}
    HIDE(?DRI:Archived_Date)
  END
  FDB3.Init(?L_G:BranchName,Queue:FileDrop.ViewPosition,FDB3::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB3.Q &= Queue:FileDrop
  FDB3.AddSortOrder(BRA:Key_BranchName)
  FDB3.AddField(BRA:BranchName,FDB3.Q.BRA:BranchName) !List box control field - type derived from field
  FDB3.AddField(BRA:BID,FDB3.Q.BRA:BID) !Primary key field - type derived from field
  FDB3.AddUpdateField(BRA:BID,DRI:BID)
  ThisWindow.AddItem(FDB3.WindowComponent)
      BRA:BID             = DRI:BID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         L_G:BranchName   = BRA:BranchName
      .
      IF SELF.Request = InsertRecord
         DO Check_Passed
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Drivers',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DRI:Archived
      IF ?DRI:Archived{PROP:Checked}
        UNHIDE(?DRI:Archived_Date)
      END
      IF NOT ?DRI:Archived{PROP:Checked}
        HIDE(?DRI:Archived_Date)
      END
      ThisWindow.Reset()
          IF DRI:Archived = TRUE AND LOC:Archived = FALSE
             DRI:Archived_Date    = TODAY()
             DRI:Archived_Time    = CLOCK()
             DISPLAY
          .
    OF ?OK
      ThisWindow.Update()
          DRI:FirstNameSurname    = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

