

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS005.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Browse the Vessels file
!!! </summary>
Browse_Vessels PROCEDURE 

CurrentTab           STRING(80)                            !
Locator              STRING(30)                            !
BRW1::View:Browse    VIEW(Vessels)
                       PROJECT(VES:Vessel)
                       PROJECT(VES:VEID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
VES:Vessel             LIKE(VES:Vessel)               !List box control field - type derived from field
VES:VEID               LIKE(VES:VEID)                 !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Vessels file'),AT(,,277,198),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_Vessels'),SYSTEM
                       LIST,AT(8,36,261,118),USE(?Browse:1),HVSCROLL,FORMAT('200L(2)|M~Vessel~@s50@40L(2)|M~VE' & |
  'ID~L(1)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Vessels file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Vessel'),USE(?Tab:2)
                           STRING(@s30),AT(9,22,121),USE(Locator)
                         END
                       END
                       BUTTON('&Close'),AT(222,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(8,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Vessels')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Vessels.Open                                      ! File Vessels used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Vessels,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,VES:Key_Vessel)                       ! Add the sort order for VES:Key_Vessel for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(?Locator,VES:Vessel,1,BRW1)     ! Initialize the browse locator using ?Locator using key: VES:Key_Vessel , VES:Vessel
  BRW1.AddField(VES:Vessel,BRW1.Q.VES:Vessel)              ! Field VES:Vessel is a hot field or requires assignment from browse
  BRW1.AddField(VES:VEID,BRW1.Q.VES:VEID)                  ! Field VES:VEID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Vessels',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Vessels.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Vessels',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    UpdateVessels
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Form Vessels
!!! </summary>
UpdateVessels PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::VES:Record  LIKE(VES:RECORD),THREAD
QuickWindow          WINDOW('Form Vessels'),AT(,,273,56),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('UpdateVessels'),SYSTEM
                       SHEET,AT(4,4,265,30),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Vessel:'),AT(8,20),USE(?VES:Vessel:Prompt),TRN
                           ENTRY(@s50),AT(61,20,204,10),USE(VES:Vessel),MSG('Vessel Name'),TIP('Vessel Name')
                         END
                       END
                       BUTTON('&OK'),AT(114,38,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(167,38,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(220,38,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateVessels')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?VES:Vessel:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Vessels)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(VES:Record,History::VES:Record)
  SELF.AddHistoryField(?VES:Vessel,2)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Vessels.Open                                      ! File Vessels used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Vessels
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?VES:Vessel{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('UpdateVessels',QuickWindow)                ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Vessels.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateVessels',QuickWindow)             ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! (p:Option)
!!! </summary>
Browse_EmailAddresses PROCEDURE (p:Option)

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(EmailAddresses)
                       PROJECT(EMAI:EmailName)
                       PROJECT(EMAI:EmailAddress)
                       PROJECT(EMAI:RateLetter)
                       PROJECT(EMAI:DefaultAddress)
                       PROJECT(EMAI:Operations)
                       PROJECT(EMAI:OperationsReference)
                       PROJECT(EMAI:EAID)
                       PROJECT(EMAI:CID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
EMAI:EmailName         LIKE(EMAI:EmailName)           !List box control field - type derived from field
EMAI:EmailAddress      LIKE(EMAI:EmailAddress)        !List box control field - type derived from field
EMAI:RateLetter        LIKE(EMAI:RateLetter)          !List box control field - type derived from field
EMAI:RateLetter_Icon   LONG                           !Entry's icon ID
EMAI:DefaultAddress    LIKE(EMAI:DefaultAddress)      !List box control field - type derived from field
EMAI:DefaultAddress_Icon LONG                         !Entry's icon ID
EMAI:Operations        LIKE(EMAI:Operations)          !List box control field - type derived from field
EMAI:Operations_Icon   LONG                           !Entry's icon ID
EMAI:OperationsReference LIKE(EMAI:OperationsReference) !List box control field - type derived from field
EMAI:EAID              LIKE(EMAI:EAID)                !Primary key field - type derived from field
EMAI:CID               LIKE(EMAI:CID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Email Addresses'),AT(,,358,198),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_EmailAddresses'),SYSTEM
                       LIST,AT(8,30,341,124),USE(?Browse:1),HVSCROLL,FORMAT('60L(2)|M~Email Name~@s35@130L(2)|' & |
  'M~Email Address~@s255@38R(2)|MI~Rate Letter~C(0)@p p@36R(2)|MI~ Default~C(0)@p p@38R' & |
  '(2)|MI~Operations~C(0)@p p@80L(2)|M~Operations Reference~C(0)@s35@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the EmailAddresses file')
                       BUTTON('&Select'),AT(7,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Client'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(303,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,178,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_EmailAddresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EmailAddresses,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,EMAI:FKey_CID)                        ! Add the sort order for EMAI:FKey_CID for sort order 1
  BRW1.AddRange(EMAI:CID,Relate:EmailAddresses,Relate:Clients) ! Add file relationship range limit for sort order 1
  BRW1.AppendOrder('+EMAI:EmailName,+EMAI:EmailAddress,+EMAI:EAID') ! Append an additional sort order
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(EMAI:EmailName,BRW1.Q.EMAI:EmailName)      ! Field EMAI:EmailName is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:EmailAddress,BRW1.Q.EMAI:EmailAddress) ! Field EMAI:EmailAddress is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:RateLetter,BRW1.Q.EMAI:RateLetter)    ! Field EMAI:RateLetter is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:DefaultAddress,BRW1.Q.EMAI:DefaultAddress) ! Field EMAI:DefaultAddress is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:Operations,BRW1.Q.EMAI:Operations)    ! Field EMAI:Operations is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:OperationsReference,BRW1.Q.EMAI:OperationsReference) ! Field EMAI:OperationsReference is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:EAID,BRW1.Q.EMAI:EAID)                ! Field EMAI:EAID is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:CID,BRW1.Q.EMAI:CID)                  ! Field EMAI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Email_Addresses(p:Option)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (EMAI:RateLetter = 1)
    SELF.Q.EMAI:RateLetter_Icon = 2                        ! Set icon from icon list
  ELSE
    SELF.Q.EMAI:RateLetter_Icon = 1                        ! Set icon from icon list
  END
  IF (EMAI:DefaultAddress = 1)
    SELF.Q.EMAI:DefaultAddress_Icon = 2                    ! Set icon from icon list
  ELSE
    SELF.Q.EMAI:DefaultAddress_Icon = 1                    ! Set icon from icon list
  END
  IF (EMAI:Operations = 1)
    SELF.Q.EMAI:Operations_Icon = 2                        ! Set icon from icon list
  ELSE
    SELF.Q.EMAI:Operations_Icon = 1                        ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Form EmailAddresses
!!! </summary>
UpdateEmailAddresses PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::EMAI:Record LIKE(EMAI:RECORD),THREAD
QuickWindow          WINDOW('Form EmailAddresses'),AT(,,358,122),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('UpdateEmailAddresses'),SYSTEM
                       SHEET,AT(4,4,350,96),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Email Name:'),AT(8,20),USE(?EMAI:EmailName:Prompt),TRN
                           ENTRY(@s35),AT(96,20,144,10),USE(EMAI:EmailName),MSG('Name of contact'),REQ,TIP('Name of contact')
                           PROMPT('Email Address:'),AT(8,34),USE(?EMAI:EmailAddress:Prompt),TRN
                           ENTRY(@s255),AT(96,34,254,10),USE(EMAI:EmailAddress),MSG('Email Address'),REQ,TIP('Email Address')
                           CHECK('Rate Letter'),AT(96,48,70,8),USE(EMAI:RateLetter),MSG('This email address gets s' & |
  'ent rate letters'),TIP('This email address gets sent rate letters')
                           PROMPT('CID:'),AT(8,60),USE(?EMAI:CID:Prompt),TRN
                           STRING(@n_10),AT(96,60,104,10),USE(EMAI:CID),RIGHT(1),TRN
                           CHECK(' Default'),AT(96,74,70,8),USE(EMAI:DefaultAddress),MSG('Default Email Address'),TIP('Default Em' & |
  'ail Address')
                           CHECK(' Operations'),AT(170,74,70,8),USE(EMAI:Operations),MSG('This email address is fo' & |
  'r the client Operations team'),TIP('This email address is for the client Operations team')
                           PROMPT('Operations Reference:'),AT(8,86),USE(?EMAI:OperationsReference:Prompt),TRN
                           ENTRY(@s35),AT(96,86,144,10),USE(EMAI:OperationsReference),MSG('A reference for the Ope' & |
  'rations team'),TIP('A reference for the Operations team')
                         END
                       END
                       BUTTON('&OK'),AT(199,104,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(252,104,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(305,104,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateEmailAddresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?EMAI:EmailName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:EmailAddresses)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(EMAI:Record,History::EMAI:Record)
  SELF.AddHistoryField(?EMAI:EmailName,3)
  SELF.AddHistoryField(?EMAI:EmailAddress,4)
  SELF.AddHistoryField(?EMAI:RateLetter,5)
  SELF.AddHistoryField(?EMAI:CID,6)
  SELF.AddHistoryField(?EMAI:DefaultAddress,7)
  SELF.AddHistoryField(?EMAI:Operations,8)
  SELF.AddHistoryField(?EMAI:OperationsReference,9)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:EmailAddresses.Open                               ! File EmailAddresses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:EmailAddresses
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?EMAI:EmailName{PROP:ReadOnly} = True
    ?EMAI:EmailAddress{PROP:ReadOnly} = True
    ?EMAI:OperationsReference{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('UpdateEmailAddresses',QuickWindow)         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EmailAddresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateEmailAddresses',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_WebClientsLoginLog PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::WLOG:Record LIKE(WLOG:RECORD),THREAD
QuickWindow          WINDOW('Form Web Clients Login Log'),AT(,,200,168),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateWebClientsLoginLog'),SYSTEM
                       SHEET,AT(4,4,192,142),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Login Log ID:'),AT(9,20),USE(?WLOG:LoginLogID:Prompt),TRN
                           ENTRY(@n-14),AT(89,20,64,10),USE(WLOG:LoginLogID),RIGHT(1)
                           PROMPT('Web Client ID:'),AT(9,34),USE(?WLOG:WebClientID:Prompt),TRN
                           ENTRY(@n-14),AT(89,34,64,10),USE(WLOG:WebClientID),RIGHT(1)
                           PROMPT('Client Login:'),AT(9,48),USE(?WLOG:ClientLogin:Prompt),TRN
                           ENTRY(@s20),AT(89,48,84,10),USE(WLOG:ClientLogin)
                           PROMPT('Failed Password:'),AT(9,62),USE(?WLOG:FailedPassword:Prompt),TRN
                           ENTRY(@s20),AT(89,62,84,10),USE(WLOG:FailedPassword)
                           PROMPT('Login IP:'),AT(9,76),USE(?WLOG:LoginIP:Prompt),TRN
                           ENTRY(@s15),AT(89,76,84,10),USE(WLOG:LoginIP)
                           PROMPT('Login Date:'),AT(9,104),USE(?WLOG:LoginDateTime_DATE:Prompt),TRN
                           ENTRY(@d17),AT(89,104,104,10),USE(WLOG:LoginDateTime_DATE),RIGHT(1)
                           PROMPT('Login Time:'),AT(9,118),USE(?WLOG:LoginDateTime_TIME:Prompt),TRN
                           ENTRY(@t7),AT(89,118,104,10),USE(WLOG:LoginDateTime_TIME),RIGHT(1)
                           PROMPT('OffLine:'),AT(9,132),USE(?WLOG:OffLine:Prompt),TRN
                           ENTRY(@n3),AT(89,132,40,10),USE(WLOG:OffLine),RIGHT(1)
                         END
                       END
                       BUTTON('&OK'),AT(94,150,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(148,150,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,150,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a WebClientsLoginLog Record'
  OF ChangeRecord
    ActionMessage = 'Changing a WebClientsLoginLog Record'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_WebClientsLoginLog')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WLOG:LoginLogID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:WebClientsLoginLog)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(WLOG:Record,History::WLOG:Record)
  SELF.AddHistoryField(?WLOG:LoginLogID,1)
  SELF.AddHistoryField(?WLOG:WebClientID,2)
  SELF.AddHistoryField(?WLOG:ClientLogin,3)
  SELF.AddHistoryField(?WLOG:FailedPassword,4)
  SELF.AddHistoryField(?WLOG:LoginIP,5)
  SELF.AddHistoryField(?WLOG:LoginDateTime_DATE,8)
  SELF.AddHistoryField(?WLOG:LoginDateTime_TIME,9)
  SELF.AddHistoryField(?WLOG:OffLine,10)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:WebClientsLoginLog.Open                           ! File WebClientsLoginLog used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:WebClientsLoginLog
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?WLOG:LoginLogID{PROP:ReadOnly} = True
    ?WLOG:WebClientID{PROP:ReadOnly} = True
    ?WLOG:ClientLogin{PROP:ReadOnly} = True
    ?WLOG:FailedPassword{PROP:ReadOnly} = True
    ?WLOG:LoginIP{PROP:ReadOnly} = True
    ?WLOG:LoginDateTime_DATE{PROP:ReadOnly} = True
    ?WLOG:LoginDateTime_TIME{PROP:ReadOnly} = True
    ?WLOG:OffLine{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_WebClientsLoginLog',QuickWindow)    ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:WebClientsLoginLog.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_WebClientsLoginLog',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Delivery_CODAddresses PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:BID)
                       PROJECT(DEL:CID)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:Rate)
                       PROJECT(DEL:DocumentCharge)
                       PROJECT(DEL:FuelSurcharge)
                       PROJECT(DEL:Charge)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:DC_ID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:DIDate             LIKE(DEL:DIDate)               !List box control field - type derived from field
DEL:BID                LIKE(DEL:BID)                  !List box control field - type derived from field
DEL:CID                LIKE(DEL:CID)                  !List box control field - type derived from field
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
DEL:Rate               LIKE(DEL:Rate)                 !List box control field - type derived from field
DEL:DocumentCharge     LIKE(DEL:DocumentCharge)       !List box control field - type derived from field
DEL:FuelSurcharge      LIKE(DEL:FuelSurcharge)        !List box control field - type derived from field
DEL:Charge             LIKE(DEL:Charge)               !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Primary key field - type derived from field
DEL:DC_ID              LIKE(DEL:DC_ID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DCADD:Record LIKE(DCADD:RECORD),THREAD
QuickWindow          WINDOW('Form Delivery COD / Pre Paid Addresses'),AT(,,249,140),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('UpdateDelivery_CODAddresses'),SYSTEM
                       SHEET,AT(4,4,242,114),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Address Name:'),AT(9,20),USE(?DCADD:AddressName:Prompt),TRN
                           ENTRY(@s35),AT(97,20,144,10),USE(DCADD:AddressName),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                           PROMPT('Line 1:'),AT(9,34),USE(?DCADD:Line1:Prompt),TRN
                           ENTRY(@s35),AT(97,34,144,10),USE(DCADD:Line1),MSG('Address line 1'),TIP('Address line 1')
                           PROMPT('Line 2:'),AT(9,48),USE(?DCADD:Line2:Prompt),TRN
                           ENTRY(@s35),AT(97,48,144,10),USE(DCADD:Line2),MSG('Address line 2'),TIP('Address line 2')
                           PROMPT('Line 3 (Suburb):'),AT(9,62),USE(?DCADD:Line3:Prompt),TRN
                           ENTRY(@s35),AT(97,62,144,10),USE(DCADD:Line3)
                           PROMPT('Line 4 (Postal Code):'),AT(9,76),USE(?DCADD:Line4:Prompt),TRN
                           ENTRY(@s35),AT(97,76,144,10),USE(DCADD:Line4)
                           PROMPT('Line 5:'),AT(9,90),USE(?DCADD:Line5:Prompt),HIDE,TRN
                           ENTRY(@s35),AT(97,90,144,10),USE(DCADD:Line5),HIDE
                           PROMPT('VAT No:'),AT(9,104),USE(?DCADD:VATNo:Prompt),TRN
                           ENTRY(@s20),AT(97,104,84,10),USE(DCADD:VATNo),MSG('VAT No.'),TIP('VAT No.')
                         END
                         TAB('&2) Deliveries'),USE(?Tab:2)
                           LIST,AT(9,20,233,93),USE(?Browse:2),HVSCROLL,FORMAT('30R(2)|M~DI No.~C(0)@n_10@46R(2)|M' & |
  '~DI Date~C(0)@d6@30R(2)|M~BID~C(0)@n_10@30R(2)|M~CID~C(0)@n_10@80L(2)|M~Client Refer' & |
  'ence~@s30@52R(1)|M~Rate~C(0)@n-11.2@64R(1)|M~Document Charge~C(0)@n-11.2@60R(1)|M~Fu' & |
  'el Surcharge~C(0)@n-11.2@60R(1)|M~Charge~C(0)@n-13.2@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he Deliveries file')
                         END
                       END
                       BUTTON('&OK'),AT(142,122,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(196,122,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,122,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a Delivery_CODAddresses Record'
  OF ChangeRecord
    ActionMessage = 'Changing a Delivery_CODAddresses Record'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Delivery_CODAddresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DCADD:AddressName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Delivery_CODAddresses)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DCADD:Record,History::DCADD:Record)
  SELF.AddHistoryField(?DCADD:AddressName,2)
  SELF.AddHistoryField(?DCADD:Line1,3)
  SELF.AddHistoryField(?DCADD:Line2,4)
  SELF.AddHistoryField(?DCADD:Line3,5)
  SELF.AddHistoryField(?DCADD:Line4,6)
  SELF.AddHistoryField(?DCADD:Line5,7)
  SELF.AddHistoryField(?DCADD:VATNo,8)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Access:Delivery_CODAddresses.UseFile                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Delivery_CODAddresses
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?DCADD:AddressName{PROP:ReadOnly} = True
    ?DCADD:Line1{PROP:ReadOnly} = True
    ?DCADD:Line2{PROP:ReadOnly} = True
    ?DCADD:Line3{PROP:ReadOnly} = True
    ?DCADD:Line4{PROP:ReadOnly} = True
    ?DCADD:Line5{PROP:ReadOnly} = True
    ?DCADD:VATNo{PROP:ReadOnly} = True
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon DEL:DC_ID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,DEL:FKey_DCID)   ! Add the sort order for DEL:FKey_DCID for sort order 1
  BRW2.AddRange(DEL:DC_ID,Relate:Deliveries,Relate:Delivery_CODAddresses) ! Add file relationship range limit for sort order 1
  BRW2.AddField(DEL:DINo,BRW2.Q.DEL:DINo)                  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW2.AddField(DEL:DIDate,BRW2.Q.DEL:DIDate)              ! Field DEL:DIDate is a hot field or requires assignment from browse
  BRW2.AddField(DEL:BID,BRW2.Q.DEL:BID)                    ! Field DEL:BID is a hot field or requires assignment from browse
  BRW2.AddField(DEL:CID,BRW2.Q.DEL:CID)                    ! Field DEL:CID is a hot field or requires assignment from browse
  BRW2.AddField(DEL:ClientReference,BRW2.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW2.AddField(DEL:Rate,BRW2.Q.DEL:Rate)                  ! Field DEL:Rate is a hot field or requires assignment from browse
  BRW2.AddField(DEL:DocumentCharge,BRW2.Q.DEL:DocumentCharge) ! Field DEL:DocumentCharge is a hot field or requires assignment from browse
  BRW2.AddField(DEL:FuelSurcharge,BRW2.Q.DEL:FuelSurcharge) ! Field DEL:FuelSurcharge is a hot field or requires assignment from browse
  BRW2.AddField(DEL:Charge,BRW2.Q.DEL:Charge)              ! Field DEL:Charge is a hot field or requires assignment from browse
  BRW2.AddField(DEL:DID,BRW2.Q.DEL:DID)                    ! Field DEL:DID is a hot field or requires assignment from browse
  BRW2.AddField(DEL:DC_ID,BRW2.Q.DEL:DC_ID)                ! Field DEL:DC_ID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Delivery_CODAddresses',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Delivery_CODAddresses',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_CODAddresses PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:Locator          STRING(35)                            !
BRW1::View:Browse    VIEW(Delivery_CODAddresses)
                       PROJECT(DCADD:AddressName)
                       PROJECT(DCADD:Line1)
                       PROJECT(DCADD:Line2)
                       PROJECT(DCADD:Line3)
                       PROJECT(DCADD:Line4)
                       PROJECT(DCADD:VATNo)
                       PROJECT(DCADD:DC_ID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DCADD:AddressName      LIKE(DCADD:AddressName)        !List box control field - type derived from field
DCADD:Line1            LIKE(DCADD:Line1)              !List box control field - type derived from field
DCADD:Line2            LIKE(DCADD:Line2)              !List box control field - type derived from field
DCADD:Line3            LIKE(DCADD:Line3)              !List box control field - type derived from field
DCADD:Line4            LIKE(DCADD:Line4)              !List box control field - type derived from field
DCADD:VATNo            LIKE(DCADD:VATNo)              !List box control field - type derived from field
DCADD:DC_ID            LIKE(DCADD:DC_ID)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Delivery COD / Pre Paid  Addresses File'),AT(,,358,198),FONT('Tahoma',8, |
  ,FONT:regular),RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_CODAddresses'),SYSTEM
                       LIST,AT(8,32,342,122),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Address Name~@s35@80L(2)' & |
  '|M~Line 1~@s35@80L(2)|M~Line 2~@s35@80L(2)|M~Line 3~@s35@80L(2)|M~Line 4~@s35@80L(2)' & |
  '|M~VAT No.~@s20@30R(2)|M~DC ID~L@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the ' & |
  'Delivery_CODAddresses file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Address Name'),USE(?Tab:2)
                           PROMPT('Locator:'),AT(9,20),USE(?LOC:Locator:Prompt),TRN
                           STRING(@s35),AT(38,20),USE(LOC:Locator),TRN
                         END
                       END
                       BUTTON('View Deliveries'),AT(4,180,,14),USE(?BrowseDeliveries),LEFT,ICON('WACHILD.ICO'),DISABLE, |
  FLAT,MSG('View Child File'),TIP('View Child File')
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(178,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_CODAddresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Delivery_CODAddresses.Open                        ! File Delivery_CODAddresses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Delivery_CODAddresses,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon DCADD:AddressName for sort order 1
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,DCADD:Key_AddressName) ! Add the sort order for DCADD:Key_AddressName for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(?LOC:Locator,DCADD:AddressName,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: DCADD:Key_AddressName , DCADD:AddressName
  BRW1.AddField(DCADD:AddressName,BRW1.Q.DCADD:AddressName) ! Field DCADD:AddressName is a hot field or requires assignment from browse
  BRW1.AddField(DCADD:Line1,BRW1.Q.DCADD:Line1)            ! Field DCADD:Line1 is a hot field or requires assignment from browse
  BRW1.AddField(DCADD:Line2,BRW1.Q.DCADD:Line2)            ! Field DCADD:Line2 is a hot field or requires assignment from browse
  BRW1.AddField(DCADD:Line3,BRW1.Q.DCADD:Line3)            ! Field DCADD:Line3 is a hot field or requires assignment from browse
  BRW1.AddField(DCADD:Line4,BRW1.Q.DCADD:Line4)            ! Field DCADD:Line4 is a hot field or requires assignment from browse
  BRW1.AddField(DCADD:VATNo,BRW1.Q.DCADD:VATNo)            ! Field DCADD:VATNo is a hot field or requires assignment from browse
  BRW1.AddField(DCADD:DC_ID,BRW1.Q.DCADD:DC_ID)            ! Field DCADD:DC_ID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_CODAddresses',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Delivery_CODAddresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_CODAddresses',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Delivery_CODAddresses
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_ServiceRequirements PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:BID)
                       PROJECT(DEL:CID)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:Rate)
                       PROJECT(DEL:DocumentCharge)
                       PROJECT(DEL:FuelSurcharge)
                       PROJECT(DEL:Charge)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:SID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:BID                LIKE(DEL:BID)                  !List box control field - type derived from field
DEL:CID                LIKE(DEL:CID)                  !List box control field - type derived from field
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
DEL:Rate               LIKE(DEL:Rate)                 !List box control field - type derived from field
DEL:DocumentCharge     LIKE(DEL:DocumentCharge)       !List box control field - type derived from field
DEL:FuelSurcharge      LIKE(DEL:FuelSurcharge)        !List box control field - type derived from field
DEL:Charge             LIKE(DEL:Charge)               !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Primary key field - type derived from field
DEL:SID                LIKE(DEL:SID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::SERI:Record LIKE(SERI:RECORD),THREAD
QuickWindow          WINDOW('Form Service Requirements'),AT(,,244,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateServiceRequirements'),SYSTEM
                       SHEET,AT(4,4,236,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Service Requirement:'),AT(9,22),USE(?SERI:ServiceRequirement:Prompt),TRN
                           ENTRY(@s35),AT(93,22,144,10),USE(SERI:ServiceRequirement),MSG('Service Requirement'),REQ,TIP('Service Requirement')
                           CHECK(' Broking'),AT(93,50),USE(SERI:Broking)
                         END
                         TAB('&2) Deliveries'),USE(?Tab:2),HIDE
                           LIST,AT(9,20,228,91),USE(?Browse:2),HVSCROLL,FORMAT('30R(2)|M~DI No.~C(0)@n_10@30R(2)|M' & |
  '~BID~C(0)@n_10@30R(2)|M~CID~C(0)@n_10@80L(2)|M~Client Reference~@s30@52R(1)|M~Rate~C' & |
  '(0)@n-11.2@64R(1)|M~Document Charge~C(0)@n-11.2@60R(1)|M~Fuel Surcharge~C(0)@n-11.2@' & |
  '60R(1)|M~Charge~C(0)@n-13.2@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the Deliveries file')
                         END
                       END
                       BUTTON('&OK'),AT(136,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(190,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Service Req. Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Service Req. Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ServiceRequirements')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SERI:ServiceRequirement:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:ServiceRequirements)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(SERI:Record,History::SERI:Record)
  SELF.AddHistoryField(?SERI:ServiceRequirement,2)
  SELF.AddHistoryField(?SERI:Broking,3)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Access:ServiceRequirements.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ServiceRequirements
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?SERI:ServiceRequirement{PROP:ReadOnly} = True
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon DEL:SID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,DEL:FKey_SID)    ! Add the sort order for DEL:FKey_SID for sort order 1
  BRW2.AddRange(DEL:SID,Relate:Deliveries,Relate:ServiceRequirements) ! Add file relationship range limit for sort order 1
  BRW2.AddField(DEL:DINo,BRW2.Q.DEL:DINo)                  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW2.AddField(DEL:BID,BRW2.Q.DEL:BID)                    ! Field DEL:BID is a hot field or requires assignment from browse
  BRW2.AddField(DEL:CID,BRW2.Q.DEL:CID)                    ! Field DEL:CID is a hot field or requires assignment from browse
  BRW2.AddField(DEL:ClientReference,BRW2.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW2.AddField(DEL:Rate,BRW2.Q.DEL:Rate)                  ! Field DEL:Rate is a hot field or requires assignment from browse
  BRW2.AddField(DEL:DocumentCharge,BRW2.Q.DEL:DocumentCharge) ! Field DEL:DocumentCharge is a hot field or requires assignment from browse
  BRW2.AddField(DEL:FuelSurcharge,BRW2.Q.DEL:FuelSurcharge) ! Field DEL:FuelSurcharge is a hot field or requires assignment from browse
  BRW2.AddField(DEL:Charge,BRW2.Q.DEL:Charge)              ! Field DEL:Charge is a hot field or requires assignment from browse
  BRW2.AddField(DEL:DID,BRW2.Q.DEL:DID)                    ! Field DEL:DID is a hot field or requires assignment from browse
  BRW2.AddField(DEL:SID,BRW2.Q.DEL:SID)                    ! Field DEL:SID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_ServiceRequirements',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_ServiceRequirements',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_ServiceRequirements PROCEDURE (p:DINo, p:Old_STID)

CurrentTab           STRING(80)                            !
LOC:Filter           BYTE                                  !
LOC:Broking          BYTE                                  !
BRW1::View:Browse    VIEW(ServiceRequirements)
                       PROJECT(SERI:ServiceRequirement)
                       PROJECT(SERI:Broking)
                       PROJECT(SERI:SID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
SERI:ServiceRequirement LIKE(SERI:ServiceRequirement) !List box control field - type derived from field
SERI:Broking           LIKE(SERI:Broking)             !List box control field - type derived from field
SERI:Broking_Icon      LONG                           !Entry's icon ID
SERI:SID               LIKE(SERI:SID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Service Requirements File'),AT(,,277,206),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('BrowseServiceRequirements')
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('150L(2)|M~Service Requirement~@s3' & |
  '5@12L(2)|MI~Broking~@p p@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the ServiceRequir' & |
  'ements file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       PROMPT('Please select a service type for this delivery.  The previous service type does' & |
  ' not match with this Manifests Transporter. Close to cancel change.'),AT(4,178,212,26), |
  USE(?Prompt1),FONT(,,,FONT:bold,CHARSET:ANSI),HIDE
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Service Requirment'),USE(?Tab:2)
                           GROUP,AT(114,158,155,14),USE(?Group_UpdateButtons)
                             BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                             BUTTON('&Change'),AT(166,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                             BUTTON('&Delete'),AT(221,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                           END
                         END
                       END
                       BUTTON('&Close'),AT(218,178,56,27),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(224,2,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_ServiceRequirements')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Filter',LOC:Filter)                            ! Added by: BrowseBox(ABC)
  BIND('LOC:Broking',LOC:Broking)                          ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:ServiceRequirements.Open                          ! File ServiceRequirements used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ServiceRequirements,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon SERI:ServiceRequirement for sort order 1
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,SERI:Key_ServiceRequirement) ! Add the sort order for SERI:Key_ServiceRequirement for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,SERI:ServiceRequirement,1,BRW1) ! Initialize the browse locator using  using key: SERI:Key_ServiceRequirement , SERI:ServiceRequirement
  BRW1.SetFilter('(LOC:Filter = 0 OR LOC:Broking = SERI:Broking)') ! Apply filter expression to browse
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(SERI:ServiceRequirement,BRW1.Q.SERI:ServiceRequirement) ! Field SERI:ServiceRequirement is a hot field or requires assignment from browse
  BRW1.AddField(SERI:Broking,BRW1.Q.SERI:Broking)          ! Field SERI:Broking is a hot field or requires assignment from browse
  BRW1.AddField(SERI:SID,BRW1.Q.SERI:SID)                  ! Field SERI:SID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_ServiceRequirements',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
      ! (p:DINo, p:Old_STID)
  
      IF p:DINo ~= 0
         UNHIDE(?Prompt1)
         HIDE(?Group_UpdateButtons)
  
         QuickWindow{PROP:Text}   = 'Select New Service Requirement for DI No. ' & p:DINo
  
         ! We are requesting the change of a Delivery Service Requirement - from a Broking to a Non-Broking or visa versa
         SERI:SID             = p:Old_STID
         IF Access:ServiceRequirements.TryFetch(SERI:PKey_SID) = LEVEL:Benign
            LOC:Filter        = 1
            IF SERI:Broking = TRUE
               LOC:Broking    = 0
            ELSE
               LOC:Broking    = 1
      .  .  .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ServiceRequirements.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_ServiceRequirements',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_ServiceRequirements
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Close
          IF p:DINo ~= 0
             CASE MESSAGE('Are you sure you want to cancel this and not update the Service Requirements?', 'Cancel', ICON:Question,|
                      BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:No
                CYCLE
          .  .
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (SERI:Broking = 1)
    SELF.Q.SERI:Broking_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.SERI:Broking_Icon = 1                           ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_UpdateButtons, Resize:FixRight+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_UpdateButtons

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_PackagingTypes PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(DeliveryItems)
                       PROJECT(DELI:ItemNo)
                       PROJECT(DELI:Type)
                       PROJECT(DELI:ContainerNo)
                       PROJECT(DELI:ContainerReturnAID)
                       PROJECT(DELI:ContainerVessel)
                       PROJECT(DELI:ETA)
                       PROJECT(DELI:ByContainer)
                       PROJECT(DELI:Length)
                       PROJECT(DELI:Breadth)
                       PROJECT(DELI:DIID)
                       PROJECT(DELI:PTID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Type              LIKE(DELI:Type)                !List box control field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !List box control field - type derived from field
DELI:ContainerReturnAID LIKE(DELI:ContainerReturnAID) !List box control field - type derived from field
DELI:ContainerVessel   LIKE(DELI:ContainerVessel)     !List box control field - type derived from field
DELI:ETA               LIKE(DELI:ETA)                 !List box control field - type derived from field
DELI:ByContainer       LIKE(DELI:ByContainer)         !List box control field - type derived from field
DELI:Length            LIKE(DELI:Length)              !List box control field - type derived from field
DELI:Breadth           LIKE(DELI:Breadth)             !List box control field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Primary key field - type derived from field
DELI:PTID              LIKE(DELI:PTID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::PACK:Record LIKE(PACK:RECORD),THREAD
QuickWindow          WINDOW('Form Packaging Types'),AT(,,213,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdatePackagingTypes'),SYSTEM
                       SHEET,AT(4,4,205,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Packaging:'),AT(9,22),USE(?PACK:Packaging:Prompt),TRN
                           ENTRY(@s35),AT(62,22,144,10),USE(PACK:Packaging),REQ
                           CHECK(' Archived'),AT(61,54),USE(PACK:Archived),MSG('Mark Packaging Type as Archived'),TIP('Mark Packa' & |
  'ging Type as Archived')
                           ENTRY(@d17),AT(62,76,60,10),USE(PACK:Archived_Date),RIGHT(1),READONLY,SKIP
                           PROMPT('Archived Date:'),AT(9,76),USE(?PACK:Archived_Date:Prompt)
                           PROMPT('Archived Time:'),AT(9,98),USE(?PACK:Archived_Time:Prompt)
                           ENTRY(@t7),AT(62,97,60,10),USE(PACK:Archived_Time),RIGHT(1),READONLY,SKIP
                         END
                         TAB('&2) Delivery Items'),USE(?Tab:2)
                           LIST,AT(9,20,197,93),USE(?Browse:2),HVSCROLL,FORMAT('32R(2)|M~Item No.~C(0)@n6@20R(2)|M' & |
  '~Type~C(0)@n3@80L(2)|M~Container No~@s35@80D(12)|M~Container Return Address~C(0)@s20' & |
  '@80L(2)|M~Container Vessel~@s35@80R(2)|M~ETA~C(0)@d6@52R(2)|M~By Container~C(0)@n3@2' & |
  '8R(2)|M~Length~C(0)@n6@32R(2)|M~Breadth~C(0)@n6@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he DeliveryItems file')
                         END
                       END
                       BUTTON('&OK'),AT(108,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(160,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Packaging Types Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Packaging Types Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_PackagingTypes')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PACK:Packaging:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:PackagingTypes)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(PACK:Record,History::PACK:Record)
  SELF.AddHistoryField(?PACK:Packaging,2)
  SELF.AddHistoryField(?PACK:Archived,3)
  SELF.AddHistoryField(?PACK:Archived_Date,6)
  SELF.AddHistoryField(?PACK:Archived_Time,7)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:DeliveryItems.SetOpenRelated()
  Relate:DeliveryItems.Open                                ! File DeliveryItems used by this procedure, so make sure it's RelationManager is open
  Access:PackagingTypes.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PackagingTypes
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:DeliveryItems,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?PACK:Packaging{PROP:ReadOnly} = True
    ?PACK:Archived_Date{PROP:ReadOnly} = True
    ?PACK:Archived_Time{PROP:ReadOnly} = True
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon DELI:PTID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,DELI:FKey_PTID)  ! Add the sort order for DELI:FKey_PTID for sort order 1
  BRW2.AddRange(DELI:PTID,Relate:DeliveryItems,Relate:PackagingTypes) ! Add file relationship range limit for sort order 1
  BRW2.AddField(DELI:ItemNo,BRW2.Q.DELI:ItemNo)            ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW2.AddField(DELI:Type,BRW2.Q.DELI:Type)                ! Field DELI:Type is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ContainerNo,BRW2.Q.DELI:ContainerNo)  ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ContainerReturnAID,BRW2.Q.DELI:ContainerReturnAID) ! Field DELI:ContainerReturnAID is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ContainerVessel,BRW2.Q.DELI:ContainerVessel) ! Field DELI:ContainerVessel is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ETA,BRW2.Q.DELI:ETA)                  ! Field DELI:ETA is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ByContainer,BRW2.Q.DELI:ByContainer)  ! Field DELI:ByContainer is a hot field or requires assignment from browse
  BRW2.AddField(DELI:Length,BRW2.Q.DELI:Length)            ! Field DELI:Length is a hot field or requires assignment from browse
  BRW2.AddField(DELI:Breadth,BRW2.Q.DELI:Breadth)          ! Field DELI:Breadth is a hot field or requires assignment from browse
  BRW2.AddField(DELI:DIID,BRW2.Q.DELI:DIID)                ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW2.AddField(DELI:PTID,BRW2.Q.DELI:PTID)                ! Field DELI:PTID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_PackagingTypes',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryItems.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_PackagingTypes',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

