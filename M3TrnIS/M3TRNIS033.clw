

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS033.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_ContainerTypes PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(ContainerTypes)
                       PROJECT(CTYP:ContainerType)
                       PROJECT(CTYP:Size)
                       PROJECT(CTYP:OpenTop)
                       PROJECT(CTYP:OverHeight)
                       PROJECT(CTYP:FlatRack)
                       PROJECT(CTYP:Reefer)
                       PROJECT(CTYP:CTID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
CTYP:ContainerType     LIKE(CTYP:ContainerType)       !List box control field - type derived from field
CTYP:Size              LIKE(CTYP:Size)                !List box control field - type derived from field
CTYP:OpenTop           LIKE(CTYP:OpenTop)             !List box control field - type derived from field
CTYP:OpenTop_Icon      LONG                           !Entry's icon ID
CTYP:OverHeight        LIKE(CTYP:OverHeight)          !List box control field - type derived from field
CTYP:OverHeight_Icon   LONG                           !Entry's icon ID
CTYP:FlatRack          LIKE(CTYP:FlatRack)            !List box control field - type derived from field
CTYP:FlatRack_Icon     LONG                           !Entry's icon ID
CTYP:Reefer            LIKE(CTYP:Reefer)              !List box control field - type derived from field
CTYP:Reefer_Icon       LONG                           !Entry's icon ID
CTYP:CTID              LIKE(CTYP:CTID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Container Types File'),AT(,,293,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('BrowseContainerTypes'),SYSTEM
                       LIST,AT(8,30,276,124),USE(?Browse:1),HVSCROLL,FORMAT('92L(2)|M~Type~@s35@22R(2)|M~Size~' & |
  'C(0)@n3@38R(2)|MI~Open Top~C(0)@p p@44R(2)|MI~Over Height~C(0)@p p@36R(2)|MI~Flat Ra' & |
  'ck~C(0)@p p@28R(2)|MI~Reefer~C(0)@p p@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the ' & |
  'ContainerTypes file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(76,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(130,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(182,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(236,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,285,172),USE(?CurrentTab)
                         TAB('&1) By Container Type'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(240,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_ContainerTypes')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:ContainerTypes.Open                               ! File ContainerTypes used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ContainerTypes,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon CTYP:ContainerType for sort order 1
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,CTYP:Key_Type)   ! Add the sort order for CTYP:Key_Type for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,CTYP:ContainerType,1,BRW1)     ! Initialize the browse locator using  using key: CTYP:Key_Type , CTYP:ContainerType
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(CTYP:ContainerType,BRW1.Q.CTYP:ContainerType) ! Field CTYP:ContainerType is a hot field or requires assignment from browse
  BRW1.AddField(CTYP:Size,BRW1.Q.CTYP:Size)                ! Field CTYP:Size is a hot field or requires assignment from browse
  BRW1.AddField(CTYP:OpenTop,BRW1.Q.CTYP:OpenTop)          ! Field CTYP:OpenTop is a hot field or requires assignment from browse
  BRW1.AddField(CTYP:OverHeight,BRW1.Q.CTYP:OverHeight)    ! Field CTYP:OverHeight is a hot field or requires assignment from browse
  BRW1.AddField(CTYP:FlatRack,BRW1.Q.CTYP:FlatRack)        ! Field CTYP:FlatRack is a hot field or requires assignment from browse
  BRW1.AddField(CTYP:Reefer,BRW1.Q.CTYP:Reefer)            ! Field CTYP:Reefer is a hot field or requires assignment from browse
  BRW1.AddField(CTYP:CTID,BRW1.Q.CTYP:CTID)                ! Field CTYP:CTID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_ContainerTypes',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ContainerTypes.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_ContainerTypes',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_ContainerTypes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (CTYP:OpenTop = 1)
    SELF.Q.CTYP:OpenTop_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.CTYP:OpenTop_Icon = 1                           ! Set icon from icon list
  END
  IF (CTYP:OverHeight = 1)
    SELF.Q.CTYP:OverHeight_Icon = 2                        ! Set icon from icon list
  ELSE
    SELF.Q.CTYP:OverHeight_Icon = 1                        ! Set icon from icon list
  END
  IF (CTYP:FlatRack = 1)
    SELF.Q.CTYP:FlatRack_Icon = 2                          ! Set icon from icon list
  ELSE
    SELF.Q.CTYP:FlatRack_Icon = 1                          ! Set icon from icon list
  END
  IF (CTYP:Reefer = 1)
    SELF.Q.CTYP:Reefer_Icon = 2                            ! Set icon from icon list
  ELSE
    SELF.Q.CTYP:Reefer_Icon = 1                            ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

