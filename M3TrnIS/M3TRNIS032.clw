

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS032.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_ContainerTypes PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(__RatesContainer)
                       PROJECT(CORA:CID)
                       PROJECT(CORA:ToMass)
                       PROJECT(CORA:RatePerKg)
                       PROJECT(CORA:CRID)
                       PROJECT(CORA:CTID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
CORA:CID               LIKE(CORA:CID)                 !List box control field - type derived from field
CORA:ToMass            LIKE(CORA:ToMass)              !List box control field - type derived from field
CORA:RatePerKg         LIKE(CORA:RatePerKg)           !List box control field - type derived from field
CORA:CRID              LIKE(CORA:CRID)                !Primary key field - type derived from field
CORA:CTID              LIKE(CORA:CTID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(DeliveryItems)
                       PROJECT(DELI:ItemNo)
                       PROJECT(DELI:Type)
                       PROJECT(DELI:ContainerNo)
                       PROJECT(DELI:ContainerReturnAID)
                       PROJECT(DELI:ContainerVessel)
                       PROJECT(DELI:ETA)
                       PROJECT(DELI:ByContainer)
                       PROJECT(DELI:Length)
                       PROJECT(DELI:Breadth)
                       PROJECT(DELI:DIID)
                       PROJECT(DELI:CTID)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Type              LIKE(DELI:Type)                !List box control field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !List box control field - type derived from field
DELI:ContainerReturnAID LIKE(DELI:ContainerReturnAID) !List box control field - type derived from field
DELI:ContainerVessel   LIKE(DELI:ContainerVessel)     !List box control field - type derived from field
DELI:ETA               LIKE(DELI:ETA)                 !List box control field - type derived from field
DELI:ByContainer       LIKE(DELI:ByContainer)         !List box control field - type derived from field
DELI:Length            LIKE(DELI:Length)              !List box control field - type derived from field
DELI:Breadth           LIKE(DELI:Breadth)             !List box control field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Primary key field - type derived from field
DELI:CTID              LIKE(DELI:CTID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::CTYP:Record LIKE(CTYP:RECORD),THREAD
QuickWindow          WINDOW('Form Container Types'),AT(,,213,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateContainerTypes'),SYSTEM
                       SHEET,AT(4,4,205,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Type:'),AT(9,20),USE(?CTYP:Type:Prompt),TRN
                           ENTRY(@s35),AT(62,20,144,10),USE(CTYP:ContainerType),MSG('Type'),REQ,TIP('Type')
                           PROMPT('Size:'),AT(9,34),USE(?CTYP:Size:Prompt),TRN
                           LIST,AT(62,34,55,10),USE(CTYP:Size),DROP(5),FROM('6|12')
                           CHECK(' Over &Height'),AT(62,56,70,8),USE(CTYP:OverHeight),TRN
                           CHECK(' Open &Top'),AT(62,68,70,8),USE(CTYP:OpenTop),MSG('Open Top'),TIP('Open Top'),TRN
                           CHECK(' &Reefer'),AT(62,80,70,8),USE(CTYP:Reefer),TRN
                           CHECK(' &Flat Rack'),AT(62,92,70,10),USE(CTYP:FlatRack),MSG('Flat Rack'),TIP('Flat Rack'), |
  TRN
                         END
                         TAB('&2) Container Rates'),USE(?Tab:2)
                           LIST,AT(9,20,197,92),USE(?Browse:2),HVSCROLL,FORMAT('40R(2)|M~CID~C(0)@n_10@56R(2)|M~To' & |
  ' Mass~C(0)@n-12.0@52D(10)|M~Rate Per Kg~C(0)@n-11.2@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he DeliveryItems file')
                         END
                         TAB('&3) Delivery Items'),USE(?Tab:3)
                           LIST,AT(9,20,197,92),USE(?Browse:4),HVSCROLL,FORMAT('32R(2)|M~Item No~C(0)@n6@20R(2)|M~' & |
  'Type~C(0)@n3@80L(2)|M~Container No~L(2)@s35@80D(12)|M~Container Return Address~C(0)@' & |
  's20@80L(2)|M~Container Vessel~L(2)@s35@80R(2)|M~ETA~C(0)@d6@52R(2)|M~By Container~C(' & |
  '0)@n3@28R(2)|M~Length~C(0)@n6@32R(2)|M~Breadth~C(0)@n6@'),FROM(Queue:Browse:4),IMM,MSG('Browsing t' & |
  'he DeliveryItems file')
                         END
                       END
                       BUTTON('&OK'),AT(108,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(160,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
                     END

BRW4::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Container Types Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Container Types Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ContainerTypes')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CTYP:Type:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CTYP:Record,History::CTYP:Record)
  SELF.AddHistoryField(?CTYP:ContainerType,2)
  SELF.AddHistoryField(?CTYP:Size,3)
  SELF.AddHistoryField(?CTYP:OverHeight,5)
  SELF.AddHistoryField(?CTYP:OpenTop,4)
  SELF.AddHistoryField(?CTYP:Reefer,7)
  SELF.AddHistoryField(?CTYP:FlatRack,6)
  SELF.AddUpdateFile(Access:ContainerTypes)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ContainerTypes.Open                               ! File ContainerTypes used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ContainerTypes
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:__RatesContainer,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:DeliveryItems,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?CTYP:ContainerType{PROP:ReadOnly} = True
    DISABLE(?CTYP:Size)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon CORA:CTID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,CORA:FKey_CTID)  ! Add the sort order for CORA:FKey_CTID for sort order 1
  BRW2.AddRange(CORA:CTID,Relate:__RatesContainer,Relate:ContainerTypes) ! Add file relationship range limit for sort order 1
  BRW2.AddField(CORA:CID,BRW2.Q.CORA:CID)                  ! Field CORA:CID is a hot field or requires assignment from browse
  BRW2.AddField(CORA:ToMass,BRW2.Q.CORA:ToMass)            ! Field CORA:ToMass is a hot field or requires assignment from browse
  BRW2.AddField(CORA:RatePerKg,BRW2.Q.CORA:RatePerKg)      ! Field CORA:RatePerKg is a hot field or requires assignment from browse
  BRW2.AddField(CORA:CRID,BRW2.Q.CORA:CRID)                ! Field CORA:CRID is a hot field or requires assignment from browse
  BRW2.AddField(CORA:CTID,BRW2.Q.CORA:CTID)                ! Field CORA:CTID is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon DELI:CTID for sort order 1
  BRW4.AddSortOrder(BRW4::Sort0:StepClass,DELI:FKey_CTID)  ! Add the sort order for DELI:FKey_CTID for sort order 1
  BRW4.AddRange(DELI:CTID,Relate:DeliveryItems,Relate:ContainerTypes) ! Add file relationship range limit for sort order 1
  BRW4.AddField(DELI:ItemNo,BRW4.Q.DELI:ItemNo)            ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW4.AddField(DELI:Type,BRW4.Q.DELI:Type)                ! Field DELI:Type is a hot field or requires assignment from browse
  BRW4.AddField(DELI:ContainerNo,BRW4.Q.DELI:ContainerNo)  ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW4.AddField(DELI:ContainerReturnAID,BRW4.Q.DELI:ContainerReturnAID) ! Field DELI:ContainerReturnAID is a hot field or requires assignment from browse
  BRW4.AddField(DELI:ContainerVessel,BRW4.Q.DELI:ContainerVessel) ! Field DELI:ContainerVessel is a hot field or requires assignment from browse
  BRW4.AddField(DELI:ETA,BRW4.Q.DELI:ETA)                  ! Field DELI:ETA is a hot field or requires assignment from browse
  BRW4.AddField(DELI:ByContainer,BRW4.Q.DELI:ByContainer)  ! Field DELI:ByContainer is a hot field or requires assignment from browse
  BRW4.AddField(DELI:Length,BRW4.Q.DELI:Length)            ! Field DELI:Length is a hot field or requires assignment from browse
  BRW4.AddField(DELI:Breadth,BRW4.Q.DELI:Breadth)          ! Field DELI:Breadth is a hot field or requires assignment from browse
  BRW4.AddField(DELI:DIID,BRW4.Q.DELI:DIID)                ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW4.AddField(DELI:CTID,BRW4.Q.DELI:CTID)                ! Field DELI:CTID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_ContainerTypes',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ContainerTypes.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_ContainerTypes',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

