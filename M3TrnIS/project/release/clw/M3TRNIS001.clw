

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Accountants PROCEDURE 

CurrentTab           STRING(80)                            ! 
BRW1::View:Browse    VIEW(Accountants)
                       PROJECT(ACCO:AccountantName)
                       PROJECT(ACCO:ACID)
                       PROJECT(ACCO:AID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ACCO:AccountantName    LIKE(ACCO:AccountantName)      !List box control field - type derived from field
ACCO:ACID              LIKE(ACCO:ACID)                !Primary key field - type derived from field
ACCO:AID               LIKE(ACCO:AID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Accountants File'),AT(,,257,188),FONT('Tahoma',8),RESIZE,GRAY,IMM,MDI,HLP('Browse_Accountants'), |
  SYSTEM
                       LIST,AT(8,20,241,124),USE(?Browse:1),HVSCROLL,FORMAT('120L(2)|M~Accountant Name~@s35@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing Records')
                       BUTTON('&Select'),AT(4,170,45,14),USE(?Select:2),LEFT,ICON('WAselect.ICO'),FLAT
                       BUTTON('&Insert'),AT(80,148,,14),USE(?Insert:3),LEFT,ICON('WAinsert.ICO'),FLAT
                       BUTTON('&Change'),AT(136,148,,14),USE(?Change:3),LEFT,ICON('WAChange.ICO'),DEFAULT,FLAT
                       BUTTON('&Delete'),AT(195,148,,14),USE(?Delete:3),LEFT,ICON('WAdelete.ICO'),FLAT
                       SHEET,AT(4,4,249,162),USE(?CurrentTab)
                         TAB('By Accountant Name'),USE(?Tab:2)
                         END
                         TAB('By Address'),USE(?Tab:3)
                           BUTTON('Addresses'),AT(9,148,,14),USE(?SelectAddresses),LEFT,ICON('WAPARENT.ICO'),FLAT
                         END
                       END
                       BUTTON('Close'),AT(201,170,,14),USE(?Close),LEFT,ICON('waclose.ico'),FLAT
                       BUTTON('Help'),AT(104,170,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Accountants')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Accountants.Open                                  ! File Accountants used by this procedure, so make sure it's RelationManager is open
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Accountants,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon ACCO:AID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,ACCO:FKey_AID)   ! Add the sort order for ACCO:FKey_AID for sort order 1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon ACCO:AccountantName for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ACCO:Key_AccountantName) ! Add the sort order for ACCO:Key_AccountantName for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,ACCO:AccountantName,1,BRW1)    ! Initialize the browse locator using  using key: ACCO:Key_AccountantName , ACCO:AccountantName
  BRW1.AddField(ACCO:AccountantName,BRW1.Q.ACCO:AccountantName) ! Field ACCO:AccountantName is a hot field or requires assignment from browse
  BRW1.AddField(ACCO:ACID,BRW1.Q.ACCO:ACID)                ! Field ACCO:ACID is a hot field or requires assignment from browse
  BRW1.AddField(ACCO:AID,BRW1.Q.ACCO:AID)                  ! Field ACCO:AID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Accountants',QuickWindow)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                                    ! Will call: Update_Accountants
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Accountants.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Accountants',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Accountants
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectAddresses
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Address()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Cities PROCEDURE 

CurrentTab           STRING(80)                            ! 
BRW1::View:Browse    VIEW(Add_Cities)
                       PROJECT(CITI:City)
                       PROJECT(CITI:CIID)
                       PROJECT(CITI:PRID)
                       JOIN(PROV:PKey_PRID,CITI:PRID)
                         PROJECT(PROV:Province)
                         PROJECT(PROV:PRID)
                         PROJECT(PROV:COID)
                         JOIN(COUN:PKey_COID,PROV:COID)
                           PROJECT(COUN:Country)
                           PROJECT(COUN:COID)
                         END
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
CITI:City              LIKE(CITI:City)                !List box control field - type derived from field
PROV:Province          LIKE(PROV:Province)            !List box control field - type derived from field
COUN:Country           LIKE(COUN:Country)             !List box control field - type derived from field
CITI:CIID              LIKE(CITI:CIID)                !Primary key field - type derived from field
CITI:PRID              LIKE(CITI:PRID)                !Browse key field - type derived from field
PROV:PRID              LIKE(PROV:PRID)                !Related join file key field - type derived from field
COUN:COID              LIKE(COUN:COID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Cities File'),AT(,,330,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Browse_Cities'),SYSTEM
                       LIST,AT(8,30,314,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~City~@s35@80L(2)|M~Provi' & |
  'nce~@s35@80L(2)|M~Country~@s50@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Add_Cities file')
                       BUTTON('&Select'),AT(61,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(114,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(167,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(220,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(273,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,322,172),USE(?CurrentTab)
                         TAB('&1) By City'),USE(?Tab:2)
                         END
                         TAB('&2) By Province && City'),USE(?Tab:3)
                           BUTTON('Add_Provi...'),AT(9,158,16,14),USE(?SelectAdd_Provinces),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(277,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepStringClass                      ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Cities')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Add_Cities.Open                                   ! File Add_Cities used by this procedure, so make sure it's RelationManager is open
  Access:Add_Provinces.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Add_Cities,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon CITI:City for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,CITI:FKey_PRID_City) ! Add the sort order for CITI:FKey_PRID_City for sort order 1
  BRW1.AddRange(CITI:PRID,Relate:Add_Cities,Relate:Add_Provinces) ! Add file relationship range limit for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,CITI:City,1,BRW1)              ! Initialize the browse locator using  using key: CITI:FKey_PRID_City , CITI:City
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon CITI:City for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,CITI:Key_City)   ! Add the sort order for CITI:Key_City for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,CITI:City,1,BRW1)              ! Initialize the browse locator using  using key: CITI:Key_City , CITI:City
  BRW1.AddField(CITI:City,BRW1.Q.CITI:City)                ! Field CITI:City is a hot field or requires assignment from browse
  BRW1.AddField(PROV:Province,BRW1.Q.PROV:Province)        ! Field PROV:Province is a hot field or requires assignment from browse
  BRW1.AddField(COUN:Country,BRW1.Q.COUN:Country)          ! Field COUN:Country is a hot field or requires assignment from browse
  BRW1.AddField(CITI:CIID,BRW1.Q.CITI:CIID)                ! Field CITI:CIID is a hot field or requires assignment from browse
  BRW1.AddField(CITI:PRID,BRW1.Q.CITI:PRID)                ! Field CITI:PRID is a hot field or requires assignment from browse
  BRW1.AddField(PROV:PRID,BRW1.Q.PROV:PRID)                ! Field PROV:PRID is a hot field or requires assignment from browse
  BRW1.AddField(COUN:COID,BRW1.Q.COUN:COID)                ! Field COUN:COID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Cities',QuickWindow)                ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                                    ! Will call: Update_Cities
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Cities.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Cities',QuickWindow)             ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Cities
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectAdd_Provinces
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Provinces()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Provinces PROCEDURE 

CurrentTab           STRING(80)                            ! 
BRW1::View:Browse    VIEW(Add_Provinces)
                       PROJECT(PROV:Province)
                       PROJECT(PROV:PRID)
                       PROJECT(PROV:COID)
                       JOIN(COUN:PKey_COID,PROV:COID)
                         PROJECT(COUN:Country)
                         PROJECT(COUN:COID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
PROV:Province          LIKE(PROV:Province)            !List box control field - type derived from field
COUN:Country           LIKE(COUN:Country)             !List box control field - type derived from field
PROV:PRID              LIKE(PROV:PRID)                !Primary key field - type derived from field
PROV:COID              LIKE(PROV:COID)                !Browse key field - type derived from field
COUN:COID              LIKE(COUN:COID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Provinces File'),AT(,,330,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Browse_Provinces'),SYSTEM
                       LIST,AT(8,30,314,124),USE(?Browse:1),HVSCROLL,FORMAT('100L(2)|M~Province~@s35@100L(2)|M' & |
  '~Country~@s50@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Add_Provinces file')
                       BUTTON('&Select'),AT(61,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(114,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(167,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(220,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(273,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,322,172),USE(?CurrentTab)
                         TAB('&1) By Province'),USE(?Tab:2)
                         END
                         TAB('&2) By Country && Province'),USE(?Tab:3)
                           BUTTON('Add_Count...'),AT(9,158,16,14),USE(?SelectAdd_Countries),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(277,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepStringClass                      ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Provinces')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Add_Countries.SetOpenRelated()
  Relate:Add_Countries.Open                                ! File Add_Countries used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Add_Provinces,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon PROV:Province for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,PROV:FKey_COID_Province) ! Add the sort order for PROV:FKey_COID_Province for sort order 1
  BRW1.AddRange(PROV:COID,Relate:Add_Provinces,Relate:Add_Countries) ! Add file relationship range limit for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,PROV:Province,1,BRW1)          ! Initialize the browse locator using  using key: PROV:FKey_COID_Province , PROV:Province
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon PROV:Province for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,PROV:Key_Province) ! Add the sort order for PROV:Key_Province for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,PROV:Province,1,BRW1)          ! Initialize the browse locator using  using key: PROV:Key_Province , PROV:Province
  BRW1.AddField(PROV:Province,BRW1.Q.PROV:Province)        ! Field PROV:Province is a hot field or requires assignment from browse
  BRW1.AddField(COUN:Country,BRW1.Q.COUN:Country)          ! Field COUN:Country is a hot field or requires assignment from browse
  BRW1.AddField(PROV:PRID,BRW1.Q.PROV:PRID)                ! Field PROV:PRID is a hot field or requires assignment from browse
  BRW1.AddField(PROV:COID,BRW1.Q.PROV:COID)                ! Field PROV:COID is a hot field or requires assignment from browse
  BRW1.AddField(COUN:COID,BRW1.Q.COUN:COID)                ! Field COUN:COID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Provinces',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                                    ! Will call: Update_Provinces
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Countries.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Provinces',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Provinces
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectAdd_Countries
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Countries()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Provinces PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
Country              STRING(50)                            ! Country
BRW2::View:Browse    VIEW(Add_Cities)
                       PROJECT(CITI:City)
                       PROJECT(CITI:CIID)
                       PROJECT(CITI:PRID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
CITI:City              LIKE(CITI:City)                !List box control field - type derived from field
CITI:CIID              LIKE(CITI:CIID)                !Primary key field - type derived from field
CITI:PRID              LIKE(CITI:PRID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::PROV:Record LIKE(PROV:RECORD),THREAD
QuickWindow          WINDOW('Form Provinces'),AT(,,213,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('UpdateAdd_Provinces'),SYSTEM
                       SHEET,AT(4,4,205,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Province:'),AT(9,20),USE(?PROV:Province:Prompt),TRN
                           ENTRY(@s35),AT(61,20,144,10),USE(PROV:Province),MSG('Province'),REQ,TIP('Province')
                           PROMPT('Country:'),AT(9,46),USE(?Country:Prompt),TRN
                           BUTTON('...'),AT(45,46,12,10),USE(?CallLookup)
                           ENTRY(@s50),AT(61,46,144,10),USE(Country),MSG('Country'),REQ,TIP('Country')
                         END
                         TAB('&2) Cities'),USE(?Tab:2)
                           LIST,AT(9,20,197,74),USE(?Browse:2),HVSCROLL,FORMAT('80L(2)|M~City~L(2)@s35@'),FROM(Queue:Browse:2), |
  IMM,MSG('Browsing the Add_Cities file')
                           BUTTON('&Insert'),AT(51,98,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(103,98,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(157,98,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(108,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(160,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW2::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW2::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Provinces')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PROV:Province:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(PROV:Record,History::PROV:Record)
  SELF.AddHistoryField(?PROV:Province,3)
  SELF.AddUpdateFile(Access:Add_Provinces)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Add_Cities.Open                                   ! File Add_Cities used by this procedure, so make sure it's RelationManager is open
  Access:Add_Provinces.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Add_Countries.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Add_Provinces
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:Add_Cities,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?PROV:Province{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?Country{PROP:ReadOnly} = True
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon CITI:City for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,CITI:FKey_PRID_City) ! Add the sort order for CITI:FKey_PRID_City for sort order 1
  BRW2.AddRange(CITI:PRID,Relate:Add_Cities,Relate:Add_Provinces) ! Add file relationship range limit for sort order 1
  BRW2.AddLocator(BRW2::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW2::Sort0:Locator.Init(,CITI:City,1,BRW2)              ! Initialize the browse locator using  using key: CITI:FKey_PRID_City , CITI:City
  BRW2.AddField(CITI:City,BRW2.Q.CITI:City)                ! Field CITI:City is a hot field or requires assignment from browse
  BRW2.AddField(CITI:CIID,BRW2.Q.CITI:CIID)                ! Field CITI:CIID is a hot field or requires assignment from browse
  BRW2.AddField(CITI:PRID,BRW2.Q.CITI:PRID)                ! Field CITI:PRID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Provinces',QuickWindow)             ! Restore window settings from non-volatile store
      COUN:COID   = PROV:COID
      IF Access:Add_Countries.TryFetch(COUN:PKey_COID) = LEVEL:Benign
         Country  = COUN:Country
      .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 2                                    ! Will call: Update_Cities
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Cities.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Provinces',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Countries
      Update_Cities
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      COUN:Country = Country
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        Country = COUN:Country
        PROV:COID = COUN:COID
      END
      ThisWindow.Reset(1)
    OF ?Country
      IF Country OR ?Country{PROP:Req}
        COUN:Country = Country
        IF Access:Add_Countries.TryFetch(COUN:Key_Country)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Country = COUN:Country
            PROV:COID = COUN:COID
          ELSE
            CLEAR(PROV:COID)
            SELECT(?Country)
            CYCLE
          END
        ELSE
          PROV:COID = COUN:COID
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Countries PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
BRW2::View:Browse    VIEW(Add_Provinces)
                       PROJECT(PROV:Province)
                       PROJECT(PROV:PRID)
                       PROJECT(PROV:COID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
PROV:Province          LIKE(PROV:Province)            !List box control field - type derived from field
PROV:PRID              LIKE(PROV:PRID)                !Primary key field - type derived from field
PROV:COID              LIKE(PROV:COID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::COUN:Record LIKE(COUN:RECORD),THREAD
QuickWindow          WINDOW('Form Countries'),AT(,,273,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('UpdateAdd_Countries'),SYSTEM
                       SHEET,AT(4,4,265,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Country:'),AT(9,20),USE(?COUN:Country:Prompt),TRN
                           ENTRY(@s50),AT(61,20,204,10),USE(COUN:Country),MSG('Country'),REQ,TIP('Country')
                         END
                         TAB('&2) Provinces'),USE(?Tab:2)
                           LIST,AT(9,20,257,74),USE(?Browse:2),HVSCROLL,FORMAT('80L(2)|M~Province~L(2)@s35@'),FROM(Queue:Browse:2), |
  IMM,MSG('Browsing the Add_Provinces file')
                           BUTTON('&Insert'),AT(111,98,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(163,98,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(217,98,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(168,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(220,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW2::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW2::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Countries')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?COUN:Country:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(COUN:Record,History::COUN:Record)
  SELF.AddHistoryField(?COUN:Country,2)
  SELF.AddUpdateFile(Access:Add_Countries)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Add_Countries.SetOpenRelated()
  Relate:Add_Countries.Open                                ! File Add_Countries used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Add_Countries
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:Add_Provinces,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?COUN:Country{PROP:ReadOnly} = True
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon PROV:Province for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,PROV:FKey_COID_Province) ! Add the sort order for PROV:FKey_COID_Province for sort order 1
  BRW2.AddRange(PROV:COID,Relate:Add_Provinces,Relate:Add_Countries) ! Add file relationship range limit for sort order 1
  BRW2.AddLocator(BRW2::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW2::Sort0:Locator.Init(,PROV:Province,1,BRW2)          ! Initialize the browse locator using  using key: PROV:FKey_COID_Province , PROV:Province
  BRW2.AddField(PROV:Province,BRW2.Q.PROV:Province)        ! Field PROV:Province is a hot field or requires assignment from browse
  BRW2.AddField(PROV:PRID,BRW2.Q.PROV:PRID)                ! Field PROV:PRID is a hot field or requires assignment from browse
  BRW2.AddField(PROV:COID,BRW2.Q.PROV:COID)                ! Field PROV:COID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Countries',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1                                    ! Will call: Update_Provinces
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Countries.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Countries',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Provinces
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Countries PROCEDURE 

CurrentTab           STRING(80)                            ! 
BRW1::View:Browse    VIEW(Add_Countries)
                       PROJECT(COUN:Country)
                       PROJECT(COUN:COID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
COUN:Country           LIKE(COUN:Country)             !List box control field - type derived from field
COUN:COID              LIKE(COUN:COID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Countries File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Browse_Countries'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Country~L(2)@s50@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the Add_Countries file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Country'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Countries')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Add_Countries.SetOpenRelated()
  Relate:Add_Countries.Open                                ! File Add_Countries used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Add_Countries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon COUN:Country for sort order 1
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,COUN:Key_Country) ! Add the sort order for COUN:Key_Country for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,COUN:Country,1,BRW1)           ! Initialize the browse locator using  using key: COUN:Key_Country , COUN:Country
  BRW1.AddField(COUN:Country,BRW1.Q.COUN:Country)          ! Field COUN:Country is a hot field or requires assignment from browse
  BRW1.AddField(COUN:COID,BRW1.Q.COUN:COID)                ! Field COUN:COID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Countries',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                                    ! Will call: Update_Countries
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Countries.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Countries',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Countries
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Cities PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
Province             STRING(35)                            ! Province
BRW2::View:Browse    VIEW(Add_Suburbs)
                       PROJECT(SUBU:Suburb)
                       PROJECT(SUBU:PostalCode)
                       PROJECT(SUBU:SUID)
                       PROJECT(SUBU:CIID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
SUBU:Suburb            LIKE(SUBU:Suburb)              !List box control field - type derived from field
SUBU:PostalCode        LIKE(SUBU:PostalCode)          !List box control field - type derived from field
SUBU:SUID              LIKE(SUBU:SUID)                !Primary key field - type derived from field
SUBU:CIID              LIKE(SUBU:CIID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::CITI:Record LIKE(CITI:RECORD),THREAD
QuickWindow          WINDOW('Form Cities'),AT(,,213,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('UpdateAdd_Cities'),SYSTEM
                       SHEET,AT(4,4,205,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('City:'),AT(9,20),USE(?CITI:City:Prompt),TRN
                           ENTRY(@s35),AT(62,20,144,10),USE(CITI:City),MSG('City'),REQ,TIP('City')
                           PROMPT('Province:'),AT(9,46),USE(?Province:Prompt),TRN
                           BUTTON('...'),AT(45,46,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(62,46,144,10),USE(Province),MSG('Province'),REQ,TIP('Province')
                         END
                         TAB('&2) Suburbs'),USE(?Tab:2)
                           LIST,AT(9,20,197,74),USE(?Browse:2),HVSCROLL,FORMAT('130L(2)|M~Suburb~@s50@48L(2)|M~Pos' & |
  'tal Code~@s10@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the Add_Suburbs file')
                           BUTTON('&Insert'),AT(50,98,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(102,98,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(157,98,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(108,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(160,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Cities')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CITI:City:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CITI:Record,History::CITI:Record)
  SELF.AddHistoryField(?CITI:City,3)
  SELF.AddUpdateFile(Access:Add_Cities)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Add_Cities.SetOpenRelated()
  Relate:Add_Cities.Open                                   ! File Add_Cities used by this procedure, so make sure it's RelationManager is open
  Access:Add_Provinces.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Add_Cities
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:Add_Suburbs,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?CITI:City{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?Province{PROP:ReadOnly} = True
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon SUBU:CIID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,SUBU:FKey_CIID)  ! Add the sort order for SUBU:FKey_CIID for sort order 1
  BRW2.AddRange(SUBU:CIID,Relate:Add_Suburbs,Relate:Add_Cities) ! Add file relationship range limit for sort order 1
  BRW2.AddField(SUBU:Suburb,BRW2.Q.SUBU:Suburb)            ! Field SUBU:Suburb is a hot field or requires assignment from browse
  BRW2.AddField(SUBU:PostalCode,BRW2.Q.SUBU:PostalCode)    ! Field SUBU:PostalCode is a hot field or requires assignment from browse
  BRW2.AddField(SUBU:SUID,BRW2.Q.SUBU:SUID)                ! Field SUBU:SUID is a hot field or requires assignment from browse
  BRW2.AddField(SUBU:CIID,BRW2.Q.SUBU:CIID)                ! Field SUBU:CIID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Cities',QuickWindow)                ! Restore window settings from non-volatile store
      PROV:PRID   = CITI:PRID
      IF Access:Add_Provinces.TryFetch(PROV:PKey_PRID) = LEVEL:Benign
         Province = PROV:Province
      .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 2                                    ! Will call: Update_Suburbs
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Cities.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Cities',QuickWindow)             ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Provinces
      Update_Suburbs
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      PROV:Province = Province
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        Province = PROV:Province
        CITI:PRID = PROV:PRID
      END
      ThisWindow.Reset(1)
    OF ?Province
      IF Province OR ?Province{PROP:Req}
        PROV:Province = Province
        IF Access:Add_Provinces.TryFetch(PROV:Key_Province)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Province = PROV:Province
            CITI:PRID = PROV:PRID
          ELSE
            CLEAR(CITI:PRID)
            SELECT(?Province)
            CYCLE
          END
        ELSE
          CITI:PRID = PROV:PRID
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Suburbs PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
L_City_Details       GROUP,PRE(L_CD)                       ! 
Country              STRING(50)                            ! Country
Province             STRING(35)                            ! Province
City                 STRING(35)                            ! City
                     END                                   ! 
BRW2::View:Browse    VIEW(Addresses)
                       PROJECT(ADD:AddressName)
                       PROJECT(ADD:Line1)
                       PROJECT(ADD:Line2)
                       PROJECT(ADD:AID)
                       PROJECT(ADD:SUID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
ADD:Line1              LIKE(ADD:Line1)                !List box control field - type derived from field
ADD:Line2              LIKE(ADD:Line2)                !List box control field - type derived from field
ADD:AID                LIKE(ADD:AID)                  !Primary key field - type derived from field
ADD:SUID               LIKE(ADD:SUID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::SUBU:Record LIKE(SUBU:RECORD),THREAD
QuickWindow          WINDOW('Update the Suburbs File'),AT(,,273,138),FONT('Tahoma',8),RESIZE,GRAY,IMM,MDI,HLP('UpdateSuburbs'), |
  SYSTEM
                       SHEET,AT(4,4,265,112),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Suburb:'),AT(9,20),USE(?SUBU:Suburb:Prompt),TRN
                           ENTRY(@s50),AT(62,20,204,10),USE(SUBU:Suburb),MSG('Suburb'),REQ,TIP('Suburb')
                           PROMPT('Postal Code:'),AT(9,34),USE(?SUBU:PostalCode:Prompt),TRN
                           ENTRY(@s10),AT(62,34,50,10),USE(SUBU:PostalCode),REQ
                           PROMPT('City:'),AT(9,52),USE(?SUBU:City:Prompt),TRN
                           BUTTON('...'),AT(43,52,14,10),USE(?Button_Browse_Cities)
                           ENTRY(@s35),AT(62,52,204,10),USE(L_CD:City)
                           PROMPT('Country:'),AT(9,74),USE(?L_CD:Country:Prompt),TRN
                           ENTRY(@s50),AT(62,74,204,10),USE(L_CD:Country),COLOR(00E9E9E9h),MSG('Country'),READONLY,REQ, |
  SKIP,TIP('Country')
                           PROMPT('Province:'),AT(9,88),USE(?L_CD:Province:Prompt),TRN
                           ENTRY(@s35),AT(62,88,204,10),USE(L_CD:Province),COLOR(00E9E9E9h),MSG('Province'),READONLY, |
  REQ,SKIP,TIP('Province')
                         END
                         TAB('Addresses'),USE(?Tab:2)
                           LIST,AT(9,20,257,90),USE(?Browse:2),HVSCROLL,FORMAT('80L(2)|M~Name~@s35@80L(2)|M~Line 1' & |
  '~@s35@80L(2)|M~Line 2~@s35@'),FROM(Queue:Browse:2),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('&OK'),AT(176,120,45,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT
                       BUTTON('Cancel'),AT(224,120,45,14),USE(?Cancel),LEFT,ICON('WACancel.ICO'),FLAT
                       BUTTON('Help'),AT(4,120,45,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a Suburbs Record'
  OF ChangeRecord
    ActionMessage = 'Changing a Suburbs Record'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Suburbs')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SUBU:Suburb:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(SUBU:Record,History::SUBU:Record)
  SELF.AddHistoryField(?SUBU:Suburb,3)
  SELF.AddHistoryField(?SUBU:PostalCode,4)
  SELF.AddUpdateFile(Access:Add_Suburbs)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Add_Cities.SetOpenRelated()
  Relate:Add_Cities.Open                                   ! File Add_Cities used by this procedure, so make sure it's RelationManager is open
  Access:Add_Suburbs.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Add_Countries.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Add_Suburbs
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:Addresses,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon ADD:SUID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,ADD:FKey_SUID)   ! Add the sort order for ADD:FKey_SUID for sort order 1
  BRW2.AddRange(ADD:SUID,Relate:Addresses,Relate:Add_Suburbs) ! Add file relationship range limit for sort order 1
  BRW2.AddField(ADD:AddressName,BRW2.Q.ADD:AddressName)    ! Field ADD:AddressName is a hot field or requires assignment from browse
  BRW2.AddField(ADD:Line1,BRW2.Q.ADD:Line1)                ! Field ADD:Line1 is a hot field or requires assignment from browse
  BRW2.AddField(ADD:Line2,BRW2.Q.ADD:Line2)                ! Field ADD:Line2 is a hot field or requires assignment from browse
  BRW2.AddField(ADD:AID,BRW2.Q.ADD:AID)                    ! Field ADD:AID is a hot field or requires assignment from browse
  BRW2.AddField(ADD:SUID,BRW2.Q.ADD:SUID)                  ! Field ADD:SUID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Suburbs',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Cities.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Suburbs',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Cities
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Browse_Cities
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_Cities()
      ThisWindow.Reset
          SUBU:CIID       = CITI:CIID
          L_CD:City       = CITI:City
          DISPLAY
          L_City_Details  = Get_City_Details( CITI:CIID )
          DISPLAY
    OF ?L_CD:City
      IF L_CD:City OR ?L_CD:City{PROP:Req}
        CITI:City = L_CD:City
        IF Access:Add_Cities.TryFetch(CITI:Key_City)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_CD:City = CITI:City
            SUBU:CIID = CITI:CIID
          ELSE
            CLEAR(SUBU:CIID)
            SELECT(?L_CD:City)
            CYCLE
          END
        ELSE
          SUBU:CIID = CITI:CIID
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          L_City_Details  = Get_City_Details(CITI:CIID)
      
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Addresses PROCEDURE (p:Address_Name, p:Type, p:Branch)

CurrentTab           STRING(80)                            ! 
LOC:Screen_Group     GROUP,PRE()                           ! 
L_SG:BranchName      STRING(35)                            ! Branch Name
L_SG:BID             ULONG                                 ! Branch ID
                     END                                   ! 
LOC:Type             STRING(2)                             ! 1 - Delivery
LOC:Locator          STRING(50)                            ! 
LOC:Show_Clients     BYTE(1)                               ! This is used for a Client
LOC:No_Client_Delete BYTE                                  ! 
LOC:Show_Archived    BYTE                                  ! Include Archived addresses in the list
LOC:Archive_Warning_Show BYTE                              ! 
BRW1::View:Browse    VIEW(Addresses)
                       PROJECT(ADD:AddressName)
                       PROJECT(ADD:Line1)
                       PROJECT(ADD:PhoneNo)
                       PROJECT(ADD:Branch)
                       PROJECT(ADD:Client)
                       PROJECT(ADD:Delivery)
                       PROJECT(ADD:Transporter)
                       PROJECT(ADD:Journey)
                       PROJECT(ADD:Accountant)
                       PROJECT(ADD:ContainerTurnIn)
                       PROJECT(ADD:Line2)
                       PROJECT(ADD:PhoneNo2)
                       PROJECT(ADD:Fax)
                       PROJECT(ADD:Archived)
                       PROJECT(ADD:AID)
                       PROJECT(ADD:BID)
                       PROJECT(ADD:SUID)
                       JOIN(SUBU:PKey_SUID,ADD:SUID)
                         PROJECT(SUBU:Suburb)
                         PROJECT(SUBU:PostalCode)
                         PROJECT(SUBU:SUID)
                         PROJECT(SUBU:CIID)
                         JOIN(CITI:PKey_CIID,SUBU:CIID)
                           PROJECT(CITI:City)
                           PROJECT(CITI:CIID)
                           PROJECT(CITI:PRID)
                           JOIN(PROV:PKey_PRID,CITI:PRID)
                             PROJECT(PROV:Province)
                             PROJECT(PROV:PRID)
                             PROJECT(PROV:COID)
                             JOIN(COUN:PKey_COID,PROV:COID)
                               PROJECT(COUN:Country)
                               PROJECT(COUN:COID)
                             END
                           END
                         END
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
ADD:AddressName_Style  LONG                           !Field style
ADD:Line1              LIKE(ADD:Line1)                !List box control field - type derived from field
SUBU:Suburb            LIKE(SUBU:Suburb)              !List box control field - type derived from field
CITI:City              LIKE(CITI:City)                !List box control field - type derived from field
ADD:PhoneNo            LIKE(ADD:PhoneNo)              !List box control field - type derived from field
ADD:Branch             LIKE(ADD:Branch)               !List box control field - type derived from field
ADD:Branch_Icon        LONG                           !Entry's icon ID
ADD:Client             LIKE(ADD:Client)               !List box control field - type derived from field
ADD:Client_Icon        LONG                           !Entry's icon ID
ADD:Delivery           LIKE(ADD:Delivery)             !List box control field - type derived from field
ADD:Delivery_Icon      LONG                           !Entry's icon ID
ADD:Transporter        LIKE(ADD:Transporter)          !List box control field - type derived from field
ADD:Transporter_Icon   LONG                           !Entry's icon ID
ADD:Journey            LIKE(ADD:Journey)              !List box control field - type derived from field
ADD:Journey_Icon       LONG                           !Entry's icon ID
ADD:Accountant         LIKE(ADD:Accountant)           !List box control field - type derived from field
ADD:Accountant_Icon    LONG                           !Entry's icon ID
ADD:ContainerTurnIn    LIKE(ADD:ContainerTurnIn)      !List box control field - type derived from field
ADD:ContainerTurnIn_Icon LONG                         !Entry's icon ID
ADD:Line2              LIKE(ADD:Line2)                !List box control field - type derived from field
SUBU:PostalCode        LIKE(SUBU:PostalCode)          !List box control field - type derived from field
COUN:Country           LIKE(COUN:Country)             !List box control field - type derived from field
PROV:Province          LIKE(PROV:Province)            !List box control field - type derived from field
ADD:PhoneNo2           LIKE(ADD:PhoneNo2)             !List box control field - type derived from field
ADD:Fax                LIKE(ADD:Fax)                  !List box control field - type derived from field
ADD:Archived           LIKE(ADD:Archived)             !List box control field - type derived from field
ADD:Archived_Icon      LONG                           !Entry's icon ID
ADD:AID                LIKE(ADD:AID)                  !List box control field - type derived from field
ADD:BID                LIKE(ADD:BID)                  !List box control field - type derived from field
LOC:Show_Archived      LIKE(LOC:Show_Archived)        !Browse hot field - type derived from local data
ADD:SUID               LIKE(ADD:SUID)                 !Browse key field - type derived from field
SUBU:SUID              LIKE(SUBU:SUID)                !Related join file key field - type derived from field
CITI:CIID              LIKE(CITI:CIID)                !Related join file key field - type derived from field
PROV:PRID              LIKE(PROV:PRID)                !Related join file key field - type derived from field
COUN:COID              LIKE(COUN:COID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB6::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Addresses File'),AT(,,358,224),FONT('Tahoma',8),RESIZE,GRAY,IMM,MAX,MDI, |
  HLP('BrowseAddresses'),SYSTEM
                       LIST,AT(8,32,342,148),USE(?Browse:1),HVSCROLL,ALRT(F2Key),FORMAT('80L(2)|MY~Name~@s35@8' & |
  '0L(2)|M~Line 1~@s35@70L(2)|M~Suburb~@s50@70L(2)|M~City~@s35@60L(2)|M~Phone No.~@s20@' & |
  '[28R(2)|MI~Branch~C(0)@p p@28R(2)|MI~Client~C(0)@p p@30R(2)|MI~Delivery~C(0)@p p@40R' & |
  '(2)|MI~Transporter~C(0)@p p@30R(2)|MI~Journey~C(0)@p p@44R(2)|MI~Accountant~C(0)@p p' & |
  '@50R(2)|MI~Container Turn In~L(1)@p p@]|M~Used For~80L(2)|M~Line 2~@s35@44L(2)|M~Pos' & |
  'tal Code~@s10@70L(2)|M~Country~@s50@70L(2)|M~Province~@s35@60L(2)|M~Phone No. 2~@s20' & |
  '@60L(2)|M~Fax~@s60@20R(2)|MI~Archived~L(0)@p p@30R(2)|M~AID~L(2)@n_10@30R(2)|M~BID~L(2)@n_10@'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing Records')
                       BUTTON('&Select'),AT(122,184,,14),USE(?Select:2),LEFT,ICON('WAselect.ICO'),FLAT
                       BUTTON('&Insert'),AT(180,184,,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT
                       BUTTON('&Change'),AT(236,184,,14),USE(?Change:3),LEFT,ICON('WAchange.ICO'),DEFAULT,FLAT
                       BUTTON('&Delete'),AT(296,184,,14),USE(?Delete:3),LEFT,ICON('WAdelete.ICO'),FLAT
                       SHEET,AT(4,4,350,198),USE(?CurrentTab)
                         TAB('By Name'),USE(?Tab:2)
                           PROMPT('Locator:'),AT(6,20),USE(?LOC:Locator:Prompt),TRN
                           STRING(@s50),AT(38,20),USE(LOC:Locator),TRN
                           GROUP,AT(5,210,181,10),USE(?Group1)
                             CHECK(' Show &Clients'),AT(5,210),USE(LOC:Show_Clients),MSG('This is used for a client'),TIP('This is us' & |
  'ed for a client')
                             PROMPT('Branch:'),AT(78,210),USE(?Prompt2)
                             LIST,AT(109,210,77,10),USE(L_SG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch N' & |
  'ame~@s35@40R(2)|M~BID~L@n_10@'),FROM(Queue:FileDrop)
                           END
                           BUTTON('Export...'),AT(9,184),USE(?BUTTON_Exprot)
                         END
                         TAB('By Suburb'),USE(?Tab:3)
                           BUTTON('Suburb'),AT(9,184,,14),USE(?SelectSuburbs),LEFT,ICON('WAPARENT.ICO'),FLAT
                         END
                       END
                       BUTTON('Close'),AT(309,206,45,14),USE(?Close),LEFT,ICON('WAclose.ICO'),FLAT
                       BUTTON('Help'),AT(308,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                       CHECK(' Show Archived'),AT(203,210),USE(LOC:Show_Archived),MSG('Include Archived addres' & |
  'ses in the list'),TIP('Include Archived addresses in the list<0DH,0AH,0DH,0AH>To tog' & |
  'gle Archived press F2')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB6                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
  !------------------------------------
  !Style for ?Browse:1
  !------------------------------------
  ?Browse:1{PROPSTYLE:FontName, 1}      = ''
  ?Browse:1{PROPSTYLE:FontSize, 1}      = 10
  ?Browse:1{PROPSTYLE:FontStyle, 1}     = 0
  ?Browse:1{PROPSTYLE:TextColor, 1}     = -1
  ?Browse:1{PROPSTYLE:BackColor, 1}     = 12440319
  ?Browse:1{PROPSTYLE:TextSelected, 1}  = -1
  ?Browse:1{PROPSTYLE:BackSelected, 1}  = 128
  !------------------------------------
!---------------------------------------------------------------------------
Check_Omitted                       ROUTINE
    ! (p:Address_Name, p:Type, p:Branch)
    ! (<STRING>, <STRING>, ULONG=0)

    IF ThisWindow.Request = SelectRecord AND ~OMITTED(1)
       IF CLIP(p:Address_Name) ~= ''
          CLEAR(ADD:Record)
          ADD:AddressName           = p:Address_Name
          IF Access:Addresses.TryFetch(ADD:Key_Name) = LEVEL:Benign
             IF ADD:BID ~= p:Branch
                p:Branch   = 0         ! Will cause to set address branch lower down here
                MESSAGE('The Branch set on the Address does not match your Client / Transporter record branch.', 'Address Branch Warning', ICON:Exclamation)
             .
          ELSE
             CASE MESSAGE('The Address Name specified was not found.||Address Name: ' & CLIP(p:Address_Name) & '||Would you like to add a record with this Address Name now?', 'Browse Addresses', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
             OF BUTTON:Yes
                ! Insert record with this name?
                IF Access:Addresses.TryPrimeAutoInc() = LEVEL:Benign
                   ADD:AddressName     = p:Address_Name
                   GlobalRequest       = InsertRecord
                   Update_Addresses()


      db.debugout('[Browse_Addresses]   Back from update...')


                   IF GlobalResponse = RequestCompleted
                      ! Check branch is same as passed
                      IF ADD:BID ~= p:Branch
                         p:Branch   = 0         ! Will cause to set address branch lower down here
                         MESSAGE('The Branch set on the Address does not match your Client / Transporter record branch.', 'Address Branch Warning', ICON:Exclamation)
                      .

                      POST(EVENT:Accepted,?Select:2)
                   .
                   !ThisWindow.Reset()
    .  .  .  .  .


    IF ~OMITTED(2)
       LOC:Type     = p:Type

       CASE LOC:Type
       OF 'DC' OROF 'DD'                    ! Delivery Collection point, Delivery Delivery Point
          LOC:Show_Clients  = FALSE
       OF 'CL'
    .  .


    ! Set to this branch then
    IF p:Branch ~= 0
       BRA:BID              = p:Branch
       IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
          L_SG:BranchName   = BRA:BranchName
          L_SG:BID          = BRA:BID
       .
    ELSIF ADD:BID ~= L_SG:BID               ! Address Branch differs from users branch
       L_SG:BID             = ADD:BID       ! Set address branch.... ??
    .
    EXIT
!Apply_User_Permissions      ROUTINE               ! Special user permissions
!    LOC:No_Client_Delete = FALSE
!    IF ADD:Client = TRUE
!       CASE Get_User_Access(GLO:UID, 'Addresses', 'Update_Addresses', 'Delete_Mode_Client_Address', 0)
!       OF 0 OROF 100
!       !OF 2
!       ELSE
!          LOC:No_Client_Delete = TRUE
!    .  .
!    EXIT
Archive_Toggle                  ROUTINE
DATA
R:Yes   BYTE

CODE  
  BRW1.UpdateViewRecord()
  BRW1.UpdateBuffer()
  
  ! Toggle archived on highlighted...
  IF LOC:Archive_Warning_Show = 0
     IF ADD:Archived = FALSE
        CASE MESSAGE('Archive this Address?','Archive Addresses',ICON:Question, 'Yes|Yes (don''t warn again)|No', 3)
        OF 1
          R:Yes = TRUE
        OF 2    
          R:Yes = TRUE          
          LOC:Archive_Warning_Show  = 1
        .
      ELSE
        CASE MESSAGE('Un-Archive this Address?','Archive Addresses',ICON:Question, 'Yes|Yes (don''t warn again)|No', 3)
        OF 1
          R:Yes = TRUE
        OF 2    
          R:Yes = TRUE          
          LOC:Archive_Warning_Show  = 1
        .        
      .
  ELSE
    R:Yes = TRUE
  .
  
  IF R:Yes = TRUE    
    BRW1.UpdateViewRecord()
    
    BRW1.UpdateBuffer()
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = Level:Benign
      IF ADD:Archived  = 1
        ADD:Archived = 0
      ELSE
        ADD:Archived = 1
        ADD:Archived_Date = TODAY()
        ADD:Archived_Time = CLOCK()
      .          
      IF Access:Addresses.TryUpdate() = Level:Benign
        BRW1.ResetFromBuffer()  
  . . .    

  EXIT
!-------------------------------------------------------------------------------
Export                        ROUTINE
   DATA
_View            VIEW(Addresses)
         JOIN(SUBU:PKey_SUID, ADD:SUID)
           JOIN(CITI:PKey_CIID, SUBU:CIID)
            JOIN(PROV:PKey_PRID, CITI:PRID)
            .
           .           
        .                   
   .
_Mgr_View        ViewManager

l:line STRING(1000)

l:query SQLQueryClass
l:queryStr STRING(1000)   


ModalWin WINDOW('Export'),AT(,,158,84),CENTER,MDI,MODAL
        STRING('Working...'),AT(58,30)
    END

l:close BYTE(0)

l:filePath STRING(1000)

   CODE
   
   res# = FILEDIALOG('Choose a location and file name for Addresses export file.', l:filePath, '*.csv' |
         , FILE:Directory + FILE:AddExtension + FILE:LongName + FILE:KeepDir)
   IF res# <= 0
      EXIT
   .
   
   l:filePath = CLIP(l:filePath) & '\addresses.csv'
   
   IF Check_File_Existence(l:filePath) 
      CASE MESSAGE('File exists, if you continue it will be written over.||' & CLIP(l:filePath), 'File', ICON:Exclamation, BUTTON:Ok+BUTTON:CANCEL,BUTTON:OK)
      OF BUTTON:Cancel
         EXIT      
   .  .
      
   OPEN(ModalWin)   
   l:queryStr = 'select distinct d.cid, c.CLIENTNO, c.CLIENTNAME ' & |
               'from Deliveries as d ' & |
	            'join Clients as c ' & |
		               'on c.CID = d.CID ' & |
               'where d.DELIVERYAID = '
   
   l:query.Init(GLO:DBOwner, '__SQLTemp2')
   
   _Mgr_View.init(_View, Relate:Addresses)
   _Mgr_View.AddSortOrder(ADD:Key_Name)
!   _Mgr_View.AddRange(ADD:)

!    _Mgr_View.AppendOrder('INV:InvoiceDate')
    _Mgr_View.SetFilter('((ADD:ShowForAllBranches = 1 OR L_SG:BID = 0 OR L_SG:BID = ADD:BID) AND (LOC:Show_Clients = 1 OR ADD:Client = 0 OR ADD:Delivery = 1) AND (LOC:Show_Archived = 1 OR ADD:Archived = 0))')


    _Mgr_View.Reset()
    IF ERRORCODE()
       MESSAGE('Error on View.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Addresses', ICON:Hand)
    .

   Add_Log('Address Name, Line1, Line2, Suburb, City, Province, Phone No, Client 1 No., Client 1 Name, Client 2 No., Client 2 Name','', l:filePath,1,,0)
   
   count# = 0
   
   DISPLAY
   SETCURSOR(CURSOR:Wait)
   ACCEPT
       LOOP 
          count# += 1
          if count# / 10
            YIELD
          .
          IF _Mgr_View.Next() ~= LEVEL:Benign
             BREAK
          .
          if ADD:Delivery = true
            l:line = '"' & CLIP(Wrap_CSV_Field(ADD:AddressName)) & '"'
            l:line = CLIP(l:line) & ',"' & CLIP(Wrap_CSV_Field(ADD:Line1)) & '"'
            l:line = CLIP(l:line) & ',"' & CLIP(Wrap_CSV_Field(ADD:Line2)) & '"'
            l:line = CLIP(l:line) & ',"' & CLIP(Wrap_CSV_Field(SUBU:Suburb)) & '"'
            l:line = CLIP(l:line) & ',"' & CLIP(Wrap_CSV_Field(CITI:City)) & '"'
            l:line = CLIP(l:line) & ',"' & CLIP(Wrap_CSV_Field(PROV:Province)) & '"'
            l:line = CLIP(l:line) & ',"' & CLIP(Wrap_CSV_Field(ADD:PhoneNo)) & '"'
            
            ! get 1 delivery and then the client
            l:query.PropSQL(l:queryStr & ADD:AID)
            loop while l:query.Next_Q() > 0         
               l:line = CLIP(l:line) & ',"' & CLIP(Wrap_CSV_Field(l:query.Get_El(2))) & '"'
               l:line = CLIP(l:line) & ',"' & CLIP(Wrap_CSV_Field(l:query.Get_El(3))) & '"'
               !db.Debugout('1: ' & CLIP(l:query.Get_El(1)) & '      2: ' & CLIP(l:query.Get_El(2)) & '    3: ' & CLIP(l:query.Get_El(3)))
            .
            
            Add_Log(l:line,count#, l:filePath,,,0)            
          .
       .
       BREAK
    .
    BEEP(BEEP:SystemExclamation)
    SETCURSOR()
    CLOSE(ModalWin)
   EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Addresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_SG:BID',L_SG:BID)                                ! Added by: BrowseBox(ABC)
  BIND('LOC:Show_Clients',LOC:Show_Clients)                ! Added by: BrowseBox(ABC)
  BIND('LOC:Show_Archived',LOC:Show_Archived)              ! Added by: BrowseBox(ABC)
                       BIND('ADD:Branch', ADD:Branch)
                       BIND('ADD:Client', ADD:Client)
                       BIND('ADD:Delivery', ADD:Delivery)
                       BIND('ADD:Transporter', ADD:Transporter)
                       BIND('ADD:Journey', ADD:Journey)
                       BIND('ADD:Accountant', ADD:Accountant)
                       BIND('ADD:ContainerTurnIn', ADD:ContainerTurnIn)
  
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Add_Suburbs.SetOpenRelated()
  Relate:Add_Suburbs.Open                                  ! File Add_Suburbs used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Addresses,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon ADD:SUID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,ADD:FKey_SUID)   ! Add the sort order for ADD:FKey_SUID for sort order 1
  BRW1.AddRange(ADD:SUID,SUBU:SUID)                        ! Add single value range limit for sort order 1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon ADD:AddressName for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ADD:Key_Name)    ! Add the sort order for ADD:Key_Name for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(?LOC:Locator,ADD:AddressName,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: ADD:Key_Name , ADD:AddressName
  BRW1.SetFilter('((ADD:ShowForAllBranches = 1 OR L_SG:BID = 0 OR L_SG:BID = ADD:BID) AND (LOC:Show_Clients = 1 OR ADD:Client = 0 OR ADD:Delivery = 1) AND (LOC:Show_Archived = 1 OR ADD:Archived = 0))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Show_Archived)                    ! Apply the reset field
  BRW1.AddResetField(LOC:Show_Clients)                     ! Apply the reset field
  BRW1.AddResetField(L_SG:BID)                             ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(ADD:AddressName,BRW1.Q.ADD:AddressName)    ! Field ADD:AddressName is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Line1,BRW1.Q.ADD:Line1)                ! Field ADD:Line1 is a hot field or requires assignment from browse
  BRW1.AddField(SUBU:Suburb,BRW1.Q.SUBU:Suburb)            ! Field SUBU:Suburb is a hot field or requires assignment from browse
  BRW1.AddField(CITI:City,BRW1.Q.CITI:City)                ! Field CITI:City is a hot field or requires assignment from browse
  BRW1.AddField(ADD:PhoneNo,BRW1.Q.ADD:PhoneNo)            ! Field ADD:PhoneNo is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Branch,BRW1.Q.ADD:Branch)              ! Field ADD:Branch is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Client,BRW1.Q.ADD:Client)              ! Field ADD:Client is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Delivery,BRW1.Q.ADD:Delivery)          ! Field ADD:Delivery is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Transporter,BRW1.Q.ADD:Transporter)    ! Field ADD:Transporter is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Journey,BRW1.Q.ADD:Journey)            ! Field ADD:Journey is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Accountant,BRW1.Q.ADD:Accountant)      ! Field ADD:Accountant is a hot field or requires assignment from browse
  BRW1.AddField(ADD:ContainerTurnIn,BRW1.Q.ADD:ContainerTurnIn) ! Field ADD:ContainerTurnIn is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Line2,BRW1.Q.ADD:Line2)                ! Field ADD:Line2 is a hot field or requires assignment from browse
  BRW1.AddField(SUBU:PostalCode,BRW1.Q.SUBU:PostalCode)    ! Field SUBU:PostalCode is a hot field or requires assignment from browse
  BRW1.AddField(COUN:Country,BRW1.Q.COUN:Country)          ! Field COUN:Country is a hot field or requires assignment from browse
  BRW1.AddField(PROV:Province,BRW1.Q.PROV:Province)        ! Field PROV:Province is a hot field or requires assignment from browse
  BRW1.AddField(ADD:PhoneNo2,BRW1.Q.ADD:PhoneNo2)          ! Field ADD:PhoneNo2 is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Fax,BRW1.Q.ADD:Fax)                    ! Field ADD:Fax is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Archived,BRW1.Q.ADD:Archived)          ! Field ADD:Archived is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AID,BRW1.Q.ADD:AID)                    ! Field ADD:AID is a hot field or requires assignment from browse
  BRW1.AddField(ADD:BID,BRW1.Q.ADD:BID)                    ! Field ADD:BID is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Show_Archived,BRW1.Q.LOC:Show_Archived) ! Field LOC:Show_Archived is a hot field or requires assignment from browse
  BRW1.AddField(ADD:SUID,BRW1.Q.ADD:SUID)                  ! Field ADD:SUID is a hot field or requires assignment from browse
  BRW1.AddField(SUBU:SUID,BRW1.Q.SUBU:SUID)                ! Field SUBU:SUID is a hot field or requires assignment from browse
  BRW1.AddField(CITI:CIID,BRW1.Q.CITI:CIID)                ! Field CITI:CIID is a hot field or requires assignment from browse
  BRW1.AddField(PROV:PRID,BRW1.Q.PROV:PRID)                ! Field PROV:PRID is a hot field or requires assignment from browse
  BRW1.AddField(COUN:COID,BRW1.Q.COUN:COID)                ! Field COUN:COID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Addresses',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      BRA:BID             = GLO:BranchID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         L_SG:BranchName  = BRA:BranchName
         L_SG:BID         = BRA:BID
      .
      DO Check_Omitted
  BRW1.AskProcedure = 1                                    ! Will call: Update_Addresses(LOC:Type)
  FDB6.Init(?L_SG:BranchName,Queue:FileDrop.ViewPosition,FDB6::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB6.Q &= Queue:FileDrop
  FDB6.AddSortOrder(BRA:Key_BranchName)
  FDB6.AddField(BRA:BranchName,FDB6.Q.BRA:BranchName) !List box control field - type derived from field
  FDB6.AddField(BRA:BID,FDB6.Q.BRA:BID) !List box control field - type derived from field
  FDB6.AddUpdateField(BRA:BID,L_SG:BID)
  ThisWindow.AddItem(FDB6.WindowComponent)
  FDB6.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
        db.debugout('[Browse_Addresses]   Kill..')
        UNBIND('L_SG:BID')                  ! these should be added by the templates
        UNBIND('LOC:Show_Clients')
  
                       UNBIND('ADD:Branch')
                       UNBIND('ADD:Client')
                       UNBIND('ADD:Delivery')
                       UNBIND('ADD:Transporter')
                       UNBIND('ADD:Journey')
                       UNBIND('ADD:Accountant')
                       UNBIND('ADD:ContainerTurnIn')
  
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Suburbs.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Addresses',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Addresses(LOC:Type)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Select:2
      ThisWindow.Update()
          IF LOC:Type = 1
             IF ADD:Client = TRUE AND ADD:Delivery = FALSE
                MESSAGE('The address selected is a Client address and not a Delivery Address, please select another address.', 'Select Address', ICON:Exclamation)
          .  .
    OF ?BUTTON_Exprot
      ThisWindow.Update()
         DO Export
    OF ?SelectSuburbs
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Update_Suburbs()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      IF KEYCODE() = F2Key
        DO Archive_Toggle
      .
      !
      !IF KEYCODE() = F2Key
      !  ! Toggle archived on highlighted...
      !  BRW1.UpdateBuffer()
      !  IF Access:AddressAlias.TryFetch(A_ADD:PKey_AID) = Level:Benign
      !    IF A_ADD:Archived  = 1
      !      A_ADD:Archived = 0
      !    ELSE
      !      A_ADD:Archived = 1
      !    .          
      !    IF Access:AddressAlias.TryUpdate() = Level:Benign
      !      !BRW1.ResetQueue(Reset:Queue)
      !      !BRW1.UpdateWindow()
      !      !BRW1.ResetFromFile
      !      BRW1.ResetFromAsk(ChangeRecord, RequestCompleted)
      !. . .    
      !  
      !  
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !    L_Return_Group      = Get_Suburb_Details(ADD:SUID)
  PARENT.SetQueueRecord
  
  IF ((LOC:Type = 'DC' OR LOC:Type = 'DD') AND ADD:Client = 1)
    SELF.Q.ADD:AddressName_Style = 1 ! 
  ELSE
    SELF.Q.ADD:AddressName_Style = 2 ! 0
  END
  IF (ADD:Branch = 1)
    SELF.Q.ADD:Branch_Icon = 2                             ! Set icon from icon list
  ELSE
    SELF.Q.ADD:Branch_Icon = 1                             ! Set icon from icon list
  END
  IF (ADD:Client = 1)
    SELF.Q.ADD:Client_Icon = 2                             ! Set icon from icon list
  ELSE
    SELF.Q.ADD:Client_Icon = 1                             ! Set icon from icon list
  END
  IF (ADD:Delivery = 1)
    SELF.Q.ADD:Delivery_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.ADD:Delivery_Icon = 1                           ! Set icon from icon list
  END
  IF (ADD:Transporter = 1)
    SELF.Q.ADD:Transporter_Icon = 2                        ! Set icon from icon list
  ELSE
    SELF.Q.ADD:Transporter_Icon = 1                        ! Set icon from icon list
  END
  IF (ADD:Journey = 1)
    SELF.Q.ADD:Journey_Icon = 2                            ! Set icon from icon list
  ELSE
    SELF.Q.ADD:Journey_Icon = 1                            ! Set icon from icon list
  END
  IF (ADD:Accountant = 1)
    SELF.Q.ADD:Accountant_Icon = 2                         ! Set icon from icon list
  ELSE
    SELF.Q.ADD:Accountant_Icon = 1                         ! Set icon from icon list
  END
  IF (ADD:ContainerTurnIn = 1)
    SELF.Q.ADD:ContainerTurnIn_Icon = 2                    ! Set icon from icon list
  ELSE
    SELF.Q.ADD:ContainerTurnIn_Icon = 1                    ! Set icon from icon list
  END
  IF (ADD:Archived = 1)
    SELF.Q.ADD:Archived_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.ADD:Archived_Icon = 1                           ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group1, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group1


FDB6.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      ! Add an All
      Queue:FileDrop.BRA:BranchName      = 'All'
      GET(Queue:FileDrop, Queue:FileDrop.BRA:BranchName)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop)
         Queue:FileDrop.BRA:BranchName      = 'All'
         Queue:FileDrop.BRA:BID             = 0
         ADD(Queue:FileDrop)
      .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Suburbs PROCEDURE 

CurrentTab           STRING(80)                            ! 
BRW1::View:Browse    VIEW(Add_Suburbs)
                       PROJECT(SUBU:Suburb)
                       PROJECT(SUBU:PostalCode)
                       PROJECT(SUBU:SUID)
                       PROJECT(SUBU:CIID)
                       JOIN(CITI:PKey_CIID,SUBU:CIID)
                         PROJECT(CITI:City)
                         PROJECT(CITI:CIID)
                         PROJECT(CITI:PRID)
                         JOIN(PROV:PKey_PRID,CITI:PRID)
                           PROJECT(PROV:Province)
                           PROJECT(PROV:PRID)
                           PROJECT(PROV:COID)
                           JOIN(COUN:PKey_COID,PROV:COID)
                             PROJECT(COUN:Country)
                             PROJECT(COUN:COID)
                           END
                         END
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
SUBU:Suburb            LIKE(SUBU:Suburb)              !List box control field - type derived from field
SUBU:PostalCode        LIKE(SUBU:PostalCode)          !List box control field - type derived from field
CITI:City              LIKE(CITI:City)                !List box control field - type derived from field
PROV:Province          LIKE(PROV:Province)            !List box control field - type derived from field
COUN:Country           LIKE(COUN:Country)             !List box control field - type derived from field
SUBU:SUID              LIKE(SUBU:SUID)                !Primary key field - type derived from field
SUBU:CIID              LIKE(SUBU:CIID)                !Browse key field - type derived from field
CITI:CIID              LIKE(CITI:CIID)                !Related join file key field - type derived from field
PROV:PRID              LIKE(PROV:PRID)                !Related join file key field - type derived from field
COUN:COID              LIKE(COUN:COID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Suburbs File'),AT(,,330,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Browse_Suburbs'),SYSTEM
                       LIST,AT(8,30,314,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Suburb~@s50@44L(2)|M~Pos' & |
  'tal Code~@s10@70L(2)|M~City~@s35@70L(2)|M~Province~@s35@70L(2)|M~Country~@s50@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the Add_Suburbs file')
                       BUTTON('&Select'),AT(61,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(114,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(167,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(220,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(273,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,322,172),USE(?CurrentTab)
                         TAB('&1) By Suburb'),USE(?Tab:2)
                         END
                         TAB('&2) By City'),USE(?Tab:3)
                           BUTTON('Select Add_Cities'),AT(9,158,49,14),USE(?SelectAdd_Cities),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(277,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Suburbs')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Add_Cities.SetOpenRelated()
  Relate:Add_Cities.Open                                   ! File Add_Cities used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Add_Suburbs,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon SUBU:CIID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,SUBU:FKey_CIID)  ! Add the sort order for SUBU:FKey_CIID for sort order 1
  BRW1.AddRange(SUBU:CIID,Relate:Add_Suburbs,Relate:Add_Cities) ! Add file relationship range limit for sort order 1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon SUBU:Suburb for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,SUBU:Key_Suburb) ! Add the sort order for SUBU:Key_Suburb for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,SUBU:Suburb,1,BRW1)            ! Initialize the browse locator using  using key: SUBU:Key_Suburb , SUBU:Suburb
  BRW1.AddField(SUBU:Suburb,BRW1.Q.SUBU:Suburb)            ! Field SUBU:Suburb is a hot field or requires assignment from browse
  BRW1.AddField(SUBU:PostalCode,BRW1.Q.SUBU:PostalCode)    ! Field SUBU:PostalCode is a hot field or requires assignment from browse
  BRW1.AddField(CITI:City,BRW1.Q.CITI:City)                ! Field CITI:City is a hot field or requires assignment from browse
  BRW1.AddField(PROV:Province,BRW1.Q.PROV:Province)        ! Field PROV:Province is a hot field or requires assignment from browse
  BRW1.AddField(COUN:Country,BRW1.Q.COUN:Country)          ! Field COUN:Country is a hot field or requires assignment from browse
  BRW1.AddField(SUBU:SUID,BRW1.Q.SUBU:SUID)                ! Field SUBU:SUID is a hot field or requires assignment from browse
  BRW1.AddField(SUBU:CIID,BRW1.Q.SUBU:CIID)                ! Field SUBU:CIID is a hot field or requires assignment from browse
  BRW1.AddField(CITI:CIID,BRW1.Q.CITI:CIID)                ! Field CITI:CIID is a hot field or requires assignment from browse
  BRW1.AddField(PROV:PRID,BRW1.Q.PROV:PRID)                ! Field PROV:PRID is a hot field or requires assignment from browse
  BRW1.AddField(COUN:COID,BRW1.Q.COUN:COID)                ! Field COUN:COID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Suburbs',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                                    ! Will call: Update_Suburbs
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Cities.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Suburbs',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Suburbs
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectAdd_Cities
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Addr_Cities()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

