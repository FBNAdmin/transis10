

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS016.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Address_Contacts PROCEDURE (p:AID)

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(AddressContacts)
                       PROJECT(ADDC:ContactName)
                       PROJECT(ADDC:PrimaryContact)
                       PROJECT(ADDC:ACID)
                       PROJECT(ADDC:AID)
                       JOIN(ADD:PKey_AID,ADDC:AID)
                         PROJECT(ADD:PhoneNo)
                         PROJECT(ADD:PhoneNo2)
                         PROJECT(ADD:Fax)
                         PROJECT(ADD:AddressName)
                         PROJECT(ADD:AID)
                         JOIN(EMAI:FKey_AID,ADD:AID)
                           PROJECT(EMAI:EmailName)
                           PROJECT(EMAI:EmailAddress)
                         END
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ADDC:ContactName       LIKE(ADDC:ContactName)         !List box control field - type derived from field
ADDC:PrimaryContact    LIKE(ADDC:PrimaryContact)      !List box control field - type derived from field
ADDC:PrimaryContact_Icon LONG                         !Entry's icon ID
ADD:PhoneNo            LIKE(ADD:PhoneNo)              !List box control field - type derived from field
ADD:PhoneNo2           LIKE(ADD:PhoneNo2)             !List box control field - type derived from field
ADD:Fax                LIKE(ADD:Fax)                  !List box control field - type derived from field
EMAI:EmailName         LIKE(EMAI:EmailName)           !List box control field - type derived from field
EMAI:EmailAddress      LIKE(EMAI:EmailAddress)        !List box control field - type derived from field
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
ADDC:ACID              LIKE(ADDC:ACID)                !Primary key field - type derived from field
ADDC:AID               LIKE(ADDC:AID)                 !Browse key field - type derived from field
ADD:AID                LIKE(ADD:AID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Address Contacts File'),AT(,,327,273),FONT('Tahoma',8),RESIZE,GRAY,IMM, |
  MAX,MDI,HLP('BrowseAddressContacts'),SYSTEM
                       LIST,AT(8,23,309,207),USE(?Browse:1),HVSCROLL,FORMAT('100L(2)|M~Contact Name~@s35@28L(2' & |
  ')|MI~Primary~L(1)@p p@42L(2)|M~Phone No.~@s20@42L(2)|M~Phone No. 2~@s20@42L(2)|M~Fax' & |
  '~@s20@42L(2)|M~Email Name~@s35@120L(2)|M~Email Address~@s255@140L(2)|M~Address Name~@s35@'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing Records')
                       BUTTON('&Select'),AT(7,234,45,14),USE(?Select:2),LEFT,ICON('WAselect.ico'),FLAT
                       BUTTON('&Insert'),AT(147,234,,14),USE(?Insert:3),LEFT,ICON('WAinsert.ICO'),FLAT
                       BUTTON('&Change'),AT(201,234,,14),USE(?Change:3),LEFT,ICON('WAChange.ICO'),DEFAULT,FLAT
                       BUTTON('&Delete'),AT(260,234,,14),USE(?Delete:3),LEFT,ICON('WADelete.ICO'),FLAT
                       SHEET,AT(4,4,319,249),USE(?CurrentTab)
                         TAB('By Address'),USE(?Tab:2)
                           BUTTON('Select Address'),AT(3,256,,14),USE(?SelectAddresses),LEFT,ICON('WAParent.ICO'),FLAT
                         END
                       END
                       BUTTON('Close'),AT(269,256,,14),USE(?Close),LEFT,ICON('waclose.ico'),FLAT
                       BUTTON('Help'),AT(97,256,45,14),USE(?Help),HIDE,STD(STD:Help)
                       STRING('**Client name goes here**'),AT(54,5,263),USE(?STRING_client)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Address_Contacts')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:AddressContacts.SetOpenRelated()
  Relate:AddressContacts.Open                              ! File AddressContacts used by this procedure, so make sure it's RelationManager is open
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:AddressContacts,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon ADDC:AID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ADDC:FKey_AID)   ! Add the sort order for ADDC:FKey_AID for sort order 1
  BRW1.AddRange(ADDC:AID,Relate:AddressContacts,Relate:Addresses) ! Add file relationship range limit for sort order 1
  BRW1.AppendOrder('-ADDC:PrimaryContact,+ADDC:ContactName,+ADDC:ACID') ! Append an additional sort order
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(ADDC:ContactName,BRW1.Q.ADDC:ContactName)  ! Field ADDC:ContactName is a hot field or requires assignment from browse
  BRW1.AddField(ADDC:PrimaryContact,BRW1.Q.ADDC:PrimaryContact) ! Field ADDC:PrimaryContact is a hot field or requires assignment from browse
  BRW1.AddField(ADD:PhoneNo,BRW1.Q.ADD:PhoneNo)            ! Field ADD:PhoneNo is a hot field or requires assignment from browse
  BRW1.AddField(ADD:PhoneNo2,BRW1.Q.ADD:PhoneNo2)          ! Field ADD:PhoneNo2 is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Fax,BRW1.Q.ADD:Fax)                    ! Field ADD:Fax is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:EmailName,BRW1.Q.EMAI:EmailName)      ! Field EMAI:EmailName is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:EmailAddress,BRW1.Q.EMAI:EmailAddress) ! Field EMAI:EmailAddress is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AddressName,BRW1.Q.ADD:AddressName)    ! Field ADD:AddressName is a hot field or requires assignment from browse
  BRW1.AddField(ADDC:ACID,BRW1.Q.ADDC:ACID)                ! Field ADDC:ACID is a hot field or requires assignment from browse
  BRW1.AddField(ADDC:AID,BRW1.Q.ADDC:AID)                  ! Field ADDC:AID is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AID,BRW1.Q.ADD:AID)                    ! Field ADD:AID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Address_Contacts',QuickWindow)      ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
      ?String_Client{PROP:Text} = ''
      IF DEFORMAT(p:AID) > 0
        HIDE(?SelectAddresses)
        
        ?String_Client{PROP:Text} = 'Addresses for client: ' & CLIP(CLI:ClientName)
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressContacts.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Address_Contacts',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Address_Contacts(p:AID)
    ReturnValue = GlobalResponse
  END
    IF DEFORMAT(p:AID) > 0  
       ADD:AID = p:AID
    .
  
  !LOC:Reset_Field += 1
  !SELF.Reset(1)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectAddresses
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Address()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (ADDC:PrimaryContact = 1)
    SELF.Q.ADDC:PrimaryContact_Icon = 2                    ! Set icon from icon list
  ELSE
    SELF.Q.ADDC:PrimaryContact_Icon = 1                    ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

