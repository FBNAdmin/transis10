

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS024.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Print_Fields PROCEDURE (p:PrintType)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(PrintLayout)
                       PROJECT(PRIL:XPos)
                       PROJECT(PRIL:YPos)
                       PROJECT(PRIL:Length)
                       PROJECT(PRIL:Alignment)
                       PROJECT(PRIL:Repeating)
                       PROJECT(PRIL:PLID)
                       PROJECT(PRIL:PFID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
PRIL:XPos              LIKE(PRIL:XPos)                !List box control field - type derived from field
PRIL:YPos              LIKE(PRIL:YPos)                !List box control field - type derived from field
PRIL:Length            LIKE(PRIL:Length)              !List box control field - type derived from field
PRIL:Alignment         LIKE(PRIL:Alignment)           !List box control field - type derived from field
PRIL:Repeating         LIKE(PRIL:Repeating)           !List box control field - type derived from field
PRIL:PLID              LIKE(PRIL:PLID)                !Primary key field - type derived from field
PRIL:PFID              LIKE(PRIL:PFID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::PRIF:Record LIKE(PRIF:RECORD),THREAD
QuickWindow          WINDOW('Form Print Fields'),AT(,,273,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdatePrintFields'),SYSTEM
                       SHEET,AT(4,4,265,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Field Name:'),AT(9,22),USE(?PRIF:FieldName:Prompt),TRN
                           ENTRY(@s50),AT(61,22,204,10),USE(PRIF:FieldName),MSG('Field Name'),TIP('Field Name')
                           PROMPT('Print Type:'),AT(9,36),USE(?PRIF:PrintType:Prompt),TRN
                           LIST,AT(61,36,100,10),USE(PRIF:PrintType),DROP(5),FROM('Invoices|#0|Credit Notes|#1|Del' & |
  'ivery Notes|#2|Statements|#3')
                         END
                         TAB('&2) Print Layout'),USE(?Tab:2)
                           LIST,AT(9,20,257,74),USE(?Browse:2),HVSCROLL,FORMAT('40R(2)|M~X Pos~C(0)@n_5@40R(2)|M~Y' & |
  ' Pos~C(0)@n_5@40R(2)|M~Length~C(0)@n_5@40R(2)|M~Alignment~C(0)@n3@40R(2)|M~Repeating~C(0)@n3@'), |
  FROM(Queue:Browse:2),IMM,MSG('Browsing the PrintLayout file')
                           BUTTON('&Insert'),AT(110,98,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(162,98,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(217,98,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(168,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(220,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Print_Fields')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PRIF:FieldName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(PRIF:Record,History::PRIF:Record)
  SELF.AddHistoryField(?PRIF:FieldName,2)
  SELF.AddHistoryField(?PRIF:PrintType,3)
  SELF.AddUpdateFile(Access:PrintFields)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:PrintFields.SetOpenRelated()
  Relate:PrintFields.Open                                  ! File PrintFields used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PrintFields
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:PrintLayout,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?PRIF:FieldName{PROP:ReadOnly} = True
    DISABLE(?PRIF:PrintType)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon PRIL:PFID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,PRIL:FKey_PFID)  ! Add the sort order for PRIL:FKey_PFID for sort order 1
  BRW2.AddRange(PRIL:PFID,Relate:PrintLayout,Relate:PrintFields) ! Add file relationship range limit for sort order 1
  BRW2.AddField(PRIL:XPos,BRW2.Q.PRIL:XPos)                ! Field PRIL:XPos is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:YPos,BRW2.Q.PRIL:YPos)                ! Field PRIL:YPos is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:Length,BRW2.Q.PRIL:Length)            ! Field PRIL:Length is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:Alignment,BRW2.Q.PRIL:Alignment)      ! Field PRIL:Alignment is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:Repeating,BRW2.Q.PRIL:Repeating)      ! Field PRIL:Repeating is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:PLID,BRW2.Q.PRIL:PLID)                ! Field PRIL:PLID is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:PFID,BRW2.Q.PRIL:PFID)                ! Field PRIL:PFID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Print_Fields',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      PRIF:PrintType  = p:PrintType
  BRW2.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PrintFields.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Print_Fields',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Print_Layout
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

