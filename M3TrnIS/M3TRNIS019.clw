

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS019.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Branches PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Setup_Group      GROUP,PRE(L_SG)                       !
Branch               STRING(35)                            !Branch Name
Address              STRING(35)                            !Name of this address
DefaultRatesClient   STRING(100)                           !
DefaultRatesTransporter STRING(35)                         !Transporters Name
GeneralFuelSurchargeClient STRING(100)                     !Client
DefaultTollRateClient STRING(100)                          !
                     END                                   !
BRW2::View:Browse    VIEW(_Invoice)
                       PROJECT(INV:IID)
                       PROJECT(INV:BID)
                       PROJECT(INV:CID)
                       PROJECT(INV:ShipperName)
                       PROJECT(INV:ShipperLine1)
                       PROJECT(INV:ShipperLine2)
                       PROJECT(INV:ShipperSuburb)
                       PROJECT(INV:ShipperPostalCode)
                       PROJECT(INV:ConsigneeName)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
INV:IID                LIKE(INV:IID)                  !List box control field - type derived from field
INV:BID                LIKE(INV:BID)                  !List box control field - type derived from field
INV:CID                LIKE(INV:CID)                  !List box control field - type derived from field
INV:ShipperName        LIKE(INV:ShipperName)          !List box control field - type derived from field
INV:ShipperLine1       LIKE(INV:ShipperLine1)         !List box control field - type derived from field
INV:ShipperLine2       LIKE(INV:ShipperLine2)         !List box control field - type derived from field
INV:ShipperSuburb      LIKE(INV:ShipperSuburb)        !List box control field - type derived from field
INV:ShipperPostalCode  LIKE(INV:ShipperPostalCode)    !List box control field - type derived from field
INV:ConsigneeName      LIKE(INV:ConsigneeName)        !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(TripSheets)
                       PROJECT(TRI:BID)
                       PROJECT(TRI:DepartDate)
                       PROJECT(TRI:DepartTime)
                       PROJECT(TRI:ReturnedDate)
                       PROJECT(TRI:ReturnedTime)
                       PROJECT(TRI:Notes)
                       PROJECT(TRI:TRID)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
TRI:BID                LIKE(TRI:BID)                  !List box control field - type derived from field
TRI:DepartDate         LIKE(TRI:DepartDate)           !List box control field - type derived from field
TRI:DepartTime         LIKE(TRI:DepartTime)           !List box control field - type derived from field
TRI:ReturnedDate       LIKE(TRI:ReturnedDate)         !List box control field - type derived from field
TRI:ReturnedTime       LIKE(TRI:ReturnedTime)         !List box control field - type derived from field
TRI:Notes              LIKE(TRI:Notes)                !List box control field - type derived from field
TRI:TRID               LIKE(TRI:TRID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(Journeys)
                       PROJECT(JOU:Journey)
                       PROJECT(JOU:BID)
                       PROJECT(JOU:JID)
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?Browse:6
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
JOU:BID                LIKE(JOU:BID)                  !List box control field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(Transporter)
                       PROJECT(TRA:TransporterName)
                       PROJECT(TRA:OpsManager)
                       PROJECT(TRA:VATNo)
                       PROJECT(TRA:BID)
                       PROJECT(TRA:ACID)
                       PROJECT(TRA:TID)
                     END
Queue:Browse:8       QUEUE                            !Queue declaration for browse/combo box using ?Browse:8
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
TRA:OpsManager         LIKE(TRA:OpsManager)           !List box control field - type derived from field
TRA:VATNo              LIKE(TRA:VATNo)                !List box control field - type derived from field
TRA:BID                LIKE(TRA:BID)                  !List box control field - type derived from field
TRA:ACID               LIKE(TRA:ACID)                 !List box control field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(Manifest)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:TID)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:Rate)
                       PROJECT(MAN:VATRate)
                       PROJECT(MAN:State)
                       PROJECT(MAN:DepartDate)
                       PROJECT(MAN:DepartTime)
                       PROJECT(MAN:ETADate)
                       PROJECT(MAN:MID)
                     END
Queue:Browse:10      QUEUE                            !Queue declaration for browse/combo box using ?Browse:10
MAN:BID                LIKE(MAN:BID)                  !List box control field - type derived from field
MAN:TID                LIKE(MAN:TID)                  !List box control field - type derived from field
MAN:Cost               LIKE(MAN:Cost)                 !List box control field - type derived from field
MAN:Rate               LIKE(MAN:Rate)                 !List box control field - type derived from field
MAN:VATRate            LIKE(MAN:VATRate)              !List box control field - type derived from field
MAN:State              LIKE(MAN:State)                !List box control field - type derived from field
MAN:DepartDate         LIKE(MAN:DepartDate)           !List box control field - type derived from field
MAN:DepartTime         LIKE(MAN:DepartTime)           !List box control field - type derived from field
MAN:ETADate            LIKE(MAN:ETADate)              !List box control field - type derived from field
MAN:MID                LIKE(MAN:MID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(Clients)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:BID)
                       PROJECT(CLI:GenerateInvoice)
                       PROJECT(CLI:MinimiumCharge)
                       PROJECT(CLI:Rate)
                       PROJECT(CLI:DocumentCharge)
                       PROJECT(CLI:ACID)
                       PROJECT(CLI:CID)
                     END
Queue:Browse:12      QUEUE                            !Queue declaration for browse/combo box using ?Browse:12
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:BID                LIKE(CLI:BID)                  !List box control field - type derived from field
CLI:GenerateInvoice    LIKE(CLI:GenerateInvoice)      !List box control field - type derived from field
CLI:MinimiumCharge     LIKE(CLI:MinimiumCharge)       !List box control field - type derived from field
CLI:Rate               LIKE(CLI:Rate)                 !List box control field - type derived from field
CLI:DocumentCharge     LIKE(CLI:DocumentCharge)       !List box control field - type derived from field
CLI:ACID               LIKE(CLI:ACID)                 !List box control field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:BID)
                       PROJECT(DEL:CID)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:Rate)
                       PROJECT(DEL:DocumentCharge)
                       PROJECT(DEL:FuelSurcharge)
                       PROJECT(DEL:Charge)
                       PROJECT(DEL:DID)
                     END
Queue:Browse:14      QUEUE                            !Queue declaration for browse/combo box using ?Browse:14
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:BID                LIKE(DEL:BID)                  !List box control field - type derived from field
DEL:CID                LIKE(DEL:CID)                  !List box control field - type derived from field
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
DEL:Rate               LIKE(DEL:Rate)                 !List box control field - type derived from field
DEL:DocumentCharge     LIKE(DEL:DocumentCharge)       !List box control field - type derived from field
DEL:FuelSurcharge      LIKE(DEL:FuelSurcharge)        !List box control field - type derived from field
DEL:Charge             LIKE(DEL:Charge)               !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB3::View:FileDrop  VIEW(Addresses)
                       PROJECT(ADD:AddressName)
                       PROJECT(ADD:AID)
                     END
FDB5::View:FileDrop  VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?ADD:AddressName
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
ADD:AID                LIKE(ADD:AID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !Queue declaration for browse/combo box using ?FLO:Floor
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::BRA:Record  LIKE(BRA:RECORD),THREAD
QuickWindow          WINDOW('Update the Branches File'),AT(,,350,262),FONT('Tahoma',8),DOUBLE,GRAY,IMM,MDI,HLP('Update_Branches'), |
  SYSTEM
                       SHEET,AT(4,4,344,238),USE(?CurrentTab),JOIN
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Branch Name:'),AT(9,25),USE(?BRA:BranchName:Prompt),TRN
                           ENTRY(@s35),AT(121,25,215,10),USE(BRA:BranchName),MSG('Branch Name'),REQ,TIP('Branch Name')
                           PROMPT('Address Name:'),AT(9,49),USE(?BRA:BranchName:Prompt:2),TRN
                           LIST,AT(121,49,215,10),USE(ADD:AddressName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Name~@s35@'), |
  FROM(Queue:FileDrop),MSG('Name of this address')
                           PROMPT('Floor:'),AT(9,71),USE(?BRA:BranchName:Prompt:3),TRN
                           LIST,AT(121,71,144,10),USE(FLO:Floor),VSCROLL,DROP(15),FORMAT('140L(2)|M~Floor~@s35@'),FROM(Queue:FileDrop:1),MSG('Floor Name')
                           LINE,AT(9,97,331,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Default Rates Client:'),AT(9,111),USE(?DefaultRatesClient:Prompt),TRN
                           BUTTON('...'),AT(105,111,12,10),USE(?CallLookup_DefaultRateClient),FONT(,,,FONT:regular)
                           ENTRY(@s100),AT(121,111,215,10),USE(L_SG:DefaultRatesClient),REQ,TIP('Client name')
                           PROMPT('Default Rates Transporter:'),AT(9,134),USE(?DefaultRatesTransporter:Prompt),TRN
                           BUTTON('...'),AT(105,134,12,10),USE(?CallLookup:Transporter)
                           ENTRY(@s35),AT(121,134,215,10),USE(L_SG:DefaultRatesTransporter),MSG('Transporters Name'), |
  REQ,TIP('Transporters Name')
                           PROMPT('Default Fuel Surcharge Client:'),AT(9,158),USE(?GeneralFuelSurchargeClient:Prompt), |
  TRN
                           BUTTON('...'),AT(105,158,12,10),USE(?CallLookup:Fuel_Client)
                           ENTRY(@s100),AT(121,158,215,10),USE(L_SG:GeneralFuelSurchargeClient),MSG('Client ID'),TIP('This clien' & |
  'ts Fuel Surcharge will be used for all Clients that have none specified')
                           PROMPT('Default Toll Rate Client:'),AT(9,181),USE(?L_SG:DefaultTollRateClient:Prompt),TRN
                           ENTRY(@s100),AT(121,180,215,10),USE(L_SG:DefaultTollRateClient),REQ,TIP('Client name')
                           BUTTON('...'),AT(105,180,12,10),USE(?CallLookup_TollClient)
                         END
                         TAB('&2) Journeys'),USE(?Tab:4)
                           LIST,AT(9,26,331,209),USE(?Browse:6),HVSCROLL,FORMAT('120L(2)|M~Journey~@s70@20R(2)|M~B' & |
  'ID~C(0)@n_10@'),FROM(Queue:Browse:6),IMM,MSG('Browsing Records')
                         END
                         TAB('Invoices'),USE(?Tab:2),HIDE
                           LIST,AT(12,27,326,207),USE(?Browse:2),HVSCROLL,FORMAT('42R(2)|M~Invoice No.~C(0)@n_10@4' & |
  '0R(2)|M~BID~C(0)@n_10@40R(2)|M~CID~C(0)@n_10@80L(2)|M~Name~@s35@80L(2)|M~Shipper Lin' & |
  'e 1~@s35@80L(2)|M~Shipper Line 2~@s35@80L(2)|M~Shipper Suburb~@s50@80L(2)|M~Shipper ' & |
  'Postal Code~@s10@80L(2)|M~Consignee Name~@s35@'),FROM(Queue:Browse:2),IMM,MSG('Browsing Records')
                         END
                         TAB('TripSheets'),USE(?Tab:3),HIDE
                           LIST,AT(7,25,336,212),USE(?Browse:4),HVSCROLL,FORMAT('40R(2)|M~BID~C(0)@n_10@48R(2)|M~D' & |
  'epart Date~C(0)@d6@46R(2)|M~Depart Time~C(0)@t7@52R(2)|M~Returned Date~C(0)@d6@52R(2' & |
  ')|M~Returned Time~C(0)@t7@80L(2)|M~Notes~@s255@'),FROM(Queue:Browse:4),IMM,MSG('Browsing Records')
                         END
                         TAB('Transporter'),USE(?Tab:5)
                           LIST,AT(7,25,336,212),USE(?Browse:8),HVSCROLL,FORMAT('80L(2)|M~Transporter Name~@s35@80' & |
  'L(2)|M~Ops Manager~@s35@80L(2)|M~VAT No.~@s20@30R(2)|M~BID~C(0)@n_10@30R(2)|M~ACID~C' & |
  '(0)@n_10@30R(2)|M~TID~C(0)@n_10@'),FROM(Queue:Browse:8),IMM,MSG('Browsing Records')
                         END
                         TAB('Manifest'),USE(?Tab:6),HIDE
                           LIST,AT(7,22,336,218),USE(?Browse:10),HVSCROLL,FORMAT('30R(2)|M~BID~C(0)@n_10@30R(2)|M~' & |
  'TID~C(0)@n_10@52R(1)|M~Cost~C(0)@n-14.2@38R(1)|M~Rate~C(0)@n-10.2@36R(1)|M~VAT Rate~' & |
  'C(0)@n-7.2@24R(2)|M~State~C(0)@n3@44R(2)|M~Depart Date~C(0)@d6@44R(2)|M~Depart Time~' & |
  'C(0)@t7@44R(2)|M~ETA Date~C(0)@d6@'),FROM(Queue:Browse:10),IMM,MSG('Browsing Records')
                         END
                         TAB('Clients'),USE(?Tab:7),HIDE
                           LIST,AT(7,22,336,217),USE(?Browse:12),HVSCROLL,FORMAT('80L(2)|M~Client Name~@s35@30R(2)' & |
  '|M~BID~C(0)@n_10@62R(2)|M~Generate Invoice~C(0)@n3@64R(1)|M~Minimium Charge~C(0)@n-1' & |
  '1.2@52R(1)|M~Rate~C(0)@n-11.2@64R(1)|M~Document Charge~C(0)@n-11.2@80R(2)|M~ACID~C(0' & |
  ')@n_10@30R(2)|M~CID~C(0)@n_10@'),FROM(Queue:Browse:12),IMM,MSG('Browsing Records')
                         END
                         TAB('Deliveries'),USE(?Tab:8),HIDE
                           LIST,AT(7,22,336,217),USE(?Browse:14),HVSCROLL,FORMAT('40R(2)|M~DI No.~C(0)@n_10@30R(2)' & |
  '|M~BID~C(0)@n_10@30R(2)|M~CID~C(0)@n_10@80L(2)|M~Client Reference~@s30@52D(24)|M~Rat' & |
  'e~C(0)@n-11.2@64D(12)|M~Document Charge~C(0)@n-11.2@60D(12)|M~Fuel Surcharge~C(0)@n-' & |
  '11.2@60D(24)|M~Charge~C(0)@n-13.2@'),FROM(Queue:Browse:14),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('OK'),AT(253,246,45,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT
                       BUTTON('Cancel'),AT(303,246,45,14),USE(?Cancel),LEFT,ICON('WAcancel.ICO'),FLAT
                       BUTTON('Help'),AT(4,246,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
                     END

BRW4::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW6                 CLASS(BrowseClass)                    ! Browse using ?Browse:6
Q                      &Queue:Browse:6                !Reference to browse queue
                     END

BRW6::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW8                 CLASS(BrowseClass)                    ! Browse using ?Browse:8
Q                      &Queue:Browse:8                !Reference to browse queue
                     END

BRW8::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW10                CLASS(BrowseClass)                    ! Browse using ?Browse:10
Q                      &Queue:Browse:10               !Reference to browse queue
                     END

BRW10::Sort0:StepClass StepRealClass                       ! Default Step Manager
BRW12                CLASS(BrowseClass)                    ! Browse using ?Browse:12
Q                      &Queue:Browse:12               !Reference to browse queue
                     END

BRW12::Sort0:StepClass StepRealClass                       ! Default Step Manager
BRW14                CLASS(BrowseClass)                    ! Browse using ?Browse:14
Q                      &Queue:Browse:14               !Reference to browse queue
                     END

BRW14::Sort0:StepClass StepRealClass                       ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB3                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB5                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Branch Record'
  OF InsertRecord
    ActionMessage = 'Branch Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Branch Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Branches')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?BRA:BranchName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(BRA:Record,History::BRA:Record)
  SELF.AddHistoryField(?BRA:BranchName,2)
  SELF.AddUpdateFile(Access:Branches)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Access:Branches.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Branches
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:_Invoice,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:TripSheets,SELF) ! Initialize the browse manager
  BRW6.Init(?Browse:6,Queue:Browse:6.ViewPosition,BRW6::View:Browse,Queue:Browse:6,Relate:Journeys,SELF) ! Initialize the browse manager
  BRW8.Init(?Browse:8,Queue:Browse:8.ViewPosition,BRW8::View:Browse,Queue:Browse:8,Relate:Transporter,SELF) ! Initialize the browse manager
  BRW10.Init(?Browse:10,Queue:Browse:10.ViewPosition,BRW10::View:Browse,Queue:Browse:10,Relate:Manifest,SELF) ! Initialize the browse manager
  BRW12.Init(?Browse:12,Queue:Browse:12.ViewPosition,BRW12::View:Browse,Queue:Browse:12,Relate:Clients,SELF) ! Initialize the browse manager
  BRW14.Init(?Browse:14,Queue:Browse:14.ViewPosition,BRW14::View:Browse,Queue:Browse:14,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon INV:IID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,INV:PKey_IID)    ! Add the sort order for INV:PKey_IID for sort order 1
  BRW2.AddRange(INV:IID,Relate:_Invoice,Relate:Branches)   ! Add file relationship range limit for sort order 1
  BRW2.AddField(INV:IID,BRW2.Q.INV:IID)                    ! Field INV:IID is a hot field or requires assignment from browse
  BRW2.AddField(INV:BID,BRW2.Q.INV:BID)                    ! Field INV:BID is a hot field or requires assignment from browse
  BRW2.AddField(INV:CID,BRW2.Q.INV:CID)                    ! Field INV:CID is a hot field or requires assignment from browse
  BRW2.AddField(INV:ShipperName,BRW2.Q.INV:ShipperName)    ! Field INV:ShipperName is a hot field or requires assignment from browse
  BRW2.AddField(INV:ShipperLine1,BRW2.Q.INV:ShipperLine1)  ! Field INV:ShipperLine1 is a hot field or requires assignment from browse
  BRW2.AddField(INV:ShipperLine2,BRW2.Q.INV:ShipperLine2)  ! Field INV:ShipperLine2 is a hot field or requires assignment from browse
  BRW2.AddField(INV:ShipperSuburb,BRW2.Q.INV:ShipperSuburb) ! Field INV:ShipperSuburb is a hot field or requires assignment from browse
  BRW2.AddField(INV:ShipperPostalCode,BRW2.Q.INV:ShipperPostalCode) ! Field INV:ShipperPostalCode is a hot field or requires assignment from browse
  BRW2.AddField(INV:ConsigneeName,BRW2.Q.INV:ConsigneeName) ! Field INV:ConsigneeName is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon TRI:BID for sort order 1
  BRW4.AddSortOrder(BRW4::Sort0:StepClass,TRI:FKey_BID)    ! Add the sort order for TRI:FKey_BID for sort order 1
  BRW4.AddRange(TRI:BID,Relate:TripSheets,Relate:Branches) ! Add file relationship range limit for sort order 1
  BRW4.AddField(TRI:BID,BRW4.Q.TRI:BID)                    ! Field TRI:BID is a hot field or requires assignment from browse
  BRW4.AddField(TRI:DepartDate,BRW4.Q.TRI:DepartDate)      ! Field TRI:DepartDate is a hot field or requires assignment from browse
  BRW4.AddField(TRI:DepartTime,BRW4.Q.TRI:DepartTime)      ! Field TRI:DepartTime is a hot field or requires assignment from browse
  BRW4.AddField(TRI:ReturnedDate,BRW4.Q.TRI:ReturnedDate)  ! Field TRI:ReturnedDate is a hot field or requires assignment from browse
  BRW4.AddField(TRI:ReturnedTime,BRW4.Q.TRI:ReturnedTime)  ! Field TRI:ReturnedTime is a hot field or requires assignment from browse
  BRW4.AddField(TRI:Notes,BRW4.Q.TRI:Notes)                ! Field TRI:Notes is a hot field or requires assignment from browse
  BRW4.AddField(TRI:TRID,BRW4.Q.TRI:TRID)                  ! Field TRI:TRID is a hot field or requires assignment from browse
  BRW6.Q &= Queue:Browse:6
  BRW6::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon JOU:BID for sort order 1
  BRW6.AddSortOrder(BRW6::Sort0:StepClass,JOU:FKey_BID)    ! Add the sort order for JOU:FKey_BID for sort order 1
  BRW6.AddRange(JOU:BID,Relate:Journeys,Relate:Branches)   ! Add file relationship range limit for sort order 1
  BRW6.AddField(JOU:Journey,BRW6.Q.JOU:Journey)            ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW6.AddField(JOU:BID,BRW6.Q.JOU:BID)                    ! Field JOU:BID is a hot field or requires assignment from browse
  BRW6.AddField(JOU:JID,BRW6.Q.JOU:JID)                    ! Field JOU:JID is a hot field or requires assignment from browse
  BRW8.Q &= Queue:Browse:8
  BRW8::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon TRA:BID for sort order 1
  BRW8.AddSortOrder(BRW8::Sort0:StepClass,TRA:FKey_BID)    ! Add the sort order for TRA:FKey_BID for sort order 1
  BRW8.AddRange(TRA:BID,Relate:Transporter,Relate:Branches) ! Add file relationship range limit for sort order 1
  BRW8.AddField(TRA:TransporterName,BRW8.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW8.AddField(TRA:OpsManager,BRW8.Q.TRA:OpsManager)      ! Field TRA:OpsManager is a hot field or requires assignment from browse
  BRW8.AddField(TRA:VATNo,BRW8.Q.TRA:VATNo)                ! Field TRA:VATNo is a hot field or requires assignment from browse
  BRW8.AddField(TRA:BID,BRW8.Q.TRA:BID)                    ! Field TRA:BID is a hot field or requires assignment from browse
  BRW8.AddField(TRA:ACID,BRW8.Q.TRA:ACID)                  ! Field TRA:ACID is a hot field or requires assignment from browse
  BRW8.AddField(TRA:TID,BRW8.Q.TRA:TID)                    ! Field TRA:TID is a hot field or requires assignment from browse
  BRW10.Q &= Queue:Browse:10
  BRW10::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)      ! Moveable thumb based upon MAN:BID for sort order 1
  BRW10.AddSortOrder(BRW10::Sort0:StepClass,MAN:FKey_BID)  ! Add the sort order for MAN:FKey_BID for sort order 1
  BRW10.AddRange(MAN:BID,Relate:Manifest,Relate:Branches)  ! Add file relationship range limit for sort order 1
  BRW10.AddField(MAN:BID,BRW10.Q.MAN:BID)                  ! Field MAN:BID is a hot field or requires assignment from browse
  BRW10.AddField(MAN:TID,BRW10.Q.MAN:TID)                  ! Field MAN:TID is a hot field or requires assignment from browse
  BRW10.AddField(MAN:Cost,BRW10.Q.MAN:Cost)                ! Field MAN:Cost is a hot field or requires assignment from browse
  BRW10.AddField(MAN:Rate,BRW10.Q.MAN:Rate)                ! Field MAN:Rate is a hot field or requires assignment from browse
  BRW10.AddField(MAN:VATRate,BRW10.Q.MAN:VATRate)          ! Field MAN:VATRate is a hot field or requires assignment from browse
  BRW10.AddField(MAN:State,BRW10.Q.MAN:State)              ! Field MAN:State is a hot field or requires assignment from browse
  BRW10.AddField(MAN:DepartDate,BRW10.Q.MAN:DepartDate)    ! Field MAN:DepartDate is a hot field or requires assignment from browse
  BRW10.AddField(MAN:DepartTime,BRW10.Q.MAN:DepartTime)    ! Field MAN:DepartTime is a hot field or requires assignment from browse
  BRW10.AddField(MAN:ETADate,BRW10.Q.MAN:ETADate)          ! Field MAN:ETADate is a hot field or requires assignment from browse
  BRW10.AddField(MAN:MID,BRW10.Q.MAN:MID)                  ! Field MAN:MID is a hot field or requires assignment from browse
  BRW12.Q &= Queue:Browse:12
  BRW12::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)      ! Moveable thumb based upon CLI:BID for sort order 1
  BRW12.AddSortOrder(BRW12::Sort0:StepClass,CLI:FKey_BID)  ! Add the sort order for CLI:FKey_BID for sort order 1
  BRW12.AddRange(CLI:BID,Relate:Clients,Relate:Branches)   ! Add file relationship range limit for sort order 1
  BRW12.AddField(CLI:ClientName,BRW12.Q.CLI:ClientName)    ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW12.AddField(CLI:BID,BRW12.Q.CLI:BID)                  ! Field CLI:BID is a hot field or requires assignment from browse
  BRW12.AddField(CLI:GenerateInvoice,BRW12.Q.CLI:GenerateInvoice) ! Field CLI:GenerateInvoice is a hot field or requires assignment from browse
  BRW12.AddField(CLI:MinimiumCharge,BRW12.Q.CLI:MinimiumCharge) ! Field CLI:MinimiumCharge is a hot field or requires assignment from browse
  BRW12.AddField(CLI:Rate,BRW12.Q.CLI:Rate)                ! Field CLI:Rate is a hot field or requires assignment from browse
  BRW12.AddField(CLI:DocumentCharge,BRW12.Q.CLI:DocumentCharge) ! Field CLI:DocumentCharge is a hot field or requires assignment from browse
  BRW12.AddField(CLI:ACID,BRW12.Q.CLI:ACID)                ! Field CLI:ACID is a hot field or requires assignment from browse
  BRW12.AddField(CLI:CID,BRW12.Q.CLI:CID)                  ! Field CLI:CID is a hot field or requires assignment from browse
  BRW14.Q &= Queue:Browse:14
  BRW14::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)      ! Moveable thumb based upon DEL:BID for sort order 1
  BRW14.AddSortOrder(BRW14::Sort0:StepClass,DEL:FKey_BID)  ! Add the sort order for DEL:FKey_BID for sort order 1
  BRW14.AddRange(DEL:BID,Relate:Deliveries,Relate:Branches) ! Add file relationship range limit for sort order 1
  BRW14.AddField(DEL:DINo,BRW14.Q.DEL:DINo)                ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW14.AddField(DEL:BID,BRW14.Q.DEL:BID)                  ! Field DEL:BID is a hot field or requires assignment from browse
  BRW14.AddField(DEL:CID,BRW14.Q.DEL:CID)                  ! Field DEL:CID is a hot field or requires assignment from browse
  BRW14.AddField(DEL:ClientReference,BRW14.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW14.AddField(DEL:Rate,BRW14.Q.DEL:Rate)                ! Field DEL:Rate is a hot field or requires assignment from browse
  BRW14.AddField(DEL:DocumentCharge,BRW14.Q.DEL:DocumentCharge) ! Field DEL:DocumentCharge is a hot field or requires assignment from browse
  BRW14.AddField(DEL:FuelSurcharge,BRW14.Q.DEL:FuelSurcharge) ! Field DEL:FuelSurcharge is a hot field or requires assignment from browse
  BRW14.AddField(DEL:Charge,BRW14.Q.DEL:Charge)            ! Field DEL:Charge is a hot field or requires assignment from browse
  BRW14.AddField(DEL:DID,BRW14.Q.DEL:DID)                  ! Field DEL:DID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Branches',QuickWindow)              ! Restore window settings from non-volatile store
      CLI:CID                     = BRA:GeneralRatesClientID
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         L_SG:DefaultRatesClient  = CLI:ClientName
      .
  
      TRA:TID                     = BRA:GeneralRatesTransporterID
      IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
         L_SG:DefaultRatesTransporter  = TRA:TransporterName
      .
  
      CLI:CID                     = BRA:GeneralFuelSurchargeClientID
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         L_SG:GeneralFuelSurchargeClient  = CLI:ClientName
      .
  
      CLI:CID                     = BRA:GeneralTollChargeClientID
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         L_SG:DefaultTollRateClient  = CLI:ClientName
      .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  FDB3.Init(?ADD:AddressName,Queue:FileDrop.ViewPosition,FDB3::View:FileDrop,Queue:FileDrop,Relate:Addresses,ThisWindow)
  FDB3.Q &= Queue:FileDrop
  FDB3.AddSortOrder(ADD:Key_Name)
  FDB3.AddField(ADD:AddressName,FDB3.Q.ADD:AddressName) !List box control field - type derived from field
  FDB3.AddField(ADD:AID,FDB3.Q.ADD:AID) !Primary key field - type derived from field
  FDB3.AddUpdateField(ADD:AID,BRA:AID)
  ThisWindow.AddItem(FDB3.WindowComponent)
  FDB3.DefaultFill = 0
  FDB5.Init(?FLO:Floor,Queue:FileDrop:1.ViewPosition,FDB5::View:FileDrop,Queue:FileDrop:1,Relate:Floors,ThisWindow)
  FDB5.Q &= Queue:FileDrop:1
  FDB5.AddSortOrder(FLO:Key_Floor)
  FDB5.AddField(FLO:Floor,FDB5.Q.FLO:Floor) !List box control field - type derived from field
  FDB5.AddField(FLO:FID,FDB5.Q.FLO:FID) !Primary key field - type derived from field
  FDB5.AddUpdateField(FLO:FID,BRA:FID)
  ThisWindow.AddItem(FDB5.WindowComponent)
  FDB5.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Branches',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_Clients
      Select_Transporter
      Select_Clients
      Select_Clients
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup_DefaultRateClient
      ThisWindow.Update()
      CLI:ClientName = L_SG:DefaultRatesClient
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SG:DefaultRatesClient = CLI:ClientName
        BRA:GeneralRatesClientID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?L_SG:DefaultRatesClient
      IF L_SG:DefaultRatesClient OR ?L_SG:DefaultRatesClient{PROP:Req}
        CLI:ClientName = L_SG:DefaultRatesClient
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SG:DefaultRatesClient = CLI:ClientName
            BRA:GeneralRatesClientID = CLI:CID
          ELSE
            CLEAR(BRA:GeneralRatesClientID)
            SELECT(?L_SG:DefaultRatesClient)
            CYCLE
          END
        ELSE
          BRA:GeneralRatesClientID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:Transporter
      ThisWindow.Update()
      TRA:TransporterName = L_SG:DefaultRatesTransporter
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_SG:DefaultRatesTransporter = TRA:TransporterName
        BRA:GeneralRatesTransporterID = TRA:TID
      END
      ThisWindow.Reset(1)
    OF ?L_SG:DefaultRatesTransporter
      IF L_SG:DefaultRatesTransporter OR ?L_SG:DefaultRatesTransporter{PROP:Req}
        TRA:TransporterName = L_SG:DefaultRatesTransporter
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            L_SG:DefaultRatesTransporter = TRA:TransporterName
            BRA:GeneralRatesTransporterID = TRA:TID
          ELSE
            CLEAR(BRA:GeneralRatesTransporterID)
            SELECT(?L_SG:DefaultRatesTransporter)
            CYCLE
          END
        ELSE
          BRA:GeneralRatesTransporterID = TRA:TID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:Fuel_Client
      ThisWindow.Update()
      CLI:ClientName = L_SG:GeneralFuelSurchargeClient
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SG:GeneralFuelSurchargeClient = CLI:ClientName
        BRA:GeneralFuelSurchargeClientID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?L_SG:GeneralFuelSurchargeClient
      IF L_SG:GeneralFuelSurchargeClient OR ?L_SG:GeneralFuelSurchargeClient{PROP:Req}
        CLI:ClientName = L_SG:GeneralFuelSurchargeClient
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SG:GeneralFuelSurchargeClient = CLI:ClientName
            BRA:GeneralFuelSurchargeClientID = CLI:CID
          ELSE
            CLEAR(BRA:GeneralFuelSurchargeClientID)
            SELECT(?L_SG:GeneralFuelSurchargeClient)
            CYCLE
          END
        ELSE
          BRA:GeneralFuelSurchargeClientID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?L_SG:DefaultTollRateClient
      IF L_SG:DefaultTollRateClient OR ?L_SG:DefaultTollRateClient{PROP:Req}
        CLI:ClientName = L_SG:DefaultTollRateClient
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SG:DefaultTollRateClient = CLI:ClientName
            BRA:GeneralTollChargeClientID = CLI:CID
          ELSE
            CLEAR(BRA:GeneralTollChargeClientID)
            SELECT(?L_SG:DefaultTollRateClient)
            CYCLE
          END
        ELSE
          BRA:GeneralTollChargeClientID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_TollClient
      ThisWindow.Update()
      CLI:ClientName = L_SG:DefaultTollRateClient
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SG:DefaultTollRateClient = CLI:ClientName
        BRA:GeneralTollChargeClientID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

