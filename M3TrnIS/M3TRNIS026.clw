

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS026.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Reminders PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:Q_Vars           GROUP,PRE()                           !
QV:ReminderType      STRING(20)                            !General, Client
QV:RemindOption      STRING(20)                            !All, Group & User, User, Group
                     END                                   !
LOC:UID              ULONG                                 !User ID
LOC:UGID             ULONG                                 !User Group ID
BRW1::View:Browse    VIEW(Reminders)
                       PROJECT(REM:Active)
                       PROJECT(REM:Popup)
                       PROJECT(REM:ReminderDate)
                       PROJECT(REM:ReminderTime)
                       PROJECT(REM:Notes)
                       PROJECT(REM:ID)
                       PROJECT(REM:RID)
                       PROJECT(REM:UGID)
                       PROJECT(REM:ReminderType)
                       PROJECT(REM:RemindOption)
                       PROJECT(REM:UID)
                       JOIN(USEG:PKey_UGID,REM:UGID)
                         PROJECT(USEG:GroupName)
                         PROJECT(USEG:UGID)
                       END
                       JOIN(USE:PKey_UID,REM:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:UID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
QV:ReminderType        LIKE(QV:ReminderType)          !List box control field - type derived from local data
REM:Active             LIKE(REM:Active)               !List box control field - type derived from field
REM:Active_Icon        LONG                           !Entry's icon ID
REM:Popup              LIKE(REM:Popup)                !List box control field - type derived from field
REM:Popup_Icon         LONG                           !Entry's icon ID
REM:ReminderDate       LIKE(REM:ReminderDate)         !List box control field - type derived from field
REM:ReminderTime       LIKE(REM:ReminderTime)         !List box control field - type derived from field
QV:RemindOption        LIKE(QV:RemindOption)          !List box control field - type derived from local data
REM:Notes              LIKE(REM:Notes)                !List box control field - type derived from field
REM:ID                 LIKE(REM:ID)                   !List box control field - type derived from field
REM:RID                LIKE(REM:RID)                  !List box control field - type derived from field
REM:UGID               LIKE(REM:UGID)                 !List box control field - type derived from field
REM:ReminderType       LIKE(REM:ReminderType)         !Browse hot field - type derived from field
REM:RemindOption       LIKE(REM:RemindOption)         !Browse hot field - type derived from field
REM:UID                LIKE(REM:UID)                  !Browse key field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Related join file key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Reminders File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('Browse_Reminders'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('44L(2)|M~User Login~C(0)@s20@48L(' & |
  '2)|M~Group Name~C(0)@s35@54L(2)|M~Reminder Type~C(0)@s20@28R(2)|MI~Active~C(0)@p p@2' & |
  '4R(2)|MI~Popup~C(0)@p p@[40R(2)|M~Date~C(0)@d5@40R(2)|M~Time~C(0)@t7@]|M~Reminder~52' & |
  'L(2)|M~Remind Option~C(0)@s20@150L(2)|M~Notes~C(0)@s255@40R(2)|M~ID~C(0)@n_10@40R(2)' & |
  '|M~RID~C(0)@n_10@40R(2)|M~UGID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he Reminders file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By User'),USE(?Tab:2)
                           BUTTON('Select User'),AT(9,158,,14),USE(?SelectUsers),LEFT,ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                         END
                         TAB('&2) By Type && ID'),USE(?Tab:4)
                         END
                         TAB('&3) By Reminder'),USE(?Tab:5)
                         END
                         TAB('&4) By User Group'),USE(?Tab:6)
                           BUTTON('Select User Group'),AT(9,158,,14),USE(?SelectUserGroups),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&5) By Date && Time'),USE(?Tab5)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Reminders')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('QV:ReminderType',QV:ReminderType)                  ! Added by: BrowseBox(ABC)
  BIND('QV:RemindOption',QV:RemindOption)                  ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:UserGroups.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
      ! Load GLO:User
      USE:UID = GLO:UID
      IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
      .
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Reminders,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,REM:SKey_Type_ID)                     ! Add the sort order for REM:SKey_Type_ID for sort order 1
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort2:Locator.Init(,REM:ReminderType,1,BRW1)       ! Initialize the browse locator using  using key: REM:SKey_Type_ID , REM:ReminderType
  BRW1.AppendOrder('-REM:ReminderDate,+REM:RID')           ! Append an additional sort order
  BRW1.AddSortOrder(,REM:PKey_RID)                         ! Add the sort order for REM:PKey_RID for sort order 2
  BRW1.AddSortOrder(,REM:FKey_UGID)                        ! Add the sort order for REM:FKey_UGID for sort order 3
  BRW1.AddRange(REM:UGID,Relate:Reminders,Relate:UserGroups) ! Add file relationship range limit for sort order 3
  BRW1.AppendOrder('-REM:ReminderDate,+REM:RID')           ! Append an additional sort order
  BRW1.AddSortOrder(,)                                     ! Add the sort order for  for sort order 4
  BRW1.AppendOrder('-REM:ReminderDate,+REM:RID')           ! Append an additional sort order
  BRW1.AddSortOrder(,REM:FKey_UID)                         ! Add the sort order for REM:FKey_UID for sort order 5
  BRW1.AddRange(REM:UID,Relate:Reminders,Relate:Users)     ! Add file relationship range limit for sort order 5
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 5
  BRW1::Sort0:Locator.Init(,REM:UID,1,BRW1)                ! Initialize the browse locator using  using key: REM:FKey_UID , REM:UID
  BRW1.AppendOrder('-REM:ReminderDate,+REM:RID')           ! Append an additional sort order
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(USE:Login,BRW1.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW1.AddField(USEG:GroupName,BRW1.Q.USEG:GroupName)      ! Field USEG:GroupName is a hot field or requires assignment from browse
  BRW1.AddField(QV:ReminderType,BRW1.Q.QV:ReminderType)    ! Field QV:ReminderType is a hot field or requires assignment from browse
  BRW1.AddField(REM:Active,BRW1.Q.REM:Active)              ! Field REM:Active is a hot field or requires assignment from browse
  BRW1.AddField(REM:Popup,BRW1.Q.REM:Popup)                ! Field REM:Popup is a hot field or requires assignment from browse
  BRW1.AddField(REM:ReminderDate,BRW1.Q.REM:ReminderDate)  ! Field REM:ReminderDate is a hot field or requires assignment from browse
  BRW1.AddField(REM:ReminderTime,BRW1.Q.REM:ReminderTime)  ! Field REM:ReminderTime is a hot field or requires assignment from browse
  BRW1.AddField(QV:RemindOption,BRW1.Q.QV:RemindOption)    ! Field QV:RemindOption is a hot field or requires assignment from browse
  BRW1.AddField(REM:Notes,BRW1.Q.REM:Notes)                ! Field REM:Notes is a hot field or requires assignment from browse
  BRW1.AddField(REM:ID,BRW1.Q.REM:ID)                      ! Field REM:ID is a hot field or requires assignment from browse
  BRW1.AddField(REM:RID,BRW1.Q.REM:RID)                    ! Field REM:RID is a hot field or requires assignment from browse
  BRW1.AddField(REM:UGID,BRW1.Q.REM:UGID)                  ! Field REM:UGID is a hot field or requires assignment from browse
  BRW1.AddField(REM:ReminderType,BRW1.Q.REM:ReminderType)  ! Field REM:ReminderType is a hot field or requires assignment from browse
  BRW1.AddField(REM:RemindOption,BRW1.Q.REM:RemindOption)  ! Field REM:RemindOption is a hot field or requires assignment from browse
  BRW1.AddField(REM:UID,BRW1.Q.REM:UID)                    ! Field REM:UID is a hot field or requires assignment from browse
  BRW1.AddField(USEG:UGID,BRW1.Q.USEG:UGID)                ! Field USEG:UGID is a hot field or requires assignment from browse
  BRW1.AddField(USE:UID,BRW1.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Reminders',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Reminders',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Reminders
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectUsers
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Users()
      ThisWindow.Reset
          LOC:UID = USE:UID
    OF ?SelectUserGroups
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_UserGroups()
      ThisWindow.Reset
          LOC:UGID    = USEG:UGID
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! General|Client
      EXECUTE REM:ReminderType + 1
         QV:ReminderType  = 'General'
         QV:ReminderType  = 'Client'
         QV:ReminderType  = 'Truck/Trailers'
      .
  
  
      ! All|Group & User|User|Group
      EXECUTE REM:RemindOption + 1
         QV:RemindOption  = 'All'
         QV:RemindOption  = 'Group & User'
         QV:RemindOption  = 'User'
         QV:RemindOption  = 'Group'
      .
  PARENT.SetQueueRecord
  
  IF (REM:Active = 1)
    SELF.Q.REM:Active_Icon = 2                             ! Set icon from icon list
  ELSE
    SELF.Q.REM:Active_Icon = 1                             ! Set icon from icon list
  END
  IF (REM:Popup = 1)
    SELF.Q.REM:Popup_Icon = 2                              ! Set icon from icon list
  ELSE
    SELF.Q.REM:Popup_Icon = 1                              ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

