

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS022.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Prints PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Add_Group        GROUP,PRE(L_AG)                       !
Idx                  ULONG                                 !
FieldName            STRING(50)                            !Field Name
                     END                                   !
LOC:Alignment        STRING(20)                            !Alignment
BRW2::View:Browse    VIEW(PrintLayout)
                       PROJECT(PRIL:XPos)
                       PROJECT(PRIL:YPos)
                       PROJECT(PRIL:Length)
                       PROJECT(PRIL:Repeating)
                       PROJECT(PRIL:PLID)
                       PROJECT(PRIL:Alignment)
                       PROJECT(PRIL:PID)
                       PROJECT(PRIL:PFID)
                       JOIN(PRIF:PKey_PFID,PRIL:PFID)
                         PROJECT(PRIF:FieldName)
                         PROJECT(PRIF:PFID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
PRIF:FieldName         LIKE(PRIF:FieldName)           !List box control field - type derived from field
PRIL:XPos              LIKE(PRIL:XPos)                !List box control field - type derived from field
PRIL:YPos              LIKE(PRIL:YPos)                !List box control field - type derived from field
PRIL:Length            LIKE(PRIL:Length)              !List box control field - type derived from field
LOC:Alignment          LIKE(LOC:Alignment)            !List box control field - type derived from local data
PRIL:Repeating         LIKE(PRIL:Repeating)           !List box control field - type derived from field
PRIL:Repeating_Icon    LONG                           !Entry's icon ID
PRIL:PLID              LIKE(PRIL:PLID)                !List box control field - type derived from field
PRIL:Alignment         LIKE(PRIL:Alignment)           !Browse hot field - type derived from field
PRIL:PID               LIKE(PRIL:PID)                 !Browse key field - type derived from field
PRIF:PFID              LIKE(PRIF:PFID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::PRI:Record  LIKE(PRI:RECORD),THREAD
QuickWindow          WINDOW('Form Prints'),AT(,,273,256),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('UpdatePrints'),SYSTEM
                       SHEET,AT(4,4,265,232),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Print Name:'),AT(9,20),USE(?PRI:PrintName:Prompt),TRN
                           ENTRY(@s50),AT(61,20,204,10),USE(PRI:PrintName),MSG('Name'),TIP('Name')
                           PROMPT('Print Type:'),AT(9,48),USE(?PRI:PrintType:Prompt),TRN
                           LIST,AT(61,48,100,10),USE(PRI:PrintType),DROP(5),FROM('Invoices|#0|Credit Notes|#1|Deli' & |
  'very Notes|#2|Statements|#3')
                           PROMPT('Page Lines:'),AT(9,78),USE(?PRI:PageLines:Prompt),TRN
                           SPIN(@n_5),AT(61,78,42,10),USE(PRI:PageLines),RIGHT(1),MSG('Lines on this page'),TIP('Lines on this page')
                           STRING('eg. Invoices are 48 lines, Statements are 65'),AT(109,78,158,10),USE(?String1),TRN
                           PROMPT('Page Columns:'),AT(9,96),USE(?PRI:PageColumns:Prompt),TRN
                           SPIN(@n7),AT(61,96,42,10),USE(PRI:PageColumns),RIGHT(1),MSG('Columns on a page'),TIP('Columns on a page')
                           STRING('eg. Invoices and Statements are 80'),AT(109,96,158,10),USE(?String1:2),TRN
                         END
                         TAB('&2) Print Layout'),USE(?Tab:2)
                           LIST,AT(9,20,257,195),USE(?Browse:2),HVSCROLL,FORMAT('100L(2)|M~Field Name~C(0)@s50@28R' & |
  '(2)|M~X Pos~C(0)@n_5@28R(2)|M~Y Pos~C(0)@n_5@28R(2)|M~Length~C(0)@n_5@38L(2)|M~Align' & |
  'ment~C(0)@s20@20L(1)|MI~Repeating~@n3@30R(2)|M~PLID~C(0)@n_10@'),FROM(Queue:Browse:2),IMM, |
  MSG('Browsing the PrintLayout file')
                           BUTTON('Create Fields / Layout'),AT(9,218,,14),USE(?Button_Create),TIP('Options for Fie' & |
  'lds or Fields & Layout')
                           BUTTON('&Insert'),AT(110,218,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(162,218,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(217,218,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(168,238,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(220,238,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(92,238,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('Designer'),AT(4,238,,14),USE(?Button_Designer),LEFT,ICON('CHANGEPG.ICO'),FLAT
                     END

BRW2::LastSortOrder       BYTE
BRW2::SortHeader  CLASS(SortHeaderClassType) !Declare SortHeader Class
QueueResorted          PROCEDURE(STRING pString),VIRTUAL
                  END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Create_Fields_Layout            ROUTINE
    ! Invoices|Credit Notes|Delivery Notes
    CASE MESSAGE('Create the Fields or the Fields & Layout for this Print?', 'Option', ICON:Question, 'Fields|Both', 1)
    OF 1
       DO Add_Print_Fields
    OF 2
       DO Add_Print_Fields
       DO Add_Layout
    .

    BRW2.ResetFromFile()
    EXIT
Add_Print_Fields                    ROUTINE
    LOGOUT(1, PrintFields)

    L_AG:Idx    = 0
    LOOP
       L_AG:Idx += 1

       CLEAR(PRIF:Record)
       IF Access:PrintFields.TryPrimeAutoInc() = LEVEL:Benign
          PRIF:PrintType   = PRI:PrintType
          EXECUTE PRI:PrintType                 ! Invoices, Credit Notes, Delivery Notes, Statements
             DO Add_CNote_Fields
             DO Add_DelN_Fields
             DO Add_Statement_Fields
          ELSE
             DO Add_Invoice_Fields
          .

          IF CLIP(PRIF:FieldName) = ''
             Access:PrintFields.CancelAutoInc()
             BREAK
          .

          IF Access:PrintFields.TryInsert() = LEVEL:Benign
             !
          ELSE
             Access:PrintFields.CancelAutoInc()
          .
       ELSE
          BREAK
    .  .

    COMMIT
    EXIT
Add_Layout                            ROUTINE
    LOGOUT(1, PrintLayout)

    L_AG:Idx        = 0
    LOOP
       L_AG:Idx    += 1

       ! Check if we already have this field or not
       CLEAR(PRIL:RECORD)
       EXECUTE PRI:PrintType                 ! Invoices, Credit Notes, Delivery Notes, Statements
          DO Add_CNote_Layout
          DO Add_DelN_Layout
          DO Add_Statement_Layout
       ELSE
          DO Add_Invoice_Layout
       .

       CLEAR(PRIF:Record)
       PRIF:FieldName   = L_AG:FieldName
       IF Access:PrintFields.TryFetch(PRIF:Key_FieldName) = LEVEL:Benign
          PRIL:PFID     = PRIF:PFID
       .

       PRIL:PID         = PRI:PID
       IF Access:PrintLayout.TryFetch(PRIL:SKey_Print_Field) = LEVEL:Benign
          ! We already have this Field in the layout...
          CYCLE
       .


       ! We dont have this field
       CLEAR(PRIL:RECORD)
       IF Access:PrintLayout.TryPrimeAutoInc() = LEVEL:Benign
          EXECUTE PRI:PrintType                 ! Invoices, Credit Notes, Delivery Notes, Statements
             DO Add_CNote_Layout
             DO Add_DelN_Layout
             DO Add_Statement_Layout
          ELSE
             DO Add_Invoice_Layout
          .

          IF CLIP(L_AG:FieldName) = ''
             Access:PrintLayout.CancelAutoInc()
             BREAK
          .

          PRIL:PID          = PRI:PID

          CLEAR(PRIF:Record)
          PRIF:FieldName    = L_AG:FieldName
          IF Access:PrintFields.TryFetch(PRIF:Key_FieldName) = LEVEL:Benign
             PRIL:PFID      = PRIF:PFID
          .

          IF Access:PrintLayout.TryInsert() = LEVEL:Benign
             !
          ELSE
             Access:PrintLayout.CancelAutoInc()
          .
       ELSE
          BREAK
    .  .
    COMMIT
    EXIT
! -----------------------------------------------------------------------------------------------------
Add_Invoice_Fields               ROUTINE
    EXECUTE L_AG:Idx
       PRIF:FieldName    = 'Invoice No.'
       PRIF:FieldName    = 'Branch'
       PRIF:FieldName    = 'Date'
       PRIF:FieldName    = 'For Account'
       PRIF:FieldName    = 'DI No.'
       PRIF:FieldName    = 'Manifest No.'
       PRIF:FieldName    = 'Shipper'
       PRIF:FieldName    = 'Ship Line 1'
       PRIF:FieldName    = 'Ship Line 2'
       PRIF:FieldName    = 'Ship Suburb'
       PRIF:FieldName    = 'Ship Post'
       PRIF:FieldName    = 'Consignee'
       PRIF:FieldName    = 'Con Line 1'
       PRIF:FieldName    = 'Con Line 2'
       PRIF:FieldName    = 'Con Suburb'
       PRIF:FieldName    = 'Con Post'
       PRIF:FieldName    = 'Items Start'
       PRIF:FieldName    = 'Items End'
       PRIF:FieldName    = 'Vol Weight'
       PRIF:FieldName    = 'Act Weight'
       PRIF:FieldName    = 'Debt Name'
       PRIF:FieldName    = 'Debt Line 1'
       PRIF:FieldName    = 'Debt Line 2'
       PRIF:FieldName    = 'Debt Suburb'
       PRIF:FieldName    = 'Debt Post'
       PRIF:FieldName    = 'Debt Vat No.'
       PRIF:FieldName    = 'Debt Ref.'
       PRIF:FieldName    = 'Charge Fuel'
       PRIF:FieldName    = 'Charge Insurance'
       PRIF:FieldName    = 'Charge Docs'
       PRIF:FieldName    = 'Charge Freight'
       PRIF:FieldName    = 'Charge VAT'
       PRIF:FieldName    = 'Charge Total'
       PRIF:FieldName    = 'Copy Invoice'
       PRIF:FieldName    = 'Fuel Surcharge'
       PRIF:FieldName    = 'Invoice Message'
    ELSE
       CLEAR(PRIF:FieldName)
    .
    EXIT
Add_Invoice_Layout              ROUTINE
    CASE L_AG:Idx
    OF 1
       L_AG:FieldName    = 'Invoice No.'
       PRIL:XPos         = 67
       PRIL:YPos         = 4
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 2
       L_AG:FieldName    = 'Branch'
       PRIL:XPos         = 64
       PRIL:YPos         = 5
       PRIL:Length       = 15
       PRIL:Alignment    = 1
    OF 3
       L_AG:FieldName    = 'Date'
       PRIL:XPos         = 4
       PRIL:YPos         = 11
       PRIL:Length       = 8
       PRIL:Alignment    = 1
    OF 4
       L_AG:FieldName    = 'For Account'
       PRIL:XPos         = 16
       PRIL:YPos         = 11
       PRIL:Length       = 24
       PRIL:Alignment    = 0
    OF 5
       L_AG:FieldName    = 'DI No.'
       PRIL:XPos         = 44
       PRIL:YPos         = 11
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 6
       L_AG:FieldName    = 'Manifest No.'
       PRIL:XPos         = 69
       PRIL:YPos         = 11
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 7
       L_AG:FieldName    = 'Shipper'
       PRIL:XPos         = 4
       PRIL:YPos         = 15
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 8
       L_AG:FieldName    = 'Ship Line 1'
       PRIL:XPos         = 4
       PRIL:YPos         = 16
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 9
       L_AG:FieldName    = 'Ship Line 2'
       PRIL:XPos         = 4
       PRIL:YPos         = 17
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 10
       L_AG:FieldName    = 'Ship Suburb'
       PRIL:XPos         = 4
       PRIL:YPos         = 18
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 11
       L_AG:FieldName    = 'Ship Post'
       PRIL:XPos         = 4
       PRIL:YPos         = 19
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 12
       L_AG:FieldName    = 'Consignee'
       PRIL:XPos         = 44
       PRIL:YPos         = 15
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 13
       L_AG:FieldName    = 'Con Line 1'
       PRIL:XPos         = 44
       PRIL:YPos         = 16
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 14
       L_AG:FieldName    = 'Con Line 2'
       PRIL:XPos         = 44
       PRIL:YPos         = 17
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 15
       L_AG:FieldName    = 'Con Suburb'
       PRIL:XPos         = 44
       PRIL:YPos         = 18
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 16
       L_AG:FieldName    = 'Con Post'
       PRIL:XPos         = 44
       PRIL:YPos         = 19
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 17
       L_AG:FieldName    = 'Items Start'
       PRIL:XPos         = 4
       PRIL:YPos         = 23
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 18
       L_AG:FieldName    = 'Items End'
       PRIL:XPos         = 4
       PRIL:YPos         = 31
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 19
       L_AG:FieldName    = 'Vol Weight'
       PRIL:XPos         = 43
       PRIL:YPos         = 32
       PRIL:Length       = 20
       PRIL:Alignment    = 2
    OF 20
       L_AG:FieldName    = 'Act Weight'
       PRIL:XPos         = 43
       PRIL:YPos         = 33
       PRIL:Length       = 20
       PRIL:Alignment    = 2
    OF 21
       L_AG:FieldName    = 'Debt Name'
       PRIL:XPos         = 4
       PRIL:YPos         = 37
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 22
       L_AG:FieldName    = 'Debt Line 1'
       PRIL:XPos         = 4
       PRIL:YPos         = 38
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 23
       L_AG:FieldName    = 'Debt Line 2'
       PRIL:XPos         = 4
       PRIL:YPos         = 39
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 24
       L_AG:FieldName    = 'Debt Suburb'
       PRIL:XPos         = 4
       PRIL:YPos         = 40
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 25
       L_AG:FieldName    = 'Debt Post'
       PRIL:XPos         = 4
       PRIL:YPos         = 41
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 26
       L_AG:FieldName    = 'Debt Vat No.'
       PRIL:XPos         = 16
       PRIL:YPos         = 12  !42
       PRIL:Length       = 19
       PRIL:Alignment    = 2
    OF 27
       L_AG:FieldName    = 'Debt Ref.'
       PRIL:XPos         = 10
       PRIL:YPos         = 42
       PRIL:Length       = 17
       PRIL:Alignment    = 2
    OF 28
       L_AG:FieldName    = 'Charge Fuel'
       PRIL:XPos         = 64
       PRIL:YPos         = 35
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 29
       L_AG:FieldName    = 'Charge Insurance'
       PRIL:XPos         = 64
       PRIL:YPos         = 37
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 30
       L_AG:FieldName    = 'Charge Docs'
       PRIL:XPos         = 64
       PRIL:YPos         = 39
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 31
       L_AG:FieldName    = 'Charge Freight'
       PRIL:XPos         = 64
       PRIL:YPos         = 41
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 32
       L_AG:FieldName    = 'Charge VAT'
       PRIL:XPos         = 64
       PRIL:YPos         = 43
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 33
       L_AG:FieldName    = 'Charge Total'
       PRIL:XPos         = 64
       PRIL:YPos         = 45
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 34
       L_AG:FieldName    = 'Copy Invoice'
       PRIL:XPos         = 67
       PRIL:YPos         = 3
       PRIL:Length       = 12
       PRIL:Alignment    = 1
    OF 35
       L_AG:FieldName    = 'Fuel Surcharge'
       PRIL:XPos         = 42
       PRIL:YPos         = 35
       PRIL:Length       = 14
       PRIL:Alignment    = 0
    OF 36
       L_AG:FieldName    = 'Invoice Message'
       PRIL:XPos         = 5
       PRIL:YPos         = 34
       PRIL:Length       = 60
       PRIL:Alignment    = 0
    ELSE
       CLEAR(L_AG:FieldName)
    .
    EXIT
! -----------------------------------------------------------------------------------------------------
Add_CNote_Fields                 ROUTINE
    EXECUTE L_AG:Idx
       PRIF:FieldName    = 'Credit Note No.'                                    ! 1
       PRIF:FieldName    = 'Branch'
       PRIF:FieldName    = 'Date'
       PRIF:FieldName    = 'For Account'
       PRIF:FieldName    = 'Debt Name'                                          ! 5
       PRIF:FieldName    = 'Debt Line 1'
       PRIF:FieldName    = 'Debt Line 2'
       PRIF:FieldName    = 'Debt Suburb'
       PRIF:FieldName    = 'Debt Post'
       PRIF:FieldName    = 'Debt Vat No.'                                       ! 10
       PRIF:FieldName    = 'Reason'
       PRIF:FieldName    = 'Reason Line 1'
       PRIF:FieldName    = 'Reason Line 2'
       PRIF:FieldName    = 'Reason Line 3'
       PRIF:FieldName    = 'Original Invoice'       ! ikb more required here    ! 15
       PRIF:FieldName    = 'Credit Amt.'
       PRIF:FieldName    = 'Credit VAT'
       PRIF:FieldName    = 'Credit Total'
       PRIF:FieldName    = 'Credit Note Text'       ! Credit Note
       PRIF:FieldName    = 'Delete Text'            ! XXXXXXXXXX                - 20
       PRIF:FieldName    = 'Invoice Details'
    ELSE
       CLEAR(PRIF:FieldName)
    .
    EXIT
Add_CNote_Layout                ROUTINE
    CASE L_AG:Idx
    OF 1
       L_AG:FieldName    = 'Credit Note No.'
       PRIL:XPos         = 67
       PRIL:YPos         = 4
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 2
       L_AG:FieldName    = 'Branch'
       PRIL:XPos         = 64
       PRIL:YPos         = 5
       PRIL:Length       = 15
       PRIL:Alignment    = 1
    OF 3
       L_AG:FieldName    = 'Date'
       PRIL:XPos         = 4
       PRIL:YPos         = 11
       PRIL:Length       = 8
       PRIL:Alignment    = 1
    OF 4
       L_AG:FieldName    = 'For Account'
       PRIL:XPos         = 16
       PRIL:YPos         = 11
       PRIL:Length       = 24
       PRIL:Alignment    = 0

    OF 5
       L_AG:FieldName    = 'Debt Name'
       PRIL:XPos         = 4
       PRIL:YPos         = 37
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 6
       L_AG:FieldName    = 'Debt Line 1'
       PRIL:XPos         = 4
       PRIL:YPos         = 38
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 7
       L_AG:FieldName    = 'Debt Line 2'
       PRIL:XPos         = 4
       PRIL:YPos         = 39
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 8
       L_AG:FieldName    = 'Debt Suburb'
       PRIL:XPos         = 4
       PRIL:YPos         = 40
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 9
       L_AG:FieldName    = 'Debt Post'
       PRIL:XPos         = 4
       PRIL:YPos         = 41
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 10
       L_AG:FieldName    = 'Debt Vat No.'
       PRIL:XPos         = 4
       PRIL:YPos         = 42
       PRIL:Length       = 19
       PRIL:Alignment    = 2

    OF 11
       L_AG:FieldName    = 'Reason'
       PRIL:XPos         = 4
       PRIL:YPos         = 24
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 12
       L_AG:FieldName    = 'Reason Line 1'
       PRIL:XPos         = 4
       PRIL:YPos         = 25
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 13
       L_AG:FieldName    = 'Reason Line 2'
       PRIL:XPos         = 4
       PRIL:YPos         = 26
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 14
       L_AG:FieldName    = 'Reason Line 3'
       PRIL:XPos         = 4
       PRIL:YPos         = 28
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 15
       L_AG:FieldName    = 'Original Invoice'     ! ikb more required here
       PRIL:XPos         = 4
       PRIL:YPos         = 30
       PRIL:Length       = 72
       PRIL:Alignment    = 0

    OF 16
       L_AG:FieldName    = 'Credit Amt.'
       PRIL:XPos         = 64
       PRIL:YPos         = 41
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 17
       L_AG:FieldName    = 'Credit VAT'
       PRIL:XPos         = 64
       PRIL:YPos         = 43
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 18
       L_AG:FieldName    = 'Credit Total'
       PRIL:XPos         = 64
       PRIL:YPos         = 45
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 19
       L_AG:FieldName    = 'Credit Note Text'
       PRIL:XPos         = 67
       PRIL:YPos         = 1
       PRIL:Length       = 12
       PRIL:Alignment    = 1
    OF 20
       L_AG:FieldName    = 'Delete Text'
       PRIL:XPos         = 60
       PRIL:YPos         = 2
       PRIL:Length       = 20
       PRIL:Alignment    = 0
    OF 21
       L_AG:FieldName    = 'Invoice Details'
       PRIL:XPos         = 4
       PRIL:YPos         = 27
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    ELSE
       CLEAR(L_AG:FieldName)
    .
    EXIT
! -----------------------------------------------------------------------------------------------------
Add_DelN_Fields                   ROUTINE
    EXECUTE L_AG:Idx
       PRIF:FieldName    = 'Invoice No.'            ! 1
       PRIF:FieldName    = 'Branch'
       PRIF:FieldName    = 'Date'
       PRIF:FieldName    = 'For Account'
       PRIF:FieldName    = 'DI No.'
       PRIF:FieldName    = 'Manifest No.'
       PRIF:FieldName    = 'Shipper'
       PRIF:FieldName    = 'Ship Line 1'
       PRIF:FieldName    = 'Ship Line 2'
       PRIF:FieldName    = 'Ship Suburb'            ! 10
       PRIF:FieldName    = 'Ship Post'
       PRIF:FieldName    = 'Consignee'
       PRIF:FieldName    = 'Con Line 1'
       PRIF:FieldName    = 'Con Line 2'
       PRIF:FieldName    = 'Con Suburb'
       PRIF:FieldName    = 'Con Post'
       PRIF:FieldName    = 'Items Start'
       PRIF:FieldName    = 'Items End'
       PRIF:FieldName    = 'Vol Weight'
       PRIF:FieldName    = 'Act Weight'             ! 20
       PRIF:FieldName    = 'Spec. Inst. 1'          ! 21
       PRIF:FieldName    = 'Spec. Inst. 2'          ! 22
       PRIF:FieldName    = 'Spec. Inst. 3'          ! 23
       PRIF:FieldName    = 'Debt Ref.'
    ELSE
       CLEAR(PRIF:FieldName)
    .
    EXIT



!       PRIF:FieldName    = 'Debt Name'
!       PRIF:FieldName    = 'Debt Line 1'
!       PRIF:FieldName    = 'Debt Line 2'
!       PRIF:FieldName    = 'Debt Suburb'
!       PRIF:FieldName    = 'Debt Post'
!       PRIF:FieldName    = 'Debt Vat No.'
!       PRIF:FieldName    = 'Charge Fuel'
!       PRIF:FieldName    = 'Charge Insurance'
!       PRIF:FieldName    = 'Charge Docs'
!       PRIF:FieldName    = 'Charge Freight'
!       PRIF:FieldName    = 'Charge VAT'
!       PRIF:FieldName    = 'Charge Total'
!       PRIF:FieldName    = 'Copy Invoice'
Add_DelN_Layout                 ROUTINE
    CASE L_AG:Idx
    OF 1
       L_AG:FieldName    = 'Invoice No.'
       PRIL:XPos         = 67
       PRIL:YPos         = 4
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 2
       L_AG:FieldName    = 'Branch'
       PRIL:XPos         = 64
       PRIL:YPos         = 5
       PRIL:Length       = 15
       PRIL:Alignment    = 1
    OF 3
       L_AG:FieldName    = 'Date'
       PRIL:XPos         = 4
       PRIL:YPos         = 11
       PRIL:Length       = 8
       PRIL:Alignment    = 1
    OF 4
       L_AG:FieldName    = 'For Account'
       PRIL:XPos         = 16
       PRIL:YPos         = 11
       PRIL:Length       = 24
       PRIL:Alignment    = 0
    OF 5
       L_AG:FieldName    = 'DI No.'
       PRIL:XPos         = 44
       PRIL:YPos         = 11
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 6
       L_AG:FieldName    = 'Manifest No.'
       PRIL:XPos         = 69
       PRIL:YPos         = 11
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 7
       L_AG:FieldName    = 'Shipper'
       PRIL:XPos         = 4
       PRIL:YPos         = 15
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 8
       L_AG:FieldName    = 'Ship Line 1'
       PRIL:XPos         = 4
       PRIL:YPos         = 16
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 9
       L_AG:FieldName    = 'Ship Line 2'
       PRIL:XPos         = 4
       PRIL:YPos         = 17
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 10
       L_AG:FieldName    = 'Ship Suburb'
       PRIL:XPos         = 4
       PRIL:YPos         = 18
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 11
       L_AG:FieldName    = 'Ship Post'
       PRIL:XPos         = 4
       PRIL:YPos         = 19
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 12
       L_AG:FieldName    = 'Consignee'
       PRIL:XPos         = 44
       PRIL:YPos         = 15
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 13
       L_AG:FieldName    = 'Con Line 1'
       PRIL:XPos         = 44
       PRIL:YPos         = 16
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 14
       L_AG:FieldName    = 'Con Line 2'
       PRIL:XPos         = 44
       PRIL:YPos         = 17
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 15
       L_AG:FieldName    = 'Con Suburb'
       PRIL:XPos         = 44
       PRIL:YPos         = 18
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 16
       L_AG:FieldName    = 'Con Post'
       PRIL:XPos         = 44
       PRIL:YPos         = 19
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 17
       L_AG:FieldName    = 'Items Start'
       PRIL:XPos         = 4
       PRIL:YPos         = 23
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 18
       L_AG:FieldName    = 'Items End'
       PRIL:XPos         = 4
       PRIL:YPos         = 31
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 19
       L_AG:FieldName    = 'Vol Weight'
       PRIL:XPos         = 43
       PRIL:YPos         = 32
       PRIL:Length       = 20
       PRIL:Alignment    = 2
    OF 20
       L_AG:FieldName    = 'Act Weight'
       PRIL:XPos         = 43
       PRIL:YPos         = 33
       PRIL:Length       = 20
       PRIL:Alignment    = 2
    OF 21
       L_AG:FieldName    = 'Spec. Inst. 1'
       PRIL:XPos         = 3
       PRIL:YPos         = 32
       PRIL:Length       = 35
       PRIL:Alignment    = 0
    OF 22
       L_AG:FieldName    = 'Spec. Inst. 2'
       PRIL:XPos         = 3
       PRIL:YPos         = 33
       PRIL:Length       = 35
       PRIL:Alignment    = 0
    OF 23
       L_AG:FieldName    = 'Spec. Inst. 3'
       PRIL:XPos         = 3
       PRIL:YPos         = 34
       PRIL:Length       = 35
       PRIL:Alignment    = 0
    OF 24
       L_AG:FieldName    = 'Debt Ref.'
       PRIL:XPos         = 24
       PRIL:YPos         = 42
       PRIL:Length       = 17
       PRIL:Alignment    = 2
    ELSE
       CLEAR(L_AG:FieldName)
    .
    EXIT









!    OF 21
!       L_AG:FieldName    = 'Debt Name'
!       PRIL:XPos         = 4
!       PRIL:YPos         = 37
!       PRIL:Length       = 34
!       PRIL:Alignment    = 0
!    OF 22
!       L_AG:FieldName    = 'Debt Line 1'
!       PRIL:XPos         = 4
!       PRIL:YPos         = 38
!       PRIL:Length       = 34
!       PRIL:Alignment    = 0
!    OF 23
!       L_AG:FieldName    = 'Debt Line 2'
!       PRIL:XPos         = 4
!       PRIL:YPos         = 39
!       PRIL:Length       = 34
!       PRIL:Alignment    = 0
!    OF 24
!       L_AG:FieldName    = 'Debt Suburb'
!       PRIL:XPos         = 4
!       PRIL:YPos         = 40
!       PRIL:Length       = 34
!       PRIL:Alignment    = 0
!    OF 25
!       L_AG:FieldName    = 'Debt Post'
!       PRIL:XPos         = 4
!       PRIL:YPos         = 41
!       PRIL:Length       = 34
!       PRIL:Alignment    = 0
!    OF 26
!       L_AG:FieldName    = 'Debt Vat No.'
!       PRIL:XPos         = 4
!       PRIL:YPos         = 42
!       PRIL:Length       = 19
!       PRIL:Alignment    = 2
!    OF 28
!       L_AG:FieldName    = 'Charge Fuel'
!       PRIL:XPos         = 64
!       PRIL:YPos         = 35
!       PRIL:Length       = 15
!       PRIL:Alignment    = 2
!    OF 29
!       L_AG:FieldName    = 'Charge Insurance'
!       PRIL:XPos         = 64
!       PRIL:YPos         = 37
!       PRIL:Length       = 15
!       PRIL:Alignment    = 2
!    OF 30
!       L_AG:FieldName    = 'Charge Docs'
!       PRIL:XPos         = 64
!       PRIL:YPos         = 39
!       PRIL:Length       = 15
!       PRIL:Alignment    = 2
!    OF 31
!       L_AG:FieldName    = 'Charge Freight'
!       PRIL:XPos         = 64
!       PRIL:YPos         = 41
!       PRIL:Length       = 15
!       PRIL:Alignment    = 2
!    OF 32
!       L_AG:FieldName    = 'Charge VAT'
!       PRIL:XPos         = 64
!       PRIL:YPos         = 43
!       PRIL:Length       = 15
!       PRIL:Alignment    = 2
!    OF 33
!       L_AG:FieldName    = 'Charge Total'
!       PRIL:XPos         = 64
!       PRIL:YPos         = 45
!       PRIL:Length       = 15
!       PRIL:Alignment    = 2
!    OF 34
!       L_AG:FieldName    = 'Copy Invoice'
!       PRIL:XPos         = 67
!       PRIL:YPos         = 3
!       PRIL:Length       = 12
!       PRIL:Alignment    = 1
!    OF 35
!       L_AG:FieldName    = 'Fuel Surcharge'
!       PRIL:XPos         = 42
!       PRIL:YPos         = 35
!       PRIL:Length       = 14
!       PRIL:Alignment    = 0
! -----------------------------------------------------------------------------------------------------
Add_Statement_Fields               ROUTINE
!   Duplicated for Please Post to
!       PRIF:FieldName    = 'F.B.N. Transport'
!       PRIF:FieldName    = 'P.O.Box 1405'
!       PRIF:FieldName    = 'Hillcrest  3650'

    EXECUTE L_AG:Idx
       PRIF:FieldName    = 'Statement No.'
       PRIF:FieldName    = 'FBN Name'               ! F.B.N. Transport'
       PRIF:FieldName    = 'FBN Line 1'             ! P.O.Box 1405'
       PRIF:FieldName    = 'FBN Line 2'             ! Suburb
       PRIF:FieldName    = 'FBN Line 3'             ! Postal Code
       PRIF:FieldName    = 'VAT No.'
       PRIF:FieldName    = 'Page No.'
       PRIF:FieldName    = 'Page No. Val'
       PRIF:FieldName    = 'FBN Phone'              ! : 031 205 1705'
       PRIF:FieldName    = 'FBN Fax'                ! Fax: 031 205 2098'
       PRIF:FieldName    = 'Client Name Left'
       PRIF:FieldName    = 'Client Line 1'
       PRIF:FieldName    = 'Client Line 2'
       PRIF:FieldName    = 'Suburb'
       PRIF:FieldName    = 'Postal'
       PRIF:FieldName    = 'Client VAT No.'
       PRIF:FieldName    = 'Client Name Right'
       PRIF:FieldName    = 'Client No.'             ! Client No.   *** new
       PRIF:FieldName    = 'Please Post To'
       PRIF:FieldName    = 'FBN 2 Name'               ! F.B.N. Transport'
       PRIF:FieldName    = 'FBN 2 Line 1'             ! P.O.Box 1405'
       PRIF:FieldName    = 'FBN 2 Line 2'             ! Suburb
       PRIF:FieldName    = 'FBN 2 Line 3'             ! Postal Code
       PRIF:FieldName    = 'FBN Line 4'             ! Croft & Johnstone Rd.'
       PRIF:FieldName    = 'FBN Line 5'             ! 4057'
       PRIF:FieldName    = 'Statement for Period Ending:'
       PRIF:FieldName    = 'Statement Date'
       PRIF:FieldName    = 'Print Date'
       PRIF:FieldName    = 'Inv Date Label'         ! Item labels
       PRIF:FieldName    = 'Inv No. Label'
       PRIF:FieldName    = 'DI No Label'
       PRIF:FieldName    = 'Debit Label'
       PRIF:FieldName    = 'Credit Label'
       PRIF:FieldName    = 'Inv No. 2 Label'
       PRIF:FieldName    = 'Amount Label'
       PRIF:FieldName    = 'Items Start'
       PRIF:FieldName    = 'Invoice Date'
       PRIF:FieldName    = 'Invoice No.'
       PRIF:FieldName    = 'DI No'
       PRIF:FieldName    = 'Debit'
       PRIF:FieldName    = 'Credit'
       PRIF:FieldName    = 'Invoice No. 2'
       PRIF:FieldName    = 'Amount'
       PRIF:FieldName    = 'Items End'
       PRIF:FieldName    = '90 Days'
       PRIF:FieldName    = '60 Days'
       PRIF:FieldName    = '30 Days'
       PRIF:FieldName    = 'Current'
       PRIF:FieldName    = 'Total'
       PRIF:FieldName    = '90 Days Val.'
       PRIF:FieldName    = '60 Days Val.'
       PRIF:FieldName    = '30 Days Val.'
       PRIF:FieldName    = 'Current Val.'
       PRIF:FieldName    = 'Total Val.'
       PRIF:FieldName    = 'Total Due'
       PRIF:FieldName    = 'Total Due Val'
       PRIF:FieldName    = 'Last Period Payments'
       PRIF:FieldName    = 'Last Period Payments Val'
       PRIF:FieldName    = 'Amount Paid'
       PRIF:FieldName    = 'Amount Paid Val'
       PRIF:FieldName    = 'Comment Line'
       PRIF:FieldName    = 'Comment Line 2'
    ELSE
       CLEAR(PRIF:FieldName)
    .
    EXIT
Add_Statement_Layout              ROUTINE
    CASE L_AG:Idx
    OF 1
       L_AG:FieldName    = 'Statement No.'
       PRIL:XPos         = 65
       PRIL:YPos         = 6
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 2
       L_AG:FieldName    = 'FBN Name'               ! F.B.N. Transport'
       PRIL:XPos         = 5
       PRIL:YPos         = 5
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 3
       L_AG:FieldName    = 'FBN Line 1'             ! P.O.Box 1405'
       PRIL:XPos         = 5
       PRIL:YPos         = 6
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 4
       L_AG:FieldName    = 'FBN Line 2'             ! Suburb
       PRIL:XPos         = 5
       PRIL:YPos         = 7
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 5
       L_AG:FieldName    = 'FBN Line 3'             ! Postal Code
       PRIL:XPos         = 5
       PRIL:YPos         = 8
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 6
       L_AG:FieldName    = 'VAT No.'
       PRIL:XPos         = 67
       PRIL:YPos         = 9
       PRIL:Length       = 12
       PRIL:Alignment    = 0
    OF 7
       L_AG:FieldName    = 'Page No.'
       PRIL:XPos         = 58
       PRIL:YPos         = 8
       PRIL:Length       = 10
       PRIL:Alignment    = 0
    OF 8
       L_AG:FieldName    = 'Page No. Val'
       PRIL:XPos         = 67
       PRIL:YPos         = 8
       PRIL:Length       = 10
       PRIL:Alignment    = 0
    OF 9
       L_AG:FieldName    = 'FBN Phone'              ! : 031 205 1705'
       PRIL:XPos         = 4
       PRIL:YPos         = 9
       PRIL:Length       = 24
       PRIL:Alignment    = 1
    OF 10
       L_AG:FieldName    = 'FBN Fax'                ! Fax: 031 205 2098'
       PRIL:XPos         = 24
       PRIL:YPos         = 9
       PRIL:Length       = 24
       PRIL:Alignment    = 1
    OF 11
       L_AG:FieldName    = 'Client Name Left'
       PRIL:XPos         = 7
       PRIL:YPos         = 12
       PRIL:Length       = 40
       PRIL:Alignment    = 0
    OF 12
       L_AG:FieldName    = 'Client Line 1'
       PRIL:XPos         = 7
       PRIL:YPos         = 13
       PRIL:Length       = 40
       PRIL:Alignment    = 0
    OF 13
       L_AG:FieldName    = 'Client Line 2'
       PRIL:XPos         = 7
       PRIL:YPos         = 14
       PRIL:Length       = 40
       PRIL:Alignment    = 0
    OF 14
       L_AG:FieldName    = 'Suburb'
       PRIL:XPos         = 7
       PRIL:YPos         = 15
       PRIL:Length       = 40
       PRIL:Alignment    = 0
    OF 15
       L_AG:FieldName    = 'Postal'
       PRIL:XPos         = 7
       PRIL:YPos         = 16
       PRIL:Length       = 40
       PRIL:Alignment    = 0
    OF 16
       L_AG:FieldName    = 'Client VAT No.'
       PRIL:XPos         = 7
       PRIL:YPos         = 17
       PRIL:Length       = 40
       PRIL:Alignment    = 0
    OF 17
       L_AG:FieldName    = 'Client Name Right'
       PRIL:XPos         = 52
       PRIL:YPos         = 11
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 18
       L_AG:FieldName    = 'Please Post To'
       PRIL:XPos         = 52
       PRIL:YPos         = 14
       PRIL:Length       = 30
       PRIL:Alignment    = 0

    ! Note: see 50+ for lines from here
    OF 19
       L_AG:FieldName    = 'FBN Line 4'             ! Croft & Johnstone Rd.'
       PRIL:XPos         = 52
       PRIL:YPos         = 18
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 20
       L_AG:FieldName    = 'FBN Line 5'             ! 4057'
       PRIL:XPos         = 52
       PRIL:YPos         = 19
       PRIL:Length       = 10
       PRIL:Alignment    = 0



    OF 21
       L_AG:FieldName    = 'Statement for Period Ending:'
       PRIL:XPos         = 4
       PRIL:YPos         = 21
       PRIL:Length       = 28
       PRIL:Alignment    = 1
    OF 22
       L_AG:FieldName    = 'Statement Date'
       PRIL:XPos         = 34
       PRIL:YPos         = 21
       PRIL:Length       = 10
       PRIL:Alignment    = 0
    OF 23
       L_AG:FieldName    = 'Print Date'
       PRIL:XPos         = 62
       PRIL:YPos         = 21
       PRIL:Length       = 20
       PRIL:Alignment    = 0


    ! Note: See 50 + for label entries for columns
    ! Items
    OF 24
       L_AG:FieldName    = 'Items Start'
       PRIL:XPos         = 2
       PRIL:YPos         = 25
       PRIL:Length       = 74
       PRIL:Alignment    = 0
       PRIL:Repeating    = 1

    OF 25
       L_AG:FieldName    = 'Invoice Date'           ! These must be set before Items Start (ie < YPos)
       PRIL:XPos         = 1
       PRIL:YPos         = 0
       PRIL:Length       = 8
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 26
       L_AG:FieldName    = 'Invoice No.'
       PRIL:XPos         = 12
       PRIL:YPos         = 0
       PRIL:Length       = 10
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 27
       L_AG:FieldName    = 'DI No'
       PRIL:XPos         = 23
       PRIL:YPos         = 0
       PRIL:Length       = 10
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 28
       L_AG:FieldName    = 'Debit'
       PRIL:XPos         = 34
       PRIL:YPos         = 0
       PRIL:Length       = 11
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 29
       L_AG:FieldName    = 'Credit'
       PRIL:XPos         = 46
       PRIL:YPos         = 0
       PRIL:Length       = 11
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 30
       L_AG:FieldName    = 'Invoice No. 2'          ! Required????
       PRIL:XPos         = 57
       PRIL:YPos         = 0
       PRIL:Length       = 10
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 31
       L_AG:FieldName    = 'Amount'
       PRIL:XPos         = 67
       PRIL:YPos         = 0
       PRIL:Length       = 12
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 32
       L_AG:FieldName    = 'Items End'
       PRIL:XPos         = 0
       PRIL:YPos         = 53
       PRIL:Length       = 0
       PRIL:Alignment    = 0
       PRIL:Repeating    = 1

    OF 33
       L_AG:FieldName    = '90 Days'
       PRIL:XPos         = 2
       PRIL:YPos         = 56
       PRIL:Length       = 8
       PRIL:Alignment    = 2
    OF 34
       L_AG:FieldName    = '60 Days'
       PRIL:XPos         = 12
       PRIL:YPos         = 56
       PRIL:Length       = 8
       PRIL:Alignment    = 2
    OF 35
       L_AG:FieldName    = '30 Days'
       PRIL:XPos         = 22
       PRIL:YPos         = 56
       PRIL:Length       = 8
       PRIL:Alignment    = 2
    OF 36
       L_AG:FieldName    = 'Current'
       PRIL:XPos         = 32
       PRIL:YPos         = 56
       PRIL:Length       = 8
       PRIL:Alignment    = 2
    OF 37
       L_AG:FieldName    = 'Total'
       PRIL:XPos         = 42
       PRIL:YPos         = 56
       PRIL:Length       = 8
       PRIL:Alignment    = 2
    OF 38
       L_AG:FieldName    = '90 Days Val.'
       PRIL:XPos         = 2
       PRIL:YPos         = 58
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 39
       L_AG:FieldName    = '60 Days Val.'
       PRIL:XPos         = 12
       PRIL:YPos         = 58
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 40
       L_AG:FieldName    = '30 Days Val.'
       PRIL:XPos         = 22
       PRIL:YPos         = 58
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 41
       L_AG:FieldName    = 'Current Val.'
       PRIL:XPos         = 32
       PRIL:YPos         = 58
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 42
       L_AG:FieldName    = 'Total Val.'
       PRIL:XPos         = 42
       PRIL:YPos         = 58
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 43
       L_AG:FieldName    = 'Total Due'
       PRIL:XPos         = 56
       PRIL:YPos         = 58
       PRIL:Length       = 10
       PRIL:Alignment    = 2
    OF 44
       L_AG:FieldName    = 'Total Due Val'
       PRIL:XPos         = 67
       PRIL:YPos         = 58
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 45
       L_AG:FieldName    = 'Last Period Payments'
       PRIL:XPos         = 2
       PRIL:YPos         = 61
       PRIL:Length       = 25
       PRIL:Alignment    = 0
    OF 46
       L_AG:FieldName    = 'Last Period Payments Val'
       PRIL:XPos         = 28
       PRIL:YPos         = 61
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 47
       L_AG:FieldName    = 'Amount Paid'
       PRIL:XPos         = 58
       PRIL:YPos         = 61
       PRIL:Length       = 12
       PRIL:Alignment    = 0
    OF 48
       L_AG:FieldName    = 'Amount Paid Val'
       PRIL:XPos         = 70
       PRIL:YPos         = 61
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 49
       L_AG:FieldName    = 'Comment Line'
       PRIL:XPos         = 2
       PRIL:YPos         = 62
       PRIL:Length       = 48
       PRIL:Alignment    = 0
    OF 50
       L_AG:FieldName    = 'Comment Line 2'
       PRIL:XPos         = 2
       PRIL:YPos         = 63
       PRIL:Length       = 48
       PRIL:Alignment    = 0

    OF 51
       L_AG:FieldName    = 'FBN 2 Name'             ! P.O.Box 1405'
       PRIL:XPos         = 52
       PRIL:YPos         = 15
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 52
       L_AG:FieldName    = 'FBN 2 Line 1'             ! P.O.Box 1405'
       PRIL:XPos         = 52
       PRIL:YPos         = 16
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 53
       L_AG:FieldName    = 'FBN 2 Line 2'             ! Suburb
       PRIL:XPos         = 52
       PRIL:YPos         = 17
       PRIL:Length       = 15
       PRIL:Alignment    = 0
    OF 54
       L_AG:FieldName    = 'FBN 2 Line 3'             ! Postal Code
       PRIL:XPos         = 67
       PRIL:YPos         = 17
       PRIL:Length       = 15
       PRIL:Alignment    = 2



    OF 55
       L_AG:FieldName    = 'Inv Date Label'
       PRIL:XPos         = 2
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 56
       L_AG:FieldName    = 'Inv No. Label'
       PRIL:XPos         = 12
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 57
       L_AG:FieldName    = 'DI No Label'
       PRIL:XPos         = 22
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 58
       L_AG:FieldName    = 'Debit Label'
       PRIL:XPos         = 34
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 59
       L_AG:FieldName    = 'Credit Label'
       PRIL:XPos         = 46
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 60
       L_AG:FieldName    = 'Inv No. 2 Label'
       PRIL:XPos         = 57
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 61
       L_AG:FieldName    = 'Amount Label'
       PRIL:XPos         = 67
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1

    OF 62
       L_AG:FieldName    = 'Client No.'             ! Client No.   *** new
       PRIL:XPos         = 65
       PRIL:YPos         = 5
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    ELSE
       CLEAR(L_AG:FieldName)
    .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Prints Record'
  OF InsertRecord
    ActionMessage = 'Prints Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Prints Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Prints')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PRI:PrintName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Alignment',LOC:Alignment)                      ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(PRI:Record,History::PRI:Record)
  SELF.AddHistoryField(?PRI:PrintName,2)
  SELF.AddHistoryField(?PRI:PrintType,5)
  SELF.AddHistoryField(?PRI:PageLines,3)
  SELF.AddHistoryField(?PRI:PageColumns,4)
  SELF.AddUpdateFile(Access:Prints)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:PrintLayout.SetOpenRelated()
  Relate:PrintLayout.Open                                  ! File PrintLayout used by this procedure, so make sure it's RelationManager is open
  Access:Prints.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Prints
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:PrintLayout,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?PRI:PrintName{PROP:ReadOnly} = True
    DISABLE(?PRI:PrintType)
    DISABLE(?Button_Create)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    DISABLE(?Button_Designer)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon PRIL:PID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,PRIL:FKey_PID)   ! Add the sort order for PRIL:FKey_PID for sort order 1
  BRW2.AddRange(PRIL:PID,Relate:PrintLayout,Relate:Prints) ! Add file relationship range limit for sort order 1
  BRW2.AppendOrder('+PRIF:FieldName')                      ! Append an additional sort order
  ?Browse:2{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:2{PROP:IconList,2} = '~checkon.ico'
  BRW2.AddField(PRIF:FieldName,BRW2.Q.PRIF:FieldName)      ! Field PRIF:FieldName is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:XPos,BRW2.Q.PRIL:XPos)                ! Field PRIL:XPos is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:YPos,BRW2.Q.PRIL:YPos)                ! Field PRIL:YPos is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:Length,BRW2.Q.PRIL:Length)            ! Field PRIL:Length is a hot field or requires assignment from browse
  BRW2.AddField(LOC:Alignment,BRW2.Q.LOC:Alignment)        ! Field LOC:Alignment is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:Repeating,BRW2.Q.PRIL:Repeating)      ! Field PRIL:Repeating is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:PLID,BRW2.Q.PRIL:PLID)                ! Field PRIL:PLID is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:Alignment,BRW2.Q.PRIL:Alignment)      ! Field PRIL:Alignment is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:PID,BRW2.Q.PRIL:PID)                  ! Field PRIL:PID is a hot field or requires assignment from browse
  BRW2.AddField(PRIF:PFID,BRW2.Q.PRIF:PFID)                ! Field PRIF:PFID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Prints',QuickWindow)                ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1
  SELF.SetAlerts()
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW2::SortHeader.Init(Queue:Browse:2,?Browse:2,'','',BRW2::View:Browse,PRIL:PKey_PLID)
  BRW2::SortHeader.UseSortColors = False
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PrintLayout.Close
  !Kill the Sort Header
  BRW2::SortHeader.Kill()
  END
  IF SELF.Opened
    INIMgr.Update('Update_Prints',QuickWindow)             ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Print_Layout
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW2::SortHeader.SetAlerts()


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Create
      ThisWindow.Update()
          DO Create_Fields_Layout
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    OF ?Button_Designer
      ThisWindow.Update()
      Print_Designer(PRI:PID)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Take Sort Headers Events
  IF BRW2::SortHeader.TakeEvents()
     RETURN Level:Notify
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.SetQueueRecord PROCEDURE

  CODE
      ! Left|Center|Right
      EXECUTE PRIL:Alignment
         LOC:Alignment    = 'Center'
         LOC:Alignment    = 'Right'
      ELSE
         LOC:Alignment    = 'Left'
      .
  PARENT.SetQueueRecord
  
  IF (PRIL:Repeating = 1)
    SELF.Q.PRIL:Repeating_Icon = 2                         ! Set icon from icon list
  ELSE
    SELF.Q.PRIL:Repeating_Icon = 1                         ! Set icon from icon list
  END


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder<>NewOrder THEN
     BRW2::SortHeader.ClearSort()
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

BRW2::SortHeader.QueueResorted       PROCEDURE(STRING pString)
  CODE
    IF pString = ''
       BRW2.RestoreSort()
       BRW2.ResetSort(True)
    ELSE
       BRW2.ReplaceSort(pString)
    END
