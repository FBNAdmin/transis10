

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS010.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_PublicHolidays PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::PUB:Record  LIKE(PUB:RECORD),THREAD
QuickWindow          WINDOW('Form Public Holidays'),AT(,,199,56),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdatePublicHolidays'),SYSTEM
                       SHEET,AT(4,4,191,30),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Public Holiday:'),AT(9,20),USE(?PUB:PublicHoliday:Prompt),TRN
                           SPIN(@d6),AT(94,20,79,10),USE(PUB:PublicHoliday),REQ
                           BUTTON('...'),AT(178,20,12,10),USE(?Calendar)
                         END
                       END
                       BUTTON('&OK'),AT(94,38,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(146,38,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,38,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar5            CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Fuel Surcharge Record'
  OF InsertRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_PublicHolidays')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PUB:PublicHoliday:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(PUB:Record,History::PUB:Record)
  SELF.AddHistoryField(?PUB:PublicHoliday,4)
  SELF.AddUpdateFile(Access:PublicHolidays)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:PublicHolidays.Open                               ! File PublicHolidays used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PublicHolidays
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?Calendar)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_PublicHolidays',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PublicHolidays.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_PublicHolidays',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',PUB:PublicHoliday)
      IF Calendar5.Response = RequestCompleted THEN
      PUB:PublicHoliday=Calendar5.SelectedDate
      DISPLAY(?PUB:PublicHoliday)
      END
      ThisWindow.Reset(True)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_PublicHolidays PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(PublicHolidays)
                       PROJECT(PUB:PublicHoliday)
                       PROJECT(PUB:PHID)
                       PROJECT(PUB:PublicHolidayDateTime)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
PUB:PublicHoliday      LIKE(PUB:PublicHoliday)        !List box control field - type derived from field
PUB:PHID               LIKE(PUB:PHID)                 !Primary key field - type derived from field
PUB:PublicHolidayDateTime LIKE(PUB:PublicHolidayDateTime) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Public Holidays File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_PublicHolidays'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('80R(2)|M~Public Holiday~C(0)@d6@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the PublicHolidays file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Public Holiday'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_PublicHolidays')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:PublicHolidays.Open                               ! File PublicHolidays used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:PublicHolidays,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,PUB:Key_PublicHoliday)                ! Add the sort order for PUB:Key_PublicHoliday for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,PUB:PublicHolidayDateTime,1,BRW1) ! Initialize the browse locator using  using key: PUB:Key_PublicHoliday , PUB:PublicHolidayDateTime
  BRW1.AddField(PUB:PublicHoliday,BRW1.Q.PUB:PublicHoliday) ! Field PUB:PublicHoliday is a hot field or requires assignment from browse
  BRW1.AddField(PUB:PHID,BRW1.Q.PUB:PHID)                  ! Field PUB:PHID is a hot field or requires assignment from browse
  BRW1.AddField(PUB:PublicHolidayDateTime,BRW1.Q.PUB:PublicHolidayDateTime) ! Field PUB:PublicHolidayDateTime is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_PublicHolidays',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PublicHolidays.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_PublicHolidays',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_PublicHolidays
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Drivers PROCEDURE (p:DI_Drivers, p:D_Type)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Group            GROUP,PRE(L_G)                        !
BranchName           STRING(35)                            !Branch Name
                     END                                   !
LOC:Archived         BYTE                                  !
BRW2::View:Browse    VIEW(Manifest)
                       PROJECT(MAN:MID)
                       PROJECT(MAN:DepartDate)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:Rate)
                       PROJECT(MAN:VATRate)
                       PROJECT(MAN:State)
                       PROJECT(MAN:CreatedDate)
                       PROJECT(MAN:DepartTime)
                       PROJECT(MAN:TID)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:DRID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
MAN:MID                LIKE(MAN:MID)                  !List box control field - type derived from field
MAN:DepartDate         LIKE(MAN:DepartDate)           !List box control field - type derived from field
MAN:Cost               LIKE(MAN:Cost)                 !List box control field - type derived from field
MAN:Rate               LIKE(MAN:Rate)                 !List box control field - type derived from field
MAN:VATRate            LIKE(MAN:VATRate)              !List box control field - type derived from field
MAN:State              LIKE(MAN:State)                !List box control field - type derived from field
MAN:CreatedDate        LIKE(MAN:CreatedDate)          !List box control field - type derived from field
MAN:DepartTime         LIKE(MAN:DepartTime)           !List box control field - type derived from field
MAN:TID                LIKE(MAN:TID)                  !List box control field - type derived from field
MAN:BID                LIKE(MAN:BID)                  !List box control field - type derived from field
MAN:DRID               LIKE(MAN:DRID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(TripSheets)
                       PROJECT(TRI:TRID)
                       PROJECT(TRI:DepartDate)
                       PROJECT(TRI:DepartTime)
                       PROJECT(TRI:ReturnedDate)
                       PROJECT(TRI:ReturnedTime)
                       PROJECT(TRI:Notes)
                       PROJECT(TRI:BID)
                       PROJECT(TRI:State)
                       PROJECT(TRI:DRID)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
TRI:TRID               LIKE(TRI:TRID)                 !List box control field - type derived from field
TRI:DepartDate         LIKE(TRI:DepartDate)           !List box control field - type derived from field
TRI:DepartTime         LIKE(TRI:DepartTime)           !List box control field - type derived from field
TRI:ReturnedDate       LIKE(TRI:ReturnedDate)         !List box control field - type derived from field
TRI:ReturnedTime       LIKE(TRI:ReturnedTime)         !List box control field - type derived from field
TRI:Notes              LIKE(TRI:Notes)                !List box control field - type derived from field
TRI:BID                LIKE(TRI:BID)                  !List box control field - type derived from field
TRI:State              LIKE(TRI:State)                !List box control field - type derived from field
TRI:DRID               LIKE(TRI:DRID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB3::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?L_G:BranchName
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DRI:Record  LIKE(DRI:RECORD),THREAD
QuickWindow          WINDOW('Form Drivers'),AT(,,281,234),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('UpdateDrivers'),SYSTEM
                       PROMPT('DrID:'),AT(191,5),USE(?DRI:DRID:Prompt)
                       STRING(@n_10),AT(215,5),USE(DRI:DRID),RIGHT(1)
                       SHEET,AT(4,4,273,208),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('First Name:'),AT(9,25),USE(?DRI:FirstName:Prompt),TRN
                           ENTRY(@s35),AT(65,25,144,10),USE(DRI:FirstName),MSG('First Name'),REQ,TIP('First Name')
                           PROMPT('Surname:'),AT(9,39),USE(?DRI:Surname:Prompt),TRN
                           ENTRY(@s35),AT(65,39,144,10),USE(DRI:Surname),MSG('Surname'),TIP('Surname')
                           PROMPT('Employee No.:'),AT(9,54),USE(?DRI:EmployeeNo:Prompt),TRN
                           ENTRY(@s20),AT(65,54,84,10),USE(DRI:EmployeeNo),MSG('Employee No.'),TIP('Employee No.')
                           PROMPT('Type:'),AT(9,73),USE(?DRI:Type:Prompt),TRN
                           LIST,AT(65,73,84,10),USE(DRI:Type),DROP(5),FROM('Driver|#0|Assistant|#1'),MSG('Driver or Assistant'), |
  TIP('Driver or Assistant')
                           PROMPT('Category:'),AT(9,92),USE(?DRI:Category:Prompt),TRN
                           LIST,AT(65,92,84,10),USE(DRI:Category),DROP(5),FROM('Branch|#0|Long Distance|#1'),MSG('Driver Category'), |
  TIP('Driver Category')
                           PROMPT('Branch:'),AT(9,118),USE(?Prompt6),TRN
                           LIST,AT(65,118,84,10),USE(L_G:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                           CHECK(' &Archived'),AT(65,134),USE(DRI:Archived),MSG('Mark driver as not active'),TIP('Mark drive' & |
  'r as not active'),TRN
                           ENTRY(@d17),AT(113,134,56,10),USE(DRI:Archived_Date),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP
                         END
                         TAB('&2) Manifest'),USE(?Tab:2)
                           LIST,AT(11,23,257,183),USE(?Browse:2),HVSCROLL,FORMAT('40R(1)|M~MID~C(0)@n_10@44R(2)|M~' & |
  'Depart Date~C(0)@d5b@52R(1)|M~Cost~C(0)@n-14.2@38R(1)|M~Rate~C(0)@n-10.2@36R(1)|M~VA' & |
  'T Rate~C(0)@n-7.2@24R(2)|M~State~C(0)@n3@48R(2)|M~Created Date~C(0)@d6@46R(2)|M~Depa' & |
  'rt Time~C(0)@t7@30R(2)|M~TID~C(0)@n_10@30R(2)|M~BID~C(0)@n_10@'),FROM(Queue:Browse:2),IMM, |
  MSG('Browsing the TripSheets file')
                         END
                         TAB('&3) TripSheets'),USE(?Tab:3)
                           LIST,AT(11,23,257,183),USE(?Browse:4),HVSCROLL,FORMAT('40R(2)|M~TRID~C(0)@N_10@48R(2)|M' & |
  '~Depart Date~C(0)@d6@48R(2)|M~Depart Time~C(0)@t7@52R(2)|M~Returned Date~C(0)@d6@52R' & |
  '(2)|M~Returned Time~C(0)@t7@80L(2)|M~Notes~@s255@30R(2)|M~BID~C(0)@n_10@24R(2)|M~Sta' & |
  'te~C(0)@n3@'),FROM(Queue:Browse:4),IMM,MSG('Browsing the TripSheets file')
                         END
                       END
                       BUTTON('&OK'),AT(173,215,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(227,215,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,215,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
                     END

BRW4::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB3                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Passed                ROUTINE
    ! (p:DI_Drivers, p:D_Type)
    IF ~OMITTED(1)
       IF p:DI_Drivers = TRUE
          DRI:Category     = 1  ! Branch|Long Distance
    .  .
    IF ~OMITTED(2)
       IF p:D_Type ~= 0
          DRI:Type     = p:D_Type
    .  .

    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Drivers Record'
  OF InsertRecord
    ActionMessage = 'Drivers Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Drivers Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Drivers')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DRI:DRID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DRI:Record,History::DRI:Record)
  SELF.AddHistoryField(?DRI:DRID,1)
  SELF.AddHistoryField(?DRI:FirstName,2)
  SELF.AddHistoryField(?DRI:Surname,3)
  SELF.AddHistoryField(?DRI:EmployeeNo,4)
  SELF.AddHistoryField(?DRI:Type,5)
  SELF.AddHistoryField(?DRI:Category,7)
  SELF.AddHistoryField(?DRI:Archived,9)
  SELF.AddHistoryField(?DRI:Archived_Date,12)
  SELF.AddUpdateFile(Access:Drivers)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Drivers
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:Manifest,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:TripSheets,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?DRI:FirstName{PROP:ReadOnly} = True
    ?DRI:Surname{PROP:ReadOnly} = True
    ?DRI:EmployeeNo{PROP:ReadOnly} = True
    DISABLE(?DRI:Type)
    DISABLE(?DRI:Category)
    DISABLE(?L_G:BranchName)
    ?DRI:Archived_Date{PROP:ReadOnly} = True
  END
      LOC:Archived    = DRI:Archived
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon MAN:DRID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,MAN:FKey_DRID)   ! Add the sort order for MAN:FKey_DRID for sort order 1
  BRW2.AddRange(MAN:DRID,Relate:Manifest,Relate:Drivers)   ! Add file relationship range limit for sort order 1
  BRW2.AddField(MAN:MID,BRW2.Q.MAN:MID)                    ! Field MAN:MID is a hot field or requires assignment from browse
  BRW2.AddField(MAN:DepartDate,BRW2.Q.MAN:DepartDate)      ! Field MAN:DepartDate is a hot field or requires assignment from browse
  BRW2.AddField(MAN:Cost,BRW2.Q.MAN:Cost)                  ! Field MAN:Cost is a hot field or requires assignment from browse
  BRW2.AddField(MAN:Rate,BRW2.Q.MAN:Rate)                  ! Field MAN:Rate is a hot field or requires assignment from browse
  BRW2.AddField(MAN:VATRate,BRW2.Q.MAN:VATRate)            ! Field MAN:VATRate is a hot field or requires assignment from browse
  BRW2.AddField(MAN:State,BRW2.Q.MAN:State)                ! Field MAN:State is a hot field or requires assignment from browse
  BRW2.AddField(MAN:CreatedDate,BRW2.Q.MAN:CreatedDate)    ! Field MAN:CreatedDate is a hot field or requires assignment from browse
  BRW2.AddField(MAN:DepartTime,BRW2.Q.MAN:DepartTime)      ! Field MAN:DepartTime is a hot field or requires assignment from browse
  BRW2.AddField(MAN:TID,BRW2.Q.MAN:TID)                    ! Field MAN:TID is a hot field or requires assignment from browse
  BRW2.AddField(MAN:BID,BRW2.Q.MAN:BID)                    ! Field MAN:BID is a hot field or requires assignment from browse
  BRW2.AddField(MAN:DRID,BRW2.Q.MAN:DRID)                  ! Field MAN:DRID is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon TRI:DRID for sort order 1
  BRW4.AddSortOrder(BRW4::Sort0:StepClass,TRI:FKey_DRID)   ! Add the sort order for TRI:FKey_DRID for sort order 1
  BRW4.AddRange(TRI:DRID,Relate:TripSheets,Relate:Drivers) ! Add file relationship range limit for sort order 1
  BRW4.AddField(TRI:TRID,BRW4.Q.TRI:TRID)                  ! Field TRI:TRID is a hot field or requires assignment from browse
  BRW4.AddField(TRI:DepartDate,BRW4.Q.TRI:DepartDate)      ! Field TRI:DepartDate is a hot field or requires assignment from browse
  BRW4.AddField(TRI:DepartTime,BRW4.Q.TRI:DepartTime)      ! Field TRI:DepartTime is a hot field or requires assignment from browse
  BRW4.AddField(TRI:ReturnedDate,BRW4.Q.TRI:ReturnedDate)  ! Field TRI:ReturnedDate is a hot field or requires assignment from browse
  BRW4.AddField(TRI:ReturnedTime,BRW4.Q.TRI:ReturnedTime)  ! Field TRI:ReturnedTime is a hot field or requires assignment from browse
  BRW4.AddField(TRI:Notes,BRW4.Q.TRI:Notes)                ! Field TRI:Notes is a hot field or requires assignment from browse
  BRW4.AddField(TRI:BID,BRW4.Q.TRI:BID)                    ! Field TRI:BID is a hot field or requires assignment from browse
  BRW4.AddField(TRI:State,BRW4.Q.TRI:State)                ! Field TRI:State is a hot field or requires assignment from browse
  BRW4.AddField(TRI:DRID,BRW4.Q.TRI:DRID)                  ! Field TRI:DRID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Drivers',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  IF ?DRI:Archived{Prop:Checked}
    UNHIDE(?DRI:Archived_Date)
  END
  IF NOT ?DRI:Archived{PROP:Checked}
    HIDE(?DRI:Archived_Date)
  END
  FDB3.Init(?L_G:BranchName,Queue:FileDrop.ViewPosition,FDB3::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB3.Q &= Queue:FileDrop
  FDB3.AddSortOrder(BRA:Key_BranchName)
  FDB3.AddField(BRA:BranchName,FDB3.Q.BRA:BranchName) !List box control field - type derived from field
  FDB3.AddField(BRA:BID,FDB3.Q.BRA:BID) !Primary key field - type derived from field
  FDB3.AddUpdateField(BRA:BID,DRI:BID)
  ThisWindow.AddItem(FDB3.WindowComponent)
      BRA:BID             = DRI:BID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         L_G:BranchName   = BRA:BranchName
      .
      IF SELF.Request = InsertRecord
         DO Check_Passed
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Drivers',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DRI:Archived
      IF ?DRI:Archived{PROP:Checked}
        UNHIDE(?DRI:Archived_Date)
      END
      IF NOT ?DRI:Archived{PROP:Checked}
        HIDE(?DRI:Archived_Date)
      END
      ThisWindow.Reset()
          IF DRI:Archived = TRUE AND LOC:Archived = FALSE
             DRI:Archived_Date    = TODAY()
             DRI:Archived_Time    = CLOCK()
             DISPLAY
          .
    OF ?OK
      ThisWindow.Update()
          DRI:FirstNameSurname    = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Drivers PROCEDURE (p:DI_Drivers, p:D_Type)

CurrentTab           STRING(80)                            !
L_G:Category         STRING(15)                            !Driver Category
L_G:Type             STRING(10)                            !Driver or Assistant
LOC:Group_Options    GROUP,PRE(L_OG)                       !
BID                  ULONG                                 !Branch ID
BranchName           STRING(35)                            !Branch Name
Selecting            BYTE                                  !
Type                 LONG                                  !Driver or Assistant
Category             LONG                                  !
                     END                                   !
LOC:Browse_Fields    GROUP,PRE()                           !
L_BG:BranchName      STRING(35)                            !Branch Name
                     END                                   !
LOC:Show_Archived    BYTE                                  !
LOC:Archive_Warning_Show BYTE                              !
BRW1::View:Browse    VIEW(Drivers)
                       PROJECT(DRI:FirstName)
                       PROJECT(DRI:Surname)
                       PROJECT(DRI:EmployeeNo)
                       PROJECT(DRI:Archived)
                       PROJECT(DRI:DRID)
                       PROJECT(DRI:FirstNameSurname)
                       PROJECT(DRI:Type)
                       PROJECT(DRI:Category)
                       PROJECT(DRI:BID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DRI:FirstName          LIKE(DRI:FirstName)            !List box control field - type derived from field
DRI:Surname            LIKE(DRI:Surname)              !List box control field - type derived from field
L_G:Category           LIKE(L_G:Category)             !List box control field - type derived from local data
L_G:Type               LIKE(L_G:Type)                 !List box control field - type derived from local data
DRI:EmployeeNo         LIKE(DRI:EmployeeNo)           !List box control field - type derived from field
L_BG:BranchName        LIKE(L_BG:BranchName)          !List box control field - type derived from local data
DRI:Archived           LIKE(DRI:Archived)             !List box control field - type derived from field
DRI:Archived_Icon      LONG                           !Entry's icon ID
DRI:DRID               LIKE(DRI:DRID)                 !List box control field - type derived from field
DRI:FirstNameSurname   LIKE(DRI:FirstNameSurname)     !Browse hot field - type derived from field
DRI:Type               LIKE(DRI:Type)                 !Browse hot field - type derived from field
DRI:Category           LIKE(DRI:Category)             !Browse hot field - type derived from field
DRI:BID                LIKE(DRI:BID)                  !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB8::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?L_OG:BranchName
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Drivers File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Browse_Drivers'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,ALRT(F2Key),FORMAT('80L(2)|M~First Name~@' & |
  's35@80L(2)|M~Surname~@s35@40L(2)|M~Category~@s15@40L(2)|M~Type~@s10@50L(2)|M~Employe' & |
  'e No.~@s15@60L(2)|M~Branch Name~@s35@35L(2)|MI~Archived~@p p@40R(2)|M~DrID~L(2)@n_10@'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing the Drivers file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By First Name && Surname'),USE(?Tab:2)
                           GROUP,AT(5,184,189,10),USE(?Group_Branch)
                             PROMPT('Branch:'),AT(5,184),USE(?Prompt1)
                             LIST,AT(34,184,81,10),USE(L_OG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                             CHECK('Show Archived'),AT(131,184),USE(LOC:Show_Archived),TIP('Include Archived in the ' & |
  'list<0DH,0AH,0DH,0AH>To toggle Archived press F2')
                           END
                         END
                         TAB('&2) By Employee No.'),USE(?Tab2)
                         END
                       END
                       BUTTON('Cl&ose'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(210,180,17,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB8                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Archive_Toggle                  ROUTINE
DATA
R:Yes   BYTE

CODE  
  BRW1.UpdateViewRecord()
  BRW1.UpdateBuffer()
  
  ! Toggle archived on highlighted...
  IF LOC:Archive_Warning_Show = 0
     IF DRI:Archived = FALSE
        CASE MESSAGE('Archive this Driver?','Archive Drivers',ICON:Question, 'Yes|Yes (don''t warn again)|No', 3)
        OF 1
          R:Yes = TRUE
        OF 2    
          R:Yes = TRUE          
          LOC:Archive_Warning_Show  = 1
        .
      ELSE
        CASE MESSAGE('Un-Archive this Driver?','Archive Drivers',ICON:Question, 'Yes|Yes (don''t warn again)|No', 3)
        OF 1
          R:Yes = TRUE
        OF 2    
          R:Yes = TRUE          
          LOC:Archive_Warning_Show  = 1
        .        
      .
  ELSE
    R:Yes = TRUE
  .
  
  IF R:Yes = TRUE    
    BRW1.UpdateViewRecord()
    
    BRW1.UpdateBuffer()
    IF Access:Drivers.TryFetch(DRI:PKey_DRID) = Level:Benign
      IF DRI:Archived  = 1
        DRI:Archived = 0
      ELSE
        DRI:Archived = 1
        DRI:Archived_Date = TODAY()
        DRI:Archived_Time = CLOCK()
      .          
      IF Access:Drivers.TryUpdate() = Level:Benign
        BRW1.ResetFromBuffer()  
  . . .    

  EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Drivers')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:BID',L_OG:BID)                                ! Added by: BrowseBox(ABC)
  BIND('L_OG:Category',L_OG:Category)                      ! Added by: BrowseBox(ABC)
  BIND('L_OG:Type',L_OG:Type)                              ! Added by: BrowseBox(ABC)
  BIND('LOC:Show_Archived',LOC:Show_Archived)              ! Added by: BrowseBox(ABC)
  BIND('L_G:Category',L_G:Category)                        ! Added by: BrowseBox(ABC)
  BIND('L_G:Type',L_G:Type)                                ! Added by: BrowseBox(ABC)
  BIND('L_BG:BranchName',L_BG:BranchName)                  ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Drivers,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,DRI:SKey_FirstNameSurname)            ! Add the sort order for DRI:SKey_FirstNameSurname for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,DRI:FirstNameSurname,1,BRW1)   ! Initialize the browse locator using  using key: DRI:SKey_FirstNameSurname , DRI:FirstNameSurname
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon DRI:FirstName for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,DRI:Key_FirstNameSurname) ! Add the sort order for DRI:Key_FirstNameSurname for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,DRI:FirstName,1,BRW1)          ! Initialize the browse locator using  using key: DRI:Key_FirstNameSurname , DRI:FirstName
  BRW1.SetFilter('((L_OG:BID = 0 OR L_OG:BID = DRI:BID) AND ((L_OG:Category = -1 OR DRI:Category = L_OG:Category) AND (L_OG:Type = -1 OR L_OG:Type = DRI:Type)) AND (LOC:Show_Archived = 1 OR DRI:Archived = 0))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Show_Archived)                    ! Apply the reset field
  BRW1.AddResetField(L_OG:BID)                             ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(DRI:FirstName,BRW1.Q.DRI:FirstName)        ! Field DRI:FirstName is a hot field or requires assignment from browse
  BRW1.AddField(DRI:Surname,BRW1.Q.DRI:Surname)            ! Field DRI:Surname is a hot field or requires assignment from browse
  BRW1.AddField(L_G:Category,BRW1.Q.L_G:Category)          ! Field L_G:Category is a hot field or requires assignment from browse
  BRW1.AddField(L_G:Type,BRW1.Q.L_G:Type)                  ! Field L_G:Type is a hot field or requires assignment from browse
  BRW1.AddField(DRI:EmployeeNo,BRW1.Q.DRI:EmployeeNo)      ! Field DRI:EmployeeNo is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:BranchName,BRW1.Q.L_BG:BranchName)    ! Field L_BG:BranchName is a hot field or requires assignment from browse
  BRW1.AddField(DRI:Archived,BRW1.Q.DRI:Archived)          ! Field DRI:Archived is a hot field or requires assignment from browse
  BRW1.AddField(DRI:DRID,BRW1.Q.DRI:DRID)                  ! Field DRI:DRID is a hot field or requires assignment from browse
  BRW1.AddField(DRI:FirstNameSurname,BRW1.Q.DRI:FirstNameSurname) ! Field DRI:FirstNameSurname is a hot field or requires assignment from browse
  BRW1.AddField(DRI:Type,BRW1.Q.DRI:Type)                  ! Field DRI:Type is a hot field or requires assignment from browse
  BRW1.AddField(DRI:Category,BRW1.Q.DRI:Category)          ! Field DRI:Category is a hot field or requires assignment from browse
  BRW1.AddField(DRI:BID,BRW1.Q.DRI:BID)                    ! Field DRI:BID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Drivers',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  FDB8.Init(?L_OG:BranchName,Queue:FileDrop.ViewPosition,FDB8::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB8.Q &= Queue:FileDrop
  FDB8.AddSortOrder(BRA:Key_BranchName)
  FDB8.AddField(BRA:BranchName,FDB8.Q.BRA:BranchName) !List box control field - type derived from field
  FDB8.AddField(BRA:BID,FDB8.Q.BRA:BID) !Primary key field - type derived from field
  FDB8.AddUpdateField(BRA:BID,L_OG:BID)
  ThisWindow.AddItem(FDB8.WindowComponent)
  FDB8.DefaultFill = 0
      ! (p:DI_Drivers, p:D_Type)
  
      IF CLIP(p:DI_Drivers) = ''
         L_OG:Category    = -1
      ELSE
         L_OG:Category    = DEFORMAT(p:DI_Drivers)
      .
  
  !    IF DEFORMAT(p:DI_Drivers) = 1
  !       L_OG:Selecting    = 1                ! Not sure what the purpose of this was meant to be
  !       HIDE(?Group_Branch)
  !    .
  
      IF CLIP(p:D_Type) = ''
         L_OG:Type        = -1
      ELSE
         L_OG:Type        = DEFORMAT(p:D_Type)
      .
      ! Load the Branch record
      BRA:BID             = GLO:BranchID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         L_OG:BID         = GLO:BranchID
         L_OG:BranchName  = BRA:BranchName
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Drivers',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Drivers(p:DI_Drivers, p:D_Type)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      IF KEYCODE() = F2Key
        DO Archive_Toggle
      .
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      CLEAR(L_G:Category)
      CLEAR(L_G:Type)
      CLEAR(L_BG:BranchName)
  
      EXECUTE DRI:Category + 1
         L_G:Category     = 'Branch'
         L_G:Category     = 'Long Distance'
      .
  
  
      EXECUTE DRI:Type + 1
         L_G:Type         = 'Driver'
         L_G:Type         = 'Assistant'
      .
  
  
  
  
      BRA:BID     = DRI:BID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         L_BG:BranchName  = BRA:BranchName
      .
  PARENT.SetQueueRecord
  
  IF (DRI:Archived)
    SELF.Q.DRI:Archived_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.DRI:Archived_Icon = 1                           ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Branch, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Branch


FDB8.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      ! Add an All
      Queue:FileDrop.BRA:BranchName      = 'All'
      GET(Queue:FileDrop, Queue:FileDrop.BRA:BranchName)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop)
         Queue:FileDrop.BRA:BranchName      = 'All'
         Queue:FileDrop.BRA:BID             = 0
         ADD(Queue:FileDrop)
      .
  RETURN ReturnValue

