

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS003.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Prints PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:PrintType        STRING(50)                            !
BRW1::View:Browse    VIEW(Prints)
                       PROJECT(PRI:PrintName)
                       PROJECT(PRI:PageLines)
                       PROJECT(PRI:PrintType)
                       PROJECT(PRI:PID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
PRI:PrintName          LIKE(PRI:PrintName)            !List box control field - type derived from field
PRI:PageLines          LIKE(PRI:PageLines)            !List box control field - type derived from field
LOC:PrintType          LIKE(LOC:PrintType)            !List box control field - type derived from local data
PRI:PrintType          LIKE(PRI:PrintType)            !Browse hot field - type derived from field
PRI:PID                LIKE(PRI:PID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Prints File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Browse_Prints'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('100L(2)|M~Print Name~@s50@40R(2)|' & |
  'M~Page Lines~C(0)@n_5@100L(2)|M~Print Type~C(0)@s50@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he Prints file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       BUTTON('Desig&ner'),AT(4,180,67,14),USE(?Button_Designer),LEFT,ICON('CHANGEPG.ICO'),FLAT
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Print Name'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(224,4,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('Copy'),AT(76,180,56,14),USE(?BUTTON_Copy),LEFT,ICON(ICON:Pick),FLAT,TIP('Copy selec' & |
  'ted to new backup')
                       BUTTON('Replace'),AT(136,180,56,14),USE(?BUTTON_Replace),LEFT,ICON(ICON:Save),FLAT,TIP('Replace in' & |
  '-use version with selected version.<0DH,0AH>Also makes a copy of original.')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

                     MAP
CopySelected            PROCEDURE(ULONG pos), BYTE

                     .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ReplaceSelected         ROUTINE
DATA
R:PID    LIKE(PRI:PID)
R:Type   LIKE(PRI:PrintType)
R:Cnt    BYTE(0)
R:Idx    LONG
R:CopyOk BYTE

R:Backup LONG

CODE
   ! loop and find in-use copy
   GET(Queue:Browse:1, CHOICE(?Browse:1))
   IF ERRORCODE()
      MESSAGE('not got')
   ELSE      
      IF Queue:Browse:1.PRI:PrintType <= 3
         MESSAGE('Cant replace in-use with same in-use')
      ELSE
         R:PID    = Queue:Browse:1.PRI:PID
         R:Type   = Queue:Browse:1.PRI:PrintType
         
         LOOP
            R:Idx += 1
            GET(Queue:Browse:1, R:Idx)
            IF ERRORCODE() 
               BREAK
            .
            IF Queue:Browse:1.PRI:PrintType = (R:Type - 4)
               BREAK
            .
         .
         
         IF Queue:Browse:1.PRI:PrintType ~= (R:Type - 4)
            ! not found in-use to copy
            MESSAGE('In-use version not found to copy, replace will continue')
            R:CopyOk = TRUE
         ELSE         
            R:CopyOk = CopySelected(R:Idx)
            
            IF R:CopyOk > 0   
               R:CopyOk = 0
               PRI:PID = Queue:Browse:1.PRI:PID
               IF Access:Prints.Fetch(PRI:PKey_PID) = Level:Benign
                  IF Access:Prints.DeleteRecord(FALSE) = Level:Benign
                     R:CopyOk = TRUE
                  .
               .               
            .       
         .  

         IF R:CopyOk <= 0
            MESSAGE('failed')
         ELSE
            PRI:PID = R:PID
            IF Access:Prints.Fetch(PRI:PKey_PID) ~= Level:Benign
               MESSAGE('not got copy to change')
            ELSE               
               PRI:PrintType = PRI:PrintType - 4
               
               R:Backup = INSTRING('- Backup', PRI:PrintName, 1, 1)
               IF R:Backup > 0
                  PRI:PrintName = SUB(PRI:PrintName, 1, R:Backup - 1)
               .
               
               IF Access:Prints.Update() = Level:Benign
                  ! all good  
                  MESSAGE('Replaced')
               ELSE
                  MESSAGE('Failed')
               .  
            .            
            
            BRW1.ResetFromFile()
         .
      .
   .   
   EXIT   

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Prints')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:PrintType',LOC:PrintType)                      ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:PrintLayout.SetOpenRelated()
  Relate:PrintLayout.Open                                  ! File PrintLayout used by this procedure, so make sure it's RelationManager is open
  Relate:PrintLayoutAlias.Open                             ! File PrintLayoutAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Prints,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon PRI:PrintName for sort order 1
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,PRI:Key_PrintName) ! Add the sort order for PRI:Key_PrintName for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,PRI:PrintName,1,BRW1)          ! Initialize the browse locator using  using key: PRI:Key_PrintName , PRI:PrintName
  BRW1.AddField(PRI:PrintName,BRW1.Q.PRI:PrintName)        ! Field PRI:PrintName is a hot field or requires assignment from browse
  BRW1.AddField(PRI:PageLines,BRW1.Q.PRI:PageLines)        ! Field PRI:PageLines is a hot field or requires assignment from browse
  BRW1.AddField(LOC:PrintType,BRW1.Q.LOC:PrintType)        ! Field LOC:PrintType is a hot field or requires assignment from browse
  BRW1.AddField(PRI:PrintType,BRW1.Q.PRI:PrintType)        ! Field PRI:PrintType is a hot field or requires assignment from browse
  BRW1.AddField(PRI:PID,BRW1.Q.PRI:PID)                    ! Field PRI:PID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Prints',QuickWindow)                ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PrintLayout.Close
    Relate:PrintLayoutAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Prints',QuickWindow)             ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Prints
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Designer
      BRW1.UpdateViewRecord()
          IF PRI:PID = 0
             CYCLE
          .
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Designer
      ThisWindow.Update()
      Print_Designer(PRI:PID)
      ThisWindow.Reset
    OF ?BUTTON_Copy
      ThisWindow.Update()
      GET(Queue:Browse:1, CHOICE(?Browse:1))
      IF ERRORCODE()
         MESSAGE('Could not copy')
      ELSE
         CopySelected(CHOICE(?Browse:1))
         
         !Queue:Browse:1.PRI:PID
         BRW1.ResetFromFile()
      .
    OF ?BUTTON_Replace
      ThisWindow.Update()
      DO ReplaceSelected
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

CopySelected         PROCEDURE(ULONG pos)   !, BYTE
R:Cnt                   BYTE(0)
R:Done                  BYTE(1)
R:Name                  LIKE(PRI:PrintName)

CODE   
   GET(Queue:Browse:1, pos)
   IF ERRORCODE()
      MESSAGE('Could not copy - not got')
      RETURN 0   
   .
   
   PRI:PID  = 0
   PRI:PrintName  = CLIP(Queue:Browse:1.PRI:PrintName) & ' - Backup'   
   IF Queue:Browse:1.PRI:PrintType <= 3
      PRI:PrintType = Queue:Browse:1.PRI:PrintType + 4   
   .
   
   R:Name = PRI:PrintName
   LOOP 10 TIMES
      IF Access:Prints.TryInsert() = Level:Benign
         BREAK
      .
      PRI:PID        = 0
      PRI:PrintName  = CLIP(R:Name) & R:Cnt         
      R:Cnt         += 1
   .
   
   IF PRI:PID = 0
      MESSAGE('could not copy')
      R:Done = FALSE
   ELSE
      CLEAR(PRIL:Record, -1)
      PRIL:PID = Queue:Browse:1.PRI:PID
      SET(PRIL:FKey_PID, PRIL:FKey_PID)
      LOOP
         NEXT(PrintLayout)         
         IF PRIL:PID ~= Queue:Browse:1.PRI:PID OR ERRORCODE()
            BREAK
         .
         
         PRIL_A:Record :=: PRIL:Record
         
         PRIL_A:PLID = 0
         PRIL_A:PID = PRI:PID
         IF Access:PrintLayoutAlias.Insert() ~= Level:Benign
            MESSAGE('problem insert')
            R:Done = FALSE
            BREAK
         .            
      .
   .      
   
   PRI:PID = Queue:Browse:1.PRI:PID
   SELECT(Queue:Browse:1, CHOICE(?Browse:1))
   RETURN R:Done      

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! Invoices|Credit Notes|Delivery Notes|Statements
      EXECUTE PRI:PrintType + 1
         LOC:PrintType    = 'Invoices'
         LOC:PrintType    = 'Credit Notes'
         LOC:PrintType    = 'Delivery Notes'
         LOC:PrintType    = 'Statements'
         LOC:PrintType    = 'Invoices - Backup'
         LOC:PrintType    = 'Credit Notes - Backup'
         LOC:PrintType    = 'Delivery Notes - Backup'
         LOC:PrintType    = 'Statements - Backup'
      .
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Prints PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Add_Group        GROUP,PRE(L_AG)                       !
Idx                  ULONG                                 !
FieldName            STRING(50)                            !Field Name
                     END                                   !
LOC:Alignment        STRING(20)                            !Alignment
BRW2::View:Browse    VIEW(PrintLayout)
                       PROJECT(PRIL:XPos)
                       PROJECT(PRIL:YPos)
                       PROJECT(PRIL:Length)
                       PROJECT(PRIL:Repeating)
                       PROJECT(PRIL:PLID)
                       PROJECT(PRIL:Alignment)
                       PROJECT(PRIL:PID)
                       PROJECT(PRIL:PFID)
                       JOIN(PRIF:PKey_PFID,PRIL:PFID)
                         PROJECT(PRIF:FieldName)
                         PROJECT(PRIF:PFID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
PRIF:FieldName         LIKE(PRIF:FieldName)           !List box control field - type derived from field
PRIL:XPos              LIKE(PRIL:XPos)                !List box control field - type derived from field
PRIL:YPos              LIKE(PRIL:YPos)                !List box control field - type derived from field
PRIL:Length            LIKE(PRIL:Length)              !List box control field - type derived from field
LOC:Alignment          LIKE(LOC:Alignment)            !List box control field - type derived from local data
PRIL:Repeating         LIKE(PRIL:Repeating)           !List box control field - type derived from field
PRIL:Repeating_Icon    LONG                           !Entry's icon ID
PRIL:PLID              LIKE(PRIL:PLID)                !List box control field - type derived from field
PRIL:Alignment         LIKE(PRIL:Alignment)           !Browse hot field - type derived from field
PRIL:PID               LIKE(PRIL:PID)                 !Browse key field - type derived from field
PRIF:PFID              LIKE(PRIF:PFID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::PRI:Record  LIKE(PRI:RECORD),THREAD
QuickWindow          WINDOW('Form Prints'),AT(,,273,256),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('UpdatePrints'),SYSTEM
                       SHEET,AT(4,4,265,232),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Print Name:'),AT(9,24),USE(?PRI:PrintName:Prompt),TRN
                           ENTRY(@s50),AT(61,24,204,10),USE(PRI:PrintName),MSG('Name'),TIP('Name')
                           PROMPT('Print Type:'),AT(9,48),USE(?PRI:PrintType:Prompt),TRN
                           LIST,AT(61,48,100,10),USE(PRI:PrintType),DROP(5),FROM('Invoices|#0|Credit Notes|#1|Deli' & |
  'very Notes|#2|Statements|#3|Invoices - Backup|#4|Credit Notes - Backup|#5|Delivery N' & |
  'otes - Backup|#6|Statements - Backup|#7')
                           PROMPT('Page Lines:'),AT(9,78),USE(?PRI:PageLines:Prompt),TRN
                           SPIN(@n_5),AT(61,78,42,10),USE(PRI:PageLines),RIGHT(1),MSG('Lines on this page'),TIP('Lines on this page')
                           STRING('eg. Invoices are 48 lines, Statements are 65'),AT(109,78,158,10),USE(?String1),TRN
                           PROMPT('Page Columns:'),AT(9,96),USE(?PRI:PageColumns:Prompt),TRN
                           SPIN(@n7),AT(61,96,42,10),USE(PRI:PageColumns),RIGHT(1),MSG('Columns on a page'),TIP('Columns on a page')
                           STRING('eg. Invoices and Statements are 80'),AT(109,96,158,10),USE(?String1:2),TRN
                         END
                         TAB('&2) Print Layout'),USE(?Tab:2)
                           LIST,AT(9,20,257,195),USE(?Browse:2),HVSCROLL,FORMAT('100L(2)|M~Field Name~C(0)@s50@28R' & |
  '(2)|M~X Pos~C(0)@n_5@28R(2)|M~Y Pos~C(0)@n_5@28R(2)|M~Length~C(0)@n_5@38L(2)|M~Align' & |
  'ment~C(0)@s20@20L(1)|MI~Repeating~@n3@30R(2)|M~PLID~C(0)@n_10@'),FROM(Queue:Browse:2),IMM, |
  MSG('Browsing the PrintLayout file')
                           BUTTON('Create Fields / Layout'),AT(9,218,,14),USE(?Button_Create),TIP('Options for Fie' & |
  'lds or Fields & Layout')
                           BUTTON('&Insert'),AT(110,218,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(162,218,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(217,218,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(168,238,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(220,238,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(92,238,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('Desig&ner'),AT(4,238,,14),USE(?Button_Designer),LEFT,ICON('CHANGEPG.ICO'),FLAT
                     END

BRW2::LastSortOrder       BYTE
BRW2::SortHeader  CLASS(SortHeaderClassType) !Declare SortHeader Class
QueueResorted          PROCEDURE(STRING pString),VIRTUAL
                  END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Create_Fields_Layout            ROUTINE
    ! Invoices|Credit Notes|Delivery Notes
    CASE MESSAGE('Create the Fields or the Fields & Layout for this Print?', 'Option', ICON:Question, 'Fields|Both', 1)
    OF 1
       DO Add_Print_Fields
    OF 2
       DO Add_Print_Fields
       DO Add_Layout
    .

    BRW2.ResetFromFile()
    EXIT
Add_Print_Fields                    ROUTINE
    LOGOUT(1, PrintFields)

    L_AG:Idx    = 0
    LOOP
       L_AG:Idx += 1

       CLEAR(PRIF:Record)
       IF Access:PrintFields.TryPrimeAutoInc() = LEVEL:Benign
          PRIF:PrintType   = PRI:PrintType
          EXECUTE PRI:PrintType                 ! Invoices, Credit Notes, Delivery Notes, Statements
             DO Add_CNote_Fields
             DO Add_DelN_Fields
             DO Add_Statement_Fields
          ELSE
             DO Add_Invoice_Fields
          .

          IF CLIP(PRIF:FieldName) = ''
             Access:PrintFields.CancelAutoInc()
             BREAK
          .

          IF Access:PrintFields.TryInsert() = LEVEL:Benign
             !
          ELSE
             Access:PrintFields.CancelAutoInc()
          .
       ELSE
          BREAK
    .  .

    COMMIT
    EXIT
Add_Layout                            ROUTINE
    LOGOUT(1, PrintLayout)

    L_AG:Idx        = 0
    LOOP
       L_AG:Idx    += 1

       ! Check if we already have this field or not
       CLEAR(PRIL:RECORD)
       EXECUTE PRI:PrintType                 ! Invoices, Credit Notes, Delivery Notes, Statements
          DO Add_CNote_Layout
          DO Add_DelN_Layout
          DO Add_Statement_Layout
       ELSE
          DO Add_Invoice_Layout
       .

       CLEAR(PRIF:Record)
       PRIF:FieldName   = L_AG:FieldName
       IF Access:PrintFields.TryFetch(PRIF:Key_FieldName) = LEVEL:Benign
          PRIL:PFID     = PRIF:PFID
       .

       PRIL:PID         = PRI:PID
       IF Access:PrintLayout.TryFetch(PRIL:SKey_Print_Field) = LEVEL:Benign
          ! We already have this Field in the layout...
          CYCLE
       .


       ! We dont have this field
       CLEAR(PRIL:RECORD)
       IF Access:PrintLayout.TryPrimeAutoInc() = LEVEL:Benign
          EXECUTE PRI:PrintType                 ! Invoices, Credit Notes, Delivery Notes, Statements
             DO Add_CNote_Layout
             DO Add_DelN_Layout
             DO Add_Statement_Layout
          ELSE
             DO Add_Invoice_Layout
          .

          IF CLIP(L_AG:FieldName) = ''
             Access:PrintLayout.CancelAutoInc()
             BREAK
          .

          PRIL:PID          = PRI:PID

          CLEAR(PRIF:Record)
          PRIF:FieldName    = L_AG:FieldName
          IF Access:PrintFields.TryFetch(PRIF:Key_FieldName) = LEVEL:Benign
             PRIL:PFID      = PRIF:PFID
          .

          IF Access:PrintLayout.TryInsert() = LEVEL:Benign
             !
          ELSE
             Access:PrintLayout.CancelAutoInc()
          .
       ELSE
          BREAK
    .  .
    COMMIT
    EXIT
! -----------------------------------------------------------------------------------------------------
Add_Invoice_Fields               ROUTINE
    EXECUTE L_AG:Idx
       PRIF:FieldName    = 'Invoice No.'
       PRIF:FieldName    = 'Branch'
       PRIF:FieldName    = 'Date'
       PRIF:FieldName    = 'For Account'
       PRIF:FieldName    = 'DI No.'
       PRIF:FieldName    = 'Manifest No.'
       PRIF:FieldName    = 'Shipper'
       PRIF:FieldName    = 'Ship Line 1'
       PRIF:FieldName    = 'Ship Line 2'
       PRIF:FieldName    = 'Ship Suburb'
       PRIF:FieldName    = 'Ship Post'
       PRIF:FieldName    = 'Consignee'
       PRIF:FieldName    = 'Con Line 1'
       PRIF:FieldName    = 'Con Line 2'
       PRIF:FieldName    = 'Con Suburb'
       PRIF:FieldName    = 'Con Post'
       PRIF:FieldName    = 'Items Start'
       PRIF:FieldName    = 'Items End'
       PRIF:FieldName    = 'Vol Weight'
       PRIF:FieldName    = 'Act Weight'
       PRIF:FieldName    = 'Debt Name'
       PRIF:FieldName    = 'Debt Line 1'
       PRIF:FieldName    = 'Debt Line 2'
       PRIF:FieldName    = 'Debt Suburb'
       PRIF:FieldName    = 'Debt Post'
       PRIF:FieldName    = 'Debt Vat No.'
       PRIF:FieldName    = 'Debt Ref.'
       PRIF:FieldName    = 'Charge Fuel'
       PRIF:FieldName    = 'Charge Insurance'
       PRIF:FieldName    = 'Charge Docs'
       PRIF:FieldName    = 'Charge Freight'
       PRIF:FieldName    = 'Charge VAT'
       PRIF:FieldName    = 'Charge Total'
       PRIF:FieldName    = 'Copy Invoice'
       PRIF:FieldName    = 'Fuel Surcharge'
       PRIF:FieldName    = 'Invoice Message'
    ELSE
       CLEAR(PRIF:FieldName)
    .
    EXIT
Add_Invoice_Layout              ROUTINE
    CASE L_AG:Idx
    OF 1
       L_AG:FieldName    = 'Invoice No.'
       PRIL:XPos         = 67
       PRIL:YPos         = 4
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 2
       L_AG:FieldName    = 'Branch'
       PRIL:XPos         = 64
       PRIL:YPos         = 5
       PRIL:Length       = 15
       PRIL:Alignment    = 1
    OF 3
       L_AG:FieldName    = 'Date'
       PRIL:XPos         = 4
       PRIL:YPos         = 11
       PRIL:Length       = 8
       PRIL:Alignment    = 1
    OF 4
       L_AG:FieldName    = 'For Account'
       PRIL:XPos         = 16
       PRIL:YPos         = 11
       PRIL:Length       = 24
       PRIL:Alignment    = 0
    OF 5
       L_AG:FieldName    = 'DI No.'
       PRIL:XPos         = 44
       PRIL:YPos         = 11
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 6
       L_AG:FieldName    = 'Manifest No.'
       PRIL:XPos         = 69
       PRIL:YPos         = 11
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 7
       L_AG:FieldName    = 'Shipper'
       PRIL:XPos         = 4
       PRIL:YPos         = 15
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 8
       L_AG:FieldName    = 'Ship Line 1'
       PRIL:XPos         = 4
       PRIL:YPos         = 16
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 9
       L_AG:FieldName    = 'Ship Line 2'
       PRIL:XPos         = 4
       PRIL:YPos         = 17
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 10
       L_AG:FieldName    = 'Ship Suburb'
       PRIL:XPos         = 4
       PRIL:YPos         = 18
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 11
       L_AG:FieldName    = 'Ship Post'
       PRIL:XPos         = 4
       PRIL:YPos         = 19
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 12
       L_AG:FieldName    = 'Consignee'
       PRIL:XPos         = 44
       PRIL:YPos         = 15
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 13
       L_AG:FieldName    = 'Con Line 1'
       PRIL:XPos         = 44
       PRIL:YPos         = 16
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 14
       L_AG:FieldName    = 'Con Line 2'
       PRIL:XPos         = 44
       PRIL:YPos         = 17
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 15
       L_AG:FieldName    = 'Con Suburb'
       PRIL:XPos         = 44
       PRIL:YPos         = 18
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 16
       L_AG:FieldName    = 'Con Post'
       PRIL:XPos         = 44
       PRIL:YPos         = 19
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 17
       L_AG:FieldName    = 'Items Start'
       PRIL:XPos         = 4
       PRIL:YPos         = 23
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 18
       L_AG:FieldName    = 'Items End'
       PRIL:XPos         = 4
       PRIL:YPos         = 31
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 19
       L_AG:FieldName    = 'Vol Weight'
       PRIL:XPos         = 43
       PRIL:YPos         = 32
       PRIL:Length       = 20
       PRIL:Alignment    = 2
    OF 20
       L_AG:FieldName    = 'Act Weight'
       PRIL:XPos         = 43
       PRIL:YPos         = 33
       PRIL:Length       = 20
       PRIL:Alignment    = 2
    OF 21
       L_AG:FieldName    = 'Debt Name'
       PRIL:XPos         = 4
       PRIL:YPos         = 37
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 22
       L_AG:FieldName    = 'Debt Line 1'
       PRIL:XPos         = 4
       PRIL:YPos         = 38
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 23
       L_AG:FieldName    = 'Debt Line 2'
       PRIL:XPos         = 4
       PRIL:YPos         = 39
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 24
       L_AG:FieldName    = 'Debt Suburb'
       PRIL:XPos         = 4
       PRIL:YPos         = 40
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 25
       L_AG:FieldName    = 'Debt Post'
       PRIL:XPos         = 4
       PRIL:YPos         = 41
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 26
       L_AG:FieldName    = 'Debt Vat No.'
       PRIL:XPos         = 16
       PRIL:YPos         = 12  !42
       PRIL:Length       = 19
       PRIL:Alignment    = 2
    OF 27
       L_AG:FieldName    = 'Debt Ref.'
       PRIL:XPos         = 10
       PRIL:YPos         = 42
       PRIL:Length       = 17
       PRIL:Alignment    = 2
    OF 28
       L_AG:FieldName    = 'Charge Fuel'
       PRIL:XPos         = 64
       PRIL:YPos         = 35
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 29
       L_AG:FieldName    = 'Charge Insurance'
       PRIL:XPos         = 64
       PRIL:YPos         = 37
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 30
       L_AG:FieldName    = 'Charge Docs'
       PRIL:XPos         = 64
       PRIL:YPos         = 39
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 31
       L_AG:FieldName    = 'Charge Freight'
       PRIL:XPos         = 64
       PRIL:YPos         = 41
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 32
       L_AG:FieldName    = 'Charge VAT'
       PRIL:XPos         = 64
       PRIL:YPos         = 43
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 33
       L_AG:FieldName    = 'Charge Total'
       PRIL:XPos         = 64
       PRIL:YPos         = 45
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 34
       L_AG:FieldName    = 'Copy Invoice'
       PRIL:XPos         = 67
       PRIL:YPos         = 3
       PRIL:Length       = 12
       PRIL:Alignment    = 1
    OF 35
       L_AG:FieldName    = 'Fuel Surcharge'
       PRIL:XPos         = 42
       PRIL:YPos         = 35
       PRIL:Length       = 14
       PRIL:Alignment    = 0
    OF 36
       L_AG:FieldName    = 'Invoice Message'
       PRIL:XPos         = 5
       PRIL:YPos         = 34
       PRIL:Length       = 60
       PRIL:Alignment    = 0
    ELSE
       CLEAR(L_AG:FieldName)
    .
    EXIT
! -----------------------------------------------------------------------------------------------------
Add_CNote_Fields                 ROUTINE
    EXECUTE L_AG:Idx
       PRIF:FieldName    = 'Credit Note No.'                                    ! 1
       PRIF:FieldName    = 'Branch'
       PRIF:FieldName    = 'Date'
       PRIF:FieldName    = 'For Account'
       PRIF:FieldName    = 'Debt Name'                                          ! 5
       PRIF:FieldName    = 'Debt Line 1'
       PRIF:FieldName    = 'Debt Line 2'
       PRIF:FieldName    = 'Debt Suburb'
       PRIF:FieldName    = 'Debt Post'
       PRIF:FieldName    = 'Debt Vat No.'                                       ! 10
       PRIF:FieldName    = 'Reason'
       PRIF:FieldName    = 'Reason Line 1'
       PRIF:FieldName    = 'Reason Line 2'
       PRIF:FieldName    = 'Reason Line 3'
       PRIF:FieldName    = 'Original Invoice'       ! ikb more required here    ! 15
       PRIF:FieldName    = 'Credit Amt.'
       PRIF:FieldName    = 'Credit VAT'
       PRIF:FieldName    = 'Credit Total'
       PRIF:FieldName    = 'Credit Note Text'       ! Credit Note
       PRIF:FieldName    = 'Delete Text'            ! XXXXXXXXXX                - 20
       PRIF:FieldName    = 'Invoice Details'
    ELSE
       CLEAR(PRIF:FieldName)
    .
    EXIT
Add_CNote_Layout                ROUTINE
    CASE L_AG:Idx
    OF 1
       L_AG:FieldName    = 'Credit Note No.'
       PRIL:XPos         = 67
       PRIL:YPos         = 4
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 2
       L_AG:FieldName    = 'Branch'
       PRIL:XPos         = 64
       PRIL:YPos         = 5
       PRIL:Length       = 15
       PRIL:Alignment    = 1
    OF 3
       L_AG:FieldName    = 'Date'
       PRIL:XPos         = 4
       PRIL:YPos         = 11
       PRIL:Length       = 8
       PRIL:Alignment    = 1
    OF 4
       L_AG:FieldName    = 'For Account'
       PRIL:XPos         = 16
       PRIL:YPos         = 11
       PRIL:Length       = 24
       PRIL:Alignment    = 0

    OF 5
       L_AG:FieldName    = 'Debt Name'
       PRIL:XPos         = 4
       PRIL:YPos         = 37
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 6
       L_AG:FieldName    = 'Debt Line 1'
       PRIL:XPos         = 4
       PRIL:YPos         = 38
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 7
       L_AG:FieldName    = 'Debt Line 2'
       PRIL:XPos         = 4
       PRIL:YPos         = 39
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 8
       L_AG:FieldName    = 'Debt Suburb'
       PRIL:XPos         = 4
       PRIL:YPos         = 40
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 9
       L_AG:FieldName    = 'Debt Post'
       PRIL:XPos         = 4
       PRIL:YPos         = 41
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 10
       L_AG:FieldName    = 'Debt Vat No.'
       PRIL:XPos         = 4
       PRIL:YPos         = 42
       PRIL:Length       = 19
       PRIL:Alignment    = 2

    OF 11
       L_AG:FieldName    = 'Reason'
       PRIL:XPos         = 4
       PRIL:YPos         = 24
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 12
       L_AG:FieldName    = 'Reason Line 1'
       PRIL:XPos         = 4
       PRIL:YPos         = 25
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 13
       L_AG:FieldName    = 'Reason Line 2'
       PRIL:XPos         = 4
       PRIL:YPos         = 26
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 14
       L_AG:FieldName    = 'Reason Line 3'
       PRIL:XPos         = 4
       PRIL:YPos         = 28
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 15
       L_AG:FieldName    = 'Original Invoice'     ! ikb more required here
       PRIL:XPos         = 4
       PRIL:YPos         = 30
       PRIL:Length       = 72
       PRIL:Alignment    = 0

    OF 16
       L_AG:FieldName    = 'Credit Amt.'
       PRIL:XPos         = 64
       PRIL:YPos         = 41
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 17
       L_AG:FieldName    = 'Credit VAT'
       PRIL:XPos         = 64
       PRIL:YPos         = 43
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 18
       L_AG:FieldName    = 'Credit Total'
       PRIL:XPos         = 64
       PRIL:YPos         = 45
       PRIL:Length       = 15
       PRIL:Alignment    = 2
    OF 19
       L_AG:FieldName    = 'Credit Note Text'
       PRIL:XPos         = 67
       PRIL:YPos         = 1
       PRIL:Length       = 12
       PRIL:Alignment    = 1
    OF 20
       L_AG:FieldName    = 'Delete Text'
       PRIL:XPos         = 60
       PRIL:YPos         = 2
       PRIL:Length       = 20
       PRIL:Alignment    = 0
    OF 21
       L_AG:FieldName    = 'Invoice Details'
       PRIL:XPos         = 4
       PRIL:YPos         = 27
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    ELSE
       CLEAR(L_AG:FieldName)
    .
    EXIT
! -----------------------------------------------------------------------------------------------------
Add_DelN_Fields                   ROUTINE
    EXECUTE L_AG:Idx
       PRIF:FieldName    = 'Invoice No.'            ! 1
       PRIF:FieldName    = 'Branch'
       PRIF:FieldName    = 'Date'
       PRIF:FieldName    = 'For Account'
       PRIF:FieldName    = 'DI No.'
       PRIF:FieldName    = 'Manifest No.'
       PRIF:FieldName    = 'Shipper'
       PRIF:FieldName    = 'Ship Line 1'
       PRIF:FieldName    = 'Ship Line 2'
       PRIF:FieldName    = 'Ship Suburb'            ! 10
       PRIF:FieldName    = 'Ship Post'
       PRIF:FieldName    = 'Consignee'
       PRIF:FieldName    = 'Con Line 1'
       PRIF:FieldName    = 'Con Line 2'
       PRIF:FieldName    = 'Con Suburb'
       PRIF:FieldName    = 'Con Post'
       PRIF:FieldName    = 'Items Start'
       PRIF:FieldName    = 'Items End'
       PRIF:FieldName    = 'Vol Weight'
       PRIF:FieldName    = 'Act Weight'             ! 20
       PRIF:FieldName    = 'Spec. Inst. 1'          ! 21
       PRIF:FieldName    = 'Spec. Inst. 2'          ! 22
       PRIF:FieldName    = 'Spec. Inst. 3'          ! 23
       PRIF:FieldName    = 'Debt Ref.'
    ELSE
       CLEAR(PRIF:FieldName)
    .
    EXIT



!       PRIF:FieldName    = 'Debt Name'
!       PRIF:FieldName    = 'Debt Line 1'
!       PRIF:FieldName    = 'Debt Line 2'
!       PRIF:FieldName    = 'Debt Suburb'
!       PRIF:FieldName    = 'Debt Post'
!       PRIF:FieldName    = 'Debt Vat No.'
!       PRIF:FieldName    = 'Charge Fuel'
!       PRIF:FieldName    = 'Charge Insurance'
!       PRIF:FieldName    = 'Charge Docs'
!       PRIF:FieldName    = 'Charge Freight'
!       PRIF:FieldName    = 'Charge VAT'
!       PRIF:FieldName    = 'Charge Total'
!       PRIF:FieldName    = 'Copy Invoice'
Add_DelN_Layout                 ROUTINE
    CASE L_AG:Idx
    OF 1
       L_AG:FieldName    = 'Invoice No.'
       PRIL:XPos         = 67
       PRIL:YPos         = 4
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 2
       L_AG:FieldName    = 'Branch'
       PRIL:XPos         = 64
       PRIL:YPos         = 5
       PRIL:Length       = 15
       PRIL:Alignment    = 1
    OF 3
       L_AG:FieldName    = 'Date'
       PRIL:XPos         = 4
       PRIL:YPos         = 11
       PRIL:Length       = 8
       PRIL:Alignment    = 1
    OF 4
       L_AG:FieldName    = 'For Account'
       PRIL:XPos         = 16
       PRIL:YPos         = 11
       PRIL:Length       = 24
       PRIL:Alignment    = 0
    OF 5
       L_AG:FieldName    = 'DI No.'
       PRIL:XPos         = 44
       PRIL:YPos         = 11
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 6
       L_AG:FieldName    = 'Manifest No.'
       PRIL:XPos         = 69
       PRIL:YPos         = 11
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 7
       L_AG:FieldName    = 'Shipper'
       PRIL:XPos         = 4
       PRIL:YPos         = 15
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 8
       L_AG:FieldName    = 'Ship Line 1'
       PRIL:XPos         = 4
       PRIL:YPos         = 16
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 9
       L_AG:FieldName    = 'Ship Line 2'
       PRIL:XPos         = 4
       PRIL:YPos         = 17
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 10
       L_AG:FieldName    = 'Ship Suburb'
       PRIL:XPos         = 4
       PRIL:YPos         = 18
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 11
       L_AG:FieldName    = 'Ship Post'
       PRIL:XPos         = 4
       PRIL:YPos         = 19
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 12
       L_AG:FieldName    = 'Consignee'
       PRIL:XPos         = 44
       PRIL:YPos         = 15
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 13
       L_AG:FieldName    = 'Con Line 1'
       PRIL:XPos         = 44
       PRIL:YPos         = 16
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 14
       L_AG:FieldName    = 'Con Line 2'
       PRIL:XPos         = 44
       PRIL:YPos         = 17
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 15
       L_AG:FieldName    = 'Con Suburb'
       PRIL:XPos         = 44
       PRIL:YPos         = 18
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 16
       L_AG:FieldName    = 'Con Post'
       PRIL:XPos         = 44
       PRIL:YPos         = 19
       PRIL:Length       = 34
       PRIL:Alignment    = 0
    OF 17
       L_AG:FieldName    = 'Items Start'
       PRIL:XPos         = 4
       PRIL:YPos         = 23
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 18
       L_AG:FieldName    = 'Items End'
       PRIL:XPos         = 4
       PRIL:YPos         = 31
       PRIL:Length       = 72
       PRIL:Alignment    = 0
    OF 19
       L_AG:FieldName    = 'Vol Weight'
       PRIL:XPos         = 43
       PRIL:YPos         = 32
       PRIL:Length       = 20
       PRIL:Alignment    = 2
    OF 20
       L_AG:FieldName    = 'Act Weight'
       PRIL:XPos         = 43
       PRIL:YPos         = 33
       PRIL:Length       = 20
       PRIL:Alignment    = 2
    OF 21
       L_AG:FieldName    = 'Spec. Inst. 1'
       PRIL:XPos         = 3
       PRIL:YPos         = 32
       PRIL:Length       = 35
       PRIL:Alignment    = 0
    OF 22
       L_AG:FieldName    = 'Spec. Inst. 2'
       PRIL:XPos         = 3
       PRIL:YPos         = 33
       PRIL:Length       = 35
       PRIL:Alignment    = 0
    OF 23
       L_AG:FieldName    = 'Spec. Inst. 3'
       PRIL:XPos         = 3
       PRIL:YPos         = 34
       PRIL:Length       = 35
       PRIL:Alignment    = 0
    OF 24
       L_AG:FieldName    = 'Debt Ref.'
       PRIL:XPos         = 24
       PRIL:YPos         = 42
       PRIL:Length       = 17
       PRIL:Alignment    = 2
    ELSE
       CLEAR(L_AG:FieldName)
    .
    EXIT









!    OF 21
!       L_AG:FieldName    = 'Debt Name'
!       PRIL:XPos         = 4
!       PRIL:YPos         = 37
!       PRIL:Length       = 34
!       PRIL:Alignment    = 0
!    OF 22
!       L_AG:FieldName    = 'Debt Line 1'
!       PRIL:XPos         = 4
!       PRIL:YPos         = 38
!       PRIL:Length       = 34
!       PRIL:Alignment    = 0
!    OF 23
!       L_AG:FieldName    = 'Debt Line 2'
!       PRIL:XPos         = 4
!       PRIL:YPos         = 39
!       PRIL:Length       = 34
!       PRIL:Alignment    = 0
!    OF 24
!       L_AG:FieldName    = 'Debt Suburb'
!       PRIL:XPos         = 4
!       PRIL:YPos         = 40
!       PRIL:Length       = 34
!       PRIL:Alignment    = 0
!    OF 25
!       L_AG:FieldName    = 'Debt Post'
!       PRIL:XPos         = 4
!       PRIL:YPos         = 41
!       PRIL:Length       = 34
!       PRIL:Alignment    = 0
!    OF 26
!       L_AG:FieldName    = 'Debt Vat No.'
!       PRIL:XPos         = 4
!       PRIL:YPos         = 42
!       PRIL:Length       = 19
!       PRIL:Alignment    = 2
!    OF 28
!       L_AG:FieldName    = 'Charge Fuel'
!       PRIL:XPos         = 64
!       PRIL:YPos         = 35
!       PRIL:Length       = 15
!       PRIL:Alignment    = 2
!    OF 29
!       L_AG:FieldName    = 'Charge Insurance'
!       PRIL:XPos         = 64
!       PRIL:YPos         = 37
!       PRIL:Length       = 15
!       PRIL:Alignment    = 2
!    OF 30
!       L_AG:FieldName    = 'Charge Docs'
!       PRIL:XPos         = 64
!       PRIL:YPos         = 39
!       PRIL:Length       = 15
!       PRIL:Alignment    = 2
!    OF 31
!       L_AG:FieldName    = 'Charge Freight'
!       PRIL:XPos         = 64
!       PRIL:YPos         = 41
!       PRIL:Length       = 15
!       PRIL:Alignment    = 2
!    OF 32
!       L_AG:FieldName    = 'Charge VAT'
!       PRIL:XPos         = 64
!       PRIL:YPos         = 43
!       PRIL:Length       = 15
!       PRIL:Alignment    = 2
!    OF 33
!       L_AG:FieldName    = 'Charge Total'
!       PRIL:XPos         = 64
!       PRIL:YPos         = 45
!       PRIL:Length       = 15
!       PRIL:Alignment    = 2
!    OF 34
!       L_AG:FieldName    = 'Copy Invoice'
!       PRIL:XPos         = 67
!       PRIL:YPos         = 3
!       PRIL:Length       = 12
!       PRIL:Alignment    = 1
!    OF 35
!       L_AG:FieldName    = 'Fuel Surcharge'
!       PRIL:XPos         = 42
!       PRIL:YPos         = 35
!       PRIL:Length       = 14
!       PRIL:Alignment    = 0
! -----------------------------------------------------------------------------------------------------
Add_Statement_Fields               ROUTINE
!   Duplicated for Please Post to
!       PRIF:FieldName    = 'F.B.N. Transport'
!       PRIF:FieldName    = 'P.O.Box 1405'
!       PRIF:FieldName    = 'Hillcrest  3650'

    EXECUTE L_AG:Idx
       PRIF:FieldName    = 'Statement No.'
       PRIF:FieldName    = 'FBN Name'               ! F.B.N. Transport'
       PRIF:FieldName    = 'FBN Line 1'             ! P.O.Box 1405'
       PRIF:FieldName    = 'FBN Line 2'             ! Suburb
       PRIF:FieldName    = 'FBN Line 3'             ! Postal Code
       PRIF:FieldName    = 'VAT No.'
       PRIF:FieldName    = 'Page No.'
       PRIF:FieldName    = 'Page No. Val'
       PRIF:FieldName    = 'FBN Phone'              ! : 031 205 1705'
       PRIF:FieldName    = 'FBN Fax'                ! Fax: 031 205 2098'
       PRIF:FieldName    = 'Client Name Left'
       PRIF:FieldName    = 'Client Line 1'
       PRIF:FieldName    = 'Client Line 2'
       PRIF:FieldName    = 'Suburb'
       PRIF:FieldName    = 'Postal'
       PRIF:FieldName    = 'Client VAT No.'
       PRIF:FieldName    = 'Client Name Right'
       PRIF:FieldName    = 'Client No.'             ! Client No.   *** new
       PRIF:FieldName    = 'Please Post To'
       PRIF:FieldName    = 'FBN 2 Name'               ! F.B.N. Transport'
       PRIF:FieldName    = 'FBN 2 Line 1'             ! P.O.Box 1405'
       PRIF:FieldName    = 'FBN 2 Line 2'             ! Suburb
       PRIF:FieldName    = 'FBN 2 Line 3'             ! Postal Code
       PRIF:FieldName    = 'FBN Line 4'             ! Croft & Johnstone Rd.'
       PRIF:FieldName    = 'FBN Line 5'             ! 4057'
       PRIF:FieldName    = 'Statement for Period Ending:'
       PRIF:FieldName    = 'Statement Date'
       PRIF:FieldName    = 'Print Date'
       PRIF:FieldName    = 'Inv Date Label'         ! Item labels
       PRIF:FieldName    = 'Inv No. Label'
       PRIF:FieldName    = 'DI No Label'
       PRIF:FieldName    = 'Debit Label'
       PRIF:FieldName    = 'Credit Label'
       PRIF:FieldName    = 'Inv No. 2 Label'
       PRIF:FieldName    = 'Amount Label'
       PRIF:FieldName    = 'Items Start'
       PRIF:FieldName    = 'Invoice Date'
       PRIF:FieldName    = 'Invoice No.'
       PRIF:FieldName    = 'DI No'
       PRIF:FieldName    = 'Debit'
       PRIF:FieldName    = 'Credit'
       PRIF:FieldName    = 'Invoice No. 2'
       PRIF:FieldName    = 'Amount'
       PRIF:FieldName    = 'Items End'
       PRIF:FieldName    = '90 Days'
       PRIF:FieldName    = '60 Days'
       PRIF:FieldName    = '30 Days'
       PRIF:FieldName    = 'Current'
       PRIF:FieldName    = 'Total'
       PRIF:FieldName    = '90 Days Val.'
       PRIF:FieldName    = '60 Days Val.'
       PRIF:FieldName    = '30 Days Val.'
       PRIF:FieldName    = 'Current Val.'
       PRIF:FieldName    = 'Total Val.'
       PRIF:FieldName    = 'Total Due'
       PRIF:FieldName    = 'Total Due Val'
       PRIF:FieldName    = 'Last Period Payments'
       PRIF:FieldName    = 'Last Period Payments Val'
       PRIF:FieldName    = 'Amount Paid'
       PRIF:FieldName    = 'Amount Paid Val'
       PRIF:FieldName    = 'Comment Line'
       PRIF:FieldName    = 'Comment Line 2'
    ELSE
       CLEAR(PRIF:FieldName)
    .
    EXIT
Add_Statement_Layout              ROUTINE
    CASE L_AG:Idx
    OF 1
       L_AG:FieldName    = 'Statement No.'
       PRIL:XPos         = 65
       PRIL:YPos         = 6
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 2
       L_AG:FieldName    = 'FBN Name'               ! F.B.N. Transport'
       PRIL:XPos         = 5
       PRIL:YPos         = 5
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 3
       L_AG:FieldName    = 'FBN Line 1'             ! P.O.Box 1405'
       PRIL:XPos         = 5
       PRIL:YPos         = 6
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 4
       L_AG:FieldName    = 'FBN Line 2'             ! Suburb
       PRIL:XPos         = 5
       PRIL:YPos         = 7
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 5
       L_AG:FieldName    = 'FBN Line 3'             ! Postal Code
       PRIL:XPos         = 5
       PRIL:YPos         = 8
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 6
       L_AG:FieldName    = 'VAT No.'
       PRIL:XPos         = 67
       PRIL:YPos         = 9
       PRIL:Length       = 12
       PRIL:Alignment    = 0
    OF 7
       L_AG:FieldName    = 'Page No.'
       PRIL:XPos         = 58
       PRIL:YPos         = 8
       PRIL:Length       = 10
       PRIL:Alignment    = 0
    OF 8
       L_AG:FieldName    = 'Page No. Val'
       PRIL:XPos         = 67
       PRIL:YPos         = 8
       PRIL:Length       = 10
       PRIL:Alignment    = 0
    OF 9
       L_AG:FieldName    = 'FBN Phone'              ! : 031 205 1705'
       PRIL:XPos         = 4
       PRIL:YPos         = 9
       PRIL:Length       = 24
       PRIL:Alignment    = 1
    OF 10
       L_AG:FieldName    = 'FBN Fax'                ! Fax: 031 205 2098'
       PRIL:XPos         = 24
       PRIL:YPos         = 9
       PRIL:Length       = 24
       PRIL:Alignment    = 1
    OF 11
       L_AG:FieldName    = 'Client Name Left'
       PRIL:XPos         = 7
       PRIL:YPos         = 12
       PRIL:Length       = 40
       PRIL:Alignment    = 0
    OF 12
       L_AG:FieldName    = 'Client Line 1'
       PRIL:XPos         = 7
       PRIL:YPos         = 13
       PRIL:Length       = 40
       PRIL:Alignment    = 0
    OF 13
       L_AG:FieldName    = 'Client Line 2'
       PRIL:XPos         = 7
       PRIL:YPos         = 14
       PRIL:Length       = 40
       PRIL:Alignment    = 0
    OF 14
       L_AG:FieldName    = 'Suburb'
       PRIL:XPos         = 7
       PRIL:YPos         = 15
       PRIL:Length       = 40
       PRIL:Alignment    = 0
    OF 15
       L_AG:FieldName    = 'Postal'
       PRIL:XPos         = 7
       PRIL:YPos         = 16
       PRIL:Length       = 40
       PRIL:Alignment    = 0
    OF 16
       L_AG:FieldName    = 'Client VAT No.'
       PRIL:XPos         = 7
       PRIL:YPos         = 17
       PRIL:Length       = 40
       PRIL:Alignment    = 0
    OF 17
       L_AG:FieldName    = 'Client Name Right'
       PRIL:XPos         = 52
       PRIL:YPos         = 11
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 18
       L_AG:FieldName    = 'Please Post To'
       PRIL:XPos         = 52
       PRIL:YPos         = 14
       PRIL:Length       = 30
       PRIL:Alignment    = 0

    ! Note: see 50+ for lines from here
    OF 19
       L_AG:FieldName    = 'FBN Line 4'             ! Croft & Johnstone Rd.'
       PRIL:XPos         = 52
       PRIL:YPos         = 18
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 20
       L_AG:FieldName    = 'FBN Line 5'             ! 4057'
       PRIL:XPos         = 52
       PRIL:YPos         = 19
       PRIL:Length       = 10
       PRIL:Alignment    = 0



    OF 21
       L_AG:FieldName    = 'Statement for Period Ending:'
       PRIL:XPos         = 4
       PRIL:YPos         = 21
       PRIL:Length       = 28
       PRIL:Alignment    = 1
    OF 22
       L_AG:FieldName    = 'Statement Date'
       PRIL:XPos         = 34
       PRIL:YPos         = 21
       PRIL:Length       = 10
       PRIL:Alignment    = 0
    OF 23
       L_AG:FieldName    = 'Print Date'
       PRIL:XPos         = 62
       PRIL:YPos         = 21
       PRIL:Length       = 20
       PRIL:Alignment    = 0


    ! Note: See 50 + for label entries for columns
    ! Items
    OF 24
       L_AG:FieldName    = 'Items Start'
       PRIL:XPos         = 2
       PRIL:YPos         = 25
       PRIL:Length       = 74
       PRIL:Alignment    = 0
       PRIL:Repeating    = 1

    OF 25
       L_AG:FieldName    = 'Invoice Date'           ! These must be set before Items Start (ie < YPos)
       PRIL:XPos         = 1
       PRIL:YPos         = 0
       PRIL:Length       = 8
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 26
       L_AG:FieldName    = 'Invoice No.'
       PRIL:XPos         = 12
       PRIL:YPos         = 0
       PRIL:Length       = 10
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 27
       L_AG:FieldName    = 'DI No'
       PRIL:XPos         = 23
       PRIL:YPos         = 0
       PRIL:Length       = 10
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 28
       L_AG:FieldName    = 'Debit'
       PRIL:XPos         = 34
       PRIL:YPos         = 0
       PRIL:Length       = 11
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 29
       L_AG:FieldName    = 'Credit'
       PRIL:XPos         = 46
       PRIL:YPos         = 0
       PRIL:Length       = 11
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 30
       L_AG:FieldName    = 'Invoice No. 2'          ! Required????
       PRIL:XPos         = 57
       PRIL:YPos         = 0
       PRIL:Length       = 10
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 31
       L_AG:FieldName    = 'Amount'
       PRIL:XPos         = 67
       PRIL:YPos         = 0
       PRIL:Length       = 12
       PRIL:Alignment    = 2
       PRIL:Repeating    = 1
    OF 32
       L_AG:FieldName    = 'Items End'
       PRIL:XPos         = 0
       PRIL:YPos         = 53
       PRIL:Length       = 0
       PRIL:Alignment    = 0
       PRIL:Repeating    = 1

    OF 33
       L_AG:FieldName    = '90 Days'
       PRIL:XPos         = 2
       PRIL:YPos         = 56
       PRIL:Length       = 8
       PRIL:Alignment    = 2
    OF 34
       L_AG:FieldName    = '60 Days'
       PRIL:XPos         = 12
       PRIL:YPos         = 56
       PRIL:Length       = 8
       PRIL:Alignment    = 2
    OF 35
       L_AG:FieldName    = '30 Days'
       PRIL:XPos         = 22
       PRIL:YPos         = 56
       PRIL:Length       = 8
       PRIL:Alignment    = 2
    OF 36
       L_AG:FieldName    = 'Current'
       PRIL:XPos         = 32
       PRIL:YPos         = 56
       PRIL:Length       = 8
       PRIL:Alignment    = 2
    OF 37
       L_AG:FieldName    = 'Total'
       PRIL:XPos         = 42
       PRIL:YPos         = 56
       PRIL:Length       = 8
       PRIL:Alignment    = 2
    OF 38
       L_AG:FieldName    = '90 Days Val.'
       PRIL:XPos         = 2
       PRIL:YPos         = 58
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 39
       L_AG:FieldName    = '60 Days Val.'
       PRIL:XPos         = 12
       PRIL:YPos         = 58
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 40
       L_AG:FieldName    = '30 Days Val.'
       PRIL:XPos         = 22
       PRIL:YPos         = 58
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 41
       L_AG:FieldName    = 'Current Val.'
       PRIL:XPos         = 32
       PRIL:YPos         = 58
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 42
       L_AG:FieldName    = 'Total Val.'
       PRIL:XPos         = 42
       PRIL:YPos         = 58
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 43
       L_AG:FieldName    = 'Total Due'
       PRIL:XPos         = 56
       PRIL:YPos         = 58
       PRIL:Length       = 10
       PRIL:Alignment    = 2
    OF 44
       L_AG:FieldName    = 'Total Due Val'
       PRIL:XPos         = 67
       PRIL:YPos         = 58
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 45
       L_AG:FieldName    = 'Last Period Payments'
       PRIL:XPos         = 2
       PRIL:YPos         = 61
       PRIL:Length       = 25
       PRIL:Alignment    = 0
    OF 46
       L_AG:FieldName    = 'Last Period Payments Val'
       PRIL:XPos         = 28
       PRIL:YPos         = 61
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 47
       L_AG:FieldName    = 'Amount Paid'
       PRIL:XPos         = 58
       PRIL:YPos         = 61
       PRIL:Length       = 12
       PRIL:Alignment    = 0
    OF 48
       L_AG:FieldName    = 'Amount Paid Val'
       PRIL:XPos         = 70
       PRIL:YPos         = 61
       PRIL:Length       = 12
       PRIL:Alignment    = 2
    OF 49
       L_AG:FieldName    = 'Comment Line'
       PRIL:XPos         = 2
       PRIL:YPos         = 62
       PRIL:Length       = 48
       PRIL:Alignment    = 0
    OF 50
       L_AG:FieldName    = 'Comment Line 2'
       PRIL:XPos         = 2
       PRIL:YPos         = 63
       PRIL:Length       = 48
       PRIL:Alignment    = 0

    OF 51
       L_AG:FieldName    = 'FBN 2 Name'             ! P.O.Box 1405'
       PRIL:XPos         = 52
       PRIL:YPos         = 15
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 52
       L_AG:FieldName    = 'FBN 2 Line 1'             ! P.O.Box 1405'
       PRIL:XPos         = 52
       PRIL:YPos         = 16
       PRIL:Length       = 30
       PRIL:Alignment    = 0
    OF 53
       L_AG:FieldName    = 'FBN 2 Line 2'             ! Suburb
       PRIL:XPos         = 52
       PRIL:YPos         = 17
       PRIL:Length       = 15
       PRIL:Alignment    = 0
    OF 54
       L_AG:FieldName    = 'FBN 2 Line 3'             ! Postal Code
       PRIL:XPos         = 67
       PRIL:YPos         = 17
       PRIL:Length       = 15
       PRIL:Alignment    = 2



    OF 55
       L_AG:FieldName    = 'Inv Date Label'
       PRIL:XPos         = 2
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 56
       L_AG:FieldName    = 'Inv No. Label'
       PRIL:XPos         = 12
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 57
       L_AG:FieldName    = 'DI No Label'
       PRIL:XPos         = 22
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 58
       L_AG:FieldName    = 'Debit Label'
       PRIL:XPos         = 34
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 59
       L_AG:FieldName    = 'Credit Label'
       PRIL:XPos         = 46
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 60
       L_AG:FieldName    = 'Inv No. 2 Label'
       PRIL:XPos         = 57
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    OF 61
       L_AG:FieldName    = 'Amount Label'
       PRIL:XPos         = 67
       PRIL:YPos         = 23
       PRIL:Length       = 10
       PRIL:Alignment    = 1

    OF 62
       L_AG:FieldName    = 'Client No.'             ! Client No.   *** new
       PRIL:XPos         = 65
       PRIL:YPos         = 5
       PRIL:Length       = 10
       PRIL:Alignment    = 1
    ELSE
       CLEAR(L_AG:FieldName)
    .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Prints Record'
  OF InsertRecord
    ActionMessage = 'Prints Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Prints Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Prints')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PRI:PrintName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Alignment',LOC:Alignment)                      ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Prints)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(PRI:Record,History::PRI:Record)
  SELF.AddHistoryField(?PRI:PrintName,2)
  SELF.AddHistoryField(?PRI:PrintType,5)
  SELF.AddHistoryField(?PRI:PageLines,3)
  SELF.AddHistoryField(?PRI:PageColumns,4)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:PrintLayout.SetOpenRelated()
  Relate:PrintLayout.Open                                  ! File PrintLayout used by this procedure, so make sure it's RelationManager is open
  Access:Prints.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Prints
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:PrintLayout,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?PRI:PrintName{PROP:ReadOnly} = True
    DISABLE(?PRI:PrintType)
    DISABLE(?Button_Create)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    DISABLE(?Button_Designer)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon PRIL:PID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,PRIL:FKey_PID)   ! Add the sort order for PRIL:FKey_PID for sort order 1
  BRW2.AddRange(PRIL:PID,Relate:PrintLayout,Relate:Prints) ! Add file relationship range limit for sort order 1
  BRW2.AppendOrder('+PRIF:FieldName')                      ! Append an additional sort order
  ?Browse:2{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:2{PROP:IconList,2} = '~checkon.ico'
  BRW2.AddField(PRIF:FieldName,BRW2.Q.PRIF:FieldName)      ! Field PRIF:FieldName is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:XPos,BRW2.Q.PRIL:XPos)                ! Field PRIL:XPos is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:YPos,BRW2.Q.PRIL:YPos)                ! Field PRIL:YPos is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:Length,BRW2.Q.PRIL:Length)            ! Field PRIL:Length is a hot field or requires assignment from browse
  BRW2.AddField(LOC:Alignment,BRW2.Q.LOC:Alignment)        ! Field LOC:Alignment is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:Repeating,BRW2.Q.PRIL:Repeating)      ! Field PRIL:Repeating is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:PLID,BRW2.Q.PRIL:PLID)                ! Field PRIL:PLID is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:Alignment,BRW2.Q.PRIL:Alignment)      ! Field PRIL:Alignment is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:PID,BRW2.Q.PRIL:PID)                  ! Field PRIL:PID is a hot field or requires assignment from browse
  BRW2.AddField(PRIF:PFID,BRW2.Q.PRIF:PFID)                ! Field PRIF:PFID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Prints',QuickWindow)                ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1
  SELF.SetAlerts()
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW2::SortHeader.Init(Queue:Browse:2,?Browse:2,'','',BRW2::View:Browse,PRIL:PKey_PLID)
  BRW2::SortHeader.UseSortColors = False
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PrintLayout.Close
  !Kill the Sort Header
  BRW2::SortHeader.Kill()
  END
  IF SELF.Opened
    INIMgr.Update('Update_Prints',QuickWindow)             ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Print_Layout
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW2::SortHeader.SetAlerts()


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Create
      ThisWindow.Update()
          DO Create_Fields_Layout
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    OF ?Button_Designer
      ThisWindow.Update()
      Print_Designer(PRI:PID)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Take Sort Headers Events
  IF BRW2::SortHeader.TakeEvents()
     RETURN Level:Notify
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.SetQueueRecord PROCEDURE

  CODE
      ! Left|Center|Right
      EXECUTE PRIL:Alignment
         LOC:Alignment    = 'Center'
         LOC:Alignment    = 'Right'
      ELSE
         LOC:Alignment    = 'Left'
      .
  PARENT.SetQueueRecord
  
  IF (PRIL:Repeating = 1)
    SELF.Q.PRIL:Repeating_Icon = 2                         ! Set icon from icon list
  ELSE
    SELF.Q.PRIL:Repeating_Icon = 1                         ! Set icon from icon list
  END


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder<>NewOrder THEN
     BRW2::SortHeader.ClearSort()
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

BRW2::SortHeader.QueueResorted       PROCEDURE(STRING pString)
  CODE
    IF pString = ''
       BRW2.RestoreSort()
       BRW2.ResetSort(True)
    ELSE
       BRW2.ReplaceSort(pString)
    END
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Print_Layout PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Field_Name       STRING(50)                            !Field Name
History::PRIL:Record LIKE(PRIL:RECORD),THREAD
QuickWindow          WINDOW('Form Print Layout'),AT(,,188,145),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdatePrintLayout'),SYSTEM
                       SHEET,AT(4,4,180,123),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Field Name:'),AT(9,22),USE(?LOC:Field_Name:Prompt),TRN
                           BUTTON('...'),AT(50,22,12,10),USE(?CallLookup)
                           ENTRY(@s50),AT(66,22,114,10),USE(LOC:Field_Name),MSG('Field Name'),TIP('Field Name')
                           PROMPT('X Pos:'),AT(9,46),USE(?PRIL:XPos:Prompt),TRN
                           SPIN(@n_5),AT(101,46,80,10),USE(PRIL:XPos),RIGHT(1),MSG('X Position'),TIP('X Position')
                           PROMPT('Y Pos:'),AT(9,60),USE(?PRIL:YPos:Prompt),TRN
                           SPIN(@n_5),AT(101,60,80,10),USE(PRIL:YPos),RIGHT(1),MSG('Y Position'),TIP('Y Position')
                           PROMPT('Length:'),AT(9,78),USE(?PRIL:Length:Prompt),TRN
                           SPIN(@n_5),AT(101,78,80,10),USE(PRIL:Length),RIGHT(1)
                           PROMPT('Alignment:'),AT(9,98),USE(?PRIL:Alignment:Prompt),TRN
                           LIST,AT(101,98,80,10),USE(PRIL:Alignment),DROP(5),FROM('Left|#0|Center|#1|Right|#2'),MSG('Alignment'), |
  TIP('Alignment')
                           CHECK(' &Repeating'),AT(101,114),USE(PRIL:Repeating),MSG('Repeating section'),TIP('Repeating section'), |
  TRN
                         END
                       END
                       BUTTON('&OK'),AT(82,130,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(136,130,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,130,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Print_Layout')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Field_Name:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:PrintLayout)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(PRIL:Record,History::PRIL:Record)
  SELF.AddHistoryField(?PRIL:XPos,4)
  SELF.AddHistoryField(?PRIL:YPos,5)
  SELF.AddHistoryField(?PRIL:Length,6)
  SELF.AddHistoryField(?PRIL:Alignment,7)
  SELF.AddHistoryField(?PRIL:Repeating,8)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:PrintFields.SetOpenRelated()
  Relate:PrintFields.Open                                  ! File PrintFields used by this procedure, so make sure it's RelationManager is open
  Access:Prints.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PrintLayout
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?LOC:Field_Name{PROP:ReadOnly} = True
    DISABLE(?PRIL:Alignment)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Print_Layout',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PrintFields.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Print_Layout',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Print_Fields(PRI:PrintType)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      PRIF:FieldName = LOC:Field_Name
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:Field_Name = PRIF:FieldName
        PRIL:PFID = PRIF:PFID
      END
      ThisWindow.Reset(1)
    OF ?LOC:Field_Name
      IF LOC:Field_Name OR ?LOC:Field_Name{PROP:Req}
        PRIF:FieldName = LOC:Field_Name
        IF Access:PrintFields.TryFetch(PRIF:Key_FieldName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:Field_Name = PRIF:FieldName
            PRIL:PFID = PRIF:PFID
          ELSE
            CLEAR(PRIL:PFID)
            SELECT(?LOC:Field_Name)
            CYCLE
          END
        ELSE
          PRIL:PFID = PRIF:PFID
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF PRIL:PFID ~= 0
             PRIF:PFID            = PRIL:PFID
             IF Access:PrintFields.TryFetch(PRIF:PKey_PFID) = LEVEL:Benign
                LOC:Field_Name    = PRIF:FieldName
          .  .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Print_Fields PROCEDURE (p:PrintType)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(PrintLayout)
                       PROJECT(PRIL:XPos)
                       PROJECT(PRIL:YPos)
                       PROJECT(PRIL:Length)
                       PROJECT(PRIL:Alignment)
                       PROJECT(PRIL:Repeating)
                       PROJECT(PRIL:PLID)
                       PROJECT(PRIL:PFID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
PRIL:XPos              LIKE(PRIL:XPos)                !List box control field - type derived from field
PRIL:YPos              LIKE(PRIL:YPos)                !List box control field - type derived from field
PRIL:Length            LIKE(PRIL:Length)              !List box control field - type derived from field
PRIL:Alignment         LIKE(PRIL:Alignment)           !List box control field - type derived from field
PRIL:Repeating         LIKE(PRIL:Repeating)           !List box control field - type derived from field
PRIL:PLID              LIKE(PRIL:PLID)                !Primary key field - type derived from field
PRIL:PFID              LIKE(PRIL:PFID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::PRIF:Record LIKE(PRIF:RECORD),THREAD
QuickWindow          WINDOW('Form Print Fields'),AT(,,273,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdatePrintFields'),SYSTEM
                       SHEET,AT(4,4,265,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Field Name:'),AT(9,22),USE(?PRIF:FieldName:Prompt),TRN
                           ENTRY(@s50),AT(61,22,204,10),USE(PRIF:FieldName),MSG('Field Name'),TIP('Field Name')
                           PROMPT('Print Type:'),AT(9,36),USE(?PRIF:PrintType:Prompt),TRN
                           LIST,AT(61,36,100,10),USE(PRIF:PrintType),DROP(5),FROM('Invoices|#0|Credit Notes|#1|Del' & |
  'ivery Notes|#2|Statements|#3')
                         END
                         TAB('&2) Print Layout'),USE(?Tab:2)
                           LIST,AT(9,20,257,74),USE(?Browse:2),HVSCROLL,FORMAT('40R(2)|M~X Pos~C(0)@n_5@40R(2)|M~Y' & |
  ' Pos~C(0)@n_5@40R(2)|M~Length~C(0)@n_5@40R(2)|M~Alignment~C(0)@n3@40R(2)|M~Repeating~C(0)@n3@'), |
  FROM(Queue:Browse:2),IMM,MSG('Browsing the PrintLayout file')
                           BUTTON('&Insert'),AT(110,98,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(162,98,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(217,98,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(168,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(220,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Print_Fields')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PRIF:FieldName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:PrintFields)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(PRIF:Record,History::PRIF:Record)
  SELF.AddHistoryField(?PRIF:FieldName,2)
  SELF.AddHistoryField(?PRIF:PrintType,3)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:PrintFields.SetOpenRelated()
  Relate:PrintFields.Open                                  ! File PrintFields used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PrintFields
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:PrintLayout,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?PRIF:FieldName{PROP:ReadOnly} = True
    DISABLE(?PRIF:PrintType)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon PRIL:PFID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,PRIL:FKey_PFID)  ! Add the sort order for PRIL:FKey_PFID for sort order 1
  BRW2.AddRange(PRIL:PFID,Relate:PrintLayout,Relate:PrintFields) ! Add file relationship range limit for sort order 1
  BRW2.AddField(PRIL:XPos,BRW2.Q.PRIL:XPos)                ! Field PRIL:XPos is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:YPos,BRW2.Q.PRIL:YPos)                ! Field PRIL:YPos is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:Length,BRW2.Q.PRIL:Length)            ! Field PRIL:Length is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:Alignment,BRW2.Q.PRIL:Alignment)      ! Field PRIL:Alignment is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:Repeating,BRW2.Q.PRIL:Repeating)      ! Field PRIL:Repeating is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:PLID,BRW2.Q.PRIL:PLID)                ! Field PRIL:PLID is a hot field or requires assignment from browse
  BRW2.AddField(PRIL:PFID,BRW2.Q.PRIL:PFID)                ! Field PRIL:PFID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Print_Fields',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      PRIF:PrintType  = p:PrintType
  BRW2.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PrintFields.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Print_Fields',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Print_Layout
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Print_Fields PROCEDURE (p:PrintType)

CurrentTab           STRING(80)                            !
LOC:PrintType        STRING(20)                            !
LOC:Filter_PrintType BYTE                                  !
BRW1::View:Browse    VIEW(PrintFields)
                       PROJECT(PRIF:FieldName)
                       PROJECT(PRIF:PrintType)
                       PROJECT(PRIF:PFID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
PRIF:FieldName         LIKE(PRIF:FieldName)           !List box control field - type derived from field
LOC:PrintType          LIKE(LOC:PrintType)            !List box control field - type derived from local data
PRIF:PrintType         LIKE(PRIF:PrintType)           !Browse hot field - type derived from field
PRIF:PFID              LIKE(PRIF:PFID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Print Fields File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Browse_Print_Fields'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('100L(2)|M~Field Name~@s50@80L(2)|' & |
  'M~Print Type~@s20@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the PrintFields file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Field Name'),USE(?Tab:2)
                         END
                         TAB('&2) By Field Name (All Types)'),USE(?Tab2)
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(222,4,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Print_Fields')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Filter_PrintType',LOC:Filter_PrintType)        ! Added by: BrowseBox(ABC)
  BIND('LOC:PrintType',LOC:PrintType)                      ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:PrintFields.SetOpenRelated()
  Relate:PrintFields.Open                                  ! File PrintFields used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:PrintFields,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,PRIF:Key_FieldName)                   ! Add the sort order for PRIF:Key_FieldName for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,PRIF:FieldName,1,BRW1)         ! Initialize the browse locator using  using key: PRIF:Key_FieldName , PRIF:FieldName
  BRW1.AppendOrder('+PRIF:PrintType,+PRIF:PFID')           ! Append an additional sort order
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon PRIF:FieldName for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,PRIF:Key_FieldName) ! Add the sort order for PRIF:Key_FieldName for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,PRIF:FieldName,1,BRW1)         ! Initialize the browse locator using  using key: PRIF:Key_FieldName , PRIF:FieldName
  BRW1.SetFilter('(LOC:Filter_PrintType = PRIF:PrintType)') ! Apply filter expression to browse
  BRW1.AddField(PRIF:FieldName,BRW1.Q.PRIF:FieldName)      ! Field PRIF:FieldName is a hot field or requires assignment from browse
  BRW1.AddField(LOC:PrintType,BRW1.Q.LOC:PrintType)        ! Field LOC:PrintType is a hot field or requires assignment from browse
  BRW1.AddField(PRIF:PrintType,BRW1.Q.PRIF:PrintType)      ! Field PRIF:PrintType is a hot field or requires assignment from browse
  BRW1.AddField(PRIF:PFID,BRW1.Q.PRIF:PFID)                ! Field PRIF:PFID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Print_Fields',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      LOC:Filter_PrintType    = p:PrintType
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PrintFields.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Print_Fields',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Print_Fields(LOC:Filter_PrintType)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! Invoices|Credit Notes|Delivery Notes|Statements
      EXECUTE PRIF:PrintType
         LOC:PrintType    = 'Credit Notes'
         LOC:PrintType    = 'Delivery Notes'
         LOC:PrintType    = 'Statements'
      ELSE
         LOC:PrintType    = 'Invoices'
      .
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Reminders PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:Q_Vars           GROUP,PRE()                           !
QV:ReminderType      STRING(20)                            !General, Client
QV:RemindOption      STRING(20)                            !All, Group & User, User, Group
                     END                                   !
LOC:UID              ULONG                                 !User ID
LOC:UGID             ULONG                                 !User Group ID
BRW1::View:Browse    VIEW(Reminders)
                       PROJECT(REM:Active)
                       PROJECT(REM:Popup)
                       PROJECT(REM:ReminderDate)
                       PROJECT(REM:ReminderTime)
                       PROJECT(REM:Notes)
                       PROJECT(REM:ID)
                       PROJECT(REM:RID)
                       PROJECT(REM:UGID)
                       PROJECT(REM:ReminderType)
                       PROJECT(REM:RemindOption)
                       PROJECT(REM:UID)
                       JOIN(USEG:PKey_UGID,REM:UGID)
                         PROJECT(USEG:GroupName)
                         PROJECT(USEG:UGID)
                       END
                       JOIN(USE:PKey_UID,REM:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:UID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
QV:ReminderType        LIKE(QV:ReminderType)          !List box control field - type derived from local data
REM:Active             LIKE(REM:Active)               !List box control field - type derived from field
REM:Active_Icon        LONG                           !Entry's icon ID
REM:Popup              LIKE(REM:Popup)                !List box control field - type derived from field
REM:Popup_Icon         LONG                           !Entry's icon ID
REM:ReminderDate       LIKE(REM:ReminderDate)         !List box control field - type derived from field
REM:ReminderTime       LIKE(REM:ReminderTime)         !List box control field - type derived from field
QV:RemindOption        LIKE(QV:RemindOption)          !List box control field - type derived from local data
REM:Notes              LIKE(REM:Notes)                !List box control field - type derived from field
REM:ID                 LIKE(REM:ID)                   !List box control field - type derived from field
REM:RID                LIKE(REM:RID)                  !List box control field - type derived from field
REM:UGID               LIKE(REM:UGID)                 !List box control field - type derived from field
REM:ReminderType       LIKE(REM:ReminderType)         !Browse hot field - type derived from field
REM:RemindOption       LIKE(REM:RemindOption)         !Browse hot field - type derived from field
REM:UID                LIKE(REM:UID)                  !Browse key field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Related join file key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Reminders File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('Browse_Reminders'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('44L(2)|M~User Login~C(0)@s20@48L(' & |
  '2)|M~Group Name~C(0)@s35@54L(2)|M~Reminder Type~C(0)@s20@28R(2)|MI~Active~C(0)@p p@2' & |
  '4R(2)|MI~Popup~C(0)@p p@[40R(2)|M~Date~C(0)@d5@40R(2)|M~Time~C(0)@t7@]|M~Reminder~52' & |
  'L(2)|M~Remind Option~C(0)@s20@150L(2)|M~Notes~C(0)@s255@40R(2)|M~ID~C(0)@n_10@40R(2)' & |
  '|M~RID~C(0)@n_10@40R(2)|M~UGID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he Reminders file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By User'),USE(?Tab:2)
                           BUTTON('Select User'),AT(9,158,,14),USE(?SelectUsers),LEFT,ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                         END
                         TAB('&2) By Type && ID'),USE(?Tab:4)
                         END
                         TAB('&3) By Reminder'),USE(?Tab:5)
                         END
                         TAB('&4) By User Group'),USE(?Tab:6)
                           BUTTON('Select User Group'),AT(9,158,,14),USE(?SelectUserGroups),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&5) By Date && Time'),USE(?Tab5)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Reminders')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('QV:ReminderType',QV:ReminderType)                  ! Added by: BrowseBox(ABC)
  BIND('QV:RemindOption',QV:RemindOption)                  ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:UserGroups.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
      ! Load GLO:User
      USE:UID = GLO:UID
      IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
      .
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Reminders,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,REM:SKey_Type_ID)                     ! Add the sort order for REM:SKey_Type_ID for sort order 1
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort2:Locator.Init(,REM:ReminderType,1,BRW1)       ! Initialize the browse locator using  using key: REM:SKey_Type_ID , REM:ReminderType
  BRW1.AppendOrder('-REM:ReminderDate,+REM:RID')           ! Append an additional sort order
  BRW1.AddSortOrder(,REM:PKey_RID)                         ! Add the sort order for REM:PKey_RID for sort order 2
  BRW1.AddSortOrder(,REM:FKey_UGID)                        ! Add the sort order for REM:FKey_UGID for sort order 3
  BRW1.AddRange(REM:UGID,Relate:Reminders,Relate:UserGroups) ! Add file relationship range limit for sort order 3
  BRW1.AppendOrder('-REM:ReminderDate,+REM:RID')           ! Append an additional sort order
  BRW1.AddSortOrder(,)                                     ! Add the sort order for  for sort order 4
  BRW1.AppendOrder('-REM:ReminderDate,+REM:RID')           ! Append an additional sort order
  BRW1.AddSortOrder(,REM:FKey_UID)                         ! Add the sort order for REM:FKey_UID for sort order 5
  BRW1.AddRange(REM:UID,Relate:Reminders,Relate:Users)     ! Add file relationship range limit for sort order 5
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 5
  BRW1::Sort0:Locator.Init(,REM:UID,1,BRW1)                ! Initialize the browse locator using  using key: REM:FKey_UID , REM:UID
  BRW1.AppendOrder('-REM:ReminderDate,+REM:RID')           ! Append an additional sort order
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(USE:Login,BRW1.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW1.AddField(USEG:GroupName,BRW1.Q.USEG:GroupName)      ! Field USEG:GroupName is a hot field or requires assignment from browse
  BRW1.AddField(QV:ReminderType,BRW1.Q.QV:ReminderType)    ! Field QV:ReminderType is a hot field or requires assignment from browse
  BRW1.AddField(REM:Active,BRW1.Q.REM:Active)              ! Field REM:Active is a hot field or requires assignment from browse
  BRW1.AddField(REM:Popup,BRW1.Q.REM:Popup)                ! Field REM:Popup is a hot field or requires assignment from browse
  BRW1.AddField(REM:ReminderDate,BRW1.Q.REM:ReminderDate)  ! Field REM:ReminderDate is a hot field or requires assignment from browse
  BRW1.AddField(REM:ReminderTime,BRW1.Q.REM:ReminderTime)  ! Field REM:ReminderTime is a hot field or requires assignment from browse
  BRW1.AddField(QV:RemindOption,BRW1.Q.QV:RemindOption)    ! Field QV:RemindOption is a hot field or requires assignment from browse
  BRW1.AddField(REM:Notes,BRW1.Q.REM:Notes)                ! Field REM:Notes is a hot field or requires assignment from browse
  BRW1.AddField(REM:ID,BRW1.Q.REM:ID)                      ! Field REM:ID is a hot field or requires assignment from browse
  BRW1.AddField(REM:RID,BRW1.Q.REM:RID)                    ! Field REM:RID is a hot field or requires assignment from browse
  BRW1.AddField(REM:UGID,BRW1.Q.REM:UGID)                  ! Field REM:UGID is a hot field or requires assignment from browse
  BRW1.AddField(REM:ReminderType,BRW1.Q.REM:ReminderType)  ! Field REM:ReminderType is a hot field or requires assignment from browse
  BRW1.AddField(REM:RemindOption,BRW1.Q.REM:RemindOption)  ! Field REM:RemindOption is a hot field or requires assignment from browse
  BRW1.AddField(REM:UID,BRW1.Q.REM:UID)                    ! Field REM:UID is a hot field or requires assignment from browse
  BRW1.AddField(USEG:UGID,BRW1.Q.USEG:UGID)                ! Field USEG:UGID is a hot field or requires assignment from browse
  BRW1.AddField(USE:UID,BRW1.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Reminders',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Reminders',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Reminders
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectUsers
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Users()
      ThisWindow.Reset
          LOC:UID = USE:UID
    OF ?SelectUserGroups
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_UserGroups()
      ThisWindow.Reset
          LOC:UGID    = USEG:UGID
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! General|Client
      EXECUTE REM:ReminderType + 1
         QV:ReminderType  = 'General'
         QV:ReminderType  = 'Client'
         QV:ReminderType  = 'Truck/Trailers'
      .
  
  
      ! All|Group & User|User|Group
      EXECUTE REM:RemindOption + 1
         QV:RemindOption  = 'All'
         QV:RemindOption  = 'Group & User'
         QV:RemindOption  = 'User'
         QV:RemindOption  = 'Group'
      .
  PARENT.SetQueueRecord
  
  IF (REM:Active = 1)
    SELF.Q.REM:Active_Icon = 2                             ! Set icon from icon list
  ELSE
    SELF.Q.REM:Active_Icon = 1                             ! Set icon from icon list
  END
  IF (REM:Popup = 1)
    SELF.Q.REM:Popup_Icon = 2                              ! Set icon from icon list
  ELSE
    SELF.Q.REM:Popup_Icon = 1                              ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Reminders PROCEDURE (p:Option, p:ID)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Users_Group      GROUP,PRE(LO)                         !
Login                STRING(20)                            !User Login
GroupName            STRING(35)                            !Name of this group of users
Login2               STRING(20)                            !User Login
                     END                                   !
LOC:Screen           GROUP,PRE(L_SG)                       !
Day                  STRING(30)                            !
CreatedBy            STRING(35)                            !
                     END                                   !
BRW10::View:Browse   VIEW(RemindersUsers)
                       PROJECT(REU:NoAction)
                       PROJECT(REU:ReminderDate)
                       PROJECT(REU:ReminderTime)
                       PROJECT(REU:RUID)
                       PROJECT(REU:RID)
                       PROJECT(REU:UID)
                       JOIN(USE:PKey_UID,REU:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:UID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
REU:NoAction           LIKE(REU:NoAction)             !List box control field - type derived from field
REU:NoAction_Icon      LONG                           !Entry's icon ID
REU:ReminderDate       LIKE(REU:ReminderDate)         !List box control field - type derived from field
REU:ReminderTime       LIKE(REU:ReminderTime)         !List box control field - type derived from field
REU:RUID               LIKE(REU:RUID)                 !Primary key field - type derived from field
REU:RID                LIKE(REU:RID)                  !Browse key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::REM:Record  LIKE(REM:RECORD),THREAD
QuickWindow          WINDOW('Form Reminders'),AT(,,314,277),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('Update_Reminders'),SYSTEM
                       SHEET,AT(4,4,307,254),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('RID:'),AT(215,6),USE(?REM:RID:Prompt),TRN
                           STRING(@n_10),AT(237,6,70,10),USE(REM:RID),RIGHT(1),TRN
                           PROMPT('Reminder Date:'),AT(9,22),USE(?REM:ReminderDate:Prompt),TRN
                           SPIN(@d5),AT(63,22,58,10),USE(REM:ReminderDate),RIGHT(1)
                           BUTTON('...'),AT(125,22,12,10),USE(?Calendar)
                           STRING('Day'),AT(143,22,77,10),USE(?String_Day),TRN
                           PROMPT('Reminder Time:'),AT(9,36),USE(?REM:ReminderTime:Prompt),TRN
                           SPIN(@t7),AT(63,36,58,10),USE(REM:ReminderTime),RIGHT(1),STEP(6000)
                           PROMPT('Type:'),AT(227,22),USE(?REM:ReminderType:Prompt),TRN
                           LIST,AT(249,22,58,10),USE(REM:ReminderType),VSCROLL,DISABLE,DROP(5),FROM('General|#0|Cl' & |
  'ient|#1|Truck / Trailer|#2'),MSG('General, Client'),TIP('General, Client')
                           PROMPT('ID:'),AT(227,36),USE(?REM:ID:Prompt),TRN
                           STRING(@n_10),AT(249,36,58,10),USE(REM:ID),RIGHT(1),TRN
                           CHECK(' &Reoccurs'),AT(63,58),USE(REM:Reoccurs),MSG('This reminder reoccurs'),TIP('This remin' & |
  'der reoccurs')
                           GROUP,AT(9,72,133,24),USE(?Group_Reoccur)
                             PROMPT('Reoccur Option:'),AT(9,72),USE(?REM:Period:Prompt),TRN
                             LIST,AT(63,72,79,10),USE(REM:Reoccur_Option),DROP(15),FROM('So many mins/hours|#1|Daily' & |
  '|#2|Weekly|#3|Bi-Weekly|#4|Monthly|#5|Bi-Monthly|#6|Quarterly|#7|Yearly|#8|None|#0')
                             PROMPT('Reoccur Period:'),AT(9,86),USE(?REM:Reoccur_Period:Prompt),TRN
                             ENTRY(@n10b),AT(63,86,58,10),USE(REM:Reoccur_Period),RIGHT(1)
                           END
                           PROMPT('Remind Option:'),AT(9,138),USE(?REM:RemindOption:Prompt),TRN
                           LIST,AT(63,138,58,10),USE(REM:RemindOption),DROP(15),FROM('All|#0|Group & User|#1|User|' & |
  '#2|Group|#3'),MSG('All, Group & User, User'),TIP('All, Group & User, User')
                           GROUP,AT(9,170,200,10),USE(?Group_UserGroup)
                             PROMPT('Group Name:'),AT(9,170),USE(?GroupName:Prompt),TRN
                             ENTRY(@s35),AT(63,170,119,10),USE(LO:GroupName),MSG('Name of this group of users'),REQ,TIP('Name of th' & |
  'is group of users')
                             BUTTON('...'),AT(189,170,12,10),USE(?CallLookup:2)
                           END
                           GROUP,AT(9,154,200,10),USE(?Group_User)
                             PROMPT('User Login:'),AT(9,154),USE(?Login:Prompt),TRN
                             ENTRY(@s20),AT(63,154,119,10),USE(LO:Login),MSG('User Login'),REQ,TIP('User Login')
                             BUTTON('...'),AT(189,154,12,10),USE(?CallLookup)
                           END
                           PROMPT('Notes:'),AT(9,192),USE(?REM:Notes:Prompt),TRN
                           TEXT,AT(63,192,245,60),USE(REM:Notes),VSCROLL
                           CHECK(' &Popup'),AT(9,232,38,8),USE(REM:Popup),RIGHT,MSG('Popup when due'),TIP('Popup when due'), |
  TRN
                           CHECK(' &Active'),AT(9,244,,8),USE(REM:Active),RIGHT,MSG('Active Reminder'),TIP('Active Reminder'), |
  TRN
                         END
                         TAB('&2) Reminder Users Extra'),USE(?Tab2)
                           LIST,AT(9,22,298,149),USE(?List),VSCROLL,FORMAT('80L(2)|M~User Login~@s20@38L(2)|MI~No ' & |
  'Action~@p p@[40L(2)|M~Date~@d5b@42L(2)|M~Time~@t7b@](269)|M~Reminder~'),FROM(Queue:Browse), |
  IMM,MSG('Browsing Records')
                         END
                         TAB('&3) Info'),USE(?Tab3)
                           PROMPT('Created by Login:'),AT(9,23),USE(?Login2:Prompt),TRN
                           ENTRY(@s20),AT(69,23,63,10),USE(LO:Login2),COLOR(00E9E9E9h),MSG('User Login'),READONLY,SKIP, |
  TIP('User Login')
                           PROMPT('Created Date:'),AT(9,40),USE(?REM:Created_Date:Prompt),TRN
                           ENTRY(@d5b),AT(69,40,63,10),USE(REM:Created_Date),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Created Time:'),AT(9,54),USE(?REM:Created_Time:Prompt),TRN
                           ENTRY(@t7),AT(69,54,63,10),USE(REM:Created_Time),COLOR(00E9E9E9h),READONLY,SKIP
                         END
                       END
                       BUTTON('&OK'),AT(210,260,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(262,260,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       ENTRY(@s35),AT(4,262,143,10),USE(L_SG:CreatedBy),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                       BUTTON('&Help'),AT(156,260,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar5            CalendarClass
BRW10                CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Do_Day          ROUTINE
    L_SG:Day                = Week_Day(REM:ReminderDate)

    IF REM:ReminderDate = TODAY()
       L_SG:Day             = 'Today (' & CLIP(L_SG:Day) & ')'
    .

    ?String_Day{PROP:Text}  = L_SG:Day
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Reminder Record'
  OF InsertRecord
    ActionMessage = 'Reminder Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Reminder Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Reminders')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REM:RID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Reminders)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(REM:Record,History::REM:Record)
  SELF.AddHistoryField(?REM:RID,1)
  SELF.AddHistoryField(?REM:ReminderDate,8)
  SELF.AddHistoryField(?REM:ReminderTime,9)
  SELF.AddHistoryField(?REM:ReminderType,2)
  SELF.AddHistoryField(?REM:ID,3)
  SELF.AddHistoryField(?REM:Reoccurs,24)
  SELF.AddHistoryField(?REM:Reoccur_Option,25)
  SELF.AddHistoryField(?REM:Reoccur_Period,26)
  SELF.AddHistoryField(?REM:RemindOption,11)
  SELF.AddHistoryField(?REM:Notes,14)
  SELF.AddHistoryField(?REM:Popup,5)
  SELF.AddHistoryField(?REM:Active,4)
  SELF.AddHistoryField(?REM:Created_Date,19)
  SELF.AddHistoryField(?REM:Created_Time,20)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Reminders.SetOpenRelated()
  Relate:Reminders.Open                                    ! File Reminders used by this procedure, so make sure it's RelationManager is open
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:UserGroups.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Reminders
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW10.Init(?List,Queue:Browse.ViewPosition,BRW10::View:Browse,Queue:Browse,Relate:RemindersUsers,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
      IF SELF.Request = InsertRecord
         ! (BYTE=0, ULONG=0)
         REM:ReminderType     = p:Option
         REM:ID               = p:ID
  
         REM:Active           = TRUE
  
         IF REM:ReminderType = 0
            REM:UID           = GLO:UID
            REM:RemindOption  = 2
         .
      ELSIF SELF.Request = ChangeRecord
         IF REM:Created_UID ~= 0 AND REM:Created_UID ~= GLO:UID
            SELF.Request      = ViewRecord
         .
      ELSIF SELF.Request = ViewRecord
         DISABLE(?REM:ReminderDate)
         DISABLE(?REM:ReminderTime)
      .
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?Calendar)
    ?REM:ReminderTime{PROP:ReadOnly} = True
    DISABLE(?REM:ReminderType)
    DISABLE(?REM:Reoccur_Option)
    ?REM:Reoccur_Period{PROP:ReadOnly} = True
    DISABLE(?REM:RemindOption)
    ?LO:GroupName{PROP:ReadOnly} = True
    DISABLE(?CallLookup:2)
    ?LO:Login{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?LO:Login2{PROP:ReadOnly} = True
    ?REM:Created_Date{PROP:ReadOnly} = True
    ?REM:Created_Time{PROP:ReadOnly} = True
    ?L_SG:CreatedBy{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW10.Q &= Queue:Browse
  BRW10.AddSortOrder(,REU:FKey_RID)                        ! Add the sort order for REU:FKey_RID for sort order 1
  BRW10.AddRange(REU:RID,REM:RID)                          ! Add single value range limit for sort order 1
  BRW10.AddLocator(BRW10::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW10::Sort0:Locator.Init(,REU:RID,1,BRW10)              ! Initialize the browse locator using  using key: REU:FKey_RID , REU:RID
  BRW10.AppendOrder('-REU:ReminderDate,+REU:RUID')         ! Append an additional sort order
  ?List{PROP:IconList,1} = '~checkoffdim.ico'
  ?List{PROP:IconList,2} = '~checkon.ico'
  BRW10.AddField(USE:Login,BRW10.Q.USE:Login)              ! Field USE:Login is a hot field or requires assignment from browse
  BRW10.AddField(REU:NoAction,BRW10.Q.REU:NoAction)        ! Field REU:NoAction is a hot field or requires assignment from browse
  BRW10.AddField(REU:ReminderDate,BRW10.Q.REU:ReminderDate) ! Field REU:ReminderDate is a hot field or requires assignment from browse
  BRW10.AddField(REU:ReminderTime,BRW10.Q.REU:ReminderTime) ! Field REU:ReminderTime is a hot field or requires assignment from browse
  BRW10.AddField(REU:RUID,BRW10.Q.REU:RUID)                ! Field REU:RUID is a hot field or requires assignment from browse
  BRW10.AddField(REU:RID,BRW10.Q.REU:RID)                  ! Field REU:RID is a hot field or requires assignment from browse
  BRW10.AddField(USE:UID,BRW10.Q.USE:UID)                  ! Field USE:UID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_Reminders',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  IF ?REM:Reoccurs{Prop:Checked}
    ENABLE(?Group_Reoccur)
  END
  IF NOT ?REM:Reoccurs{PROP:Checked}
    DISABLE(?Group_Reoccur)
  END
  BRW10.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW10.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Reminders.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Reminders',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  REM:Popup = 1
  REM:Created_UID = GLO:UID
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_UserGroups
      Select_Users
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?REM:ReminderDate
          DO Do_Day
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',REM:ReminderDate)
      IF Calendar5.Response = RequestCompleted THEN
      REM:ReminderDate=Calendar5.SelectedDate
      DISPLAY(?REM:ReminderDate)
      END
      ThisWindow.Reset(True)
          DO Do_Day
    OF ?REM:Reoccurs
      IF ?REM:Reoccurs{PROP:Checked}
        ENABLE(?Group_Reoccur)
      END
      IF NOT ?REM:Reoccurs{PROP:Checked}
        DISABLE(?Group_Reoccur)
      END
      ThisWindow.Reset()
    OF ?REM:Reoccur_Option
          IF REM:Reoccurs = TRUE
             ! So many mins/hours|#1|Daily|#2|Weekly|#3|Bi-Weekly|#4|Monthly|#5|Bi-Monthly|#6|Quarterly|#7|Yearly|#8
             IF REM:Reoccur_Option = 1
                ?REM:Reoccur_Period{PROP:Text}    = '@t4b'
                ?REM:Reoccur_Period{PROP:Color}   = -1
                ENABLE(?REM:Reoccur_Period)
             ELSE
                DISABLE(?REM:Reoccur_Period)
                ?REM:Reoccur_Period{PROP:Color}   = COLOR:Gray
             .
          ELSE
             DISABLE(?REM:Reoccur_Period)
             ?REM:Reoccur_Period{PROP:Color}      = COLOR:Gray
          .
    OF ?REM:RemindOption
          ! 'All|#0|Group & User|#1|User|#2|Group|#3'
          CASE REM:RemindOption
          OF 1
             ENABLE(?Group_UserGroup)
             ENABLE(?Group_User)
          OF 2
             DISABLE(?Group_UserGroup)
             ENABLE(?Group_User)
          OF 3
             ENABLE(?Group_UserGroup)
             DISABLE(?Group_User)
          ELSE
             DISABLE(?Group_UserGroup)
             DISABLE(?Group_User)
          .
    OF ?LO:GroupName
      IF NOT QuickWindow{PROP:AcceptAll}
        IF LO:GroupName OR ?LO:GroupName{PROP:Req}
          USEG:GroupName = LO:GroupName
          IF Access:UserGroups.TryFetch(USEG:Key_GroupName)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              LO:GroupName = USEG:GroupName
              REM:UGID = USEG:UGID
            ELSE
              CLEAR(REM:UGID)
              SELECT(?LO:GroupName)
              CYCLE
            END
          ELSE
            REM:UGID = USEG:UGID
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update()
      USEG:GroupName = LO:GroupName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LO:GroupName = USEG:GroupName
        REM:UGID = USEG:UGID
      END
      ThisWindow.Reset(1)
    OF ?LO:Login
      IF NOT QuickWindow{PROP:AcceptAll}
        IF LO:Login OR ?LO:Login{PROP:Req}
          USE:Login = LO:Login
          IF Access:Users.TryFetch(USE:Key_Login)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              LO:Login = USE:Login
              REM:UID = USE:UID
            ELSE
              CLEAR(REM:UID)
              SELECT(?LO:Login)
              CYCLE
            END
          ELSE
            REM:UID = USE:UID
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      USE:Login = LO:Login
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LO:Login = USE:Login
        REM:UID = USE:UID
      END
      ThisWindow.Reset(1)
    OF ?OK
      ThisWindow.Update()
          CASE REM:RemindOption
          OF 1
          OF 2
             CLEAR(REM:UGID)
          OF 3
             CLEAR(REM:UID)
          ELSE
             CLEAR(REM:UID)
             CLEAR(REM:UGID)
          .
          IF REM:Reoccurs = TRUE
             IF REM:Reoccur_Option = 0
                MESSAGE('For reoccuring reminders you must specify a reoccuring option.', 'Reoccuring Reminder', ICON:Exclamation)
                QuickWindow{PROP:AcceptAll}   = FALSE
                CYCLE
          .  .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
          GLO:Reminder_Inc    += 1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?REM:ReminderDate
          DO Do_Day
    OF ?REM:RemindOption
          ! 'All|#0|Group & User|#1|User|#2|Group|#3'
          CASE REM:RemindOption
          OF 1
             ENABLE(?Group_UserGroup)
             ENABLE(?Group_User)
          OF 2
             DISABLE(?Group_UserGroup)
             ENABLE(?Group_User)
          OF 3
             ENABLE(?Group_UserGroup)
             DISABLE(?Group_User)
          ELSE
             DISABLE(?Group_UserGroup)
             DISABLE(?Group_User)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          ! 'All|#0|Group & User|#1|User|#2|Group|#3'
          CASE REM:RemindOption
          OF 1
             ENABLE(?Group_UserGroup)
             ENABLE(?Group_User)
          OF 2
             DISABLE(?Group_UserGroup)
             ENABLE(?Group_User)
          OF 3
             ENABLE(?Group_UserGroup)
             DISABLE(?Group_User)
          ELSE
             DISABLE(?Group_UserGroup)
             DISABLE(?Group_User)
          .
      
      
          IF SELF.Request = InsertRecord
             ! (BYTE=0, ULONG=0)
             REM:ReminderType     = p:Option
             REM:ID               = p:ID
      
             REM:ReminderTime     = INT(CLOCK() / 100 / 60 / 60 + 1) * 100 * 60 * 60 + 1
             REM:ReminderDate     = TODAY() + 1
      
             REM:Active           = TRUE
          .
      
          db.debugout('[Update_Reminders]  Option: ' & p:Option & ',  ID: ' & p:ID)
      
      
          !DISABLE(?REM:ReminderType)
      
      
      
          USE:UID         = REM:UID
          IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
             LO:Login     = USE:Login
          .
      
          USEG:UGID       = REM:UGID
          IF Access:UserGroups.TryFetch(USEG:PKey_UGID) = LEVEL:Benign
             LO:GroupName = USEG:GroupName
          .
      
      
      
          USE:UID         = REM:Created_UID
          IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
             LO:Login2    = USE:Login
          .
          DO Do_Day
          IF SELF.Request = ChangeRecord AND REM:Active = FALSE
             ?REM:Notes{PROP:BackGround} = 0E9E9E9H
             ?REM:Notes{PROP:ReadOnly}   = TRUE
          .
          L_SG:CreatedBy  = 'Created - ' & CLIP(LO:Login2) & ' - ' & FORMAT(REM:Created_Date,@d5) & ' @ ' & FORMAT(REM:Created_Time,@t4)
          DISPLAY
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW10.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (REU:NoAction = 1)
    SELF.Q.REU:NoAction_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.REU:NoAction_Icon = 1                           ! Set icon from icon list
  END

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_AdditionalChargeCategories PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(AdditionalCharges)
                       PROJECT(ACCA:Description)
                       PROJECT(ACCA:ACCID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ACCA:Description       LIKE(ACCA:Description)         !List box control field - type derived from field
ACCA:ACCID             LIKE(ACCA:ACCID)               !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Additional Charges File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_AdditionalChargeCategories'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Description~L(2)@s35@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the AdditionalCharges file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Description'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_AdditionalChargeCategories')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:AdditionalCharges.Open                            ! File AdditionalCharges used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:AdditionalCharges,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,ACCA:Key_Description)                 ! Add the sort order for ACCA:Key_Description for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,ACCA:Description,1,BRW1)       ! Initialize the browse locator using  using key: ACCA:Key_Description , ACCA:Description
  BRW1.AddField(ACCA:Description,BRW1.Q.ACCA:Description)  ! Field ACCA:Description is a hot field or requires assignment from browse
  BRW1.AddField(ACCA:ACCID,BRW1.Q.ACCA:ACCID)              ! Field ACCA:ACCID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_AdditionalChargeCategories',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AdditionalCharges.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_AdditionalChargeCategories',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_AdditionalChargesCategories
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_AdditionalChargesCategories PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(__RatesAdditionalCharges)
                       PROJECT(CARA:CID)
                       PROJECT(CARA:Charge)
                       PROJECT(CARA:Effective_Date)
                       PROJECT(CARA:ACID)
                       PROJECT(CARA:ACCID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
CARA:CID               LIKE(CARA:CID)                 !List box control field - type derived from field
CARA:Charge            LIKE(CARA:Charge)              !List box control field - type derived from field
CARA:Effective_Date    LIKE(CARA:Effective_Date)      !List box control field - type derived from field
CARA:ACID              LIKE(CARA:ACID)                !Primary key field - type derived from field
CARA:ACCID             LIKE(CARA:ACCID)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::ACCA:Record LIKE(ACCA:RECORD),THREAD
QuickWindow          WINDOW('Form Additional Charges'),AT(,,213,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateAdditionalCharges'),SYSTEM
                       SHEET,AT(4,4,205,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Description:'),AT(9,22),USE(?ACCA:Description:Prompt),TRN
                           ENTRY(@s35),AT(61,22,144,10),USE(ACCA:Description),MSG('Description'),TIP('Description')
                         END
                         TAB('&2) Additional Charges Clients'),USE(?Tab:2),HIDE
                           LIST,AT(9,20,197,92),USE(?Browse:2),HVSCROLL,FORMAT('40R(2)|M~CID~C(0)@n_10@68R(2)|M~Ch' & |
  'arge~C(0)@n-15.2@80R(2)|M~Effective Date~C(0)@d5@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he __RatesAdditionalCharges file')
                         END
                       END
                       BUTTON('&OK'),AT(108,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(160,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_AdditionalChargesCategories')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ACCA:Description:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:AdditionalCharges)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(ACCA:Record,History::ACCA:Record)
  SELF.AddHistoryField(?ACCA:Description,2)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:AdditionalCharges.SetOpenRelated()
  Relate:AdditionalCharges.Open                            ! File AdditionalCharges used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:AdditionalCharges
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:__RatesAdditionalCharges,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?ACCA:Description{PROP:ReadOnly} = True
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,CARA:FKey_ACCID)                      ! Add the sort order for CARA:FKey_ACCID for sort order 1
  BRW2.AddRange(CARA:ACCID,Relate:__RatesAdditionalCharges,Relate:AdditionalCharges) ! Add file relationship range limit for sort order 1
  BRW2.AddField(CARA:CID,BRW2.Q.CARA:CID)                  ! Field CARA:CID is a hot field or requires assignment from browse
  BRW2.AddField(CARA:Charge,BRW2.Q.CARA:Charge)            ! Field CARA:Charge is a hot field or requires assignment from browse
  BRW2.AddField(CARA:Effective_Date,BRW2.Q.CARA:Effective_Date) ! Field CARA:Effective_Date is a hot field or requires assignment from browse
  BRW2.AddField(CARA:ACID,BRW2.Q.CARA:ACID)                ! Field CARA:ACID is a hot field or requires assignment from browse
  BRW2.AddField(CARA:ACCID,BRW2.Q.CARA:ACCID)              ! Field CARA:ACCID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_AdditionalChargesCategories',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AdditionalCharges.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_AdditionalChargesCategories',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_ContainerOperators PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(DeliveryItems)
                       PROJECT(DELI:ItemNo)
                       PROJECT(DELI:Type)
                       PROJECT(DELI:ContainerNo)
                       PROJECT(DELI:ContainerReturnAID)
                       PROJECT(DELI:ContainerVessel)
                       PROJECT(DELI:ETA)
                       PROJECT(DELI:ByContainer)
                       PROJECT(DELI:Length)
                       PROJECT(DELI:Breadth)
                       PROJECT(DELI:DIID)
                       PROJECT(DELI:COID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Type              LIKE(DELI:Type)                !List box control field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !List box control field - type derived from field
DELI:ContainerReturnAID LIKE(DELI:ContainerReturnAID) !List box control field - type derived from field
DELI:ContainerVessel   LIKE(DELI:ContainerVessel)     !List box control field - type derived from field
DELI:ETA               LIKE(DELI:ETA)                 !List box control field - type derived from field
DELI:ByContainer       LIKE(DELI:ByContainer)         !List box control field - type derived from field
DELI:Length            LIKE(DELI:Length)              !List box control field - type derived from field
DELI:Breadth           LIKE(DELI:Breadth)             !List box control field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Primary key field - type derived from field
DELI:COID              LIKE(DELI:COID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::CONO:Record LIKE(CONO:RECORD),THREAD
QuickWindow          WINDOW('Form Container Operators'),AT(,,240,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateContainerOperators'),SYSTEM
                       SHEET,AT(4,4,232,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Container Operator:'),AT(9,22),USE(?CONO:ContainerOperator:Prompt),TRN
                           ENTRY(@s35),AT(89,22,144,10),USE(CONO:ContainerOperator),MSG('Container Operator'),REQ,TIP('Container Operator')
                         END
                         TAB('&2) Delivery Items'),USE(?Tab:2)
                           LIST,AT(9,20,224,93),USE(?Browse:2),HVSCROLL,FORMAT('32R(2)|M~Item No~C(0)@n6@20R(2)|M~' & |
  'Type~C(0)@n3@80L(2)|M~Container No~L(2)@s35@80D(12)|M~Container Return Address~C(0)@' & |
  's20@80L(2)|M~Container Vessel~L(2)@s35@80R(2)|M~ETA~C(0)@d6@52R(2)|M~By Container~C(' & |
  '0)@n3@28R(2)|M~Length~C(0)@n6@32R(2)|M~Breadth~C(0)@n6@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he DeliveryItems file')
                         END
                       END
                       BUTTON('&OK'),AT(134,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(188,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Container Operators Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Container Operators Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ContainerOperators')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CONO:ContainerOperator:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:ContainerOperators)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CONO:Record,History::CONO:Record)
  SELF.AddHistoryField(?CONO:ContainerOperator,2)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ContainerOperators.SetOpenRelated()
  Relate:ContainerOperators.Open                           ! File ContainerOperators used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ContainerOperators
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:DeliveryItems,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?CONO:ContainerOperator{PROP:ReadOnly} = True
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon DELI:COID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,DELI:FKey_COID)  ! Add the sort order for DELI:FKey_COID for sort order 1
  BRW2.AddRange(DELI:COID,Relate:DeliveryItems,Relate:ContainerOperators) ! Add file relationship range limit for sort order 1
  BRW2.AddField(DELI:ItemNo,BRW2.Q.DELI:ItemNo)            ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW2.AddField(DELI:Type,BRW2.Q.DELI:Type)                ! Field DELI:Type is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ContainerNo,BRW2.Q.DELI:ContainerNo)  ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ContainerReturnAID,BRW2.Q.DELI:ContainerReturnAID) ! Field DELI:ContainerReturnAID is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ContainerVessel,BRW2.Q.DELI:ContainerVessel) ! Field DELI:ContainerVessel is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ETA,BRW2.Q.DELI:ETA)                  ! Field DELI:ETA is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ByContainer,BRW2.Q.DELI:ByContainer)  ! Field DELI:ByContainer is a hot field or requires assignment from browse
  BRW2.AddField(DELI:Length,BRW2.Q.DELI:Length)            ! Field DELI:Length is a hot field or requires assignment from browse
  BRW2.AddField(DELI:Breadth,BRW2.Q.DELI:Breadth)          ! Field DELI:Breadth is a hot field or requires assignment from browse
  BRW2.AddField(DELI:DIID,BRW2.Q.DELI:DIID)                ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW2.AddField(DELI:COID,BRW2.Q.DELI:COID)                ! Field DELI:COID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_ContainerOperators',QuickWindow)    ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ContainerOperators.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_ContainerOperators',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

