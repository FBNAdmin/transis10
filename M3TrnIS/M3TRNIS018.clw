

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS018.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Audit PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:User_Access      LONG                                  !
LOC:Filter           GROUP,PRE(L_F)                        !
Severity             BYTE(10)                              !
                     END                                   !
BRW1::View:Browse    VIEW(Audit)
                       PROJECT(AUD:AUID)
                       PROJECT(AUD:AuditDate)
                       PROJECT(AUD:AuditTime)
                       PROJECT(AUD:Severity)
                       PROJECT(AUD:AppSection)
                       PROJECT(AUD:Data1)
                       PROJECT(AUD:Data2)
                       PROJECT(AUD:AccessLevel)
                       PROJECT(AUD:AuditDateTime)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
AUD:AUID               LIKE(AUD:AUID)                 !List box control field - type derived from field
AUD:AuditDate          LIKE(AUD:AuditDate)            !List box control field - type derived from field
AUD:AuditTime          LIKE(AUD:AuditTime)            !List box control field - type derived from field
AUD:Severity           LIKE(AUD:Severity)             !List box control field - type derived from field
AUD:AppSection         LIKE(AUD:AppSection)           !List box control field - type derived from field
AUD:Data1              LIKE(AUD:Data1)                !List box control field - type derived from field
AUD:Data2              LIKE(AUD:Data2)                !List box control field - type derived from field
AUD:AccessLevel        LIKE(AUD:AccessLevel)          !List box control field - type derived from field
AUD:AuditDateTime      LIKE(AUD:AuditDateTime)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Audit File'),AT(,,358,214),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('Browse_Audit'),SYSTEM
                       GROUP,AT(4,178,291,34),USE(?Group1)
                         PROMPT('App Section:'),AT(4,178),USE(?AUD:AppSection:Prompt)
                         TEXT,AT(50,178,245,10),USE(AUD:AppSection),VSCROLL,BOXED,MSG('Program section'),TIP('Program section')
                         PROMPT('Data 1:'),AT(4,191),USE(?AUD:Data1:Prompt)
                         TEXT,AT(50,191,245,10),USE(AUD:Data1),VSCROLL,BOXED
                         PROMPT('Data 2:'),AT(4,202),USE(?AUD:Data2:Prompt)
                         TEXT,AT(50,202,245,10),USE(AUD:Data2),VSCROLL,BOXED
                       END
                       LIST,AT(8,20,342,134),USE(?Browse:1),HVSCROLL,FORMAT('38R(2)|M~AUID~C(0)@n_10@36R(2)|M~' & |
  'Date~C(0)@d5@35R(2)|M~Time~C(0)@t7@30R(2)|M~Severity~C(0)@n3@80L(2)|M~App. Section~@' & |
  's200@80L(2)|M~Data 1~@s255@80L(2)|M~Data 2~@s255@52R(2)|M~Access Level~C(0)@n3@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the Audit file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By AUID'),USE(?Tab:2)
                           GROUP,AT(9,162,67,10),USE(?Group2)
                             PROMPT('Severity:'),AT(9,162),USE(?L_F:Severity:Prompt)
                             SPIN(@n3),AT(41,162,35,10),USE(L_F:Severity),RIGHT(1)
                           END
                         END
                         TAB('&2) By Date && Time'),USE(?Tab:3)
                         END
                         TAB('&3) By Application Section'),USE(?Tab:4)
                         END
                       END
                       BUTTON('&Close'),AT(300,180,,30),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,4,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Audit')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?AUD:AppSection:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_F:Severity',L_F:Severity)                        ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Audit.Open                                        ! File Audit used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Audit,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,AUD:SKey_DateTime)                    ! Add the sort order for AUD:SKey_DateTime for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,AUD:AuditDateTime,1,BRW1)      ! Initialize the browse locator using  using key: AUD:SKey_DateTime , AUD:AuditDateTime
  BRW1.AppendOrder('+AUD:AUID')                            ! Append an additional sort order
  BRW1.SetFilter('(AUD:Severity <<= L_F:Severity)')        ! Apply filter expression to browse
  BRW1.AddResetField(L_F:Severity)                         ! Apply the reset field
  BRW1.AddSortOrder(,AUD:SKey_Section)                     ! Add the sort order for AUD:SKey_Section for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,AUD:AppSection,1,BRW1)         ! Initialize the browse locator using  using key: AUD:SKey_Section , AUD:AppSection
  BRW1.AppendOrder('+AUD:AuditDate,+AUD:AUID')             ! Append an additional sort order
  BRW1.SetFilter('(AUD:Severity <<= L_F:Severity)')        ! Apply filter expression to browse
  BRW1.AddResetField(L_F:Severity)                         ! Apply the reset field
  BRW1.AddSortOrder(,AUD:PKey_AUID)                        ! Add the sort order for AUD:PKey_AUID for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(,AUD:AUID,1,BRW1)               ! Initialize the browse locator using  using key: AUD:PKey_AUID , AUD:AUID
  BRW1.SetFilter('(AUD:Severity <<= L_F:Severity)')        ! Apply filter expression to browse
  BRW1.AddResetField(L_F:Severity)                         ! Apply the reset field
  BRW1.AddField(AUD:AUID,BRW1.Q.AUD:AUID)                  ! Field AUD:AUID is a hot field or requires assignment from browse
  BRW1.AddField(AUD:AuditDate,BRW1.Q.AUD:AuditDate)        ! Field AUD:AuditDate is a hot field or requires assignment from browse
  BRW1.AddField(AUD:AuditTime,BRW1.Q.AUD:AuditTime)        ! Field AUD:AuditTime is a hot field or requires assignment from browse
  BRW1.AddField(AUD:Severity,BRW1.Q.AUD:Severity)          ! Field AUD:Severity is a hot field or requires assignment from browse
  BRW1.AddField(AUD:AppSection,BRW1.Q.AUD:AppSection)      ! Field AUD:AppSection is a hot field or requires assignment from browse
  BRW1.AddField(AUD:Data1,BRW1.Q.AUD:Data1)                ! Field AUD:Data1 is a hot field or requires assignment from browse
  BRW1.AddField(AUD:Data2,BRW1.Q.AUD:Data2)                ! Field AUD:Data2 is a hot field or requires assignment from browse
  BRW1.AddField(AUD:AccessLevel,BRW1.Q.AUD:AccessLevel)    ! Field AUD:AccessLevel is a hot field or requires assignment from browse
  BRW1.AddField(AUD:AuditDateTime,BRW1.Q.AUD:AuditDateTime) ! Field AUD:AuditDateTime is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
      ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section)
      ! Returns
      !   1   - Disable
      !   2   - Hide
      !   100 - Allow
      !   101 - Disallow
  
      LOC:User_Access     = Get_User_Access(GLO:UID, 'Audit', 'Browse_Audit', 'Update')
  
      db.debugout('LOC:User_Access: ' & LOC:User_Access)
  
      IF LOC:User_Access = 100 OR LOC:User_Access = 0
      ELSE
         DISABLE(?Change:4)
         DISABLE(?Delete:4)
         DISABLE(?Insert:4)
      .
  INIMgr.Fetch('Browse_Audit',QuickWindow)                 ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Audit.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Audit',QuickWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Audit
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
      IF LOC:User_Access = 100
      ELSE
         SELF.InsertControl=0
         SELF.ChangeControl=0
         SELF.DeleteControl=0
      .
  
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group1, Resize:FixLeft+Resize:FixBottom, Resize:ConstantRight+Resize:LockHeight) ! Override strategy for ?Group1
  SELF.SetStrategy(?Group2, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group2

