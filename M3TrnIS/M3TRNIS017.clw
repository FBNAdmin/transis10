

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS017.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Audit PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::AUD:Record  LIKE(AUD:RECORD),THREAD
QuickWindow          WINDOW('Form Audit'),AT(,,260,180),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM,MDI, |
  HLP('UpdateAudit'),SYSTEM
                       SHEET,AT(4,4,252,154),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('AUID:'),AT(194,6),USE(?AUD:AUID:Prompt),TRN
                           STRING(@n_10),AT(213,6,,10),USE(AUD:AUID),RIGHT(1),TRN
                           PROMPT('Date && Time:'),AT(9,22),USE(?AUD:AuditDate:Prompt),TRN
                           ENTRY(@d17),AT(57,22,81,10),USE(AUD:AuditDate),MSG('Audit entry date'),TIP('Audit entry date')
                           ENTRY(@t7),AT(166,22,84,10),USE(AUD:AuditTime),MSG('Audit entry time'),TIP('Audit entry time')
                           PROMPT('Severity:'),AT(9,36),USE(?AUD:Severity:Prompt),TRN
                           ENTRY(@n3),AT(57,36,40,10),USE(AUD:Severity),MSG('Severity level'),TIP('Severity level')
                           PROMPT('App Section:'),AT(9,52),USE(?AUD:AppSection:Prompt),TRN
                           TEXT,AT(57,52,194,30),USE(AUD:AppSection),VSCROLL,MSG('Program section'),TIP('Program section')
                           PROMPT('Data 1:'),AT(9,86),USE(?AUD:Data1:Prompt),TRN
                           TEXT,AT(57,86,194,30),USE(AUD:Data1),VSCROLL
                           PROMPT('Data 2:'),AT(9,120),USE(?AUD:Data2:Prompt),TRN
                           TEXT,AT(57,120,194,30),USE(AUD:Data2),VSCROLL
                           PROMPT('Access Level:'),AT(161,36),USE(?AUD:AccessLevel:Prompt),TRN
                           ENTRY(@n3),AT(210,36,40,10),USE(AUD:AccessLevel),MSG('Access Level required to see this entry'), |
  TIP('Access Level required to see this entry')
                         END
                       END
                       BUTTON('&OK'),AT(156,162,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(210,162,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,162,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Audit')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?AUD:AUID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(AUD:Record,History::AUD:Record)
  SELF.AddHistoryField(?AUD:AUID,1)
  SELF.AddHistoryField(?AUD:AuditDate,4)
  SELF.AddHistoryField(?AUD:AuditTime,5)
  SELF.AddHistoryField(?AUD:Severity,6)
  SELF.AddHistoryField(?AUD:AppSection,7)
  SELF.AddHistoryField(?AUD:Data1,8)
  SELF.AddHistoryField(?AUD:Data2,9)
  SELF.AddHistoryField(?AUD:AccessLevel,10)
  SELF.AddUpdateFile(Access:Audit)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Audit.Open                                        ! File Audit used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Audit
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?AUD:AuditDate{PROP:ReadOnly} = True
    ?AUD:AuditTime{PROP:ReadOnly} = True
    ?AUD:Severity{PROP:ReadOnly} = True
    ?AUD:AccessLevel{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Audit',QuickWindow)                 ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Audit.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Audit',QuickWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

