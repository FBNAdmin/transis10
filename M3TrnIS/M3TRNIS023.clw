

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS023.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Print_Layout PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Field_Name       STRING(50)                            !Field Name
History::PRIL:Record LIKE(PRIL:RECORD),THREAD
QuickWindow          WINDOW('Form Print Layout'),AT(,,188,145),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdatePrintLayout'),SYSTEM
                       SHEET,AT(4,4,180,123),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Field Name:'),AT(9,22),USE(?LOC:Field_Name:Prompt),TRN
                           BUTTON('...'),AT(50,22,12,10),USE(?CallLookup)
                           ENTRY(@s50),AT(66,22,114,10),USE(LOC:Field_Name),MSG('Field Name'),TIP('Field Name')
                           PROMPT('X Pos:'),AT(9,46),USE(?PRIL:XPos:Prompt),TRN
                           SPIN(@n_5),AT(101,46,80,10),USE(PRIL:XPos),RIGHT(1),MSG('X Position'),TIP('X Position')
                           PROMPT('Y Pos:'),AT(9,60),USE(?PRIL:YPos:Prompt),TRN
                           SPIN(@n_5),AT(101,60,80,10),USE(PRIL:YPos),RIGHT(1),MSG('Y Position'),TIP('Y Position')
                           PROMPT('Length:'),AT(9,78),USE(?PRIL:Length:Prompt),TRN
                           SPIN(@n_5),AT(101,78,80,10),USE(PRIL:Length),RIGHT(1)
                           PROMPT('Alignment:'),AT(9,98),USE(?PRIL:Alignment:Prompt),TRN
                           LIST,AT(101,98,80,10),USE(PRIL:Alignment),DROP(5),FROM('Left|#0|Center|#1|Right|#2'),MSG('Alignment'), |
  TIP('Alignment')
                           CHECK(' &Repeating'),AT(101,114),USE(PRIL:Repeating),MSG('Repeating section'),TIP('Repeating section'), |
  TRN
                         END
                       END
                       BUTTON('&OK'),AT(82,130,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(136,130,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,130,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Print_Layout')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Field_Name:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(PRIL:Record,History::PRIL:Record)
  SELF.AddHistoryField(?PRIL:XPos,4)
  SELF.AddHistoryField(?PRIL:YPos,5)
  SELF.AddHistoryField(?PRIL:Length,6)
  SELF.AddHistoryField(?PRIL:Alignment,7)
  SELF.AddHistoryField(?PRIL:Repeating,8)
  SELF.AddUpdateFile(Access:PrintLayout)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:PrintFields.SetOpenRelated()
  Relate:PrintFields.Open                                  ! File PrintFields used by this procedure, so make sure it's RelationManager is open
  Access:Prints.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PrintLayout
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?LOC:Field_Name{PROP:ReadOnly} = True
    DISABLE(?PRIL:Alignment)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Print_Layout',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PrintFields.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Print_Layout',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Print_Fields(PRI:PrintType)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      PRIF:FieldName = LOC:Field_Name
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:Field_Name = PRIF:FieldName
        PRIL:PFID = PRIF:PFID
      END
      ThisWindow.Reset(1)
    OF ?LOC:Field_Name
      IF LOC:Field_Name OR ?LOC:Field_Name{PROP:Req}
        PRIF:FieldName = LOC:Field_Name
        IF Access:PrintFields.TryFetch(PRIF:Key_FieldName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:Field_Name = PRIF:FieldName
            PRIL:PFID = PRIF:PFID
          ELSE
            CLEAR(PRIL:PFID)
            SELECT(?LOC:Field_Name)
            CYCLE
          END
        ELSE
          PRIL:PFID = PRIF:PFID
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF PRIL:PFID ~= 0
             PRIF:PFID            = PRIL:PFID
             IF Access:PrintFields.TryFetch(PRIF:PKey_PFID) = LEVEL:Benign
                LOC:Field_Name    = PRIF:FieldName
          .  .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

