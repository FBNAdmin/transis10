

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS014.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! & Packaging Types
!!! </summary>
Window_Merge_Commodities PROCEDURE (p:Type, p:ID)

LOC:Group            GROUP,PRE(LO)                         !
Commodity            STRING(35)                            !Commodity
CMID                 ULONG                                 !Commodity ID
                     END                                   !
LOC:Group2           GROUP,PRE(LO2)                        !
Commodity            STRING(35)                            !Commodity
CMID                 ULONG                                 !Commodity ID
                     END                                   !
LOC:Success          BYTE                                  !
LOC:Type             BYTE                                  !0 commodity, 1 packagingtype
QuickWindow          WINDOW('Merge Commodities'),AT(,,284,160),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('Window_Merge_Commodities'),SYSTEM
                       SHEET,AT(3,4,278,136),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           PROMPT('Commodity (keep):'),AT(10,30),USE(?Commodity:Prompt),TRN
                           BUTTON('...'),AT(99,30,12,10),USE(?CallLookup:3)
                           ENTRY(@s35),AT(115,30,154,10),USE(LO:Commodity),COLOR(00E9E9E9h),MSG('Commosity'),READONLY, |
  REQ,SKIP,TIP('Commosity')
                           PROMPT('Commodity (remove):'),AT(10,58),USE(?Commodity:Prompt:2),TRN
                           BUTTON('...'),AT(99,58,12,10),USE(?CallLookup:4)
                           ENTRY(@s35),AT(115,58,154,10),USE(LO2:Commodity),COLOR(00E9E9E9h),MSG('Commosity'),READONLY, |
  REQ,SKIP,TIP('Commosity')
                           BUTTON('&Merge'),AT(226,122,49,14),USE(?Ok:2),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                         END
                       END
                       BUTTON('&Cancel'),AT(232,142,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(3,142,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Window_Merge_Commodities')
  SELF.Request = GlobalRequest                             ! Store the incoming request
      LOC:Type    = p:Type
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Commodity:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Commodities.SetOpenRelated()
  Relate:Commodities.Open                                  ! File Commodities used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Access:DeliveryItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PackagingTypes.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Window_Merge_Commodities',QuickWindow)     ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      IF LOC:Type ~= 0
         ?Commodity:Prompt{PROP:Text}   = 'Packaging (keep):'
         ?Commodity:Prompt:2{PROP:Text} = 'Packaging (remove):'
         QuickWindow{PROP:Text}         = 'Merge Packagings'
  
         ?LO:Commodity{PROP:ToolTip}    = 'Packaging'
         ?LO2:Commodity{PROP:ToolTip}   = 'Packaging'
      .
  
  
      IF DEFORMAT(p:ID) ~= 0
         IF LOC:Type = 0
            COM:CMID  = p:ID
            IF Access:Commodities.TryFetch(COM:PKey_CMID) = LEVEL:Benign
               LO:CMID        = COM:CMID
               LO:Commodity   = COM:Commodity
            .
         ELSE
            PACK:PTID = p:ID
            IF Access:PackagingTypes.TryFetch(PACK:PKey_PTID) = LEVEL:Benign
               LO:CMID        = PACK:PTID
               LO:Commodity   = PACK:Packaging
      .  .  .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Commodities.Close
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('Window_Merge_Commodities',QuickWindow)  ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup:3
      ThisWindow.Update()
          GlobalRequest   = SelectRecord
          IF LOC:Type = 0
             COM:Commodity    = LO:Commodity
             Browse_Commodities('')
             IF GlobalResponse = RequestCompleted
                LO:Commodity  = COM:Commodity
                LO:CMID       = COM:CMID
             .
          ELSE
             PACK:Packaging   = LO:Commodity
             Browse_PackagingTypes()
             IF GlobalResponse = RequestCompleted
                LO:Commodity  = PACK:Packaging
                LO:CMID       = PACK:PTID
          .  .
          ThisWindow.Reset(1)
      
    OF ?CallLookup:4
      ThisWindow.Update()
          GlobalRequest   = SelectRecord
          IF LOC:Type = 0
             COM:Commodity    = LO2:Commodity
             Browse_Commodities('')
             IF GlobalResponse = RequestCompleted
                LO2:Commodity  = COM:Commodity
                LO2:CMID       = COM:CMID
             .
          ELSE
             PACK:Packaging   = LO2:Commodity
             Browse_PackagingTypes()
             IF GlobalResponse = RequestCompleted
                LO2:Commodity  = PACK:Packaging
                LO2:CMID       = PACK:PTID
          .  .
          ThisWindow.Reset(1)
      
    OF ?LO2:Commodity
          IF LO:CMID ~= 0
             IF LO:CMID = LO2:CMID
                MESSAGE('Cannot merge the same commodity.', 'Select Another', ICON:Exclamation)
                CLEAR(LOC:Group2)
                CYCLE
          .  .
      
      
          
    OF ?Ok:2
      ThisWindow.Update()
          Word_1_"    = 'commodities'
          Word_2_"    = 'commodity'
          Word_3_"    = 'CMID'
          IF LOC:Type = 1
             Word_1_"    = 'packagings'
             Word_2_"    = 'packaging'
             Word_3_"    = 'PTID'
          .
      
          IF LO:CMID = 0 OR LO2:CMID = 0
             MESSAGE('Please select both ' & CLIP(Word_1_") & '.', 'Select Commodity', ICON:Exclamation)
             CYCLE
          ELSE
             IF LO:CMID = LO2:CMID
                MESSAGE('Cannot merge the same ' & CLIP(Word_2_") & '.', 'Select Another', ICON:Exclamation)
                CLEAR(LOC:Group2)
                CYCLE
          .  .
      
      
          LOC:Success = 0
      
          _SQLTemp{PROP:SQL}  = 'UPDATE DeliveryItems SET ' & CLIP(Word_3_") & ' = ' & LO:CMID & ' WHERE ' & CLIP(Word_3_") & ' = ' & LO2:CMID
          IF ERRORCODE()
             LOC:Success  = 1
             MESSAGE('Failed to update Delivery Items||Error: ' & FILEERROR(), 'Failed', ICON:Hand)
          .
      
          IF LOC:Type = 0
             _SQLTemp{PROP:SQL}  = 'UPDATE _InvoiceItems SET CMID = ' & LO:CMID & ' WHERE CMID = ' & LO2:CMID
             IF ERRORCODE()
                LOC:Success  = 1
                MESSAGE('Failed to update Invoice Items||Error: ' & FILEERROR(), 'Failed', ICON:Hand)
          .  .
      
          IF LOC:Success > 0
             MESSAGE('There was at least one error, ' & CLIP(Word_2_") & ' will not be deleted.', 'Failed', ICON:Hand)
          ELSE
             IF LOC:Type = 1
                PACK:PTID = LO2:CMID
                IF Access:PackagingTypes.TryFetch(PACK:PKey_PTID) = LEVEL:Benign
                   IF Access:PackagingTypes.DeleteRecord(0) ~= LEVEL:Benign
                      MESSAGE('There was an error deleting the ' & CLIP(Word_2_") & '.', 'Failed', ICON:Hand)
                .  .
             ELSE
                COM:CMID = LO2:CMID
                IF Access:Commodities.TryFetch(COM:PKey_CMID) = LEVEL:Benign
                   IF Access:Commodities.DeleteRecord(0) ~= LEVEL:Benign
                      MESSAGE('There was an error deleting the ' & CLIP(Word_2_") & '.', 'Failed', ICON:Hand)
             .  .  .
      
             MESSAGE('Merging complete.', 'Complete', ICON:Asterisk)
             CLEAR(LOC:Group2)
             DISPlAY
          .
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Commodities PROCEDURE (p:Commodity)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:New_Commodity    STRING(35)                            !Commodity
BRW2::View:Browse    VIEW(_InvoiceItems)
                       PROJECT(INI:Type)
                       PROJECT(INI:Commodity)
                       PROJECT(INI:Description)
                       PROJECT(INI:Units)
                       PROJECT(INI:ITID)
                       PROJECT(INI:CMID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
INI:Type               LIKE(INI:Type)                 !List box control field - type derived from field
INI:Commodity          LIKE(INI:Commodity)            !List box control field - type derived from field
INI:Description        LIKE(INI:Description)          !List box control field - type derived from field
INI:Units              LIKE(INI:Units)                !List box control field - type derived from field
INI:ITID               LIKE(INI:ITID)                 !Primary key field - type derived from field
INI:CMID               LIKE(INI:CMID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(DeliveryItems)
                       PROJECT(DELI:ItemNo)
                       PROJECT(DELI:Type)
                       PROJECT(DELI:ContainerNo)
                       PROJECT(DELI:ContainerReturnAID)
                       PROJECT(DELI:ContainerVessel)
                       PROJECT(DELI:ETA)
                       PROJECT(DELI:ByContainer)
                       PROJECT(DELI:Length)
                       PROJECT(DELI:Breadth)
                       PROJECT(DELI:DIID)
                       PROJECT(DELI:CMID)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Type              LIKE(DELI:Type)                !List box control field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !List box control field - type derived from field
DELI:ContainerReturnAID LIKE(DELI:ContainerReturnAID) !List box control field - type derived from field
DELI:ContainerVessel   LIKE(DELI:ContainerVessel)     !List box control field - type derived from field
DELI:ETA               LIKE(DELI:ETA)                 !List box control field - type derived from field
DELI:ByContainer       LIKE(DELI:ByContainer)         !List box control field - type derived from field
DELI:Length            LIKE(DELI:Length)              !List box control field - type derived from field
DELI:Breadth           LIKE(DELI:Breadth)             !List box control field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Primary key field - type derived from field
DELI:CMID              LIKE(DELI:CMID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::COM:Record  LIKE(COM:RECORD),THREAD
QuickWindow          WINDOW('Form Commodities'),AT(,,213,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdateCommodities'),SYSTEM
                       SHEET,AT(4,4,205,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Commodity:'),AT(9,22),USE(?COM:Commodity:Prompt),TRN
                           ENTRY(@s35),AT(53,23,144,10),USE(COM:Commodity),MSG('Commosity'),REQ,TIP('Commosity')
                         END
                         TAB('&2) Invoice Items'),USE(?Tab:2)
                           LIST,AT(9,20,197,92),USE(?Browse:2),HVSCROLL,FORMAT('20R(2)|M~Type~C(0)@n3@80L(2)|M~Com' & |
  'modity~L(2)@s35@80L(2)|M~Description~L(2)@s150@28R(2)|M~Units~C(0)@n6@'),FROM(Queue:Browse:2), |
  IMM,MSG('Browsing the DeliveryItems file')
                         END
                         TAB('&3) Delivery Items'),USE(?Tab:3)
                           LIST,AT(9,20,197,92),USE(?Browse:4),HVSCROLL,FORMAT('32R(2)|M~Item No.~C(0)@n6@20R(2)|M' & |
  '~Type~C(0)@n3@80L(2)|M~Container No.~@s35@80L(2)|M~Container Return Address~C(0)@s20' & |
  '@80L(2)|M~Container Vessel~@s35@40R(2)|M~ETA~C(0)@d5@52R(2)|M~By Container~C(0)@n3@2' & |
  '8R(2)|M~Length~C(0)@n6@32R(2)|M~Breadth~C(0)@n6@'),FROM(Queue:Browse:4),IMM,MSG('Browsing t' & |
  'he DeliveryItems file')
                         END
                       END
                       BUTTON('&OK'),AT(108,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(160,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
                     END

BRW4::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Omitted               ROUTINE
    IF ~OMITTED(1)
       LOC:New_Commodity    = p:Commodity
    .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Commodities')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?COM:Commodity:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(COM:Record,History::COM:Record)
  SELF.AddHistoryField(?COM:Commodity,2)
  SELF.AddUpdateFile(Access:Commodities)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Commodities.SetOpenRelated()
  Relate:Commodities.Open                                  ! File Commodities used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Commodities
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:_InvoiceItems,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:DeliveryItems,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?COM:Commodity{PROP:ReadOnly} = True
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon INI:CMID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,INI:FKey_CMID)   ! Add the sort order for INI:FKey_CMID for sort order 1
  BRW2.AddRange(INI:CMID,Relate:_InvoiceItems,Relate:Commodities) ! Add file relationship range limit for sort order 1
  BRW2.AddField(INI:Type,BRW2.Q.INI:Type)                  ! Field INI:Type is a hot field or requires assignment from browse
  BRW2.AddField(INI:Commodity,BRW2.Q.INI:Commodity)        ! Field INI:Commodity is a hot field or requires assignment from browse
  BRW2.AddField(INI:Description,BRW2.Q.INI:Description)    ! Field INI:Description is a hot field or requires assignment from browse
  BRW2.AddField(INI:Units,BRW2.Q.INI:Units)                ! Field INI:Units is a hot field or requires assignment from browse
  BRW2.AddField(INI:ITID,BRW2.Q.INI:ITID)                  ! Field INI:ITID is a hot field or requires assignment from browse
  BRW2.AddField(INI:CMID,BRW2.Q.INI:CMID)                  ! Field INI:CMID is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon DELI:CMID for sort order 1
  BRW4.AddSortOrder(BRW4::Sort0:StepClass,DELI:FKey_CMID)  ! Add the sort order for DELI:FKey_CMID for sort order 1
  BRW4.AddRange(DELI:CMID,Relate:DeliveryItems,Relate:Commodities) ! Add file relationship range limit for sort order 1
  BRW4.AddField(DELI:ItemNo,BRW4.Q.DELI:ItemNo)            ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW4.AddField(DELI:Type,BRW4.Q.DELI:Type)                ! Field DELI:Type is a hot field or requires assignment from browse
  BRW4.AddField(DELI:ContainerNo,BRW4.Q.DELI:ContainerNo)  ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW4.AddField(DELI:ContainerReturnAID,BRW4.Q.DELI:ContainerReturnAID) ! Field DELI:ContainerReturnAID is a hot field or requires assignment from browse
  BRW4.AddField(DELI:ContainerVessel,BRW4.Q.DELI:ContainerVessel) ! Field DELI:ContainerVessel is a hot field or requires assignment from browse
  BRW4.AddField(DELI:ETA,BRW4.Q.DELI:ETA)                  ! Field DELI:ETA is a hot field or requires assignment from browse
  BRW4.AddField(DELI:ByContainer,BRW4.Q.DELI:ByContainer)  ! Field DELI:ByContainer is a hot field or requires assignment from browse
  BRW4.AddField(DELI:Length,BRW4.Q.DELI:Length)            ! Field DELI:Length is a hot field or requires assignment from browse
  BRW4.AddField(DELI:Breadth,BRW4.Q.DELI:Breadth)          ! Field DELI:Breadth is a hot field or requires assignment from browse
  BRW4.AddField(DELI:DIID,BRW4.Q.DELI:DIID)                ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW4.AddField(DELI:CMID,BRW4.Q.DELI:CMID)                ! Field DELI:CMID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Commodities',QuickWindow)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      DO Check_Omitted
  
      IF SELF.Request = InsertRecord AND CLIP(COM:Commodity) = '' AND CLIP(LOC:New_Commodity) ~= ''
         COM:Commodity    = LOC:New_Commodity
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Commodities.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Commodities',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Commodities PROCEDURE (p:Commodity)

CurrentTab           STRING(80)                            !
LOC:New_Commodity    STRING(35)                            !Commodity
LOC:Locator          STRING(50)                            !
BRW1::View:Browse    VIEW(Commodities)
                       PROJECT(COM:Commodity)
                       PROJECT(COM:CMID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Commodities File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('BrowseCommodities'),SYSTEM
                       LIST,AT(8,37,261,118),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Commodity~L(2)@s35@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the Commodities file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Commodity'),USE(?Tab:2)
                           PROMPT('Locator:'),AT(9,23),USE(?LOC:Locator:Prompt),TRN
                           STRING(@s50),AT(37,23),USE(LOC:Locator),TRN
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(128,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('Merge'),AT(4,180,,14),USE(?Button_Merge),LEFT,ICON('WIZDITTO.ICO'),FLAT,TIP('Merge 2 Co' & |
  'mmodity entries')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Omitted               ROUTINE
    IF CLIP(p:Commodity) ~= ''
       LOC:New_Commodity    = p:Commodity
    .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Commodities')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Commodities.Open                                  ! File Commodities used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Commodities,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon COM:Commodity for sort order 1
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,COM:Key_Commodity) ! Add the sort order for COM:Key_Commodity for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(?LOC:Locator,COM:Commodity,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: COM:Key_Commodity , COM:Commodity
  BRW1.AddField(COM:Commodity,BRW1.Q.COM:Commodity)        ! Field COM:Commodity is a hot field or requires assignment from browse
  BRW1.AddField(COM:CMID,BRW1.Q.COM:CMID)                  ! Field COM:CMID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Commodities',QuickWindow)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
      DO Check_Omitted
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Commodities.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Commodities',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Commodities(LOC:New_Commodity)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Merge
      BRW1.UpdateViewRecord()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Merge
      ThisWindow.Update()
      START(Window_Merge_Commodities, 25000, '0',COM:CMID)
      ThisWindow.Reset
          POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF SELF.Request = SelectRecord
             DISABLE(?Button_Merge)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_VehicleMakeModel PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(TruckTrailer)
                       PROJECT(TRU:TID)
                       PROJECT(TRU:Type)
                       PROJECT(TRU:Registration)
                       PROJECT(TRU:Capacity)
                       PROJECT(TRU:TTID)
                       PROJECT(TRU:VMMID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
TRU:TID                LIKE(TRU:TID)                  !List box control field - type derived from field
TRU:Type               LIKE(TRU:Type)                 !List box control field - type derived from field
TRU:Registration       LIKE(TRU:Registration)         !List box control field - type derived from field
TRU:Capacity           LIKE(TRU:Capacity)             !List box control field - type derived from field
TRU:TTID               LIKE(TRU:TTID)                 !Primary key field - type derived from field
TRU:VMMID              LIKE(TRU:VMMID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::VMM:Record  LIKE(VMM:RECORD),THREAD
QuickWindow          WINDOW('Form Vehicle Make / Model'),AT(,,220,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateVehicleMakeModel'),SYSTEM
                       SHEET,AT(4,4,212,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Make && Model:'),AT(9,22),USE(?VMM:MakeModel:Prompt),TRN
                           ENTRY(@s35),AT(69,22,144,10),USE(VMM:MakeModel),MSG('Make & Model'),REQ,TIP('Make & Model')
                           PROMPT('Type:'),AT(9,36),USE(?VMM:Type:Prompt),TRN
                           LIST,AT(69,36,66,10),USE(VMM:Type),DROP(5),FROM('Horse|#0|Trailer|#1|Rigid|#2'),MSG('Type of vehicle'), |
  TIP('Type of vehicle')
                           PROMPT('Capacity:'),AT(9,50),USE(?VMM:Capacity:Prompt),TRN
                           SPIN(@n-8.0),AT(69,50,66,10),USE(VMM:Capacity),RIGHT(1),MSG('In Kgs'),TIP('In Kgs')
                         END
                         TAB('&2) Truck / Trailer'),USE(?Tab:2),HIDE
                           LIST,AT(7,22,204,92),USE(?Browse:2),HVSCROLL,FORMAT('30R(2)|M~TID~C(0)@n_10@20R(2)|M~Ty' & |
  'pe~C(0)@n3@80L(2)|M~Registration~@s20@40R(2)|M~Capacity~C(0)@n-8.0@'),FROM(Queue:Browse:2), |
  IMM,MSG('Browsing the TruckTrailer file')
                         END
                       END
                       BUTTON('&OK'),AT(114,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(168,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Make / Model Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Make / Model Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_VehicleMakeModel')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?VMM:MakeModel:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(VMM:Record,History::VMM:Record)
  SELF.AddHistoryField(?VMM:MakeModel,2)
  SELF.AddHistoryField(?VMM:Type,3)
  SELF.AddHistoryField(?VMM:Capacity,4)
  SELF.AddUpdateFile(Access:VehicleMakeModel)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:TruckTrailer.Open                                 ! File TruckTrailer used by this procedure, so make sure it's RelationManager is open
  Access:VehicleMakeModel.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:VehicleMakeModel
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:TruckTrailer,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?VMM:MakeModel{PROP:ReadOnly} = True
    DISABLE(?VMM:Type)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon TRU:VMMID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,TRU:FKey_VMMID)  ! Add the sort order for TRU:FKey_VMMID for sort order 1
  BRW2.AddRange(TRU:VMMID,Relate:TruckTrailer,Relate:VehicleMakeModel) ! Add file relationship range limit for sort order 1
  BRW2.AddField(TRU:TID,BRW2.Q.TRU:TID)                    ! Field TRU:TID is a hot field or requires assignment from browse
  BRW2.AddField(TRU:Type,BRW2.Q.TRU:Type)                  ! Field TRU:Type is a hot field or requires assignment from browse
  BRW2.AddField(TRU:Registration,BRW2.Q.TRU:Registration)  ! Field TRU:Registration is a hot field or requires assignment from browse
  BRW2.AddField(TRU:Capacity,BRW2.Q.TRU:Capacity)          ! Field TRU:Capacity is a hot field or requires assignment from browse
  BRW2.AddField(TRU:TTID,BRW2.Q.TRU:TTID)                  ! Field TRU:TTID is a hot field or requires assignment from browse
  BRW2.AddField(TRU:VMMID,BRW2.Q.TRU:VMMID)                ! Field TRU:VMMID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_VehicleMakeModel',QuickWindow)      ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TruckTrailer.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_VehicleMakeModel',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_VehicleMakeModels PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:Type             STRING(10)                            !
BRW1::View:Browse    VIEW(VehicleMakeModel)
                       PROJECT(VMM:MakeModel)
                       PROJECT(VMM:Capacity)
                       PROJECT(VMM:Type)
                       PROJECT(VMM:VMMID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
VMM:MakeModel          LIKE(VMM:MakeModel)            !List box control field - type derived from field
LOC:Type               LIKE(LOC:Type)                 !List box control field - type derived from local data
VMM:Capacity           LIKE(VMM:Capacity)             !List box control field - type derived from field
VMM:Type               LIKE(VMM:Type)                 !Browse hot field - type derived from field
VMM:VMMID              LIKE(VMM:VMMID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Vehicle Makes / Models File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_VehicleMakeModels'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Make & Model~@s35@40L(2)' & |
  '|M~Type~@s10@40R(2)|M~Capacity~C(0)@n-8.0@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he VehicleMakeModel file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Make && Model'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_VehicleMakeModels')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Type',LOC:Type)                                ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:VehicleMakeModel.Open                             ! File VehicleMakeModel used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:VehicleMakeModel,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon VMM:MakeModel for sort order 1
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,VMM:Key_MakeModel) ! Add the sort order for VMM:Key_MakeModel for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,VMM:MakeModel,1,BRW1)          ! Initialize the browse locator using  using key: VMM:Key_MakeModel , VMM:MakeModel
  BRW1.AddField(VMM:MakeModel,BRW1.Q.VMM:MakeModel)        ! Field VMM:MakeModel is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Type,BRW1.Q.LOC:Type)                  ! Field LOC:Type is a hot field or requires assignment from browse
  BRW1.AddField(VMM:Capacity,BRW1.Q.VMM:Capacity)          ! Field VMM:Capacity is a hot field or requires assignment from browse
  BRW1.AddField(VMM:Type,BRW1.Q.VMM:Type)                  ! Field VMM:Type is a hot field or requires assignment from browse
  BRW1.AddField(VMM:VMMID,BRW1.Q.VMM:VMMID)                ! Field VMM:VMMID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_VehicleMakeModels',QuickWindow)     ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:VehicleMakeModel.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_VehicleMakeModels',QuickWindow)  ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_VehicleMakeModel
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.SetQueueRecord PROCEDURE

  CODE
      EXECUTE VMM:Type
         LOC:Type     = 'Trailer'
         LOC:Type     = 'Combined'
      ELSE
         LOC:Type     = 'Horse'
      .
  
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! --- filtering done on client - slow - make temp table?  whenever rates changed for client delete temp records?
!!! </summary>
Browse_Journeys PROCEDURE (p:CID)

CurrentTab           STRING(80)                            !
LOC:This_Client      BYTE                                  !Show Journeys for this client only
LOC:Locator          STRING(35)                            !
LOC:Clients_Using    ULONG                                 !
LOC:Clients_Using_Load BYTE                                !Show the no. of clients that use the Journey in their Rates (slow)
BRW1::View:Browse    VIEW(Journeys)
                       PROJECT(JOU:Journey)
                       PROJECT(JOU:EToll)
                       PROJECT(JOU:Description)
                       PROJECT(JOU:JID)
                       PROJECT(JOU:BID)
                       JOIN(BRA:PKey_BID,JOU:BID)
                         PROJECT(BRA:BranchName)
                         PROJECT(BRA:BID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
JOU:EToll              LIKE(JOU:EToll)                !List box control field - type derived from field
JOU:EToll_Icon         LONG                           !Entry's icon ID
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
JOU:Description        LIKE(JOU:Description)          !List box control field - type derived from field
LOC:Clients_Using      LIKE(LOC:Clients_Using)        !List box control field - type derived from local data
JOU:JID                LIKE(JOU:JID)                  !List box control field - type derived from field
JOU:BID                LIKE(JOU:BID)                  !Browse key field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Journeys File'),AT(,,355,246),FONT('Tahoma',8),RESIZE,GRAY,IMM,MAX,MDI, |
  HLP('BrowseJourneys'),SYSTEM
                       LIST,AT(10,37,335,164),USE(?Browse:1),HVSCROLL,FORMAT('124L(2)|M~Journey~26L(2)|MI~E-To' & |
  'll~C(0)@p p@52L(2)|M~Branch Name~C(0)@s35@124L(2)|M~Description~C(0)46R(2)|M~Clients' & |
  ' Using~C(0)@n13b@30R(2)|M~JID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing Records')
                       BUTTON('&Select'),AT(7,205,,14),USE(?Select:2),LEFT,ICON('WAselect.ICO'),FLAT
                       BUTTON('&Insert'),AT(172,205,,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT
                       BUTTON('&Change'),AT(227,205,,14),USE(?Change:3),LEFT,ICON('WAchange.ICO'),DEFAULT,FLAT
                       BUTTON('&Delete'),AT(288,205,,14),USE(?Delete:3),LEFT,ICON('WAdelete.ICO'),FLAT
                       CHECK(' This &Client'),AT(3,231),USE(LOC:This_Client),MSG('Show Journeys for this client only'), |
  TIP('Show Journeys for this client only')
                       SHEET,AT(4,4,347,220),USE(?CurrentTab)
                         TAB('By Journey'),USE(?Tab:2)
                           PROMPT('Locator:'),AT(10,23),USE(?LOC:Locator:Prompt),TRN
                           STRING(@s35),AT(39,23,166,10),USE(LOC:Locator),TRN
                         END
                         TAB('By Branch'),USE(?Tab:3),HIDE
                           BUTTON('Branches'),AT(72,205,45,14),USE(?SelectBranches)
                         END
                       END
                       BUTTON('Close'),AT(297,227,,14),USE(?Close),LEFT,ICON('WAclose.ico'),FLAT
                       BUTTON('Help'),AT(297,2,,14),USE(?Help),LEFT,ICON('WAhelp.ico'),FLAT,HIDE,STD(STD:Help)
                       CHECK(' Load Clients Using'),AT(172,231),USE(LOC:Clients_Using_Load),MSG('Show the no. ' & |
  'of clients that use the Journey in their Rates (slow)'),TIP('Show the no. of clients' & |
  ' that use the Journey in their Rates (slow)')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Journeys')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('GLO:BranchID',GLO:BranchID)                        ! Added by: BrowseBox(ABC)
  BIND('LOC:Clients_Using',LOC:Clients_Using)              ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Journeys,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW1.AddSortOrder(,JOU:FKey_BID)                         ! Add the sort order for JOU:FKey_BID for sort order 1
  BRW1.AddRange(JOU:BID,Relate:Journeys,Relate:Branches)   ! Add file relationship range limit for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(?LOC:Locator,JOU:BID,1,BRW1)    ! Initialize the browse locator using ?LOC:Locator using key: JOU:FKey_BID , JOU:BID
  BRW1.AppendOrder('+JOU:Journey,+JOU:JID')                ! Append an additional sort order
  BRW1.AddResetField(LOC:This_Client)                      ! Apply the reset field
  BRW1.AddSortOrder(,JOU:Key_Journey)                      ! Add the sort order for JOU:Key_Journey for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(?LOC:Locator,JOU:Journey,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: JOU:Key_Journey , JOU:Journey
  BRW1.AppendOrder('+JOU:JID')                             ! Append an additional sort order
  BRW1.AddResetField(LOC:Clients_Using_Load)               ! Apply the reset field
  BRW1.AddResetField(LOC:This_Client)                      ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(JOU:Journey,BRW1.Q.JOU:Journey)            ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW1.AddField(JOU:EToll,BRW1.Q.JOU:EToll)                ! Field JOU:EToll is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BranchName,BRW1.Q.BRA:BranchName)      ! Field BRA:BranchName is a hot field or requires assignment from browse
  BRW1.AddField(JOU:Description,BRW1.Q.JOU:Description)    ! Field JOU:Description is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Clients_Using,BRW1.Q.LOC:Clients_Using) ! Field LOC:Clients_Using is a hot field or requires assignment from browse
  BRW1.AddField(JOU:JID,BRW1.Q.JOU:JID)                    ! Field JOU:JID is a hot field or requires assignment from browse
  BRW1.AddField(JOU:BID,BRW1.Q.JOU:BID)                    ! Field JOU:BID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BID,BRW1.Q.BRA:BID)                    ! Field BRA:BID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Journeys',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      BRA:BID     = GLO:BranchID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
      .
      IF p:CID = 0
         HIDE(?LOC:This_Client)
      ELSE
         LOC:This_Client  = TRUE
         UNHIDE(?LOC:This_Client)
      .
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Journeys',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Journeys
    ReturnValue = GlobalResponse
  END
    IF SELF.Request = SelectRecord
       IF Request = InsertRecord
          IF ReturnValue = RequestCompleted
             ! Then assume the user has added a Client they want to Select
             IF JOU:JID ~= 0
                POST(EVENT:Accepted, ?Select:2)
    .  .  .  .
  
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LOC:This_Client
      !    ! Report this as a bug to SV
      !    IF RECORDS(Queue:Browse:1) = 0
      !       BRW1.ResetQueue(1)
      !    .
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Branches()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  IF LOC:Clients_Using_Load = TRUE
      CLEAR(LOC:Clients_Using)
  
      _SQLTemp{PROP:SQL}  = 'SELECT Journeys.JID, COUNT(DISTINCT Clients.CID) '   & |
                            'FROM __Rates INNER JOIN '                            & |
                            'Clients ON __Rates.CID = Clients.CID INNER JOIN '    & |
                            'Journeys ON __Rates.JID = Journeys.JID '             & |
                            'WHERE (Journeys.JID = ' & JOU:JID & ') '             & |
                            'GROUP BY Journeys.JID'
      IF ERRORCODE()
         db.debugout('[Browse_Journeys]  SQL error: ' & CLIP(ERROR()) & ',  File Err:  ' & FILEERROR())
      .
  
  !    db.debugout('[Browse_Journeys]  SQL: ' & _SQLTemp{PROP:SQL})
  
  
      NEXT(_SQLTemp)
      IF ~ERRORCODE()
         LOC:Clients_Using    = _SQ:S2
      ELSE
  !       db.debugout('[Browse_Journeys]  JOU:JID: ' & JOU:JID & ',  SQL error: ' & CLIP(ERROR()) & ',  File Err:  ' & FILEERROR())
      .
  
  
  
  
      _SQLTemp{PROP:SQL}  = 'SELECT     COUNT(DISTINCT Clients_ContainerParkDiscounts.CID) AS Expr1 ' & |
            'FROM         Journeys INNER JOIN ' & |
            '                      __RatesContainerPark ON Journeys.JID = __RatesContainerPark.JID INNER JOIN ' & |
            '                      Clients_ContainerParkDiscounts ON __RatesContainerPark.FID = Clients_ContainerParkDiscounts.FID ' & |
            'WHERE     (Journeys.JID = ' & JOU:JID & ')'
      IF ERRORCODE()
         db.debugout('[Browse_Journeys]  SQL error: ' & CLIP(ERROR()) & ',  File Err:  ' & FILEERROR())
      .
      NEXT(_SQLTemp)
      IF ~ERRORCODE()
         LOC:Clients_Using   += _SQ:S1
      ELSE
  !       db.debugout('[Browse_Journeys]  JOU:JID: ' & JOU:JID & ',  SQL error: ' & CLIP(ERROR()) & ',  File Err:  ' & FILEERROR())
      .
  .
  PARENT.SetQueueRecord
  
  IF (JOU:EToll = TRUE)
    SELF.Q.JOU:EToll_Icon = 2                              ! Set icon from icon list
  ELSE
    SELF.Q.JOU:EToll_Icon = 1                              ! Set icon from icon list
  END


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  !    Start_#     = CLOCK()
  
      IF LOC:This_Client = TRUE AND p:CID ~= 0
         IF Get_Clients_Related(p:CID, JOU:JID, 0, 1) = 0
            IF Get_Clients_CP_Related(p:CID, JOU:JID, 1) = 0
               RETURN Record:Filtered
      .  .  .
  
  !    db.debugout('Browse_Journeys - Time: ' & CLOCK() - Start_#)
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_LoadTypes PROCEDURE (p:Option, p:CID_str)

CurrentTab           STRING(80)                            !
LOC:This_Client      BYTE(1)                               !
LOC:CID              ULONG                                 !Client ID
LOC:Locals           GROUP,PRE()                           !
LO:LoadOption        STRING(20)                            !Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
                     END                                   !
BRW1::View:Browse    VIEW(LoadTypes2)
                       PROJECT(LOAD2:LoadType)
                       PROJECT(LOAD2:TurnIn)
                       PROJECT(LOAD2:Hazchem)
                       PROJECT(LOAD2:ContainerParkStandard)
                       PROJECT(LOAD2:LTID)
                       PROJECT(LOAD2:LoadOption)
                       PROJECT(LOAD2:FID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
LOAD2:LoadType         LIKE(LOAD2:LoadType)           !List box control field - type derived from field
LO:LoadOption          LIKE(LO:LoadOption)            !List box control field - type derived from local data
LOAD2:TurnIn           LIKE(LOAD2:TurnIn)             !List box control field - type derived from field
LOAD2:TurnIn_Icon      LONG                           !Entry's icon ID
LOAD2:Hazchem          LIKE(LOAD2:Hazchem)            !List box control field - type derived from field
LOAD2:Hazchem_Icon     LONG                           !Entry's icon ID
LOAD2:ContainerParkStandard LIKE(LOAD2:ContainerParkStandard) !List box control field - type derived from field
LOAD2:ContainerParkStandard_Icon LONG                 !Entry's icon ID
LOAD2:LTID             LIKE(LOAD2:LTID)               !List box control field - type derived from field
LOAD2:LoadOption       LIKE(LOAD2:LoadOption)         !Browse hot field - type derived from field
LOAD2:FID              LIKE(LOAD2:FID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Load Types File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('Browse_LoadTypes'),SYSTEM
                       LIST,AT(8,22,261,132),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Load Type~@s100@60L(2)|M' & |
  '~Load Option~C(0)@s20@32R(2)|MI~Turn In~C(0)@p p@36R(2)|MI~Hazchem~C(0)@p p@83R(10)|' & |
  'MI~Container Park Standard~C(0)@p p@40R(1)|M~LTID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM, |
  MSG('Browsing the LoadTypes2 file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       CHECK(' This Client'),AT(4,184),USE(LOC:This_Client),TIP('Show only Load Types for this Client')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Load Type'),USE(?Tab:2)
                         END
                         TAB('&2) By Floor'),USE(?Tab:3),HIDE
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(162,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_LoadTypes')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:LoadOption',LO:LoadOption)                      ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:ClientsAlias.Open                                 ! File ClientsAlias used by this procedure, so make sure it's RelationManager is open
  Relate:LoadTypes2.SetOpenRelated()
  Relate:LoadTypes2.Open                                   ! File LoadTypes2 used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LoadTypes2,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
          IF p:Option = 'V'
             DISABLE(?Insert:4)
             DISABLE(?Delete:4)
             DISABLE(?Change:4)
          .
      
          LOC:CID = p:CID_Str
      
          IF LOC:CID ~= 0
             A_CLI:CID    = LOC:CID
             IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) = LEVEL:Benign
                ?LOC:This_Client{PROP:Text}   = CLIP(?LOC:This_Client{PROP:Text}) & ' - ' & CLIP(A_CLI:ClientName)
             .
          ELSE
             DISABLE(?LOC:This_Client)
          .  
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,LOAD2:FKey_FID)                       ! Add the sort order for LOAD2:FKey_FID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,LOAD2:FID,1,BRW1)              ! Initialize the browse locator using  using key: LOAD2:FKey_FID , LOAD2:FID
  BRW1.AddSortOrder(,LOAD2:Key_LoadType)                   ! Add the sort order for LOAD2:Key_LoadType for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,LOAD2:LoadType,1,BRW1)         ! Initialize the browse locator using  using key: LOAD2:Key_LoadType , LOAD2:LoadType
  BRW1.AddResetField(LOC:This_Client)                      ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(LOAD2:LoadType,BRW1.Q.LOAD2:LoadType)      ! Field LOAD2:LoadType is a hot field or requires assignment from browse
  BRW1.AddField(LO:LoadOption,BRW1.Q.LO:LoadOption)        ! Field LO:LoadOption is a hot field or requires assignment from browse
  BRW1.AddField(LOAD2:TurnIn,BRW1.Q.LOAD2:TurnIn)          ! Field LOAD2:TurnIn is a hot field or requires assignment from browse
  BRW1.AddField(LOAD2:Hazchem,BRW1.Q.LOAD2:Hazchem)        ! Field LOAD2:Hazchem is a hot field or requires assignment from browse
  BRW1.AddField(LOAD2:ContainerParkStandard,BRW1.Q.LOAD2:ContainerParkStandard) ! Field LOAD2:ContainerParkStandard is a hot field or requires assignment from browse
  BRW1.AddField(LOAD2:LTID,BRW1.Q.LOAD2:LTID)              ! Field LOAD2:LTID is a hot field or requires assignment from browse
  BRW1.AddField(LOAD2:LoadOption,BRW1.Q.LOAD2:LoadOption)  ! Field LOAD2:LoadOption is a hot field or requires assignment from browse
  BRW1.AddField(LOAD2:FID,BRW1.Q.LOAD2:FID)                ! Field LOAD2:FID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_LoadTypes',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ClientsAlias.Close
    Relate:LoadTypes2.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_LoadTypes',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_LoadTypes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      !          IF p:Option = 'V'
      !             DISABLE(?Insert:4)
      !             DISABLE(?Delete:4)
      !             DISABLE(?Change:4)
      !          .
      !      
      !          LOC:CID = p:CID_Str
      !      
      !          IF LOC:CID ~= 0
      !             A_CLI:CID    = LOC:CID
      !             IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) = LEVEL:Benign
      !                ?LOC:This_Client{PROP:Text}   = CLIP(?LOC:This_Client{PROP:Text}) & ' - ' & CLIP(A_CLI:ClientName)
      !             .
      !          ELSE
      !             DISABLE(?LOC:This_Client)
      !          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
    IF p:Option ~= 'V'
  
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
     End
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
      EXECUTE LOAD2:LoadOption + 1
         LO:LoadOption    = 'Consolidated'
         LO:LoadOption    = 'Container Park'
         LO:LoadOption    = 'Container'
         LO:LoadOption    = 'Full Load'
         LO:LoadOption    = 'Empty Container'
         LO:LoadOption    = 'Local Delivery'
      .
  
  
  
  PARENT.SetQueueRecord
  
  IF (LOAD2:TurnIn = 1)
    SELF.Q.LOAD2:TurnIn_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.LOAD2:TurnIn_Icon = 1                           ! Set icon from icon list
  END
  IF (LOAD2:Hazchem = 1)
    SELF.Q.LOAD2:Hazchem_Icon = 2                          ! Set icon from icon list
  ELSE
    SELF.Q.LOAD2:Hazchem_Icon = 1                          ! Set icon from icon list
  END
  IF (LOAD2:ContainerParkStandard = 1)
    SELF.Q.LOAD2:ContainerParkStandard_Icon = 2            ! Set icon from icon list
  ELSE
    SELF.Q.LOAD2:ContainerParkStandard_Icon = 1            ! Set icon from icon list
  END


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
        IF LOC:This_Client = TRUE AND LOC:CID ~= 0
           ! Consolidated|Container Park|Container|Full Load|Empty Container|Local Delivery
           !      0           1              2         3           4               5
  
           IF LOAD2:LoadOption = 1
              IF Get_Clients_CP_Related(LOC:CID) = 0
                 RETURN Record:Filtered
              .
           ELSE
              IF Get_Clients_Related(LOC:CID, LOAD2:LTID, 1) = 0
                 RETURN Record:Filtered
        .  .  .
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_AdditionalCharges PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Client           STRING(35)                            !
Description          STRING(35)                            !Description
BRW2::View:Browse    VIEW(DeliveriesAdditionalCharges)
                       PROJECT(DELA:AdditionalCharge)
                       PROJECT(DELA:Charge)
                       PROJECT(DELA:DAID)
                       PROJECT(DELA:ACID)
                       PROJECT(DELA:DID)
                       JOIN(DEL:PKey_DID,DELA:DID)
                         PROJECT(DEL:DINo)
                         PROJECT(DEL:AdditionalCharge)
                         PROJECT(DEL:DID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DELA:AdditionalCharge  LIKE(DELA:AdditionalCharge)    !List box control field - type derived from field
DELA:Charge            LIKE(DELA:Charge)              !List box control field - type derived from field
DEL:AdditionalCharge   LIKE(DEL:AdditionalCharge)     !List box control field - type derived from field
DELA:DAID              LIKE(DELA:DAID)                !Primary key field - type derived from field
DELA:ACID              LIKE(DELA:ACID)                !Browse key field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::CARA:Record LIKE(CARA:RECORD),THREAD
QuickWindow          WINDOW('Form Additional Charges'),AT(,,240,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_AdditionalCharges'),SYSTEM
                       SHEET,AT(4,4,232,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Client:'),AT(9,22),USE(?LOC:Client:Prompt),TRN
                           BUTTON('...'),AT(97,22,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(113,22,118,10),USE(LOC:Client),REQ,TIP('Client name')
                           PROMPT('CID:'),AT(155,34),USE(?CARA:CID:Prompt),TRN
                           STRING(@n_10),AT(171,34,,10),USE(CARA:CID),RIGHT(1),TRN
                           PROMPT('Charge Description:'),AT(9,46),USE(?Description:Prompt),TRN
                           BUTTON('...'),AT(97,46,12,10),USE(?CallLookup:2)
                           ENTRY(@s35),AT(113,46,118,10),USE(Description),MSG('Description'),TIP('Description')
                           PROMPT('ACCID:'),AT(155,58),USE(?CARA:ACCID:Prompt),TRN
                           STRING(@n_10),AT(171,58),USE(CARA:ACCID),RIGHT(1),TRN
                           PROMPT('Charge:'),AT(9,84),USE(?CARA:Charge:Prompt),TRN
                           ENTRY(@n-15.2),AT(162,84,68,10),USE(CARA:Charge),RIGHT(1),MSG('Charge amount'),TIP('Charge amount')
                           PROMPT('Effective Date:'),AT(9,100),USE(?CARA:Effective_Date:Prompt),TRN
                           BUTTON('...'),AT(146,100,12,10),USE(?Calendar)
                           SPIN(@d5),AT(162,100,68,10),USE(CARA:Effective_Date),RIGHT(1),MSG('Effective from this date'), |
  REQ,TIP('Effective from this date')
                         END
                         TAB('&2) Deliveries Additional Charges'),USE(?Tab:2),HIDE
                           LIST,AT(9,20,223,92),USE(?Browse:2),HVSCROLL,FORMAT('30R(2)|M~DI No.~L@n_10@80L(2)|M~Ad' & |
  'ditional Charge~@s35@54R(1)|M~Setup Charge~C(0)@n-15.2@54R(1)|M~Additional Charge~C(' & |
  '0)@n-15.2@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the DeliveriesAdditionalCharges file')
                         END
                       END
                       BUTTON('&OK'),AT(134,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(186,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar3            CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record Additional Charges Record'
  OF InsertRecord
    ActionMessage = 'Additional Charges Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Add. Charges Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_AdditionalCharges')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Client:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CARA:Record,History::CARA:Record)
  SELF.AddHistoryField(?CARA:CID,3)
  SELF.AddHistoryField(?CARA:ACCID,2)
  SELF.AddHistoryField(?CARA:Charge,4)
  SELF.AddHistoryField(?CARA:Effective_Date,7)
  SELF.AddUpdateFile(Access:__RatesAdditionalCharges)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:AdditionalCharges.SetOpenRelated()
  Relate:AdditionalCharges.Open                            ! File AdditionalCharges used by this procedure, so make sure it's RelationManager is open
  Access:__RatesAdditionalCharges.UseFile                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:__RatesAdditionalCharges
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:DeliveriesAdditionalCharges,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?LOC:Client{PROP:ReadOnly} = True
    DISABLE(?CallLookup:2)
    ?Description{PROP:ReadOnly} = True
    ?CARA:Charge{PROP:ReadOnly} = True
    DISABLE(?Calendar)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,DELA:FKey_ACID)                       ! Add the sort order for DELA:FKey_ACID for sort order 1
  BRW2.AddRange(DELA:ACID,Relate:DeliveriesAdditionalCharges,Relate:__RatesAdditionalCharges) ! Add file relationship range limit for sort order 1
  BRW2.AddField(DEL:DINo,BRW2.Q.DEL:DINo)                  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW2.AddField(DELA:AdditionalCharge,BRW2.Q.DELA:AdditionalCharge) ! Field DELA:AdditionalCharge is a hot field or requires assignment from browse
  BRW2.AddField(DELA:Charge,BRW2.Q.DELA:Charge)            ! Field DELA:Charge is a hot field or requires assignment from browse
  BRW2.AddField(DEL:AdditionalCharge,BRW2.Q.DEL:AdditionalCharge) ! Field DEL:AdditionalCharge is a hot field or requires assignment from browse
  BRW2.AddField(DELA:DAID,BRW2.Q.DELA:DAID)                ! Field DELA:DAID is a hot field or requires assignment from browse
  BRW2.AddField(DELA:ACID,BRW2.Q.DELA:ACID)                ! Field DELA:ACID is a hot field or requires assignment from browse
  BRW2.AddField(DEL:DID,BRW2.Q.DEL:DID)                    ! Field DEL:DID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_AdditionalCharges',QuickWindow)     ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AdditionalCharges.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_AdditionalCharges',QuickWindow)  ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_RateMod_Clients
      Browse_AdditionalChargeCategories
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = LOC:Client
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:Client = CLI:ClientName
        CARA:CID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?LOC:Client
      IF LOC:Client OR ?LOC:Client{PROP:Req}
        CLI:ClientName = LOC:Client
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:Client = CLI:ClientName
            CARA:CID = CLI:CID
          ELSE
            CLEAR(CARA:CID)
            SELECT(?LOC:Client)
            CYCLE
          END
        ELSE
          CARA:CID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update()
      ACCA:Description = Description
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        Description = ACCA:Description
        CARA:ACCID = ACCA:ACCID
      END
      ThisWindow.Reset(1)
    OF ?Description
      IF Description OR ?Description{PROP:Req}
        ACCA:Description = Description
        IF Access:AdditionalCharges.TryFetch(ACCA:Key_Description)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            Description = ACCA:Description
            CARA:ACCID = ACCA:ACCID
          ELSE
            CLEAR(CARA:ACCID)
            SELECT(?Description)
            CYCLE
          END
        ELSE
          CARA:ACCID = ACCA:ACCID
        END
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
      Calendar3.SelectOnClose = True
      Calendar3.Ask('Select a Date',CARA:Effective_Date)
      IF Calendar3.Response = RequestCompleted THEN
      CARA:Effective_Date=Calendar3.SelectedDate
      DISPLAY(?CARA:Effective_Date)
      END
      ThisWindow.Reset(True)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF SELF.Request = InsertRecord
             CARA:CID     = CLI:CID
          .
      
          CLI:CID         = CARA:CID
          IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
             LOC:Client   = CLI:ClientName
          .
      
          ACCA:ACCID      = CARA:ACCID
          IF Access:AdditionalCharges.TryFetch(ACCA:PKey_ACCID) = LEVEL:Benign
             Description  = ACCA:Description
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Statement_Run_Desc PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(_Statement_Run_Desc)
                       PROJECT(STDES:RunDescription)
                       PROJECT(STDES:SRDID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
STDES:RunDescription   LIKE(STDES:RunDescription)     !List box control field - type derived from field
STDES:SRDID            LIKE(STDES:SRDID)              !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Statement Run Desc. File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_Statement_Run_Desc'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Run Description~L(2)@s35@'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing the _Statement_Run_Desc file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Run Description'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Statement_Run_Desc')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:_Statement_Run_Desc.SetOpenRelated()
  Relate:_Statement_Run_Desc.Open                          ! File _Statement_Run_Desc used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_Statement_Run_Desc,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,STDES:Key_RunDesc)                    ! Add the sort order for STDES:Key_RunDesc for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,STDES:RunDescription,1,BRW1)   ! Initialize the browse locator using  using key: STDES:Key_RunDesc , STDES:RunDescription
  BRW1.AddField(STDES:RunDescription,BRW1.Q.STDES:RunDescription) ! Field STDES:RunDescription is a hot field or requires assignment from browse
  BRW1.AddField(STDES:SRDID,BRW1.Q.STDES:SRDID)            ! Field STDES:SRDID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Statement_Run_Desc',QuickWindow)    ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Statement_Run_Desc.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Statement_Run_Desc',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Statement_Run_Desc
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

