

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS002.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Addresses PROCEDURE (p:Type)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
L_Return_Group       GROUP,PRE(L_RG)                       !
Suburb               STRING(50)                            !Suburb
PostalCode           STRING(10)                            !
Country              STRING(50)                            !Country
Province             STRING(35)                            !Province
City                 STRING(35)                            !City
                     END                                   !
LOC:Type             STRING(2)                             !
LOC:Screen_Group     GROUP,PRE(L_SG)                       !
BranchName           STRING(35)                            !Branch Name
                     END                                   !
LOC:Clients_Using    LONG                                  !
LOC:No_Change        BYTE                                  !
BRW8::View:Browse    VIEW(EmailAddresses)
                       PROJECT(EMAI:EmailName)
                       PROJECT(EMAI:RateLetter)
                       PROJECT(EMAI:EmailAddress)
                       PROJECT(EMAI:EAID)
                       PROJECT(EMAI:AID)
                     END
Queue:Browse:8       QUEUE                            !Queue declaration for browse/combo box using ?Browse:8
EMAI:EmailName         LIKE(EMAI:EmailName)           !List box control field - type derived from field
EMAI:RateLetter        LIKE(EMAI:RateLetter)          !List box control field - type derived from field
EMAI:RateLetter_Icon   LONG                           !Entry's icon ID
EMAI:EmailAddress      LIKE(EMAI:EmailAddress)        !List box control field - type derived from field
EMAI:EAID              LIKE(EMAI:EAID)                !Primary key field - type derived from field
EMAI:AID               LIKE(EMAI:AID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(AddressContacts)
                       PROJECT(ADDC:ContactName)
                       PROJECT(ADDC:ACID)
                       PROJECT(ADDC:AID)
                     END
Queue:Browse:10      QUEUE                            !Queue declaration for browse/combo box using ?Browse:10
ADDC:ContactName       LIKE(ADDC:ContactName)         !List box control field - type derived from field
ADDC:ACID              LIKE(ADDC:ACID)                !Primary key field - type derived from field
ADDC:AID               LIKE(ADDC:AID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB2::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?L_SG:BranchName
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::ADD:Record  LIKE(ADD:RECORD),THREAD
QuickWindow          WINDOW('Update the Addresses File'),AT(,,270,309),FONT('Tahoma',8),RESIZE,GRAY,IMM,MDI,HLP('UpdateAddresses'), |
  SYSTEM
                       SHEET,AT(4,4,261,282),USE(?CurrentTab),JOIN
                         TAB('General'),USE(?Tab:1)
                           PROMPT('AID:'),AT(165,6),USE(?ADD:AID:Prompt)
                           STRING(@n_10),AT(178,6),USE(ADD:AID),RIGHT(1),TRN
                           PROMPT('Name:'),AT(11,23),USE(?ADD:Name:Prompt),TRN
                           ENTRY(@s35),AT(67,23,144,10),USE(ADD:AddressName),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                           PROMPT('Name && Suburb:'),AT(11,79),USE(?ADD:AddressNameSuburb:Prompt),TRN
                           ENTRY(@s50),AT(67,79,144,10),USE(ADD:AddressNameSuburb),COLOR(00E9E9E9h),MSG('Address Na' & |
  'me & Suburb'),READONLY,SKIP,TIP('Address Name & Suburb')
                           PROMPT('Line 1:'),AT(11,36),USE(?ADD:Line1:Prompt),TRN
                           ENTRY(@s35),AT(67,36,144,10),USE(ADD:Line1),MSG('Address line 1'),TIP('Address line 1')
                           PROMPT('Line 2:'),AT(11,50),USE(?ADD:Line2:Prompt),TRN
                           ENTRY(@s35),AT(67,50,144,10),USE(ADD:Line2),MSG('Address line 2'),TIP('Address line 2')
                           STRING('Suburb:'),AT(11,66),USE(?String1),TRN
                           BUTTON('...'),AT(51,66,12,10),USE(?CallLookup)
                           ENTRY(@s50),AT(67,66,144,10),USE(L_RG:Suburb),MSG('Suburb'),REQ,TIP('Suburb')
                           PROMPT('Postal Code:'),AT(11,95),USE(?L_RG:PostalCode:Prompt),TRN
                           ENTRY(@s10),AT(67,95,70,10),USE(L_RG:PostalCode),COLOR(00E9E9E9h),READONLY,REQ,SKIP
                           PROMPT('Country:'),AT(11,135),USE(?L_RG:Country:Prompt),TRN
                           ENTRY(@s50),AT(67,135,144,10),USE(L_RG:Country),COLOR(00E9E9E9h),MSG('Country'),READONLY,REQ, |
  SKIP,TIP('Country')
                           PROMPT('Province:'),AT(11,122),USE(?L_RG:Province:Prompt),TRN
                           ENTRY(@s35),AT(67,122,144,10),USE(L_RG:Province),COLOR(00E9E9E9h),MSG('Province'),READONLY, |
  REQ,SKIP,TIP('Province')
                           PROMPT('City:'),AT(11,109),USE(?L_RG:City:Prompt),TRN
                           ENTRY(@s35),AT(67,109,144,10),USE(L_RG:City),COLOR(00E9E9E9h),MSG('City'),READONLY,REQ,SKIP, |
  TIP('City')
                           PROMPT('Phone Nos.:'),AT(11,151),USE(?ADD:PhoneNo:Prompt),TRN
                           ENTRY(@s20),AT(67,151,70,10),USE(ADD:PhoneNo),MSG('Phone no.'),TIP('Phone no.')
                           ENTRY(@s20),AT(140,151,70,10),USE(ADD:PhoneNo2),MSG('Phone no. 2'),TIP('Phone no. 2')
                           PROMPT('Fax:'),AT(11,167),USE(?ADD:Fax:Prompt),TRN
                           ENTRY(@s60),AT(67,167,144,10),USE(ADD:Fax),MSG('Fax'),TIP('Fax')
                           PROMPT('Branch:'),AT(11,183),USE(?ADD:Fax:Prompt:2),TRN
                           LIST,AT(67,183,70,10),USE(L_SG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                           CHECK(' Show all Branches'),AT(140,183),USE(ADD:ShowForAllBranches),MSG('Show this addr' & |
  'ess for all branches'),TIP('Show this address for all branches')
                           CHECK(' &Journey'),AT(67,199),USE(ADD:Journey),MSG('This is used for a Journey'),TIP('This is us' & |
  'ed for a Journey'),TRN
                           CHECK(' &Branch'),AT(67,210,70,10),USE(ADD:Branch),MSG('This is used for a branch'),TIP('This is us' & |
  'ed for a branch'),TRN
                           CHECK(' &Accountant'),AT(67,223,70,10),USE(ADD:Accountant),MSG('This is used for an accountant'), |
  TIP('This is used for an accountant'),TRN
                           CHECK(' &Transporter'),AT(140,199,70,10),USE(ADD:Transporter),MSG('This is used for a T' & |
  'ransporter'),TIP('This is used for a Transporter'),TRN
                           CHECK(' &Client'),AT(140,210,70,10),USE(ADD:Client),MSG('This is used for a client'),TIP('This is us' & |
  'ed for a client'),TRN
                           CHECK(' &Delivery'),AT(140,223,70,10),USE(ADD:Delivery),MSG('This is used for a delivery'), |
  TIP('This is used for a delivery'),TRN
                           CHECK(' Archived'),AT(66,254),USE(ADD:Archived),MSG('Mark Address as Archived'),TIP('Mark Addre' & |
  'ss as Archived')
                           PROMPT('Archived Date:'),AT(135,254),USE(?ADD:Archived_Date:Prompt)
                           ENTRY(@d17),AT(191,253,60,10),USE(ADD:Archived_Date),RIGHT(1),READONLY,SKIP
                           PROMPT('Archived Time:'),AT(135,268),USE(?ADD:Archived_Time:Prompt)
                           ENTRY(@t7),AT(191,267,60,10),USE(ADD:Archived_Time),RIGHT(1),READONLY,SKIP
                         END
                         TAB('Email Addresses'),USE(?Tab:6)
                           LIST,AT(9,26,249,237),USE(?Browse:8),HVSCROLL,FORMAT('80L(2)|M~Name~@s35@24L(2)|MI~Rate' & |
  ' Letter~@p p@80L(2)|M~Email Address~@s255@'),FROM(Queue:Browse:8),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(86,267,,14),USE(?Insert:9),LEFT,ICON('wainsert.ico'),FLAT
                           BUTTON('&Change'),AT(142,267,,14),USE(?Change:9),LEFT,ICON('wachange.ico'),FLAT
                           BUTTON('&Delete'),AT(202,267,,14),USE(?Delete:9),LEFT,ICON('wadelete.ico'),FLAT
                         END
                         TAB('Address Contacts'),USE(?Tab:7)
                           LIST,AT(9,27,249,236),USE(?Browse:10),HVSCROLL,FORMAT('80L(2)|M~Contact Name~L(2)@s35@'),FROM(Queue:Browse:10), |
  IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(86,267,,14),USE(?Insert:11),LEFT,ICON('wainsert.ico'),FLAT
                           BUTTON('&Change'),AT(142,267,,14),USE(?Change:11),LEFT,ICON('wachange.ico'),FLAT
                           BUTTON('&Delete'),AT(202,267,,14),USE(?Delete:11),LEFT,ICON('wadelete.ico'),FLAT
                         END
                       END
                       BUTTON('&OK'),AT(169,290,45,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT
                       BUTTON('Cancel'),AT(217,290,45,14),USE(?Cancel),LEFT,ICON('WACancel.ICO'),FLAT
                       BUTTON('Help'),AT(4,290,45,14),USE(?Help),LEFT,ICON('WAhelp.ICO'),FLAT,HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)                    ! Browse using ?Browse:8
Q                      &Queue:Browse:8                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW8::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW10                CLASS(BrowseClass)                    ! Browse using ?Browse:10
Q                      &Queue:Browse:10               !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW10::Sort0:StepClass StepRealClass                       ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB2                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
View_Clients            VIEW(ClientsAlias)
    PROJECT(A_CLI:AID,A_CLI:CID,A_CLI:ClientNo,A_CLI:ClientName)
    .


Clients_View            ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Get_Suburb                         ROUTINE
    L_Return_Group          = Get_Suburb_Details( ADD:SUID )

    ! Create combined name - which is used on Deliveries lookups...
    ADD:AddressNameSuburb   = CLIP(ADD:AddressName) & ' - ' & L_RG:Suburb

    DISPLAY
    EXIT
Check_Omitted                    ROUTINE
    IF ~OMITTED(1)
       LOC:Type = p:Type
    .
    EXIT
Check_Address_Usage       ROUTINE
    DATA
R:Count         LONG

    CODE
    Clients_View.Init(View_Clients, Relate:ClientsAlias)
    Clients_View.AddSortOrder(A_CLI:FKey_AID)
    Clients_View.AddRange(A_CLI:AID, ADD:AID)

    Clients_View.Reset()
    LOOP
       IF Clients_View.Next() ~= LEVEL:Benign
          BREAK
       .

       R:Count  += 1
    .

    LOC:Clients_Using       = R:Count

    Clients_View.Kill()
    EXIT
Advise_Address_Usage       ROUTINE
    DATA
R:Of            LONG
R:Show_Names    BYTE

    CODE
    IF LOC:Clients_Using > 1
       CASE MESSAGE('There are ' & LOC:Clients_Using & ' Clients using this address.||If you change it all these Clients will have their addresses changed.|To avoid this create a new Address for this Client instead.||Would you like to see the Clients using this address?', 'Address Usage', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
       OF BUTTON:Yes
          R:Show_Names  = TRUE
    .  .

    IF R:Show_Names = TRUE
       R:Of     = 0

       Clients_View.Init(View_Clients, Relate:ClientsAlias)
       Clients_View.AddSortOrder(A_CLI:FKey_AID)
       Clients_View.AddRange(A_CLI:AID, ADD:AID)
       Clients_View.Reset()
       LOOP
          IF Clients_View.Next() ~= LEVEL:Benign
             BREAK
          .

          R:Of  += 1
          CASE MESSAGE('Client ' & R:Of & ' of ' & LOC:Clients_Using & ' using this address:||Client: ' & CLIP(A_CLI:ClientName) & ' (' & A_CLI:ClientNo & ')', 'Address Usage', ICON:Question, BUTTON:Ok+BUTTON:Cancel, BUTTON:Ok)
          OF BUTTON:Cancel
             BREAK
    .  .  .
    EXIT
Apply_User_Permissions      ROUTINE               ! Special user permissions
    !IF ThisWindow.Request = ChangeRecord AND ADD:Client = TRUE AND LOC:Clients_Using > 0
    IF GlobalRequest = ChangeRecord AND ADD:Client = TRUE AND LOC:Clients_Using > 0
                !AND LOC:Original_Request = ChangeRecord
       ! (ULONG, STRING, STRING, <STRING>, BYTE=0, <*BYTE>),LONG
       ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned)

       ! Returns
       !   0   - Allow (default)
       !   1   - Disable
       !   2   - Hide
       !   100 - Allow                 - Default action
       !   101 - Disallow              - Default action

       CASE Get_User_Access(GLO:UID, 'Addresses', 'Update_Addresses', 'Change_Mode_Client_Address', 0)
       OF 0 OROF 100
       !OF 2
       ELSE
          LOC:No_Change = TRUE
    .  .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Address Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Address Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Addresses')
    DO Check_Address_Usage
    DO Apply_User_Permissions
  
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ADD:AID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Addresses)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ADD:Record,History::ADD:Record)
  SELF.AddHistoryField(?ADD:AID,1)
  SELF.AddHistoryField(?ADD:AddressName,3)
  SELF.AddHistoryField(?ADD:AddressNameSuburb,4)
  SELF.AddHistoryField(?ADD:Line1,5)
  SELF.AddHistoryField(?ADD:Line2,6)
  SELF.AddHistoryField(?ADD:PhoneNo,8)
  SELF.AddHistoryField(?ADD:PhoneNo2,9)
  SELF.AddHistoryField(?ADD:Fax,10)
  SELF.AddHistoryField(?ADD:ShowForAllBranches,11)
  SELF.AddHistoryField(?ADD:Journey,19)
  SELF.AddHistoryField(?ADD:Branch,13)
  SELF.AddHistoryField(?ADD:Accountant,15)
  SELF.AddHistoryField(?ADD:Transporter,18)
  SELF.AddHistoryField(?ADD:Client,14)
  SELF.AddHistoryField(?ADD:Delivery,16)
  SELF.AddHistoryField(?ADD:Archived,20)
  SELF.AddHistoryField(?ADD:Archived_Date,23)
  SELF.AddHistoryField(?ADD:Archived_Time,24)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Add_Countries.SetOpenRelated()
  Relate:Add_Countries.Open                                ! File Add_Countries used by this procedure, so make sure it's RelationManager is open
  Relate:ClientsAlias.Open                                 ! File ClientsAlias used by this procedure, so make sure it's RelationManager is open
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Add_Suburbs.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Addresses
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW8.Init(?Browse:8,Queue:Browse:8.ViewPosition,BRW8::View:Browse,Queue:Browse:8,Relate:EmailAddresses,SELF) ! Initialize the browse manager
  BRW10.Init(?Browse:10,Queue:Browse:10.ViewPosition,BRW10::View:Browse,Queue:Browse:10,Relate:AddressContacts,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW8.Q &= Queue:Browse:8
  BRW8::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon EMAI:AID for sort order 1
  BRW8.AddSortOrder(BRW8::Sort0:StepClass,EMAI:FKey_AID)   ! Add the sort order for EMAI:FKey_AID for sort order 1
  BRW8.AddRange(EMAI:AID,ADD:AID)                          ! Add single value range limit for sort order 1
  BRW8.AppendOrder('+EMAI:EmailName,+EMAI:EAID')           ! Append an additional sort order
  ?Browse:8{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:8{PROP:IconList,2} = '~checkon.ico'
  BRW8.AddField(EMAI:EmailName,BRW8.Q.EMAI:EmailName)      ! Field EMAI:EmailName is a hot field or requires assignment from browse
  BRW8.AddField(EMAI:RateLetter,BRW8.Q.EMAI:RateLetter)    ! Field EMAI:RateLetter is a hot field or requires assignment from browse
  BRW8.AddField(EMAI:EmailAddress,BRW8.Q.EMAI:EmailAddress) ! Field EMAI:EmailAddress is a hot field or requires assignment from browse
  BRW8.AddField(EMAI:EAID,BRW8.Q.EMAI:EAID)                ! Field EMAI:EAID is a hot field or requires assignment from browse
  BRW8.AddField(EMAI:AID,BRW8.Q.EMAI:AID)                  ! Field EMAI:AID is a hot field or requires assignment from browse
  BRW10.Q &= Queue:Browse:10
  BRW10::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)      ! Moveable thumb based upon ADDC:AID for sort order 1
  BRW10.AddSortOrder(BRW10::Sort0:StepClass,ADDC:FKey_AID) ! Add the sort order for ADDC:FKey_AID for sort order 1
  BRW10.AddRange(ADDC:AID,ADD:AID)                         ! Add single value range limit for sort order 1
  BRW10.AddField(ADDC:ContactName,BRW10.Q.ADDC:ContactName) ! Field ADDC:ContactName is a hot field or requires assignment from browse
  BRW10.AddField(ADDC:ACID,BRW10.Q.ADDC:ACID)              ! Field ADDC:ACID is a hot field or requires assignment from browse
  BRW10.AddField(ADDC:AID,BRW10.Q.ADDC:AID)                ! Field ADDC:AID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Addresses',QuickWindow)             ! Restore window settings from non-volatile store
      DO Get_Suburb
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      DO Check_Omitted
  
      CASE LOC:Type
      OF 'DC' OROF 'DD'
         ADD:Delivery     = TRUE
      OF 'CL'
         ADD:Client       = TRUE
      .
      IF SELF.Request = InsertRecord
         BRA:BID             = GLO:BranchID
         IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
            L_SG:BranchName  = BRA:BranchName
            ADD:BID          = BRA:BID
        .
      ELSE
         BRA:BID             = ADD:BID
         IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
            L_SG:BranchName  = BRA:BranchName
      .  .
  
  BRW8.AskProcedure = 2
  BRW10.AskProcedure = 3
  FDB2.Init(?L_SG:BranchName,Queue:FileDrop.ViewPosition,FDB2::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB2.Q &= Queue:FileDrop
  FDB2.AddSortOrder(BRA:Key_BranchName)
  FDB2.AddField(BRA:BranchName,FDB2.Q.BRA:BranchName) !List box control field - type derived from field
  FDB2.AddField(BRA:BID,FDB2.Q.BRA:BID) !Primary key field - type derived from field
  FDB2.AddUpdateField(BRA:BID,ADD:BID)
  ThisWindow.AddItem(FDB2.WindowComponent)
  FDB2.DefaultFill = 0
     IF LOC:No_Change = TRUE
        DISABLE(?ADD:Name:Prompt, ?ADD:ShowForAllBranches)
        DISABLE(?ADD:Client)
     .
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Countries.Close
    Relate:ClientsAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Addresses',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Suburbs
      Update_Email_Addresses
      Update_Address_Contacts
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      SUBU:Suburb = L_RG:Suburb
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_RG:Suburb = SUBU:Suburb
        ADD:SUID = SUBU:SUID
      END
      ThisWindow.Reset(1)
          DO Get_Suburb
    OF ?L_RG:Suburb
      IF L_RG:Suburb OR ?L_RG:Suburb{PROP:Req}
        SUBU:Suburb = L_RG:Suburb
        IF Access:Add_Suburbs.TryFetch(SUBU:Key_Suburb)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_RG:Suburb = SUBU:Suburb
            ADD:SUID = SUBU:SUID
          ELSE
            CLEAR(ADD:SUID)
            SELECT(?L_RG:Suburb)
            CYCLE
          END
        ELSE
          ADD:SUID = SUBU:SUID
        END
      END
      ThisWindow.Reset()
          DO Get_Suburb
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF SELF.Request = ChangeRecord
             DO Advise_Address_Usage
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:9
    SELF.ChangeControl=?Change:9
    SELF.DeleteControl=?Delete:9
  END


BRW8.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (EMAI:RateLetter = 1)
    SELF.Q.EMAI:RateLetter_Icon = 2                        ! Set icon from icon list
  ELSE
    SELF.Q.EMAI:RateLetter_Icon = 1                        ! Set icon from icon list
  END


BRW10.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:11
    SELF.ChangeControl=?Change:11
    SELF.DeleteControl=?Delete:11
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! (p:Option)
!!! </summary>
Update_Email_Addresses PROCEDURE (p:Option)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
Clientname           STRING(100)                           !
History::EMAI:Record LIKE(EMAI:RECORD),THREAD
QuickWindow          WINDOW('Update the Email Addresses File'),AT(,,408,187),FONT('Tahoma',8),DOUBLE,GRAY,IMM,MDI, |
  HLP('Update_Email_Addresses'),SYSTEM
                       SHEET,AT(4,4,401,162),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Client Name:'),AT(9,26),USE(?Clientname:Prompt),TRN
                           BUTTON('...'),AT(71,26,12,10),USE(?CallLookup)
                           ENTRY(@s100),AT(88,26,303,10),USE(Clientname),REQ,TIP('Client name')
                           PROMPT('Name:'),AT(9,42),USE(?EMAI:Name:Prompt),TRN
                           ENTRY(@s35),AT(88,42,144,10),USE(EMAI:EmailName),MSG('Name of contact'),REQ,TIP('Name of contact')
                           PROMPT('Email Address:'),AT(9,55),USE(?EMAI:EmailAddress:Prompt),TRN
                           ENTRY(@s255),AT(88,55,303,10),USE(EMAI:EmailAddress),MSG('Email Address'),REQ,TIP('Email Address')
                           CHECK(' Rate Letter'),AT(87,71),USE(EMAI:RateLetter),MSG('This email address gets sent ' & |
  'rate letters'),TIP('This email address gets sent rate letters'),TRN
                           CHECK(' Default Address'),AT(87,85),USE(EMAI:DefaultAddress),MSG('Default Email Address'), |
  TIP('Default Email Address for the client'),TRN
                           CHECK(' Operations'),AT(87,113),USE(EMAI:Operations),MSG('This email address is for the' & |
  ' client Operations team'),TIP('This email address is for the client Operations team')
                           PROMPT('Operations Reference:'),AT(10,127),USE(?EMAI:OperationsReference:Prompt)
                           ENTRY(@s35),AT(88,127,126,10),USE(EMAI:OperationsReference),MSG('A reference for the Op' & |
  'erations team'),TIP('A reference for the Operations team')
                           CHECK(' Default On DI'),AT(87,148),USE(EMAI:DefaultOnDI),MSG('Always use this email add' & |
  'ress on new DIs'),TIP('Always use this email address on new DIs')
                         END
                       END
                       BUTTON('&OK'),AT(291,170,,14),USE(?OK),LEFT,ICON('waok.ico'),DEFAULT,FLAT
                       BUTTON('Cancel'),AT(347,170,,14),USE(?Cancel),LEFT,ICON('wacancel.ico'),FLAT
                       BUTTON('Help'),AT(4,170,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Email Address Record'
  OF InsertRecord
    ActionMessage = 'Email Address Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Email Address Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Email_Addresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Clientname:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:EmailAddresses)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(EMAI:Record,History::EMAI:Record)
  SELF.AddHistoryField(?EMAI:EmailName,3)
  SELF.AddHistoryField(?EMAI:EmailAddress,4)
  SELF.AddHistoryField(?EMAI:RateLetter,5)
  SELF.AddHistoryField(?EMAI:DefaultAddress,7)
  SELF.AddHistoryField(?EMAI:Operations,8)
  SELF.AddHistoryField(?EMAI:OperationsReference,9)
  SELF.AddHistoryField(?EMAI:DefaultOnDI,10)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:EmailAddresses
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Email_Addresses',QuickWindow)       ! Restore window settings from non-volatile store
      Clientname  = CLI:ClientName
      EMAI:CID    = CLI:CID
    IF p:Option = 2       ! this is passed direct from option to Update_Clients
       DISABLE(?CallLookup)
       DISABLE(?Clientname)
       DISABLE(?EMAI:DefaultAddress)
       DISABLE(?EMAI:RateLetter)
  
       EMAI:Operations  = 1
    .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  IF ?EMAI:Operations{Prop:Checked}
    ENABLE(?EMAI:OperationsReference)
  END
  IF NOT ?EMAI:Operations{PROP:Checked}
    DISABLE(?EMAI:OperationsReference)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Email_Addresses',QuickWindow)    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Select_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = Clientname
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        Clientname = CLI:ClientName
        EMAI:CID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?Clientname
      IF Clientname OR ?Clientname{PROP:Req}
        CLI:ClientName = Clientname
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Clientname = CLI:ClientName
            EMAI:CID = CLI:CID
          ELSE
            CLEAR(EMAI:CID)
            SELECT(?Clientname)
            CYCLE
          END
        ELSE
          EMAI:CID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?EMAI:Operations
      IF ?EMAI:Operations{PROP:Checked}
        ENABLE(?EMAI:OperationsReference)
      END
      IF NOT ?EMAI:Operations{PROP:Checked}
        DISABLE(?EMAI:OperationsReference)
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Address_Contacts PROCEDURE (p:AID)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
AddressName          STRING(35)                            !Name of this address
History::ADDC:Record LIKE(ADDC:RECORD),THREAD
QuickWindow          WINDOW('Update the Address Contacts File'),AT(,,216,122),FONT('Tahoma',8),RESIZE,GRAY,IMM, |
  MDI,HLP('UpdateAddressContacts'),SYSTEM
                       SHEET,AT(4,4,208,99),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           GROUP,AT(9,22,200,10),USE(?Group_Add)
                             PROMPT('Address:'),AT(9,22),USE(?AddressName:Prompt),TRN
                             BUTTON('...'),AT(61,22,12,10),USE(?CallLookup)
                             ENTRY(@s35),AT(77,22,132,10),USE(AddressName),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                           END
                           PROMPT('Contact Name:'),AT(9,46),USE(?ADDC:ContactName:Prompt),TRN
                           ENTRY(@s35),AT(77,46,132,10),USE(ADDC:ContactName),MSG('Contacts Name'),REQ,TIP('Contacts Name')
                           CHECK(' Primary'),AT(77,70),USE(ADDC:PrimaryContact),MSG('Is this contact the primary contact?'), |
  TIP('Is this contact the primary contact?<0DH,0AH>They will show in various places')
                         END
                       END
                       BUTTON('&OK'),AT(116,106,45,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT
                       BUTTON('Cancel'),AT(166,106,45,14),USE(?Cancel),LEFT,ICON('WAcancel.ICO'),FLAT
                       BUTTON('Help'),AT(4,106,45,14),USE(?Help),LEFT,ICON('WAhelp.ICO'),FLAT,HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record Contact Record'
  OF InsertRecord
    ActionMessage = 'Contact Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Contact Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Address_Contacts')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?AddressName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:AddressContacts)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ADDC:Record,History::ADDC:Record)
  SELF.AddHistoryField(?ADDC:ContactName,3)
  SELF.AddHistoryField(?ADDC:PrimaryContact,4)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:AddressContacts.SetOpenRelated()
  Relate:AddressContacts.Open                              ! File AddressContacts used by this procedure, so make sure it's RelationManager is open
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:AddressContacts
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Address_Contacts',QuickWindow)      ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      IF ADD:AID ~= 0
         AddressName  = ADD:AddressName
         ADDC:AID     = ADD:AID
      .
  
  
  
      IF p:AID > 0
         IF ADD:AID ~= p:AID
            ADD:AID   = p:AID
            IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
               !
         .  .
  
         AddressName  = ADD:AddressName
         ADDC:AID     = ADD:AID
  
         DISABLE(?Group_Add)
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressContacts.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Address_Contacts',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Select_Address
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      ADD:AddressName = AddressName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        AddressName = ADD:AddressName
        ADDC:AID = ADD:AID
      END
      ThisWindow.Reset(1)
    OF ?AddressName
      IF AddressName OR ?AddressName{PROP:Req}
        ADD:AddressName = AddressName
        IF Access:Addresses.TryFetch(ADD:Key_Name)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            AddressName = ADD:AddressName
            ADDC:AID = ADD:AID
          ELSE
            CLEAR(ADDC:AID)
            SELECT(?AddressName)
            CYCLE
          END
        ELSE
          ADDC:AID = ADD:AID
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF CLIP(AddressName) ~= ''
             SELECT(?ADDC:ContactName)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Accountants PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Address          STRING(35)                            !Name of this address
BRW2::View:Browse    VIEW(Clients)
                       PROJECT(CLI:CID)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:BID)
                       PROJECT(CLI:GenerateInvoice)
                       PROJECT(CLI:MinimiumCharge)
                       PROJECT(CLI:Rate)
                       PROJECT(CLI:DocumentCharge)
                       PROJECT(CLI:ACID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
CLI:CID                LIKE(CLI:CID)                  !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:BID                LIKE(CLI:BID)                  !List box control field - type derived from field
CLI:GenerateInvoice    LIKE(CLI:GenerateInvoice)      !List box control field - type derived from field
CLI:MinimiumCharge     LIKE(CLI:MinimiumCharge)       !List box control field - type derived from field
CLI:Rate               LIKE(CLI:Rate)                 !List box control field - type derived from field
CLI:DocumentCharge     LIKE(CLI:DocumentCharge)       !List box control field - type derived from field
CLI:ACID               LIKE(CLI:ACID)                 !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::ACCO:Record LIKE(ACCO:RECORD),THREAD
QuickWindow          WINDOW('Update the Accountants File'),AT(,,228,138),FONT('Tahoma',8),RESIZE,GRAY,IMM,MDI,HLP('UpdateAccountants'), |
  SYSTEM
                       SHEET,AT(4,4,220,112),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Accountant Name:'),AT(9,22),USE(?ACCO:AccountantName:Prompt),TRN
                           ENTRY(@s35),AT(75,22,144,10),USE(ACCO:AccountantName),MSG('Accountants Name'),REQ,TIP('Accountants Name')
                           PROMPT('Address:'),AT(9,48),USE(?LOC:Address:Prompt),TRN
                           BUTTON('...'),AT(59,48,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(75,48,144,10),USE(LOC:Address),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                         END
                         TAB('Clients'),USE(?Tab:2)
                           LIST,AT(9,20,212,93),USE(?Browse:2),HVSCROLL,FORMAT('30R(2)|M~CID~C(0)@n_10@80L(2)|M~Cl' & |
  'ient Name~@s35@20R(2)|M~BID~C(0)@n_10@60R(2)|M~Generate Invoice~C(0)@n3@58R(2)|M~Min' & |
  'imium Charge~C(0)@n-11.2@44R(2)|M~Rate~C(0)@n-11.2@64R(2)|M~Document Charge~C(0)@n-1' & |
  '1.2@30R(2)|M~ACID~C(0)@n_10@'),FROM(Queue:Browse:2),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('OK'),AT(114,120,,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT
                       BUTTON('Cancel'),AT(170,120,,14),USE(?Cancel),LEFT,ICON('WAcancel.ICO'),FLAT
                       BUTTON('Help'),AT(4,120,,14),USE(?Help),LEFT,ICON('WAhelp.ICO'),FLAT,HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Accountants')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ACCO:AccountantName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Accountants)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ACCO:Record,History::ACCO:Record)
  SELF.AddHistoryField(?ACCO:AccountantName,2)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Accountants.SetOpenRelated()
  Relate:Accountants.Open                                  ! File Accountants used by this procedure, so make sure it's RelationManager is open
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Accountants
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:Clients,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon CLI:ACID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,CLI:FKey_ACID)   ! Add the sort order for CLI:FKey_ACID for sort order 1
  BRW2.AddRange(CLI:ACID,Relate:Clients,Relate:Accountants) ! Add file relationship range limit for sort order 1
  BRW2.AddField(CLI:CID,BRW2.Q.CLI:CID)                    ! Field CLI:CID is a hot field or requires assignment from browse
  BRW2.AddField(CLI:ClientName,BRW2.Q.CLI:ClientName)      ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW2.AddField(CLI:BID,BRW2.Q.CLI:BID)                    ! Field CLI:BID is a hot field or requires assignment from browse
  BRW2.AddField(CLI:GenerateInvoice,BRW2.Q.CLI:GenerateInvoice) ! Field CLI:GenerateInvoice is a hot field or requires assignment from browse
  BRW2.AddField(CLI:MinimiumCharge,BRW2.Q.CLI:MinimiumCharge) ! Field CLI:MinimiumCharge is a hot field or requires assignment from browse
  BRW2.AddField(CLI:Rate,BRW2.Q.CLI:Rate)                  ! Field CLI:Rate is a hot field or requires assignment from browse
  BRW2.AddField(CLI:DocumentCharge,BRW2.Q.CLI:DocumentCharge) ! Field CLI:DocumentCharge is a hot field or requires assignment from browse
  BRW2.AddField(CLI:ACID,BRW2.Q.CLI:ACID)                  ! Field CLI:ACID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Accountants',QuickWindow)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      ADD:AID             = ACCO:AID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
         LOC:Address      = ADD:AddressName
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Accountants.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Accountants',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Addresses(ACCO:AccountantName)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      ADD:AddressName = LOC:Address
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:Address = ADD:AddressName
        ACCO:AID = ADD:AID
      END
      ThisWindow.Reset(1)
    OF ?LOC:Address
      IF LOC:Address OR ?LOC:Address{PROP:Req}
        ADD:AddressName = LOC:Address
        IF Access:Addresses.TryFetch(ADD:Key_Name)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:Address = ADD:AddressName
            ACCO:AID = ADD:AID
          ELSE
            CLEAR(ACCO:AID)
            SELECT(?LOC:Address)
            CYCLE
          END
        ELSE
          ACCO:AID = ADD:AID
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! ** empty **
!!! </summary>
Main_M3              PROCEDURE                             ! Declare Procedure

  CODE
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Address_Contacts PROCEDURE (p:AID)

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(AddressContacts)
                       PROJECT(ADDC:ContactName)
                       PROJECT(ADDC:PrimaryContact)
                       PROJECT(ADDC:ACID)
                       PROJECT(ADDC:AID)
                       JOIN(ADD:PKey_AID,ADDC:AID)
                         PROJECT(ADD:PhoneNo)
                         PROJECT(ADD:PhoneNo2)
                         PROJECT(ADD:Fax)
                         PROJECT(ADD:AddressName)
                         PROJECT(ADD:AID)
                         JOIN(EMAI:FKey_AID,ADD:AID)
                           PROJECT(EMAI:EmailName)
                           PROJECT(EMAI:EmailAddress)
                         END
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ADDC:ContactName       LIKE(ADDC:ContactName)         !List box control field - type derived from field
ADDC:PrimaryContact    LIKE(ADDC:PrimaryContact)      !List box control field - type derived from field
ADDC:PrimaryContact_Icon LONG                         !Entry's icon ID
ADD:PhoneNo            LIKE(ADD:PhoneNo)              !List box control field - type derived from field
ADD:PhoneNo2           LIKE(ADD:PhoneNo2)             !List box control field - type derived from field
ADD:Fax                LIKE(ADD:Fax)                  !List box control field - type derived from field
EMAI:EmailName         LIKE(EMAI:EmailName)           !List box control field - type derived from field
EMAI:EmailAddress      LIKE(EMAI:EmailAddress)        !List box control field - type derived from field
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
ADDC:ACID              LIKE(ADDC:ACID)                !Primary key field - type derived from field
ADDC:AID               LIKE(ADDC:AID)                 !Browse key field - type derived from field
ADD:AID                LIKE(ADD:AID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Address Contacts File'),AT(,,327,273),FONT('Tahoma',8),RESIZE,GRAY,IMM, |
  MAX,MDI,HLP('BrowseAddressContacts'),SYSTEM
                       LIST,AT(8,23,309,207),USE(?Browse:1),HVSCROLL,FORMAT('100L(2)|M~Contact Name~@s35@28L(2' & |
  ')|MI~Primary~L(1)@p p@42L(2)|M~Phone No.~@s20@42L(2)|M~Phone No. 2~@s20@42L(2)|M~Fax' & |
  '~@s20@42L(2)|M~Email Name~@s35@120L(2)|M~Email Address~@s255@140L(2)|M~Address Name~@s35@'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing Records')
                       BUTTON('&Select'),AT(7,234,45,14),USE(?Select:2),LEFT,ICON('WAselect.ico'),FLAT
                       BUTTON('&Insert'),AT(147,234,,14),USE(?Insert:3),LEFT,ICON('WAinsert.ICO'),FLAT
                       BUTTON('&Change'),AT(201,234,,14),USE(?Change:3),LEFT,ICON('WAChange.ICO'),DEFAULT,FLAT
                       BUTTON('&Delete'),AT(260,234,,14),USE(?Delete:3),LEFT,ICON('WADelete.ICO'),FLAT
                       SHEET,AT(4,4,319,249),USE(?CurrentTab)
                         TAB('By Address'),USE(?Tab:2)
                           BUTTON('Select Address'),AT(3,256,,14),USE(?SelectAddresses),LEFT,ICON('WAParent.ICO'),FLAT
                         END
                       END
                       BUTTON('Close'),AT(269,256,,14),USE(?Close),LEFT,ICON('waclose.ico'),FLAT
                       BUTTON('Help'),AT(97,256,45,14),USE(?Help),HIDE,STD(STD:Help)
                       STRING('**Client name goes here**'),AT(54,5,263),USE(?STRING_client)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Address_Contacts')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:AddressContacts.SetOpenRelated()
  Relate:AddressContacts.Open                              ! File AddressContacts used by this procedure, so make sure it's RelationManager is open
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:AddressContacts,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon ADDC:AID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ADDC:FKey_AID)   ! Add the sort order for ADDC:FKey_AID for sort order 1
  BRW1.AddRange(ADDC:AID,Relate:AddressContacts,Relate:Addresses) ! Add file relationship range limit for sort order 1
  BRW1.AppendOrder('-ADDC:PrimaryContact,+ADDC:ContactName,+ADDC:ACID') ! Append an additional sort order
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(ADDC:ContactName,BRW1.Q.ADDC:ContactName)  ! Field ADDC:ContactName is a hot field or requires assignment from browse
  BRW1.AddField(ADDC:PrimaryContact,BRW1.Q.ADDC:PrimaryContact) ! Field ADDC:PrimaryContact is a hot field or requires assignment from browse
  BRW1.AddField(ADD:PhoneNo,BRW1.Q.ADD:PhoneNo)            ! Field ADD:PhoneNo is a hot field or requires assignment from browse
  BRW1.AddField(ADD:PhoneNo2,BRW1.Q.ADD:PhoneNo2)          ! Field ADD:PhoneNo2 is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Fax,BRW1.Q.ADD:Fax)                    ! Field ADD:Fax is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:EmailName,BRW1.Q.EMAI:EmailName)      ! Field EMAI:EmailName is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:EmailAddress,BRW1.Q.EMAI:EmailAddress) ! Field EMAI:EmailAddress is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AddressName,BRW1.Q.ADD:AddressName)    ! Field ADD:AddressName is a hot field or requires assignment from browse
  BRW1.AddField(ADDC:ACID,BRW1.Q.ADDC:ACID)                ! Field ADDC:ACID is a hot field or requires assignment from browse
  BRW1.AddField(ADDC:AID,BRW1.Q.ADDC:AID)                  ! Field ADDC:AID is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AID,BRW1.Q.ADD:AID)                    ! Field ADD:AID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Address_Contacts',QuickWindow)      ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
      ?String_Client{PROP:Text} = ''
      IF DEFORMAT(p:AID) > 0
        HIDE(?SelectAddresses)
        
        ?String_Client{PROP:Text} = 'Addresses for client: ' & CLIP(CLI:ClientName)
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressContacts.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Address_Contacts',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Address_Contacts(p:AID)
    ReturnValue = GlobalResponse
  END
    IF DEFORMAT(p:AID) > 0  
       ADD:AID = p:AID
    .
  
  !LOC:Reset_Field += 1
  !SELF.Reset(1)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectAddresses
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Address()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (ADDC:PrimaryContact = 1)
    SELF.Q.ADDC:PrimaryContact_Icon = 2                    ! Set icon from icon list
  ELSE
    SELF.Q.ADDC:PrimaryContact_Icon = 1                    ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Audit PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::AUD:Record  LIKE(AUD:RECORD),THREAD
QuickWindow          WINDOW('Form Audit'),AT(,,260,180),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM,MDI, |
  HLP('UpdateAudit'),SYSTEM
                       SHEET,AT(4,4,252,154),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('AUID:'),AT(194,6),USE(?AUD:AUID:Prompt),TRN
                           STRING(@n_10),AT(213,6,,10),USE(AUD:AUID),RIGHT(1),TRN
                           PROMPT('Date && Time:'),AT(9,22),USE(?AUD:AuditDate:Prompt),TRN
                           ENTRY(@d17),AT(57,22,81,10),USE(AUD:AuditDate),MSG('Audit entry date'),TIP('Audit entry date')
                           ENTRY(@t7),AT(166,22,84,10),USE(AUD:AuditTime),MSG('Audit entry time'),TIP('Audit entry time')
                           PROMPT('Severity:'),AT(9,36),USE(?AUD:Severity:Prompt),TRN
                           ENTRY(@n3),AT(57,36,40,10),USE(AUD:Severity),MSG('Severity level'),TIP('Severity level')
                           PROMPT('App Section:'),AT(9,52),USE(?AUD:AppSection:Prompt),TRN
                           TEXT,AT(57,52,194,30),USE(AUD:AppSection),VSCROLL,MSG('Program section'),TIP('Program section')
                           PROMPT('Data 1:'),AT(9,86),USE(?AUD:Data1:Prompt),TRN
                           TEXT,AT(57,86,194,30),USE(AUD:Data1),VSCROLL
                           PROMPT('Data 2:'),AT(9,120),USE(?AUD:Data2:Prompt),TRN
                           TEXT,AT(57,120,194,30),USE(AUD:Data2),VSCROLL
                           PROMPT('Access Level:'),AT(161,36),USE(?AUD:AccessLevel:Prompt),TRN
                           ENTRY(@n3),AT(210,36,40,10),USE(AUD:AccessLevel),MSG('Access Level required to see this entry'), |
  TIP('Access Level required to see this entry')
                         END
                       END
                       BUTTON('&OK'),AT(156,162,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(210,162,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,162,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Audit')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?AUD:AUID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Audit)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(AUD:Record,History::AUD:Record)
  SELF.AddHistoryField(?AUD:AUID,1)
  SELF.AddHistoryField(?AUD:AuditDate,4)
  SELF.AddHistoryField(?AUD:AuditTime,5)
  SELF.AddHistoryField(?AUD:Severity,6)
  SELF.AddHistoryField(?AUD:AppSection,7)
  SELF.AddHistoryField(?AUD:Data1,8)
  SELF.AddHistoryField(?AUD:Data2,9)
  SELF.AddHistoryField(?AUD:AccessLevel,10)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Audit.Open                                        ! File Audit used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Audit
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?AUD:AuditDate{PROP:ReadOnly} = True
    ?AUD:AuditTime{PROP:ReadOnly} = True
    ?AUD:Severity{PROP:ReadOnly} = True
    ?AUD:AccessLevel{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Audit',QuickWindow)                 ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Audit.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Audit',QuickWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Audit PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:User_Access      LONG                                  !
LOC:Filter           GROUP,PRE(L_F)                        !
Severity             BYTE(10)                              !
                     END                                   !
BRW1::View:Browse    VIEW(Audit)
                       PROJECT(AUD:AUID)
                       PROJECT(AUD:AuditDate)
                       PROJECT(AUD:AuditTime)
                       PROJECT(AUD:Severity)
                       PROJECT(AUD:AppSection)
                       PROJECT(AUD:Data1)
                       PROJECT(AUD:Data2)
                       PROJECT(AUD:AccessLevel)
                       PROJECT(AUD:AuditDateTime)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
AUD:AUID               LIKE(AUD:AUID)                 !List box control field - type derived from field
AUD:AuditDate          LIKE(AUD:AuditDate)            !List box control field - type derived from field
AUD:AuditTime          LIKE(AUD:AuditTime)            !List box control field - type derived from field
AUD:Severity           LIKE(AUD:Severity)             !List box control field - type derived from field
AUD:AppSection         LIKE(AUD:AppSection)           !List box control field - type derived from field
AUD:Data1              LIKE(AUD:Data1)                !List box control field - type derived from field
AUD:Data2              LIKE(AUD:Data2)                !List box control field - type derived from field
AUD:AccessLevel        LIKE(AUD:AccessLevel)          !List box control field - type derived from field
AUD:AuditDateTime      LIKE(AUD:AuditDateTime)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Audit File'),AT(,,358,214),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('Browse_Audit'),SYSTEM
                       GROUP,AT(4,178,291,34),USE(?Group1)
                         PROMPT('App Section:'),AT(4,178),USE(?AUD:AppSection:Prompt)
                         TEXT,AT(50,178,245,10),USE(AUD:AppSection),VSCROLL,BOXED,MSG('Program section'),TIP('Program section')
                         PROMPT('Data 1:'),AT(4,191),USE(?AUD:Data1:Prompt)
                         TEXT,AT(50,191,245,10),USE(AUD:Data1),VSCROLL,BOXED
                         PROMPT('Data 2:'),AT(4,202),USE(?AUD:Data2:Prompt)
                         TEXT,AT(50,202,245,10),USE(AUD:Data2),VSCROLL,BOXED
                       END
                       LIST,AT(8,20,342,134),USE(?Browse:1),HVSCROLL,FORMAT('38R(2)|M~AUID~C(0)@n_10@36R(2)|M~' & |
  'Date~C(0)@d5@35R(2)|M~Time~C(0)@t7@30R(2)|M~Severity~C(0)@n3@80L(2)|M~App. Section~@' & |
  's200@80L(2)|M~Data 1~@s255@80L(2)|M~Data 2~@s255@52R(2)|M~Access Level~C(0)@n3@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the Audit file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By AUID'),USE(?Tab:2)
                           GROUP,AT(9,162,67,10),USE(?Group2)
                             PROMPT('Severity:'),AT(9,162),USE(?L_F:Severity:Prompt)
                             SPIN(@n3),AT(41,162,35,10),USE(L_F:Severity),RIGHT(1)
                           END
                         END
                         TAB('&2) By Date && Time'),USE(?Tab:3)
                         END
                         TAB('&3) By Application Section'),USE(?Tab:4)
                         END
                       END
                       BUTTON('&Close'),AT(300,180,,30),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,4,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Audit')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?AUD:AppSection:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_F:Severity',L_F:Severity)                        ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Audit.Open                                        ! File Audit used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Audit,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,AUD:SKey_DateTime)                    ! Add the sort order for AUD:SKey_DateTime for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,AUD:AuditDateTime,1,BRW1)      ! Initialize the browse locator using  using key: AUD:SKey_DateTime , AUD:AuditDateTime
  BRW1.AppendOrder('+AUD:AUID')                            ! Append an additional sort order
  BRW1.SetFilter('(AUD:Severity <<= L_F:Severity)')        ! Apply filter expression to browse
  BRW1.AddResetField(L_F:Severity)                         ! Apply the reset field
  BRW1.AddSortOrder(,AUD:SKey_Section)                     ! Add the sort order for AUD:SKey_Section for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,AUD:AppSection,1,BRW1)         ! Initialize the browse locator using  using key: AUD:SKey_Section , AUD:AppSection
  BRW1.AppendOrder('+AUD:AuditDate,+AUD:AUID')             ! Append an additional sort order
  BRW1.SetFilter('(AUD:Severity <<= L_F:Severity)')        ! Apply filter expression to browse
  BRW1.AddResetField(L_F:Severity)                         ! Apply the reset field
  BRW1.AddSortOrder(,AUD:PKey_AUID)                        ! Add the sort order for AUD:PKey_AUID for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(,AUD:AUID,1,BRW1)               ! Initialize the browse locator using  using key: AUD:PKey_AUID , AUD:AUID
  BRW1.SetFilter('(AUD:Severity <<= L_F:Severity)')        ! Apply filter expression to browse
  BRW1.AddResetField(L_F:Severity)                         ! Apply the reset field
  BRW1.AddField(AUD:AUID,BRW1.Q.AUD:AUID)                  ! Field AUD:AUID is a hot field or requires assignment from browse
  BRW1.AddField(AUD:AuditDate,BRW1.Q.AUD:AuditDate)        ! Field AUD:AuditDate is a hot field or requires assignment from browse
  BRW1.AddField(AUD:AuditTime,BRW1.Q.AUD:AuditTime)        ! Field AUD:AuditTime is a hot field or requires assignment from browse
  BRW1.AddField(AUD:Severity,BRW1.Q.AUD:Severity)          ! Field AUD:Severity is a hot field or requires assignment from browse
  BRW1.AddField(AUD:AppSection,BRW1.Q.AUD:AppSection)      ! Field AUD:AppSection is a hot field or requires assignment from browse
  BRW1.AddField(AUD:Data1,BRW1.Q.AUD:Data1)                ! Field AUD:Data1 is a hot field or requires assignment from browse
  BRW1.AddField(AUD:Data2,BRW1.Q.AUD:Data2)                ! Field AUD:Data2 is a hot field or requires assignment from browse
  BRW1.AddField(AUD:AccessLevel,BRW1.Q.AUD:AccessLevel)    ! Field AUD:AccessLevel is a hot field or requires assignment from browse
  BRW1.AddField(AUD:AuditDateTime,BRW1.Q.AUD:AuditDateTime) ! Field AUD:AuditDateTime is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
      ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section)
      ! Returns
      !   1   - Disable
      !   2   - Hide
      !   100 - Allow
      !   101 - Disallow
  
      LOC:User_Access     = Get_User_Access(GLO:UID, 'Audit', 'Browse_Audit', 'Update')
  
      db.debugout('LOC:User_Access: ' & LOC:User_Access)
  
      IF LOC:User_Access = 100 OR LOC:User_Access = 0
      ELSE
         DISABLE(?Change:4)
         DISABLE(?Delete:4)
         DISABLE(?Insert:4)
      .
  INIMgr.Fetch('Browse_Audit',QuickWindow)                 ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Audit.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Audit',QuickWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Audit
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
      IF LOC:User_Access = 100
      ELSE
         SELF.InsertControl=0
         SELF.ChangeControl=0
         SELF.DeleteControl=0
      .
  
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group1, Resize:FixLeft+Resize:FixBottom, Resize:ConstantRight+Resize:LockHeight) ! Override strategy for ?Group1
  SELF.SetStrategy(?Group2, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group2

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Branches PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Setup_Group      GROUP,PRE(L_SG)                       !
Branch               STRING(35)                            !Branch Name
Address              STRING(35)                            !Name of this address
DefaultRatesClient   STRING(100)                           !
DefaultRatesTransporter STRING(35)                         !Transporters Name
GeneralFuelSurchargeClient STRING(100)                     !Client
DefaultTollRateClient STRING(100)                          !
                     END                                   !
BRW2::View:Browse    VIEW(_Invoice)
                       PROJECT(INV:IID)
                       PROJECT(INV:BID)
                       PROJECT(INV:CID)
                       PROJECT(INV:ShipperName)
                       PROJECT(INV:ShipperLine1)
                       PROJECT(INV:ShipperLine2)
                       PROJECT(INV:ShipperSuburb)
                       PROJECT(INV:ShipperPostalCode)
                       PROJECT(INV:ConsigneeName)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
INV:IID                LIKE(INV:IID)                  !List box control field - type derived from field
INV:BID                LIKE(INV:BID)                  !List box control field - type derived from field
INV:CID                LIKE(INV:CID)                  !List box control field - type derived from field
INV:ShipperName        LIKE(INV:ShipperName)          !List box control field - type derived from field
INV:ShipperLine1       LIKE(INV:ShipperLine1)         !List box control field - type derived from field
INV:ShipperLine2       LIKE(INV:ShipperLine2)         !List box control field - type derived from field
INV:ShipperSuburb      LIKE(INV:ShipperSuburb)        !List box control field - type derived from field
INV:ShipperPostalCode  LIKE(INV:ShipperPostalCode)    !List box control field - type derived from field
INV:ConsigneeName      LIKE(INV:ConsigneeName)        !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(TripSheets)
                       PROJECT(TRI:BID)
                       PROJECT(TRI:DepartDate)
                       PROJECT(TRI:DepartTime)
                       PROJECT(TRI:ReturnedDate)
                       PROJECT(TRI:ReturnedTime)
                       PROJECT(TRI:Notes)
                       PROJECT(TRI:TRID)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
TRI:BID                LIKE(TRI:BID)                  !List box control field - type derived from field
TRI:DepartDate         LIKE(TRI:DepartDate)           !List box control field - type derived from field
TRI:DepartTime         LIKE(TRI:DepartTime)           !List box control field - type derived from field
TRI:ReturnedDate       LIKE(TRI:ReturnedDate)         !List box control field - type derived from field
TRI:ReturnedTime       LIKE(TRI:ReturnedTime)         !List box control field - type derived from field
TRI:Notes              LIKE(TRI:Notes)                !List box control field - type derived from field
TRI:TRID               LIKE(TRI:TRID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(Journeys)
                       PROJECT(JOU:Journey)
                       PROJECT(JOU:BID)
                       PROJECT(JOU:JID)
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?Browse:6
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
JOU:BID                LIKE(JOU:BID)                  !List box control field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(Transporter)
                       PROJECT(TRA:TransporterName)
                       PROJECT(TRA:OpsManager)
                       PROJECT(TRA:VATNo)
                       PROJECT(TRA:BID)
                       PROJECT(TRA:ACID)
                       PROJECT(TRA:TID)
                     END
Queue:Browse:8       QUEUE                            !Queue declaration for browse/combo box using ?Browse:8
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
TRA:OpsManager         LIKE(TRA:OpsManager)           !List box control field - type derived from field
TRA:VATNo              LIKE(TRA:VATNo)                !List box control field - type derived from field
TRA:BID                LIKE(TRA:BID)                  !List box control field - type derived from field
TRA:ACID               LIKE(TRA:ACID)                 !List box control field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(Manifest)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:TID)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:Rate)
                       PROJECT(MAN:VATRate)
                       PROJECT(MAN:State)
                       PROJECT(MAN:DepartDate)
                       PROJECT(MAN:DepartTime)
                       PROJECT(MAN:ETADate)
                       PROJECT(MAN:MID)
                     END
Queue:Browse:10      QUEUE                            !Queue declaration for browse/combo box using ?Browse:10
MAN:BID                LIKE(MAN:BID)                  !List box control field - type derived from field
MAN:TID                LIKE(MAN:TID)                  !List box control field - type derived from field
MAN:Cost               LIKE(MAN:Cost)                 !List box control field - type derived from field
MAN:Rate               LIKE(MAN:Rate)                 !List box control field - type derived from field
MAN:VATRate            LIKE(MAN:VATRate)              !List box control field - type derived from field
MAN:State              LIKE(MAN:State)                !List box control field - type derived from field
MAN:DepartDate         LIKE(MAN:DepartDate)           !List box control field - type derived from field
MAN:DepartTime         LIKE(MAN:DepartTime)           !List box control field - type derived from field
MAN:ETADate            LIKE(MAN:ETADate)              !List box control field - type derived from field
MAN:MID                LIKE(MAN:MID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(Clients)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:BID)
                       PROJECT(CLI:GenerateInvoice)
                       PROJECT(CLI:MinimiumCharge)
                       PROJECT(CLI:Rate)
                       PROJECT(CLI:DocumentCharge)
                       PROJECT(CLI:ACID)
                       PROJECT(CLI:CID)
                     END
Queue:Browse:12      QUEUE                            !Queue declaration for browse/combo box using ?Browse:12
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:BID                LIKE(CLI:BID)                  !List box control field - type derived from field
CLI:GenerateInvoice    LIKE(CLI:GenerateInvoice)      !List box control field - type derived from field
CLI:MinimiumCharge     LIKE(CLI:MinimiumCharge)       !List box control field - type derived from field
CLI:Rate               LIKE(CLI:Rate)                 !List box control field - type derived from field
CLI:DocumentCharge     LIKE(CLI:DocumentCharge)       !List box control field - type derived from field
CLI:ACID               LIKE(CLI:ACID)                 !List box control field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:BID)
                       PROJECT(DEL:CID)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:Rate)
                       PROJECT(DEL:DocumentCharge)
                       PROJECT(DEL:FuelSurcharge)
                       PROJECT(DEL:Charge)
                       PROJECT(DEL:DID)
                     END
Queue:Browse:14      QUEUE                            !Queue declaration for browse/combo box using ?Browse:14
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:BID                LIKE(DEL:BID)                  !List box control field - type derived from field
DEL:CID                LIKE(DEL:CID)                  !List box control field - type derived from field
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
DEL:Rate               LIKE(DEL:Rate)                 !List box control field - type derived from field
DEL:DocumentCharge     LIKE(DEL:DocumentCharge)       !List box control field - type derived from field
DEL:FuelSurcharge      LIKE(DEL:FuelSurcharge)        !List box control field - type derived from field
DEL:Charge             LIKE(DEL:Charge)               !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB3::View:FileDrop  VIEW(Addresses)
                       PROJECT(ADD:AddressName)
                       PROJECT(ADD:AID)
                     END
FDB5::View:FileDrop  VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?ADD:AddressName
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
ADD:AID                LIKE(ADD:AID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !Queue declaration for browse/combo box using ?FLO:Floor
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::BRA:Record  LIKE(BRA:RECORD),THREAD
QuickWindow          WINDOW('Update the Branches File'),AT(,,350,262),FONT('Tahoma',8),DOUBLE,GRAY,IMM,MDI,HLP('Update_Branches'), |
  SYSTEM
                       SHEET,AT(4,4,344,238),USE(?CurrentTab),JOIN
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Branch Name:'),AT(9,25),USE(?BRA:BranchName:Prompt),TRN
                           ENTRY(@s35),AT(121,25,215,10),USE(BRA:BranchName),MSG('Branch Name'),REQ,TIP('Branch Name')
                           PROMPT('Address Name:'),AT(9,49),USE(?BRA:BranchName:Prompt:2),TRN
                           LIST,AT(121,49,215,10),USE(ADD:AddressName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Name~@s35@'), |
  FROM(Queue:FileDrop),MSG('Name of this address')
                           PROMPT('Floor:'),AT(9,71),USE(?BRA:BranchName:Prompt:3),TRN
                           LIST,AT(121,71,144,10),USE(FLO:Floor),VSCROLL,DROP(15),FORMAT('140L(2)|M~Floor~@s35@'),FROM(Queue:FileDrop:1),MSG('Floor Name')
                           LINE,AT(9,97,331,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Default Rates Client:'),AT(9,111),USE(?DefaultRatesClient:Prompt),TRN
                           BUTTON('...'),AT(105,111,12,10),USE(?CallLookup_DefaultRateClient),FONT(,,,FONT:regular)
                           ENTRY(@s100),AT(121,111,215,10),USE(L_SG:DefaultRatesClient),REQ,TIP('Client name')
                           PROMPT('Default Rates Transporter:'),AT(9,134),USE(?DefaultRatesTransporter:Prompt),TRN
                           BUTTON('...'),AT(105,134,12,10),USE(?CallLookup:Transporter)
                           ENTRY(@s35),AT(121,134,215,10),USE(L_SG:DefaultRatesTransporter),MSG('Transporters Name'), |
  REQ,TIP('Transporters Name')
                           PROMPT('Default Fuel Surcharge Client:'),AT(9,158),USE(?GeneralFuelSurchargeClient:Prompt), |
  TRN
                           BUTTON('...'),AT(105,158,12,10),USE(?CallLookup:Fuel_Client)
                           ENTRY(@s100),AT(121,158,215,10),USE(L_SG:GeneralFuelSurchargeClient),MSG('Client ID'),TIP('This clien' & |
  'ts Fuel Surcharge will be used for all Clients that have none specified')
                           PROMPT('Default Toll Rate Client:'),AT(9,181),USE(?L_SG:DefaultTollRateClient:Prompt),TRN
                           ENTRY(@s100),AT(121,180,215,10),USE(L_SG:DefaultTollRateClient),REQ,TIP('Client name')
                           BUTTON('...'),AT(105,180,12,10),USE(?CallLookup_TollClient)
                         END
                         TAB('&2) Journeys'),USE(?Tab:4)
                           LIST,AT(9,26,331,209),USE(?Browse:6),HVSCROLL,FORMAT('120L(2)|M~Journey~@s70@20R(2)|M~B' & |
  'ID~C(0)@n_10@'),FROM(Queue:Browse:6),IMM,MSG('Browsing Records')
                         END
                         TAB('Invoices'),USE(?Tab:2),HIDE
                           LIST,AT(12,27,326,207),USE(?Browse:2),HVSCROLL,FORMAT('42R(2)|M~Invoice No.~C(0)@n_10@4' & |
  '0R(2)|M~BID~C(0)@n_10@40R(2)|M~CID~C(0)@n_10@80L(2)|M~Name~@s35@80L(2)|M~Shipper Lin' & |
  'e 1~@s35@80L(2)|M~Shipper Line 2~@s35@80L(2)|M~Shipper Suburb~@s50@80L(2)|M~Shipper ' & |
  'Postal Code~@s10@80L(2)|M~Consignee Name~@s35@'),FROM(Queue:Browse:2),IMM,MSG('Browsing Records')
                         END
                         TAB('TripSheets'),USE(?Tab:3),HIDE
                           LIST,AT(7,25,336,212),USE(?Browse:4),HVSCROLL,FORMAT('40R(2)|M~BID~C(0)@n_10@48R(2)|M~D' & |
  'epart Date~C(0)@d6@46R(2)|M~Depart Time~C(0)@t7@52R(2)|M~Returned Date~C(0)@d6@52R(2' & |
  ')|M~Returned Time~C(0)@t7@80L(2)|M~Notes~@s255@'),FROM(Queue:Browse:4),IMM,MSG('Browsing Records')
                         END
                         TAB('Transporter'),USE(?Tab:5)
                           LIST,AT(7,25,336,212),USE(?Browse:8),HVSCROLL,FORMAT('80L(2)|M~Transporter Name~@s35@80' & |
  'L(2)|M~Ops Manager~@s35@80L(2)|M~VAT No.~@s20@30R(2)|M~BID~C(0)@n_10@30R(2)|M~ACID~C' & |
  '(0)@n_10@30R(2)|M~TID~C(0)@n_10@'),FROM(Queue:Browse:8),IMM,MSG('Browsing Records')
                         END
                         TAB('Manifest'),USE(?Tab:6),HIDE
                           LIST,AT(7,22,336,218),USE(?Browse:10),HVSCROLL,FORMAT('30R(2)|M~BID~C(0)@n_10@30R(2)|M~' & |
  'TID~C(0)@n_10@52R(1)|M~Cost~C(0)@n-14.2@38R(1)|M~Rate~C(0)@n-10.2@36R(1)|M~VAT Rate~' & |
  'C(0)@n-7.2@24R(2)|M~State~C(0)@n3@44R(2)|M~Depart Date~C(0)@d6@44R(2)|M~Depart Time~' & |
  'C(0)@t7@44R(2)|M~ETA Date~C(0)@d6@'),FROM(Queue:Browse:10),IMM,MSG('Browsing Records')
                         END
                         TAB('Clients'),USE(?Tab:7),HIDE
                           LIST,AT(7,22,336,217),USE(?Browse:12),HVSCROLL,FORMAT('80L(2)|M~Client Name~@s35@30R(2)' & |
  '|M~BID~C(0)@n_10@62R(2)|M~Generate Invoice~C(0)@n3@64R(1)|M~Minimium Charge~C(0)@n-1' & |
  '1.2@52R(1)|M~Rate~C(0)@n-11.2@64R(1)|M~Document Charge~C(0)@n-11.2@80R(2)|M~ACID~C(0' & |
  ')@n_10@30R(2)|M~CID~C(0)@n_10@'),FROM(Queue:Browse:12),IMM,MSG('Browsing Records')
                         END
                         TAB('Deliveries'),USE(?Tab:8),HIDE
                           LIST,AT(7,22,336,217),USE(?Browse:14),HVSCROLL,FORMAT('40R(2)|M~DI No.~C(0)@n_10@30R(2)' & |
  '|M~BID~C(0)@n_10@30R(2)|M~CID~C(0)@n_10@80L(2)|M~Client Reference~@s30@52D(24)|M~Rat' & |
  'e~C(0)@n-11.2@64D(12)|M~Document Charge~C(0)@n-11.2@60D(12)|M~Fuel Surcharge~C(0)@n-' & |
  '11.2@60D(24)|M~Charge~C(0)@n-13.2@'),FROM(Queue:Browse:14),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('OK'),AT(253,246,45,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT
                       BUTTON('Cancel'),AT(303,246,45,14),USE(?Cancel),LEFT,ICON('WAcancel.ICO'),FLAT
                       BUTTON('Help'),AT(4,246,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
                     END

BRW4::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW6                 CLASS(BrowseClass)                    ! Browse using ?Browse:6
Q                      &Queue:Browse:6                !Reference to browse queue
                     END

BRW6::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW8                 CLASS(BrowseClass)                    ! Browse using ?Browse:8
Q                      &Queue:Browse:8                !Reference to browse queue
                     END

BRW8::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW10                CLASS(BrowseClass)                    ! Browse using ?Browse:10
Q                      &Queue:Browse:10               !Reference to browse queue
                     END

BRW10::Sort0:StepClass StepRealClass                       ! Default Step Manager
BRW12                CLASS(BrowseClass)                    ! Browse using ?Browse:12
Q                      &Queue:Browse:12               !Reference to browse queue
                     END

BRW12::Sort0:StepClass StepRealClass                       ! Default Step Manager
BRW14                CLASS(BrowseClass)                    ! Browse using ?Browse:14
Q                      &Queue:Browse:14               !Reference to browse queue
                     END

BRW14::Sort0:StepClass StepRealClass                       ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB3                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB5                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Branch Record'
  OF InsertRecord
    ActionMessage = 'Branch Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Branch Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Branches')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?BRA:BranchName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Branches)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(BRA:Record,History::BRA:Record)
  SELF.AddHistoryField(?BRA:BranchName,2)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Access:Branches.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Branches
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:_Invoice,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:TripSheets,SELF) ! Initialize the browse manager
  BRW6.Init(?Browse:6,Queue:Browse:6.ViewPosition,BRW6::View:Browse,Queue:Browse:6,Relate:Journeys,SELF) ! Initialize the browse manager
  BRW8.Init(?Browse:8,Queue:Browse:8.ViewPosition,BRW8::View:Browse,Queue:Browse:8,Relate:Transporter,SELF) ! Initialize the browse manager
  BRW10.Init(?Browse:10,Queue:Browse:10.ViewPosition,BRW10::View:Browse,Queue:Browse:10,Relate:Manifest,SELF) ! Initialize the browse manager
  BRW12.Init(?Browse:12,Queue:Browse:12.ViewPosition,BRW12::View:Browse,Queue:Browse:12,Relate:Clients,SELF) ! Initialize the browse manager
  BRW14.Init(?Browse:14,Queue:Browse:14.ViewPosition,BRW14::View:Browse,Queue:Browse:14,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon INV:IID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,INV:PKey_IID)    ! Add the sort order for INV:PKey_IID for sort order 1
  BRW2.AddRange(INV:IID,Relate:_Invoice,Relate:Branches)   ! Add file relationship range limit for sort order 1
  BRW2.AddField(INV:IID,BRW2.Q.INV:IID)                    ! Field INV:IID is a hot field or requires assignment from browse
  BRW2.AddField(INV:BID,BRW2.Q.INV:BID)                    ! Field INV:BID is a hot field or requires assignment from browse
  BRW2.AddField(INV:CID,BRW2.Q.INV:CID)                    ! Field INV:CID is a hot field or requires assignment from browse
  BRW2.AddField(INV:ShipperName,BRW2.Q.INV:ShipperName)    ! Field INV:ShipperName is a hot field or requires assignment from browse
  BRW2.AddField(INV:ShipperLine1,BRW2.Q.INV:ShipperLine1)  ! Field INV:ShipperLine1 is a hot field or requires assignment from browse
  BRW2.AddField(INV:ShipperLine2,BRW2.Q.INV:ShipperLine2)  ! Field INV:ShipperLine2 is a hot field or requires assignment from browse
  BRW2.AddField(INV:ShipperSuburb,BRW2.Q.INV:ShipperSuburb) ! Field INV:ShipperSuburb is a hot field or requires assignment from browse
  BRW2.AddField(INV:ShipperPostalCode,BRW2.Q.INV:ShipperPostalCode) ! Field INV:ShipperPostalCode is a hot field or requires assignment from browse
  BRW2.AddField(INV:ConsigneeName,BRW2.Q.INV:ConsigneeName) ! Field INV:ConsigneeName is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon TRI:BID for sort order 1
  BRW4.AddSortOrder(BRW4::Sort0:StepClass,TRI:FKey_BID)    ! Add the sort order for TRI:FKey_BID for sort order 1
  BRW4.AddRange(TRI:BID,Relate:TripSheets,Relate:Branches) ! Add file relationship range limit for sort order 1
  BRW4.AddField(TRI:BID,BRW4.Q.TRI:BID)                    ! Field TRI:BID is a hot field or requires assignment from browse
  BRW4.AddField(TRI:DepartDate,BRW4.Q.TRI:DepartDate)      ! Field TRI:DepartDate is a hot field or requires assignment from browse
  BRW4.AddField(TRI:DepartTime,BRW4.Q.TRI:DepartTime)      ! Field TRI:DepartTime is a hot field or requires assignment from browse
  BRW4.AddField(TRI:ReturnedDate,BRW4.Q.TRI:ReturnedDate)  ! Field TRI:ReturnedDate is a hot field or requires assignment from browse
  BRW4.AddField(TRI:ReturnedTime,BRW4.Q.TRI:ReturnedTime)  ! Field TRI:ReturnedTime is a hot field or requires assignment from browse
  BRW4.AddField(TRI:Notes,BRW4.Q.TRI:Notes)                ! Field TRI:Notes is a hot field or requires assignment from browse
  BRW4.AddField(TRI:TRID,BRW4.Q.TRI:TRID)                  ! Field TRI:TRID is a hot field or requires assignment from browse
  BRW6.Q &= Queue:Browse:6
  BRW6::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon JOU:BID for sort order 1
  BRW6.AddSortOrder(BRW6::Sort0:StepClass,JOU:FKey_BID)    ! Add the sort order for JOU:FKey_BID for sort order 1
  BRW6.AddRange(JOU:BID,Relate:Journeys,Relate:Branches)   ! Add file relationship range limit for sort order 1
  BRW6.AddField(JOU:Journey,BRW6.Q.JOU:Journey)            ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW6.AddField(JOU:BID,BRW6.Q.JOU:BID)                    ! Field JOU:BID is a hot field or requires assignment from browse
  BRW6.AddField(JOU:JID,BRW6.Q.JOU:JID)                    ! Field JOU:JID is a hot field or requires assignment from browse
  BRW8.Q &= Queue:Browse:8
  BRW8::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon TRA:BID for sort order 1
  BRW8.AddSortOrder(BRW8::Sort0:StepClass,TRA:FKey_BID)    ! Add the sort order for TRA:FKey_BID for sort order 1
  BRW8.AddRange(TRA:BID,Relate:Transporter,Relate:Branches) ! Add file relationship range limit for sort order 1
  BRW8.AddField(TRA:TransporterName,BRW8.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW8.AddField(TRA:OpsManager,BRW8.Q.TRA:OpsManager)      ! Field TRA:OpsManager is a hot field or requires assignment from browse
  BRW8.AddField(TRA:VATNo,BRW8.Q.TRA:VATNo)                ! Field TRA:VATNo is a hot field or requires assignment from browse
  BRW8.AddField(TRA:BID,BRW8.Q.TRA:BID)                    ! Field TRA:BID is a hot field or requires assignment from browse
  BRW8.AddField(TRA:ACID,BRW8.Q.TRA:ACID)                  ! Field TRA:ACID is a hot field or requires assignment from browse
  BRW8.AddField(TRA:TID,BRW8.Q.TRA:TID)                    ! Field TRA:TID is a hot field or requires assignment from browse
  BRW10.Q &= Queue:Browse:10
  BRW10::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)      ! Moveable thumb based upon MAN:BID for sort order 1
  BRW10.AddSortOrder(BRW10::Sort0:StepClass,MAN:FKey_BID)  ! Add the sort order for MAN:FKey_BID for sort order 1
  BRW10.AddRange(MAN:BID,Relate:Manifest,Relate:Branches)  ! Add file relationship range limit for sort order 1
  BRW10.AddField(MAN:BID,BRW10.Q.MAN:BID)                  ! Field MAN:BID is a hot field or requires assignment from browse
  BRW10.AddField(MAN:TID,BRW10.Q.MAN:TID)                  ! Field MAN:TID is a hot field or requires assignment from browse
  BRW10.AddField(MAN:Cost,BRW10.Q.MAN:Cost)                ! Field MAN:Cost is a hot field or requires assignment from browse
  BRW10.AddField(MAN:Rate,BRW10.Q.MAN:Rate)                ! Field MAN:Rate is a hot field or requires assignment from browse
  BRW10.AddField(MAN:VATRate,BRW10.Q.MAN:VATRate)          ! Field MAN:VATRate is a hot field or requires assignment from browse
  BRW10.AddField(MAN:State,BRW10.Q.MAN:State)              ! Field MAN:State is a hot field or requires assignment from browse
  BRW10.AddField(MAN:DepartDate,BRW10.Q.MAN:DepartDate)    ! Field MAN:DepartDate is a hot field or requires assignment from browse
  BRW10.AddField(MAN:DepartTime,BRW10.Q.MAN:DepartTime)    ! Field MAN:DepartTime is a hot field or requires assignment from browse
  BRW10.AddField(MAN:ETADate,BRW10.Q.MAN:ETADate)          ! Field MAN:ETADate is a hot field or requires assignment from browse
  BRW10.AddField(MAN:MID,BRW10.Q.MAN:MID)                  ! Field MAN:MID is a hot field or requires assignment from browse
  BRW12.Q &= Queue:Browse:12
  BRW12::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)      ! Moveable thumb based upon CLI:BID for sort order 1
  BRW12.AddSortOrder(BRW12::Sort0:StepClass,CLI:FKey_BID)  ! Add the sort order for CLI:FKey_BID for sort order 1
  BRW12.AddRange(CLI:BID,Relate:Clients,Relate:Branches)   ! Add file relationship range limit for sort order 1
  BRW12.AddField(CLI:ClientName,BRW12.Q.CLI:ClientName)    ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW12.AddField(CLI:BID,BRW12.Q.CLI:BID)                  ! Field CLI:BID is a hot field or requires assignment from browse
  BRW12.AddField(CLI:GenerateInvoice,BRW12.Q.CLI:GenerateInvoice) ! Field CLI:GenerateInvoice is a hot field or requires assignment from browse
  BRW12.AddField(CLI:MinimiumCharge,BRW12.Q.CLI:MinimiumCharge) ! Field CLI:MinimiumCharge is a hot field or requires assignment from browse
  BRW12.AddField(CLI:Rate,BRW12.Q.CLI:Rate)                ! Field CLI:Rate is a hot field or requires assignment from browse
  BRW12.AddField(CLI:DocumentCharge,BRW12.Q.CLI:DocumentCharge) ! Field CLI:DocumentCharge is a hot field or requires assignment from browse
  BRW12.AddField(CLI:ACID,BRW12.Q.CLI:ACID)                ! Field CLI:ACID is a hot field or requires assignment from browse
  BRW12.AddField(CLI:CID,BRW12.Q.CLI:CID)                  ! Field CLI:CID is a hot field or requires assignment from browse
  BRW14.Q &= Queue:Browse:14
  BRW14::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)      ! Moveable thumb based upon DEL:BID for sort order 1
  BRW14.AddSortOrder(BRW14::Sort0:StepClass,DEL:FKey_BID)  ! Add the sort order for DEL:FKey_BID for sort order 1
  BRW14.AddRange(DEL:BID,Relate:Deliveries,Relate:Branches) ! Add file relationship range limit for sort order 1
  BRW14.AddField(DEL:DINo,BRW14.Q.DEL:DINo)                ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW14.AddField(DEL:BID,BRW14.Q.DEL:BID)                  ! Field DEL:BID is a hot field or requires assignment from browse
  BRW14.AddField(DEL:CID,BRW14.Q.DEL:CID)                  ! Field DEL:CID is a hot field or requires assignment from browse
  BRW14.AddField(DEL:ClientReference,BRW14.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW14.AddField(DEL:Rate,BRW14.Q.DEL:Rate)                ! Field DEL:Rate is a hot field or requires assignment from browse
  BRW14.AddField(DEL:DocumentCharge,BRW14.Q.DEL:DocumentCharge) ! Field DEL:DocumentCharge is a hot field or requires assignment from browse
  BRW14.AddField(DEL:FuelSurcharge,BRW14.Q.DEL:FuelSurcharge) ! Field DEL:FuelSurcharge is a hot field or requires assignment from browse
  BRW14.AddField(DEL:Charge,BRW14.Q.DEL:Charge)            ! Field DEL:Charge is a hot field or requires assignment from browse
  BRW14.AddField(DEL:DID,BRW14.Q.DEL:DID)                  ! Field DEL:DID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Branches',QuickWindow)              ! Restore window settings from non-volatile store
      CLI:CID                     = BRA:GeneralRatesClientID
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         L_SG:DefaultRatesClient  = CLI:ClientName
      .
  
      TRA:TID                     = BRA:GeneralRatesTransporterID
      IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
         L_SG:DefaultRatesTransporter  = TRA:TransporterName
      .
  
      CLI:CID                     = BRA:GeneralFuelSurchargeClientID
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         L_SG:GeneralFuelSurchargeClient  = CLI:ClientName
      .
  
      CLI:CID                     = BRA:GeneralTollChargeClientID
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         L_SG:DefaultTollRateClient  = CLI:ClientName
      .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  FDB3.Init(?ADD:AddressName,Queue:FileDrop.ViewPosition,FDB3::View:FileDrop,Queue:FileDrop,Relate:Addresses,ThisWindow)
  FDB3.Q &= Queue:FileDrop
  FDB3.AddSortOrder(ADD:Key_Name)
  FDB3.AddField(ADD:AddressName,FDB3.Q.ADD:AddressName) !List box control field - type derived from field
  FDB3.AddField(ADD:AID,FDB3.Q.ADD:AID) !Primary key field - type derived from field
  FDB3.AddUpdateField(ADD:AID,BRA:AID)
  ThisWindow.AddItem(FDB3.WindowComponent)
  FDB3.DefaultFill = 0
  FDB5.Init(?FLO:Floor,Queue:FileDrop:1.ViewPosition,FDB5::View:FileDrop,Queue:FileDrop:1,Relate:Floors,ThisWindow)
  FDB5.Q &= Queue:FileDrop:1
  FDB5.AddSortOrder(FLO:Key_Floor)
  FDB5.AddField(FLO:Floor,FDB5.Q.FLO:Floor) !List box control field - type derived from field
  FDB5.AddField(FLO:FID,FDB5.Q.FLO:FID) !Primary key field - type derived from field
  FDB5.AddUpdateField(FLO:FID,BRA:FID)
  ThisWindow.AddItem(FDB5.WindowComponent)
  FDB5.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Branches',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_Clients
      Select_Transporter
      Select_Clients
      Select_Clients
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup_DefaultRateClient
      ThisWindow.Update()
      CLI:ClientName = L_SG:DefaultRatesClient
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SG:DefaultRatesClient = CLI:ClientName
        BRA:GeneralRatesClientID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?L_SG:DefaultRatesClient
      IF L_SG:DefaultRatesClient OR ?L_SG:DefaultRatesClient{PROP:Req}
        CLI:ClientName = L_SG:DefaultRatesClient
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SG:DefaultRatesClient = CLI:ClientName
            BRA:GeneralRatesClientID = CLI:CID
          ELSE
            CLEAR(BRA:GeneralRatesClientID)
            SELECT(?L_SG:DefaultRatesClient)
            CYCLE
          END
        ELSE
          BRA:GeneralRatesClientID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:Transporter
      ThisWindow.Update()
      TRA:TransporterName = L_SG:DefaultRatesTransporter
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_SG:DefaultRatesTransporter = TRA:TransporterName
        BRA:GeneralRatesTransporterID = TRA:TID
      END
      ThisWindow.Reset(1)
    OF ?L_SG:DefaultRatesTransporter
      IF L_SG:DefaultRatesTransporter OR ?L_SG:DefaultRatesTransporter{PROP:Req}
        TRA:TransporterName = L_SG:DefaultRatesTransporter
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            L_SG:DefaultRatesTransporter = TRA:TransporterName
            BRA:GeneralRatesTransporterID = TRA:TID
          ELSE
            CLEAR(BRA:GeneralRatesTransporterID)
            SELECT(?L_SG:DefaultRatesTransporter)
            CYCLE
          END
        ELSE
          BRA:GeneralRatesTransporterID = TRA:TID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:Fuel_Client
      ThisWindow.Update()
      CLI:ClientName = L_SG:GeneralFuelSurchargeClient
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SG:GeneralFuelSurchargeClient = CLI:ClientName
        BRA:GeneralFuelSurchargeClientID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?L_SG:GeneralFuelSurchargeClient
      IF L_SG:GeneralFuelSurchargeClient OR ?L_SG:GeneralFuelSurchargeClient{PROP:Req}
        CLI:ClientName = L_SG:GeneralFuelSurchargeClient
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SG:GeneralFuelSurchargeClient = CLI:ClientName
            BRA:GeneralFuelSurchargeClientID = CLI:CID
          ELSE
            CLEAR(BRA:GeneralFuelSurchargeClientID)
            SELECT(?L_SG:GeneralFuelSurchargeClient)
            CYCLE
          END
        ELSE
          BRA:GeneralFuelSurchargeClientID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?L_SG:DefaultTollRateClient
      IF L_SG:DefaultTollRateClient OR ?L_SG:DefaultTollRateClient{PROP:Req}
        CLI:ClientName = L_SG:DefaultTollRateClient
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SG:DefaultTollRateClient = CLI:ClientName
            BRA:GeneralTollChargeClientID = CLI:CID
          ELSE
            CLEAR(BRA:GeneralTollChargeClientID)
            SELECT(?L_SG:DefaultTollRateClient)
            CYCLE
          END
        ELSE
          BRA:GeneralTollChargeClientID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_TollClient
      ThisWindow.Update()
      CLI:ClientName = L_SG:DefaultTollRateClient
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SG:DefaultTollRateClient = CLI:ClientName
        BRA:GeneralTollChargeClientID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Branches PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                       PROJECT(BRA:AID)
                       PROJECT(BRA:FID)
                       JOIN(FLO:PKey_FID,BRA:FID)
                         PROJECT(FLO:Floor)
                         PROJECT(FLO:FID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
BRA:AID                LIKE(BRA:AID)                  !Browse key field - type derived from field
BRA:FID                LIKE(BRA:FID)                  !Browse key field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Branches File'),AT(,,298,206),FONT('Tahoma',8),RESIZE,GRAY,IMM,MDI,HLP('Browse_Branches'), |
  SYSTEM
                       LIST,AT(8,20,282,143),USE(?Browse:1),HVSCROLL,FORMAT('144L(2)|M~Branch Name~@s35@140L(2' & |
  ')|M~Branch Floor~@s35@'),FROM(Queue:Browse:1),IMM,MSG('Browsing Records')
                       BUTTON('&Select'),AT(96,166,45,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT
                       BUTTON('&Insert'),AT(146,166,45,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT
                       BUTTON('&Change'),AT(194,166,48,14),USE(?Change:3),LEFT,ICON('WAChange.ICO'),DEFAULT,FLAT
                       BUTTON('&Delete'),AT(245,166,45,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT
                       SHEET,AT(4,4,290,180),USE(?CurrentTab)
                         TAB('By Branch Name'),USE(?Tab:2)
                         END
                       END
                       BUTTON('Close'),AT(249,188,45,14),USE(?Close),LEFT,ICON('WAclose.ICO'),FLAT
                       BUTTON('Help'),AT(8,188,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW1::Sort2:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Branches')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Branches,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon BRA:AID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,BRA:FKey_AID)    ! Add the sort order for BRA:FKey_AID for sort order 1
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon BRA:FID for sort order 2
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,BRA:FKey_FID)    ! Add the sort order for BRA:FKey_FID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,BRA:FID,1,BRW1)                ! Initialize the browse locator using  using key: BRA:FKey_FID , BRA:FID
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon BRA:BranchName for sort order 3
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,BRA:Key_BranchName) ! Add the sort order for BRA:Key_BranchName for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(,BRA:BranchName,1,BRW1)         ! Initialize the browse locator using  using key: BRA:Key_BranchName , BRA:BranchName
  BRW1.AddField(BRA:BranchName,BRW1.Q.BRA:BranchName)      ! Field BRA:BranchName is a hot field or requires assignment from browse
  BRW1.AddField(FLO:Floor,BRW1.Q.FLO:Floor)                ! Field FLO:Floor is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BID,BRW1.Q.BRA:BID)                    ! Field BRA:BID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:AID,BRW1.Q.BRA:AID)                    ! Field BRA:AID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:FID,BRW1.Q.BRA:FID)                    ! Field BRA:FID is a hot field or requires assignment from browse
  BRW1.AddField(FLO:FID,BRW1.Q.FLO:FID)                    ! Field FLO:FID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Branches',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Branches',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Branches
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

