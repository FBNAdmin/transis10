

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS027.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Reminders PROCEDURE (p:Option, p:ID)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Users_Group      GROUP,PRE(LO)                         !
Login                STRING(20)                            !User Login
GroupName            STRING(35)                            !Name of this group of users
Login2               STRING(20)                            !User Login
                     END                                   !
LOC:Screen           GROUP,PRE(L_SG)                       !
Day                  STRING(30)                            !
CreatedBy            STRING(35)                            !
                     END                                   !
BRW10::View:Browse   VIEW(RemindersUsers)
                       PROJECT(REU:NoAction)
                       PROJECT(REU:ReminderDate)
                       PROJECT(REU:ReminderTime)
                       PROJECT(REU:RUID)
                       PROJECT(REU:RID)
                       PROJECT(REU:UID)
                       JOIN(USE:PKey_UID,REU:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:UID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
REU:NoAction           LIKE(REU:NoAction)             !List box control field - type derived from field
REU:NoAction_Icon      LONG                           !Entry's icon ID
REU:ReminderDate       LIKE(REU:ReminderDate)         !List box control field - type derived from field
REU:ReminderTime       LIKE(REU:ReminderTime)         !List box control field - type derived from field
REU:RUID               LIKE(REU:RUID)                 !Primary key field - type derived from field
REU:RID                LIKE(REU:RID)                  !Browse key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::REM:Record  LIKE(REM:RECORD),THREAD
QuickWindow          WINDOW('Form Reminders'),AT(,,314,277),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('Update_Reminders'),SYSTEM
                       SHEET,AT(4,4,307,254),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('RID:'),AT(215,6),USE(?REM:RID:Prompt),TRN
                           STRING(@n_10),AT(237,6,70,10),USE(REM:RID),RIGHT(1),TRN
                           PROMPT('Reminder Date:'),AT(9,22),USE(?REM:ReminderDate:Prompt),TRN
                           SPIN(@d5),AT(63,22,58,10),USE(REM:ReminderDate),RIGHT(1)
                           BUTTON('...'),AT(125,22,12,10),USE(?Calendar)
                           STRING('Day'),AT(143,22,77,10),USE(?String_Day),TRN
                           PROMPT('Reminder Time:'),AT(9,36),USE(?REM:ReminderTime:Prompt),TRN
                           SPIN(@t7),AT(63,36,58,10),USE(REM:ReminderTime),RIGHT(1),STEP(6000)
                           PROMPT('Type:'),AT(227,22),USE(?REM:ReminderType:Prompt),TRN
                           LIST,AT(249,22,58,10),USE(REM:ReminderType),VSCROLL,DISABLE,DROP(5),FROM('General|#0|Cl' & |
  'ient|#1|Truck / Trailer|#2'),MSG('General, Client'),TIP('General, Client')
                           PROMPT('ID:'),AT(227,36),USE(?REM:ID:Prompt),TRN
                           STRING(@n_10),AT(249,36,58,10),USE(REM:ID),RIGHT(1),TRN
                           CHECK(' &Reoccurs'),AT(63,58),USE(REM:Reoccurs),MSG('This reminder reoccurs'),TIP('This remin' & |
  'der reoccurs')
                           GROUP,AT(9,72,133,24),USE(?Group_Reoccur)
                             PROMPT('Reoccur Option:'),AT(9,72),USE(?REM:Period:Prompt),TRN
                             LIST,AT(63,72,79,10),USE(REM:Reoccur_Option),DROP(15),FROM('So many mins/hours|#1|Daily' & |
  '|#2|Weekly|#3|Bi-Weekly|#4|Monthly|#5|Bi-Monthly|#6|Quarterly|#7|Yearly|#8|None|#0')
                             PROMPT('Reoccur Period:'),AT(9,86),USE(?REM:Reoccur_Period:Prompt),TRN
                             ENTRY(@n10b),AT(63,86,58,10),USE(REM:Reoccur_Period),RIGHT(1)
                           END
                           PROMPT('Remind Option:'),AT(9,138),USE(?REM:RemindOption:Prompt),TRN
                           LIST,AT(63,138,58,10),USE(REM:RemindOption),DROP(15),FROM('All|#0|Group & User|#1|User|' & |
  '#2|Group|#3'),MSG('All, Group & User, User'),TIP('All, Group & User, User')
                           GROUP,AT(9,170,200,10),USE(?Group_UserGroup)
                             PROMPT('Group Name:'),AT(9,170),USE(?GroupName:Prompt),TRN
                             ENTRY(@s35),AT(63,170,119,10),USE(LO:GroupName),MSG('Name of this group of users'),REQ,TIP('Name of th' & |
  'is group of users')
                             BUTTON('...'),AT(189,170,12,10),USE(?CallLookup:2)
                           END
                           GROUP,AT(9,154,200,10),USE(?Group_User)
                             PROMPT('User Login:'),AT(9,154),USE(?Login:Prompt),TRN
                             ENTRY(@s20),AT(63,154,119,10),USE(LO:Login),MSG('User Login'),REQ,TIP('User Login')
                             BUTTON('...'),AT(189,154,12,10),USE(?CallLookup)
                           END
                           PROMPT('Notes:'),AT(9,192),USE(?REM:Notes:Prompt),TRN
                           TEXT,AT(63,192,245,60),USE(REM:Notes),VSCROLL
                           CHECK(' &Popup'),AT(9,232,38,8),USE(REM:Popup),RIGHT,MSG('Popup when due'),TIP('Popup when due'), |
  TRN
                           CHECK(' &Active'),AT(9,244,,8),USE(REM:Active),RIGHT,MSG('Active Reminder'),TIP('Active Reminder'), |
  TRN
                         END
                         TAB('&2) Reminder Users Extra'),USE(?Tab2)
                           LIST,AT(9,22,298,149),USE(?List),VSCROLL,FORMAT('80L(2)|M~User Login~@s20@38L(2)|MI~No ' & |
  'Action~@p p@[40L(2)|M~Date~@d5b@42L(2)|M~Time~@t7b@](269)|M~Reminder~'),FROM(Queue:Browse), |
  IMM,MSG('Browsing Records')
                         END
                         TAB('&3) Info'),USE(?Tab3)
                           PROMPT('Created by Login:'),AT(9,23),USE(?Login2:Prompt),TRN
                           ENTRY(@s20),AT(69,23,63,10),USE(LO:Login2),COLOR(00E9E9E9h),MSG('User Login'),READONLY,SKIP, |
  TIP('User Login')
                           PROMPT('Created Date:'),AT(9,40),USE(?REM:Created_Date:Prompt),TRN
                           ENTRY(@d5b),AT(69,40,63,10),USE(REM:Created_Date),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Created Time:'),AT(9,54),USE(?REM:Created_Time:Prompt),TRN
                           ENTRY(@t7),AT(69,54,63,10),USE(REM:Created_Time),COLOR(00E9E9E9h),READONLY,SKIP
                         END
                       END
                       BUTTON('&OK'),AT(210,260,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(262,260,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       ENTRY(@s35),AT(4,262,143,10),USE(L_SG:CreatedBy),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                       BUTTON('&Help'),AT(156,260,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar5            CalendarClass
BRW10                CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Do_Day          ROUTINE
    L_SG:Day                = Week_Day(REM:ReminderDate)

    IF REM:ReminderDate = TODAY()
       L_SG:Day             = 'Today (' & CLIP(L_SG:Day) & ')'
    .

    ?String_Day{PROP:Text}  = L_SG:Day
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Reminder Record'
  OF InsertRecord
    ActionMessage = 'Reminder Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Reminder Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Reminders')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REM:RID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(REM:Record,History::REM:Record)
  SELF.AddHistoryField(?REM:RID,1)
  SELF.AddHistoryField(?REM:ReminderDate,8)
  SELF.AddHistoryField(?REM:ReminderTime,9)
  SELF.AddHistoryField(?REM:ReminderType,2)
  SELF.AddHistoryField(?REM:ID,3)
  SELF.AddHistoryField(?REM:Reoccurs,24)
  SELF.AddHistoryField(?REM:Reoccur_Option,25)
  SELF.AddHistoryField(?REM:Reoccur_Period,26)
  SELF.AddHistoryField(?REM:RemindOption,11)
  SELF.AddHistoryField(?REM:Notes,14)
  SELF.AddHistoryField(?REM:Popup,5)
  SELF.AddHistoryField(?REM:Active,4)
  SELF.AddHistoryField(?REM:Created_Date,19)
  SELF.AddHistoryField(?REM:Created_Time,20)
  SELF.AddUpdateFile(Access:Reminders)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Reminders.SetOpenRelated()
  Relate:Reminders.Open                                    ! File Reminders used by this procedure, so make sure it's RelationManager is open
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:UserGroups.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Reminders
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW10.Init(?List,Queue:Browse.ViewPosition,BRW10::View:Browse,Queue:Browse,Relate:RemindersUsers,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
      IF SELF.Request = InsertRecord
         ! (BYTE=0, ULONG=0)
         REM:ReminderType     = p:Option
         REM:ID               = p:ID
  
         REM:Active           = TRUE
  
         IF REM:ReminderType = 0
            REM:UID           = GLO:UID
            REM:RemindOption  = 2
         .
      ELSIF SELF.Request = ChangeRecord
         IF REM:Created_UID ~= 0 AND REM:Created_UID ~= GLO:UID
            SELF.Request      = ViewRecord
         .
      ELSIF SELF.Request = ViewRecord
         DISABLE(?REM:ReminderDate)
         DISABLE(?REM:ReminderTime)
      .
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?Calendar)
    ?REM:ReminderTime{PROP:ReadOnly} = True
    DISABLE(?REM:ReminderType)
    DISABLE(?REM:Reoccur_Option)
    ?REM:Reoccur_Period{PROP:ReadOnly} = True
    DISABLE(?REM:RemindOption)
    ?LO:GroupName{PROP:ReadOnly} = True
    DISABLE(?CallLookup:2)
    ?LO:Login{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?LO:Login2{PROP:ReadOnly} = True
    ?REM:Created_Date{PROP:ReadOnly} = True
    ?REM:Created_Time{PROP:ReadOnly} = True
    ?L_SG:CreatedBy{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW10.Q &= Queue:Browse
  BRW10.AddSortOrder(,REU:FKey_RID)                        ! Add the sort order for REU:FKey_RID for sort order 1
  BRW10.AddRange(REU:RID,REM:RID)                          ! Add single value range limit for sort order 1
  BRW10.AddLocator(BRW10::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW10::Sort0:Locator.Init(,REU:RID,1,BRW10)              ! Initialize the browse locator using  using key: REU:FKey_RID , REU:RID
  BRW10.AppendOrder('-REU:ReminderDate,+REU:RUID')         ! Append an additional sort order
  ?List{PROP:IconList,1} = '~checkoffdim.ico'
  ?List{PROP:IconList,2} = '~checkon.ico'
  BRW10.AddField(USE:Login,BRW10.Q.USE:Login)              ! Field USE:Login is a hot field or requires assignment from browse
  BRW10.AddField(REU:NoAction,BRW10.Q.REU:NoAction)        ! Field REU:NoAction is a hot field or requires assignment from browse
  BRW10.AddField(REU:ReminderDate,BRW10.Q.REU:ReminderDate) ! Field REU:ReminderDate is a hot field or requires assignment from browse
  BRW10.AddField(REU:ReminderTime,BRW10.Q.REU:ReminderTime) ! Field REU:ReminderTime is a hot field or requires assignment from browse
  BRW10.AddField(REU:RUID,BRW10.Q.REU:RUID)                ! Field REU:RUID is a hot field or requires assignment from browse
  BRW10.AddField(REU:RID,BRW10.Q.REU:RID)                  ! Field REU:RID is a hot field or requires assignment from browse
  BRW10.AddField(USE:UID,BRW10.Q.USE:UID)                  ! Field USE:UID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_Reminders',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  IF ?REM:Reoccurs{Prop:Checked}
    ENABLE(?Group_Reoccur)
  END
  IF NOT ?REM:Reoccurs{PROP:Checked}
    DISABLE(?Group_Reoccur)
  END
  BRW10.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW10.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Reminders.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Reminders',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  REM:Popup = 1
  REM:Created_UID = GLO:UID
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_UserGroups
      Select_Users
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?REM:ReminderDate
          DO Do_Day
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',REM:ReminderDate)
      IF Calendar5.Response = RequestCompleted THEN
      REM:ReminderDate=Calendar5.SelectedDate
      DISPLAY(?REM:ReminderDate)
      END
      ThisWindow.Reset(True)
          DO Do_Day
    OF ?REM:Reoccurs
      IF ?REM:Reoccurs{PROP:Checked}
        ENABLE(?Group_Reoccur)
      END
      IF NOT ?REM:Reoccurs{PROP:Checked}
        DISABLE(?Group_Reoccur)
      END
      ThisWindow.Reset()
    OF ?REM:Reoccur_Option
          IF REM:Reoccurs = TRUE
             ! So many mins/hours|#1|Daily|#2|Weekly|#3|Bi-Weekly|#4|Monthly|#5|Bi-Monthly|#6|Quarterly|#7|Yearly|#8
             IF REM:Reoccur_Option = 1
                ?REM:Reoccur_Period{PROP:Text}    = '@t4b'
                ?REM:Reoccur_Period{PROP:Color}   = -1
                ENABLE(?REM:Reoccur_Period)
             ELSE
                DISABLE(?REM:Reoccur_Period)
                ?REM:Reoccur_Period{PROP:Color}   = COLOR:Gray
             .
          ELSE
             DISABLE(?REM:Reoccur_Period)
             ?REM:Reoccur_Period{PROP:Color}      = COLOR:Gray
          .
    OF ?REM:RemindOption
          ! 'All|#0|Group & User|#1|User|#2|Group|#3'
          CASE REM:RemindOption
          OF 1
             ENABLE(?Group_UserGroup)
             ENABLE(?Group_User)
          OF 2
             DISABLE(?Group_UserGroup)
             ENABLE(?Group_User)
          OF 3
             ENABLE(?Group_UserGroup)
             DISABLE(?Group_User)
          ELSE
             DISABLE(?Group_UserGroup)
             DISABLE(?Group_User)
          .
    OF ?LO:GroupName
      IF NOT QuickWindow{PROP:AcceptAll}
        IF LO:GroupName OR ?LO:GroupName{PROP:Req}
          USEG:GroupName = LO:GroupName
          IF Access:UserGroups.TryFetch(USEG:Key_GroupName)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              LO:GroupName = USEG:GroupName
              REM:UGID = USEG:UGID
            ELSE
              CLEAR(REM:UGID)
              SELECT(?LO:GroupName)
              CYCLE
            END
          ELSE
            REM:UGID = USEG:UGID
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update()
      USEG:GroupName = LO:GroupName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LO:GroupName = USEG:GroupName
        REM:UGID = USEG:UGID
      END
      ThisWindow.Reset(1)
    OF ?LO:Login
      IF NOT QuickWindow{PROP:AcceptAll}
        IF LO:Login OR ?LO:Login{PROP:Req}
          USE:Login = LO:Login
          IF Access:Users.TryFetch(USE:Key_Login)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              LO:Login = USE:Login
              REM:UID = USE:UID
            ELSE
              CLEAR(REM:UID)
              SELECT(?LO:Login)
              CYCLE
            END
          ELSE
            REM:UID = USE:UID
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      USE:Login = LO:Login
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LO:Login = USE:Login
        REM:UID = USE:UID
      END
      ThisWindow.Reset(1)
    OF ?OK
      ThisWindow.Update()
          CASE REM:RemindOption
          OF 1
          OF 2
             CLEAR(REM:UGID)
          OF 3
             CLEAR(REM:UID)
          ELSE
             CLEAR(REM:UID)
             CLEAR(REM:UGID)
          .
          IF REM:Reoccurs = TRUE
             IF REM:Reoccur_Option = 0
                MESSAGE('For reoccuring reminders you must specify a reoccuring option.', 'Reoccuring Reminder', ICON:Exclamation)
                QuickWindow{PROP:AcceptAll}   = FALSE
                CYCLE
          .  .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
          GLO:Reminder_Inc    += 1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?REM:ReminderDate
          DO Do_Day
    OF ?REM:RemindOption
          ! 'All|#0|Group & User|#1|User|#2|Group|#3'
          CASE REM:RemindOption
          OF 1
             ENABLE(?Group_UserGroup)
             ENABLE(?Group_User)
          OF 2
             DISABLE(?Group_UserGroup)
             ENABLE(?Group_User)
          OF 3
             ENABLE(?Group_UserGroup)
             DISABLE(?Group_User)
          ELSE
             DISABLE(?Group_UserGroup)
             DISABLE(?Group_User)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          ! 'All|#0|Group & User|#1|User|#2|Group|#3'
          CASE REM:RemindOption
          OF 1
             ENABLE(?Group_UserGroup)
             ENABLE(?Group_User)
          OF 2
             DISABLE(?Group_UserGroup)
             ENABLE(?Group_User)
          OF 3
             ENABLE(?Group_UserGroup)
             DISABLE(?Group_User)
          ELSE
             DISABLE(?Group_UserGroup)
             DISABLE(?Group_User)
          .
      
      
          IF SELF.Request = InsertRecord
             ! (BYTE=0, ULONG=0)
             REM:ReminderType     = p:Option
             REM:ID               = p:ID
      
             REM:ReminderTime     = INT(CLOCK() / 100 / 60 / 60 + 1) * 100 * 60 * 60 + 1
             REM:ReminderDate     = TODAY() + 1
      
             REM:Active           = TRUE
          .
      
          db.debugout('[Update_Reminders]  Option: ' & p:Option & ',  ID: ' & p:ID)
      
      
          !DISABLE(?REM:ReminderType)
      
      
      
          USE:UID         = REM:UID
          IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
             LO:Login     = USE:Login
          .
      
          USEG:UGID       = REM:UGID
          IF Access:UserGroups.TryFetch(USEG:PKey_UGID) = LEVEL:Benign
             LO:GroupName = USEG:GroupName
          .
      
      
      
          USE:UID         = REM:Created_UID
          IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
             LO:Login2    = USE:Login
          .
          DO Do_Day
          IF SELF.Request = ChangeRecord AND REM:Active = FALSE
             ?REM:Notes{PROP:BackGround} = 0E9E9E9H
             ?REM:Notes{PROP:ReadOnly}   = TRUE
          .
          L_SG:CreatedBy  = 'Created - ' & CLIP(LO:Login2) & ' - ' & FORMAT(REM:Created_Date,@d5) & ' @ ' & FORMAT(REM:Created_Time,@t4)
          DISPLAY
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW10.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (REU:NoAction = 1)
    SELF.Q.REU:NoAction_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.REU:NoAction_Icon = 1                           ! Set icon from icon list
  END

