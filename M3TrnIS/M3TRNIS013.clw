

   MEMBER('M3TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M3TRNIS013.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Remittance_Advice PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(_Remittance)
                       PROJECT(REMI:REMID)
                       PROJECT(REMI:RemittanceDate)
                       PROJECT(REMI:RemittanceTime)
                       PROJECT(REMI:Total)
                       PROJECT(REMI:TID)
                       PROJECT(REMI:RERID)
                       PROJECT(REMI:BID)
                       JOIN(REMR:PKey_RERID,REMI:RERID)
                         PROJECT(REMR:RunDescription)
                         PROJECT(REMR:RERID)
                       END
                       JOIN(TRA:PKey_TID,REMI:TID)
                         PROJECT(TRA:TransporterName)
                         PROJECT(TRA:TID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
REMI:REMID             LIKE(REMI:REMID)               !List box control field - type derived from field
REMR:RunDescription    LIKE(REMR:RunDescription)      !List box control field - type derived from field
REMI:RemittanceDate    LIKE(REMI:RemittanceDate)      !List box control field - type derived from field
REMI:RemittanceTime    LIKE(REMI:RemittanceTime)      !List box control field - type derived from field
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
REMI:Total             LIKE(REMI:Total)               !List box control field - type derived from field
REMI:TID               LIKE(REMI:TID)                 !List box control field - type derived from field
REMI:RERID             LIKE(REMI:RERID)               !List box control field - type derived from field
REMI:BID               LIKE(REMI:BID)                 !List box control field - type derived from field
REMR:RERID             LIKE(REMR:RERID)               !Related join file key field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Remittance Advice'),AT(,,398,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('Browse_Remittance_Advice'),SYSTEM
                       LIST,AT(8,30,381,124),USE(?Browse:1),HVSCROLL,FORMAT('30R(2)|M~REM ID~C(0)@n_10@70L(2)|' & |
  'M~Run Description~C(0)@s35@[38R(2)|M~Date~C(0)@d5b@38R(2)|M~Time~C(0)@t7@]|M~Remitta' & |
  'nce~70L(2)|M~Transporter Name~C(0)@s35@56R(2)|M~Total~C(0)@n-14.2@40R(2)|M~TID~C(0)@' & |
  'n_10@30R(2)|M~Run ID~C(0)@n_10@40R(2)|M~BID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he _Remittance file')
                       BUTTON('&Select'),AT(128,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(182,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(234,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(288,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                       BUTTON('&Delete'),AT(340,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,390,172),USE(?CurrentTab)
                         TAB('&1) By Remittance ID'),USE(?Tab:2)
                         END
                         TAB('&2) By Transporter'),USE(?Tab:3)
                           BUTTON('Select Transporter'),AT(9,158,,14),USE(?SelectTransporter),LEFT,ICON('WAPARENT.ICO'), |
  DISABLE,FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&3) By Branch'),USE(?Tab:4)
                         END
                         TAB('&4) By Remittance Run'),USE(?Tab:5)
                           BUTTON('Select Remittance Run'),AT(9,158,,14),USE(?Select_Remittance_Runs),LEFT,ICON('WAPARENT.ICO'), |
  DISABLE,FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(345,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(306,2,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Remittance_Advice')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                                  ! File Transporter used by this procedure, so make sure it's RelationManager is open
  Access:_Remittance_Runs.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_Remittance,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,REMI:FKey_TID)                        ! Add the sort order for REMI:FKey_TID for sort order 1
  BRW1.AddRange(REMI:TID,Relate:_Remittance,Relate:Transporter) ! Add file relationship range limit for sort order 1
  BRW1.AddSortOrder(,REMI:FKey_BID)                        ! Add the sort order for REMI:FKey_BID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,REMI:BID,1,BRW1)               ! Initialize the browse locator using  using key: REMI:FKey_BID , REMI:BID
  BRW1.AddSortOrder(,REMI:FKey_RERID)                      ! Add the sort order for REMI:FKey_RERID for sort order 3
  BRW1.AddRange(REMI:RERID,Relate:_Remittance,Relate:_Remittance_Runs) ! Add file relationship range limit for sort order 3
  BRW1.AddSortOrder(,REMI:PKey_REMID)                      ! Add the sort order for REMI:PKey_REMID for sort order 4
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 4
  BRW1::Sort0:Locator.Init(,REMI:REMID,1,BRW1)             ! Initialize the browse locator using  using key: REMI:PKey_REMID , REMI:REMID
  BRW1.AddField(REMI:REMID,BRW1.Q.REMI:REMID)              ! Field REMI:REMID is a hot field or requires assignment from browse
  BRW1.AddField(REMR:RunDescription,BRW1.Q.REMR:RunDescription) ! Field REMR:RunDescription is a hot field or requires assignment from browse
  BRW1.AddField(REMI:RemittanceDate,BRW1.Q.REMI:RemittanceDate) ! Field REMI:RemittanceDate is a hot field or requires assignment from browse
  BRW1.AddField(REMI:RemittanceTime,BRW1.Q.REMI:RemittanceTime) ! Field REMI:RemittanceTime is a hot field or requires assignment from browse
  BRW1.AddField(TRA:TransporterName,BRW1.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW1.AddField(REMI:Total,BRW1.Q.REMI:Total)              ! Field REMI:Total is a hot field or requires assignment from browse
  BRW1.AddField(REMI:TID,BRW1.Q.REMI:TID)                  ! Field REMI:TID is a hot field or requires assignment from browse
  BRW1.AddField(REMI:RERID,BRW1.Q.REMI:RERID)              ! Field REMI:RERID is a hot field or requires assignment from browse
  BRW1.AddField(REMI:BID,BRW1.Q.REMI:BID)                  ! Field REMI:BID is a hot field or requires assignment from browse
  BRW1.AddField(REMR:RERID,BRW1.Q.REMR:RERID)              ! Field REMR:RERID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:TID,BRW1.Q.TRA:TID)                    ! Field TRA:TID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Remittance_Advice',QuickWindow)     ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Transporter.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Remittance_Advice',QuickWindow)  ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Remittance
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Form LFM_CFile
!!! </summary>
UpdateLFM_CFile PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::CFG:Record  LIKE(CFG:RECORD),THREAD
QuickWindow          WINDOW('Form LFM_CFile'),AT(,,358,174),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('UpdateLFM_CFile'),SYSTEM
                       SHEET,AT(4,4,350,148),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('App Name:'),AT(9,20),USE(?CFG:AppName:Prompt),TRN
                           ENTRY(@s255),AT(61,20,289,10),USE(CFG:AppName)
                           PROMPT('Proc Id:'),AT(9,34),USE(?CFG:ProcId:Prompt),TRN
                           ENTRY(@s255),AT(61,34,289,10),USE(CFG:ProcId)
                           PROMPT('User Id:'),AT(9,48),USE(?CFG:UserId:Prompt),TRN
                           ENTRY(@n-7),AT(61,48,40,10),USE(CFG:UserId),RIGHT(1)
                           PROMPT('Ctrl Id:'),AT(9,62),USE(?CFG:CtrlId:Prompt),TRN
                           ENTRY(@n-7),AT(61,62,40,10),USE(CFG:CtrlId),RIGHT(1)
                           PROMPT('Format Id:'),AT(9,76),USE(?CFG:FormatId:Prompt),TRN
                           ENTRY(@n-7),AT(61,76,40,10),USE(CFG:FormatId),RIGHT(1)
                           PROMPT('Format Name:'),AT(9,90),USE(?CFG:FormatName:Prompt),TRN
                           ENTRY(@s30),AT(61,90,124,10),USE(CFG:FormatName)
                           PROMPT('Flag:'),AT(9,104),USE(?CFG:Flag:Prompt),TRN
                           ENTRY(@n3),AT(61,104,40,10),USE(CFG:Flag)
                           PROMPT('Format:'),AT(9,118),USE(?CFG:Format:Prompt),TRN
                           TEXT,AT(61,118,289,30),USE(CFG:Format)
                         END
                         TAB('&2) General (cont.)'),USE(?Tab:2)
                           PROMPT('Var Line:'),AT(9,20),USE(?CFG:VarLine:Prompt),TRN
                           TEXT,AT(62,20,289,30),USE(CFG:VarLine)
                         END
                       END
                       BUTTON('&OK'),AT(252,156,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(306,156,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,156,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a LFM_CFile Record'
  OF ChangeRecord
    ActionMessage = 'Changing a LFM_CFile Record'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateLFM_CFile')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CFG:AppName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CFG:Record,History::CFG:Record)
  SELF.AddHistoryField(?CFG:AppName,1)
  SELF.AddHistoryField(?CFG:ProcId,2)
  SELF.AddHistoryField(?CFG:UserId,3)
  SELF.AddHistoryField(?CFG:CtrlId,4)
  SELF.AddHistoryField(?CFG:FormatId,5)
  SELF.AddHistoryField(?CFG:FormatName,6)
  SELF.AddHistoryField(?CFG:Flag,7)
  SELF.AddHistoryField(?CFG:Format,8)
  SELF.AddHistoryField(?CFG:VarLine,9)
  SELF.AddUpdateFile(Access:LFM_CFile)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:LFM_CFile.Open                                    ! File LFM_CFile used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LFM_CFile
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?CFG:AppName{PROP:ReadOnly} = True
    ?CFG:ProcId{PROP:ReadOnly} = True
    ?CFG:UserId{PROP:ReadOnly} = True
    ?CFG:CtrlId{PROP:ReadOnly} = True
    ?CFG:FormatId{PROP:ReadOnly} = True
    ?CFG:FormatName{PROP:ReadOnly} = True
    ?CFG:Flag{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('UpdateLFM_CFile',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LFM_CFile.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateLFM_CFile',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_ListManager PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:ProcId           STRING(255)                           !
LOC:Proc_Q           QUEUE,PRE(L_PQ)                       !
ProcId               STRING(255)                           !
                     END                                   !
BRW1::View:Browse    VIEW(LFM_CFile)
                       PROJECT(CFG:ProcId)
                       PROJECT(CFG:UserId)
                       PROJECT(CFG:CtrlId)
                       PROJECT(CFG:FormatId)
                       PROJECT(CFG:FormatName)
                       PROJECT(CFG:Flag)
                       PROJECT(CFG:Format)
                       PROJECT(CFG:VarLine)
                       PROJECT(CFG:AppName)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
CFG:ProcId             LIKE(CFG:ProcId)               !List box control field - type derived from field
CFG:UserId             LIKE(CFG:UserId)               !List box control field - type derived from field
CFG:CtrlId             LIKE(CFG:CtrlId)               !List box control field - type derived from field
CFG:FormatId           LIKE(CFG:FormatId)             !List box control field - type derived from field
CFG:FormatName         LIKE(CFG:FormatName)           !List box control field - type derived from field
CFG:Flag               LIKE(CFG:Flag)                 !List box control field - type derived from field
CFG:Format             LIKE(CFG:Format)               !List box control field - type derived from field
CFG:VarLine            LIKE(CFG:VarLine)              !List box control field - type derived from field
CFG:AppName            LIKE(CFG:AppName)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the List Formats File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_ListManager'),SYSTEM
                       GROUP,AT(4,180,191,14),USE(?Group1)
                         PROMPT('Proc. Id:'),AT(4,182),USE(?Prompt1)
                         LIST,AT(36,182,95,10),USE(LOC:ProcId),VSCROLL,DROP(20,200),FORMAT('1020L(2)|M~Proc. Id~@s255@'), |
  FROM(LOC:Proc_Q)
                         BUTTON('Delete All'),AT(140,180,55,14),USE(?Button_Delete)
                       END
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Proc. Id~@s255@30R(2)|M~' & |
  'User Id~C(0)@n-7@30R(2)|M~Ctrl Id~C(0)@n-7@35R(2)|M~Format Id~C(0)@n-7@60L(2)|M~Form' & |
  'at Name~@s30@20R(2)|M~Flag~C(0)@n3@80L(2)|M~Format~@s255@80L(2)|M~Var Line~@s255@60L' & |
  '(2)|M~App Name~@s255@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the LFM_CFile file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Proc. Id'),USE(?Tab:2)
                         END
                         TAB('&2) By Name'),USE(?Tab2)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(242,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_ListManager')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:LFM_CFile.Open                                    ! File LFM_CFile used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LFM_CFile,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW1.AddSortOrder(,)                                     ! Add the sort order for  for sort order 1
  BRW1.AppendOrder('+CFG:AppName,+CFG:FormatName,+CFG:FormatId') ! Append an additional sort order
  BRW1.AddSortOrder(,CFG:key_Main)                         ! Add the sort order for CFG:key_Main for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,CFG:AppName,1,BRW1)            ! Initialize the browse locator using  using key: CFG:key_Main , CFG:AppName
  BRW1.AppendOrder('+CFG:FormatName')                      ! Append an additional sort order
  BRW1.AddField(CFG:ProcId,BRW1.Q.CFG:ProcId)              ! Field CFG:ProcId is a hot field or requires assignment from browse
  BRW1.AddField(CFG:UserId,BRW1.Q.CFG:UserId)              ! Field CFG:UserId is a hot field or requires assignment from browse
  BRW1.AddField(CFG:CtrlId,BRW1.Q.CFG:CtrlId)              ! Field CFG:CtrlId is a hot field or requires assignment from browse
  BRW1.AddField(CFG:FormatId,BRW1.Q.CFG:FormatId)          ! Field CFG:FormatId is a hot field or requires assignment from browse
  BRW1.AddField(CFG:FormatName,BRW1.Q.CFG:FormatName)      ! Field CFG:FormatName is a hot field or requires assignment from browse
  BRW1.AddField(CFG:Flag,BRW1.Q.CFG:Flag)                  ! Field CFG:Flag is a hot field or requires assignment from browse
  BRW1.AddField(CFG:Format,BRW1.Q.CFG:Format)              ! Field CFG:Format is a hot field or requires assignment from browse
  BRW1.AddField(CFG:VarLine,BRW1.Q.CFG:VarLine)            ! Field CFG:VarLine is a hot field or requires assignment from browse
  BRW1.AddField(CFG:AppName,BRW1.Q.CFG:AppName)            ! Field CFG:AppName is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_ListManager',QuickWindow)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LFM_CFile.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_ListManager',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    UpdateLFM_CFile
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Delete
      ThisWindow.Update()
          CLEAR(CFG:Record,-1)
          !CFG:ProcId  = LOC:ProcId
          !SET(CFG:SKey_ProcId,CFG:SKey_ProcId)
          SET(CFG:key_Main) !, CFG:key_Main)
          LOOP
             NEXT(LFM_CFile)
             IF ERRORCODE()
                BREAK
             .
             IF CFG:ProcId = LOC:ProcId
                !BREAK
                DELETE(LFM_CFile)
          .  .
      
          BRW1.ResetFromFile()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          SET(CFG:key_Main)
          LOOP
             NEXT(LFM_CFile)
             IF ERRORCODE()
                BREAK
             .
      
             L_PQ:ProcId      = CFG:ProcId
             GET(LOC:Proc_Q, L_PQ:ProcId)
             IF ERRORCODE()
                L_PQ:ProcId   = CFG:ProcId
                ADD(LOC:Proc_Q, +L_PQ:ProcId)
          .  .
      
          DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group1, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group1

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_DeliveryStatuses PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(DeliveryProgress)
                       PROJECT(DELP:StatusDate)
                       PROJECT(DELP:StatusTime)
                       PROJECT(DELP:DPID)
                       PROJECT(DELP:DSID)
                       PROJECT(DELP:DID)
                       JOIN(DEL:PKey_DID,DELP:DID)
                         PROJECT(DEL:DINo)
                         PROJECT(DEL:DID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DELP:StatusDate        LIKE(DELP:StatusDate)          !List box control field - type derived from field
DELP:StatusTime        LIKE(DELP:StatusTime)          !List box control field - type derived from field
DELP:DPID              LIKE(DELP:DPID)                !Primary key field - type derived from field
DELP:DSID              LIKE(DELP:DSID)                !Browse key field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DELS:Record LIKE(DELS:RECORD),THREAD
QuickWindow          WINDOW('Form Delivery Status'),AT(,,213,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateDeliveryStatuses'),SYSTEM
                       SHEET,AT(4,4,205,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Status:'),AT(9,22),USE(?DELS:Status:Prompt),TRN
                           ENTRY(@s35),AT(61,22,144,10),USE(DELS:DeliveryStatus),REQ
                           PROMPT('Report Column:'),AT(9,50),USE(?DELS:ReportColumn:Prompt),TRN
                           LIST,AT(61,50,60,10),USE(DELS:ReportColumn),DROP(5),FROM('None|#0|Un-Pack|#1|Up-Lift|#2'), |
  MSG('Select the report column that you want this status date to show under'),TIP('Select the' & |
  ' report column that you want this status date to show under')
                         END
                         TAB('&2) Delivery Progress'),USE(?Tab:2)
                           LIST,AT(9,20,197,93),USE(?Browse:2),HVSCROLL,FORMAT('40R(2)|M~DI No.~C(0)@n_10@48R(2)|M' & |
  '~Status Date~C(0)@d6@48R(2)|M~Status Time~C(0)@t7@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he DeliveryProgress file')
                         END
                       END
                       BUTTON('&OK'),AT(108,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(160,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Delivery Status Record'
  OF InsertRecord
    ActionMessage = 'Delivery Status Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Delivery Status Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_DeliveryStatuses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DELS:Status:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DELS:Record,History::DELS:Record)
  SELF.AddHistoryField(?DELS:DeliveryStatus,2)
  SELF.AddHistoryField(?DELS:ReportColumn,3)
  SELF.AddUpdateFile(Access:DeliveryStatuses)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:DeliveryProgress.SetOpenRelated()
  Relate:DeliveryProgress.Open                             ! File DeliveryProgress used by this procedure, so make sure it's RelationManager is open
  Access:DeliveryStatuses.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveryStatuses
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:DeliveryProgress,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?DELS:DeliveryStatus{PROP:ReadOnly} = True
    DISABLE(?DELS:ReportColumn)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon DELP:DSID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,DELP:FKey_DSID)  ! Add the sort order for DELP:FKey_DSID for sort order 1
  BRW2.AddRange(DELP:DSID,Relate:DeliveryProgress,Relate:DeliveryStatuses) ! Add file relationship range limit for sort order 1
  BRW2.AddField(DEL:DINo,BRW2.Q.DEL:DINo)                  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW2.AddField(DELP:StatusDate,BRW2.Q.DELP:StatusDate)    ! Field DELP:StatusDate is a hot field or requires assignment from browse
  BRW2.AddField(DELP:StatusTime,BRW2.Q.DELP:StatusTime)    ! Field DELP:StatusTime is a hot field or requires assignment from browse
  BRW2.AddField(DELP:DPID,BRW2.Q.DELP:DPID)                ! Field DELP:DPID is a hot field or requires assignment from browse
  BRW2.AddField(DELP:DSID,BRW2.Q.DELP:DSID)                ! Field DELP:DSID is a hot field or requires assignment from browse
  BRW2.AddField(DEL:DID,BRW2.Q.DEL:DID)                    ! Field DEL:DID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_DeliveryStatuses',QuickWindow)      ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryProgress.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_DeliveryStatuses',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_ScheduledRuns PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::SCH:Record  LIKE(SCH:RECORD),THREAD
QuickWindow          WINDOW('Form Scheduled Runs'),AT(,,268,173),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdateScheduledRuns'),SYSTEM
                       SHEET,AT(4,4,260,150),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Schedule Description:'),AT(9,20),USE(?SCH:ScheduleDescription:Prompt),TRN
                           ENTRY(@s40),AT(97,20,164,10),USE(SCH:ScheduleDescription),MSG('Description of this sche' & |
  'duled item'),REQ,TIP('Description of this scheduled item')
                           PROMPT('Notes:'),AT(9,34),USE(?SCH:Notes:Prompt),TRN
                           TEXT,AT(97,34,164,30),USE(SCH:Notes),VSCROLL
                           PROMPT('Schedule Type:'),AT(9,68),USE(?SCH:ScheduleType:Prompt),TRN
                           LIST,AT(97,68,100,10),USE(SCH:ScheduleType),RIGHT(1),DROP(5),FROM('Unknown|#0|Client Balances|#1'), |
  MSG('Schedule Type'),TIP('Schedule Type')
                           PROMPT('Schedule Option:'),AT(9,82),USE(?SCH:ScheduleOption:Prompt),TRN
                           LIST,AT(97,82,100,10),USE(SCH:ScheduleOption),DROP(5),FROM('None|#0|Daily|#1')
                           PROMPT('Schedule Time:'),AT(9,96),USE(?SCH:ScheduleTime:Prompt),TRN
                           SPIN(@t7),AT(97,96,60,10),USE(SCH:ScheduleTime),MSG('Time'),STEP(6000),TIP('Time')
                           LINE,AT(9,115,251,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           BUTTON('Reset'),AT(210,134,49,14),USE(?Button_Reset)
                           PROMPT('Last Executed Date:'),AT(9,122),USE(?SCH:LastExecuted_Date:Prompt),FONT('Tahoma'), |
  TRN
                           ENTRY(@d6b),AT(97,122,60,10),USE(SCH:LastExecuted_Date),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Last Executed Time:'),AT(9,138),USE(?SCH:LastExecuted_Time:Prompt),FONT('Tahoma'), |
  TRN
                           ENTRY(@t8),AT(97,138,60,10),USE(SCH:LastExecuted_Time),COLOR(00E9E9E9h),READONLY,SKIP
                         END
                       END
                       BUTTON('&OK'),AT(160,156,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(214,156,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,156,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Fuel Surcharge Record'
  OF InsertRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ScheduledRuns')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SCH:ScheduleDescription:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(SCH:Record,History::SCH:Record)
  SELF.AddHistoryField(?SCH:ScheduleDescription,2)
  SELF.AddHistoryField(?SCH:Notes,3)
  SELF.AddHistoryField(?SCH:ScheduleType,4)
  SELF.AddHistoryField(?SCH:ScheduleOption,5)
  SELF.AddHistoryField(?SCH:ScheduleTime,9)
  SELF.AddHistoryField(?SCH:LastExecuted_Date,12)
  SELF.AddHistoryField(?SCH:LastExecuted_Time,13)
  SELF.AddUpdateFile(Access:ScheduledRuns)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ScheduledRuns.Open                                ! File ScheduledRuns used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ScheduledRuns
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?SCH:ScheduleDescription{PROP:ReadOnly} = True
    DISABLE(?SCH:ScheduleType)
    DISABLE(?SCH:ScheduleOption)
    DISABLE(?Button_Reset)
    ?SCH:LastExecuted_Date{PROP:ReadOnly} = True
    ?SCH:LastExecuted_Time{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_ScheduledRuns',QuickWindow)         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ScheduledRuns.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_ScheduledRuns',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Reset
      ThisWindow.Update()
          CLEAR(SCH:LastExecuted_Date)
          CLEAR(SCH:LastExecuted_Time)
          DISPLAY
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_ScheduledRuns PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:Group            GROUP,PRE()                           !
LG:ScheduleType      STRING(20)                            !Schedule Type - hardcoded to a procedure
LG:ScheduleOption    STRING(20)                            !
                     END                                   !
BRW1::View:Browse    VIEW(ScheduledRuns)
                       PROJECT(SCH:ScheduleDescription)
                       PROJECT(SCH:Notes)
                       PROJECT(SCH:ScheduleTime)
                       PROJECT(SCH:LastExecuted_Date)
                       PROJECT(SCH:LastExecuted_Time)
                       PROJECT(SCH:ScheduleType)
                       PROJECT(SCH:ScheduleOption)
                       PROJECT(SCH:SRID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
SCH:ScheduleDescription LIKE(SCH:ScheduleDescription) !List box control field - type derived from field
SCH:Notes              LIKE(SCH:Notes)                !List box control field - type derived from field
LG:ScheduleType        LIKE(LG:ScheduleType)          !List box control field - type derived from local data
LG:ScheduleOption      LIKE(LG:ScheduleOption)        !List box control field - type derived from local data
SCH:ScheduleTime       LIKE(SCH:ScheduleTime)         !List box control field - type derived from field
SCH:LastExecuted_Date  LIKE(SCH:LastExecuted_Date)    !List box control field - type derived from field
SCH:LastExecuted_Time  LIKE(SCH:LastExecuted_Time)    !List box control field - type derived from field
SCH:ScheduleType       LIKE(SCH:ScheduleType)         !Browse hot field - type derived from field
SCH:ScheduleOption     LIKE(SCH:ScheduleOption)       !Browse hot field - type derived from field
SCH:SRID               LIKE(SCH:SRID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Scheduled Runs File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Browse_ScheduledRuns'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Schedule Description~C(0' & |
  ')@s40@80L(2)|M~Notes~@s255@56L(2)|M~Type~C(0)@s20@50L(2)|M~Option~C(0)@s20@54L(2)|M~' & |
  'Schedule Time~C(0)@t7@[48L(2)|M~Date~C(0)@d6b@32L(2)|M~Time~C(0)@t8@]|M~Last Executed~'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing the ScheduledRuns file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Schedule Description'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(134,182,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('Create Standard'),AT(4,180,,14),USE(?Button_CreateStandard),LEFT,ICON(ICON:Thumbnail), |
  FLAT,TIP('Create the standard Schedules (will not create duplicates)')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_ScheduledRuns')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LG:ScheduleType',LG:ScheduleType)                  ! Added by: BrowseBox(ABC)
  BIND('LG:ScheduleOption',LG:ScheduleOption)              ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:ScheduledRuns.Open                                ! File ScheduledRuns used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ScheduledRuns,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,SCH:Key_ScheduleDescription)          ! Add the sort order for SCH:Key_ScheduleDescription for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,SCH:ScheduleDescription,1,BRW1) ! Initialize the browse locator using  using key: SCH:Key_ScheduleDescription , SCH:ScheduleDescription
  BRW1.AddField(SCH:ScheduleDescription,BRW1.Q.SCH:ScheduleDescription) ! Field SCH:ScheduleDescription is a hot field or requires assignment from browse
  BRW1.AddField(SCH:Notes,BRW1.Q.SCH:Notes)                ! Field SCH:Notes is a hot field or requires assignment from browse
  BRW1.AddField(LG:ScheduleType,BRW1.Q.LG:ScheduleType)    ! Field LG:ScheduleType is a hot field or requires assignment from browse
  BRW1.AddField(LG:ScheduleOption,BRW1.Q.LG:ScheduleOption) ! Field LG:ScheduleOption is a hot field or requires assignment from browse
  BRW1.AddField(SCH:ScheduleTime,BRW1.Q.SCH:ScheduleTime)  ! Field SCH:ScheduleTime is a hot field or requires assignment from browse
  BRW1.AddField(SCH:LastExecuted_Date,BRW1.Q.SCH:LastExecuted_Date) ! Field SCH:LastExecuted_Date is a hot field or requires assignment from browse
  BRW1.AddField(SCH:LastExecuted_Time,BRW1.Q.SCH:LastExecuted_Time) ! Field SCH:LastExecuted_Time is a hot field or requires assignment from browse
  BRW1.AddField(SCH:ScheduleType,BRW1.Q.SCH:ScheduleType)  ! Field SCH:ScheduleType is a hot field or requires assignment from browse
  BRW1.AddField(SCH:ScheduleOption,BRW1.Q.SCH:ScheduleOption) ! Field SCH:ScheduleOption is a hot field or requires assignment from browse
  BRW1.AddField(SCH:SRID,BRW1.Q.SCH:SRID)                  ! Field SCH:SRID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_ScheduledRuns',QuickWindow)         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ScheduledRuns.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_ScheduledRuns',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_ScheduledRuns
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_CreateStandard
      ThisWindow.Update()
          CLEAR(SCH:Record)
          IF Access:ScheduledRuns.PrimeRecord() = LEVEL:Benign
             SCH:ScheduleDescription  = 'Client Update (balances etc.)'
             SCH:Notes                = ''
             SCH:ScheduleType         = 1         ! Client Update
             SCH:ScheduleOption       = 1         ! Daily
             SCH:ScheduleTime         = DEFORMAT('06:00', @t1)
      
             SCH:ScheduleDate         = TODAY()
      
             IF Access:ScheduledRuns.Insert() ~= LEVEL:Benign
                Access:ScheduledRuns.CancelAutoInc()
             ELSE
                BRW1.ResetFromFile()
                BRW1.ResetQueue(1)
          .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.SetQueueRecord PROCEDURE

  CODE
      EXECUTE SCH:ScheduleType            ! Unknown|Client Balances
         LG:ScheduleType      = 'Client Balances'
      ELSE
         LG:ScheduleType      = 'Unknown'
      .
  
      EXECUTE SCH:ScheduleOption          ! None|Daily
         LG:ScheduleOption    = 'Daily'
      ELSE
         LG:ScheduleOption    = 'None'
      .
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

