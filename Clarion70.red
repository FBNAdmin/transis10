-- Default Redirection for Clarion 7.2

[Copy]
-- Directories only used when copying dlls
*.dll = %BIN%;%BIN%\AddIns\BackendBindings\ClarionBinding\Common;%ROOT%\Accessory\bin

[Debug]
*.obj = obj\debug
*.res = obj\debug
*.rsc = obj\debug
*.lib = %TRANSIS%\Lib
*.dll = %TRANSIS%\Bin
*.FileList.xml = obj\debug
*.map = map\debug

[Release]
*.obj = obj\release
*.res = obj\release
*.rsc = obj\release
*.lib = %TRANSIS%\Lib
*.dll = %TRANSIS%\Bin
*.FileList.xml = obj\release
*.map = map\release

[Common]
*.tp? = %ROOT%\template\win
*.trf = %ROOT%\template\win
*.txs = %ROOT%\template\win
*.stt = %ROOT%\template\win
*.*   = .; %TRANSIS%\Images; %TRANSIS%; %TRANSIS%\Classes2; %TRANSIS%\IKB_Lib; %ROOT%\libsrc\win; %ROOT%\images; %ROOT%\template\win
*.lib = %TRANSIS%\Lib;.;%ROOT%\lib
*.exe = %TRANSIS%\Bin;.;
*.dll = %TRANSIS%\Bin;.;
*.obj = %ROOT%\lib
*.res = %ROOT%\lib
*.hlp = %BIN%;%ROOT%\Accessory\bin
*.dll = %BIN%;%ROOT%\Accessory\bin
*.exe = %BIN%;%ROOT%\Accessory\bin
*.tp? = %ROOT%\Accessory\template\win
*.txs = %ROOT%\Accessory\template\win
*.stt = %ROOT%\Accessory\template\win
*.lib = %ROOT%\Accessory\lib
*.obj = %ROOT%\Accessory\lib
*.res = %ROOT%\Accessory\lib
*.dll = %ROOT%\Accessory\bin
*.*   = %ROOT%\Accessory\images; %ROOT%\Accessory\resources; %ROOT%\Accessory\libsrc\win; %ROOT%\Accessory\template\win
