

   MEMBER('TRANSIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('TRANSISM002.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Check_Branch_Set     PROCEDURE                             ! Declare Procedure
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Branches.Open()
     .
     Access:Branches.UseFile()
    ! Check Branch is Set
    Failed_#            = FALSE
    BRA:BID             = GETINI('Browse_Setup', 'Branch_ID', 0, GLO:Global_INI)
    IF BRA:BID ~= 0
       IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
          GLO:BranchID  = BRA:BID
       ELSE
          Failed_#      = TRUE
          MESSAGE('Please use the My Settings option to select a Branch before doing any work on the system.', 'My Settings (Global_Assign)', ICON:Hand)
       .
    ELSE
       Failed_#         = TRUE
       MESSAGE('Please use the My Settings option under the Setup pull down menu to select a Branch before doing any work on the system.', 'My Settings (Global_Assign)', ICON:Hand)
    .
    IF Failed_# = TRUE
       START(MySettings)
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:Branches.Close()
    Exit
