

   MEMBER('TRANSIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('TRANSISM001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('TRANSISM002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('TRANSISM003.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Splash
!!! </summary>
AboutWindow PROCEDURE 

Build_No             ULONG                                 ! 
Build_Date           LONG                                  ! 
Build_Time           LONG                                  ! 
Window               WINDOW('About...'),AT(,,227,124),FONT('MS Sans Serif',8,,FONT:regular),NOFRAME,CENTER,HLP('~AboutWindow'), |
  PALETTE(256)
                       IMAGE('InfoSolutions_Logo.bmp'),AT(5,4),USE(?Image),CENTERED
                       PROMPT('Build No.'),AT(6,40,87,10),USE(?Prompt_Build)
                       STRING('Copyright 2004 - '),AT(111,40,110,10),USE(?CopyrightDate),TRN
                       PROMPT('Build_Date'),AT(6,54,87,10),USE(?Prompt_BDate)
                       STRING('Produced by InfoSolutions'),AT(111,54,110,10),USE(?Developer),TRN
                       PROMPT('Build_Time'),AT(6,68,87,10),USE(?Prompt_BTime)
                       STRING('for FBN Transport'),AT(111,68,110,10),USE(?AboutHeading),TRN
                       PANEL,AT(5,82,215,4),USE(?Panel:6),BEVEL(1,0,1536)
                       PROMPT('Warning:  This computer program is protected by copyright law and international' & |
  ' treaties.  Unauthorised reproduction or distribution of this program, or any portio' & |
  'n of it, may result in severe civil and criminal penalties. Known violators will be ' & |
  'prosecuted to the maximum extent possible under the law.'),AT(5,88,163,34),USE(?PromptCopyright), |
  FONT('Small Fonts',6)
                       BUTTON('&Close'),AT(175,104,45,14),USE(?Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AboutWindow')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
      ?Prompt_Build{Prop:Text} = 'Build No.: ' & ?Prompt_Build{Prop:Text}
      ?Prompt_BDate{Prop:Text} = 'Build Date: ' & ?Prompt_BDate{Prop:Text}
      ?Prompt_BTime{Prop:Text} = 'Build Time: ' & ?Prompt_BTime{Prop:Text}

      !Build_No        = ?Build_No{Prop:Text}
      !Build_Date      = DEFORMAT(?Build_Date{Prop:Text}, @d6)
      !Build_Time      = DEFORMAT(?Build_Time{Prop:Text}, @t7)

  
!      Str_1"          = EQUATE:BuildTime
!      Str_"           = ''
!      LOOP
!         Str_"        = CLIP(Str_") & Get_1st_Element_From_Delim_Str(Str_1", ' ', TRUE)
!         IF CLIP(Str_1") = ''
!            BREAK
!      .  .
  
!      Build_Time    = DEFORMAT(Str_", @t3)
  
      ! EQUATE:BuildDate   EQUATE('06/02/2005')
      ! EQUATE:BuildTime   EQUATE(' 8:19 PM')
      text_"                      = ?CopyrightDate{PROP:Text}
      ?CopyrightDate{PROP:Text}   = CLIP(text_") & ' ' & YEAR(TODAY())
  INIMgr.Fetch('AboutWindow',Window)                       ! Restore window settings from non-volatile store
  TARGET{Prop:Timer} = 0                                   ! Close window on timer event, so configure timer
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Application Icon
  !--------------------------------------------------------------------------
    SYSTEM{PROP:Icon} = '~dip_blue.ico'
    IF CLIP(0{PROP:Icon}) = ''
        0{PROP:Icon} = '~dip_blue.ico'
    END
  !--------------------------------------------------------------------------
  ! Tinman Application Icon
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('AboutWindow',Window)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:LoseFocus
        POST(Event:CloseWindow)                            ! Splash window will close when focus is lost
    OF Event:Timer
      POST(Event:CloseWindow)                              ! Splash window will close on event timer
    ELSE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Frame
!!! </summary>
Main PROCEDURE 

LOC:Orig_Title       STRING(35)                            ! 
LOC:Timer_It         SHORT                                 ! 
LOC:Logged_In        BYTE                                  ! 
LOC:Closing          BYTE                                  ! 
LOC:Test_Database    BYTE                                  ! 
AppFrame             APPLICATION('TransIS'),AT(0,0,525,324),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTERED,HVSCROLL, |
  ICON('dip_blue.ico'),MAX,HLP('~Main'),PALETTE(256),SYSTEM,TIMER(100),WALLPAPER('fbn_logo_small2.gif'),IMM
                       MENUBAR,USE(?Menubar)
                         MENU('&File'),USE(?File)
                           ITEM('&Login User'),USE(?FileLoginUser)
                           ITEM('L&ogout User'),USE(?FileLogoutUser)
                           ITEM,USE(?SEPARATOR1),SEPARATOR
                           ITEM('&My Settings'),USE(?SetupMySettings)
                           ITEM,USE(?SEPARATOR2),SEPARATOR
                           ITEM('Database Connection'),USE(?UtilitiesDatabaseConnection)
                           ITEM,USE(?SEPARATOR3),SEPARATOR
                           ITEM('&Print Setup ...'),USE(?PrintSetup),MSG('Setup printer'),STD(STD:PrintSetup)
                           ITEM,USE(?SEPARATOR4),SEPARATOR
                           ITEM('Date'),USE(?FileDate)
                           ITEM,USE(?SEPARATOR5),SEPARATOR
                           ITEM('E&xit'),USE(?Abort),STD(STD:Close)
                         END
                         MENU('&Edit'),USE(?Edit)
                           ITEM('C&ut'),USE(?Cut),STD(STD:Cut)
                           ITEM('&Copy'),USE(?Copy),MSG('Copy item to Windows Clipboard'),STD(STD:Copy)
                           ITEM('&Paste'),USE(?Paste),MSG('Paste contents of Windows Clipboard'),STD(STD:Paste)
                         END
                         MENU('&Browse'),USE(?Browse)
                           MENU('Debtors'),USE(?BrowseDebtors)
                             ITEM('Deliveries to Release'),USE(?BrowseOperationsDeliveriestoRelease)
                             ITEM('Release DI''s (Bulk)'),USE(?OperationsReleaseDIsOperations)
                             ITEM('Bulk &POD Capture'),USE(?UtilitiesBulkPODCapture),MSG('Capture of Trip Sheet return info')
                             ITEM,USE(?SEPARATOR6),SEPARATOR
                             ITEM('Clients'),USE(?BrowseClients)
                             ITEM,USE(?SEPARATOR7),SEPARATOR
                             ITEM('Invoices'),USE(?Browse_Invoice)
                             ITEM('Payments'),USE(?BrowseClientPayments)
                             ITEM,USE(?SEPARATOR8),SEPARATOR
                             ITEM('Credit Notes'),USE(?BrowseCreditNotes)
                             ITEM('Journals'),USE(?BrowseJournals)
                             ITEM,USE(?SEPARATOR9),SEPARATOR
                             ITEM('Statements'),USE(?BrowseStatements)
                             ITEM('Statement Run (Generate)'),USE(?UtilitiesStatementRun)
                             ITEM('Statement Run History'),USE(?ReportsAccountsStatementHistory)
                             ITEM,USE(?SEPARATOR10),SEPARATOR
                             ITEM('Rates Batch Update'),USE(?BrowseDebtorsRatesBatchUpdate)
                             ITEM('Rates Batch Update History'),USE(?BrowseDebtorsRatesBatchUpdateHistory)
                             ITEM,USE(?SEPARATOR11),SEPARATOR
                             ITEM('Fuel Costs'),USE(?BrowseDebtorsFuelCosts)
                             ITEM('Fuel Base Rates'),USE(?BrowseDebtorsFuelBaseRates),HIDE
                             ITEM,USE(?SEPARATOR12),SEPARATOR
                             MENU('&Reports'),USE(?ReportsAccounts)
                               ITEM('Invoices Listing'),USE(?BrowseDebtorsReportsInvoicesListing)
                               ITEM,USE(?SEPARATOR13),SEPARATOR
                               ITEM('Invoices (for Clients)'),USE(?ReportsInvoicesall)
                               ITEM,USE(?SEPARATOR14),SEPARATOR
                               ITEM('Reciepts'),USE(?ReportsAccountsReciepts)
                               ITEM('Reciepts with Headings'),USE(?BrowseDebtorsReportsRecieptswithHeadings)
                               ITEM('Advance Payments (un-allocated amts.)'),USE(?BrowseDebtorsReportsAdvancePayments)
                               ITEM,USE(?SEPARATOR15),SEPARATOR
                               ITEM('&Age Analysis'),USE(?ReportsAccountsDebitorAgeAnalysis)
                               ITEM('Credit &Limit Exceeded'),USE(?BrowseDebtorsReportsDebtorsOverCreditLimit)
                               ITEM,USE(?SEPARATOR16),SEPARATOR
                               ITEM('&Bad Debt Listing'),USE(?BrowseDebtorsReportsBadDebtListing)
                               ITEM,USE(?SEPARATOR17),SEPARATOR
                               ITEM('Debtor Listing'),USE(?ReportsDebtorListing)
                               ITEM,USE(?SEPARATOR18),SEPARATOR
                               ITEM('DI Rates Check Report'),USE(?ReportsDIRatesCheckReport)
                               ITEM('Rates Report'),USE(?ReportsRatesReport)
                               ITEM,USE(?SEPARATOR69),SEPARATOR
                               ITEM('Fuel Surcharges'),USE(?FuelSurchargersReport)
                             END
                           END
                           MENU('Creditors'),USE(?BrowseCreditors)
                             ITEM('Transporters'),USE(?BrowseTransporter)
                             ITEM,USE(?SEPARATOR19),SEPARATOR
                             ITEM('Transporter Invoices'),USE(?BrowseCreditorsTransporterInvoices)
                             ITEM('Transporter Payments'),USE(?BrowseTransporterPayments)
                             ITEM,USE(?SEPARATOR20),SEPARATOR
                             ITEM('Remittance Advice'),USE(?BrowseCreditorsReportsRemittanceAdvice)
                             ITEM('Remittance Advice Run (Generate)'),USE(?BrowseCreditorsReportsRemittance)
                             ITEM('Remittance Advice Run History'),USE(?BrowseCreditorsReportsRemittanceAdviceRunHistor)
                             ITEM,USE(?SEPARATOR21),SEPARATOR
                             MENU('Reports'),USE(?BrowseCreditorsReports)
                               ITEM('Manifests'),USE(?BrowseCreditorsManifests),MSG('(Print_Creditor_Manifests)')
                               ITEM('Invoices (and Credit Notes)'),USE(?BrowseCreditorsCreditorInvoicesCreditNotes),MSG('(Print_Cre' & |
  'ditor_Invoices)')
                               ITEM('Invoices with Creditor Details'),USE(?BrowseCreditorsReportsInvoiceswithCreditorDetai), |
  MSG('(Print_Transporter_Invoices_VAT)')
                               ITEM('Invoices for Vehicle Compositions'),USE(?BrowseCreditorsReportsInvoicesforVehicleComposi), |
  MSG('(Print_Transporter_Invoices)')
                               ITEM,USE(?SEPARATOR22),SEPARATOR
                               ITEM('Payments'),USE(?BrowseCreditorsCreditorPayments)
                               ITEM('Payments (Simple)'),USE(?BrowseCreditorsReportsPaymentsSimple)
                               ITEM,USE(?SEPARATOR23),SEPARATOR
                               ITEM('Creditors Age Analysis'),USE(?BrowseCreditorsCreditorsAgeAnalysis)
                               ITEM,USE(?SEPARATOR24),SEPARATOR
                               ITEM('Transporter Details'),USE(?BrowseCreditorsReportsTransporterDetails)
                               ITEM,USE(?SEPARATOR68),SEPARATOR
                               ITEM('Sales Rep Reports'),USE(?SalesRepReports)
                             END
                           END
                           ITEM,USE(?SEPARATOR25),SEPARATOR
                           ITEM('Floors'),USE(?BrowseFloors)
                           ITEM,USE(?SEPARATOR26),SEPARATOR
                           MENU('Management Reports'),USE(?BrowseManagement)
                             ITEM('Turnover / &Management Profit'),USE(?BrowseDebtorsReportsTurnover)
                             ITEM('Turnover / Management &Profit Summary'),USE(?BrowseManagementReportsTurnoverManagementProfit)
                             ITEM,USE(?SEPARATOR27),SEPARATOR
                             ITEM('Manifest Details'),USE(?BrowseManagementReportsManifest)
                             ITEM,USE(?SEPARATOR28),SEPARATOR
                             ITEM('&Invoice Summary'),USE(?BrowseDebtorsReportsInvoiceSummary)
                             ITEM,USE(?SEPARATOR29),SEPARATOR
                             ITEM('&VAT Summary'),USE(?BrowseManagementVATSummary)
                             ITEM,USE(?SEPARATOR30),SEPARATOR
                             ITEM('Client &Delivery'),USE(?BrowseManagementClientDeliveryReport)
                             ITEM,USE(?SEPARATOR31),SEPARATOR
                             ITEM('&Undelivered POD'),USE(?BrowseManagementUndeliveredPODs)
                             ITEM('&Shortages && Damages'),USE(?BrowseManagementReportsShortagesDamages)
                             ITEM,USE(?SEPARATOR32),SEPARATOR
                             ITEM('DI Statistics'),USE(?UtilitiesDIStatistics)
                             ITEM('Client Activity'),USE(?BrowseManagementReportsClientActivity)
                             ITEM('Client Inactive'),USE(?BrowseManagementReportsClientInactive)
                             ITEM('Clients Over Time'),USE(?ITEM1)
                             ITEM,USE(?SEPARATOR33),SEPARATOR
                             ITEM('Web Users'),USE(?BrowseManagementReportsWebUsers)
                           END
                         END
                         MENU('&Operations'),USE(?Setup_Operations)
                           ITEM('Deliveries'),USE(?BrowseDeliveries)
                           ITEM('Multi Part Deliveries (DI creation)'),USE(?OperationsDeliveryCombinations)
                           ITEM('Multi Part Deliveries Browse'),USE(?OperationsDeliveryCompositions)
                           ITEM,USE(?SEPARATOR34),SEPARATOR
                           ITEM('Manifests'),USE(?BrowseManifest)
                           ITEM('Trip Sheets'),USE(?BrowseTripSheets)
                           ITEM,USE(?SEPARATOR35),SEPARATOR
                           ITEM('Journeys'),USE(?BrowseJourneys)
                           ITEM,USE(?SEPARATOR36),SEPARATOR
                           ITEM('Commodities'),USE(?BrowseCommodities)
                           ITEM('Container Types'),USE(?BrowseContainerTypes)
                           ITEM('Packaging Types'),USE(?BrowsePackagingTypes)
                           ITEM,USE(?SEPARATOR37),SEPARATOR
                           ITEM('Container Operators'),USE(?BrowseContainerOperators)
                           ITEM,USE(?SEPARATOR38),SEPARATOR
                           ITEM('Addresses'),USE(?BrowseAddresses)
                           ITEM('COD / Pre Paid Addresses'),USE(?SetupCODPrePaidAddresses)
                           ITEM('Countries'),USE(?BrowseCountries)
                           ITEM('Suburbs'),USE(?BrowseSuburbs)
                           ITEM,USE(?SEPARATOR39),SEPARATOR
                           ITEM('Drivers'),USE(?SetupDrivers)
                         END
                         MENU('&Setup'),USE(?Setup)
                           ITEM('Branches'),USE(?BrowseBranches)
                           ITEM,USE(?SEPARATOR40),SEPARATOR
                           ITEM('Delivery Statuses'),USE(?SetupDeliveryStatuses)
                           ITEM('Delivery Tracking Email Setup'),USE(?DeliveryTrackingEmailSetup)
                           ITEM('Load Types'),USE(?BrowseLoadTypes)
                           ITEM('Service Requirement Types'),USE(?BrowseServiceRequirements)
                           ITEM('Client Rate Types'),USE(?ITEM_ClientRateTypes)
                           ITEM,USE(?SEPARATOR41),SEPARATOR
                           ITEM('Accountants'),USE(?BrowseAccountants)
                           ITEM('Sales Reps.'),USE(?BrowseSalesReps)
                           ITEM,USE(?SEPARATOR42),SEPARATOR
                           ITEM('Truck && Trailer'),USE(?BrowseTruckTrailer)
                           ITEM('Vehicle Composition'),USE(?BrowseVehicleComposition)
                           ITEM('Vessels'),USE(?BrowseVessels)
                           ITEM,USE(?SEPARATOR43),SEPARATOR
                           ITEM('&Users'),USE(?SetupUsers)
                           ITEM('User &Groups'),USE(?SetupUserGroups)
                           ITEM('&Web Users'),USE(?SetupWebUsers)
                           ITEM,USE(?SEPARATOR44),SEPARATOR
                           ITEM('Application Sections'),USE(?SetupApplicationSections)
                           ITEM,USE(?SEPARATOR45),SEPARATOR
                           ITEM('Scheduled Runs'),USE(?SetupScheduledRuns)
                           ITEM('Public Holidays'),USE(?SetupPublicHolidays)
                           ITEM,USE(?SEPARATOR46),SEPARATOR
                           ITEM('Replication ID Control'),USE(?SetupReplicationIDControl)
                           ITEM('Replication Table IDs'),USE(?SetupReplicationTableIDs)
                           ITEM,USE(?SEPARATOR47),SEPARATOR
                           ITEM('MS Word Report Templates'),USE(?SetupItem208)
                           ITEM,USE(?SEPARATOR48),SEPARATOR
                           ITEM('&Setup'),USE(?SetupSetup)
                           ITEM('&Settings'),USE(?SetupSettings)
                         END
                         MENU('&Utilities'),USE(?Utilities)
                           ITEM('&Reminders'),USE(?UtilitiesReminders)
                           ITEM,USE(?SEPARATOR49),SEPARATOR
                           ITEM('Rate Checker'),USE(?UtilitiesRateChecker)
                           ITEM,USE(?SEPARATOR50),SEPARATOR
                           MENU('&Data Utilities'),USE(?UtilitiesFileUtilities)
                             MENU('Clients / Debtors'),USE(?UtilitiesFileUtilitiesClientsDebtors)
                               ITEM('Update All Client Balances'),USE(?UtilitiesDataUtilitiesClientsDebtorsUpdateAllCl)
                               ITEM('Update Client Search'),USE(?UtilitiesDataUtilitiesClientsDebtorsUpdateClien)
                               ITEM,USE(?SEPARATOR51),SEPARATOR
                               ITEM('Check / Reset Delivery Manifested / Delivered (On Tripsheet)'),USE(?UtilitiesCheckResetDeliveryManifested)
                               ITEM('Check / Update Invoice Statuses'),USE(?UtilitiesCheckUpdateInvoiceStatuses)
                               ITEM,USE(?SEPARATOR52),SEPARATOR
                               ITEM('Reset Status Up To Date on Client Invoices'),USE(?UtilitiesFileUtilitiesResetStatusUpToDateonInvo)
                               ITEM('Reset Status Up To Date on Client Payment Allocations'),USE(?UtilitiesFileUtilitiesResetStatusUpToDateonClie)
                               ITEM,USE(?SEPARATOR53),SEPARATOR
                               ITEM('Reset Status Up To Date on Client Payments'),USE(?UtilitiesFileUtilitiesResetStatusUpToDateonClie2)
                               ITEM,USE(?SEPARATOR54),SEPARATOR
                               ITEM('Check DI''s Invoiced'),USE(?UtilitiesDataUtilitiesClientsDebtorsCheckDIsInv),MSG('Check all ' & |
  'DI''s that are Manifested have Invoices')
                               ITEM,USE(?SEPARATOR55),SEPARATOR
                               ITEM('Delete Bad DI''s'),USE(?UtilitiesDataUtilitiesClientsDebtorsDeleteBadDI),MSG('Where DI h' & |
  'as no Client, Branch, Journey or Charge')
                               ITEM('De-Duplicate Email Addresses'),USE(?ITEM_DeDupEmail),MSG('Always use with caution')
                             END
                             MENU('Transporters / Creditors'),USE(?UtilitiesFileUtilitiesTransportersCreditors)
                               ITEM('Check / Update Invoice Statuses'),USE(?UtilitiesFileUtilitiesTransportersCreditorsChec)
                               ITEM,USE(?SEPARATOR56),SEPARATOR
                               ITEM('Reset Status Up To Date on Creditor Invoices'),USE(?UtilitiesFileUtilitiesResetStatusUpToDateonCred)
                               ITEM('Reset Status Up To Date on Transporter Payment Allocations'),USE(?UtilitiesFileUtilitiesResetStatusUpToDateonTran)
                             END
                             ITEM,USE(?SEPARATOR57),SEPARATOR
                             ITEM('Remove Orphaned Manifest Delivery Items'),USE(?UtilitiesOrphaned)
                             ITEM('Remove Orphaned Manifest Loads'),USE(?UtilitiesFileUtilitiesRemoveOrphanedManifestLoa)
                             ITEM('Remove Orphaned Trip Sheet Delivery Items'),USE(?UtilitiesRemoveOrphanedTripSheetDeliveryItems)
                             ITEM,USE(?SEPARATOR58),SEPARATOR
                             ITEM('Check Invoices match DI''s'),USE(?UtilitiesFileUtilitiesCheckInvoicesmatchDIs),MSG('Checks Inv' & |
  'oice DI No. matches Deliveries DI No. (should always be the case)')
                             ITEM,USE(?SEPARATOR59),SEPARATOR
                             ITEM('Remove Browse Layouts Table'),USE(?UtilitiesFileUtilitiesRemoveBrowseLayoutsTable)
                             ITEM,USE(?SEPARATOR60),SEPARATOR
                             MENU('Special (use with care)'),USE(?UtilitiesFileUtilitiesSpecialDONOTUSE)
                               ITEM('Debtor Invoices - Set Invoiced Date to Manifest Date'),USE(?UtilitiesFileUtilitiesSpecialSetInvoicedDatetoD)
                               ITEM('Transporter Invoices - Set Invoiced Date to Manifest Date'),USE(?UtilitiesDataUtilitiesSpecialusewithcareTranspo)
                               ITEM,USE(?SEPARATOR61),SEPARATOR
                               MENU('Others - DO NOT USE'),USE(?UtilitiesFileUtilitiesSpecialDONOTUSE2)
                                 ITEM('Generate all Transporter Invoices for Manifests'),USE(?UtilitiesFileUtilitiesSpecialDONOTUSEGenerateal)
                               END
                             END
                           END
                           ITEM,USE(?SEPARATOR62),SEPARATOR
                           ITEM('Browse Audit'),USE(?UtilitiesBrowseAudit)
                           ITEM('Browse Audit Profit Report'),USE(?UtilitiesDataUtilitiesAuditProfitReport)
                           ITEM('Browse List Manager'),USE(?UtilitiesBrowseListManager)
                           ITEM,USE(?SEPARATOR63),SEPARATOR
                           MENU('Reference Lists'),USE(?UtilitiesReferenceLists)
                             ITEM('Statement Run Descriptions'),USE(?UtilitiesReferenceListsStatementRunDescriptions)
                           END
                           ITEM,USE(?UtilitiesSepBelowAudit),SEPARATOR
                           MENU('&Direct '),USE(?Direct)
                             ITEM('Browse Address Contacts'),USE(?BrowseAddressContacts)
                             ITEM('Browse Reminders'),USE(?DirectBrowseReminders)
                           END
                           ITEM,USE(?SEPARATOR64),SEPARATOR
                           ITEM('Prints Configuration'),USE(?UtilitiesPrintsConfiguration)
                         END
                         MENU('&Reports'),USE(?Reports)
                           ITEM('Status Report'),USE(?ReportsStatusReport)
                           ITEM,USE(?SEPARATOR65),SEPARATOR
                           ITEM('What''s Where'),USE(?ReportsWhatsWhere)
                           ITEM,USE(?SEPARATOR66),SEPARATOR
                           MENU('&Setup Listings'),USE(?ReportsSetupListings)
                             ITEM('Journeys Listing'),USE(?ReportsSetupListingsJourneysListing)
                             ITEM('Load Types Listing'),USE(?ReportsSetupListingsLoadTypesListing)
                           END
                           ITEM,USE(?SEPARATOR67),HIDE,SEPARATOR
                           MENU('Report All Entries'),USE(?ReportsReportAllEntries),HIDE
                             ITEM('Delivery Notes (all)'),USE(?ReportsDeliveryNotesall)
                             ITEM('Manifests (all)'),USE(?ReportsManifestsall)
                           END
                         END
                         MENU('&Window'),USE(?Window),STD(STD:WindowList)
                           ITEM('&Cascade'),USE(?Cascade),MSG('Stack all open windows'),STD(STD:CascadeWindow)
                           ITEM('Tile &Horizontal'),USE(?TileHorizontal),MSG('Make all open windows visible'),STD(STD:TileHorizontal)
                           ITEM('Tile &Vertical'),USE(?TileVertical),MSG('Make all open windows visible'),STD(STD:TileVertical)
                           ITEM('&Arrange Icons'),USE(?Arrange),MSG('Align all window icons'),STD(STD:ArrangeIcons)
                         END
                         MENU('&Help'),USE(?Help)
                           ITEM('&Contents'),USE(?Helpindex),MSG('View the contents of the help file'),STD(STD:HelpIndex)
                           ITEM('&Search for Help On...'),USE(?HelpSearch),MSG('Search for help on a subject'),STD(STD:HelpSearch)
                           ITEM('&How to Use Help'),USE(?HelpOnHelp),MSG('How to use Windows Help'),STD(STD:HelpOnHelp)
                           ITEM('&About...'),USE(?About)
                         END
                       END
                       TOOLBAR,AT(0,0,525,20),USE(?Tools)
                         BUTTON('Manifests'),AT(67,2,63,16),USE(?Button_Manifest),LEFT,ICON('FilesBound.ico'),FLAT, |
  TIP('List Manifests')
                         BUTTON('Trip Sheets'),AT(133,2,63,16),USE(?Button_TripSheets),LEFT,ICON('Van.ico'),FLAT,TIP('List Trip Sheets')
                         BUTTON('Floors'),AT(200,2,50,16),USE(?Button_Floors),LEFT,ICON('BoxesN.ico'),FLAT
                         BUTTON('Rates'),AT(249,2,50,16),USE(?Button_Rates),LEFT,ICON('PeopleS.ico'),FLAT,TIP('Clients Rates')
                         BUTTON('Transporters'),AT(303,2,66,16),USE(?Button_Transporters),LEFT,ICON('Warn.ico'),FLAT
                         BUTTON('DI Search'),AT(428,2,62,16),USE(?Button_DI_Search),LEFT,ICON('Web.ico'),FLAT
                         BUTTON('Enter DI'),AT(2,2,61,16),USE(?Button_Deliveries:2),LEFT,ICON('VanPC.ico'),FLAT,TIP('Capture a new DI')
                         BUTTON('Arrivals'),AT(373,2,51,16),USE(?Button_Arrivals),LEFT,ICON('1396494565_22999.ico'), |
  FLAT
                         BUTTON('Man Wizard'),AT(487,2,62,16),USE(?Button_ManWiz),LEFT,ICON(ICON:Clarion),FLAT
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
User_Login_Logout           ROUTINE
    IF GLO:AccessLevel > 9
       UNHIDE(?UtilitiesBrowseAudit)
       UNHIDE(?UtilitiesSepBelowAudit)
    ELSE
       HIDE(?UtilitiesBrowseAudit)
       HIDE(?UtilitiesSepBelowAudit)
    .

    IF CLIP(GLO:Login) = ''
       AppFrame{PROP:Text}  = CLIP(LOC:Orig_Title) & ' - <No User>'
    ELSE
       AppFrame{PROP:Text}  = CLIP(LOC:Orig_Title) & ' - ' & CLIP(GLO:Login)
    .
    EXIT
Apply_User_Menus           ROUTINE
    ! (ULONG, STRING, STRING, <STRING>, BYTE=0, <*BYTE>),LONG
    ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned)

    ! Returns
    !   0   - Allow (default)
    !   1   - Disable
    !   2   - Hide
    !   100 - Allow                 - Default action
    !   101 - Disallow              - Default action

    CASE Get_User_Access(GLO:UID, 'Menus', 'Main', 'Setup Menu', 0)
    OF 0 OROF 100
       ENABLE(?Setup)
       ENABLE(?Direct)
       ENABLE(?Utilities)
       UNHIDE(?Setup)
       UNHIDE(?Direct)
       UNHIDE(?Utilities)
    OF 2
       HIDE(?Setup)
       HIDE(?Direct)
       HIDE(?Utilities)
    ELSE
       DISABLE(?Setup)
       DISABLE(?Direct)
       DISABLE(?Utilities)
    .

    CASE Get_User_Access(GLO:UID, 'Menus', 'Main', 'DI Release', 1)     ! Disable by default
    OF 0 OROF 100
       ENABLE(?BrowseOperationsDeliveriestoRelease)
       ENABLE(?OperationsReleaseDIsOperations)
    OF 2
       HIDE(?BrowseOperationsDeliveriestoRelease)
       HIDE(?OperationsReleaseDIsOperations)
    ELSE
       DISABLE(?BrowseOperationsDeliveriestoRelease)
       DISABLE(?OperationsReleaseDIsOperations)
	.
	
    EXIT
Menu::Menubar ROUTINE                                      ! Code for menu items on ?Menubar
Menu::File ROUTINE                                         ! Code for menu items on ?File
  CASE ACCEPTED()
  OF ?FileLoginUser
    START(UserLogin, 25000)
  OF ?FileLogoutUser
    UserLogout()
        LOC:Logged_In   = FALSE
        AppFrame{PROP:Text}   = CLIP(LOC:Orig_Title)
    
    !    IF GETINI('Setup', 'StartupLogin', '1', GLO:Local_INI) > 0
           UserLogin()
           IF GLO:UID > 0
              !DO Check_User_Options
    
              LOC:Logged_In         = TRUE
              AppFrame{PROP:Text}   = CLIP(LOC:Orig_Title) & ' - ' & CLIP(GLO:Login)
           ELSE
              POST(EVENT:CloseWindow)
           .
    !    ELSE
    !       LOC:Logged_In            = TRUE
    !    .
    
        IF LOC:Logged_In = FALSE
           POST(EVENT:CloseWindow)
        ELSE
    
           Check_Branch_Set()
    
           Check_ScheduledRuns()
    
           Check_Replication()
    
           DO Apply_User_Menus
    
        .
  OF ?SetupMySettings
    START(MySettings, 25000)
  OF ?UtilitiesDatabaseConnection
    START(Set_Connection, 25000)
  OF ?FileDate
    START(Date_Test, 25000)
  END
Menu::Edit ROUTINE                                         ! Code for menu items on ?Edit
Menu::Browse ROUTINE                                       ! Code for menu items on ?Browse
  CASE ACCEPTED()
  OF ?BrowseFloors
    START(Browse_Floors_h, 25000, '')
  END
Menu::BrowseDebtors ROUTINE                                ! Code for menu items on ?BrowseDebtors
  CASE ACCEPTED()
  OF ?BrowseOperationsDeliveriestoRelease
    START(Browse_Deliveries_h, 25000, 'R', '')
  OF ?OperationsReleaseDIsOperations
    START(Operations_DI_Release, 25000)
  OF ?UtilitiesBulkPODCapture
    START(TripSheet_BulkPOD_h, 25000)
  OF ?BrowseClients
    START(Browse_Clients_h, 25000, '0')
  OF ?Browse_Invoice
    START(Browse_Invoice_h, 25000)
  OF ?BrowseClientPayments
    START(Browse_Client_Payments, 25000)
  OF ?BrowseCreditNotes
    START(Create_Credit_Note, 25000)
  OF ?BrowseJournals
    START(Create_Journal, 25000)
  OF ?BrowseStatements
    START(Browse_Statements, 25000)
  OF ?UtilitiesStatementRun
    START(Gen_Statements, 25000)
  OF ?ReportsAccountsStatementHistory
    START(Browse_StatementRuns, 25000)
  OF ?BrowseDebtorsRatesBatchUpdate
    START(Clients_Rates_Mod, 25000)
  OF ?BrowseDebtorsRatesBatchUpdateHistory
    START(Browse_Rates_Update, 25000)
  OF ?BrowseDebtorsFuelCosts
    START(Browse_FuelCost, 25000, (''))
  OF ?BrowseDebtorsFuelBaseRates
    START(Browse_FuelBaseRate, 25000)
  END
Menu::ReportsAccounts ROUTINE                              ! Code for menu items on ?ReportsAccounts
  CASE ACCEPTED()
  OF ?BrowseDebtorsReportsInvoicesListing
    START(Print_Invoices_List, 25000)
  OF ?ReportsInvoicesall
    START(Print_Invoices_Window, 25000)
  OF ?ReportsAccountsReciepts
    START(Print_Reciepts, 25000)
  OF ?BrowseDebtorsReportsRecieptswithHeadings
    START(Print_Reciepts_with_Headings, 25000)
  OF ?BrowseDebtorsReportsAdvancePayments
    START(Print_Client_Advance_Payments, 25000)
  OF ?ReportsAccountsDebitorAgeAnalysis
    START(Print_Debtor_Age_Analysis_Window_h, 25000, '')
  OF ?BrowseDebtorsReportsDebtorsOverCreditLimit
    START(Print_Debtor_Age_Analysis_Window_h, 25000, '1')
  OF ?BrowseDebtorsReportsBadDebtListing
    START(Print_Client_BadDebts, 25000)
  OF ?ReportsDebtorListing
    START(Print_Debtor_Listing, 25000)
  OF ?ReportsDIRatesCheckReport
    START(Print_Deliveries_Rates_Check, 25000)
  OF ?ReportsRatesReport
    START(Print_Rates_h, 25000, '')
  OF ?FuelSurchargersReport
    START(Start_FuelSurchargeReport, 25000)
  END
Menu::BrowseCreditors ROUTINE                              ! Code for menu items on ?BrowseCreditors
  CASE ACCEPTED()
  OF ?BrowseTransporter
    START(Browse_Transporter, 25000)
  OF ?BrowseCreditorsTransporterInvoices
    START(Browse_Transporter_Invoices_h, 25000)
  OF ?BrowseTransporterPayments
    START(Browse_Transporter_Payments, 25000)
  OF ?BrowseCreditorsReportsRemittanceAdvice
    START(Browse_Remittance_Advice, 25000)
  OF ?BrowseCreditorsReportsRemittance
    START(Gen_Remittances, 25000)
  OF ?BrowseCreditorsReportsRemittanceAdviceRunHistor
    START(Browse_Remittance_Runs, 25000)
  END
Menu::BrowseCreditorsReports ROUTINE                       ! Code for menu items on ?BrowseCreditorsReports
  CASE ACCEPTED()
  OF ?BrowseCreditorsManifests
    START(Print_Creditor_Manifests, 25000)
  OF ?BrowseCreditorsCreditorInvoicesCreditNotes
    START(Print_Creditor_Invoices, 25000)
  OF ?BrowseCreditorsReportsInvoiceswithCreditorDetai
    START(Print_Transporter_Invoices_VAT, 25000)
  OF ?BrowseCreditorsReportsInvoicesforVehicleComposi
    START(Print_Transporter_Invoices, 25000)
  OF ?BrowseCreditorsCreditorPayments
    START(Print_Creditor_Reciepts, 25000)
  OF ?BrowseCreditorsReportsPaymentsSimple
    START(Print_Creditor_Reciepts_Simple, 25000)
  OF ?BrowseCreditorsCreditorsAgeAnalysis
    START(Print_Creditor_Age_Analysis_Window, 25000)
  OF ?BrowseCreditorsReportsTransporterDetails
    START(Print_Transporters, 25000)
  OF ?SalesRepReports
    START(Start_Print_SalesRep, 25000)
  END
Menu::BrowseManagement ROUTINE                             ! Code for menu items on ?BrowseManagement
  CASE ACCEPTED()
  OF ?BrowseDebtorsReportsTurnover
    START(Print_Turnover, 25000)
  OF ?BrowseManagementReportsTurnoverManagementProfit
    START(Print_ManagementProfit_Summary, 25000)
  OF ?BrowseManagementReportsManifest
    START(Print_Manifests, 25000)
  OF ?BrowseDebtorsReportsInvoiceSummary
    START(Print_Invoice_Summary, 25000)
  OF ?BrowseManagementVATSummary
    START(Print_VAT_Summary, 25000)
  OF ?BrowseManagementClientDeliveryReport
    START(Print_Client_DeliveryReport, 25000)
  OF ?BrowseManagementUndeliveredPODs
    START(Print_UndeliveredPODs, 25000)
  OF ?BrowseManagementReportsShortagesDamages
    START(Browse_Shortages_Damages, 25000)
  OF ?UtilitiesDIStatistics
    START(DIs_Processed, 25000)
  OF ?BrowseManagementReportsClientActivity
    START(Window_Client_Activity, 25000)
  OF ?BrowseManagementReportsClientInactive
    START(Print_Debtor_Inactive, 25000)
  OF ?ITEM1
    START(Clients_OverTime, 25000)
  OF ?BrowseManagementReportsWebUsers
    START(Print_WebUsers, 25000)
  END
Menu::Setup_Operations ROUTINE                             ! Code for menu items on ?Setup_Operations
  CASE ACCEPTED()
  OF ?BrowseDeliveries
    START(Browse_Deliveries_h, 25000, '', '')
  OF ?OperationsDeliveryCombinations
    START(Update_Delivery_h, 25000, '2')
  OF ?OperationsDeliveryCompositions
    START(Browse_DeliveryCompositions, 25000)
  OF ?BrowseManifest
    START(Browse_Manifests_h, 25000, '')
  OF ?BrowseTripSheets
    START(Browse_TripSheets, 25000)
  OF ?BrowseJourneys
    START(Browse_Journeys_h, 25000)
  OF ?BrowseCommodities
    START(Browse_Commodities_h, 25000, '')
  OF ?BrowseContainerTypes
    START(Browse_ContainerTypes, 25000)
  OF ?BrowsePackagingTypes
    START(Browse_PackagingTypes, 25000)
  OF ?BrowseContainerOperators
    START(Browse_ContainerOperators, 25000)
  OF ?BrowseAddresses
    START(Browse_Addresses_h, 25000)
  OF ?SetupCODPrePaidAddresses
    START(Browse_CODAddresses, 25000)
  OF ?BrowseCountries
    START(Browse_Countries, 25000)
  OF ?BrowseSuburbs
    START(Browse_Suburbs, 25000)
  OF ?SetupDrivers
    START(Browse_Drivers_h, 25000, '')
  END
Menu::Setup ROUTINE                                        ! Code for menu items on ?Setup
  CASE ACCEPTED()
  OF ?BrowseBranches
    START(Browse_Branches, 25000)
  OF ?SetupDeliveryStatuses
    START(Browse_DeliveryStatuses, 25000)
  OF ?DeliveryTrackingEmailSetup
    START(Delivery_Tracking_Emails_Setup, 25000)
  OF ?BrowseLoadTypes
    START(Browse_LoadTypes_h, 25000, '','')
  OF ?BrowseServiceRequirements
    START(Browse_ServiceRequirements_h, 25000)
  OF ?ITEM_ClientRateTypes
    START(Browse_ClientsRateTypes_h, 25000)
  OF ?BrowseAccountants
    START(Browse_Accountants, 25000)
  OF ?BrowseSalesReps
    START(Browse_SalesReps, 25000)
  OF ?BrowseTruckTrailer
    START(Browse_TruckTrailer_h, 25000)
  OF ?BrowseVehicleComposition
    START(Browse_VehicleComposition, 25000)
  OF ?BrowseVessels
    START(Browse_Vessels, 25000)
  OF ?SetupUsers
    START(Browse_Users, 25000)
  OF ?SetupUserGroups
    START(Browse_UserGroups, 25000)
  OF ?SetupWebUsers
    START(Browse_WebUsers, 25000)
  OF ?SetupApplicationSections
    START(Browse_Application_Sections, 25000)
  OF ?SetupScheduledRuns
    START(Browse_ScheduledRuns, 25000)
  OF ?SetupPublicHolidays
    START(Browse_PublicHolidays, 25000)
  OF ?SetupReplicationIDControl
    START(Browse_ReplicationIDControl, 25000)
  OF ?SetupReplicationTableIDs
    START(Browse_ReplicationTableIDs, 25000)
  OF ?SetupItem208
    START(BrowseWordTemplates, 25000)
  OF ?SetupSetup
    START(Setup_h, 25000)
  OF ?SetupSettings
    START(Browse_Settings, 25000)
  END
Menu::Utilities ROUTINE                                    ! Code for menu items on ?Utilities
  CASE ACCEPTED()
  OF ?UtilitiesReminders
    START(Reminding, 25000)
  OF ?UtilitiesRateChecker
    START(Process_Rates_Checker, 25000)
  OF ?UtilitiesBrowseAudit
    START(Browse_Audit, 25000)
  OF ?UtilitiesDataUtilitiesAuditProfitReport
    START(Browse_Profit_Audit, 25000)
  OF ?UtilitiesBrowseListManager
    START(Browse_ListManager, 25000)
  OF ?UtilitiesPrintsConfiguration
    START(Browse_Prints, 25000)
  END
Menu::UtilitiesFileUtilities ROUTINE                       ! Code for menu items on ?UtilitiesFileUtilities
  CASE ACCEPTED()
  OF ?UtilitiesOrphaned
    START(Process_ManifestLoadDels, 25000)
  OF ?UtilitiesFileUtilitiesRemoveOrphanedManifestLoa
    START(Process_ManifestLoads, 25000)
  OF ?UtilitiesRemoveOrphanedTripSheetDeliveryItems
    START(Process_TripSheetDeliveries, 25000)
  OF ?UtilitiesFileUtilitiesCheckInvoicesmatchDIs
    START(Process_Invoices_Check, 25000)
  OF ?UtilitiesFileUtilitiesRemoveBrowseLayoutsTable
    START(Remove_Browse_Layout, 25000)
  END
Menu::UtilitiesFileUtilitiesClientsDebtors ROUTINE         ! Code for menu items on ?UtilitiesFileUtilitiesClientsDebtors
  CASE ACCEPTED()
  OF ?UtilitiesDataUtilitiesClientsDebtorsUpdateAllCl
    START(Process_Client_Balances, 25000)
  OF ?UtilitiesDataUtilitiesClientsDebtorsUpdateClien
    START(Process_Clients, 25000)
  OF ?UtilitiesCheckResetDeliveryManifested
    START(Process_Deliveries_h, 25000, '')
  OF ?UtilitiesCheckUpdateInvoiceStatuses
    START(Process_Invoices, 25000, '')
  OF ?UtilitiesFileUtilitiesResetStatusUpToDateonInvo
    START(Process_Invoice_StatusUpToDate_h, 25000, '1')
  OF ?UtilitiesFileUtilitiesResetStatusUpToDateonClie
    START(Process_ClientPaymentsAllocation_StatusUpToDate_h, 25000, '1')
  OF ?UtilitiesFileUtilitiesResetStatusUpToDateonClie2
    START(Process_ClientPayments_StatusUpToDate, 25000)
  OF ?UtilitiesDataUtilitiesClientsDebtorsCheckDIsInv
    START(Process_DIs_h, 25000, 'I')
  OF ?UtilitiesDataUtilitiesClientsDebtorsDeleteBadDI
    START(Process_DIs_h, 25000, 'D')
  OF ?ITEM_DeDupEmail
    START(RunOnce_De_Duplicate_EmailAddresses, 25000)
  END
Menu::UtilitiesFileUtilitiesTransportersCreditors ROUTINE  ! Code for menu items on ?UtilitiesFileUtilitiesTransportersCreditors
  CASE ACCEPTED()
  OF ?UtilitiesFileUtilitiesTransportersCreditorsChec
    START(Process_TransporterInvoices, 25000)
  OF ?UtilitiesFileUtilitiesResetStatusUpToDateonCred
    START(Process_InvoiceTransporter_StatusUpToDate_h, 25000, '1')
  OF ?UtilitiesFileUtilitiesResetStatusUpToDateonTran
    START(Process_TransporterPayments_StatusUpToDate_h, 25000, '1')
  END
Menu::UtilitiesFileUtilitiesSpecialDONOTUSE ROUTINE        ! Code for menu items on ?UtilitiesFileUtilitiesSpecialDONOTUSE
  CASE ACCEPTED()
  OF ?UtilitiesFileUtilitiesSpecialSetInvoicedDatetoD
    START(Special_Set_Invoiced_Dates, 25000)
  OF ?UtilitiesDataUtilitiesSpecialusewithcareTranspo
    START(Special_Set_Invoiced_Dates, 25000)
  END
Menu::UtilitiesFileUtilitiesSpecialDONOTUSE2 ROUTINE       ! Code for menu items on ?UtilitiesFileUtilitiesSpecialDONOTUSE2
  CASE ACCEPTED()
  OF ?UtilitiesFileUtilitiesSpecialDONOTUSEGenerateal
    START(Process_Manifests, 25000)
  END
Menu::UtilitiesReferenceLists ROUTINE                      ! Code for menu items on ?UtilitiesReferenceLists
  CASE ACCEPTED()
  OF ?UtilitiesReferenceListsStatementRunDescriptions
    START(Browse_Statement_Run_Desc, 25000)
  END
Menu::Direct ROUTINE                                       ! Code for menu items on ?Direct
  CASE ACCEPTED()
  OF ?BrowseAddressContacts
    START(Browse_Address_Contacts_h, 25000, '')
  OF ?DirectBrowseReminders
    START(Browse_Reminders, 25000)
  END
Menu::Reports ROUTINE                                      ! Code for menu items on ?Reports
  CASE ACCEPTED()
  OF ?ReportsStatusReport
    START(Print_Delivery_Status, 25000)
  OF ?ReportsWhatsWhere
    START(Print_Whats_Where, 25000)
  END
Menu::ReportsSetupListings ROUTINE                         ! Code for menu items on ?ReportsSetupListings
  CASE ACCEPTED()
  OF ?ReportsSetupListingsJourneysListing
    START(Print_Journeys, 25000)
  OF ?ReportsSetupListingsLoadTypesListing
    START(Print_ClientsRateTypes, 25000)
  END
Menu::ReportsReportAllEntries ROUTINE                      ! Code for menu items on ?ReportsReportAllEntries
  CASE ACCEPTED()
  OF ?ReportsDeliveryNotesall
    START(Print_DN_POD_h, 25000)
  OF ?ReportsManifestsall
    START(Print_Manifest_h, 25000)
  END
Menu::Window ROUTINE                                       ! Code for menu items on ?Window
Menu::Help ROUTINE                                         ! Code for menu items on ?Help
  CASE ACCEPTED()
  OF ?About
    AboutWindow()
  END

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.Open(AppFrame)                                      ! Open window
      !    Global_Assign()        - now called in Startup globals
      AppFrame{PROP:Timer} = 0
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Main',AppFrame)                            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
      AppFrame{PROP:TabBarVisible}  = False
  !--------------------------------------------------------------------------
  ! Tinman Application Icon
  !--------------------------------------------------------------------------
    SYSTEM{PROP:Icon} = '~dip_blue.ico'
    IF CLIP(0{PROP:Icon}) = ''
        0{PROP:Icon} = '~dip_blue.ico'
    END
  !--------------------------------------------------------------------------
  ! Tinman Application Icon
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('Main',AppFrame)                         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Deliveries:2
          ! (p:Procedure, p:Thread, p:Option)
          Count_#     = Thread_Add_Del_Check_Global('Update_Delivery_h', '', '3')
          IF Count_# > 0
             ! Have existing thread(s)
             CASE MESSAGE('You already have an instance of the Deliveries Update open.||Would you like to open another?', 'Deliveries', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:Yes
                ?Button_Deliveries:2{PROP:Text}     = 'Enter DI (' & Count_# + 1 & ')'
             ELSE
                Thread_Send_Procs_Events('Update_Delivery_h', EVENT:Restore)
                
                CYCLE
             .
          ELSE
             ?Button_Deliveries:2{PROP:Text}        = 'Enter DI (1)'
          .
      
    ELSE
      DO Menu::Menubar                                     ! Process menu items on ?Menubar menu
      DO Menu::File                                        ! Process menu items on ?File menu
      DO Menu::Edit                                        ! Process menu items on ?Edit menu
      DO Menu::Browse                                      ! Process menu items on ?Browse menu
      DO Menu::BrowseDebtors                               ! Process menu items on ?BrowseDebtors menu
      DO Menu::ReportsAccounts                             ! Process menu items on ?ReportsAccounts menu
      DO Menu::BrowseCreditors                             ! Process menu items on ?BrowseCreditors menu
      DO Menu::BrowseCreditorsReports                      ! Process menu items on ?BrowseCreditorsReports menu
      DO Menu::BrowseManagement                            ! Process menu items on ?BrowseManagement menu
      DO Menu::Setup_Operations                            ! Process menu items on ?Setup_Operations menu
      DO Menu::Setup                                       ! Process menu items on ?Setup menu
      DO Menu::Utilities                                   ! Process menu items on ?Utilities menu
      DO Menu::UtilitiesFileUtilities                      ! Process menu items on ?UtilitiesFileUtilities menu
      DO Menu::UtilitiesFileUtilitiesClientsDebtors        ! Process menu items on ?UtilitiesFileUtilitiesClientsDebtors menu
      DO Menu::UtilitiesFileUtilitiesTransportersCreditors ! Process menu items on ?UtilitiesFileUtilitiesTransportersCreditors menu
      DO Menu::UtilitiesFileUtilitiesSpecialDONOTUSE       ! Process menu items on ?UtilitiesFileUtilitiesSpecialDONOTUSE menu
      DO Menu::UtilitiesFileUtilitiesSpecialDONOTUSE2      ! Process menu items on ?UtilitiesFileUtilitiesSpecialDONOTUSE2 menu
      DO Menu::UtilitiesReferenceLists                     ! Process menu items on ?UtilitiesReferenceLists menu
      DO Menu::Direct                                      ! Process menu items on ?Direct menu
      DO Menu::Reports                                     ! Process menu items on ?Reports menu
      DO Menu::ReportsSetupListings                        ! Process menu items on ?ReportsSetupListings menu
      DO Menu::ReportsReportAllEntries                     ! Process menu items on ?ReportsReportAllEntries menu
      DO Menu::Window                                      ! Process menu items on ?Window menu
      DO Menu::Help                                        ! Process menu items on ?Help menu
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Manifest
      START(Browse_Manifests_h, 25000, '')
    OF ?Button_TripSheets
      START(Browse_TripSheets, 25000)
    OF ?Button_Floors
      START(Browse_Floors_h, 25000, '1')
    OF ?Button_Rates
      START(Browse_Clients_h, 25000, '1')
    OF ?Button_Transporters
      START(Browse_Transporter, 25000)
    OF ?Button_DI_Search
      START(Browse_Deliveries_h, 25000, 'S','')
    OF ?Button_Deliveries:2
      START(Update_Delivery_h, 25000, '')
    OF ?Button_Arrivals
      START(Manifest_Arrivals, 25000)
    OF ?Button_ManWiz
      START(Update_Manifest_Wizard, 25000)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseDown
        GLO:ClosingDown   = TRUE
    OF EVENT:OpenWindow
          LOC:Test_Database               = Get_Setup_Info(6)
          IF LOC:Test_Database = TRUE
             AppFrame{PROP:Text}          = CLIP(AppFrame{PROP:Text}) & ' *** Test Database ***'
             ?Tools{PROP:Background}      = 09191FFH      !COLOR:Red
             
          .
      
          LOC:Orig_Title  = AppFrame{PROP:Text}
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !   Removed - 10/12/2007
      !    Date_#      = DATE(02,15,2009)
      !
      !    IF TODAY() > Date_#
      !        MESSAGE('Please contact support on support@infosolutions.co.za||System TransIS will shutdown now.', 'TrasnIS', ICON:Asterisk)
      !        POST(EVENT:CloseDown)
      !        POST(EVENT:CloseWindow)
      !
      !        LOC:Closing = TRUE
      !    ELSIF TODAY() > Date_# - 15
      !        MESSAGE('Please contact support on support@infosolutions.co.za with this message.||System TransIS will need maintenance in the next ' & Date_# - TODAY() & ' days.', 'TrasnIS', ICON:Asterisk)
      !    .
          IF LOC:Closing = FALSE
                 UserLogin()
             IF GLO:UID > 0
                !DO Check_User_Options
                LOC:Logged_In         = TRUE
                AppFrame{PROP:Text}   = CLIP(LOC:Orig_Title) & ' - ' & CLIP(GLO:Login)
             ELSE
                POST(EVENT:CloseWindow)
             .
      
      !    IF GETINI('Setup', 'StartupLogin', '1', GLO:Local_INI) > 0
      !       !AppFrame{PROP:Timer} = 50
      !       !DISABLE(FIRSTFIELD(),LASTFIELD())
      !       UserLogin()
      !       IF GLO:UID > 0
      !          !DO Check_User_Options
      !          LOC:Logged_In         = TRUE
      !          AppFrame{PROP:Text}   = CLIP(LOC:Orig_Title) & ' - ' & CLIP(GLO:Login)
      !       ELSE
      !          POST(EVENT:CloseWindow)
      !       .
      !    ELSE
      !       LOC:Logged_In            = TRUE
      !    .
      
      
          IF LOC:Logged_In = TRUE
             Check_Branch_Set()
      
             START(Check_ScheduledRuns)                ! Oh yes?
      
             Check_Replication()
      
      !    db.debugout('1')
             DO Apply_User_Menus
      !    db.debugout('2')
          .
      
          .           ! EndIf  -  Closing
    OF EVENT:Timer
          LOC:Timer_It    += 1
      
          IF LOC:Timer_It >= 3
             AppFrame{PROP:Timer}    = 0
             UserLogin()
          .
    ELSE
      CASE EVENT()
      OF EVENT:User               ! Login / Logout events
         DO User_Login_Logout
  
      OF EVENT:User+1
         ! (p:Procedure, p:Thread, p:Option)
         Count_#     = Thread_Add_Del_Check_Global('Update_Delivery_h', '', '3')
         IF Count_# > 0
            ?Button_Deliveries:2{PROP:Text}     = 'Enter DI (' & Count_# & ')'
         ELSE
            ?Button_Deliveries:2{PROP:Text}     = 'Enter DI'
      .  .
  
  
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! ** global **
!!! </summary>
Global_Assign        PROCEDURE                             ! Declare Procedure
LOC:Failed           BYTE                                  ! 
Tek_Failed_File     STRING(100)


  CODE
    GLO:Local_INI       = GETINI('MySettings', 'Local_INI-' & CLIP(GLO:Login), GLO:Local_INI, PATH())
    GLO:Global_INI      = GETINI('MySettings', 'Global_INI-' & CLIP(GLO:Login), GLO:Global_INI, GLO:Local_INI)


      IF GETINI('Startup', 'Database_Open', '1', GLO:Local_INI) = 0
         ! Failed open last attempt - load connection config screen.
         Set_Connection()
      .
  
      IF Setup{PROP:SQLDriver} = TRUE
         IF GETINI('Setup', 'Trusted_Connection', '1', GLO:Global_INI) = TRUE
            SEND(Setup, '/TRUSTEDCONNECTION = TRUE' )
            SEND(Branches, '/TRUSTEDCONNECTION = TRUE' )
         .
  
         GLO:DBOwner         = GETINI('Setup', 'DBOwner', 'TransIS;UID=sa;PWD=sa;DATABASE=TransIS', GLO:Global_INI)
      .
  
      PUTINI('Startup', 'Database_Open', '0', GLO:Local_INI)
  
  !    db.debugout('Global_Assign 2')
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Setup.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Branches.Open()
     .
     Access:Setup.UseFile()
         PUTINI('Startup', 'Database_Open', '1', GLO:Local_INI)
     
     
         ! DB Open
     Access:Branches.UseFile()
    ! General Globals - after file opens and use test
    GLO:ReplicatedDatabaseID    = GETINI('Replication_Setup', 'ReplicatedDatabaseID', , GLO:Global_INI)
!               old
!    ! Called in Global Startup
!    !    GLO:Local_INI       = 'TransISL.INI'     - done in DCT globals iniital value,
!
!    BRA:BID             = GETINI('Browse_Setup', 'Branch_ID', 0, GLO:Global_INI)
!    IF BRA:BID ~= 0
!       IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
!          GLO:BranchID  = BRA:BID
!       ELSE
!          LOC:Failed    = TRUE
!          MESSAGE('Please use the My Settings option to select a Branch before doing any work on the system.', 'My Settings (Global_Assign)', ICON:Hand)
!       .
!    ELSE
!       LOC:Failed       = TRUE
!       MESSAGE('Please use the My Settings option under the Setup pull down menu to select a Branch before doing any work on the system.', 'My Settings (Global_Assign)', ICON:Hand)
!    .
!
!
!    IF LOC:Failed = TRUE
!       START(Setup_h)
!    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:Setup.Close()
    Relate:Branches.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Splash
!!! </summary>
SplashWindow PROCEDURE 

Window               WINDOW,AT(,,212,89),FONT('MS Sans Serif',8,,FONT:regular),TILED,CENTER,ICON(ICON:Application), |
  HLP('~SplashWindow'),PALETTE(256),SYSTEM,WALLPAPER('wizBck05.jpg'),IMM
                       BOX,AT(8,0,197,42),USE(?BoxTop),FILL(COLOR:ACTIVECAPTION),LINEWIDTH(1)
                       BOX,AT(8,0,51,15),USE(?BoxTopInternal),FILL(COLOR:WINDOWTEXT),LINEWIDTH(1)
                       STRING('InfoSolutions'),AT(12,2,,11),USE(?SplashHeadingSmall),FONT('Arial',,COLOR:CAPTIONTEXT), |
  TRN
                       STRING('FBN Transport'),AT(40,15,,27),USE(?SplashHeadingTop),FONT('Arial',26,COLOR:CAPTIONTEXT, |
  FONT:bold),TRN
                       STRING('TransIS'),AT(89,42,123,27),USE(?SplashHeadingBottom),FONT('Arial',26,COLOR:ACTIVECAPTION, |
  FONT:bold),TRN
                       STRING('Copyright 2004 - 2007'),AT(102,69,110,10),USE(?CopyrightDate),FONT(,,COLOR:ACTIVECAPTION), |
  TRN
                       STRING('by InfoSolutions'),AT(102,79,110,10),USE(?Developer),FONT(,,COLOR:ACTIVECAPTION),TRN
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SplashWindow')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?BoxTop
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  TARGET{Prop:Timer} = 200                                 ! Close window on timer event, so configure timer
  TARGET{Prop:Alrt,255} = MouseLeft                        ! Alert mouse clicks that will close window
  TARGET{Prop:Alrt,254} = MouseLeft2
  TARGET{Prop:Alrt,253} = MouseRight
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Application Icon
  !--------------------------------------------------------------------------
    SYSTEM{PROP:Icon} = '~dip_blue.ico'
    IF CLIP(0{PROP:Icon}) = ''
        0{PROP:Icon} = '~dip_blue.ico'
    END
  !--------------------------------------------------------------------------
  ! Tinman Application Icon
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
      IF GETINI('MySettings', 'StartReminders-' & CLIP(GLO:Login), 1, GLO:Local_INI) > 0
         START(Reminding)
      .
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      CASE KEYCODE()
      OF MouseLeft
      OROF MouseLeft2
      OROF MouseRight
        POST(Event:CloseWindow)                            ! Splash window will close on mouse click
      END
    OF EVENT:LoseFocus
        POST(Event:CloseWindow)                            ! Splash window will close when focus is lost
    OF Event:Timer
      POST(Event:CloseWindow)                              ! Splash window will close on event timer
    OF Event:AlertKey
      CASE KEYCODE()                                       ! Splash window will close on mouse click
      OF MouseLeft
      OROF MouseLeft2
      OROF MouseRight
        POST(Event:CloseWindow)
      END
    ELSE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Address_Contacts_h PROCEDURE  (p)                   ! Declare Procedure

  CODE
    Browse_Address_Contacts(p)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Addresses_h   PROCEDURE                             ! Declare Procedure

  CODE
    Browse_Addresses()
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Clients_h     PROCEDURE  (p_Option)                 ! Declare Procedure

  CODE
    Browse_Clients(, p_Option)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Commodities_h PROCEDURE  (p)                        ! Declare Procedure

  CODE
    Browse_Commodities(p)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Transporter_Invoices_h PROCEDURE                    ! Declare Procedure

  CODE
    Browse_Transporter_Invoices()
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Print_Debtor_Age_Analysis_Window_h PROCEDURE  (p)          ! Declare Procedure

  CODE
    Print_Debtor_Age_Analysis_Window(p)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Deliveries_h  PROCEDURE  (STRING p, STRING p2)      ! Declare Procedure

  CODE
    Browse_Deliveries(p,p2)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Floors_h      PROCEDURE  (p)                        ! Declare Procedure

  CODE
    Browse_Floors(p)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Journeys_h    PROCEDURE                             ! Declare Procedure

  CODE
    Browse_Journeys()
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_LoadTypes_h   PROCEDURE  (p, p2)                    ! Declare Procedure

  CODE
    Browse_LoadTypes(p, p2)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Manifests_h   PROCEDURE  (p)                        ! Declare Procedure

  CODE
    Browse_Manifests(p)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_ServiceRequirements_h PROCEDURE                     ! Declare Procedure

  CODE
    Browse_ServiceRequirements()
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_TruckTrailer_h PROCEDURE                            ! Declare Procedure

  CODE
    Browse_TruckTrailer()
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Invoice_h     PROCEDURE                             ! Declare Procedure

  CODE
    Browse_Invoice()
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Print_Rates_h        PROCEDURE  (p)                        ! Declare Procedure

  CODE
    Print_Rates(p)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Drivers_h     PROCEDURE  (p)                        ! Declare Procedure

  CODE
    Browse_Drivers(p, '')
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Setup_h              PROCEDURE                             ! Declare Procedure
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Setup.Open()
     .
     Access:Setup.UseFile()
    GlobalRequest   = ChangeRecord
    SET(Setup)
    IF Access:Setup.TryNext() = LEVEL:Benign
       Setup()
    ELSIF RECORDS(Setup) = 0
       IF Access:Setup.PrimeRecord() = LEVEL:Benign
          Access:Setup.Insert()
          Setup()
       ELSE
          MESSAGE('Setup File not primed problem.')
       .
    ELSE
       MESSAGE('Setup File next problem.')
    .


    GLO:Setup_Loaded_ID         += 1

!    Global_Assign()
    RETURN
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:Setup.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Process_Deliveries_h PROCEDURE  (p)                        ! Declare Procedure

  CODE
    Process_Deliveries(p)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Process_DIs_h        PROCEDURE  (p)                        ! Declare Procedure

  CODE
    Process_DIs(p)

    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Process_ClientPaymentsAllocation_StatusUpToDate_h PROCEDURE  (p) ! Declare Procedure

  CODE
    Process_ClientPaymentsAllocation_StatusUpToDate(p)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Process_InvoiceTransporter_StatusUpToDate_h PROCEDURE  (p) ! Declare Procedure

  CODE
    Process_InvoiceTransporter_StatusUpToDate(p)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Process_Invoice_StatusUpToDate_h PROCEDURE  (p)            ! Declare Procedure

  CODE
    Process_Invoice_StatusUpToDate(p)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Process_TransporterPayments_StatusUpToDate_h PROCEDURE  (p) ! Declare Procedure

  CODE
    Process_TransporterPayments_StatusUpToDate(p)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Update_Delivery_h    PROCEDURE  (p:Option)                 ! Declare Procedure
LOC:Deliveries_Count ULONG                                 ! 
LOC:Cycles           SHORT(100)                            ! Cycles between calls - default is 10
LOC:Option           BYTE                                  ! 
Tek_Failed_File     STRING(100)

ProgressWindow WINDOW,AT(,,93,20),CENTER,TIMER(1),GRAY,MDI
       PROMPT('Loading...'),AT(10,5,73,10),USE(?Prompt1),CENTER
     END

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Deliveries.Open()
     .
     Access:Deliveries.UseFile()
    ! (p:Option)
    ! (STRING)
    !   p:Option            (same as upd DI options)
    !
    ! DI
    ! p:Option
    ! (BYTE=0)
    !           0   - Normal mode
    !           1   - Admin editing mode
    !           2   - Multi-part delivery (insert only)


    LOC:Option  = p:Option

    Thread_Add_Del_Check_Global('Update_Delivery_h', THREAD(), 0)           ! Add

    OPEN(ProgressWindow)
    ProgressWindow{PROP:Timer}  = 0
    DISPLAY

    LOOP
       IF Access:Deliveries.PrimeRecord() ~= LEVEL:Benign
          BREAK
       ELSE
          GlobalRequest   = InsertRecord
          Update_Deliveries( LOC:Option )


          IF GlobalResponse = RequestCancelled
             Access:Deliveries.CancelAutoInc()
             BREAK
          ELSE
             ! Loop
             LOC:Deliveries_Count  += 1

             IF LOC:Option = 2
                CASE MESSAGE('Do you want to continue adding Multi-Part Deliveries?||Selecting No will return you to normal DI adding mode.', 'Add Deliveries', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
                OF BUTTON:No
                   CLEAR(LOC:Option)
          .  .  .

          ?Prompt1{PROP:Text}       = 'Loading...  (' & LOC:Deliveries_Count + 1 & ')'
          DISPLAY

          Thread_Send_Procs_Events('Browse_Deliveries', EVENT:User)
    .  .
    CLOSE(ProgressWindow)

    
    IF Thread_Add_Del_Check_Global('Browse_Deliveries', '', 2) < 1 AND GLO:ClosingDown = FALSE
       CASE MESSAGE('Would you like to Browse the Deliveries now?', 'Browse Option', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
       OF BUTTON:Yes
          Browse_Deliveries('','')
    .  .

    Thread_Add_Del_Check_Global('Update_Delivery_h', THREAD(), 1)           ! Remove

    POST(EVENT:User+1,,1)                                                   ! The renumber event
    RETURN
!               old
!    Thread_Add_Del_Check_Global('Update_Delivery_h', THREAD(), 0)           ! Add
!
!!    IF Thread_Add_Del_Check_Global('Browse_Deliveries', '', 2) < 1
!!       START(Browse_Deliveries,, '')
!!    .
!
!!    LOC:Cycles          = 30
!
!    OPEN(ProgressWindow)
!!    ProgressWindow{PROP:Hide}   = TRUE
!    ProgressWindow{PROP:Timer}  = 0
!    DISPLAY
!
!    ! Wait for Browse to Open - so we open update over it...
!!    ACCEPT
!!       LOC:Cycles      -= 1
!!       IF LOC:Cycles <= 0
!!          BREAK
!!    .  .
!!    CLOSE(ProgressWindow)
!
!    LOOP
!       IF Access:Deliveries.PrimeRecord() ~= LEVEL:Benign
!          BREAK
!       ELSE
!!          ProgressWindow{PROP:Hide}  = TRUE
!
!          GlobalRequest   = InsertRecord
!          Update_Deliveries()
!
!
!          IF GlobalResponse = RequestCancelled
!             Access:Deliveries.CancelAutoInc()
!             BREAK
!          ELSE
!             ! Loop
!             LOC:Deliveries_Count   += 1
!          .
!
!!          ProgressWindow{PROP:Hide}  = FALSE
!          DISPLAY
!
!          Thread_Send_Procs_Events('Browse_Deliveries', EVENT:User)
!    .  .
!    CLOSE(ProgressWindow)
!
!    
!    IF Thread_Add_Del_Check_Global('Browse_Deliveries', '', 2) < 1 AND GLO:ClosingDown = FALSE
!       ! ??? Need to check for closing down here before asking
!       CASE MESSAGE('Would you like to Browse the Deliveries now?', 'Browse Option', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
!       OF BUTTON:Yes
!          Browse_Deliveries('')
!    .  .
!
!    Thread_Add_Del_Check_Global('Update_Delivery_h', THREAD(), 1)           ! Remove
!
!    POST(EVENT:User+1,,1)                                                   ! The renumber event
!    RETURN
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:Deliveries.Close()
    Exit
