

   MEMBER('TRANSIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('TRANSISM003.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
TripSheet_BulkPOD_h  PROCEDURE                             ! Declare Procedure

  CODE
    TripSheet_BulkPOD()
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_ClientsRateTypes_h PROCEDURE                        ! Declare Procedure

  CODE
   Browse_ClientsRateTypes()
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Manifest_Arrivals    PROCEDURE                             ! Declare Procedure

  CODE
   Browse_Manifests('', 1)
