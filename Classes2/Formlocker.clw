 
 MEMBER('FunTrIS.CLW')


 MAP
 END

TFormLocker.Construct       Procedure
 CODE
    SELF.QLockFields &= New(TIDQueue)

TFormLocker.Destruct        Procedure
 CODE
    IF ~(SELF.QLockFields &= Null) THEN DISPOSE(SELF.QLockFields).

TFormLocker.Init            Procedure(LONG pLockButton,BYTE pWindowRequest)
 CODE
    SELF.LockButton = pLockButton
    IF pWindowRequest=InsertRecord THEN HIDE(SELF.LockButton).
    SELF.WindowRequest = pWindowRequest

TFormLocker.TakeEvent       Procedure() 
 CODE
    CASE EVENT()
    OF EVENT:Accepted
        CASE Field()
        OF SELF.LockButton
            LOOP i#=1 TO RECORDS(SELF.QLockFields)
                GET(SELF.QLockFields,i#)
                SELF.SetCtrlReadOnly(false,SELF.QLockFields.ID)
            END
            DISABLE(SELF.LockButton)
        END
    END

TFormLocker.AddField        Procedure(LONG pField)
 CODE
    SELF.QLockFields.Id = pField
    GET(SELF.QLockFields,SELF.QLockFields.Id)
    IF ErrorCode()
        ADD(SELF.QLockFields)
        IF SELF.WindowRequest<>InsertRecord
            SELF.SetCtrlReadOnly(true,pField)
        END
    END
    
TFormLocker.CanBeUnlocked   Procedure(LONG pField)
 CODE
    RETURN true

TFormLocker.SetCtrlReadOnly Procedure(Byte pStatus,LONG pCtrl)
 CODE
    IF pStatus
        IF ~Self.CanBeUnlocked(pCtrl) THEN RETURN.
    END
    SetCtrlReadOnly(pStatus,pCtrl)


