! *****************  copy of DatePeriods.inc because of Clarion mapping problem

 Member('TRANSIS')
! Member('MANTRNIS')
 Include('DatePeriods_D.inc'),Once



DatePeriodClass.Init        PROCEDURE(*tgDatePeriodsParams pParams)
 CODE
  SELF.ListControl     = pParams.ListControl
  SELF.DateFromControl = pParams.DateFromControl
  SELF.DateToControl   = pParams.DateToControl

  CLEAR(SELF.gLastPeriod)
  CLEAR(SELF.gCurPeriod)
  SELF.PeriodId &= SELF.gCurPeriod.PeriodId

  SELF.DateFrom &= pParams.CurDateFrom
  SELF.DateTo   &= pParams.CurDateTo

  IF SELF.DateFromControl
      SELF.DateFromControl{prop:use}=SELF.DateFrom
  END
  IF SELF.DateToControl
      SELF.DateToControl{prop:use}=SELF.DateTo
  END
  SELF.qPeriod &= NEW(tqPeriod)
  SELF.FillqPeriod(pParams.PeriodEquates)
  SELF.ListControl{prop:from} = SELF.qPeriod  
  SELF.SelectPeriod(pParams.DefaultPeriod)

  SELF.PrevControl = pParams.PrevControl
  SELF.NextControl = pParams.NextControl
  SELF.SetTips 


DatePeriodClass.Kill    PROCEDURE
 CODE
   IF NOT(SELF.qPeriod &= NULL)
       free(SELF.qPeriod)
       DISPOSE(SELF.qPeriod)
   END


DatePeriodClass.FillqPeriod PROCEDURE(pPeriodEquates)
curPeriod   ULONG
 CODE
  free(SELF.QPeriod)
  curPeriod=1B
  LOOP i#=1 TO 1000 
    IF curPeriod>Period:DateRange THEN break.
    IF BAND(pPeriodEquates,curPeriod)>0
        CLEAR(SELF.QPeriod)
        SELF.QPeriod.PerName = clip(SELF.GetPeriodName(curPeriod))
        SELF.QPeriod.PeriodID = curPeriod
        Add(SELF.QPeriod) 
    END
    curPeriod=BSHIFT(curPeriod,1)
  END 

  LOOP I#=1 TO RECORDS(SELF.QPeriod) 
    GET(SELF.QPeriod,I#)
    IF ErrorCode() THEN Cycle.
    CASE SELF.QPeriod.PeriodId
    OF Period:Today    
        SELF.QPeriod.DateFrom = Today()
        !Loop
        !  if SELF.QPeriod.DateFrom%7 >= 1 and SELF.QPeriod.DateFrom%7 <= 5 then break.
        !  SELF.QPeriod.DateFrom -= 1
        !end
        SELF.QPeriod.DateTo = SELF.QPeriod.DateFrom
    OF Period:Yesterday
        SELF.QPeriod.DateFrom = Today()-1
        !SELF.QPeriod.DateFrom = Today()
        !BackDays# = -1
        !Loop
        !  if SELF.QPeriod.DateFrom%7 >= 1 and SELF.QPeriod.DateFrom%7 <= 5 then BackDays# += 1.
        !  if BackDays# = 1 then break.
        !  SELF.QPeriod.DateFrom -= 1
        !End
        SELF.QPeriod.DateTo = SELF.QPeriod.DateFrom
    OF Period:ThisWeek 
        if Today()%7 = 0
           SELF.QPeriod.DateFrom = Today() - 6
        else
           SELF.QPeriod.DateFrom = Today() - Today()%7 + 1
        end
        !SELF.QPeriod.DateTo = Today()
        SELF.QPeriod.DateTo=SELF.QPeriod.DateFrom + 6
    OF Period:LastWeek 
        if Today()%7 = 0
           SELF.QPeriod.DateFrom = Today() - 6 - 7
        else
           SELF.QPeriod.DateFrom = Today() - Today()%7 + 1 - 7
        end
        SELF.QPeriod.DateTo = SELF.QPeriod.DateFrom + 6
    OF Period:ThisMonth
        SELF.QPeriod.DateFrom = Date(Month(Today()),1,Year(Today()))
    !SELF.QPeriod.DateTo   = Today()
    SELF.QPeriod.DateTo   = Date(Month(Today())+1,1,Year(Today()))-1
    OF Period:LastMonth
        SELF.QPeriod.DateTo   = Date(Month(Today()),1,Year(Today())) - 1
        SELF.QPeriod.DateFrom = Date(Month(SELF.QPeriod.DateTo),1,Year(SELF.QPeriod.DateTo))
    END
    PUT(SELF.QPeriod)
  END
  

  !!!! For derived classes  !!!!
  ! Parent.FillQPeriod(pPeriodEquates)
  ! QPeriod.PeriodID=YourOwnPeriodEquate
  ! GET(QPeriod,YourOwnEquate)
  ! IF ~ErrorCode()
  !   SELF.QPeriod.DateFrom=...
  !   SELF.QPeriod.DateTo=...
  !   PUT(SELF.QPeriod)
  ! END
  
DatePeriodClass.GetPeriodName   PROCEDURE(ULONG pPeriodEquate)
retStr    String(50)
  CODE
   CASE pPeriodEquate
   OF Period:All         
      retStr = '(All)'
   OF Period:Today       
      retStr = 'Today'
   OF Period:Yesterday   
      retStr = 'Yesterday'
   OF Period:ThisWeek    
      retStr = 'This Week'
   OF Period:LastWeek    
      retStr = 'Last Week'
   OF Period:ThisMonth   
      retStr = 'This Month'
   OF Period:LastMonth   
      retStr = 'Last Month'
   OF Period:DateRange   
      retStr = 'Date Range'
   ELSE
      retStr = ''
   END
   RETURN retStr

DatePeriodClass.SelectPeriod   PROCEDURE(ULONG pPeriodEquate=0)
 CODE
  IF pPeriodEquate
    SELF.QPeriod.PeriodID=pPeriodEquate
    GET(SELF.QPeriod,SELF.QPeriod.PeriodID)
  ELSE
    GET(SELF.QPeriod,CHOICE(SELF.ListControl))    
  END
  SELF.Synchronize()

DatePeriodClass.Synchronize      PROCEDURE
 CODE
 IF SELF.QPeriod.PeriodID<>SELF.gLastPeriod.PeriodId
     IF SELF.QPeriod.PeriodID = Period:DateRange AND SELF.QPeriod.DateFrom = 0
        SELF.QPeriod.DateFrom = SELF.gLastPeriod.DateFrom
        SELF.QPeriod.DateTo   = SELF.gLastPeriod.DateTo
        PUT(SELF.QPeriod)
     END
     SELF.gCurPeriod :=: SELF.QPeriod
     SELF.DateFrom = SELF.gCurPeriod.DateFrom
     SELF.DateTo   = SELF.gCurPeriod.DateTo
     Display(SELF.DateFromControl)
     Display(SELF.DateToControl)
 END
 SELF.gLastPeriod :=: SELF.gCurPeriod
 SELF.ListControl{prop:selected}=POINTER(SELF.QPeriod)
 Display(SELF.ListControl)

DatePeriodClass.ChangeDateRange  PROCEDURE
 CODE
  IF SELF.DateFrom<>SELF.gLastPeriod.DateFrom OR SELF.DateTo<>SELF.gLastPeriod.DateTo
    SELF.QPeriod.PeriodID=Period:DateRange
    GET(SELF.QPeriod,SELF.QPeriod.PeriodID)
    SELF.QPeriod.DateFrom=SELF.DateFrom
    SELF.QPeriod.DateTo=SELF.DateTo
    PUT(SELF.QPeriod)
    SELF.Synchronize()
  END
  
DatePeriodClass.TakeEvent    PROCEDURE()
ThisField Short,Auto
 CODE
  ThisField = Field()
  Case Event()
  of EVENT:Accepted
     CASE ThisField
     OF SELF.ListControl
         SELF.SelectPeriod()
         SELF.SetTips
     OF SELF.DateFromControl OROF SELF.DateToControl
         SELF.ChangeDateRange()
         SELF.SetTips
     OF SELF.PrevControl
     SELF.MovePeriod(1)
     OF SELF.NextControl
         SELF.MovePeriod(0)
     END
  End


DatePeriodClass.MovePeriod               PROCEDURE(BYTE Direction) ! 1=NextPeriod
date1   &DATE
date2   &DATE
sign    SHORT
delta   SHORT
 CODE
   IF ~(SELF.PrevControl AND SELF.NextControl) THEN RETURN.
   IF ~Direction
      date1 &= SELF.DateFrom
      date2 &= SELF.DateTo
      sign  = 1
   ELSE
      date2 &= SELF.DateFrom
      date1 &= SELF.DateTo
      sign  = -1
   END

   delta=SELF.GetPeriodType()
   CASE delta
   OF -1
      Date1=Date2+sign
      Date2=Date2+sign*7
   OF -2 !!!! Try To Rewrite to same style
      IF sign=1
        SELF.DateFrom=SELF.DateTo+1
        SELF.DateTo=DATE(MONTH(SELF.DateFrom)+1,1,YEAR(SELF.DateFrom))-1
      ELSE
        SELF.DateTo=SELF.DateFrom-1
        SELF.DateFrom=DATE(MONTH(SELF.DateTo),1,YEAR(SELF.DateTo))
      END
   OF -3 !!!! Try To Rewrite to same style
      IF sign=1
        SELF.DateFrom=DATE(1,1,YEAR(SELF.DateFrom)+1)
        SELF.DateTo=DATE(12,31,YEAR(SELF.DateFrom))
      ELSE
        SELF.DateFrom=DATE(1,1,YEAR(SELF.DateFrom)-1)
        SELF.DateTo=DATE(12,31,YEAR(SELF.DateFrom))
      END
   OF 0
    
   ELSE
      Date1=Date2+sign
      Date2=Date2+sign*Delta
  END
  Display(SELF.DateFromControl)
  Display(SELF.DateToControl)
  POST(Event:Accepted,SELF.DateFromControl)

DatePeriodClass.SetTips             PROCEDURE()
delta     SHORT
 CODE
  IF ~(SELF.PrevControl AND SELF.NextControl) THEN RETURN.
  SELF.PrevControl{prop:disable} = false
  SELF.NextControl{prop:disable} = false
  delta=SELF.GetPeriodType()
  CASE delta
  OF -1
    SELF.NextControl{prop:Tip} = 'Next Week'
    SELF.PrevControl{prop:Tip} = 'Prev Week'
    If Clip(SELF.NextControl{PROP:Text}) ~= '' Then SELF.NextControl{PROP:Text} = 'Next Week' .
    If Clip(SELF.PrevControl{PROP:Text}) ~= '' Then SELF.PrevControl{PROP:Text} = 'Prev Week' .
  OF -2
    SELF.NextControl{prop:Tip} = 'Next Month'
    SELF.PrevControl{prop:Tip} = 'Prev Month'
    If Clip(SELF.NextControl{PROP:Text}) ~= '' Then SELF.NextControl{PROP:Text} = 'Next Month' .
    If Clip(SELF.PrevControl{PROP:Text}) ~= '' Then SELF.PrevControl{PROP:Text} = 'Prev Month' .
  OF -3
    SELF.NextControl{prop:Tip} = 'Next Year'
    SELF.PrevControl{prop:Tip} = 'Prev Year'
    If Clip(SELF.NextControl{PROP:Text}) ~= '' Then SELF.NextControl{PROP:Text} = 'Next Year' .
    If Clip(SELF.PrevControl{PROP:Text}) ~= '' Then SELF.PrevControl{PROP:Text} = 'Prev Year' .
  OF 0
    SELF.NextControl{prop:disable} = true
    SELF.PrevControl{prop:disable} = true
  OF 1
    SELF.NextControl{prop:Tip} = 'Next Day'
    SELF.PrevControl{prop:Tip} = 'Prev Day'
    If Clip(SELF.NextControl{PROP:Text}) ~= '' Then SELF.NextControl{PROP:Text} = 'Next Day' .
    If Clip(SELF.PrevControl{PROP:Text}) ~= '' Then SELF.PrevControl{PROP:Text} = 'Prev Day' .
  ELSE
    SELF.NextControl{prop:Tip} = 'Next '&delta&' Days'
    SELF.PrevControl{prop:Tip} = 'Prev '&delta&' Days'
    If Clip(SELF.NextControl{PROP:Text}) ~= '' Then SELF.NextControl{PROP:Text} = 'Next '&delta&' Days' .
    If Clip(SELF.PrevControl{PROP:Text}) ~= '' Then SELF.PrevControl{PROP:Text} = 'Prev '&delta&' Days' .
  END


DatePeriodClass.GetPeriodType       PROCEDURE
delta       LONG
 CODE
  delta = SELF.DateTo - SELF.DateFrom
  IF delta<0 THEN RETURN 0.
  IF delta=6 AND SELF.DateFrom%7=1 THEN return -1. ! Week
  IF DAY(SELF.DateFrom)=1 AND DAY(SELF.DateTo+1)=1 AND Month(SELF.DateFrom)=Month(SELF.DateTo) AND Year(SELF.DateFrom)=Year(SELF.DateTo) THEN return -2.
  IF DAY(SELF.DateFrom)=1 AND MONTH(SELF.DateFrom)=1 AND DAY(SELF.DateTo)=31 AND MONTH(SELF.DateTo)=12 AND Year(SELF.DateFrom)=Year(SELF.DateTo) THEN return -3.
  RETURN delta+1

