
!ABCIncludeFile

OMIT('_EndOfInclude_',_ISSQLClass_)
_ISSQLClass_ EQUATE(1)

!   ----------------------------   Class Types       ----------------------------
cl_DBOwner              STRING(255),STATIC,THREAD
cl_TableName            STRING(255),STATIC,THREAD

cl_TableOpen            BYTE,STATIC,THREAD

cl_Columns              EQUATE(18)                      ! Columns available

                      COMPILE('***',NOSQL=1)
cl_Table                FILE, DRIVER('DOS'), PRE(SQ), CREATE, THREAD, OWNER(cl_DBOwner)
                      ***
                      COMPILE('***',NOSQL=0)
cl_Table                FILE,DRIVER('MSSQL'), PRE(SQ), CREATE, THREAD, OWNER(cl_DBOwner), NAME(cl_TableName)
                      ***

Record                  RECORD
Field1                    STRING(255)
Field2                    STRING(255)
Field3                    STRING(255)
Field4                    STRING(255)
Field5                    STRING(255)
Field6                    STRING(255)
Field7                    STRING(255)
Field8                    STRING(255)
Field9                    STRING(255)
Field10                   STRING(255)
Field11                   STRING(255)
Field12                   STRING(255)
Field13                   STRING(255)
Field14                   STRING(255)
Field15                   STRING(255)
Field16                   STRING(255)
Field17                   STRING(255)
Field18                   STRING(255)
                      . .

SQLQueryQueueType     QUEUE(),PRE(SQQT),TYPE
                         LIKE(cl_Table.Record)
                      .


!   ----------------------------   Class            ----------------------------
SQLQueryClass         CLASS(),TYPE,MODULE('issqlcls.clw'),LINK('issqlcls.clw',_ABCLinkMode_),DLL(_ABCDllMode_)

!   ----------------------------   Properties       ----------------------------
Data_G                  GROUP
F1                         STRING(255)
F2                         STRING(255)
F3                         STRING(255)
F4                         STRING(255)
F5                         STRING(255)
F6                         STRING(255)
F7                         STRING(255)
F8                         STRING(255)
F9                         STRING(255)
F10                        STRING(255)
F11                        STRING(255)
F12                        STRING(255)
F13                        STRING(255)
F14                        STRING(255)
F15                        STRING(255)
F16                        STRING(255)
F17                        STRING(255)
F18                        STRING(255)
                        .

List_Headers            STRING(50), DIM(cl_Columns)

SuppressMessages        BYTE
q                       &SQLQueryQueueType

DBOwner                 STRING(255)
Table_Name              STRING(255)
List_On                 BYTE

listcontrol             LONG
profile                 LONG
Log                     BYTE

LastError               LONG

_HighInColumn           BYTE, DIM(cl_Columns)
_HighColumn             BYTE                            !_maxcolumns             BYTE

Cur_Q_Pos               ULONG

!   ----------------------------   Methods          ----------------------------
PropSQL                 PROCEDURE   (STRING p_sql, LONG p_LC=0), LONG, PROC, VIRTUAL

Next_Q                  PROCEDURE   (<STRING p_Rec>, <ULONG p:Pos>),ULONG
Free_Q                  PROCEDURE   ()

Get_El                  PROCEDURE   (LONG p_No),STRING

_Open                   PROCEDURE   (), LONG, VIRTUAL

ErrorTrap               PROCEDURE   (STRING p_where, STRING p_Sql), VIRTUAL

_FormatListBox          PROCEDURE   (LONG p_LC) ,VIRTUAL
_CountColumns           PROCEDURE   ,VIRTUAL

AddLog                  PROCEDURE   (STRING p_q) ,VIRTUAL

Init                    PROCEDURE   (STRING p_DBOwner, STRING p_TableName)
Construct               PROCEDURE
Destruct                PROCEDURE
                     .

_EndOfInclude_
