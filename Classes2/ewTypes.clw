TIDQueue         Queue,Type
ID                  ULong
                 End

TStringQueue     Queue,Type
Str                 CString(256)
                 End


TStringIdQueue   Queue,Type
Str             CString(256)
Id          ULong
                 End


TTwoStringQueue  Queue,Type
Str1               CString(1024)
Str2               CString(1024)
                 End

WIN_PROCESS_INFORMATION        GROUP,TYPE
hProcess                     Unsigned
hThread                      Unsigned
dwProcessId                  Long
dwThreadId                   Long
                           END
