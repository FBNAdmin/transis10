
!ABCIncludeFile

OMIT('_EndOfInclude_',_tagging_)
_tagging_ EQUATE(1)

shpTagQueue   Queue,Type
Ptr             String(1024)
              End

! Updated 02/04/14

shpTagClass   CLASS,TYPE,MODULE('shpTagging.clw'),LINK('shpTagging.clw',_ABCLinkMode_),DLL(_ABCDllMode_)
!shpTagClass   CLASS,TYPE,MODULE('shpTagging.clw'),LINK('shpTagging.clw',1),DLL(0)
TagQueue        &shpTagQueue

QPos            ULONG        ! Q position

No_Tagged_Ctrl  LONG                ! Screen Control

Init            PROCEDURE(<LONG p_Tag_Ctrl>)

Construct       Procedure
Destruct        Procedure

IsTagged        Procedure(String pPoint),Byte
MakeTag         Procedure(String pPoint)
ClearTag        Procedure(String pPoint)
ClearAllTags      Procedure

SetPosition       PROCEDURE(ULONG p_Pos=0)    ! Set to start
NextTagged        PROCEDURE(),STRING          ! Gets and returns tagged
ReturnTagged      PROCEDURE(),STRING    

NumberTagged    Procedure(),Long
              End

_EndOfInclude_
