
  MEMBER

  omit('***',_c55_)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
  ***

Include('IKB_Stats.inc'),ONCE

  Map
  End


IKB_Stats_class._Stats     PROCEDURE() 
stats                         STRING(1001)

idx                           LONG(0)
athing                        &IKB_Stats_thing_class

thing_Stats                         GROUP
Count                                  ULONG
Time                                   ULONG
                                    .
TotalCount                    ULONG(0)
TotalTime                     DECIMAL(12,4)

CODE
   TotalTime = 0.0
   LOOP
      idx += 1
      GET(SELF._things, idx)      
      IF ERRORCODE()
         BREAK
      .
      
      athing &= SELF._things.thing
      thing_Stats = athing._Stats()
      
      ! Add_to_List
      ! (p:Add , p:List , p:Delim, p:Option, p:Prefix, p:Beginning, p:IgnoreIfExists)
      ! (STRING, *STRING, STRING , BYTE=0  , <STRING>, BYTE=0, BYTE=0)
      !   1         2        3       4          5        6      7
      ! p:Add
      !   The bit to be added
      ! p:Option
      !   0   - Left new element if list blank
      !   1   - No Left (Normal)
      ! p:Prefix
      !   If the item is populated then prefix with this...
      ! p:Beginning
      !   Add to the beginning of the list
      TotalCount += thing_Stats.Count
      TotalTime  += thing_Stats.Time

!      Add_to_List(SELF._things.name & ' -   Cnt: ' & thing_Stats.Count & ' / Time: ' & LEFT(FORMAT(thing_Stats.Time / 100, @n-10.2)), stats, ',')      
      stats = CLIP(stats) & ',' & SELF._things.name & ',Cnt: ' & thing_Stats.Count & ' / Time: ' & LEFT(FORMAT(thing_Stats.Time / 100, @n-_10.2))
!      db.Debugout('[Stats] ' & SELF._things.name & ':       Count: ' & thing_Stats.Count & '       Time (s): ' & FORMAT(thing_Stats.Time / 100, @n-10.2))
   .
!   Add_to_List('  TOTALS -   Cnt: ' & TotalCount & ' / Time: ' & LEFT(FORMAT(TotalTime / 100, @n-10.2)), stats, ',')      
   stats = 'TOTALS -   Cnt: ' & TotalCount & ' / Time: ' & LEFT(FORMAT(TotalTime / 100, @n-_10.2)) & ',' & CLIP(stats)
!   db.Debugout(' 2 stats ' & stats)
!   stats = SUB(stats, 3, LEN(CLIP(stats)))   
   
!   db.Debugout('____________________________________________________________________________________________')
!   db.Debugout('[TOTALS]   Cnt: ' & TotalCount & ' / Time (s): ' & LEFT(FORMAT(TotalTime / 100, @n-10.2)))
   
   RETURN CLIP(stats)

IKB_Stats_class._Start     PROCEDURE(STRING thing) 
idx                           LONG(0)
athing                        &IKB_Stats_thing_class
CODE
   SELF._things.name = thing
   GET(SELF._things, SELF._things.name)      
   IF ERRORCODE()         
      SELF._things.name = thing
      
      athing &= NEW(IKB_Stats_thing_class)
      SELF._things.thing &= athing
      
      athing._Start()
      ADD(SELF._things)
   ELSE
      athing &= SELF._things.thing
      athing._Start()    
      PUT(SELF._things)
   .
   RETURN
   
IKB_Stats_class._Stop          PROCEDURE(STRING thing) 
idx                                 LONG(0)
athing                              &IKB_Stats_thing_class
CODE   
   SELF._things.name = thing
   GET(SELF._things, SELF._things.name)      
   IF ERRORCODE()         
      ! not found
   ELSE
      athing &= SELF._things.thing
      athing._Stop()
      PUT(SELF._things)
   .
   RETURN

IKB_Stats_class._Clear     PROCEDURE() 
athing                        &IKB_Stats_thing_class
   CODE
   LOOP
      GET(SELF._things, 1)      
      IF ERRORCODE()
         BREAK
      .
      
      athing &= SELF._things.thing
      DISPOSE(athing)      
      DELETE(SELF._things)      
   .
   RETURN
   

IKB_Stats_class.Construct   PROCEDURE() 
CODE
   SELF._things &= NEW(_things)
   RETURN
   

IKB_Stats_class.Destruct   PROCEDURE() 
CODE
   SELF._Clear()
   DISPOSE(SELF._things)
   RETURN
   
   

! ===========  IKB_Stats_thing_class  ==========
IKB_Stats_thing_class._Stats     PROCEDURE() 
Stats                               GROUP
Count                                  ULONG
Time                                   ULONG
                                    .
CODE
   Stats.Count = SELF._cnt
   Stats.Time = SELF._time
   RETURN Stats

IKB_Stats_thing_class._Start     PROCEDURE() 
CODE
   SELF._cnt += 1
   SELF._started = CLOCK()
   RETURN
   
IKB_Stats_thing_class._Stop          PROCEDURE() 
CODE   
   SELF._time += CLOCK() - SELF._started
   RETURN
   