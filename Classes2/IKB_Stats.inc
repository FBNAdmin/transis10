!ABCIncludeFile

OMIT('_EndOfInclude_',_IKB_StatsClass_)
_IKB_StatsClass_ EQUATE(1)


IKB_Stats_thing_class      CLASS,TYPE,MODULE('ikb_stats.clw'),LINK('ikb_stats.clw',_ABCLinkMode_),DLL(_ABCDllMode_)
_cnt                    ULONG(0)
_time                   ULONG(0)
_started                ULONG

_Start                  PROCEDURE()
_Stop                   PROCEDURE()
_Stats                  PROCEDURE(),STRING                    
                           .

_things                 QUEUE,TYPE
name                      CSTRING(101)
thing                     &IKB_Stats_thing_class
                        .

IKB_Stats_class      CLASS,TYPE,MODULE('ikb_stats.clw'),LINK('ikb_stats.clw',_ABCLinkMode_),DLL(_ABCDllMode_)
_things                 &_things

_Start                  PROCEDURE(STRING thing)
_Stop                   PROCEDURE(STRING thing)
_Stats                  PROCEDURE(),STRING

_Clear                  PROCEDURE() 

Destruct                PROCEDURE()
Construct               PROCEDURE()

                     .


_EndOfInclude_
