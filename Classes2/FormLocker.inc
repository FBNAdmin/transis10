
TFormLocker   Class,Type,Module('Formlocker'),link('Formlocker',_FunLinkMode_),dll(_FunDLLMode_)
LockButton      LONG,Private
WindowRequest   BYTE,Private
QLockFields     &TIDQueue,Private

!! Methods !!
Construct       Procedure
Destruct        Procedure
Init            Procedure(LONG pLockButton,BYTE pWindowRequest)
TakeEvent       Procedure()
AddField        Procedure(LONG pField)
SetCtrlReadOnly Procedure(Byte pStatus,LONG pCtrl)

CanBeUnlocked   Procedure(LONG pField),Byte,Virtual
          End
