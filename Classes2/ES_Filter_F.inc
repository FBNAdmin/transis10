!A B C I n c l u d e F i l e

! *****************  copy of ES_Filter_F.inc because of Clarion mapping problem

! Elmis / Win-IT Filter Class - Comment added 8 Oct 06

!_FilterLinkMode_ Equate(1)
!_FilterDllMode_  Equate(0)

FILTER_String    Equate(1)
FILTER_Numeric   Equate(2)
FILTER_Date      Equate(3)
FILTER_Time      Equate(4)
FILTER_Bool      Equate(5)
FILTER_List      Equate(6)

CONDITION_Off         Equate(0)
CONDITION_Equal       Equate(1)
CONDITION_NotEqual    Equate(2)
CONDITION_Less        Equate(3)
CONDITION_More        Equate(4)
CONDITION_LessEqual   Equate(5)
CONDITION_MoreEqual   Equate(6)
CONDITION_InRange     Equate(7)
CONDITION_OutOfRange  Equate(8)
CONDITION_Ticked      Equate(9)
CONDITION_UnTicked    Equate(10)
CONDITION_InList      Equate(11)
CONDITION_NotInList   Equate(12)

FILTERREQUEST_Refresh Equate(1)

TFilterValueQueue Queue
Display              CSTring(128)
Icon                 Short
UseValue             CString(128)
                  End

TFilterValueGroup Group
Mode                Byte
Str                 Any
ValueFrom           Any
ValueTo             Any
QMulti              &TFilterValueQueue
                  End

TFilterField     Queue,Type
FieldType           Short
FieldName           CString(61)
PmtCtrl             Short
ModeCtrl            Short
EntryCtrl1          Short
ButtonCtrl1         Short
EntryCtrl2          Short
ButtonCtrl2         Short
flt                 Like(TFilterValueGroup)
scr                 Like(TFilterValueGroup)
                 End

TFilterCriterion Queue,Type

                 End

! Filter
TStrMode            Queue
Str                     String(20)
Mode                    Long
                     End
TValMode            Queue
Val                     String(20)
Mode                    Long
                     End
TBoolMode            Queue
Bool                    String(20)
Mode                    Long
                     End
TListMode            Queue
List                    String(20)
Mode                    Long
                     End


! **********************************************************************************************
! Filter Class
! **********************************************************************************************

FilterClass CLASS,Type,MODULE('ES_Filter_F.CLW'),LINK('ES_Filter_F.CLW',_FilterLinkMode_),DLL(_FilterDllMode_)

              ! Properties -----------------------------------------------------------------------

FilterName       CSTring(41)
ListCtrl         Long
QField           &TFilterField
QCrit            &TFilterCriterion
QStrMode         &TStrMode
QValMode         &TValMode
QBoolMode        &TBoolMode
QListMode        &TListMode
FilterID         ULong

              ! Methods -------------------------------------------------------------------------
Construct        Procedure
Destruct         Procedure
Init             Procedure(String pFilterName)
Kill             Procedure
AddStrField      Procedure(String pFieldName)
AddNumericField  Procedure(String pFieldName)
AddDateField     Procedure(String pFieldName)
AddTimeField     Procedure(String pFieldName)
AddBoolField     Procedure(String pFieldName)
AddListField     Procedure(String pFieldName)

SetStrField      Procedure(*? pValue,String pFieldName,Short pPmtCtrl,Short pModeCtrl,Short pEntryCtrl,Short pButtonCtrl)
SetNumericField  Procedure(*? pValueFrom,*? pValueTo,String pFieldName,Short pPmtCtrl,Short pModeCtrl,Short pEntryCtrl1,Short pButtonCtrl1,Short pEntryCtrl2,Short pButtonCtrl2)
SetDateField     Procedure(*? pValueFrom,*? pValueTo,String pFieldName,Short pPmtCtrl,Short pModeCtrl,Short pEntryCtrl1,Short pButtonCtrl1,Short pEntryCtrl2,Short pButtonCtrl2)
SetTimeField     Procedure(*? pValueFrom,*? pValueTo,String pFieldName,Short pPmtCtrl,Short pModeCtrl,Short pEntryCtrl1,Short pButtonCtrl1,Short pEntryCtrl2,Short pButtonCtrl2)
SetBoolField     Procedure(String pFieldName,Short pPmtCtrl,Short pModeCtrl)
SetListField      Procedure(*? pValue,String pFieldName,Short pPmtCtrl,Short pModeCtrl,Short pEntryCtrl,Short pButtonCtrl1,Short pButtonCtrl2)

AddListFieldValue Procedure(String pFieldName,? pValue)
ClearListFieldValues Procedure(String pFieldName)
ShowListValues    Procedure(String pFieldName)

TakeEvent        Procedure
TryAskPopup      Procedure(Short pCtrl)

GetFieldByName   Procedure(String pFieldName,Byte pShowMessage),Byte
GetListValueName Procedure(String pFieldName,String pUseValue),String,Virtual
FilterToScreen   Procedure
ScreenToFilter   Procedure

ShowFromMode     Procedure
SetMode          Procedure(Short pFieldType,Short pModeCtrl,Long pMode)
SetPrefix        Procedure(String pFieldName,String pPrefix),String
GetMode          Procedure(Short pFieldType,Short pModeCtrl),Long
SQLDate          Procedure(Date pDate),String,Virtual
SQLTime          Procedure(Date pTime),String,Virtual
ValidateFilter   Procedure(),Byte
GetSQLRequest    Procedure(String pPrefix),String

FilterButtonClick Procedure(*? FilterOn,Short FilterCtrl),Byte
SetupFilter       Procedure(),Byte,Virtual
UpdateFilter      Procedure(),Virtual
BrowseFilters     Procedure(),Virtual
ClearFilter       Procedure(Byte pMode)

FilterPopup       Procedure(Short xPos,Short yPos,Byte FilterOn),Long

!LoadFilter        Procedure(ULong pFilterID),Byte
!SaveFilter        Procedure(ULong pFilterID)
!SaveFilterAs      Procedure()
            End
