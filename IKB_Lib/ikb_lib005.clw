

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Convert_Text_To_MS   PROCEDURE (P:Text)                    ! Declare Procedure
LOC:Ret_Text         STRING(65000)                         !
LOC:Add_Chars        STRING(3)                             !
LOC:Text_Pos         ULONG                                 !
LOC:Normal           BYTE                                  !
  CODE                                                     ! Begin processed code
    LOC:Text_Pos = 0

    LOOP I# = 1 TO LEN(CLIP(P:Text))
       CASE P:Text[I#]
       OF EQU:CR
          LOC:Add_Chars  = CLIP(EQU:MS_CR)
       OF EQU:LF
          LOC:Add_Chars  = CLIP(EQU:MS_LF)
       OF EQU:SP
          LOC:Add_Chars  = CLIP(EQU:MS_SP)
       OF EQU:CO
          LOC:Add_Chars  = CLIP(EQU:MS_CO)
       OF EQU:QU
          LOC:Add_Chars  = CLIP(EQU:MS_QU)
       OF EQU:PE
          LOC:Add_Chars  = CLIP(EQU:MS_PE)
       OF EQU:EX
          LOC:Add_Chars  = CLIP(EQU:MS_EX)
       OF EQU:CL
          LOC:Add_Chars  = CLIP(EQU:MS_CL)
       OF EQU:SC
          LOC:Add_Chars  = CLIP(EQU:MS_SC)
       ELSE
          LOC:Add_Chars  = P:Text[I#]
       .
       LOC:Ret_Text = CLIP(LEFT(LOC:Ret_Text)) & CLIP(LOC:Add_Chars)
    .

    RETURN(CLIP(LOC:Ret_Text))
