

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

File_User_Access     FUNCTION (p:Mode)                     ! Declare Procedure
LOC:Str_Byte         STRING(8)                             !
LOC:Nibble           BYTE                                  !
  CODE                                                     ! Begin processed code
    LOC:Str_Byte    = Byte_To_StrB(p:Mode)

    LOC:Nibble      = StrB_To_Byte('0000' & LOC:Str_Byte[4:8])

    RETURN(LOC:Nibble)

