

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Count_SubStr_In_Str  FUNCTION (p:Sub_Str, p:Str)           ! Declare Procedure
LOC:No_SubStr        ULONG                                 !
LOC:Pos              ULONG                                 !
LOC:Len_Sub          ULONG                                 !
LOC:Len_Str          ULONG                                 !
LOC:Idx              LONG                                  !
  CODE                                                     ! Begin processed code
    ! (p:Sub_Str, p:Str)
    ! (STRING,STRING),ULONG

    LOC:No_SubStr   = 0
    LOC:Idx         = 0

    LOC:Len_Sub     = LEN(CLIP(p:Sub_Str))
    LOC:Len_Str     = LEN(CLIP(p:Str))

    LOOP
       LOC:Idx  += 1

       ! If index pos + length of sub - 1 greater than string
       ! 1 + 10 - 1 = 10 > 9 = cannot possibly equal string
       IF (LOC:Idx + (LOC:Len_Sub - 1)) > LOC:Len_Str
          BREAK
       .

       ! xyz = 5 : (5 + 3 - 1) = 5 : 7 = pos 5,6,7
       IF CLIP(p:Sub_Str) = p:Str[LOC:Idx : (LOC:Idx + (LOC:Len_Sub - 1))]
          LOC:No_SubStr     += 1
          LOC:Idx           += (LOC:Len_Sub - 1)        ! Move forward the whole substring
    .  .

    RETURN(LOC:No_SubStr)

