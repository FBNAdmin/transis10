

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Output_Text          FUNCTION (P:Text, P:Chars, P:FileName, p:Create) ! Declare Procedure
LOC:Start            ULONG                                 !
LOC:End              ULONG                                 !
LOC:Error            LONG                                  !
OutFile    FILE,DRIVER('ASCII'),CREATE,PRE(OF)
Rec           RECORD
Out_Line         STRING(10000)
           .  .
  CODE                                                     ! Begin processed code
    ! Output_Text
    ! (STRING, USHORT=0, STRING, BYTE=1)
    ! (P:Text, P:Chars, P:FileName, p:Create)
    !
    ! P:Chars - if not 0 and less than total in p:Text then add a line for every this no. of chars
    !
    ! ***** Change log
    ! ** 06/06/2012 - made file STRING 10000 from 1000
    !               - made chars param defaulted
    !

!OutFile    FILE,DRIVER('ASCII')
!Rec           RECORD
!Out_Line         STRING(10000)
!           .  .

    IF P:Chars = 0
       P:Chars  = LEN(CLIP(p:Text))
    .

!    message('text: ' & CLIP(p:Text)  & '||Chars: ' & p:Chars)

    OutFile{PROP:NAME}  = CLIP(P:FileName)
    IF p:Create = TRUE
       CREATE(OutFile)
       IF ERRORCODE()
          LOC:Error = -1
          MESSAGE('Error trying to CREATE file: ' & P:FileName,'Error - ' & ERROR(),ICON:Hand)
    .  .

    OutFile{PROP:NAME}  = CLIP(P:FileName)
    OPEN(OutFile)
    IF ERRORCODE()
       LOC:Error    = -2
       MESSAGE('Error trying to OPEN file: ' & P:FileName,'Error - ' & ERROR(),ICON:Hand)
    .

    IF LOC:Error >= 0
        LOC:Start   = 1
        LOC:End     = LEN(CLIP(P:Text))

       LOOP
          IF (LOC:Start + P:Chars) < LOC:End
             OF:Out_Line  = CLIP(LEFT(P:Text[LOC:Start : LOC:Start + P:Chars - 1]))
          ELSE
             OF:Out_Line  = CLIP(LEFT(P:Text[LOC:Start : LOC:End]))
             ADD(OutFile)
             BREAK
          .
           ADD(OutFile)
          LOC:Start    += P:Chars
       .

       CLOSE(OutFile)
    .
    RETURN(LOC:Error)
