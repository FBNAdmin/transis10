

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

SQL_Ret_DateT_G      FUNCTION (p:DT_Str, p:Date_Format)    ! Declare Procedure
L:DateTime_Group     GROUP,PRE(L_DT)                       !
Date                 DATE                                  !
Time                 TIME                                  !
                     END                                   !
LOC:Date_Format      BYTE,STATIC                           !static
  CODE                                                     ! Begin processed code
    ! (p:DT_Str, p:Date_Format)
    ! (STRING, <BYTE>),STRING
    !                                   1         2
    !                          1234567890123456789012345678
    ! p:Date_Format
    !                     0 :  yyyy-mm-dd hh:mm:ss.hhhhhhhh
    !                     1 :  dd-mm-yyyy hh:mm:ss.hhhhhhhh

    IF ~OMITTED(2)
       LOC:Date_Format   = p:Date_Format
    ELSE
       ! Try to guess correctly....
       IF NUMERIC(SUB(p:DT_Str, 1, 4)) = TRUE
          LOC:Date_Format   = 0
       ELSIF NUMERIC(SUB(p:DT_Str, 7, 4)) = TRUE
          LOC:Date_Format   = 1
       ELSE
          MESSAGE('Unknown date format sting.||String: ' & CLIP(p:DT_Str), 'SQL_Ret_DateT_G', ICON:Exclamation)
    .  .


    ! DATE function - mm,dd,yyyy

    p:DT_Str        = LEFT(p:DT_Str)

    EXECUTE LOC:Date_Format
       L_DT:Date    = DATE(SUB(p:DT_Str, 4, 2), SUB(p:DT_Str, 1, 2), SUB(p:DT_Str, 7, 4))
    ELSE
       L_DT:Date    = DATE(SUB(p:DT_Str, 6, 2), SUB(p:DT_Str, 9, 2), SUB(p:DT_Str, 1, 4))
    .

    L_DT:Time       = DEFORMAT(SUB(p:DT_Str, 12, 19), @t4)

    RETURN(L:DateTime_Group)
Add_to_List_C        PROCEDURE (p:Add, p:List, p:Delim, p:Option, p:Prefix, p:Beginning) ! Declare Procedure
p_List              &STRING
  CODE                                                     ! Begin processed code
    ! (p:Add , p:List , p:Delim, p:Option, p:Prefix, p:Beginning)
    ! (STRING, *CSTRING, STRING , BYTE=0  , <STRING>, BYTE=0)
    !   1          2        3       4          5        6
    ! p:Add
    !   The bit to be added
    ! p:Option
    !   0   - Left new element if list blank
    !   1   - No Left (Normal)
    ! p:Prefix
    !   If the item is populated then prefix with this...
    ! p:Beginning
    !   Add to the beginning of the list

    p_List &= NEW(STRING(SIZE(p:List)))

    p_List  = CLIP(p:List)

    IF OMITTED(5)                   ! p:Prefix
       Add_to_List(p:Add , p_List, p:Delim, p:Option, , p:Beginning)
    ELSE
       Add_to_List(p:Add , p_List, p:Delim, p:Option, p:Prefix, p:Beginning)
    .

    p:List      = p_List

    DISPOSE(p_List)
    RETURN
PopupCalendar        PROCEDURE (*? PassedDate,Byte FixYear,<LONG XPos>,<Long YPos>) ! Declare Procedure
DateSelected         Byte(False)
DateSaved            Long
DayNumber            DATE
MonthNumber          SHORT
YearNumber           SHORT
ChangeMonthYear      BYTE(FALSE)
UpdateAssign         BYTE(TRUE)

CalendarWindow WINDOW('Caption'),AT(,,124,82),FONT('MS Sans Serif',8,,FONT:regular),CENTER,ALRT(UpKey), |
         ALRT(DownKey),ALRT(PgUpKey),ALRT(PgDnKey),ALRT(EscKey),ALRT(RightKey),ALRT(LeftKey),ALRT(MouseRight), |
         ALRT(EnterKey),SYSTEM,GRAY,DOUBLE
       STRING('Mo'),AT(2,1,10,10),USE(?StringMo),CENTER,FONT(,,COLOR:Navy,)
       STRING('Tu'),AT(14,1,10,10),USE(?StringTu),CENTER,FONT(,,COLOR:Navy,)
       STRING('We'),AT(26,1,12,10),USE(?StringWe),CENTER,FONT(,,COLOR:Navy,)
       STRING('Th'),AT(38,1,10,10),USE(?StringTh),CENTER,FONT(,,COLOR:Navy,)
       STRING('Fr'),AT(50,1,10,10),USE(?StringFr),CENTER,FONT(,,COLOR:Navy,)
       STRING('Sa'),AT(62,1,10,10),USE(?StringSa),CENTER,FONT(,,COLOR:Green,)
       STRING('Su'),AT(74,1,10,10),USE(?StringSu),CENTER,FONT(,,COLOR:Red,)
       BUTTON,AT(112,0,11,11),USE(?TodayButton),TIP('Jump to today'),ICON('DATEPop.ICO')
       PANEL,AT(0,0,,12),USE(?Panel1),FULL,BEVEL(0,0,09H)
       PANEL,AT(89,17,32,29),USE(?Panel3),BEVEL(-1)
       STRING('Month'),AT(94,20),USE(?StringMonth),TRN,CENTER,FONT(,,COLOR:Maroon,)
       SPIN(@n-6),AT(93,29,24,12),USE(MonthNumber),IMM,HSCROLL,TIP('Change month'),STEP(1)
       PANEL,AT(89,50,32,29),USE(?Panel2),BEVEL(-1)
       STRING('Year'),AT(98,53),USE(?StringYear),TRN,CENTER,FONT(,,COLOR:Maroon,)
       SPIN(@n-6),AT(93,62,24,12),USE(YearNumber),IMM,HSCROLL,TIP('Change year'),STEP(1)
       REGION,AT(1,15,12,11),USE(?DayRegion1)!,IMM
       REGION,AT(13,15,12,11),USE(?DayRegion2)!,IMM
       REGION,AT(25,15,12,11),USE(?DayRegion3)!,IMM
       REGION,AT(37,15,12,11),USE(?DayRegion4)!,IMM
       REGION,AT(49,15,12,11),USE(?DayRegion5)!,IMM
       REGION,AT(61,15,12,11),USE(?DayRegion6)!,IMM
       REGION,AT(73,15,12,11),USE(?DayRegion7)!,IMM
       REGION,AT(1,26,12,11),USE(?DayRegion8)!,IMM
       REGION,AT(13,26,12,11),USE(?DayRegion9)!,IMM
       REGION,AT(25,26,12,11),USE(?DayRegion10)!,IMM
       REGION,AT(37,26,12,11),USE(?DayRegion11)!,IMM
       REGION,AT(49,26,12,11),USE(?DayRegion12)!,IMM
       REGION,AT(61,26,12,11),USE(?DayRegion13)!,IMM
       REGION,AT(73,26,12,11),USE(?DayRegion14)!,IMM
       REGION,AT(1,37,12,11),USE(?DayRegion15)!,IMM
       REGION,AT(13,37,12,11),USE(?DayRegion16)!,IMM
       REGION,AT(25,37,12,11),USE(?DayRegion17)!,IMM
       REGION,AT(37,37,12,11),USE(?DayRegion18)!,IMM
       REGION,AT(49,37,12,11),USE(?DayRegion19)!,IMM
       REGION,AT(61,37,12,11),USE(?DayRegion20)!,IMM
       REGION,AT(73,37,12,11),USE(?DayRegion21)!,IMM
       REGION,AT(1,48,12,11),USE(?DayRegion22)!,IMM
       REGION,AT(13,48,12,11),USE(?DayRegion23)!,IMM
       REGION,AT(25,48,12,11),USE(?DayRegion24)!,IMM
       REGION,AT(37,48,12,11),USE(?DayRegion25)!,IMM
       REGION,AT(49,48,12,11),USE(?DayRegion26)!,IMM
       REGION,AT(61,48,12,11),USE(?DayRegion27)!,IMM
       REGION,AT(73,48,12,11),USE(?DayRegion28)!,IMM
       REGION,AT(1,59,12,11),USE(?DayRegion29)!,IMM
       REGION,AT(13,59,12,11),USE(?DayRegion30)!,IMM
       REGION,AT(25,59,12,11),USE(?DayRegion31)!,IMM
       REGION,AT(37,59,12,11),USE(?DayRegion32)!,IMM
       REGION,AT(49,59,12,11),USE(?DayRegion33)!,IMM
       REGION,AT(61,59,12,11),USE(?DayRegion34)!,IMM
       REGION,AT(73,59,12,11),USE(?DayRegion35)!,IMM
       REGION,AT(1,70,12,11),USE(?DayRegion36)!,IMM
       REGION,AT(13,70,12,11),USE(?DayRegion37)!,IMM
       STRING(''),AT(1,16,10,10),USE(?DayString1),TRN,RIGHT
       STRING(''),AT(13,16,10,10),USE(?DayString2),TRN,RIGHT
       STRING(''),AT(25,16,10,10),USE(?DayString3),TRN,RIGHT
       STRING(''),AT(37,16,10,10),USE(?DayString4),TRN,RIGHT
       STRING(''),AT(49,16,10,10),USE(?DayString5),TRN,RIGHT
       STRING(''),AT(61,16,10,10),USE(?DayString6),TRN,RIGHT
       STRING(''),AT(73,16,10,10),USE(?DayString7),TRN,RIGHT
       STRING(''),AT(1,27,10,10),USE(?DayString8),TRN,RIGHT
       STRING(''),AT(13,27,10,10),USE(?DayString9),TRN,RIGHT
       STRING(''),AT(25,27,10,10),USE(?DayString10),TRN,RIGHT
       STRING(''),AT(37,27,10,10),USE(?DayString11),TRN,RIGHT
       STRING(''),AT(49,27,10,10),USE(?DayString12),TRN,RIGHT
       STRING(''),AT(61,27,10,10),USE(?DayString13),TRN,RIGHT
       STRING(''),AT(73,27,10,10),USE(?DayString14),TRN,RIGHT
       STRING(''),AT(1,38,10,10),USE(?DayString15),TRN,RIGHT
       STRING(''),AT(13,38,10,10),USE(?DayString16),TRN,RIGHT
       STRING(''),AT(25,38,10,10),USE(?DayString17),TRN,RIGHT
       STRING(''),AT(37,38,10,10),USE(?DayString18),TRN,RIGHT
       STRING(''),AT(49,38,10,10),USE(?DayString19),TRN,RIGHT
       STRING(''),AT(61,38,10,10),USE(?DayString20),TRN,RIGHT
       STRING(''),AT(73,38,10,10),USE(?DayString21),TRN,RIGHT
       STRING(''),AT(1,49,10,10),USE(?DayString22),TRN,RIGHT
       STRING(''),AT(13,49,10,10),USE(?DayString23),TRN,RIGHT
       STRING(''),AT(25,49,10,10),USE(?DayString24),TRN,RIGHT
       STRING(''),AT(37,49,10,10),USE(?DayString25),TRN,RIGHT
       STRING(''),AT(49,49,10,10),USE(?DayString26),TRN,RIGHT
       STRING(''),AT(61,49,10,10),USE(?DayString27),TRN,RIGHT
       STRING(''),AT(73,49,10,10),USE(?DayString28),TRN,RIGHT
       STRING(''),AT(1,60,10,10),USE(?DayString29),TRN,RIGHT
       STRING(''),AT(13,60,10,10),USE(?DayString30),TRN,RIGHT
       STRING(''),AT(25,60,10,10),USE(?DayString31),TRN,RIGHT
       STRING(''),AT(37,60,10,10),USE(?DayString32),TRN,RIGHT
       STRING(''),AT(49,60,10,10),USE(?DayString33),TRN,RIGHT
       STRING(''),AT(61,60,10,10),USE(?DayString34),TRN,RIGHT
       STRING(''),AT(73,60,10,10),USE(?DayString35),TRN,RIGHT
       STRING(''),AT(1,71,10,10),USE(?DayString36),TRN,RIGHT
       STRING(''),AT(13,71,10,10),USE(?DayString37),TRN,RIGHT
     END

PrevX ULong
PrevY ULong
  CODE                                                     ! Begin processed code
  PrevX = 0{Prop:XPos}
  PrevY = 0{Prop:YPos}

  PUSHBIND
  OPEN(CalendarWindow)
  IF XPOS > 0
    CalendarWindow{prop:xpos}=PrevX + xpos
  END
  IF YPos > 0
    CalendarWindow{PROP:YPos} = PrevY + YPos
  ELSIF YPos < 0
    CalendarWindow{PROP:YPos} = ABS(YPos) - CalendarWindow{PROP:Height}
  END
  DateSaved = PassedDate
  IF PassedDate = ''
    PassedDate=Today()
  END
  DayNumber = PassedDate
  MonthNumber = MONTH(PassedDate)
  YearNumber = YEAR(PassedDate)
  DO CheckRange
  ACCEPT
    CASE EVENT()
    OF EVENT:AlertKey
      CASE KEYCODE()
        OF LeftKey
           PassedDate -=1
           IF MonthNumber <> MONTH(PassedDate) OR YearNumber <> YEAR(PassedDate)
             UpdateAssign = TRUE
             MonthNumber = MONTH(PassedDate)
             YearNumber  = YEAR(PassedDate)
           END
        OF RightKey
           PassedDate +=1
           IF MonthNumber <> MONTH(PassedDate) OR YearNumber <> YEAR(PassedDate)
             UpdateAssign = TRUE
             MonthNumber = MONTH(PassedDate)
             YearNumber  = YEAR(PassedDate)
           END
        OF UpKey
           MonthNumber -=1
           ChangeMonthYear = TRUE
        OF DownKey
           MonthNumber +=1
           ChangeMonthYear = TRUE
        OF PgUpKey
           YearNumber -= 1
           ChangeMonthYear = TRUE
        OF PgDnKey
           YearNumber += 1
           ChangeMonthYear = TRUE
        OF EnterKey
           BREAK
        OF EscKey
           PassedDate = DayNumber
           BREAK
        OF MouseRight
           PassedDate = DayNumber
           BREAK
        END
      IF    MonthNumber > 12
            MonthNumber = 1
            YearNumber +=1
      ELSIF MonthNumber < 1
            MonthNumber = 12
            YearNumber -=1
      END
      IF ChangeMonthYear
         ChangeMonthYear = FALSE
         PassedDate = DEFORMAT(('1/' &MonthNumber &'/' &YearNumber),@D6)
         UpdateAssign = TRUE
      END
      DO CheckRange
    END
    CASE FIELD()
    OF ?MonthNumber
      IF EVENT() = EVENT:NewSelection
        IF MonthNumber > 12
              MonthNumber = 1
              YearNumber +=1
        ELSIF MonthNumber < 1
              MonthNumber = 12
              YearNumber -=1
        END
        PassedDate = DEFORMAT(('1/' &MonthNumber &'/' &YearNumber),@D6)
        UpdateAssign = TRUE
        DO CheckRange
      END
    OF ?YearNumber
      IF EVENT() = EVENT:NewSelection
        PassedDate = DEFORMAT(('1/' &MonthNumber &'/' &YearNumber),@D6)
        UpdateAssign = TRUE
        DO CheckRange
      END
    OF ?TodayButton
      IF EVENT() = EVENT:Accepted
        MonthNumber = MONTH(TODAY())
        YearNumber  = YEAR(TODAY())
        PassedDate  = TODAY()
        UpdateAssign = TRUE
        DO CheckRange
      END
    END
    IF EVENT() = EVENT:Accepted
      LOOP DayRegion# = ?DayRegion1 TO ?DayRegion37
        IF FIELD() = DayRegion#
          DayString# = DayRegion#+37
          IF DayString#{PROP:TEXT} <> ''
            PassedDate = DEFORMAT(DayString#{PROP:TEXT} &'/' &MonthNumber &'/' &YearNumber,@D6)
            DateSelected = True
            POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  CLOSE(CalendarWindow)
  POPBIND
  if ~DateSelected then
     PassedDate = DateSaved
  End
!----------------------------------------------------------------------------
SetWindowTitle ROUTINE
!|
!| S?t tittel p? kalender vindue
!|
  EXECUTE MonthNumber
    CalendarWindow{PROP:TEXT}='January ' &YearNumber
    CalendarWindow{PROP:TEXT}='February ' &YearNumber
    CalendarWindow{PROP:TEXT}='March ' &YearNumber
    CalendarWindow{PROP:TEXT}='April ' &YearNumber
    CalendarWindow{PROP:TEXT}='May ' &YearNumber
    CalendarWindow{PROP:TEXT}='June ' &YearNumber
    CalendarWindow{PROP:TEXT}='July ' &YearNumber
    CalendarWindow{PROP:TEXT}='August ' &YearNumber
    CalendarWindow{PROP:TEXT}='September ' &YearNumber
    CalendarWindow{PROP:TEXT}='October ' &YearNumber
    CalendarWindow{PROP:TEXT}='November ' &YearNumber
    CalendarWindow{PROP:TEXT}='December ' &YearNumber
  END
!|----------------------------------------------------------------------------------------------
AssignAllStrings ROUTINE
!|
!| Opdater kalender for den p?g?ldende m?ned
!|
  CASE MonthNumber
    OF     9
    OROF   4
    OROF   6
    OROF   11
           MaxDays#=30                 
    OF     2
           IF YearNumber % 100 = 0         ! If year is a Century
             IF YearNumber % 400 = 0       ! It has to be divisible by 400
                MaxDays# = 29              !  2000 is a leap year
             ELSE
                MaxDays# = 28              !  1900 and 2100 are not
             END
           ELSE
             IF YearNumber % 4 = 0
                MaxDays# = 29
             ELSE
                MaxDays# = 28
             END
           END
    ELSE   MaxDays# = 31
  END
  LOOP DayString# = ?DayString1 TO ?DayString7
    IF DayString# - ?DayString1 + 1 = DEFORMAT(('1/' &MonthNumber &'/' &YearNumber),@D6) % 7 OR DayString# = ?DayString7
      DayString#{PROP:TEXT} = '1'
      IF DEFORMAT(DayString#{PROP:Text},@n2) = DAY(DayNumber) AND MonthNumber = MONTH(DayNumber) AND YearNumber = YEAR(DayNumber)
         DayString#{PROP:FontColor} = COLOR:Blue
         DayString#{PROP:FontStyle} = FONT:Bold
      END
      LOOP DayRegion# = DayString#+1 TO ?DayString37
        DayPrev# = DayRegion#-1
        IF DayPrev#{PROP:TEXT} <> '' AND (DEFORMAT(DayPrev#{PROP:TEXT},@n2) + 1) NOT > MaxDays#
           DayRegion#{PROP:TEXT} = DEFORMAT(DayPrev#{PROP:TEXT},@n2) + 1
           IF DEFORMAT(DayRegion#{PROP:Text},@n2) = DAY(DayNumber) AND MonthNumber = MONTH(DayNumber) AND YearNumber = YEAR(DayNumber)
              DayRegion#{PROP:FontColor} = COLOR:Blue
              DayRegion#{PROP:FontStyle} = FONT:Bold
           ELSE
              DayRegion#{PROP:FontColor} = COLOR:Black
              DayRegion#{PROP:FontStyle} = FONT:Regular
           END
        ELSE
           DayRegion#{PROP:TEXT} = ''
        END
      END
      BREAK
    ELSE
      DayString#{PROP:TEXT} = ''
    END
  END
!----------------------------------------------------------------------------
HighlightPassedDate ROUTINE
!|
!| S?t Bevel for aktuel dato
!|
  LOOP DayString# = ?DayString1 TO ?DayString37
    IF DayString#{PROP:Text} = DAY(PassedDate) AND MonthNumber = MONTH(PassedDate) AND YearNumber = YEAR(PassedDate)
      DayRegion# = DayString#-37
      DayRegion#{PROP:BevelOuter} = 1
      DayRegion#{PROP:BevelInner} = -1
      BREAK
    END
  END
!----------------------------------------------------------------------------
ClearHighlight ROUTINE
!|
!| Fjern Bevel
!|
  LOOP DayRegion# = ?DayRegion1 TO ?DayRegion37
    IF DayRegion#{PROP:Bevel} <> 0
      DayRegion#{PROP:Bevel} = 0
!      BREAK
    END
  END
!----------------------------------------------------------------------------
CheckRange ROUTINE
!|
!| Giv fejlmelding hvis ?r ligger uden for lovligt omr?de
!|
  IF    YearNumber < 1801
        PassedDate = 4
        MonthNumber = MONTH(PassedDate)
        YearNumber = YEAR(PassedDate)
        UpdateAssign = TRUE
        BEEP(BEEP:SystemAsterisk)
        MESSAGE('You can not use the calendar for|dates before the year 1801','Ilegal date',Icon:Asterisk)
  ELSIF YearNumber > 2099
        PassedDate = 109211
        MonthNumber = MONTH(PassedDate)
        YearNumber = YEAR(PassedDate)
        UpdateAssign = TRUE
        BEEP(BEEP:SystemAsterisk)
        MESSAGE('You can not use the calendar for|dates after the year 2099','Ilegal date',Icon:Asterisk)
 END
 IF UpdateAssign
    DO SetWindowTitle
  END
  DO ClearHighlight
  IF UpdateAssign
    DO AssignAllStrings
  END
  DO HighlightPassedDate
  UpdateAssign = FALSE

PopupTime            PROCEDURE (*? PassedTime, Byte FixYear, <LONG XPos>, <Long YPos>) ! Declare Procedure
  CODE                                                     ! Begin processed code
    MESSAGE('Time popup not implemented yet!||Just type into the entry field for now...')
GetRatio             PROCEDURE (*LONG p_DuPiX, *LONG p_DuPiY) ! Declare Procedure
  CODE                                                     ! Begin processed code
    ! ES Procedure, ratio of Dialog units to pixels

    DUwidth#              = Target{prop:width}
    DUheight#             = Target{prop:height}

    PixelsState#          = Target{prop:pixels}
    Target{prop:pixels}   = 1

    PIwidth#              = Target{prop:width}
    PIheight#             = Target{prop:height}
    Target{prop:pixels}   = PixelsState#

    p_DuPiX               = DUwidth# / PIwidth#
    p_DuPiY               = DUheight# / PIheight#
