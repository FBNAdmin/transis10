

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Month_Week           FUNCTION (p:Date)                     ! Declare Procedure
LOC:Week             LONG                                  !
LOC:Days_Diff        LONG                                  !
LOC:Beginning        LONG                                  !
  CODE                                                     ! Begin processed code
    ! (p:Date)
    ! DATE(mm,dd,yy)

    LOC:Beginning   = DATE(MONTH(p:Date), 1, YEAR(p:Date))
    LOC:Days_Diff   = p:Date - LOC:Beginning + 1 + (LOC:Beginning % 7)

    LOC:Week        = (LOC:Days_Diff / 7)
    IF (LOC:Days_Diff % 7) ~= 0
       LOC:Week    += 1
    .

    RETURN(LOC:Week)
