

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

MyPopup              FUNCTION (String Items, Long XPos, Long YPos) ! Declare Procedure
ShowPoint       GROUP
x       SIGNED
y       SIGNED
                .

LOC:DuPiX       LONG
LOC:DuPiY       LONG

!ShowPoint       LIKE(POINT)

  CODE                                                     ! Begin processed code
    ! (String Items, Long XPos, Long YPos)
    GetRatio(LOC:DuPiX, LOC:DuPiY)

    ShowPoint.X = XPos / LOC:DuPiX
    ShowPoint.Y = YPos / LOC:DuPiY

    !ClientToScreen(0{Prop:Handle}, ShowPoint)           ! WinAPI

    RETURN( Popup(Items, ShowPoint.X, ShowPoint.Y) )
SQL_Deformat         FUNCTION (p:Str)                      ! Declare Procedure
LOC_Use_Str             &STRING
Str_Cls     CLASS, TYPE
Str_1           &STRING
Init            PROCEDURE(STRING)
Destruct        PROCEDURE
            .


LOC_My_Str_Ele      Str_Cls
  CODE                                                     ! Begin processed code
    ! (p:Str)
    ! (*STRING),STRING
    IF CLIP(p:Str) ~= '' AND NUMERIC(CLIP(p:Str)) = TRUE
!       LOC_Use_Str            &= NEW(String(LOC:Len))
!       LOC_Use_Str             = DEFORMAT(p:Str)
!
!       LOC_My_Str_Ele.Init( LOC_Use_Str )
!
!       DISPOSE(LOC_Use_Str)
!
!       RETURN( LOC_My_Str_Ele.Str_1 )          ! Clipping not needed - string is made right size

       RETURN( DEFORMAT(p:Str) )
    ELSE
       RETURN( '' )
    .


! ----------- Class Procs -----------------
Str_Cls.Init        PROCEDURE(STRING Our_Str)
    CODE
    IF LEN(CLIP(Our_Str)) > 0
       SELF.Str_1     &= NEW(STRING( LEN(CLIP(Our_Str)) ))
    ELSE
       SELF.Str_1     &= NEW(STRING( 1 ))
    .

    SELF.Str_1      = Our_Str
    RETURN



Str_Cls.Destruct    PROCEDURE()
    CODE
    DISPOSE(SELF.Str_1)
    RETURN
Get_Tag              FUNCTION (p_Str, p_Remove, p_TagLeft, p_TagRight) ! Declare Procedure
LOC:Tag_Left         CSTRING(20)                           !
LOC:Tag_Right        CSTRING(20)                           !
LOC:Pos_Left         LONG                                  !
LOC:Pos_Right        LONG                                  !
Str_Cls     CLASS,TYPE
Str_1           &STRING
Init            PROCEDURE(STRING)
Destruct        PROCEDURE
            .


LOC_My_Str_Ele      Str_Cls
LOC_Use_Str         &STRING
  CODE                                                     ! Begin processed code
    ! (p_Str, p_Remove, p_TagLeft, p_TagRight)
    ! (*STRING, BYTE=0, <STRING>, <STRING>), STRING
    LOC:Tag_Left    = '['
    LOC:Tag_Right   = ']'

    IF ~OMITTED(3)
       LOC:Tag_Left     = p_TagLeft
    .
    IF ~OMITTED(4)
       LOC:Tag_Right    = p_TagRight
    .

    ! Returns 1st tag found or nothing if no tag found

    LOC:Pos_Left        = INSTRING(CLIP(LOC:Tag_Left), p_Str, 1)
    LOC:Pos_Right       = INSTRING(CLIP(LOC:Tag_Right), p_Str, 1)

    IF LOC:Pos_Left < LOC:Pos_Right
       Len_#    = LEN(CLIP(p_Str))
       LOC_Use_Str     &= NEW(String( Len_# ))

       LOC_Use_Str      = SUB(p_Str, LOC:Pos_Left + 1, LOC:Pos_Right - LOC:Pos_Left - 1)

       LOC_My_Str_Ele.Init( LOC_Use_Str )
       DISPOSE(LOC_Use_Str)

       IF p_Remove = TRUE
          p_Str         = SUB(p_Str, 1, LOC:Pos_Left - 1)  &  SUB(p_Str, LOC:Pos_Right + 1, LEN(p_Str) - LOC:Pos_Right)
       .
    ELSE
       LOC_My_Str_Ele.Init( ' ' )           ! Zero length string will be created, problem??
    .

    RETURN( LOC_My_Str_Ele.Str_1 )          ! Clipping not needed - string is made right size
! ----------- Class Procs -----------------
Str_Cls.Init        PROCEDURE(STRING Our_Str)
    CODE
    IF LEN(CLIP(Our_Str)) > 0
       SELF.Str_1     &= NEW(STRING( LEN(CLIP(Our_Str)) ))
    ELSE
       SELF.Str_1     &= NEW(STRING( 1 ))
    .

    SELF.Str_1      = Our_Str
    RETURN



Str_Cls.Destruct    PROCEDURE()
    CODE
    DISPOSE(SELF.Str_1)
    RETURN
Date_Time_Advance    PROCEDURE (p_Date, p_Time, p_Option, p_Val) ! Declare Procedure
LOC:Tmp              LONG                                  !
LOC:Tmp2             LONG                                  !
  CODE                                                     ! Begin processed code
    ! Date_Time_Advance
    ! (*DATE, <*TIME>, BYTE, LONG)
    ! (p_Date, p_Time, p_Option, p_Val)
    !
    ! p_Option
    !       1 - Time
    !       2 - Days
    !       3 - Weeks
    !       4 - Months
    !       5 - Quarters
    !       6 - Years
    !
    ! 20 April 08 - Extended to allow for negative months (only months... at this stage)

!    message('1 here: ' & p_Val & '||p_Date: ' & FORMAT(p_Date,@d6) & '||p_Option: ' & p_Option & |
!                    '||MONTH(p_Date): ' & MONTH(p_Date))

    CASE p_Option
    OF 1                         ! Time
       IF OMITTED(2)
          MESSAGE('Time option but no time reference passed.', 'Date_Time_Advance (ikb lib)', ICON:Hand)
       ELSE
          IF p_Time + p_Val > 8640000
             LOC:Tmp   = INT((p_Time + p_Val) / 8640000)       ! Days
             LOC:Tmp2  = (p_Time + p_Val) % 8640000            ! Time

             p_Date   += LOC:Tmp
             p_Time    = LOC:Tmp2   
          ELSE
             p_Time   += p_Val
       .  .
    OF 2                         ! Days
       p_Date      += p_Val
    OF 3                         ! Weeks
       p_Date      += (p_Val * 7)
    OF 4                         ! Months
       IF p_Val < 0 OR MONTH(p_Date) <= p_Val      ! eg   1 & -1;  3 & -20
          p_Val    += MONTH(p_Date)                 ! eg   -1 + 1 = 0;  -20 + 3 = -17
          LOOP
             p_Val += 12                            ! eg  -2 + 12 = 10;  -20 + 12 = -8;  -1 + 12 = 11;  -12 + 12 = 0

             IF p_Val >= 0
                ! Go back 1 year and set month
                p_Date = DATE(p_Val, DAY(p_Date), YEAR(p_Date) - 1)
                BREAK
             ELSE
                ! Go back 1 year and loop
                p_Date = DATE(MONTH(p_Date), DAY(p_Date), YEAR(p_Date) - 1)
          .  .
       ELSE
          p_Date    = DATE(MONTH(p_Date) + p_Val, DAY(p_Date), YEAR(p_Date))
       .
    OF 5                         ! Quarters
       p_Date       = DATE(MONTH(p_Date) + (3 * p_Val), DAY(p_Date), YEAR(p_Date))
    OF 6                         ! Years
       p_Date       = DATE(MONTH(p_Date), DAY(p_Date), YEAR(p_Date) + p_Val)
    ELSE
       MESSAGE('Unknown option: ' & p_Option, 'Date_Time_Advance (ikb lib)', ICON:Hand)
    .
Replace_Chars_old    FUNCTION (p:Str, p:Chars, p:Rep, p:Occurances, p:Method, p:Quiet) ! Declare Procedure
LOC:Found_At         ULONG                                 !
LOC:Replacement      STRING(1000)                          !
LOC:Count            LONG                                  !
LOC:Delim_Pos        ULONG                                 !Position of deliminator
LOC:Delim_Len        ULONG                                 !Length of the deliminator
LOC:Delim            CSTRING(50)                           !
LOC:Len              SHORT                                 !
LOC_Ret_Str         &CSTRING
LOC_Use_Str         &CSTRING
Str_Cls                   CLASS
Str_1           &STRING
Init            PROCEDURE(STRING)
Destruct        PROCEDURE
            .


LOC_My_Str_Ele      Str_Cls
  CODE                                                     ! Begin processed code
    ! (STRING, *CSTRING, STRING, BYTE=0, BYTE=0, BYTE=0),STRING
    ! (p:Str, p:Chars, p:Rep, p:Occurances, p:Method, p:Quiet)
    !
    ! p:Method
    !   0       - Replacement must match the size of the to be replaced, otherwise it will be padded/truncated
    !             String cannot be bigger than passed in p:Str
    !   1 (X)   -

    IF CLIP(p:Rep) = CLIP(p:Chars)
       ! This will cause an infinite loop here
       IF p:Quiet = FALSE
          MESSAGE('Cannot replace string with same string!','Replace_Chars - Bad data',ICON:Hand)
       .
       RETURN(p:Str)
    .
    IF CLIP(p:Rep) = ''
       ! Then what? will it work ok?
    .
    IF LEN(p:Chars) = 0
       ! can't do this
       IF p:Quiet = FALSE
          MESSAGE('Cannot replace nothing!','Replace_Chars - Bad data - 2',ICON:Hand)
       .
       RETURN(p:Str)
    .

    LOC:Replacement = p:Rep

    IF p:Method = 0
       ! If there are more characters in the string to be replaced than the replacement then pad replacement
       ! with blanks    ELSE chop off characters off the end?
       IF LEN(p:Chars) > LEN(CLIP(LOC:Replacement))
          ! Add on chars to the front
          LOOP ( LEN(CLIP(LOC:Replacement)) - LEN(p:Chars) ) TIMES
             LOC:Replacement = ' ' & CLIP(LOC:Replacement)
          .
       ELSIF LEN(p:Chars) < LEN(CLIP(LOC:Replacement))
          ! Truncate replacement chars
          LOC:Replacement = SUB(LOC:Replacement,1, LEN(p:Chars))
       .
    ELSE
       ! (p:Delim_List, p:Delim, p:Chop_Out - optional defaulted)
       !  String, Delim, Chop
       LOC:Len         = LEN(p:Str) * 1.6

       LOC_Ret_Str    &= NEW(CSTRING(LOC:Len))
       LOC_Use_Str    &= NEW(CSTRING(LOC:Len))

       LOC_Ret_Str     = ''
       LOC_Use_Str     = p:Str
    .


    LOC:Count = 0

    LOOP
       IF p:Occurances ~= 0
          IF p:Occurances <= LOC:Count
             BREAK
       .  .

       LOC:Count  += 1

       IF p:Method = 0
          LOC:Found_At = INSTRING(p:Chars, CLIP(p:Str), 1)
          IF LOC:Found_At > 0
             p:Str[ LOC:Found_At : ( LOC:Found_At + LEN(p:Chars) - 1 ) ] = CLIP(LOC:Replacement)
          ELSE
             BREAK
          .
       ELSE
          ! New string
          LOC:Found_At         = INSTRING(p:Chars, CLIP(LOC_Use_Str), 1)

          IF LOC:Found_At > 0
             IF LOC:Found_At > 1
                LOC_Ret_Str    = LOC_Ret_Str & LOC_Use_Str[1 : (LOC:Found_At - 1)] & CLIP(LOC:Replacement)
             ELSE
                LOC_Ret_Str    = LOC_Ret_Str & CLIP(LOC:Replacement)
             .

             ! If our temp str is greater than last found position plus length of delim
             IF LEN(LOC_Use_Str) > (LOC:Found_At + LEN(p:Chars))
                LOC_Use_Str    = LOC_Use_Str[(LOC:Found_At + LEN(p:Chars)) : LEN(CLIP(LOC_Use_Str))]
             ELSE
                LOC_Use_Str    = ''
                BREAK
             .
          ELSE
             LOC_Ret_Str    = LOC_Ret_Str & LOC_Use_Str
             BREAK
          .
          !LOC_Ret_Str    = LOC_Ret_Str & LOC_Use_Str
    .  .

    IF p:Method = 0
       RETURN(p:Str)
    ELSE
       DISPOSE(LOC_Use_Str)

       LOC_My_Str_Ele.Init(LOC_Ret_Str)

       DISPOSE(LOC_Ret_Str)

       RETURN(CLIP(LOC_My_Str_Ele.Str_1))
    .

! ----------- Class Procs -----------------
Str_Cls.Init        PROCEDURE(STRING Our_Str)
    CODE
    Len_SCls_#      = LEN(CLIP(Our_Str))

    SELF.Str_1     &= NEW(STRING(Len_SCls_#))

    SELF.Str_1      = Our_Str

    !MESSAGE('Our_Str: ' & CLIP(Our_Str) & '||Str_Cls.Str_1: ' & CLIP(Str_Cls.Str_1))

    RETURN



Str_Cls.Destruct    PROCEDURE()
    CODE
    DISPOSE(SELF.Str_1)
    RETURN
Wrap_CSV_Field       FUNCTION (STRING __str1)              ! Declare Procedure
Ret                  STRING(2100)                          !
l_Pos                ULONG                                 !
l_Ins                ULONG                                 !
  CODE                                                     ! Begin processed code
   ! any " make """

   l_Ins = 0
   LOOP
      l_Ins = INSTRING('"', __str1, 1, 1)
      if (l_Ins <= 0)
        BREAK    
      .
      
      Ret = CLIP(Ret) & SUB(__str1, 1, l_Ins) & '"'
      __str1 = SUB(__str1, l_Ins+1, LEN(__str1))
   .

   Ret = CLIP(Ret) & CLIP(__str1)

   RETURN Ret
      
      
      
