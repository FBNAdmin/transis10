

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Week_Day_No          FUNCTION (p:Date)                     ! Declare Procedure
LOC:Week             BYTE                                  !
  CODE                                                     ! Begin processed code
    ! (p:Date)

    ! Dividing a standard date by modulo 7 gives you the day of the week: zero = Sunday,
    ! one = Monday, etc.

    LOC:Week    = p:Date % 7

    RETURN(LOC:Week)
