

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

DNibble_To_Hex       FUNCTION (p:Byte)                     ! Declare Procedure
LOC:Chr              STRING(1)                             !
  CODE                                                     ! Begin processed code
    ! (p:Byte)

    CASE p:Byte
    OF 0 TO 9
       LOC:Chr  = p:Byte
    OF 10
       LOC:Chr  = 'A'
    OF 11
       LOC:Chr  = 'B'
    OF 12
       LOC:Chr  = 'C'
    OF 13
       LOC:Chr  = 'D'
    OF 14
       LOC:Chr  = 'E'
    OF 15
       LOC:Chr  = 'F'
    .

    RETURN(LOC:Chr)
