

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

URLHandler           PROCEDURE (whandle, URL, p:Params, p:LauchDir) ! Declare Procedure
URLBuffer            CSTRING(500)
URLParms             CSTRING(1000)
URLDir               CSTRING(1000)

EmptyString          CSTRING(254)
  CODE                                                     ! Begin processed code
    ! (whandle, URL, p:Params, p:LauchDir)
    IF ~OMITTED(3)
       URLParms = CLIP(p:Params)
    ELSE
       URLParms = ''
    .

    IF ~OMITTED(4)
       URLDir   = CLIP(p:LauchDir)
    ELSE
       URLDir   = ''
    .


    URLBuffer   = CLIP(URL)

    !STOP('URLBuffer: ' & CLIP(URLBuffer))

    EmptyString = 'open'

                 !ShellExecute(UNSIGNED,LONG,*CSTRING,LONG,*CSTRING,SIGNED)

!    x#          = ShellExecute(whandle, 0, URLBuffer, 0, EmptyString, 1)
    x#          = ShellExecute(whandle, EmptyString, URLBuffer, URLParms, URLDir, 1)

    !STOP('x: ' & x#)


    RETURN



!HINSTANCE ShellExecute(          HWND hwnd,
!    LPCTSTR lpOperation,
!    LPCTSTR lpFile,
!    LPCTSTR lpParameters,
!    LPCTSTR lpDirectory,
!    INT nShowCmd
!);
