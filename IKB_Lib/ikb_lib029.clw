

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Quarter              FUNCTION (p:Date)                     ! Declare Procedure
LOC:Month            BYTE                                  !
LOC:Quarter          BYTE                                  !
  CODE                                                     ! Begin processed code
    ! (p:Date)
    LOC:Month   = MONTH(p:Date)

    CASE LOC:Month
    OF 1  TO 3
       LOC:Quarter  = 1
    OF 4  TO 6
       LOC:Quarter  = 2
    OF 7  TO 9
       LOC:Quarter  = 3
    OF 10 TO 12
       LOC:Quarter  = 4
    .

    RETURN(LOC:Quarter)
