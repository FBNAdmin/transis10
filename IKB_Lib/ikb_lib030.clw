

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Remove_White_Space   FUNCTION (p:Str)                      ! Declare Procedure
LOC:Idx              LONG                                  !
LOC_Ret_Str         &CSTRING
LOC:Len             SHORT
  CODE                                                     ! Begin processed code
    ! p:Str

    LOC:Len         = LEN(p:Str)
    LOC_Ret_Str    &= NEW(CString(LOC:Len+1))

    LOOP LOC:Idx = 1 TO LEN(CLIP(p:Str))
                        ! tab                     ! space
       IF (VAL(p:Str[LOC:Idx]) = 9) OR (VAL(p:Str[LOC:Idx]) = 32)
          ! Ignore
       ELSE
          LOC_Ret_Str = LOC_Ret_Str & p:Str[LOC:Idx]
    .  .

    p:Str   = LOC_Ret_Str

    DISPOSE(LOC_Ret_Str)

    RETURN(p:Str)
