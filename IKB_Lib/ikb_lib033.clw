

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

StrB_To_Octet        FUNCTION (p:StrB)                     ! Declare Procedure
LOC:Octet            STRING(2)                             !
LOC:Byte             BYTE                                  !
LOC:DStr             STRING(10)                            !
LOC:Padding          CSTRING(2)                            !
  CODE                                                     ! Begin processed code
    ! (p:StrB)
    ! Hopefully all 1 and 0's only!

    LOC:Byte    = StrB_To_Byte(p:StrB)

    !MESSAGE('LOC:Byte: ' & LOC:Byte)

    LOC:DStr    = Dec_To_Hex(LOC:Byte)

    !MESSAGE('LOC:DStr: ' & LOC:DStr)

    LOC:Padding = '0'

    LOC:Octet   = Pad_Str(LOC:DStr, 2, LOC:Padding)

    !MESSAGE('LOC:Octet: ' & LOC:Octet)


    RETURN(LOC:Octet)
