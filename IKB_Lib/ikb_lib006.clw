

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Count_Delim_Str_Elements FUNCTION (p:String, p:Delim, p:Compress) ! Declare Procedure
LOC:Result           LONG                                  !
LOC_String_Q        QUEUE,PRE(LO_SQ)
Str                     &STRING
                    .


L:New_Str           &STRING
  CODE                                                     ! Begin processed code
    ! (p:String, p:Delim, p:Compress)
    ! (*STRING, STRING, BYTE=0),LONG,PROC

    ! p:Compress
    !   - this options removes white space from the string except for 1 leading char after each comma
    !   - empty elements are also removed from the list
    !
    ! The returned number is always the number of elements originally in the string including
    ! empty elements.

    FREE(LOC_String_Q)

    Len_#       = LEN(CLIP(p:String))

    L:New_Str  &= NEW(STRING(Len_#))
    L:New_Str   = p:String

    LOOP
       IF CLIP(L:New_Str) = ''
          BREAK
       .

       LO_SQ:Str   &= NEW(STRING(Len_#))
       LO_SQ:Str    = Get_1st_Element_From_Delim_Str(L:New_Str, p:Delim, TRUE)

       IF p:Compress = TRUE
          LO_SQ:Str = LEFT(LO_SQ:Str)
       .

       ADD(LOC_String_Q)
    .


    IF p:Compress = TRUE
       p:String = ''
    .

    Idx_#        = 0
    LOOP
       Idx_# += 1
       GET(LOC_String_Q, Idx_#)
       IF ERRORCODE()
          BREAK
       .

       IF p:Compress = TRUE
          IF CLIP(LO_SQ:Str) ~= ''                      ! There is an element in there
             IF CLIP(p:String) = ''
                p:String  = LO_SQ:Str
             ELSE
                p:String  = CLIP(p:String)    & p:Delim & ' ' &   LO_SQ:Str
       .  .  .

       DISPOSE(LO_SQ:Str)
    .

    LOC:Result   = RECORDS(LOC_String_Q)

    FREE(LOC_String_Q)

    RETURN(LOC:Result)
