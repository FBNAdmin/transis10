

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Check_File_Existence FUNCTION (LOC:File, p:Option)         ! Declare Procedure
LOC:C_String         CSTRING(1000)                         !
LOC:Returned         BYTE                                  !
LOC:Api_Returned     ULONG                                 !
  CODE                                                     ! Begin processed code
    ! (LOC:File, p:Option)

    LOC:C_String        = CLIP(LOC:File)
    !RETURN(GetFileAttributesA(LOC:C_String))

    LOC:Api_Returned    = GetFileAttributesA(LOC:C_String)

!    STOP('Returned: ' & LOC:Returned)

    ! FFFFFFFFh = 4294967295

    IF p:Option = 0
       IF LOC:Api_Returned = 4294967295                ! > 128
          LOC:Returned      = 0
       ELSE
          LOC:Returned      = 1
       .
    ELSE
       LOC:Returned         = LOC:Api_Returned
    .


    RETURN(LOC:Returned)
