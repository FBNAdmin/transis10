

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Weeks                FUNCTION (p:Date, p:Month, p:Year)    ! Declare Procedure
LOC:Week             LONG                                  !
LOC:Days_Diff        LONG                                  !
LOC:Beginning        LONG                                  !
LOC:Year             SHORT                                 !
  CODE                                                     ! Begin processed code
    ! (p:Date, <p:Month = 1>, <p:Year>)
    ! DATE(mm,dd,yy)
    IF OMITTED(3)
       LOC:Year = YEAR(p:Date)
    ELSE
       LOC:Year = p:Year
    .

    LOC:Beginning   = DATE(p:Month, 1, LOC:Year)
    LOC:Days_Diff   = p:Date - LOC:Beginning + 1

    LOC:Week        = (LOC:Days_Diff / 7)
    IF (LOC:Days_Diff % 7) ~= 0
       LOC:Week += 1
    .

    RETURN(LOC:Week)
