

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Replace_Chars        FUNCTION (p:Str, p:Chars, p:Rep, p:Occurances, p:Method, p:Quiet) ! Declare Procedure
LOC:Found_At         ULONG                                 !
LOC:Replacement      STRING(1000)                          !
LOC:Count            LONG                                  !
LOC:Occurances       LONG                                  !
LOC:ReplacementsDone LONG                                  !
LOC_Ret_Str         &CSTRING
LOC_Use_Str         &STRING
Str_Cls                   CLASS
Str_1           &STRING
Init            PROCEDURE(STRING)

Replace_        PROCEDURE(STRING s2)

Destruct        PROCEDURE
            .


LOC_My_Str_Ele      Str_Cls
  CODE                                                     ! Begin processed code
    ! (STRING, *CSTRING, STRING, <*LONG>, BYTE=0, BYTE=0),STRING
    ! (p:Str, p:Chars, p:Rep, p:Occurances, p:Method, p:Quiet)
    !
    ! p:Method
    !   0       - Replacement must match the size of the to be replaced, otherwise it will be padded/truncated
    !             String cannot be bigger than passed in p:Str
    !   1 (X)   - No limit, replaces only with a clipped p:Rep (26/11/07)
    !             (Was limited to 60 % bigger and there were at least 3 bugs) (26/11/07)
    !             Flaws    - Replacement is a string, trailing spaces will be lost in current code - ok?
    !
    !   Limits:
    !             LOC:Replacement is limited to 1000 chars

    LOC:Occurances      = 0
    IF ~OMITTED(4)
       LOC:Occurances   = p:Occurances
    .

    IF CLIP(p:Rep) = CLIP(p:Chars)
       ! This will cause an infinite loop here
       IF p:Quiet = FALSE
          MESSAGE('Cannot replace string with same string!','Replace_Chars - Bad data',ICON:Hand)
       .
       RETURN(p:Str)
    .
    IF CLIP(p:Rep) = ''
       ! Then what? will it work ok?
    .
    IF LEN(p:Chars) = 0
       ! can't do this
       IF p:Quiet = FALSE
          MESSAGE('Cannot replace nothing!','Replace_Chars - Bad data - 2',ICON:Hand)
       .
       RETURN(p:Str)
    .

    LOC:Replacement = p:Rep

    IF p:Method = 0
       ! If there are more characters in the string to be replaced than the replacement then pad replacement
       ! with blanks    ELSE chop off characters off the end?
       IF LEN(p:Chars) > LEN(CLIP(LOC:Replacement))
          ! Add on chars to the front
          LOOP ( LEN(CLIP(LOC:Replacement)) - LEN(p:Chars) ) TIMES
             LOC:Replacement = ' ' & CLIP(LOC:Replacement)
          .
       ELSIF LEN(p:Chars) < LEN(CLIP(LOC:Replacement))
          ! Truncate replacement chars
          LOC:Replacement = SUB(LOC:Replacement,1, LEN(p:Chars))
       .
    ELSE
       ! (p:Delim_List, p:Delim, p:Chop_Out - optional defaulted)
       !  String, Delim, Chop
       LOC_Ret_Str    &= NEW(CSTRING( LEN(p:Str) * 1.2 ))       ! 20 % bigger
       LOC_Use_Str    &= NEW(STRING(LEN( CLIP(p:Str) )))

       LOC_Ret_Str     = ''
       LOC_Use_Str     = p:Str
    .


    LOC:Count = 0

    LOOP
       IF LOC:Occurances ~= 0
          IF LOC:Occurances <= LOC:Count
             BREAK
       .  .

       LOC:Count  += 1

       IF p:Method = 0
          LOC:Found_At = INSTRING(p:Chars, CLIP(p:Str), 1)
          IF LOC:Found_At > 0
             p:Str[ LOC:Found_At : ( LOC:Found_At + LEN(p:Chars) - 1 ) ] = CLIP(LOC:Replacement)

             LOC:ReplacementsDone  += 1
          ELSE
             BREAK
          .
       ELSE
          ! New string
          LOC:Found_At          = INSTRING(p:Chars, CLIP(LOC_Use_Str), 1)

          IF LOC:Found_At = 0                                 ! Not found
             LOC_My_Str_Ele.Replace_(CLIP(LOC_Use_Str))
             !LOC_Ret_Str       = LOC_Ret_Str & LOC_Use_Str     ! Add remaining to return and exit
             BREAK
          ELSE
             IF LOC:Found_At = 1                              ! Found at position 1
                LOC_My_Str_Ele.Replace_(CLIP(LOC:Replacement))
                !LOC_Ret_Str    = LOC_Ret_Str & CLIP(LOC:Replacement)
             ELSE                                             ! Found, add to return, cut out and replace found
                LOC_My_Str_Ele.Replace_(LOC_Use_Str[1 : (LOC:Found_At - 1)] & CLIP(LOC:Replacement))
                !LOC_Ret_Str    = LOC_Ret_Str & LOC_Use_Str[1 : (LOC:Found_At - 1)] & CLIP(LOC:Replacement)
             .

             LOC:ReplacementsDone  += 1

             ! If our remaining chars (temp str) is greater than last found position plus length of replacement (delim)
             IF LEN(CLIP(LOC_Use_Str)) > (LOC:Found_At + LEN(p:Chars) - 1)      ! If the replacement can fit in the remaining
                ! 1234567890      str
                !     5           found at
                !     567         found string
                ! Chop down the remaining by the already processed bit
                LOC_Use_Str     = LOC_Use_Str[(LOC:Found_At + LEN(p:Chars)) : LEN(CLIP(LOC_Use_Str))]
             ELSE
                LOC_Use_Str     = ''
                BREAK
       .  .  .
          !LOC_Ret_Str    = LOC_Ret_Str & LOC_Use_Str
       LOC_My_Str_Ele.Replace_(CLIP(LOC_Use_Str))
    .



    IF ~OMITTED(4)
       p:Occurances     = LOC:ReplacementsDone
    .

    IF p:Method = 0
       RETURN(p:Str)
    ELSE
       DISPOSE(LOC_Use_Str)

       LOC_My_Str_Ele.Init(LOC_Ret_Str)

       DISPOSE(LOC_Ret_Str)

       RETURN(CLIP(LOC_My_Str_Ele.Str_1))
    .

! ----------- Class Procs -----------------
Str_Cls.Init        PROCEDURE(STRING p_Our_Str)
Len_        LONG
    CODE
    Len_            = LEN(CLIP(p_Our_Str))

    SELF.Str_1     &= NEW(STRING(Len_))       

    SELF.Str_1      = p_Our_Str

    !MESSAGE('Our_Str: ' & CLIP(Our_Str) & '||Str_Cls.Str_1: ' & CLIP(Str_Cls.Str_1))

    RETURN



Str_Cls.Destruct    PROCEDURE()
    CODE
    DISPOSE(SELF.Str_1)
    RETURN



! -----   This function is in the class but in fact forms no part of it - just using the container     -----
Str_Cls.Replace_        PROCEDURE(STRING s2)
Tmp     &CSTRING
    CODE
    ! (STRING, *CSTRING, STRING, BYTE=0, BYTE=0, BYTE=0),STRING
    ! (p:Str, p:Chars, p:Rep, p:Occurances, p:Method, p:Quiet)

    ! If p1 + p2 wont fit in p1
    IF SIZE(LOC_Ret_Str) - 1 < LEN(LOC_Ret_Str) + LEN(CLIP(s2))
       Tmp          &= NEW(CSTRING( (LEN(LOC_Ret_Str) + LEN(CLIP(s2))) * 1.2 ))
       Tmp           = LOC_Ret_Str
       DISPOSE(LOC_Ret_Str)
       LOC_Ret_Str  &= Tmp
    .

    LOC_Ret_Str      = LOC_Ret_Str & CLIP(s2)
    RETURN

