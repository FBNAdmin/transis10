

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Insert_Into_Delim_Str_2 FUNCTION (p:String, p:Delim, p:Element, p:Position) ! Declare Procedure
L:From               LONG                                  !
L:Pos                LONG                                  !
L:Count              LONG                                  !
LOC_String_Q        QUEUE,PRE(LO_SQ)
Str                     &STRING
                    .

  CODE                                                     ! Begin processed code
    ! (p:String, p:Delim, p:Element, p:Position)
    ! (*STRING, STRING, STRING, SHORT=0),LONG,PROC

    IF p:Position = 0                                                           ! End
       p:String     = CLIP(p:String) & CLIP(p:Delim) & p:Element
    ELSIF p:Position = 1                                                        ! Begining
       p:String     = p:Element & CLIP(p:Delim) & CLIP(p:String)
    ELSE
       L:From   = 1
       LOOP
          L:Pos     = INSTRING(p:Delim, p:String, 1, L:From)
          IF L:Pos <= 0
             BREAK
          .
          L:Count  += 1

          IF L:Count >= p:Position                                              ! Position 2 and up
             ! Found our spot
             p:String   = p:String[1 : L:Pos] &    p:Element    & p:Delim & p:String[L:Pos+1 : LEN(p:String)]
             BREAK
          .

          L:From    = L:Pos + 1
       .

       IF L:Pos <= 0                                                            ! No insert pos found - add delims to end
          LOOP p:Position - L:Count TIMES                                       ! 12 - 9 = 3
             p:String   = CLIP(p:String) &    p:Delim
          .

          p:String   = CLIP(p:String) & p:Element
    .  .

    RETURN( L:Count )
Get_Element_From_Delim_Str FUNCTION (p:String, p:Delim, p:Position) ! Declare Procedure
L:Len                LONG                                  !
L:Start              LONG                                  !
LOC:Found            LONG                                  !
LOC:Pos              LONG                                  !
L:D_Len              LONG                                  !
LOC_String_Q        QUEUE,PRE(LO_SQ)
Str                     &STRING
                    .

  CODE                                                     ! Begin processed code
    ! (p:String, p:Delim, p:Position)

    ! xxx, |, 1
    ! xxx|yyy, |, 2
    ! xxx|yyy|zzz, |, 2

    ! 1,2,3,4
    !
    ! Position  1   | 2  2  | 1 | 2  2
    ! Found     1     1  2    1   1  2
    ! Pos       2     2  4    4   4  8
    ! Start     1     3  3    1   5  5
    ! End       2        4    4      8


    L:D_Len     = LEN(CLIP(p:Delim))
    IF L:D_Len = 0                      ! Assume blank search
       L:D_Len  = 1
    .

    L:Start     = 1

    LOOP p:Position TIMES
       LOC:Found   += 1                 ! Position 1 is before any delims
       LOC:Pos      = INSTRING(p:Delim[1 : L:D_Len], p:String, 1, L:Start)
       IF LOC:Pos <= 0
          BREAK
       .


       IF LOC:Found = p:Position
          BREAK
       .

       L:Start      = LOC:Pos + 1
    .


    IF LOC:Found = p:Position
       !L:End        = INSTRING(p:Delim[1 : L:D_Len], p:String, 1, L:Start)
       !IF L:End > 0
       !   RETURN( SUB(p:String, L:Start, L:End - 1 ) )
       IF LOC:Pos > 0
          RETURN( SUB(p:String, L:Start, LOC:Pos - L:Start + 1 - 1 ) )              ! -1 for the delim
       ELSE
          RETURN( SUB(p:String, L:Start, LEN(CLIP(p:String)) - L:Start + 1 ) )      ! no end delim
       .
    ELSE
       RETURN( '' )
    .
Add_to_List          PROCEDURE (p:Add, p:List, p:Delim, p:Option, p:Prefix, p:Beginning, p:IgnoreIfExists) ! Declare Procedure
LOC:CStr             CSTRING(20)                           !
  CODE                                                     ! Begin processed code
    ! Add_to_List
    ! (p:Add , p:List , p:Delim, p:Option, p:Prefix, p:Beginning, p:IgnoreIfExists)
    ! (STRING, *STRING, STRING , BYTE=0  , <STRING>, BYTE=0, BYTE=0)
    !   1         2        3       4          5        6      7
    ! p:Add
    !   The bit to be added
    ! p:Option
    !   0   - Left new element if list blank
    !   1   - No Left (Normal)
    ! p:Prefix
    !   If the item is populated then prefix with this...
    ! p:Beginning
    !   Add to the beginning of the list

    ! ??? 8 Oct 06 - potential problem is that the Add(ition) cannot be blank, otherwise no element
    ! will be added.  Not sure if any code is relying on this quirk so no change done to correct this
    
    ! Match_Element_In_Delim_Str
    ! (p:Delim_List, p:Delim, p:Element, p:Chop_Out, p:Spaced)
    
    IF p:IgnoreIfExists = TRUE
       IF Match_Element_In_Delim_Str(p:List, p:Delim, p:Add) > 0
          ! Then nothing to do!
          RETURN
       .
    .
    

    IF ~OMITTED(5)                                      ! Is p:Prefix not Omittied
       IF CLIP(p:Prefix) ~= '' AND CLIP(p:Add) ~= ''
          LOC:CStr  = p:Prefix
    .  .

    IF CLIP(p:List) = ''
       p:List       = LOC:CStr & p:Add                  ! Then add prefix (if set) and return Add

       IF p:Option = 0
          p:List    = LEFT(p:List)
       .
    ELSE
       IF CLIP(p:Add) ~= ''
          IF p:Beginning = TRUE
             p:List    = LOC:CStr & p:Add & p:Delim & CLIP(p:List)
          ELSE
             p:List    = CLIP(p:List) & p:Delim & LOC:CStr & p:Add
    .  .  .

    RETURN
Get_File_List        FUNCTION (FILE:queue_ikb p_FileQ, p_Path, p_Extensions, p_SubDirs, p_Attributes, p_SearchString) ! Declare Procedure
LOC:Locals           GROUP,PRE(LOC)                        !
Current_Path         STRING(5000)                          !
Set_Path             STRING(5000)                          !
Recs                 LONG                                  !
Idx                  LONG                                  !
Depth                LONG                                  !
Result               LONG                                  !
                     END                                   !
LOC:Extensions_Q     QUEUE,PRE(L_EQ)                       !
Extension            STRING(3)                             !
                     END                                   !
AllFiles QUEUE(File:queue_IKB),PRE(FIL)        !Inherit exact declaration of File:queue
         END

AllDirs QUEUE(File:queue_IKB),PRE(DIR)        !Inherit exact declaration of File:queue
Depth       LONG
         END


!FILE:queue_ikb QUEUE,PRE(File_IKB),TYPE
!name        STRING(FILE:MAXFILENAME)  !FILE:MAXFILENAME is an EQUATE
!shortname   STRING(13)
!date        LONG
!time        LONG
!size        LONG
!attrib      BYTE                      !A bitmap, the same as the attributes EQUATEs
!Directory   STRING(FILE:MAXFILENAME)
!           END
  CODE                                                     ! Begin processed code
    ! (FILE:queue_ikb p_FileQ, p_Path, p_Extensions, p_SubDirs, p_Attributes, p_SearchString)
    ! (FILE:queue_ikb p_FileQ, STRING, <STRING>    , LONG=1   , LONG=0      , <STRING>      ), LONG, PROC
    !               1            2          3           4           5               6

    ! p_Attributes - as per the equates,
    ! ff_:READONLY  EQUATE(1)       !Not for use as attributes parameter
    ! ff_:HIDDEN    EQUATE(2)
    ! ff_:SYSTEM    EQUATE(4)
    ! ff_:DIRECTORY EQUATE(10H)
    ! ff_:ARCHIVE   EQUATE(20H) 

    ! p_Extensions
    ! Assumed to be a comma delimited list - eg. log, exe, txt

    ! p_SubDirs
    ! > 0   = All
    ! 0     = none
    ! < 0   = depth

    ! p_SearchString
    ! Will not store any entries but ones that match this string

    IF p_Attributes = -1
       p_Attributes         = ff_:READONLY + ff_:HIDDEN + ff_:SYSTEM + ff_:DIRECTORY + ff_:ARCHIVE
    .

    ! Load the extensions Q
    IF ~OMITTED(3)
       LOOP
          IF CLIP(p_Extensions) = ''
             BREAK
          .
          L_EQ:Extension       = LEFT(Get_1st_Element_From_Delim_Str(p_Extensions, ',', TRUE))
          ADD(LOC:Extensions_Q)
    .  .

    IF p_SubDirs ~= 0           ! Then make sure we are also getting directories
       p_Attributes         = BOR(p_Attributes, ff_:DIRECTORY)
    .

    LOC:Current_Path        = PATH()

    ! Check if we have a file or path
    CLEAR(AllDirs)

    IF SUB(CLIP(p_Path), -1, 1) = ':'
       SETPATH(CLIP(p_Path) & '\')
    ELSE
       SETPATH(p_Path)
    .

    IF ERRORCODE()
       ! May be a file, so lop off last bit after last \
       MESSAGE('Could not set path - ' & CLIP(ERROR()) & ' (' & ERRORCODE() & ')' & '||Will try remove any file.||Path: ' & CLIP(p_Path))
       AllDirs.Name         = Get_Last_Element_From_Delim_Str(p_Path, '\', TRUE)
    .

    AllDirs.Directory       = p_Path
    AllDirs.Depth           = 0
    ADD(AllDirs)

    ! Directories
    LOOP
       GET(AllDirs, 1)
       IF ERRORCODE()
          BREAK
       .

       DELETE(AllDirs)
       LOC:Depth    = AllDirs.Depth

       ! Check the depth of this dir
       IF p_SubDirs = 0
          IF LOC:Depth > 0
             CYCLE
          .
       ELSIF p_SubDirs > 0
          ! All dirs
       ELSE
          IF LOC:Depth > -(p_SubDirs)
             CYCLE
       .  .

       LOC:Set_Path     = AllDirs.Directory

       SETPATH( AllDirs.Directory )
       IF ERRORCODE()
          MESSAGE('Could not set path - ' & CLIP(ERROR()) & ' (' & ERRORCODE() & ')' & '||Path: ' & AllDirs.Directory)
          CYCLE
       .

       FREE(AllFiles)
       DIRECTORY(AllFiles, '*.*', p_Attributes)
       LOC:Recs             = RECORDS(AllFiles)
       LOOP LOC:Idx = LOC:Recs TO 1 BY -1
          GET(AllFiles, LOC:Idx)
          IF BAND(FIL:Attrib, ff_:DIRECTORY)
             IF AllFiles.Name = '.' OR AllFiles.Name = '..'
                CYCLE
             .

             AllDirs            :=: AllFiles
             AllDirs.Directory   =  CLIP(LOC:Set_Path) & '\' & AllFiles.Name
             AllDirs.Depth       =  LOC:Depth + 1
             ADD(AllDirs)
          ELSE
             IF RECORDS(LOC:Extensions_Q) > 0        ! If none specified than all
                L_EQ:Extension   = LEFT(CLIP(Get_Last_Element_From_Delim_Str(AllFiles.Name, '.', FALSE)))
                GET(LOC:Extensions_Q, L_EQ:Extension)
                IF ERRORCODE()
                   CYCLE
             .  .

             IF ~OMITTED(6)
                IF INSTRING(CLIP(p_SearchString), CLIP(AllFiles.Name), 1) = 0
                   CYCLE
             .  .

             p_FileQ            :=: AllFiles
             p_FileQ.Directory   =  LOC:Set_Path
             ADD(p_FileQ)

             LOC:Result += 1
    .  .  .

    SETPATH( LOC:Current_Path )

    FREE(AllFiles)
    FREE(AllDirs)
    RETURN( LOC:Result )
SQL_Get_DateT_G      FUNCTION (p:Date, p:Time, p:Enclosed) ! Declare Procedure
LOC:Date_Time        STRING(30)                            !
  CODE                                                     ! Begin processed code
    ! (p:Date, p:Time)
    !                                   1         2
    !                          1234567890123456789012345678
    ! Expects something like:  yyyy-mm-dd hh:mm:ss.hhhhhhhh

!    LOC:Date_Time       = FORMAT(p:Date, @d12)        ! yyyymmdd
!    LOC:Date_Time       = LOC:Date_Time[1 : 4] & '-' & LOC:Date_Time[5 : 6] & '-' & LOC:Date_Time[7 : 8]

    LOC:Date_Time       = YEAR(p:Date) & '-'  & FORMAT(MONTH(p:Date),@N02) & '-' & FORMAT(DAY(p:Date),@N02)
    ! 39  = '
    ! 123 = {
    ! 125 = }


    IF p:Time ~= 0
       LOC:Date_Time    = '<123>ts <39>' & CLIP(LOC:Date_Time) & ' ' & FORMAT(p:Time, @t04) & '<39><125>'
    ELSE
       LOC:Date_Time    = '<123>d <39>' & CLIP(LOC:Date_Time) & '<39><125>'
    .

    RETURN( CLIP(LOC:Date_Time) )
!    ! (p:Date, p:Time, p:Enclosed)
!    !                                   1         2
!    !                          1234567890123456789012345678
!    ! Expects something like:  yyyy-mm-dd hh:mm:ss.hhhhhhhh
!
!    LOC:Date_Time       = FORMAT(p:Date, @d12)        ! yyyymmdd
!    LOC:Date_Time       = LOC:Date_Time[1 : 4] & '-' & LOC:Date_Time[5 : 6] & '-' & LOC:Date_Time[7 : 8]
!
!    IF p:Time ~= 0
!       LOC:Date_Time    = CLIP(LOC:Date_Time) & ' ' & FORMAT(p:Time, @t4)
!    .
!
!    IF p:Enclosed = TRUE
!       LOC:Date_Time    = '<39>' & CLIP(LOC:Date_Time) & '<39>'
!    .
!
!    RETURN( CLIP(LOC:Date_Time) )
