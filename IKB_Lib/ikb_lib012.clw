

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Enumerate_Drives     FUNCTION (p:Show_Drives_List, p:Drive_List_To_Check) ! Declare Procedure
LOC:Current_Path     STRING(255)                           !The current path before this executes
LOC:Test_Path        STRING(20)                            !The test drive
LOC:Return_String    STRING(26)                            !The string of drives that are found
LOC:Drive_Letters    STRING('''BCDEFGHIJKLMNOPQRSTUVWXYZ'' ') !
  CODE                                                     ! Begin processed code
    ! (BYTE = 0, <STRING>),STRING
    ! (p:Show_Drives_List, p:Drive_List_To_Check)

    ! We use a string to return the drive letters available - there can only be 26
    ! abcdefghijklmnopqrstuvwxyz
    ! ABCDEFGHIJKLMNOPQRSTUVWXYZ
    LOC:Current_Path    = PATH()


    IF OMITTED(2) = FALSE
       LOC:Drive_Letters    = CLIP(LEFT(p:Drive_List_To_Check))
    .

    LOOP I# = 1 TO 26
       LOC:Test_Path    = LOC:Drive_Letters[I#] & ':\'

       SETPATH(LOC:Test_Path)
       IF ERRORCODE() = 3
          ! Path not found
       ELSE
          LOC:Return_String = CLIP(LOC:Return_String) & LOC:Drive_Letters[I#]
    .  .

    SETPATH(CLIP(LOC:Current_Path))

    IF p:Show_Drives_List = TRUE
       MESSAGE('Drives: ' & LOC:Return_String)
    .

    RETURN(LOC:Return_String)


