

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Double_Char          FUNCTION (p:Str, p:Char)              ! Declare Procedure
LOC:No_Occurances    LONG                                  !
LOC_Use_Str         &STRING
Str_Cls     CLASS
Str_1           &STRING
Init            PROCEDURE(STRING)
Destruct        PROCEDURE
            .


LOC_My_Str_Ele      Str_Cls
  CODE                                                     ! Begin processed code
    ! (p:Str, p:Char)
    Len_#           = LEN(CLIP(p:Str))

    Idx_#   = 0
    LOOP
       Idx_#    += 1
       IF Idx_# > Len_#
          BREAK
       .

       IF p:Str[Idx_#] = p:Char[1]
          LOC:No_Occurances    += 1
    .  .


    LOC_Use_Str    &= NEW(String(Len_# + LOC:No_Occurances))


    Idx_#   = 0
    Idx_2_# = 0
    LOOP
       Idx_#    += 1
       IF Idx_# > Len_#
          BREAK
       .

       Idx_2_#  += 1

       LOC_Use_Str[Idx_2_#]  = p:Str[Idx_#]

       IF p:Str[Idx_#] = p:Char[1]
          Idx_2_#              += 1
          LOC_Use_Str[Idx_2_#]  = p:Str[Idx_#]
       ELSE
          LOC_Use_Str[Idx_2_#]  = p:Str[Idx_#]
    .  .


    LOC_My_Str_Ele.Init(LOC_Use_Str)

    DISPOSE(LOC_Use_Str)

    RETURN(CLIP(LOC_My_Str_Ele.Str_1))
! ----------- Class Procs -----------------
Str_Cls.Init        PROCEDURE(STRING Our_Str)
    CODE
    Len_SCls_#      = LEN(CLIP(Our_Str))

    SELF.Str_1     &= NEW(STRING(Len_SCls_#))

    SELF.Str_1      = Our_Str

    !MESSAGE('Our_Str: ' & CLIP(Our_Str) & '||Str_Cls.Str_1: ' & CLIP(Str_Cls.Str_1))

    RETURN



Str_Cls.Destruct    PROCEDURE()
    CODE
    DISPOSE(SELF.Str_1)
    RETURN
