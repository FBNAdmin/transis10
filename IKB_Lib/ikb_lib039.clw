

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

WA_Play_Button       FUNCTION (p:Handle)                   ! Declare Procedure
LOC:WinAmpHandle     UNSIGNED                              !
LOC:Null             UNSIGNED                              !
LOC:User_Command     LONG                                  !
LOC:Returned         ULONG                                 !
LOC:WM_COMMAND       LONG(0111H)                           !
  CODE                                                     ! Begin processed code
       !LOC:WinAmpHandle    = Get_WinAmp_Handle()
       LOC:WinAmpHandle    = p:Handle

       LOC:User_Command    = 40045               ! play button

       LOC:Returned        = SendMessage(LOC:WinAmpHandle, LOC:WM_COMMAND, LOC:User_Command, LOC:Null)

       RETURN(LOC:Returned)
