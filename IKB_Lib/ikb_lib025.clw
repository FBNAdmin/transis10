

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Months_Between       FUNCTION (LOC:Date1, LOC:Date2)       ! Declare Procedure
LOC:BaseDate         LONG                                  !
LOC:ToDate           LONG                                  !
LOC:Years            LONG                                  !
LOC:Months           SHORT                                 !
LOC:Days             SHORT                                 !
LOC:TotalMonths      LONG                                  !
  CODE                                                     ! Begin processed code
    ! (LOC:Date1, LOC:Date2)
    ! (LONG, <LONG>),LONG

    IF OMITTED(2)
       IF LOC:Date1 < TODAY()
          LOC:BaseDate = LOC:Date1
          LOC:ToDate   = TODAY()
       ELSE
          LOC:BaseDate = TODAY()
          LOC:ToDate   = LOC:Date1
       .
    ELSE
       IF LOC:Date1 < LOC:Date2
          LOC:BaseDate = LOC:Date1
          LOC:ToDate   = LOC:Date2
       ELSE
          LOC:BaseDate = LOC:Date2
          LOC:ToDate   = LOC:Date1
    .  .

    LOC:Years       = YEAR(LOC:ToDate)      - YEAR(LOC:BaseDate)
    LOC:Months      = MONTH(LOC:ToDate)     - MONTH(LOC:BaseDate)

!    STOP('years and months: ' & LOC:YEARS & ', ' & LOC:Months)
!    STOP('y1 y2 m1 m2: ' & YEAR(LOC:ToDate) & ', ' & YEAR(LOC:BaseDate) & ' - ' & MONTH(LOC:ToDate) |
!          & ', ' & MONTH(LOC:BaseDate))

    LOC:TotalMonths = (LOC:Years * 12) + LOC:Months

    LOC:Days        = DAY(LOC:ToDate)       - DAY(LOC:BaseDate)
    IF LOC:Days >= 0                                     ! Day is later in ToDate
       ! LOC:Months is therefore correct number of months difference
    ELSE
       LOC:TotalMonths -= 1                              ! Last month not complete yet
    .

    RETURN LOC:TotalMonths
