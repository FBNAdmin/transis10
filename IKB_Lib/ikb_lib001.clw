

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Byte_To_StrB         FUNCTION (p:Byte)                     ! Declare Procedure
LOC:Ret_Str          STRING(8)                             !
  CODE                                                     ! Begin processed code
    ! (p:Byte)
    LOC:Ret_Str = '00000000'

    IF BAND(p:Byte, 10000000b) > 0
       LOC:Ret_Str[1]   = '1'
    .

    IF BAND(p:Byte, 01000000b) > 0
       LOC:Ret_Str[2]   = '1'
    .

    IF BAND(p:Byte, 00100000b) > 0
       LOC:Ret_Str[3]   = '1'
    .

    IF BAND(p:Byte, 00010000b) > 0
       LOC:Ret_Str[4]   = '1'
    .

    IF BAND(p:Byte, 00001000b) > 0
       LOC:Ret_Str[5]   = '1'
    .

    IF BAND(p:Byte, 00000100b) > 0
       LOC:Ret_Str[6]   = '1'
    .

    IF BAND(p:Byte, 00000010b) > 0
       LOC:Ret_Str[7]   = '1'
    .

    IF BAND(p:Byte, 00000001b) > 0
       LOC:Ret_Str[8]   = '1'
    .

    RETURN(LOC:Ret_Str)
