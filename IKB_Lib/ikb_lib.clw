   PROGRAM

   INCLUDE('Equates.CLW')
   INCLUDE('TplEqu.CLW')
   INCLUDE('Keycodes.CLW')
   INCLUDE('Errors.CLW')
    INCLUDE('IvansAPI.CLW','Equates')
    INCLUDE('IKB_LIB.EQU')
    INCLUDE('Ikb_lib.typ')
   MAP
     MODULE('ikb_lib001.clw')
       Byte_To_StrB(BYTE),STRING
     END
     MODULE('ikb_lib002.clw')
       Calc_Registration_No(ULONG),ULONG
     END
     MODULE('ikb_lib003.clw')
       Centre_Control_In_Control(?, ?)
     END
     MODULE('ikb_lib004.clw')
       Check_File_Existence(*STRING, BYTE=0),BYTE
     END
     MODULE('ikb_lib005.clw')
       Convert_Text_To_MS(STRING),?
     END
     MODULE('ikb_lib006.clw')
       Count_Delim_Str_Elements(*STRING, STRING, BYTE=0),LONG,PROC
     END
     MODULE('ikb_lib007.clw')
       Count_SubStr_In_Str(STRING,STRING),ULONG
     END
     MODULE('ikb_lib008.clw')
       Date_Time_Difference(LONG,LONG,LONG,LONG, BYTE=0),STRING
     END
     MODULE('ikb_lib009.clw')
       Dec_To_Hex(LONG),STRING
     END
     MODULE('ikb_lib010.clw')
       DNibble_To_Hex(BYTE),STRING
     END
     MODULE('ikb_lib011.clw')
       Double_Char(STRING, STRING),STRING
     END
     MODULE('ikb_lib012.clw')
       Enumerate_Drives(BYTE = 0, <STRING>),STRING
     END
     MODULE('ikb_lib013.clw')
       File_User_Access(BYTE),BYTE
     END
     MODULE('ikb_lib014.clw')
       Get_1st_Element_From_Delim_Str(*STRING, STRING, BYTE = 0, BYTE = 0),STRING
     END
     MODULE('ikb_lib015.clw')
       Get_Last_Element_From_Delim_Str(*STRING, STRING, BYTE = 0, BYTE = 0), STRING
     END
     MODULE('ikb_lib016.clw')
       Get_New_Control_Size(?, LONG=0,  LONG=0, BYTE=0)
     END
     MODULE('ikb_lib017.clw')
       Get_Registration_Id(),ULONG
     END
     MODULE('ikb_lib018.clw')
       Get_Registration_No(),ULONG
     END
     MODULE('ikb_lib019.clw')
       Get_String_Up_To_Delim(STRING, STRING),STRING
     END
     MODULE('ikb_lib020.clw')
       Get_Volume_Info(),ULONG
     END
     MODULE('ikb_lib021.clw')
       Hex_To_Dec(STRING),LONG
     END
     MODULE('ikb_lib022.clw')
       Insert_Into_Delim_Str(*STRING, STRING, STRING, SHORT=0, BYTE=0, BYTE=1),LONG,PROC
     END
     MODULE('ikb_lib023.clw')
       Is_Even(LONG),BYTE
     END
     MODULE('ikb_lib024.clw')
       Match_Element_In_Delim_Str(*STRING, STRING, STRING, BYTE=0, BYTE=1),LONG
     END
     MODULE('ikb_lib025.clw')
       Months_Between(LONG, <LONG>),LONG
     END
     MODULE('ikb_lib026.clw')
       Month_Name(LONG),STRING
     END
     MODULE('ikb_lib027.clw')
       Output_Text(STRING, USHORT=0, STRING, BYTE=1),LONG,PROC
     END
     MODULE('ikb_lib028.clw')
       Pad_Str(STRING, SHORT, *CSTRING),STRING
     END
     MODULE('ikb_lib029.clw')
       Quarter(LONG),BYTE
     END
     MODULE('ikb_lib030.clw')
       Remove_White_Space(STRING),STRING
     END
     MODULE('ikb_lib031.clw')
       Replace_Chars(STRING, *CSTRING, STRING, <*LONG>, BYTE=0, BYTE=0),STRING
     END
     MODULE('ikb_lib032.clw')
       StrB_To_Byte(STRING),BYTE
     END
     MODULE('ikb_lib033.clw')
       StrB_To_Octet(STRING),STRING
     END
     MODULE('ikb_lib034.clw')
       String_Splice(STRING,*CString,*CString,LONG=0, LONG=0, <*CSTRING>),BYTE
     END
     MODULE('ikb_lib035.clw')
       Time_Difference(LONG,LONG),LONG
     END
     MODULE('ikb_lib036.clw')
       To_ASCII_Codes(STRING),STRING
     END
     MODULE('ikb_lib037.clw')
       Transpose_Str(STRING),STRING
     END
     MODULE('ikb_lib038.clw')
       URLHandler(unsigned, STRING, <STRING>, <STRING>)
     END
     MODULE('ikb_lib039.clw')
       WA_Play_Button(UNSIGNED),ULONG
     END
     MODULE('ikb_lib040.clw')
       Week(LONG),BYTE
     END
     MODULE('ikb_lib041.clw')
       Weeks(LONG, BYTE=1, <SHORT>),LONG
     END
     MODULE('ikb_lib042.clw')
       Week_Day(LONG),STRING
     END
     MODULE('ikb_lib043.clw')
       Week_Day_No(LONG),BYTE
     END
     MODULE('ikb_lib044.clw')
       Month_Week(LONG),LONG
     END
     MODULE('ikb_lib045.clw')
       ISExecute(unsigned, STRING, <STRING>, <STRING>),LONG,PROC
     END
     MODULE('ikb_lib046.clw')
       Capitalise(STRING, BYTE=0, BYTE=1),STRING
       Get_1st_Element_From_Delim_Str_(STRING, STRING, BYTE = 0),STRING
       DataPath_To_Path(STRING, STRING, <STRING>),STRING
       Path_To_DataPath(STRING, STRING, <STRING>),STRING
       Get_1st_Element_From_Delim_Str_Fast(*STRING, *STRING, BYTE = 0, BYTE = 0, LONG=100),STRING
     END
     MODULE('ikb_lib047.clw')
       Get_1st_Element_From_Delim_Str_Fast_(*STRING, STRING, BYTE = 0, BYTE = 0, LONG=100),STRING
       Get_1st_Element_From_Delim_Str_old(*STRING, STRING, BYTE = 0, BYTE = 0),STRING
       Get_1st_Element_From_Delim_Str_100(*STRING, STRING, BYTE = 0, BYTE = 0),STRING
       Get_1st_Element_From_Delim_Str_2(*STRING, *STRING, BYTE = 0, BYTE = 0),STRING
       Replace_Strings(*STRING, STRING, STRING, LONG=0, BYTE=0), LONG
     END
     MODULE('ikb_lib048.clw')
       Insert_Into_Delim_Str_2(*STRING, STRING, STRING, SHORT=0),LONG,PROC
       Get_Element_From_Delim_Str(*STRING, *STRING, LONG=0),STRING
       Add_to_List(STRING, *STRING, STRING, BYTE=0, <STRING>, BYTE=0, BYTE=0)
       Get_File_List(FILE:queue_ikb p_FileQ, STRING, <STRING>, LONG=1, LONG=0, <STRING>), LONG, PROC
       SQL_Get_DateT_G(LONG, LONG=0, BYTE=0),STRING
     END
     MODULE('ikb_lib049.clw')
       SQL_Ret_DateT_G(STRING, <BYTE>),STRING
       Add_to_List_C(STRING, *CSTRING, STRING, BYTE=0, <STRING>, BYTE=0)
       PopupCalendar(*? PassedDate,Byte FixYear,<LONG XPos>,<Long YPos>)
       PopupTime(*? PassedTime, Byte FixYear, <LONG XPos>, <Long YPos>)
       GetRatio(*LONG p_DuPiX, *LONG p_DuPiY)
     END
     MODULE('ikb_lib050.clw')
       MyPopup(String Items, Long XPos, Long YPos), BYTE
       SQL_Deformat(STRING),STRING
       Get_Tag(*STRING, BYTE=0, <STRING>, <STRING>), STRING
       Date_Time_Advance(*DATE, <*TIME>, BYTE, LONG)
       Replace_Chars_old(STRING, *CSTRING, STRING, BYTE=0, BYTE=0, BYTE=0),STRING
       Wrap_CSV_Field(STRING), STRING
     END
     MODULE('ikb_l_SF.CLW')
       CheckOpen(FILE File,<BYTE OverrideCreate>,<BYTE OverrideOpenMode>)
       ReportPreview(QUEUE PrintPreviewQueue)
       Preview:JumpToPage(LONG Input:CurrentPage, LONG Input:TotalPages),LONG
       Preview:SelectDisplay(*LONG Input:PagesAcross, *LONG Input:PagesDown)
       StandardWarning(LONG WarningID),LONG,PROC
       StandardWarning(LONG WarningID,STRING WarningText1),LONG,PROC
       StandardWarning(LONG WarningID,STRING WarningText1,STRING WarningText2),LONG,PROC
       SetupStringStops(STRING ProcessLowLimit,STRING ProcessHighLimit,LONG InputStringSize,<LONG ListType>)
       NextStringStop,STRING
       SetupRealStops(REAL InputLowLimit,REAL InputHighLimit)
       NextRealStop,REAL
       INIRestoreWindow(STRING ProcedureName,STRING INIFileName)
       INISaveWindow(STRING ProcedureName,STRING INIFileName)
       RISaveError
     END
     MODULE('ikb_l_RU.CLW')
     END
     MODULE('ikb_l_RD.CLW')
     END
         INCLUDE('IvansAPI.CLW','Prototypes')
         MODULE('Windows.DLL')
     ShellExecute_(UNSIGNED,LONG,*CSTRING,LONG,*CSTRING,SIGNED),UNSIGNED,PASCAL,RAW  !,NAME('SHELLEXECUTEA')
     ShellExecute(UNSIGNED,*CSTRING,*CSTRING,*CSTRING,*CSTRING,SIGNED),UNSIGNED,PASCAL,RAW,NAME('ShellExecuteA')
     
     GetVolumeInformation(*CSTRING,*CSTRING,ULONG,*ULONG,*ULONG,*ULONG,*CSTRING,ULONG),BOOL,PASCAL,RAW,NAME('GetVolumeInformationA')
     
      FindWindow(*CSTRING,*CSTRING),UNSIGNED,PASCAL,RAW,NAME('FindWindowA')
      PostMessage(UNSIGNED,UNSIGNED,UNSIGNED,LONG),BOOL,PASCAL,NAME('PostMessageA')
      SendMessage(UNSIGNED,UNSIGNED,UNSIGNED,LONG),ULONG,PASCAL,NAME('SendMessageA')
     
         END
     
     !LPCSTR                  EQUATE(CSTRING)    !Usage:Pass the Label of the LPCSTR
     !DWORD                   EQUATE(ULONG)
     !BOOL                    EQUATE(SIGNED)
     
     
     
   END

GLO:BranchID         ULONG,EXTERNAL,DLL(dll_mode)
GLO:ReplicatedDatabaseID LONG,EXTERNAL,DLL(dll_mode)
GLO:DBOwner          STRING(255),EXTERNAL,DLL(dll_mode)
GLO:Global_INI       CSTRING('.\TransIS.INI<0>{241}'),EXTERNAL,DLL(dll_mode)
GLO:Local_INI        CSTRING('TransISL.INI<0>{242}'),EXTERNAL,DLL(dll_mode)
GLO:Rep_ID           LONG,EXTERNAL,DLL(dll_mode)
GLO:ClosingDown      BYTE,EXTERNAL,DLL(dll_mode)
GLO:Global_Controls  GROUP,PRE(GLO),EXTERNAL,DLL(dll_mode)
Reminding_Waiting      BYTE
Frame_Size             GROUP,PRE(GLO)
Width                    LONG
Height                   LONG
                       END
                     END
GLO:Development_Group GROUP,PRE(GLO),EXTERNAL,DLL(dll_mode)
Testing_Mode           BYTE(1)
                     END
GLO:Loaded_IDs_Group GROUP,PRE(GLO),EXTERNAL,DLL(dll_mode)
Setup_Loaded_ID        ULONG(1)
Get_Delivery_ManIDs_Loaded_ID LONG(1)
Rates_Caching_No       ULONG
Reminder_Inc           LONG
                     END
GLO:Thread_Q         QUEUE,PRE(GL_TQ),EXTERNAL,DLL(dll_mode)
ProcedureName          STRING(150)
Thread                 SIGNED
Q_ID                   ULONG
                     END
GLO:User_Group       GROUP,PRE(GLO),EXTERNAL,DLL(dll_mode)
UID                    ULONG
UGIDs                  STRING(100)
Login                  STRING(20)
AccessLevel            BYTE
LoggedInDate           DATE
LoggedInTime           TIME
                     END
GLO:String_Q_Type    QUEUE,PRE(GLO_SQT),EXTERNAL,DLL(dll_mode)
Str1                   STRING(1000)
Str2                   STRING(1000)
Str3                   STRING(1000)
                     END
GLO:Clock_Audit      TIME,EXTERNAL,DLL(dll_mode)

SaveErrorCode        LONG
SaveError            CSTRING(255)
SaveFileErrorCode    LONG
SaveFileError        CSTRING(255)
GlobalRequest        LONG(0),EXTERNAL,DLL(dll_mode),THREAD
GlobalResponse       LONG(0),EXTERNAL,DLL(dll_mode),THREAD
VCRRequest           LONG(0),EXTERNAL,DLL(dll_mode),THREAD
!region File Declaration
!endregion

Sort:Name            STRING(ScrollSort:Name)                
Sort:Name:Array      STRING(3),DIM(100),OVER(Sort:Name)
Sort:Alpha           STRING(ScrollSort:Alpha)
Sort:Alpha:Array     STRING(2),DIM(100),OVER(Sort:Alpha)


  CODE
  
!---------------------------------------------------------------------------
