

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Get_1st_Element_From_Delim_Str FUNCTION (p:Delim_List, p:Delim, p:Chop_Out, p:Case_Sensitive) ! Declare Procedure
LOC:Delim_Pos        ULONG,AUTO                            !Position of deliminator
LOC:Delim_Len        ULONG,AUTO                            !Length of the deliminator
LOC:Len              ULONG,AUTO                            !
LOC_Use_Str             &STRING
LOC_Use_Str_No_Case     &CSTRING
Str_Cls     CLASS
Str_1           &STRING
Init            PROCEDURE(STRING)
Destruct        PROCEDURE
            .


LOC_My_Str_Ele      Str_Cls
  CODE                                                     ! Begin processed code
    ! Get_1st_Element_From_Delim_Str
    ! (p:Delim_List, p:Delim, p:Chop_Out - optional defaulted, p:Case_Sensitive)
    ! (*STRING, STRING, BYTE = 0, BYTE = 0),STRING
    !  String, Delim, Chop
    IF CLIP(p:Delim_List) ~= ''
       LOC:Len                 = LEN(CLIP(p:Delim_List))

       LOC_Use_Str            &= NEW(String(LOC:Len))
       LOC_Use_Str_No_Case    &= NEW(CString(LOC:Len+1))

       ! We are going to assume that if the p:Delim is empty the user wants to split on space!
       IF CLIP(p:Delim) = ''
          LOC:Delim_Len        = 1
       ELSE
          IF p:Case_Sensitive = FALSE
             p:Delim           = UPPER(CLIP(p:Delim))
          .
          LOC:Delim_Len        = LEN(CLIP(p:Delim))
       .

       LOC_Use_Str             = p:Delim_List

       IF p:Case_Sensitive = FALSE
          LOC_Use_Str_No_Case  = UPPER(CLIP(p:Delim_List))
       ELSE
          LOC_Use_Str_No_Case  = p:Delim_List                   ! No clip needed - CLIP(p:Delim_List)
       .

       LOC:Delim_Pos           = INSTRING(p:Delim[1 : LOC:Delim_Len], LOC_Use_Str_No_Case, 1)

       IF LOC:Delim_Pos > 0                                     ! We have a comma
          IF LOC:Delim_Pos > 1                                  ! If we dont have ,something
             LOC_My_Str_Ele.Init( LOC_Use_Str[1 : (LOC:Delim_Pos - 1)] )
          ELSE                                                  ! > 0 but NOT > 1    =   1  - found at pos 1 so chop off - done below
             LOC_My_Str_Ele.Init( ' ' )
          .

          IF p:Chop_Out = TRUE
             IF (LOC:Delim_Pos + LOC:Delim_Len) <= LOC:Len
                p:Delim_List   = LOC_Use_Str[(LOC:Delim_Pos + LOC:Delim_Len) : LOC:Len]
             ELSE
                p:Delim_List   = ''
          .  .
       ELSE                                                     ! No comma in the string
          LOC_My_Str_Ele.Init( LOC_Use_Str )
          IF p:Chop_Out = TRUE
             p:Delim_List      = ''
       .  .

       DISPOSE(LOC_Use_Str)
       DISPOSE(LOC_Use_Str_No_Case)

       RETURN( LOC_My_Str_Ele.Str_1 )          ! Clipping not needed - string is made right size
    ELSE
       RETURN( '' )
    .


! ----------- Class Procs -----------------
Str_Cls.Init        PROCEDURE(STRING Our_Str)
    CODE
    IF LEN(CLIP(Our_Str)) > 0
       SELF.Str_1     &= NEW(STRING( LEN(CLIP(Our_Str)) ))
    ELSE
       SELF.Str_1     &= NEW(STRING( 1 ))
    .

    SELF.Str_1      = Our_Str
    RETURN



Str_Cls.Destruct    PROCEDURE()
    CODE
    DISPOSE(SELF.Str_1)
    RETURN
