

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Week                 FUNCTION (p:Date)                     ! Declare Procedure
LOC:Week             BYTE                                  !
LOC:Days_Diff        LONG                                  !
LOC:Beginning        LONG                                  !
  CODE                                                     ! Begin processed code
    ! (p:Date)
    ! DATE(mm,dd,yy)

    LOC:Beginning   = DATE(1, 1, YEAR(p:Date))
    LOC:Days_Diff   = p:Date - LOC:Beginning + 1

    LOC:Week        = (LOC:Days_Diff / 7)
    IF (LOC:Days_Diff % 7) ~= 0
       LOC:Week += 1
    .

    RETURN(LOC:Week)
