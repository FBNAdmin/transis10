

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Capitalise           FUNCTION (p:Word, p:Lower_Joining, p:Lower_J_Ignore_1st) ! Declare Procedure
LOC:Flag             BYTE                                  !
LOC:Word             STRING(100)                           !
LOC:Idx              LONG                                  !
  CODE                                                     ! Begin processed code
    ! (p:Word, p:Lower_Joining, p:Lower_J_Ignore_1st)

    LOC:Flag    = TRUE
    LOC:Idx     = 0

    LOOP LEN(CLIP(p:Word)) TIMES
       LOC:Idx  += 1

       IF CLIP(p:Word[LOC:Idx]) = '' OR p:Word[LOC:Idx] = '(' OR p:Word[LOC:Idx] = '[' |
            OR p:Word[LOC:Idx] = '{{'  OR p:Word[LOC:Idx] = '<'
          ! It's white space or (
          !INRANGE(VAL(p:Word[LOC:Idx]), 65, 90) OR INRANGE(VAL(p:Word[LOC:Idx]), 97, 122)
            
          LOC:Flag              = TRUE                      ! Next word capitalise
       ELSE
          p:Word[LOC:Idx]       = LOWER( p:Word[LOC:Idx] )

          IF LOC:Flag = TRUE
             IF p:Lower_Joining = TRUE
                IF LOC:Idx = 1 AND p:Lower_J_Ignore_1st = TRUE
                   ! We don't want to keep 1st words in lower case
                ELSE
                   ! Keep joining words in lower case, words like; and, of
                   LOC:Word    = LOWER( Get_1st_Element_From_Delim_Str_(SUB(p:Word, LOC:Idx, LEN(CLIP(p:Word))), '') )
                   IF INLIST(CLIP(LOC:Word), 'of', 'and', 'the') > 0
                      LOC:Flag = FALSE
             .  .  .

             IF LOC:Flag = TRUE
                p:Word[LOC:Idx]    = UPPER( p:Word[LOC:Idx] )
             .

             LOC:Flag   = FALSE
    .  .  .

    RETURN(p:Word)
Get_1st_Element_From_Delim_Str_ FUNCTION (p:Delim_List, p:Delim, p:Case_Sensitive) ! Declare Procedure
  CODE                                                     ! Begin processed code
    ! (p:Delim_List, p:Delim, p:Case_Sensitive)

    RETURN( Get_1st_Element_From_Delim_Str(p:Delim_List, p:Delim, FALSE, p:Case_Sensitive) )
DataPath_To_Path     FUNCTION (p:DataPath, p:Gen_Path, p:Data_Path_Symb) ! Declare Procedure
LOC:Data_Path_Symb   STRING(50)                            !
LOC:Data_Symb_Len    LONG                                  !
  CODE                                                     ! Begin processed code
    ! (p:DataPath, p:Gen_Path, p:Data_Path_Symb)
    IF OMITTED(3)
       LOC:Data_Path_Symb   = '(DATA)'
    ELSE
       LOC:Data_Path_Symb   = UPPER(p:Data_Path_Symb)
    .
    LOC:Data_Symb_Len   = LEN(CLIP(LOC:Data_Path_Symb))

    ! We want to check that the user is not trying to point this icon off the data dir
    IF UPPER(p:DataPath[1 : LOC:Data_Symb_Len]) = LOC:Data_Path_Symb[1 : LOC:Data_Symb_Len]    ! (DATA)
       ! Then the user is trying to say data dir

       IF p:Gen_Path[LEN(CLIP(p:Gen_Path))] ~= '\'
          p:Gen_Path   = CLIP(p:Gen_Path) & '\'
       .

       IF p:DataPath[LOC:Data_Symb_Len + 1] = '\'
          p:DataPath  = CLIP(p:Gen_Path) & p:DataPath[LOC:Data_Symb_Len + 2 : LEN(CLIP(p:DataPath))]
       ELSE
          p:DataPath  = CLIP(p:Gen_Path) & p:DataPath[LOC:Data_Symb_Len + 1 : LEN(CLIP(p:DataPath))]
       .
    ELSE
       ! no change to the variable
    .


    RETURN(p:DataPath)
Path_To_DataPath     FUNCTION (p:Location, p:Gen_Path, p:Data_Path_Symb) ! Declare Procedure
LOC:Data_Path_Symb   STRING(50)                            !
  CODE                                                     ! Begin processed code
    ! (p:Location, p:Gen_Path, p:Data_Path_Symb)
    IF OMITTED(3)
       LOC:Data_Path_Symb   = '(DATA)'
    ELSE
       LOC:Data_Path_Symb   = p:Data_Path_Symb
    .

    p:Gen_Path      = UPPER(p:Gen_Path)
    Len_data_path_# = LEN(CLIP(p:Gen_Path))

    IF Len_data_path_# > 0
       IF UPPER(p:Location[1 : Len_data_path_#]) = p:Gen_Path[1 : Len_data_path_#]
          ! Then the data path is in the location
          !p:Location   = '(DATA)\' & CLIP(p:Location)

          p:Location   = p:Location[Len_data_path_# + 1 : LEN(CLIP(p:Location))]
          p:Location   = CLIP(LOC:Data_Path_Symb) & '\' & CLIP(p:Location)
    .  .

    RETURN(p:Location)


Get_1st_Element_From_Delim_Str_Fast FUNCTION (p:Delim_List, p:Delim, p:Chop_Out, p:Case_Sensitive, p:Size) ! Declare Procedure
LOC:Delim_Len        ULONG                                 !Length of the deliminator
LOC:Len              ULONG                                 !
LOC:Start            ULONG                                 !
LOC:End              ULONG                                 !
LOC:Found            BYTE                                  !
LOC:Method           BYTE                                  !
LOC:Pos_Found        ULONG                                 !
LOC_Ret_Str             &STRING     

LOC_Delim               &CSTRING


LOC_Str                 &STRING
LOC_Result              &STRING
LOC_Str_2               &STRING

Str_Cls             CLASS
Str_1               &STRING
Init                PROCEDURE(*STRING)
Destruct            PROCEDURE
                    .


LOC_My_Str_Ele      Str_Cls
  CODE                                                     ! Begin processed code
    ! (p:Delim_List, p:Delim, p:Chop_Out, p:Case_Sensitive, p:Size)
    ! (*STRING, STRING, BYTE = 0, BYTE = 0),STRING
    !  String, Delim, Chop
    IF CLIP(p:Delim_List) = ''
       RETURN('')
    .

    ! We want to break the text up if too big and only process a bit of it if possible
    LOC:Len         = LEN(CLIP( p:Delim_List ))
    LOC:Delim_Len   = LEN(CLIP( p:Delim ))
    IF LOC:Delim_Len = 0
       LOC:Delim_Len    = 1
    .

    IF LOC:Delim_Len <= (p:Size / 5)                ! Much smaller than - 1 fith or less
       IF p:Case_Sensitive = FALSE
          LOC_Delim    &= NEW(CSTRING(LOC:Delim_Len+1))
          IF CLIP(p:Delim) = ''
             LOC_Delim     = ' '
          ELSE
             LOC_Delim     = UPPER(CLIP(p:Delim))
       .  .

       LOC_Str         &= NEW(STRING(p:Size))
       LOC_Result      &= NEW(STRING(p:Size))
       LOC_Str_2       &= NEW(STRING(p:Size))

       LOC:Method       = 1
       LOC:Start        = 1
       LOC:End          = p:Size

       LOOP
          LOC_Str           = CLIP(p:Delim_List[LOC:Start : LOC:End])
          IF p:Case_Sensitive = FALSE
             LOC_Str        = UPPER(LOC_Str)

             LOC:Pos_Found  = INSTRING(LOC_Delim, CLIP(LOC_Str), 1)
          ELSE
             LOC:Pos_Found  = INSTRING(CLIP(p:Delim), CLIP(LOC_Str), 1)
          .

          IF LOC:Pos_Found > 0
             LOC_Str_2      = CLIP(p:Delim_List[LOC:Start : LOC:End])

             !LOC_Result     = Get_1st_Element_From_Delim_Str_100(LOC_Str_2, p:Delim, TRUE, p:Case_Sensitive)
             LOC_Result     = Get_1st_Element_From_Delim_Str_2(LOC_Str_2, p:Delim, TRUE, p:Case_Sensitive)

             ! Delim found
             ! - need to return all up to this point + stuff in LOC:Result
             ! - need to return all left in p:Delim_List + LOC:Str
             LOC:Found   = TRUE
             BREAK
          .

          IF LOC:End >= LOC:Len                          ! No more str to search
             BREAK
          .

          LOC:Start      = LOC:End - LOC:Delim_Len       ! 1 + 1000 - 2  = 999

          IF (LOC:End + p:Size) < LOC:Len
             LOC:End    += p:Size                        ! 1000 + 1000   = 2000
          ELSE
             LOC:End     = LOC:Len
    .  .  .



    IF LOC:Method ~= 1
       RETURN( Get_1st_Element_From_Delim_Str_2(p:Delim_List, p:Delim, p:Chop_Out, p:Case_Sensitive) )
    ELSE
       IF LOC:Found = TRUE
          IF LOC:Start > 1
             LOC_Ret_Str   &= NEW( String( LEN(CLIP(LOC_Result)) + LOC:Start - 1) )
          ELSE
             LOC_Ret_Str   &= NEW( String( LEN(CLIP(LOC_Result)) ))
          .

          ! We have LOC:Str reduced by Delim size and LOC:Pos_Found
          ! 1001 + 2 + 2 - 1 = 1004

          LOC_Ret_Str       = LOC_Result
          IF LOC:Start > 1
             LOC_Ret_Str    = p:Delim_List[1 : LOC:Start - 1] & CLIP(LOC_Ret_Str)
          .

          IF p:Chop_Out = TRUE
             IF LOC:Len > LOC:End
                p:Delim_List   = p:Delim_List[LOC:Start + LOC:Pos_Found + LOC:Delim_Len - 1 : LOC:Len]
!                p:Delim_List   = p:Delim_List[LOC:Start + LOC:Pos_Found + LOC:Delim_Len - 1 : LOC:End] & p:Delim_List[LOC:End + 1 : LOC:Len]
             ELSE
                p:Delim_List   = p:Delim_List[LOC:Start + LOC:Pos_Found + LOC:Delim_Len - 1 : LOC:End]
          .  .
       ELSE
          LOC_Ret_Str      &= NEW(String(LOC:Len))

          LOC_Ret_Str       = p:Delim_List
          IF p:Chop_Out = TRUE
             p:Delim_List   = ''
       .  .

       IF p:Case_Sensitive = FALSE
          DISPOSE(LOC_Delim)
       .

       DISPOSE(LOC_Str)
       DISPOSE(LOC_Result)
       DISPOSE(LOC_Str_2)

       LOC_My_Str_Ele.Init( LOC_Ret_Str )
       DISPOSE(LOC_Ret_Str)

       RETURN( LOC_My_Str_Ele.Str_1 )               ! Clipping not req. - string is made right size
    .

! ----------- Class Procs -----------------
Str_Cls.Init        PROCEDURE(*STRING Our_Str)
    CODE
    SELF.Str_1     &= NEW(STRING( LEN(CLIP(Our_Str)) ))
    SELF.Str_1      = Our_Str
    RETURN



Str_Cls.Destruct    PROCEDURE()
    CODE
    DISPOSE(SELF.Str_1)
    RETURN
