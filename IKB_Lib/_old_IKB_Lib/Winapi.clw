  SECTION('Equates')

WM_USER                 EQUATE(0400H)
WM_COMMAND              EQUATE(0111H)
WM_COPYDATA             EQUATE(004Ah)

LPCSTR                  EQUATE(CSTRING)    !Usage:Pass the Label of the LPCSTR
HANDLE                  EQUATE(UNSIGNED)
HWND                    EQUATE(HANDLE)
LPSTR                   EQUATE(CSTRING)    !Usage:Pass the Label of the LPSTR
LPARAM                  EQUATE(LONG)
WPARAM                  EQUATE(UNSIGNED)

PVOID                   EQUATE(ULONG)
DWORD                   EQUATE(ULONG)

COPYDATASTRUCT  GROUP,TYPE
dwData           DWORD
cbData           DWORD
lpData           PVOID
                END

  SECTION('Prototypes')

  MODULE('Windows.DLL')

   OMIT('***',_WIDTH32_)
 FindWindow(*LPCSTR,*LPCSTR),HWND,PASCAL,RAW
 PostMessage(HWND, WORD, WPARAM, LPARAM),BOOL,PASCAL
 SendMessage(HWND, WORD, WPARAM, LPARAM),LRESULT,PASCAL
   ***
   COMPILE('***',_WIDTH32_)
 FindWindow(*LPCSTR,*LPCSTR),HWND,PASCAL,RAW,NAME('FindWindowA')
 PostMessage(HWND,UNSIGNED,WPARAM,LPARAM),BOOL,PASCAL,NAME('PostMessageA')
 !SendMessage(HWND,UNSIGNED,WPARAM,LPARAM),ANY,PASCAL,NAME('SendMessageA')
 SendMessage(HWND,UNSIGNED,WPARAM,LPARAM),ULONG,PASCAL,NAME('SendMessageA')
   ***

  END

