

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Centre_Control_In_Control PROCEDURE (p_Control_1, p_Control_2) ! Declare Procedure
L:C_1_W              LONG                                  !
L:C_2_W              STRING(20)                            !
L:C_XP               LONG                                  !
  CODE                                                     ! Begin processed code
    ! (p_Control_1, p_Control_2)

    L:C_1_W     = p_Control_1{PROP:Width}
    L:C_2_W     = p_Control_2{PROP:Width}
    L:C_XP      = p_Control_2{PROP:XPos}

    ! 10 > 8
    !   XP      = 100
    !   c1 Xpos = 100 + (10 - 8) / 2 = 101
    ! 8 > 10
    !   XP      =

    p_Control_1{PROP:XPos}   = L:C_XP + (L:C_2_W - L:C_1_W) / 2


    L:C_1_W     = p_Control_1{PROP:Height}
    L:C_2_W     = p_Control_2{PROP:Height}
    L:C_XP      = p_Control_2{PROP:YPos}

!    IF L:C_2_W > L:C_1_W
    p_Control_1{PROP:YPos}   = L:C_XP + (L:C_2_W - L:C_1_W) / 2

    RETURN
