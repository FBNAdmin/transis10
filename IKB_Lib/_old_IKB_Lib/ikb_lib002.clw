

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Calc_Registration_No FUNCTION (P:Reg_ID)                   ! Declare Procedure
LOC:Registration_No  ULONG                                 !
  CODE                                                     ! Begin processed code
    LOC:Registration_No = INT(((P:Reg_ID / 6) + 420) / 35)

    IF LOC:Registration_No = 111111111
       ! This is our testing reg no.
       LOC:Registration_No  += 1
    .

    RETURN(LOC:Registration_No)
