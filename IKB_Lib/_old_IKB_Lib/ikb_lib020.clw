

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Get_Volume_Info      FUNCTION                              ! Declare Procedure
LOC:Volume_Group     GROUP,PRE()                           !
LOC:Root_Path_Name   CSTRING('C:\<0>{16}')                 !Root of the volume to get the info on
LOC:Volume_Name_Buffer CSTRING(256)                        !
LOC:Volume_Name_Size ULONG(256)                            !
LOC:Volume_Serial_Number ULONG(0)                          !
LOC:Maximum_Component_Length ULONG(256)                    !
LOC:File_System_Flags ULONG(0)                             !
LOC:File_System_Name_Buffer CSTRING(256)                   !
LOC:File_System_Name_Size ULONG(256)                       !
LOC:Return_Value     BYTE                                  !
                     END                                   !
  CODE                                                     ! Begin processed code
    !GetVolumeInformation(*CSTRING,*CSTRING,ULONG,*ULONG,*ULONG,*ULONG,*CSTRING,ULONG),BOOL,PASCAL,RAW,NAME('GetVolumeInformationA')

    LOC:Return_Value    =                                                                   |
    GetVolumeInformation(LOC:Root_Path_Name, LOC:Volume_Name_Buffer, LOC:Volume_Name_Size,  |
                         LOC:Volume_Serial_Number, LOC:Maximum_Component_Length,            |
                         LOC:File_System_Flags, LOC:File_System_Name_Buffer,                |
                         LOC:File_System_Name_Size)

    !Show_Volume_Info(LOC:Volume_Group)

    RETURN(LOC:Volume_Serial_Number)

