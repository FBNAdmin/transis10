

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Dec_To_Hex           FUNCTION (p:Dec)                      ! Declare Procedure
LOC:Dec              LONG                                  !
LOC:Negative         BYTE                                  !
LOC:Int              LONG                                  !
LOC:Rem              LONG                                  !
LOC:Result           STRING(35)                            !
  CODE                                                     ! Begin processed code
    ! (p:Dec)
    ! 1111111111111111111111111111111 = 2,147,483,647 = 31 bits long

    LOC:Dec = p:Dec

    IF LOC:Dec < 0
       LOC:Negative = TRUE
       LOC:Dec      = -LOC:Dec
    .


    LOC:Int      = INT(LOC:Dec / 16)

    IF LOC:Int  ~= 0
       LOC:Result   = Dec_To_Hex(LOC:Int)
    .

    LOC:Rem      = LOC:Dec % 16

    LOC:Result   = CLIP(LOC:Result) & DNibble_To_Hex(LOC:Rem)


    IF LOC:Negative = TRUE
       LOC:Result   = '-' & CLIP(LOC:Result)
    .

    RETURN(LOC:Result)
