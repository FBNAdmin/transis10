

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Get_1st_Element_From_Delim_Str_Fast_ FUNCTION (p:Delim_List, p:Delim, p:Chop_Out, p:Case_Sensitive, p:Size) ! Declare Procedure
LOC:Delim       &STRING
  CODE                                                     ! Begin processed code
    LOC:Delim  &= NEW(STRING(LEN(p:Delim)))
    LOC:Delim   = p:Delim
    RETURN( Get_1st_Element_From_Delim_Str_Fast(p:Delim_List, LOC:Delim, p:Chop_Out, p:Case_Sensitive, p:Size) )
Get_1st_Element_From_Delim_Str_old FUNCTION (p:Delim_List, p:Delim, p:Chop_Out, p:Case_Sensitive) ! Declare Procedure
LOC:Delim_Pos        ULONG                                 !Position of deliminator
LOC:Delim_Len        ULONG                                 !Length of the deliminator
LOC:Len              ULONG                                 !
LOC_Use_Str             &STRING

LOC_Use_Str_No_Case     &CSTRING

LOC_Delim               &CSTRING
Str_Cls     CLASS
Str_1           &STRING
Init            PROCEDURE(STRING)
Destruct        PROCEDURE
            .


LOC_My_Str_Ele      Str_Cls
  CODE                                                     ! Begin processed code
    ! (p:Delim_List, p:Delim, p:Chop_Out - optional defaulted, p:Case_Sensitive)
    ! (*STRING, STRING, BYTE = 0, BYTE = 0),STRING
    !  String, Delim, Chop
    IF CLIP(p:Delim_List) ~= ''
       LOC:Len                 = LEN(CLIP(p:Delim_List))

       LOC_Use_Str            &= NEW(String(LOC:Len))
       LOC_Use_Str_No_Case    &= NEW(CString(LOC:Len+1))

       IF LEN(CLIP(p:Delim)) = 0
          LOC_Delim           &= NEW(CString(1+1))
       ELSE
          LOC_Delim           &= NEW(CString(LEN(CLIP(p:Delim))+1))
       .
       ! We are going to assume that if the p:Delim is empty the user wants to split on space!
       IF CLIP(p:Delim) = ''
          LOC_Delim            = ' '
          LOC:Delim_Len        = 1
       ELSE
          IF p:Case_Sensitive = FALSE
             LOC_Delim         = CLIP( UPPER( p:Delim ) )       ! took out LEFT
          ELSE
             LOC_Delim         = CLIP( p:Delim )
          .
          LOC:Delim_Len        = LEN(CLIP(LOC_Delim))
       .

       LOC_Use_Str             = p:Delim_List

       IF p:Case_Sensitive = FALSE
          LOC_Use_Str_No_Case  = UPPER(CLIP(p:Delim_List))
       ELSE
          LOC_Use_Str_No_Case  = p:Delim_List                   ! No clip needed - CLIP(p:Delim_List)
       .

       LOC:Delim_Pos           = INSTRING(LOC_Delim, LOC_Use_Str_No_Case, 1)

       IF LOC:Delim_Pos > 0                                     ! We have a comma
          IF LOC:Delim_Pos > 1                                  ! If we dont have ,something
             LOC_My_Str_Ele.Init( LOC_Use_Str[1 : (LOC:Delim_Pos - 1)] )
          ELSE                                                  ! > 0 but NOT > 1    =   1  - found at pos 1 so chop off - done below
             LOC_My_Str_Ele.Init( ' ' )
          .

          IF p:Chop_Out = TRUE
             IF (LOC:Delim_Pos + LOC:Delim_Len) <= LOC:Len
                p:Delim_List   = LOC_Use_Str[(LOC:Delim_Pos + LOC:Delim_Len) : LOC:Len]
             ELSE
                p:Delim_List   = ''
          .  .
       ELSE                                                     ! No comma in the string
          LOC_My_Str_Ele.Init( LOC_Use_Str )
          IF p:Chop_Out = TRUE
             p:Delim_List      = ''
       .  .

       DISPOSE(LOC_Use_Str)
       DISPOSE(LOC_Use_Str_No_Case)
       DISPOSE(LOC_Delim)

       RETURN( LOC_My_Str_Ele.Str_1 )          ! Clipping not needed - string is made right size
    ELSE
       RETURN( '' )
    .


! ----------- Class Procs -----------------
Str_Cls.Init        PROCEDURE(STRING Our_Str)
    CODE
    IF LEN(CLIP(Our_Str)) > 0
       SELF.Str_1     &= NEW(STRING( LEN(CLIP(Our_Str)) ))
    ELSE
       SELF.Str_1     &= NEW(STRING( 1 ))
    .

    SELF.Str_1      = Our_Str
    RETURN



Str_Cls.Destruct    PROCEDURE()
    CODE
    DISPOSE(SELF.Str_1)
    RETURN
Get_1st_Element_From_Delim_Str_100 FUNCTION (p:Delim_List, p:Delim, p:Chop_Out, p:Case_Sensitive) ! Declare Procedure
LOC:Delim_Pos        ULONG,AUTO                            !Position of deliminator
LOC:Delim_Len        ULONG,AUTO                            !Length of the deliminator
LOC:Len              ULONG,AUTO                            !
LOC_Use_Str             STRING(100),AUTO
LOC_Use_Str_No_Case     CSTRING(101),AUTO

LOC_Result              STRING(100),AUTO
  CODE                                                     ! Begin processed code
    ! (p:Delim_List, p:Delim, p:Chop_Out - optional defaulted, p:Case_Sensitive)
    ! (*STRING, STRING, BYTE = 0, BYTE = 0),STRING
    !  String, Delim, Chop
    IF CLIP(p:Delim_List) ~= ''
       LOC:Len                 = LEN(CLIP(p:Delim_List))

       ! We are going to assume that if the p:Delim is empty the user wants to split on space!
       IF CLIP(p:Delim) = ''
          LOC:Delim_Len        = 1
       ELSE
          IF p:Case_Sensitive = FALSE
             p:Delim           = UPPER(CLIP(p:Delim))
          .
          LOC:Delim_Len        = LEN(CLIP(p:Delim))
       .

       LOC_Use_Str             = p:Delim_List

       IF p:Case_Sensitive = FALSE
          LOC_Use_Str_No_Case  = UPPER(CLIP(p:Delim_List))
       ELSE
          LOC_Use_Str_No_Case  = p:Delim_List                   ! No clip needed - CLIP(p:Delim_List)
       .

       LOC:Delim_Pos           = INSTRING(p:Delim[1 : LOC:Delim_Len], LOC_Use_Str_No_Case, 1)

       IF LOC:Delim_Pos > 0                                     ! We have a comma
          IF LOC:Delim_Pos > 1                                  ! If we dont have ,something
             LOC_Result = LOC_Use_Str[1 : (LOC:Delim_Pos - 1)]
          ELSE                                                  ! > 0 but NOT > 1    =   1  - found at pos 1 so chop off - done below
             LOC_Result = ''
          .

          IF p:Chop_Out = TRUE
             IF (LOC:Delim_Pos + LOC:Delim_Len) <= LOC:Len
                p:Delim_List   = LOC_Use_Str[(LOC:Delim_Pos + LOC:Delim_Len) : LOC:Len]
             ELSE
                p:Delim_List   = ''
          .  .
       ELSE                                                     ! No comma in the string
          LOC_Result    = LOC_Use_Str
          IF p:Chop_Out = TRUE
             p:Delim_List      = ''
       .  .

       RETURN( LOC_Result )          ! Clipping not needed - string is made right size
    ELSE
       RETURN( '' )
    .


Get_1st_Element_From_Delim_Str_2 FUNCTION (p:Delim_List, p:Delim, p:Chop_Out, p:Case_Sensitive) ! Declare Procedure
LOC:Delim_Pos        ULONG,AUTO                            !Position of deliminator
LOC:Delim_Len        ULONG,AUTO                            !Length of the deliminator
LOC:Len              ULONG,AUTO                            !
LOC_Use_Str             &STRING
LOC_Use_Str_No_Case     &CSTRING
Str_Cls     CLASS
Str_1           &STRING
Init            PROCEDURE(STRING)
Destruct        PROCEDURE
            .


LOC_My_Str_Ele      Str_Cls
  CODE                                                     ! Begin processed code
    ! (p:Delim_List, p:Delim, p:Chop_Out - optional defaulted, p:Case_Sensitive)
    ! (*STRING, STRING, BYTE = 0, BYTE = 0),STRING
    !  String, Delim, Chop
    IF CLIP(p:Delim_List) ~= ''
       LOC:Len                 = LEN(CLIP(p:Delim_List))

       LOC_Use_Str            &= NEW(String(LOC:Len))
       LOC_Use_Str_No_Case    &= NEW(CString(LOC:Len+1))

       ! We are going to assume that if the p:Delim is empty the user wants to split on space!
       IF CLIP(p:Delim) = ''
          LOC:Delim_Len        = 1
       ELSE
          IF p:Case_Sensitive = FALSE
             p:Delim           = UPPER(CLIP(p:Delim))
          .
          LOC:Delim_Len        = LEN(CLIP(p:Delim))
       .

       LOC_Use_Str             = p:Delim_List

       IF p:Case_Sensitive = FALSE
          LOC_Use_Str_No_Case  = UPPER(CLIP(p:Delim_List))
       ELSE
          LOC_Use_Str_No_Case  = p:Delim_List                   ! No clip needed - CLIP(p:Delim_List)
       .


       LOC:Delim_Pos   = 0
       LOOP
          LOC:Delim_Pos   += 1

          IF (LOC:Delim_Pos + LOC:Delim_Len - 1) > LOC:Len
             LOC:Delim_Pos  = 0
             BREAK
          .
          IF LOC_Use_Str_No_Case[LOC:Delim_Pos : (LOC:Delim_Pos + LOC:Delim_Len - 1)] = p:Delim[1 : LOC:Delim_Len]
             ! Found a delimiter at Delim Pos - want to exit
             BREAK
       .  .


       IF LOC:Delim_Pos > 0                                     ! We have a comma
          IF LOC:Delim_Pos > 1                                  ! If we dont have ,something
             LOC_My_Str_Ele.Init( LOC_Use_Str[1 : (LOC:Delim_Pos - 1)] )
          ELSE                                                  ! > 0 but NOT > 1    =   1  - found at pos 1 so chop off - done below
             LOC_My_Str_Ele.Init( ' ' )
          .

          IF p:Chop_Out = TRUE
             IF (LOC:Delim_Pos + LOC:Delim_Len) <= LOC:Len
                p:Delim_List   = LOC_Use_Str[(LOC:Delim_Pos + LOC:Delim_Len) : LOC:Len]
             ELSE
                p:Delim_List   = ''
          .  .
       ELSE                                                     ! No comma in the string
          LOC_My_Str_Ele.Init( LOC_Use_Str )
          IF p:Chop_Out = TRUE
             p:Delim_List      = ''
       .  .

       DISPOSE(LOC_Use_Str)
       DISPOSE(LOC_Use_Str_No_Case)

       RETURN( LOC_My_Str_Ele.Str_1 )          ! Clipping not needed - string is made right size
    ELSE
       RETURN( '' )
    .


! ----------- Class Procs -----------------
Str_Cls.Init        PROCEDURE(STRING Our_Str)
    CODE
    IF LEN(CLIP(Our_Str)) > 0
       SELF.Str_1     &= NEW(STRING( LEN(CLIP(Our_Str)) ))
    ELSE
       SELF.Str_1     &= NEW(STRING( 1 ))
    .

    SELF.Str_1      = Our_Str
    RETURN



Str_Cls.Destruct    PROCEDURE()
    CODE
    DISPOSE(SELF.Str_1)
    RETURN
Replace_Strings      FUNCTION (p:String, p:Find, p:Replace, p:Occurances, p:No_Case) ! Declare Procedure
L:Count              LONG                                  !
L:Pos                LONG                                  !
  CODE                                                     ! Begin processed code
    ! (p:String, p:Find, p:Replace, p:Occurances, p:No_Case)
    ! (*STRING, STRING, STRING, LONG=0, BYTE=0), LONG

    ! Notes: p:Find & p:Replace must be the length of the string to find / replace - i.e. they will not be clipped
    !        To have a no-case option we would have to make a copy of the string and work with that for the search

    IF LEN(p:Find) = 0                                                  ! Nothing - not even a space
       L:Count  = -2
    ELSIF p:Find = p:Replace                                        ! Find = Replace - nothing to do
       L:Count  = -1
    ELSIF p:No_Case = TRUE AND UPPER(p:Find) = UPPER(p:Replace)     ! Find = Replace - nothing to do - no case
       L:Count  = -1
    ELSE
       IF p:No_Case = TRUE
          p:Find    = UPPER(p:Find)
       .

       LOOP
          IF p:Occurances > 0
             IF p:Occurances <= L:Count
                BREAK
          .  .

          IF p:No_Case = TRUE
             L:Pos    = INSTRING(p:Find, UPPER(p:String), 1)
          ELSE
             L:Pos    = INSTRING(p:Find, p:String, 1)
          .

          IF L:Pos <= 0
             BREAK
          .

          L:Count    += 1

          IF L:Pos = 1
             IF L:Pos + LEN(p:Find) <= LEN(CLIP(p:String))
                p:String   = p:Replace & p:String[L:Pos + LEN(p:Find) : LEN(CLIP(p:String))]
             ELSE
                p:String   = ''
             .
          ELSE
             IF L:Pos + LEN(p:Find) <= LEN(CLIP(p:String))
                p:String   = p:String[1 : L:Pos - 1] & p:Replace & p:String[L:Pos + LEN(p:Find) : LEN(CLIP(p:String))]
             ELSE
                p:String   = p:String[1 : L:Pos - 1] & p:Replace
    .  .  .  .

    RETURN( L:Count )







