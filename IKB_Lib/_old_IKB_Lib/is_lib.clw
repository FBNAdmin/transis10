  PROGRAM
_abcdllmode_   equate(0)
_abclinkmode_  equate(1)

    INCLUDE('IvansAPI.CLW','Equates')
    INCLUDE('IKB_LIB.EQU')

  MAP
     MODULE('IS_Execute.clw')
       IS_Execute(unsigned, STRING, <STRING>, <STRING>)
     END

    INCLUDE('IvansAPI.CLW','Prototypes')
    MODULE('Windows.DLL')
 ShellExecute_(UNSIGNED,LONG,*CSTRING,LONG,*CSTRING,SIGNED),UNSIGNED,PASCAL,RAW  !,NAME('SHELLEXECUTEA')
 ShellExecute(UNSIGNED,*CSTRING,*CSTRING,*CSTRING,*CSTRING,SIGNED),UNSIGNED,PASCAL,RAW,NAME('ShellExecuteA')

 GetVolumeInformation(*CSTRING,*CSTRING,ULONG,*ULONG,*ULONG,*ULONG,*CSTRING,ULONG),BOOL,PASCAL,RAW,NAME('GetVolumeInformationA')

 FindWindow(*CSTRING,*CSTRING),UNSIGNED,PASCAL,RAW,NAME('FindWindowA')
 PostMessage(UNSIGNED,UNSIGNED,UNSIGNED,LONG),BOOL,PASCAL,NAME('PostMessageA')
 SendMessage(UNSIGNED,UNSIGNED,UNSIGNED,LONG),ULONG,PASCAL,NAME('SendMessageA')
    END

!LPCSTR                  EQUATE(CSTRING)    !Usage:Pass the Label of the LPCSTR
!DWORD                   EQUATE(ULONG)
!BOOL                    EQUATE(SIGNED)
  END
    !INCLUDE('stdcom2.inc')


    CODE
    RETURN