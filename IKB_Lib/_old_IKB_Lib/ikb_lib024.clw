

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Match_Element_In_Delim_Str FUNCTION (p:Delim_List, p:Delim, p:Element, p:Chop_Out, p:Spaced) ! Declare Procedure
LOC:Result           LONG                                  !
LOC:Len              LONG                                  !
LOC_Ret_Str         &STRING
!LOC_Use_Str         &STRING

LOC_String_Q        QUEUE,PRE(LO_SQ)
Str                     &STRING
                    .

  CODE                                                     ! Begin processed code
    ! (p:Delim_List, p:Delim, p:Element, p:Chop_Out, p:Spaced)
    ! (*STRING, STRING, STRING, BYTE=0, BYTE=1),LONG
    !
    ! p:Spaced      - strip leading spaces off elements
    !                 also adds a space before each element when changing the string
    !
    !
    ! We want to check through the delimited list and report if we find the element what position we found it
    ! at.
    ! We also want to change our delim list by removing the string if we have been asked to.

    LOC:Len         = LEN(p:Delim_List)

    LOC_Ret_Str    &= NEW(String(LOC:Len))
    !LOC_Use_Str    &= NEW(String(LOC:Len))

    LOC_Ret_Str     = p:Delim_List

    FREE(LOC_String_Q)
    LOOP
       IF CLIP(LOC_Ret_Str) = ''
          BREAK
       .

       LO_SQ:Str   &= NEW(STRING(LOC:Len))
       LO_SQ:Str    = Get_1st_Element_From_Delim_Str(LOC_Ret_Str, p:Delim, TRUE)

       IF p:Spaced = TRUE
          LO_SQ:Str = LEFT(LO_SQ:Str)
       .

       ADD(LOC_String_Q)
    .


    ! Now look through the elements for our element - creating the return string if required
    LOC_Ret_Str  = ''

    Idx_#        = 0
    LOOP
       Idx_# += 1
       GET(LOC_String_Q, Idx_#)
       IF ERRORCODE()
          BREAK
       .

       IF LOC:Result = 0                                            ! Return 1st occurance
          IF CLIP(LO_SQ:Str) = CLIP(p:Element)
             LOC:Result     = Idx_#
       .  .

       IF p:Chop_Out = TRUE
          IF LOC:Result ~= Idx_#
             IF CLIP(LOC_Ret_Str) = ''
                LOC_Ret_Str    = LO_SQ:Str
             ELSE
                IF p:Spaced = TRUE
                   LOC_Ret_Str = CLIP(LOC_Ret_Str) & p:Delim & ' ' & LO_SQ:Str
                ELSE
                   LOC_Ret_Str = CLIP(LOC_Ret_Str) & p:Delim & LO_SQ:Str
       .  .  .  .

       DISPOSE(LO_SQ:Str)
    .

    IF p:Chop_Out = TRUE
       p:Delim_List = LOC_Ret_Str
    .

    FREE(LOC_String_Q,)
    DISPOSE(LOC_Ret_Str)

    RETURN(LOC:Result)
