

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

To_ASCII_Codes       FUNCTION (p:To_Convert)               ! Declare Procedure
LOC:Converted        STRING(10000)                         !
  CODE                                                     ! Begin processed code
    ! (p:To_Convert)

    LOC:Converted  = ''

    LOOP I# = 1 TO LEN(CLIP(p:To_Convert))
       IF (p:To_Convert[I#] = EQU:CR) OR (p:To_Convert[I#] = EQU:LF)
          LOC:Converted   = CLIP(LOC:Converted) & ' -<<' & VAL(p:To_Convert[I#]) & '>- '
       ELSE
          LOC:Converted   = CLIP(LOC:Converted) & '<<' & VAL(p:To_Convert[I#]) & '>'
    .  .

    !LOC:Reply_Area  = CLIP(R:Str_)


    RETURN(LOC:Converted)
