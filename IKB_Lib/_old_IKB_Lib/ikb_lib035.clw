

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Time_Difference      FUNCTION (p:Time_From, p:Time_To)     ! Declare Procedure
LOC:Difference       LONG                                  !
  CODE                                                     ! Begin processed code
    ! (p:Time_From, p:Time_To)

    IF p:Time_From < p:Time_To
       LOC:Difference   = p:Time_To - p:Time_From
    ELSE
       ! Out Time From is greater than Time To - the next day is assumed
       
       LOC:Difference  = 8640000 - p:Time_From              ! Left in previous day
       LOC:Difference += (p:Time_To - 1)                    ! Add on from next day
    .

    RETURN(LOC:Difference)
