

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

String_Splice        FUNCTION (p:TheString, p:ReturnString1, p:ReturnString2, p:SplicePos, p:len_to_sp, p:Other_Split_Chrs) ! Declare Procedure
LI1                  LONG                                  !Loop index 1
LOC:Last_Space_Pos   LONG                                  !Position of the last space found
LOC:TheString        STRING(50000)                         !local copy of passed string
LOC:ReturnString1    CSTRING(50001)                        !return string 1 local definition
LOC:ReturnString2    CSTRING(50001)                        !2nd return string local definition
LOC:Splice_Pos       LONG                                  !Splice the string close to this position
LOC:TheString_Length LONG                                  !
LOC:ReturnString1_Size LONG                                !
LOC:ReturnString2_Size LONG                                !
LOC:Return           BYTE                                  !State of success
LOC:Len_Split_Chars  ULONG                                 !
  CODE                                                     ! Begin processed code
    ! (STRING     , *CString       , *CString       , LONG=0     , LONG=0     , <*CSTRING>),BYTE
    ! (p:TheString, p:ReturnString1, p:ReturnString2, p:SplicePos, p:len_to_sp, p:Other_Split_Chrs)
    !    1              2                3              4            5             6
    !
    ! Note: p:Other_Split_Chrs is used char by char
    !
    ! EQU:CR      EQUATE(CHR(13))     ! Clarion
    ! EQU:LF      EQUATE(CHR(10))     ! Clarion

    LOC:Len_Split_Chars  = 0
    IF OMITTED(6) = FALSE
       LOC:Len_Split_Chars  = LEN(p:Other_Split_Chrs)
    .

    !MESSAGE('p:Other_Split_Chrs 1: ' & VAL(p:Other_Split_Chrs[1]))


    ! Assign parameters to local variables
    LOC:Return          = 0                         ! success flag = False
    LOC:TheString       = p:TheString
    LOC:ReturnString1   = p:ReturnString1
    LOC:ReturnString2   = p:ReturnString2
    LOC:Splice_Pos      = p:SplicePos

    LOC:TheString_Length    = LEN(CLIP(LOC:TheString))
    LOC:ReturnString1_Size  = SIZE(p:ReturnString1)
    LOC:ReturnString2_Size  = SIZE(p:ReturnString2)

    IF LOC:TheString_Length = 0                     ! no characters in the string to split!
      RETURN(0)
    .

        ! Zero given OR given splice point is greater then the 1st return string size
    IF (LOC:Splice_Pos = 0) OR (LOC:ReturnString1_Size < LOC:Splice_Pos)
      LOC:Splice_Pos = LOC:ReturnString1_Size           ! Set splice point to 1st return size
    .

        ! Out of bounds check (MAXIMUN) could be used instead but length of actual string is ok
    IF LOC:Splice_Pos < LOC:TheString_Length            ! Do not allow out of bounds
      LOC:Last_Space_Pos = 0
      LI1 = 0
      LOOP UNTIL LI1 > LOC:Splice_Pos
        LI1 += 1
        IF LI1 > LOC:Splice_Pos
             ! IF chars left in string MORE then space in string 2 or NO split pos found
          IF (LOC:TheString_Length - LOC:Last_Space_Pos) > LOC:ReturnString2_Size
              ! Assign Split_Pos no of chars to retstring and the up to
            LOC:ReturnString1 = LOC:TheString[1 : LOC:Splice_Pos]
            LOC:ReturnString2 = LOC:TheString[(LOC:Splice_Pos + 1) : (LOC:Splice_Pos + LOC:ReturnString2_Size)]
            LOC:Return        = 0
            BREAK
          ELSIF LOC:Last_Space_Pos = 0              ! No space found to split on
            LOC:ReturnString1 = LOC:TheString[1 : LOC:Splice_Pos]
            LOC:ReturnString2 = LOC:TheString[(LOC:Splice_Pos + 1) : LOC:TheString_Length]
            BREAK
          ELSE                                      ! Perform split on found space
            ! IKB - 10_05_01 - start
            IF p:len_to_sp ~= 0
!               MESSAGE('LOC:Last_Space_Pos + p:len_to_sp) >= LOC:Splice_Pos||' & |
!                        LOC:Last_Space_Pos & ' + ' & p:len_to_sp & ' >= ' & LOC:Splice_Pos)

               ! Check that the space is within this many chars of the LOC:Splice_Pos
               ! else splice at the splice pos
               IF (LOC:Last_Space_Pos + p:len_to_sp) >= LOC:Splice_Pos
                  ! This is ok - eg.  150 + 16 >= 160
               ELSE
                  ! This is not - eg.  150 + 6 ~>= 160
                  LOC:Last_Space_Pos    = LOC:Splice_Pos
            .  .
            ! IKB - 10_05_01 - end

            LOC:ReturnString1 = LOC:TheString[1 : LOC:Last_Space_Pos]
            LOC:ReturnString2 = LOC:TheString[(LOC:Last_Space_Pos + 1) : LOC:TheString_Length]
            LOC:Return        = 1
            BREAK
        . .

        IF LOC:TheString[LI1] = ' '
          LOC:Last_Space_Pos = LI1
        .

        ! IKB - 17_06_01
        IF (LOC:Last_Space_Pos ~= LI1) AND (LOC:Len_Split_Chars > 0)
           ! There are other split chars as well
           LOOP I_# = 1 TO LOC:Len_Split_Chars
              IF LOC:TheString[LI1] = p:Other_Split_Chrs[I_#]
                 LOC:Last_Space_Pos = LI1
                 BREAK
      . .  .  .
    ELSE
      LOC:ReturnString1 = LOC:TheString[1 : LOC:TheString_Length]
      LOC:ReturnString2 = ''                                ! There aren't any more chars
    .

    ! Assign return parameters to local variables
    p:ReturnString1 = LOC:ReturnString1
    p:ReturnString2 = LOC:ReturnString2

    RETURN(LOC:Return)
