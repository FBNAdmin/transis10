

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Insert_Into_Delim_Str FUNCTION (p:String, p:Delim, p:Element, p:Position, p:Replace, p:Add_Space) ! Declare Procedure
LOC:Result           LONG                                  !
LOC:Insert_Space     CSTRING(2)                            !
LOC_String_Q        QUEUE,PRE(LO_SQ)
Str                     &STRING
                    .

  CODE                                                     ! Begin processed code
    ! (p:String, p:Delim, p:Element, p:Position, p:Replace, p:Add_Space)
    ! Problem with this is if p:String is exceeded in size?

    FREE(LOC_String_Q)

    IF p:Add_Space  = TRUE
       LOC:Insert_Space     = ' '
    .

    IF p:Position = 0                                                           ! End
       IF p:Replace = TRUE
          throw_away_"   = Get_Last_Element_From_Delim_Str(p:String, p:Delim, TRUE)
       .

       IF CLIP(p:String) = ''
          p:String     = p:Element
          LOC:Result   = 1
       ELSE
          p:String     = CLIP(p:String)    & CLIP(p:Delim) & LOC:Insert_Space &   p:Element
          LOC:Result   = -2
       .
    ELSIF p:Position = 1                                                        ! Begining
       IF p:Replace = TRUE
          throw_away_"   = Get_1st_Element_From_Delim_Str(p:String, p:Delim, TRUE)
       .

       IF CLIP(p:String) = ''
          p:String     = p:Element
          LOC:Result   = 1
       ELSE
          p:String     = CLIP(p:Element)   & CLIP(p:Delim) & LOC:Insert_Space &   p:String
          LOC:Result   = -1
       .
    ELSE
       Len_#    = LEN(CLIP(p:String))
       LOOP
          IF CLIP(p:String) = ''
             BREAK
          .

          LO_SQ:Str   &= NEW(STRING(Len_#))
          LO_SQ:Str    = Get_1st_Element_From_Delim_Str(p:String, p:Delim, TRUE)

          !IF p:Chopped = TRUE
          LO_SQ:Str    = LEFT(LO_SQ:Str)
          !.

          ADD(LOC_String_Q)
       .

       Inserted_#   = FALSE
       p:String     = ''
       Idx_#        = 0
       LOOP
          Idx_# += 1
          GET(LOC_String_Q, Idx_#)
          IF ERRORCODE()
             BREAK
          .

          IF Idx_# = p:Position
             Inserted_#   = TRUE

             ! Insert element
             IF CLIP(p:String) = ''
                p:String  = p:Element
             ELSE
                p:String  = CLIP(p:String)    & CLIP(p:Delim) & LOC:Insert_Space &   p:Element
             .

             IF p:Replace = FALSE                                           ! Add on old str from pos
                p:String  = CLIP(p:String)    & CLIP(p:Delim) & LOC:Insert_Space &   LO_SQ:Str
             .
          ELSE
             IF CLIP(p:String) = ''
                p:String  = LO_SQ:Str
             ELSE
                p:String  = CLIP(p:String)    & CLIP(p:Delim) & LOC:Insert_Space &   LO_SQ:Str
          .  .

          DISPOSE(LO_SQ:Str)
       .

       IF Inserted_# = FALSE                        ! Maybe not enough elements in list
          Recs_#    = RECORDS(LOC_String_Q)
          IF Recs_# >= p:Position
             !?
          ELSE
             LOOP
                Recs_# += 1
                IF Recs_# <= p:Position
                   p:String  = CLIP(p:String)    & CLIP(p:Delim) & LOC:Insert_Space &   p:Element
                   BREAK
                ELSE
                   p:String  = CLIP(p:String)    & CLIP(p:Delim)
       .  .  .  .

       LOC:Result   = RECORDS(LOC_String_Q) + 1
    .

    FREE(LOC_String_Q)

    RETURN(LOC:Result)
