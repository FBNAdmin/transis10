

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Get_String_Up_To_Delim FUNCTION (p:String, p:Delim)        ! Declare Procedure
R:Last_Delim         ULONG                                 !Last point delim found at
R:Length_Of_String   ULONG                                 !
R:String             STRING(1000)                          !
LOC:Ret_String       STRING(1000)                          !
LOC:Length_Delim     BYTE                                  !
LOC:Delim            STRING(1000)                          !
  CODE                                                     ! Begin processed code
    !(p:String, p:Delim)

    R:String    = LEFT(p:String)
    LOC:Delim   = LEFT(p:Delim)

    LOC:Length_Delim    = LEN(CLIP(LOC:Delim))
    R:Length_Of_String  = LEN(CLIP(R:String))

    LOOP R:Last_Delim = R:Length_Of_String TO 1 BY -1
       IF R:String[R:Last_Delim] = CLIP(LEFT(LOC:Delim))
          BREAK
    .  .

    IF R:Last_Delim > 0
       IF R:Last_Delim > 1                       ! We have one not at position 1
          LOC:Ret_String    = R:String[1 : (R:Last_Delim - 1)]
       ELSE
          IF R:String[R:Last_Delim] = CLIP(LEFT(LOC:Delim))
             LOC:Ret_String   = ''               ! nothing before it then
          ELSE
             LOC:Ret_String   = R:String         ! not found in string
       .  .
    ELSE
       LOC:Ret_String   = R:String
    .

    RETURN(LOC:Ret_String)
