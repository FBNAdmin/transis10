

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Week_Day             FUNCTION (P:Date)                     ! Declare Procedure
LOC:Week_Day         STRING(9)                             !
  CODE                                                     ! Begin processed code
  EXECUTE (P:Date % 7 + 1)
     LOC:Week_Day = 'Sunday'
     LOC:Week_Day = 'Monday'
     LOC:Week_Day = 'Tuesday'
     LOC:Week_Day = 'Wednesday'
     LOC:Week_Day = 'Thursday'
     LOC:Week_Day = 'Friday'
     LOC:Week_Day = 'Saturday'
  .

  RETURN(LOC:Week_Day)
