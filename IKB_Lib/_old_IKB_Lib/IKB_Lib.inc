!WA_Play_Button         FUNCTION(UNSIGNED),ULONG

Check_File_Existence   PROCEDURE(*STRING, BYTE=0),BYTE
Get_Registration_No    FUNCTION(),ULONG
Get_Registration_Id    FUNCTION(),ULONG
Calc_Registration_No   FUNCTION(ULONG),ULONG
Week_Day               FUNCTION(LONG),STRING
Week_Day_No            FUNCTION(LONG),BYTE
String_Splice          FUNCTION(STRING,*CString,*CString,LONG=0,LONG=0,<*CSTRING>),BYTE
Months_Between         FUNCTION(LONG, <LONG>),LONG
URLHandler             PROCEDURE(unsigned,STRING,<STRING>,<STRING>)
ISExecute              PROCEDURE(unsigned,STRING,<STRING>,<STRING>),LONG,PROC   ! (whandle, URL, p:Params, p:LauchDir)
Convert_Text_To_MS     FUNCTION(STRING),?  
Output_Text            PROCEDURE(STRING, USHORT=0, STRING, BYTE=1),LONG,PROC

Get_Last_Element_From_Delim_Str   FUNCTION(*STRING, STRING, BYTE = 0, BYTE = 0), STRING
Get_1st_Element_From_Delim_Str    FUNCTION(*STRING, STRING, BYTE = 0, BYTE = 0),STRING
Get_1st_Element_From_Delim_Str_2       FUNCTION(*STRING, *STRING, BYTE = 0, BYTE = 0),STRING
Get_1st_Element_From_Delim_Str_Fast    FUNCTION(*STRING, *STRING, BYTE = 0, BYTE = 0, LONG=100),STRING
Get_1st_Element_From_Delim_Str_Fast_   FUNCTION(*STRING, STRING, BYTE = 0, BYTE = 0, LONG=100),STRING
Get_Element_From_Delim_Str             PROCEDURE(*STRING, *STRING, LONG=0),STRING
Get_String_Up_To_Delim            FUNCTION(STRING, STRING),STRING
Match_Element_In_Delim_Str        FUNCTION(*STRING, STRING, STRING, BYTE=0, BYTE=1),LONG
Count_Delim_Str_Elements          FUNCTION(*STRING, STRING, BYTE=0),LONG,PROC
Insert_Into_Delim_Str             FUNCTION(*STRING, STRING, STRING, SHORT=0, BYTE=0, BYTE=1),LONG,PROC  ! (p:String, p:Delim, p:Element, p:Position, p:Replace, p:Add_Space)
Insert_Into_Delim_Str_2           PROCEDURE(*STRING, STRING, STRING, SHORT=0),LONG,PROC     ! (p:String, p:Delim, p:Element, p:Position)

Replace_Strings        PROCEDURE(*STRING, STRING, STRING, LONG=0, BYTE=0), LONG     ! (p:String, p:Find, p:Replace, p:Occurances, p:No_Case), Count
Replace_Chars          FUNCTION(STRING, *CSTRING, STRING, <*LONG>, BYTE=0, BYTE=0),STRING

To_ASCII_Codes         FUNCTION(STRING),STRING

Transpose_Str          FUNCTION(STRING),STRING
Hex_To_Dec             FUNCTION(STRING),LONG 
Byte_To_StrB           FUNCTION(BYTE),STRING
StrB_To_Byte           FUNCTION(STRING),BYTE

Dec_To_Hex             FUNCTION(LONG),STRING
DNibble_To_Hex         FUNCTION(BYTE),STRING

Pad_Str                FUNCTION(STRING, SHORT, *CSTRING),STRING
StrB_To_Octet          FUNCTION(STRING),STRING
Is_Even                FUNCTION(LONG),BYTE

!Get_Volume_Info        FUNCTION(),ULONG
!Show_Volume_Info       PROCEDURE(STRING)

Week                   FUNCTION(LONG),BYTE
Quarter                FUNCTION(LONG),BYTE
Weeks                  FUNCTION(LONG, BYTE=1, <SHORT>),LONG
File_User_Access       PROCEDURE(BYTE),BYTE
Month_Week             FUNCTION(LONG),LONG

Remove_White_Space     PROCEDURE(STRING),STRING

Count_SubStr_In_Str    PROCEDURE(STRING,STRING),ULONG

Enumerate_Drives       PROCEDURE(BYTE = 0, <STRING>),STRING
Time_Difference        PROCEDURE(LONG,LONG),LONG

Month_Name             PROCEDURE(LONG),STRING

Date_Time_Difference   PROCEDURE(LONG,LONG,LONG,LONG, BYTE=0),STRING
Double_Char            PROCEDURE(STRING, STRING),STRING

Centre_Control_In_Control   PROCEDURE(?, ?)

Capitalise             PROCEDURE(STRING, BYTE=0, BYTE=1),STRING

Get_New_Control_Size   PROCEDURE(?, LONG=0,  LONG=0, BYTE=0)
DataPath_To_Path       PROCEDURE(STRING,STRING,<STRING>),STRING,PROC
Path_To_DataPath       PROCEDURE(STRING,STRING,<STRING>),STRING,PROC

Add_to_List            PROCEDURE(STRING, *STRING, STRING, BYTE=0, <STRING>, BYTE=0, BYTE=0)
Add_to_List_C          PROCEDURE(STRING, *CSTRING, STRING, BYTE=0, <STRING>, BYTE=0)
Get_File_List          PROCEDURE(FILE:Queue_IKB p_FileQ, STRING, <STRING>, LONG=1, LONG=0, <STRING>), LONG, PROC

SQL_Ret_DateT_G        PROCEDURE(STRING, <BYTE>),STRING
SQL_Get_DateT_G        PROCEDURE(LONG, LONG=0, BYTE=0),STRING

! ES Procedures
PopupCalendar          PROCEDURE(*? PassedDate,Byte FixYear, <LONG XPos>, <Long YPos>)
PopupTime              PROCEDURE(*? PassedTime,Byte FixYear, <LONG XPos>, <Long YPos>)
MyPopup                PROCEDURE(String Items, Long XPos, Long YPos), BYTE
GetRatio               PROCEDURE(*LONG p_DuPiX, *LONG p_DuPiY)

SQL_Deformat           PROCEDURE(STRING),STRING

Get_Tag                PROCEDURE(*STRING p_Str, BYTE p_Remove=0, <STRING p_TagLeft>, <STRING p_TagRight>), STRING

Date_Time_Advance      PROCEDURE(*DATE, <*TIME>, BYTE, LONG)    ! (p_Date, p_Time, p_Option, p_Val)
