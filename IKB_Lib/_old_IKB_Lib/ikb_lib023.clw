

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Is_Even              FUNCTION (p:Value)                    ! Declare Procedure
LOC:Even             BYTE,AUTO                             !
  CODE                                                     ! Begin processed code
    ! (p:Value)

    LOC:Even = FALSE

    IF INT(p:Value / 2) = (p:Value / 2)
       LOC:Even = TRUE
    .

    RETURN(LOC:Even)
