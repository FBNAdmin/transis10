

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

StrB_To_Byte         FUNCTION (p:Str)                      ! Declare Procedure
LOC:Byte_Val         BYTE                                  !
  CODE                                                     ! Begin processed code
    ! (p:Str)   - should never be greater than 8
    p:Str           = LEFT(p:Str)

    Len_#           = LEN(CLIP(p:Str))
    LOC:Byte_Val    = 0

    Exp_#           = 0
    LOOP I_# = Len_# TO 1 By -1             ! 10001000 - start at end
       Exp_#  += 1

       IF p:Str[I_#] = 1
          LOC:Byte_Val    += 1 * 2^(Exp_# - 1)
       ELSE
          IF p:Str[I_#] ~= 0
             ! Not a binary value
             LOC:Byte_Val    = 0
             BREAK
    .  .  .

          
    RETURN(LOC:Byte_Val)
