

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Month_Name           FUNCTION (p:Date)                     ! Declare Procedure
LOC:Month            STRING(20)                            !
  CODE                                                     ! Begin processed code
    ! (p:Date)

    EXECUTE MONTH(p:Date)
       LOC:Month    = 'January'
       LOC:Month    = 'February'
       LOC:Month    = 'March'
       LOC:Month    = 'April'
       LOC:Month    = 'May'
       LOC:Month    = 'June'
       LOC:Month    = 'July'
       LOC:Month    = 'August'
       LOC:Month    = 'September'
       LOC:Month    = 'October'
       LOC:Month    = 'November'
       LOC:Month    = 'December'
    .

    RETURN(CLIP(LOC:Month))
