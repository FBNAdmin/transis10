

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Date_Time_Difference FUNCTION (p:Date_From, p:Time_From, p:Date_To, p:Time_To, p:Option) ! Declare Procedure
LOC:Difference       STRING(50)                            !
LOC:From_Date        LONG                                  !
LOC:From_Time        LONG                                  !
LOC:To_Date          LONG                                  !
LOC:To_Time          LONG                                  !
LOC:TDY_Group        GROUP,PRE(L_TDY)                      !
Time                 LONG                                  !
Days                 LONG                                  !
Years                LONG                                  !
                     END                                   !
  CODE                                                     ! Begin processed code
    ! (p:Date_From, p:Time_From, p:Date_To, p:Time_To, p:Option)
    ! Returns:  (years,) days, time (hours:minutes:seconds) t4

    DO From_To_Alloc

    L_TDY:Time  = LOC:To_Time - LOC:From_Time + 1

    L_TDY:Days  = LOC:To_Date - LOC:From_Date
     
    IF L_TDY:Time < 0                                               ! To time less than from time
       L_TDY:Days  -= 1                                             ! Reduce days by 1 for less than full day
       L_TDY:Time   = 8640000 + L_TDY:Time                              ! L_TDY:Time = 100 000 - 150 000 = - 50 000
    .


    IF p:Option = 1                                             ! We want years also
       L_TDY:Years      = L_TDY:Days / 365
       L_TDY:Days       = L_TDY:Days - (L_TDY:Years * 365)

       LOC:Difference   = L_TDY:Years & ','
    .

    LOC:Difference  = CLIP(LOC:Difference) & L_TDY:Days & ',' & FORMAT(L_TDY:Time,@t4)

    RETURN(LOC:Difference)



!    L_TDY:Years     = YEAR(LOC:From_Date) - YEAR(LOC:To_Date)
!    L_TDY:Time      = p:Time_To - p:Time_From
!
!    IF MONTH(p:Date_From) < MONTH(p:Date_To)             ! June < July = more than a year
!       L_TDY:Days    = MONTH(p:Date_From) - MONTH(p:Date_To)
!    ELSE
!       L_TDY:Years   -= 1                                    ! June > July = less than a year, reduce years
!    .
From_To_Alloc           ROUTINE
    IF p:Date_From < p:Date_To
       LOC:From_Date    = p:Date_From
       LOC:From_Time    = p:Time_From
       LOC:To_Date      = p:Date_To
       LOC:To_Time      = p:Time_To
    ELSE
       IF p:Date_From = p:Date_To                               ! Same day or no day - assumed same day
          IF p:Time_From < p:Time_To                            ! Order time entries
             LOC:From_Time    = p:Time_From
             LOC:To_Time      = p:Time_To
          ELSE
             LOC:From_Time    = p:Time_To
             LOC:To_Time      = p:Time_From
          .
       ELSE
          LOC:From_Date    = p:Date_To
          LOC:From_Time    = p:Time_To
          LOC:To_Date      = p:Date_From
          LOC:To_Time      = p:Time_From
    .  .

    EXIT
