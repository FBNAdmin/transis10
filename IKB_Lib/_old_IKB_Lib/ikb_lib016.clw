

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Get_New_Control_Size PROCEDURE (LOC_Myimage, p:F_Width, p:F_Height, p:Type) ! Declare Procedure
LOC:Height_R         DECIMAL(9,4)                          !Ratio
LOC:Width_R          DECIMAL(9,4)                          !
LOC:I_Width          LONG                                  !
LOC:I_Height         LONG                                  !
LOC:Fit_Height       LONG                                  !
LOC:Fit_Width        LONG                                  !
LOC:Orig_Width       LONG                                  !
LOC:Orig_Height      LONG                                  !
LOC:Width_First      BYTE                                  !
  CODE                                                     ! Begin processed code
    ! Parameters
    ! (LOC_Myimage, p:F_Width, p:F_Height, p:Type)
    ! p:Type:
    !           0 - Keep aspect ratio and fit to image size or passed size
    !               If only width or height passed then fit to passed one and make other aspect ratio equivalent
    !           1 - Fit to width (passed or image), - scrolling cannot be implemented at this time - scrolling height if aspect bigger than passed or image
    !           2 - Fit to height (passed or image), - scrolling cannot be implemented at this time - scrolling width if aspect bigger than passed or image

    IF LOC_Myimage{PROP:Text} = ''                          ! If no image then return
       RETURN
    .

    LOC:Orig_Width              = LOC_Myimage{PROP:Width}
    LOC:Orig_Height             = LOC_Myimage{PROP:Height}

    LOC_Myimage{PROP:NoWidth}   = TRUE
    LOC_Myimage{PROP:NoHeight}  = TRUE

    LOC:I_Width                 = LOC_Myimage{PROP:Width}
    LOC:I_Height                = LOC_Myimage{PROP:Height}


    IF OMITTED(2) AND OMITTED(3)
       LOC:Fit_Width    = LOC:Orig_Width
       LOC:Fit_Height   = LOC:Orig_Height
    ELSIF OMITTED(2)
       LOC:Fit_Height   = p:F_Height
    ELSIF OMITTED(3)
       LOC:Fit_Width    = p:F_Width
    ELSE
       LOC:Fit_Width    = p:F_Width
       LOC:Fit_Height   = p:F_Height
    .


    LOC:Width_R                 = LOC:I_Width  / LOC:Fit_Width          ! 1000 / 100 = 10
    LOC:Height_R                = LOC:I_Height / LOC:Fit_Height         ! 2000 / 100 = 20


    CASE p:Type
    OF 0
       IF OMITTED(2) XOR OMITTED(3)
          IF OMITTED(2)                                     ! Width omitted         but height provided
             LOC:Width_First = FALSE
          ELSIF OMITTED(3)                                  ! Height omitted        but width provided
             LOC:Width_First = TRUE
          .
       ELSE
          IF LOC:Width_R > LOC:Height_R
             LOC:Width_First = TRUE
       .  .
    OF 1
       LOC:Width_First              = TRUE
       !LOC_Myimage{PROP:VSCROLL}    = TRUE
    OF 2
       LOC:Width_First              = FALSE
       !LOC_Myimage{PROP:HSCROLL}    = TRUE
    .


    IF LOC:Width_First = TRUE
       LOC:I_Width      = LOC:I_Width  / LOC:Width_R        ! 1000 / 10 = 100 which is the width of the image
       LOC:I_Height     = LOC:I_Height / LOC:Width_R        ! 2000 / 10 = 200 which is the height of the image
    ELSE
       LOC:I_Width      = LOC:I_Width  / LOC:Height_R
       LOC:I_Height     = LOC:I_Height / LOC:Height_R
    .

    LOC_Myimage{PROP:Width}         = LOC:I_Width
    LOC_Myimage{PROP:Height}        = LOC:I_Height

    RETURN

