

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Get_Last_Element_From_Delim_Str FUNCTION (p:String, p:Delim, p:Chop, p:Case_Sensitive) ! Declare Procedure
LOC:Last_Found_After ULONG                                 !
LOC:Len              ULONG                                 !
LOC:Delim_Pos        ULONG                                 !
LOC:Delim_Len        ULONG                                 !
LOC_Ret_Str             &STRING
LOC_Use_Str             &STRING

LOC_Use_Str_No_Case     &STRING

LOC_Delim               &STRING
Str_Cls     CLASS
Str_1           &STRING
Init            PROCEDURE(*STRING)
Destruct        PROCEDURE
            .


LOC_My_Str_Ele      Str_Cls
  CODE                                                     ! Begin processed code
    ! (p:String, p:Delim, p:Chop, p:Case_Sensitive)
    !  String, Delim, Chop

    LOC:Len                 = LEN(CLIP(p:String))
    LOC_Ret_Str            &= NEW(String(LOC:Len))
    LOC_Use_Str            &= NEW(String(LOC:Len))

    LOC_Use_Str_No_Case    &= NEW(String(LOC:Len))

    IF LEN(p:Delim) = 0
       LOC_Delim           &= NEW(String(1))
    ELSE
       LOC_Delim           &= NEW(String(LEN(p:Delim)))
    .

    LOC_Ret_Str     = ''
    LOC_Use_Str     = p:String

    IF p:Case_Sensitive = FALSE
       LOC_Use_Str_No_Case  = UPPER(p:String)
    ELSE
       LOC_Use_Str_No_Case  = p:String
    .

    ! We are going to assume that if the p:Delim is empty the user wants to split on space!
    IF CLIP(p:Delim) = ''
       LOC_Delim            = ' '
       LOC:Delim_Len        = 1
    ELSE
       IF p:Case_Sensitive = FALSE
          LOC_Delim         = UPPER( p:Delim )
       ELSE
          LOC_Delim         = p:Delim
       .
       LOC:Delim_Len        = LEN(CLIP(LOC_Delim))
    .


    LOC:Delim_Pos   = LOC:Len + 1                               ! +1 because we will be doing -1 1st thing
    LOOP
       LOC:Delim_Pos    -= 1

       IF LOC:Delim_Pos = 0
          BREAK
       .
       IF LOC_Use_Str_No_Case[(LOC:Delim_Pos - LOC:Delim_Len + 1) : LOC:Delim_Pos] = LOC_Delim[1 : LOC:Delim_Len]
          ! Found a delimiter - want to exit
          BREAK
    .  .

    IF LOC:Delim_Pos = 0                                        ! Delim not found in string - return string
       LOC_Ret_Str  = LOC_Use_Str
    ELSE
       IF (LOC:Delim_Pos + 1) <= LOC:Len
          LOC_Ret_Str  = LOC_Use_Str[(LOC:Delim_Pos + 1) : LOC:Len]
       ELSE
          LOC_Ret_Str  = ''
    .  .


    IF p:Chop = TRUE
       IF LOC:Delim_Pos = 0                                     ! Delim not found in string - return string
          p:String      = ''
       ELSE
          IF (LOC:Delim_Pos - LOC:Delim_Len) >= 1
             p:String   = LOC_Use_Str[1 : (LOC:Delim_Pos - LOC:Delim_Len)]
          ELSE
             p:String   = ''
    .  .  .

    DISPOSE(LOC_Use_Str)
    DISPOSE(LOC_Use_Str_No_Case)
    DISPOSE(LOC_Delim)

    LOC_My_Str_Ele.Init(LOC_Ret_Str)

    DISPOSE(LOC_Ret_Str)

    RETURN( LOC_My_Str_Ele.Str_1 )          ! Clipping not nessisary - string is exactly right size








!    IF CLIP(LOC_Ret_Str) ~= ''
!       ! This should be the last element in the string...
!       IF p:Chop = TRUE
!          LOC:Last_Found_After  = LEN(CLIP(p:String)) - LEN(CLIP(LOC_Ret_Str))
!          IF LOC:Last_Found_After ~= 0                                          ! Same length = no delim found
!             LOC_Use_Str           = SUB(p:String, 1, ( LOC:Last_Found_After - LEN(CLIP(p:Delim)) ))
!             !                         INSTRING(LOC:Element, p:String, 1, LOC:Last_Found_After - 1) - 1)
!             p:String              = LOC_Use_Str
!          ELSE
!             p:String              = ''
!    .  .  .
!
!
!
!
!    !LOC:String      = CLIP(LEFT(p:String))
!    LOOP UNTIL CLIP(LOC_Use_Str) = ''
!        !In#                     = LEN(CLIP(LOC:String))
!
!                                                          ! (p:Delim_List, p:Delim, p:Chop_Out)
!        LOC_Ret_Str         = Get_1st_Element_From_Delim_Str(LOC_Use_Str, p:Delim, TRUE)
!
!        ! eg: "  xxyy| x xyy  xx| y"
!        !   1st iteration - Ret: "  xxyy"       and    Use: " x xyy  xx| y"
!        !   2nd iteration - Ret: " x xyy  xx"   and    Use: " y"
!        !   3rd iteration - Ret: " y"           and    Use: ""
!
!            
!        !Out#                    = LEN(CLIP(LOC:String))
!        !LOC:Last_Found_After   += In# - Out#
!
!        !IF CLIP(LOC_Ret_Str) = CLIP(LOC_Use_Str)
!           ! This could happen if the delimiter is not found in the string...
!           ! No it couldn't because the Chop is on - LOC_Use_Str would be empty in that case
!           ! If we had xxxyyy|xxxyyy then the above condition would be true but it wouldn't be the last element
!
!           !MESSAGE('The string and element are the same??|| E - S: ' & CLIP(LOC:Element) & ' - ' |
!           !         & CLIP(LOC:String),'Error - bug type',ICON:Hand)
!        !   BREAK
!    .   !.
! ----------- Class Procs -----------------
Str_Cls.Init        PROCEDURE(*STRING Our_Str)
    CODE
    SELF.Str_1     &= NEW(STRING( LEN(CLIP(Our_Str)) ))
    SELF.Str_1      = Our_Str

    RETURN



Str_Cls.Destruct    PROCEDURE()
    CODE
    DISPOSE(SELF.Str_1)
    RETURN
