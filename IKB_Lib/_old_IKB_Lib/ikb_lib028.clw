

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Pad_Str              FUNCTION (p:Str, p:Padding, p:With)   ! Declare Procedure
LOC:Len              LONG                                  !
  CODE                                                     ! Begin processed code
    ! (p:Str, p:Padding, p:With)
    ! p:With is a CSTRING so could be a space

    ! Note: Even is both are white space the function will complete correctly

    LOC:Len     = LEN(CLIP(p:Str))
    IF LOC:Len >= p:Padding
       ! Then we have the right length
    ELSE
       ! 10 - 5 = 5

       LOOP (p:Padding - LOC:Len) TIMES
          p:Str = p:With & CLIP(p:Str)
    .  .

    RETURN(p:Str)

