

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Transpose_Str        FUNCTION (p:Str)                      ! Declare Procedure
LOC:Ret_Str          ANY                                   !
LOC:Use_Str          CSTRING(50000)                        !
LOC:Len_S            ULONG                                 !
  CODE                                                     ! Begin processed code
    ! (p:Str)
    LOC:Use_Str = p:Str

    LOC:Ret_Str = LOC:Use_Str                               ! Assign to set the size
    LOC:Ret_Str = ''

    LOC:Len_S   = LEN(CLIP(LOC:Use_Str))

    LOOP II_# = LOC:Len_S TO 1 BY -1
       LOC:Ret_Str = LOC:Ret_Str & LOC:Use_Str[II_#]
    .

    RETURN( CLIP(LOC:Ret_Str) )
