

                     MEMBER('ikb_lib.clw')                 ! This is a MEMBER module

Hex_To_Dec           FUNCTION (p:Hex)                      ! Declare Procedure
LOC:Decimal_Val      LONG                                  !
LOC:Negative         BYTE                                  !
  CODE                                                     ! Begin processed code
    ! (p:Hex)
    LOC:Decimal_Val = 0

    p:Hex   = LEFT(p:Hex)

    Len_#   = LEN(CLIP(p:Hex))

    LOC:Negative = FALSE
    IF p:Hex[1] = '-'
       LOC:Negative = TRUE

       p:Hex        = p:Hex[2 : Len_#]
       Len_#        = LEN(CLIP(p:Hex))
    .

    Exp_#   = 0
    LOOP I# = Len_# TO 1 By -1
       Exp_#  += 1

       CASE p:Hex[I#]
       OF '0' TO '9'
          LOC:Decimal_Val   += (p:Hex[I#] * (16^(Exp_# - 1)))
       OF 'A'
          LOC:Decimal_Val   += 10 * (16^(Exp_# - 1))
       OF 'B'
          LOC:Decimal_Val   += 11 * (16^(Exp_# - 1))
       OF 'C'
          LOC:Decimal_Val   += 12 * (16^(Exp_# - 1))
       OF 'D'
          LOC:Decimal_Val   += 13 * (16^(Exp_# - 1))
       OF 'E'
          LOC:Decimal_Val   += 14 * (16^(Exp_# - 1))
       OF 'F'
          LOC:Decimal_Val   += 15 * (16^(Exp_# - 1))
       ELSE
          ! This is not Hex then!
          LOC:Decimal_Val   = 0
          BREAK
    .  .

    IF LOC:Decimal_Val ~= 0
       IF LOC:Negative = TRUE
          LOC:Decimal_Val   = -LOC:Decimal_Val
    .  .

    RETURN(LOC:Decimal_Val)
