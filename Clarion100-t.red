-- Default Redirection for Clarion 10.0

[Copy]
-- Directories only used when copying dlls
*.dll = %BIN%;%BIN%\AddIns\BackendBindings\ClarionBinding\Common;%ROOT%\Accessory\bin;%libpath%\bin\%configuration%

[Debug]
*.obj = obj\debug
*.res = obj\debug
*.rsc = obj\debug
*.lib = .\Lib
*.dll = .\Bin
*.FileList.xml = obj\debug
*.map = map\debug

[Release]
-- Directories only used when building with Release configuration

*.obj = project\release\obj
*.res = project\release\obj
*.rsc = project\release\obj
*.lib = project\release\obj
*.FileList.xml = project\release\obj
*.map = project\release\map
*.inc = project\release\inc
*.clw = project\release\clw


[Common]
*.chm = %BIN%;%ROOT%\Accessory\bin
*.tp? = %ROOT%\template\win
*.trf = %ROOT%\template\win
*.txs = %ROOT%\template\win
*.stt = %ROOT%\template\win
*.*   = .; %ROOT%\libsrc\win; %ROOT%\images; %ROOT%\template\win
*.lib = %ROOT%\lib
*.obj = %ROOT%\lib
*.res = %ROOT%\lib
*.hlp = %BIN%;%ROOT%\Accessory\bin
*.dll = %BIN%;%ROOT%\Accessory\bin
*.exe = %BIN%;%ROOT%\Accessory\bin
*.tp? = %ROOT%\Accessory\template\win
*.txs = %ROOT%\Accessory\template\win
*.stt = %ROOT%\Accessory\template\win
*.lib = %ROOT%\Accessory\lib
*.obj = %ROOT%\Accessory\lib
*.res = %ROOT%\Accessory\lib
*.dll = %ROOT%\Accessory\bin
*.*   = %ROOT%\Accessory\images; %ROOT%\Accessory\resources; %ROOT%\Accessory\libsrc\win; %ROOT%\Accessory\template\win
*.hlp = %ROOT%\bin
*.dll = %ROOT%\Accessory\bin\SafeUpdate;%ROOT%\Accessory\NetTalk\Web Server\php (58)\php\ext;%ROOT%\Accessory\NetTalk\Web Server\php (58)\php
*.exe = %ROOT%\Accessory\bin\mBuild;%ROOT%\Accessory\bin\SafeUpdate;%ROOT%\Accessory\draw\3Dmap;%ROOT%\Accessory\draw\Barcode Report;%ROOT%\Accessory\draw\Demo;%ROOT%\Accessory\Ingasoftplus\EasyExcel4;%ROOT%\Accessory\makeover\ABC;%ROOT%\Accessory\NetTalk\FTP\ABC\Backup;%ROOT%\Accessory\NetTalk\FTP\ABC;%ROOT%\Accessory\NetTalk\LimitCopies;%ROOT%\Accessory\NetTalk\NetAuto\File Put And Get;%ROOT%\Accessory\NetTalk\NetDrive\ABC;%ROOT%\Accessory\NetTalk\NetLDAP\ABC;%ROOT%\Accessory\NetTalk\NetMaps\ABC;%ROOT%\Accessory\NetTalk\Web Server\FileDownload (40);%ROOT%\Accessory\NetTalk\Web Server\php (58)\php;%ROOT%\Accessory\NetTalk\Web Server\WebServiceRequiresXFiles (77);%ROOT%\Accessory\OddJob\Demo\utils;%ROOT%\Accessory\OddJob\HtmlEditor;%ROOT%\Accessory\OfficeInside4\Demo;%ROOT%\Accessory\OfficeInside4\Editable Reports (C6);%ROOT%\Accessory\OfficeInside4\Word Quick Letter;%ROOT%\Accessory\ResizeAndSplit\ABC;%ROOT%\Accessory\Secwin6\Demo_AC;%ROOT%\Accessory\Secwin6\UsingPin;%ROOT%\Accessory\SelfService\JumpStart1 AppIsManager;%ROOT%\Accessory\StringTheory\Demo;%ROOT%\Accessory\Uninstall;%ROOT%\Accessory\WinEvent5\ABC;%ROOT%\Accessory\WinEvent5\Demo;%ROOT%\Accessory\xFiles\Demo;%ROOT%\Accessory\xFiles\Demo Full;%ROOT%\Accessory\xFiles\xFilesFastTimer;%ROOT%\Accessory
