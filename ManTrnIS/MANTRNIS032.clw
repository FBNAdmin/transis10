

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS032.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_TruckTrailer PROCEDURE (p:Type, p:Type2)

CurrentTab           STRING(80)                            !
LOC:Type             STRING(10)                            !Type of vehicle - Horse, Trailer, Combined
LOC:Composition      STRING(50)                            !
LOC:Type_Limit       BYTE                                  !Type of vehicle - Horse, Trailer, Combined
LOC:Type_Limit_2     BYTE                                  !Type of vehicle - Horse, Trailer, Combined
LOC:Apply_Limit      BYTE                                  !
LOC:Open_TTID        ULONG                                 !Openned with this one in the record buffer
BRW1::View:Browse    VIEW(TruckTrailer)
                       PROJECT(TRU:Registration)
                       PROJECT(TRU:Capacity)
                       PROJECT(TRU:LicenseExpiryDate)
                       PROJECT(TRU:LicenseInfo)
                       PROJECT(TRU:TID)
                       PROJECT(TRU:Type)
                       PROJECT(TRU:TTID)
                       PROJECT(TRU:VMMID)
                       JOIN(VMM:PKey_VMMID,TRU:VMMID)
                         PROJECT(VMM:MakeModel)
                         PROJECT(VMM:VMMID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
TRU:Registration       LIKE(TRU:Registration)         !List box control field - type derived from field
LOC:Type               LIKE(LOC:Type)                 !List box control field - type derived from local data
VMM:MakeModel          LIKE(VMM:MakeModel)            !List box control field - type derived from field
TRU:Capacity           LIKE(TRU:Capacity)             !List box control field - type derived from field
LOC:Composition        LIKE(LOC:Composition)          !List box control field - type derived from local data
TRU:LicenseExpiryDate  LIKE(TRU:LicenseExpiryDate)    !List box control field - type derived from field
TRU:LicenseInfo        LIKE(TRU:LicenseInfo)          !List box control field - type derived from field
TRU:TID                LIKE(TRU:TID)                  !List box control field - type derived from field
TRU:Type               LIKE(TRU:Type)                 !List box control field - type derived from field
TRU:TTID               LIKE(TRU:TTID)                 !List box control field - type derived from field
VMM:VMMID              LIKE(VMM:VMMID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Truck or Trailer File'),AT(,,419,285),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MAX,MDI,HLP('BrowseTruckTrailer'),SYSTEM
                       LIST,AT(8,30,401,210),USE(?Browse:1),HVSCROLL,FORMAT('50L(2)|M~Registration~@s20@35L(2)' & |
  '|M~Type~L(1)@s10@70L(2)|M~Make & Model~@s35@34R(2)|M~Capacity~L(1)@n-8.0@100L(2)|M~C' & |
  'omposition~L(1)@s50@50R(2)|M~License Expiry Date~L(1)@d5b@100L(2)|M~License Info~L(1' & |
  ')@s250@40R(2)|M~TID~C(0)@n_10@24R(2)|M~Type~C(0)@n3@40R(2)|M~TTID~C(0)@n_10@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the TruckTrailer file')
                       BUTTON('&Select'),AT(84,245,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,245,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(254,245,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(307,245,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(361,245,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,411,260),USE(?CurrentTab)
                         TAB('&1) By Tansporter, Make && Registration'),USE(?Tab:2)
                           GROUP,AT(4,270,232,10),USE(?Group1)
                             CHECK(' Apply Type Limit'),AT(4,270),USE(LOC:Apply_Limit)
                             PROMPT('Type:'),AT(77,270),USE(?LOC:Type_Limit:Prompt)
                             LIST,AT(100,270,50,10),USE(LOC:Type_Limit),DROP(5),FROM('Horse|#0|Trailer|#1|Combined|#2'), |
  MSG('Type of vehicle'),TIP('Type of vehicle')
                             PROMPT('Type 2:'),AT(157,270),USE(?LOC:Type_Limit_2:Prompt)
                             LIST,AT(185,270,50,10),USE(LOC:Type_Limit_2),DROP(5),FROM('Horse|#0|Trailer|#1|Combined|#2'), |
  MSG('Type of vehicle'),TIP('Type of vehicle')
                           END
                           BUTTON('Transporter'),AT(9,245,,14),USE(?SelectTransporter),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&2) By Registration'),USE(?Tab:3)
                         END
                         TAB('&3) By License Expiry'),USE(?Tab3)
                         END
                       END
                       BUTTON('&Close'),AT(365,268,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('View Composition'),AT(283,268),USE(?BUTTON_View_Comp)
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW1::Sort1:StepClass StepStringClass                      ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Cls                  CLASS
CompositionCheck        PROCEDURE(ULONG p_TTID, ULONG p_TID)
CompositionReturn       PROCEDURE(ULONG p_TTID, ULONG p_TID),STRING
                     .
_rVC_View                         VIEW(VehicleComposition)
                                    PROJECT(VCO:CompositionName, VCO:Capacity)
                                 .
_rVC                           ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Omitted        ROUTINE
!       LOC:Type     = 'Horse'       0 
!       LOC:Type     = 'Trailer'     1
!       LOC:Type     = 'Combined'    2
    IF ~OMITTED(1)
       LOC:Apply_Limit  = TRUE
       LOC:Type_Limit       = p:Type

       IF OMITTED(2)
          LOC:Type_Limit_2  = p:Type
       ELSE
          LOC:Type_Limit_2  = p:Type2
    .  .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_TruckTrailer')
  LOC:Open_TTID  = TRU:TTID
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Apply_Limit',LOC:Apply_Limit)         ! Added by: BrowseBox(ABC)
  BIND('LOC:Type_Limit',LOC:Type_Limit)           ! Added by: BrowseBox(ABC)
  BIND('LOC:Type_Limit_2',LOC:Type_Limit_2)       ! Added by: BrowseBox(ABC)
  BIND('LOC:Type',LOC:Type)                       ! Added by: BrowseBox(ABC)
  BIND('LOC:Composition',LOC:Composition)         ! Added by: BrowseBox(ABC)
     BIND('VCO:TTID0', VCO:TTID0)
     BIND('VCO:TTID1', VCO:TTID1)
     BIND('VCO:TTID2', VCO:TTID2)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                         ! File Transporter used by this procedure, so make sure it's RelationManager is open
  Access:VehicleComposition.UseFile               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TruckTrailer,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon TRU:Registration for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,TRU:Key_Registration) ! Add the sort order for TRU:Key_Registration for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)            ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,TRU:Registration,1,BRW1) ! Initialize the browse locator using  using key: TRU:Key_Registration , TRU:Registration
  BRW1.AddSortOrder(,)                            ! Add the sort order for  for sort order 2
  BRW1.AppendOrder('+TRU:LicenseExpiryDate,+TRU:Registration,+TRU:TTID') ! Append an additional sort order
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon TRU:TID for sort order 3
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,TRU:FKey_TID) ! Add the sort order for TRU:FKey_TID for sort order 3
  BRW1.AddRange(TRU:TID,Relate:TruckTrailer,Relate:Transporter) ! Add file relationship range limit for sort order 3
  BRW1.AppendOrder('+VMM:MakeModel,+TRU:Registration') ! Append an additional sort order
  BRW1.SetFilter('(LOC:Apply_Limit = 0 OR (LOC:Type_Limit = TRU:Type OR LOC:Type_Limit_2 = TRU:Type))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Apply_Limit)             ! Apply the reset field
  BRW1.AddResetField(LOC:Type_Limit)              ! Apply the reset field
  BRW1.AddResetField(LOC:Type_Limit_2)            ! Apply the reset field
  BRW1.AddField(TRU:Registration,BRW1.Q.TRU:Registration) ! Field TRU:Registration is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Type,BRW1.Q.LOC:Type)         ! Field LOC:Type is a hot field or requires assignment from browse
  BRW1.AddField(VMM:MakeModel,BRW1.Q.VMM:MakeModel) ! Field VMM:MakeModel is a hot field or requires assignment from browse
  BRW1.AddField(TRU:Capacity,BRW1.Q.TRU:Capacity) ! Field TRU:Capacity is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Composition,BRW1.Q.LOC:Composition) ! Field LOC:Composition is a hot field or requires assignment from browse
  BRW1.AddField(TRU:LicenseExpiryDate,BRW1.Q.TRU:LicenseExpiryDate) ! Field TRU:LicenseExpiryDate is a hot field or requires assignment from browse
  BRW1.AddField(TRU:LicenseInfo,BRW1.Q.TRU:LicenseInfo) ! Field TRU:LicenseInfo is a hot field or requires assignment from browse
  BRW1.AddField(TRU:TID,BRW1.Q.TRU:TID)           ! Field TRU:TID is a hot field or requires assignment from browse
  BRW1.AddField(TRU:Type,BRW1.Q.TRU:Type)         ! Field TRU:Type is a hot field or requires assignment from browse
  BRW1.AddField(TRU:TTID,BRW1.Q.TRU:TTID)         ! Field TRU:TTID is a hot field or requires assignment from browse
  BRW1.AddField(VMM:VMMID,BRW1.Q.VMM:VMMID)       ! Field VMM:VMMID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_TruckTrailer',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
      DO Check_Omitted
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_TruckTrailer',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,10,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
     _rVC.Init(_rVC_View, Relate:VehicleComposition)
     _rVC.AddSortOrder(VCO:FKey_TID)
     _rVC.AppendOrder('-VCO:VCID')
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
     _rVC.Kill()
     UNBIND('VCO:TTID0')
     UNBIND('VCO:TTID1')
     UNBIND('VCO:TTID2')
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Transporter.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_TruckTrailer',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_TruckTrailer
    ReturnValue = GlobalResponse
  END
     IF CHOICE(?CurrentTab) = 1
        IF ReturnValue = RequestCompleted
           ! Get the transporter for this added entry
           TRA:TID  = TRU:TID
           IF Access:Transporter.TryFetch(TRA:PKey_TID) ~= LEVEL:Benign
  
     .  .  .
  
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectTransporter
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Transporter()
      ThisWindow.Reset
    OF ?BUTTON_View_Comp
      ThisWindow.Update()
      GET(Queue:Browse:1, CHOICE(?Browse:1))
      IF ERRORCODE()
         MESSAGE('Error getting entry.  Please select an item from the list.')
      ELSE
         Cls.CompositionCheck(Queue:Browse:1.TRU:TTID, Queue:Browse:1.TRU:TID)   
      .
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      IF LOC:Open_TTID > 0
         !Select it if in list
         i# = 0
         LOOP
            i# += 1
            GET(Queue:Browse:1,i#)
            IF ERRORCODE()
               BREAK
            .
            IF Queue:Browse:1.TRU:TTID = LOC:Open_TTID
               SELECT(?Browse:1, i#)
               BREAK
            .
         .
      .
      
            
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Cls.CompositionCheck       PROCEDURE(ULONG p_TTID, ULONG p_TID)
l_List                        STRING(1000)
l_add                         STRING(100)

_q                            QUEUE
VCID                             ULONG
                              .

_VC_View                      VIEW(VehicleComposition)
                              .
_VC                           ViewManager

CODE
   ! Search for the Truck on a combination.  Should only be on 1 really?
   _VC.Init(_VC_View, Relate:VehicleComposition)
   _VC.AddSortOrder(VCO:FKey_TID)
   _VC.AppendOrder('-VCO:VCID')
   _VC.AddRange(VCO:TID, p_TID)
   _VC.SetFilter('VCO:TTID0 = ' & p_TTID & ' OR VCO:TTID1 = ' & p_TTID & ' OR VCO:TTID2 = ' & p_TTID)      
   _VC.Reset()
   LOOP
      IF _VC.Next() <> Level:Benign
         BREAK
      .
      
      l_add          = VCO:CompositionName      
      IF VCO:Archived = TRUE
         l_add       = CLIP(l_add) & '  (Archived)'
      .
      Add_to_List(CLIP(l_add), l_List, '|')
      
      _q.VCID  = VCO:VCID
      ADD(_q)   
   .   

   IF CLIP(l_List) = ''
      MESSAGE('No Composition available for TTID: ' & p_TTID)
   ELSE
      IF RECORDS(_q) = 1
         GET(_q, 1)
      ELSE
         GET(_q, POPUP(l_List))                    ! POPUP list
      .
      
      IF ~ERRORCODE()
         VCO:VCID   = _q.VCID
         IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = Level:Benign
            GlobalRequest  = ViewRecord
            Update_VehicleComposition
         .
      .
   .
   _VC.Kill()
   RETURN
        
        
Cls.CompositionReturn       PROCEDURE(ULONG p_TTID, ULONG p_TID)     !,STRING
L_VCName                      STRING(50)

CODE
   ! Search for the Truck on a combination.  Should only be on 1 really?
   _rVC.AddRange(VCO:TID, p_TID)
   _rVC.SetFilter('VCO:TTID0 = ' & p_TTID & ' OR VCO:TTID1 = ' & p_TTID & ' OR VCO:TTID2 = ' & p_TTID, '1')
   _rVC.SetFilter('VCO:Archived = 0', '2')          ! Dont show archived compositions here
   _rVC.Reset()
   IF _rVC.Next() = Level:Benign      ! We have highest VCID
      L_VCName = VCO:CompositionName & ' (' & LEFT(FORMAT(VCO:Capacity,@n10)) & ' kgs)'
   .   
   RETURN(L_VCName)
        
        

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      EXECUTE TRU:Type
         LOC:Type     = 'Trailer'
         LOC:Type     = 'Combined'
      ELSE
         LOC:Type     = 'Horse'
      .
  
  
      ! Search for the Truck on a combination.  Should only be on 1 really?
     LOC:Composition   = Cls.CompositionReturn(TRU:TTID, TRU:TID)
  !    CLEAR(VCO:Record, -1)
  !    VCO:TID          = TRU:TID
  !    SET(VCO:FKey_TID, VCO:FKey_TID)
  !    LOOP
  !       IF Access:VehicleComposition.TryNext() ~= LEVEL:Benign
  !          BREAK
  !       .
  !       IF VCO:TID ~= TRU:TID
  !          BREAK
  !       .
  !
  !       IF VCO:TTID0 = TRU:TTID OR VCO:TTID1 = TRU:TTID OR VCO:TTID2 = TRU:TTID OR VCO:TTID3 = TRU:TTID
  !          LOC:Composition     = VCO:CompositionName
  !          BREAK
  !    .  .
  PARENT.SetQueueRecord
  


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>2,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>2,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group1, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group1

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Delivery_PODs        PROCEDURE  (ULONG p:DID)              ! Declare Procedure
LOC:DIID             ULONG                                 !Delivery Item ID
LOC:TRID             ULONG                                 !Tripsheet ID
LOC:FID              ULONG                                 !Floor ID
LOC:Del_Group        GROUP,PRE(L_DG)                       !
Date                 LONG                                  !
Time                 LONG                                  !
                     END                                   !
View_TripDels   VIEW(TripSheetDeliveriesAlias)
                     PROJECT(A_TRDI:TDID, A_TRDI:DIID, A_TRDI:TRID)
                     JOIN(A_DELI:PKey_DIID, A_TRDI:DIID)
                        JOIN(A_DEL:PKey_DID, A_DELI:DID)
                           PROJECT(A_DEL:DID)  
                        .
                     .
                  .

TripDelsView    ViewManager

  CODE
   ! Loop through the Deliveries
   PUSHBIND()
   BIND('A_DEL:DID',A_DEL:DID)

   TripDelsView.Init(View_TripDels, Relate:TripSheetDeliveriesAlias)
   TripDelsView.AddSortOrder(A_TRDI:FKey_TRID)
   TripDelsView.AppendOrder('A_TRDI:TDID')            !A_TRDI:DIID')
   !TripDelsView.AddRange(A_TRDI:TRID, TRI:TRID)

   TripDelsView.SetFilter('A_DEL:DID = ' & p:DID)

   TripDelsView.Reset()
   LOOP              !UNTIL L_Cancel_Item = TRUE
      IF TripDelsView.Next() ~= LEVEL:Benign
         BREAK
      .
      
      LOC:TRID      = A_TRDI:TRID

      LOC:DIID      = A_TRDI:DIID

      TRDI:TDID     = A_TRDI:TDID
      IF Access:TripSheetDeliveries.TryFetch(TRDI:PKey_TDID) ~= LEVEL:Benign
         MESSAGE('Could not fetch the Trip Sheet Delivery: ' & A_TRDI:TDID, 'Trip Sheet POD', ICON:Hand)
         CYCLE
      .

      GlobalRequest     = ChangeRecord
      Update_TripSheetDeliveries(LOC:Del_Group)
      IF GlobalResponse = RequestCancelled
         ! Do what?
         !CASE MESSAGE('Would you like to stop updating the Trip Sheet Deliveries?', 'Trip Sheet POD', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         !OF BUTTON:Yes
            BREAK
         !.
      ELSE
         ! TODO Advance state of TripSheet when all have PODs
         
         ! Change the Floor that the entire DI is on....
         ! Or should this be done only when all have been moved?  In theory
         ! they could go to different floors too...
         !DEL:DID        = A_DEL:DID
         !IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
         !   DEL:FID     = LOC:FID                  !TODO - set the floor to delivered???????????????????
         !   IF Access:Deliveries.TryUpdate() = LEVEL:Benign
   .  .  !.  . 
      
   Manifest_Emails_Setup(p:DID, 16)
               
   UNBIND('A_DEL:DID')
   POPBIND()

      
      
   ! (p:TRID, p:DIIDs)
   ! (ULONG, <*STRING>),ULONG
   IF Get_TripDelItems_Incomplete(LOC:TRID) <= 0      ! none incomplete, update to finalised then
      A_TRI:TRID = LOC:TRID

      IF Access:TripSheetsAlias.TryFetch(A_TRI:PKey_TID) = LEVEL:Benign
         A_TRI:State   = 4
         IF Access:TripSheetsAlias.Update() = LEVEL:Benign
         .         
      .
   .
