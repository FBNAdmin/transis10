

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('MANTRNIS012.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Invoices_List PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
BID                  ULONG                                 !Branch ID
BranchName           STRING('All {32}')                    !Branch Name
                     END                                   !
LOC:Options_2        GROUP,PRE(L_O2)                       !
Invoice_Credit_Option BYTE                                 !Show both, Invoices, Credit Notes
Clients_Option       BYTE                                  !0 is all, 1 is 1, 2 is tagged
                     END                                   !
Process:View         VIEW(_Invoice)
                       PROJECT(INV:BID)
                       PROJECT(INV:ClientNo)
                       PROJECT(INV:Documentation)
                       PROJECT(INV:FuelSurcharge)
                       PROJECT(INV:IID)
                       PROJECT(INV:InvoiceDate)
                       PROJECT(INV:TollCharge)
                       PROJECT(INV:Total)
                       PROJECT(INV:VAT)
                       PROJECT(INV:CID)
                       JOIN(CLI:PKey_CID,INV:CID)
                       END
                     END
ProgressWindow       WINDOW('Report - Invoice Listing'),AT(,,208,148),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,MDI,TIMER(1)
                       SHEET,AT(3,4,201,123),USE(?Sheet1)
                         TAB('Options'),USE(?Tab2)
                           PROMPT('From Date:'),AT(7,28),USE(?LO:From_Date:Prompt),TRN
                           SPIN(@d5b),AT(74,28,60,10),USE(LO:From_Date),RIGHT(1)
                           BUTTON('...'),AT(59,28,12,10),USE(?Calendar)
                           PROMPT('To Date:'),AT(7,44),USE(?LO:To_Date:Prompt),TRN
                           SPIN(@d5b),AT(74,44,60,10),USE(LO:To_Date),RIGHT(1)
                           BUTTON('...'),AT(59,44,12,10),USE(?Calendar:2)
                           PROMPT('Branch Name:'),AT(7,74),USE(?LO:To_Date:Prompt:2),TRN
                           BUTTON('...'),AT(59,74,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(74,74,120,10),USE(LO:BranchName),MSG('Branch Name'),TIP('Branch Name - l' & |
  'eave blank for All')
                         END
                         TAB('Progress'),USE(?Tab1)
                           STRING(''),AT(27,50,141,10),USE(?Progress:UserString),CENTER,TRN
                           PROGRESS,AT(42,62,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(27,78,141,10),USE(?Progress:PctText),CENTER,TRN
                         END
                       END
                       BUTTON('Pause'),AT(98,130,,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                       BUTTON('Cancel'),AT(156,130,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('_Invoice Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?ReportTitle:2),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING(@s35),AT(4448,167,1302,167),USE(LO:BranchName),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Invoice List'),AT(156,31,3646,313),USE(?ReportTitle),FONT(,14,,FONT:bold),CENTER,TRN
                         STRING('Branch:'),AT(3958,167,385,167),USE(?String25),TRN
                         BOX,AT(0,344,7750,250),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@d5b),AT(6104,167),USE(LO:From_Date),FONT(,,,FONT:bold,CHARSET:ANSI),RIGHT(1)
                         STRING('-    '),AT(6875,167,208,156),USE(?String24),TRN
                         STRING(@d5b),AT(7021,167),USE(LO:To_Date),FONT(,,,FONT:bold,CHARSET:ANSI),RIGHT(1)
                         LINE,AT(687,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1500,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(2385,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(5792,354,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(3417,365,0,250),USE(?DetailLine:9),COLOR(COLOR:Black)
                         LINE,AT(6802,365,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                         STRING('Documentation'),AT(2458,396,868,170),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('Invoice No.'),AT(42,396,573,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Invoice Date'),AT(760,396,729,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Total'),AT(5865,396,868,170),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Client No.'),AT(1625,396,740,170),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('VAT Amount'),AT(6875,396,823,167),USE(?String13),CENTER,TRN
                         STRING('Fuel Surcharge'),AT(4865,396,868,170),USE(?HeaderTitle:4),CENTER,TRN
                         LINE,AT(4792,354,0,250),USE(?DetailLine:10),COLOR(COLOR:Black)
                         STRING('Toll Charge'),AT(3687,396,865,167),USE(?HeaderTitle:5),CENTER,TRN
                       END
Detail                 DETAIL,AT(0,0,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(687,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(1500,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(2385,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(3417,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(5792,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(6802,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                         STRING(@n_10),AT(42,31,573,167),USE(INV:IID),RIGHT
                         STRING(@d6),AT(760,31,,170),USE(INV:InvoiceDate),RIGHT(1)
                         STRING(@n_10b),AT(1573,31,740,167),USE(INV:ClientNo),RIGHT(1)
                         STRING(@n-14.2),AT(2458,31),USE(INV:Documentation),RIGHT(1)
                         STRING(@n-14.2),AT(4865,31),USE(INV:FuelSurcharge),RIGHT(1)
                         STRING(@n-15.2),AT(5865,31,868,170),USE(INV:Total),RIGHT
                         STRING(@n-14.2),AT(6875,31,823),USE(INV:VAT),RIGHT(1)
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         LINE,AT(4792,-20,0,250),USE(?DetailLine:11),COLOR(COLOR:Black)
                         STRING(@n-14.2),AT(3667,31),USE(INV:TollCharge),RIGHT(1)
                       END
grandtotals            DETAIL,AT(0,0,,490),USE(?grandtotals)
                         LINE,AT(104,52,7552,0),USE(?Line12),COLOR(COLOR:Black)
                         STRING(@n-14.2),AT(4865,94),USE(INV:FuelSurcharge,,?INV:FuelSurcharge:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(6813,94),USE(INV:VAT,,?INV:VAT:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(2458,94),USE(INV:Documentation,,?INV:Documentation:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING('Totals'),AT(208,94),USE(?String22),TRN
                         LINE,AT(104,281,7552,0),USE(?Line12:2),COLOR(COLOR:Black)
                         STRING(@n-15.2),AT(5865,94,868,170),USE(INV:Total,,?INV:Total:2),RIGHT,SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(3667,94,885,167),USE(INV:TollCharge,,?INV:TollCharge:2),RIGHT(1),SUM,TALLY(Detail)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar6            CalendarClass
Calendar7            CalendarClass
LOC_Client_Tags     shpTagClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Invoice No.' & |
      '|' & 'By Invoice Date' & |
      '|' & 'By Client & Invoice No.' & |
      '|' & 'By DI No.' & |
      '|' & 'By Invoice Composition' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:grandtotals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Invoices_List')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LO:From_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  BIND('LO:BID',LO:BID)                                    ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
      CASE MESSAGE('Would you like to select Clients to report on?', 'Select Clients', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
      OF BUTTON:No
         L_O2:Clients_Option      = 0
      OF BUTTON:Yes
         L_O2:Clients_Option      = 1
  
         Globalrequest            = SelectRecord
         Browse_Clients(LOC_Client_Tags)
  
         IF LOC_Client_Tags.NumberTagged() <= 0
            LOC_Client_Tags.MakeTag(CLI:CID)
      .  .
  
  
      IF LOC_Client_Tags.NumberTagged() > 0 OR L_O2:Clients_Option = 0
         L_O2:Invoice_Credit_Option   = MESSAGE('Please select what to include in the list.', 'Invocie / Credit Note Listing', ICON:Question, 'All|Invoices|Credits', 1)
      ELSE
         ReturnValue                  = LEVEL:Fatal
      .
  
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Invoices_List',ProgressWindow)       ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:_Invoice, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice No.')) THEN
     ThisReport.AppendOrder('+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice Date')) THEN
     ThisReport.AppendOrder('+INV:InvoiceDate,+INV:ClientName,+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client & Invoice No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientName,+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By DI No.')) THEN
     ThisReport.AppendOrder('+INV:DINo,+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice Composition')) THEN
     ThisReport.AppendOrder('+INV:ICID,+INV:IID')
  END
  ThisReport.SetFilter('(0 = LO:From_Date OR INV:InvoiceDate >= LO:From_Date) AND (0 = LO:To_Date OR INV:InvoiceDate << LO:To_Date + 1) AND (0 = LO:BID OR LO:BID = INV:BID)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_Invoice.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Invoices_List',ProgressWindow)    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
       SETTARGET(Report)        !,?ReportTitle:2)
  
       ! All|Invoices|Credits
       IF L_O2:Invoice_Credit_Option = 1
          ?ReportTitle{PROP:Text}   = 'Invoices & Credit Notes'
       ELSIF L_O2:Invoice_Credit_Option = 3
          ?ReportTitle{PROP:Text}   = 'Credit Notes'
       .
  
       SETTARGET()
  
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Branches
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
          IF LO:From_Date = 0 AND LO:To_Date = 0
             MESSAGE('A date or date range is required.','Date Validation',ICON:Exclamation)
             SELECT(?LO:From_Date)
             CYCLE
          ELSIF LO:From_Date = 0
             LO:From_Date     = LO:To_Date
          ELSIF LO:To_Date = 0
             LO:To_Date       = LO:From_Date
          .
      
      
      !    IF ReturnValue = LEVEL:Fatal
      !       RETURN( ReturnValue )
      !    .
      
      
          SELECT(?Tab1)
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',LO:From_Date)
      IF Calendar6.Response = RequestCompleted THEN
      LO:From_Date=Calendar6.SelectedDate
      DISPLAY(?LO:From_Date)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar7.SelectOnClose = True
      Calendar7.Ask('Select a Date',LO:To_Date)
      IF Calendar7.Response = RequestCompleted THEN
      LO:To_Date=Calendar7.SelectedDate
      DISPLAY(?LO:To_Date)
      END
      ThisWindow.Reset(True)
    OF ?CallLookup
      ThisWindow.Update()
      BRA:BranchName = LO:BranchName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LO:BranchName = BRA:BranchName
        LO:BID = BRA:BID
      END
      ThisWindow.Reset(1)
    OF ?LO:BranchName
      IF NOT ProgressWindow{PROP:AcceptAll}
        IF LO:BranchName OR ?LO:BranchName{PROP:Req}
          BRA:BranchName = LO:BranchName
          IF Access:Branches.TryFetch(BRA:Key_BranchName)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              LO:BranchName = BRA:BranchName
              LO:BID = BRA:BID
            ELSE
              CLEAR(LO:BID)
              SELECT(?LO:BranchName)
              CYCLE
            END
          ELSE
            LO:BID = BRA:BID
          END
        END
      END
      ThisWindow.Reset()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:grandtotals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF L_O2:Invoice_Credit_Option = 1
      ELSE
         IF L_O2:Invoice_Credit_Option = 2
            IF INV:Total < 0.0
               ReturnValue  = Record:Filtered
            .
         ELSE
            IF INV:Total >= 0.0
               ReturnValue  = Record:Filtered
      .  .  .
  
  
      IF L_O2:Clients_Option = 0          ! All
      ELSE
         IF LOC_Client_Tags.IsTagged(INV:CID) ~= TRUE
            ReturnValue  = Record:Filtered
      .  .
  
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','MANTRNIS','Print_Invoices_List','Print_Invoices_List','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Invoice_Summary PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
LOC:Options_2        GROUP,PRE(L_O2)                       !
Invoice_Credit_Option BYTE                                 !Show both, Invoices, Credit Notes
Clients_Option       BYTE                                  !0 is all, 1 is 1, 2 is tagged
                     END                                   !
Process:View         VIEW(_Invoice)
                       PROJECT(INV:BranchName)
                       PROJECT(INV:ClientName)
                       PROJECT(INV:ClientNo)
                       PROJECT(INV:DINo)
                       PROJECT(INV:IID)
                       PROJECT(INV:InvoiceDate)
                       PROJECT(INV:Total)
                       PROJECT(INV:Weight)
                       PROJECT(INV:CID)
                       JOIN(CLI:PKey_CID,INV:CID)
                       END
                       JOIN(BRA:PKey_BID,INV:IID)
                       END
                     END
ProgressWindow       WINDOW('Report Invoice Summary'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('_Invoice Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Summary of Invoices'),AT(0,20,4885),USE(?ReportTitle),FONT(,14,,FONT:bold),CENTER
                         BOX,AT(10,354,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         STRING(@d5b),AT(5417,104),USE(LO:From_Date),RIGHT(1)
                         STRING('-    '),AT(6198,104,208,156),USE(?String24),TRN
                         STRING(@d5b),AT(6615,104),USE(LO:To_Date),RIGHT(1)
                         LINE,AT(688,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1500,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(2302,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(3781,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(4604,354,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(5438,354,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(6781,350,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         STRING('Invoice No.'),AT(42,385,,170),USE(?HeaderTitle:1),TRN
                         STRING('Branch Name'),AT(760,385,,170),USE(?HeaderTitle:2),TRN
                         STRING('Client No.'),AT(1552,385,688,167),USE(?HeaderTitle:3),TRN
                         STRING('Client Name'),AT(2365,385,868,170),USE(?HeaderTitle:4),TRN
                         STRING('DI No.'),AT(3844,385,688,167),USE(?HeaderTitle:5),TRN
                         STRING('Invoice Date'),AT(4677,385,729,167),USE(?HeaderTitle:6),TRN
                         STRING('Total'),AT(6833,385,868,170),USE(?HeaderTitle:7),TRN
                         STRING('Weight'),AT(5521,385,677,167),USE(?HeaderTitle:8),TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(688,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(1500,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(2302,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(3781,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(4604,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(5438,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(6781,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                         STRING(@n_10),AT(42,52,573,167),USE(INV:IID),RIGHT
                         STRING(@s35),AT(760,52,677,167),USE(INV:BranchName),LEFT
                         STRING(@n_10b),AT(1552,52,,170),USE(INV:ClientNo),RIGHT
                         STRING(@s100),AT(2365,52,1365,167),USE(INV:ClientName),LEFT
                         STRING(@n_10),AT(3844,52,,170),USE(INV:DINo),RIGHT
                         STRING(@d6),AT(4677,52,,170),USE(INV:InvoiceDate),RIGHT
                         STRING(@n-15.2),AT(6833,52,868,170),USE(INV:Total),RIGHT
                         STRING(@n-11.2),AT(5521,52,,170),USE(INV:Weight),RIGHT
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
grandtotals            DETAIL,AT(,,,396),USE(?grandtotals)
                         LINE,AT(115,31,7542,0),USE(?Line18),COLOR(COLOR:Black)
                         STRING('Totals'),AT(885,104),USE(?String28),TRN
                         STRING(@n-15.2),AT(5302,104,,170),USE(INV:Weight,,?INV:Weight:2),RIGHT,SUM,TALLY(Detail),TRN
                         STRING(@n-15.2),AT(6833,104,868,170),USE(INV:Total,,?INV:Total:2),RIGHT,SUM,TALLY(Detail), |
  TRN
                         LINE,AT(125,313,7542,0),USE(?Line18:2),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

LOC_Client_Tags     shpTagClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Invoice No.' & |
      '|' & 'By Invoice Date' & |
      '|' & 'By Client & Invoice No.' & |
      '|' & 'By DI No.' & |
      '|' & 'By Branch & Invoice No.' & |
      '|' & 'By Invoice Composition' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:grandtotals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Invoice_Summary')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:_Invoice.SetOpenRelated()
  Relate:_Invoice.Open                                     ! File _Invoice used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
      LOC:Options     = Ask_Date_Range()
      CASE MESSAGE('Would you like to select Clients to report on?', 'Select Clients', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
      OF BUTTON:No
         L_O2:Clients_Option      = 0
      OF BUTTON:Yes
         L_O2:Clients_Option      = 1
  
         Globalrequest            = SelectRecord
         Browse_Clients(LOC_Client_Tags)
  
         IF LOC_Client_Tags.NumberTagged() <= 0
            IF CLI:CID > 0
               LOC_Client_Tags.MakeTag(CLI:CID)
      .  .  .
  
  
      IF LOC_Client_Tags.NumberTagged() > 0 OR L_O2:Clients_Option = 0
         L_O2:Invoice_Credit_Option  = MESSAGE('Please select what to include in the list.', 'Invocie / Credit Note Listing', ICON:Question, 'All|Invoices|Credits', 1)
      ELSE
         ReturnValue  = LEVEL:Fatal
      .
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Invoice_Summary',ProgressWindow)     ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:_Invoice, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice No.')) THEN
     ThisReport.AppendOrder('+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice Date')) THEN
     ThisReport.AppendOrder('+INV:InvoiceDate,+INV:ClientName,+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client & Invoice No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientName,+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By DI No.')) THEN
     ThisReport.AppendOrder('+INV:DINo,+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch & Invoice No.')) THEN
     ThisReport.AppendOrder('+BRA:BranchName,+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice Composition')) THEN
     ThisReport.AppendOrder('+INV:ICID,+INV:IID')
  END
  ThisReport.SetFilter('INV:InvoiceDate >= LO:From_Date AND INV:InvoiceDate << LO:To_Date + 1')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_Invoice.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Invoice.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Invoice_Summary',ProgressWindow)  ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:grandtotals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF L_O2:Invoice_Credit_Option = 1
      ELSE
         IF L_O2:Invoice_Credit_Option = 2
            IF INV:Total < 0.0
               ReturnValue  = Record:Filtered
            .
         ELSE
            IF INV:Total >= 0.0
               ReturnValue  = Record:Filtered
      .  .  .
  
  
      IF L_O2:Clients_Option = 0          ! All
      ELSE
         IF LOC_Client_Tags.IsTagged(INV:CID) ~= TRUE
            ReturnValue  = Record:Filtered
      .  .
  
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','MANTRNIS','Print_Invoice_Summary','Print_Invoice_Summary','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Transporter_Invoices_VAT PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Options_Group    GROUP,PRE(L_OG)                       !
FromDate             DATE                                  !
ToDate               DATE                                  !
TID                  ULONG                                 !Transporter ID
TransporterName      STRING(35)                            !Transporters Name
CIN_FromDate         DATE                                  !
CIN_ToDate           DATE                                  !
PrintInvoices_WO_CD  BYTE                                  !
                     END                                   !
LO:Report_Vars       GROUP,PRE()                           !
R:Total              DECIMAL(11,2)                         !
                     END                                   !
LOC:Condition        STRING(2500)                          !
Process:View         VIEW(_InvoiceTransporter)
                       PROJECT(INT:CIN)
                       PROJECT(INT:CIN_DateReceived)
                       PROJECT(INT:Cost)
                       PROJECT(INT:InvoiceDate)
                       PROJECT(INT:MID)
                       PROJECT(INT:TID)
                       PROJECT(INT:TIN)
                       PROJECT(INT:VAT)
                       JOIN(TRA:PKey_TID,INT:TID)
                         PROJECT(TRA:TransporterName)
                       END
                     END
ProgressWindow       WINDOW('Report Invoice Transporter'),AT(,,237,223),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,MDI,TIMER(1)
                       SHEET,AT(3,4,231,197),USE(?Sheet1)
                         TAB('Options'),USE(?Tab1)
                           PROMPT('Transporter Name:'),AT(9,24),USE(?TransporterName:Prompt),TRN
                           BUTTON('...'),AT(74,24,12,10),USE(?CallLookup_Transporter)
                           ENTRY(@s35),AT(91,24,137,10),USE(L_OG:TransporterName),MSG('Transporters Name'),TIP('Transporte' & |
  'rs Name<0DH,0AH>Leave blank for all')
                           GROUP('FBN Invoice   (leave blank for no restriction)'),AT(7,44,221,48),USE(?Group1),BOXED, |
  TRN
                             PROMPT('From Date:'),AT(17,57),USE(?L_OG:FromDate:Prompt),TRN
                             BUTTON('...'),AT(73,57,12,10),USE(?Calendar)
                             SPIN(@d6),AT(91,57,60,10),USE(L_OG:FromDate),RIGHT(1)
                             PROMPT('To Date:'),AT(17,73),USE(?ToDate:Prompt),TRN
                             BUTTON('...'),AT(73,73,12,10),USE(?Calendar:2)
                             SPIN(@d6),AT(91,73,60,10),USE(L_OG:ToDate),RIGHT(1)
                           END
                           GROUP('Creditors Details   (leave blank for no restriction)'),AT(7,98,221,48),USE(?Group2), |
  BOXED,TRN
                             BUTTON('...'),AT(73,114,12,10),USE(?Calendar:CINFrom)
                             PROMPT('From Date:'),AT(17,114),USE(?L_OG:CIN_FromDate:Prompt),TRN
                             SPIN(@d6b),AT(91,114,60,10),USE(L_OG:CIN_FromDate),RIGHT(1)
                             PROMPT('To Date:'),AT(17,130),USE(?L_OG:CIN_ToDate:Prompt),TRN
                             BUTTON('...'),AT(73,130,12,10),USE(?Calendar:CINTo)
                             SPIN(@d6b),AT(91,130,60,10),USE(L_OG:CIN_ToDate),RIGHT(1)
                           END
                           CHECK(' Print Invoices Without Creditor Invoice Details'),AT(18,165),USE(L_OG:PrintInvoices_WO_CD), |
  TRN
                         END
                         TAB('Progress'),USE(?Tab2)
                           PROGRESS,AT(63,86,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(47,72,141,10),USE(?Progress:UserString),CENTER,TRN
                           STRING(''),AT(47,104,141,10),USE(?Progress:PctText),CENTER,TRN
                         END
                       END
                       BUTTON('Pause'),AT(122,204,,15),USE(?Pause),LEFT,ICON('Blue_R.ico'),FLAT
                       BUTTON('Cancel'),AT(186,204,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('_InvoiceTransporter Report'),AT(250,1125,7750,10063),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,875),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Transporter Invoices - Creditor Details'),AT(0,31,7750,220),USE(?ReportTitle),FONT('MS Sans Serif', |
  10,,FONT:bold),CENTER
                         STRING('Cred. To:'),AT(6229,385),USE(?String25:5),TRN
                         STRING('To:'),AT(1583,385),USE(?String25:2),TRN
                         STRING(@d6b),AT(500,385),USE(L_OG:FromDate),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('From:'),AT(135,385),USE(?String25),TRN
                         STRING(@d6b),AT(1844,385),USE(L_OG:ToDate),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Cred. From:'),AT(4583,385),USE(?String25:3),TRN
                         STRING(@d6b),AT(6771,385),USE(L_OG:CIN_ToDate),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@d6b),AT(5219,385),USE(L_OG:CIN_FromDate),FONT(,,,FONT:bold,CHARSET:ANSI)
                         BOX,AT(0,625,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(1042,625,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1813,625,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(2656,625,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(4177,625,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         LINE,AT(5146,625,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(6104,625,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(6906,625,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         STRING('Total'),AT(6979,677,698,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('TIN  (Invoice No.)'),AT(52,677,885,156),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('MID'),AT(1875,677,729,156),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Creditor No.'),AT(2760,677,1406,156),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Date Recd.'),AT(4375,677,,156),USE(?HeaderTitle:8),CENTER,TRN
                         STRING('Cost'),AT(5365,677,698,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('VAT'),AT(6167,677,698,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Invoice Date'),AT(1104,677,,156),USE(?HeaderTitle:5),CENTER,TRN
                       END
break_tid              BREAK(INT:TID)
                         HEADER,AT(0,0,,438),WITHNEXT(1)
                           STRING('Transporter:'),AT(573,104),USE(?String36),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@s35),AT(1563,104),USE(TRA:TransporterName),FONT(,10,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(0,438,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         END
Detail                   DETAIL,AT(,,7750,250),USE(?Detail)
                           LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                           LINE,AT(1042,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                           LINE,AT(1813,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                           LINE,AT(2656,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                           LINE,AT(4177,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                           LINE,AT(5146,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                           LINE,AT(6104,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                           LINE,AT(6906,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                           LINE,AT(7750,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                           STRING(@n_10),AT(73,52,885,167),USE(INT:TIN),RIGHT(1)
                           STRING(@d5b),AT(4375,52),USE(INT:CIN_DateReceived)
                           STRING(@s30),AT(2760,52,1406,156),USE(INT:CIN)
                           STRING(@n_10),AT(1875,52,729,167),USE(INT:MID),RIGHT(1)
                           STRING(@n-14.2),AT(5365,52,700,167),USE(INT:Cost),RIGHT,TRN
                           STRING(@n-14.2),AT(6167,52,700,167),USE(INT:VAT),RIGHT,TRN
                           STRING(@n-15.2),AT(6979,52,700,167),USE(R:Total),RIGHT(1),TRN
                           STRING(@d5b),AT(1115,52,635,167),USE(INT:InvoiceDate),RIGHT(1)
                           LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         END
                         FOOTER,AT(0,0,,542)
                           STRING('Transporter Totals:'),AT(1604,104),USE(?String36:2),TRN
                           STRING(@s35),AT(2635,104),USE(TRA:TransporterName,,?TRA:TransporterName:2),TRN
                           LINE,AT(1563,52,6146,0),USE(?Line14:4),COLOR(COLOR:Black)
                           STRING(@n-14.2),AT(5365,104,700,167),USE(INT:Cost,,?INT:Cost:3),RIGHT,SUM,TALLY(Detail),RESET(break_tid), |
  TRN
                           STRING(@n-14.2),AT(6167,104,700,167),USE(INT:VAT,,?INT:VAT:3),RIGHT,SUM,TALLY(Detail),RESET(break_tid), |
  TRN
                           STRING(@n-15.2),AT(6979,104,700,167),USE(R:Total,,?R:Total:3),RIGHT(1),SUM,TALLY(Detail),RESET(break_tid), |
  TRN
                           LINE,AT(1563,313,6146,0),USE(?Line14:5),COLOR(COLOR:Black)
                         END
                       END
detail_totals          DETAIL,AT(,,,729),USE(?detail_totals)
                         LINE,AT(3646,271,3969,0),USE(?Line14:3),COLOR(COLOR:Black)
                         STRING(@n-14.2),AT(4719,365,800,167),USE(INT:Cost,,?INT:Cost:2),RIGHT,SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(5771,365,802,167),USE(INT:VAT,,?INT:VAT:2),RIGHT,SUM,TALLY(Detail),TRN
                         STRING(@n-15.2),AT(6750,365,802,167),USE(R:Total,,?R:Total:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         LINE,AT(3646,604,3969,0),USE(?Line14:2),COLOR(COLOR:Black)
                         STRING('Grand Totals:'),AT(3781,365),USE(?String25:4),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass
Calendar3            CalendarClass
Calendar8            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.AskPreview PROCEDURE

  CODE
        PRINT(RPT:detail_totals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Transporter_Invoices_VAT')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TransporterName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:FromDate',L_OG:FromDate)                      ! Added by: Report
  BIND('L_OG:ToDate',L_OG:ToDate)                          ! Added by: Report
  BIND('L_OG:CIN_FromDate',L_OG:CIN_FromDate)              ! Added by: Report
  BIND('L_OG:CIN_ToDate',L_OG:CIN_ToDate)                  ! Added by: Report
  BIND('L_OG:TID',L_OG:TID)                                ! Added by: Report
  BIND('L_OG:PrintInvoices_WO_CD',L_OG:PrintInvoices_WO_CD) ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                                  ! File Transporter used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Transporter_Invoices_VAT',ProgressWindow) ! Restore window settings from non-volatile store
      L_OG:TID    = GETINI('Print_Transporter_Invoices_VAT', 'TID', , GLO:Local_INI)
  
      CLEAR(L_OG:TransporterName)
      TRA:TID = L_OG:TID
      IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
         L_OG:TransporterName = TRA:TransporterName
      .
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:_InvoiceTransporter, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder('+TRA:TransporterName,+INT:InvoiceDate')
  ThisReport.SetFilter('( (0 = L_OG:FromDate OR INT:InvoiceDate >= L_OG:FromDate)  AND  (0 = L_OG:ToDate OR INT:InvoiceDate << L_OG:ToDate + 1) AND  (0 = L_OG:CIN_FromDate OR INT:CIN_DateReceived >= L_OG:CIN_FromDate) AND  (0 = L_OG:CIN_ToDate OR INT:CIN_DateReceived << L_OG:CIN_ToDate + 1) AND  (L_OG:TID = 0 OR L_OG:TID = INT:TID) ) AND (L_OG:PrintInvoices_WO_CD = 1 OR INT:CIN_DateReceived <<> 0)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_InvoiceTransporter.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Transporter.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Transporter_Invoices_VAT',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Transporter
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      !    IF L_OG:FromDate = 0 OR L_OG:ToDate = 0
      !       MESSAGE('A From and To date is required to run the report.', 'Date Validation', ICON:Exclamation)
      !       SELECT(?L_OG:FromDate)
      !       CYCLE
      !    .
          CLEAR(LOC:Condition)
          ThisReport.SetFilter('')
          IF L_OG:FromDate ~= 0
             ThisReport.SetFilter('INT:InvoiceDate >= L_OG:FromDate', '1')
          .
          IF L_OG:ToDate ~= 0
             ThisReport.SetFilter('INT:InvoiceDate << L_OG:ToDate + 1', '2')
          .
          IF L_OG:CIN_FromDate ~= 0
             ThisReport.SetFilter('INT:CIN_DateReceived >= L_OG:CIN_FromDate', '3')
          .
          IF L_OG:CIN_ToDate ~= 0
             ThisReport.SetFilter('INT:CIN_DateReceived << L_OG:CIN_ToDate + 1', '4')
          .
          IF L_OG:TID ~= 0
             ThisReport.SetFilter('L_OG:TID = INT:TID', '5')
          .
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup_Transporter
      ThisWindow.Update()
      TRA:TransporterName = L_OG:TransporterName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_OG:TransporterName = TRA:TransporterName
        L_OG:TID = TRA:TID
      END
      ThisWindow.Reset(1)
    OF ?L_OG:TransporterName
          IF CLIP(L_OG:TransporterName) = ''
             CLEAR(L_OG:TID)
          .
      IF NOT ProgressWindow{PROP:AcceptAll}
        IF L_OG:TransporterName OR ?L_OG:TransporterName{PROP:Req}
          TRA:TransporterName = L_OG:TransporterName
          IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              L_OG:TransporterName = TRA:TransporterName
              L_OG:TID = TRA:TID
            ELSE
              CLEAR(L_OG:TID)
              SELECT(?L_OG:TransporterName)
              CYCLE
            END
          ELSE
            L_OG:TID = TRA:TID
          END
        END
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',L_OG:FromDate)
      IF Calendar5.Response = RequestCompleted THEN
      L_OG:FromDate=Calendar5.SelectedDate
      DISPLAY(?L_OG:FromDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',L_OG:ToDate)
      IF Calendar6.Response = RequestCompleted THEN
      L_OG:ToDate=Calendar6.SelectedDate
      DISPLAY(?L_OG:ToDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:CINFrom
      ThisWindow.Update()
      Calendar3.SelectOnClose = True
      Calendar3.Ask('Select a Date',L_OG:CIN_FromDate)
      IF Calendar3.Response = RequestCompleted THEN
      L_OG:CIN_FromDate=Calendar3.SelectedDate
      DISPLAY(?L_OG:CIN_FromDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:CINTo
      ThisWindow.Update()
      Calendar8.SelectOnClose = True
      Calendar8.Ask('Select a Date',L_OG:CIN_ToDate)
      IF Calendar8.Response = RequestCompleted THEN
      L_OG:CIN_ToDate=Calendar8.SelectedDate
      DISPLAY(?L_OG:CIN_ToDate)
      END
      ThisWindow.Reset(True)
    OF ?Pause
      ThisWindow.Update()
          SELECT(?Tab2)
      
          PUTINI('Print_Transporter_Invoices_VAT', 'TID', L_OG:TID, GLO:Local_INI)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      R:Total     = INT:Cost + INT:VAT
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:detail_totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','MANTRNIS','Print_Transporter_Invoices_VAT','Print_Transporter_Invoices_VAT','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! ===========
!!! </summary>
Print_Client_DeliveryReport PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Footer           BYTE(0)                               !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
LOC:Report           GROUP,PRE(L_REP)                      !
CID                  ULONG                                 !Client ID
Items                ULONG                                 !
Weight               DECIMAL(9,1)                          !
DelDate              DATE                                  !
DelTime              TIME                                  !
DatesAndTimes        STRING(255)                           !
Total_Items          ULONG                                 !
Total_Weight         DECIMAL(12,2)                         !
                     END                                   !
Process:View         VIEW(Deliveries)
                       PROJECT(DEL:CID)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:DITime)
                       PROJECT(DEL:Delivered)
                       PROJECT(DEL:SpecialDeliveryInstructions)
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                       END
                     END
ProgressWindow       WINDOW('Report - Client Deliveries'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('_Invoice Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Client Delivery Report'),AT(63,21,4885),USE(?ReportTitle),FONT('Arial',14,,FONT:bold), |
  CENTER
                         BOX,AT(0,354,7750,250),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@d5b),AT(5417,104),USE(LO:From_Date),RIGHT(1)
                         STRING('-    '),AT(6250,104,208,156),USE(?String24),TRN
                         STRING(@d5b),AT(6615,104),USE(LO:To_Date),RIGHT(1)
                         LINE,AT(6604,354,0,250),USE(?HeaderLine:9),COLOR(COLOR:Black)
                         LINE,AT(646,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2302,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(3781,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(5906,354,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(1427,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(4510,354,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         LINE,AT(5208,354,0,250),USE(?HeaderLine:8),COLOR(COLOR:Black)
                         LINE,AT(3083,354,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         STRING('Weight'),AT(3042,417,760,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('Col. Date'),AT(3885,417,583,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Del. Date'),AT(5260,417,583,167),USE(?HeaderTitle:8),CENTER,TRN
                         STRING('Del. Time'),AT(5969,417,583,167),USE(?HeaderTitle:9),CENTER,TRN
                         STRING('DI No.'),AT(42,417,573,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Invoice No.'),AT(698,417,729,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('No. Pieces'),AT(2417,417,,170),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Col. Time'),AT(4573,417,573,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('Reference'),AT(1469,417,868,170),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Special Inst.'),AT(6667,417,1042,167),USE(?String13),CENTER,TRN
                       END
Client                 BREAK(DEL:CID),USE(?BREAK1)
                         HEADER,AT(0,0,,292),USE(?GROUPHEADER1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s100),AT(938,31,4740,208),USE(CLI:ClientName)
                           STRING('Client No.:'),AT(5781,31,,208),USE(?String32:2),TRN
                           STRING(@n_10b),AT(6563,31),USE(CLI:ClientNo),RIGHT(1),TRN
                           STRING('Client:'),AT(344,31,427,208),USE(?String32),TRN
                           LINE,AT(0,292,7750,0),USE(?DetailEndLine11),COLOR(COLOR:Black)
                         END
Detail                   DETAIL,AT(0,0,7750,250),USE(?Detail)
                           LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                           LINE,AT(646,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                           LINE,AT(1427,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                           LINE,AT(2302,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                           LINE,AT(3083,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                           LINE,AT(3781,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                           LINE,AT(4510,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                           LINE,AT(5208,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                           LINE,AT(5906,0,0,250),USE(?DetailLine:9),COLOR(COLOR:Black)
                           LINE,AT(6604,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                           LINE,AT(0,250,7750,0),USE(?DetailEndLine12),COLOR(COLOR:Black)
                           STRING(@n6b),AT(2417,52,542,167),USE(L_REP:Items),RIGHT(1),TRN
                           STRING(@n_10),AT(42,52,573,167),USE(DEL:DINo),RIGHT(1)
                           STRING(@n_10b),AT(698,52),USE(INV:IID),RIGHT(1)
                           STRING(@s30),AT(1469,52,865,156),USE(DEL:ClientReference),TRN
                           STRING(@n-13),AT(2948,52),USE(L_REP:Weight),RIGHT(1),TRN
                           STRING(@d5b),AT(3885,52),USE(DEL:DIDate),RIGHT(1),TRN
                           STRING(@t7b),AT(4573,52),USE(DEL:DITime),RIGHT(1),TRN
                           STRING(@d5b),AT(5260,52),USE(L_REP:DelDate),RIGHT(1),TRN
                           STRING(@t7b),AT(5969,52),USE(L_REP:DelTime),RIGHT(1),TRN
                           STRING(@s255),AT(6667,52,1042,156),USE(DEL:SpecialDeliveryInstructions)
                         END
ReportFooter             DETAIL,AT(0,0,7750,719),USE(?DETAIL1)
                           STRING(@n-13),AT(3052,417,854,167),USE(L_REP:Total_Weight),RIGHT(1),TRN
                           STRING(@n8b),AT(2240,417,542,167),USE(L_REP:Total_Items),RIGHT(1),TRN
                           STRING('Total Pieces'),AT(2156,187),USE(?STRING1)
                           STRING('Total Weight'),AT(3281,187,625,167),USE(?STRING1:2)
                           BOX,AT(1448,73,3312,604),USE(?BOX1),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         END
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.AskPreview PROCEDURE

  CODE
    PRINT(  RPT:ReportFooter)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Client_DeliveryReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  BIND('L_REP:CID',L_REP:CID)                              ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
      LOC:Options     = Ask_Date_Range(,,, 'Select Delivered Dates to Report on')
      CASE MESSAGE('Would you like to select a Client to report on?', 'Select Client', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
      OF BUTTON:Yes
         Globalrequest            = SelectRecord
         Browse_Clients()
         IF GlobalResponse = RequestCancelled
            ReturnValue           = LEVEL:Fatal
         ELSE
            L_REP:CID             = CLI:CID
      .  .
  
  
  
  
  !    CASE MESSAGE('Would you like to select Clients to report on?', 'Select Clients', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
  !    OF BUTTON:No
  !       L_O2:Clients_Option      = 0
  !    OF BUTTON:Yes
  !       L_O2:Clients_Option      = 1
  !
  !       Globalrequest            = SelectRecord
  !       Browse_Clients(LOC_Client_Tags)
  !
  !       IF LOC_Client_Tags.NumberTagged() <= 0
  !          LOC_Client_Tags.MakeTag(CLI:CID)
  !    .  .
  !
  !
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF NOT TargetSelector.ASK(1) THEN
     SELF.Kill()
     RETURN Level:Fatal
  END
  IF NOT TargetSelector.GetPrintSelected() THEN
     SELF.SetReportTarget(TargetSelector.GetReportSelected())
  END
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:Deliveries, ?Progress:PctText, Progress:Thermometer, ProgressMgr, DEL:CID)
  ThisReport.AddSortOrder(DEL:FKey_CID)
  ThisReport.AppendOrder('+DEL:DINo,+DEL:DID')
  ThisReport.SetFilter('(DEL:DIDate >= LO:From_Date AND DEL:DIDate << (LO:To_Date + 1)) AND (L_REP:CID = 0 OR DEL:CID = L_REP:CID) AND (DEL:Delivered = 2)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Deliveries.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      CLEAR(INV:Record)
      INV:DID     = DEL:DID
      IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
      .
  
      ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache)
      ! (ULONG, BYTE, <ULONG>, <*STRING>, BYTE=0),ULONG
      ! p:Option
      !   0. Weight of all DID items - Volumetric considered
      !   1. Total Items not loaded on the DID    - Manifest
      !   2. Total Items loaded on the DID        - Manifest
      !   3. Total Items on the DID
      !   4. Total Items not loaded on the DID    - Tripsheet
      !   5. Total Items loaded on the DID        - Tripsheet
      !   6. Weight of all DID items - real weight
      !
      ! p:DIID is passed to check a single DIID
      ! p:Items_DIID_List is passed to return a list of DIIDs for the given Option
      !
      ! Cached entries are renewed every 1.5 seconds
  
      L_REP:Items     = Get_DelItem_s_Totals(DEL:DID, 3)
      L_REP:Weight    = Get_DelItem_s_Totals(DEL:DID, 6) / 100  ! weights as integer
  
      L_REP:Total_Items   += L_REP:Items
      L_REP:Total_Weight  += L_REP:Weight
  
      ! (p:Option, p:ID, p:ResultString)
      ! (BYTE=0, ULONG, <*STRING>), LONG
      !   p:Option
      !       0 - Unique set of Delivery Dates returned in a passed string
      !       1 - Unique set of Delivery Commodities returned in a passed string
      !       2 - Unique set of Delivery Packagings returned in a passed string
      !       3 - Unique set of Delivery Dates & Times returned in a passed string - eg. Date, Time, Date, Time
  
      Get_Info_SQL(3, DEL:DID, L_REP:DatesAndTimes)
  
      ! Just take the 1st Date and Time
                                                    ! (p:Delim_List, p:Delim, p:Chop_Out, p:Case_Sensitive)
    !message ('L_REP:DatesAndTimes: ' & CLIP(L_REP:DatesAndTimes))                                                    
      L_REP:DelDate   = DEFORMAT(Get_1st_Element_From_Delim_Str(L_REP:DatesAndTimes, ',', TRUE), @D6)
      L_REP:DelTime   = DEFORMAT(Get_1st_Element_From_Delim_Str(L_REP:DatesAndTimes, ',', TRUE), @T1)
  
      ! These should be set somewhere.....  ???
      IF DEL:ReceivedDate = 0
         DEL:ReceivedTime = DEL:DITime
      .
      IF DEL:ReceivedTime = 0 AND DEL:ReceivedDate ~= 0
         DEL:ReceivedTime = DEL:DITime
      .
  ReturnValue = PARENT.TakeRecord()
  IF LOC:Footer
    PRINT(RPT:ReportFooter)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','MANTRNIS','Print_Client_DeliveryReport','Print_Client_DeliveryReport','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! ===========
!!! </summary>
Print_UndeliveredPODs PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
LOC:Report           GROUP,PRE(L_REP)                      !
CID                  ULONG                                 !Client ID
Items                ULONG                                 !
Weight               DECIMAL(9,1)                          !
DatesAndTimes        STRING(255)                           !
MIDs                 STRING(35)                            !
ManifestDesc         STRING(30)                            !
Print_Detail         BYTE                                  !
Invoices             STRING(30)                            !
                     END                                   !
Process:View         VIEW(Deliveries)
                       PROJECT(DEL:CID)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:Delivered)
                       PROJECT(DEL:ReceivedDate)
                       PROJECT(DEL:ReceivedTime)
                       PROJECT(DEL:SpecialDeliveryInstructions)
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                       END
                     END
ProgressWindow       WINDOW('Report - Client Deliveries'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('_Invoice Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Undelivered Goods Report'),AT(63,21,4885),USE(?ReportTitle),FONT(,14,,FONT:bold),CENTER
                         BOX,AT(0,354,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         STRING(@d5b),AT(5417,104),USE(LO:From_Date),RIGHT(1)
                         STRING('-    '),AT(6250,104,208,156),USE(?String24),TRN
                         STRING(@d5b),AT(6615,104),USE(LO:To_Date),RIGHT(1)
                         LINE,AT(646,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1927,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(4552,354,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         LINE,AT(5365,354,0,250),USE(?HeaderLine:8),COLOR(COLOR:Black)
                         LINE,AT(4063,354,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(6438,354,0,250),USE(?HeaderLine:9),COLOR(COLOR:Black)
                         STRING('Invoice(s)'),AT(2969,406,868,170),USE(?HeaderTitle:5),CENTER,TRN
                         LINE,AT(2885,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         STRING('Weight'),AT(4563,406,760,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('Col. Date / Time'),AT(5396,417,990,156),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('DI No.'),AT(42,406,573,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Manifest Nos.'),AT(729,406,1146,156),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Manifest Desc.'),AT(1990,406,868,170),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Items'),AT(4094,406,417,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Special Inst.'),AT(6510,406,1198,167),USE(?String13),CENTER,TRN
                       END
Client                 BREAK(DEL:CID)
                         HEADER,AT(0,0,,292),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s100),AT(854,31,4740,208),USE(CLI:ClientName)
                           STRING('Client No.:'),AT(5802,31,,208),USE(?String32:2),TRN
                           STRING(@n_10b),AT(6563,31),USE(CLI:ClientNo),RIGHT(1),TRN
                           STRING('Client:'),AT(344,31,427,208),USE(?String32),TRN
                           LINE,AT(0,292,7750,0),USE(?DetailEndLine11),COLOR(COLOR:Black)
                         END
Detail                   DETAIL,AT(,,7750,250),USE(?Detail)
                           LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                           LINE,AT(646,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                           LINE,AT(1927,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                           LINE,AT(4063,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                           LINE,AT(4552,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                           LINE,AT(5365,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                           LINE,AT(6438,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                           LINE,AT(0,250,7750,0),USE(?DetailEndLine12),COLOR(COLOR:Black)
                           STRING(@n_10),AT(42,52,573,167),USE(DEL:DINo),RIGHT(1)
                           STRING(@s35),AT(729,52,1146,156),USE(L_REP:MIDs),CENTER(1)
                           STRING(@s30),AT(1990,52,865,156),USE(L_REP:ManifestDesc),LEFT(1),TRN
                           STRING(@s30),AT(2969,52,1042,156),USE(L_REP:Invoices),LEFT(1)
                           LINE,AT(2885,0,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                           STRING(@n6),AT(4094,52,417,156),USE(L_REP:Items),RIGHT(1)
                           STRING(@n-13),AT(4563,52,760,167),USE(L_REP:Weight),RIGHT(1),TRN
                           STRING(@d5b),AT(5375,52),USE(DEL:ReceivedDate),RIGHT(1),TRN
                           STRING(@t7b),AT(6010,52),USE(DEL:ReceivedTime),RIGHT(1),TRN
                           STRING(@s255),AT(6510,52,1198,156),USE(DEL:SpecialDeliveryInstructions)
                         END
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_UndeliveredPODs')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  BIND('L_REP:CID',L_REP:CID)                              ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
      LOC:Options     = Ask_Date_Range()
      CASE MESSAGE('Would you like to select a Client to report on?', 'Select Client', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
      OF BUTTON:Yes
         Globalrequest            = SelectRecord
         Browse_Clients()
         IF GlobalResponse = RequestCancelled
            ReturnValue           = LEVEL:Fatal
         ELSE
            L_REP:CID             = CLI:CID
      .  .
  
  
  
  
  !    CASE MESSAGE('Would you like to select Clients to report on?', 'Select Clients', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
  !    OF BUTTON:No
  !       L_O2:Clients_Option      = 0
  !    OF BUTTON:Yes
  !       L_O2:Clients_Option      = 1
  !
  !       Globalrequest            = SelectRecord
  !       Browse_Clients(LOC_Client_Tags)
  !
  !       IF LOC_Client_Tags.NumberTagged() <= 0
  !          LOC_Client_Tags.MakeTag(CLI:CID)
  !    .  .
  !
  !
  Do DefineListboxStyle
  INIMgr.Fetch('Print_UndeliveredPODs',ProgressWindow)     ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:Deliveries, ?Progress:PctText, Progress:Thermometer, ProgressMgr, DEL:CID)
  ThisReport.AddSortOrder(DEL:FKey_CID)
  ThisReport.AppendOrder('+DEL:DINo,+DEL:DID')
  ThisReport.SetFilter('(DEL:DIDate >= LO:From_Date AND DEL:DIDate << LO:To_Date + 1) AND (L_REP:CID = 0 OR DEL:CID = L_REP:CID) AND (DEL:Delivered << 2)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Deliveries.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_UndeliveredPODs',ProgressWindow)  ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  !    CLEAR(INV:Record)
  !    INV:DID     = DEL:DID
  !    IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
  !    .
  
      ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache)
      ! (ULONG, BYTE, <ULONG>, <*STRING>, BYTE=0),ULONG
      ! p:Option
      !   0. Weight of all DID items - Volumetric considered
      !   1. Total Items not loaded on the DID    - Manifest
      !   2. Total Items loaded on the DID        - Manifest
      !   3. Total Items on the DID
      !   4. Total Items not loaded on the DID    - Tripsheet
      !   5. Total Items loaded on the DID        - Tripsheet
      !   6. Weight of all DID items - real weight
      !
      ! p:DIID is passed to check a single DIID
      ! p:Items_DIID_List is passed to return a list of DIIDs for the given Option
      !
      ! Cached entries are renewed every 1.5 seconds
  
  
      L_REP:Items     = Get_DelItem_s_Totals(DEL:DID, 3)
      L_REP:Weight    = Get_DelItem_s_Totals(DEL:DID, 6) / 100    ! Weight is a ULONG X 100
  
  
  
      ! These should be set somewhere.....  ???
      IF DEL:ReceivedDate = 0
         DEL:ReceivedDate = DEL:DIDate
      .
      IF DEL:ReceivedTime = 0 AND DEL:ReceivedDate ~= 0
         DEL:ReceivedTime = DEL:DITime
      .
        
  
  
      ! (p:DID, p:No, p:MAN_DIID_List, p:List_Option, p:DIID)
      ! (ULONG, SHORT, <*STRING>, BYTE=0, ULONG=0),ULONG, PROC
      ! Look through all the Items for the Delivery and enumerate the MAN:MIDs into a Q
      ! p:No
      !   0. Return number of different MIDs (for this DID)
      !   x. Return this number MID - return the nth MID (for this DID)
      !
      ! p:List_Option
      !   0. Return list with MID, DIID, MID, DIID
      !   x. Return list with MID, MID
      !
      ! If not omitted - p:MAN_DIID_List
      !       Load it with MID,DIID,MID,DIID - which is the MIDs and the 1st DIID on each
      !
      ! A delivery item can be loaded on different trailers - i.e. different ManLoads
      ! Item can appear on more than one ManifestLoad but a single ManifestLoad can only have 1 instance of the Item
      !
      ! If p:DIID provided then only get for this DIID
  
      CLEAR(L_REP:MIDs)
      IF Get_Delivery_ManIDs(DEL:DID, 0, L_REP:MIDs, 1) > 0
      .
  
  
      L_REP:Print_Detail      = 1
  
      IF Upd_Delivery_Del_Status(DEL:DID) = 2         ! Delivered
         L_REP:Print_Detail   = 0
      .
  
  
  
      ! Show the invoice nos.
      CLEAR(L_REP:Invoices)
  
      _SQLTemp{PROP:SQL}  = 'SELECT IID FROM _Invoice WHERE DID = ' & DEL:DID
      IF ERRORCODE()
         MESSAGE('SQL Error: ' & ERROR() & '||File Err: ' & FILEERROR())
      ELSE
         LOOP
            NEXT(_SQLTemp)
            IF ERRORCODE()
               BREAK
            .
  
            L_REP:Invoices    = CLIP(L_REP:Invoices) & ', ' & CLIP(_SQ:S1)
      .  .
      L_REP:Invoices  = SUB(L_REP:Invoices, 3, LEN(L_REP:Invoices))
  ReturnValue = PARENT.TakeRecord()
  IF L_REP:Print_Detail = 1
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','MANTRNIS','Print_UndeliveredPODs','Print_UndeliveredPODs','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Shortages_Damages PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Shortages_Damages)
                       PROJECT(SHO:SDID)
                       PROJECT(SHO:DINo)
                       PROJECT(SHO:DI_Date)
                       PROJECT(SHO:TRID)
                       PROJECT(SHO:IID)
                       PROJECT(SHO:POD_IID)
                       PROJECT(SHO:DeliveryDepot)
                       PROJECT(SHO:Supervisor1)
                       PROJECT(SHO:Supervisor2)
                       PROJECT(SHO:CollectionBID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
SHO:SDID               LIKE(SHO:SDID)                 !List box control field - type derived from field
SHO:DINo               LIKE(SHO:DINo)                 !List box control field - type derived from field
SHO:DI_Date            LIKE(SHO:DI_Date)              !List box control field - type derived from field
SHO:TRID               LIKE(SHO:TRID)                 !List box control field - type derived from field
SHO:IID                LIKE(SHO:IID)                  !List box control field - type derived from field
SHO:POD_IID            LIKE(SHO:POD_IID)              !List box control field - type derived from field
SHO:DeliveryDepot      LIKE(SHO:DeliveryDepot)        !List box control field - type derived from field
SHO:Supervisor1        LIKE(SHO:Supervisor1)          !List box control field - type derived from field
SHO:Supervisor2        LIKE(SHO:Supervisor2)          !List box control field - type derived from field
SHO:CollectionBID      LIKE(SHO:CollectionBID)        !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Shortages & Damages file'),AT(,,358,208),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_Shortages_Damages'),SYSTEM
                       LIST,AT(8,22,342,142),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~S. & D. ID~C(0)@n_10@40R' & |
  '(2)|M~DI No.~C(0)@n_10@38R(2)|M~DI Date~C(0)@d5@40R(2)|M~Tripsheet ID~C(0)@N_10b@40R' & |
  '(2)|M~Invoice No.~C(0)@n_10@40R(2)|M~POD IID~C(0)@n_10@60L(2)|M~Delivery Depot~@s35@' & |
  '60L(2)|M~Supervisor 1~@s35@60L(2)|M~Supervisor 2~@s35@40R(2)|M~Collection BID~C(0)@n_10@'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing the Shortages_Damages file')
                       BUTTON('&Select'),AT(89,168,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,168,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,168,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,168,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,168,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,182),USE(?CurrentTab)
                         TAB('&1) By Shortages && Damages ID'),USE(?Tab:2)
                         END
                         TAB('&2) By Delivery'),USE(?Tab:3)
                         END
                         TAB('&3) By Tripsheet'),USE(?Tab:5)
                         END
                       END
                       BUTTON('&Close'),AT(305,190,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(164,190,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('&Print'),AT(4,190,,14),USE(?Print),LEFT,ICON(ICON:Print1),FLAT
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Shortages_Damages')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Shortages_Damages.Open                   ! File Shortages_Damages used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Shortages_Damages,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,)                            ! Add the sort order for  for sort order 1
  BRW1.AppendOrder('+SHO:DINo,+SHO:SDID')         ! Append an additional sort order
  BRW1.AddSortOrder(,SHO:FKey_TRID)               ! Add the sort order for SHO:FKey_TRID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,SHO:TRID,1,BRW1)      ! Initialize the browse locator using  using key: SHO:FKey_TRID , SHO:TRID
  BRW1.AppendOrder('+SHO:SDID')                   ! Append an additional sort order
  BRW1.AddSortOrder(,SHO:PKey_SDID)               ! Add the sort order for SHO:PKey_SDID for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(,SHO:SDID,1,BRW1)      ! Initialize the browse locator using  using key: SHO:PKey_SDID , SHO:SDID
  BRW1.AddField(SHO:SDID,BRW1.Q.SHO:SDID)         ! Field SHO:SDID is a hot field or requires assignment from browse
  BRW1.AddField(SHO:DINo,BRW1.Q.SHO:DINo)         ! Field SHO:DINo is a hot field or requires assignment from browse
  BRW1.AddField(SHO:DI_Date,BRW1.Q.SHO:DI_Date)   ! Field SHO:DI_Date is a hot field or requires assignment from browse
  BRW1.AddField(SHO:TRID,BRW1.Q.SHO:TRID)         ! Field SHO:TRID is a hot field or requires assignment from browse
  BRW1.AddField(SHO:IID,BRW1.Q.SHO:IID)           ! Field SHO:IID is a hot field or requires assignment from browse
  BRW1.AddField(SHO:POD_IID,BRW1.Q.SHO:POD_IID)   ! Field SHO:POD_IID is a hot field or requires assignment from browse
  BRW1.AddField(SHO:DeliveryDepot,BRW1.Q.SHO:DeliveryDepot) ! Field SHO:DeliveryDepot is a hot field or requires assignment from browse
  BRW1.AddField(SHO:Supervisor1,BRW1.Q.SHO:Supervisor1) ! Field SHO:Supervisor1 is a hot field or requires assignment from browse
  BRW1.AddField(SHO:Supervisor2,BRW1.Q.SHO:Supervisor2) ! Field SHO:Supervisor2 is a hot field or requires assignment from browse
  BRW1.AddField(SHO:CollectionBID,BRW1.Q.SHO:CollectionBID) ! Field SHO:CollectionBID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_Shortages_Damages',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  BRW1.PrintProcedure = 2
  BRW1.PrintControl = ?Print
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_Shortages_Damages',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,10,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Shortages_Damages.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_Shortages_Damages',QuickWindow)  ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Update_Shortages_Damages
      Print_Shortages_Damages
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Print
      BRW1.UpdateViewRecord()
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>2,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>2,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Delivery_Status PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Manifested       STRING(30)                            !Manifested status - this has maintenance function - Not Manifested, Partially Manifested, Partially Manifested Multiple, Fully Manifested, Fully Manifested Multiple
LOC:Delivered        STRING(20)                            !Delivered Status - this has maintenance function - Not Delivered, Partially Delivered, Delivered
LOC:Last_3_Statuses  GROUP,PRE(L_LS)                       !
Status_1             STRING(100)                           !Status and contact
Status_1_Date        DATE                                  !
Status_2             STRING(100)                           !Status and contact
Status_2_Date        DATE                                  !
Status_3             STRING(100)                           !Status and contact
Status_3_Date        DATE                                  !
                     END                                   !
LOC:IDx              LONG                                  !
LOC:Run_Options      GROUP,PRE(L_RO)                       !
From_Date            DATE                                  !
To_Date              DATE                                  !
CID                  ULONG                                 !Client ID
PrintLastStatuses    BYTE                                  !
                     END                                   !
LOC:Screen           GROUP,PRE(L_SG)                       !
From_Day             STRING(10)                            !
To_Day               STRING(10)                            !
OnlyShowDIsWithProgressInfo BYTE                           !
ClientName           STRING(100)                           !
ClientNo             ULONG                                 !Client No.
                     END                                   !
LOC:Report_Vars      GROUP,PRE(L_RV)                       !
Client_Contacts      STRING(50)                            !
UnpackDate           DATE                                  !
UpliftDate           DATE                                  !
DeliverDate          DATE                                  !
                     END                                   !
Process:View         VIEW(Deliveries)
                       PROJECT(DEL:CID)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:Delivered)
                       PROJECT(DEL:Manifested)
                       PROJECT(DEL:CollectionAID)
                       PROJECT(DEL:DeliveryAID)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:BID)
                       PROJECT(DEL:FID)
                       JOIN(ADD:PKey_AID,DEL:CollectionAID)
                         PROJECT(ADD:AddressName)
                       END
                       JOIN(A_ADD:PKey_AID,DEL:DeliveryAID)
                         PROJECT(A_ADD:AddressName)
                       END
                       JOIN(DELI:FKey_DID_ItemNo,DEL:DID)
                         PROJECT(DELI:ContainerNo)
                         PROJECT(DELI:ContainerVessel)
                         PROJECT(DELI:ETA)
                         PROJECT(DELI:Units)
                         PROJECT(DELI:PTID)
                         PROJECT(DELI:CMID)
                         JOIN(PACK:PKey_PTID,DELI:PTID)
                           PROJECT(PACK:Packaging)
                         END
                         JOIN(COM:PKey_CMID,DELI:CMID)
                           PROJECT(COM:Commodity)
                         END
                       END
                       JOIN(BRA:PKey_BID,DEL:BID)
                       END
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:AID)
                         PROJECT(CLI:ClientName)
                       END
                       JOIN(FLO:PKey_FID,DEL:FID)
                       END
                     END
ProgressWindow       WINDOW('Report Deliveries Status'),AT(,,216,160),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,MDI,TIMER(1)
                       SHEET,AT(4,4,209,136),USE(?Sheet1)
                         TAB('Options'),USE(?Tab_Options)
                           PROMPT('Client Name:'),AT(13,24),USE(?L_SG:ClientName:Prompt),TRN
                           BUTTON('...'),AT(65,24,12,10),USE(?CallLookup_Client)
                           ENTRY(@s100),AT(81,24,126,10),USE(L_SG:ClientName),TIP('Client name')
                           STRING('Leave Client blank for all.'),AT(81,38,126,10),USE(?String5),TRN
                           PROMPT('From Date (DI):'),AT(13,60),USE(?L_RO:From_Date:Prompt),TRN
                           BUTTON('...'),AT(65,60,12,10),USE(?Calendar)
                           SPIN(@d5b),AT(81,60,66,10),USE(L_RO:From_Date),RIGHT(1)
                           STRING(@s10),AT(150,60),USE(L_SG:From_Day),TRN
                           PROMPT('To Date (DI):'),AT(13,82),USE(?To_Date:Prompt),TRN
                           BUTTON('...'),AT(65,82,12,10),USE(?Calendar:2)
                           SPIN(@d5b),AT(81,82,66,10),USE(L_RO:To_Date),RIGHT(1)
                           STRING(@s10),AT(150,82),USE(L_SG:To_Day),TRN
                           CHECK(' Print Last Statuses'),AT(81,106),USE(L_RO:PrintLastStatuses),TRN
                           CHECK(' Only Show DI''s with Progress Info'),AT(81,126),USE(L_SG:OnlyShowDIsWithProgressInfo), |
  TRN
                         END
                         TAB('Progress'),USE(?Tab_Progress),DISABLE
                           PROGRESS,AT(49,54,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(33,42,141,10),USE(?Progress:UserString),CENTER,TRN
                           STRING(''),AT(33,70,141,10),USE(?Progress:PctText),CENTER,TRN
                         END
                       END
                       BUTTON('Pause'),AT(110,142,49,15),USE(?Pause),LEFT,ICON('VCRDOWN.ICO'),FLAT
                       BUTTON('Cancel'),AT(164,142,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Deliveries Report'),AT(250,1219,10396,6510),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,10396,958),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Less than Container Loads & Containers Status Report'),AT(4781,333,5573,313),USE(?ReportTitle), |
  FULL,FONT(,14,COLOR:Black,FONT:bold,CHARSET:ANSI),CENTER
                         IMAGE('fbn_logo_small.jpg'),AT(52,42,,938),USE(?Image2),CENTERED
                       END
break_client           BREAK(DEL:CID)
                         HEADER,AT(0,0,,583)
                           STRING(@s50),AT(5521,94),USE(L_RV:Client_Contacts)
                           LINE,AT(9688,333,0,250),USE(?HeaderLine:12),COLOR(COLOR:Black)
                           STRING('Client:'),AT(1094,63),USE(?String28),FONT(,10,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                           STRING(@s35),AT(1583,63),USE(CLI:ClientName),FONT(,10,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           STRING('DI No.'),AT(60,396,521,167),USE(?HeaderTitle:1),CENTER,TRN
                           STRING('Client Ref.'),AT(698,396,688,167),USE(?HeaderTitle:2),CENTER,TRN
                           STRING('Container No.'),AT(1490,396,927,167),USE(?String26:3),CENTER,TRN
                           STRING('Vessel'),AT(2490,396,927,167),USE(?String26:2),CENTER,TRN
                           STRING('ETA'),AT(3490,396,729,167),USE(?String26),CENTER,TRN
                           STRING('Depot (from)'),AT(4115,396,729,167),USE(?HeaderTitle:3),CENTER,TRN
                           STRING('Consignee'),AT(4948,396,927,167),USE(?HeaderTitle:4),CENTER,TRN
                           STRING('Commodity'),AT(5969,396,750,167),USE(?HeaderTitle:5),CENTER,TRN
                           BOX,AT(0,323,10365,260),USE(?HeaderBox),COLOR(COLOR:Black)
                           LINE,AT(630,333,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                           LINE,AT(1440,333,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                           LINE,AT(2440,333,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                           LINE,AT(3440,333,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                           LINE,AT(4073,333,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                           LINE,AT(4890,333,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                           LINE,AT(5917,323,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                           LINE,AT(7229,323,0,250),USE(?HeaderLine:8),COLOR(COLOR:Black)
                           LINE,AT(8042,333,0,250),USE(?HeaderLine:9),COLOR(COLOR:Black)
                           LINE,AT(9010,333,0,250),USE(?HeaderLine:11),COLOR(COLOR:Black)
                           STRING('Uplift'),AT(9063,396,583,167),USE(?HeaderTitle:10),CENTER,TRN
                           LINE,AT(6750,333,0,250),USE(?HeaderLine:10),COLOR(COLOR:Black)
                           STRING('Qty'),AT(6781,396,406,167),USE(?HeaderTitle:6),CENTER,TRN
                           STRING('Packaging'),AT(7281,396,,167),USE(?HeaderTitle:7),CENTER,TRN
                           STRING('Unpack'),AT(8385,396,583,167),USE(?HeaderTitle:9),CENTER,TRN
                           STRING('Deliver'),AT(9740,396,583,167),USE(?HeaderTitle:11),CENTER,TRN
                         END
Detail                   DETAIL,AT(,,11063,250),USE(?Detail)
                           LINE,AT(0,0,10365,0),USE(?DetailEndLine:2),COLOR(COLOR:Black)
                           LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                           LINE,AT(630,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                           LINE,AT(1440,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                           LINE,AT(2440,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                           LINE,AT(3440,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                           LINE,AT(4073,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                           LINE,AT(4890,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                           LINE,AT(5917,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                           LINE,AT(6750,0,0,250),USE(?DetailLine:13),COLOR(COLOR:Black)
                           LINE,AT(7229,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                           LINE,AT(8042,0,0,250),USE(?DetailLine:9),COLOR(COLOR:Black)
                           LINE,AT(10365,0,0,250),USE(?DetailLine:10),COLOR(COLOR:Black)
                           LINE,AT(0,240,10365,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                           STRING(@n_10),AT(-104,42,,167),USE(DEL:DINo),RIGHT(1),TRN
                           STRING(@s30),AT(698,42,688,167),USE(DEL:ClientReference),LEFT
                           STRING(@s35),AT(1490,42,927,167),USE(DELI:ContainerNo)
                           STRING(@s35),AT(2490,42,927,167),USE(DELI:ContainerVessel)
                           STRING(@d5b),AT(3490,42,,167),USE(DELI:ETA)
                           STRING(@s35),AT(4115,42,729,167),USE(ADD:AddressName)
                           STRING(@s35),AT(4948,52,927,156),USE(A_ADD:AddressName)
                           STRING(@s35),AT(5969,42,750,167),USE(COM:Commodity),TRN
                           STRING(@n6),AT(6781,42),USE(DELI:Units),RIGHT(1),TRN
                           STRING(@s35),AT(7292,52,677,156),USE(PACK:Packaging),TRN
                           STRING(@d5b),AT(8385,52),USE(L_RV:UnpackDate)
                           STRING(@d5b),AT(9063,52),USE(L_RV:UpliftDate)
                           STRING(@d5b),AT(9740,52),USE(L_RV:DeliverDate)
                           LINE,AT(9688,0,0,250),USE(?DetailLine:15),COLOR(COLOR:Black)
                           LINE,AT(9010,0,0,250),USE(?DetailLine:14),COLOR(COLOR:Black)
                         END
detail2                  DETAIL,AT(,,,302),USE(?detail2)
                           BOX,AT(625,20,9750,260),USE(?HeaderBox:3),COLOR(COLOR:Black)
                           STRING(@s100),AT(3854,73,3125,156),USE(L_LS:Status_2)
                           STRING(@s100),AT(677,73,3021,156),USE(L_LS:Status_1)
                           STRING(@s100),AT(7135,73,3177,156),USE(L_LS:Status_3)
                           LINE,AT(3792,20,0,250),USE(?DetailLine:11),COLOR(COLOR:Black)
                           LINE,AT(7083,20,0,250),USE(?DetailLine:12),COLOR(COLOR:Black)
                         END
                       END
                       FOOTER,AT(250,7729,10396,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(9615,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass
DelProg         VIEW(DeliveryProgress)
    PROJECT(DELP:DID, DELP:DSID, DELP:StatusDate, DELP:StatusTime, DELP:ActionDate)
       JOIN(DELS:PKey_DSID, DELP:DSID)
       PROJECT(DELS:DSID, DELS:DeliveryStatus, DELS:ReportColumn)
       .
       JOIN(ADDC:PKey_ACID, DELP:ACID)
       PROJECT(ADDC:ContactName)
    .  .


DelProg_Mgr     ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Days                                    ROUTINE
    L_SG:From_Day       = Week_Day(L_RO:From_Date)
    L_SG:To_Day         = Week_Day(L_RO:To_Date)

    DISPLAY
    EXIT
Contact_Info                        ROUTINE
    DATA
Buf_        USHORT

    CODE
    Buf_    = Access:Addresses.SaveBuffer()

    CLEAR(L_RV:Client_Contacts)
    ADD:AID                 = CLI:AID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       ! (p:Add, p:List, p:Delim)
       !Add_to_List(CLIP(ADD:Fax), L_RV:Client_Contacts, ',',, '  F: ')
       L_RV:Client_Contacts     = 'Fax: ' & CLIP(ADD:Fax)
    .

    Access:Addresses.RestoreBuffer(Buf_)
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Delivery_Status')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?L_SG:ClientName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_RO:From_Date',L_RO:From_Date)                    ! Added by: Report
  BIND('L_RO:To_Date',L_RO:To_Date)                        ! Added by: Report
  BIND('L_RO:CID',L_RO:CID)                                ! Added by: Report
      DelProg_Mgr.Init(DelProg, Relate:DeliveryProgress)
      DelProg_Mgr.AddSortOrder(DELP:FKey_DID)
      DelProg_Mgr.AppendOrder('-DELP:StatusDate')         !, -DELP:StatusTime')
      L_RO:From_Date      = TODAY() - 6
      L_RO:To_Date        = TODAY()
  
      L_SG:From_Day       = Week_Day(L_RO:From_Date)
      L_SG:To_Day         = Week_Day(L_RO:To_Date)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:AddressContacts.SetOpenRelated()
  Relate:AddressContacts.Open                              ! File AddressContacts used by this procedure, so make sure it's RelationManager is open
  Access:DeliveryProgress.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryStatuses.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
      L_SG:OnlyShowDIsWithProgressInfo    = GETINI('Print_Delivery_Status', 'OnlyShowDIsWithProgressInfo', , GLO:Local_INI)
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Deliveries, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder('+CLI:ClientName,+DEL:DINo,+DELI:ItemNo')
  ThisReport.SetFilter('(L_RO:From_Date = 0 OR DEL:DIDate >= L_RO:From_Date) AND (L_RO:To_Date = 0 OR DEL:DIDate << L_RO:To_Date + 1) AND (L_RO:CID = DEL:CID OR L_RO:CID = 0)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Deliveries.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      DelProg_Mgr.Kill()
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressContacts.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
          PUTINI('Print_Delivery_Status', 'OnlyShowDIsWithProgressInfo', L_SG:OnlyShowDIsWithProgressInfo, GLO:Local_INI)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup_Client
      ThisWindow.Update()
      CLI:ClientName = L_SG:ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SG:ClientName = CLI:ClientName
        L_RO:CID = CLI:CID
        L_SG:ClientNo = CLI:ClientNo
      END
      ThisWindow.Reset(1)
    OF ?L_SG:ClientName
      IF NOT ProgressWindow{PROP:AcceptAll}
        IF L_SG:ClientName OR ?L_SG:ClientName{PROP:Req}
          CLI:ClientName = L_SG:ClientName
          IF Access:Clients.TryFetch(CLI:Key_ClientName)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              L_SG:ClientName = CLI:ClientName
              L_RO:CID = CLI:CID
              L_SG:ClientNo = CLI:ClientNo
            ELSE
              CLEAR(L_RO:CID)
              CLEAR(L_SG:ClientNo)
              SELECT(?L_SG:ClientName)
              CYCLE
            END
          ELSE
            L_RO:CID = CLI:CID
            L_SG:ClientNo = CLI:ClientNo
          END
        END
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
          DO Days
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',L_RO:From_Date)
      IF Calendar5.Response = RequestCompleted THEN
      L_RO:From_Date=Calendar5.SelectedDate
      DISPLAY(?L_RO:From_Date)
      END
      ThisWindow.Reset(True)
    OF ?L_RO:From_Date
          DO Days
    OF ?Calendar:2
      ThisWindow.Update()
          DO Days
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',L_RO:To_Date)
      IF Calendar6.Response = RequestCompleted THEN
      L_RO:To_Date=Calendar6.SelectedDate
      DISPLAY(?L_RO:To_Date)
      END
      ThisWindow.Reset(True)
    OF ?L_RO:To_Date
          DO Days
    OF ?Pause
      ThisWindow.Update()
          IF ?Tab_Options{PROP:Enabled} = FALSE
             ENABLE(?Tab_Options)
             DISABLE(?Tab_Progress)
          ELSE
             ENABLE(?Tab_Progress)
             DISABLE(?Tab_Options)
          .
      
          !DISABLE(?Pause)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_RO:From_Date
          DO Days
    OF ?L_RO:To_Date
          DO Days
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      DO Contact_Info
      ! Not Manifested|Partially Manifested|Partially Manifested Multiple|Fully Manifested|Fully Manifested Multiple
      EXECUTE DEL:Manifested + 1
         LOC:Manifested   = 'Not Manifested'
         LOC:Manifested   = 'Partially Manifested'
         LOC:Manifested   = 'Partially Manifested Multiple'
         LOC:Manifested   = 'Fully Manifested'
         LOC:Manifested   = 'Fully Manifested Multiple'
      ELSE
         LOC:Manifested   = '<unknown>'
      .
  
      ! Not Delivered|Partially Delivered|Delivered
      EXECUTE DEL:Delivered + 1
         LOC:Delivered    = 'Not Delivered'
         LOC:Delivered    = 'Partially Delivered'
         LOC:Delivered    = 'Delivered'
      ELSE
         LOC:Delivered    = '<unknown>'
      .
  
  !       old view
  !    CLEAR(LOC:Last_3_Statuses)
  !
  !    DelProg_Mgr.Init(DelProg, Relate:DeliveryProgress)
  !    DelProg_Mgr.AddSortOrder(DELP:FKey_DID)
  !    DelProg_Mgr.AddRange(DELP:DID, DEL:DID)
  !    DelProg_Mgr.AppendOrder('-DELP:StatusDate, -DELP:StatusTime')
  !
  !    LOC:Idx = 0
  !
  !    OPEN(DelProg)
  !    DelProg{PROP:Filter}        = 'DELP:DID = ' & DEL:DID
  !    DelProg{PROP:Order}         = '-DELP:StatusDate, -DELP:StatusTime'
  !
  !    SET(DelProg)
  !
  !    LOOP
  !       NEXT(DelProg)
  !       IF ERRORCODE()
  !          BREAK
  !       .
  !
  !       LOC:Idx  += 1
  !
  !       CASE LOC:Idx
  !       OF 1
  !          L_LS:Status_3         = DELS:DeliveryStatus
  !          L_LS:Status_3_Date    = DELP:StatusDate
  !       OF 2
  !          L_LS:Status_2         = DELS:DeliveryStatus
  !          L_LS:Status_2_Date    = DELP:StatusDate
  !       OF 3
  !          L_LS:Status_1         = DELS:DeliveryStatus
  !          L_LS:Status_1_Date    = DELP:StatusDate
  !    .  .
  !    CLOSE(DelProg)
  !
      CLEAR(LOC:Last_3_Statuses)
      CLEAR(L_RV:UpliftDate)
      CLEAR(L_RV:DeliverDate)
      LOC:Idx = 0
  
      DelProg_Mgr.AddRange(DELP:DID, DEL:DID)
  
      DelProg_Mgr.Reset()
      LOOP
         IF DelProg_Mgr.Next() ~= LEVEL:Benign
            BREAK
         .
  
         LOC:Idx  += 1
  
         CASE LOC:Idx
         OF 1
            L_LS:Status_3         = FORMAT(DELP:StatusDate,@d5b)
            IF DELP:StatusDate ~= DELP:ActionDate
               L_LS:Status_3      = CLIP(L_LS:Status_3) & ' (' & FORMAT(DELP:ActionDate,@d5b) & ')'
            .
            L_LS:Status_3         = CLIP(L_LS:Status_3) & '  ' & CLIP(DELS:DeliveryStatus) & ' (' & CLIP(ADDC:ContactName) & ')'
         OF 2
            L_LS:Status_2         = FORMAT(DELP:StatusDate,@d5b)
            IF DELP:StatusDate ~= DELP:ActionDate
               L_LS:Status_2      = CLIP(L_LS:Status_2) & ' (' & FORMAT(DELP:ActionDate,@d5b) & ')'
            .
            L_LS:Status_2         = CLIP(L_LS:Status_2) & '  ' & CLIP(DELS:DeliveryStatus) & ' (' & CLIP(ADDC:ContactName) & ')'
         OF 3
            L_LS:Status_1         = FORMAT(DELP:StatusDate,@d5b)
            IF DELP:StatusDate ~= DELP:ActionDate
               L_LS:Status_1      = CLIP(L_LS:Status_1) & ' (' & FORMAT(DELP:ActionDate,@d5b) & ')'
            .
            L_LS:Status_1         = CLIP(L_LS:Status_1) & '  ' & CLIP(DELS:DeliveryStatus) & ' (' & CLIP(ADDC:ContactName) & ')'
         .
  
         ! We want to make sure the latest dates in each category are preserved, not the earliest
         !  None|Un-Pack|Up-Lift
         CASE DELS:ReportColumn
         OF 1
            IF L_RV:UnpackDate = 0
               L_RV:UnpackDate     = DELP:ActionDate
            .
         OF 2
            IF L_RV:UpliftDate = 0
               L_RV:UpliftDate     = DELP:ActionDate
      .  .  .
  
      ! Delivered Date
  
      ! Get_TripDelItems_Info
      ! (p:TRID, p:Option, p:DID, p:TDID, p:DIID, p:Del_List)
      ! (<ULONG>, BYTE=0, <ULONG>, <ULONG>, <ULONG>, <*STRING>),ULONG
      !   1         2        3        4       5           6
      ! p:Del_List
      !   Either list of Dates & Times or TRID's
      ! p:Option
      ! TRID passed
      !   0. Weight (real weight - not volumetric)
      !   1. Units
      !       if p:DID is provided then the result is limited to the DID passed
      !       if p:TDID is provided then exclude this from weight
      !   2. List of TS-Delivery delivered Dates, no of entries returned - for DIID or TRID!
      ! DID passed only       (no of entries returned )
      !   3. List of TS delivered Dates & Times   (d5 - t1, d5 - t1)
      !   4. List of TS TRIDs
      !
      !   Note - if option 3 or 4 used but param 3 ommitted then this will cause a GPF in current form!
  
      IF Get_TripDelItems_Info(, 3, DEL:DID,,, dates_") > 0
         L_RV:DeliverDate     = DEFORMAT(SUB(dates_",1,8), @d5)
      .
  
  ReturnValue = PARENT.TakeRecord()
      IF L_SG:OnlyShowDIsWithProgressInfo = FALSE OR LOC:Idx ~= 0
      
  IF 0
    PRINT(RPT:detail2)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
        IF L_RO:PrintLastStatuses
          PRINT(RPT:detail2)
        END
      End
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','MANTRNIS','Print_Status','Print_Status','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Operations_DI_Release PROCEDURE 

LOC:Group_Options    GROUP,PRE(L_GO)                       !
From_Date            DATE                                  !
To_Date              DATE                                  !
Acc_Pre_Paid         BYTE(1)                               !
Day_60_Balance       BYTE(1)                               !
Account_Limit        BYTE(1)                               !
Manifested           BYTE(1)                               !
Timer                LONG                                  !
Confirm              BYTE                                  !Confirm each DI
CID                  ULONG                                 !Client ID
ClientNo             ULONG                                 !Client No.
ClientName           STRING(100)                           !
                     END                                   !
LOC:Counts_Group     GROUP,PRE(L_CG)                       !
Estimated            ULONG                                 !
Progress             LONG                                  !
Start_Time           LONG                                  !
                     END                                   !
Window               WINDOW('DI Release - Bulk'),AT(,,331,325),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  MDI,TIMER(100)
                       SHEET,AT(3,4,327,302),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           GROUP,AT(3,19,324,284),USE(?Group2),FONT('Tahoma')
                             PROMPT('From DI Date:'),AT(7,24,,10),USE(?From_Date:Prompt),TRN
                             SPIN(@d6b),AT(71,24,70,10),USE(L_GO:From_Date)
                             BUTTON('...'),AT(55,24,12,10),USE(?Calendar)
                             PROMPT('To DI Date:'),AT(7,40,,10),USE(?To_Date:Prompt),TRN
                             SPIN(@d6b),AT(71,40,70,10),USE(L_GO:To_Date)
                             BUTTON('...'),AT(55,40,12,10),USE(?Calendar:2)
                             PROMPT('Manifested:'),AT(7,59,,10),USE(?L_GO:Manifested:Prompt),TRN
                             LIST,AT(71,59,70,10),USE(L_GO:Manifested),DISABLE,DROP(5),FROM('All|#0|Manifested|#1|No' & |
  't Manifested|#2')
                             PROMPT('Client Name:'),AT(7,106),USE(?ClientName:Prompt),TRN
                             BUTTON('...'),AT(55,106,12,10),USE(?CallLookup)
                             ENTRY(@s100),AT(71,106,183,10),USE(L_GO:ClientName),REQ,TIP('Client name')
                             PROMPT('Client No.:'),AT(7,122),USE(?L_GO:ClientNo:Prompt),TRN
                             ENTRY(@n_10b),AT(71,122,70,10),USE(L_GO:ClientNo),RIGHT(1),COLOR(00E9E9E9h),MSG('Client No.'), |
  READONLY,SKIP,TIP('Client No.')
                             BUTTON('X'),AT(147,122,12,10),USE(?Button_Clear),TIP('Clear Client details')
                             LINE,AT(3,138,324,0),USE(?Line1:2),COLOR(COLOR:Black)
                             GROUP('Release Types'),AT(7,148,255,59),USE(?Group1),BOXED,TRN
                               CHECK('&Acc. Pre-Paid'),AT(71,161),USE(L_GO:Acc_Pre_Paid),TRN
                               CHECK('&60+ Day Balance'),AT(71,177),USE(L_GO:Day_60_Balance),TRN
                               CHECK('Account &Limit'),AT(71,193),USE(L_GO:Account_Limit),TRN
                             END
                             LINE,AT(3,215,324,0),USE(?Line1),COLOR(COLOR:Black)
                             CHECK(' &Confirm each DI'),AT(71,226),USE(L_GO:Confirm),MSG('Confirm each DI'),TIP('Confirm each DI'), |
  TRN
                             PROMPT('Estimated DI''s:'),AT(159,226),USE(?L_CG:Estimated:Prompt),TRN
                             STRING(@n13),AT(210,226),USE(L_CG:Estimated),RIGHT(1),TRN
                             STRING('Optionally select a Client  (leave blank for all clients)'),AT(7,92),USE(?STRING1), |
  FONT(,,,FONT:bold),TRN
                           END
                         END
                       END
                       BUTTON('&OK'),AT(251,309,35,14),USE(?OkButton),DEFAULT
                       BUTTON('&Close'),AT(293,309,36,14),USE(?CancelButton)
                       PROGRESS,AT(39,311,144,8),USE(?Progress1),HIDE,RANGE(0,100)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Calendar2            CalendarClass
Calendar3            CalendarClass
View_           VIEW(Deliveries)
                    PROJECT(DEL:DID, DEL:DINo, DEL:ReleasedUID)
                .

VM              ViewManager



ReleaseClass        CLASS,TYPE

Q_              &SQLQueryClass
                
Count_Rel       LONG

Set_Filter      PROCEDURE()
ReleaseDI       PROCEDURE(ULONG p_DID)

SQL             PROCEDURE()

Construct       PROCEDURE()
Destruct        PROCEDURE()
    .




Release_        ReleaseClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Operations_DI_Release')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?From_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Operations_DI_Release',Window)             ! Restore window settings from non-volatile store
  L_GO:From_Date	= TODAY()-1
  L_GO:To_Date	= TODAY()
  SELF.SetAlerts()
      Window{PROP:Timer}  = 0
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Operations_DI_Release',Window)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Select_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_GO:From_Date
          Release_.SQL()
    OF ?Calendar
      ThisWindow.Update()
      Calendar2.SelectOnClose = True
      Calendar2.Ask('Select a Date',L_GO:From_Date)
      IF Calendar2.Response = RequestCompleted THEN
      L_GO:From_Date=Calendar2.SelectedDate
      DISPLAY(?L_GO:From_Date)
      END
      ThisWindow.Reset(True)
    OF ?L_GO:To_Date
          Release_.SQL()
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar3.SelectOnClose = True
      Calendar3.Ask('Select a Date',L_GO:To_Date)
      IF Calendar3.Response = RequestCompleted THEN
      L_GO:To_Date=Calendar3.SelectedDate
      DISPLAY(?L_GO:To_Date)
      END
      ThisWindow.Reset(True)
    OF ?L_GO:Manifested
          Release_.SQL()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = L_GO:ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_GO:ClientName = CLI:ClientName
        L_GO:CID = CLI:CID
        L_GO:ClientNo = CLI:ClientNo
      END
      ThisWindow.Reset(1)
          Release_.SQL()
    OF ?L_GO:ClientName
          Release_.SQL()
      IF NOT Window{PROP:AcceptAll}
        IF L_GO:ClientName OR ?L_GO:ClientName{PROP:Req}
          CLI:ClientName = L_GO:ClientName
          IF Access:Clients.TryFetch(CLI:Key_ClientName)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              L_GO:ClientName = CLI:ClientName
              L_GO:CID = CLI:CID
              L_GO:ClientNo = CLI:ClientNo
            ELSE
              CLEAR(L_GO:CID)
              CLEAR(L_GO:ClientNo)
              SELECT(?L_GO:ClientName)
              CYCLE
            END
          ELSE
            L_GO:CID = CLI:CID
            L_GO:ClientNo = CLI:ClientNo
          END
        END
      END
      ThisWindow.Reset()
    OF ?Button_Clear
      ThisWindow.Update()
          CLEAR(L_GO:CID)
          CLEAR(L_GO:ClientNo)
          CLEAR(L_GO:ClientName)
          DISPLAY
          Release_.SQL()
    OF ?OkButton
      ThisWindow.Update()
          IF L_GO:Acc_Pre_Paid = FALSE AND L_GO:Day_60_Balance = FALSE AND L_GO:Account_Limit = FALSE
             MESSAGE('You must select at least one Release Type', 'Release DIs', ICON:Exclamation)
          ELSE
             DISABLE(?Group2)
      
             DISABLE(?OkButton)
             DISABLE(?CancelButton)
      
             UNHIDE(?Progress1)
      
             Release_.SQL()
      
             ?Progress1{PROP:RangeHigh}   = L_CG:Estimated
             L_CG:Progress                = 0
             ?Progress1{PROP:Progress}    = L_CG:Progress
             Release_.Count_Rel           = 0
      
             Release_.Set_Filter()
      
             L_GO:Timer                   = 10
             Window{PROP:Timer}  = L_GO:Timer
          .
    OF ?CancelButton
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_GO:From_Date
          Release_.SQL()
    OF ?L_GO:To_Date
          Release_.SQL()
    OF ?L_GO:Manifested
          Release_.SQL()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          Release_.SQL()
    OF EVENT:Timer
          Window{PROP:Timer}  = 0
      
          L_CG:Start_Time = CLOCK()
      
          LOOP
             IF CLOCK() - L_CG:Start_Time > 100
                BREAK
             .
      
          ! (p_Statement_Info, p:DID, p_Release_Status_Str)
          ! (<*STRING>, ULONG=0, <*STRING>),LONG
          ! p:DID
          !   0   - Delivery and Client records loaded
          !           Client fields   - CLI:Terms, CLI:UpdatedDate, CLI:BalanceCurrent, CLI:Balance30Days, CLI:Balance60Days
          !                             CLI:Balance90Days, CLI:CID, CLI:Status, CLI:Limit
          !           Delivery fields - DEL:ReleasedUID
          !
          !   Returns
          !   0   - None of the Below
          !   1   - Released
      		
          !   2   - Acc. Pre-Paid
          !   3   - 60 Day+ Balance
          !   4   - Account Limit
      		
          !   5   - Acc. On Hold
          !   6   - Acc. Closed
          !   7   - Acc. Dormant
          !
          !   8   - (Payment) Period exceeded
          !
          !   p_Statement_Info - only populated in some circumstances!   Shouldn't be a return option???  27/04/08
      
      		
      		IF VM.Next() ~= LEVEL:Benign
                L_GO:Timer    = 0
      
                ENABLE(?Group2)
                ENABLE(?CancelButton)
                SELECT(?CancelButton)
      
                MESSAGE('Releasing Complete.||Released: ' & Release_.Count_Rel,'Release DIs',ICON:Asterisk)
      
                HIDE(?Progress1)
                BREAK
             .
      
             L_CG:Progress    += 1
             ?Progress1{PROP:Progress}    = L_CG:Progress
      
             CASE Check_Delivery_Release(, DEL:DID)
             OF 1 OROF 0
                ! No need
             OF 2
                IF L_GO:Acc_Pre_Paid = TRUE
                   Release_.ReleaseDI(DEL:DID)
                .
             OF 3
                IF L_GO:Day_60_Balance = TRUE
                   Release_.ReleaseDI(DEL:DID)
                .
             OF 4
                IF L_GO:Account_Limit = TRUE
                   Release_.ReleaseDI(DEL:DID)
             .  .
      
             DISABLE(?OkButton)
             DISABLE(?CancelButton)
          .
      
          Window{PROP:Timer}  = L_GO:Timer
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

ReleaseClass.ReleaseDI           PROCEDURE(ULONG p_DID)

Rel     BYTE

    CODE
    IF DEL:ReleasedUID = 0
       IF L_GO:Confirm = TRUE
          CASE MESSAGE('Would you like to release this DI?||DI No. ' & DEL:DINo, 'Release DI', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
          OF BUTTON:Yes
             Rel    = TRUE
          .
       ELSE
          Rel       = TRUE
       .

	   IF Rel = TRUE
		  DEL:DID	= p_DID
		  IF Access:Deliveries.TryFetch(DEL:PKey_DID) ~= LEVEL:Benign
			 ! Record failures?
				
		  ELSE
             DEL:ReleasedUID   = GLO:UID

             IF Access:Deliveries.TryUpdate() = LEVEL:Benign
                SELF.Count_Rel += 1
    .  .  .  .
    RETURN
ReleaseClass.Set_Filter             PROCEDURE()
    CODE
    ! (p:From, p:To, p:Option, p:Heading)
    VM.AddSortOrder(DEL:Key_DINo)

    VM.SetFilter('DEL:ReleasedUID = 0','ikbrel1')

    IF L_GO:From_Date > 0
       VM.SetFilter('DEL:DIDate >= ' & L_GO:From_Date,'ikbrel2')
    .
    IF L_GO:To_Date > 0
       VM.SetFilter('DEL:DIDate < ' & L_GO:To_Date + 1,'ikbrel21')
    .


    IF L_GO:Manifested = 1
       VM.SetFilter('DEL:Manifested <> 0','ikbrel3')
    ELSIF L_GO:Manifested = 2
       VM.SetFilter('DEL:Manifested = 0','ikbrel3')
    ELSE
       VM.SetFilter('','ikbrel3')
    .

    IF L_GO:CID ~= 0
       VM.SetFilter('DEL:CID = ' & L_GO:CID,'ikbrel4')
    ELSE
       VM.SetFilter('','ikbrel4')
    .

    VM.Reset()
    RETURN
ReleaseClass.SQL                      PROCEDURE()

!    Next_Q                  PROCEDURE   (<STRING p_Rec>, <ULONG p:Pos>),ULONG
!    Free_Q                  PROCEDURE   ()
!    Get_El                  PROCEDURE   (LONG p_No),STRING

S_      STRING(1024)

    CODE
    SETCURSOR(CURSOR:WAIT)

    SELF.Q_.Init(GLO:DBOwner, '_SQLTemp2')

    S_              = 'SELECT COUNT(*) FROM Deliveries WHERE ReleasedUID = 0 '

    IF L_GO:From_Date > 0
       ! Returns SQL Date / Time string - (p:Date, p:Time, p:Encompass)
       S_           = CLIP(S_) & ' AND DIDateAndTime >= ' & SQL_Get_DateT_G(L_GO:From_Date)
    .
    IF L_GO:To_Date > 0
       S_           = CLIP(S_) & ' AND DIDateAndTime < ' & SQL_Get_DateT_G(L_GO:To_Date + 1)
    .

    ! DEL:Manifested - Not Manifested|Partially Manifested|Partially Manifested Multiple|Fully Manifested|Fully Manifested Multiple
    !
    ! All|Manifested|Not Manifested
    IF L_GO:Manifested = 1
       S_           = CLIP(S_) & ' AND Manifested <> 0'
    ELSIF L_GO:Manifested = 2
       S_           = CLIP(S_) & ' AND Manifested = 0'
    .

    IF L_GO:CID ~= 0
       S_           = CLIP(S_) & ' AND CID = ' & L_GO:CID
    .

    SELF.Q_.PropSQL( CLIP(S_) )
    SELF.Q_.Next_Q()

    !L_CG:Estimated  = SELF.Q_.Get_El(1)        - gives link error?
    L_CG:Estimated  = SELF.Q_.Data_G.F1
    SETCURSOR()
    RETURN
ReleaseClass.Construct              PROCEDURE()
    CODE
    BIND('DEL:DIDate', DEL:DIDate)
    BIND('DEL:ReleasedUID', DEL:ReleasedUID)
    BIND('DEL:Manifested', DEL:Manifested)
    BIND('DEL:CID', DEL:CID)

    SELF.Q_      &= NEW(SQLQueryClass)
  
    VM.Init(View_, Relate:Deliveries)
    RETURN


ReleaseClass.Destruct               PROCEDURE()
    CODE
    VM.Kill()
    DISPOSE(SELF.Q_)

    UNBIND('DEL:DIDate')
    UNBIND('DEL:ReleasedUID')
    UNBIND('DEL:Manifested')
    UNBIND('DEL:CID')
    RETURN
