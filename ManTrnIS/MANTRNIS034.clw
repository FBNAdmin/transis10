

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS034.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_TripSheetDeliveries PROCEDURE (p:Del_Date_Time)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Screen_Vars      GROUP,PRE(L_SV)                       !
Reg_Make_Model       STRING(100)                           !Registration & Make / Model
Capacity             DECIMAL(6)                            !In Kgs
Loaded_Capacity      DECIMAL(6)                            !In Kgs
Remaining_Capacity   DECIMAL(6)                            !In Kgs
No_Items             USHORT                                !Number of units
Reload_Deliveries    ULONG                                 !
Last_DIID_Set        ULONG                                 !
                     END                                   !
LOC:Item_Loaded_Group GROUP,PRE(L_ILG)                     !
DINo                 ULONG                                 !Delivery Instruction Number
DIDate               DATE                                  !DI Date
Journey              STRING(70)                            !Description
ItemNo               USHORT                                !Item Number
Commodity            STRING(35)                            !Commodity
                     END                                   !
LOC:Item_Desc        STRING(150)                           !
LOC:Config_Vars      GROUP,PRE(L_CV)                       !
FID                  ULONG                                 !Floor ID
DID                  ULONG                                 !Delivery ID
DIID                 ULONG                                 !Delivery Item ID
DID_Loaded_Del       ULONG                                 !DID of selected loaded Delivery Item
                     END                                   !
L_DG:Terms           STRING(20)                            !Terms
L_DG:Delivered       STRING(20)                            !Delivered Status - this has maintenance function - Not Delivered, Partially Delivered, Delivered
L_IG:Item_Desc       STRING(150)                           !
L_IG:Item_Weight     DECIMAL(8,2)                          !In kg's
L_IG:Item_Units      USHORT                                !Number of units
LOC:State            BYTE                                  !State of this Trip Sheet
LOC:Del_Date_Time    GROUP,PRE(L_DT)                       !
Date                 LONG                                  !
Time                 LONG                                  !
                     END                                   !
BRW2::View:Browse    VIEW(TripSheetDeliveriesAlias)
                       PROJECT(A_TRDI:TRID)
                       PROJECT(A_TRDI:UnitsLoaded)
                       PROJECT(A_TRDI:UnitsDelivered)
                       PROJECT(A_TRDI:UnitsNotAccepted)
                       PROJECT(A_TRDI:DeliveredDate)
                       PROJECT(A_TRDI:DeliveredTime)
                       PROJECT(A_TRDI:DIID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
A_TRDI:TRID            LIKE(A_TRDI:TRID)              !List box control field - type derived from field
A_TRDI:UnitsLoaded     LIKE(A_TRDI:UnitsLoaded)       !List box control field - type derived from field
A_TRDI:UnitsDelivered  LIKE(A_TRDI:UnitsDelivered)    !List box control field - type derived from field
A_TRDI:UnitsNotAccepted LIKE(A_TRDI:UnitsNotAccepted) !List box control field - type derived from field
A_TRDI:DeliveredDate   LIKE(A_TRDI:DeliveredDate)     !List box control field - type derived from field
A_TRDI:DeliveredTime   LIKE(A_TRDI:DeliveredTime)     !List box control field - type derived from field
A_TRDI:DIID            LIKE(A_TRDI:DIID)              !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::TRDI:Record LIKE(TRDI:RECORD),THREAD
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Trip Sheet Deliveries'),AT(,,368,273),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateManifestLoad'),SYSTEM
                       SHEET,AT(4,4,360,252),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab2)
                           STRING(''),AT(245,6,117,10),USE(?String_State),RIGHT(1)
                           PROMPT('DI Date:'),AT(238,60),USE(?DEL:DIDate:Prompt)
                           PROMPT('Type:'),AT(238,74),USE(?DELI:Type:Prompt)
                           PROMPT('Total Weight:'),AT(238,88),USE(?DELI:Weight:Prompt)
                           PROMPT('Total Volume:'),AT(238,102),USE(?DELI:Volume:Prompt)
                           PROMPT('Volumetric Weight:'),AT(238,116),USE(?DELI:VolumetricWeight:Prompt)
                           PROMPT('Total Units:'),AT(238,144),USE(?DELI:Units:Prompt)
                           PROMPT('Vehicle Composition:'),AT(9,22),USE(?L_SV:Reg_Make_Model:Prompt:2),TRN
                           ENTRY(@s100),AT(85,22,121,10),USE(L_SV:Reg_Make_Model),COLOR(00E9E9E9h),MSG('Registrati' & |
  'on & Make / Model'),READONLY,SKIP,TIP('Registration & Make / Model')
                           PROMPT('Capacity:'),AT(238,22),USE(?Capacity:Prompt:2),TRN
                           ENTRY(@n-8.0),AT(309,22,50,10),USE(L_SV:Capacity),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                           PROMPT('Remaining Capacity:'),AT(238,36),USE(?Remaining_Capacity:Prompt:2),TRN
                           ENTRY(@n-8.0),AT(309,36,50,10),USE(L_SV:Remaining_Capacity),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                           LINE,AT(10,54,348,0),USE(?Line2),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('DI No.:'),AT(9,60),USE(?DEL:DINo:Prompt),TRN
                           PROMPT('Client Name:'),AT(9,88),USE(?CLI:ClientName:Prompt),TRN
                           PROMPT('Journey:'),AT(9,102),USE(?JOU:Journey:Prompt),TRN
                           PROMPT('Item No.:'),AT(9,74),USE(?DELI:ItemNo:Prompt),TRN
                           GROUP,AT(85,60,274,94),USE(?Group2),COLOR(00E9E9E9h),SKIP
                             ENTRY(@n_10),AT(85,60,50,10),USE(DEL:DINo),RIGHT(1),MSG('Delivery Instruction Number'),READONLY, |
  SKIP,TIP('Delivery Instruction Number')
                             ENTRY(@d6),AT(309,60,50,10),USE(DEL:DIDate),RIGHT(1),MSG('DI Date'),READONLY,SKIP,TIP('DI Date')
                             ENTRY(@n6),AT(85,73,50,10),USE(DELI:ItemNo),RIGHT(1),MSG('Item Number'),READONLY,SKIP,TIP('Item Number')
                             LIST,AT(309,73,50,10),USE(DELI:Type),DISABLE,DROP(5),FROM('Container|#0|Loose|#1'),MSG('Type of Item'), |
  SKIP,TIP('Type of Item')
                             ENTRY(@s35),AT(85,88,121,10),USE(CLI:ClientName),READONLY,SKIP,TIP('Client name')
                             ENTRY(@n-11.2),AT(309,88,50,10),USE(DELI:Weight),RIGHT(1),MSG('In kg''s'),READONLY,SKIP,TIP('In kg''s')
                             ENTRY(@s70),AT(85,102,121,10),USE(JOU:Journey),MSG('Description'),READONLY,SKIP,TIP('Description')
                             ENTRY(@n-11.2),AT(309,102,50,10),USE(DELI:Volume),RIGHT(1),MSG('Volume for manual entry'), |
  READONLY,SKIP,TIP('Volume for manual entry (metres cubed)')
                             ENTRY(@n-11.2),AT(309,116,50,10),USE(DELI:VolumetricWeight),RIGHT(1),MSG('Weight based ' & |
  'on Volumetric calculation'),READONLY,SKIP,TIP('Weight based on Volumetric calculatio' & |
  'n (in kgs)')
                             ENTRY(@s35),AT(85,129,121,10),USE(PACK:Packaging),READONLY,SKIP
                             ENTRY(@s35),AT(85,144,121,10),USE(COM:Commodity),MSG('Commodity'),READONLY,SKIP,TIP('Commodity')
                             ENTRY(@n6),AT(309,144,50,10),USE(DELI:Units),RIGHT(1),MSG('Number of units'),READONLY,SKIP, |
  TIP('Number of units')
                           END
                           PROMPT('Packaging:'),AT(9,130),USE(?PACK:Packaging:Prompt),TRN
                           PROMPT('Commodity:'),AT(9,144),USE(?COM:Commodity:Prompt),TRN
                           LINE,AT(10,159,348,0),USE(?Line2:2),COLOR(COLOR:Black),LINEWIDTH(2)
                           GROUP,AT(238,184,120,30),USE(?Group_Delivered)
                             PROMPT('Delivered Date:'),AT(238,186),USE(?TRDI:DeliverdDate:Prompt),TRN
                             SPIN(@d6),AT(298,186,60,10),USE(TRDI:DeliveredDate),RIGHT(1),MSG('Date delivered'),TIP('Date delivered')
                             PROMPT('Delivered Time:'),AT(238,204),USE(?TRDI:DeliverdTime:Prompt),TRN
                             ENTRY(@t7),AT(298,204,60,10),USE(TRDI:DeliveredTime),RIGHT(1),MSG('Time delivered'),TIP('Time delivered')
                           END
                           PROMPT('Notes:'),AT(9,220),USE(?TRDI:Notes:Prompt),TRN
                           TEXT,AT(85,220,274,30),USE(TRDI:Notes),VSCROLL,BOXED,MSG('Notes'),TIP('Notes')
                           GROUP('group3'),AT(79,163),USE(?GROUP_UnitsDel_LEFT),TRN
                             PROMPT('Units Loaded:'),AT(9,166),USE(?TRDI:UnitsLoaded:Prompt),TRN
                             SPIN(@n6),AT(85,166,50,10),USE(TRDI:UnitsLoaded),RIGHT(1),COLOR(00E9E9E9h),MSG('Number of units'), |
  READONLY,SKIP,TIP('Number of units')
                             PROMPT('Units Delivered:'),AT(9,186),USE(?TRDI:UnitsDelivered:Prompt),TRN
                             SPIN(@n6),AT(85,186,50,10),USE(TRDI:UnitsDelivered),RIGHT(1)
                             CHECK(' Clear &Signature'),AT(150,186),USE(TRDI:ClearSignature),MSG('Does the POD have ' & |
  'a clear signature'),TIP('Does the POD have a clear signature'),TRN
                             PROMPT('Units Not Accepted:'),AT(9,204),USE(?TRDI:UnitsNotAccepted:Prompt),TRN
                             ENTRY(@n6),AT(85,204,50,10),USE(TRDI:UnitsNotAccepted),RIGHT(1),COLOR(00E9E9E9h),MSG('Units that' & |
  ' were not accepted for delivery'),READONLY,SKIP,TIP('Units that were not accepted fo' & |
  'r delivery')
                           END
                         END
                         TAB('&2) All Trips for this Item'),USE(?Tab3)
                           LIST,AT(7,20,353,120),USE(?List),VSCROLL,FORMAT('40R(1)|M~TRID~L(2)@N_10@[36R(1)|M~Load' & |
  'ed~L(2)@n6@38R(1)|M~Delivered~L(2)@n6@28R(1)|M~Not Accepted~L(2)@n6@](127)|M~Units~[' & |
  '48R(1)|M~Date~L(2)@d6b@36R(1)|M~Time~L(2)@t7b@](219)|M~Delivered~'),FROM(Queue:Browse), |
  IMM,MSG('Browsing Records')
                         END
                       END
                       PROMPT('TRID:'),AT(4,262),USE(?TRDI:TRID:Prompt)
                       STRING(@N_10),AT(26,262),USE(TRDI:TRID),RIGHT(1)
                       BUTTON('&OK'),AT(264,258,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(316,258,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(154,258,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       PROMPT('TDID:'),AT(74,262),USE(?TRDI:TDID:Prompt)
                       STRING(@n_10),AT(96,262),USE(TRDI:TDID),RIGHT(1)
                     END

BRW2::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW2                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Remaining_Capacity                  ROUTINE
    ! Get weight less this TDID

    L_SV:Loaded_Capacity       = Get_TripDelItems_Info(TRDI:TRID, 0,, TRDI:TDID) / 100

    ! Add this TDID weight on
    L_SV:Loaded_Capacity      += DELI:Weight * (TRDI:UnitsLoaded / DELI:Units)

    !  Loading|Loaded|On Route|Transferred|Finalised
    !   0       1       2           3           4
    IF LOC:State <= 0
       IF L_SV:Capacity - L_SV:Loaded_Capacity < 0.0
          CASE MESSAGE('Loading Capacity has been exceeded.||If you would like to overload this vehicle continue, otherwise, please reduce the load.||Continue?', 'Loading Capacity', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
          OF BUTTON:No
             SELECT(?TRDI:UnitsLoaded)
    .  .  .

    L_SV:Remaining_Capacity    = L_SV:Capacity - L_SV:Loaded_Capacity

    DISPLAY(?L_SV:Remaining_Capacity)
    EXIT
! ---------------------------------------------------------------------
Maint_Del                                   ROUTINE
    CASE LOC:State              ! Loading|Loaded|On Route|Transferred
    OF 0                        ! Loading
    OF 1                        ! Loaded
    OF 2 OROF 3                 ! On Route / Transferred
       IF TRDI:UnitsDelivered > TRDI:UnitsLoaded
          TRDI:UnitsDelivered  = TRDI:UnitsLoaded
       .

       TRDI:UnitsNotAccepted   = TRDI:UnitsLoaded - TRDI:UnitsDelivered
    .
    DISPLAY
    EXIT
Check_Parameters                     ROUTINE
    IF ~OMITTED(1)
       LOC:Del_Date_Time    = p:Del_Date_Time

       TRDI:DeliveredDate   = L_DT:Date
       TRDI:DeliveredTime   = L_DT:Time  + (100 * 60 * 60)

       ! Assume this also means that we are capturing delivered no. etc.
       TRDI:ClearSignature  = TRUE              ! Default this
    .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Trip Sheet Del. Record'
  OF InsertRecord
    ActionMessage = 'Trip Sheet Del. Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Trip Sheet Del. Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_TripSheetDeliveries')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String_State
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:TripSheetDeliveries)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(TRDI:Record,History::TRDI:Record)
  SELF.AddHistoryField(?TRDI:DeliveredDate,8)
  SELF.AddHistoryField(?TRDI:DeliveredTime,9)
  SELF.AddHistoryField(?TRDI:Notes,13)
  SELF.AddHistoryField(?TRDI:UnitsLoaded,4)
  SELF.AddHistoryField(?TRDI:UnitsDelivered,10)
  SELF.AddHistoryField(?TRDI:ClearSignature,12)
  SELF.AddHistoryField(?TRDI:UnitsNotAccepted,11)
  SELF.AddHistoryField(?TRDI:TRID,2)
  SELF.AddHistoryField(?TRDI:TDID,1)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Deliveries.Open                          ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Relate:Shortages_Damages.Open                   ! File Shortages_Damages used by this procedure, so make sure it's RelationManager is open
  Relate:TripSheetDeliveriesAlias.Open            ! File TripSheetDeliveriesAlias used by this procedure, so make sure it's RelationManager is open
  Access:TripSheets.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleComposition.UseFile               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TripSheetDeliveries
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:TripSheetDeliveriesAlias,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  IF TRI:TRID ~= TRDI:TRID
     TRI:TRID = TRDI:TRID
     IF Access:TripSheets.TryFetch(TRI:PKey_TID) = Level:Benign
  .  .
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    ?L_SV:Reg_Make_Model{PROP:ReadOnly} = True
    ?DEL:DINo{PROP:ReadOnly} = True
    DISABLE(?DELI:Type)
    ?CLI:ClientName{PROP:ReadOnly} = True
    ?DELI:Weight{PROP:ReadOnly} = True
    ?JOU:Journey{PROP:ReadOnly} = True
    ?DELI:Volume{PROP:ReadOnly} = True
    ?DELI:VolumetricWeight{PROP:ReadOnly} = True
    ?PACK:Packaging{PROP:ReadOnly} = True
    ?COM:Commodity{PROP:ReadOnly} = True
    ?TRDI:DeliveredTime{PROP:ReadOnly} = True
  END
    IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
       DISABLE(?TRDI:UnitsDelivered)
       DISABLE(?TRDI:ClearSignature)
       DISABLE(?Group_Delivered)
       DISABLE(?TRDI:Notes)
    .
  
  
  
  QuickWindow{PROP:MinWidth} = 368                ! Restrict the minimum window width
  QuickWindow{PROP:MinHeight} = 273               ! Restrict the minimum window height
  QuickWindow{PROP:MaxWidth} = QuickWindow{PROP:Width} ! Restrict the maximum window width to the 'design time' width
  QuickWindow{PROP:MaxHeight} = 273               ! Restrict the maximum window height
  Resizer.Init(AppStrategy:Surface)               ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,A_TRDI:FKey_DIID)            ! Add the sort order for A_TRDI:FKey_DIID for sort order 1
  BRW2.AddRange(A_TRDI:DIID,TRDI:DIID)            ! Add single value range limit for sort order 1
  BRW2.AddLocator(BRW2::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW2::Sort0:Locator.Init(,A_TRDI:DIID,1,BRW2)   ! Initialize the browse locator using  using key: A_TRDI:FKey_DIID , A_TRDI:DIID
  BRW2.AddField(A_TRDI:TRID,BRW2.Q.A_TRDI:TRID)   ! Field A_TRDI:TRID is a hot field or requires assignment from browse
  BRW2.AddField(A_TRDI:UnitsLoaded,BRW2.Q.A_TRDI:UnitsLoaded) ! Field A_TRDI:UnitsLoaded is a hot field or requires assignment from browse
  BRW2.AddField(A_TRDI:UnitsDelivered,BRW2.Q.A_TRDI:UnitsDelivered) ! Field A_TRDI:UnitsDelivered is a hot field or requires assignment from browse
  BRW2.AddField(A_TRDI:UnitsNotAccepted,BRW2.Q.A_TRDI:UnitsNotAccepted) ! Field A_TRDI:UnitsNotAccepted is a hot field or requires assignment from browse
  BRW2.AddField(A_TRDI:DeliveredDate,BRW2.Q.A_TRDI:DeliveredDate) ! Field A_TRDI:DeliveredDate is a hot field or requires assignment from browse
  BRW2.AddField(A_TRDI:DeliveredTime,BRW2.Q.A_TRDI:DeliveredTime) ! Field A_TRDI:DeliveredTime is a hot field or requires assignment from browse
  BRW2.AddField(A_TRDI:DIID,BRW2.Q.A_TRDI:DIID)   ! Field A_TRDI:DIID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_TripSheetDeliveries',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      VCO:VCID                 = TRI:VCID
      IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
         L_SV:Reg_Make_Model   = VCO:CompositionName
  
         L_SV:Capacity         = Get_VehComp_Info(TRI:VCID, 1)
      .
  
  
      IF SELF.Request ~= ViewRecord
         LOC:State                               = TRI:State
           
           db.Debugout('loc;state: ' & LOC:State)
  
         ?TRDI:UnitsDelivered{PROP:Background}   = 0E9E9E9H
         ?TRDI:UnitsDelivered{PROP:Readonly}     = TRUE
         ?TRDI:UnitsDelivered{PROP:Skip}         = TRUE
         ?TRDI:UnitsDelivered{PROP:RangeHigh}    = TRDI:UnitsDelivered       ! set so cannot alter
         ?TRDI:UnitsDelivered{PROP:RangeLow}     = TRDI:UnitsDelivered
  
         DISABLE(?Group_Delivered)
  
         ?TRDI:UnitsLoaded{PROP:Step}            = 0
         !RANGE(1,1),STEP(1)
  
         CASE LOC:State              ! Loading|Loaded|On Route|Transferred|Finalised
         OF 0                        ! Loading
            ?String_State{PROP:Text}                 = 'Loading'
  
            ENABLE(?TRDI:UnitsLoaded)
            ?TRDI:UnitsLoaded{PROP:Background}       = -1
            ?TRDI:UnitsLoaded{PROP:Skip}             = FALSE
            ?TRDI:UnitsLoaded{PROP:Readonly}         = FALSE
            ?TRDI:UnitsLoaded{PROP:Step}             = 1
  
            ! Get all units Delivered, Rejected and uncomplete trips
            ! Note: The assumption is that there will be only 1 trip per DIID!!?????
            ?TRDI:UnitsLoaded{PROP:Rangehigh}        = DELI:Units - Get_TripItem_Del_Units(TRDI:DIID, 3) + TRDI:UnitsLoaded
            ?TRDI:UnitsLoaded{PROP:Rangelow}         = 0
  
     !  db.debugout('Range High: ' & DELI:Units - Get_TripItem_Del_Units(TRDI:DIID, 3) + TRDI:UnitsLoaded)
         OF 1                        ! Loaded
            ?String_State{PROP:Text}                 = 'Loaded'
         OF 2                        ! On Route
            ?String_State{PROP:Text}                 = 'On Route'
  
            ?TRDI:UnitsDelivered{PROP:Background}    = -1
            ?TRDI:UnitsDelivered{PROP:Readonly}      = FALSE
            ?TRDI:UnitsDelivered{PROP:Skip}          = FALSE
            ?TRDI:UnitsDelivered{PROP:RangeHigh}     = TRDI:UnitsLoaded
            ?TRDI:UnitsDelivered{PROP:RangeLow}      = 0
            ENABLE(?Group_Delivered)
         OF 3                        ! Transferred
            ?String_State{PROP:Text}                 = 'Transferred'
  
            ?TRDI:UnitsDelivered{PROP:Background}    = -1
            ?TRDI:UnitsDelivered{PROP:Readonly}      = FALSE
            ?TRDI:UnitsDelivered{PROP:Skip}          = FALSE
            ?TRDI:UnitsDelivered{PROP:RangeHigh}     = TRDI:UnitsLoaded
            ?TRDI:UnitsDelivered{PROP:RangeLow}      = 0
            ENABLE(?Group_Delivered)
         OF 4                        ! Finalised
            ?String_State{PROP:Text}                 = 'Finalised'
      .  .
      DO Check_Parameters
  BRW2.AddToolbarTarget(Toolbar)                  ! Browse accepts toolbar control
  BRW2.ToolbarItem.HelpButton = ?Help
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_TripSheetDeliveries',1,?List,2,BRW2::PopupTextExt,Queue:Browse,6,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
    Relate:Shortages_Damages.Close
    Relate:TripSheetDeliveriesAlias.Close
  END
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_TripSheetDeliveries',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  TRDI:TRID = TRI:TRID
  PARENT.PrimeFields


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  DELI:DIID = TRDI:DIID                                    ! Assign linking field value
  Access:DeliveryItems.Fetch(DELI:PKey_DIID)
  DEL:DID = DELI:DID                                       ! Assign linking field value
  Access:Deliveries.Fetch(DEL:PKey_DID)
  CLI:CID = DEL:CID                                        ! Assign linking field value
  Access:Clients.Fetch(CLI:PKey_CID)
  JOU:JID = DEL:JID                                        ! Assign linking field value
  Access:Journeys.Fetch(JOU:PKey_JID)
  PACK:PTID = DELI:PTID                                    ! Assign linking field value
  Access:PackagingTypes.Fetch(PACK:PKey_PTID)
  COM:CMID = DELI:CMID                                     ! Assign linking field value
  Access:Commodities.Fetch(COM:PKey_CMID)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?TRDI:UnitsLoaded
          DO Remaining_Capacity
    OF ?TRDI:UnitsDelivered
          DO Maint_Del
    OF ?TRDI:UnitsNotAccepted
          DO Maint_Del
    OF ?OK
      ThisWindow.Update()
          IF TRDI:UnitsLoaded <= 0
             CASE MESSAGE('You have 0 units loaded.||Would you like to continue?', 'Trip Sheet Deliveries', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No)
             OF BUTTON:No
                QuickWindow{PROP:AcceptAll}       = FALSE
                CYCLE
          .  .
      
          IF LOC:State = 3              ! Loading|Loaded|On Route|Transferred|Finalised
             IF TRDI:UnitsDelivered <= 0
                CASE MESSAGE('Are you sure that all units were not accepted?', 'Delivered Information', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
                OF BUTTON:No
                   SELECT(?TRDI:UnitsDelivered)
                   QuickWindow{PROP:AcceptAll}     = FALSE
                   CYCLE
             .  .
      
             IF TRDI:UnitsDelivered > 0 OR TRDI:UnitsNotAccepted > 0
                IF TRDI:DeliveredDate = 0
                   MESSAGE('You have specified that some units have been Delivered or Returned, please set the Delivered Date and Time.', 'Delivered Information', ICON:Exclamation)
                   SELECT(?TRDI:DeliveredDate)
                   QuickWindow{PROP:AcceptAll}     = FALSE
                   CYCLE
          .  .  .
      
      
          IF TRDI:ClearSignature = TRUE
             IF TRDI:UnitsDelivered ~= TRDI:UnitsLoaded OR TRDI:UnitsNotAccepted > 0
                CASE MESSAGE('You have some units not delivered or not accepted - is Clear Signature supposed to be on?', 'Trip Sheet Deliveries', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No)
                OF BUTTON:No
                   SELECT(?TRDI:ClearSignature)
                   QuickWindow{PROP:AcceptAll}       = FALSE
                   CYCLE
          .  .  .
      
      
          DELI:DIID   = TRDI:DIID
          IF Access:DeliveryItems.TryFetch(DELI:PKey_DIID) = LEVEL:Benign
             Upd_Delivery_Del_Status(DELI:DID)
      
             ! Check if we need to request that the user complete a Shortages & Damages report
             IF LOC:State >= 3             ! Loading|Loaded|On Route|Transferred|Finalised
                IF TRDI:ClearSignature = FALSE
                   ! Create a Shortages & Damages report
                   ! Check if we have one already
                   SHO:DID   = DELI:DID
                   IF Access:Shortages_Damages.TryFetch(SHO:FKey_DID) = LEVEL:Benign
                      ! We have one
                   ELSE
                      CLEAR(SHO:Record)
                      IF Access:Shortages_Damages.PrimeRecord() = LEVEL:Benign
                         SHO:DID  = DELI:DID
                         IF Access:Shortages_Damages.Insert() = LEVEL:Benign
                            GlobalRequest = ChangeRecord
                            Update_Shortages_Damages(1)
                         ELSE
                            Access:Shortages_Damages.CancelAutoInc()
                         .
                      ELSE
                         MESSAGE('Could not create a Shortages & Damages report!', 'Update_TripSheetDeliveries', ICON:Hand)
          .  .  .  .  .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?TRDI:UnitsLoaded
          DO Remaining_Capacity
    OF ?TRDI:UnitsDelivered
          DO Maint_Del
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          DO Remaining_Capacity
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?GROUP_UnitsDel_LEFT, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?GROUP_UnitsDel_LEFT
  SELF.SetStrategy(?TRDI:Notes, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight) ! Override strategy for ?TRDI:Notes
  SELF.SetStrategy(?Group_Delivered, Resize:FixRight+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Delivered
  SELF.SetStrategy(?TRDI:ClearSignature, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?TRDI:ClearSignature


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END

