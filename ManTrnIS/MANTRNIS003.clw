

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS003.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Transporter PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
Accountant1          STRING(35)                            !Accountants Name
LOC:Vehicles_Group   GROUP,PRE()                           !
L_VG:MakeModel_0     STRING(20)                            !
L_VG:MakeModel_1     STRING(50)                            !Make & Model
L_VG:MakeModel_2     STRING(50)                            !Make & Model
L_VG:MakeModel_3     STRING(50)                            !Make & Model
L_VG:Registration1   STRING(20)                            !
L_VG:Registration2   STRING(20)                            !
L_VG:Registration3   STRING(20)                            !
L_VG:Registration4   STRING(20)                            !
DriverName           STRING(70)                            !First Name
                     END                                   !
LOC:AddName          STRING(35)                            !Name of this address
LOC:BranchName       STRING(35)                            !Branch Name
LOC:Truck_Type       STRING(20)                            !
LOC:TruckTrailer_Composition STRING(35)                    !
LOC:Linked_Client    STRING(100)                           !
LOC:Invoice_Browse   GROUP,PRE()                           !
LO_IB:Total          DECIMAL(10,2)                         !
LO_IB:Payments       DECIMAL(12,2)                         !
LO_IB:Credits        DECIMAL(12,2)                         !
LO_IB:Outstanding    DECIMAL(12,2)                         !
                     END                                   !
LOC:Statement_Info   GROUP,PRE(L_SI)                       !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
                     END                                   !
LOC:Invoice_Filter_Group GROUP,PRE(L_FG)                   !
Status               BYTE                                  !used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid
LOC:DateGroup        GROUP,PRE()                           !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
BranchName           STRING(35)                            !Branch Name
BID                  ULONG                                 !Branch ID
MID                  ULONG                                 !Manifest ID
MID_SortOrder        LONG                                  !
Time_1st             BYTE                                  !
TIN                  ULONG                                 !Transporter Invoice No.
                     END                                   !
LOC:Invoice_Buffer   USHORT                                !
LOC:Transporter_Contacts STRING(200)                       !
LOC:VAT_Option_Group GROUP,PRE(L_VG)                       !
VAT_Option_User      BYTE                                  !User has set the VAT option
Previous_VATNo       STRING(20)                            !VAT No.
                     END                                   !
BRW2::View:Browse    VIEW(__RatesTransporter)
                       PROJECT(TRRA:MinimiumLoad)
                       PROJECT(TRRA:BaseCharge)
                       PROJECT(TRRA:PerRate)
                       PROJECT(TRRA:TID)
                       PROJECT(TRRA:TRID)
                       PROJECT(TRRA:VCID)
                       PROJECT(TRRA:JID)
                       JOIN(VCO:PKey_VCID,TRRA:VCID)
                         PROJECT(VCO:CompositionName)
                         PROJECT(VCO:VCID)
                         PROJECT(VCO:TTID0)
                         JOIN(TruckTrailer,'TRU:TTID = VCO:TTID0')
                         END
                       END
                       JOIN(JOU:PKey_JID,TRRA:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
VCO:CompositionName    LIKE(VCO:CompositionName)      !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
TRRA:MinimiumLoad      LIKE(TRRA:MinimiumLoad)        !List box control field - type derived from field
TRRA:BaseCharge        LIKE(TRRA:BaseCharge)          !List box control field - type derived from field
TRRA:PerRate           LIKE(TRRA:PerRate)             !List box control field - type derived from field
TRRA:TID               LIKE(TRRA:TID)                 !List box control field - type derived from field
TRRA:TRID              LIKE(TRRA:TRID)                !Primary key field - type derived from field
VCO:VCID               LIKE(VCO:VCID)                 !Related join file key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW22::View:Browse   VIEW(_InvoiceTransporter)
                       PROJECT(INT:TIN)
                       PROJECT(INT:CIN)
                       PROJECT(INT:InvoiceDate)
                       PROJECT(INT:MID)
                       PROJECT(INT:Cost)
                       PROJECT(INT:VAT)
                       PROJECT(INT:VATRate)
                       PROJECT(INT:Comment)
                       PROJECT(INT:CreatedDate)
                       PROJECT(INT:CreatedTime)
                       PROJECT(INT:ExtraInv)
                       PROJECT(INT:CR_TIN)
                       PROJECT(INT:Rate)
                       PROJECT(INT:Status)
                       PROJECT(INT:BID)
                       PROJECT(INT:TID)
                       PROJECT(INT:UID)
                       JOIN(MAN:PKey_MID,INT:MID)
                         PROJECT(MAN:MID)
                       END
                       JOIN(USE:PKey_UID,INT:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:UID)
                       END
                       JOIN(BRA:PKey_BID,INT:BID)
                         PROJECT(BRA:BranchName)
                         PROJECT(BRA:BID)
                       END
                     END
Queue:Browse_Invoices QUEUE                           !Queue declaration for browse/combo box using ?List_Invoices
INT:TIN                LIKE(INT:TIN)                  !List box control field - type derived from field
INT:CIN                LIKE(INT:CIN)                  !List box control field - type derived from field
INT:InvoiceDate        LIKE(INT:InvoiceDate)          !List box control field - type derived from field
INT:MID                LIKE(INT:MID)                  !List box control field - type derived from field
LO_IB:Outstanding      LIKE(LO_IB:Outstanding)        !List box control field - type derived from local data
LO_IB:Total            LIKE(LO_IB:Total)              !List box control field - type derived from local data
LO_IB:Payments         LIKE(LO_IB:Payments)           !List box control field - type derived from local data
LO_IB:Credits          LIKE(LO_IB:Credits)            !List box control field - type derived from local data
INT:Cost               LIKE(INT:Cost)                 !List box control field - type derived from field
INT:VAT                LIKE(INT:VAT)                  !List box control field - type derived from field
INT:VATRate            LIKE(INT:VATRate)              !List box control field - type derived from field
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
INT:Comment            LIKE(INT:Comment)              !List box control field - type derived from field
INT:CreatedDate        LIKE(INT:CreatedDate)          !List box control field - type derived from field
INT:CreatedTime        LIKE(INT:CreatedTime)          !List box control field - type derived from field
INT:ExtraInv           LIKE(INT:ExtraInv)             !List box control field - type derived from field
INT:ExtraInv_Icon      LONG                           !Entry's icon ID
INT:CR_TIN             LIKE(INT:CR_TIN)               !List box control field - type derived from field
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
INT:Rate               LIKE(INT:Rate)                 !List box control field - type derived from field
INT:Status             LIKE(INT:Status)               !Browse hot field - type derived from field
INT:BID                LIKE(INT:BID)                  !Browse hot field - type derived from field
INT:TID                LIKE(INT:TID)                  !Browse key field - type derived from field
MAN:MID                LIKE(MAN:MID)                  !Related join file key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW23::View:Browse   VIEW(TransporterPayments)
                       PROJECT(TRAP:TPID)
                       PROJECT(TRAP:DateCaptured)
                       PROJECT(TRAP:TimeCaptured)
                       PROJECT(TRAP:DateMade)
                       PROJECT(TRAP:TimeMade)
                       PROJECT(TRAP:Amount)
                       PROJECT(TRAP:Notes)
                       PROJECT(TRAP:TID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
TRAP:TPID              LIKE(TRAP:TPID)                !List box control field - type derived from field
TRAP:DateCaptured      LIKE(TRAP:DateCaptured)        !List box control field - type derived from field
TRAP:TimeCaptured      LIKE(TRAP:TimeCaptured)        !List box control field - type derived from field
TRAP:DateMade          LIKE(TRAP:DateMade)            !List box control field - type derived from field
TRAP:TimeMade          LIKE(TRAP:TimeMade)            !List box control field - type derived from field
TRAP:Amount            LIKE(TRAP:Amount)              !List box control field - type derived from field
TRAP:Notes             LIKE(TRAP:Notes)               !List box control field - type derived from field
TRAP:TID               LIKE(TRAP:TID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(VehicleComposition)
                       PROJECT(VCO:CompositionName)
                       PROJECT(VCO:TID)
                       PROJECT(VCO:TTID0)
                       PROJECT(VCO:TTID1)
                       PROJECT(VCO:TTID2)
                       PROJECT(VCO:TTID3)
                       PROJECT(VCO:VCID)
                       JOIN(TRU:PKey_TTID,VCO:TTID0)
                         PROJECT(TRU:DRID)
                         PROJECT(TRU:TTID)
                       END
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
VCO:CompositionName    LIKE(VCO:CompositionName)      !List box control field - type derived from field
DriverName             LIKE(DriverName)               !List box control field - type derived from local data
L_VG:MakeModel_0       LIKE(L_VG:MakeModel_0)         !List box control field - type derived from local data
L_VG:MakeModel_1       LIKE(L_VG:MakeModel_1)         !List box control field - type derived from local data
L_VG:MakeModel_2       LIKE(L_VG:MakeModel_2)         !List box control field - type derived from local data
L_VG:MakeModel_3       LIKE(L_VG:MakeModel_3)         !List box control field - type derived from local data
L_VG:Registration1     LIKE(L_VG:Registration1)       !List box control field - type derived from local data
L_VG:Registration2     LIKE(L_VG:Registration2)       !List box control field - type derived from local data
L_VG:Registration3     LIKE(L_VG:Registration3)       !List box control field - type derived from local data
L_VG:Registration4     LIKE(L_VG:Registration4)       !List box control field - type derived from local data
VCO:TID                LIKE(VCO:TID)                  !List box control field - type derived from field
VCO:TTID0              LIKE(VCO:TTID0)                !Browse hot field - type derived from field
VCO:TTID1              LIKE(VCO:TTID1)                !Browse hot field - type derived from field
VCO:TTID2              LIKE(VCO:TTID2)                !Browse hot field - type derived from field
VCO:TTID3              LIKE(VCO:TTID3)                !Browse hot field - type derived from field
TRU:DRID               LIKE(TRU:DRID)                 !Browse hot field - type derived from field
VCO:VCID               LIKE(VCO:VCID)                 !Primary key field - type derived from field
TRU:TTID               LIKE(TRU:TTID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(TruckTrailer)
                       PROJECT(TRU:Registration)
                       PROJECT(TRU:Capacity)
                       PROJECT(TRU:Type)
                       PROJECT(TRU:TID)
                       PROJECT(TRU:TTID)
                       PROJECT(TRU:VMMID)
                       JOIN(VMM:PKey_VMMID,TRU:VMMID)
                         PROJECT(VMM:MakeModel)
                         PROJECT(VMM:VMMID)
                       END
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?Browse:6
TRU:Registration       LIKE(TRU:Registration)         !List box control field - type derived from field
LOC:TruckTrailer_Composition LIKE(LOC:TruckTrailer_Composition) !List box control field - type derived from local data
VMM:MakeModel          LIKE(VMM:MakeModel)            !List box control field - type derived from field
LOC:Truck_Type         LIKE(LOC:Truck_Type)           !List box control field - type derived from local data
TRU:Capacity           LIKE(TRU:Capacity)             !List box control field - type derived from field
TRU:Type               LIKE(TRU:Type)                 !Browse hot field - type derived from field
TRU:TID                LIKE(TRU:TID)                  !Browse hot field - type derived from field
TRU:TTID               LIKE(TRU:TTID)                 !Primary key field - type derived from field
VMM:VMMID              LIKE(VMM:VMMID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(Manifest)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:Rate)
                       PROJECT(MAN:VATRate)
                       PROJECT(MAN:State)
                       PROJECT(MAN:DepartDate)
                       PROJECT(MAN:DepartTime)
                       PROJECT(MAN:ETADate)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:TID)
                       PROJECT(MAN:MID)
                       JOIN(BRA:PKey_BID,MAN:BID)
                         PROJECT(BRA:BranchName)
                         PROJECT(BRA:BID)
                       END
                     END
Queue:Browse:8       QUEUE                            !Queue declaration for browse/combo box using ?Browse:8
MAN:Cost               LIKE(MAN:Cost)                 !List box control field - type derived from field
MAN:Rate               LIKE(MAN:Rate)                 !List box control field - type derived from field
MAN:VATRate            LIKE(MAN:VATRate)              !List box control field - type derived from field
MAN:State              LIKE(MAN:State)                !List box control field - type derived from field
MAN:DepartDate         LIKE(MAN:DepartDate)           !List box control field - type derived from field
MAN:DepartTime         LIKE(MAN:DepartTime)           !List box control field - type derived from field
MAN:ETADate            LIKE(MAN:ETADate)              !List box control field - type derived from field
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
MAN:BID                LIKE(MAN:BID)                  !List box control field - type derived from field
MAN:TID                LIKE(MAN:TID)                  !List box control field - type derived from field
MAN:MID                LIKE(MAN:MID)                  !Primary key field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(DeliveryLegs)
                       PROJECT(DELL:Leg)
                       PROJECT(DELL:Cost)
                       PROJECT(DELL:DLID)
                       PROJECT(DELL:TID)
                       PROJECT(DELL:DID)
                       PROJECT(DELL:JID)
                       JOIN(DEL:PKey_DID,DELL:DID)
                         PROJECT(DEL:DINo)
                         PROJECT(DEL:DIDate)
                         PROJECT(DEL:DID)
                       END
                       JOIN(JOU:PKey_JID,DELL:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                     END
Queue:Browse:10      QUEUE                            !Queue declaration for browse/combo box using ?Browse:10
DELL:Leg               LIKE(DELL:Leg)                 !List box control field - type derived from field
DELL:Cost              LIKE(DELL:Cost)                !List box control field - type derived from field
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:DIDate             LIKE(DEL:DIDate)               !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
DELL:DLID              LIKE(DELL:DLID)                !Primary key field - type derived from field
DELL:TID               LIKE(DELL:TID)                 !Browse key field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Related join file key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB19::View:FileDrop VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
FDB26::View:FileDrop VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?LOC:BranchName
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:InvBranch QUEUE                        !Queue declaration for browse/combo box using ?L_FG:BranchName
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::TRA:Record  LIKE(TRA:RECORD),THREAD
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW22::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW22::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW22::PopupChoice   SIGNED                       ! Popup current choice
BRW22::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW22::PopupChoiceExec BYTE(0)                    ! Popup executed
BRW23::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW23::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW23::PopupChoice   SIGNED                       ! Popup current choice
BRW23::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW23::PopupChoiceExec BYTE(0)                    ! Popup executed
BRW4::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW4::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW4::PopupChoice    SIGNED                       ! Popup current choice
BRW4::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW4::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW6::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW6::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW6::PopupChoice    SIGNED                       ! Popup current choice
BRW6::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW6::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW8::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW8::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW8::PopupChoice    SIGNED                       ! Popup current choice
BRW8::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW8::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW10::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW10::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW10::PopupChoice   SIGNED                       ! Popup current choice
BRW10::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW10::PopupChoiceExec BYTE(0)                    ! Popup executed
QuickWindow          WINDOW('Form Transporter'),AT(,,493,329),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdateTransporter'),SYSTEM
                       SHEET,AT(4,4,483,307),USE(?CurrentTab),JOIN
                         TAB('&1) General'),USE(?Tab:1)
                           GROUP,AT(9,25,337,280),USE(?Group1),TRN
                             PROMPT('TID:'),AT(237,25),USE(?TRA:TID:Prompt),TRN
                             STRING(@n_10),AT(257,25,,10),USE(TRA:TID),RIGHT(1),TRN
                             PROMPT('ACID:'),AT(237,95),USE(?TRA:ACID:Prompt),TRN
                             STRING(@n_10b),AT(257,95,44,10),USE(TRA:ACID),RIGHT(1),TRN
                             PROMPT('BID:'),AT(169,180),USE(?TRA:BID:Prompt),TRN
                             STRING(@n_10),AT(181,180,44,10),USE(TRA:BID),RIGHT(1),TRN
                             PROMPT('Transporter Name:'),AT(9,25),USE(?TRA:TransporterName:Prompt),TRN
                             ENTRY(@s35),AT(81,25,144,10),USE(TRA:TransporterName),MSG('Transporters Name'),REQ,TIP('Transporters Name')
                             PROMPT('Address Name:'),AT(9,44),USE(?Name:Prompt),TRN
                             BUTTON('...'),AT(61,44,12,10),USE(?CallLookup:2)
                             ENTRY(@s35),AT(81,44,144,10),USE(LOC:AddName),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                             PROMPT('Address'),AT(81,57,373,10),USE(?Prompt_AddCont),TRN
                             PROMPT('Contacts:'),AT(9,71),USE(?LOC:Transporter_Contacts:Prompt),TRN
                             ENTRY(@s200),AT(81,71,373,10),USE(LOC:Transporter_Contacts),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP
                             PROMPT('Accountant:'),AT(9,95),USE(?Accountant:Prompt),TRN
                             BUTTON('...'),AT(61,95,12,10),USE(?CallLookup)
                             ENTRY(@s35),AT(81,95,144,10),USE(Accountant1),MSG('Accountants Name'),TIP('Accountants Name')
                             PROMPT('Operations Manager:'),AT(9,114),USE(?TRA:OpsManager:Prompt),TRN
                             ENTRY(@s35),AT(81,114,144,10),USE(TRA:OpsManager)
                             PROMPT('VAT No:'),AT(9,127),USE(?TRA:VATNo:Prompt),TRN
                             ENTRY(@s20),AT(81,127,84,10),USE(TRA:VATNo),MSG('VAT No.'),TIP('VAT No.')
                             CHECK(' Charges &VAT'),AT(81,140),USE(TRA:ChargesVAT),MSG('This Transporter charges / pays VAT'), |
  TIP('This Transporter charges / pays VAT'),TRN
                             PROMPT('Linked Client:'),AT(9,156),USE(?LOC:Linked_Client:Prompt),TRN
                             BUTTON('...'),AT(61,156,12,10),USE(?CallLookup:3)
                             ENTRY(@s100),AT(81,156,144,10),USE(LOC:Linked_Client),TIP('Client name')
                             PROMPT('Branch:'),AT(9,180),USE(?Prompt9),TRN
                             LIST,AT(81,180,84,10),USE(LOC:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                             CHECK(' Broking'),AT(81,196),USE(TRA:Broking),MSG('This is a broking transporter'),TIP('This is a ' & |
  'broking transporter'),TRN
                             CHECK(' Archived'),AT(81,295),USE(TRA:Archived)
                             PROMPT('Archived Date:'),AT(149,295),USE(?TRA:Archived_Date:Prompt)
                             ENTRY(@d6b),AT(202,295,60,10),USE(TRA:Archived_Date),RIGHT(1),READONLY,SKIP
                             ENTRY(@t4),AT(269,295,46,10),USE(TRA:Archived_Time),RIGHT(1),READONLY,SKIP
                             PROMPT('Comments:'),AT(9,218),USE(?TRA:Comment:Prompt)
                             TEXT,AT(81,218,253,40),USE(TRA:Comments),VSCROLL
                             PROMPT('Status:'),AT(9,267),USE(?TRA:Status:Prompt)
                             LIST,AT(81,268,84,10),USE(TRA:Status),DROP(3,100),FROM('Normal|#0|Pending|#1|Do Not Use|#2'), |
  MSG('Normal, Pending, Do Not Use'),TIP('Normal, Pending, Do Not Use')
                           END
                           GROUP,AT(348,100,132,93),USE(?Group_Right)
                             BUTTON('Display'),AT(427,103,52,10),USE(?Button_Calc_Age_Analysis),TIP('Calculate the A' & |
  'ges Analysis with respect to today')
                             PROMPT('Current:'),AT(392,116),USE(?L_SI:Current:Prompt),TRN
                             ENTRY(@n-14.2),AT(427,116,52,10),USE(L_SI:Current),RIGHT(1),COLOR(00E9E9E9h),MSG('Current'), |
  READONLY,SKIP,TIP('Current')
                             PROMPT('30 Days:'),AT(391,132),USE(?L_SI:Days30:Prompt),TRN
                             ENTRY(@n-14.2),AT(427,132,52,10),USE(L_SI:Days30),RIGHT(1),COLOR(00E9E9E9h),MSG('30 Das'), |
  READONLY,SKIP,TIP('30 Das')
                             PROMPT('60 Days:'),AT(391,146),USE(?L_SI:Days60:Prompt),TRN
                             ENTRY(@n-14.2),AT(427,146,52,10),USE(L_SI:Days60),RIGHT(1),COLOR(00E9E9E9h),MSG('60 Days'), |
  READONLY,SKIP,TIP('60 Days')
                             PROMPT('90 Days:'),AT(391,159),USE(?L_SI:Days90:Prompt),TRN
                             ENTRY(@n-14.2),AT(427,159,52,10),USE(L_SI:Days90),RIGHT(1),COLOR(00E9E9E9h),MSG('90 Days'), |
  READONLY,SKIP,TIP('90 Days')
                             PROMPT('Total:'),AT(400,175),USE(?L_SI:Total:Prompt),TRN
                             ENTRY(@n-14.2),AT(427,175,52,10),USE(L_SI:Total),RIGHT(1),COLOR(00E9E9E9h),MSG('Total'),READONLY, |
  SKIP,TIP('Total')
                           END
                           BUTTON('&Save'),AT(5,313,49,14),USE(?Save),LEFT,KEY(CtrlAltEnter),ICON('SAVE.ICO'),FLAT,TIP('Save Recor' & |
  'd and Close')
                         END
                         TAB('&2) Rates'),USE(?Tab:2)
                           LIST,AT(8,23,473,265),USE(?Browse:2),HVSCROLL,FORMAT('70L(2)|M~Vehicle Composition~C(0)' & |
  '@s35@70L(2)|M~Journey~C(0)@s70@54R(2)|M~Minimium Load~C(0)@n-11.0@48R(1)|M~Base Char' & |
  'ge~C(0)@n-13.2@43R(1)|M~Per Rate~C(0)@n-10.2@40R(2)|M~TID~C(0)@n_10@'),FROM(Queue:Browse:2), |
  IMM,MSG('Browsing the DeliveryLegs file')
                           BUTTON('&Insert'),AT(325,292,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(379,292,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(432,292,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&3) Vehicle Compositions'),USE(?Tab:3)
                           LIST,AT(8,23,473,265),USE(?Browse:4),HVSCROLL,FORMAT('130L(2)|M~Vehicle Composition~@s3' & |
  '5@46L(2)|M~Driver Name~@s35@60L(2)|M~Make Model~@s20@60L(2)|M~Make & Model 2~@s50@60' & |
  'L(2)|M~Make & Model 3~@s50@60L(2)|M~Make & Model 3~@s50@50L(2)|M~Registration 1~@s20' & |
  '@50L(2)|M~Registration 2~@s20@50L(2)|M~Registration 3~@s20@50L(2)|M~Registration 4~@' & |
  's20@80R(2)|M~TID~C(0)@n_10@'),FROM(Queue:Browse:4),IMM
                           BUTTON('&Insert'),AT(327,292,49,14),USE(?Insert:5),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(380,292,49,14),USE(?Change:5),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(432,292,49,14),USE(?Delete:5),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&4) Truck / Trailer'),USE(?Tab:4)
                           LIST,AT(8,23,473,264),USE(?Browse:6),HVSCROLL,FORMAT('50L(2)|M~Registration~@s20@65L(2)' & |
  '|M~Composition~@s35@54L(2)|M~Make & Model~@s35@36L(2)|M~Type~@s20@32D(2)|M~Capacity~L@n-8.0@'), |
  FROM(Queue:Browse:6),IMM,MSG('Browsing the DeliveryLegs file')
                           BUTTON('&Insert'),AT(327,292,49,14),USE(?Insert:7),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(380,292,49,14),USE(?Change:7),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(432,292,49,14),USE(?Delete:7),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&5) Manifests'),USE(?Tab:5)
                           LIST,AT(8,24,473,263),USE(?Browse:8),HVSCROLL,FORMAT('64R(1)|M~Cost~C(0)@n-14.2@48R(1)|' & |
  'M~Rate~C(0)@n-10.2@36R(1)|M~VAT Rate~C(0)@n-7.2@24R(2)|M~State~C(0)@n3@[48R(2)|M~Dat' & |
  'e~C(0)@d6@36R(2)|M~Time~C(0)@t7@]|M~Depart~46R(2)|M~ETA Date~C(0)@d6@60L(1)|M~Branch' & |
  '~C(0)@s35@30R(2)|M~BID~C(0)@n_10@30R(2)|M~TID~C(0)@n_10@'),FROM(Queue:Browse:8),IMM,MSG('Browsing t' & |
  'he DeliveryLegs file')
                           BUTTON('&Insert'),AT(327,292,49,14),USE(?Insert:9),LEFT,ICON('WAINSERT.ICO'),FLAT,HIDE,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(380,292,49,14),USE(?Change:9),LEFT,ICON('WACHANGE.ICO'),FLAT,HIDE,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(432,292,49,14),USE(?Delete:9),LEFT,ICON('WADELETE.ICO'),FLAT,HIDE,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&6) Delivery Legs'),USE(?Tab:6)
                           LIST,AT(8,23,473,264),USE(?Browse:10),HVSCROLL,FORMAT('28R(2)|M~Leg~C(0)@n6@50R(1)|M~Co' & |
  'st~C(0)@n-13.2@40R(1)|M~DI No.~C(0)@n_10@38R(1)|M~DI Date~C(0)@d5b@100L(1)|M~Journey' & |
  '~C(0)@s70@'),FROM(Queue:Browse:10),IMM,MSG('Browsing the DeliveryLegs file')
                           BUTTON('&Insert'),AT(327,292,49,14),USE(?Insert:11),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(380,292,49,14),USE(?Change:11),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(432,292,49,14),USE(?Delete:11),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&7) Invoices'),USE(?Tab7)
                           GROUP,AT(123,294,170,10),USE(?Group_InvBrw_Bot2)
                             PROMPT('Find TIN:'),AT(122,294),USE(?L_FG:TIN:Prompt),TRN
                             ENTRY(@n_10),AT(155,294,44,10),USE(L_FG:TIN),RIGHT(1),MSG('Transporter Invoice No.'),TIP('Transporte' & |
  'r Invoice No.<0DH,0AH>Clear (set to 0) to reset the list')
                             PROMPT('Find MID:'),AT(206,294),USE(?L_FG:MID:Prompt),TRN
                             ENTRY(@n_10),AT(242,294,44,10),USE(L_FG:MID),RIGHT(1),MSG('Manifest ID'),TIP('Manifest I' & |
  'D<0DH,0AH>Clear (set to 0) to reset the list')
                           END
                           GROUP,AT(4,313,275,14),USE(?Group_Inv)
                             BUTTON('Limit to Date Range'),AT(4,313,,14),USE(?Button_DateRange)
                             PROMPT('Status:'),AT(84,314),USE(?L_FG:Status:Prompt)
                             LIST,AT(108,314,65,10),USE(L_FG:Status),VSCROLL,DROP(15),FROM('All|#255|No Payments|#0|' & |
  'Partially Paid|#1|Credit Note|#2|Fully Paid|#3'),TIP('No Payments, Partially Paid, C' & |
  'redit Note, Fully Paid')
                             PROMPT('Branch:'),AT(185,314),USE(?L_FG:Status:Prompt:2)
                             LIST,AT(213,314,65,10),USE(L_FG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch N' & |
  'ame~@s35@40R(2)|M~BID~L@n_10@'),FROM(Queue:FileDrop:InvBranch)
                           END
                           LIST,AT(8,23,472,263),USE(?List_Invoices),HVSCROLL,FORMAT('40R(1)|M~TIN~L(2)@n_10@44L(1' & |
  ')|M~Creditor Inv.~L(2)@s30@[36R(1)|M~Date~C(0)@d5b@]|M~Invoice~40R(1)|M~MID~L(2)@n_1' & |
  '0b@[44R(1)|M~Outstanding~L(2)@n-17.2@44R(1)|M~Total~L(2)@n-14.2@44R(1)|M~Payments~L(' & |
  '2)@n-17.2@44R(1)|M~Credits~L(2)@n-17.2@44R(1)|M~Cost~L(2)@n-14.2@34R(1)|M~VAT~L(2)@n' & |
  '-14.2@36R(1)|M~VAT Rate~L(2)@n-7.2@]|M~Charges~50L(1)|M~Branch~L(2)@s35@150L(1)|M~Co' & |
  'mment~L(2)@s255@[38R(1)|M~Date~L(2)@d5b@36R(1)|M~Time~L(2)@t7@]|M~Created~22L(1)|MI~' & |
  'Extra Inv~L(2)@p p@40R(1)|M~Related TIN~L(2)@n_10b@40L(1)|M~Login~L(2)@s20@50R(1)|M~' & |
  'Rate~L(2)@n-13.4@'),FROM(Queue:Browse_Invoices),IMM,MSG('Browsing Records')
                           BUTTON('Create Credit / Invoice'),AT(10,291,,14),USE(?Button_Credit_Invoice),LEFT,ICON('CLEAR.ICO'), |
  FLAT
                           BUTTON('&Insert'),AT(326,291,49,14),USE(?Insert),LEFT,ICON('WAinsert.ICO'),FLAT
                           BUTTON('&Change'),AT(379,291,49,14),USE(?Change),LEFT,ICON('WAchange.ICO'),FLAT
                           BUTTON('&Delete'),AT(431,291,49,14),USE(?Delete),LEFT,ICON('WADELETE.ICO'),FLAT
                         END
                         TAB('&8) Payments'),USE(?Tab8)
                           LIST,AT(8,23,473,264),USE(?List:2),HVSCROLL,FORMAT('40R(1)|M~TP ID~L(2)@n_10@[40R(1)|M~' & |
  'Date~L(2)@d5b@36R(1)|M~Time~L(2)@t7@]|M~Captured~[40R(1)|M~Date~L(2)@d5@36R(1)|M~Tim' & |
  'e~L(2)@t7@]|M~Made~52R(1)|M~Paid Amount~L(2)@n-14.2@100L(1)|M~Notes~L(2)@s255@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(327,292,49,14),USE(?Insert:2),LEFT,ICON('WAinsert.ICO'),FLAT
                           BUTTON('&Change'),AT(379,292,49,14),USE(?Change:2),LEFT,ICON('WAchange.ICO'),FLAT
                           BUTTON('&Delete'),AT(432,292,49,14),USE(?Delete:2),LEFT,ICON('WAdelete.ICO'),FLAT
                         END
                       END
                       BUTTON('&OK'),AT(384,313,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(438,313,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                     END

BRW2::LastSortOrder       BYTE
BRW22::LastSortOrder       BYTE
BRW23::LastSortOrder       BYTE
BRW4::LastSortOrder       BYTE
BRW6::LastSortOrder       BYTE
BRW8::LastSortOrder       BYTE
BRW10::LastSortOrder       BYTE
BRW22::SortHeader  CLASS(SortHeaderClassType) !Declare SortHeader Class
QueueResorted          PROCEDURE(STRING pString),VIRTUAL
                  END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW_Invoices         CLASS(BrowseClass)                    ! Browse using ?List_Invoices
Q                      &Queue:Browse_Invoices         !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW22::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW23                CLASS(BrowseClass)                    ! Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW23::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW6                 CLASS(BrowseClass)                    ! Browse using ?Browse:6
Q                      &Queue:Browse:6                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW8                 CLASS(BrowseClass)                    ! Browse using ?Browse:8
Q                      &Queue:Browse:8                !Reference to browse queue
SetAlerts              PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW10                CLASS(BrowseClass)                    ! Browse using ?Browse:10
Q                      &Queue:Browse:10               !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW10::Sort0:StepClass StepRealClass                       ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB19                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB26                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:InvBranch      !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_Address                          ROUTINE
    ?Prompt_AddCont{PROP:Text}  = ''
    ADD:AID                     = TRA:AID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       LOC:AddName              = ADD:AddressName

       SUBU:SUID                = ADD:SUID
       IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) ~= LEVEL:Benign
          CLEAR(SUBU:Record)
       .

       ?Prompt_AddCont{PROP:Text} = CLIP(ADD:AddressName) & ', ' & CLIP(ADD:Line1) & ', ' & CLIP(ADD:Line2) & ', ' & CLIP(SUBU:Suburb) & ', ' & CLIP(SUBU:PostalCode)
    .

    DO Contact_Info

    EXIT
Contact_Info                            ROUTINE
    CLEAR(LOC:Transporter_Contacts)
    ADD:AID                 = TRA:AID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       ! (p:Add, p:List, p:Delim)
       Add_to_List(CLIP(ADD:PhoneNo), LOC:Transporter_Contacts, ',',, 'P: ')
       Add_to_List(CLIP(ADD:PhoneNo2), LOC:Transporter_Contacts, ',',, '  P2: ')
       Add_to_List(CLIP(ADD:Fax), LOC:Transporter_Contacts, ',',, '  F: ')
    .

    DISPLAY(?LOC:Transporter_Contacts)
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Transport Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Transport Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Transporter')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRA:TID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_FG:Status',L_FG:Status)                 ! Added by: BrowseBox(ABC)
  BIND('L_FG:From_Date',L_FG:From_Date)           ! Added by: BrowseBox(ABC)
  BIND('L_FG:To_Date',L_FG:To_Date)               ! Added by: BrowseBox(ABC)
  BIND('L_FG:BID',L_FG:BID)                       ! Added by: BrowseBox(ABC)
  BIND('LO_IB:Outstanding',LO_IB:Outstanding)     ! Added by: BrowseBox(ABC)
  BIND('LO_IB:Total',LO_IB:Total)                 ! Added by: BrowseBox(ABC)
  BIND('LO_IB:Payments',LO_IB:Payments)           ! Added by: BrowseBox(ABC)
  BIND('LO_IB:Credits',LO_IB:Credits)             ! Added by: BrowseBox(ABC)
  BIND('DriverName',DriverName)                   ! Added by: BrowseBox(ABC)
  BIND('L_VG:MakeModel_0',L_VG:MakeModel_0)       ! Added by: BrowseBox(ABC)
  BIND('L_VG:MakeModel_1',L_VG:MakeModel_1)       ! Added by: BrowseBox(ABC)
  BIND('L_VG:MakeModel_2',L_VG:MakeModel_2)       ! Added by: BrowseBox(ABC)
  BIND('L_VG:MakeModel_3',L_VG:MakeModel_3)       ! Added by: BrowseBox(ABC)
  BIND('L_VG:Registration1',L_VG:Registration1)   ! Added by: BrowseBox(ABC)
  BIND('L_VG:Registration2',L_VG:Registration2)   ! Added by: BrowseBox(ABC)
  BIND('L_VG:Registration3',L_VG:Registration3)   ! Added by: BrowseBox(ABC)
  BIND('L_VG:Registration4',L_VG:Registration4)   ! Added by: BrowseBox(ABC)
  BIND('LOC:TruckTrailer_Composition',LOC:TruckTrailer_Composition) ! Added by: BrowseBox(ABC)
  BIND('LOC:Truck_Type',LOC:Truck_Type)           ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Transporter)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(TRA:Record,History::TRA:Record)
  SELF.AddHistoryField(?TRA:TID,1)
  SELF.AddHistoryField(?TRA:ACID,5)
  SELF.AddHistoryField(?TRA:BID,3)
  SELF.AddHistoryField(?TRA:TransporterName,2)
  SELF.AddHistoryField(?TRA:OpsManager,6)
  SELF.AddHistoryField(?TRA:VATNo,7)
  SELF.AddHistoryField(?TRA:ChargesVAT,9)
  SELF.AddHistoryField(?TRA:Broking,10)
  SELF.AddHistoryField(?TRA:Archived,11)
  SELF.AddHistoryField(?TRA:Archived_Date,14)
  SELF.AddHistoryField(?TRA:Archived_Time,15)
  SELF.AddHistoryField(?TRA:Comments,16)
  SELF.AddHistoryField(?TRA:Status,17)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Accountants.SetOpenRelated()
  Relate:Accountants.Open                         ! File Accountants used by this procedure, so make sure it's RelationManager is open
  Access:Transporter.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailerAlias.UseFile                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Transporter
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.SaveControl = ?Save
  SELF.DisableCancelButton = 1
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:__RatesTransporter,SELF) ! Initialize the browse manager
  BRW_Invoices.Init(?List_Invoices,Queue:Browse_Invoices.ViewPosition,BRW22::View:Browse,Queue:Browse_Invoices,Relate:_InvoiceTransporter,SELF) ! Initialize the browse manager
  BRW23.Init(?List:2,Queue:Browse:1.ViewPosition,BRW23::View:Browse,Queue:Browse:1,Relate:TransporterPayments,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:VehicleComposition,SELF) ! Initialize the browse manager
  BRW6.Init(?Browse:6,Queue:Browse:6.ViewPosition,BRW6::View:Browse,Queue:Browse:6,Relate:TruckTrailer,SELF) ! Initialize the browse manager
  BRW8.Init(?Browse:8,Queue:Browse:8.ViewPosition,BRW8::View:Browse,Queue:Browse:8,Relate:Manifest,SELF) ! Initialize the browse manager
  BRW10.Init(?Browse:10,Queue:Browse:10.ViewPosition,BRW10::View:Browse,Queue:Browse:10,Relate:DeliveryLegs,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    ?TRA:TransporterName{PROP:ReadOnly} = True
    DISABLE(?CallLookup:2)
    ?LOC:AddName{PROP:ReadOnly} = True
    ?LOC:Transporter_Contacts{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?Accountant1{PROP:ReadOnly} = True
    ?TRA:OpsManager{PROP:ReadOnly} = True
    ?TRA:VATNo{PROP:ReadOnly} = True
    DISABLE(?CallLookup:3)
    ?LOC:Linked_Client{PROP:ReadOnly} = True
    DISABLE(?LOC:BranchName)
    ?TRA:Archived_Date{PROP:ReadOnly} = True
    ?TRA:Archived_Time{PROP:ReadOnly} = True
    DISABLE(?TRA:Status)
    DISABLE(?Button_Calc_Age_Analysis)
    ?L_SI:Current{PROP:ReadOnly} = True
    ?L_SI:Days30{PROP:ReadOnly} = True
    ?L_SI:Days60{PROP:ReadOnly} = True
    ?L_SI:Days90{PROP:ReadOnly} = True
    ?L_SI:Total{PROP:ReadOnly} = True
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    DISABLE(?Insert:5)
    DISABLE(?Change:5)
    DISABLE(?Delete:5)
    DISABLE(?Insert:7)
    DISABLE(?Change:7)
    DISABLE(?Delete:7)
    DISABLE(?Insert:9)
    DISABLE(?Change:9)
    DISABLE(?Delete:9)
    DISABLE(?Insert:11)
    DISABLE(?Change:11)
    DISABLE(?Delete:11)
    ?L_FG:TIN{PROP:ReadOnly} = True
    ?L_FG:MID{PROP:ReadOnly} = True
    DISABLE(?Button_DateRange)
    DISABLE(?L_FG:Status)
    DISABLE(?L_FG:BranchName)
    DISABLE(?Button_Credit_Invoice)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
    DISABLE(?Insert:2)
    DISABLE(?Change:2)
    DISABLE(?Delete:2)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon TRRA:TID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,TRRA:FKey_TID) ! Add the sort order for TRRA:FKey_TID for sort order 1
  BRW2.AddRange(TRRA:TID,TRA:TID)                 ! Add single value range limit for sort order 1
  BRW2.AppendOrder('+JOU:Journey,+VCO:CompositionName,+TRRA:TRID') ! Append an additional sort order
  BRW2.AddField(VCO:CompositionName,BRW2.Q.VCO:CompositionName) ! Field VCO:CompositionName is a hot field or requires assignment from browse
  BRW2.AddField(JOU:Journey,BRW2.Q.JOU:Journey)   ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW2.AddField(TRRA:MinimiumLoad,BRW2.Q.TRRA:MinimiumLoad) ! Field TRRA:MinimiumLoad is a hot field or requires assignment from browse
  BRW2.AddField(TRRA:BaseCharge,BRW2.Q.TRRA:BaseCharge) ! Field TRRA:BaseCharge is a hot field or requires assignment from browse
  BRW2.AddField(TRRA:PerRate,BRW2.Q.TRRA:PerRate) ! Field TRRA:PerRate is a hot field or requires assignment from browse
  BRW2.AddField(TRRA:TID,BRW2.Q.TRRA:TID)         ! Field TRRA:TID is a hot field or requires assignment from browse
  BRW2.AddField(TRRA:TRID,BRW2.Q.TRRA:TRID)       ! Field TRRA:TRID is a hot field or requires assignment from browse
  BRW2.AddField(VCO:VCID,BRW2.Q.VCO:VCID)         ! Field VCO:VCID is a hot field or requires assignment from browse
  BRW2.AddField(JOU:JID,BRW2.Q.JOU:JID)           ! Field JOU:JID is a hot field or requires assignment from browse
  BRW_Invoices.Q &= Queue:Browse_Invoices
  BRW_Invoices.AddSortOrder(,INT:FKey_TID)        ! Add the sort order for INT:FKey_TID for sort order 1
  BRW_Invoices.AddRange(INT:TID,TRA:TID)          ! Add single value range limit for sort order 1
  BRW_Invoices.AddLocator(BRW22::Sort0:Locator)   ! Browse has a locator for sort order 1
  BRW22::Sort0:Locator.Init(,INT:TID,1,BRW_Invoices) ! Initialize the browse locator using  using key: INT:FKey_TID , INT:TID
  BRW_Invoices.AppendOrder('+INT:InvoiceDate,+INT:TIN') ! Append an additional sort order
  BRW_Invoices.SetFilter('((L_FG:Status = 255 OR INT:Status = L_FG:Status) AND (0 = L_FG:From_Date OR INT:InvoiceDate >= L_FG:From_Date) AND (0 = L_FG:To_Date OR INT:InvoiceDate << L_FG:To_Date) AND (L_FG:BID =  0 OR L_FG:BID = INT:BID))') ! Apply filter expression to browse
  BRW_Invoices.AddResetField(L_FG:BID)            ! Apply the reset field
  BRW_Invoices.AddResetField(L_FG:From_Date)      ! Apply the reset field
  BRW_Invoices.AddResetField(L_FG:Status)         ! Apply the reset field
  BRW_Invoices.AddResetField(L_FG:To_Date)        ! Apply the reset field
  ?List_Invoices{PROP:IconList,1} = '~checkoffdim.ico'
  ?List_Invoices{PROP:IconList,2} = '~checkon.ico'
  BRW_Invoices.AddField(INT:TIN,BRW_Invoices.Q.INT:TIN) ! Field INT:TIN is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:CIN,BRW_Invoices.Q.INT:CIN) ! Field INT:CIN is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:InvoiceDate,BRW_Invoices.Q.INT:InvoiceDate) ! Field INT:InvoiceDate is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:MID,BRW_Invoices.Q.INT:MID) ! Field INT:MID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(LO_IB:Outstanding,BRW_Invoices.Q.LO_IB:Outstanding) ! Field LO_IB:Outstanding is a hot field or requires assignment from browse
  BRW_Invoices.AddField(LO_IB:Total,BRW_Invoices.Q.LO_IB:Total) ! Field LO_IB:Total is a hot field or requires assignment from browse
  BRW_Invoices.AddField(LO_IB:Payments,BRW_Invoices.Q.LO_IB:Payments) ! Field LO_IB:Payments is a hot field or requires assignment from browse
  BRW_Invoices.AddField(LO_IB:Credits,BRW_Invoices.Q.LO_IB:Credits) ! Field LO_IB:Credits is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:Cost,BRW_Invoices.Q.INT:Cost) ! Field INT:Cost is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:VAT,BRW_Invoices.Q.INT:VAT) ! Field INT:VAT is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:VATRate,BRW_Invoices.Q.INT:VATRate) ! Field INT:VATRate is a hot field or requires assignment from browse
  BRW_Invoices.AddField(BRA:BranchName,BRW_Invoices.Q.BRA:BranchName) ! Field BRA:BranchName is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:Comment,BRW_Invoices.Q.INT:Comment) ! Field INT:Comment is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:CreatedDate,BRW_Invoices.Q.INT:CreatedDate) ! Field INT:CreatedDate is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:CreatedTime,BRW_Invoices.Q.INT:CreatedTime) ! Field INT:CreatedTime is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:ExtraInv,BRW_Invoices.Q.INT:ExtraInv) ! Field INT:ExtraInv is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:CR_TIN,BRW_Invoices.Q.INT:CR_TIN) ! Field INT:CR_TIN is a hot field or requires assignment from browse
  BRW_Invoices.AddField(USE:Login,BRW_Invoices.Q.USE:Login) ! Field USE:Login is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:Rate,BRW_Invoices.Q.INT:Rate) ! Field INT:Rate is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:Status,BRW_Invoices.Q.INT:Status) ! Field INT:Status is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:BID,BRW_Invoices.Q.INT:BID) ! Field INT:BID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INT:TID,BRW_Invoices.Q.INT:TID) ! Field INT:TID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(MAN:MID,BRW_Invoices.Q.MAN:MID) ! Field MAN:MID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(USE:UID,BRW_Invoices.Q.USE:UID) ! Field USE:UID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(BRA:BID,BRW_Invoices.Q.BRA:BID) ! Field BRA:BID is a hot field or requires assignment from browse
  BRW23.Q &= Queue:Browse:1
  BRW23.AddSortOrder(,TRAP:FKey_TID)              ! Add the sort order for TRAP:FKey_TID for sort order 1
  BRW23.AddRange(TRAP:TID,TRA:TID)                ! Add single value range limit for sort order 1
  BRW23.AddLocator(BRW23::Sort0:Locator)          ! Browse has a locator for sort order 1
  BRW23::Sort0:Locator.Init(,TRAP:TID,1,BRW23)    ! Initialize the browse locator using  using key: TRAP:FKey_TID , TRAP:TID
  BRW23.AppendOrder('+TRAP:DateMade,+TRAP:TPID')  ! Append an additional sort order
  BRW23.AddField(TRAP:TPID,BRW23.Q.TRAP:TPID)     ! Field TRAP:TPID is a hot field or requires assignment from browse
  BRW23.AddField(TRAP:DateCaptured,BRW23.Q.TRAP:DateCaptured) ! Field TRAP:DateCaptured is a hot field or requires assignment from browse
  BRW23.AddField(TRAP:TimeCaptured,BRW23.Q.TRAP:TimeCaptured) ! Field TRAP:TimeCaptured is a hot field or requires assignment from browse
  BRW23.AddField(TRAP:DateMade,BRW23.Q.TRAP:DateMade) ! Field TRAP:DateMade is a hot field or requires assignment from browse
  BRW23.AddField(TRAP:TimeMade,BRW23.Q.TRAP:TimeMade) ! Field TRAP:TimeMade is a hot field or requires assignment from browse
  BRW23.AddField(TRAP:Amount,BRW23.Q.TRAP:Amount) ! Field TRAP:Amount is a hot field or requires assignment from browse
  BRW23.AddField(TRAP:Notes,BRW23.Q.TRAP:Notes)   ! Field TRAP:Notes is a hot field or requires assignment from browse
  BRW23.AddField(TRAP:TID,BRW23.Q.TRAP:TID)       ! Field TRAP:TID is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon VCO:TID for sort order 1
  BRW4.AddSortOrder(BRW4::Sort0:StepClass,VCO:FKey_TID) ! Add the sort order for VCO:FKey_TID for sort order 1
  BRW4.AddRange(VCO:TID,TRA:TID)                  ! Add single value range limit for sort order 1
  BRW4.AppendOrder('+VCO:CompositionName,+VCO:VCID') ! Append an additional sort order
  BRW4.AddField(VCO:CompositionName,BRW4.Q.VCO:CompositionName) ! Field VCO:CompositionName is a hot field or requires assignment from browse
  BRW4.AddField(DriverName,BRW4.Q.DriverName)     ! Field DriverName is a hot field or requires assignment from browse
  BRW4.AddField(L_VG:MakeModel_0,BRW4.Q.L_VG:MakeModel_0) ! Field L_VG:MakeModel_0 is a hot field or requires assignment from browse
  BRW4.AddField(L_VG:MakeModel_1,BRW4.Q.L_VG:MakeModel_1) ! Field L_VG:MakeModel_1 is a hot field or requires assignment from browse
  BRW4.AddField(L_VG:MakeModel_2,BRW4.Q.L_VG:MakeModel_2) ! Field L_VG:MakeModel_2 is a hot field or requires assignment from browse
  BRW4.AddField(L_VG:MakeModel_3,BRW4.Q.L_VG:MakeModel_3) ! Field L_VG:MakeModel_3 is a hot field or requires assignment from browse
  BRW4.AddField(L_VG:Registration1,BRW4.Q.L_VG:Registration1) ! Field L_VG:Registration1 is a hot field or requires assignment from browse
  BRW4.AddField(L_VG:Registration2,BRW4.Q.L_VG:Registration2) ! Field L_VG:Registration2 is a hot field or requires assignment from browse
  BRW4.AddField(L_VG:Registration3,BRW4.Q.L_VG:Registration3) ! Field L_VG:Registration3 is a hot field or requires assignment from browse
  BRW4.AddField(L_VG:Registration4,BRW4.Q.L_VG:Registration4) ! Field L_VG:Registration4 is a hot field or requires assignment from browse
  BRW4.AddField(VCO:TID,BRW4.Q.VCO:TID)           ! Field VCO:TID is a hot field or requires assignment from browse
  BRW4.AddField(VCO:TTID0,BRW4.Q.VCO:TTID0)       ! Field VCO:TTID0 is a hot field or requires assignment from browse
  BRW4.AddField(VCO:TTID1,BRW4.Q.VCO:TTID1)       ! Field VCO:TTID1 is a hot field or requires assignment from browse
  BRW4.AddField(VCO:TTID2,BRW4.Q.VCO:TTID2)       ! Field VCO:TTID2 is a hot field or requires assignment from browse
  BRW4.AddField(VCO:TTID3,BRW4.Q.VCO:TTID3)       ! Field VCO:TTID3 is a hot field or requires assignment from browse
  BRW4.AddField(TRU:DRID,BRW4.Q.TRU:DRID)         ! Field TRU:DRID is a hot field or requires assignment from browse
  BRW4.AddField(VCO:VCID,BRW4.Q.VCO:VCID)         ! Field VCO:VCID is a hot field or requires assignment from browse
  BRW4.AddField(TRU:TTID,BRW4.Q.TRU:TTID)         ! Field TRU:TTID is a hot field or requires assignment from browse
  BRW6.Q &= Queue:Browse:6
  BRW6::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon TRU:TID for sort order 1
  BRW6.AddSortOrder(BRW6::Sort0:StepClass,TRU:FKey_TID) ! Add the sort order for TRU:FKey_TID for sort order 1
  BRW6.AddRange(TRU:TID,TRA:TID)                  ! Add single value range limit for sort order 1
  BRW6.AppendOrder('+LOC:TruckTrailer_Composition,+TRU:Registration,+TRU:TTID') ! Append an additional sort order
  BRW6.AddField(TRU:Registration,BRW6.Q.TRU:Registration) ! Field TRU:Registration is a hot field or requires assignment from browse
  BRW6.AddField(LOC:TruckTrailer_Composition,BRW6.Q.LOC:TruckTrailer_Composition) ! Field LOC:TruckTrailer_Composition is a hot field or requires assignment from browse
  BRW6.AddField(VMM:MakeModel,BRW6.Q.VMM:MakeModel) ! Field VMM:MakeModel is a hot field or requires assignment from browse
  BRW6.AddField(LOC:Truck_Type,BRW6.Q.LOC:Truck_Type) ! Field LOC:Truck_Type is a hot field or requires assignment from browse
  BRW6.AddField(TRU:Capacity,BRW6.Q.TRU:Capacity) ! Field TRU:Capacity is a hot field or requires assignment from browse
  BRW6.AddField(TRU:Type,BRW6.Q.TRU:Type)         ! Field TRU:Type is a hot field or requires assignment from browse
  BRW6.AddField(TRU:TID,BRW6.Q.TRU:TID)           ! Field TRU:TID is a hot field or requires assignment from browse
  BRW6.AddField(TRU:TTID,BRW6.Q.TRU:TTID)         ! Field TRU:TTID is a hot field or requires assignment from browse
  BRW6.AddField(VMM:VMMID,BRW6.Q.VMM:VMMID)       ! Field VMM:VMMID is a hot field or requires assignment from browse
  BRW8.Q &= Queue:Browse:8
  BRW8::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon MAN:TID for sort order 1
  BRW8.AddSortOrder(BRW8::Sort0:StepClass,MAN:FKey_TID) ! Add the sort order for MAN:FKey_TID for sort order 1
  BRW8.AddRange(MAN:TID,TRA:TID)                  ! Add single value range limit for sort order 1
  BRW8.AppendOrder('+MAN:MID')                    ! Append an additional sort order
  BRW8.AddField(MAN:Cost,BRW8.Q.MAN:Cost)         ! Field MAN:Cost is a hot field or requires assignment from browse
  BRW8.AddField(MAN:Rate,BRW8.Q.MAN:Rate)         ! Field MAN:Rate is a hot field or requires assignment from browse
  BRW8.AddField(MAN:VATRate,BRW8.Q.MAN:VATRate)   ! Field MAN:VATRate is a hot field or requires assignment from browse
  BRW8.AddField(MAN:State,BRW8.Q.MAN:State)       ! Field MAN:State is a hot field or requires assignment from browse
  BRW8.AddField(MAN:DepartDate,BRW8.Q.MAN:DepartDate) ! Field MAN:DepartDate is a hot field or requires assignment from browse
  BRW8.AddField(MAN:DepartTime,BRW8.Q.MAN:DepartTime) ! Field MAN:DepartTime is a hot field or requires assignment from browse
  BRW8.AddField(MAN:ETADate,BRW8.Q.MAN:ETADate)   ! Field MAN:ETADate is a hot field or requires assignment from browse
  BRW8.AddField(BRA:BranchName,BRW8.Q.BRA:BranchName) ! Field BRA:BranchName is a hot field or requires assignment from browse
  BRW8.AddField(MAN:BID,BRW8.Q.MAN:BID)           ! Field MAN:BID is a hot field or requires assignment from browse
  BRW8.AddField(MAN:TID,BRW8.Q.MAN:TID)           ! Field MAN:TID is a hot field or requires assignment from browse
  BRW8.AddField(MAN:MID,BRW8.Q.MAN:MID)           ! Field MAN:MID is a hot field or requires assignment from browse
  BRW8.AddField(BRA:BID,BRW8.Q.BRA:BID)           ! Field BRA:BID is a hot field or requires assignment from browse
  BRW10.Q &= Queue:Browse:10
  BRW10::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon DELL:TID for sort order 1
  BRW10.AddSortOrder(BRW10::Sort0:StepClass,DELL:FKey_TID) ! Add the sort order for DELL:FKey_TID for sort order 1
  BRW10.AddRange(DELL:TID,TRA:TID)                ! Add single value range limit for sort order 1
  BRW10.AppendOrder('+DEL:DINo,+DELL:Leg,+DELL:DLID') ! Append an additional sort order
  BRW10.AddField(DELL:Leg,BRW10.Q.DELL:Leg)       ! Field DELL:Leg is a hot field or requires assignment from browse
  BRW10.AddField(DELL:Cost,BRW10.Q.DELL:Cost)     ! Field DELL:Cost is a hot field or requires assignment from browse
  BRW10.AddField(DEL:DINo,BRW10.Q.DEL:DINo)       ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW10.AddField(DEL:DIDate,BRW10.Q.DEL:DIDate)   ! Field DEL:DIDate is a hot field or requires assignment from browse
  BRW10.AddField(JOU:Journey,BRW10.Q.JOU:Journey) ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW10.AddField(DELL:DLID,BRW10.Q.DELL:DLID)     ! Field DELL:DLID is a hot field or requires assignment from browse
  BRW10.AddField(DELL:TID,BRW10.Q.DELL:TID)       ! Field DELL:TID is a hot field or requires assignment from browse
  BRW10.AddField(DEL:DID,BRW10.Q.DEL:DID)         ! Field DEL:DID is a hot field or requires assignment from browse
  BRW10.AddField(JOU:JID,BRW10.Q.JOU:JID)         ! Field JOU:JID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_Transporter',QuickWindow)  ! Restore window settings from non-volatile store
      ! Add an order to be used when the user specifies an MID to find...
      L_FG:MID_SortOrder      = BRW_Invoices.AddSortOrder(,INT:Fkey_MID)
      BRW_Invoices.AddRange(INT:MID,L_FG:MID)         ! Add single value range limit for sort order 1
  
      !  BRW_Invoices.SetFilter('((L_FG:Status = 255 OR INT:Status = L_FG:Status) AND (0 = L_FG:From_Date OR INT:InvoiceDate >= L_FG:From_Date) AND (0 = L_FG:To_Date OR INT:InvoiceDate << L_FG:To_Date) AND (L_FG:BID =  0 OR L_FG:BID = INT:BID))')
  
  
  
      BRW_Invoices.AddSortOrder(,INT:PKey_TIN)
      BRW_Invoices.AddRange(INT:TIN,L_FG:TIN)         ! Add single value range limit for sort order 1
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      ACCO:ACID           = TRA:ACID
      IF Access:Accountants.TryFetch(ACCO:PKey_ACID) = LEVEL:Benign
         Accountant1      = ACCO:AccountantName
      .
  
  
      ADD:AID             = TRA:AID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
         LOC:AddName      = ADD:AddressName
      .
  
  
  
      BRA:BID             = TRA:BID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         LOC:BranchName   = BRA:BranchName
      .
  
  
  
      IF TRA:Linked_CID ~= 0
         CLI:CID          = TRA:Linked_CID
         IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
            LOC:Linked_Client = CLI:ClientName
      .  .
  BRW2.AskProcedure = 4
  BRW_Invoices.AskProcedure = 5
  BRW23.AskProcedure = 6
  BRW4.AskProcedure = 7
  BRW6.AskProcedure = 8
  BRW10.AskProcedure = 9
  FDB19.Init(?LOC:BranchName,Queue:FileDrop.ViewPosition,FDB19::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB19.Q &= Queue:FileDrop
  FDB19.AddSortOrder(BRA:Key_BranchName)
  FDB19.AddField(BRA:BranchName,FDB19.Q.BRA:BranchName) !List box control field - type derived from field
  FDB19.AddField(BRA:BID,FDB19.Q.BRA:BID) !Primary key field - type derived from field
  FDB19.AddUpdateField(BRA:BID,TRA:BID)
  ThisWindow.AddItem(FDB19.WindowComponent)
  FDB19.DefaultFill = 0
  FDB26.Init(?L_FG:BranchName,Queue:FileDrop:InvBranch.ViewPosition,FDB26::View:FileDrop,Queue:FileDrop:InvBranch,Relate:Branches,ThisWindow)
  FDB26.Q &= Queue:FileDrop:InvBranch
  FDB26.AddSortOrder(BRA:Key_BranchName)
  FDB26.AddField(BRA:BranchName,FDB26.Q.BRA:BranchName) !List box control field - type derived from field
  FDB26.AddField(BRA:BID,FDB26.Q.BRA:BID) !List box control field - type derived from field
  FDB26.AddUpdateField(BRA:BID,L_FG:BID)
  ThisWindow.AddItem(FDB26.WindowComponent)
  FDB26.DefaultFill = 0
  BRW_Invoices.AddToolbarTarget(Toolbar)          ! Browse accepts toolbar control
  BRW23.AddToolbarTarget(Toolbar)                 ! Browse accepts toolbar control
      IF SELF.Request = InsertRecord
         DISABLE(?Tab:2)
         DISABLE(?Tab:3)
         DISABLE(?Tab:4)
         DISABLE(?Tab:5)
         DISABLE(?Tab:6)
      .
      DO Load_Address
      IF CLIP(TRA:VATNo) ~= ''
         L_VG:VAT_Option_User     = 1
      .
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_Transporter',1,?Browse:2,2,BRW2::PopupTextExt,Queue:Browse:2,6,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW22::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW22::FormatManager.Init('MANTRNIS','Update_Transporter',1,?List_Invoices,22,BRW22::PopupTextExt,Queue:Browse_Invoices,20,LFM_CFile,LFM_CFile.Record)
  BRW22::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW23::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW23::FormatManager.Init('MANTRNIS','Update_Transporter',1,?List:2,23,BRW23::PopupTextExt,Queue:Browse:1,7,LFM_CFile,LFM_CFile.Record)
  BRW23::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW4::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW4::FormatManager.Init('MANTRNIS','Update_Transporter',1,?Browse:4,4,BRW4::PopupTextExt,Queue:Browse:4,11,LFM_CFile,LFM_CFile.Record)
  BRW4::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW6::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW6::FormatManager.Init('MANTRNIS','Update_Transporter',1,?Browse:6,6,BRW6::PopupTextExt,Queue:Browse:6,5,LFM_CFile,LFM_CFile.Record)
  BRW6::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW8::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW8::FormatManager.Init('MANTRNIS','Update_Transporter',1,?Browse:8,8,BRW8::PopupTextExt,Queue:Browse:8,10,LFM_CFile,LFM_CFile.Record)
  BRW8::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW10::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW10::FormatManager.Init('MANTRNIS','Update_Transporter',1,?Browse:10,10,BRW10::PopupTextExt,Queue:Browse:10,5,LFM_CFile,LFM_CFile.Record)
  BRW10::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW22::SortHeader.Init(Queue:Browse_Invoices,?List_Invoices,'','',BRW22::View:Browse,INT:PKey_TIN)
  BRW22::SortHeader.UseSortColors = False
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Accountants.Close
  !Kill the Sort Header
  BRW22::SortHeader.Kill()
  END
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW22::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW23::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW4::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW6::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW8::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW10::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_Transporter',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  TRA:BID = GLO:BranchID
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Addresses(TRA:TransporterName,, TRA:BID)
      Browse_Accountants
      Select_RateMod_Clients
      Update_TransporterRates
      Update_TransporterInvoice
      Update_TransporterPayments
      Update_VehicleComposition
      Update_TruckTrailer
      Update_DeliveryLegs
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW22::SortHeader.SetAlerts()


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Save
          IF CLIP(TRA:TransporterName) = ''
             ! This should be a full record validation.......... ???
             MESSAGE('Please provide a Transporter Name.', 'Save Transporter', ICON:Hand)
             CYCLE
          .
    OF ?Button_DateRange
            L_FG:From_Date  = GETINI('Update_Transporter', 'Inv_From_Date', , GLO:Local_INI)
            L_FG:To_Date    = GETINI('Update_Transporter', 'Inv_To_Date', , GLO:Local_INI)
    OF ?Button_Credit_Invoice
          LOC:Invoice_Buffer      = Access:_InvoiceTransporter.SaveBuffer()
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup:2
      ThisWindow.Update()
      ADD:AddressName = LOC:AddName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:AddName = ADD:AddressName
        TRA:AID = ADD:AID
      END
      ThisWindow.Reset(1)
    OF ?LOC:AddName
      IF LOC:AddName OR ?LOC:AddName{PROP:Req}
        ADD:AddressName = LOC:AddName
        IF Access:Addresses.TryFetch(ADD:Key_Name)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:AddName = ADD:AddressName
            TRA:AID = ADD:AID
          ELSE
            CLEAR(TRA:AID)
            SELECT(?LOC:AddName)
            CYCLE
          END
        ELSE
          TRA:AID = ADD:AID
        END
      END
      ThisWindow.Reset()
          DO Load_Address
    OF ?CallLookup
      ThisWindow.Update()
      ACCO:AccountantName = Accountant1
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        Accountant1 = ACCO:AccountantName
        TRA:ACID = ACCO:ACID
      END
      ThisWindow.Reset(1)
    OF ?Accountant1
      IF Accountant1 OR ?Accountant1{PROP:Req}
        ACCO:AccountantName = Accountant1
        IF Access:Accountants.TryFetch(ACCO:Key_AccountantName)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            Accountant1 = ACCO:AccountantName
            TRA:ACID = ACCO:ACID
          ELSE
            CLEAR(TRA:ACID)
            SELECT(?Accountant1)
            CYCLE
          END
        ELSE
          TRA:ACID = ACCO:ACID
        END
      END
      ThisWindow.Reset()
    OF ?TRA:VATNo
          IF L_VG:VAT_Option_User = 0             ! No VAT no. set, and user hasnt pressed the check box
             IF CLIP(TRA:VATNo) ~= ''
                TRA:ChargesVAT        = TRUE
                DISPLAY(?TRA:ChargesVAT)
          .  .
    OF ?TRA:ChargesVAT
          L_VG:VAT_Option_User    = 1
    OF ?CallLookup:3
      ThisWindow.Update()
      CLI:ClientName = LOC:Linked_Client
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        LOC:Linked_Client = CLI:ClientName
        TRA:Linked_CID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?LOC:Linked_Client
      IF LOC:Linked_Client OR ?LOC:Linked_Client{PROP:Req}
        CLI:ClientName = LOC:Linked_Client
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            LOC:Linked_Client = CLI:ClientName
            TRA:Linked_CID = CLI:CID
          ELSE
            CLEAR(TRA:Linked_CID)
            SELECT(?LOC:Linked_Client)
            CYCLE
          END
        ELSE
          TRA:Linked_CID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?TRA:Archived
      IF TRA:Archived = 1
         TRA:Archived_Date = TODAY()
         TRA:Archived_Time = CLOCK()
         DISPLAY()
      .
    OF ?Button_Calc_Age_Analysis
      ThisWindow.Update()
          SETCURSOR(CURSOR:WAIT)
                                               ! (p:TID, p:Date, p:Owing, p:Statement_Info, p:MonthEndDay, p:OutPut)
          Junk_#      = Gen_Aging_Transporter(TRA:TID, TODAY(), , LOC:Statement_Info)
      
          DISPLAY
          SETCURSOR()
    OF ?Save
      ThisWindow.Update()
          IF SELF.Request ~= InsertRecord
             ENABLE(?Tab:2)
             ENABLE(?Tab:3)
             ENABLE(?Tab:4)
             ENABLE(?Tab:5)
             ENABLE(?Tab:6)
          .
    OF ?L_FG:TIN
          IF L_FG:TIN = 0
             ENABLE(?Group_Inv)
             BRW_Invoices.SetSort(L_FG:MID_SortOrder - 1, 1)
          ELSE
             INT:TIN         = L_FG:TIN
             IF Access:_InvoiceTransporter.TryFetch(INT:PKey_TIN) = LEVEL:Benign
                DISABLE(?Group_Inv)
                BRW_Invoices.SetSort(L_FG:MID_SortOrder + 1, 1)
             ELSE
                ENABLE(?Group_Inv)
                BRW_Invoices.SetSort(L_FG:MID_SortOrder - 1, 1)
                MESSAGE('No Transporter Invoice found for Invoice ID (TIN).', 'Find Invoice ID', ICON:Asterisk)
          .  .
    OF ?L_FG:MID
          IF L_FG:MID = 0
             ENABLE(?Group_Inv)
             BRW_Invoices.SetSort(L_FG:MID_SortOrder - 1, 1)
          ELSE
             INT:MID         = L_FG:MID
             IF Access:_InvoiceTransporter.TryFetch(INT:Fkey_MID) = LEVEL:Benign
                DISABLE(?Group_Inv)
                BRW_Invoices.SetSort(L_FG:MID_SortOrder)
             ELSE
                ENABLE(?Group_Inv)
                BRW_Invoices.SetSort(L_FG:MID_SortOrder - 1, 1)
                MESSAGE('No Transporter Invoice found for this Manifest ID (MID).', 'Find Manifest ID', ICON:Asterisk)
          .  .
    OF ?Button_DateRange
      ThisWindow.Update()
      L_FG:LOC:DateGroup = Ask_Date_Range(L_FG:From_Date, L_FG:To_Date)
      ThisWindow.Reset
          IF L_FG:To_Date ~= 0
             L_FG:To_Date    += 1
          .
      
          IF L_FG:From_Date > 0 AND L_FG:To_Date > 0
             ?Button_DateRange{PROP:Text}    = FORMAT(L_FG:From_Date,@d5) & ' - ' & FORMAT(L_FG:To_Date,@d5)
          ELSIF L_FG:From_Date > 0
             ?Button_DateRange{PROP:Text}    = 'From ' & FORMAT(L_FG:From_Date,@d5)
          ELSIF L_FG:To_Date > 0
             ?Button_DateRange{PROP:Text}    = 'To ' & FORMAT(L_FG:To_Date-1,@d5)
          ELSE
             ?Button_DateRange{PROP:Text}    = 'Limit to Date Range'
          .
      
      
          PUTINI('Update_Transporter', 'Inv_From_Date', L_FG:From_Date, GLO:Local_INI)
          PUTINI('Update_Transporter', 'Inv_To_Date', L_FG:To_Date, GLO:Local_INI)
      
      
          !LOC:Reset       += 1
    OF ?Button_Credit_Invoice
      ThisWindow.Update()
      GlobalRequest = InsertRecord
      Update_TransporterInvoice(INT:TIN, 1)
      ThisWindow.Reset
          Access:_InvoiceTransporter.RestoreBuffer(LOC:Invoice_Buffer)
          BRW_Invoices.ResetFromBuffer()           
      
      !    IF GlobalResponse = RequestCompleted
      !       BRW_Invoices.ResetQueue(Reset:Done)
      !    ELSE
      !       BRW_Invoices.ResetQueue(Reset:Queue)
      !    .
      
          ! Doesnt have the desired result, when a new record is added this should be selected upon return (? maybe not from Marindas point of view)
          ! FBN want the cursor to remain on the record that it was on before calling the update / insert
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Take Sort Headers Events
  IF BRW22::SortHeader.TakeEvents()
     RETURN Level:Notify
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          BRW_Invoices.SetSort(L_FG:MID_SortOrder - 1, 1)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END


BRW_Invoices.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW_Invoices.SetQueueRecord PROCEDURE

  CODE
      LO_IB:Total  = INT:Cost + INT:VAT
  
      ! (p:TIN, p:CR_TIN, p:Status, p_b:Cost_Total, pb:Payments, pb:Credits, pb:Outstanding)
      Get_Transporter_Totals(INT:TIN, INT:CR_TIN, INT:Status, LO_IB:Total, LO_IB:Payments, LO_IB:Credits, LO_IB:Outstanding)
  
  !    ! Invoices may be credited or have payments
  !    LO_IB:Payments       = Get_TransportersPay_Alloc_Amt(INT:TIN)
  !
  !    ! p:Options
  !    !   0.  All
  !    !   1.  Credits
  !    !   2.  Debits
  !    LO_IB:Credits        = -Get_InvTransporter_Credited(INT:TIN,1)       ! Credited is negative
  !
  !    ! Credit notes may have associated invoices or not??
  !    IF INT:CR_TIN ~= 0                                                  ! Should only be for credit notes!
  !       ! No Payments|Partially Paid|Credit Note|Fully Paid
  !       IF INT:Status = 2
  !       ELSE
  !          ! ??? problem, have Credit Note ID but not Credit Note!
  !       .
  !
  !       ! The LO_IB:Credited should be zero as Credit Notes cannot be credited.
  !
  !       LO_IB:Credits    += -Get_InvTransporter_Credited(INT:CR_TIN, 1)   ! This is total credited (and will include this Credit Note)
  !       LO_IB:Payments   += Get_TransportersPay_Alloc_Amt(INT:CR_TIN)
  !
  !       A_INT:TIN        = INT:CR_TIN
  !       IF Access:InvoiceTransporterAlias.TryFetch(A_INT:PKey_TIN) ~= LEVEL:Benign
  !          CLEAR(A_INT:Record)
  !       .
  !
  !       LO_IB:Outstanding = (A_INT:Cost + A_INT:VAT) - (LO_IB:Payments + LO_IB:Credits)
  !    ELSE
  !       LO_IB:Outstanding = LO_IB:Total  - (LO_IB:Payments + LO_IB:Credits)
  !    .
  PARENT.SetQueueRecord
  
  IF (INT:ExtraInv = 1)
    SELF.Q.INT:ExtraInv_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.INT:ExtraInv_Icon = 1                           ! Set icon from icon list
  END


BRW_Invoices.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW22::LastSortOrder<>NewOrder THEN
     BRW22::SortHeader.ClearSort()
  END
  IF BRW22::LastSortOrder <> NewOrder THEN
     BRW22::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW22::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_Invoices.TakeNewSelection PROCEDURE

  CODE
  IF BRW22::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW22::PopupTextExt = ''
        BRW22::PopupChoiceExec = True
        BRW22::FormatManager.MakePopup(BRW22::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW22::PopupTextExt = '|-|' & CLIP(BRW22::PopupTextExt)
        END
        BRW22::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW22::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW22::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW22::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW22::PopupChoiceOn AND BRW22::PopupChoiceExec THEN
     BRW22::PopupChoiceExec = False
     BRW22::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW22::PopupTextExt)
     BRW22::SortHeader.RestoreHeaderText()
     BRW_Invoices.RestoreSort()
     IF BRW22::FormatManager.DispatchChoice(BRW22::PopupChoice)
        BRW22::SortHeader.ResetSort()
     ELSE
        BRW22::SortHeader.SortQueue()
     END
  END


BRW23.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW23.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW23::LastSortOrder <> NewOrder THEN
     BRW23::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW23::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW23.TakeNewSelection PROCEDURE

  CODE
  IF BRW23::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW23::PopupTextExt = ''
        BRW23::PopupChoiceExec = True
        BRW23::FormatManager.MakePopup(BRW23::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW23::PopupTextExt = '|-|' & CLIP(BRW23::PopupTextExt)
        END
        BRW23::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW23::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW23::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW23::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW23::PopupChoiceOn AND BRW23::PopupChoiceExec THEN
     BRW23::PopupChoiceExec = False
     BRW23::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW23::PopupTextExt)
     IF BRW23::FormatManager.DispatchChoice(BRW23::PopupChoice)
     ELSE
     END
  END


BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:5
    SELF.ChangeControl=?Change:5
    SELF.DeleteControl=?Delete:5
  END


BRW4.SetQueueRecord PROCEDURE

  CODE
      CLEAR(LOC:Vehicles_Group)
      CLEAR(DriverName)
  
         DRI:DRID             = A_TRU:DRID
         IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
            DriverName        = DRI:FirstName & ' ' & DRI:Surname
         .
  
  
      A_TRU:TTID              = VCO:TTID0
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_VG:Registration1   = A_TRU:Registration
         VMM:VMMID            = A_TRU:VMMID
         IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
            L_VG:MakeModel_0  = VMM:MakeModel
      .  .
  
      A_TRU:TTID                = VCO:TTID1
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_VG:Registration2   = A_TRU:Registration
         VMM:VMMID            = A_TRU:VMMID
         IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
            L_VG:MakeModel_1  = VMM:MakeModel
      .  .
  
      A_TRU:TTID                = VCO:TTID2
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_VG:Registration3   = A_TRU:Registration
         VMM:VMMID            = A_TRU:VMMID
         IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
            L_VG:MakeModel_2  = VMM:MakeModel
      .  .
  
      A_TRU:TTID                = VCO:TTID3
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_VG:Registration4   = A_TRU:Registration
         VMM:VMMID            = A_TRU:VMMID
         IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
            L_VG:MakeModel_3  = VMM:MakeModel
      .  .
  PARENT.SetQueueRecord
  


BRW4.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW4::LastSortOrder <> NewOrder THEN
     BRW4::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW4::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW4.TakeNewSelection PROCEDURE

  CODE
  IF BRW4::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW4::PopupTextExt = ''
        BRW4::PopupChoiceExec = True
        BRW4::FormatManager.MakePopup(BRW4::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW4::PopupTextExt = '|-|' & CLIP(BRW4::PopupTextExt)
        END
        BRW4::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW4::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW4::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW4::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW4::PopupChoiceOn AND BRW4::PopupChoiceExec THEN
     BRW4::PopupChoiceExec = False
     BRW4::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW4::PopupTextExt)
     IF BRW4::FormatManager.DispatchChoice(BRW4::PopupChoice)
     ELSE
     END
  END


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:7
    SELF.ChangeControl=?Change:7
    SELF.DeleteControl=?Delete:7
  END


BRW6.SetQueueRecord PROCEDURE

  CODE
      EXECUTE TRU:Type
         LOC:Truck_Type     = 'Trailer'
         LOC:Truck_Type     = 'Combined'
      ELSE
         LOC:Truck_Type     = 'Horse'
      .
  
  
      ! Search for the Truck on a combination.  Should only be on 1 really?
      CLEAR(VCO:Record, -1)
      VCO:TID             = TRU:TID
      SET(VCO:FKey_TID, VCO:FKey_TID)
      LOOP
         IF Access:VehicleComposition.TryNext() ~= LEVEL:Benign
            BREAK
         .
         IF VCO:TID ~= TRU:TID
            BREAK
         .
  
         IF VCO:TTID0 = TRU:TTID OR VCO:TTID1 = TRU:TTID OR VCO:TTID2 = TRU:TTID OR VCO:TTID3 = TRU:TTID
            LOC:TruckTrailer_Composition = VCO:CompositionName
            BREAK
      .  .
  PARENT.SetQueueRecord
  


BRW6.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW6::LastSortOrder <> NewOrder THEN
     BRW6::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW6::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW6.TakeNewSelection PROCEDURE

  CODE
  IF BRW6::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW6::PopupTextExt = ''
        BRW6::PopupChoiceExec = True
        BRW6::FormatManager.MakePopup(BRW6::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW6::PopupTextExt = '|-|' & CLIP(BRW6::PopupTextExt)
        END
        BRW6::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW6::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW6::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW6::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW6::PopupChoiceOn AND BRW6::PopupChoiceExec THEN
     BRW6::PopupChoiceExec = False
     BRW6::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW6::PopupTextExt)
     IF BRW6::FormatManager.DispatchChoice(BRW6::PopupChoice)
     ELSE
     END
  END


BRW8.SetAlerts PROCEDURE

  CODE
  SELF.EditViaPopup = False
  PARENT.SetAlerts


BRW8.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW8::LastSortOrder <> NewOrder THEN
     BRW8::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW8::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  IF BRW8::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW8::PopupTextExt = ''
        BRW8::PopupChoiceExec = True
        BRW8::FormatManager.MakePopup(BRW8::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW8::PopupTextExt = '|-|' & CLIP(BRW8::PopupTextExt)
        END
        BRW8::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW8::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW8::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW8::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW8::PopupChoiceOn AND BRW8::PopupChoiceExec THEN
     BRW8::PopupChoiceExec = False
     BRW8::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW8::PopupTextExt)
     IF BRW8::FormatManager.DispatchChoice(BRW8::PopupChoice)
     ELSE
     END
  END


BRW10.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:11
    SELF.ChangeControl=?Change:11
    SELF.DeleteControl=?Delete:11
  END


BRW10.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW10::LastSortOrder <> NewOrder THEN
     BRW10::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW10::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW10.TakeNewSelection PROCEDURE

  CODE
  IF BRW10::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW10::PopupTextExt = ''
        BRW10::PopupChoiceExec = True
        BRW10::FormatManager.MakePopup(BRW10::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW10::PopupTextExt = '|-|' & CLIP(BRW10::PopupTextExt)
        END
        BRW10::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW10::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW10::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW10::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW10::PopupChoiceOn AND BRW10::PopupChoiceExec THEN
     BRW10::PopupChoiceExec = False
     BRW10::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW10::PopupTextExt)
     IF BRW10::FormatManager.DispatchChoice(BRW10::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group1
  SELF.SetStrategy(?Group_Inv, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Inv
  SELF.SetStrategy(?Group_InvBrw_Bot2, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_InvBrw_Bot2
  SELF.SetStrategy(?Group1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group1
  SELF.SetStrategy(?Group_Right, Resize:FixRight+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Right


FDB26.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      ! Add an All
  
      Queue:FileDrop:InvBranch.BRA:BranchName      = 'All'
      GET(Queue:FileDrop:InvBranch, Queue:FileDrop:InvBranch.BRA:BranchName)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop:InvBranch   )
         Queue:FileDrop:InvBranch.BRA:BranchName      = 'All'
         Queue:FileDrop:InvBranch.BRA:BID             = 0
         ADD(Queue:FileDrop:InvBranch,1)
      .
  
  
      IF L_FG:Time_1st = TRUE         !AND CLIP(p:TID) ~= ''
         SELECT(?L_FG:BranchName, 1)  !RECORDS(Queue:FileDrop:InvBranch))
  
         L_FG:BID         = 0
         L_FG:BranchName  = 'All'
      .
  
      L_FG:Time_1st     = FALSE
  RETURN ReturnValue

BRW22::SortHeader.QueueResorted       PROCEDURE(STRING pString)
  CODE
    IF pString = ''
       BRW_Invoices.RestoreSort()
       BRW_Invoices.ResetSort(True)
    ELSE
       BRW_Invoices.ReplaceSort(pString,BRW22::Sort0:Locator)
       BRW_Invoices.SetLocatorFromSort()
    END
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_DeliveryLegs PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Scr_Vars         GROUP,PRE(L_SV)                       !
Journey              STRING(70)                            !Description
FromAddress          STRING(35)                            !Name of this address
ToAddress            STRING(35)                            !Name of this address
TransporterName      STRING(35)                            !Transporters Name
Prev_Cost            STRING(50)                            !
Rate                 DECIMAL(8,2)                          !Rate for DI (debtors journey rate possibly adjusted) - Charge will be Rate multiplied by the total weight in kgs.
ChargesVAT           BYTE                                  !This Transporter charges / pays VAT
VAT                  DECIMAL(12,2)                         !
Total                DECIMAL(12,2)                         !
                     END                                   !
LOC:Tot_Weight       DECIMAL(10,2)                         !In kg's
LOC:PreviousCosts    QUEUE,PRE(L_PCQ)                      !Previous Costs for this Transporter and Journey
TransporterName      STRING(50)                            !Transporters Name
Rate                 DECIMAL(8,2)                          !Rate for DI (debtors journey rate possibly adjusted) - Charge will be Rate multiplied by the total weight in kgs.
Cost                 DECIMAL(9,2)                          !Cost for this leg (from Transporter)
DIDate               DATE                                  !DI Date
Display              BYTE                                  !
                     END                                   !
History::DELL:Record LIKE(DELL:RECORD),THREAD
QuickWindow          WINDOW('Form Delivery Legs'),AT(,,258,226),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdateDeliveryLegs'),SYSTEM
                       SHEET,AT(4,4,250,203),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           GROUP,AT(9,110,202,10),USE(?Group1),DISABLE
                             PROMPT('Previous Rates:'),AT(9,110),USE(?DELL:Cost:Prompt:2),TRN
                             LIST,AT(73,110,138,10),USE(L_SV:Prev_Cost),VSCROLL,COLOR(00E9E9E9h),DROP(15,200),FORMAT('70L(2)|M~T' & |
  'ransporter Name~@s50@28R(2)|M~Rate~L@n-11.2@48R(1)|M~Cost~L(2)@n-13.2@40D(2)|M~DI Date~L@d6@'), |
  FROM(LOC:PreviousCosts)
                           END
                           PROMPT('Leg:'),AT(9,20),USE(?DELL:Leg:Prompt),TRN
                           SPIN(@n6),AT(73,20,43,10),USE(DELL:Leg),RIGHT(1),COLOR(00E9E9E9h),MSG('Leg Number'),REQ,TIP('Leg Number')
                           PROMPT('Journey:'),AT(9,36),USE(?Journey:Prompt),TRN
                           BUTTON('...'),AT(57,36,12,10),USE(?CallLookup)
                           ENTRY(@s70),AT(73,36,138,10),USE(L_SV:Journey),MSG('Description'),REQ,TIP('Description')
                           PROMPT('From Address:'),AT(9,54),USE(?FromAddress:Prompt),TRN
                           BUTTON('...'),AT(57,54,12,10),USE(?CallLookup:2)
                           ENTRY(@s35),AT(73,54,138,10),USE(L_SV:FromAddress),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                           PROMPT('To Address:'),AT(9,70),USE(?ToAddress:Prompt),TRN
                           BUTTON('...'),AT(57,70,12,10),USE(?CallLookup:3)
                           ENTRY(@s35),AT(73,70,138,10),USE(L_SV:ToAddress),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                           PROMPT('Transporter:'),AT(9,92),USE(?TransporterName:Prompt),TRN
                           BUTTON('...'),AT(57,92,12,10),USE(?CallLookup:4)
                           ENTRY(@s35),AT(73,92,138,10),USE(L_SV:TransporterName),MSG('Transporters Name'),REQ,TIP('Transporters Name')
                           PROMPT('Cost:'),AT(9,154),USE(?DELL:Cost:Prompt),TRN
                           ENTRY(@n-13.2),AT(73,154,60,10),USE(DELL:Cost),DECIMAL(12),MSG('Cost for this leg (from' & |
  ' Transporter)'),TIP('Cost for this leg (from Transporter)')
                           PROMPT('Rate:'),AT(166,132),USE(?Rate:Prompt),HIDE,TRN
                           ENTRY(@n-11.2),AT(189,132,60,10),USE(L_SV:Rate),RIGHT(1),COLOR(00E9E9E9h),HIDE,MSG('Rate used ' & |
  'on this DI'),READONLY,SKIP,TIP('Rate used on this DI<0DH,0AH>Charge will be this rat' & |
  'e multiplied by the total weight in kgs.')
                           PROMPT('Total Incl.:'),AT(9,190),USE(?Total:Prompt),TRN
                           ENTRY(@n-17.2),AT(73,190,60,10),USE(L_SV:Total),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Weight:'),AT(9,132),USE(?LOC:Tot_Weight:Prompt),TRN
                           ENTRY(@n-11.2),AT(73,132,60,10),USE(LOC:Tot_Weight),RIGHT(1),COLOR(00E9E9E9h),MSG('In kg''s'), |
  READONLY,SKIP,TIP('In kg''s')
                           PROMPT('VAT Rate:'),AT(150,154),USE(?DELL:VATRate:Prompt),TRN
                           ENTRY(@n-7.2),AT(189,154,60,10),USE(DELL:VATRate),RIGHT(1),MSG('VAT rate'),TIP('VAT rate')
                           PROMPT('VAT:'),AT(9,174),USE(?VAT:Prompt),TRN
                           ENTRY(@n-17.2),AT(73,174,60,10),USE(L_SV:VAT),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP
                         END
                       END
                       PROMPT('IKB - missing vehicle composition!  Rate cannot be got without this.{20}'),AT(54,2, |
  197,18),USE(?Prompt10),HIDE,TRN
                       BUTTON('&OK'),AT(154,210,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(206,210,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,210,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
Chg_View            VIEW(DeliveryLegsAlias)
    PROJECT(A_DELL:DID, A_DELL:TID, A_DELL:Cost, A_DELL:JID)
    JOIN(TRA:PKey_TID, A_DELL:TID)
       PROJECT(TRA:TransporterName)
    .
    JOIN(DEL:PKey_DID, A_DELL:DID)
       PROJECT(DEL:DIDate)
    .               .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
New_Journey                 ROUTINE
    ! Here we should check that the journey has actually changed.
    ! If it has we should check that the collection and delivery points are the same
    ! If not then we should warn the user and offer to change them to the Journey.
    !db.debugout('Journey new - DELL:CollectionAID: ' & DELL:CollectionAID & ',  DELL:DeliveryAID: ' & DELL:DeliveryAID)

!    IF JOU:CollectionAID ~= DELL:CollectionAID AND DELL:CollectionAID ~= 0
!       CASE MESSAGE('The Journies Collection Address differs from the specified Collection address on this delivery.||' |
!                    & 'Would you like to change it to the Journey Collection Address?', 'Collection Address', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
!       OF BUTTON:Yes
!          DELL:CollectionAID    = JOU:CollectionAID
!       .
!    ELSE
!       DELL:CollectionAID       = JOU:CollectionAID
!    .
    ADD:AID                     = DELL:CollectionAID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       L_SV:FromAddress         = ADD:AddressName
    .


!    IF JOU:DeliveryAID ~= DELL:DeliveryAID AND DELL:DeliveryAID ~= 0
!       CASE MESSAGE('The Journies Delivery Address differs from the specified Delivery address on this delivery.||' |
!                    & 'Would you like to change it to the Journey Delivery Address?', 'Delivery Address', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
!       OF BUTTON:Yes
!          DELL:DeliveryAID      = JOU:DeliveryAID
!       .
!    ELSE
!       DELL:DeliveryAID         = JOU:DeliveryAID
!    .
    ADD:AID                     = DELL:DeliveryAID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       L_SV:ToAddress           = ADD:AddressName
    .

    DO Load_Charges_Q

    DISPLAY
    EXIT
Load_Charges_Q           ROUTINE
    DATA
L:FoundPos      LONG
L:Idx           ULONG
L:Top           BYTE(1)

    CODE
    FREE(LOC:PreviousCosts)

    IF DELL:JID ~= 0
       SET(DeliveryLegsAlias)
       OPEN(Chg_View)
       Chg_View{PROP:Filter}        = 'A_DELL:JID = ' & DELL:JID

       LOOP
          NEXT(Chg_View)
          IF ERRORCODE()
             BREAK
          .

          ! Dont load duplicates of the same Transporter and Cost
          L_PCQ:TransporterName     = TRA:TransporterName
          L_PCQ:Cost                = A_DELL:Cost
          GET(LOC:PreviousCosts, +L_PCQ:TransporterName, +L_PCQ:Cost)
          IF ERRORCODE()
             CLEAR(LOC:PreviousCosts)
             L_PCQ:TransporterName  = TRA:TransporterName
             L_PCQ:Rate             = A_DELL:Cost / (Get_DelItem_s_Totals(A_DELL:DID,0) / 100)
             L_PCQ:Cost             = A_DELL:Cost
             L_PCQ:DIDate           = DEL:DIDate
             ADD(LOC:PreviousCosts)
       .  .
       CLOSE(Chg_View)

       SORT(LOC:PreviousCosts, +L_PCQ:TransporterName, -L_PCQ:DIDate)

       ! Now move all this Legs transporter entries to the top
       L:Top    = TRUE
       L:Idx    = 0
       LOOP
          L:Idx += 1
          GET(LOC:PreviousCosts, L:Idx)
          IF ERRORCODE()
             BREAK
          .

          IF L_PCQ:TransporterName ~= L_SV:TransporterName
             L:Top  = FALSE
          ELSIF L:Top = FALSE           ! We know names are same too
             ! Move this entry to the top
             ADD(LOC:PreviousCosts, 1)
             GET(LOC:PreviousCosts, L:Idx+1)
             DELETE(LOC:PreviousCosts)
             L:Idx  -= 1
       .  .

       IF RECORDS(LOC:PreviousCosts) > 0
          ?L_SV:Prev_Cost{PROP:Background} = 0C8FFFFH
          ENABLE(?Group1)

          GET(LOC:PreviousCosts, 1)
          L_SV:Prev_Cost        = 'Rate ' & CLIP(LEFT(FORMAT(L_PCQ:Rate,@n12.2))) & ' - ' & FORMAT(L_PCQ:DIDate, @d5) & ' - ' & CLIP(L_PCQ:TransporterName)
          L_PCQ:TransporterName = L_SV:Prev_Cost
          L_PCQ:Display         = TRUE
          ADD(LOC:PreviousCosts)

          DISPLAY(?L_SV:Prev_Cost)
       ELSE
          ?L_SV:Prev_Cost{PROP:Background} = 0E9E9E9H
          DISABLE(?Group1)
    .  .
    EXIT
Calcs                              ROUTINE
    L_SV:VAT        = DELL:Cost * (DELL:VATRate / 100)
    L_SV:Total      = DELL:Cost + L_SV:VAT
    DISPLAY
    EXIT


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Delivery Legs Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Delivery Legs Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_DeliveryLegs')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DELL:Cost:Prompt:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:DeliveryLegs)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DELL:Record,History::DELL:Record)
  SELF.AddHistoryField(?DELL:Leg,3)
  SELF.AddHistoryField(?DELL:Cost,9)
  SELF.AddHistoryField(?DELL:VATRate,10)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:AddressAlias.SetOpenRelated()
  Relate:AddressAlias.Open                                 ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryLegsAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveryLegs
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?L_SV:Prev_Cost)
    DISABLE(?CallLookup)
    ?L_SV:Journey{PROP:ReadOnly} = True
    DISABLE(?CallLookup:2)
    ?L_SV:FromAddress{PROP:ReadOnly} = True
    DISABLE(?CallLookup:3)
    ?L_SV:ToAddress{PROP:ReadOnly} = True
    DISABLE(?CallLookup:4)
    ?L_SV:TransporterName{PROP:ReadOnly} = True
    ?DELL:Cost{PROP:ReadOnly} = True
    ?L_SV:Rate{PROP:ReadOnly} = True
    ?L_SV:Total{PROP:ReadOnly} = True
    ?LOC:Tot_Weight{PROP:ReadOnly} = True
    ?DELL:VATRate{PROP:ReadOnly} = True
    ?L_SV:VAT{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_DeliveryLegs',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      ! Load lookup fileds & additionals
      JOU:JID             = DELL:JID
      IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
         L_SV:Journey     = JOU:Journey
      .
  
      ADD:AID             = DELL:CollectionAID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
         L_SV:FromAddress = ADD:AddressName
      .
  
      ADD:AID             = DELL:DeliveryAID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
         L_SV:ToAddress   = ADD:AddressName
      .
  
  
      TRA:TID                 = DELL:TID
      IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
         L_SV:TransporterName = TRA:TransporterName
      .
      LOC:Tot_Weight      = Get_DelItem_s_Totals(DELL:DID,0) / 100
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_DeliveryLegs',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  DELL:DID = DEL:DID
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_Journey
      Browse_Addresses
      Browse_Addresses_Alias_h
      Browse_Transporter
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      JOU:Journey = L_SV:Journey
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SV:Journey = JOU:Journey
        DELL:JID = JOU:JID
      END
      ThisWindow.Reset(1)
          DO New_Journey
    OF ?L_SV:Journey
      IF L_SV:Journey OR ?L_SV:Journey{PROP:Req}
        JOU:Journey = L_SV:Journey
        IF Access:Journeys.TryFetch(JOU:Key_Journey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SV:Journey = JOU:Journey
            DELL:JID = JOU:JID
          ELSE
            CLEAR(DELL:JID)
            SELECT(?L_SV:Journey)
            CYCLE
          END
        ELSE
          DELL:JID = JOU:JID
        END
      END
      ThisWindow.Reset()
          DO New_Journey
    OF ?CallLookup:2
      ThisWindow.Update()
      ADD:AddressName = L_SV:FromAddress
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_SV:FromAddress = ADD:AddressName
        DELL:CollectionAID = ADD:AID
      END
      ThisWindow.Reset(1)
          DO Load_Charges_Q
    OF ?L_SV:FromAddress
      IF L_SV:FromAddress OR ?L_SV:FromAddress{PROP:Req}
        ADD:AddressName = L_SV:FromAddress
        IF Access:Addresses.TryFetch(ADD:Key_Name)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            L_SV:FromAddress = ADD:AddressName
            DELL:CollectionAID = ADD:AID
          ELSE
            CLEAR(DELL:CollectionAID)
            SELECT(?L_SV:FromAddress)
            CYCLE
          END
        ELSE
          DELL:CollectionAID = ADD:AID
        END
      END
      ThisWindow.Reset()
          DO Load_Charges_Q
    OF ?CallLookup:3
      ThisWindow.Update()
      A_ADD:AddressName = L_SV:ToAddress
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        L_SV:ToAddress = A_ADD:AddressName
        DELL:DeliveryAID = A_ADD:AID
      END
      ThisWindow.Reset(1)
          DO Load_Charges_Q
    OF ?L_SV:ToAddress
      IF L_SV:ToAddress OR ?L_SV:ToAddress{PROP:Req}
        A_ADD:AddressName = L_SV:ToAddress
        IF Access:AddressAlias.TryFetch(A_ADD:Key_Name)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            L_SV:ToAddress = A_ADD:AddressName
            DELL:DeliveryAID = A_ADD:AID
          ELSE
            CLEAR(DELL:DeliveryAID)
            SELECT(?L_SV:ToAddress)
            CYCLE
          END
        ELSE
          DELL:DeliveryAID = A_ADD:AID
        END
      END
      ThisWindow.Reset()
          DO Load_Charges_Q
    OF ?CallLookup:4
      ThisWindow.Update()
      TRA:TransporterName = L_SV:TransporterName
      IF SELF.Run(4,SelectRecord) = RequestCompleted
        L_SV:TransporterName = TRA:TransporterName
        DELL:TID = TRA:TID
        L_SV:ChargesVAT = TRA:ChargesVAT
      END
      ThisWindow.Reset(1)
          DO Load_Charges_Q
          IF L_SV:ChargesVAT = FALSE
             DELL:VATRate     = 0.0
          ELSIF DELL:VATRate = 0.0
             DELL:VATRate     = Get_Setup_Info(4)
          .
          DISPLAY(?DELL:VATRate)
    OF ?L_SV:TransporterName
      IF L_SV:TransporterName OR ?L_SV:TransporterName{PROP:Req}
        TRA:TransporterName = L_SV:TransporterName
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            L_SV:TransporterName = TRA:TransporterName
            DELL:TID = TRA:TID
            L_SV:ChargesVAT = TRA:ChargesVAT
          ELSE
            CLEAR(DELL:TID)
            CLEAR(L_SV:ChargesVAT)
            SELECT(?L_SV:TransporterName)
            CYCLE
          END
        ELSE
          DELL:TID = TRA:TID
          L_SV:ChargesVAT = TRA:ChargesVAT
        END
      END
      ThisWindow.Reset()
          DO Load_Charges_Q
          IF L_SV:ChargesVAT = FALSE
             DELL:VATRate     = 0.0
          ELSIF DELL:VATRate = 0.0
             DELL:VATRate     = Get_Setup_Info(4)
          .
          DISPLAY(?DELL:VATRate)
    OF ?DELL:Cost
          L_SV:Rate   = DELL:Cost / LOC:Tot_Weight
          DISPLAY(?L_SV:Rate)
          DO Calcs
    OF ?DELL:VATRate
          DO Calcs
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
          IF DELL:Cost <= 0.0
             CASE MESSAGE('There is a zero or negative cost specified on this leg.||Is this correct?', 'Leg Cost', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:No
                QuickWindow{PROP:AcceptAll}   = FALSE
                SELECT(?DELL:Cost)
                CYCLE
          .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_SV:Prev_Cost
          GET(LOC:PreviousCosts, CHOICE(?L_SV:Prev_Cost))
          IF ~ERRORCODE()
             IF L_PCQ:Display = TRUE
      !          message('display')
             ELSE
                IF L_SV:Rate ~= 0.0
                   IF L_SV:Rate ~= L_PCQ:Rate
                      CASE MESSAGE('Would you like to update the Delivery Cost specified on this leg with the selected rated cost?', 'Update Delivery Cost & Rate', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                      OF BUTTON:Yes
                         DELL:Cost    = L_PCQ:Rate * LOC:Tot_Weight
                         L_SV:Rate    = L_PCQ:Rate
                         DISPLAY
                   .  .
                ELSE
                   DELL:Cost          = L_PCQ:Rate * LOC:Tot_Weight
                   L_SV:Rate          = L_PCQ:Rate
                   DISPLAY
                .
      
                L_SV:Prev_Cost        = 'Rate ' & CLIP(LEFT(FORMAT(L_PCQ:Rate,@n12.2))) & ' - ' & FORMAT(L_PCQ:DIDate, @d5) & ' - ' & CLIP(L_PCQ:TransporterName)
      
                L_PCQ:Display         = TRUE
                GET(LOC:PreviousCosts, +L_PCQ:Display)
                IF ~ERRORCODE() AND L_PCQ:Display = TRUE
                   DELETE(LOC:PreviousCosts)
                .
      
                L_PCQ:Display         = TRUE
                L_PCQ:TransporterName = L_SV:Prev_Cost
                ADD(LOC:PreviousCosts)
      
                SELECT(?L_SV:Prev_Cost, RECORDS(LOC:PreviousCosts))
             .
          ELSE
      !        message('error')
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          SELECT(?L_SV:Journey)
          DO Calcs
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Transporter PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:BID              ULONG                                 !
LOC:Branch           STRING(35)                            !
LOC:Locator          STRING(100)                           !
LOC:Reset_Var        ULONG                                 !
LOC:Show_Archived    BYTE                                  !
LOC:Archive_Warning_Show BYTE                              !
LOC:Status           STRING(14)                            !Normal, Pending, Do Not Use
BRW1::View:Browse    VIEW(Transporter)
                       PROJECT(TRA:TransporterName)
                       PROJECT(TRA:OpsManager)
                       PROJECT(TRA:VATNo)
                       PROJECT(TRA:ChargesVAT)
                       PROJECT(TRA:Broking)
                       PROJECT(TRA:Comments)
                       PROJECT(TRA:Archived)
                       PROJECT(TRA:TID)
                       PROJECT(TRA:BID)
                       PROJECT(TRA:ACID)
                       PROJECT(TRA:Status)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
TRA:TransporterName_NormalFG LONG                     !Normal forground color
TRA:TransporterName_NormalBG LONG                     !Normal background color
TRA:TransporterName_SelectedFG LONG                   !Selected forground color
TRA:TransporterName_SelectedBG LONG                   !Selected background color
TRA:OpsManager         LIKE(TRA:OpsManager)           !List box control field - type derived from field
TRA:VATNo              LIKE(TRA:VATNo)                !List box control field - type derived from field
TRA:ChargesVAT         LIKE(TRA:ChargesVAT)           !List box control field - type derived from field
TRA:ChargesVAT_Icon    LONG                           !Entry's icon ID
LOC:Branch             LIKE(LOC:Branch)               !List box control field - type derived from local data
TRA:Broking            LIKE(TRA:Broking)              !List box control field - type derived from field
TRA:Broking_Icon       LONG                           !Entry's icon ID
TRA:Comments           LIKE(TRA:Comments)             !List box control field - type derived from field
TRA:Comments_Icon      LONG                           !Entry's icon ID
LOC:Status             LIKE(LOC:Status)               !List box control field - type derived from local data
TRA:Archived           LIKE(TRA:Archived)             !List box control field - type derived from field
TRA:Archived_Icon      LONG                           !Entry's icon ID
TRA:TID                LIKE(TRA:TID)                  !List box control field - type derived from field
TRA:BID                LIKE(TRA:BID)                  !List box control field - type derived from field
TRA:ACID               LIKE(TRA:ACID)                 !List box control field - type derived from field
LOC:Show_Archived      LIKE(LOC:Show_Archived)        !Browse hot field - type derived from local data
TRA:Status             LIKE(TRA:Status)               !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB8::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?LOC:Branch
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Transporter File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MAX,MDI,HLP('BrowseTransporter'),SYSTEM
                       LIST,AT(8,36,342,119),USE(?Browse:1),HVSCROLL,ALRT(F2Key),FORMAT('80L(2)|M*~Transporter' & |
  '~@s35@52L(2)|M~Ops. Manager~@s35@50L(2)|M~VAT No.~@s20@19L(1)|MI~VAT (Charges / Pays' & |
  ')~L(2)@p p@50L(2)|M~Branch~@s35@26L(1)|MI~Broking~L(2)@p p@150L(1)|MI~Comment~L(0)@s' & |
  '255@40L(2)|M~Status~@s14@30L(1)|MI~Archived~@p p@40R(2)|M~TID~C(0)@n_10@40R(2)|M~BID' & |
  '~C(0)@n_10@40R(2)|M~ACID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Tra' & |
  'nsporter file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Transporter'),USE(?Tab:2)
                         END
                         TAB('&2) By Transporter (branch)'),USE(?Tab:3)
                           GROUP,AT(5,184,160,10),USE(?Group_branch)
                             PROMPT('Branch:'),AT(5,184),USE(?Prompt2)
                             LIST,AT(34,184,93,10),USE(LOC:Branch),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                           END
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,4,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       PROMPT('Locator:'),AT(8,22),USE(?LOC:Locator:Prompt),TRN
                       STRING(@s100),AT(36,22),USE(LOC:Locator),TRN
                       CHECK(' Show Archived'),AT(229,183),USE(LOC:Show_Archived),TIP('Show Archived records i' & |
  'n the list<0DH,0AH,0DH,0AH>Press F2 to toggle archived state')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB8                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

Archive_Toggle                  ROUTINE
DATA
R:Yes   BYTE

CODE  
  BRW1.UpdateViewRecord()
  BRW1.UpdateBuffer()
  
  ! Toggle archived on highlighted...
  IF LOC:Archive_Warning_Show = 0
     IF TRA:Archived = FALSE
        CASE MESSAGE('Archive this Transporter?','Archive Transporters',ICON:Question, 'Yes|Yes (don''t warn again)|No', 3)
        OF 1
          R:Yes = TRUE
        OF 2    
          R:Yes = TRUE          
          LOC:Archive_Warning_Show  = 1
        .
      ELSE
        CASE MESSAGE('Un-Archive this Transporter?','Archive Transporters',ICON:Question, 'Yes|Yes (don''t warn again)|No', 3)
        OF 1
          R:Yes = TRUE
        OF 2    
          R:Yes = TRUE          
          LOC:Archive_Warning_Show  = 1
        .        
      .
  ELSE
    R:Yes = TRUE
  .
  
  IF R:Yes = TRUE    
    BRW1.UpdateViewRecord()
    
    BRW1.UpdateBuffer()
    IF Access:Transporter.TryFetch(TRA:PKey_TID) = Level:Benign
      IF TRA:Archived  = 1
        TRA:Archived = 0
      ELSE
        TRA:Archived = 1
        TRA:Archived_Date = TODAY()
        TRA:Archived_Time = CLOCK()
      .          
      IF Access:Transporter.TryUpdate() = Level:Benign
        BRW1.ResetFromBuffer()  
  . . .    

  EXIT
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Transporter')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Show_Archived',LOC:Show_Archived)     ! Added by: BrowseBox(ABC)
  BIND('LOC:BID',LOC:BID)                         ! Added by: BrowseBox(ABC)
  BIND('LOC:Branch',LOC:Branch)                   ! Added by: BrowseBox(ABC)
  BIND('LOC:Status',LOC:Status)                   ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                           ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Access:Branches.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Transporter,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.FileLoaded = 1                             ! This is a 'file loaded' browse
  BRW1.AddSortOrder(,TRA:Key_TransporterName)     ! Add the sort order for TRA:Key_TransporterName for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)            ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(?LOC:Locator,TRA:TransporterName,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: TRA:Key_TransporterName , TRA:TransporterName
  BRW1.AppendOrder('+TRA:TID')                    ! Append an additional sort order
  BRW1.SetFilter('(LOC:BID = 0 OR LOC:BID = TRA:BID)') ! Apply filter expression to browse
  BRW1.AddSortOrder(,TRA:Key_TransporterName)     ! Add the sort order for TRA:Key_TransporterName for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(?LOC:Locator,TRA:TransporterName,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: TRA:Key_TransporterName , TRA:TransporterName
  BRW1.AppendOrder('+TRA:TID')                    ! Append an additional sort order
  BRW1.SetFilter('(LOC:Show_Archived = 1 OR TRA:Archived <<> 1)') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Reset_Var)               ! Apply the reset field
  BRW1.AddResetField(LOC:Show_Archived)           ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(TRA:TransporterName,BRW1.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW1.AddField(TRA:OpsManager,BRW1.Q.TRA:OpsManager) ! Field TRA:OpsManager is a hot field or requires assignment from browse
  BRW1.AddField(TRA:VATNo,BRW1.Q.TRA:VATNo)       ! Field TRA:VATNo is a hot field or requires assignment from browse
  BRW1.AddField(TRA:ChargesVAT,BRW1.Q.TRA:ChargesVAT) ! Field TRA:ChargesVAT is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Branch,BRW1.Q.LOC:Branch)     ! Field LOC:Branch is a hot field or requires assignment from browse
  BRW1.AddField(TRA:Broking,BRW1.Q.TRA:Broking)   ! Field TRA:Broking is a hot field or requires assignment from browse
  BRW1.AddField(TRA:Comments,BRW1.Q.TRA:Comments) ! Field TRA:Comments is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Status,BRW1.Q.LOC:Status)     ! Field LOC:Status is a hot field or requires assignment from browse
  BRW1.AddField(TRA:Archived,BRW1.Q.TRA:Archived) ! Field TRA:Archived is a hot field or requires assignment from browse
  BRW1.AddField(TRA:TID,BRW1.Q.TRA:TID)           ! Field TRA:TID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:BID,BRW1.Q.TRA:BID)           ! Field TRA:BID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:ACID,BRW1.Q.TRA:ACID)         ! Field TRA:ACID is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Show_Archived,BRW1.Q.LOC:Show_Archived) ! Field LOC:Show_Archived is a hot field or requires assignment from browse
  BRW1.AddField(TRA:Status,BRW1.Q.TRA:Status)     ! Field TRA:Status is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_Transporter',QuickWindow)  ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      BRA:BID     = GLO:BranchID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
      .
  BRW1.AskProcedure = 1
  FDB8.Init(?LOC:Branch,Queue:FileDrop.ViewPosition,FDB8::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB8.Q &= Queue:FileDrop
  FDB8.AddSortOrder(BRA:Key_BranchName)
  FDB8.AddField(BRA:BranchName,FDB8.Q.BRA:BranchName) !List box control field - type derived from field
  FDB8.AddField(BRA:BID,FDB8.Q.BRA:BID) !Primary key field - type derived from field
  FDB8.AddUpdateField(BRA:BID,LOC:BID)
  ThisWindow.AddItem(FDB8.WindowComponent)
  FDB8.DefaultFill = 0
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_Transporter',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,20,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_Transporter',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Transporter
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LOC:Branch
          LOC:Reset_Var   += 1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      IF KEYCODE() = F2Key
        DO Archive_Toggle
      .
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LOC:Branch
          LOC:Reset_Var   += 1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      LOC:Branch      = ''
      BRA:BID         = TRA:BID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         LOC:Branch   = BRA:BranchName
      .
  
  
  
     LOC:Status = 'Normal'
     IF TRA:Status = 1
        LOC:Status = 'Pending'
     ELSIF TRA:Status = 2
        LOC:Status = 'Do Not Use'
     .
  PARENT.SetQueueRecord
  
  IF (TRA:Status = 1)
    SELF.Q.TRA:TransporterName_NormalFG = -1               ! Set conditional color values for TRA:TransporterName
    SELF.Q.TRA:TransporterName_NormalBG = 33023
    SELF.Q.TRA:TransporterName_SelectedFG = 33023
    SELF.Q.TRA:TransporterName_SelectedBG = 16777215
  ELSIF (TRA:Status = 2)
    SELF.Q.TRA:TransporterName_NormalFG = -1               ! Set conditional color values for TRA:TransporterName
    SELF.Q.TRA:TransporterName_NormalBG = 255
    SELF.Q.TRA:TransporterName_SelectedFG = 255
    SELF.Q.TRA:TransporterName_SelectedBG = 16777215
  ELSE
    SELF.Q.TRA:TransporterName_NormalFG = -1               ! Set color values for TRA:TransporterName
    SELF.Q.TRA:TransporterName_NormalBG = -1
    SELF.Q.TRA:TransporterName_SelectedFG = -1
    SELF.Q.TRA:TransporterName_SelectedBG = -1
  END
  IF (TRA:ChargesVAT = 1)
    SELF.Q.TRA:ChargesVAT_Icon = 2                         ! Set icon from icon list
  ELSE
    SELF.Q.TRA:ChargesVAT_Icon = 1                         ! Set icon from icon list
  END
  IF (TRA:Broking = 1)
    SELF.Q.TRA:Broking_Icon = 2                            ! Set icon from icon list
  ELSE
    SELF.Q.TRA:Broking_Icon = 1                            ! Set icon from icon list
  END
  SELF.Q.TRA:Comments_Icon = 0
  IF (TRA:Archived = 1)
    SELF.Q.TRA:Archived_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.TRA:Archived_Icon = 1                           ! Set icon from icon list
  END


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>1,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>1,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_branch, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_branch

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Addresses_Alias_h PROCEDURE  (p:Address_Name, p:Type) ! Declare Procedure
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:AddressAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Addresses.Open()
     .
     Access:AddressAlias.UseFile()
     Access:Addresses.UseFile()
    ! (p:Address_Name, p:Type)
    ! (<STRING>, <STRING>)

    ! Load any existing details

    ADD:AID                 = A_ADD:AID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) ~= LEVEL:Benign
       CLEAR(ADD:Record)
       IF ~OMITTED(1) AND CLIP(p:Address_Name) ~= ''
          ADD:AddressName      = p:Address_Name
       ELSE
          ADD:AddressName      = A_ADD:AddressName
    .  .

    Browse_Addresses(p:Address_Name, p:Type)

    IF GlobalResponse = RequestCompleted
       A_ADD:AID            = ADD:AID
       IF Access:AddressAlias.TryFetch(A_ADD:PKey_AID) ~= LEVEL:Benign
          CLEAR(A_ADD:Record)
       .
    ELSE
       CLEAR(A_ADD:Record)
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:AddressAlias.Close()
    Relate:Addresses.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_TruckTrailer PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
Transporter1         STRING(35)                            !Transporters Name
LOC:MakeModel        STRING(35)                            !Make & Model
LOC:Driver_Group     GROUP,PRE(LOC)                        !
Name                 STRING(70)                            !Firstname & Surname
                     END                                   !
History::TRU:Record  LIKE(TRU:RECORD),THREAD
QuickWindow          WINDOW('Form Truck or Trailer'),AT(,,280,243),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('UpdateTruckTrailer'),SYSTEM
                       SHEET,AT(4,4,272,218),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Transporter:'),AT(8,23),USE(?Transporter:Prompt),TRN
                           BUTTON('...'),AT(62,23,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(78,23,144,10),USE(Transporter1),MSG('Transporters Name'),REQ,TIP('Transporters Name')
                           STRING(@n_10),AT(224,23,,10),USE(TRU:TID),RIGHT(1),TRN
                           PROMPT('Registration:'),AT(8,38),USE(?TRU:Registration:Prompt),TRN
                           ENTRY(@s20),AT(78,38,84,10),USE(TRU:Registration),REQ
                           PROMPT('Make && Model:'),AT(8,55),USE(?TRU:MakeModel:Prompt),TRN
                           ENTRY(@s35),AT(78,55,144,10),USE(LOC:MakeModel),MSG('Make & Model'),REQ,TIP('Make & Model')
                           PROMPT('Type:'),AT(8,70),USE(?TRU:Type:Prompt),TRN
                           BUTTON('...'),AT(62,55,12,10),USE(?CallLookup:2)
                           LIST,AT(78,70,84,10),USE(TRU:Type),VSCROLL,DISABLE,DROP(15),FROM('Horse|#0|Trailer|#1|Rigid|#2'), |
  MSG('Type of vehicle'),TIP('Type of vehicle')
                           GROUP,AT(8,104,266,24),USE(?Group1),TRN
                             PROMPT('Driver:'),AT(8,114),USE(?LOC:Driver:Prompt),TRN
                             BUTTON('...'),AT(62,114,12,10),USE(?CallLookup:3)
                             ENTRY(@s70),AT(78,114,189,10),USE(LOC:Name),MSG('First Name'),READONLY,SKIP,TIP('First Name')
                           END
                           LINE,AT(10,133,262,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('License Expiry Date:'),AT(8,146),USE(?TRU:LicenseExpiryDate:Prompt),TRN
                           SPIN(@d6b),AT(78,146,84,10),USE(TRU:LicenseExpiryDate),RIGHT(1),MSG('License expires'),TIP('License expires')
                           BUTTON('...'),AT(169,146,12,10),USE(?Calendar)
                           PROMPT('License Info:'),AT(8,162),USE(?TRU:LicenseInfo:Prompt),TRN
                           TEXT,AT(78,162,189,48),USE(TRU:LicenseInfo),VSCROLL,BOXED
                           PROMPT('Capacity:'),AT(8,84),USE(?TRU:Capacity:Prompt),TRN
                           SPIN(@n-8.0),AT(78,84,84,10),USE(TRU:Capacity),RIGHT(1),DISABLE,MSG('In Kgs'),TIP('In Kgs')
                         END
                         TAB('&2) Manifest Load'),USE(?Tab:2),HIDE
                         END
                       END
                       BUTTON('&OK'),AT(174,224,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(228,224,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,224,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar10           CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
New_Type                    ROUTINE
    IF TRU:Type = 0 OR TRU:Type = 2
       ENABLE(?Group1)
    ELSE
       DISABLE(?Group1)
    .

    IF TRU:Type = 0
       CLEAR(TRU:Capacity)
    .

    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Truck Trailer Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Truck Trailer Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_TruckTrailer')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Transporter:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:TruckTrailer)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(TRU:Record,History::TRU:Record)
  SELF.AddHistoryField(?TRU:TID,2)
  SELF.AddHistoryField(?TRU:Registration,6)
  SELF.AddHistoryField(?TRU:Type,4)
  SELF.AddHistoryField(?TRU:LicenseExpiryDate,11)
  SELF.AddHistoryField(?TRU:LicenseInfo,13)
  SELF.AddHistoryField(?TRU:Capacity,7)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Drivers.SetOpenRelated()
  Relate:Drivers.Open                                      ! File Drivers used by this procedure, so make sure it's RelationManager is open
  Access:TruckTrailer.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleMakeModel.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TruckTrailer
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?Transporter1{PROP:ReadOnly} = True
    ?TRU:Registration{PROP:ReadOnly} = True
    ?LOC:MakeModel{PROP:ReadOnly} = True
    DISABLE(?CallLookup:2)
    DISABLE(?TRU:Type)
    DISABLE(?CallLookup:3)
    ?LOC:Name{PROP:ReadOnly} = True
    DISABLE(?Calendar)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_TruckTrailer',QuickWindow)          ! Restore window settings from non-volatile store
      TRA:TID             = TRU:TID
      IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
         Transporter1     = TRA:TransporterName
      .
  
  
      VMM:VMMID           = TRU:VMMID
      IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
         LOC:MakeModel    = VMM:MakeModel
      .
  
  
  
      DRI:DRID            = TRU:DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         LOC:Name         = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      DO New_Type
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Drivers.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_TruckTrailer',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Transporter
      Browse_VehicleMakeModels
      Browse_Drivers('','')
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      TRA:TransporterName = Transporter1
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        Transporter1 = TRA:TransporterName
        TRU:TID = TRA:TID
      END
      ThisWindow.Reset(1)
    OF ?Transporter1
      IF Transporter1 OR ?Transporter1{PROP:Req}
        TRA:TransporterName = Transporter1
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Transporter1 = TRA:TransporterName
            TRU:TID = TRA:TID
          ELSE
            CLEAR(TRU:TID)
            SELECT(?Transporter1)
            CYCLE
          END
        ELSE
          TRU:TID = TRA:TID
        END
      END
      ThisWindow.Reset()
    OF ?LOC:MakeModel
      IF LOC:MakeModel OR ?LOC:MakeModel{PROP:Req}
        VMM:MakeModel = LOC:MakeModel
        IF Access:VehicleMakeModel.TryFetch(VMM:Key_MakeModel)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            LOC:MakeModel = VMM:MakeModel
            TRU:VMMID = VMM:VMMID
            TRU:Type = VMM:Type
            TRU:Capacity = VMM:Capacity
          ELSE
            CLEAR(TRU:VMMID)
            CLEAR(TRU:Type)
            CLEAR(TRU:Capacity)
            SELECT(?LOC:MakeModel)
            CYCLE
          END
        ELSE
          TRU:VMMID = VMM:VMMID
          TRU:Type = VMM:Type
          TRU:Capacity = VMM:Capacity
        END
      END
      ThisWindow.Reset()
          DO New_Type
    OF ?CallLookup:2
      ThisWindow.Update()
      VMM:MakeModel = LOC:MakeModel
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LOC:MakeModel = VMM:MakeModel
        TRU:VMMID = VMM:VMMID
        TRU:Type = VMM:Type
        TRU:Capacity = VMM:Capacity
      END
      ThisWindow.Reset(1)
          DO New_Type
    OF ?TRU:Type
          DO New_Type
    OF ?CallLookup:3
      ThisWindow.Update()
      DRI:FirstNameSurname = LOC:Name
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        LOC:Name = DRI:FirstNameSurname
        TRU:DRID = DRI:DRID
      END
      ThisWindow.Reset(1)
          CLEAR(LOC:Name)
      
          DRI:DRID            = TRU:DRID
          IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
             LOC:Name         = CLIP(DRI:FirstName) & ' ' & DRI:Surname
          .
      
          DISPLAY
    OF ?LOC:Name
      IF LOC:Name OR ?LOC:Name{PROP:Req}
        DRI:FirstNameSurname = LOC:Name
        IF Access:Drivers.TryFetch(DRI:SKey_FirstNameSurname)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            LOC:Name = DRI:FirstNameSurname
            TRU:DRID = DRI:DRID
          ELSE
            CLEAR(TRU:DRID)
            SELECT(?LOC:Name)
            CYCLE
          END
        ELSE
          TRU:DRID = DRI:DRID
        END
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
      Calendar10.SelectOnClose = True
      Calendar10.Ask('Select a Date',TRU:LicenseExpiryDate)
      IF Calendar10.Response = RequestCompleted THEN
      TRU:LicenseExpiryDate=Calendar10.SelectedDate
      DISPLAY(?TRU:LicenseExpiryDate)
      END
      ThisWindow.Reset(True)
    OF ?OK
      ThisWindow.Update()
          IF CLIP(TRU:Registration) = ''
             MESSAGE('You have not specified a registration number.||Registration is required.', 'Truck / Trailer Validation', ICON:Exclamation)
             SELECT(?TRU:Registration)
             QuickWindow{PROP:AcceptAll}    = 0
             CYCLE
          .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?TRU:Type
          DO New_Type
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_VehicleComposition PROCEDURE (p:TID)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
Transporter1         STRING(35)                            !Transporters Name
LOC:TruckTrailer     STRING(20)                            !Truck / Trailer
Trailer1             STRING(20)                            !Make & Model
Trailer2             STRING(20)                            !Make & Model
Trailer3             STRING(20)                            !Make & Model
LOC:Prev_Capacity    DECIMAL(7)                            !Total capacity not to exceed this amount
LOC:Capacity_Group   GROUP,PRE(L_CG)                       !
Capacity             DECIMAL(6)                            !In Kgs
Capacity1            DECIMAL(6)                            !In Kgs
Capacity2            DECIMAL(6)                            !In Kgs
Capacity3            DECIMAL(6)                            !In Kgs
TotalCapacity        DECIMAL(6)                            !In Kgs
                     END                                   !
LOC:Make_Model       GROUP,PRE(L_MM)                       !Make_Models
MakeModel            STRING(35)                            !Make & Model
MakeModel1           STRING(35)                            !Make & Model
MakeModel2           STRING(35)                            !Make & Model
MakeModel3           STRING(35)                            !Make & Model
                     END                                   !
LOC:Make_Model_IDs   GROUP,PRE(L_MM)                       !
VMMID                ULONG                                 !Vehicle Make & Model ID
VMMID1               ULONG                                 !Vehicle Make & Model ID
VMMID2               ULONG                                 !Vehicle Make & Model ID
VMMID3               ULONG                                 !Vehicle Make & Model ID
                     END                                   !
BRW2::View:Browse    VIEW(__RatesTransporter)
                       PROJECT(TRRA:MinimiumLoad)
                       PROJECT(TRRA:BaseCharge)
                       PROJECT(TRRA:PerRate)
                       PROJECT(TRRA:TRID)
                       PROJECT(TRRA:VCID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
TRRA:MinimiumLoad      LIKE(TRRA:MinimiumLoad)        !List box control field - type derived from field
TRRA:BaseCharge        LIKE(TRRA:BaseCharge)          !List box control field - type derived from field
TRRA:PerRate           LIKE(TRRA:PerRate)             !List box control field - type derived from field
TRRA:TRID              LIKE(TRRA:TRID)                !Primary key field - type derived from field
TRRA:VCID              LIKE(TRRA:VCID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(Manifest)
                       PROJECT(MAN:MID)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:TID)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:Rate)
                       PROJECT(MAN:VATRate)
                       PROJECT(MAN:State)
                       PROJECT(MAN:DepartDate)
                       PROJECT(MAN:DepartTime)
                       PROJECT(MAN:ETADate)
                       PROJECT(MAN:VCID)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
MAN:MID                LIKE(MAN:MID)                  !List box control field - type derived from field
MAN:BID                LIKE(MAN:BID)                  !List box control field - type derived from field
MAN:TID                LIKE(MAN:TID)                  !List box control field - type derived from field
MAN:Cost               LIKE(MAN:Cost)                 !List box control field - type derived from field
MAN:Rate               LIKE(MAN:Rate)                 !List box control field - type derived from field
MAN:VATRate            LIKE(MAN:VATRate)              !List box control field - type derived from field
MAN:State              LIKE(MAN:State)                !List box control field - type derived from field
MAN:DepartDate         LIKE(MAN:DepartDate)           !List box control field - type derived from field
MAN:DepartTime         LIKE(MAN:DepartTime)           !List box control field - type derived from field
MAN:ETADate            LIKE(MAN:ETADate)              !List box control field - type derived from field
MAN:VCID               LIKE(MAN:VCID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(TripSheets)
                       PROJECT(TRI:BID)
                       PROJECT(TRI:DepartDate)
                       PROJECT(TRI:DepartTime)
                       PROJECT(TRI:ReturnedDate)
                       PROJECT(TRI:ReturnedTime)
                       PROJECT(TRI:Notes)
                       PROJECT(TRI:TRID)
                       PROJECT(TRI:VCID)
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?Browse:6
TRI:BID                LIKE(TRI:BID)                  !List box control field - type derived from field
TRI:DepartDate         LIKE(TRI:DepartDate)           !List box control field - type derived from field
TRI:DepartTime         LIKE(TRI:DepartTime)           !List box control field - type derived from field
TRI:ReturnedDate       LIKE(TRI:ReturnedDate)         !List box control field - type derived from field
TRI:ReturnedTime       LIKE(TRI:ReturnedTime)         !List box control field - type derived from field
TRI:Notes              LIKE(TRI:Notes)                !List box control field - type derived from field
TRI:TRID               LIKE(TRI:TRID)                 !Primary key field - type derived from field
TRI:VCID               LIKE(TRI:VCID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::VCO:Record  LIKE(VCO:RECORD),THREAD
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW4::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW4::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW4::PopupChoice    SIGNED                       ! Popup current choice
BRW4::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW4::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW6::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW6::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW6::PopupChoice    SIGNED                       ! Popup current choice
BRW6::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW6::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Vehicle Composition'),AT(,,300,191),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateVehicleComposition'),SYSTEM
                       SHEET,AT(4,4,292,169),USE(?CurrentTab),JOIN
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Transporter:'),AT(9,23),USE(?Transporter:Prompt),TRN
                           BUTTON('...'),AT(73,23,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(89,23,139,10),USE(Transporter1),MSG('Transporters Name'),REQ,TIP('Transporters Name')
                           STRING(@n_10),AT(233,23,,10),USE(VCO:TID),RIGHT(1),TRN
                           CHECK(' Show for All Transporters'),AT(89,39),USE(VCO:ShowForAllTransporters),MSG('Show this ' & |
  'Vehicle Composition for all Transporters'),TIP('Show this Vehicle Composition for al' & |
  'l Transporters<0DH,0AH>when selecting from Manifest'),TRN
                           ENTRY(@n-8.0),AT(241,71,50,10),USE(L_CG:Capacity),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                           PROMPT('Registrations'),AT(89,55,77,10),USE(?Prompt7),FONT(,,COLOR:Black,FONT:underline,CHARSET:ANSI), |
  TRN
                           PROMPT('Capacity'),AT(241,55,50,10),USE(?Prompt8),FONT(,,COLOR:Black,FONT:underline,CHARSET:ANSI), |
  TRN
                           LINE,AT(9,68,282,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Name:'),AT(9,141),USE(?VCO:Name:Prompt),TRN
                           BUTTON('Set Name'),AT(41,141,,10),USE(?Button_SetName),TIP('Set the name based on the T' & |
  'ruck / Horse and Trailer')
                           ENTRY(@s35),AT(89,141,139,10),USE(VCO:CompositionName)
                           ENTRY(@n-8.0),AT(241,141,50,10),USE(L_CG:TotalCapacity),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                           PROMPT('Total Capacity Allowed:'),AT(9,159),USE(?VCO:Capacity:Prompt),TRN
                           ENTRY(@n10.0),AT(89,159,77,10),USE(VCO:Capacity),RIGHT(1),MSG('Total capacity not to ex' & |
  'ceed this amount'),TIP('Total capacity not to exceed this amount')
                           STRING(@n_10),AT(26,71),USE(VCO:TTID0),RIGHT(1),HIDE,TRN
                           PROMPT('Truck / Horse:'),AT(9,71),USE(?TruckTrailer::Prompt),TRN
                           BUTTON('...'),AT(73,71,12,10),USE(?CallLookup:2)
                           ENTRY(@s20),AT(89,71,77,10),USE(LOC:TruckTrailer),MSG('Make & Model'),REQ,TIP('Make & Model')
                           PROMPT('Make && Models'),AT(170,55,65,10),USE(?L_MM:MakeModel:Prompt),FONT(,,COLOR:Black,FONT:underline, |
  CHARSET:ANSI),TRN
                           STRING(@s35),AT(170,71,65,10),USE(L_MM:MakeModel),TRN
                           PROMPT('Trailer 1:'),AT(9,87),USE(?Trailer1:Prompt),TRN
                           BUTTON('...'),AT(73,87,12,10),USE(?CallLookup:3)
                           ENTRY(@s20),AT(89,87,77,10),USE(Trailer1),MSG('Make & Model'),TIP('Make & Model')
                           STRING(@s35),AT(170,87,65,10),USE(L_MM:MakeModel1),TRN
                           ENTRY(@n-8.0),AT(241,87,50,10),USE(L_CG:Capacity1),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                           STRING(@n_10),AT(12,87),USE(VCO:TTID1),RIGHT(1),TRN
                           PROMPT('Trailer 2:'),AT(9,100),USE(?Trailer2:Prompt),TRN
                           BUTTON('...'),AT(73,100,12,10),USE(?CallLookup:4)
                           ENTRY(@s20),AT(89,100,77,10),USE(Trailer2),MSG('Make & Model'),TIP('Make & Model')
                           STRING(@s35),AT(170,100,65,10),USE(L_MM:MakeModel2),TRN
                           ENTRY(@n-8.0),AT(241,100,50,10),USE(L_CG:Capacity2),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                           STRING(@n_10),AT(12,100),USE(VCO:TTID2),RIGHT(1),TRN
                           PROMPT('Trailer 3:'),AT(9,114),USE(?Trailer3:Prompt),TRN
                           BUTTON('...'),AT(73,114,12,10),USE(?CallLookup:5)
                           ENTRY(@s20),AT(89,114,77,10),USE(Trailer3),MSG('Make & Model'),TIP('Make & Model')
                           STRING(@s35),AT(170,114,65,10),USE(L_MM:MakeModel3),TRN
                           ENTRY(@n-8.0),AT(241,114,50,10),USE(L_CG:Capacity3),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                           STRING(@n_10),AT(12,114),USE(VCO:TTID3),RIGHT(1),TRN
                           LINE,AT(9,130,282,0),USE(?Line1:2),COLOR(COLOR:Black)
                         END
                         TAB('&2) Transporter Rates'),USE(?Tab:2)
                           LIST,AT(9,22,283,146),USE(?Browse:2),HVSCROLL,FORMAT('52R(2)|M~Minimium Load~C(0)@n-11.' & |
  '0@46R(1)|M~Base Charge~C(0)@n-13.2@48R(1)|M~Per Rate~C(0)@n-10.2@'),FROM(Queue:Browse:2), |
  IMM,MSG('Browsing the TripSheets file')
                         END
                         TAB('&3) Manifest'),USE(?Tab:3)
                           LIST,AT(8,22,283,147),USE(?Browse:4),HVSCROLL,FORMAT('30R(2)|M~MID~C(0)@n_10@30R(2)|M~B' & |
  'ID~C(0)@n_10@30R(2)|M~TID~C(0)@n_10@64R(1)|M~Cost~C(0)@n-14.2@48R(1)|M~Rate~C(0)@n-1' & |
  '0.2@36R(1)|M~VAT Rate~C(0)@n-7.2@24R(2)|M~State~C(0)@n3@44R(2)|M~Depart Date~C(0)@d6' & |
  '@44R(2)|M~Depart Time~C(0)@t7@44R(2)|M~ETA Date~C(0)@d6@'),FROM(Queue:Browse:4),IMM,MSG('Browsing t' & |
  'he TripSheets file')
                         END
                         TAB('&4) Trip Sheets'),USE(?Tab:4)
                           LIST,AT(8,22,283,146),USE(?Browse:6),HVSCROLL,FORMAT('80R(2)|M~BID~C(0)@n_10@80R(2)|M~D' & |
  'epart Date~C(0)@d6@80R(2)|M~Depart Time~C(0)@t7@80R(2)|M~Returned Date~C(0)@d6@80R(2' & |
  ')|M~Returned Time~C(0)@t7@80L(2)|M~Notes~L(2)@s255@'),FROM(Queue:Browse:6),IMM,MSG('Browsing t' & |
  'he TripSheets file')
                         END
                       END
                       BUTTON('&OK'),AT(196,174,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(248,174,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       CHECK(' &Archived'),AT(250,6),USE(VCO:Archived),MSG('Is this vehicle composition archived?'), |
  TIP('Is this vehicle composition archived?<0DH,0AH>Archived vehicle compositions cann' & |
  'ot be used on new Manifests')
                       BUTTON('&Help'),AT(108,175,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       PROMPT('VCID:'),AT(4,177),USE(?VCO:VCID:Prompt)
                       STRING(@n_10),AT(27,177),USE(VCO:VCID),RIGHT(1)
                     END

BRW2::LastSortOrder       BYTE
BRW4::LastSortOrder       BYTE
BRW6::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW6                 CLASS(BrowseClass)                    ! Browse using ?Browse:6
Q                      &Queue:Browse:6                !Reference to browse queue
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Vehicle_CompName                ROUTINE
    IF CLIP(VCO:CompositionName) = '' OR CLIP(VCO:CompositionName) = CLIP(Transporter1) & ' - ' |
            OR CLIP(VCO:CompositionName) = ' - ' & LOC:TruckTrailer
       VCO:CompositionName      = CLIP(Transporter1) & ' - ' & LOC:TruckTrailer
    .

    ! Check validity of all entries
    IF TRU:TTID = VCO:TTID0 AND VCO:TTID0 ~= 0
       IF TRU:Type ~= 0 AND TRU:Type ~= 2
          MESSAGE('Horse or Combination required for Truck / Trailer.', 'Wrong Vehicle Type', ICON:Hand)
          CLEAR(VCO:TTID0)
          CLEAR(TruckTrailer)
          CLEAR(L_CG:Capacity)
    .  .

    IF TRU:TTID = VCO:TTID1 AND VCO:TTID1 ~= 0
       IF TRU:Type ~= 1 
          MESSAGE('Trailer required for Trailer 1.', 'Wrong Vehicle Type', ICON:Hand)
          CLEAR(VCO:TTID1)
          CLEAR(Trailer1)
          CLEAR(L_CG:Capacity1)
    .  .

    IF TRU:TTID = VCO:TTID2 AND VCO:TTID2 ~= 0
       IF TRU:Type ~= 1 
          MESSAGE('Trailer required for Trailer 2.', 'Wrong Vehicle Type', ICON:Hand)
          CLEAR(VCO:TTID2)
          CLEAR(Trailer2)
          CLEAR(L_CG:Capacity2)
    .  .

    IF TRU:TTID = VCO:TTID3 AND VCO:TTID3 ~= 0
       IF TRU:Type ~= 1 
          MESSAGE('Trailer required for Trailer 3.', 'Wrong Vehicle Type', ICON:Hand)
          CLEAR(VCO:TTID3)
          CLEAR(Trailer3)
          CLEAR(L_CG:Capacity3)
    .  .

    IF VCO:TTID1 = VCO:TTID2 OR VCO:TTID2 = VCO:TTID3 OR VCO:TTID1 = VCO:TTID3
       IF VCO:TTID1 = VCO:TTID3 AND VCO:TTID3 ~= 0
          MESSAGE('Can''t duplicate Trailer.', 'Duplicated Vehicle', ICON:Hand)
          CLEAR(VCO:TTID3)
          CLEAR(Trailer3)
          CLEAR(L_CG:Capacity3)
       .
       IF VCO:TTID2 = VCO:TTID3 AND VCO:TTID3 ~= 0
          MESSAGE('Can''t duplicate Trailer.', 'Duplicated Vehicle', ICON:Hand)
          CLEAR(VCO:TTID3)
          CLEAR(Trailer3)
          CLEAR(L_CG:Capacity3)
       .
       IF VCO:TTID1 = VCO:TTID2 AND VCO:TTID2 ~= 0
          MESSAGE('Can''t duplicate Trailer.', 'Duplicated Vehicle', ICON:Hand)
          CLEAR(VCO:TTID2)
          CLEAR(Trailer2)
          CLEAR(L_CG:Capacity2)
    .  .

    L_CG:TotalCapacity      = L_CG:Capacity + L_CG:Capacity1 + L_CG:Capacity2 + L_CG:Capacity3

    IF VCO:Capacity = LOC:Prev_Capacity
       LOC:Prev_Capacity    = L_CG:TotalCapacity
       VCO:Capacity         = L_CG:TotalCapacity
    .

    DO Set_Makes_Models

    DISPLAY
    EXIT
Set_Makes_Models                ROUTINE
    CLEAR(LOC:Make_Model)

    VMM:VMMID           = L_MM:VMMID
    IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
       L_MM:MakeModel   = VMM:MakeModel
    .
    VMM:VMMID           = L_MM:VMMID1
    IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
       L_MM:MakeModel1  = VMM:MakeModel
    .
    VMM:VMMID           = L_MM:VMMID2
    IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
       L_MM:MakeModel2  = VMM:MakeModel
    .
    VMM:VMMID           = L_MM:VMMID3
    IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
       L_MM:MakeModel3  = VMM:MakeModel
    .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Vehicle Composition Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Vehicle Comp. Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_VehicleComposition')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Transporter:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:VehicleComposition)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(VCO:Record,History::VCO:Record)
  SELF.AddHistoryField(?VCO:TID,2)
  SELF.AddHistoryField(?VCO:ShowForAllTransporters,9)
  SELF.AddHistoryField(?VCO:CompositionName,3)
  SELF.AddHistoryField(?VCO:Capacity,8)
  SELF.AddHistoryField(?VCO:TTID0,4)
  SELF.AddHistoryField(?VCO:TTID1,5)
  SELF.AddHistoryField(?VCO:TTID2,6)
  SELF.AddHistoryField(?VCO:TTID3,7)
  SELF.AddHistoryField(?VCO:Archived,10)
  SELF.AddHistoryField(?VCO:VCID,1)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Manifest.SetOpenRelated()
  Relate:Manifest.Open                            ! File Manifest used by this procedure, so make sure it's RelationManager is open
  Access:VehicleComposition.UseFile               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailer.UseFile                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleMakeModel.UseFile                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:VehicleComposition
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:__RatesTransporter,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:Manifest,SELF) ! Initialize the browse manager
  BRW6.Init(?Browse:6,Queue:Browse:6.ViewPosition,BRW6::View:Browse,Queue:Browse:6,Relate:TripSheets,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?Transporter1{PROP:ReadOnly} = True
    DISABLE(?Button_SetName)
    ?VCO:CompositionName{PROP:ReadOnly} = True
    ?VCO:Capacity{PROP:ReadOnly} = True
    DISABLE(?CallLookup:2)
    ?LOC:TruckTrailer{PROP:ReadOnly} = True
    DISABLE(?CallLookup:3)
    ?Trailer1{PROP:ReadOnly} = True
    DISABLE(?CallLookup:4)
    ?Trailer2{PROP:ReadOnly} = True
    DISABLE(?CallLookup:5)
    ?Trailer3{PROP:ReadOnly} = True
  END
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon TRRA:VCID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,TRRA:FKey_VCID) ! Add the sort order for TRRA:FKey_VCID for sort order 1
  BRW2.AddRange(TRRA:VCID,Relate:__RatesTransporter,Relate:VehicleComposition) ! Add file relationship range limit for sort order 1
  BRW2.AddField(TRRA:MinimiumLoad,BRW2.Q.TRRA:MinimiumLoad) ! Field TRRA:MinimiumLoad is a hot field or requires assignment from browse
  BRW2.AddField(TRRA:BaseCharge,BRW2.Q.TRRA:BaseCharge) ! Field TRRA:BaseCharge is a hot field or requires assignment from browse
  BRW2.AddField(TRRA:PerRate,BRW2.Q.TRRA:PerRate) ! Field TRRA:PerRate is a hot field or requires assignment from browse
  BRW2.AddField(TRRA:TRID,BRW2.Q.TRRA:TRID)       ! Field TRRA:TRID is a hot field or requires assignment from browse
  BRW2.AddField(TRRA:VCID,BRW2.Q.TRRA:VCID)       ! Field TRRA:VCID is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon MAN:VCID for sort order 1
  BRW4.AddSortOrder(BRW4::Sort0:StepClass,MAN:FKey_VCID) ! Add the sort order for MAN:FKey_VCID for sort order 1
  BRW4.AddRange(MAN:VCID,Relate:Manifest,Relate:VehicleComposition) ! Add file relationship range limit for sort order 1
  BRW4.AddField(MAN:MID,BRW4.Q.MAN:MID)           ! Field MAN:MID is a hot field or requires assignment from browse
  BRW4.AddField(MAN:BID,BRW4.Q.MAN:BID)           ! Field MAN:BID is a hot field or requires assignment from browse
  BRW4.AddField(MAN:TID,BRW4.Q.MAN:TID)           ! Field MAN:TID is a hot field or requires assignment from browse
  BRW4.AddField(MAN:Cost,BRW4.Q.MAN:Cost)         ! Field MAN:Cost is a hot field or requires assignment from browse
  BRW4.AddField(MAN:Rate,BRW4.Q.MAN:Rate)         ! Field MAN:Rate is a hot field or requires assignment from browse
  BRW4.AddField(MAN:VATRate,BRW4.Q.MAN:VATRate)   ! Field MAN:VATRate is a hot field or requires assignment from browse
  BRW4.AddField(MAN:State,BRW4.Q.MAN:State)       ! Field MAN:State is a hot field or requires assignment from browse
  BRW4.AddField(MAN:DepartDate,BRW4.Q.MAN:DepartDate) ! Field MAN:DepartDate is a hot field or requires assignment from browse
  BRW4.AddField(MAN:DepartTime,BRW4.Q.MAN:DepartTime) ! Field MAN:DepartTime is a hot field or requires assignment from browse
  BRW4.AddField(MAN:ETADate,BRW4.Q.MAN:ETADate)   ! Field MAN:ETADate is a hot field or requires assignment from browse
  BRW4.AddField(MAN:VCID,BRW4.Q.MAN:VCID)         ! Field MAN:VCID is a hot field or requires assignment from browse
  BRW6.Q &= Queue:Browse:6
  BRW6::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon TRI:VCID for sort order 1
  BRW6.AddSortOrder(BRW6::Sort0:StepClass,TRI:FKey_VCID) ! Add the sort order for TRI:FKey_VCID for sort order 1
  BRW6.AddRange(TRI:VCID,Relate:TripSheets,Relate:VehicleComposition) ! Add file relationship range limit for sort order 1
  BRW6.AddField(TRI:BID,BRW6.Q.TRI:BID)           ! Field TRI:BID is a hot field or requires assignment from browse
  BRW6.AddField(TRI:DepartDate,BRW6.Q.TRI:DepartDate) ! Field TRI:DepartDate is a hot field or requires assignment from browse
  BRW6.AddField(TRI:DepartTime,BRW6.Q.TRI:DepartTime) ! Field TRI:DepartTime is a hot field or requires assignment from browse
  BRW6.AddField(TRI:ReturnedDate,BRW6.Q.TRI:ReturnedDate) ! Field TRI:ReturnedDate is a hot field or requires assignment from browse
  BRW6.AddField(TRI:ReturnedTime,BRW6.Q.TRI:ReturnedTime) ! Field TRI:ReturnedTime is a hot field or requires assignment from browse
  BRW6.AddField(TRI:Notes,BRW6.Q.TRI:Notes)       ! Field TRI:Notes is a hot field or requires assignment from browse
  BRW6.AddField(TRI:TRID,BRW6.Q.TRI:TRID)         ! Field TRI:TRID is a hot field or requires assignment from browse
  BRW6.AddField(TRI:VCID,BRW6.Q.TRI:VCID)         ! Field TRI:VCID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
      IF SELF.Request = InsertRecord AND VCO:TID = 0
         VCO:TID  = p:TID
      .
  
      TRA:TID                 = VCO:TID
      IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
         Transporter1         = TRA:TransporterName
      .
  
  
      TRU:TTID                = VCO:TTID0
      IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
         LOC:TruckTrailer     = TRU:Registration
         L_CG:Capacity        = TRU:Capacity
         L_MM:VMMID           = TRU:VMMID
      .
  
      TRU:TTID                = VCO:TTID1
      IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
         Trailer1             = TRU:Registration
         L_CG:Capacity1       = TRU:Capacity
         L_MM:VMMID1          = TRU:VMMID
      .
  
      TRU:TTID                = VCO:TTID2
      IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
         Trailer2             = TRU:Registration
         L_CG:Capacity2       = TRU:Capacity
         L_MM:VMMID2          = TRU:VMMID
      .
  
      TRU:TTID                = VCO:TTID3
      IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
         Trailer3             = TRU:Registration
         L_CG:Capacity3       = TRU:Capacity
         L_MM:VMMID3          = TRU:VMMID
      .
  
  
      L_CG:TotalCapacity      = L_CG:Capacity + L_CG:Capacity1 + L_CG:Capacity2 + L_CG:Capacity3
  
      DO Set_Makes_Models
  IF ?VCO:Archived{Prop:Checked}
    VCO:Archived_Date = TODAY()
    VCO:Archived_Time = CLOCK()
  END
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_VehicleComposition',1,?Browse:2,2,BRW2::PopupTextExt,Queue:Browse:2,3,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW4::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW4::FormatManager.Init('MANTRNIS','Update_VehicleComposition',1,?Browse:4,4,BRW4::PopupTextExt,Queue:Browse:4,10,LFM_CFile,LFM_CFile.Record)
  BRW4::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW6::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW6::FormatManager.Init('MANTRNIS','Update_VehicleComposition',1,?Browse:6,6,BRW6::PopupTextExt,Queue:Browse:6,6,LFM_CFile,LFM_CFile.Record)
  BRW6::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Manifest.Close
  END
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW4::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW6::FormatManager.Kill() 
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Transporter
      Browse_TruckTrailer(0,2)
      Browse_TruckTrailer(1)
      Browse_TruckTrailer(1)
      Browse_TruckTrailer(1)
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      TRA:TransporterName = Transporter1
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        Transporter1 = TRA:TransporterName
        VCO:TID = TRA:TID
      END
      ThisWindow.Reset(1)
          DO Vehicle_CompName
    OF ?Transporter1
      IF Transporter1 OR ?Transporter1{PROP:Req}
        TRA:TransporterName = Transporter1
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Transporter1 = TRA:TransporterName
            VCO:TID = TRA:TID
          ELSE
            CLEAR(VCO:TID)
            SELECT(?Transporter1)
            CYCLE
          END
        ELSE
          VCO:TID = TRA:TID
        END
      END
      ThisWindow.Reset()
          DO Vehicle_CompName
    OF ?Button_SetName
      ThisWindow.Update()
          CLEAR(VCO:CompositionName)
          DO Vehicle_CompName
    OF ?CallLookup:2
      ThisWindow.Update()
      TRU:Registration = LOC:TruckTrailer
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LOC:TruckTrailer = TRU:Registration
        VCO:TTID0 = TRU:TTID
        L_CG:Capacity = TRU:Capacity
        L_MM:VMMID = TRU:VMMID
      END
      ThisWindow.Reset(1)
          DO Vehicle_CompName
    OF ?LOC:TruckTrailer
      IF LOC:TruckTrailer OR ?LOC:TruckTrailer{PROP:Req}
        TRU:Registration = LOC:TruckTrailer
        IF Access:TruckTrailer.TryFetch(TRU:Key_Registration)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            LOC:TruckTrailer = TRU:Registration
            VCO:TTID0 = TRU:TTID
            L_CG:Capacity = TRU:Capacity
            L_MM:VMMID = TRU:VMMID
          ELSE
            CLEAR(VCO:TTID0)
            CLEAR(L_CG:Capacity)
            CLEAR(L_MM:VMMID)
            SELECT(?LOC:TruckTrailer)
            CYCLE
          END
        ELSE
          VCO:TTID0 = TRU:TTID
          L_CG:Capacity = TRU:Capacity
          L_MM:VMMID = TRU:VMMID
        END
      END
      ThisWindow.Reset()
          DO Vehicle_CompName
    OF ?CallLookup:3
      ThisWindow.Update()
      TRU:Registration = Trailer1
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        Trailer1 = TRU:Registration
        VCO:TTID1 = TRU:TTID
        L_CG:Capacity1 = TRU:Capacity
        L_MM:VMMID1 = TRU:VMMID
      END
      ThisWindow.Reset(1)
          DO Vehicle_CompName
    OF ?Trailer1
      IF Trailer1 OR ?Trailer1{PROP:Req}
        TRU:Registration = Trailer1
        IF Access:TruckTrailer.TryFetch(TRU:Key_Registration)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            Trailer1 = TRU:Registration
            VCO:TTID1 = TRU:TTID
            L_CG:Capacity1 = TRU:Capacity
            L_MM:VMMID1 = TRU:VMMID
          ELSE
            CLEAR(VCO:TTID1)
            CLEAR(L_CG:Capacity1)
            CLEAR(L_MM:VMMID1)
            SELECT(?Trailer1)
            CYCLE
          END
        ELSE
          VCO:TTID1 = TRU:TTID
          L_CG:Capacity1 = TRU:Capacity
          L_MM:VMMID1 = TRU:VMMID
        END
      END
      ThisWindow.Reset()
          DO Vehicle_CompName
    OF ?CallLookup:4
      ThisWindow.Update()
      TRU:Registration = Trailer2
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        Trailer2 = TRU:Registration
        VCO:TTID2 = TRU:TTID
        L_CG:Capacity2 = TRU:Capacity
        L_MM:VMMID2 = TRU:VMMID
      END
      ThisWindow.Reset(1)
          DO Vehicle_CompName
    OF ?Trailer2
      IF Trailer2 OR ?Trailer2{PROP:Req}
        TRU:Registration = Trailer2
        IF Access:TruckTrailer.TryFetch(TRU:Key_Registration)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            Trailer2 = TRU:Registration
            VCO:TTID2 = TRU:TTID
            L_CG:Capacity2 = TRU:Capacity
            L_MM:VMMID2 = TRU:VMMID
          ELSE
            CLEAR(VCO:TTID2)
            CLEAR(L_CG:Capacity2)
            CLEAR(L_MM:VMMID2)
            SELECT(?Trailer2)
            CYCLE
          END
        ELSE
          VCO:TTID2 = TRU:TTID
          L_CG:Capacity2 = TRU:Capacity
          L_MM:VMMID2 = TRU:VMMID
        END
      END
      ThisWindow.Reset()
          DO Vehicle_CompName
    OF ?CallLookup:5
      ThisWindow.Update()
      TRU:Registration = Trailer3
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        Trailer3 = TRU:Registration
        VCO:TTID3 = TRU:TTID
        L_CG:Capacity3 = TRU:Capacity
        L_MM:VMMID3 = TRU:VMMID
      END
      ThisWindow.Reset(1)
          DO Vehicle_CompName
    OF ?Trailer3
      IF Trailer3 OR ?Trailer3{PROP:Req}
        TRU:Registration = Trailer3
        IF Access:TruckTrailer.TryFetch(TRU:Key_Registration)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            Trailer3 = TRU:Registration
            VCO:TTID3 = TRU:TTID
            L_CG:Capacity3 = TRU:Capacity
            L_MM:VMMID3 = TRU:VMMID
          ELSE
            CLEAR(VCO:TTID3)
            CLEAR(L_CG:Capacity3)
            CLEAR(L_MM:VMMID3)
            SELECT(?Trailer3)
            CYCLE
          END
        ELSE
          VCO:TTID3 = TRU:TTID
          L_CG:Capacity3 = TRU:Capacity
          L_MM:VMMID3 = TRU:VMMID
        END
      END
      ThisWindow.Reset()
          DO Vehicle_CompName
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
          IF VCO:Capacity = 0.0
             VCO:Capacity = L_CG:TotalCapacity
          ELSIF VCO:Capacity > L_CG:TotalCapacity
             CASE MESSAGE('The Composition capacity exceeds the total capacity of all the Trucks / Trailers specified for this Composition.||Is this correct?', 'Update Vehicle Composition', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:No
                SELECT(?VCO:Capacity)
                QuickWindow{PROP:AcceptAll}    = FALSE
                CYCLE
          .  .
    OF ?VCO:Archived
      IF ?VCO:Archived{PROP:Checked}
        VCO:Archived_Date = TODAY()
        VCO:Archived_Time = CLOCK()
      END
      ThisWindow.Reset()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END


BRW4.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW4::LastSortOrder <> NewOrder THEN
     BRW4::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW4::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW4.TakeNewSelection PROCEDURE

  CODE
  IF BRW4::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW4::PopupTextExt = ''
        BRW4::PopupChoiceExec = True
        BRW4::FormatManager.MakePopup(BRW4::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW4::PopupTextExt = '|-|' & CLIP(BRW4::PopupTextExt)
        END
        BRW4::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW4::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW4::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW4::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW4::PopupChoiceOn AND BRW4::PopupChoiceExec THEN
     BRW4::PopupChoiceExec = False
     BRW4::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW4::PopupTextExt)
     IF BRW4::FormatManager.DispatchChoice(BRW4::PopupChoice)
     ELSE
     END
  END


BRW6.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW6::LastSortOrder <> NewOrder THEN
     BRW6::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW6::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW6.TakeNewSelection PROCEDURE

  CODE
  IF BRW6::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW6::PopupTextExt = ''
        BRW6::PopupChoiceExec = True
        BRW6::FormatManager.MakePopup(BRW6::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW6::PopupTextExt = '|-|' & CLIP(BRW6::PopupTextExt)
        END
        BRW6::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW6::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW6::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW6::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW6::PopupChoiceOn AND BRW6::PopupChoiceExec THEN
     BRW6::PopupChoiceExec = False
     BRW6::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW6::PopupTextExt)
     IF BRW6::FormatManager.DispatchChoice(BRW6::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

