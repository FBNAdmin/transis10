

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('MANTRNIS005.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_TransporterRates PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
TransporterName      STRING(35)                            ! Transporters Name
VehicleCompositionName STRING(35)                          ! 
JourneyName          STRING(70)                            ! Description
History::TRRA:Record LIKE(TRRA:RECORD),THREAD
QuickWindow          WINDOW('Form Transporter Rates'),AT(,,200,177),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_TransporterRates'),SYSTEM
                       SHEET,AT(4,4,192,153),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('TID:'),AT(138,6),USE(?Prompt7)
                           STRING(@n_10),AT(153,6,44,10),USE(TRRA:TID),RIGHT(1),TRN
                           PROMPT('Transporter:'),AT(9,22),USE(?Transporter:Prompt),TRN
                           BUTTON('...'),AT(78,22,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(97,22,94,10),USE(TransporterName),MSG('Transporters Name'),REQ,TIP('Transporters Name')
                           PROMPT('Vehicle Composition:'),AT(9,46),USE(?VehicleComposition:Prompt),TRN
                           BUTTON('...'),AT(78,46,12,10),USE(?CallLookup:2)
                           ENTRY(@s35),AT(97,46,94,10),USE(VehicleCompositionName),REQ
                           PROMPT('Journey:'),AT(9,60),USE(?Journey:Prompt),TRN
                           BUTTON('...'),AT(78,60,12,10),USE(?CallLookup:3)
                           ENTRY(@s70),AT(97,60,94,10),USE(JourneyName),MSG('Description'),REQ,TIP('Description')
                           PROMPT('Minimium Load:'),AT(9,82),USE(?TRRA:MinimiumLoad:Prompt),TRN
                           ENTRY(@n-11.0),AT(97,82,94,10),USE(TRRA:MinimiumLoad),RIGHT(1),MSG('In Kgs'),TIP('In Kgs')
                           PROMPT('Base Charge:'),AT(9,96),USE(?TRRA:BaseCharge:Prompt),TRN
                           ENTRY(@n-13.2),AT(97,96,94,10),USE(TRRA:BaseCharge),DECIMAL(12)
                           PROMPT('Per kg Rate (above min.):'),AT(9,110),USE(?TRRA:PerRate:Prompt),TRN
                           ENTRY(@n-10.2),AT(97,110,94,10),USE(TRRA:PerRate),DECIMAL(12),MSG('Above Minimium Load'),TIP('Rate per k' & |
  'g above Minimium Load')
                           PROMPT('Effective Date:'),AT(9,138),USE(?TRRA:Effective_Date:Prompt),TRN
                           SPIN(@d5),AT(97,138,60,10),USE(TRRA:Effective_Date),RIGHT(1),MSG('Effective from date'),TIP('Effective from date')
                         END
                       END
                       BUTTON('&OK'),AT(92,162,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(146,162,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,162,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Transporter Rates Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Transporter Rates Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_TransporterRates')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt7
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(TRRA:Record,History::TRRA:Record)
  SELF.AddHistoryField(?TRRA:TID,2)
  SELF.AddHistoryField(?TRRA:MinimiumLoad,5)
  SELF.AddHistoryField(?TRRA:BaseCharge,6)
  SELF.AddHistoryField(?TRRA:PerRate,7)
  SELF.AddHistoryField(?TRRA:Effective_Date,10)
  SELF.AddUpdateFile(Access:__RatesTransporter)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Journeys.Open                                     ! File Journeys used by this procedure, so make sure it's RelationManager is open
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleComposition.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:__RatesTransporter
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?TransporterName{PROP:ReadOnly} = True
    DISABLE(?CallLookup:2)
    ?VehicleCompositionName{PROP:ReadOnly} = True
    DISABLE(?CallLookup:3)
    ?JourneyName{PROP:ReadOnly} = True
    ?TRRA:MinimiumLoad{PROP:ReadOnly} = True
    ?TRRA:BaseCharge{PROP:ReadOnly} = True
    ?TRRA:PerRate{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_TransporterRates',QuickWindow)      ! Restore window settings from non-volatile store
      TransporterName             = TRA:TransporterName
      IF SELF.Request = InsertRecord
      ELSE
         VCO:VCID                 = TRRA:VCID
         IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
            VehicleCompositionName = VCO:CompositionName
         .
  
         JOU:JID                  = TRRA:JID
         IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
            JourneyName           = JOU:Journey
      .  .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Journeys.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_TransporterRates',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  TRRA:Effective_Date = TODAY()
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Transporter
      Browse_VehicleComposition
      Browse_Journeys
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      TRA:TransporterName = TransporterName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        TransporterName = TRA:TransporterName
      END
      ThisWindow.Reset(1)
    OF ?TransporterName
      IF TransporterName OR ?TransporterName{PROP:Req}
        TRA:TransporterName = TransporterName
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            TransporterName = TRA:TransporterName
          ELSE
            SELECT(?TransporterName)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update()
      VCO:CompositionName = VehicleCompositionName
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        VehicleCompositionName = VCO:CompositionName
        TRRA:VCID = VCO:VCID
      END
      ThisWindow.Reset(1)
    OF ?VehicleCompositionName
      IF VehicleCompositionName OR ?VehicleCompositionName{PROP:Req}
        VCO:CompositionName = VehicleCompositionName
        IF Access:VehicleComposition.TryFetch(VCO:Key_Name)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            VehicleCompositionName = VCO:CompositionName
            TRRA:VCID = VCO:VCID
          ELSE
            CLEAR(TRRA:VCID)
            SELECT(?VehicleCompositionName)
            CYCLE
          END
        ELSE
          TRRA:VCID = VCO:VCID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:3
      ThisWindow.Update()
      JOU:Journey = JourneyName
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        JourneyName = JOU:Journey
        TRRA:JID = JOU:JID
      END
      ThisWindow.Reset(1)
    OF ?JourneyName
      IF JourneyName OR ?JourneyName{PROP:Req}
        JOU:Journey = JourneyName
        IF Access:Journeys.TryFetch(JOU:Key_Journey)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            JourneyName = JOU:Journey
            TRRA:JID = JOU:JID
          ELSE
            CLEAR(TRRA:JID)
            SELECT(?JourneyName)
            CYCLE
          END
        ELSE
          TRRA:JID = JOU:JID
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
          IF TRRA:JID = 0
             CASE MESSAGE('You have not specified a Journey.|Unless this is a Rate entry for a local delivery (Trip Sheet) a Journey is required.||Would you like to specify a Journey now?', 'Journey', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
             OF BUTTON:Yes
                SELECT(?JourneyName)
                QuickWindow{PROP:AcceptAll}    = FALSE
                CYCLE
          .  .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Select_VehicleComposition PROCEDURE 

CurrentTab           STRING(80)                            ! 
LOC:Capacity         ULONG                                 ! In Kgs
LOC:Registrations    STRING(150)                           ! 
LOC:MakesModels      STRING(150)                           ! 
LOC:Transporter      STRING(35)                            ! Transporters Name
LOC:Driver           STRING(50)                            ! 
LOC:New_Transporter  ULONG                                 ! 
LOC:Show_Archived    BYTE                                  ! 
BRW1::View:Browse    VIEW(VehicleComposition)
                       PROJECT(VCO:CompositionName)
                       PROJECT(VCO:Capacity)
                       PROJECT(VCO:VCID)
                       PROJECT(VCO:Archived)
                       PROJECT(VCO:TTID0)
                       JOIN(TransporterAlias,'VCO:TID = A_TRA:TID')
                         PROJECT(A_TRA:TransporterName)
                       END
                       JOIN(TRU:PKey_TTID,VCO:TTID0)
                         PROJECT(TRU:Registration)
                         PROJECT(TRU:TTID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
VCO:CompositionName    LIKE(VCO:CompositionName)      !List box control field - type derived from field
A_TRA:TransporterName  LIKE(A_TRA:TransporterName)    !List box control field - type derived from field
TRU:Registration       LIKE(TRU:Registration)         !List box control field - type derived from field
LOC:Capacity           LIKE(LOC:Capacity)             !List box control field - type derived from local data
VCO:Capacity           LIKE(VCO:Capacity)             !List box control field - type derived from field
LOC:Driver             LIKE(LOC:Driver)               !List box control field - type derived from local data
VCO:VCID               LIKE(VCO:VCID)                 !List box control field - type derived from field
VCO:VCID_Icon          LONG                           !Entry's icon ID
VCO:Archived           LIKE(VCO:Archived)             !List box control field - type derived from field
VCO:Archived_Icon      LONG                           !Entry's icon ID
VCO:TTID0              LIKE(VCO:TTID0)                !Browse key field - type derived from field
TRU:TTID               LIKE(TRU:TTID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Vehicle Composition File'),AT(,,340,206),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('BrowseVehicleComposition'),SYSTEM
                       GROUP,AT(4,180,262,24),USE(?Group_Bottom_Left)
                         PROMPT('Registrations:'),AT(4,180),USE(?LOC:Registrations:Prompt)
                         ENTRY(@s150),AT(62,180,203,10),USE(LOC:Registrations),COLOR(00E9E9E9h),READONLY,SKIP
                         PROMPT('Makes && Models:'),AT(4,194),USE(?LOC:MakesModels:Prompt)
                         ENTRY(@s150),AT(62,194,203,10),USE(LOC:MakesModels),COLOR(00E9E9E9h),READONLY,SKIP
                       END
                       CHECK(' Show Archived'),AT(270,180),USE(LOC:Show_Archived)
                       LIST,AT(8,30,323,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Combination Name~C(0)@s3' & |
  '5@66L(2)|M~Transporter Name~C(0)@s35@52L(2)|M~Horse Reg.~C(0)@s20@38R(2)|M~Capacity~' & |
  'C(0)@n13b@40R(2)|M~Capacity~C(0)@n10.0b@60L(2)|M~Driver~C(0)@s50@40R(2)|MI~VCID~C(0)' & |
  '@n_10@12L(2)|MI~Archived~C(0)@p p@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Vehi' & |
  'cleComposition file')
                       BUTTON('&Select'),AT(282,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(230,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       SHEET,AT(4,4,332,172),USE(?CurrentTab)
                         TAB('&1) By Transporter'),USE(?Tab:2)
                           GROUP,AT(97,160,133,12),USE(?Group1),HIDE
                             BUTTON('&Insert'),AT(97,160,42,12),USE(?Insert)
                             BUTTON('&Change'),AT(138,160,42,12),USE(?Change)
                             BUTTON('&Delete'),AT(181,160,42,12),USE(?Delete)
                           END
                           BUTTON('Select Transporter'),AT(9,158,,16),USE(?SelectTransporter),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&2) By Name'),USE(?Tab:3)
                         END
                         TAB('&3) By Truck'),USE(?Tab:4)
                         END
                       END
                       BUTTON('&Close'),AT(287,191,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(286,2,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_VehicleComposition')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Registrations:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Show_Archived',LOC:Show_Archived)     ! Added by: BrowseBox(ABC)
  BIND('LOC:Capacity',LOC:Capacity)               ! Added by: BrowseBox(ABC)
  BIND('LOC:Driver',LOC:Driver)                   ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                         ! File Transporter used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterAlias.Open                    ! File TransporterAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:VehicleComposition,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.FileLoaded = 1                             ! This is a 'file loaded' browse
  BRW1.AddSortOrder(,VCO:Key_Name)                ! Add the sort order for VCO:Key_Name for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)            ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,VCO:CompositionName,1,BRW1) ! Initialize the browse locator using  using key: VCO:Key_Name , VCO:CompositionName
  BRW1.SetFilter('(LOC:Show_Archived = 1 OR VCO:Archived = 0)') ! Apply filter expression to browse
  BRW1.AddSortOrder(,VCO:FKey_TID0)               ! Add the sort order for VCO:FKey_TID0 for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,VCO:TTID0,1,BRW1)     ! Initialize the browse locator using  using key: VCO:FKey_TID0 , VCO:TTID0
  BRW1.SetFilter('(LOC:Show_Archived = 1 OR VCO:Archived = 0)') ! Apply filter expression to browse
  BRW1.AddSortOrder(,VCO:Key_Name)                ! Add the sort order for VCO:Key_Name for sort order 3
  BRW1.SetFilter('((VCO:TID = TRA:TID OR VCO:ShowForAllTransporters = 1) AND (LOC:Show_Archived = 1 OR VCO:Archived = 0))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Show_Archived)           ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(VCO:CompositionName,BRW1.Q.VCO:CompositionName) ! Field VCO:CompositionName is a hot field or requires assignment from browse
  BRW1.AddField(A_TRA:TransporterName,BRW1.Q.A_TRA:TransporterName) ! Field A_TRA:TransporterName is a hot field or requires assignment from browse
  BRW1.AddField(TRU:Registration,BRW1.Q.TRU:Registration) ! Field TRU:Registration is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Capacity,BRW1.Q.LOC:Capacity) ! Field LOC:Capacity is a hot field or requires assignment from browse
  BRW1.AddField(VCO:Capacity,BRW1.Q.VCO:Capacity) ! Field VCO:Capacity is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Driver,BRW1.Q.LOC:Driver)     ! Field LOC:Driver is a hot field or requires assignment from browse
  BRW1.AddField(VCO:VCID,BRW1.Q.VCO:VCID)         ! Field VCO:VCID is a hot field or requires assignment from browse
  BRW1.AddField(VCO:Archived,BRW1.Q.VCO:Archived) ! Field VCO:Archived is a hot field or requires assignment from browse
  BRW1.AddField(VCO:TTID0,BRW1.Q.VCO:TTID0)       ! Field VCO:TTID0 is a hot field or requires assignment from browse
  BRW1.AddField(TRU:TTID,BRW1.Q.TRU:TTID)         ! Field TRU:TTID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Select_VehicleComposition',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                           ! Will call: Update_VehicleComposition
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Select_VehicleComposition',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,10,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Transporter.Close
    Relate:TransporterAlias.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Select_VehicleComposition',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_VehicleComposition
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectTransporter
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_Transporter()
      ThisWindow.Reset
          LOC:New_Transporter     += 1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! (p:VCID, p:Option, p:Truck, p:Additional)
  !    LOC:Capacity        = Get_VehComp_Info( VCO:VCID, 1,, 9 )
     ! lets use the capacity on the record!    26 Feb 14
     
      LOC:Driver          = Get_VehComp_Info( VCO:VCID, 9 )
  
  !    TRA:TID             = VCO:TID
  !    IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
  !       LOC:Transporter  = TRA:TransporterName
  !    .
  
  
  PARENT.SetQueueRecord
  
  SELF.Q.VCO:VCID_Icon = 0
  IF (VCO:Archived = 1)
    SELF.Q.VCO:Archived_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.VCO:Archived_Icon = 1                           ! Set icon from icon list
  END


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>2,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>2,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
      LOC:Registrations   = Get_VehComp_Info( VCO:VCID, 2,, 3 )
      LOC:MakesModels     = Get_VehComp_Info( VCO:VCID, 3 )
      DISPLAY(?LOC:Registrations)
      DISPLAY(?LOC:MakesModels)
  
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Bottom_Left, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Bottom_Left

!!! <summary>
!!! Generated from procedure template - Report
!!! Report the _Invoice File
!!! </summary>
Print_Manifest_Invoices PROCEDURE (ULONG pMID)

Progress:Thermometer BYTE                                  ! 
ReportTitle          CSTRING(101)                          ! 
MID                  ULONG                                 ! Manifest ID
TotalWeightOfManifest DECIMAL(13,4)                        ! 
Gross_Profit         DECIMAL(16,4)                         ! 
InvoiceTotalExVat    DECIMAL(16,4)                         ! 
ExtraLegs            DECIMAL(13,4)                         ! 
DIWeightCost         DECIMAL(16,4)                         ! 
InvoiceGP            DECIMAL(16,4)                         ! 
GP                   DECIMAL(16,4)                         ! 
GPPerCent            DECIMAL(16,4)                         ! 
WeightPerCent        DECIMAL(9,4)                          ! 
GTotal               GROUP,PRE(SUM)                        ! 
DINo                 ULONG                                 ! Delivery Instruction Number
IID                  ULONG                                 ! Invoice Number
Total                DECIMAL(16,4)                         ! 
Documentation        DECIMAL(16,4)                         ! 
FuelSurcharge        DECIMAL(16,4)                         ! 
TollCharge           DECIMAL(11,2)                         ! 
FreightCharge        DECIMAL(16,4)                         ! 
VAT                  DECIMAL(16,4)                         ! 
ExtraLegs            DECIMAL(16,4)                         ! 
Weight               DECIMAL(16,4)                         ! 
WeightPerCent        DECIMAL(16,4)                         ! 
DIWeightCost         DECIMAL(16,4)                         ! 
InvoiceGP            DECIMAL(16,4)                         ! 
GP                   DECIMAL(16,4)                         ! 
GPPerCent            DECIMAL(9,4)                          ! 
                     END                                   ! 
Process:View         VIEW(_Invoice)
                       PROJECT(INV:DINo)
                       PROJECT(INV:Documentation)
                       PROJECT(INV:FreightCharge)
                       PROJECT(INV:FuelSurcharge)
                       PROJECT(INV:IID)
                       PROJECT(INV:MID)
                       PROJECT(INV:TollCharge)
                       PROJECT(INV:Total)
                       PROJECT(INV:VAT)
                       PROJECT(INV:Weight)
                     END
ProgressWindow       WINDOW('Detailed Manifest report'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Detailed Manifest report'),AT(525,2327,7750,10338),PRE(RPT),PAPER(PAPER:A4SMALL),LANDSCAPE, |
  FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT),THOUS
                       HEADER,AT(525,250,10771,2073),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT)
                         STRING('Manifest'),AT(0,20,10260,220),USE(?ReportTitle),FONT('Microsoft Sans Serif',14,COLOR:Black, |
  FONT:bold,CHARSET:DEFAULT),CENTER
                         BOX,AT(0,1437,10760,625),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(552,1448,0,625),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1104,1448,0,625),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(1948,1448,0,625),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(2500,1448,0,625),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(3250,1448,0,625),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(5021,1448,0,625),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(5771,1448,0,625),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         LINE,AT(6583,1448,0,625),USE(?HeaderLine:8),COLOR(COLOR:Black)
                         LINE,AT(7250,1448,0,625),USE(?HeaderLine:9),COLOR(COLOR:Black)
                         LINE,AT(7844,1448,0,625),USE(?HeaderLine:10),COLOR(COLOR:Black)
                         LINE,AT(8583,1448,0,625),USE(?HeaderLine:11),COLOR(COLOR:Black)
                         LINE,AT(9333,1448,0,625),USE(?HeaderLine:12),COLOR(COLOR:Black)
                         LINE,AT(10083,1437,0,625),USE(?HeaderLine:13),COLOR(COLOR:Black)
                         STRING('DI'),AT(52,1483,453,167),USE(?HeaderTitle:1),TRN
                         STRING('Invoice'),AT(604,1483,453,170),USE(?HeaderTitle:2),TRN
                         STRING('Total'),AT(1156,1483,453,170),USE(?HeaderTitle:3),TRN
                         STRING('Doc'),AT(2031,1479,302,170),USE(?HeaderTitle:4),TRN
                         STRING('Fuel'),AT(2573,1479,656,170),USE(?HeaderTitle:5),TRN
                         STRING('Freight'),AT(4229,1479,719,170),USE(?HeaderTitle:6),TRN
                         STRING('VAT'),AT(5073,1479,453,170),USE(?HeaderTitle:7),TRN
                         STRING('Legs Cost'),AT(5812,1479,698,170),USE(?HeaderTitle:8),TRN
                         STRING('Weight'),AT(6615,1479,615,170),USE(?HeaderTitle:9),TRN
                         STRING('Weight %'),AT(7281,1479,542,170),USE(?HeaderTitle:10),TRN
                         STRING('Cost per DI'),AT(7896,1479,615,170),USE(?HeaderTitle:11),TRN
                         STRING('GP per inv.'),AT(8635,1479,771,167),USE(?HeaderTitle:12),TRN
                         STRING('GP per inv.'),AT(9406,1479,604,170),USE(?HeaderTitle:13),TRN
                         STRING('GP % for'),AT(10156,1479,604,170),USE(?HeaderTitle:14),TRN
                         STRING('MID:'),AT(115,469,937),USE(?STRING1)
                         STRING('Cost:'),AT(115,698,937,167),USE(?STRING1:2)
                         STRING('Total Weight:'),AT(115,927,937,167),USE(?STRING1:3)
                         STRING('GP for Manifest:'),AT(115,1156,937,167),USE(?STRING1:4)
                         STRING(@n_10),AT(1583,469),USE(MAN:MID),RIGHT(1)
                         STRING(@n-14.2),AT(1448,698),USE(MAN:Cost),RIGHT(1)
                         STRING(@n-19.2),AT(1156,927),USE(TotalWeightOfManifest,,?TotalWeight:2),RIGHT(1)
                         STRING(@n-19.2),AT(1156,1156),USE(Gross_Profit),RIGHT(12)
                         STRING('Invoice'),AT(10156,1604,604,167),USE(?HeaderTitle:15),TRN
                         STRING('based on'),AT(10156,1729,604,167),USE(?HeaderTitle:16),TRN
                         STRING('Manifest'),AT(10156,1865,604,167),USE(?HeaderTitle:18),TRN
                         STRING('based on'),AT(9406,1604,604,167),USE(?HeaderTitle:19),TRN
                         STRING('weight cost'),AT(9406,1729,604,167),USE(?HeaderTitle:20),TRN
                         STRING('based on'),AT(8635,1604,771,167),USE(?HeaderTitle:21),TRN
                         STRING('Manifest GP'),AT(8625,1740,771,167),USE(?HeaderTitle:22),TRN
                         STRING('based on'),AT(7896,1604,615,167),USE(?HeaderTitle:23),TRN
                         STRING('weight'),AT(7896,1729,615,167),USE(?HeaderTitle:24),TRN
                         STRING('ex VAT'),AT(5812,1604,698,167),USE(?HeaderTitle:25),TRN
                         LINE,AT(4187,1448,0,625),USE(?HeaderLine:14),COLOR(COLOR:Black)
                         STRING('Tolls'),AT(3344,1479,656,167),USE(?HeaderTitle:17),TRN
                       END
Detail                 DETAIL,AT(0,0,10771,292),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(552,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(1104,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(1948,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(2500,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(3250,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(5021,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(5771,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(6583,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                         LINE,AT(7250,0,0,250),USE(?DetailLine:9),COLOR(COLOR:Black)
                         LINE,AT(8583,0,0,250),USE(?DetailLine:10),COLOR(COLOR:Black)
                         LINE,AT(7844,0,0,250),USE(?DetailLine:11),COLOR(COLOR:Black)
                         LINE,AT(9333,0,0,250),USE(?DetailLine:12),COLOR(COLOR:Black)
                         LINE,AT(10083,-9,0,250),USE(?DetailLine:13),COLOR(COLOR:Black)
                         LINE,AT(10750,-9,0,250),USE(?DetailLine:14),COLOR(COLOR:Black)
                         STRING(@n_10),AT(50,50,453,170),USE(INV:DINo),RIGHT
                         STRING(@n_10),AT(603,50,453,170),USE(INV:IID),RIGHT
                         STRING(@n-12.2),AT(1156,31,740,170),USE(INV:Total),RIGHT
                         STRING(@n-14.2),AT(2000,42,427,170),USE(INV:Documentation),RIGHT
                         STRING(@n-10.2),AT(2573,31,600,170),USE(INV:FuelSurcharge),RIGHT
                         STRING(@n-12.2),AT(4260,31,677,170),USE(INV:FreightCharge),RIGHT
                         STRING(@n-14.2),AT(5104,31,594,170),USE(INV:VAT),RIGHT
                         STRING(@N-15.2B),AT(5844,31,600,170),USE(ExtraLegs),RIGHT
                         STRING(@n-11.2),AT(6615,31,604,170),USE(INV:Weight),RIGHT
                         STRING(@N-8.2~%~),AT(7281,31,469,170),USE(WeightPerCent),RIGHT
                         STRING(@N-15.2),AT(7896,31,615,170),USE(DIWeightCost),RIGHT
                         STRING(@N-15.2),AT(8656,31,604,170),USE(InvoiceGP),RIGHT
                         STRING(@N-15.2),AT(9406,31,604,170),USE(GP),RIGHT
                         STRING(@N-8.2~%~),AT(10156,31,500,170),USE(GPPerCent),RIGHT
                         LINE,AT(0,250,10750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         LINE,AT(4187,-9,0,250),USE(?DetailLine:15),COLOR(COLOR:Black)
                         STRING(@n-15.2),AT(3302,-416),USE(INV:TollCharge),DECIMAL(12)
                         STRING(@n-15.2),AT(3417,31,698),USE(INV:TollCharge,,?INV:TollCharge:2),RIGHT
                       END
TotalDetail            DETAIL,AT(0,0,10771,250),USE(?Detail:2)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0:2),COLOR(COLOR:Black)
                         LINE,AT(1104,0,0,250),USE(?DetailLine:16),COLOR(COLOR:Black)
                         LINE,AT(1948,0,0,250),USE(?DetailLine:17),COLOR(COLOR:Black)
                         LINE,AT(2500,-20,0,250),USE(?DetailLine:18),COLOR(COLOR:Black)
                         LINE,AT(3250,-20,0,250),USE(?DetailLine:19),COLOR(COLOR:Black)
                         LINE,AT(5021,-20,0,250),USE(?DetailLine:20),COLOR(COLOR:Black)
                         LINE,AT(5771,0,0,250),USE(?DetailLine:21),COLOR(COLOR:Black)
                         LINE,AT(6583,-20,0,250),USE(?DetailLine:22),COLOR(COLOR:Black)
                         LINE,AT(7250,-20,0,250),USE(?DetailLine:23),COLOR(COLOR:Black)
                         LINE,AT(7844,-20,0,250),USE(?DetailLine:24),COLOR(COLOR:Black)
                         LINE,AT(8583,-20,0,250),USE(?DetailLine:25),COLOR(COLOR:Black)
                         LINE,AT(9333,-41,0,250),USE(?DetailLine:26),COLOR(COLOR:Black)
                         LINE,AT(10083,-20,0,250),USE(?DetailLine:27),COLOR(COLOR:Black)
                         LINE,AT(10750,-20,0,250),USE(?DetailLine:28),COLOR(COLOR:Black)
                         LINE,AT(0,250,10312,0),USE(?DetailEndLine:2),COLOR(COLOR:Black)
                         STRING(@n-12.2),AT(1156,42,740,167),USE(SUM:Total),RIGHT
                         STRING(@n-14.2),AT(2000,52,427,167),USE(SUM:Documentation),RIGHT
                         STRING(@n-12.2),AT(2573,42,600,167),USE(SUM:FuelSurcharge),RIGHT
                         STRING(@n-12.2),AT(4271,42,677,167),USE(SUM:FreightCharge),RIGHT
                         STRING(@n-14.2),AT(5104,42,594,167),USE(SUM:VAT),RIGHT
                         STRING(@N-15.2B),AT(5844,42,600,167),USE(SUM:ExtraLegs),RIGHT
                         STRING(@n-11.2),AT(6625,42,604,167),USE(SUM:Weight),RIGHT
                         STRING(@N-15.2),AT(7896,42,615,167),USE(SUM:DIWeightCost),RIGHT
                         STRING(@N-15.2),AT(8656,42,604,167),USE(SUM:InvoiceGP),RIGHT
                         STRING(@N-15.2),AT(9406,42,604,167),USE(SUM:GP),RIGHT
                         LINE,AT(4187,-20,0,250),USE(?DetailLine:29),COLOR(COLOR:Black)
                         STRING(@n-15.2),AT(3323,31,792),USE(SUM:TollCharge),RIGHT
                       END
                       FOOTER,AT(525,7750,10771,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt:2),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp:2),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt:2),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp:2),FONT('Arial',8,, |
  FONT:regular),TRN
                         STRING(@pPage <<#p),AT(10052,52,700,135),USE(?PageCount:2),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.AskPreview PROCEDURE

  CODE
  PRINT(RPT:TotalDetail)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  MID = pMID
  GlobalErrors.SetProcedureName('Print_Manifest_Invoices')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Manifest.Open                                     ! File Manifest used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  MAN:MID = MID
  Access:Manifest.Fetch(MAN:PKey_MID)
  TotalWeightOfManifest = Get_Manifest_Info(MAN:MID, 2,,, 0)
  Gross_Profit = Get_Manifest_Info(MAN:MID,0,,,0) - MAN:Cost - Get_Manifest_Info(MAN:MID, 3,,, 0) !Del charges ex vat - Cost -  DI legs cost ex vat
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Manifest_Invoices',ProgressWindow)   ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF NOT TargetSelector.ASK(1) THEN
     SELF.Kill()
     RETURN Level:Fatal
  END
  IF NOT TargetSelector.GetPrintSelected() THEN
     SELF.SetReportTarget(TargetSelector.GetReportSelected())
  END
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:_Invoice, ?Progress:PctText, Progress:Thermometer, ProgressMgr, INV:MID)
  ThisReport.AddSortOrder(INV:FKey_MID)
  ThisReport.AddRange(INV:MID,MID)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_Invoice.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Manifest.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Manifest_Invoices',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  Self.Report $ ?ReportTitle{PROP:Text} = GetSQLVAlue('SELECT BranchName  From Branches WHERE BID = '&MAN:BID) &' - Manifest '& MAN:MID & |
                Choose(MAN:CreatedDate~=0,' - '&Format(MAN:CreatedDate,@D18B),'')
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp:2{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp:2{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  InvoiceTotalExVat = INV:Total - INV:VAT
  ExtraLegs = Get_DelLegs_info(INV:DID,1)
  WeightPerCent = INV:Weight / TotalWeightOfManifest * 100.00
  DIWeightCost = INV:Weight / TotalWeightOfManifest * MAN:Cost
  WeightPerCent = INV:Weight / TotalWeightOfManifest * 100.00
  InvoiceGP = INV:Weight / TotalWeightOfManifest * Gross_Profit
  GP = InvoiceTotalExVat - DIWeightCost - ExtraLegs
  GPPerCent = GP / InvoiceTotalExVat * 100.00
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  SUM:Total         += INV:Total
  SUM:Documentation += INV:Documentation
  SUM:FuelSurcharge += INV:FuelSurcharge
  SUM:TollCharge    += INV:TollCharge
  SUM:FreightCharge += INV:FreightCharge
  SUM:VAT           += INV:VAT
  SUM:ExtraLegs     += ExtraLegs
  SUM:Weight        += INV:Weight
  SUM:DIWeightCost  += DIWeightCost
  SUM:InvoiceGP     += InvoiceGP
  SUM:GP            += GP  
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','MANTRNIS','Print_Manifest_Invoices','Print_Manifest_Invoices','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Update_ClientsPayments_h PROCEDURE  (p:CPID)               ! Declare Procedure

  CODE
    GlobalRequest   = ChangeRecord
    Update_ClientsPayments(2, p:CPID)
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Update_Invoice_h     PROCEDURE  (p:IID, p:Mode)            ! Declare Procedure

  CODE
!    IF p:Mode = ChangeRecord
!       GlobalRequest   = ChangeRecord
!    ELSIF p:Mode = ViewRecord
!       GlobalRequest   = ViewRecord
!    .

    GlobalRequest   = p:Mode

    Update_Invoice(p:IID)
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_DeliveryComposition PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
ClientName           STRING(100)                           ! 
LOC:DeliveryComposition_Ret_Group GROUP,PRE(L_DC)          ! Totals so far
Items                ULONG                                 ! Total no. of items
Weight               DECIMAL(15,2)                         ! Total weight
Volume               DECIMAL(12,3)                         ! Total volume
Deliveries           ULONG                                 ! Total deliveries
First_DID            ULONG                                 ! Delivery ID
                     END                                   ! 
BRW5::View:Browse    VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:Charge)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:DELCID)
                       PROJECT(DEL:CID)
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:DIDate             LIKE(DEL:DIDate)               !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
DEL:Charge             LIKE(DEL:Charge)               !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !List box control field - type derived from field
DEL:DELCID             LIKE(DEL:DELCID)               !Browse key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DELC:Record LIKE(DELC:RECORD),THREAD
BRW5::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW5::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW5::PopupChoice    SIGNED                       ! Popup current choice
BRW5::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW5::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Multi Part Deliveries'),AT(,,278,285),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_DeliveryComposition'),SYSTEM
                       PROMPT('CID:'),AT(196,6),USE(?DELC:CID:Prompt),TRN
                       STRING(@n_10),AT(223,6,45,10),USE(DELC:CID),RIGHT(1),TRN
                       SHEET,AT(4,4,269,260),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           GROUP,AT(174,76,94,24),USE(?Group_Extras),HIDE,TRN
                             PROMPT('Items:'),AT(174,76),USE(?DELC:Items:Prompt),TRN
                             ENTRY(@n6),AT(225,76,43,10),USE(DELC:Items),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total no. of items'), |
  READONLY,SKIP,TIP('Total no. of items')
                             PROMPT('Deliveries:'),AT(174,90),USE(?DELC:Deliveries:Prompt),TRN
                             ENTRY(@n6),AT(225,90,43,10),USE(DELC:Deliveries),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total no. ' & |
  'of deliveries'),READONLY,SKIP,TIP('Total no. of deliveries')
                           END
                           PROMPT('Client Name:'),AT(9,24),USE(?ClientName:Prompt),TRN
                           BUTTON('...'),AT(54,24,12,10),USE(?CallLookup)
                           ENTRY(@s100),AT(70,24,198,10),USE(ClientName),REQ,TIP('Client name')
                           PROMPT('Reference:'),AT(9,44),USE(?DELC:Reference:Prompt),TRN
                           ENTRY(@s35),AT(70,44,198,10),USE(DELC:Reference),MSG('Reference for this Delivery Composition'), |
  TIP('Reference for this Delivery Composition')
                           PROMPT('Total Weight:'),AT(9,76),USE(?DELC:Weight:Prompt),TRN
                           ENTRY(@n-21.2),AT(70,76,72,10),USE(DELC:Weight),RIGHT(1),MSG('Total weight'),REQ,TIP('Total weight')
                           PROMPT('Total Volume:'),AT(9,90),USE(?DELC:Volume:Prompt),HIDE,TRN
                           ENTRY(@n-16.3),AT(70,90,72,10),USE(DELC:Volume),RIGHT(1),HIDE,MSG('Total volume'),TIP('Total volume')
                           GROUP,AT(62,122,211,39),USE(?Group_States),DISABLE
                             CHECK(' Deliveries Loaded'),AT(70,125,80,8),USE(DELC:DeliveriesLoaded),MSG('All deliver' & |
  'ies have been loaded'),TIP('All deliveries have been loaded'),TRN
                             CHECK(' Manifested'),AT(70,138,80,8),USE(DELC:Manifested),MSG('All deliveries have been' & |
  ' manifested'),TIP('All deliveries have been manifested'),TRN
                             CHECK(' Delivered'),AT(70,149,80,8),USE(DELC:Delivered),MSG('All deliveries have been delivered'), |
  TIP('All deliveries have been delivered'),TRN
                           END
                           PROMPT('Date Added:'),AT(9,170),USE(?DELC:DateAdded:Prompt),TRN
                           ENTRY(@d5),AT(70,170,67,10),USE(DELC:DateAdded),COLOR(00E9E9E9h),FLAT,SKIP
                           PROMPT('Time Added:'),AT(157,170),USE(?DELC:TimeAdded:Prompt),TRN
                           ENTRY(@t7),AT(201,170,67,10),USE(DELC:TimeAdded),COLOR(00E9E9E9h),FLAT,SKIP
                           LINE,AT(10,188,258,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Totals on DI''s'),AT(10,194),USE(?Prompt13),TRN
                           PANEL,AT(10,204,257,55),USE(?Panel1),BEVEL(-1,-1)
                           PROMPT('Items:'),AT(18,242),USE(?L_DC:Items:Prompt),TRN
                           ENTRY(@n13),AT(70,242,67,10),USE(L_DC:Items),RIGHT(1),COLOR(00E9E9E9h),MSG('Total no. of items'), |
  READONLY,SKIP,TIP('Total no. of items')
                           PROMPT('Weight:'),AT(18,210),USE(?L_DC:Weight:Prompt),TRN
                           ENTRY(@n-21.2),AT(70,210,67,10),USE(L_DC:Weight),RIGHT(1),COLOR(00E9E9E9h),MSG('Total weight'), |
  READONLY,SKIP,TIP('Total weight')
                           PROMPT('Deliveries:'),AT(18,226),USE(?L_DC:Deliveries:Prompt),TRN
                           ENTRY(@n13),AT(70,226,67,10),USE(L_DC:Deliveries),RIGHT(1),COLOR(00E9E9E9h),MSG('Total no. ' & |
  'of deliveries'),READONLY,SKIP,TIP('Total no. of deliveries')
                         END
                         TAB('&2) Deliveries'),USE(?Tab2)
                           LIST,AT(9,21,261,222),USE(?List),HVSCROLL,FORMAT('40R(2)|M~DI No.~L@n_10@36R(2)|M~DI Da' & |
  'te~L@d5b@80L(2)|M~Client Name~@s100@50L(2)|M~Client Ref.~@s30@50R(2)|M~Charge~L@n-15' & |
  '.2@40R(2)|M~DID~L@n_10@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           BUTTON('&View'),AT(9,248,42,12),USE(?View)
                           BUTTON('&Insert'),AT(145,248,42,12),USE(?Insert)
                           BUTTON('&Change'),AT(187,248,42,12),USE(?Change)
                           BUTTON('&Delete'),AT(229,248,42,12),USE(?Delete)
                         END
                       END
                       BUTTON('&OK'),AT(170,268,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(224,268,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,268,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW5::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW5                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Delivery Composition Record'
  OF InsertRecord
    ActionMessage = 'Delivery Comp. Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Delivery Comp. Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_DeliveryComposition')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DELC:CID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DELC:Record,History::DELC:Record)
  SELF.AddHistoryField(?DELC:CID,2)
  SELF.AddHistoryField(?DELC:Items,9)
  SELF.AddHistoryField(?DELC:Deliveries,12)
  SELF.AddHistoryField(?DELC:Reference,3)
  SELF.AddHistoryField(?DELC:Weight,10)
  SELF.AddHistoryField(?DELC:Volume,11)
  SELF.AddHistoryField(?DELC:DeliveriesLoaded,13)
  SELF.AddHistoryField(?DELC:Manifested,14)
  SELF.AddHistoryField(?DELC:Delivered,15)
  SELF.AddHistoryField(?DELC:DateAdded,6)
  SELF.AddHistoryField(?DELC:TimeAdded,7)
  SELF.AddUpdateFile(Access:DeliveryComposition)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Clients.Open                             ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveryComposition
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?ClientName{PROP:ReadOnly} = True
    ?DELC:Reference{PROP:ReadOnly} = True
    ?DELC:Weight{PROP:ReadOnly} = True
    ?DELC:Volume{PROP:ReadOnly} = True
    ?DELC:DateAdded{PROP:ReadOnly} = True
    ?DELC:TimeAdded{PROP:ReadOnly} = True
    ?L_DC:Items{PROP:ReadOnly} = True
    ?L_DC:Weight{PROP:ReadOnly} = True
    ?L_DC:Deliveries{PROP:ReadOnly} = True
    DISABLE(?View)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,DEL:FKey_DELCID)             ! Add the sort order for DEL:FKey_DELCID for sort order 1
  BRW5.AddRange(DEL:DELCID,DELC:DELCID)           ! Add single value range limit for sort order 1
  BRW5.AddLocator(BRW5::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW5::Sort0:Locator.Init(,DEL:DELCID,1,BRW5)    ! Initialize the browse locator using  using key: DEL:FKey_DELCID , DEL:DELCID
  BRW5.AppendOrder('+DEL:DINo')                   ! Append an additional sort order
  BRW5.AddField(DEL:DINo,BRW5.Q.DEL:DINo)         ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW5.AddField(DEL:DIDate,BRW5.Q.DEL:DIDate)     ! Field DEL:DIDate is a hot field or requires assignment from browse
  BRW5.AddField(CLI:ClientName,BRW5.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW5.AddField(DEL:ClientReference,BRW5.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW5.AddField(DEL:Charge,BRW5.Q.DEL:Charge)     ! Field DEL:Charge is a hot field or requires assignment from browse
  BRW5.AddField(DEL:DID,BRW5.Q.DEL:DID)           ! Field DEL:DID is a hot field or requires assignment from browse
  BRW5.AddField(DEL:DELCID,BRW5.Q.DEL:DELCID)     ! Field DEL:DELCID is a hot field or requires assignment from browse
  BRW5.AddField(CLI:CID,BRW5.Q.CLI:CID)           ! Field CLI:CID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_DeliveryComposition',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW5.AskProcedure = 2                           ! Will call: Update_Deliveries
  BRW5.AddToolbarTarget(Toolbar)                  ! Browse accepts toolbar control
  BRW5.ToolbarItem.HelpButton = ?Help
      CLI:CID         = DELC:CID
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         ClientName   = CLI:ClientName
      .
      LOC:DeliveryComposition_Ret_Group   = Get_Delivery_Info(DELC:DELCID)
  BRW5::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW5::FormatManager.Init('MANTRNIS','Update_DeliveryComposition',1,?List,5,BRW5::PopupTextExt,Queue:Browse,6,LFM_CFile,LFM_CFile.Record)
  BRW5::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  ! List Format Manager destructor
  BRW5::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_DeliveryComposition',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  DELC:DateAdded = TODAY()
  DELC:TimeAdded = CLOCK()
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Clients
      Update_Deliveries
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        ClientName = CLI:ClientName
        DELC:CID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?ClientName
      IF ClientName OR ?ClientName{PROP:Req}
        CLI:ClientName = ClientName
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            ClientName = CLI:ClientName
            DELC:CID = CLI:CID
          ELSE
            CLEAR(DELC:CID)
            SELECT(?ClientName)
            CYCLE
          END
        ELSE
          DELC:CID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
          IF DELC:Weight <= 0.0
             MESSAGE('Please specify a total Weight.', 'Delivery Composition', ICON:Exclamation)
             QuickWindow{PROP:AcceptAll}  = FALSE
             SELECT(?DELC:Weight)
             CYCLE
          .
!          IF DELC:Items <= 0
!             QuickWindow{PROP:AcceptAll}  = FALSE
!             SELECT(?DELC:Items)
!             CYCLE
!          .
!          IF DELC:Deliveries <= 0
!             QuickWindow{PROP:AcceptAll}  = FALSE
!             SELECT(?DELC:Deliveries)
!             CYCLE
!          .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          DELC:CID    = DEL:CID
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END
  SELF.ViewControl = ?View                                 ! Setup the control used to initiate view only mode


BRW5.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW5::LastSortOrder <> NewOrder THEN
     BRW5::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW5::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  IF BRW5::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW5::PopupTextExt = ''
        BRW5::PopupChoiceExec = True
        BRW5::FormatManager.MakePopup(BRW5::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW5::PopupTextExt = '|-|' & CLIP(BRW5::PopupTextExt)
        END
        BRW5::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW5::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW5::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW5::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW5::PopupChoiceOn AND BRW5::PopupChoiceExec THEN
     BRW5::PopupChoiceExec = False
     BRW5::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW5::PopupTextExt)
     IF BRW5::FormatManager.DispatchChoice(BRW5::PopupChoice)
     ELSE
     END
  END

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_DeliveryCompositions PROCEDURE 

CurrentTab           STRING(80)                            ! 
LOC:DELCID           ULONG                                 ! Delivery Composition ID
LOC:DeliveryComposition_Ret_Group GROUP,PRE()              ! Totals so far
L_DC:Items           ULONG                                 ! Total no. of items
L_DC:Weight          DECIMAL(15,2)                         ! Total weight
L_DC:Volume          DECIMAL(12,3)                         ! Total volume
L_DC:Deliveries      ULONG                                 ! Total deliveries
L_DC:First_DID       ULONG                                 ! Delivery ID
                     END                                   ! 
BRW1::View:Browse    VIEW(DeliveryComposition)
                       PROJECT(DELC:DateAdded)
                       PROJECT(DELC:Reference)
                       PROJECT(DELC:Weight)
                       PROJECT(DELC:DELCID)
                       PROJECT(DELC:CID)
                       JOIN(CLI:PKey_CID,DELC:CID)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DELC:DateAdded         LIKE(DELC:DateAdded)           !List box control field - type derived from field
DELC:Reference         LIKE(DELC:Reference)           !List box control field - type derived from field
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
DELC:Weight            LIKE(DELC:Weight)              !List box control field - type derived from field
L_DC:Items             LIKE(L_DC:Items)               !List box control field - type derived from local data
L_DC:Weight            LIKE(L_DC:Weight)              !List box control field - type derived from local data
L_DC:Deliveries        LIKE(L_DC:Deliveries)          !List box control field - type derived from local data
DELC:DELCID            LIKE(DELC:DELCID)              !List box control field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Multi Part Deliveries File'),AT(,,361,249),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_DeliveryCompositions'),SYSTEM
                       LIST,AT(8,20,345,186),USE(?Browse:1),HVSCROLL,FORMAT('38R(2)|M~Date~C(0)@d5@50L(1)|M~Mu' & |
  'lti Part Ref.~C(0)@s35@36R(2)|M~Client No.~C(0)@n_10b@80L(2)|M~Client Name~C(0)@s100' & |
  '@52R(1)|M~Total Weight~C(0)@n-21.2@[29R(1)|M~Items~C(0)@n13@52R(1)|M~Weight~C(0)@n-2' & |
  '1.2@52R(1)|M~Deliveries~C(0)@n13@](121)|M~DIs Total Captured~40R(1)|M~DELCID~C(0)@n_10@'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing the DeliveryComposition file')
                       BUTTON('&Select'),AT(8,210,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(144,210,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(198,210,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(250,210,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(304,210,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,354,225),USE(?CurrentTab)
                         TAB('&1) By Date && Time'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(309,232,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,232,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop
  RETURN(LOC:DELCID)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_DeliveryCompositions')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_DC:Items',L_DC:Items)                   ! Added by: BrowseBox(ABC)
  BIND('L_DC:Weight',L_DC:Weight)                 ! Added by: BrowseBox(ABC)
  BIND('L_DC:Deliveries',L_DC:Deliveries)         ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:DeliveryComposition.Open                 ! File DeliveryComposition used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:DeliveryComposition,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,)                            ! Add the sort order for  for sort order 1
  BRW1.AppendOrder('-DELC:DateAdded')             ! Append an additional sort order
  BRW1.AddField(DELC:DateAdded,BRW1.Q.DELC:DateAdded) ! Field DELC:DateAdded is a hot field or requires assignment from browse
  BRW1.AddField(DELC:Reference,BRW1.Q.DELC:Reference) ! Field DELC:Reference is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientNo,BRW1.Q.CLI:ClientNo) ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientName,BRW1.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW1.AddField(DELC:Weight,BRW1.Q.DELC:Weight)   ! Field DELC:Weight is a hot field or requires assignment from browse
  BRW1.AddField(L_DC:Items,BRW1.Q.L_DC:Items)     ! Field L_DC:Items is a hot field or requires assignment from browse
  BRW1.AddField(L_DC:Weight,BRW1.Q.L_DC:Weight)   ! Field L_DC:Weight is a hot field or requires assignment from browse
  BRW1.AddField(L_DC:Deliveries,BRW1.Q.L_DC:Deliveries) ! Field L_DC:Deliveries is a hot field or requires assignment from browse
  BRW1.AddField(DELC:DELCID,BRW1.Q.DELC:DELCID)   ! Field DELC:DELCID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:CID,BRW1.Q.CLI:CID)           ! Field CLI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_DeliveryCompositions',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                           ! Will call: Update_DeliveryComposition
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_DeliveryCompositions',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,9,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryComposition.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_DeliveryCompositions',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_DeliveryComposition
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Select:2
      ThisWindow.Update()
      BRW1.UpdateViewRecord()
          LOC:DELCID  = DELC:DELCID
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.SetQueueRecord PROCEDURE

  CODE
      CLEAR(LOC:DeliveryComposition_Ret_Group)
      LOC:DeliveryComposition_Ret_Group   = Get_Delivery_Info(DELC:DELCID)
  PARENT.SetQueueRecord
  


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_DeliveryProgress PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Labels_Group     GROUP,PRE(L_LG)                       ! 
ContactName          STRING(35)                            ! Contacts Name
DeliveryStatus       STRING(35)                            ! 
                     END                                   ! 
FloorAID             ULONG                                 ! Address ID - note once used certain information should not be changeable, such as the suburb
Floor                STRING(35)                            ! Floor Name
FDCB8::View:FileDropCombo VIEW(AddressContacts)
                       PROJECT(ADDC:ContactName)
                       PROJECT(ADDC:ACID)
                     END
FDB5::View:FileDrop  VIEW(DeliveryStatuses)
                       PROJECT(DELS:DeliveryStatus)
                       PROJECT(DELS:DSID)
                     END
Queue:FileDropCombo  QUEUE                            !
ADDC:ContactName       LIKE(ADDC:ContactName)         !List box control field - type derived from field
ADDC:ACID              LIKE(ADDC:ACID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop       QUEUE                            !
DELS:DeliveryStatus    LIKE(DELS:DeliveryStatus)      !List box control field - type derived from field
DELS:DSID              LIKE(DELS:DSID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DELP:Record LIKE(DELP:RECORD),THREAD
QuickWindow          WINDOW('Form Delivery Progress'),AT(,,205,187),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateDeliveryProgress'),SYSTEM
                       SHEET,AT(4,4,197,162),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Floor:'),AT(9,22),USE(?Floor:Prompt),TRN
                           ENTRY(@s35),AT(70,22,100,10),USE(Floor),COLOR(00E9E9E9h),FLAT,MSG('Floor Name'),READONLY,REQ, |
  SKIP,TIP('Floor Name')
                           PROMPT('Contact Name:'),AT(9,36),USE(?ContactName:Prompt:2),TRN
                           COMBO(@s35),AT(70,36,100,10),USE(L_LG:ContactName),VSCROLL,DROP(15),FORMAT('140L(2)|M~C' & |
  'ontact Name~@s35@'),FROM(Queue:FileDropCombo),IMM
                           PROMPT('Delivery Status:'),AT(9,58),USE(?L_LG:DeliveryStatus:Prompt:2),TRN
                           LIST,AT(70,58,100,10),USE(DELS:DeliveryStatus),VSCROLL,DROP(15),FORMAT('140L(2)|M~Deliv' & |
  'ery Status~@s35@'),FROM(Queue:FileDrop)
                           STRING('Day'),AT(141,90,50,10),USE(?String_Day2),TRN
                           PROMPT('Action Date:'),AT(9,90),USE(?DELP:ActionDate:Prompt),TRN
                           BUTTON('...'),AT(54,90,12,10),USE(?Calendar:2)
                           SPIN(@d6b),AT(70,90,65,10),USE(DELP:ActionDate),TIP('The Date that the Status is report' & |
  'ed to have changed')
                           PROMPT('Action Time:'),AT(9,104),USE(?DELP:ActionTime:Prompt),TRN
                           ENTRY(@t7),AT(70,104,65,10),USE(DELP:ActionTime)
                           PROMPT('Status Date:'),AT(9,138),USE(?DELP:StatusDate:Prompt),TRN
                           BUTTON('...'),AT(54,138,12,10),USE(?Calendar)
                           SPIN(@d6),AT(70,138,65,10),USE(DELP:StatusDate),MSG('Date of status entry'),TIP('Date of th' & |
  'is status entry')
                           STRING('Day'),AT(141,138,50,10),USE(?String_Day),TRN
                           PROMPT('Status Time:'),AT(9,152),USE(?DELP:StatusTime:Prompt),TRN
                           ENTRY(@t7),AT(70,152,65,10),USE(DELP:StatusTime),MSG('Time of status entry'),TIP('Time of st' & |
  'atus entry')
                         END
                       END
                       BUTTON('&OK'),AT(100,170,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(152,170,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(-2,170,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB8                CLASS(FileDropComboClass)             ! File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDB5                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

Calendar9            CalendarClass
Calendar10           CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Week_Day                ROUTINE
    ?String_Day{PROP:Text}  = Week_Day(DELP:StatusDate)
    ?String_Day2{PROP:Text} = Week_Day(DELP:ActionDate)
    DISPLAY
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Delivery Progress Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Delivery Progress Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_DeliveryProgress')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Floor:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DELP:Record,History::DELP:Record)
  SELF.AddHistoryField(?DELP:ActionDate,12)
  SELF.AddHistoryField(?DELP:ActionTime,13)
  SELF.AddHistoryField(?DELP:StatusDate,8)
  SELF.AddHistoryField(?DELP:StatusTime,9)
  SELF.AddUpdateFile(Access:DeliveryProgress)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:AddressContacts.SetOpenRelated()
  Relate:AddressContacts.Open                              ! File AddressContacts used by this procedure, so make sure it's RelationManager is open
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Floors.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveryProgress
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?Floor{PROP:ReadOnly} = True
    DISABLE(?L_LG:ContactName)
    DISABLE(?DELS:DeliveryStatus)
    DISABLE(?Calendar:2)
    ?DELP:ActionTime{PROP:ReadOnly} = True
    DISABLE(?Calendar)
    ?DELP:StatusTime{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_DeliveryProgress',QuickWindow)      ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      ! Load the Floor AID
      FLO:FID         = DEL:FID
      IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
         FloorAID     = FLO:AID
         Floor        = FLO:Floor
      .
  
      !message('floor: ' & DEL:FID & ',   FloorAID: ' & FloorAID)
      DELP:ActionDate     = TODAY()
      DELP:ActionTime     = DEFORMAT('12:00:00', @t4)
  FDCB8.Init(L_LG:ContactName,?L_LG:ContactName,Queue:FileDropCombo.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo,Relate:AddressContacts,ThisWindow,GlobalErrors,1,1,0)
  FDCB8.AskProcedure = 1
  FDCB8.Q &= Queue:FileDropCombo
  FDCB8.AddSortOrder(ADDC:FKey_AID)
  FDCB8.AppendOrder('ADDC:ContactName')
  FDCB8.AddRange(ADDC:AID,FloorAID)
  FDCB8.AddField(ADDC:ContactName,FDCB8.Q.ADDC:ContactName) !List box control field - type derived from field
  FDCB8.AddField(ADDC:ACID,FDCB8.Q.ADDC:ACID) !Primary key field - type derived from field
  FDCB8.AddUpdateField(ADDC:ContactName,L_LG:ContactName)
  FDCB8.AddUpdateField(ADDC:ACID,DELP:ACID)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  FDB5.Init(?DELS:DeliveryStatus,Queue:FileDrop.ViewPosition,FDB5::View:FileDrop,Queue:FileDrop,Relate:DeliveryStatuses,ThisWindow)
  FDB5.Q &= Queue:FileDrop
  FDB5.AddSortOrder(DELS:Key_Status)
  FDB5.AddField(DELS:DeliveryStatus,FDB5.Q.DELS:DeliveryStatus) !List box control field - type derived from field
  FDB5.AddField(DELS:DSID,FDB5.Q.DELS:DSID) !Primary key field - type derived from field
  FDB5.AddUpdateField(DELS:DSID,DELP:DSID)
  ThisWindow.AddItem(FDB5.WindowComponent)
  FDB5.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressContacts.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_DeliveryProgress',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
      ! Fetch the Address record for the floor then
      ADD:AID     = FloorAID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) ~= LEVEL:Benign
         CLEAR(ADD:Record)
      .
  
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Address_Contacts
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_LG:ContactName
      FDCB8.TakeAccepted()
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar10.SelectOnClose = True
      Calendar10.Ask('Select a Date',DELP:ActionDate)
      IF Calendar10.Response = RequestCompleted THEN
      DELP:ActionDate=Calendar10.SelectedDate
      DISPLAY(?DELP:ActionDate)
      END
      ThisWindow.Reset(True)
          DO Week_Day
    OF ?Calendar
      ThisWindow.Update()
      Calendar9.SelectOnClose = True
      Calendar9.Ask('Select a Date',DELP:StatusDate)
      IF Calendar9.Response = RequestCompleted THEN
      DELP:StatusDate=Calendar9.SelectedDate
      DISPLAY(?DELP:StatusDate)
      END
      ThisWindow.Reset(True)
          DO Week_Day
    OF ?DELP:StatusDate
          DO Week_Day
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?DELP:StatusDate
          DO Week_Day
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          ADDC:ACID           = DELP:ACID
          IF Access:AddressContacts.TryFetch(ADDC:PKey_ACID) = LEVEL:Benign
             L_LG:ContactName = ADDC:ContactName
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

