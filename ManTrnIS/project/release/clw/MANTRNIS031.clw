

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS031.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_DeliveryItems_old_change_to_commodity PROCEDURE (p:RealMode, p:DID)

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Enter_Volume     BYTE                                  ! Enter the volume (not calculated)
LOC:Local_Vars       GROUP,PRE(L_LV)                       ! 
Commodity            STRING(35)                            ! Commodity
ContainerOperator    STRING(35)                            ! Container Operator
ContainerType        STRING(35)                            ! Type
ContainerReturnAddressName STRING(35)                      ! Name of this address
Packaging            STRING(35)                            ! 
CID                  ULONG                                 ! Client ID
Container_Info_Not_Available BYTE                          ! For containerised this is an option
Volume_Per_Unit      BYTE                                  ! Volume is per unit
VolumetricRatio      DECIMAL(8,2)                          ! x square cubes weigh this amount
Updated_Child        BYTE                                  ! Has the user updated the children at all
Volume_Rounding      BYTE                                  ! Round volume value to kgs
VolumetricRatio_FBN  DECIMAL(4,2)                          ! x square cubes weigh this amount
                     END                                   ! 
LOC:Load_Type_Group  GROUP,PRE(L_LT)                       ! 
TurnIn               BYTE                                  ! Container Turn In required for this Load Type
LoadOption           BYTE                                  ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
ContainerParkStandard BYTE                                 ! Container Park Standard Rates
                     END                                   ! 
LOC:Browse_Totals    GROUP,PRE(L_BT)                       ! 
TripSheet_Loaded     ULONG                                 ! 
                     END                                   ! 
LOC:Item_Comp        GROUP,PRE(L_ICG)                      ! 
DICID                ULONG                                 ! 
DIID                 ULONG                                 ! Delivery Item ID
Length               DECIMAL(6)                            ! Length in metres
Breadth              DECIMAL(6)                            ! Breadth in metres
Height               DECIMAL(6)                            ! Height in metres
Volume               DECIMAL(8,3)                          ! Volume for manual entry (metres cubed)
Volume_Unit          DECIMAL(8,3)                          ! Volume of 1 unit
Units                USHORT                                ! Number of units
Weight               DECIMAL(8,2)                          ! In kg's
VolumetricWeight     DECIMAL(8,2)                          ! Weight based on Volumetric calculation (in kgs)
                     END                                   ! 
LOC:Tot_Comp_Q_Group GROUP,PRE(L_TCG)                      ! 
Units                USHORT                                ! Number of units
Weight               DECIMAL(8,2)                          ! In kg's
VolumetricWeight     DECIMAL(8,2)                          ! Weight based on Volumetric calculation (in kgs)
Volume               DECIMAL(8,3)                          ! Volume for manual entry (metres cubed)
                     END                                   ! 
LOC:Vol_Ratio_Q      QUEUE,PRE(L_VRQ)                      ! 
VolumetricRatio_Vol  DECIMAL(8,2)                          ! x square cubes weigh this amount
VolumetricRatio      DECIMAL(8,2)                          ! x square cubes weigh this amount
Client               STRING(10)                            ! 
                     END                                   ! 
LOC:cm_Volume        GROUP,PRE(L_cV)                       ! in cm ....
Length               DECIMAL(5)                            ! Length in metres
Breadth              DECIMAL(5)                            ! Breadth in metres
Height               DECIMAL(5)                            ! Height in metres
                     END                                   ! 
BRW4::View:Browse    VIEW(TripSheetDeliveries)
                       PROJECT(TRDI:TRID)
                       PROJECT(TRDI:UnitsLoaded)
                       PROJECT(TRDI:UnitsDelivered)
                       PROJECT(TRDI:UnitsNotAccepted)
                       PROJECT(TRDI:DeliveredDate)
                       PROJECT(TRDI:DeliveredTime)
                       PROJECT(TRDI:TDID)
                       PROJECT(TRDI:DIID)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
TRDI:TRID              LIKE(TRDI:TRID)                !List box control field - type derived from field
TRDI:UnitsLoaded       LIKE(TRDI:UnitsLoaded)         !List box control field - type derived from field
TRDI:UnitsDelivered    LIKE(TRDI:UnitsDelivered)      !List box control field - type derived from field
TRDI:UnitsNotAccepted  LIKE(TRDI:UnitsNotAccepted)    !List box control field - type derived from field
TRDI:DeliveredDate     LIKE(TRDI:DeliveredDate)       !List box control field - type derived from field
TRDI:DeliveredTime     LIKE(TRDI:DeliveredTime)       !List box control field - type derived from field
TRDI:TDID              LIKE(TRDI:TDID)                !Primary key field - type derived from field
TRDI:DIID              LIKE(TRDI:DIID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(DeliveryItems_Components)
                       PROJECT(DELIC:Length)
                       PROJECT(DELIC:Breadth)
                       PROJECT(DELIC:Height)
                       PROJECT(DELIC:Volume)
                       PROJECT(DELIC:VolumetricWeight)
                       PROJECT(DELIC:Units)
                       PROJECT(DELIC:DICID)
                       PROJECT(DELIC:DIID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
DELIC:Length           LIKE(DELIC:Length)             !List box control field - type derived from field
DELIC:Breadth          LIKE(DELIC:Breadth)            !List box control field - type derived from field
DELIC:Height           LIKE(DELIC:Height)             !List box control field - type derived from field
DELIC:Volume           LIKE(DELIC:Volume)             !List box control field - type derived from field
DELIC:VolumetricWeight LIKE(DELIC:VolumetricWeight)   !List box control field - type derived from field
DELIC:Units            LIKE(DELIC:Units)              !List box control field - type derived from field
DELIC:DICID            LIKE(DELIC:DICID)              !List box control field - type derived from field
DELIC:DIID             LIKE(DELIC:DIID)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(ManifestLoadDeliveries)
                       PROJECT(MALD:MLDID)
                       PROJECT(MALD:MLID)
                       PROJECT(MALD:UnitsLoaded)
                       PROJECT(MALD:DIID)
                       JOIN(MAL:PKey_MLID,MALD:MLID)
                         PROJECT(MAL:MID)
                         PROJECT(MAL:MLID)
                         PROJECT(MAL:TTID)
                         JOIN(TRU:PKey_TTID,MAL:TTID)
                           PROJECT(TRU:Registration)
                           PROJECT(TRU:TTID)
                         END
                         JOIN(MAN:PKey_MID,MAL:MID)
                           PROJECT(MAN:MID)
                           PROJECT(MAN:CreatedDate)
                         END
                       END
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?Browse:6
MAN:MID                LIKE(MAN:MID)                  !List box control field - type derived from field
MALD:MLDID             LIKE(MALD:MLDID)               !List box control field - type derived from field
MALD:MLID              LIKE(MALD:MLID)                !List box control field - type derived from field
MALD:UnitsLoaded       LIKE(MALD:UnitsLoaded)         !List box control field - type derived from field
MAN:CreatedDate        LIKE(MAN:CreatedDate)          !List box control field - type derived from field
TRU:Registration       LIKE(TRU:Registration)         !List box control field - type derived from field
MAL:MID                LIKE(MAL:MID)                  !List box control field - type derived from field
MALD:DIID              LIKE(MALD:DIID)                !Browse key field - type derived from field
MAL:MLID               LIKE(MAL:MLID)                 !Related join file key field - type derived from field
TRU:TTID               LIKE(TRU:TTID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB19::View:FileDropCombo VIEW(Vessels)
                       PROJECT(VES:Vessel)
                       PROJECT(VES:VEID)
                     END
FDCB18::View:FileDropCombo VIEW(PackagingTypes)
                       PROJECT(PACK:Packaging)
                       PROJECT(PACK:PTID)
                     END
FDCB20::View:FileDropCombo VIEW(Commodities)
                       PROJECT(COM:Commodity)
                       PROJECT(COM:CMID)
                     END
Queue:FileDropCombo  QUEUE                            !
VES:Vessel             LIKE(VES:Vessel)               !List box control field - type derived from field
VES:VEID               LIKE(VES:VEID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !
PACK:Packaging         LIKE(PACK:Packaging)           !List box control field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DELI:Record LIKE(DELI:RECORD),THREAD
BRW4::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW4::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW4::PopupChoice    SIGNED                       ! Popup current choice
BRW4::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW4::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW6::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW6::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW6::PopupChoice    SIGNED                       ! Popup current choice
BRW6::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW6::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Delivery Items'),AT(,,373,284),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  IMM,MDI,HLP('Update_DeliveryItems'),SYSTEM
                       SHEET,AT(4,2,366,266),USE(?CurrentTab),JOIN
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('DIID:'),AT(307,4),USE(?DELI:DIID:Prompt),TRN
                           STRING(@n_10),AT(317,4),USE(DELI:DIID),FONT(,7),RIGHT(1),TRN
                           PROMPT('Commodity:'),AT(9,20),USE(?Commodity:Prompt:3),TRN
                           COMBO(@s35),AT(73,20,137,10),USE(L_LV:Commodity),VSCROLL,DROP(15),FORMAT('140L(2)|M~Com' & |
  'modity~@s35@'),FROM(Queue:FileDropCombo:2),IMM
                           PROMPT('Item No.:'),AT(277,20),USE(?DELI:ItemNo:Prompt),TRN
                           SPIN(@n6),AT(317,20,48,10),USE(DELI:ItemNo),RIGHT(1),COLOR(00E9E9E9h),MSG('Item Number'),REQ, |
  SKIP,TIP('Item Number')
                           PROMPT('Packaging:'),AT(9,36),USE(?Commodity:Prompt:2),TRN
                           COMBO(@s35),AT(73,36,137,10),USE(L_LV:Packaging),VSCROLL,DROP(15),FORMAT('140L(2)|M~Pac' & |
  'kaging~@s35@'),FROM(Queue:FileDropCombo:1),IMM
                           PROMPT('Units:'),AT(9,52),USE(?DELI:Units:Prompt),TRN
                           SPIN(@n6),AT(73,52,50,10),USE(DELI:Units),RIGHT(1),MSG('Number of units'),TIP('Number of units')
                           PROMPT('Delivered:'),AT(277,52),USE(?DELI:DeliveredUnits:Prompt),TRN
                           SPIN(@n6),AT(317,52,48,10),USE(DELI:DeliveredUnits),RIGHT(1),COLOR(00E6E6E6h),MSG('Units delivered'), |
  SKIP,TIP('Units delivered')
                           LINE,AT(10,70,354,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('&Weight (kgs):'),AT(9,77),USE(?DELI:Weight:Prompt),TRN
                           ENTRY(@n-11.1b),AT(73,77,50,10),USE(DELI:Weight),RIGHT(1),MSG('In kg''s')
                           CHECK(' Volume Rounding'),AT(206,77),USE(L_LV:Volume_Rounding),MSG('Round volume value to kgs'), |
  TIP('Round volume value to kgs'),TRN
                           CHECK(' Volume Per Unit'),AT(294,77),USE(L_LV:Volume_Per_Unit),MSG('Volume is per unit'),TIP('Volume is per unit'), |
  TRN
                           GROUP,AT(9,93,358,34),USE(?Volume_Group)
                             GROUP,AT(73,93,163,20),USE(?Group_Measure),TRN
                               PROMPT('Length (cm):'),AT(73,93),USE(?DELI:Length:Prompt),TRN
                               ENTRY(@n-7.0b),AT(73,103,38,10),USE(L_cV:Length),RIGHT(1),MSG('Length in metres'),TIP('Length in metres')
                               STRING('X'),AT(117,103),USE(?String1),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                               PROMPT('Breadth (cm):'),AT(129,93),USE(?DELI:Breadth:Prompt),TRN
                               ENTRY(@n-7.0b),AT(129,103,38,10),USE(L_cV:Breadth),RIGHT(1),MSG('Breadth in metres'),TIP('Breadth in metres')
                               STRING('X'),AT(173,103),USE(?String1:2),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                               PROMPT('Height (cm):'),AT(185,93),USE(?DELI:Height:Prompt),TRN
                               ENTRY(@n-7.0b),AT(185,103,38,10),USE(L_cV:Height),RIGHT(1),MSG('Height in metres'),TIP('Height in metres')
                               STRING('='),AT(230,103),USE(?String3),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                             END
                             GROUP,AT(242,93,52,20),USE(?Group_VolTot)
                               PROMPT('Volume (m):'),AT(242,93),USE(?DELI:Volume:Prompt),TRN
                               ENTRY(@n-11.3),AT(242,103,52,10),USE(DELI:Volume_Unit),RIGHT(1),MSG('Volume for manual entry'), |
  TIP('Volume for manual entry (metres cubed)')
                             END
                             PROMPT('Total Volume (m):'),AT(313,93),USE(?DELI:Volume:Prompt:2),TRN
                             ENTRY(@n-11.3),AT(313,103,52,10),USE(DELI:Volume),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP, |
  TIP('Volume X Unis if Volume Per Unit specified'),MSG('Volume for manual entry (metres cubed)')
                             PROMPT('Volume Option:'),AT(9,93),USE(?LOC:Enter_Volume:Prompt),TRN
                             LIST,AT(9,103,60,10),USE(LOC:Enter_Volume),DROP(5),FROM('Calculate Vol.|#0|Enter Vol.|#' & |
  '1|Split Vol.|#2'),MSG('Enter the volume (not calculated)'),TIP('Enter the volume (no' & |
  't calculated)')
                             PROMPT('Volumetric Weight:'),AT(9,116),USE(?DELI:VolumetricWeight:Prompt),TRN
                             ENTRY(@n-11.1),AT(73,116,50,10),USE(DELI:VolumetricWeight),RIGHT(1),COLOR(00E9E9E9h),MSG('Weight bas' & |
  'ed on Volumetric calculation'),READONLY,SKIP,TIP('Weight based on Volumetric calculation')
                             BUTTON('S'),AT(138,116,18,10),USE(?Button_Set_Vol_Ratio),TIP('Set volumetric ratio from' & |
  ' Client details')
                             BUTTON('E'),AT(161,116,18,10),USE(?Button_Set_Vol_Ratio:2),TIP('Allow to enter a volume' & |
  'tric ratio')
                             PROMPT('Volumetric Ratio:'),AT(185,116),USE(?DELI:VolumetricRatio:Prompt)
                             ENTRY(@n6.2),AT(242,116,52,10),USE(L_LV:VolumetricRatio_FBN),RIGHT(1),COLOR(00E9E9E9h),MSG('A square c' & |
  'ube is equivalent to this weight'),READONLY,SKIP,TIP('A square cube is equivalent to' & |
  ' this weight')
                             LIST,AT(313,116,52,10),USE(L_LV:VolumetricRatio),RIGHT(1),VSCROLL,DROP(10,150),FORMAT('50R(1)|M~V' & |
  'olumetric Ratio~L(2)@n-11.2@50R(1)|M~Kg''s per cubic metre~L(2)@n-11.2@40L(1)|M~Clie' & |
  'nt~L(2)@s10@'),FROM(LOC:Vol_Ratio_Q),TIP('Metre''s cubed that = 1 ton')
                           END
                           LINE,AT(10,130,354,0),USE(?Line1:2),COLOR(COLOR:Black),LINEWIDTH(2)
                           SHEET,AT(9,135,358,128),USE(?Sheet2)
                             TAB('Container'),USE(?Tab_Container)
                               CHECK(' Container Info Not Available'),AT(257,151),USE(L_LV:Container_Info_Not_Available), |
  MSG('For containerised this is an option'),TIP('For containerised this is an option'),TRN
                               GROUP('Container Information'),AT(13,159,350,100),USE(?Group_Container),BOXED
                                 GROUP,AT(19,169,202,10),USE(?Group_ContainerType)
                                   PROMPT('Type:'),AT(19,169),USE(?L_LV:ContainerType:Prompt),TRN
                                   BUTTON('...'),AT(61,169,12,10),USE(?CallLookup:2)
                                   ENTRY(@s35),AT(77,169,130,10),USE(L_LV:ContainerType),MSG('Type'),TIP('Type')
                                 END
                                 GROUP,AT(19,183,290,70),USE(?Group_Container_Sub)
                                   PROMPT('Operator:'),AT(19,183),USE(?ContainerOperator:Prompt),TRN
                                   BUTTON('...'),AT(61,183,12,10),USE(?CallLookup)
                                   ENTRY(@s35),AT(77,183,130,10),USE(L_LV:ContainerOperator),MSG('Container Operator'),TIP('Container Operator')
                                   PROMPT('Container No.:'),AT(19,197),USE(?DELI:ContainerNo:Prompt),TRN
                                   ENTRY(@s35),AT(77,197,130,10),USE(DELI:ContainerNo)
                                   PROMPT('Seal No.:'),AT(19,210),USE(?DELI:SealNo:Prompt),TRN
                                   ENTRY(@s35),AT(77,210,130,10),USE(DELI:SealNo),MSG('Container Seal no.'),TIP('Container Seal no.')
                                   PROMPT('Vessel:'),AT(19,226),USE(?DELI:ContainerVessel:Prompt:2),TRN
                                   COMBO(@s35),AT(77,226,130,10),USE(DELI:ContainerVessel),VSCROLL,DROP(15),FORMAT('200L(2)|M~' & |
  'Vessel~@s50@'),FROM(Queue:FileDropCombo),IMM,MSG('Vessel this container arrived on')
                                   PROMPT('ETA:'),AT(227,226),USE(?DELI:ETA:Prompt),TRN
                                   SPIN(@d6),AT(249,226,60,10),USE(DELI:ETA),RIGHT(1),MSG('Estimated time of arrival for t' & |
  'his Vessel'),TIP('Estimated time of arrival for this Vessel')
                                   GROUP,AT(19,242,202,10),USE(?Group_TurnIn)
                                     PROMPT('Turn In:'),AT(19,242),USE(?ContainerReturnAddressName:Prompt),TRN
                                     BUTTON('...'),AT(61,242,12,10),USE(?CallLookup:3)
                                     ENTRY(@s35),AT(77,242,130,10),USE(L_LV:ContainerReturnAddressName),MSG('Name of this address'), |
  TIP('Name of this address')
                                   END
                                 END
                                 PROMPT(''),AT(213,171,96,36),USE(?Prompt_ContainerInfo),CENTER,TRN
                                 CHECK(' &Show on Invoice'),AT(173,151),USE(DELI:ShowOnInvoice),MSG('Show these containe' & |
  'r details on the Invoice generated from this DI'),TIP('Show these container details ' & |
  'on the Invoice generated from this DI'),TRN
                               END
                             END
                             TAB('Item Components'),USE(?Tab_Split_Vol)
                               LIST,AT(13,154,300,78),USE(?List),VSCROLL,FORMAT('32R(1)|M~Length~C(0)@n8@32R(1)|M~Brea' & |
  'dth~C(0)@n8@32R(1)|M~Height~C(0)@n8@44R(1)|M~Volume~C(0)@n-11.3@64R(1)|M~Volumetric ' & |
  'Weight~C(0)@n-11.1@38R(1)|M~Units~C(0)@n6@40R(1)|M~DICID~C(0)@n_10@'),FROM(Queue:Browse), |
  IMM,MSG('Browsing Records')
                               BUTTON('Add'),AT(319,153,42,12),USE(?Button_Add)
                               BUTTON('Update'),AT(319,167,42,12),USE(?Button_Update)
                               BUTTON('Delete'),AT(319,182,42,12),USE(?Button_Delete)
                               PROMPT('Total Units:'),AT(319,209,39,10),USE(?Units:Prompt:2)
                               ENTRY(@n6),AT(319,222,39,10),USE(L_TCG:Units),RIGHT(1),COLOR(00E9E9E9h),MSG('Number of units'), |
  READONLY,SKIP,TIP('Number of units')
                               PROMPT('Length (cm):'),AT(13,236),USE(?Length:Prompt),TRN
                               PROMPT('Breadth (cm):'),AT(65,236),USE(?Breadth:Prompt),TRN
                               PROMPT('Height (cm):'),AT(117,236),USE(?Height:Prompt),TRN
                               PROMPT('Volume (m):'),AT(213,236),USE(?Volume:Prompt),TRN
                               ENTRY(@n8b),AT(13,247,38,10),USE(L_ICG:Length),RIGHT(1),MSG('Length in cm'),TIP('Length in cm')
                               ENTRY(@n8b),AT(65,247,38,10),USE(L_ICG:Breadth),RIGHT(1),MSG('Breadth in cm'),TIP('Breadth in cm')
                               ENTRY(@n8b),AT(117,247,38,10),USE(L_ICG:Height),RIGHT(1),MSG('Height in cm'),TIP('Height in cm')
                               SPIN(@n6),AT(177,247,30,10),USE(L_ICG:Units),RIGHT(1),MSG('Number of units'),TIP('Number of units')
                               ENTRY(@n-11.3),AT(213,247,33,10),USE(L_ICG:Volume),RIGHT(1),COLOR(00E9E9E9h),MSG('Volume for' & |
  ' manual entry'),READONLY,SKIP,TIP('Volume for manual entry (metres cubed)')
                               PROMPT('Vol. Weight:'),AT(257,236),USE(?VolumetricWeight:Prompt),TRN
                               ENTRY(@n-11.1),AT(257,247,39,10),USE(L_ICG:VolumetricWeight),RIGHT(1),COLOR(00E9E9E9h),MSG('Weight bas' & |
  'ed on Volumetric calculation'),READONLY,SKIP,TIP('Weight based on Volumetric calcula' & |
  'tion (in kgs)')
                               BUTTON('&Insert'),AT(187,135,42,12),USE(?Insert),HIDE,TIP('Insert Delivery Component')
                               BUTTON('&Change'),AT(231,135,42,12),USE(?Change),HIDE
                               BUTTON('&Delete'),AT(275,135,42,12),USE(?Delete),HIDE
                               PROMPT('Units:'),AT(177,236),USE(?Units:Prompt),TRN
                             END
                           END
                         END
                         TAB('&2) Trip Sheet Deliveries'),USE(?Tab:4)
                           LIST,AT(7,22,358,226),USE(?Browse:4),HVSCROLL,FORMAT('40R(2)|M~Trip Sheet ID (TRID)~L@N' & |
  '_10@[36R(2)|M~Loaded~C(0)@n6@36R(2)|M~Delivered~C(0)@n6@52R(2)|M~Not Accepted~L@n6@]' & |
  '|M~Units~[48R(2)|M~Date~C(0)@d6b@36R(2)|M~Time~C(0)@t7b@]|M~Deliverd~'),FROM(Queue:Browse:4), |
  IMM,MSG('Browsing the MainfestLoadDeliveries file')
                           PROMPT('Trip Sheet Loaded:'),AT(9,253),USE(?TripSheet_Loaded:Prompt),TRN
                           ENTRY(@n13),AT(77,253,55,10),USE(L_BT:TripSheet_Loaded),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                         END
                         TAB('&3) Mainfest Load Deliveries'),USE(?Tab:5)
                           LIST,AT(7,22,357,242),USE(?Browse:6),HVSCROLL,FORMAT('40R(2)|M~MID~C(0)@n_10@40R(2)|M~M' & |
  'LDID~C(0)@n_10@40R(2)|M~MLID~C(0)@n_10@48R(2)|M~Units Loaded~C(0)@n6@48R(2)|M~Create' & |
  'd Date~C(0)@d6@80L(2)|M~Registration~C(0)@s20@40R(2)|M~MID (Load)~C(0)@n_10@'),FROM(Queue:Browse:6), |
  IMM,MSG('Browsing the MainfestLoadDeliveries file')
                         END
                       END
                       BUTTON('&OK'),AT(264,268,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(317,268,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,268,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW4::LastSortOrder       BYTE
BRW6::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
ResetFromView          PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW2                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
PrimeRecord            PROCEDURE(BYTE SuppressClear = 0),BYTE,PROC,DERIVED
ResetFromView          PROCEDURE(),DERIVED
SetAlerts              PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW6                 CLASS(BrowseClass)                    ! Browse using ?Browse:6
Q                      &Queue:Browse:6                !Reference to browse queue
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB19               CLASS(FileDropComboClass)             ! File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB18               CLASS(FileDropComboClass)             ! File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB20               CLASS(FileDropComboClass)             ! File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

LOC:Delivery_Composition_Group GROUP,PRE(L_DCG)
CMID                       LIKE(DELI:CMID)
Type                       LIKE(DELI:Type)
ShowOnInvoice              LIKE(DELI:ShowOnInvoice)
COID                       LIKE(DELI:COID)
CTID                       LIKE(DELI:CTID)
ContainerVessel            LIKE(DELI:ContainerVessel)
ETA                        LIKE(DELI:ETA)
ETA_TIME                   LIKE(DELI:ETA_TIME)
ByContainer                LIKE(DELI:ByContainer)
Length                     LIKE(DELI:Length)
Breadth                    LIKE(DELI:Breadth)
Height                     LIKE(DELI:Height)
Volume                     LIKE(DELI:Volume)
Volume_Unit                LIKE(DELI:Volume_Unit)
PTID                       LIKE(DELI:PTID)
VolumetricRatio            LIKE(DELI:VolumetricRatio)
                         END
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Compute_Vol                       ROUTINE
    DELI:Length         = L_cV:Length  / 100
    DELI:Breadth        = L_cV:Breadth / 100
    DELI:Height         = L_cV:Height  / 100

    CASE LOC:Enter_Volume
    OF 0                            ! Calc. Volume
       DELI:Volume_Unit         = DELI:Length * DELI:Breadth * DELI:Height
    OF 1                            ! Enter Volume
    OF 2                            ! Split vol.
    .

    IF LOC:Enter_Volume ~= 2
       IF L_LV:Volume_Per_Unit = TRUE
          DELI:Volume           = DELI:Volume_Unit * DELI:Units
       ELSE
          DELI:Volume           = DELI:Volume_Unit
       .

       DELI:VolumetricWeight    = DELI:Volume * DELI:VolumetricRatio

       IF L_LV:Volume_Rounding = TRUE
          DELI:Volume           = ROUND(DELI:Volume,1)
    .  .

    !L_cV:Volume         = DELI:Volume
    !L_cV:Volume_Unit    = DELI:Volume_Unit

    DISPLAY
    EXIT
Delivery_Comp_WVol          ROUTINE
    L_ICG:Volume            = (L_ICG:Length / 100) * (L_ICG:Breadth / 100) * (L_ICG:Height / 100)
    L_ICG:Volume_Unit       = L_ICG:Volume


    !DELIC:Volume              = DELIC:Length * DELIC:Breadth * DELIC:Height

    IF L_LV:Volume_Per_Unit = TRUE
       L_ICG:Volume         = L_ICG:Volume * L_ICG:Units
       !DELIC:Volume           = DELIC:Volume * DELIC:Units
    .

    L_ICG:VolumetricWeight  = L_ICG:Volume * DELI:VolumetricRatio
    !DELIC:VolumetricWeight    = DELIC:Volume * DELI:VolumetricRatio


    DISPLAY
    EXIT
Vol_Q                                   ROUTINE
    FREE(LOC:Vol_Ratio_Q)

    L_VRQ:VolumetricRatio_Vol   = 2.5
    L_VRQ:VolumetricRatio       = 1000 / L_VRQ:VolumetricRatio_Vol
    ADD(LOC:Vol_Ratio_Q)

    L_VRQ:VolumetricRatio_Vol   = 3
    L_VRQ:VolumetricRatio       = 1000 / L_VRQ:VolumetricRatio_Vol
    ADD(LOC:Vol_Ratio_Q)

    L_VRQ:VolumetricRatio_Vol   = 4
    L_VRQ:VolumetricRatio       = 1000 / L_VRQ:VolumetricRatio_Vol
    ADD(LOC:Vol_Ratio_Q)

    L_VRQ:VolumetricRatio_Vol   = 5
    L_VRQ:VolumetricRatio       = 1000 / L_VRQ:VolumetricRatio_Vol
    ADD(LOC:Vol_Ratio_Q)
    EXIT
DelComp_SetSplit                ROUTINE
    ! When the split button is pressed update the records
    CLEAR(DELIC:Record,-1)
    DELIC:DIID  = DELI:DIID
    SET(DELIC:FKey_DIID, DELIC:FKey_DIID)
    LOOP
       IF Access:DeliveryItems_Components.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF DELIC:DIID ~= DELI:DIID
          BREAK
       .

       DELIC:Volume            = (DELIC:Length / 100) * (DELIC:Breadth / 100) * (DELIC:Height / 100)
       DELIC:Volume_Unit       = DELIC:Volume


       IF L_LV:Volume_Per_Unit = TRUE
          DELIC:Volume         = DELIC:Volume * DELIC:Units
       .

       DELIC:VolumetricWeight  = DELIC:Volume * DELI:VolumetricRatio
       IF Access:DeliveryItems_Components.TryUpdate() ~= LEVEL:Benign
    .  .

    BRW2.ResetFromFile()
    BRW2.ResetFromView()
    EXIT
Enter_Vol_Check                 ROUTINE
    CASE LOC:Enter_Volume
    OF 0                        ! Calc. Volume
    OF 1                        ! Enter Volume
    OF 2                        ! Split vol.
       ! Set Del Item Vol.
       DO DelComp_SetSplit

       DELI:Volume_Unit      = L_TCG:Volume                             ! Rounded if required

       DELI:Volume           = DELI:Volume_Unit

       DELI:VolumetricWeight = DELI:Volume * DELI:VolumetricRatio       !L_TCG:VolumetricWeight
    .
    EXIT
Container_Options                ROUTINE
    IF QuickWindow{PROP:AcceptAll} = FALSE
       IF L_LV:Container_Info_Not_Available = TRUE
          CLEAR(L_LV:ContainerOperator)
          !CLEAR(L_LV:ContainerType)
          CLEAR(L_LV:ContainerReturnAddressName)

          CLEAR(DELI:ContainerNo)
          CLEAR(DELI:ContainerVessel)
          CLEAR(DELI:ContainerReturnAID)
          CLEAR(DELI:COID)
          !CLEAR(DELI:CTID)

          ?L_LV:ContainerOperator{PROP:Req}                = FALSE
          !?L_LV:ContainerType{PROP:Req}                    = FALSE
          ?L_LV:ContainerReturnAddressName{PROP:Req}       = FALSE
          ?DELI:ContainerNo{PROP:Req}                      = FALSE
          ?DELI:ContainerVessel{PROP:Req}                  = FALSE

          DISABLE(?Group_Container_Sub)
       ELSE
          ENABLE(?Group_Container_Sub)

          CLEAR(L_LV:ContainerOperator)
          !CLEAR(L_LV:ContainerType)
          CLEAR(L_LV:ContainerReturnAddressName)

          CLEAR(DELI:ContainerNo)
          CLEAR(DELI:ContainerVessel)
          CLEAR(DELI:ContainerReturnAID)
          CLEAR(DELI:COID)
          !CLEAR(DELI:CTID)

          IF L_LT:LoadOption = 1 OR L_LT:LoadOption = 2             ! Container Park or Container
             ?L_LV:ContainerOperator{PROP:Req}             = TRUE
             !?L_LV:ContainerType{PROP:Req}                 = TRUE
             ?DELI:ContainerNo{PROP:Req}                   = TRUE
             ?DELI:ContainerVessel{PROP:Req}               = TRUE
          .
          IF L_LT:TurnIn = TRUE
             ?L_LV:ContainerReturnAddressName{PROP:Req}    = TRUE
       .  .

       IF DELI:Type = 1                        ! Loose
          ?L_LV:ContainerType{PROP:Req}                    = FALSE
       .

       DISPLAY
    .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Delivery Items Record'
  OF InsertRecord
    ActionMessage = 'Delivery Items Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Delivery Items Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_DeliveryItems_old_change_to_commodity')
  SELF.Request = GlobalRequest                    ! Store the incoming request
      IF p:RealMode = 0                   ! If not passed set to current, this wasnt done until  27 Nov 06
         p:RealMode     = SELF.Request
      .
  
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DELI:DIID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DELI:Record,History::DELI:Record)
  SELF.AddHistoryField(?DELI:DIID,1)
  SELF.AddHistoryField(?DELI:ItemNo,3)
  SELF.AddHistoryField(?DELI:Units,25)
  SELF.AddHistoryField(?DELI:DeliveredUnits,31)
  SELF.AddHistoryField(?DELI:Weight,27)
  SELF.AddHistoryField(?DELI:Volume_Unit,24)
  SELF.AddHistoryField(?DELI:Volume,23)
  SELF.AddHistoryField(?DELI:VolumetricWeight,29)
  SELF.AddHistoryField(?DELI:ContainerNo,10)
  SELF.AddHistoryField(?DELI:SealNo,13)
  SELF.AddHistoryField(?DELI:ContainerVessel,12)
  SELF.AddHistoryField(?DELI:ETA,16)
  SELF.AddHistoryField(?DELI:ShowOnInvoice,7)
  SELF.AddUpdateFile(Access:DeliveryItems)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                           ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Relate:Vessels.Open                             ! File Vessels used by this procedure, so make sure it's RelationManager is open
  Access:DeliveryItems.UseFile                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Commodities.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ContainerOperators.UseFile               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ContainerTypes.UseFile                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PackagingTypes.UseFile                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ClientsRateTypes.UseFile                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Floors.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItemAlias2.UseFile               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveryItems
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:TripSheetDeliveries,SELF) ! Initialize the browse manager
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:DeliveryItems_Components,SELF) ! Initialize the browse manager
  BRW6.Init(?Browse:6,Queue:Browse:6.ViewPosition,BRW6::View:Browse,Queue:Browse:6,Relate:ManifestLoadDeliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    ?L_LV:Commodity{PROP:ReadOnly} = True
    ?L_LV:Packaging{PROP:ReadOnly} = True
    ?DELI:Weight{PROP:ReadOnly} = True
    ?L_cV:Length{PROP:ReadOnly} = True
    ?L_cV:Breadth{PROP:ReadOnly} = True
    ?L_cV:Height{PROP:ReadOnly} = True
    ?DELI:Volume_Unit{PROP:ReadOnly} = True
    ?DELI:Volume{PROP:ReadOnly} = True
    ?DELI:VolumetricWeight{PROP:ReadOnly} = True
    DISABLE(?Button_Set_Vol_Ratio)
    DISABLE(?Button_Set_Vol_Ratio:2)
    ?L_LV:VolumetricRatio_FBN{PROP:ReadOnly} = True
    DISABLE(?L_LV:VolumetricRatio)
    DISABLE(?CallLookup:2)
    ?L_LV:ContainerType{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?L_LV:ContainerOperator{PROP:ReadOnly} = True
    ?DELI:ContainerNo{PROP:ReadOnly} = True
    ?DELI:SealNo{PROP:ReadOnly} = True
    DISABLE(?DELI:ContainerVessel)
    DISABLE(?CallLookup:3)
    ?L_LV:ContainerReturnAddressName{PROP:ReadOnly} = True
    DISABLE(?Button_Add)
    DISABLE(?Button_Update)
    DISABLE(?Button_Delete)
    ?L_TCG:Units{PROP:ReadOnly} = True
    ?L_ICG:Length{PROP:ReadOnly} = True
    ?L_ICG:Breadth{PROP:ReadOnly} = True
    ?L_ICG:Height{PROP:ReadOnly} = True
    ?L_ICG:Volume{PROP:ReadOnly} = True
    ?L_ICG:VolumetricWeight{PROP:ReadOnly} = True
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
    ?L_BT:TripSheet_Loaded{PROP:ReadOnly} = True
  END
  BRW4.Q &= Queue:Browse:4
  BRW4::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon TRDI:DIID for sort order 1
  BRW4.AddSortOrder(BRW4::Sort0:StepClass,TRDI:FKey_DIID) ! Add the sort order for TRDI:FKey_DIID for sort order 1
  BRW4.AddRange(TRDI:DIID,Relate:TripSheetDeliveries,Relate:DeliveryItems) ! Add file relationship range limit for sort order 1
  BRW4.AddField(TRDI:TRID,BRW4.Q.TRDI:TRID)       ! Field TRDI:TRID is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:UnitsLoaded,BRW4.Q.TRDI:UnitsLoaded) ! Field TRDI:UnitsLoaded is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:UnitsDelivered,BRW4.Q.TRDI:UnitsDelivered) ! Field TRDI:UnitsDelivered is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:UnitsNotAccepted,BRW4.Q.TRDI:UnitsNotAccepted) ! Field TRDI:UnitsNotAccepted is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:DeliveredDate,BRW4.Q.TRDI:DeliveredDate) ! Field TRDI:DeliveredDate is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:DeliveredTime,BRW4.Q.TRDI:DeliveredTime) ! Field TRDI:DeliveredTime is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:TDID,BRW4.Q.TRDI:TDID)       ! Field TRDI:TDID is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:DIID,BRW4.Q.TRDI:DIID)       ! Field TRDI:DIID is a hot field or requires assignment from browse
  BRW2.Q &= Queue:Browse
  BRW2.FileLoaded = 1                             ! This is a 'file loaded' browse
  BRW2.AddSortOrder(,DELIC:FKey_DIID)             ! Add the sort order for DELIC:FKey_DIID for sort order 1
  BRW2.AddRange(DELIC:DIID,DELI:DIID)             ! Add single value range limit for sort order 1
  BRW2.AddLocator(BRW2::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW2::Sort0:Locator.Init(,DELIC:DIID,1,BRW2)    ! Initialize the browse locator using  using key: DELIC:FKey_DIID , DELIC:DIID
  BRW2.AppendOrder('+DELIC:DICID')                ! Append an additional sort order
  BRW2.AddField(DELIC:Length,BRW2.Q.DELIC:Length) ! Field DELIC:Length is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Breadth,BRW2.Q.DELIC:Breadth) ! Field DELIC:Breadth is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Height,BRW2.Q.DELIC:Height) ! Field DELIC:Height is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Volume,BRW2.Q.DELIC:Volume) ! Field DELIC:Volume is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:VolumetricWeight,BRW2.Q.DELIC:VolumetricWeight) ! Field DELIC:VolumetricWeight is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Units,BRW2.Q.DELIC:Units)   ! Field DELIC:Units is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:DICID,BRW2.Q.DELIC:DICID)   ! Field DELIC:DICID is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:DIID,BRW2.Q.DELIC:DIID)     ! Field DELIC:DIID is a hot field or requires assignment from browse
  BRW6.Q &= Queue:Browse:6
  BRW6::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon MALD:DIID for sort order 1
  BRW6.AddSortOrder(BRW6::Sort0:StepClass,MALD:FKey_DIID) ! Add the sort order for MALD:FKey_DIID for sort order 1
  BRW6.AddRange(MALD:DIID,Relate:ManifestLoadDeliveries,Relate:DeliveryItems) ! Add file relationship range limit for sort order 1
  BRW6.AppendOrder('+MAN:MID,+TRU:Registration')  ! Append an additional sort order
  BRW6.AddField(MAN:MID,BRW6.Q.MAN:MID)           ! Field MAN:MID is a hot field or requires assignment from browse
  BRW6.AddField(MALD:MLDID,BRW6.Q.MALD:MLDID)     ! Field MALD:MLDID is a hot field or requires assignment from browse
  BRW6.AddField(MALD:MLID,BRW6.Q.MALD:MLID)       ! Field MALD:MLID is a hot field or requires assignment from browse
  BRW6.AddField(MALD:UnitsLoaded,BRW6.Q.MALD:UnitsLoaded) ! Field MALD:UnitsLoaded is a hot field or requires assignment from browse
  BRW6.AddField(MAN:CreatedDate,BRW6.Q.MAN:CreatedDate) ! Field MAN:CreatedDate is a hot field or requires assignment from browse
  BRW6.AddField(TRU:Registration,BRW6.Q.TRU:Registration) ! Field TRU:Registration is a hot field or requires assignment from browse
  BRW6.AddField(MAL:MID,BRW6.Q.MAL:MID)           ! Field MAL:MID is a hot field or requires assignment from browse
  BRW6.AddField(MALD:DIID,BRW6.Q.MALD:DIID)       ! Field MALD:DIID is a hot field or requires assignment from browse
  BRW6.AddField(MAL:MLID,BRW6.Q.MAL:MLID)         ! Field MAL:MLID is a hot field or requires assignment from browse
  BRW6.AddField(TRU:TTID,BRW6.Q.TRU:TTID)         ! Field TRU:TTID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_DeliveryItems_old_change_to_commodity',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      L_LV:CID                        = DEL:CID
      CLI:CID                         = DEL:CID
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         IF p:RealMode = InsertRecord          !SELF.Request =
            DELI:VolumetricRatio      = CLI:VolumetricRatio
      .  .
  
      L_LV:VolumetricRatio_FBN        = 1000 / DELI:VolumetricRatio
      ! Load Type
      !DISABLE(?Group_Container)
      DISABLE(?Group_TurnIn)
      DISABLE(?L_LV:Container_Info_Not_Available)
  
      ?L_LV:ContainerOperator{PROP:Req}                   = FALSE
      ?L_LV:ContainerType{PROP:Req}                       = FALSE
      ?L_LV:ContainerReturnAddressName{PROP:Req}          = FALSE
      ?DELI:ContainerNo{PROP:Req}                         = FALSE
      ?DELI:ContainerVessel{PROP:Req}                     = FALSE

      LOAD2:LTID              = DEL:LTID
      IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
         LOC:Load_Type_Group :=: LOAD2:Record
  
         ! If we have a Container load that is not a Full load we can only allow 1 container type
         ! because the Container Type determines the Rate which is at Delivery level so we cannot
         ! have different rated Items on same Delivery.

         ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
         !              0               1             2          3              4               5
         IF L_LT:LoadOption = 2 OR L_LT:LoadOption = 4
            DELI:Type         = 0                         ! Container
            ?Prompt_ContainerInfo{PROP:Text}    = 'Container type required.'

            IF p:RealMode = InsertRecord        ! SELF.Request =
               DELI:Units  = 1
            .
            DISABLE(?DELI:Units)
  
            DISABLE(?Volume_Group)

            Item_No_#         = 0
            DELI:CTID         = Get_DelItems_ContainerType(DEL:DID, Item_No_#)
            IF Get_ContainerType_Info(DELI:CTID,0) = 1
               ENABLE(?Volume_Group)
            .

            ! Not sure what this was about...
            !IF LOAD:FullLoad_ContainerPark ~= 1
               ! Check for other items, if found set Container type ID and disable
               IF DELI:ItemNo ~= Item_No_#                ! If we aren't on the 1st Delivery Item of the DI
                  IF DELI:CTID ~= 0
                     DISABLE(?Group_ContainerType)
                     ?Prompt_ContainerInfo{PROP:Text}        = 'Container type set to 1st Items type.'
               .  .
         ELSE
            DELI:Type         = 1                         ! Loose
         .

         IF L_LT:LoadOption = 2 OR L_LT:LoadOption = 4 OR L_LT:LoadOption = 1
         !IF L_LT:ContainerOption = 1 OR L_LT:ContainerOption = 2      ! None|Container|Containerised
       !     ENABLE(?Group_Container)
            ?L_LV:ContainerOperator{PROP:Req}             = TRUE
            ?L_LV:ContainerType{PROP:Req}                 = TRUE
            ?DELI:ContainerNo{PROP:Req}                   = TRUE
            ?DELI:ContainerVessel{PROP:Req}               = TRUE

            !IF L_LT:ContainerOption = 2
               ENABLE(?L_LV:Container_Info_Not_Available)
         .  !.

         IF L_LT:TurnIn = TRUE
            ENABLE(?Group_TurnIn)
            ?L_LV:ContainerReturnAddressName{PROP:Req}    = TRUE
      .  .
      ! (p:RealMode, p:DID)
      ! (BYTE=0, ULONG=0)
  
      CASE p:RealMode
      OF InsertRecord
         ! p:DID
         ! ULONG=0
         ! p:DID is a different DID to the one calling this insert / update.  It is used in Delivery Compositions to
         !       get the Item from this DID so that some fields can be defaulted for this new DID Item.
  
         ! Set item defaults from previous item in passed DID
         IF p:DID ~= 0
            ! Using the Delivery Items record, load the Delivery Items record
            A_DELI2:DID              = p:DID
            A_DELI2:ItemNo           = 1
            IF Access:DeliveryItemAlias2.TryFetch(A_DELI2:FKey_DID_ItemNo) = LEVEL:Benign
               ! Set defaults     - using a group means we dont need to do individual assignments!
               LOC:Delivery_Composition_Group :=: A_DELI2:Record
               DELI:Record                    :=: LOC:Delivery_Composition_Group
         .  .
      OF ViewRecord
         DISABLE(?L_LV:Container_Info_Not_Available)
      OF ChangeRecord            ! SELF.Request
         ! Check if we specified a volume or not
         IF DELI:Volume ~= 0.0
            IF DELI:Length ~= 0.0 AND DELI:Breadth ~= 0.0 AND DELI:Height ~= 0.0
               ! We calculated the volume
               L_cV:Length        = DELI:Length  * 100
               L_cV:Breadth       = DELI:Breadth * 100
               L_cV:Height        = DELI:Height  * 100
            ELSE
               DELIC:DIID         = DELI:DIID
               IF Access:DeliveryItems_Components.TryFetch(DELIC:FKey_DIID) = LEVEL:Benign
                  LOC:Enter_Volume   = 2
               ELSE
                  LOC:Enter_Volume   = 1
      .  .  .  .
  
  
      CASE p:RealMode
      OF InsertRecord
         IF L_LT:LoadOption = 2 OR L_LT:LoadOption = 4 OR L_LT:LoadOption = 1 OR L_LT:TurnIn = TRUE
            DELI:ShowOnInvoice    = TRUE                ! This may be
         .
  
         IF L_LT:LoadOption = 1 OR L_LT:LoadOption = 2             ! Container Park or Container
            ! If FBN Floor then dont ask for container info.
            FLO:FID = DEL:FID
            IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
               IF FLO:FBNFloor = TRUE
                  L_LV:Container_Info_Not_Available  = TRUE
                  DELI:ShowOnInvoice                 = FALSE
                  DO Container_Options
         .  .  .
      ELSE
         IF L_LT:LoadOption = 1 OR L_LT:LoadOption = 2             ! Container Park or Container
            IF DELI:COID ~= 0 OR CLIP(DELI:ContainerNo) ~= '' OR CLIP(DELI:ContainerVessel) ~= 0 OR CLIP(DELI:SealNo) ~= '' |
                  OR DELI:ContainerReturnAID ~= 0
            ELSE
               L_LV:Container_Info_Not_Available      = TRUE
               DO Container_Options
      .  .  .
  FDCB19.Init(DELI:ContainerVessel,?DELI:ContainerVessel,Queue:FileDropCombo.ViewPosition,FDCB19::View:FileDropCombo,Queue:FileDropCombo,Relate:Vessels,ThisWindow,GlobalErrors,1,1,0)
  FDCB19.AskProcedure = 6
  FDCB19.Q &= Queue:FileDropCombo
  FDCB19.AddSortOrder(VES:Key_Vessel)
  FDCB19.AddField(VES:Vessel,FDCB19.Q.VES:Vessel) !List box control field - type derived from field
  FDCB19.AddField(VES:VEID,FDCB19.Q.VES:VEID) !Primary key field - type derived from field
  FDCB19.AddUpdateField(VES:Vessel,DELI:ContainerVessel)
  ThisWindow.AddItem(FDCB19.WindowComponent)
  FDCB19.DefaultFill = 0
  FDCB18.Init(L_LV:Packaging,?L_LV:Packaging,Queue:FileDropCombo:1.ViewPosition,FDCB18::View:FileDropCombo,Queue:FileDropCombo:1,Relate:PackagingTypes,ThisWindow,GlobalErrors,1,1,0)
  FDCB18.AskProcedure = 7
  FDCB18.Q &= Queue:FileDropCombo:1
  FDCB18.AddSortOrder(PACK:Key_Packaging)
  FDCB18.AddField(PACK:Packaging,FDCB18.Q.PACK:Packaging) !List box control field - type derived from field
  FDCB18.AddField(PACK:PTID,FDCB18.Q.PACK:PTID) !Primary key field - type derived from field
  FDCB18.AddUpdateField(PACK:PTID,DELI:PTID)
  ThisWindow.AddItem(FDCB18.WindowComponent)
  FDCB18.DefaultFill = 0
  FDCB20.Init(L_LV:Commodity,?L_LV:Commodity,Queue:FileDropCombo:2.ViewPosition,FDCB20::View:FileDropCombo,Queue:FileDropCombo:2,Relate:Commodities,ThisWindow,GlobalErrors,1,1,0)
  FDCB20.AskProcedure = 8
  FDCB20.Q &= Queue:FileDropCombo:2
  FDCB20.AddSortOrder(COM:Key_Commodity)
  FDCB20.AddField(COM:Commodity,FDCB20.Q.COM:Commodity) !List box control field - type derived from field
  FDCB20.AddField(COM:CMID,FDCB20.Q.COM:CMID) !Primary key field - type derived from field
  FDCB20.AddUpdateField(COM:CMID,DELI:CMID)
  ThisWindow.AddItem(FDCB20.WindowComponent)
  FDCB20.DefaultFill = 0
  BRW2.AddToolbarTarget(Toolbar)                  ! Browse accepts toolbar control
  BRW2.ToolbarItem.HelpButton = ?Help
  BRW4::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW4::FormatManager.Init('MANTRNIS','Update_DeliveryItems_old_change_to_commodity',1,?Browse:4,4,BRW4::PopupTextExt,Queue:Browse:4,6,LFM_CFile,LFM_CFile.Record)
  BRW4::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW6::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW6::FormatManager.Init('MANTRNIS','Update_DeliveryItems_old_change_to_commodity',1,?Browse:6,6,BRW6::PopupTextExt,Queue:Browse:6,7,LFM_CFile,LFM_CFile.Record)
  BRW6::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
    Relate:Vessels.Close
  END
  ! List Format Manager destructor
  BRW4::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW6::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_DeliveryItems_old_change_to_commodity',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  DELI:Units = 1
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Commodities(L_LV:Commodity)
      Browse_PackagingTypes
      Browse_ContainerTypes
      Browse_ContainerOperators
      Browse_Addresses
      Update_Vessels
      Update_PackagingTypes
      Update_Commodities
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_LV:Commodity
      FDCB20.TakeAccepted()
    OF ?L_LV:Packaging
      FDCB18.TakeAccepted()
    OF ?L_LV:Volume_Rounding
          DO Compute_Vol
    OF ?L_LV:Volume_Per_Unit
          DO Compute_Vol
      
          DO Enter_Vol_Check
      
          DISPLAY
    OF ?L_cV:Length
          DO Compute_Vol
    OF ?L_cV:Breadth
          DO Compute_Vol
    OF ?L_cV:Height
          DO Compute_Vol
    OF ?DELI:Volume_Unit
          DO Compute_Vol
    OF ?LOC:Enter_Volume
          IF QuickWindow{PROP:AcceptAll} = FALSE
             CASE LOC:Enter_Volume
             OF 0                        ! Calc. Volume
                ENABLE(?Group_Measure)
                DISABLE(?Group_VolTot)
                DISABLE(?Tab_Split_Vol)
             OF 1                        ! Enter Volume
                DISABLE(?Group_Measure)
                ENABLE(?Group_VolTot)
                DISABLE(?Tab_Split_Vol)
      
                L_LV:Volume_Per_Unit = FALSE
                DISPLAY(?L_LV:Volume_Per_Unit)
             OF 2                        ! Split vol.
                DISABLE(?Group_Measure)
                DISABLE(?Group_VolTot)
                ENABLE(?Tab_Split_Vol)
      
                SELECT(?Tab_Split_Vol)
          .  .
          
    OF ?Button_Set_Vol_Ratio
      ThisWindow.Update()
          CLI:CID                       = L_LV:CID
          IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
             DELI:VolumetricRatio    = CLI:VolumetricRatio
             DISPLAY(?L_LV:VolumetricRatio_FBN)
      
             DO Vol_Q
      
             CLEAR(LOC:Vol_Ratio_Q)
             L_VRQ:VolumetricRatio           = DELI:VolumetricRatio
             GET(LOC:Vol_Ratio_Q, L_VRQ:VolumetricRatio)
             IF ERRORCODE()
                L_VRQ:VolumetricRatio        = DELI:VolumetricRatio
                L_VRQ:VolumetricRatio_Vol    = 1000 / L_VRQ:VolumetricRatio
                L_VRQ:Client                 = 'Client'
                ADD(LOC:Vol_Ratio_Q, +L_VRQ:VolumetricRatio)
             ELSE
                L_VRQ:Client                 = 'Client'
                PUT(LOC:Vol_Ratio_Q)
             .
      
      
             L_LV:VolumetricRatio    = L_VRQ:VolumetricRatio_Vol
      
      
             DO Compute_Vol
      
             DO Enter_Vol_Check
          .
    OF ?Button_Set_Vol_Ratio:2
      ThisWindow.Update()
          ?L_LV:VolumetricRatio_FBN{PROP:Background}  = -1
          ?L_LV:VolumetricRatio_FBN{PROP:ReadOnly}    = FALSE
    OF ?L_LV:VolumetricRatio_FBN
          DELI:VolumetricRatio      = 1000 / L_LV:VolumetricRatio_FBN
          
          DO Compute_Vol
    OF ?L_LV:Container_Info_Not_Available
          DO Container_Options
    OF ?CallLookup:2
      ThisWindow.Update()
      CTYP:ContainerType = L_LV:ContainerType
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        L_LV:ContainerType = CTYP:ContainerType
        DELI:CTID = CTYP:CTID
      END
      ThisWindow.Reset(1)
          ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
          IF L_LT:LoadOption ~= 2 AND L_LT:LoadOption ~= 4 OR Get_ContainerType_Info(DELI:CTID,0) = 1
             ENABLE(?Volume_Group)
          ELSE
             DISABLE(?Volume_Group)
          .
    OF ?L_LV:ContainerType
      IF L_LV:ContainerType OR ?L_LV:ContainerType{PROP:Req}
        CTYP:ContainerType = L_LV:ContainerType
        IF Access:ContainerTypes.TryFetch(CTYP:Key_Type)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            L_LV:ContainerType = CTYP:ContainerType
            DELI:CTID = CTYP:CTID
          ELSE
            CLEAR(DELI:CTID)
            SELECT(?L_LV:ContainerType)
            CYCLE
          END
        ELSE
          DELI:CTID = CTYP:CTID
        END
      END
      ThisWindow.Reset()
          IF L_LT:LoadOption ~= 2 AND L_LT:LoadOption ~= 4 OR Get_ContainerType_Info(DELI:CTID,0) = 1
             ENABLE(?Volume_Group)
          ELSE
             DISABLE(?Volume_Group)
          .
    OF ?CallLookup
      ThisWindow.Update()
      CONO:ContainerOperator = L_LV:ContainerOperator
      IF SELF.Run(4,SelectRecord) = RequestCompleted
        L_LV:ContainerOperator = CONO:ContainerOperator
        DELI:COID = CONO:COID
      END
      ThisWindow.Reset(1)
    OF ?L_LV:ContainerOperator
      IF L_LV:ContainerOperator OR ?L_LV:ContainerOperator{PROP:Req}
        CONO:ContainerOperator = L_LV:ContainerOperator
        IF Access:ContainerOperators.TryFetch(CONO:Key_Operator)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            L_LV:ContainerOperator = CONO:ContainerOperator
            DELI:COID = CONO:COID
          ELSE
            CLEAR(DELI:COID)
            SELECT(?L_LV:ContainerOperator)
            CYCLE
          END
        ELSE
          DELI:COID = CONO:COID
        END
      END
      ThisWindow.Reset()
    OF ?DELI:ContainerVessel
      FDCB19.TakeAccepted()
    OF ?CallLookup:3
      ThisWindow.Update()
      ADD:AddressName = L_LV:ContainerReturnAddressName
      IF SELF.Run(5,SelectRecord) = RequestCompleted
        L_LV:ContainerReturnAddressName = ADD:AddressName
        DELI:ContainerReturnAID = ADD:AID
      END
      ThisWindow.Reset(1)
    OF ?L_LV:ContainerReturnAddressName
      IF L_LV:ContainerReturnAddressName OR ?L_LV:ContainerReturnAddressName{PROP:Req}
        ADD:AddressName = L_LV:ContainerReturnAddressName
        IF Access:Addresses.TryFetch(ADD:Key_Name)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            L_LV:ContainerReturnAddressName = ADD:AddressName
            DELI:ContainerReturnAID = ADD:AID
          ELSE
            CLEAR(DELI:ContainerReturnAID)
            SELECT(?L_LV:ContainerReturnAddressName)
            CYCLE
          END
        ELSE
          DELI:ContainerReturnAID = ADD:AID
        END
      END
      ThisWindow.Reset()
    OF ?Button_Add
      ThisWindow.Update()
          IF L_ICG:Units <= 0
             MESSAGE('Please enter a value for units.', 'Delivery Item Components', ICON:Exclamation)
             SELECT(?L_ICG:Units)
             CYCLE
          .
          IF L_ICG:Length = 0 OR L_ICG:Breadth = 0 OR L_ICG:Height = 0
             MESSAGE('Please enter a value for all the dimensions.', 'Delivery Item Components', ICON:Exclamation)
      !       MESSAGE('Please enter a value for all the dimensions.||L_ICG:Length: ' & L_ICG:Length &',  L_ICG:Breadth: ' & L_ICG:Breadth & ',  L_ICG:Height: ' & L_ICG:Height, 'Delivery Item Components', ICON:Exclamation)
             SELECT(?L_ICG:Length)
             CYCLE
          .
      
      
          IF L_TCG:Units + L_ICG:Units > DELI:Units
             MESSAGE('Too many units would be loaded.', 'Delivery Item Components', ICON:Exclamation)
             L_ICG:Units                  = DELI:Units - L_TCG:Units
          ELSE
             IF Access:DeliveryItems_Components.TryPrimeAutoInc() ~= LEVEL:Benign
                MESSAGE('File problem on add prime.', 'Delivery Item Components', ICON:Exclamation)
             ELSE
                DO Delivery_Comp_WVol
      
                !DELIC:DICID
                DELIC:DIID                = DELI:DIID
      
                DELIC:Length              = L_ICG:Length
                DELIC:Breadth             = L_ICG:Breadth
                DELIC:Height              = L_ICG:Height
      
                DELIC:Units               = L_ICG:Units
                DELIC:Weight              = L_ICG:Weight
      
                DELIC:Volume              = L_ICG:Volume
                DELIC:Volume_Unit         = L_ICG:Volume_Unit
      
                DELIC:VolumetricWeight    = L_ICG:VolumetricWeight
      
                !DELIC:Volume              = DELIC:Length * DELIC:Breadth * DELIC:Height
                !DELIC:VolumetricWeight    = DELIC:Volume * DELI:VolumetricRatio
      
                IF Access:DeliveryItems_Components.Insert() ~= LEVEL:Benign
                   !MESSAGE('File problem on add.', 'Delivery Item Components', ICON:Exclamation)
                ELSE
                   !BRW2.ResetFromFile()
                   BRW2.ResetFromView()
      
                   CLEAR(LOC:Item_Comp)
      
                   DO Enter_Vol_Check
      
                   L_LV:Updated_Child     = TRUE
          .  .  .
      
          DISPLAY
    OF ?Button_Update
      ThisWindow.Update()
          IF L_ICG:Units <= 0
             MESSAGE('Please enter a value for units or delete this entry.', 'Delivery Item Components', ICON:Exclamation)
             SELECT(?L_ICG:Units)
             CYCLE
          .
          IF L_ICG:Length = 0.0 OR L_ICG:Breadth = 0.0 OR L_ICG:Height = 0.0
             MESSAGE('Please enter a value for all the dimensions.', 'Delivery Item Components', ICON:Exclamation)
             SELECT(?L_ICG:Length)
             CYCLE
          .
      
          IF (L_TCG:Units + L_ICG:Units - DELIC:Units) > DELI:Units
             MESSAGE('Too many units would be loaded.', 'Delivery Item Components', ICON:Exclamation)
          ELSE
             DO Delivery_Comp_WVol
      
             DELIC:Length              = L_ICG:Length
             DELIC:Breadth             = L_ICG:Breadth
             DELIC:Height              = L_ICG:Height
      
             DELIC:Units               = L_ICG:Units
             DELIC:Weight              = L_ICG:Weight
      
             DELIC:Volume              = L_ICG:Volume
             DELIC:VolumetricWeight    = L_ICG:VolumetricWeight
      
             IF Access:DeliveryItems_Components.TryUpdate() ~= LEVEL:Benign
                MESSAGE('File problem on update.', 'Delivery Item Components', ICON:Exclamation)
             ELSE
                BRW2.ResetFromFile()
                BRW2.ResetFromView()
      
                DO Enter_Vol_Check
      
                L_LV:Updated_Child     = TRUE
          .  .
      
          DISPLAY
    OF ?Button_Delete
      ThisWindow.Update()
      BRW2.UpdateViewRecord()
      !    CASE MESSAGE('Are you sure?', 'Delete Component', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
      !    OF BUTTON:Yes
          IF Relate:DeliveryItems_Components.Delete() = LEVEL:Benign
             CLEAR(LOC:Item_Comp)
             BRW2.ResetFromFile()
             BRW2.ResetFromView()
      
             DO Enter_Vol_Check
      
             L_LV:Updated_Child     = TRUE
          .
    OF ?L_ICG:Length
          DO Delivery_Comp_WVol
    OF ?L_ICG:Breadth
          DO Delivery_Comp_WVol
    OF ?L_ICG:Height
          DO Delivery_Comp_WVol
    OF ?L_ICG:Units
          DO Delivery_Comp_WVol
    OF ?OK
      ThisWindow.Update()
          IF DELI:Units <= 0
             QuickWindow{PROP:AcceptAll}       = FALSE
             SELECT(?DELI:Units)
             CYCLE
          .
      
      
          IF DELI:Weight = 0.0 AND DELI:VolumetricWeight = 0.0
             CASE MESSAGE('You have no Weight or Volumetric Weight.||Would you like to change this?', 'Missing Information', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
             OF BUTTON:Yes
                QuickWindow{PROP:AcceptAll}    = FALSE
                SELECT(?DELI:Weight)
                CYCLE
          .  .
      
          IF LOC:Enter_Volume = 2         ! Splitting - then we must have these agree
             IF DELI:Volume_Unit ~= L_TCG:Volume OR DELI:VolumetricWeight ~= L_TCG:VolumetricWeight OR L_TCG:Units ~= DELI:Units
                BRW2.ResetFromView()
                IF DELI:Volume_Unit ~= L_TCG:Volume OR DELI:VolumetricWeight ~= L_TCG:VolumetricWeight OR L_TCG:Units ~= DELI:Units
                   MESSAGE('When Volume Option is Split, the split totals must by kept the same as the item values.||The Volumes or Units are not totaling to the Delivery Item specified amounts.', 'Missing Information', ICON:Hand)
                   QuickWindow{PROP:AcceptAll}    = FALSE
                   SELECT(?)
                   CYCLE
          .  .  .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
    IF p:RealMode ~= InsertRecord AND L_LV:Updated_Child = TRUE
       IF LOC:Enter_Volume = 2         ! Splitting - then we must have these agree
          IF DELI:Volume_Unit ~= L_TCG:Volume OR DELI:VolumetricWeight ~= L_TCG:VolumetricWeight OR L_TCG:Units ~= DELI:Units
             BRW2.ResetFromView()
             IF DELI:Volume_Unit ~= L_TCG:Volume OR DELI:VolumetricWeight ~= L_TCG:VolumetricWeight OR L_TCG:Units ~= DELI:Units
                MESSAGE('When Volume Option is Split, the split totals must by kept the same as the item values.||The Volumes or Units are not totaling to the Delivery Item specified amounts.', 'Missing Information', ICON:Hand)
                QuickWindow{PROP:AcceptAll}    = FALSE
                SELECT(?DELI:Units)
                RETURN( LEVEL:Notify )
    .  .  .  .
  
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LOC:Enter_Volume
          CASE LOC:Enter_Volume
          OF 0                        ! Calc. Volume
             ENABLE(?Group_Measure)
             DISABLE(?Group_VolTot)
             DISABLE(?Tab_Split_Vol)
          OF 1                        ! Enter Volume
             DISABLE(?Group_Measure)
             ENABLE(?Group_VolTot)
             DISABLE(?Tab_Split_Vol)
      
             L_LV:Volume_Per_Unit = FALSE
             DISPLAY(?L_LV:Volume_Per_Unit)
          OF 2                        ! Split vol.
             DISABLE(?Group_Measure)
             DISABLE(?Group_VolTot)
             SELECT(?Tab_Split_Vol)
          .
          
    OF ?L_LV:VolumetricRatio
          IF QuickWindow{PROP:AcceptAll} = FALSE
             GET(LOC:Vol_Ratio_Q, CHOICE(?L_LV:VolumetricRatio))
             IF ~ERRORCODE()
                DELI:VolumetricRatio      = L_VRQ:VolumetricRatio
                L_LV:VolumetricRatio_FBN  = L_VRQ:VolumetricRatio_Vol         ! FBN style...
                DISPLAY
                DO Compute_Vol
          .  .
    OF ?List
      BRW2.UpdateViewRecord()
          LOC:Item_Comp   :=: DELIC:RECORD
          DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          COM:CMID                    = DELI:CMID
          IF Access:Commodities.TryFetch(COM:PKey_CMID) = LEVEL:Benign
             L_LV:Commodity           = COM:Commodity
          .
      
          CONO:COID                   = DELI:COID
          IF Access:ContainerOperators.TryFetch(CONO:PKey_COID) = LEVEL:Benign
             L_LV:ContainerOperator   = CONO:ContainerOperator
          .
      
          CTYP:CTID                   = DELI:CTID
          IF Access:ContainerTypes.TryFetch(CTYP:PKey_CTID) = LEVEL:Benign
             L_LV:ContainerType       = CTYP:ContainerType
          .
      
          ADD:AID                     = DELI:ContainerReturnAID
          IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
             L_LV:ContainerReturnAddressName  = ADD:AddressName
          .
      
          PACK:PTID                   = DELI:PTID
          IF Access:PackagingTypes.TryFetch(PACK:PKey_PTID) = LEVEL:Benign
             L_LV:Packaging           = PACK:Packaging
          .
      
          L_LV:Volume_Per_Unit        = TRUE
          IF p:RealMode = InsertRecord
          ELSE
             IF DELI:Volume ~= DELI:Volume_Unit
                L_LV:Volume_Per_Unit  = TRUE
             ELSE
                L_LV:Volume_Per_Unit  = FALSE
      
                DELIC:DIID   = DELI:DIID
                IF Access:DeliveryItems_Components.TryFetch(DELIC:FKey_DIID) = LEVEL:Benign
                   IF DELIC:Volume ~= DELIC:Volume_Unit
                      L_LV:Volume_Per_Unit  = TRUE
          .  .  .  .
      
      
          CASE LOC:Enter_Volume
          OF 0                        ! Calc. Volume
             ENABLE(?Group_Measure)
             DISABLE(?Group_VolTot)
             DISABLE(?Tab_Split_Vol)
          OF 1                        ! Enter Volume
             DISABLE(?Group_Measure)
             ENABLE(?Group_VolTot)
             DISABLE(?Tab_Split_Vol)
          OF 2                        ! Split vol.
             DISABLE(?Group_Measure)
             DISABLE(?Group_VolTot)
             ENABLE(?Tab_Split_Vol)
      
             SELECT(?Tab_Split_Vol)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          DO Vol_Q
      
          CLEAR(LOC:Vol_Ratio_Q)
          L_VRQ:VolumetricRatio           = DELI:VolumetricRatio
          GET(LOC:Vol_Ratio_Q, L_VRQ:VolumetricRatio)
          IF ERRORCODE()
             L_VRQ:VolumetricRatio        = DELI:VolumetricRatio
             L_VRQ:VolumetricRatio_Vol    = 1000 / L_VRQ:VolumetricRatio
             L_VRQ:Client                 = 'Client'
             ADD(LOC:Vol_Ratio_Q, +L_VRQ:VolumetricRatio)
          ELSE
             L_VRQ:Client                 = 'Client'
             PUT(LOC:Vol_Ratio_Q)
          .
      
      
          L_LV:VolumetricRatio    = L_VRQ:VolumetricRatio_Vol
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW4.ResetFromView PROCEDURE

L_BT:TripSheet_Loaded:Sum REAL                             ! Sum variable for browse totals
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:TripSheetDeliveries.SetQuickScan(1)
  SELF.Reset
  IF SELF.UseMRP
     IF SELF.View{PROP:IPRequestCount} = 0
          SELF.View{PROP:IPRequestCount} = 60
     END
  END
  LOOP
    IF SELF.UseMRP
       IF SELF.View{PROP:IPRequestCount} = 0
            SELF.View{PROP:IPRequestCount} = 60
       END
    END
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      SETCURSOR()
      RETURN
    END
    SELF.SetQueueRecord
    L_BT:TripSheet_Loaded:Sum += TRDI:UnitsLoaded
  END
  SELF.View{PROP:IPRequestCount} = 0
  L_BT:TripSheet_Loaded = L_BT:TripSheet_Loaded:Sum
  PARENT.ResetFromView
  Relate:TripSheetDeliveries.SetQuickScan(0)
  SETCURSOR()


BRW4.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW4::LastSortOrder <> NewOrder THEN
     BRW4::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW4::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW4.TakeNewSelection PROCEDURE

  CODE
  IF BRW4::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW4::PopupTextExt = ''
        BRW4::PopupChoiceExec = True
        BRW4::FormatManager.MakePopup(BRW4::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW4::PopupTextExt = '|-|' & CLIP(BRW4::PopupTextExt)
        END
        BRW4::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW4::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW4::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW4::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW4::PopupChoiceOn AND BRW4::PopupChoiceExec THEN
     BRW4::PopupChoiceExec = False
     BRW4::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW4::PopupTextExt)
     IF BRW4::FormatManager.DispatchChoice(BRW4::PopupChoice)
     ELSE
     END
  END


BRW2.PrimeRecord PROCEDURE(BYTE SuppressClear = 0)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.PrimeRecord(SuppressClear)
  DELIC:Units = 1
  RETURN ReturnValue


BRW2.ResetFromView PROCEDURE

L_TCG:Units:Sum      REAL                                  ! Sum variable for browse totals
L_TCG:Weight:Sum     REAL                                  ! Sum variable for browse totals
L_TCG:VolumetricWeight:Sum REAL                            ! Sum variable for browse totals
L_TCG:Volume:Sum     REAL                                  ! Sum variable for browse totals
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:DeliveryItems_Components.SetQuickScan(1)
  SELF.Reset
  IF SELF.UseMRP
     IF SELF.View{PROP:IPRequestCount} = 0
          SELF.View{PROP:IPRequestCount} = 60
     END
  END
  LOOP
    IF SELF.UseMRP
       IF SELF.View{PROP:IPRequestCount} = 0
            SELF.View{PROP:IPRequestCount} = 60
       END
    END
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      SETCURSOR()
      RETURN
    END
    SELF.SetQueueRecord
    L_TCG:Units:Sum += DELIC:Units
    L_TCG:Weight:Sum += DELIC:Weight
    L_TCG:VolumetricWeight:Sum += DELIC:VolumetricWeight
    L_TCG:Volume:Sum += DELIC:Volume
  END
  SELF.View{PROP:IPRequestCount} = 0
  L_TCG:Units = L_TCG:Units:Sum
  L_TCG:Weight = L_TCG:Weight:Sum
  L_TCG:VolumetricWeight = L_TCG:VolumetricWeight:Sum
  L_TCG:Volume = L_TCG:Volume:Sum
       IF L_LV:Volume_Rounding = TRUE
          L_TCG:Volume         = ROUND(L_TCG:Volume,1)
       .
  PARENT.ResetFromView
  Relate:DeliveryItems_Components.SetQuickScan(0)
  SETCURSOR()


BRW2.SetAlerts PROCEDURE

  CODE
  SELF.EditViaPopup = False
  PARENT.SetAlerts


BRW6.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW6::LastSortOrder <> NewOrder THEN
     BRW6::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW6::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW6.TakeNewSelection PROCEDURE

  CODE
  IF BRW6::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW6::PopupTextExt = ''
        BRW6::PopupChoiceExec = True
        BRW6::FormatManager.MakePopup(BRW6::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW6::PopupTextExt = '|-|' & CLIP(BRW6::PopupTextExt)
        END
        BRW6::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW6::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW6::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW6::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW6::PopupChoiceOn AND BRW6::PopupChoiceExec THEN
     BRW6::PopupChoiceExec = False
     BRW6::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW6::PopupTextExt)
     IF BRW6::FormatManager.DispatchChoice(BRW6::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
TripSheet_BulkPOD_Win PROCEDURE (p:TRID)

LOC:Options          GROUP,PRE(L_OG)                       ! 
Branch               STRING(35)                            ! Branch Name
BID                  ULONG                                 ! Branch ID
                     END                                   ! 
LOC:Capture_Group    GROUP,PRE(L_CG)                       ! 
ReturnedDate         DATE                                  ! 
ReturnedTime         TIME                                  ! 
Notes                STRING(500)                           ! Notes
Floor                STRING(35)                            ! Floor Name
FID                  ULONG                                 ! Floor ID
                     END                                   ! 
LOC:Returned         LONG                                  ! 
FDB4::View:FileDrop  VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FID)
                     END
Queue:FileDrop       QUEUE                            !
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('POD Bulk Capture (Trip Sheets Returned)'),AT(,,318,183),FONT('Tahoma',8,,FONT:regular), |
  DOUBLE,CENTER,GRAY,MDI
                       SHEET,AT(4,4,309,160),USE(?Sheet1)
                         TAB('Trip Sheet'),USE(?Tab1)
                           PROMPT('Transporter Name:'),AT(10,22),USE(?TRA:TransporterName:Prompt),TRN
                           ENTRY(@s35),AT(77,22,231,10),USE(TRA:TransporterName),COLOR(00E9E9E9h),MSG('Transporters Name'), |
  READONLY,SKIP,TIP('Transporters Name')
                           PROMPT('Composition Name:'),AT(10,36),USE(?VCO:CompositionName:Prompt),TRN
                           ENTRY(@s35),AT(77,36,231,10),USE(VCO:CompositionName),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Depart Date:'),AT(10,50),USE(?TRI:DepartDate:Prompt),TRN
                           ENTRY(@d6),AT(77,50,71,10),USE(TRI:DepartDate),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Depart Time:'),AT(10,64),USE(?TRI:DepartTime:Prompt),TRN
                           ENTRY(@t7),AT(77,64,71,10),USE(TRI:DepartTime),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Tripsheet ID (TRID):'),AT(170,64),USE(?TRI:DepartTime:Prompt:2),TRN
                           ENTRY(@N_10),AT(237,64,71,10),USE(TRI:TRID),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP,MSG('Tripsheet ID')
                           LINE,AT(10,79,298,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Returned Date:'),AT(10,86),USE(?ReturnedDate:Prompt),TRN
                           SPIN(@d6),AT(77,86,71,10),USE(L_CG:ReturnedDate)
                           BUTTON('...'),AT(150,86,12,10),USE(?Calendar)
                           PROMPT('Returned Time:'),AT(10,102),USE(?ReturnedTime:Prompt),TRN
                           ENTRY(@t7),AT(77,102,71,10),USE(L_CG:ReturnedTime)
                           PROMPT('Move to &Floor:'),AT(189,86),USE(?ReturnedDate:Prompt:2),TRN
                           LIST,AT(237,86,71,10),USE(L_CG:Floor),VSCROLL,DROP(15),FORMAT('140L(2)|M~Floor~@s35@'),FROM(Queue:FileDrop)
                           PROMPT('Notes:'),AT(10,118),USE(?Notes:Prompt),TRN
                           TEXT,AT(77,118,231,40),USE(L_CG:Notes),VSCROLL,BOXED,MSG('Notes'),TIP('Notes')
                         END
                       END
                       BUTTON('&OK'),AT(200,166,,14),USE(?OkButton),LEFT,ICON('waok.ico'),DEFAULT,FLAT
                       BUTTON('&Cancel'),AT(258,166,,14),USE(?CancelButton),LEFT,ICON('wacancel.ico'),FLAT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Calendar3            CalendarClass
FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:Returned)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('TripSheet_BulkPOD_Win')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRA:TransporterName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Floors.Open                                       ! File Floors used by this procedure, so make sure it's RelationManager is open
  Access:TripSheets.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleComposition.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
      TRI:TRID        = p:TRID
      IF Access:TripSheets.TryFetch(TRI:PKey_TID) ~= LEVEL:Benign
         LOC:Returned = -2
         RETURN(LEVEL:Fatal)
      .
  
  
      VCO:VCID        = TRI:VCID
      IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) ~= LEVEL:Benign
         CLEAR(VCO:Record)
      .
      TRA:TID         = VCO:TID
      IF Access:Transporter.TryFetch(TRA:PKey_TID) ~= LEVEL:Benign
         CLEAR(TRA:Record)
      .
  
  
      L_CG:ReturnedDate   = TRI:ReturnedDate
      L_CG:ReturnedTime   = TRI:ReturnedTime
      L_CG:Notes          = TRI:Notes
  Do DefineListboxStyle
  INIMgr.Fetch('TripSheet_BulkPOD_Win',Window)             ! Restore window settings from non-volatile store
      FLO:FID = GETINI('TripSheet_BulkPOD_Win', 'Move_Floor', , '.\TransIS.INI')    ! Changed to use setting, here to set default only
  
      ! Get_Setting
      ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip, p:Update)
      ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0),STRING
      !   1       2       3           4           5
      FLO:FID = Get_Setting('Trip Sheet Delivery Floor', 1, FLO:FID, '@n_3','The Floor ID (FID) to default all Trip Sheet Captured POD<39>s to.')
  
      IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
         L_CG:FID     = FLO:FID
         L_CG:Floor   = FLO:Floor
      .
  FDB4.Init(?L_CG:Floor,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:Floors,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(FLO:Key_Floor)
  FDB4.AddField(FLO:Floor,FDB4.Q.FLO:Floor) !List box control field - type derived from field
  FDB4.AddField(FLO:FID,FDB4.Q.FLO:FID) !Primary key field - type derived from field
  FDB4.AddUpdateField(FLO:FID,L_CG:FID)
  ThisWindow.AddItem(FDB4.WindowComponent)
  FDB4.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Floors.Close
  END
  IF SELF.Opened
    INIMgr.Update('TripSheet_BulkPOD_Win',Window)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
          IF L_CG:FID = 0
             MESSAGE('Please select a Floor.','Floor Required',ICON:Exclamation)
             SELECT(?L_CG:Floor)
          .
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar3.SelectOnClose = True
      Calendar3.Ask('Select a Date',L_CG:ReturnedDate)
      IF Calendar3.Response = RequestCompleted THEN
      L_CG:ReturnedDate=Calendar3.SelectedDate
      DISPLAY(?L_CG:ReturnedDate)
      END
      ThisWindow.Reset(True)
    OF ?OkButton
      ThisWindow.Update()
          IF L_CG:ReturnedDate = 0
             MESSAGE('Please specify a Returned Date.', 'Trip Sheet POD', ICON:Exclamation)
             Window{PROP:AcceptAll}   = FALSE
             SELECT(?L_CG:ReturnedDate)
             CYCLE
          .
          IF L_CG:ReturnedTime = 0
             MESSAGE('Please specify a Returned Time.', 'Trip Sheet POD', ICON:Exclamation)
             Window{PROP:AcceptAll}   = FALSE
             SELECT(?L_CG:ReturnedTime)
             CYCLE
          .
      
          IF TRI:DepartDate > L_CG:ReturnedDate
             CASE MESSAGE('The Departure date is later than the Return date.||Are you sure?', 'Trip Sheet POD', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
             OF BUTTON:No
                Window{PROP:AcceptAll}   = FALSE
                SELECT(?L_CG:ReturnedDate)
                CYCLE
             .
          ELSIF TRI:DepartDate = L_CG:ReturnedDate
             IF TRI:DepartTime > L_CG:ReturnedTime
                CASE MESSAGE('The Departure time is later than the Return time.||Are you sure?', 'Trip Sheet POD', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
                OF BUTTON:No
                   Window{PROP:AcceptAll}   = FALSE
                   SELECT(?L_CG:ReturnedTime)
                   CYCLE
          .  .  .
      
          
      
          TRI:ReturnedDate    = L_CG:ReturnedDate
          TRI:ReturnedTime    = L_CG:ReturnedTime
          TRI:Notes           = L_CG:Notes
          TRI:State           = 4
          TRI:FID             = L_CG:FID
      
          IF Access:TripSheets.TryUpdate() = LEVEL:Benign
               LOC:Returned     = 1
               Manifest_Emails_Setup(TRI:TRID,14)     ! Send emails
          ELSE
             LOC:Returned     = -2
             MESSAGE('Could not update the Trip Sheet.', 'Trip Sheet POD', ICON:Hand)
          .
          !PUTINI('TripSheet_BulkPOD_Win', 'Move_Floor', L_CG:FID, '.\TransIS.INI') - changed to setting
       POST(EVENT:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update()
          LOC:Returned    = -1
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

