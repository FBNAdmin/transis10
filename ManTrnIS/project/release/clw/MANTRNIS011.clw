

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('MANTRNIS011.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Reminding
!!! </summary>
Reminding PROCEDURE 

LOC:Static_Group     GROUP,PRE(L_SG),STATIC                ! 
Reminder_Load_Inc    LONG                                  ! 
                     END                                   ! 
LOC:Reminder_Group   GROUP,PRE(L_RG)                       ! 
Timer                LONG(1000)                            ! 
Last_Collected_Data  LONG                                  ! 
Idx                  LONG                                  ! 
Remind_IDs           STRING(1000)                          ! 
User_Popup           BYTE                                  ! User over all popup option
                     END                                   ! 
LOC:Temp_Group       GROUP,PRE(LOC)                        ! 
Files_Open           BYTE                                  ! 
                     END                                   ! 
LOC:Reminder_Q       QUEUE,PRE(L_RQ)                       ! 
RID                  ULONG                                 ! Reminder ID
ReminderDate         DATE                                  ! 
ReminderTime         TIME                                  ! 
Popup                BYTE                                  ! Popup when due
                     END                                   ! 
LOC:Mouse_Group      GROUP,PRE(LMO)                        ! 
Down                 BYTE                                  ! 
XPos                 LONG                                  ! 
YPos                 LONG                                  ! 
WinXPos              LONG                                  ! 
WinYPos              LONG                                  ! 
                     END                                   ! 
LOC:Rem_Win_Thread   LONG                                  ! 
Window               WINDOW,AT(1,1,53,17),FONT('Tahoma',8,,FONT:regular),ALRT(EscKey),TIMER(100),TOOLBOX
                       IMAGE('GrayMarb.bmp'),AT(1,2,18,14),USE(?Image1),CENTERED
                       REGION,AT(0,-1,19,17),USE(?Region),IMM
                       BUTTON,AT(36,2,15,15),USE(?CancelButton),LEFT,ICON('CANCEL2.ICO'),FLAT,SKIP
                       STRING('00'),AT(2,5,15),USE(?String_Status),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),CENTER, |
  TRN
                       BUTTON,AT(20,2,15,15),USE(?Activate_Button),LEFT,ICON('Today.ico'),FLAT,SKIP,TIP('Show all r' & |
  'eminders now')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

View_Rem            VIEW(Reminders)
       JOIN(REU:FKey_RID, REM:RID)
    .  .

Rem_View            ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Update_Reminders            ROUTINE
    DATA
R:Time      TIME
R:Date      DATE
R:LastRID   LIKE(REM:RID)

    CODE
    ! We want to know what reminders to pop up when.
    FREE(LOC:Reminder_Q)
    Relate:Reminders.Open()
    Access:Reminders.UseFile()
    Relate:RemindersUsers.Open()
    Access:RemindersUsers.UseFile()

    Rem_View.Init(View_Rem, Relate:Reminders)
    Rem_View.AddSortOrder()
    Rem_View.AppendOrder('-REM:ReminderDateTime,REM:RID')
    !Rem_View.AddRange()
    Rem_View.SetFilter('REM:Active = 1', '1')

    !Alias_" = RemindersUsers{PROP:Alias}
    ! Note:         Adding files will require changing the Alias used below!!   i.e. "A." and "B."
    !   A = Reminders, B = RemindersUsers

    ! Filter - No User ID OR User Group ID (all users?) OR User ID OR User Group ID is in Users list
    Rem_View.SetFilter('(REM:UID = 0 AND REM:UGID = 0) OR (REM:UID = ' & GLO:UID & ' OR SQL(A.UGID IN (SELECT UGID FROM dbo.UsersGroups WHERE UID = ' & GLO:UID & ')))', '2')

    R:Date  = TODAY()
    R:Time  = CLOCK() + (100 * 60 * 90)
    IF CLOCK() + (100 * 60 * 90) > 8640000
       R:Time   = CLOCK() + (100 * 60 * 90) - 8640000 + 1
       R:Date  += 1
    .

    ! Returns SQL Date / Time string - (p:Date, p:Time, p:Encompass)
    ! SQL_Get_DateT_G
    

    ! Filter - Date is Today AND Earlier than Cur Time OR Date is earlier then yesterday
    Rem_View.SetFilter('SQL(A.ReminderDateTime < ' & SQL_Get_DateT_G(R:Date, R:Time) & ')', '3')    ! 1.5 hours from now

    ! Reminder User Table
    ! Filter - No Reminder User OR Reminder User is 0 OR Reminder User Action is 0 (joined tables!)
    Rem_View.SetFilter('SQL(B.RUID IS NULL) OR REU:RUID = 0 OR REU:NoAction = 0', '91')     ! Not no action

    ! Filter - No Reminder User OR Reminder User is 0 OR RU Date is today AND RU Time is earlier OR RU Date is earlier
!    Rem_View.SetFilter('(SQL(B.RUID IS NULL) OR REU:RUID = 0 OR REU:UID <> ' & GLO:UID & ' OR (REU:UID = ' & GLO:UID & ' AND ((REU:ReminderDate = ' & R:Date & ' AND REU:ReminderTime < ' & R:Time & |
!                       ') OR REU:ReminderDate < ' & R:Date & ')))', '92')                   ! 1.5 hours from now
    Rem_View.SetFilter('(SQL(B.RUID IS NULL) OR REU:RUID = 0 OR REU:UID <> ' & GLO:UID & ' OR (REU:UID = ' & GLO:UID & |
                        ' AND (SQL(B.ReminderDateTime < ' & SQL_Get_DateT_G(R:Date, R:Time) & '))))', '92')                   ! 1.5 hours from now


    ! Note: All reminders will have X no of returned results where X is the number of users that have a
    !       sub record in the ReminderUsers table for the above found Reminders.
    !       Meaning that we need to manually filter these at this loop stage.
    !       Additionally, there maybe a user personal delay that is not included in the set because
    !       the delay is later then the time frame.  So we need to check for specific user delay
    !       in addition on result.
    Rem_View.Reset()
    LOOP
       IF Rem_View.Next() ~= LEVEL:Benign
          BREAK
       .

       IF R:LastRID = REM:RID
          ! Although we have already processed the general case, there may be an override for this user
          IF REU:UID = GLO:UID AND L_RQ:RID = REM:RID
             L_RQ:ReminderDate = REU:ReminderDate
             L_RQ:ReminderTime = REU:ReminderTime

             PUT(LOC:Reminder_Q)
          .
          CYCLE
       .

       R:LastRID            = REM:RID

       IF REU:UID ~= GLO:UID
          ! Check to see if we have a User Reminder record for this user out side our date/time range
          REU:RID   = REM:RID
          REU:UID   = GLO:UID
          IF Access:RemindersUsers.TryFetch(REU:SKey_UID_RID) = LEVEL:Benign
             IF (REU:ReminderDate = R:Date AND REU:ReminderTime < R:Time) OR REU:ReminderDate < R:Date
                ! Ok
             ELSE
                CYCLE
             .
          ELSE
             CLEAR(REU:Record)
       .  .

       !REM:RID
       !REM:ReminderDate
       !REM:ReminderTime

       LOC:Reminder_Q      :=: REM:Record

       IF REU:UID = GLO:UID                         ! Use these date and times if we have user info
          L_RQ:ReminderDate = REU:ReminderDate
          L_RQ:ReminderTime = REU:ReminderTime
          !L_RQ:Popup        = REU:Popup
       .

       L_RQ:ReminderTime   -= L_RG:Timer            ! Reduce time by max possible late time

!       db.debugout('[Reminding]  Adding - Date: ' & FORMAT(L_RQ:ReminderDate,@d5) & ', Time: ' & |
!                FORMAT(L_RQ:ReminderTime,@t4) & ',   Act Date: ' & FORMAT(REM:ReminderDate,@d5) & |
!                ',  Act Time: ' & FORMAT(REM:ReminderTime,@t4) & ',   REU:UID: ' & REU:UID)

       ADD(LOC:Reminder_Q)
    .
    Rem_View.Kill()
    Relate:Reminders.Close()
    Relate:RemindersUsers.Close()

    L_RG:Last_Collected_Data    = CLOCK()

    SORT(LOC:Reminder_Q, -L_RQ:ReminderDate, -L_RQ:ReminderTime)
    ?String_Status{PROP:Text}   = RECORDS(LOC:Reminder_Q)
    DISPLAY(?String_Status)

    IF RECORDS(LOC:Reminder_Q) <= 0
       DISABLE(?Activate_Button)
    ELSE
       ENABLE(?Activate_Button)
    .
    ?CancelButton{PROP:Tip}  = 'Close reminders (Last updated at: ' & FORMAT(CLOCK(),@t4) & ')'

!    db.debugout('[Reminding]  Checked for new')
    EXIT
Action_Reminders              ROUTINE
    CLEAR(L_RG:Remind_IDs)

    L_RG:Idx    = RECORDS(LOC:Reminder_Q) + 1
    LOOP
       L_RG:Idx    -= 1
       GET(LOC:Reminder_Q, L_RG:Idx)
       IF ERRORCODE()
          BREAK
       .

       ! Dates in Q are either today or earlier so no need for check on date when doing Time
       IF L_RQ:Popup = TRUE AND (L_RQ:ReminderDate < TODAY() OR L_RQ:ReminderTime <= CLOCK())
          ! Load and check that this reminder is still relevant
          IF LOC:Files_Open = FALSE
             LOC:Files_Open     = TRUE
             Relate:Reminders.Open()
             Access:Reminders.UseFile()
          .

          REM:RID   = L_RQ:RID
          IF Access:Reminders.TryFetch(REM:PKey_RID) ~= LEVEL:Benign
             db.debugout('[Reminding]  Action - Getting reminder failed.')
          .

          !IF LOC:Add = TRUE
             ! Remind now
             L_RG:Remind_IDs   = CLIP(L_RG:Remind_IDs) & ',' & L_RQ:RID
          !.

          DELETE(LOC:Reminder_Q)
    .  .

    IF LOC:Files_Open = TRUE
       LOC:Files_Open   = FALSE
       Relate:Reminders.Close()
    .

    IF CLIP(L_RG:Remind_IDs) ~= ''
       ?String_Status{PROP:Text}       = RECORDS(LOC:Reminder_Q)
       DISPLAY(?String_Status)

        db.debugout('[Reminding]   About to start rem window with string: ' & CLIP(L_RG:Remind_IDs))

       LOC:Rem_Win_Thread   = START(Reminding_Window,, L_RG:Remind_IDs, '')
       IF LOC:Rem_Win_Thread > 0
          ! Mutex - ???
          GLO:Reminding_Waiting     = TRUE                      ! Waiting for the window to be closed

          L_RG:Last_Collected_Data  = 0
    .  .

    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Reminding')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
      PUSHBIND
      BIND('REM:Active',REM:Active)
      BIND('REM:ReminderDate',REM:ReminderDate)
      BIND('REM:ReminderTime',REM:ReminderTime)
      BIND('REU:RUID',REU:RUID)
      BIND('REU:NoAction',REU:NoAction)
      BIND('REU:ReminderDate',REU:ReminderDate)
      BIND('REU:ReminderTime',REU:ReminderTime)
      BIND('REM:UID',REM:UID)
      BIND('REM:UGID',REM:UGID)
      BIND('REU:UID',REU:UID)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Reminders.SetOpenRelated()
  Relate:Reminders.Open                                    ! File Reminders used by this procedure, so make sure it's RelationManager is open
  Access:RemindersUsers.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
    IF Thread_Add_Del_Check_Global('Reminding', '', '2') > 0
       MESSAGE('The Reminding window is already open - look on the right hand side of the screen.', 'Reminding', ICON:Exclamation)
       RETURN(LEVEL:Fatal)
    .
    Thread_Add_Del_Check_Global('Reminding', THREAD(), '0')
  
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Reminding',Window)                         ! Restore window settings from non-volatile store
      L_RG:User_Popup     = GETINI('MySettings', 'ReminderPopups-' & CLIP(GLO:Login), '1', GLO:Local_INI)
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      UNBIND('REM:Active')
      UNBIND('REM:ReminderDate')
      UNBIND('REM:ReminderTime')
      UNBIND('REU:RUID')
      UNBIND('REU:NoAction')
      UNBIND('REU:ReminderDate')
      UNBIND('REU:ReminderTime')
      UNBIND('REM:UID')
      UNBIND('REM:UGID')
      UNBIND('REU:UID')
      POPBIND
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Reminders.Close
  END
  IF SELF.Opened
    INIMgr.Update('Reminding',Window)                      ! Save window data to non-volatile store
  END
    Thread_Add_Del_Check_Global('Reminding', THREAD(), '1')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Region
          LMO:Down    = TRUE
      
          LMO:XPos    = MOUSEX()
          LMO:YPos    = MOUSEY()
      
          LMO:WinXPos = TARGET{PROP:XPos}   !Window{PROP:XPos}
          LMO:WinYPos = TARGET{PROP:YPos}   !Window{PROP:YPos}
    OF ?CancelButton
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    OF ?Activate_Button
      ThisWindow.Update()
          IF GLO:Reminding_Waiting = TRUE
             !MESSAGE('The Reminders window is already open.', 'Reminding', ICON:Exclamation)
      
             POST(EVENT:GainFocus,,LOC:Rem_Win_Thread)
          ELSE
             CLEAR(L_RG:Remind_IDs)
      
             L_RG:Idx    = RECORDS(LOC:Reminder_Q) + 1
             LOOP
                L_RG:Idx    -= 1
                GET(LOC:Reminder_Q, L_RG:Idx)
                IF ERRORCODE()
                   BREAK
                .
      
                IF LOC:Files_Open = FALSE
                   LOC:Files_Open     = TRUE
                   Relate:Reminders.Open()
                   Access:Reminders.UseFile()
                .
      
                REM:RID   = L_RQ:RID
                IF Access:Reminders.TryFetch(REM:PKey_RID) ~= LEVEL:Benign
                   db.debugout('[Reminding]  Action - Getting reminder failed.')
                .
      
                !IF LOC:Add = TRUE
                   ! Remind now
                   L_RG:Remind_IDs   = CLIP(L_RG:Remind_IDs) & ',' & L_RQ:RID
                !.
      
                DELETE(LOC:Reminder_Q)
             .
      
             IF LOC:Files_Open = TRUE
                LOC:Files_Open   = FALSE
                Relate:Reminders.Close()
             .
      
             IF CLIP(L_RG:Remind_IDs) ~= ''
                ?String_Status{PROP:Text}       = RECORDS(LOC:Reminder_Q)
                DISPLAY(?String_Status)
      
                 db.debugout('[Reminding]   User - About to start rem window with string: ' & CLIP(L_RG:Remind_IDs))
                LOC:Rem_Win_Thread   = START(Reminding_Window,, L_RG:Remind_IDs, '1')
                IF LOC:Rem_Win_Thread  > 0
                   ! Mutex - ???
                   GLO:Reminding_Waiting     = TRUE                      ! Waiting for the window to be closed
      
                   L_RG:Last_Collected_Data  = 0
          .  .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Region
    CASE EVENT()
    OF EVENT:MouseUp
          LMO:Down = FALSE
    OF EVENT:MouseMove
          IF LMO:Down = TRUE
             !Window{PROP:XPos}   = LMO:WinXPos + (MOUSEX() - LMO:XPos)
             LMO:WinYPos         += (MOUSEY() - LMO:YPos)
      
             IF LMO:WinYPos <= 0
                LMO:WinYPos       = 1
             .
      
      !    db.debugout('[]  GLO:Height: ' & GLO:Height  & ',   LMO:WinYPos: ' & LMO:WinYPos & ',   Win: ' & Window{PROP:Height} & ',   Target: ' & TARGET{PROP:Height})
      
             IF LMO:WinYPos >= (GLO:Height - Window{PROP:Height} - 23)         ! 100  10 = 88
                LMO:WinYPos       = GLO:Height - Window{PROP:Height} - 23 - 1
             .
      
             TARGET{PROP:YPos}    = LMO:WinYPos
          .
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          Window{PROP:XPos}    = GLO:Width - Window{PROP:Width} - 3
      
          
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
          IF GLO:Reminding_Waiting ~= TRUE
      !       DISABLE(?Activate_Button)
      
      !    db.debugout('[Reminding]  GLO:Reminding_Waiting: ' & GLO:Reminding_Waiting & |
      !            ', L_SG:Reminder_Load_Inc: ' & L_SG:Reminder_Load_Inc & ' ~= GLO:Reminder_Inc: ' & GLO:Reminder_Inc & |
      !            ', Time since: ' & ABS(CLOCK() - L_RG:Last_Collected_Data))
      
             ! Check we have up to date data
             IF L_SG:Reminder_Load_Inc ~= GLO:Reminder_Inc OR ABS(CLOCK() - L_RG:Last_Collected_Data) > (100 * 60 * 5)  ! Every 30 mins
                L_SG:Reminder_Load_Inc   = GLO:Reminder_Inc
                DO Update_Reminders
             .
      
      
             ! Action any reminders
             IF L_RG:User_Popup = TRUE
                DO Action_Reminders
             .
      
             Window{PROP:Timer}  = L_RG:Timer
          .
    ELSE
      IF EVENT() = EVENT:User
         Window{PROP:XPos}    = GLO:Width - Window{PROP:Width} - 3
         !Window{PROP:YPos}    = GLO:Width - Window{PROP:Width} - 5
         !GLO:Height
      .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Create_Journal PROCEDURE 

LO_G:Locals          GROUP,PRE(LO_G)                       ! 
Old_ClientName       STRING(35)                            ! 
Old_ClientNo         ULONG                                 ! Client No.
IID                  ULONG                                 ! Invoice Number
InvoiceDate          DATE                                  ! Invoice Date
InvoiceDate_2        DATE                                  ! Invoice Date
Notes                STRING(255)                           ! 
Total                DECIMAL(11,2)                         ! 
New_IID              ULONG                                 ! Invoice Number
CID                  ULONG                                 ! Client ID
ClientName           STRING(35)                            ! 
ClientNo             ULONG                                 ! Client No.
Statement            STRING(150)                           ! 
Statements           STRING(100)                           ! 
                     END                                   ! 
QuickWindow          WINDOW('Create a Journal'),AT(,,457,332),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('Create_Credit_Note')
                       SHEET,AT(4,4,449,310),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           PROMPT('Invoice Details - From Account'),AT(94,142,203,10),USE(?Prompt22),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           PROMPT('Client Name:'),AT(10,154),USE(?CLI:ClientName:Prompt),TRN
                           PROMPT('Client No.:'),AT(353,154),USE(?CLI:ClientNo:Prompt),TRN
                           PROMPT('DI No.:'),AT(10,168),USE(?INV:DINo:Prompt),TRN
                           PROMPT('Client Reference:'),AT(330,168),USE(?INV:ClientReference:Prompt),TRN
                           PROMPT('MIDs:'),AT(10,184),USE(?INV:MIDs:Prompt),TRN
                           PROMPT('Shipper Name:'),AT(10,200),USE(?INV:ShipperName:Prompt),TRN
                           PROMPT('Consignee Name:'),AT(258,200),USE(?INV:ConsigneeName:Prompt),TRN
                           PROMPT('Invoice Date:'),AT(10,216),USE(?INV:InvoiceDate:Prompt),TRN
                           PROMPT('Weight:'),AT(361,218),USE(?INV:Weight:Prompt),TRN
                           PROMPT('Charges'),AT(94,232,122,10),USE(?Prompt21),FONT(,,,FONT:bold+FONT:underline,CHARSET:ANSI), |
  TRN
                           PROMPT('Insurance:'),AT(10,244),USE(?INV:Insurance:Prompt),TRN
                           PROMPT('Documentation:'),AT(10,258),USE(?INV:Documentation:Prompt),TRN
                           PROMPT('Fuel Surcharge:'),AT(10,272),USE(?INV:FuelSurcharge:Prompt),TRN
                           PROMPT('Additional Charge:'),AT(10,286),USE(?INV:AdditionalCharge:Prompt),TRN
                           PROMPT('VAT:'),AT(369,286),USE(?INV:VAT:Prompt),TRN
                           PROMPT('Freight Charge:'),AT(10,300),USE(?INV:FreightCharge:Prompt),TRN
                           PROMPT('Total:'),AT(366,300),USE(?INV:Total:Prompt),TRN
                           PROMPT('Invoice Number:'),AT(10,22),USE(?LO_G:IID:Prompt),TRN
                           BUTTON('...'),AT(78,22,12,10),USE(?CallLookup)
                           ENTRY(@n_10),AT(94,22,60,10),USE(LO_G:IID),RIGHT(1),MSG('Invoice ID'),REQ,TIP('Invoice ID')
                           PROMPT('Credit Note Date:'),AT(10,38),USE(?LO_G:InvoiceDate:Prompt),TRN
                           BUTTON('...'),AT(78,38,12,10),USE(?Calendar)
                           SPIN(@d6),AT(94,38,60,10),USE(LO_G:InvoiceDate),RIGHT(1),MSG('Invoice Date'),TIP('Invoice Date')
                           BUTTON('...'),AT(373,38,12,10),USE(?Calendar:2)
                           PROMPT('New Invoice Date:'),AT(309,38),USE(?InvoiceDate_2:Prompt),TRN
                           SPIN(@d6),AT(389,38,60,10),USE(LO_G:InvoiceDate_2),RIGHT(1),MSG('Invoice Date'),TIP('Invoice Date')
                           PROMPT('Client Name To:'),AT(10,56),USE(?ClientName:Prompt),TRN
                           BUTTON('...'),AT(78,56,12,10),USE(?CallLookup:2)
                           ENTRY(@s35),AT(94,57,224,10),USE(LO_G:ClientName),REQ,TIP('Client name')
                           PROMPT('Client No.:'),AT(334,56),USE(?LO_G:ClientNo:Prompt),TRN
                           ENTRY(@n_10b),AT(389,57,60,10),USE(LO_G:ClientNo),RIGHT(1),COLOR(00E9E9E9h),MSG('Client No.'), |
  READONLY,SKIP,TIP('Client No.')
                           PROMPT('Notes:'),AT(10,74),USE(?LO_G:Notes:Prompt),TRN
                           TEXT,AT(94,73,354,48),USE(LO_G:Notes),VSCROLL,BOXED
                           PROMPT('These will appear on the from account entry.'),AT(10,88,79,33),USE(?Prompt23),TRN
                           LINE,AT(9,129,440,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           GROUP,AT(10,142,438,168),USE(?Group_Invoice),COLOR(00E9E9E9h),SKIP
                             ENTRY(@s35),AT(94,153,224,10),USE(LO_G:Old_ClientName),READONLY,TIP('Client name')
                             ENTRY(@n_10b),AT(389,153,60,10),USE(LO_G:Old_ClientNo),RIGHT(1),MSG('Client No.'),READONLY, |
  SKIP,TIP('Client No.')
                             ENTRY(@n_10),AT(94,169,60,10),USE(INV:DINo),RIGHT(1),MSG('Delivery Instruction Number'),READONLY, |
  TIP('Delivery Instruction Number')
                             ENTRY(@s30),AT(389,169,60,10),USE(INV:ClientReference),MSG('Client Reference'),READONLY,TIP('Client Reference')
                             ENTRY(@s20),AT(94,185,131,10),USE(INV:MIDs),MSG('List of Manifest IDs that the delivery' & |
  ' is currently manifested on'),READONLY,TIP('List of Manifest IDs that the delivery i' & |
  's currently manifested on')
                             ENTRY(@s35),AT(94,201,131,10),USE(INV:ShipperName),MSG('Name of this address'),READONLY,TIP('Name of th' & |
  'is address')
                             ENTRY(@s35),AT(317,201,131,10),USE(INV:ConsigneeName),MSG('Name of this address'),READONLY, |
  TIP('Name of this address')
                             SPIN(@d6),AT(94,217,60,10),USE(INV:InvoiceDate),RIGHT(1),MSG('Invoice Date'),READONLY,TIP('Invoice Date')
                             ENTRY(@n-14.2),AT(389,217,60,10),USE(INV:Weight),RIGHT(1),MSG('In kg''s'),READONLY,TIP('In kg''s')
                             ENTRY(@n-14.2),AT(94,243,60,10),USE(INV:Insurance),RIGHT(1),READONLY
                             ENTRY(@n-14.2),AT(94,257,60,10),USE(INV:Documentation),RIGHT(1),READONLY
                             ENTRY(@n-14.2),AT(94,273,60,10),USE(INV:FuelSurcharge),RIGHT(1),READONLY
                             ENTRY(@n-15.2),AT(94,286,60,10),USE(INV:AdditionalCharge),DECIMAL(12),MSG('Addi'),READONLY, |
  TIP('Addi')
                             ENTRY(@n-14.2),AT(389,286,60,10),USE(INV:VAT),RIGHT(1),MSG('VAT'),READONLY,TIP('VAT')
                             ENTRY(@n-15.2),AT(94,300,60,10),USE(INV:FreightCharge),RIGHT(1),READONLY
                             ENTRY(@n-15.2),AT(389,300,60,10),USE(INV:Total),RIGHT(1),READONLY
                           END
                         END
                       END
                       STRING(@s150),AT(4,320,343,10),USE(LO_G:Statement)
                       BUTTON('&OK'),AT(352,316,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(404,316,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(265,316,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar5            CalendarClass
Calendar8            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Create_Journal                                  ROUTINE
    DATA
N:CIID      LIKE(INV:IID)
N:NIID      LIKE(INV:IID)

    CODE
    ! Create Credit note with From CID then create new invoice in To CID

    ! Journal
    IF Access:_InvoiceJournals.PrimeRecord() ~= LEVEL:Benign
       Access:_InvoiceJournals.CancelAutoInc()
       MESSAGE('Failed to create Journal record.','Journal',ICON:Hand)
    ELSE
       IJOU:UID                       = GLO:UID
       IJOU:Notes                     = LO_G:Notes
       IF Access:_InvoiceJournals.TryInsert() ~= LEVEL:Benign
          Access:_InvoiceJournals.CancelAutoInc()
          MESSAGE('Failed to insert Journal record.','Journal',ICON:Hand)
       ELSE
          ! Credit Note
          CLEAR(A_INV:Record)
          IF Access:InvoiceAlias.TryPrimeAutoInc() ~= LEVEL:Benign
             MESSAGE('Failed to Create Invoice record (Credit Note).','Journal',ICON:Hand)
          ELSE
             !A_INV:IID
             LO_G:New_IID             = A_INV:IID

             A_INV:Record            :=: INV:Record                   ! Same details

             A_INV:UID                = GLO:UID
             A_INV:IJID               = IJOU:IJID

             A_INV:IID                = LO_G:New_IID

             A_INV:CR_IID             = INV:IID                       ! This marks this as a credit note...

             A_INV:InvoiceDate        = LO_G:InvoiceDate
             A_INV:InvoiceTime        = CLOCK()

             CLEAR(A_INV:Charges_Group)

             A_INV:FreightCharge      = -(INV:Insurance + INV:Documentation + INV:FuelSurcharge + INV:AdditionalCharge + INV:FreightCharge)
             A_INV:VAT                = -INV:VAT        !(A_INV:FreightCharge * (LO_G:VAT / 100))
             A_INV:Total              = A_INV:FreightCharge + A_INV:VAT

             A_INV:VATRate            = INV:VATRate

             A_INV:Printed            = 0
             A_INV:Weight             = 0.0
             A_INV:VolumetricWeight   = 0.0
             A_INV:Volume             = 0.0
             A_INV:Status             = 0

             CLEAR(A_INV:Shipper_Group)
             A_INV:ShipperName        = LEFT(FORMAT(INV:Total,@n12.2))
             CLEAR(A_INV:Consignee_Group)

             ! No Payments, Partially Paid, Credit Note, Fully Paid, Credit Note Shown
             A_INV:Status             = 2                             ! Credit Note................................................

             A_INV:InvoiceMessage     = LO_G:Notes

             IF Access:InvoiceAlias.TryInsert() ~= LEVEL:Benign
                MESSAGE('Failed Credit Note Insert.','Journal',ICON:Hand)
                Access:InvoiceAlias.CancelAutoInc()
             ELSE
                ! Create new invoice
                CLEAR(A_INV:Record)
                IF Access:InvoiceAlias.TryPrimeAutoInc() ~= LEVEL:Benign
                   MESSAGE('Failed.','New Invoice', ICON:Hand)
                ELSE
                   N:CIID                   = A_INV:IID

                   !A_INV:IID
                   LO_G:New_IID             = A_INV:IID

                   A_INV:Record            :=: INV:Record                   ! Same details

                   A_INV:UID                = GLO:UID
                   A_INV:IJID               = IJOU:IJID

                   A_INV:IID                = LO_G:New_IID

                   A_INV:CID                = LO_G:CID

                   ! Client Address
                   !IF A_INV:Terms < 2
                   !   DO Client_Address_From_COD
                   !ELSE
                      DO Client_Address_From_General
                   !.

                   INV:POD_IID              = INV:IID                       ! Set correct POD

                   A_INV:InvoiceDate        = LO_G:InvoiceDate_2            ! Specified
                   A_INV:InvoiceTime        = CLOCK()

                   IF Access:InvoiceAlias.TryInsert() ~= LEVEL:Benign
                      MESSAGE('Failed New Invoice Insert.','Journal',ICON:Hand)
                      Access:InvoiceAlias.CancelAutoInc()

                      ! Remove entry without counter entry
                      CLEAR(A_INV:Record)
                      A_INV:IID             = N:CIID
                      IF Access:InvoiceAlias.TryFetch(A_INV:PKey_IID) = LEVEL:Benign
                         IF Access:InvoiceAlias.DeleteRecord(0) = LEVEL:Benign
                            ! Remove Journal header
                            IF Access:_InvoiceJournals.DeleteRecord(0) ~= LEVEL:Benign
                               Add_Audit(1, '[Create_Journal]', 'Failed on Journal new Invoice.', 'Failed to delete corresponding journal record.')
                               MESSAGE('Failed to remove the Journal record of the failed Journal entry!', 'Journal', ICON:Hand)
                            .
                         ELSE
                            ! (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel)
                            Add_Audit(1, '[Create_Journal]', 'Failed on Journal new Invoice.', 'Failed to delete corresponding credit note.')
                            MESSAGE('Failed to remove the Credit Note part of the failed Journal entry!', 'Journal', ICON:Hand)
                      .  .
                   ELSE
                      ! Invoie Items.....
                      DO Add_Items


                      CASE MESSAGE('Print Credit Note now?', 'Journal', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                      OF BUTTON:Yes
                         ! (p:ID, p:PrintType, p:ID_Options, p:Preview_Option)
                         Print_Cont(N:CIID, 1)
                      .
                      CASE MESSAGE('Print New Invoice now?', 'Journal', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                      OF BUTTON:Yes
                         ! (p:ID, p:PrintType, p:ID_Options, p:Preview_Option)
                         Print_Cont(A_INV:IID, 0)
                      .

                      POST(EVENT:CloseWindow)
    .  .  .  .  .  .
    EXIT
!   Create_Journal_old                                  ROUTINE
!    DATA
!N:CIID      LIKE(INV:IID)
!N:NIID      LIKE(INV:IID)
!
!    CODE
!    ! Create Credit note with From CID then create new invoice in To CID
!
!    ! Journal
!    IF Access:_InvoiceJournals.PrimeRecord() ~= LEVEL:Benign
!       Access:_InvoiceJournals.CancelAutoInc()
!       MESSAGE('Failed to create Journal record.','Journal',ICON:Hand)
!    ELSE
!       IJOU:UID                 = GLO:UID
!       IF Access:_InvoiceJournals.TryInsert() ~= LEVEL:Benign
!       ELSE
!
!
!
!
!    ! Credit Note
!    CLEAR(A_INV:Record)
!    IF Access:InvoiceAlias.TryPrimeAutoInc() ~= LEVEL:Benign
!       MESSAGE('Failed to Create Invoice record (Credit Note).','Journal',ICON:Hand)
!    ELSE
!       !A_INV:IID
!       LO_G:New_IID             = A_INV:IID
!
!       A_INV:Record            :=: INV:Record                   ! Same details
!
!       A_INV:IID                = LO_G:New_IID
!
!       A_INV:CR_IID             = INV:IID                       ! This marks this as a credit note...
!
!       A_INV:InvoiceDate        = LO_G:InvoiceDate
!       A_INV:InvoiceTime        = CLOCK()
!
!       CLEAR(A_INV:Charges_Group)
!
!       A_INV:FreightCharge      = -(INV:Insurance + INV:Documentation + INV:FuelSurcharge + INV:AdditionalCharge + INV:FreightCharge)
!       A_INV:VAT                = -INV:VAT        !(A_INV:FreightCharge * (LO_G:VAT / 100))
!       A_INV:Total              = A_INV:FreightCharge + A_INV:VAT
!
!       A_INV:VATRate            = INV:VATRate
!
!       A_INV:Printed            = 0
!       A_INV:Weight             = 0.0
!       A_INV:VolumetricWeight   = 0.0
!       A_INV:Volume             = 0.0
!       A_INV:Status             = 0
!
!       CLEAR(A_INV:Shipper_Group)
!       A_INV:ShipperName        = LEFT(FORMAT(INV:Total,@n12.2))
!       CLEAR(A_INV:Consignee_Group)
!
!       ! No Payments, Partially Paid, Credit Note, Fully Paid, Credit Note Shown
!       A_INV:Status             = 2                             ! Credit Note................................................
!
!       A_INV:InvoiceMessage     = LO_G:Notes
!
!       IF Access:InvoiceAlias.TryInsert() ~= LEVEL:Benign
!          MESSAGE('Failed Credit Note Insert.','Journal',ICON:Hand)
!          Access:InvoiceAlias.CancelAutoInc()
!       ELSE
!          ! Create new invoice
!          CLEAR(A_INV:Record)
!          IF Access:InvoiceAlias.TryPrimeAutoInc() ~= LEVEL:Benign
!             MESSAGE('Failed.','New Invoice', ICON:Hand)
!          ELSE
!             N:CIID                   = A_INV:IID
!
!             !A_INV:IID
!             LO_G:New_IID             = A_INV:IID
!
!             A_INV:Record            :=: INV:Record                   ! Same details
!
!             A_INV:IID                = LO_G:New_IID
!
!             A_INV:CID                = LO_G:CID
!
!             ! Client Address
!             !IF A_INV:Terms < 2
!             !   DO Client_Address_From_COD
!             !ELSE
!                DO Client_Address_From_General
!             !.
!
!             INV:POD_IID              = INV:IID                       ! Set correct POD
!
!             A_INV:InvoiceDate        = LO_G:InvoiceDate_2            ! Specified
!             A_INV:InvoiceTime        = CLOCK()
!
!             IF Access:InvoiceAlias.TryInsert() ~= LEVEL:Benign
!                MESSAGE('Failed New Invoice Insert.','Journal',ICON:Hand)
!                Access:InvoiceAlias.CancelAutoInc()
!
!                ! Remove entry without counter entry
!                CLEAR(A_INV:Record)
!                A_INV:IID             = N:CIID
!                IF Access:InvoiceAlias.TryFetch(A_INV:PKey_IID) = LEVEL:Benign
!                   IF Access:InvoiceAlias.DeleteRecord(0) ~= LEVEL:Benign
!                      ! (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel)
!                      Add_Audit(1, '[Create_Journal]', 'Failed on Journal new Invoice.', 'Failed to delete corresponding credit note.')
!                      MESSAGE('Failed to remove the Credit Note part of the failed Journal entry!', 'Journal', ICON:Hand)
!                .  .
!             ELSE
!                ! Invoie Items.....
!                DO Add_Items
!
!
!                CASE MESSAGE('Print Credit Note now?', 'Journal', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
!                OF BUTTON:Yes
!                   ! (p:ID, p:PrintType, p:ID_Options, p:Preview_Option)
!                   Print_Cont(N:CIID, 1)
!                .
!                CASE MESSAGE('Print New Invoice now?', 'Journal', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
!                OF BUTTON:Yes
!                   ! (p:ID, p:PrintType, p:ID_Options, p:Preview_Option)
!                   Print_Cont(A_INV:IID, 0)
!                .
!
!                POST(EVENT:CloseWindow)
!    .  .  .  .
!    EXIT
Add_Items                                         ROUTINE
    DATA
R:ITID          LIKE(A_INI:ITID)


    CODE
    CLEAR(INI:Record,-1)
    INI:IID     = INV:IID
    SET(INI:FKey_IID, INI:FKey_IID)
    LOOP
       IF Access:_InvoiceItems.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF INI:IID ~= INV:IID
          BREAK
       .

       IF Access:InvoiceItemsAlias.TryPrimeAutoInc() ~= LEVEL:Benign
          CYCLE
       .

       R:ITID        = A_INI:ITID

       A_INI:Record :=: INI:Record

       A_INI:ITID    = R:ITID
       A_INI:IID     = A_INV:IID

       IF Access:InvoiceItemsAlias.TryInsert() ~= LEVEL:Benign
          Access:InvoiceItemsAlias.CancelAutoInc()
          ! Then what?
    .  .

    EXIT
Get_Invoice                                      ROUTINE
    ! Nothing to do, it's got already
    !LO_G:InvoiceDate_2  = INV:InvoiceDate

    ! Get Client
    IF LO_G:IID ~= 0
       CLEAR(LO_G:Old_ClientName)
       CLEAR(LO_G:Old_ClientNo)

       A_CLI:CID             = INV:CID
       IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) = LEVEL:Benign
          LO_G:Old_ClientName  = A_CLI:ClientName
          LO_G:Old_ClientNo    = A_CLI:ClientNo
       .

       LO_G:Statements      = Check_StatementItems(LO_G:IID)
       IF CLIP(LO_G:Statements) ~= ''
          LO_G:Statement    = 'This Invoice has appeared on Client Statement(s) (Statement no''s. ' & CLIP(LO_G:Statements) & ').'
       ELSE
          LO_G:Statement    = 'Invoice not on any Client Statements.'
       .
    ELSE
       LO_G:Statement       = ''
    .

    DISPLAY
    EXIT
! ------------------------------------------------------------------------------------
Client_Address_From_General           ROUTINE
    CLI:CID                   = A_INV:CID
    IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
       A_INV:ClientNo         = CLI:ClientNo
       A_INV:ClientName       = CLI:ClientName
       A_INV:VATNo            = CLI:VATNo

       A_INV:InvoiceMessage   = CLI:InvoiceMessage

       A_ADD:AID              = CLI:AID

       IF Access:AddressAlias.TryFetch(A_ADD:PKey_AID) = LEVEL:Benign
          A_INV:ClientLine1   = A_ADD:Line1
          A_INV:ClientLine2   = A_ADD:Line2

          SUBU:SUID           = A_ADD:SUID
          IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) = LEVEL:Benign
             A_INV:ClientSuburb     = SUBU:Suburb
             A_INV:ClientPostalCode = SUBU:PostalCode
    .  .  .
    EXIT
!Client_Address_From_COD               ROUTINE
!    DCADD:DC_ID               = A_INV:DC_ID
!    IF Access:Delivery_CODAddresses.TryFetch(DCADD:PKey_DC_ID) = LEVEL:Benign
!       A_INV:ClientName       = DCADD:AddressName
!       A_INV:VATNo            = DCADD:VATNo
!
!       A_INV:ClientLine1      = DCADD:Line1
!       A_INV:ClientLine2      = DCADD:Line2
!       A_INV:ClientSuburb     = DCADD:Line3
!       A_INV:ClientPostalCode = DCADD:Line4
!    ELSE
!       MESSAGE('Delivery COD Address is missing - DC_ID: ' & A_INV:DC_ID, 'Create Journal', ICON:Hand)
!    .
!    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Create_Journal')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt22
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Add_Suburbs.SetOpenRelated()
  Relate:Add_Suburbs.Open                                  ! File Add_Suburbs used by this procedure, so make sure it's RelationManager is open
  Relate:ClientsAlias.Open                                 ! File ClientsAlias used by this procedure, so make sure it's RelationManager is open
  Relate:InvoiceAlias.Open                                 ! File InvoiceAlias used by this procedure, so make sure it's RelationManager is open
  Relate:InvoiceItemsAlias.Open                            ! File InvoiceItemsAlias used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Delivery_CODAddresses.UseFile                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:AddressAlias.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceJournals.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Create_Journal',QuickWindow)               ! Restore window settings from non-volatile store
      LO_G:InvoiceDate    = TODAY()           ! Credit note date
      LO_G:InvoiceDate_2  = TODAY()
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Suburbs.Close
    Relate:ClientsAlias.Close
    Relate:InvoiceAlias.Close
    Relate:InvoiceItemsAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Create_Journal',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Invoice
      Browse_Clients
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Ok
          IF LO_G:IID = 0
             QuickWindow{PROP:AcceptAll}   = FALSE
             MESSAGE('Please select an Invoice.', 'Journal', ICON:Hand)
             SELECT(?LO_G:IID)
             CYCLE
          .
      
      
          QuickWindow{PROP:AcceptAll}   = FALSE
      
          IF CLIP(Check_StatementItems(LO_G:IID)) = ''
             CASE MESSAGE('The selected Invoice has not appeared on a Client Statement.||Would you like to change the Client on the original Invoice?|Select no to create a Journal or Cancel to cancel.', 'Journal', ICON:Question, BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
             OF BUTTON:Yes
                ! Just change the Client on the Invoice
                A_INV:IID         = LO_G:IID
                IF Access:InvoiceAlias.TryFetch(A_INV:PKey_IID) = LEVEL:Benign
                   IF A_INV:CID   = LO_G:CID
                      MESSAGE('The selected Client is the same as the Client already used on the Invoice.', 'Journal', ICON:Hand)
                   ELSE
                      A_INV:CID   = LO_G:CID
                      DO Client_Address_From_General
      
                      IF Access:InvoiceAlias.TryUpdate() ~= LEVEL:Benign
                         MESSAGE('The Invoice could not be updated.', 'Journal', ICON:Hand)
                      ELSE
                         MESSAGE('The Invoice has been updated.', 'Journal', ICON:Hand)
                         POST(EVENT:CloseWindow)
                .  .  .
      
                CYCLE         ! Don't continue with the Create Code, chosen to move Invoice
             OF BUTTON:Cancel
                CYCLE
          .  .
      
      
          IF LO_G:InvoiceDate = 0
             QuickWindow{PROP:AcceptAll}   = FALSE
             MESSAGE('Please specify a new Invoice date.', 'Journal', ICON:Hand)
             SELECT(?LO_G:InvoiceDate)
             CYCLE
          .
      
      
          INV:IID   = LO_G:IID
          IF Access:_Invoice.TryFetch(INV:PKey_IID) = LEVEL:Benign
             DO Create_Journal
          ELSE
             MESSAGE('Invoice could not be loaded!', 'Journal', ICON:Hand)
          .
          CYCLE
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      INV:IID = LO_G:IID
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LO_G:IID = INV:IID
      END
      ThisWindow.Reset(1)
          DO Get_Invoice
      
    OF ?LO_G:IID
          db.debugout('[Create_Journal]   IID before lookup')
      
      IF LO_G:IID OR ?LO_G:IID{PROP:Req}
        INV:IID = LO_G:IID
        IF Access:_Invoice.TryFetch(INV:PKey_IID)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LO_G:IID = INV:IID
          ELSE
            SELECT(?LO_G:IID)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
          db.debugout('[Create_Journal]   IID after lookup')
          DO Get_Invoice
          db.debugout('[Create_Journal]   IID after lookup - after DO Get_Invoice')
      
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',LO_G:InvoiceDate)
      IF Calendar5.Response = RequestCompleted THEN
      LO_G:InvoiceDate=Calendar5.SelectedDate
      DISPLAY(?LO_G:InvoiceDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar8.SelectOnClose = True
      Calendar8.Ask('Select a Date',LO_G:InvoiceDate_2)
      IF Calendar8.Response = RequestCompleted THEN
      LO_G:InvoiceDate_2=Calendar8.SelectedDate
      DISPLAY(?LO_G:InvoiceDate_2)
      END
      ThisWindow.Reset(True)
    OF ?CallLookup:2
      ThisWindow.Update()
      CLI:ClientName = LO_G:ClientName
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LO_G:ClientName = CLI:ClientName
        LO_G:CID = CLI:CID
        LO_G:ClientNo = CLI:ClientNo
      END
      ThisWindow.Reset(1)
    OF ?LO_G:ClientName
      IF LO_G:ClientName OR ?LO_G:ClientName{PROP:Req}
        CLI:ClientName = LO_G:ClientName
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            LO_G:ClientName = CLI:ClientName
            LO_G:CID = CLI:CID
            LO_G:ClientNo = CLI:ClientNo
          ELSE
            CLEAR(LO_G:CID)
            CLEAR(LO_G:ClientNo)
            SELECT(?LO_G:ClientName)
            CYCLE
          END
        ELSE
          LO_G:CID = CLI:CID
          LO_G:ClientNo = CLI:ClientNo
        END
      END
      ThisWindow.Reset()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?LO_G:Statement, Resize:FixLeft+Resize:FixBottom, Resize:ConstantRight+Resize:LockHeight) ! Override strategy for ?LO_G:Statement

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Clients_Rates_Mod    PROCEDURE                             ! Declare Procedure

  CODE
    Select_RateMod_Clients()
    RETURN
!!! <summary>
!!! Generated from procedure template - Window
!!! (also Bad Debt)
!!! </summary>
Create_Credit_Note PROCEDURE 

LO_G:Locals          GROUP,PRE(LO_G)                       ! 
IID                  ULONG                                 ! Invoice Number
InvoiceDate          DATE                                  ! Invoice Date
FreightCharge        DECIMAL(11,2)                         ! 
Notes                STRING(255)                           ! 
VAT                  DECIMAL(10,2)                         ! VAT
VATRate              DECIMAL(5,2)                          ! VAT Rate
Total                DECIMAL(11,2)                         ! 
New_IID              ULONG                                 ! Invoice Number
Statement            STRING(150)                           ! 
Statements           STRING(100)                           ! 
Bad_Debt             BYTE                                  ! This is a bad debt
BID                  ULONG                                 ! Branch ID
BranchName           STRING(35)                            ! Branch Name
Another_CreditNote   BYTE                                  ! Does the user want to do another one afterwards
SubTotal             DECIMAL(10,2)                         ! 
Calc_Backwards       BYTE                                  ! Calculate backwards from VAT incl. amount
                     END                                   ! 
QuickWindow          WINDOW('Create a Credit Note'),AT(,,457,332),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,IMM,MDI,HLP('Create_Credit_Note'),SYSTEM
                       SHEET,AT(4,4,449,310),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           PROMPT('Invoice Details'),AT(94,142,122,10),USE(?Prompt22),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           PROMPT('Client Name:'),AT(10,154),USE(?CLI:ClientName:Prompt),TRN
                           PROMPT('Client No.:'),AT(346,154),USE(?CLI:ClientNo:Prompt),TRN
                           PROMPT('DI No.:'),AT(10,168),USE(?INV:DINo:Prompt),TRN
                           PROMPT('Client Reference:'),AT(323,168),USE(?INV:ClientReference:Prompt),TRN
                           PROMPT('MIDs:'),AT(10,184),USE(?INV:MIDs:Prompt),TRN
                           PROMPT('Shipper Name:'),AT(10,200),USE(?INV:ShipperName:Prompt),TRN
                           PROMPT('Consignee Name:'),AT(254,200),USE(?INV:ConsigneeName:Prompt),TRN
                           PROMPT('Invoice Date:'),AT(10,216),USE(?INV:InvoiceDate:Prompt),TRN
                           PROMPT('Weight:'),AT(355,218),USE(?INV:Weight:Prompt),TRN
                           PROMPT('Charges'),AT(94,232,122,10),USE(?Prompt21),FONT(,,,FONT:bold+FONT:underline,CHARSET:ANSI), |
  TRN
                           PROMPT('Insurance:'),AT(10,244),USE(?INV:Insurance:Prompt),TRN
                           PROMPT('Documentation:'),AT(10,258),USE(?INV:Documentation:Prompt),TRN
                           PROMPT('Fuel Surcharge:'),AT(10,272),USE(?INV:FuelSurcharge:Prompt),TRN
                           PROMPT('Additional Charge:'),AT(10,286),USE(?INV:AdditionalCharge:Prompt),TRN
                           PROMPT('VAT:'),AT(365,286),USE(?INV:VAT:Prompt),TRN
                           PROMPT('Freight Charge:'),AT(10,300),USE(?INV:FreightCharge:Prompt),TRN
                           PROMPT('Total:'),AT(362,300),USE(?INV:Total:Prompt),TRN
                           GROUP,AT(10,142,438,168),USE(?Group_Invoice),COLOR(00E9E9E9h),SKIP
                             ENTRY(@s35),AT(94,154,224,10),USE(CLI:ClientName),READONLY,SKIP,TIP('Client name')
                             ENTRY(@n_10b),AT(385,154,60,10),USE(CLI:ClientNo),RIGHT(1),MSG('Client No.'),READONLY,SKIP, |
  TIP('Client No.')
                             ENTRY(@n_10),AT(94,168,60,10),USE(INV:DINo),RIGHT(1),MSG('Delivery Instruction Number'),READONLY, |
  SKIP,TIP('Delivery Instruction Number')
                             ENTRY(@s30),AT(385,168,60,10),USE(INV:ClientReference),MSG('Client Reference'),READONLY,SKIP, |
  TIP('Client Reference')
                             ENTRY(@s20),AT(94,184,131,10),USE(INV:MIDs),MSG('List of Manifest IDs that the delivery' & |
  ' is currently manifested on'),READONLY,SKIP,TIP('List of Manifest IDs that the deliv' & |
  'ery is currently manifested on')
                             ENTRY(@s35),AT(94,200,131,10),USE(INV:ShipperName),MSG('Name of this address'),READONLY,SKIP, |
  TIP('Name of this address')
                             ENTRY(@s35),AT(313,200,131,10),USE(INV:ConsigneeName),MSG('Name of this address'),READONLY, |
  SKIP,TIP('Name of this address')
                             ENTRY(@d6),AT(94,216,60,10),USE(INV:InvoiceDate),RIGHT(1),MSG('Invoice Date'),READONLY,SKIP, |
  TIP('Invoice Date')
                             ENTRY(@n-14.2),AT(385,218,60,10),USE(INV:Weight),RIGHT(1),MSG('In kg''s'),READONLY,SKIP,TIP('In kg''s')
                             ENTRY(@n-14.2),AT(94,244,60,10),USE(INV:Insurance),RIGHT(1),READONLY,SKIP
                             ENTRY(@n-14.2),AT(94,258,60,10),USE(INV:Documentation),RIGHT(1),READONLY,SKIP
                             ENTRY(@n-14.2),AT(94,272,60,10),USE(INV:FuelSurcharge),RIGHT(1),READONLY,SKIP
                             PROMPT('Sub Total:'),AT(347,272),USE(?SubTotal:Prompt)
                             ENTRY(@n-14.2),AT(385,272,60,10),USE(LO_G:SubTotal),RIGHT(1),MSG(' '),READONLY,SKIP,TIP('Sub Total')
                             ENTRY(@n-15.2),AT(94,286,60,10),USE(INV:AdditionalCharge),DECIMAL(12),MSG('Addi'),READONLY, |
  SKIP,TIP('Addi')
                             ENTRY(@n-14.2),AT(385,286,60,10),USE(INV:VAT),RIGHT(1),MSG('VAT'),READONLY,SKIP,TIP('VAT')
                             ENTRY(@n-15.2),AT(94,300,60,10),USE(INV:FreightCharge),RIGHT(1),READONLY,SKIP
                             ENTRY(@n-15.2),AT(385,300,60,10),USE(INV:Total),RIGHT(1),READONLY,SKIP
                             PROMPT('Toll Charge:'),AT(341,244),USE(?INV:TollCharge:Prompt)
                             ENTRY(@n-15.2),AT(385,244,60,10),USE(INV:TollCharge),RIGHT(1),READONLY,SKIP
                           END
                           PROMPT('Invoice Number:'),AT(10,22),USE(?LO_G:IID:Prompt),TRN
                           BUTTON('...'),AT(78,22,12,10),USE(?CallLookup)
                           ENTRY(@n_10b),AT(94,22,60,10),USE(LO_G:IID),RIGHT(1),MSG('Invoice ID'),REQ,TIP('Invoice ID')
                           PROMPT('Credit Note Date:'),AT(10,38),USE(?LO_G:InvoiceDate:Prompt),TRN
                           BUTTON('...'),AT(78,38,12,10),USE(?Calendar)
                           SPIN(@d6),AT(94,38,60,10),USE(LO_G:InvoiceDate),RIGHT(1),MSG('Invoice Date'),TIP('Invoice Date')
                           PROMPT('VAT Rate:'),AT(349,22),USE(?VATRate:Prompt),TRN
                           ENTRY(@n7.2),AT(385,22,60,10),USE(LO_G:VATRate),DECIMAL(12),MSG('VAT Rate'),SKIP,TIP('VAT Rate')
                           PROMPT('Credit Amount:'),AT(10,54),USE(?FreightCharge:Prompt),TRN
                           ENTRY(@n-15.2),AT(94,54,60,10),USE(LO_G:FreightCharge),RIGHT(1)
                           CHECK(' &Calc. Backwards'),AT(277,54),USE(LO_G:Calc_Backwards),MSG('Calculate backwards' & |
  ' from VAT incl. amount'),TIP('Calculate backwards from VAT incl. amount'),TRN
                           PROMPT('Total:'),AT(362,54),USE(?Total:Prompt),TRN
                           ENTRY(@n-15.2),AT(385,54,60,10),USE(LO_G:Total),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('VAT:'),AT(365,38),USE(?VAT:Prompt),TRN
                           ENTRY(@n-14.2),AT(385,38,60,10),USE(LO_G:VAT),RIGHT(1),COLOR(00E9E9E9h),MSG('VAT'),READONLY, |
  SKIP,TIP('VAT')
                           PROMPT('Notes:'),AT(10,70),USE(?LO_G:Notes:Prompt),TRN
                           TEXT,AT(94,70,354,40),USE(LO_G:Notes),VSCROLL,BOXED
                           PROMPT('Branch Name:'),AT(10,118),USE(?INV:BranchName:Prompt),TRN
                           BUTTON('...'),AT(78,118,12,10),USE(?CallLookup:2)
                           ENTRY(@s35),AT(94,118,131,10),USE(LO_G:BranchName),MSG('Branch Name'),REQ,TIP('Branch Name')
                           CHECK(' &Bad Debt'),AT(397,118),USE(LO_G:Bad_Debt),LEFT,MSG('This is a bad debt'),TIP('This is a bad debt'), |
  TRN
                           LINE,AT(9,135,440,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                         END
                       END
                       STRING(@s150),AT(4,320,341,10),USE(LO_G:Statement)
                       BUTTON('&OK'),AT(352,316,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(404,316,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(265,316,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar5            CalendarClass
    MAP
Apply_User_Permissions    PROCEDURE(),LONG
    .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Get_Invoice                   ROUTINE
    IF CLIP(LO_G:BranchName) = ''
       LO_G:BranchName      = INV:BranchName
       LO_G:BID             = INV:BID
    .


    ! Nothing to do, it's got already

    ! Get Client
    IF LO_G:IID ~= 0
       CLI:CID              = INV:CID
       IF Access:Clients.TryFetch(CLI:PKey_CID) ~= LEVEL:Benign
          MESSAGE('Client not found.', 'Credit Note', ICON:Exclamation)
          CLEAR(CLI:Record)
       .

       LO_G:Statements      = Check_StatementItems(LO_G:IID)
       IF CLIP(LO_G:Statements) ~= ''
          LO_G:Statement    = 'This Invoice has appeared on Client Statement(s) (Statement no''s. ' & CLIP(LO_G:Statements) & ').'
       ELSE
          LO_G:Statement    = 'Invoice not on any Client Statements.'
       .


       ! Check invoice status
       ! (ULONG, BYTE=0),BYTE,PROC
       ! (p:IID, p:Option)
       !   p:Option
       !       - 1 is for Client Statement runs
       !       - x is other
       CASE Upd_Invoice_Paid_Status(LO_G:IID)
       OF 4 OROF 5 OROF 8
          ! No Payments|Partially Paid|Credit Note|Fully Paid - Credit|Fully Paid|Credit Note Shown|Bad Debt|Over Paid
          !        1           2           3                   4           5               6           7           8

          ! Check security, if too low then deny, if high warn?
          IF Apply_User_Permissions() = 0
             MESSAGE('This invoice (' & LO_G:IID & ') has been fully paid.||You do not have permission to do this.', 'Credit Notes', ICON:Hand)
             CLEAR(LO_G:IID)
             SELECT(?LO_G:IID)
          ELSE
             MESSAGE('This invoice (' & LO_G:IID & ') has been fully paid.||You have special permission to credit it, but do you want to?', 'Credit Notes', ICON:Hand)
       .  .
    ELSE
       LO_G:Statement       = ''
    .

    LO_G:SubTotal           = INV:Insurance + INV:Documentation + INV:FuelSurcharge + INV:AdditionalCharge + |
                                INV:FreightCharge + INV:TollCharge

    DISPLAY
    EXIT
Credit_Amount               ROUTINE
    ! Check that it is not greater than the Invoice amount
    LO_G:VAT    = (LO_G:VATRate / 100) * LO_G:FreightCharge
    LO_G:Total  = LO_G:VAT + LO_G:FreightCharge

    DISPLAY
    EXIT                                                                                                           
Create_Credit_Note       ROUTINE
    ! Create note

    CLEAR(A_INV:Record)    
    IF Access:InvoiceAlias.TryPrimeAutoInc() ~= LEVEL:Benign
       MESSAGE('Failed.','Credit Note',ICON:Hand)
    ELSE
       !A_INV:IID
       LO_G:New_IID             = A_INV:IID

        
       A_INV:Record            :=: INV:Record                  ! Same details        

       A_INV:BID                = LO_G:BID
       A_INV:BranchName         = LO_G:BranchName


       A_INV:IID                = LO_G:New_IID

       A_INV:POD_IID            = 0
       A_INV:CR_IID             = INV:IID                       ! This marks this as a credit note...

       A_INV:InvoiceDate        = LO_G:InvoiceDate
       A_INV:InvoiceTime        = CLOCK()

       CLEAR(A_INV:Charges_Group)

       A_INV:FreightCharge      = -LO_G:FreightCharge

       A_INV:VAT                = (A_INV:FreightCharge * (LO_G:VATRate / 100))      ! Minus in amount
       A_INV:Total              = A_INV:FreightCharge + A_INV:VAT

       A_INV:VATRate            = LO_G:VATRate                  ! INV:VATRate
       A_INV:UID                = GLO:UID

       A_INV:Printed            = 0
       A_INV:Weight             = 0.0
       A_INV:VolumetricWeight   = 0.0
       A_INV:Volume             = 0.0
       A_INV:Status             = 0
       A_INV:StatusUpToDate     = 0
       A_INV:BadDebt            = 0
       A_INV:MID                = 0
       A_INV:MIDs               = ''
       A_INV:ICID               = 0

       A_INV:FuelSurcharge      = 0                            ! Added 13/02/14 
       A_INV:TollCharge         = 0
       A_INV:AdditionalCharge   = 0
         
       CLEAR(A_INV:Shipper_Group)
       A_INV:ShipperLine1       = 'Invoice Total: ' & LEFT(FORMAT(INV:Total,@n12.2))      ! Sticking the old total in here
       CLEAR(A_INV:Consignee_Group)

       ! No Payments|Partially Paid|Credit Note|Fully Paid - Credit|Fully Paid|Credit Note (shown)|Bad Debt (shown)
       !      0           1                2              3             4               5                 6
       IF LO_G:Bad_Debt = TRUE
          A_INV:BadDebt         = TRUE
       .

       A_INV:Status             = 2                             ! Credit Note / Bad Debt ..................................


       A_INV:InvoiceMessage     = LO_G:Notes
       

       IF Access:InvoiceAlias.TryInsert() ~= LEVEL:Benign
          MESSAGE('Failed Insert.','Credit Note',ICON:Hand)
          Access:InvoiceAlias.CancelAutoInc()
       ELSE
          ! (ULONG, BYTE=0, BYTE=0),BYTE,PROC
          ! (p:IID, p:Option, p:CreditChanged)
          Upd_Invoice_Paid_Status(A_INV:CR_IID, 0, 1)

          CASE MESSAGE('Print Credit Note now?', 'Credit Note', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
          OF BUTTON:Yes
             ! (p:ID, p:PrintType, p:ID_Options, p:Preview_Option)
             Print_Cont(A_INV:IID, 1)
          .

          CASE MESSAGE('Would you like to create another Credit Note now?', 'Credit Note', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
          OF BUTTON:Yes
             LO_G:Another_CreditNote    = TRUE
          .

          POST(EVENT:CloseWindow)
    .  .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Create_Credit_Note')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt22
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:InvoiceAlias.Open                                 ! File InvoiceAlias used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Create_Credit_Note',QuickWindow)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  IF ?LO_G:Calc_Backwards{Prop:Checked}
    ENABLE(?LO_G:Total)
  END
  IF NOT ?LO_G:Calc_Backwards{PROP:Checked}
    DISABLE(?LO_G:Total)
  END
      LO_G:VATRate        = Get_Setup_Info(4)
  
      LO_G:InvoiceDate    = TODAY()
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:InvoiceAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Create_Credit_Note',QuickWindow)        ! Save window data to non-volatile store
  END
      IF LO_G:Another_CreditNote = TRUE
         START(Create_Credit_Note)
      .
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Invoice
      Browse_Branches
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Ok
          IF LO_G:IID = 0
             QuickWindow{PROP:AcceptAll}   = FALSE
             MESSAGE('Please select an Invoice.', 'Credit Note', ICON:Hand)
             SELECT(?LO_G:IID)
             CYCLE
          .
          DO Get_Invoice
      
          IF LO_G:InvoiceDate = 0
             QuickWindow{PROP:AcceptAll}   = FALSE
             MESSAGE('Please specify a date.', 'Credit Note', ICON:Hand)
             SELECT(?LO_G:InvoiceDate)
             CYCLE
          .
      
          IF LO_G:Total = 0.0
             QuickWindow{PROP:AcceptAll}   = FALSE
             MESSAGE('The total credit cannot be zero.', 'Credit Note', ICON:Hand)
             SELECT(?LO_G:FreightCharge)
             CYCLE
          .
          IF LO_G:Total > INV:Total
             QuickWindow{PROP:AcceptAll}   = FALSE
             MESSAGE('The total credit would be greater than the invocie Total.', 'Credit Note', ICON:Hand)
             SELECT(?LO_G:FreightCharge)
             CYCLE
          .
      
          IF LO_G:Bad_Debt = TRUE
             CASE MESSAGE('This Credit is marked as a Bad Debt.||Are you sure that this is correct?', 'Credit Note - Bad Debt', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:No
                QuickWindow{PROP:AcceptAll}   = FALSE
                SELECT(?LO_G:FreightCharge)
                CYCLE
          .  .
      
      
          ! Check if there has been a Client statement generated for this Invoice yet...  - journals
          !IF Check_StatementItems(LO_G:IID) <= 0 AND INV:Total = LO_G:Total
      
      
          IF LO_G:BID = 0 OR CLIP(LO_G:BranchName) = ''
             MESSAGE('Please specify a Branch.', 'Credit Note - Branch', ICON:Exclamation)
             QuickWindow{PROP:AcceptAll}   = FALSE
             SELECT(?LO_G:BranchName)
             CYCLE
          .
      
      
          QuickWindow{PROP:AcceptAll}   = FALSE
          DO Create_Credit_Note
          CYCLE
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      INV:IID = LO_G:IID
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LO_G:IID = INV:IID
      END
      ThisWindow.Reset(1)
          DO Get_Invoice
      
    OF ?LO_G:IID
      IF LO_G:IID OR ?LO_G:IID{PROP:Req}
        INV:IID = LO_G:IID
        IF Access:_Invoice.TryFetch(INV:PKey_IID)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LO_G:IID = INV:IID
          ELSE
            SELECT(?LO_G:IID)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
          DO Get_Invoice
      
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',LO_G:InvoiceDate)
      IF Calendar5.Response = RequestCompleted THEN
      LO_G:InvoiceDate=Calendar5.SelectedDate
      DISPLAY(?LO_G:InvoiceDate)
      END
      ThisWindow.Reset(True)
    OF ?LO_G:FreightCharge
          DO Credit_Amount
    OF ?LO_G:Calc_Backwards
      IF ?LO_G:Calc_Backwards{PROP:Checked}
        ENABLE(?LO_G:Total)
      END
      IF NOT ?LO_G:Calc_Backwards{PROP:Checked}
        DISABLE(?LO_G:Total)
      END
      ThisWindow.Reset()
          IF LO_G:Calc_Backwards = TRUE
             ?LO_G:Total{PROP:Background} = -1
             ?LO_G:Total{PROP:ReadOnly}   = FALSE
             ?LO_G:Total{PROP:Skip}       = FALSE
          ELSE
             ?LO_G:Total{PROP:Background} = 0E9E9E9H
             ?LO_G:Total{PROP:ReadOnly}   = TRUE
             ?LO_G:Total{PROP:Skip}       = TRUE
          .
          
    OF ?LO_G:Total
          IF LO_G:Calc_Backwards = TRUE
             ! t = f * (1 + v% / 100)
             ! f = t / (1 + v% / 100)
             ! f = 114 / 1.14 = 100
             ! v = f * v%
      
             LO_G:FreightCharge   = LO_G:Total / (1 + LO_G:VATRate / 100)
             LO_G:VAT             = LO_G:Total - LO_G:FreightCharge
             DISPLAY
          .
    OF ?CallLookup:2
      ThisWindow.Update()
      BRA:BranchName = LO_G:BranchName
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LO_G:BranchName = BRA:BranchName
        LO_G:BID = BRA:BID
      END
      ThisWindow.Reset(1)
    OF ?LO_G:BranchName
      IF NOT QuickWindow{PROP:AcceptAll}
        IF LO_G:BranchName OR ?LO_G:BranchName{PROP:Req}
          BRA:BranchName = LO_G:BranchName
          IF Access:Branches.TryFetch(BRA:Key_BranchName)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              LO_G:BranchName = BRA:BranchName
              LO_G:BID = BRA:BID
            ELSE
              CLEAR(LO_G:BID)
              SELECT(?LO_G:BranchName)
              CYCLE
            END
          ELSE
            LO_G:BID = BRA:BID
          END
        END
      END
      ThisWindow.Reset()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Apply_User_Permissions    PROCEDURE()         ! Special user permissions, Can credit Fully Paid

L:Result        LONG(0)

    CODE
    ! (ULONG, STRING, STRING, <STRING>, BYTE=0, <*BYTE>),LONG
    ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned)

    ! Returns
    !   0   - Allow (default)
    !   1   - Disable
    !   2   - Hide
    !   100 - Allow                 - Default action
    !   101 - Disallow              - Default action

    CASE Get_User_Access(GLO:UID, 'Credit Notes', 'Create_Credit_Note', 'Credit_Full_Paid', 2)
    OF 0 OROF 100
       ! This user is allowed to credit Full Paid invoices..
       L:Result     = 1
    OF 2
    ELSE
    .

    RETURN( L:Result )

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?LO_G:Statement, Resize:FixLeft+Resize:FixBottom, Resize:ConstantRight+Resize:LockHeight) ! Override strategy for ?LO_G:Statement

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Transporter_Invoices PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Options_Group    GROUP,PRE(L_OG)                       ! 
FromDate             DATE                                  ! 
ToDate               DATE                                  ! 
VCID                 ULONG                                 ! Vehicle Composition ID
CompositionName      STRING(35)                            ! 
TID                  ULONG                                 ! Transporter ID
TransporterName      STRING(35)                            ! Transporters Name
ExcludeExtraLeg      BYTE                                  ! Exclude Invoices for Extra Legs
ExcludeExtraInv      BYTE                                  ! Exclude Extra Invoices
                     END                                   ! 
LO:Report_Vars       GROUP,PRE()                           ! 
R:Total              DECIMAL(11,2)                         ! 
                     END                                   ! 
Process:View         VIEW(_InvoiceTransporter)
                       PROJECT(INT:Cost)
                       PROJECT(INT:DID)
                       PROJECT(INT:InvoiceDate)
                       PROJECT(INT:MID)
                       PROJECT(INT:TIN)
                       PROJECT(INT:VAT)
                       JOIN(MAN:PKey_MID,INT:MID)
                         PROJECT(MAN:VCID)
                       END
                     END
ProgressWindow       WINDOW('Report Invoice Transporter'),AT(,,237,153),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,MDI,TIMER(1)
                       SHEET,AT(3,4,231,129),USE(?Sheet1)
                         TAB('Options'),USE(?Tab1)
                           PROMPT('Composition Name:'),AT(10,26),USE(?CompositionName:Prompt),TRN
                           BUTTON('...'),AT(75,26,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(91,26,137,10),USE(L_OG:CompositionName),REQ
                           PROMPT('Transporter Name:'),AT(10,42),USE(?TransporterName:Prompt),TRN
                           ENTRY(@s35),AT(91,42,137,10),USE(L_OG:TransporterName),COLOR(00E9E9E9h),MSG('Transporters Name'), |
  READONLY,SKIP,TIP('Transporters Name')
                           CHECK(' &Exclude Extra Leg Invoices'),AT(91,57),USE(L_OG:ExcludeExtraLeg),MSG('Exclude In' & |
  'voices for Extra Legs'),TIP('Exclude Invoices for Extra Legs'),TRN
                           CHECK(' Exclude Extra &Invoices'),AT(91,72),USE(L_OG:ExcludeExtraInv),MSG('Exclude Extr' & |
  'a Invoices'),TIP('Exclude Extra Invoices'),TRN
                           BUTTON('...'),AT(75,92,12,10),USE(?Calendar)
                           PROMPT('From Date:'),AT(10,92),USE(?L_OG:FromDate:Prompt),TRN
                           SPIN(@d6),AT(91,92,60,10),USE(L_OG:FromDate),RIGHT(1)
                           BUTTON('...'),AT(75,108,12,10),USE(?Calendar:2)
                           PROMPT('To Date:'),AT(10,108),USE(?ToDate:Prompt),TRN
                           SPIN(@d6),AT(91,108,60,10),USE(L_OG:ToDate),RIGHT(1)
                         END
                         TAB('Progress'),USE(?Tab2)
                           PROGRESS,AT(63,50,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(47,38,141,10),USE(?Progress:UserString),CENTER,TRN
                           STRING(''),AT(47,64,141,10),USE(?Progress:PctText),CENTER,TRN
                         END
                       END
                       BUTTON('Pause'),AT(122,136,,15),USE(?Pause),LEFT,ICON('Blue_R.ico'),FLAT
                       BUTTON('Cancel'),AT(186,136,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('_InvoiceTransporter Report'),AT(250,1125,7750,10063),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,875),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Transporter Invoices'),AT(0,20,7750,220),USE(?ReportTitle),FONT('MS Sans Serif',12, |
  ,FONT:bold),CENTER
                         STRING('Vehicle Composition:'),AT(4010,385),USE(?String25:3),TRN
                         STRING('To:'),AT(1583,385),USE(?String25:2),TRN
                         STRING(@d6b),AT(500,385),USE(L_OG:FromDate),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s35),AT(5156,385),USE(L_OG:CompositionName),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('From:'),AT(135,385),USE(?String25),TRN
                         STRING(@d6b),AT(1844,385),USE(L_OG:ToDate),FONT(,,,FONT:bold,CHARSET:ANSI)
                         BOX,AT(0,625,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(1292,625,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2240,625,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(3229,625,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(6615,625,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(5625,625,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         STRING('Total'),AT(6750,656,802,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('TIN  (Invoice No.)'),AT(52,656,1198,156),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('MID'),AT(2323,656,833,156),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Cost'),AT(4719,656,802,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('VAT'),AT(5771,656,802,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Invoice Date'),AT(1354,656,833,156),USE(?HeaderTitle:5),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(1291,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(2240,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(3229,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(5625,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(6615,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         STRING(@n_10),AT(52,52,885,167),USE(INT:TIN),RIGHT(1)
                         STRING(@n_10),AT(2323,42,729,167),USE(INT:MID),RIGHT(1)
                         STRING(@n-14.2),AT(4719,42,800,167),USE(INT:Cost),RIGHT
                         STRING(@n-14.2),AT(5771,42,802,167),USE(INT:VAT),RIGHT
                         STRING(@n-15.2),AT(6750,42,802,167),USE(R:Total),RIGHT(1)
                         STRING(@d5b),AT(1365,42,635,167),USE(INT:InvoiceDate),RIGHT(1)
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
detail_totals          DETAIL,AT(,,,417),USE(?detail_totals)
                         LINE,AT(3958,73,3656,0),USE(?Line14:3),COLOR(COLOR:Black)
                         STRING(@n-14.2),AT(4719,146,800,167),USE(INT:Cost,,?INT:Cost:2),RIGHT,SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(5771,146,802,167),USE(INT:VAT,,?INT:VAT:2),RIGHT,SUM,TALLY(Detail),TRN
                         STRING(@n-15.2),AT(6750,146,802,167),USE(R:Total,,?R:Total:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING('Totals:'),AT(4010,146),USE(?String25:4),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         LINE,AT(3958,354,3656,0),USE(?Line14:2),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Transporter Invoice No.' & |
      '|' & 'By Invoice Date' & |
      '|' & 'By Manifest' & |
      '|' & 'By Transporter' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
        PRINT(RPT:detail_totals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Transporter_Invoices')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CompositionName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:FromDate',L_OG:FromDate)                      ! Added by: Report
  BIND('L_OG:ToDate',L_OG:ToDate)                          ! Added by: Report
  BIND('L_OG:VCID',L_OG:VCID)                              ! Added by: Report
  BIND('L_OG:ExcludeExtraLeg',L_OG:ExcludeExtraLeg)        ! Added by: Report
  BIND('L_OG:ExcludeExtraInv',L_OG:ExcludeExtraInv)        ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                                  ! File Transporter used by this procedure, so make sure it's RelationManager is open
  Access:VehicleComposition.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Transporter_Invoices',ProgressWindow) ! Restore window settings from non-volatile store
      L_OG:TID                = GETINI('Print_Transporter_Invoices', 'TID', , GLO:Local_INI)
  
      !L_OG:VCID               = GETINI('Print_Transporter_Invoices', 'VCID', L_OG:VCID, GLO:Local_INI)
  
      L_OG:FromDate           = GETINI('Print_Transporter_Invoices', 'FromDate', , GLO:Local_INI)
      L_OG:ToDate             = GETINI('Print_Transporter_Invoices', 'ToDate', , GLO:Local_INI)
  
      L_OG:ExcludeExtraLeg    = GETINI('Print_Transporter_Invoices', 'ExcludeExtraLeg', , GLO:Local_INI)
      L_OG:ExcludeExtraInv    = GETINI('Print_Transporter_Invoices', 'ExcludeExtraInv', , GLO:Local_INI)
  
  
  
      CLEAR(L_OG:TransporterName)
      TRA:TID = L_OG:TID
      IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
         L_OG:TransporterName = TRA:TransporterName
      .
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:_InvoiceTransporter, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter Invoice No.')) THEN
     ThisReport.AppendOrder('+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice Date')) THEN
     ThisReport.AppendOrder('+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Manifest')) THEN
     ThisReport.AppendOrder('+INT:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter')) THEN
     ThisReport.AppendOrder('+INT:TID')
  END
  ThisReport.SetFilter('(0 = L_OG:FromDate OR (INT:InvoiceDate >= L_OG:FromDate)) AND (0 = L_OG:ToDate OR (INT:InvoiceDate << L_OG:ToDate + 1)) AND (L_OG:VCID = 0 OR L_OG:VCID = MAN:VCID) AND (L_OG:ExcludeExtraLeg = 0 OR INT:DID = 0) AND (L_OG:ExcludeExtraInv = 0 OR INT:ExtraInv = 0)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_InvoiceTransporter.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Transporter.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Transporter_Invoices',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_VehicleComposition
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
          IF L_OG:FromDate = 0 OR L_OG:ToDate = 0
             MESSAGE('A From and To date is required to run the report.', 'Date Validation', ICON:Exclamation)
             SELECT(?L_OG:FromDate)
             CYCLE
          .
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      VCO:CompositionName = L_OG:CompositionName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_OG:CompositionName = VCO:CompositionName
        L_OG:VCID = VCO:VCID
        L_OG:TID = VCO:TID
      END
      ThisWindow.Reset(1)
          CLEAR(L_OG:TransporterName)
      
          TRA:TID = L_OG:TID
          IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
             L_OG:TransporterName = TRA:TransporterName
          .
      
          DISPLAY
    OF ?L_OG:CompositionName
      IF L_OG:CompositionName OR ?L_OG:CompositionName{PROP:Req}
        VCO:CompositionName = L_OG:CompositionName
        IF Access:VehicleComposition.TryFetch(VCO:Key_Name)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_OG:CompositionName = VCO:CompositionName
            L_OG:VCID = VCO:VCID
            L_OG:TID = VCO:TID
          ELSE
            CLEAR(L_OG:VCID)
            CLEAR(L_OG:TID)
            SELECT(?L_OG:CompositionName)
            CYCLE
          END
        ELSE
          L_OG:VCID = VCO:VCID
          L_OG:TID = VCO:TID
        END
      END
      ThisWindow.Reset()
          CLEAR(L_OG:TransporterName)
      
          TRA:TID = L_OG:TID
          IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
             L_OG:TransporterName = TRA:TransporterName
          .
      
          DISPLAY
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',L_OG:FromDate)
      IF Calendar5.Response = RequestCompleted THEN
      L_OG:FromDate=Calendar5.SelectedDate
      DISPLAY(?L_OG:FromDate)
      END
      ThisWindow.Reset(True)
    OF ?L_OG:FromDate
          IF L_OG:ToDate = 0 OR L_OG:ToDate < L_OG:FromDate
             L_OG:ToDate  = L_OG:FromDate + 6
          .
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',L_OG:ToDate)
      IF Calendar6.Response = RequestCompleted THEN
      L_OG:ToDate=Calendar6.SelectedDate
      DISPLAY(?L_OG:ToDate)
      END
      ThisWindow.Reset(True)
    OF ?Pause
      ThisWindow.Update()
          PUTINI('Print_Transporter_Invoices', 'TID', L_OG:TID, GLO:Local_INI)
          PUTINI('Print_Transporter_Invoices', 'VCID', L_OG:VCID, GLO:Local_INI)
          PUTINI('Print_Transporter_Invoices', 'FromDate', L_OG:FromDate, GLO:Local_INI)
          PUTINI('Print_Transporter_Invoices', 'ToDate', L_OG:ToDate, GLO:Local_INI)
          PUTINI('Print_Transporter_Invoices', 'ExcludeExtraLeg', L_OG:ExcludeExtraLeg, GLO:Local_INI)
          PUTINI('Print_Transporter_Invoices', 'ExcludeExtraInv', L_OG:ExcludeExtraInv, GLO:Local_INI)
          SELECT(?Tab2)
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_OG:FromDate
          IF L_OG:ToDate = 0 OR L_OG:ToDate < L_OG:FromDate
             L_OG:ToDate  = L_OG:FromDate + 6
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      R:Total     = INT:Cost + INT:VAT
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:detail_totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','MANTRNIS','Print_Transporter_Invoices','Print_Transporter_Invoices','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Reciepts PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Options          GROUP,PRE(L_OG)                       ! 
Date_Option          BYTE                                  ! 
From_Date            DATE                                  ! From Date
To_Date              DATE                                  ! To Date
Option               BYTE                                  ! All Clients or a single Client
CID                  ULONG                                 ! Client ID
CLientName           STRING(35)                            ! 
ClientNo             ULONG                                 ! Client No.
Owing                DECIMAL(11,2)                         ! 
AllocationDetails    BYTE(1)                               ! Print allocation details
                     END                                   ! 
Process:View         VIEW(ClientsPaymentsAllocation)
                       PROJECT(CLIPA:AllocationDate)
                       PROJECT(CLIPA:AllocationNo)
                       PROJECT(CLIPA:Amount)
                       PROJECT(CLIPA:CPID)
                       PROJECT(CLIPA:IID)
                       JOIN(INV:PKey_IID,CLIPA:IID)
                       END
                       JOIN(CLIP:PKey_CPID,CLIPA:CPID)
                         PROJECT(CLIP:DateMade)
                         PROJECT(CLIP:CID)
                         JOIN(CLI:PKey_CID,CLIP:CID)
                           PROJECT(CLI:CID)
                           PROJECT(CLI:ClientName)
                           PROJECT(CLI:ClientNo)
                         END
                       END
                     END
ProgressWindow       WINDOW('Report Client Payments Allocation'),AT(,,202,180),FONT('Tahoma',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,MDI,TIMER(1)
                       SHEET,AT(3,3,196,156),USE(?Sheet1)
                         TAB('Options'),USE(?Tab1)
                           GROUP,AT(10,83,182,28),USE(?Group_From_To)
                             PROMPT('From Date:'),AT(10,83),USE(?From_Date:Prompt),TRN
                             BUTTON('...'),AT(55,83,12,10),USE(?Calendar)
                             SPIN(@d6b),AT(71,83,60,10),USE(L_OG:From_Date),MSG('From Date'),TIP('From Date')
                             PROMPT('To Date:'),AT(10,102),USE(?To_Date:Prompt),TRN
                             BUTTON('...'),AT(55,102,12,10),USE(?Calendar:2)
                             SPIN(@d6b),AT(71,102,60,10),USE(L_OG:To_Date),MSG('To Date'),TIP('To Date')
                           END
                           CHECK(' Allocation Details'),AT(71,134),USE(L_OG:AllocationDetails),DISABLE,MSG('Print allo' & |
  'cation details'),TIP('Print allocation details'),TRN
                           GROUP,AT(10,38,183,10),USE(?Group_Client)
                             PROMPT('Client Name:'),AT(10,38),USE(?CLientName:Prompt),TRN
                             BUTTON('...'),AT(55,38,12,10),USE(?CallLookup)
                             ENTRY(@s35),AT(71,38,121,10),USE(L_OG:CLientName),REQ,TIP('Client name')
                           END
                           PROMPT('Date Option:'),AT(10,67),USE(?Date_Option:Prompt),TRN
                           LIST,AT(71,67,121,10),USE(L_OG:Date_Option),DROP(5),FROM('Specify Date Range|#0|All Entries|#1')
                           PROMPT('Client Option:'),AT(10,22),USE(?Option:Prompt),TRN
                           LIST,AT(71,22,60,10),USE(L_OG:Option),DROP(5),FROM('All|#0|Single|#1'),MSG('All Clients' & |
  ' or a single Client'),TIP('All Clients or a single Client')
                         END
                         TAB('Run'),USE(?Tab2),DISABLE
                           PROGRESS,AT(46,34,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(30,23,141,10),USE(?Progress:UserString),CENTER,TRN
                           STRING(''),AT(30,49,141,10),USE(?Progress:PctText),CENTER,TRN
                         END
                       END
                       BUTTON('&Pause'),AT(98,162,49,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                       BUTTON('Cancel'),AT(150,162,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('ClientsPaymentsAllocation Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4), |
  FONT('MS Sans Serif',8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Clients Payments Allocation'),AT(0,20,7750),USE(?ReportTitle),FONT('Arial',14,,FONT:bold), |
  CENTER
                         STRING('Client'),AT(4323,385,3333,156),USE(?HeaderTitle:8),CENTER,TRN
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(635,350,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1240,350,0,250),USE(?HeaderLine:11),COLOR(COLOR:Black)
                         LINE,AT(2260,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(3125,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(4042,354,0,250),USE(?HeaderLine:31),COLOR(COLOR:Black)
                         LINE,AT(7750,350,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         STRING('Pay ID'),AT(52,385,500,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Alloc. No.'),AT(688,385,,170),USE(?HeaderTitle:2),TRN
                         STRING('Invoice No.'),AT(3313,385,625,156),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('Alloc. Amount'),AT(1313,385,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Alloc. Date'),AT(2438,385,583,167),USE(?HeaderTitle:5),TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(635,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(1240,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(2260,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(3125,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(4042,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         STRING(@n_10),AT(-115,52,,170),USE(CLIPA:CPID),RIGHT,TRN
                         STRING(@n_5),AT(688,52,479,167),USE(CLIPA:AllocationNo),RIGHT
                         STRING(@n_10),AT(3229,52,,170),USE(CLIPA:IID),RIGHT,TRN
                         STRING(@n-14.2),AT(1354,52,,170),USE(CLIPA:Amount),RIGHT,TRN
                         STRING(@d5),AT(2438,52,,167),USE(CLIPA:AllocationDate),LEFT
                         STRING(@s35),AT(4323,52,2604,156),USE(CLI:ClientName)
                         STRING(@n_10b),AT(7031,52),USE(CLI:ClientNo),RIGHT(1),TRN
                       END
grandtotals            DETAIL,AT(,,,396),USE(?grandtotals)
                         LINE,AT(156,52,7500,0),USE(?Line19),COLOR(COLOR:Black)
                         STRING('Total:'),AT(313,104),USE(?String27),TRN
                         STRING(@n-14.2),AT(1354,104,,170),USE(CLIPA:Amount,,?CLIPA:Amount:2),RIGHT,SUM,TALLY(Detail), |
  TRN
                         LINE,AT(167,313,7500,0),USE(?Line19:2),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Client, Payment & Allocation No.' & |
      '|' & 'By Invoice' & |
      '|' & 'By Allocation Date' & |
      '|' & 'By Date Payment Made' & |
      '|' & 'By Date Captured' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:grandtotals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Reciepts')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?From_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:Option',L_OG:Option)                          ! Added by: Report
  BIND('L_OG:CID',L_OG:CID)                                ! Added by: Report
  BIND('L_OG:Date_Option',L_OG:Date_Option)                ! Added by: Report
  BIND('L_OG:From_Date',L_OG:From_Date)                    ! Added by: Report
  BIND('L_OG:To_Date',L_OG:To_Date)                        ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Reciepts',ProgressWindow)            ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:ClientsPaymentsAllocation, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client, Payment & Allocation No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientNo,+CLIPA:CPID,+CLIPA:AllocationNo,+CLIPA:CPAID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice')) THEN
     ThisReport.AppendOrder('+CLIPA:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Allocation Date')) THEN
     ThisReport.AppendOrder('+CLIPA:AllocationDateAndTime,+CLIPA:CPAID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Date Payment Made')) THEN
     ThisReport.AppendOrder('+CLIP:DateAndTimeMade,+CLIPA:CPAID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Date Captured')) THEN
     ThisReport.AppendOrder('+CLIP:DateAndTimeCaptured,+CLIPA:CPAID')
  END
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:ClientsPaymentsAllocation.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Reciepts',ProgressWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',L_OG:From_Date)
      IF Calendar5.Response = RequestCompleted THEN
      L_OG:From_Date=Calendar5.SelectedDate
      DISPLAY(?L_OG:From_Date)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',L_OG:To_Date)
      IF Calendar6.Response = RequestCompleted THEN
      L_OG:To_Date=Calendar6.SelectedDate
      DISPLAY(?L_OG:To_Date)
      END
      ThisWindow.Reset(True)
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = L_OG:CLientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_OG:CLientName = CLI:ClientName
        L_OG:CID = CLI:CID
        L_OG:ClientNo = CLI:ClientNo
      END
      ThisWindow.Reset(1)
    OF ?L_OG:CLientName
      IF L_OG:CLientName OR ?L_OG:CLientName{PROP:Req}
        CLI:ClientName = L_OG:CLientName
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_OG:CLientName = CLI:ClientName
            L_OG:CID = CLI:CID
            L_OG:ClientNo = CLI:ClientNo
          ELSE
            CLEAR(L_OG:CID)
            CLEAR(L_OG:ClientNo)
            SELECT(?L_OG:CLientName)
            CYCLE
          END
        ELSE
          L_OG:CID = CLI:CID
          L_OG:ClientNo = CLI:ClientNo
        END
      END
      ThisWindow.Reset()
    OF ?L_OG:Date_Option
          IF L_OG:Date_Option = 1
             DISABLE(?Group_From_To)
          ELSE
             ENABLE(?Group_From_To)
          .
          
    OF ?L_OG:Option
          IF L_OG:Option = 1
             ENABLE(?Group_Client)
          ELSE
             DISABLE(?Group_Client)
          .
    OF ?Pause
      ThisWindow.Update()
          IF SELF.Paused = FALSE
             ! 1 = single client
             IF L_OG:Option = 1 AND CLI:CID = 0
                MESSAGE('Select a Client to use this option.', 'Option', ICON:Asterisk)
                SELECT(?L_OG:CLientName)
                SELF.Paused = TRUE
                CYCLE
             .
      
             ! 'Specify Date Range|#0|All Entries|#1'
             IF L_OG:Date_Option = 0 AND L_OG:From_Date = 0 AND L_OG:To_Date = 0
                MESSAGE('Enter something for at least one of From Date or To Date.', 'Date Option', ICON:Asterisk)
                SELECT(?L_OG:From_Date)
                SELF.Paused = TRUE
                CYCLE
          .  .
          IF SELF.Paused = FALSE
             ENABLE(?Tab2)
             SELECT(?Tab2)
      
      
             ! 1 = single client
             IF L_OG:Option = 1
                ThisReport.SetFilter('L_OG:CID = CLI:CID','1')
             .
      
             ! 'Specify Date Range|#0|All Entries|#1'
             IF L_OG:Date_Option = 0
                IF L_OG:From_Date ~= 0 AND L_OG:To_Date ~= 0
                   !ThisReport.SetFilter('CLIP:DateMade >= L_OG:From_Date AND CLIP:DateMade < L_OG:To_Date + 1','2')
                   ThisReport.SetFilter('CLIPA:AllocationDate >= L_OG:From_Date AND CLIPA:AllocationDate < L_OG:To_Date + 1','2')
                   
                ELSIF L_OG:From_Date = 0
                   ThisReport.SetFilter('CLIPA:AllocationDate < L_OG:To_Date + 1','2')
                ELSE
                   ThisReport.SetFilter('CLIPA:AllocationDate >= L_OG:From_Date','2')
          .  .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_OG:Date_Option
          IF L_OG:Date_Option = 1
             DISABLE(?Group_From_To)
          ELSE
             ENABLE(?Group_From_To)
          .
          
    OF ?L_OG:Option
          IF L_OG:Option = 1
             ENABLE(?Group_Client)
          ELSE
             DISABLE(?Group_Client)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF L_OG:Option = 1
             ENABLE(?Group_Client)
          ELSE
             DISABLE(?Group_Client)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      ! p:Option
      !   0.  Invoice Paid
      !   1.  Clients Payments Allocation total
  
      ! (p:ID, p:Option)
  
      L_OG:Owing      = INV:Total - Get_ClientsPay_Alloc_Amt(CLIPA:IID, 0)
       If L_OG:AllocationDetails = TRUE
       ElSE
          SetTarget(Report,?Detail)
  
          ?DetailLine:0{Prop:Hide}          = True
          ?DetailLine:1{Prop:Hide}          = True
          ?DetailLine:2{Prop:Hide}          = True
          ?DetailLine:3{Prop:Hide}          = True
          ?DetailLine:4{Prop:Hide}          = True
          ?DetailLine:5{Prop:Hide}          = True
          !?DetailLine:8{Prop:Hide}          = True
          !?DetailLine:7{Prop:Hide}          = True
          ?DetailLine:6{Prop:Hide}          = True
          ?DetailEndLine{Prop:Hide}         = True
  
          !?L_OG:Owing{Prop:Hide}            = True
          !?INV:Total{Prop:Hide}             = True
          ?CLIPA:CPID{Prop:Hide}            = True
          ?CLIPA:AllocationNo{Prop:Hide}    = True
          ?CLIPA:IID{Prop:Hide}             = True
  
          ?CLIPA:Amount{Prop:Hide}          = True
          ?CLIPA:AllocationDate{Prop:Hide}  = True
          ?CLI:ClientName{Prop:Hide}        = True
          ?CLI:ClientNo{Prop:Hide}          = True
  
          ?Detail{Prop:Height}              = 0
  
          SetTarget()
       .
  IF 0
    PRINT(RPT:grandtotals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','MANTRNIS','Print_Reciepts','Print_Reciepts','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Reciepts_with_Headings PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Options          GROUP,PRE(L_OG)                       ! 
Date_Option          BYTE                                  ! 
From_Date            DATE                                  ! From Date
To_Date              DATE                                  ! To Date
Option               BYTE                                  ! All Clients or a single Client
CID                  ULONG                                 ! Client ID
CLientName           STRING(35)                            ! 
ClientNo             ULONG                                 ! Client No.
Owing                DECIMAL(11,2)                         ! 
AllocationDetails    BYTE(1)                               ! Print allocation details
                     END                                   ! 
Process:View         VIEW(ClientsPaymentsAllocation)
                       PROJECT(CLIPA:AllocationDate)
                       PROJECT(CLIPA:AllocationNo)
                       PROJECT(CLIPA:Amount)
                       PROJECT(CLIPA:CPID)
                       PROJECT(CLIPA:Comment)
                       PROJECT(CLIPA:IID)
                       JOIN(INV:PKey_IID,CLIPA:IID)
                         PROJECT(INV:Total)
                       END
                       JOIN(CLIP:PKey_CPID,CLIPA:CPID)
                         PROJECT(CLIP:Amount)
                         PROJECT(CLIP:CPID)
                         PROJECT(CLIP:DateCaptured)
                         PROJECT(CLIP:DateMade)
                         PROJECT(CLIP:Notes)
                         PROJECT(CLIP:CID)
                         JOIN(CLI:PKey_CID,CLIP:CID)
                           PROJECT(CLI:CID)
                           PROJECT(CLI:ClientName)
                           PROJECT(CLI:ClientNo)
                         END
                       END
                     END
ProgressWindow       WINDOW('Report Client Payments Allocation'),AT(,,202,172),FONT('Tahoma',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,MDI,TIMER(1)
                       SHEET,AT(3,3,196,149),USE(?Sheet1)
                         TAB('Options'),USE(?Tab1)
                           GROUP,AT(9,39,183,10),USE(?Group_Client)
                             PROMPT('Client Name:'),AT(9,39),USE(?CLientName:Prompt),TRN
                             BUTTON('...'),AT(54,39,12,10),USE(?CallLookup)
                             ENTRY(@s35),AT(71,39,121,10),USE(L_OG:CLientName),REQ,TIP('Client name')
                           END
                           PROMPT('Date Option:'),AT(9,68),USE(?Date_Option:Prompt),TRN
                           LIST,AT(71,68,121,10),USE(L_OG:Date_Option),DROP(5),FROM('Specify Date Range|#0|All Entries|#1')
                           PROMPT('From Date:'),AT(9,84),USE(?From_Date:Prompt),TRN
                           BUTTON('...'),AT(54,84,12,10),USE(?Calendar)
                           SPIN(@d6b),AT(71,84,60,10),USE(L_OG:From_Date),MSG('From Date'),TIP('From Date')
                           PROMPT('To Date:'),AT(9,103),USE(?To_Date:Prompt),TRN
                           BUTTON('...'),AT(54,103,12,10),USE(?Calendar:2)
                           SPIN(@d6b),AT(71,103,60,10),USE(L_OG:To_Date),MSG('To Date'),TIP('To Date')
                           CHECK(' Allocation Details'),AT(71,137),USE(L_OG:AllocationDetails),MSG('Print allocati' & |
  'on details'),TIP('Print allocation details'),TRN
                           PROMPT('Client Option:'),AT(9,23),USE(?Option:Prompt),TRN
                           LIST,AT(71,23,60,10),USE(L_OG:Option),DROP(5),FROM('All|#0|Single|#1'),MSG('All Clients' & |
  ' or a single Client'),TIP('All Clients or a single Client')
                         END
                         TAB('Run'),USE(?Tab2),DISABLE
                           PROGRESS,AT(47,33,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(31,22,141,10),USE(?Progress:UserString),CENTER,TRN
                           STRING(''),AT(31,49,141,10),USE(?Progress:PctText),CENTER,TRN
                         END
                       END
                       BUTTON('&Pause'),AT(98,154,49,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                       BUTTON('Cancel'),AT(150,154,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('ClientsPaymentsAllocation Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4), |
  FONT('MS Sans Serif',8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Clients Payments Allocation'),AT(0,20,7750),USE(?ReportTitle),FONT('Arial',14,,FONT:bold), |
  CENTER
                         STRING('Invoice Tot.'),AT(3490,385,781,156),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Invoice Owe'),AT(4385,385,,167),USE(?HeaderTitle:7),DECIMAL,TRN
                         STRING('Comments'),AT(5156,385,2500,156),USE(?HeaderTitle:8),CENTER,TRN
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(635,350,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1240,350,0,250),USE(?HeaderLine:11),COLOR(COLOR:Black)
                         LINE,AT(2063,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(2698,350,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(3406,350,0,250),USE(?HeaderLine:31),COLOR(COLOR:Black)
                         LINE,AT(4302,365,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(5083,350,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(7750,350,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         STRING('Pay ID'),AT(52,385,500,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Alloc. No.'),AT(688,385,,170),USE(?HeaderTitle:2),TRN
                         STRING('Invoice No.'),AT(2760,385,625,156),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('Alloc. Amount'),AT(1313,385,708,167),USE(?HeaderTitle:4),TRN
                         STRING('Alloc. Date'),AT(2104,385,583,167),USE(?HeaderTitle:5),TRN
                       END
break_client           BREAK(CLI:CID),USE(?break_client)
                         HEADER,AT(0,0,,385),USE(?Client_Hdr)
                           STRING('Client:'),AT(469,63),USE(?String26),FONT('Arial',12,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@s35),AT(1198,63,3385,260),USE(CLI:ClientName,,?CLI:ClientName:2),FONT('Arial',12,, |
  FONT:bold,CHARSET:ANSI)
                           STRING(@n_10b),AT(4688,63,1302,260),USE(CLI:ClientNo,,?CLI:ClientNo:2),FONT('Arial',12,,FONT:bold, |
  CHARSET:ANSI),RIGHT(1)
                         END
break_ClientPayments     BREAK(CLIP:CPID),USE(?break_ClientPayments)
                           HEADER,AT(0,0,,500),USE(?Payment_Hdr)
                             STRING('Payment ID'),AT(125,52,688,156),USE(?String32),FONT(,,,FONT:underline,CHARSET:ANSI), |
  CENTER,TRN
                             STRING('Amount'),AT(1031,52,833,167),USE(?String33),FONT(,,,FONT:underline,CHARSET:ANSI),CENTER, |
  TRN
                             STRING('Date Made'),AT(2104,52,740,167),USE(?String34),FONT(,,,FONT:underline,CHARSET:ANSI), |
  CENTER,TRN
                             STRING('Date Captured'),AT(3094,52,740,167),USE(?String34:2),FONT(,,,FONT:underline,CHARSET:ANSI), |
  CENTER,TRN
                             STRING('Notes'),AT(4135,52,1094,156),USE(?String34:3),FONT(,,,FONT:underline,CHARSET:ANSI), |
  TRN
                             STRING(@n-14.2),AT(1031,260),USE(CLIP:Amount),RIGHT(1)
                             STRING(@d6),AT(2104,260),USE(CLIP:DateMade),RIGHT(1)
                             STRING(@d6),AT(3094,260),USE(CLIP:DateCaptured),RIGHT(1)
                             STRING(@n_10),AT(125,260),USE(CLIP:CPID),RIGHT(1)
                             STRING(@s255),AT(4135,260,3490,156),USE(CLIP:Notes)
                             LINE,AT(0,500,7750,0),USE(?Line19),COLOR(COLOR:Black)
                           END
Detail                     DETAIL,AT(,,7750,250),USE(?Detail)
                             LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                             LINE,AT(635,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                             LINE,AT(1240,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                             LINE,AT(2063,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                             LINE,AT(2698,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                             LINE,AT(3406,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                             LINE,AT(4302,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                             LINE,AT(5083,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                             LINE,AT(7750,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                             LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                             STRING(@n-15.2),AT(4156,52),USE(L_OG:Owing),RIGHT(1),TRN
                             STRING(@n-15.2),AT(3354,52),USE(INV:Total),RIGHT(1),TRN
                             STRING(@n_10),AT(-115,52,,170),USE(CLIPA:CPID),RIGHT,TRN
                             STRING(@n_5),AT(688,52,479,167),USE(CLIPA:AllocationNo),RIGHT
                             STRING(@n_10),AT(2677,52,,170),USE(CLIPA:IID),RIGHT,TRN
                             STRING(@n-14.2),AT(1188,52,,170),USE(CLIPA:Amount),RIGHT,TRN
                             STRING(@d5),AT(2094,52,,167),USE(CLIPA:AllocationDate),RIGHT(1)
                             STRING(@s255),AT(5156,52,2552,156),USE(CLIPA:Comment)
                           END
grandtotals                DETAIL,AT(,,,354),USE(?grandtotals)
                             LINE,AT(52,52,7604,0),USE(?Line20),COLOR(COLOR:Black)
                             STRING('Totals'),AT(104,83),USE(?String39),TRN
                             STRING(@n-14.2),AT(1188,83,,170),USE(CLIPA:Amount,,?CLIPA:Amount:2),RIGHT,SUM,TALLY(Detail), |
  TRN
                             STRING(@n-15.2),AT(3354,83),USE(INV:Total,,?INV:Total:2),RIGHT(1),SUM,TALLY(Detail),TRN
                             STRING(@n-15.2),AT(4156,83),USE(L_OG:Owing,,?L_OG:Owing:2),RIGHT(1),SUM,TALLY(Detail),TRN
                             LINE,AT(52,281,7604,0),USE(?Line20:2),COLOR(COLOR:Black)
                           END
                         END
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Client, Payment & Allocation No.' & |
      '|' & 'By Invoice' & |
      '|' & 'By Allocation Date' & |
      '|' & 'By Date Payment Made' & |
      '|' & 'By Date Captured' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:grandtotals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Reciepts_with_Headings')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CLientName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:Option',L_OG:Option)                          ! Added by: Report
  BIND('L_OG:CID',L_OG:CID)                                ! Added by: Report
  BIND('L_OG:Date_Option',L_OG:Date_Option)                ! Added by: Report
  BIND('L_OG:From_Date',L_OG:From_Date)                    ! Added by: Report
  BIND('L_OG:To_Date',L_OG:To_Date)                        ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Reciepts_with_Headings',ProgressWindow) ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:ClientsPaymentsAllocation, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client, Payment & Allocation No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientNo,+CLIPA:CPID,+CLIPA:AllocationNo,+CLIPA:CPAID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice')) THEN
     ThisReport.AppendOrder('+CLIPA:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Allocation Date')) THEN
     ThisReport.AppendOrder('+CLIPA:AllocationDateAndTime,+CLIPA:CPAID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Date Payment Made')) THEN
     ThisReport.AppendOrder('+CLIP:DateAndTimeMade,+CLIPA:CPAID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Date Captured')) THEN
     ThisReport.AppendOrder('+CLIP:DateAndTimeCaptured,+CLIPA:CPAID')
  END
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:ClientsPaymentsAllocation.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Reciepts_with_Headings',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = L_OG:CLientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_OG:CLientName = CLI:ClientName
        L_OG:CID = CLI:CID
        L_OG:ClientNo = CLI:ClientNo
      END
      ThisWindow.Reset(1)
    OF ?L_OG:CLientName
      IF L_OG:CLientName OR ?L_OG:CLientName{PROP:Req}
        CLI:ClientName = L_OG:CLientName
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_OG:CLientName = CLI:ClientName
            L_OG:CID = CLI:CID
            L_OG:ClientNo = CLI:ClientNo
          ELSE
            CLEAR(L_OG:CID)
            CLEAR(L_OG:ClientNo)
            SELECT(?L_OG:CLientName)
            CYCLE
          END
        ELSE
          L_OG:CID = CLI:CID
          L_OG:ClientNo = CLI:ClientNo
        END
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',L_OG:From_Date)
      IF Calendar5.Response = RequestCompleted THEN
      L_OG:From_Date=Calendar5.SelectedDate
      DISPLAY(?L_OG:From_Date)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',L_OG:To_Date)
      IF Calendar6.Response = RequestCompleted THEN
      L_OG:To_Date=Calendar6.SelectedDate
      DISPLAY(?L_OG:To_Date)
      END
      ThisWindow.Reset(True)
    OF ?L_OG:Option
          IF L_OG:Option = 1
             ENABLE(?Group_Client)
          ELSE
             DISABLE(?Group_Client)
          .
    OF ?Pause
      ThisWindow.Update()
          IF SELF.Paused = FALSE
             ! 1 = single client
             IF L_OG:Option = 1 AND CLI:CID = 0
                MESSAGE('Select a Client to use this option.', 'Option', ICON:Asterisk)
                SELECT(?L_OG:CLientName)
                SELF.Paused = TRUE
                CYCLE
             .
      
             ! 'Specify Date Range|#0|All Entries|#1'
             IF L_OG:Date_Option = 0 AND L_OG:From_Date = 0 AND L_OG:To_Date = 0
                MESSAGE('Enter something for at least one of From Date or To Date.', 'Date Option', ICON:Asterisk)
                SELECT(?L_OG:From_Date)
                SELF.Paused = TRUE
                CYCLE
          .  .
          IF SELF.Paused = FALSE
             ENABLE(?Tab2)
             SELECT(?Tab2)
      
      
             ! 1 = single client
             IF L_OG:Option = 1
                ThisReport.SetFilter('L_OG:CID = CLI:CID','1')
             .
      
             ! 'Specify Date Range|#0|All Entries|#1'
             IF L_OG:Date_Option = 0
                IF L_OG:From_Date ~= 0 AND L_OG:To_Date ~= 0
                   !ThisReport.SetFilter('CLIP:DateMade >= L_OG:From_Date AND CLIP:DateMade < L_OG:To_Date + 1','2')
                   ThisReport.SetFilter('CLIPA:AllocationDate >= L_OG:From_Date AND CLIPA:AllocationDate < L_OG:To_Date + 1','2')
                   
                ELSIF L_OG:From_Date = 0
                   ThisReport.SetFilter('CLIPA:AllocationDate < L_OG:To_Date + 1','2')
                ELSE
                   ThisReport.SetFilter('CLIPA:AllocationDate >= L_OG:From_Date','2')
          .  .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_OG:Option
          IF L_OG:Option = 1
             ENABLE(?Group_Client)
          ELSE
             DISABLE(?Group_Client)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF L_OG:Option = 1
             ENABLE(?Group_Client)
          ELSE
             DISABLE(?Group_Client)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      ! p:Option
      !   0.  Invoice Paid
      !   1.  Clients Payments Allocation total
  
      ! (p:ID, p:Option)
  
  !    L_OG:Owing      = Get_ClientsPay_Alloc_Amt(CLIPA:IID, 1)
  
  
      L_OG:Owing      = INV:Total - Get_ClientsPay_Alloc_Amt(CLIPA:IID, 0)
       If L_OG:AllocationDetails = TRUE
       ElSE
          SetTarget(Report,?Detail)
  
          ?DetailLine:0{Prop:Hide}          = True
          ?DetailLine:1{Prop:Hide}          = True
          ?DetailLine:2{Prop:Hide}          = True
          ?DetailLine:3{Prop:Hide}          = True
          ?DetailLine:4{Prop:Hide}          = True
          ?DetailLine:5{Prop:Hide}          = True
          ?DetailLine:8{Prop:Hide}          = True
          ?DetailLine:7{Prop:Hide}          = True
          ?DetailLine:6{Prop:Hide}          = True
          ?DetailEndLine{Prop:Hide}         = True
  
          ?L_OG:Owing{Prop:Hide}            = True
          ?INV:Total{Prop:Hide}             = True
          ?CLIPA:CPID{Prop:Hide}            = True
          ?CLIPA:AllocationNo{Prop:Hide}    = True
          ?CLIPA:IID{Prop:Hide}             = True
  
          ?CLIPA:Amount{Prop:Hide}          = True
          ?CLIPA:AllocationDate{Prop:Hide}  = True
          !?CLI:ClientName{Prop:Hide}        = True
          !?CLI:ClientNo{Prop:Hide}          = True
  
          ?Detail{Prop:Height}              = 0
  
          SetTarget()
       .
  IF 0
    PRINT(RPT:grandtotals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','MANTRNIS','Print_Reciepts_with_Headings','Print_Reciepts_with_Headings','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! **** not used ****
!!! </summary>
Print_Reciepts_Extended PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Options          GROUP,PRE(L_OG)                       ! 
Date_Option          BYTE                                  ! 
From_Date            DATE                                  ! From Date
To_Date              DATE                                  ! To Date
Option               BYTE                                  ! All Clients or a single Client
CID                  ULONG                                 ! Client ID
CLientName           STRING(35)                            ! 
ClientNo             ULONG                                 ! Client No.
Owing                DECIMAL(11,2)                         ! 
AllocationDetails    BYTE(1)                               ! Print allocation details
                     END                                   ! 
Process:View         VIEW(ClientsPaymentsAllocation)
                       PROJECT(CLIPA:AllocationDate)
                       PROJECT(CLIPA:AllocationNo)
                       PROJECT(CLIPA:Amount)
                       PROJECT(CLIPA:CPID)
                       PROJECT(CLIPA:IID)
                       JOIN(INV:PKey_IID,CLIPA:IID)
                         PROJECT(INV:Total)
                       END
                       JOIN(CLIP:PKey_CPID,CLIPA:CPID)
                         PROJECT(CLIP:DateMade)
                         PROJECT(CLIP:CID)
                         JOIN(CLI:PKey_CID,CLIP:CID)
                           PROJECT(CLI:CID)
                           PROJECT(CLI:ClientName)
                           PROJECT(CLI:ClientNo)
                         END
                       END
                     END
ProgressWindow       WINDOW('Report Client Payments Allocation'),AT(,,202,180),FONT('Tahoma',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,MDI,TIMER(1)
                       SHEET,AT(3,3,196,156),USE(?Sheet1)
                         TAB('Options'),USE(?Tab1)
                           GROUP,AT(9,84,182,28),USE(?Group_From_To)
                             PROMPT('From Date:'),AT(9,84),USE(?From_Date:Prompt),TRN
                             BUTTON('...'),AT(54,84,12,10),USE(?Calendar)
                             SPIN(@d6b),AT(71,84,60,10),USE(L_OG:From_Date),MSG('From Date'),TIP('From Date')
                             PROMPT('To Date:'),AT(9,102),USE(?To_Date:Prompt),TRN
                             BUTTON('...'),AT(54,102,12,10),USE(?Calendar:2)
                             SPIN(@d6b),AT(71,102,60,10),USE(L_OG:To_Date),MSG('To Date'),TIP('To Date')
                           END
                           CHECK(' Allocation Details'),AT(71,135),USE(L_OG:AllocationDetails),DISABLE,MSG('Print allo' & |
  'cation details'),TIP('Print allocation details'),TRN
                           GROUP,AT(9,39,183,10),USE(?Group_Client)
                             PROMPT('Client Name:'),AT(9,39),USE(?CLientName:Prompt),TRN
                             BUTTON('...'),AT(54,39,12,10),USE(?CallLookup)
                             ENTRY(@s35),AT(71,39,121,10),USE(L_OG:CLientName),REQ,TIP('Client name')
                           END
                           PROMPT('Date Option:'),AT(9,68),USE(?Date_Option:Prompt),TRN
                           LIST,AT(71,68,121,10),USE(L_OG:Date_Option),DROP(5),FROM('Specify Date Range|#0|All Entries|#1')
                           PROMPT('Client Option:'),AT(9,23),USE(?Option:Prompt),TRN
                           LIST,AT(71,23,60,10),USE(L_OG:Option),DROP(5),FROM('All|#0|Single|#1'),MSG('All Clients' & |
  ' or a single Client'),TIP('All Clients or a single Client')
                         END
                         TAB('Run'),USE(?Tab2),DISABLE
                           PROGRESS,AT(47,33,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(31,22,141,10),USE(?Progress:UserString),CENTER,TRN
                           STRING(''),AT(31,49,141,10),USE(?Progress:PctText),CENTER,TRN
                         END
                       END
                       BUTTON('&Pause'),AT(98,162,49,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                       BUTTON('Cancel'),AT(150,162,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('ClientsPaymentsAllocation Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4), |
  FONT('MS Sans Serif',8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Clients Payments Allocation'),AT(0,20,7750),USE(?ReportTitle),FONT('Arial',14,,FONT:bold), |
  CENTER
                         STRING('Invoice Tot.'),AT(3490,385,781,156),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Invoice Owe'),AT(4385,385,,167),USE(?HeaderTitle:7),DECIMAL,TRN
                         STRING('Client'),AT(5156,385,2500,156),USE(?HeaderTitle:8),CENTER,TRN
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(635,350,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1240,350,0,250),USE(?HeaderLine:11),COLOR(COLOR:Black)
                         LINE,AT(2063,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(2698,350,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(3406,350,0,250),USE(?HeaderLine:31),COLOR(COLOR:Black)
                         LINE,AT(4323,365,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(5083,350,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(7750,350,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         STRING('Pay ID'),AT(52,385,500,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Alloc. No.'),AT(688,385,,170),USE(?HeaderTitle:2),TRN
                         STRING('Invoice No.'),AT(2760,385,625,156),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('Alloc. Amount'),AT(1313,385,708,167),USE(?HeaderTitle:4),TRN
                         STRING('Alloc. Date'),AT(2104,385,583,167),USE(?HeaderTitle:5),TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(635,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(1240,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(2063,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(2698,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(3406,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(4323,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                         LINE,AT(5083,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         STRING(@n-15.2),AT(4156,52),USE(L_OG:Owing),RIGHT(1),TRN
                         STRING(@n-15.2),AT(3354,52),USE(INV:Total),RIGHT(1),TRN
                         STRING(@n_10),AT(-115,52,,170),USE(CLIPA:CPID),RIGHT,TRN
                         STRING(@n_5),AT(688,52,479,167),USE(CLIPA:AllocationNo),RIGHT
                         STRING(@n_10),AT(2677,52,,170),USE(CLIPA:IID),RIGHT,TRN
                         STRING(@n-14.2),AT(1188,52,,170),USE(CLIPA:Amount),RIGHT,TRN
                         STRING(@d5),AT(2104,52,,167),USE(CLIPA:AllocationDate),LEFT
                         STRING(@s35),AT(5135,52),USE(CLI:ClientName)
                         STRING(@n_10b),AT(7031,52),USE(CLI:ClientNo),RIGHT(1),TRN
                       END
grandtotals            DETAIL,AT(,,,396),USE(?grandtotals)
                         LINE,AT(156,52,7500,0),USE(?Line19),COLOR(COLOR:Black)
                         STRING('Totals'),AT(313,104),USE(?String27),TRN
                         STRING(@n-15.2),AT(3438,104),USE(INV:Total,,?INV:Total:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(1354,104,,170),USE(CLIPA:Amount,,?CLIPA:Amount:2),RIGHT,SUM,TALLY(Detail), |
  TRN
                         STRING(@n-15.2),AT(4479,104),USE(L_OG:Owing,,?L_OG:Owing:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         LINE,AT(167,313,7500,0),USE(?Line19:2),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Client, Payment & Allocation No.' & |
      '|' & 'By Invoice' & |
      '|' & 'By Allocation Date' & |
      '|' & 'By Date Payment Made' & |
      '|' & 'By Date Captured' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:grandtotals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Reciepts_Extended')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?From_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:Option',L_OG:Option)                          ! Added by: Report
  BIND('L_OG:CID',L_OG:CID)                                ! Added by: Report
  BIND('L_OG:Date_Option',L_OG:Date_Option)                ! Added by: Report
  BIND('L_OG:From_Date',L_OG:From_Date)                    ! Added by: Report
  BIND('L_OG:To_Date',L_OG:To_Date)                        ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Reciepts_Extended',ProgressWindow)   ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:ClientsPaymentsAllocation, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client, Payment & Allocation No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientNo,+CLIPA:CPID,+CLIPA:AllocationNo,+CLIPA:CPAID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice')) THEN
     ThisReport.AppendOrder('+CLIPA:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Allocation Date')) THEN
     ThisReport.AppendOrder('+CLIPA:AllocationDateAndTime,+CLIPA:CPAID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Date Payment Made')) THEN
     ThisReport.AppendOrder('+CLIP:DateAndTimeMade,+CLIPA:CPAID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Date Captured')) THEN
     ThisReport.AppendOrder('+CLIP:DateAndTimeCaptured,+CLIPA:CPAID')
  END
  ThisReport.SetFilter('(L_OG:Option = 0 OR L_OG:CID = CLI:CID) AND (L_OG:Date_Option = 1 OR (CLIP:DateMade  >= L_OG:From_Date AND CLIP:DateMade << L_OG:To_Date + 1))')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:ClientsPaymentsAllocation.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Reciepts_Extended',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',L_OG:From_Date)
      IF Calendar5.Response = RequestCompleted THEN
      L_OG:From_Date=Calendar5.SelectedDate
      DISPLAY(?L_OG:From_Date)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',L_OG:To_Date)
      IF Calendar6.Response = RequestCompleted THEN
      L_OG:To_Date=Calendar6.SelectedDate
      DISPLAY(?L_OG:To_Date)
      END
      ThisWindow.Reset(True)
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = L_OG:CLientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_OG:CLientName = CLI:ClientName
        L_OG:CID = CLI:CID
        L_OG:ClientNo = CLI:ClientNo
      END
      ThisWindow.Reset(1)
    OF ?L_OG:CLientName
      IF L_OG:CLientName OR ?L_OG:CLientName{PROP:Req}
        CLI:ClientName = L_OG:CLientName
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_OG:CLientName = CLI:ClientName
            L_OG:CID = CLI:CID
            L_OG:ClientNo = CLI:ClientNo
          ELSE
            CLEAR(L_OG:CID)
            CLEAR(L_OG:ClientNo)
            SELECT(?L_OG:CLientName)
            CYCLE
          END
        ELSE
          L_OG:CID = CLI:CID
          L_OG:ClientNo = CLI:ClientNo
        END
      END
      ThisWindow.Reset()
    OF ?L_OG:Date_Option
          IF L_OG:Date_Option = 1
             DISABLE(?Group_From_To)
          ELSE
             ENABLE(?Group_From_To)
          .
          
    OF ?L_OG:Option
          IF L_OG:Option = 1
             ENABLE(?Group_Client)
          ELSE
             DISABLE(?Group_Client)
          .
    OF ?Pause
      ThisWindow.Update()
        IF SELF.Paused = FALSE
           ENABLE(?Tab2)
           SELECT(?Tab2)
        .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_OG:Date_Option
          IF L_OG:Date_Option = 1
             DISABLE(?Group_From_To)
          ELSE
             ENABLE(?Group_From_To)
          .
          
    OF ?L_OG:Option
          IF L_OG:Option = 1
             ENABLE(?Group_Client)
          ELSE
             DISABLE(?Group_Client)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF L_OG:Option = 1
             ENABLE(?Group_Client)
          ELSE
             DISABLE(?Group_Client)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      ! p:Option
      !   0.  Invoice Paid
      !   1.  Clients Payments Allocation total
  
      ! (p:ID, p:Option)
  
      L_OG:Owing      = INV:Total - Get_ClientsPay_Alloc_Amt(CLIPA:IID, 0)
       If L_OG:AllocationDetails = TRUE
       ElSE
          SetTarget(Report,?Detail)
  
          ?DetailLine:0{Prop:Hide}          = True
          ?DetailLine:1{Prop:Hide}          = True
          ?DetailLine:2{Prop:Hide}          = True
          ?DetailLine:3{Prop:Hide}          = True
          ?DetailLine:4{Prop:Hide}          = True
          ?DetailLine:5{Prop:Hide}          = True
          ?DetailLine:8{Prop:Hide}          = True
          ?DetailLine:7{Prop:Hide}          = True
          ?DetailLine:6{Prop:Hide}          = True
          ?DetailEndLine{Prop:Hide}         = True
  
          ?L_OG:Owing{Prop:Hide}            = True
          ?INV:Total{Prop:Hide}             = True
          ?CLIPA:CPID{Prop:Hide}            = True
          ?CLIPA:AllocationNo{Prop:Hide}    = True
          ?CLIPA:IID{Prop:Hide}             = True
  
          ?CLIPA:Amount{Prop:Hide}          = True
          ?CLIPA:AllocationDate{Prop:Hide}  = True
          ?CLI:ClientName{Prop:Hide}        = True
          ?CLI:ClientNo{Prop:Hide}          = True
  
          ?Detail{Prop:Height}              = 0
  
          SetTarget()
       .
  IF 0
    PRINT(RPT:grandtotals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','MANTRNIS','Print_Reciepts_Extended','Print_Reciepts_Extended','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

