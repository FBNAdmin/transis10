

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS010.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Rates_Update PROCEDURE 

CurrentTab           STRING(80)                            ! 
LOC:Locals           GROUP,PRE()                           ! 
LO:IncreaseDecrease  STRING(20)                            ! Increase or Decrease
LO:MinimiumCharge    STRING(20)                            ! Percentage, No Change
LO:CompletedStatus   STRING(20)                            ! Completed Successfully
LO:Rates_Option      STRING(35)                            ! 
LO:AdHoc             STRING(35)                            ! Ad Hoc option - Don't Adjust, Adjust (leave Ad Hoc), Adjust (change to non Ad Hoc)
                     END                                   ! 
LOC:User_Access      BYTE                                  ! 
BRW1::View:Browse    VIEW(__RateUpdates)
                       PROJECT(RATU:RUBID)
                       PROJECT(RATU:Effective_Date)
                       PROJECT(RATU:RunDate)
                       PROJECT(RATU:RunTime)
                       PROJECT(RATU:Percentage)
                       PROJECT(RATU:Set_Percent)
                       PROJECT(RATU:ClientsUpdated)
                       PROJECT(RATU:RatesAdded)
                       PROJECT(RATU:IncreaseDecrease)
                       PROJECT(RATU:MinimiumCharge)
                       PROJECT(RATU:CompletedStatus)
                       PROJECT(RATU:Rates_Option)
                       PROJECT(RATU:AdHoc)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
RATU:RUBID             LIKE(RATU:RUBID)               !List box control field - type derived from field
RATU:Effective_Date    LIKE(RATU:Effective_Date)      !List box control field - type derived from field
RATU:RunDate           LIKE(RATU:RunDate)             !List box control field - type derived from field
RATU:RunTime           LIKE(RATU:RunTime)             !List box control field - type derived from field
LO:Rates_Option        LIKE(LO:Rates_Option)          !List box control field - type derived from local data
LO:IncreaseDecrease    LIKE(LO:IncreaseDecrease)      !List box control field - type derived from local data
RATU:Percentage        LIKE(RATU:Percentage)          !List box control field - type derived from field
LO:MinimiumCharge      LIKE(LO:MinimiumCharge)        !List box control field - type derived from local data
LO:AdHoc               LIKE(LO:AdHoc)                 !List box control field - type derived from local data
RATU:Set_Percent       LIKE(RATU:Set_Percent)         !List box control field - type derived from field
RATU:Set_Percent_Icon  LONG                           !Entry's icon ID
RATU:ClientsUpdated    LIKE(RATU:ClientsUpdated)      !List box control field - type derived from field
RATU:RatesAdded        LIKE(RATU:RatesAdded)          !List box control field - type derived from field
LO:CompletedStatus     LIKE(LO:CompletedStatus)       !List box control field - type derived from local data
RATU:IncreaseDecrease  LIKE(RATU:IncreaseDecrease)    !Browse hot field - type derived from field
RATU:MinimiumCharge    LIKE(RATU:MinimiumCharge)      !Browse hot field - type derived from field
RATU:CompletedStatus   LIKE(RATU:CompletedStatus)     !Browse hot field - type derived from field
RATU:Rates_Option      LIKE(RATU:Rates_Option)        !Browse hot field - type derived from field
RATU:AdHoc             LIKE(RATU:AdHoc)               !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Batch Rate Updates File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_RatesUpdate'),SYSTEM
                       LIST,AT(8,20,342,134),USE(?Browse:1),HVSCROLL,FORMAT('34R(2)|M~RUB ID~C(0)@n_10@46R(2)|' & |
  'M~Effective Date~L(1)@d6@46R(2)|M~Run Date~L(0)@d6@36R(2)|M~Run Time~C(0)@t7@54L(2)|' & |
  'M~Rates Option~C(0)@s30@42L(2)|M~Inc. / Dec.~@s20@32D(12)|M~% (percentage)~L(1)@n7.2' & |
  '@60L(2)|M~Minimium Charge~C(0)@s20@60L(2)|M~Ad Hoc~C(0)@s35@30L(2)|MI~Set Percent~L(' & |
  '1)@p p@[56R(2)|M~Clients Updated~C(0)@n13@52R(2)|M~Rates Added~C(0)@n13@50L(2)|M~Com' & |
  'pleted Status~C(0)@s20@]|M~Result~'),FROM(Queue:Browse:1),IMM,MSG('Browsing the __Ra' & |
  'teUpdates file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(240,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,182,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),DISABLE,FLAT, |
  HIDE,MSG('Insert a Record'),TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,182,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,DISABLE, |
  FLAT,HIDE,MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Effective Date'),USE(?Tab2)
                         END
                         TAB('&2) By Run Date'),USE(?Tab3)
                         END
                         TAB('&3) By RUB ID'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Rates_Update')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:Rates_Option',LO:Rates_Option)         ! Added by: BrowseBox(ABC)
  BIND('LO:IncreaseDecrease',LO:IncreaseDecrease) ! Added by: BrowseBox(ABC)
  BIND('LO:MinimiumCharge',LO:MinimiumCharge)     ! Added by: BrowseBox(ABC)
  BIND('LO:AdHoc',LO:AdHoc)                       ! Added by: BrowseBox(ABC)
  BIND('LO:CompletedStatus',LO:CompletedStatus)   ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:__RateUpdates.SetOpenRelated()
  Relate:__RateUpdates.Open                       ! File __RateUpdates used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:__RateUpdates,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,)                            ! Add the sort order for  for sort order 1
  BRW1.AppendOrder('-RATU:RunDate,-RATU:RUBID')   ! Append an additional sort order
  BRW1.AddSortOrder(,)                            ! Add the sort order for  for sort order 2
  BRW1.AppendOrder('-RATU:RUBID')                 ! Append an additional sort order
  BRW1.AddSortOrder(,)                            ! Add the sort order for  for sort order 3
  BRW1.AppendOrder('-RATU:Effective_Date,-RATU:RUBID') ! Append an additional sort order
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(RATU:RUBID,BRW1.Q.RATU:RUBID)     ! Field RATU:RUBID is a hot field or requires assignment from browse
  BRW1.AddField(RATU:Effective_Date,BRW1.Q.RATU:Effective_Date) ! Field RATU:Effective_Date is a hot field or requires assignment from browse
  BRW1.AddField(RATU:RunDate,BRW1.Q.RATU:RunDate) ! Field RATU:RunDate is a hot field or requires assignment from browse
  BRW1.AddField(RATU:RunTime,BRW1.Q.RATU:RunTime) ! Field RATU:RunTime is a hot field or requires assignment from browse
  BRW1.AddField(LO:Rates_Option,BRW1.Q.LO:Rates_Option) ! Field LO:Rates_Option is a hot field or requires assignment from browse
  BRW1.AddField(LO:IncreaseDecrease,BRW1.Q.LO:IncreaseDecrease) ! Field LO:IncreaseDecrease is a hot field or requires assignment from browse
  BRW1.AddField(RATU:Percentage,BRW1.Q.RATU:Percentage) ! Field RATU:Percentage is a hot field or requires assignment from browse
  BRW1.AddField(LO:MinimiumCharge,BRW1.Q.LO:MinimiumCharge) ! Field LO:MinimiumCharge is a hot field or requires assignment from browse
  BRW1.AddField(LO:AdHoc,BRW1.Q.LO:AdHoc)         ! Field LO:AdHoc is a hot field or requires assignment from browse
  BRW1.AddField(RATU:Set_Percent,BRW1.Q.RATU:Set_Percent) ! Field RATU:Set_Percent is a hot field or requires assignment from browse
  BRW1.AddField(RATU:ClientsUpdated,BRW1.Q.RATU:ClientsUpdated) ! Field RATU:ClientsUpdated is a hot field or requires assignment from browse
  BRW1.AddField(RATU:RatesAdded,BRW1.Q.RATU:RatesAdded) ! Field RATU:RatesAdded is a hot field or requires assignment from browse
  BRW1.AddField(LO:CompletedStatus,BRW1.Q.LO:CompletedStatus) ! Field LO:CompletedStatus is a hot field or requires assignment from browse
  BRW1.AddField(RATU:IncreaseDecrease,BRW1.Q.RATU:IncreaseDecrease) ! Field RATU:IncreaseDecrease is a hot field or requires assignment from browse
  BRW1.AddField(RATU:MinimiumCharge,BRW1.Q.RATU:MinimiumCharge) ! Field RATU:MinimiumCharge is a hot field or requires assignment from browse
  BRW1.AddField(RATU:CompletedStatus,BRW1.Q.RATU:CompletedStatus) ! Field RATU:CompletedStatus is a hot field or requires assignment from browse
  BRW1.AddField(RATU:Rates_Option,BRW1.Q.RATU:Rates_Option) ! Field RATU:Rates_Option is a hot field or requires assignment from browse
  BRW1.AddField(RATU:AdHoc,BRW1.Q.RATU:AdHoc)     ! Field RATU:AdHoc is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_Rates_Update',QuickWindow) ! Restore window settings from non-volatile store
      LOC:User_Access     = Get_User_Access(GLO:UID, 'Rates History - Delete ', 'Browse_Rates_Update')   ! Allow, View, Disallow
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                           ! Will call: Update_Rate_Updates
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_Rates_Update',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,14,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:__RateUpdates.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_Rates_Update',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Rate_Updates
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Delete:4
          CASE MESSAGE('By removing this Batch Rate Update record you will be removing the updated rates reocrds which were made when this batch was run.|This will most likely affect what rates are used on future DI''s.||Are you sure you want to do this?', 'Batch Rate Update Warning', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
          OF BUTTON:No
             CYCLE
          .
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF LOC:User_Access ~= 0
             DISABLE(?Delete:4)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
      SELF.InsertControl=0
      SELF.ChangeControl=0
      IF LOC:User_Access ~= 0
         SELF.DeleteControl=0
      .
  
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      CLEAR(LOC:Locals)
  
      EXECUTE RATU:IncreaseDecrease + 1
         LO:IncreaseDecrease  = 'Increase'
         LO:IncreaseDecrease  = 'Decrease'
      .
  
      EXECUTE RATU:MinimiumCharge + 1
         LO:MinimiumCharge    = 'Percentage'
         LO:MinimiumCharge    = 'No Change'
      .
  
      EXECUTE RATU:CompletedStatus + 1
         LO:CompletedStatus   = 'Started'
         LO:CompletedStatus   = 'Completed Successfully'
         LO:CompletedStatus   = 'Failed 2'
         LO:CompletedStatus   = 'Failed 3'
      .
  
  
      ! Rates|Additional Rates|Container Park Rates|Container Park Discount Minimiums|Fuel Surcharges|(legacy)
      EXECUTE RATU:Rates_Option + 1    ! 0 is legacy!
         LO:Rates_Option      = '(legacy)'
         LO:Rates_Option      = 'Rates'
         LO:Rates_Option      = 'Additional Rates'
         LO:Rates_Option      = 'Container Park Rates'
         LO:Rates_Option      = 'Container Park Discount Minimiums'
         LO:Rates_Option      = 'Fuel Surcharges'
      .
  
  
      ! Don't Adjust|Adjust (leave Ad Hoc)|Adjust (change to non Ad Hoc)
      EXECUTE RATU:AdHoc + 1
         LO:AdHoc             = 'Don''t Adjust'
         LO:AdHoc             = 'Adjust (leave Ad Hoc)'
         LO:AdHoc             = 'Adjust (change to non Ad Hoc)'
      .
  PARENT.SetQueueRecord
  
  IF (RATU:Set_Percent = 1)
    SELF.Q.RATU:Set_Percent_Icon = 2                       ! Set icon from icon list
  ELSE
    SELF.Q.RATU:Set_Percent_Icon = 1                       ! Set icon from icon list
  END


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>2,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>2,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_DeliveryStatuses PROCEDURE 

CurrentTab           STRING(80)                            ! 
LOC:ReportColumn     STRING(20)                            ! 
BRW1::View:Browse    VIEW(DeliveryStatuses)
                       PROJECT(DELS:DeliveryStatus)
                       PROJECT(DELS:ReportColumn)
                       PROJECT(DELS:DSID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DELS:DeliveryStatus    LIKE(DELS:DeliveryStatus)      !List box control field - type derived from field
LOC:ReportColumn       LIKE(LOC:ReportColumn)         !List box control field - type derived from local data
DELS:ReportColumn      LIKE(DELS:ReportColumn)        !Browse hot field - type derived from field
DELS:DSID              LIKE(DELS:DSID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Delivery Statuses File'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('BrowseDeliveryStatuses'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('146L(2)|M~Status~@s35@80L(2)|M~Re' & |
  'port Column~@s20@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the DeliveryStatuses file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Status'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_DeliveryStatuses')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:ReportColumn',LOC:ReportColumn)       ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:DeliveryStatuses.Open                    ! File DeliveryStatuses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:DeliveryStatuses,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon DELS:DeliveryStatus for sort order 1
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,DELS:Key_Status) ! Add the sort order for DELS:Key_Status for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,DELS:DeliveryStatus,1,BRW1) ! Initialize the browse locator using  using key: DELS:Key_Status , DELS:DeliveryStatus
  BRW1.AddField(DELS:DeliveryStatus,BRW1.Q.DELS:DeliveryStatus) ! Field DELS:DeliveryStatus is a hot field or requires assignment from browse
  BRW1.AddField(LOC:ReportColumn,BRW1.Q.LOC:ReportColumn) ! Field LOC:ReportColumn is a hot field or requires assignment from browse
  BRW1.AddField(DELS:ReportColumn,BRW1.Q.DELS:ReportColumn) ! Field DELS:ReportColumn is a hot field or requires assignment from browse
  BRW1.AddField(DELS:DSID,BRW1.Q.DELS:DSID)       ! Field DELS:DSID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_DeliveryStatuses',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                           ! Will call: Update_DeliveryStatuses
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_DeliveryStatuses',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,2,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryStatuses.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_DeliveryStatuses',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_DeliveryStatuses
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! None|Un-Pack|Up-Lift
      CLEAR(LOC:ReportColumn)
      EXECUTE DELS:ReportColumn + 1
         LOC:ReportColumn = 'None'
         LOC:ReportColumn = 'Un-Pack'
         LOC:ReportColumn = 'Up-Lift'
      .
  PARENT.SetQueueRecord
  


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Client_Payments PROCEDURE 

CurrentTab           STRING(80)                            ! 
LOC:Type             STRING(10)                            ! Payment, Reversal
LOC:Status           STRING(20)                            ! Status - Not Allocated, Partial Allocation, Fully Allocated
LOC:Screen_Group     GROUP,PRE(L_SG)                       ! 
Status               LONG(-1)                              ! Status - Not Allocated, Partial Allocation, Fully Allocated
                     END                                   ! 
BRW1::View:Browse    VIEW(ClientsPayments)
                       PROJECT(CLIP:CPID)
                       PROJECT(CLIP:DateCaptured)
                       PROJECT(CLIP:DateMade)
                       PROJECT(CLIP:Amount)
                       PROJECT(CLIP:Notes)
                       PROJECT(CLIP:CID)
                       PROJECT(CLIP:StatusUpToDate)
                       PROJECT(CLIP:Type)
                       PROJECT(CLIP:Status)
                       JOIN(CLI:PKey_CID,CLIP:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
CLIP:CPID              LIKE(CLIP:CPID)                !List box control field - type derived from field
CLIP:DateCaptured      LIKE(CLIP:DateCaptured)        !List box control field - type derived from field
CLIP:DateMade          LIKE(CLIP:DateMade)            !List box control field - type derived from field
CLIP:Amount            LIKE(CLIP:Amount)              !List box control field - type derived from field
LOC:Type               LIKE(LOC:Type)                 !List box control field - type derived from local data
LOC:Status             LIKE(LOC:Status)               !List box control field - type derived from local data
CLIP:Notes             LIKE(CLIP:Notes)               !List box control field - type derived from field
CLIP:CID               LIKE(CLIP:CID)                 !List box control field - type derived from field
CLIP:StatusUpToDate    LIKE(CLIP:StatusUpToDate)      !List box control field - type derived from field
CLIP:StatusUpToDate_Icon LONG                         !Entry's icon ID
CLIP:Type              LIKE(CLIP:Type)                !Browse hot field - type derived from field
CLIP:Status            LIKE(CLIP:Status)              !Browse hot field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Client Payments File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MAX,MDI,HLP('Client_Payments'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Client Name~C(0)@s35@36R' & |
  '(2)|M~Client No.~C(0)@n_10b@42R(2)|M~Payment ID~C(0)@n_10@[38R(2)|M~Captured~C(0)@d5' & |
  '@38R(2)|M~Made~C(0)@d5@]|M~Dates~52R(1)|M~Amount~C(0)@n-14.2@40L(2)|M~Type~C(0)@s10@' & |
  '60L(2)|M~Status~C(0)@s20@150L(2)|M~Notes~@s255@40R(2)|M~CID~C(0)@n_10@66L(2)|MI~Stat' & |
  'us Up To Date~C(0)@p p@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the ClientsPayments file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Payment ID'),USE(?Tab:2)
                         END
                         TAB('&2) By Client'),USE(?Tab:3)
                           BUTTON('Select Clients'),AT(9,158,118,14),USE(?SelectClients),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(248,182,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('&Reverse'),AT(4,180,,14),USE(?Button_Reverse),LEFT,ICON('UNDO.ICO'),FLAT,TIP('Reverse se' & |
  'lected payment & allocations')
                       GROUP,AT(83,183,122,10),USE(?Group_Status)
                         PROMPT('Show Status:'),AT(83,183),USE(?L_SG:Status:Prompt)
                         LIST,AT(130,183,75,10),USE(L_SG:Status),DROP(5),FROM('Not Allocated|#0|Partial Allocati' & |
  'on|#1|Fully Allocated|#2|All|#-1'),MSG('Status'),TIP('Status')
                       END
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Client_Payments')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_SG:Status',L_SG:Status)                 ! Added by: BrowseBox(ABC)
  BIND('LOC:Type',LOC:Type)                       ! Added by: BrowseBox(ABC)
  BIND('LOC:Status',LOC:Status)                   ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Clients.Open                             ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ClientsPayments,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon CLIP:CID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,CLIP:FKey_CID) ! Add the sort order for CLIP:FKey_CID for sort order 1
  BRW1.AddRange(CLIP:CID,Relate:ClientsPayments,Relate:Clients) ! Add file relationship range limit for sort order 1
  BRW1.AppendOrder('+CLIP:CPID')                  ! Append an additional sort order
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon CLIP:CPID for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,CLIP:PKey_CPID) ! Add the sort order for CLIP:PKey_CPID for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,CLIP:CPID,1,BRW1)     ! Initialize the browse locator using  using key: CLIP:PKey_CPID , CLIP:CPID
  BRW1.SetFilter('(L_SG:Status = -1 OR CLIP:Status = L_SG:Status)') ! Apply filter expression to browse
  BRW1.AddResetField(L_SG:Status)                 ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(CLI:ClientName,BRW1.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientNo,BRW1.Q.CLI:ClientNo) ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW1.AddField(CLIP:CPID,BRW1.Q.CLIP:CPID)       ! Field CLIP:CPID is a hot field or requires assignment from browse
  BRW1.AddField(CLIP:DateCaptured,BRW1.Q.CLIP:DateCaptured) ! Field CLIP:DateCaptured is a hot field or requires assignment from browse
  BRW1.AddField(CLIP:DateMade,BRW1.Q.CLIP:DateMade) ! Field CLIP:DateMade is a hot field or requires assignment from browse
  BRW1.AddField(CLIP:Amount,BRW1.Q.CLIP:Amount)   ! Field CLIP:Amount is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Type,BRW1.Q.LOC:Type)         ! Field LOC:Type is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Status,BRW1.Q.LOC:Status)     ! Field LOC:Status is a hot field or requires assignment from browse
  BRW1.AddField(CLIP:Notes,BRW1.Q.CLIP:Notes)     ! Field CLIP:Notes is a hot field or requires assignment from browse
  BRW1.AddField(CLIP:CID,BRW1.Q.CLIP:CID)         ! Field CLIP:CID is a hot field or requires assignment from browse
  BRW1.AddField(CLIP:StatusUpToDate,BRW1.Q.CLIP:StatusUpToDate) ! Field CLIP:StatusUpToDate is a hot field or requires assignment from browse
  BRW1.AddField(CLIP:Type,BRW1.Q.CLIP:Type)       ! Field CLIP:Type is a hot field or requires assignment from browse
  BRW1.AddField(CLIP:Status,BRW1.Q.CLIP:Status)   ! Field CLIP:Status is a hot field or requires assignment from browse
  BRW1.AddField(CLI:CID,BRW1.Q.CLI:CID)           ! Field CLI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_Client_Payments',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                           ! Will call: Update_ClientsPayments
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_Client_Payments',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,12,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_Client_Payments',QuickWindow)    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_ClientsPayments
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Reverse
      BRW1.UpdateViewRecord()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectClients
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_RateMod_Clients()
      ThisWindow.Reset
    OF ?Button_Reverse
      ThisWindow.Update()
      GlobalRequest = InsertRecord
      Update_ClientsPayments(1, CLIP:CPID)
      ThisWindow.Reset
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      EXECUTE CLIP:Type + 1
         LOC:Type     = 'Payment'
         LOC:Type     = 'Reversal'
      ELSE
         LOC:Type     = '?: ' & CLIP:Type
      .
  
  
      EXECUTE CLIP:Status + 1
         LOC:Status   = 'Not Allocated'
         LOC:Status   = 'Partial Allocation'
         LOC:Status   = 'Fully Allocated'
      ELSE
         LOC:Status   = '?: ' & CLIP:Status
      .
  PARENT.SetQueueRecord
  
  IF (CLIP:StatusUpToDate = 1)
    SELF.Q.CLIP:StatusUpToDate_Icon = 2                    ! Set icon from icon list
  ELSE
    SELF.Q.CLIP:StatusUpToDate_Icon = 1                    ! Set icon from icon list
  END


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>1,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>1,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Status, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Status

!!! <summary>
!!! Generated from procedure template - Window
!!!              ***   depricated
!!! </summary>
Upd_DeliveryComp_Wizard PROCEDURE 

LG_DC:Delivery_Composition GROUP,PRE(LG_DC)                ! 
DID                  ULONG                                 ! Delivery ID
DELCID               ULONG                                 ! Delivery Composition ID
CID                  ULONG                                 ! Client ID
Items                USHORT                                ! Total no. of items
Weight               DECIMAL(15,2)                         ! Total weight
Deliveries           USHORT                                ! Total deliveries
Commodity            STRING(35)                            ! Commodity
CMID                 ULONG                                 ! Commodity ID
Packaging            STRING(35)                            ! 
PTID                 ULONG                                 ! Packaging Type ID
                     END                                   ! 
LG_OG:Options_Group  GROUP,PRE(LG_OG)                      ! 
Weight_Per_DI        DECIMAL(15,2)                         ! What weight do you want to allow per DI
All_Charges_on_1st_DI BYTE                                 ! Are all the charges for all the DI's in this Delivery Composition
                     END                                   ! 
LG_CG:Calculated_Group GROUP,PRE(LG_CG)                    ! 
No_DIs               LONG                                  ! Number of DI's
Weight_per_Item      DECIMAL(15,2)                         ! 
                     END                                   ! 
LOC:Control_Group    GROUP,PRE(L_CG)                       ! 
Wiz_Pos              BYTE(1)                               ! 
Fail                 BYTE                                  ! 
                     END                                   ! 
LG_CR:Created_Group  GROUP,PRE(LG_CR)                      ! 
No_DIs               ULONG                                 ! 
                     END                                   ! 
BRW5::View:Browse    VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:Rate)
                       PROJECT(DEL:Charge)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:DELCID)
                       PROJECT(DEL:CID)
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:DIDate             LIKE(DEL:DIDate)               !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
DEL:Rate               LIKE(DEL:Rate)                 !List box control field - type derived from field
DEL:Charge             LIKE(DEL:Charge)               !List box control field - type derived from field
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !List box control field - type derived from field
DEL:DELCID             LIKE(DEL:DELCID)               !Browse key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB6::View:FileDrop  VIEW(Commodities)
                       PROJECT(COM:Commodity)
                       PROJECT(COM:CMID)
                     END
FDB7::View:FileDrop  VIEW(PackagingTypes)
                       PROJECT(PACK:Packaging)
                       PROJECT(PACK:PTID)
                     END
Queue:FileDrop_Commodity QUEUE                        !
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop_Packaging QUEUE                        !
PACK:Packaging         LIKE(PACK:Packaging)           !List box control field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW5::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW5::PopupChoice    SIGNED                       ! Popup current choice
BRW5::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW5::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Delivery Composition Wizard'),AT(,,355,264),FONT('Tahoma',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,IMM,MDI,HLP('Upd_DeliveryComp_Wizard'),SYSTEM
                       SHEET,AT(3,4,349,240),USE(?Sheet1)
                         TAB('Start'),USE(?Tab_Start)
                           PROMPT('Welcome to the Delivery Composition Wizard'),AT(10,25),USE(?Prompt1),FONT(,10,,FONT:bold, |
  CHARSET:ANSI),TRN
                           PROMPT('Use this wizard to create DI Compositions.  A DI Composition is a record which ' & |
  'holds a collection of DI''s that are too large to fit onto a single delivery load.'),AT(10, |
  46,335,70),USE(?Prompt1:2),TRN
                         END
                         TAB('Delivery Composition'),USE(?Tab_DelComp)
                           PROMPT('Weight per DI:'),AT(21,33),USE(?LG_OG:Weight_Per_DI:Prompt),TRN
                           ENTRY(@n-21.2),AT(83,33,60,10),USE(LG_OG:Weight_Per_DI),RIGHT(1),TIP('What weight do yo' & |
  'u want to allow per DI')
                           PROMPT('This is the rough weight you want to have on each DI and should be no more then' & |
  ' the maximium weight you can have per single delivery load.'),AT(153,33,185,30),USE(?Prompt6), |
  TRN
                           PROMPT('Total Weight:'),AT(21,74),USE(?Weight:Prompt),TRN
                           ENTRY(@n-21.2),AT(83,74,60,10),USE(LG_DC:Weight),RIGHT(1),MSG('Total weight'),TIP('Total weight')
                           PROMPT('Total weight for this Delivery'),AT(153,74),USE(?Prompt6:2),TRN
                           PROMPT('No. of Items:'),AT(21,90),USE(?Items:Prompt),TRN
                           SPIN(@n6),AT(83,90,60,10),USE(LG_DC:Items),RIGHT(1),MSG('Total no. of items'),TIP('Total no. of items')
                           PROMPT('No. of Items in this delivery'),AT(153,90),USE(?Prompt6:3),TRN
                           PROMPT('Commodity:'),AT(21,117),USE(?Prompt13),TRN
                           LIST,AT(83,117,153,10),USE(LG_DC:Commodity),VSCROLL,DROP(15),FORMAT('140L(2)|M~Commodity~@s35@'), |
  FROM(Queue:FileDrop_Commodity)
                           PROMPT('Packaging:'),AT(21,132),USE(?Prompt13:2),TRN
                           LIST,AT(83,132,153,10),USE(LG_DC:Packaging),VSCROLL,DROP(15),FORMAT('140L(2)|M~Packaging~@s35@'), |
  FROM(Queue:FileDrop_Packaging)
                           CHECK(' All Charges on 1 st DI'),AT(83,154),USE(LG_OG:All_Charges_on_1st_DI),MSG('Are all th' & |
  'e charges for all the DI''s in this Delivery Composition'),TIP('Are all the charges ' & |
  'for all the DI''s in this Delivery Composition specified on the 1st DI?'),TRN
                           PANEL,AT(10,174,334,45),USE(?Panel1),BEVEL(1,1)
                           PROMPT('No. DI''s:'),AT(21,182),USE(?No_DIs:Prompt),TRN
                           ENTRY(@n10),AT(83,182,60,10),USE(LG_CG:No_DIs),RIGHT(1),COLOR(00E9E9E9h),MSG('Number of DI''s'), |
  READONLY,SKIP,TIP('Number of DI''s')
                           PROMPT('Weight per Item:'),AT(21,200),USE(?LG_CG:Weight_per_Item:Prompt),TRN
                           ENTRY(@n-21.2),AT(83,200,60,10),USE(LG_CG:Weight_per_Item),DECIMAL(12),COLOR(00E9E9E9h),READONLY, |
  SKIP
                         END
                         TAB('Create DI'),USE(?Tab_DI)
                           PROMPT('The next step is to specify the DI details.  These details will be used on all ' & |
  'the DI''s that are created from this Delivery Composition.'),AT(21,26,314,34),USE(?Prompt11), |
  TRN
                           PROMPT('When you press the Next button you will be presented with a DI form to complete' & |
  '.  Once you click OK on that DI form the rest of the DI''s will be created.'),AT(21,70, |
  314,34),USE(?Prompt11:2),TRN
                           PROMPT(''),AT(21,114,314,34),USE(?Prompt11:3),TRN
                         END
                         TAB('Show DI List'),USE(?Tab_DI_List)
                           PROMPT('This is a list of the DI''s that have been created for this Delivery Combination.'), |
  AT(10,22,334,26),USE(?Prompt16),TRN
                           LIST,AT(7,54,340,167),USE(?List),VSCROLL,FORMAT('36R(1)|M~DI No.~L(2)@n_10@36R(1)|M~DI ' & |
  'Date~L(2)@d5b@100L(1)|M~Client Name~L(2)@s100@50L(1)|M~Client Ref.~L(2)@s60@38R(1)|M' & |
  '~Rate~L(2)@n-13.3@38R(1)|M~Charge~L(2)@n-15.2@50L(1)|M~Client Ref.~L(2)@s60@40R(1)|M' & |
  '~DID~L(2)@n_10@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           BUTTON('&View'),AT(7,226,49,14),USE(?View)
                           BUTTON('&Insert'),AT(177,226,49,14),USE(?Insert),DISABLE,HIDE
                           BUTTON('&Change'),AT(57,226,49,14),USE(?Change)
                           BUTTON('&Delete'),AT(127,226,49,14),USE(?Delete),DISABLE,HIDE
                         END
                       END
                       BUTTON('Next &>'),AT(298,226,49,14),USE(?Button_Next)
                       BUTTON('&<< Back'),AT(246,226,49,14),USE(?Button_Back),DISABLE
                       BUTTON('&OK'),AT(250,246,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),DISABLE,FLAT,HIDE,MSG('Accept operation'), |
  TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(302,246,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       PROMPT('Wiz Pos:'),AT(5,249),USE(?L_CG:Wiz_Pos:Prompt)
                       ENTRY(@n3),AT(38,249,35,10),USE(L_CG:Wiz_Pos),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                       BUTTON('&Help'),AT(186,246,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW5::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW5                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                      ! Default Locator
FDB6                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop_Commodity      !Reference to display queue
                     END

FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop_Packaging      !Reference to display queue
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Next_Button                   ROUTINE
    DATA
R:Fail          BYTE

    CODE
    CLEAR(L_CG:Fail)

    CASE L_CG:Wiz_Pos
    OF 1
    OF 2                        ! Tab_DelComp
       DO Comp_Checks
    OF 3
       DO User_DI_Create
    OF 4
    OF 5
    .

    IF L_CG:Fail = FALSE
       L_CG:Wiz_Pos += 1
       DO Change_Tab
    .
    EXIT
Change_Tab                   ROUTINE
    IF L_CG:Wiz_Pos <= 0
       L_CG:Wiz_Pos = 1
    ELSIF L_CG:Wiz_Pos > 4      ! Finished
       POST(EVENT:CloseWindow)
    .

    EXECUTE L_CG:Wiz_Pos
       SELECT(?Tab_Start)
       SELECT(?Tab_DelComp)
       SELECT(?Tab_DI)
       SELECT(?Tab_DI_List)
    .

    ENABLE(?Button_Back)
    ENABLE(?Button_Next)

    CASE L_CG:Wiz_Pos
    OF 1
       DISABLE(?Button_Back)
    OF 3
       ?Button_Next{PROP:Text}  = '&Finish'
    OF 4
       DISABLE(?Button_Back)
    .

    IF L_CG:Wiz_Pos < 3
       IF ?Button_Next{PROP:Text} = '&Finish'
          ?Button_Next{PROP:Text} = '&Next'
    .  .

    DISPLAY
    EXIT
! ---------------------------------------------------
Comp_Calcs                   ROUTINE
    DATA
R:Fail      BYTE

    CODE
    IF LG_DC:Items = 0
       R:Fail   = TRUE
    .
    IF LG_DC:Weight <= 0.0
       R:Fail   = TRUE
    .
    IF LG_OG:Weight_Per_DI <= 0.0
       R:Fail   = TRUE
    .

    IF R:Fail = TRUE
!       DISABLE(?Button_Next)
       CLEAR(LG_CG:Calculated_Group)
    ELSE
       ENABLE(?Button_Next)
       LG_CG:Weight_per_Item   = LG_DC:Weight / LG_DC:Items

       ! No. items per DI...
       Items_Per_DI_#          = INT(LG_OG:Weight_Per_DI / LG_CG:Weight_per_Item)
       
       LG_CG:No_DIs            = INT(LG_DC:Items / Items_Per_DI_#)
       IF LG_DC:Items / Items_Per_DI_# > 0
          LG_CG:No_DIs        += 1
    .  .

    DISPLAY
    EXIT
Comp_Checks                ROUTINE
    DATA
R:Fail      BYTE

    CODE
    DO Comp_Calcs

    LOOP
       IF R:Fail = TRUE
          L_CG:Fail    = TRUE
          BREAK
       .

       IF LG_DC:Items = 0
          MESSAGE('Please specify the number of Items.', 'Delivery Composition', ICON:Hand)
          R:Fail   = TRUE
          SELECT(?LG_DC:Items)
          CYCLE
       .
       IF LG_DC:Weight <= 0.0
          MESSAGE('Please specify the Total Weight.', 'Delivery Composition', ICON:Hand)
          R:Fail   = TRUE
          SELECT(?LG_DC:Weight)
          CYCLE
       .
       IF LG_OG:Weight_Per_DI <= 0.0
          MESSAGE('Please specify the Weight per DI.', 'Delivery Composition', ICON:Hand)
          R:Fail   = TRUE
          SELECT(?LG_OG:Weight_Per_DI)
          CYCLE
       .
       IF LG_CG:Weight_per_Item > LG_OG:Weight_Per_DI
          MESSAGE('A single item would weight more then the Weight per DI.', 'Delivery Composition', ICON:Hand)
          R:Fail   = TRUE
          SELECT(?LG_OG:Weight_Per_DI)
          CYCLE
       .
       IF LG_DC:CMID = 0
          MESSAGE('Please specify a Commodity.', 'Delivery Composition', ICON:Hand)
          R:Fail   = TRUE
          SELECT(?LG_DC:Commodity)
          CYCLE
       .
       IF LG_DC:PTID = 0
          MESSAGE('Please specify a Packaging type.', 'Delivery Composition', ICON:Hand)
          R:Fail   = TRUE
          SELECT(?LG_DC:Packaging)
          CYCLE
       .

       BREAK
    .
    IF R:Fail = FALSE
       PUTINI('Upd_DeliveryComp_Wizard', 'Weight_Per_DI' , LG_OG:Weight_Per_DI, GLO:Local_INI)
    .
    EXIT
! ---------------------------------------------------
User_DI_Create              ROUTINE
    ! Create DeliveryComposition
    IF Access:DeliveryComposition.PrimeAutoInc() ~= LEVEL:Benign
       MESSAGE('Failed to Prime Delivery Composition Record?', 'Create DI', ICON:Hand)
    ELSE
       DELC:DateAdded   = TODAY()
       DELC:TimeAdded   = CLOCK()
       DELC:Items       = LG_DC:Items
       DELC:Weight      = LG_DC:Weight
       !DELC:Volume      =

       IF Access:DeliveryComposition.Insert() = LEVEL:Benign
          LG_DC:DELCID  = DELC:DELCID
    .  .

    IF LG_DC:DELCID ~= 0
       CLEAR(DEL:Record)
       CLEAR(DELI:Record)

       !IF Access:Deliveries.PrimeAutoInc() ~= LEVEL:Benign
       IF Access:Deliveries.PrimeRecord() ~= LEVEL:Benign
          MESSAGE('Failed to Prime Delivery Record?', 'Create DI', ICON:Hand)
       ELSE
          !IF Access:DeliveryItems.PrimeAutoInc() ~= LEVEL:Benign
          IF Access:DeliveryItems.PrimeRecord() ~= LEVEL:Benign
             Access:Deliveries.CancelAutoInc()
             MESSAGE('Failed to Prime DeliveryItem Record?', 'Create DI', ICON:Hand)
          ELSE
             ! Populate DI fields?
             DEL:DELCID    = LG_DC:DELCID

             ! -------   Item    -------   Create Delivery Item record
             DELI:DID      = DEL:DID
             DELI:ItemNo   = 1

             DELI:CMID     = LG_DC:CMID
             DELI:PTID     = LG_DC:PTID

             DELI:Units    = INT(LG_OG:Weight_Per_DI / LG_CG:Weight_per_Item)
             DELI:Weight   = LG_CG:Weight_per_Item * DELI:Units

             Access:DeliveryItems.Insert()

             ! -------   Delivery    -------   Allow user to complete DI
             GlobalRequest      = InsertRecord
             Update_Deliveries()
             IF GlobalResponse = RequestCancelled
                Access:Deliveries.CancelAutoInc()
                L_CG:Fail       = TRUE
             ELSE
                LG_DC:DID       = DEL:DID

                ! Check that suitable values have been populated on the Item to not exceed the weight max per DI?

                ! Add additional DIs if all well, exactly the same as this DI
                LG_CR:No_DIs    = 1
                DO DI_Create
          .  .

          ! Update Del. Comp.
          DELC:CID         = DEL:CID
          DELC:Deliveries  = LG_CR:No_DIs           ! Actual created
          Access:DeliveryComposition.Update()

          Thread_Send_Procs_Events('Browse_Deliveries', EVENT:User)
    .  .
    EXIT
DI_Create                       ROUTINE
    DATA
R:DID           LIKE(DEL:DID)
R:DINo          LIKE(DEL:DINo)
R:DIID          LIKE(DELI:DIID)
R:Fail          BYTE

R:Tot_Items     LIKE(LG_DC:Items)

    CODE
    db.debugout('[Upd Comp Wiz - DI_Create]  LG_DC:DID: ' & LG_DC:DID)

    ! Get Alias record to copy values from..
    A_DEL:DID       = LG_DC:DID
    IF Access:DeliveriesAlias.TryFetch(A_DEL:PKey_DID) ~= LEVEL:Benign
       R:Fail       = TRUE
       message('failed to get del alias?   A_DEL:DID: ' & A_DEL:DID)
    ELSE
       CLEAR(A_DELI:Record,-1)
       A_DELI:DID   = A_DEL:DID
       SET(A_DELI:FKey_DID_ItemNo, A_DELI:FKey_DID_ItemNo)
       IF Access:DeliveryItemAlias.TryNext() ~= LEVEL:Benign
          R:Fail    = TRUE
       message('failed to get del item alias?   A_DEL:DID: ' & A_DEL:DID)
    .  .

    IF R:Fail = FALSE       ! Create new records
       ! Work out how much you have to create
    db.debugout('[Upd Comp Wiz - DI_Create]  LG_DC:Items: ' & LG_DC:Items & ',   A_DELI:Units: ' & A_DELI:Units)

       R:Tot_Items  = LG_DC:Items
       R:Tot_Items -= A_DELI:Units

       LOOP UNTIL R:Tot_Items <= 0
          CLEAR(DEL:Record)
          CLEAR(DELI:Record)

          !IF Access:Deliveries.PrimeAutoInc() ~= LEVEL:Benign
          IF Access:Deliveries.PrimeRecord() ~= LEVEL:Benign
             MESSAGE('Failed to Prime Delivery Record?', 'Create DI 2', ICON:Hand)
          ELSE
             !IF Access:DeliveryItems.PrimeAutoInc() ~= LEVEL:Benign
             IF Access:DeliveryItems.PrimeRecord() ~= LEVEL:Benign
                Access:Deliveries.CancelAutoInc()
                MESSAGE('Failed to Prime DeliveryItem Record?', 'Create DI 2', ICON:Hand)
             ELSE
    db.debugout('[Upd Comp Wiz - DI_Create]  New - DEL:DID: ' & DEL:DID & ',  DEL:DINo: ' & DEL:DINo & ',  DELI:DIID: ' & DELI:DIID)

                R:DID         = DEL:DID
                R:DINo        = DEL:DINo
                R:DIID        = DELI:DIID

                ! Set Delivery
                DEL:Record   :=: A_DEL:Record
                DEL:DID       = R:DID
                DEL:DINo      = R:DINo

                IF LG_OG:All_Charges_on_1st_DI = TRUE
                   CLEAR(DEL:Charges_Group)
                   CLEAR(DEL:Insure)
                   CLEAR(DEL:TotalConsignmentValue)
                   CLEAR(DEL:InsuranceRate)
                .

                Access:Deliveries.Insert()
                LG_CR:No_DIs += 1

                ! Set DeliveryItems
                DELI:Record  :=: A_DELI:Record
                DELI:DID      = DEL:DID
                DELI:DIID     = R:DIID

                IF R:Tot_Items < INT(LG_OG:Weight_Per_DI / LG_CG:Weight_per_Item)
                   DELI:Units = R:Tot_Items
                ELSE
                   DELI:Units = INT(LG_OG:Weight_Per_DI / LG_CG:Weight_per_Item)
                .

                DELI:Weight   = LG_CG:Weight_per_Item * DELI:Units

                IF Access:DeliveryItems.Insert() = LEVEL:Benign
                   IF LG_OG:All_Charges_on_1st_DI ~= TRUE
                      DEL:Charge               = DELI:Weight * A_DEL:Rate            ! Only 1 item line so weight is total weight

                      IF A_DEL:FuelSurcharge > 0
                         DEL:FuelSurcharge     = DEL:Charge * (Get_Client_FuelSurcharge(DEL:CID, DEL:DIDate) / 100)
                      .

                      IF A_DEL:Insure = TRUE
                         DEL:TotalConsignmentValue  = (A_DEL:TotalConsignmentValue / A_DELI:Units) * DELI:Units
                      .

                      Access:Deliveries.Update()
                .  .

    db.debugout('[Upd Comp Wiz - DI_Create]  DEL:DID: ' & DEL:DID & ',    2. LG_DC:Items: ' & LG_DC:Items & ',   A_DELI:Units: ' & A_DELI:Units)

                R:Tot_Items  -= DELI:Units
       .  .  .
       Thread_Send_Procs_Events('Browse_Deliveries', EVENT:User)
    .
    EXIT
    

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Upd_DeliveryComp_Wizard')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)           ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)           ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Commodities.SetOpenRelated()
  Relate:Commodities.Open                         ! File Commodities used by this procedure, so make sure it's RelationManager is open
  Relate:DeliveriesAlias.Open                     ! File DeliveriesAlias used by this procedure, so make sure it's RelationManager is open
  Access:Deliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItems.UseFile                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItemAlias.UseFile                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryComposition.UseFile              ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,DEL:FKey_DELCID)             ! Add the sort order for DEL:FKey_DELCID for sort order 1
  BRW5.AddRange(DEL:DELCID,LG_DC:DELCID)          ! Add single value range limit for sort order 1
  BRW5.AddLocator(BRW5::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW5::Sort0:Locator.Init(,DEL:DELCID,1,BRW5)    ! Initialize the browse locator using  using key: DEL:FKey_DELCID , DEL:DELCID
  BRW5.AppendOrder('+DEL:DINo')                   ! Append an additional sort order
  BRW5.AddField(DEL:DINo,BRW5.Q.DEL:DINo)         ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW5.AddField(DEL:DIDate,BRW5.Q.DEL:DIDate)     ! Field DEL:DIDate is a hot field or requires assignment from browse
  BRW5.AddField(CLI:ClientName,BRW5.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW5.AddField(DEL:ClientReference,BRW5.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW5.AddField(DEL:Rate,BRW5.Q.DEL:Rate)         ! Field DEL:Rate is a hot field or requires assignment from browse
  BRW5.AddField(DEL:Charge,BRW5.Q.DEL:Charge)     ! Field DEL:Charge is a hot field or requires assignment from browse
  BRW5.AddField(DEL:ClientReference,BRW5.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW5.AddField(DEL:DID,BRW5.Q.DEL:DID)           ! Field DEL:DID is a hot field or requires assignment from browse
  BRW5.AddField(DEL:DELCID,BRW5.Q.DEL:DELCID)     ! Field DEL:DELCID is a hot field or requires assignment from browse
  BRW5.AddField(CLI:CID,BRW5.Q.CLI:CID)           ! Field CLI:CID is a hot field or requires assignment from browse
  INIMgr.Fetch('Upd_DeliveryComp_Wizard',QuickWindow) ! Restore window settings from non-volatile store
      LG_OG:Weight_Per_DI     = GETINI('Upd_DeliveryComp_Wizard', 'Weight_Per_DI' , , GLO:Local_INI)
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW5.AskProcedure = 1                           ! Will call: Update_Deliveries
  FDB6.Init(?LG_DC:Commodity,Queue:FileDrop_Commodity.ViewPosition,FDB6::View:FileDrop,Queue:FileDrop_Commodity,Relate:Commodities,ThisWindow)
  FDB6.Q &= Queue:FileDrop_Commodity
  FDB6.AddSortOrder(COM:Key_Commodity)
  FDB6.AddField(COM:Commodity,FDB6.Q.COM:Commodity) !List box control field - type derived from field
  FDB6.AddField(COM:CMID,FDB6.Q.COM:CMID) !Primary key field - type derived from field
  FDB6.AddUpdateField(COM:CMID,LG_DC:CMID)
  ThisWindow.AddItem(FDB6.WindowComponent)
  FDB6.DefaultFill = 0
  FDB7.Init(?LG_DC:Packaging,Queue:FileDrop_Packaging.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop_Packaging,Relate:PackagingTypes,ThisWindow)
  FDB7.Q &= Queue:FileDrop_Packaging
  FDB7.AddSortOrder(PACK:Key_Packaging)
  FDB7.AddField(PACK:Packaging,FDB7.Q.PACK:Packaging) !List box control field - type derived from field
  FDB7.AddField(PACK:PTID,FDB7.Q.PACK:PTID) !Primary key field - type derived from field
  FDB7.AddUpdateField(PACK:PTID,LG_DC:PTID)
  ThisWindow.AddItem(FDB7.WindowComponent)
  FDB7.DefaultFill = 0
  BRW5.AddToolbarTarget(Toolbar)                  ! Browse accepts toolbar control
  BRW5.ToolbarItem.HelpButton = ?Help
  BRW5::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW5::FormatManager.Init('MANTRNIS','Upd_DeliveryComp_Wizard',1,?List,5,BRW5::PopupTextExt,Queue:Browse,8,LFM_CFile,LFM_CFile.Record)
  BRW5::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Commodities.Close
    Relate:DeliveriesAlias.Close
  END
  ! List Format Manager destructor
  BRW5::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Upd_DeliveryComp_Wizard',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Deliveries
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
          IF L_CG:Wiz_Pos = 4
             CASE MESSAGE('Are you sure you want to cancel?||This will cause the Deliveries and Delivery Combination to be removed.', 'Cancel Wizard', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:Yes
                ! Loop through and remove the DIs (no cascade on the Delivery Composition)
                LOOP
                   DEL:DELCID    = DELC:DELCID
                   IF Access:Deliveries.TryFetch(DEL:FKey_DELCID) ~= LEVEL:Benign
                      BREAK
                   ELSE
                      IF Relate:Deliveries.Delete(0) ~= LEVEL:Benign
                         BREAK
                .  .  .
      
                Relate:DeliveryComposition.Delete(0)
             ELSE
                CYCLE
          .  .
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LG_OG:Weight_Per_DI
          DO Comp_Calcs
    OF ?LG_DC:Weight
          DO Comp_Calcs
    OF ?LG_DC:Items
          DO Comp_Calcs
    OF ?Button_Next
      ThisWindow.Update()
          DO Next_Button
    OF ?Button_Back
      ThisWindow.Update()
          L_CG:Wiz_Pos -= 1
          DO Change_Tab
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LG_DC:Items
          DO Comp_Calcs
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END
  SELF.ViewControl = ?View                                 ! Setup the control used to initiate view only mode


BRW5.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW5::LastSortOrder <> NewOrder THEN
     BRW5::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW5::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  IF BRW5::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW5::PopupTextExt = ''
        BRW5::PopupChoiceExec = True
        BRW5::FormatManager.MakePopup(BRW5::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW5::PopupTextExt = '|-|' & CLIP(BRW5::PopupTextExt)
        END
        BRW5::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW5::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW5::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW5::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW5::PopupChoiceOn AND BRW5::PopupChoiceExec THEN
     BRW5::PopupChoiceExec = False
     BRW5::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW5::PopupTextExt)
     IF BRW5::FormatManager.DispatchChoice(BRW5::PopupChoice)
     ELSE
     END
  END

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update__Container_Rates PROCEDURE (p:Disable_Client)

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
ClientName           STRING(35)                            ! 
Journey              STRING(70)                            ! Description
FDB4::View:FileDrop  VIEW(Clients)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:CID)
                     END
FDB6::View:FileDrop  VIEW(Journeys)
                       PROJECT(JOU:Journey)
                       PROJECT(JOU:JID)
                     END
FDB7::View:FileDrop  VIEW(ContainerTypes)
                       PROJECT(CTYP:ContainerType)
                       PROJECT(CTYP:CTID)
                     END
Queue:FileDrop       QUEUE                            !
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:2     QUEUE                            !
CTYP:ContainerType     LIKE(CTYP:ContainerType)       !List box control field - type derived from field
CTYP:CTID              LIKE(CTYP:CTID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::CORA:Record LIKE(CORA:RECORD),THREAD
QuickWindow          WINDOW('Update the Container Rates File'),AT(,,200,170),FONT('Tahoma',8),DOUBLE,GRAY,IMM,MDI, |
  HLP('Update__RatesContainer'),SYSTEM
                       SHEET,AT(4,4,192,146),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Client:'),AT(9,22),USE(?CORA:ToMass:Prompt:2),TRN
                           LIST,AT(65,24,126,9),USE(ClientName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Client Name~@s35@'), |
  FROM(Queue:FileDrop)
                           PROMPT('Journey:'),AT(9,37),USE(?CORA:ToMass:Prompt:3),TRN
                           BUTTON('...'),AT(46,37,13,10),USE(?Button_Browse_Journey),TIP('Browse Journies')
                           LIST,AT(65,37,126,10),USE(Journey),HVSCROLL,DROP(15,200),FORMAT('280L(2)|M~Journey~@s70@'), |
  FROM(Queue:FileDrop:1)
                           PROMPT('Container Type:'),AT(9,55),USE(?CORA:ToMass:Prompt:4),TRN
                           LIST,AT(65,55,126,10),USE(CTYP:ContainerType),VSCROLL,DROP(15),FORMAT('140L(2)|M~Type~@s35@'), |
  FROM(Queue:FileDrop:2),MSG('Type')
                           PROMPT('To Mass:'),AT(9,82),USE(?CORA:ToMass:Prompt),TRN
                           SPIN(@n-12.0),AT(118,82,71,10),USE(CORA:ToMass),RIGHT(2),MSG('Up to this mass in Kgs'),TIP('Up to this' & |
  ' mass in Kgs')
                           PROMPT('Minimium Charge:'),AT(9,114),USE(?CORA:MinimiumCharge:Prompt),TRN
                           ENTRY(@n-14.2),AT(118,114,71,10),USE(CORA:MinimiumCharge),RIGHT(1)
                           PROMPT('Rate Per Kg:'),AT(9,100),USE(?CORA:RatePerKg:Prompt),TRN
                           ENTRY(@n-11.2),AT(118,100,71,10),USE(CORA:RatePerKg),RIGHT(2),MSG('Rate per Kg'),TIP('Rate per Kg')
                           PROMPT('Effective Date:'),AT(9,134),USE(?CORA:Effective_Date:Prompt),TRN
                           SPIN(@d5),AT(118,134,71,10),USE(CORA:Effective_Date),RIGHT(1),MSG('Effective from date'),REQ, |
  TIP('Effective from date')
                         END
                       END
                       BUTTON('&OK'),AT(84,154,,14),USE(?OK),LEFT,ICON('waok.ico'),DEFAULT,FLAT
                       BUTTON('Cancel'),AT(140,154,,14),USE(?Cancel),LEFT,ICON('wacancel.ico'),FLAT
                       BUTTON('Help'),AT(4,154,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB6                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:2              !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Container Rates Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Container Rates Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update__Container_Rates')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CORA:ToMass:Prompt:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(CORA:Record,History::CORA:Record)
  SELF.AddHistoryField(?CORA:ToMass,5)
  SELF.AddHistoryField(?CORA:MinimiumCharge,7)
  SELF.AddHistoryField(?CORA:RatePerKg,6)
  SELF.AddHistoryField(?CORA:Effective_Date,10)
  SELF.AddUpdateFile(Access:__RatesContainer)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:__RatesContainer
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
      ! Load the Client & Journey locals
      ClientName      = CLI:ClientName
  
      CORA:CID        = CLI:CID
  
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update__Container_Rates',QuickWindow)      ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      IF SELF.Request = InsertRecord
         CORA:Effective_Date  = GETINI('ContainerRates', 'Last_Used_Eff_Date', , '.\TransIS.INI')
      .
  FDB4.Init(?ClientName,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:Clients,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(CLI:Key_ClientName)
  FDB4.AddField(CLI:ClientName,FDB4.Q.CLI:ClientName) !List box control field - type derived from field
  FDB4.AddField(CLI:CID,FDB4.Q.CLI:CID) !Primary key field - type derived from field
  FDB4.AddUpdateField(CLI:CID,CORA:CID)
  ThisWindow.AddItem(FDB4.WindowComponent)
  FDB4.DefaultFill = 0
  FDB6.Init(?Journey,Queue:FileDrop:1.ViewPosition,FDB6::View:FileDrop,Queue:FileDrop:1,Relate:Journeys,ThisWindow)
  FDB6.Q &= Queue:FileDrop:1
  FDB6.AddSortOrder(JOU:Key_Journey)
  FDB6.AddField(JOU:Journey,FDB6.Q.JOU:Journey) !List box control field - type derived from field
  FDB6.AddField(JOU:JID,FDB6.Q.JOU:JID) !Primary key field - type derived from field
  FDB6.AddUpdateField(JOU:JID,CORA:JID)
  ThisWindow.AddItem(FDB6.WindowComponent)
  FDB6.DefaultFill = 0
  FDB7.Init(?CTYP:ContainerType,Queue:FileDrop:2.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop:2,Relate:ContainerTypes,ThisWindow)
  FDB7.Q &= Queue:FileDrop:2
  FDB7.AddSortOrder(CTYP:Key_Type)
  FDB7.AddField(CTYP:ContainerType,FDB7.Q.CTYP:ContainerType) !List box control field - type derived from field
  FDB7.AddField(CTYP:CTID,FDB7.Q.CTYP:CTID) !Primary key field - type derived from field
  FDB7.AddUpdateField(CTYP:CTID,CORA:CTID)
  ThisWindow.AddItem(FDB7.WindowComponent)
      IF p:Disable_Client = TRUE
         DISABLE(?ClientName)
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update__Container_Rates',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Browse_Journey
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Journey()
      ThisWindow.Reset
          Journey     = JOU:Journey
          CORA:JID    = JOU:JID
          DISPLAY
      FDB6.ResetQueue(1)
          JOU:Journey = Journey
          JOU:JID     = CORA:JID
          DISPLAY
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Print_Debtor_Age_Analysis_Window PROCEDURE (p_Option)

LOC:Options          GROUP,PRE(L_OG)                       ! 
Statement_Option     BYTE(2)                               ! 
STRID                ULONG                                 ! Statement Run ID
Statement_Run_Info   STRING(50)                            ! 
Run_Date             DATE                                  ! Run with effective date of
MonthEndDay          BYTE(25)                              ! Calculate the aging based on this month day
Ignore_Zero_Balance  BYTE(1)                               ! Ignore debtors / clients that have a zero balance
Output_Option        BYTE                                  ! 
SingleClient         BYTE                                  ! 
CID                  ULONG                                 ! Client ID
ClientName           STRING(100)                           ! 
Only_Show_Over_Credit_Limit BYTE                           ! Only show Clients that are over their credit limit
Status_Byte          BYTE(255)                             ! Normal, On Hold, Closed, Dormant
Client_All           BYTE                                  ! 
Client_Normal        BYTE                                  ! 
Client_OnHold        BYTE                                  ! 
Client_Closed        BYTE                                  ! 
Client_Dormant       BYTE                                  ! 
Print_Option         BYTE                                  ! 
BID                  ULONG                                 ! Branch ID
BranchName           STRING(35)                            ! Branch Name
BID_FirstTime        BYTE(1)                               ! 
                     END                                   ! 
FDB5::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop_Branch QUEUE                           !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('Debitor Age Analysis'),AT(,,335,255),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,MDI
                       SHEET,AT(3,4,329,230),USE(?Sheet1)
                         TAB('Options'),USE(?Tab1)
                           GROUP,AT(10,22,195,35),USE(?Group_Date)
                             PROMPT('Run Date:'),AT(10,22),USE(?Run_Date:Prompt),TRN
                             SPIN(@d6b),AT(79,22,68,10),USE(L_OG:Run_Date),RIGHT(1),MSG('Run with effective date of'),TIP('Run with e' & |
  'ffective date of')
                             BUTTON('...'),AT(151,22,12,10),USE(?Calendar)
                             PROMPT('Month End Day:'),AT(10,40),USE(?MonthEndDay:Prompt),TRN
                             SPIN(@n3),AT(79,40,68,10),USE(L_OG:MonthEndDay),RIGHT(1),MSG('Calculate the aging based' & |
  ' on this month day'),TIP('Calculate the aging based on this month day')
                           END
                           CHECK(' &Ignore Zero Balance'),AT(79,62),USE(L_OG:Ignore_Zero_Balance),MSG('Ignore debt' & |
  'ors / clients that have a zero balance'),TIP('Ignore debtors / clients that have a z' & |
  'ero balance'),TRN
                           CHECK(' Only show Clients over their Credit Limit'),AT(79,76),USE(L_OG:Only_Show_Over_Credit_Limit), |
  MSG('Only show Clients that are over their credit limit'),TIP('Only show Clients that' & |
  ' are over their credit limit'),TRN
                           PROMPT('Client Statuses:'),AT(10,99),USE(?Status:Prompt:3),TRN
                           CHECK(' &All'),AT(79,99),USE(L_OG:Client_All),TRN
                           GROUP,AT(115,94,200,20),USE(?Group_Client_Status),BEVEL(1,-1),BOXED
                             CHECK(' Normal'),AT(125,99),USE(L_OG:Client_Normal),TRN
                             CHECK(' On Hold'),AT(169,99),USE(L_OG:Client_OnHold),TRN
                             CHECK(' Closed'),AT(217,99),USE(L_OG:Client_Closed),TRN
                             CHECK(' Dormant'),AT(259,99),USE(L_OG:Client_Dormant),TRN
                           END
                           PROMPT('Branch:'),AT(10,122),USE(?Status:Prompt:2)
                           LIST,AT(79,122,68,10),USE(L_OG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop_Branch)
                           CHECK(' &Single Client'),AT(79,146),USE(L_OG:SingleClient),TIP('To run for a single cli' & |
  'ent click here'),TRN
                           GROUP,AT(10,168,276,10),USE(?Group_Client)
                             PROMPT('To run for a single client specify the client below.'),AT(79,157,207,10),USE(?Prompt7), |
  FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                             PROMPT('Client Name:'),AT(10,168),USE(?ClientName:Prompt),TRN
                             BUTTON('...'),AT(62,168,12,10),USE(?CallLookup)
                             ENTRY(@s100),AT(79,168,207,10),USE(L_OG:ClientName),REQ,TIP('Client name')
                           END
                           PROMPT('Print Option:'),AT(200,218),USE(?Print_Option:Prompt),HIDE,TRN
                           LIST,AT(246,217,68,10),USE(L_OG:Print_Option),DROP(5),FROM('Printer|#0|PDF|#1'),HIDE
                           PROMPT('Log Output Option:'),AT(10,218),USE(?Output_Option:Prompt),TRN
                           LIST,AT(79,218,68,10),USE(L_OG:Output_Option),VSCROLL,DROP(15),FROM('None|#0|All|#1|Cur' & |
  'rent|#2|30 Days|#3|60 Days|#4|90 Days|#5'),TIP('Output details for the following bal' & |
  'ances<0DH,0AH>These are log file output options only')
                         END
                         TAB('Additional Options'),USE(?Tab2)
                           GROUP,AT(10,46,276,28),USE(?Group_SRun)
                             PROMPT('Statement Run Info:'),AT(10,46),USE(?L_OG:Statement_Run_Info:Prompt),TRN
                             ENTRY(@s50),AT(79,46,207,10),USE(L_OG:Statement_Run_Info),COLOR(00E9E9E9h),READONLY,SKIP
                             BUTTON('Select Statement &Run'),AT(79,60,,14),USE(?Button_Select_Statement)
                           END
                           PROMPT('Statement Option:'),AT(10,24),USE(?Statement_Option:Prompt),TRN
                           LIST,AT(79,24,94,10),USE(L_OG:Statement_Option),DROP(5),FROM('All|#0|Last Statement|#1|None|#2'), |
  TIP('All - all Debtors on printout with or without statements<0DH,0AH>Last Statement ' & |
  '- Debtors who had a statement generated last run')
                         END
                       END
                       BUTTON('&OK'),AT(218,238,55,14),USE(?OkButton),LEFT,ICON('waok.ico'),DEFAULT,FLAT
                       BUTTON('&Cancel'),AT(276,238,,14),USE(?CancelButton),LEFT,ICON('wacancel.ico'),FLAT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Calendar3            CalendarClass
FDB5                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop_Branch         !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Debtor_Age_Analysis_Window')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Run_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Access:_Statement_Runs.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
      L_OG:Client_Normal  = GETINI('Print_Debtor_Age_Analysis_Window', 'Client_Normal', 1, GLO:Local_INI)
      L_OG:Client_OnHold  = GETINI('Print_Debtor_Age_Analysis_Window', 'Client_OnHold', 1, GLO:Local_INI)
      L_OG:Client_Closed  = GETINI('Print_Debtor_Age_Analysis_Window', 'Client_Closed', 1, GLO:Local_INI)
      L_OG:Client_Dormant = GETINI('Print_Debtor_Age_Analysis_Window', 'Client_Dormant', 1, GLO:Local_INI)
  
      IF L_OG:Client_Normal + L_OG:Client_OnHold + L_OG:Client_Closed + L_OG:Client_Dormant = 4
         L_OG:Client_All  = TRUE
      .
  INIMgr.Fetch('Print_Debtor_Age_Analysis_Window',Window)  ! Restore window settings from non-volatile store
      IF p_Option = '1'
         L_OG:Only_Show_Over_Credit_Limit = TRUE
      .
  IF ?L_OG:Only_Show_Over_Credit_Limit{Prop:Checked}
    DISABLE(?Group_Client)
  END
  IF NOT ?L_OG:Only_Show_Over_Credit_Limit{PROP:Checked}
    ENABLE(?Group_Client)
  END
  IF ?L_OG:Client_All{Prop:Checked}
    DISABLE(?Group_Client_Status)
  END
  IF NOT ?L_OG:Client_All{PROP:Checked}
    ENABLE(?Group_Client_Status)
  END
  IF ?L_OG:SingleClient{Prop:Checked}
    ENABLE(?Group_Client)
  END
  IF NOT ?L_OG:SingleClient{PROP:Checked}
    DISABLE(?Group_Client)
  END
  FDB5.Init(?L_OG:BranchName,Queue:FileDrop_Branch.ViewPosition,FDB5::View:FileDrop,Queue:FileDrop_Branch,Relate:Branches,ThisWindow)
  FDB5.Q &= Queue:FileDrop_Branch
  FDB5.AddSortOrder(BRA:Key_BranchName)
  FDB5.AddField(BRA:BranchName,FDB5.Q.BRA:BranchName) !List box control field - type derived from field
  FDB5.AddField(BRA:BID,FDB5.Q.BRA:BID) !Primary key field - type derived from field
  FDB5.AddUpdateField(BRA:BID,L_OG:BID)
  ThisWindow.AddItem(FDB5.WindowComponent)
      ! Get latest run info
      SET(STAR:Key_DateTime)
      NEXT(_Statement_Runs)
      IF ~ERRORCODE()
         L_OG:STRID               = STAR:STRID
         L_OG:Statement_Run_Info  = 'Run on ' & FORMAT(STAR:RunDate,@d5) & ' @ ' & FORMAT(STAR:RunTime,@t4) & ' for date ' & FORMAT(STAR:EntryDate,@d5)
      .
      L_OG:Run_Date       = TODAY()
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Debtor_Age_Analysis_Window',Window) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
          ! Save settings...
          PUTINI('Print_Debtor_Age_Analysis_Window', 'Client_Normal', L_OG:Client_Normal, GLO:Local_INI)
          PUTINI('Print_Debtor_Age_Analysis_Window', 'Client_OnHold', L_OG:Client_OnHold, GLO:Local_INI)
          PUTINI('Print_Debtor_Age_Analysis_Window', 'Client_Closed', L_OG:Client_Closed, GLO:Local_INI)
          PUTINI('Print_Debtor_Age_Analysis_Window', 'Client_Dormant', L_OG:Client_Dormant, GLO:Local_INI)
      
      
          L_OG:Status_Byte    = 0
      
          IF L_OG:Client_Normal = TRUE
             L_OG:Status_Byte     = BOR(L_OG:Status_Byte, 0001b)
          .
          IF L_OG:Client_OnHold = TRUE
             L_OG:Status_Byte     = BOR(L_OG:Status_Byte, 0010b)
          .
          IF L_OG:Client_Closed = TRUE
             L_OG:Status_Byte     = BOR(L_OG:Status_Byte, 0100b)
          .
          IF L_OG:Client_Dormant = TRUE
             L_OG:Status_Byte     = BOR(L_OG:Status_Byte, 1000b)
          .
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar3.SelectOnClose = True
      Calendar3.Ask('Select a Date',L_OG:Run_Date)
      IF Calendar3.Response = RequestCompleted THEN
      L_OG:Run_Date=Calendar3.SelectedDate
      DISPLAY(?L_OG:Run_Date)
      END
      ThisWindow.Reset(True)
    OF ?L_OG:Only_Show_Over_Credit_Limit
      IF ?L_OG:Only_Show_Over_Credit_Limit{PROP:Checked}
        DISABLE(?Group_Client)
      END
      IF NOT ?L_OG:Only_Show_Over_Credit_Limit{PROP:Checked}
        ENABLE(?Group_Client)
      END
      ThisWindow.Reset()
    OF ?L_OG:Client_All
      IF ?L_OG:Client_All{PROP:Checked}
        DISABLE(?Group_Client_Status)
      END
      IF NOT ?L_OG:Client_All{PROP:Checked}
        ENABLE(?Group_Client_Status)
      END
      ThisWindow.Reset()
          L_OG:Client_Normal  = 1
          L_OG:Client_OnHold  = 1
          L_OG:Client_Closed  = 1
          L_OG:Client_Dormant = 1
          DISPLAY
    OF ?L_OG:SingleClient
      IF ?L_OG:SingleClient{PROP:Checked}
        ENABLE(?Group_Client)
      END
      IF NOT ?L_OG:SingleClient{PROP:Checked}
        DISABLE(?Group_Client)
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = L_OG:ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_OG:ClientName = CLI:ClientName
        L_OG:CID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?L_OG:ClientName
      IF NOT Window{PROP:AcceptAll}
        IF L_OG:ClientName OR ?L_OG:ClientName{PROP:Req}
          CLI:ClientName = L_OG:ClientName
          IF Access:Clients.TryFetch(CLI:Key_ClientName)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              L_OG:ClientName = CLI:ClientName
              L_OG:CID = CLI:CID
            ELSE
              CLEAR(L_OG:CID)
              SELECT(?L_OG:ClientName)
              CYCLE
            END
          ELSE
            L_OG:CID = CLI:CID
          END
        END
      END
      ThisWindow.Reset()
    OF ?Button_Select_Statement
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_StatementRuns()
      ThisWindow.Reset
          IF GlobalResponse = RequestCompleted
             L_OG:STRID               = STAR:STRID
             L_OG:Statement_Run_Info  = 'Run on ' & FORMAT(STAR:RunDate,@d5) & ' @ ' & FORMAT(STAR:RunTime,@t4) & ' for date ' & FORMAT(STAR:EntryDate,@d5)
          .
      
    OF ?L_OG:Statement_Option
          IF L_OG:Statement_Option = 2
             ENABLE(?Group_Date)
             DISABLE(?Group_SRun)
          ELSE
             DISABLE(?Group_Date)
             ENABLE(?Group_SRun)
          .
    OF ?OkButton
      ThisWindow.Update()
         ThisWindow.Update
         
          IF L_OG:Statement_Option < 2
             ! (p:SRID, p:Option, p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:CID)
             ! (p:SRID, p:Option, p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:CID, p:Credit_Limit_Option, p:ClientStatus)
             !     1       2           3           4           5                 6       7
             EXECUTE L_OG:Print_Option + 1
                Print_Debtor_Age_Analysis(L_OG:STRID, L_OG:Statement_Option,, L_OG:MonthEndDay, L_OG:Ignore_Zero_Balance, L_OG:Output_Option, L_OG:CID, L_OG:Only_Show_Over_Credit_Limit, L_OG:Status_Byte, L_OG:BID)
                Print_Debtor_Age_Analysis_PDF(L_OG:STRID, L_OG:Statement_Option,, L_OG:MonthEndDay, L_OG:Ignore_Zero_Balance, L_OG:Output_Option, L_OG:CID, L_OG:Only_Show_Over_Credit_Limit, L_OG:Status_Byte, L_OG:BID)
             .
          ELSE
             EXECUTE L_OG:Print_Option + 1
                Print_Debtor_Age_Analysis(0, L_OG:Statement_Option, L_OG:Run_Date, L_OG:MonthEndDay, L_OG:Ignore_Zero_Balance, L_OG:Output_Option, L_OG:CID, L_OG:Only_Show_Over_Credit_Limit, L_OG:Status_Byte, L_OG:BID)
                Print_Debtor_Age_Analysis_PDF(0, L_OG:Statement_Option, L_OG:Run_Date, L_OG:MonthEndDay, L_OG:Ignore_Zero_Balance, L_OG:Output_Option, L_OG:CID, L_OG:Only_Show_Over_Credit_Limit, L_OG:Status_Byte, L_OG:BID)
          .  .
      
          ThisWindow.Reset
      
    OF ?CancelButton
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_OG:Statement_Option
          IF L_OG:Statement_Option = 2
             ENABLE(?Group_Date)
             DISABLE(?Group_SRun)
          ELSE
             DISABLE(?Group_Date)
             ENABLE(?Group_SRun)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


FDB5.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
        Queue:FileDrop_Branch.BRA:BranchName       = 'All'
        GET(Queue:FileDrop_Branch, Queue:FileDrop_Branch.BRA:BranchName)
        IF ERRORCODE()
           CLEAR(Queue:FileDrop_Branch)
           Queue:FileDrop_Branch.BRA:BranchName    = 'All'
           Queue:FileDrop_Branch.BRA:BID           = 0
           ADD(Queue:FileDrop_Branch,1)
        .
    
    
        IF L_OG:BID_FirstTime = TRUE
           !SELECT(?L_OG:BranchName, 1)        !RECORDS(Queue:FileDrop_Branch))
    
           L_OG:BID           = 0
           L_OG:BranchName    = 'All'
        .
    
        L_OG:BID_FirstTime     = FALSE
  RETURN ReturnValue

    
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
DIs_Processed PROCEDURE 

LOC:Options          GROUP,PRE(L_OG)                       ! 
Earliest_Date        DATE                                  ! Earliest date to check from
Latest_Date          DATE                                  ! Latest Date to check to
Auto_Update          BYTE                                  ! Auto update screen every this often
Use_Dates            BYTE                                  ! DI Date / Created Date
Run_Period           BYTE                                  ! Run for this period
Last_Ran             LONG                                  ! 
                     END                                   ! 
LOC:DIs_Q            QUEUE,PRE(L_DQ)                       ! 
Branch               STRING(35)                            ! Branch Name
CreatedDate          DATE                                  ! Entry created on
NumberDIs            ULONG                                 ! Number of DI's
NumberClients        ULONG                                 ! Number of unique Clients
Weight               DECIMAL(12,1)                         ! Total Weight
Units                ULONG                                 ! Units
BID                  ULONG                                 ! Branch ID
                     END                                   ! 
LOC:Problem_DI_Q     QUEUE,PRE(L_DQP)                      ! 
DINo                 ULONG                                 ! Delivery Instruction Number
DIDate               DATE                                  ! DI Date
Comment              STRING(100)                           ! 
DID                  ULONG                                 ! Delivery ID
                     END                                   ! 
LOC:Timer_Group      GROUP,PRE(L_TG)                       ! 
Complete             BYTE                                  ! 
Complete_Items       BYTE                                  ! 
Loops                LONG                                  ! 
Time_In              LONG                                  ! 
Idx                  LONG                                  ! 
                     END                                   ! 
LOC:DateWord         CSTRING(50)                           ! 
ClickedOnPointName   STRING(255)                           ! 
ClickedOnPointNumber REAL                                  ! 
ClickedOnSetNumber   LONG                                  ! 
LOC:Graph_Q          QUEUE,PRE(L_GQ)                       ! 
CreatedDate          DATE                                  ! 
Weight_BID1          DECIMAL(12,1)                         ! In kg's
Weight_BID2          DECIMAL(12,1)                         ! In kg's
No                   ULONG                                 ! 
                     END                                   ! 
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q1
ThisGraph4    Class(GlobalInsight)
ValidateRecord  PROCEDURE(long graphID),Long ,VIRTUAL
SetPointName    PROCEDURE (Long graphSet),String ,VIRTUAL
Reset           PROCEDURE (Byte graphForce=0),VIRTUAL
              End
ThisGraph4:Popup  Class(PopupClass)
               End
ThisGraph4:ClickedOnPointName   String(255)
ThisGraph4:ClickedOnPointNumber Real
ThisGraph4:ClickedOnSetNumber   Long
ThisGraph4:Color                Group(iColorGroupType), PRE(ThisGraph4:Color)
                                End
ThisGraph4:FileName              String(255)
!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
Window               WINDOW('DI Statistics'),AT(,,326,260),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,MAX, |
  MDI,TIMER(1000),IMM
                       GROUP,AT(4,247,211,10),USE(?Group_BottomLeft)
                         PROMPT('Last Ran:'),AT(4,247),USE(?L_OG:Last_Ran:Prompt)
                         STRING(@T4b),AT(38,247),USE(L_OG:Last_Ran),RIGHT(1)
                         STRING(''),AT(112,247,137,10),USE(?String_DI)
                       END
                       SHEET,AT(4,4,318,238),USE(?Sheet1)
                         TAB('&1) Options'),USE(?Tab3)
                           GROUP,AT(11,21,223,152),USE(?Group_Options)
                             PROMPT('Use Dates:'),AT(15,29),USE(?Use_Dates:Prompt),TRN
                             LIST,AT(71,29,76,10),USE(L_OG:Use_Dates),DROP(5),FROM('DI Date|#0|Created Date|#1'),MSG('Created Da' & |
  'te / DI Date'),TIP('Created Date / DI Date')
                             GROUP,AT(15,73,188,23),USE(?Group_Dates)
                               PROMPT('Earliest Date:'),AT(15,73),USE(?Earliest_Date:Prompt),TRN
                               SPIN(@d6b),AT(71,73,60,10),USE(L_OG:Earliest_Date),RIGHT(2),MSG('Earliest date to check from'), |
  TIP('Earliest date to check from<0DH,0AH>Leave zero for all dates')
                               BUTTON('...'),AT(135,73,12,10),USE(?Calendar)
                               BUTTON('Clear'),AT(159,73,45,10),USE(?Button_Clear)
                               PROMPT('Latest Date:'),AT(15,86),USE(?Latest_Date:Prompt),TRN
                               SPIN(@d6b),AT(71,86,60,10),USE(L_OG:Latest_Date),RIGHT(2),MSG('Latest Date to check to'),TIP('Latest Dat' & |
  'e to check to<0DH,0AH>Leave zero for all dates')
                               BUTTON('...'),AT(135,86,12,10),USE(?Calendar:2)
                               BUTTON('Clear'),AT(159,86,45,10),USE(?Button_Clear:2)
                             END
                             PROMPT('Auto Update:'),AT(15,109),USE(?L_OG:Auto_Update:Prompt),TRN
                             LIST,AT(71,109,76,10),USE(L_OG:Auto_Update),DROP(15),FROM('None|#0|30 Seconds|#30|Minut' & |
  'e|#1|2 Minutes|#2|5 Minutes|#5|10 Minutes|#10'),MSG('Auto update screen every this often'), |
  TIP('Auto update screen every this often')
                             PROMPT('Run Period:'),AT(15,47),USE(?L_OG:Run_Period:Prompt),TRN
                             LIST,AT(71,47,76,10),USE(L_OG:Run_Period),DROP(5),FROM('Set From - To|#0|Last 7 Days|#1' & |
  '|This Month|#2'),MSG('Run for this period'),TIP('Run for this period')
                           END
                           BUTTON('&Run'),AT(273,224,45,14),USE(?OkButton),LEFT,ICON('waok.ico'),DEFAULT,FLAT
                         END
                         TAB('&2) General'),USE(?Tab_General)
                           LIST,AT(9,22,307,215),USE(?List_DIs),VSCROLL,FORMAT('50L(2)|M~Branch~@s35@50R(2)|M~Crea' & |
  'ted Date~L@d6b@52R(2)|M~Number DI''s~L@n13@52R(2)|M~Number Clients (unique per day)~' & |
  'L@n13@56R(2)|M~Weight~L@n-14.1@52R(2)|M~Units~L@n13@'),FROM(LOC:DIs_Q)
                         END
                         TAB('&3) Problem DI''s'),USE(?Tab2)
                           LIST,AT(9,21,255,217),USE(?List2),HVSCROLL,FORMAT('50R(2)|M~DI No.~L@n_10@50R(2)|M~DI D' & |
  'ate~L@d6b@100L(2)|M~Comment~@s100@40R(2)|M~DID~L@n_10@'),FROM(LOC:Problem_DI_Q)
                         END
                         TAB('&4) Graph'),USE(?Tab4)
                           SHEET,AT(7,21,311,219),USE(?Sheet2)
                             TAB('Graph'),USE(?Tab5)
                               REGION,AT(10,38,303,198),USE(?Insight),BEVEL(1,-1),IMM
                             END
                             TAB('Data'),USE(?Tab6)
                               LIST,AT(10,38,302,198),USE(?List6),VSCROLL,FORMAT('60R(2)|M~Created Date~L@d5@100R(2)|M' & |
  '~DBN~L@n-17.1@100R(2)|M~JHB~L@n-17.1@52R(2)|M~No.~L@n13@'),FROM(LOC:Graph_Q)
                             END
                           END
                         END
                       END
                       BUTTON('&Cancel'),AT(267,244,,14),USE(?CancelButton),LEFT,ICON('wacancel.ico'),FLAT
                     END
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q2

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Calendar2            CalendarClass
Calendar3            CalendarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

sql_        SQLQueryClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_DIs_Q              ROUTINE
    L_TG:Complete   = FALSE

    CASE L_OG:Use_Dates
    OF 0
       ?List_DIs{PROP:Format}  = '50L(2)|M~Branch~@s35@50R(2)|M~DI Date~L@d6b@52R(2)|M~Number DI''s~L@n13@52R(' & |
                              '2)|M~Number Clients (unique per day)~L@n13@56R(2)|M~Weight~L@n-14.1@52R(2)|M~Uni'  & |
                              'ts~L@n13@'
    OF 1
       ?List_DIs{PROP:Format}  = '50L(2)|M~Branch~@s35@50R(2)|M~Created Date~L@d6b@52R(2)|M~Number DI''s~L@n13@52R(' & |
                              '2)|M~Number Clients (unique per day)~L@n13@56R(2)|M~Weight~L@n-14.1@52R(2)|M~Uni'  & |
                              'ts~L@n13@'
    .

    FREE(LOC:DIs_Q)
    FREE(LOC:Problem_DI_Q)

    ! For all branches
    CLEAR(BRA:Record)
    SET(BRA:Key_BranchName)

    DO Next_Branch
    IF L_TG:Complete ~= TRUE
       ?OkButton{PROP:Text} = '&Stop'

       Window{PROP:Timer}   = 10
       SETCURSOR(CURSOR:Wait)
    ELSE
       DO Process_Complete
    .
    EXIT
Next_Branch             ROUTINE
    IF Access:Branches.TryNext() ~= LEVEL:Benign
       L_TG:Complete    = TRUE
    ELSE
       DO Process_Branch
       IF RECORDS(LOC:DIs_Q) <= 0
          L_TG:Complete    = TRUE
    .  .
    EXIT
Process_Branch         ROUTINE
    DATA
R:Where             STRING(150)

    CODE
    L_TG:Idx                = 0
    L_TG:Loops              = 100

    CLEAR(LOC:DIs_Q)

    EXECUTE L_OG:Use_Dates + 1
       LOC:DateWord           = 'DIDateAndTime'
       LOC:DateWord           = 'CreatedDateTime'
    .

    ! Get all dates
    CLEAR(R:Where)
    SET(_SQLTemp)
    IF L_OG:Earliest_Date > 0
       R:Where              = CLIP(LOC:DateWord) & ' > <39>' & FORMAT(L_OG:Earliest_Date-1,@d8) & '<39>'
    .
    IF L_OG:Latest_Date > 0
       IF CLIP(R:Where) ~= ''
          R:Where           = CLIP(R:Where) & ' AND ' & LOC:DateWord & ' < <39>' & FORMAT(L_OG:Latest_Date+1,@d8) & '<39>'
       ELSE
          R:Where           = LOC:DateWord & ' < <39>' & FORMAT(L_OG:Latest_Date+1,@d8) & '<39>'
    .  .
    IF CLIP(R:Where) ~= ''
       R:Where              = CLIP(R:Where) & ' AND BID = ' & BRA:BID
    ELSE
       R:Where              = 'BID = ' & BRA:BID
    .


    R:Where                 = ' WHERE ' & CLIP(R:Where)
     

    _SQLTemp{PROP:SQL}      = 'SELECT DISTINCT <39>1<39>, <39>1<39>, ' & |
          'CAST(CAST(DATEPART(yy, ' & LOC:DateWord & ') AS varchar) + <39>-<39> + ' & |
          'CAST(DATEPART(mm, ' & LOC:DateWord & ') AS varchar) + <39>-<39> + ' & |
          'CAST(DATEPART(dd, ' & LOC:DateWord & ') AS varchar) AS datetime) ' & |
                              ' FROM Deliveries ' & CLIP(R:Where)
    !  & ' ORDER BY CreatedDateTime'
    IF ERRORCODE()
       MESSAGE('SQL Error: ' & CLIP(ERROR()) & '||F Err: ' & FILEERROR() & '||SQL: ' & _SQLTemp{PROP:SQL}, 'DIs Processed - 1', ICON:Hand)
    .
    LOOP
       NEXT(_SQLTemp)
       IF ERRORCODE()
          BREAK
       .

       L_DQ:Branch          = BRA:BranchName
       L_DQ:BID             = BRA:BID
       L_DQ:CreatedDate     = _SQ:SDate
       ADD(LOC:DIs_Q)
    .
    SORT(LOC:DIs_Q, L_DQ:Branch, L_DQ:CreatedDate)
    EXIT
Process_Entry             ROUTINE
    DATA
R:Weight            LIKE(DELI:Weight)
R:Units             LIKE(DELI:Units)

    CODE
    L_TG:Complete_Items   = FALSE

    L_TG:Idx    += 1
    GET(LOC:DIs_Q, L_TG:Idx)
    IF ERRORCODE()
       L_TG:Complete_Items  = TRUE
    ELSIF CLIP(L_DQ:Branch) = CLIP(BRA:BranchName)
       ?String_DI{PROP:Text} = 'Searching date: ' & FORMAT(L_DQ:CreatedDate,@d5)

       ! Get number of DIs
       _SQLTemp{PROP:SQL}    = 'SELECT COUNT(*) FROM Deliveries WHERE ' & LOC:DateWord & ' BETWEEN <39>' & FORMAT(L_DQ:CreatedDate,@d8) & '<39> AND <39>' & FORMAT(L_DQ:CreatedDate + 1,@d8) & '<39> AND BID = ' & BRA:BID
       NEXT(_SQLTemp)
       IF ~ERRORCODE()
          L_DQ:NumberDIs     = _SQ:S1
       .

       ! Get number of Clients
       _SQLTemp{PROP:SQL}    = 'SELECT COUNT(DISTINCT CID) FROM Deliveries WHERE ' & LOC:DateWord & ' BETWEEN <39>' & FORMAT(L_DQ:CreatedDate,@d8) & '<39> AND <39>' & FORMAT(L_DQ:CreatedDate + 1,@d8) & '<39> AND BID = ' & BRA:BID
       IF ERRORCODE()
          MESSAGE('SQL Error: ' & CLIP(ERROR()) & '||F Err: ' & FILEERROR() & '||SQL: ' & _SQLTemp{PROP:SQL}, 'DIs Processed', ICON:Hand)
       .
       NEXT(_SQLTemp)
       IF ~ERRORCODE()
          L_DQ:NumberClients = _SQ:S1
       .

       ! Get total weight
       sql_.PropSQL('SELECT DISTINCT(d.DID), SUM(di.WEIGHT), SUM(di.UNITS) FROM Deliveries AS d ' & | 
              ' JOIN DeliveryItems as di ON di.DID = d.DID ' & |
              ' WHERE ' & LOC:DateWord & ' BETWEEN <39>' & FORMAT(L_DQ:CreatedDate,@d8) & '<39> AND <39>' & | 
              FORMAT(L_DQ:CreatedDate + 1,@d8) & '<39> AND BID = ' & BRA:BID & |
              ' GROUP BY d.did')
       IF ERRORCODE()
          MESSAGE('SQL Error: ' & CLIP(ERROR()) & '||F Err: ' & FILEERROR() & '||SQL: ' & _SQLTemp{PROP:SQL}, 'DIs Processed', ICON:Hand)
       .
       LOOP          
          IF sql_.Next_Q() <= 0
             BREAK
          .
          
          ! 4 July 12 - sums added to query and so changes done here too
          !R:Weight           = Get_DelItem_s_Totals(DEFORMAT(_SQ:S1), 6) / 100  ! Returns a Ulong
          !R:Units            = Get_DelItem_s_Totals(DEFORMAT(_SQ:S1), 3)        ! Get items (used in del comb)
          
          !MESSAGE('_SQ:S2: ' &  sql_.Data_G.F2 & '||_SQ:S3: ' & sql_.Data_G.F3)          
          
          R:Weight            = DEFORMAT(sql_.Data_G.F2)
          R:Units             = DEFORMAT(sql_.Data_G.F3)

          L_DQ:Weight       += R:Weight
          L_DQ:Units        += R:Units
       .
      
       PUT(LOC:DIs_Q)

       ! 4 July 12 - Was in the loop but now agregates in there.
       DO Check_noweight_nounits
    .
    EXIT
Check_noweight_nounits      ROUTINE
  DATA
R:Weight            LIKE(DELI:Weight)
R:Units             LIKE(DELI:Units)

  CODE
  sql_.PropSQL( 'SELECT DISTINCT d.DID, di.WEIGHT, di.UNITS FROM Deliveries AS d ' & | 
              ' JOIN DeliveryItems as di ON di.DID = d.DID ' & |
              ' WHERE ' & LOC:DateWord & ' BETWEEN <39>' & FORMAT(L_DQ:CreatedDate,@d8) & '<39> AND <39>' & | 
              FORMAT(L_DQ:CreatedDate + 1,@d8) & '<39> AND BID = ' & BRA:BID & |
              ' AND (di.WEIGHT <= 0.0 OR di.UNITS <= 0.0)')
  IF ERRORCODE()
     MESSAGE('SQL Error: ' & CLIP(ERROR()) & '||F Err: ' & FILEERROR(), 'DIs Processed - No Weight Units', ICON:Hand)
  .
  LOOP
    IF sql_.Next_Q() <= 0     
       BREAK
    .
   
    R:Weight   = DEFORMAT(sql_.Data_G.F2)
    R:Units    = DEFORMAT(sql_.Data_G.F3)
   
    IF R:Weight <= 0.0 OR R:Units <= 0
      DEL:DID         = sql_.Data_G.F1
      IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
        L_DQP:DINo      = DEL:DINo
        L_DQP:DIDate    = DEL:DIDate
        L_DQP:DID       = DEL:DID

        IF R:Weight = 0.0
           L_DQP:Comment   = 'Weight is Zero'
        ELSE
           L_DQP:Comment   = 'Units is Zero'
        .

        ADD(LOC:Problem_DI_Q)
  . . .
    
  EXIT
        
Process_Complete        ROUTINE
    SORT(LOC:Problem_DI_Q, L_DQP:DINo, L_DQP:DIDate)

    SETCURSOR()
    L_OG:Last_Ran = CLOCK()
    DISPLAY(?L_OG:Last_Ran)

    ?String_DI{PROP:Text} = ''

    DO Load_GQ
    EXIT
Set_Dates                   ROUTINE
    CASE L_OG:Run_Period        ! Set From - To|Last 7 Days|This Month
    OF 0

    OF 1
       L_OG:Earliest_Date   = TODAY() - 6
       L_OG:Latest_Date     = TODAY()
    OF 2
       L_OG:Earliest_Date   = DATE(MONTH(TODAY()), 1, YEAR(TODAY()))
       L_OG:Latest_Date     = DATE(MONTH(TODAY()) + 1, 1, YEAR(TODAY())) - 1
    .
    DISPLAY
    EXIT
! -----------------------------------------------------------------------------------------
Load_GQ                     ROUTINE
    FREE(LOC:Graph_Q)
    L_TG:Idx        = 0

    LOOP
       L_TG:Idx    += 1
       GET(LOC:DIs_Q, L_TG:Idx)
       IF ERRORCODE()
          BREAK
       .

       L_GQ:CreatedDate         = L_DQ:CreatedDate
       GET(LOC:Graph_Q, L_GQ:CreatedDate)
       IF ~ERRORCODE()
          L_GQ:CreatedDate      = L_DQ:CreatedDate
          IF L_DQ:BID = 1
             L_GQ:Weight_BID1  += L_DQ:Weight
          ELSE
             L_GQ:Weight_BID2  += L_DQ:Weight
          .
          PUT(LOC:Graph_Q)
       ELSE
          CLEAR(LOC:Graph_Q)
          L_GQ:CreatedDate      = L_DQ:CreatedDate
          IF L_DQ:BID = 1
             L_GQ:Weight_BID1  += L_DQ:Weight
          ELSE
             L_GQ:Weight_BID2  += L_DQ:Weight
          .
          ADD(LOC:Graph_Q)
    .  .


    SORT(LOC:Graph_Q, L_GQ:CreatedDate)

    L_TG:Idx        = 0
    LOOP
       L_TG:Idx    += 1
       GET(LOC:Graph_Q, L_TG:Idx)
       IF ERRORCODE()
          BREAK
       .

       L_GQ:No               = L_TG:Idx
       PUT(LOC:Graph_Q)
    .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DIs_Processed')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?L_OG:Last_Ran:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  Window{PROP:MinWidth} = 326                              ! Restrict the minimum window width
  Window{PROP:MinHeight} = 260                             ! Restrict the minimum window height
  Resizer.Init(AppStrategy:Surface)                        ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('DIs_Processed',Window)                     ! Restore window settings from non-volatile store
      L_OG:Earliest_Date  = GETINI('DIs_Processed', 'Earliest_Date-' & CLIP(GLO:Login), 0, GLO:Local_INI)
      L_OG:Latest_Date    = GETINI('DIs_Processed', 'Latest_Date-' & CLIP(GLO:Login), 0, GLO:Local_INI)
      L_OG:Auto_Update    = GETINI('DIs_Processed', 'Auto_Update-' & CLIP(GLO:Login), 0, GLO:Local_INI)
      L_OG:Use_Dates      = GETINI('DIs_Processed', 'Use_Dates-' & CLIP(GLO:Login), 0, GLO:Local_INI)
      L_OG:Run_Period     = GETINI('DIs_Processed', 'Run_Period-' & CLIP(GLO:Login), 0, GLO:Local_INI)
  
  
  
  sql_.Init(GLO:DBOwner, '_SQLTemp2')
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
    ! Start Of Insight InitStuff Group
  Bind('ThisGraph4:ClickedOnPointNumber',ThisGraph4:ClickedOnPointNumber) ! Insight ClickedOnVariable
  Bind('ThisGraph4:ClickedOnPointName',ThisGraph4:ClickedOnPointName)       ! Insight ClickedOnVariable
  Bind('ThisGraph4:ClickedOnSetNumber',ThisGraph4:ClickedOnSetNumber)     ! Insight ClickedOnVariable
      ! LOC:DateWord - CSTRING(50) -
      ! ClickedOnPointName - STRING(255) -
      ! ClickedOnPointNumber - REAL -
      ! ClickedOnSetNumber - LONG -
  if ThisGraph4.Init(?Insight,Insight:Line).
  if not ThisGraph4:Color:Fetched then ThisGraph4.GetWindowsColors(ThisGraph4:Color).
  Window{prop:buffer} = 1
  ThisGraph4.LegendPosition = 9
  ThisGraph4.LegendText = 0
  ThisGraph4.LegendValue = 0
  ThisGraph4.LegendPercent = 0
  ThisGraph4.Stacked=0
  ThisGraph4.BlackAndWhite=0
  ThisGraph4.BackgroundPicture=''
  ThisGraph4.BackgroundColor=Color:Silver
  ThisGraph4.BackgroundShadeColor=Color:None
  ThisGraph4.BorderColor=Color:Black
  ThisGraph4.MinPointWidth=0
  ThisGraph4.PaperZoom=4
  ThisGraph4.ActiveInvisible=0
  ThisGraph4.PrintPortrait=0
  ThisGraph4.RightBorder=10
  ThisGraph4.MaxXGridTicks=16
  ThisGraph4.MaxYGridTicks=16
  ThisGraph4.AutoShade=1
  ThisGraph4.TopShade=0
  ThisGraph4.RightShade=0
  ThisGraph4.LongShade=0
  ThisGraph4.Pattern=Insight:None
  ThisGraph4.Float=0
  ThisGraph4.ZCluster=0
  ThisGraph4.Fill=0
  ThisGraph4.FillToZero=0
  ThisGraph4.SquareWave=0
  ThisGraph4.InnerRadius=0
  ThisGraph4.MaxPieRadius=0
  ThisGraph4.Shape=Insight:Auto
  ThisGraph4.PieAngle=0
  ThisGraph4.WorkSpaceWidth=0
  ThisGraph4.WorkSpaceHeight=0
  ThisGraph4.PieLabelLineColor=Color:None
  ThisGraph4.AspectRatio=1
  ThisGraph4.PieLabelLines=0
  ThisGraph4.StackLines=1
  ThisGraph4.LineFromZero=0
  ThisGraph4.LineWidth = 1
      ! Fonts
  ThisGraph4.LegendAngle   = 0
  ThisGraph4.XFont         = 'Verdana'
  ThisGraph4.XFontAngle    = 90
  ThisGraph4.ShowXLabelsEvery = 2
  ThisGraph4.DisplayOnX = 2   ! 0= don't display, 1= display number, 2= display name.
  ThisGraph4.SeparateYAxis = 0
  ThisGraph4.AutoXLabels = 0
  ThisGraph4.SpreadXLabels       = 1
  ThisGraph4.AutoXGridTicks = 1
  ThisGraph4.AutoScale = Scale:Low + Scale:High
  ThisGraph4.AutoYGridTicks = 0
  ThisGraph4.Depth = 200 / 10
          
  ThisGraph4.AddItem(1,1,LOC:Graph_Q,Insight:GraphField,L_GQ:Weight_BID1,,,,,L_GQ:No,)
  ThisGraph4.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  
  ThisGraph4.SetSetType(1,Insight:Line)
  If ThisGraph4.GetSet(1) = 0 then ThisGraph4.AddSetQ(1).
  ThisGraph4.SetQ.SquareWave = -1
  ThisGraph4.SetQ.Fill       = -1
  ThisGraph4.SetQ.FillToZero = -1
  ThisGraph4.SetQ.PointWidth = 66
  ThisGraph4.SetQ.Points     = 0
  Put(ThisGraph4.SetQ)
          
  ThisGraph4.AddItem(2,2,LOC:Graph_Q,Insight:GraphField,L_GQ:Weight_BID2,,,,,L_GQ:No,)
  ThisGraph4.SetSetYAxis(2,Scale:Low + Scale:High,,,)
  
  ThisGraph4.SetSetType(2,Insight:Line)
  If ThisGraph4.GetSet(2) = 0 then ThisGraph4.AddSetQ(2).
  ThisGraph4.SetQ.SquareWave = -1
  ThisGraph4.SetQ.Fill       = -1
  ThisGraph4.SetQ.FillToZero = -1
  ThisGraph4.SetQ.PointWidth = 66
  ThisGraph4.SetQ.Points     = 0
  Put(ThisGraph4.SetQ)
  !Mouse
  ThisGraph4.setMouseMoveParameters(1, '')
  if ThisGraph4.SavedGraphType<>0 then ThisGraph4.Type = ThisGraph4.SavedGraphType.
  ThisGraph4:Popup.Init()
  ThisGraph4:Popup.AddItem('Zoom Out','ZoomOut','',1)
  ThisGraph4:Popup.AddItemEvent('ZoomOut',INSIGHT:ZoomOut,?Insight)
  ThisGraph4:Popup.AddItem('Zoom In','ZoomIn','',1)
  ThisGraph4:Popup.AddItemEvent('ZoomIn',INSIGHT:ZoomIn,?Insight)
  ThisGraph4:Popup.AddItem('Cycle Sets','CycleSets','',1)
  ThisGraph4:Popup.AddItemEvent('CycleSets',INSIGHT:CycleSets,?Insight)
  ThisGraph4:Popup.AddItem('Copy','Copy','',1)
  ThisGraph4:Popup.AddItemEvent('Copy',INSIGHT:Copy,?Insight)
  ThisGraph4:Popup.AddItem('Save As...','SaveAs','',1)
  ThisGraph4:Popup.AddItemEvent('SaveAs',INSIGHT:SaveAs,?Insight)
  ThisGraph4:Popup.AddItem('Print Graph','Print','',1)
  ThisGraph4:Popup.AddItemEvent('Print',INSIGHT:PrintGraph,?Insight)
  ThisGraph4:Popup.AddItem('Graph Type','SetGraphType','',1)
  ThisGraph4:Popup.AddItem('Line','LineType','SetGraphType',2)
  ThisGraph4:Popup.AddItem('Bar','BarType','SetGraphType',2)
  ThisGraph4:Popup.AddItem('Pareto','ParetoType','SetGraphType',2)
  ThisGraph4:Popup.AddItemEvent('LineType',INSIGHT:SelectGraphType+Insight:Line,?Insight)
  ThisGraph4:Popup.AddItemEvent('BarType',INSIGHT:SelectGraphType+Insight:Bar,?Insight)
  ThisGraph4:Popup.AddItemEvent('ParetoType',INSIGHT:SelectGraphType+Insight:Pareto,?Insight)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('DIs_Processed',Window)                  ! Save window data to non-volatile store
  END
   ThisGraph4.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     ThisGraph4.Reset()
     Post(Event:accepted,?Insight)


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?CancelButton
          IF L_TG:Complete = FALSE AND Window{PROP:Timer} > 0
             L_TG:Complete    = TRUE
             CYCLE
          .
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar2.SelectOnClose = True
      Calendar2.Ask('Select a Date',L_OG:Earliest_Date)
      IF Calendar2.Response = RequestCompleted THEN
      L_OG:Earliest_Date=Calendar2.SelectedDate
      DISPLAY(?L_OG:Earliest_Date)
      END
      ThisWindow.Reset(True)
    OF ?Button_Clear
      ThisWindow.Update()
          CLEAR(L_OG:Earliest_Date)
          DISPLAY
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar3.SelectOnClose = True
      Calendar3.Ask('Select a Date',L_OG:Latest_Date)
      IF Calendar3.Response = RequestCompleted THEN
      L_OG:Latest_Date=Calendar3.SelectedDate
      DISPLAY(?L_OG:Latest_Date)
      END
      ThisWindow.Reset(True)
    OF ?Button_Clear:2
      ThisWindow.Update()
          CLEAR(L_OG:Latest_Date)
      
          DISPLAY
    OF ?L_OG:Run_Period
          EXECUTE L_OG:Run_Period + 1         ! Set From - To|Last 7 Days|This Month
             ENABLE(?Group_Dates)
             DISABLE(?Group_Dates)
             DISABLE(?Group_Dates)
          .
          DO Set_Dates
    OF ?OkButton
      ThisWindow.Update()
          IF L_TG:Complete = FALSE AND Window{PROP:Timer} > 0
             L_TG:Complete    = TRUE
          ELSE
             PUTINI('DIs_Processed', 'Earliest_Date-' & CLIP(GLO:Login), L_OG:Earliest_Date, GLO:Local_INI)
             PUTINI('DIs_Processed', 'Latest_Date-' & CLIP(GLO:Login), L_OG:Latest_Date, GLO:Local_INI)
             PUTINI('DIs_Processed', 'Auto_Update-' & CLIP(GLO:Login), L_OG:Auto_Update, GLO:Local_INI)
             PUTINI('DIs_Processed', 'Use_Dates-' & CLIP(GLO:Login), L_OG:Use_Dates, GLO:Local_INI)
             PUTINI('DIs_Processed', 'Run_Period-' & CLIP(GLO:Login), L_OG:Run_Period, GLO:Local_INI)
      
             SELECT(?Tab_General)
             DISPLAY
      
             DO Load_DIs_Q
          .
    OF ?Insight
      If ((?Insight{prop:visible} or ThisGraph4.ActiveInvisible) and not 0{prop:acceptall})
        ThisGraph4.Draw()
      End
    OF ?CancelButton
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Insight
    CASE EVENT()
    Of INSIGHT:ResetAndDraw
      ThisGraph4.Reset()
      Post(event:accepted,?Insight)
    Of INSIGHT:PrintGraph
      ThisGraph4.PrintGraph('',255)
    Of INSIGHT:Copy
         ThisGraph4.ToClipBoard()
    Of INSIGHT:SaveAs
      if FileDialog('Save As...',ThisGraph4:FileName,'Windows Bitmap|*.bmp|Portable Network Graphics|*.png',10010011b) = 1
        ThisGraph4.SaveAs(ThisGraph4:FileName)
      End
    Of INSIGHT:ZoomIn
      ThisGraph4.Zoom(,-25,ThisGraph4:ClickedOnPointNumber)
      if ThisGraph4.zoom < 5 then ThisGraph4:Popup.SetItemEnable('ZoomIn',0).
      if ThisGraph4.zoom < 100 then ThisGraph4:Popup.SetItemEnable('ZoomOut',1).
    Of INSIGHT:ZoomOut
      ThisGraph4.Zoom(,+25,ThisGraph4:ClickedOnPointNumber)
      if ThisGraph4.zoom = 100 then ThisGraph4:Popup.SetItemEnable('ZoomOut',0).
      if ThisGraph4.zoom > 4 then ThisGraph4:Popup.SetItemEnable('ZoomIn',1).
    Of INSIGHT:CycleSets
      ThisGraph4.SwapSets(1)
      ThisGraph4.Draw()
    Of INSIGHT:SelectGraphType + Insight:Bar to INSIGHT:SelectGraphType + Insight:Gantt
      ThisGraph4.Type = event() - INSIGHT:SelectGraphType
      ThisGraph4.Draw()
    OF EVENT:MouseUp
        If ThisGraph4.GetPoint(ThisGraph4:ClickedOnSetNumber,ThisGraph4:ClickedOnPointNumber) = true
          ThisGraph4:ClickedOnPointName = ThisGraph4.GetPointName(ThisGraph4:ClickedOnSetNumber,ThisGraph4:ClickedOnPointNumber)
        End
      Case Keycode()
      Of MouseRight !Up
        ThisGraph4:Popup.Ask()
      End
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_OG:Run_Period
          EXECUTE L_OG:Run_Period + 1         ! Set From - To|Last 7 Days|This Month
             ENABLE(?Group_Dates)
             DISABLE(?Group_Dates)
             DISABLE(?Group_Dates)
          .
          DO Set_Dates
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
          Last_#              = Window{PROP:Timer}
          Window{PROP:Timer}  = 0
      
          IF L_TG:Complete = TRUE
             IF L_OG:Auto_Update > 0
                ! 0|30|1|2|5|10
                Run_#    = FALSE
                CASE L_OG:Auto_Update
                OF 30
                   IF ABS(CLOCK() - L_OG:Last_Ran) > 3000
                      Run_#  = TRUE
                   .
                OF 1
                   IF ABS(CLOCK() - L_OG:Last_Ran) > 6000
                      Run_#  = TRUE
                   .
                OF 2
                   IF ABS(CLOCK() - L_OG:Last_Ran) > 12000
                      Run_#  = TRUE
                   .
                OF 5
                   IF ABS(CLOCK() - L_OG:Last_Ran) > 30000
                      Run_#  = TRUE
                   .
                OF 10
                   IF ABS(CLOCK() - L_OG:Last_Ran) > 60000
                      Run_#  = TRUE
                .  .
      
                IF Run_# = TRUE
                   DO Load_DIs_Q
             .  .
          ELSE
             L_TG:Time_In     = CLOCK()
      
             LOOP L_TG:Loops TIMES
                IF ABS(CLOCK() - L_TG:Time_In) > 75
                   L_TG:Loops             = L_TG:Loops / 0.9
                   BREAK
                .
      
                DO Process_Entry                         ! Sets complete entry
                IF L_TG:Complete_Items = TRUE
                   DO Next_Branch                        ! Sets complete
                .
      
                IF L_TG:Complete = TRUE
                   ?OkButton{PROP:Text}   = '&Run'
                   Window{PROP:Timer}     = 1000         ! 10 Secs
                   DO Process_Complete
                   BREAK
             .  .
      
             IF ABS(CLOCK() - L_TG:Time_In) < 100
                L_TG:Loops                = L_TG:Loops * 1.2
          .  .
      
          Window{PROP:Timer}  = Last_#
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph4.ValidateRecord  PROCEDURE (long graphID)
ReturnValue  Long  ! set to > 0 to reject record
  Code
  ReturnValue = Parent.ValidateRecord(GraphID)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph4.SetPointName     PROCEDURE(Long graphID)
ReturnValue  String(255)
  Code
  ReturnValue = FORMAT(L_GQ:CreatedDate,@d5)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph4.Reset     PROCEDURE (Byte graphForce=0)
  Code
  self.HeaderName = 'DI Statistics'
  self.XAxisName = 'Dates'
  self.YAxisName = 'Weight'
  Sort(LOC:Graph_Q,L_GQ:CreatedDate)
      
  Self.SetSetPoints(1,0)
  Self.SetSetDescription(1,'Durban')
  Self.SetSetDataLabels(1,0,'', 1,0)
  ThisGraph4.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  
      
  Self.SetSetPoints(2,0)
  Self.SetSetDescription(2,'Johannesburg')
  Self.SetSetDataLabels(2,0,'', 1,0)
  ThisGraph4.SetSetYAxis(2,Scale:Low + Scale:High,,,)
  
  Parent.Reset(graphForce)
  if self.Type = INSIGHT:LINE !and self.LineFromZero
    self.Points = (round((self.points + (2 - 1))/2, 1))*2
  end
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_BottomLeft, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_BottomLeft
  SELF.SetStrategy(?Group_Options, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Options

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Reminding_Window PROCEDURE (p:RIDs, p:CancelButton)

LOC:Locals           GROUP,PRE(LO)                         ! 
Delay_Time           LONG                                  ! Time to delay the reminder by
Delay_Options        BYTE(2)                               ! Delay options
Category_Details     STRING(1000)                          ! 
Q_Item               LONG                                  ! Currently showing
                     END                                   ! 
LOC:Reminder_Q       QUEUE,PRE(L_RQ)                       ! 
RID                  ULONG                                 ! Reminder ID
                     END                                   ! 
LOC:GainFocus_Time   LONG                                  ! Last time event posted
QuickWindow          WINDOW('Reminders'),AT(,,248,209),FONT('Tahoma',8,,FONT:regular),DOUBLE,ALRT(EscKey),CENTER, |
  GRAY,IMM,MDI,HLP('Reminding_Window'),TIMER(200)
                       SHEET,AT(3,2,242,205),USE(?Sheet1),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Type:'),AT(7,7),USE(?REM:ReminderType:Prompt),TRN
                           LIST,AT(62,7,60,10),USE(REM:ReminderType),COLOR(00E9E9E9h),DISABLE,DROP(5),FROM('General|#0' & |
  '|Client|#1|Truck / Trailer|#2'),MSG('General, Client'),SKIP,TIP('General, Client')
                           PROMPT('ID:'),AT(166,7),USE(?REM:ID:Prompt),TRN
                           STRING(@n_10),AT(195,7),USE(REM:ID),RIGHT(1)
                           PROMPT('Reminder Date:'),AT(7,23),USE(?REM:ReminderDate:Prompt),TRN
                           ENTRY(@d5),AT(62,23,60,10),USE(REM:ReminderDate),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Time:'),AT(158,23),USE(?REM:ReminderTime:Prompt),TRN
                           ENTRY(@t7),AT(179,23,60,10),USE(REM:ReminderTime),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Notes:'),AT(7,39),USE(?REM:Notes:Prompt),TRN
                           TEXT,AT(62,39,177,50),USE(REM:Notes),VSCROLL,BOXED
                           LINE,AT(7,95,233,0),USE(?Line1:2),COLOR(COLOR:Black)
                           PROMPT('Details:'),AT(7,98),USE(?Category_Details:Prompt),TRN
                           TEXT,AT(62,98,177,50),USE(LO:Category_Details),VSCROLL,BOXED,COLOR(00E9E9E9h),FLAT,READONLY
                           BUTTON('Jump To'),AT(7,132,51,14),USE(?Button_Update),LEFT,ICON('wachange.ico'),FLAT,TIP('Load Updat' & |
  'e form for Reminder or Reminder Category')
                           BUTTON('Cancel'),AT(62,151,,14),USE(?Button_Cancel),LEFT,ICON('wacancel.ico'),DISABLE,FLAT
                           PROMPT('Delay Options:'),AT(11,175),USE(?Delay_Options:Prompt),TRN
                           LIST,AT(63,175,60,10),USE(LO:Delay_Options),VSCROLL,DROP(15),FROM('Specified Time|#0|5 ' & |
  'Mins|#1|10 Mins|#2|30 Mins|#3|1 Hour|#4|2 Hours|#5|3 Hours|#6|1 Day|#7'),MSG('Delay options'), |
  TIP('Delay options')
                           GROUP,AT(11,191,112,10),USE(?Group_DelayTime)
                             PROMPT('Delay Time:'),AT(11,191),USE(?Delay_Time:Prompt),TRN
                             SPIN(@t4b),AT(63,191,60,10),USE(LO:Delay_Time),MSG('Time to delay the reminder by'),STEP(6000), |
  TIP('Time to delay the reminder by')
                           END
                           BUTTON('No Action'),AT(187,173,49,14),USE(?Button_NoAction),TIP('User (you) will take n' & |
  'o action on this (don''t remind again)')
                           BUTTON('Delay'),AT(131,173,49,14),USE(?Button_Delay),TIP('Delay Action on this for user (you)')
                           BUTTON('Delay All'),AT(131,189,49,14),USE(?Button_Delay_All),TIP('Delay Action on this ' & |
  'for all users<0DH,0AH>Amendments to the Notes will be saved')
                           BUTTON('&Back'),AT(130,151,,14),USE(?Button_Back),LEFT,ICON(ICON:PrevPage),FLAT
                           BUTTON('&Next'),AT(186,151,,14),USE(?Button_Next),LEFT,ICON(ICON:NextPage),FLAT
                           LINE,AT(7,167,233,0),USE(?Line1),COLOR(COLOR:Black)
                           BUTTON('Complete'),AT(187,189,49,14),USE(?Button_Completed),TIP('Completed (marks as in' & |
  '-active)<0DH,0AH>Amendments to the Notes will be saved')
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

    MAP
Check_Reminder_Active           PROCEDURE(ULONG p:RID),LONG
    .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Extract_IDs                 ROUTINE
    !(p:RIDs)
    LOOP
       IF CLIP(p:RIDs) = ''
          BREAK
       .

       L_RQ:RID     = Get_1st_Element_From_Delim_Str(p:RIDs,',',TRUE)
       IF L_RQ:RID > 0
          ADD(LOC:Reminder_Q)
    .  .
    EXIT
Load_Reminder           ROUTINE
    LOOP
       IF RECORDS(LOC:Reminder_Q) = 0
          CLEAR(REM:Record)
          CLEAR(LO:Category_Details)
          DISPLAY
          MESSAGE('There are no more reminders.', 'Reminders', ICON:Asterisk)
          POST(EVENT:CloseWindow)
          BREAK
       .

       IF LO:Q_Item <= 0
          LO:Q_Item    = 1
       ELSIF LO:Q_Item > RECORDS(LOC:Reminder_Q)
          LO:Q_Item    = RECORDS(LOC:Reminder_Q)
       .

       GET(LOC:Reminder_Q, LO:Q_Item)
       IF ~ERRORCODE()
          REM:RID  = L_RQ:RID
          IF Access:Reminders.TryFetch(REM:PKey_RID) ~= LEVEL:Benign
             BREAK
          .
          IF REM:Active = FALSE
             DELETE(LOC:Reminder_Q)
             CYCLE
          .

          CLEAR(LO:Category_Details)

          CASE REM:ReminderType
          OF 1                     ! Client
             CLI:CID   = REM:ID
             IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
                LO:Category_Details    = 'Client: ' & CLIP(CLI:ClientName) & '  (' & CLI:ClientNo & ')'    ! & '<13,10>'
             ELSE
                CLEAR(CLI:Record)
             .
          OF 2                     ! Truck/Trailer
             ! (p:TTID, p:Type, p:Effective_Date, p:LicenseExpiryDate)
             ! (ULONG, BYTE=0, LONG=0, <*LONG>),STRING
             !
             ! p:Type    0 - Expiry - Message if less than Settings expiry, otherwise nothing
             !           1 - Expiry - CSV info if less than Settings expiry, otherwise nothing
             !           2 - Message
             !           3 - CSV info
             !           4 - LicenseInfo
             !
             ! p:Effective_Date
             !           0 - Today
             !           x - Passed date
             Date_#                 = 0
             LO:Category_Details    = Get_TruckTrailer_Info(REM:ID, 4,, Date_#)
             LO:Category_Details    = 'Expires: ' & FORMAT(Date_#,@d5b) & '<13,10>License Info: ' & CLIP(LO:Category_Details)
          ELSE                     ! General
       .  .

       BREAK
    .

    IF LO:Q_Item = RECORDS(LOC:Reminder_Q)
       DISABLE(?Button_Next)
    ELSE
       ENABLE(?Button_Next)
    .
    IF LO:Q_Item = 1
       DISABLE(?Button_Back)
    ELSE
       ENABLE(?Button_Back)
    .

    DISPLAY
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Reminding_Window')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REM:ReminderType:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Relate:RemindersAlias.Open                               ! File RemindersAlias used by this procedure, so make sure it's RelationManager is open
  Access:Reminders.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:RemindersUsers.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Reminding_Window',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      DO Extract_IDs
  
      IF RECORDS(LOC:Reminder_Q) <= 0
         ReturnValue  = LEVEL:Fatal
         POST(EVENT:CloseWindow)
         db.debugout('[Reminding_Window]  Nothing loaded into Q.  Closing Window')
      ELSE
         LO:Q_Item    = 1
         DO Load_Reminder
      .
      LO:Delay_Time   = (100 * 60 * 15) + 1
      IF CLIP(p:CancelButton) ~= ''
         ENABLE(?Button_Cancel)
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
    Relate:RemindersAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Reminding_Window',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
      ! Mutex ???
      GLO:Reminding_Waiting   = FALSE
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Update
      ThisWindow.Update()
          CASE REM:ReminderType
          OF 1
             Update_Clients()
          OF 2
             Update_TruckTrailer()
          ELSE
             Update_Reminders()
          .
      
      
          DO Load_Reminder
    OF ?Button_Cancel
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    OF ?LO:Delay_Options
          ! Specified Time|5 Mins|10 Mins|30 Mins|1 Hour|2 Hours|3 Hours|1 Day
          IF LO:Delay_Options = 0
             ENABLE(?Group_DelayTime)
          ELSE
             DISABLE(?Group_DelayTime)
          .
    OF ?Button_NoAction
      ThisWindow.Update()
          IF Check_Reminder_Active(REM:RID) = FALSE
             MESSAGE('This reminder has already been completed.','Reminders',ICON:Exclamation)
          ELSE
             REU:RID             = REM:RID
             REU:UID             = GLO:UID
             IF Access:RemindersUsers.TryFetch(REU:SKey_UID_RID) = LEVEL:Benign
                REU:NoAction     = TRUE
                IF Access:RemindersUsers.TryUpdate() = LEVEL:Benign
                .
             ELSE
                IF Access:RemindersUsers.TryPrimeAutoInc() = LEVEL:Benign
                   REU:RID       = REM:RID
                   REU:UID       = GLO:UID
                   REU:NoAction  = TRUE
      
                   IF Access:RemindersUsers.TryInsert() ~= LEVEL:Benign
                      Access:RemindersUsers.CancelAutoInc()
          .  .  .  .
      
      
          ! Check and set status as appropriate
          IF REM:UID = GLO:UID AND REM:UGID = 0
             REM:Active  = FALSE
             IF Access:Reminders.TryUpdate() = LEVEL:Benign
          .  .
      
      
          DELETE(LOC:Reminder_Q)
          DO Load_Reminder
      
      
      
    OF ?Button_Delay
      ThisWindow.Update()
          IF Check_Reminder_Active(REM:RID) = FALSE
             MESSAGE('This reminder has already been completed.','Reminders',ICON:Exclamation)
          ELSE
             ! Specified Time|5 Mins|10 Mins|30 Mins|1 Hour|2 Hours|3 Hours|1 Day
      
             CASE LO:Delay_Options
             OF 1                         ! 5 Mins
                LO:Delay_Time = 100 * 60 * 5
             OF 2                         ! 10 Mins
                LO:Delay_Time = 100 * 60 * 10
             OF 3                         ! 30 Mins
                LO:Delay_Time = 100 * 60 * 30
             OF 4                         ! 1 Hour
                LO:Delay_Time = 100 * 60 * 60
             OF 5                         ! 2 Hours
                LO:Delay_Time = 100 * 60 * 120
             OF 6                         ! 3 Hours
                LO:Delay_Time = 100 * 60 * 180
             OF 7                         ! 1 Day
                LO:Delay_Time = 0
             ELSE
                ! Time
             .
      
             IF LO:Delay_Time = 0
                REM:ReminderDate      = TODAY() + 1
             ELSE
                IF CLOCK() + LO:Delay_Time >= 8640000
                   REM:ReminderDate   = TODAY() + 1
                   REM:ReminderTime   = (CLOCK() + LO:Delay_Time) - 8640000
                ELSE
                   REM:ReminderDate   = TODAY()
                   REM:ReminderTime   = CLOCK() + LO:Delay_Time
             .  .
      
      
             REU:RID             = REM:RID
             REU:UID             = GLO:UID
             IF Access:RemindersUsers.TryFetch(REU:SKey_UID_RID) = LEVEL:Benign
                REU:ReminderDate = REM:ReminderDate
                REU:ReminderTime = REM:ReminderTime
      
                IF Access:RemindersUsers.TryUpdate() = LEVEL:Benign
                .
             ELSE
                IF Access:RemindersUsers.TryPrimeAutoInc() = LEVEL:Benign
                   REU:RID            = REM:RID
                   REU:UID            = GLO:UID
      
                   REU:ReminderDate   = REM:ReminderDate
                   REU:ReminderTime   = REM:ReminderTime
      
                   IF Access:RemindersUsers.TryInsert() ~= LEVEL:Benign
                      Access:RemindersUsers.CancelAutoInc()
          .  .  .  .
      
          DELETE(LOC:Reminder_Q)
          DO Load_Reminder
    OF ?Button_Delay_All
      ThisWindow.Update()
          IF Check_Reminder_Active(REM:RID) = FALSE
             MESSAGE('This reminder has already been completed.','Reminders',ICON:Exclamation)
      
             DELETE(LOC:Reminder_Q)
             DO Load_Reminder
          ELSE
             ! Specified Time|5 Mins|10 Mins|30 Mins|1 Hour|2 Hours|3 Hours|1 Day
      
             CASE LO:Delay_Options
             OF 1                         ! 5 Mins
                LO:Delay_Time = 100 * 60 * 5
             OF 2                         ! 10 Mins
                LO:Delay_Time = 100 * 60 * 10
             OF 3                         ! 30 Mins
                LO:Delay_Time = 100 * 60 * 30
             OF 4                         ! 1 Hour
                LO:Delay_Time = 100 * 60 * 60
             OF 5                         ! 2 Hours
                LO:Delay_Time = 100 * 60 * 120
             OF 6                         ! 3 Hours
                LO:Delay_Time = 100 * 60 * 180
             OF 7                         ! 1 Day
                LO:Delay_Time = 0
             ELSE
                ! Time
             .
      
             IF LO:Delay_Time = 0
                REM:ReminderDate     += TODAY() + 1
             ELSE
                IF CLOCK() + LO:Delay_Time >= 8640000
                   REM:ReminderDate   = TODAY() + 1
                   REM:ReminderTime   = (CLOCK() + LO:Delay_Time) - 8640000
                ELSE
                   REM:ReminderDate   = TODAY()
                   REM:ReminderTime   = CLOCK() + LO:Delay_Time
             .  .
      
             IF Access:Reminders.TryUpdate() = LEVEL:Benign
                DELETE(LOC:Reminder_Q)
                DO Load_Reminder
          .  .
    OF ?Button_Back
      ThisWindow.Update()
          LO:Q_Item   -= 1
          DO Load_Reminder
    OF ?Button_Next
      ThisWindow.Update()
          LO:Q_Item   += 1
          DO Load_Reminder
    OF ?Button_Completed
      ThisWindow.Update()
          IF Check_Reminder_Active(REM:RID) = FALSE
             MESSAGE('This reminder has already been completed.||Additional comments made will be lost.','Reminders',ICON:Exclamation)
      
             DELETE(LOC:Reminder_Q)
             DO Load_Reminder
          ELSE
             REM:Notes   = REM:Notes & '<13,10>Completed by: ' & CLIP(GLO:Login) & '  (UID: ' & GLO:UID & ')'
             REM:Notes   = REM:Notes & '<13,10>On: ' & FORMAT(TODAY(),@d6) & ' ' & FORMAT(CLOCK(),@t4)
      
             REM:Active  = FALSE
      
             IF REM:Reoccurs = TRUE
                ! So many mins/hours|#1|Daily|#2|Weekly|#3|Bi-Weekly|#4|Monthly|#5|Bi-Monthly|#6|Quarterly|#7|Yearly|#8
                EXECUTE REM:Reoccur_Period
                   Date_Time_Advance(REM:ReminderDate, REM:ReminderTime, 1, REM:Reoccur_Period)
                   Date_Time_Advance(REM:ReminderDate, REM:ReminderTime, 2, 1)
                   Date_Time_Advance(REM:ReminderDate, REM:ReminderTime, 3, 1)
                   Date_Time_Advance(REM:ReminderDate, REM:ReminderTime, 3, 2)
                   Date_Time_Advance(REM:ReminderDate, REM:ReminderTime, 4, 1)
                   Date_Time_Advance(REM:ReminderDate, REM:ReminderTime, 4, 2)
                   Date_Time_Advance(REM:ReminderDate, REM:ReminderTime, 5, 1)
                   Date_Time_Advance(REM:ReminderDate, REM:ReminderTime, 6, 1)
                .
      
                ! Date_Time_Advance
                ! (*LONG, *LONG, BYTE, LONG)
                ! (p_Date, p_Time, p_Option, p_Val)
                !
                ! p_Option
                !       - Time
                !       - Days
                !       - Weeks
                !       - Months
                !       - Quarters
                !       - Years
                REM:Active  = TRUE
             .
      
             IF Access:Reminders.TryUpdate() = LEVEL:Benign
                DELETE(LOC:Reminder_Q)
                DO Load_Reminder
          .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LO:Delay_Options
          ! Specified Time|5 Mins|10 Mins|30 Mins|1 Hour|2 Hours|3 Hours|1 Day
          IF LO:Delay_Options = 0
             ENABLE(?Group_DelayTime)
          ELSE
             DISABLE(?Group_DelayTime)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          ! Specified Time|5 Mins|10 Mins|30 Mins|1 Hour|2 Hours|3 Hours|1 Day
          IF LO:Delay_Options = 0
             ENABLE(?Group_DelayTime)
          ELSE
             DISABLE(?Group_DelayTime)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
          IF SELF.Opened
             IF QuickWindow{PROP:Iconize} = TRUE
                QuickWindow{PROP:Iconize} = FALSE
             .
             IF QuickWindow{PROP:Active} <> TRUE
                QuickWindow{PROP:Active} = TRUE
          .  .
      
    OF EVENT:Timer
          IF CLOCK() - LOC:GainFocus_Time > (100 * 60 * 5)
             LOC:GainFocus_Time   = CLOCK()
             POST(EVENT:GainFocus)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Check_Reminder_Active           PROCEDURE(ULONG p:RID)
L:State     LONG
    CODE

    A_REM:RID   = p:RID
    IF Access:RemindersAlias.TryFetch(A_REM:PKey_RID) = LEVEL:Benign
       L:State  = A_REM:Active
    ELSE
       L:State  = -1
    .

    RETURN(L:State)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

