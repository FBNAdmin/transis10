

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS002.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Deliveries_Search PROCEDURE (p_Grp)

Criteria             GROUP,PRE(SFC)                        ! 
DINo                 ULONG                                 ! Delivery Instruction Number
InvoiceNo            ULONG                                 ! Invoice Number
ClientNo             ULONG                                 ! Client No.
Date                 DATE                                  ! 
Sender               ULONG                                 ! Address ID - note once used certain information should not be changeable, such as the suburb
Receiver             ULONG                                 ! Address ID - note once used certain information should not be changeable, such as the suburb
Manifest             ULONG                                 ! Manifest ID
Vessel               STRING(35)                            ! Vessel this container arrived on
Container            STRING(35)                            ! 
ClientRef            STRING(60)                            ! Client Reference
Weights              BYTE                                  ! 
FromWeight           DECIMAL(8,2)                          ! In kg's
ToWeight             DECIMAL(8,2)                          ! In kg's
SpecialInstructions  CSTRING(256)                          ! This will print on the DI and Delivery Note (& POD)
                     END                                   ! 
LOC:Result           BYTE                                  ! 
Criteria_Strs        GROUP,PRE(L_CS)                       ! 
Sender               STRING(36)                            ! Name of this address
Receiver             STRING(36)                            ! Name of this address
                     END                                   ! 
LOC:Search_Options   GROUP,PRE(LSO)                        ! 
DINo                 ULONG                                 ! Delivery Instruction Number
IID                  ULONG                                 ! Invoice Number
                     END                                   ! 
LOC:Search_Control   GROUP,PRE(LSC)                        ! 
Found_DI             BYTE                                  ! 
Found_Inv            BYTE                                  ! 
Load                 BYTE                                  ! 
                     END                                   ! 
QuickWindow          WINDOW('Deliveries Search'),AT(,,405,267),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  IMM,MDI,HLP('Deliveries_Search')
                       SHEET,AT(4,4,398,242),USE(?Sheet1)
                         TAB('General'),USE(?Tab_General)
                           PROMPT('DI No.:'),AT(10,22),USE(?SFC:DINo:Prompt),TRN
                           ENTRY(@n_10),AT(73,22,60,10),USE(SFC:DINo),RIGHT(1),MSG('Delivery Instruction Number'),REQ, |
  TIP('Delivery Instruction Number')
                           PROMPT('DI Date:'),AT(10,36),USE(?SFC:DIDate:Prompt),TRN
                           SPIN(@d6b),AT(73,36,60,10),USE(SFC:Date),RIGHT(1)
                           BUTTON('...'),AT(138,36,15,10),USE(?Calendar)
                           PROMPT('Client No.:'),AT(10,58),USE(?SFC:ClientNo:Prompt),TRN
                           ENTRY(@n_10b),AT(73,58,60,10),USE(SFC:ClientNo),RIGHT(1),MSG('Client No.'),TIP('Client No.')
                           BUTTON('...'),AT(138,58,15,10),USE(?Button_LookupClient)
                           PROMPT('Client Reference:'),AT(10,72),USE(?SFC:ClientRef:Prompt),TRN
                           ENTRY(@s60),AT(73,72,121,10),USE(SFC:ClientRef),LEFT,MSG('Client Reference'),TIP('Client Reference')
                           PROMPT('MID:'),AT(10,92),USE(?SFC:Manifest:Prompt),TRN
                           ENTRY(@n_10),AT(73,92,60,10),USE(SFC:Manifest),RIGHT(1),MSG('Manifest ID'),REQ,TIP('Manifest ID')
                           PROMPT('Invoice No.:'),AT(10,106),USE(?SFC:InvoiceNo:Prompt),TRN
                           ENTRY(@n_10),AT(73,106,60,10),USE(SFC:InvoiceNo),RIGHT(1),MSG('Invoice ID'),REQ,TIP('Invoice ID')
                           PROMPT('Container No.:'),AT(10,126),USE(?SFC:ContainerNo:Prompt),TRN
                           ENTRY(@s35),AT(73,126,121,10),USE(SFC:Container),LEFT,REQ
                           PROMPT('Special Instruct.:'),AT(10,212),USE(?SFC:SpecialInstructions:Prompt),TRN
                           PROMPT('Vessel:'),AT(10,140),USE(?SFC:Vessel:Prompt)
                           ENTRY(@s35),AT(73,141,121,10),USE(SFC:Vessel),MSG('Vessel this container arrived on'),TIP('Vessel thi' & |
  's container arrived on')
                           BUTTON('...'),AT(199,140,15,10),USE(?CallLookup)
                           PROMPT('Sender:'),AT(10,170),USE(?L_CS:Sender:Prompt)
                           ENTRY(@s35),AT(73,169,121,10),USE(L_CS:Sender),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                           BUTTON('...'),AT(199,169,15,10),USE(?CallLookup_Sender)
                           PROMPT('Receiver:'),AT(10,183),USE(?L_CS:Receiver:Prompt)
                           ENTRY(@s35),AT(73,184,121,10),USE(L_CS:Receiver),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                           BUTTON('...'),AT(199,183,15,10),USE(?CallLookup_Receiver)
                           ENTRY(@s200),AT(73,212,165,10),USE(SFC:SpecialInstructions),TIP('Will match to any DI t' & |
  'hat contains what you enter here,  Eg. entering "1234" here would find DIs with "012' & |
  '345" or "023541234" or "12340936"')
                           CHECK(' Weights'),AT(261,22),USE(SFC:Weights),TRN
                           GROUP,AT(230,36,168,10),USE(?Group_Weights)
                             PROMPT('Weight:'),AT(230,36),USE(?FromWeight:Prompt),TRN
                             ENTRY(@n-11.2),AT(261,36,60,10),USE(SFC:FromWeight),RIGHT(1),MSG('In kg''s'),TIP('In kg''s')
                             STRING('to'),AT(325,36),USE(?String1),TRN
                             ENTRY(@n-11.2),AT(338,36,60,10),USE(SFC:ToWeight),RIGHT(1),MSG('In kg''s'),TIP('In kg''s')
                           END
                           BUTTON('&Search'),AT(316,201,81,40),USE(?Ok:2),ICON('WAOK.ICO'),DEFAULT,MSG('Accept operation'), |
  TIP('Accept Operation')
                           BUTTON('&Clear'),AT(316,175,81,22),USE(?BUTTON1),SKIP
                           BUTTON('&Restore'),AT(316,150,81,22),USE(?BUTTON1:2),SKIP
                         END
                       END
                       BUTTON('&Cancel'),AT(352,250,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(4,250,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar7            CalendarClass
Criteria2    GROUP,PRE(SFC2)
DINo            LIKE(DEL:DINo)
InvoiceNo       LIKE(INV:IID)

ClientNo        LIKE(CLI:ClientNo)
Date            DATE

Sender          LIKE(ADD:AID)
Receiver        LIKE(ADD:AID)

Manifest        LIKE(MAN:MID)

Vessel          LIKE(DELI:ContainerVessel)
Container       LIKE(DELI:ContainerNo)
ClientRef       LIKE(DEL:ClientReference)

Weights         BYTE
FromWeight      LIKE(DELI:Weight)
ToWeight        LIKE(DELI:Weight)

SpecialInstructions LIKE(DEL:SpecialDeliveryInstructions)
            .


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:Result)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Save_Conditions     ROUTINE    
    I# = 0
    LOOP
       I# += 1
       IF CLIP(WHO(Criteria, I#)) = ''
          BREAK
       .   
       PUTINI('Criteria', WHO(Criteria, I#), WHAT(Criteria, I#), GLO:Local_INI)
    .    
    DISPLAY
    EXIT 
       
    
Load_Conditions     ROUTINE
    DATA
Fieldx  ANY
    
    CODE
    I# = 0
    LOOP
       I# += 1
       IF CLIP(WHO(Criteria, I#)) = ''
          BREAK
       .   
       Fieldx   &= WHAT(Criteria,I#)
       Fieldx    = GETINI('Criteria', WHO(Criteria, I#), '', GLO:Local_INI)
    .    
    DISPLAY
    EXIT
    

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Deliveries_Search')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SFC:DINo:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:AddressAlias.Open                                 ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  Relate:Vessels.Open                                      ! File Vessels used by this procedure, so make sure it's RelationManager is open
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Deliveries_Search',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  IF ?SFC:Weights{Prop:Checked}
    ENABLE(?Group_Weights)
  END
  IF NOT ?SFC:Weights{PROP:Checked}
    DISABLE(?Group_Weights)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressAlias.Close
    Relate:Vessels.Close
  END
  IF SELF.Opened
    INIMgr.Update('Deliveries_Search',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Vessels
      Browse_Addresses
      Browse_Addresses_Alias_h
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar7.SelectOnClose = True
      Calendar7.Ask('Select a Date')
      IF Calendar7.Response = RequestCompleted THEN
      END
      ThisWindow.Reset(True)
    OF ?Button_LookupClient
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_Clients()
      ThisWindow.Reset
          IF GlobalResponse = RequestCompleted
             SFC:ClientNo = CLI:ClientNo
             DISPLAY
          .
    OF ?SFC:Vessel
      IF NOT QuickWindow{PROP:AcceptAll}
        IF SFC:Vessel OR ?SFC:Vessel{PROP:Req}
          VES:Vessel = SFC:Vessel
          IF Access:Vessels.TryFetch(VES:Key_Vessel)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              SFC:Vessel = VES:Vessel
            ELSE
              SELECT(?SFC:Vessel)
              CYCLE
            END
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      VES:Vessel = SFC:Vessel
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        SFC:Vessel = VES:Vessel
      END
      ThisWindow.Reset(1)
    OF ?L_CS:Sender
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_CS:Sender OR ?L_CS:Sender{PROP:Req}
          ADD:AddressName = L_CS:Sender
          IF Access:Addresses.TryFetch(ADD:Key_Name)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              L_CS:Sender = ADD:AddressName
              SFC:Sender = ADD:AID
            ELSE
              CLEAR(SFC:Sender)
              SELECT(?L_CS:Sender)
              CYCLE
            END
          ELSE
            SFC:Sender = ADD:AID
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_Sender
      ThisWindow.Update()
      ADD:AddressName = L_CS:Sender
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_CS:Sender = ADD:AddressName
        SFC:Sender = ADD:AID
      END
      ThisWindow.Reset(1)
    OF ?L_CS:Receiver
      IF L_CS:Receiver OR ?L_CS:Receiver{PROP:Req}
        A_ADD:AddressName = L_CS:Receiver
        IF Access:AddressAlias.TryFetch(A_ADD:Key_Name)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            L_CS:Receiver = A_ADD:AddressName
            SFC:Receiver = A_ADD:AID
          ELSE
            CLEAR(SFC:Receiver)
            SELECT(?L_CS:Receiver)
            CYCLE
          END
        ELSE
          SFC:Receiver = A_ADD:AID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_Receiver
      ThisWindow.Update()
      A_ADD:AddressName = L_CS:Receiver
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        L_CS:Receiver = A_ADD:AddressName
        SFC:Receiver = A_ADD:AID
      END
      ThisWindow.Reset(1)
    OF ?SFC:Weights
      IF ?SFC:Weights{PROP:Checked}
        ENABLE(?Group_Weights)
      END
      IF NOT ?SFC:Weights{PROP:Checked}
        DISABLE(?Group_Weights)
      END
      ThisWindow.Reset()
    OF ?Ok:2
      ThisWindow.Update()
          UPDATE()
      
          IF SFC:DINo = 0 AND SFC:InvoiceNo = 0 AND SFC:ClientNo = 0 AND SFC:Date = 0 AND SFC:Sender = 0 AND |
                  SFC:Receiver = 0 AND SFC:Manifest = 0 AND CLIP(SFC:Vessel) = '' AND CLIP(SFC:Container) = '' AND |
                  CLIP(SFC:ClientRef) = '' AND SFC:Weights = FALSE AND CLIP(SFC:SpecialInstructions) = ''
             MESSAGE('You have not specified any search criteria.', 'Search Options', ICON:Exclamation)
             SELECT(?SFC:DINo)
          ELSE
             LOC:Result  = TRUE
      
             Criteria2  :=: Criteria
      
             p_Grp       = Criteria2
      
             DO Save_Conditions
      
             POST(EVENT:CloseWindow)
          .
    OF ?BUTTON1
      ThisWindow.Update()
          DO Save_Conditions
          
          CLEAR(Criteria)
          DISPLAY
          
    OF ?BUTTON1:2
      ThisWindow.Update()
          DO Load_Conditions
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Invoice_Items PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
History::INI:Record  LIKE(INI:RECORD),THREAD
QuickWindow          WINDOW('Form Invoice Items'),AT(,,358,124),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('Update_Invoice_Items'),SYSTEM
                       SHEET,AT(4,4,350,100),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Item No.:'),AT(9,20),USE(?INI:ItemNo:Prompt),TRN
                           ENTRY(@n6),AT(82,20,55,10),USE(INI:ItemNo),RIGHT(1),COLOR(00E9E9E9h),MSG('Item Number'),READONLY, |
  SKIP,TIP('Item Number')
                           PROMPT('Type:'),AT(9,34),USE(?INI:Type:Prompt),TRN
                           LIST,AT(82,34,55,10),USE(INI:Type),DROP(5),FROM('Container|#0|Loose|#1|Other|#2'),MSG('Type of Item'), |
  TIP('Type of Item')
                           PROMPT('Commodity:'),AT(9,76),USE(?INI:Commodity:Prompt),TRN
                           ENTRY(@s35),AT(82,76,144,10),USE(INI:Commodity),COLOR(00E9E9E9h),MSG('Commosity'),READONLY, |
  SKIP,TIP('Commosity')
                           PROMPT('Description:'),AT(9,48),USE(?INI:Description:Prompt),TRN
                           ENTRY(@s150),AT(82,48,267,10),USE(INI:Description),MSG('Description'),TIP('Description')
                           PROMPT('Units:'),AT(9,62),USE(?INI:Units:Prompt),TRN
                           ENTRY(@n6b),AT(82,62,55,10),USE(INI:Units),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Container Description:'),AT(9,90),USE(?INI:ContainerDescription:Prompt),TRN
                           ENTRY(@s150),AT(82,90,267,10),USE(INI:ContainerDescription),COLOR(00E9E9E9h),SKIP
                         END
                       END
                       BUTTON('&OK'),AT(252,108,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(306,108,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,108,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Invoice Item Record'
  OF InsertRecord
    ActionMessage = 'Invoice Item Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Invoice Item Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Invoice_Items')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?INI:ItemNo:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(INI:Record,History::INI:Record)
  SELF.AddHistoryField(?INI:ItemNo,4)
  SELF.AddHistoryField(?INI:Type,5)
  SELF.AddHistoryField(?INI:Commodity,7)
  SELF.AddHistoryField(?INI:Description,8)
  SELF.AddHistoryField(?INI:Units,9)
  SELF.AddHistoryField(?INI:ContainerDescription,10)
  SELF.AddUpdateFile(Access:_InvoiceItems)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:_Invoice.SetOpenRelated()
  Relate:_Invoice.Open                                     ! File _Invoice used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_InvoiceItems
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?INI:Type)
    ?INI:Commodity{PROP:ReadOnly} = True
    ?INI:Description{PROP:ReadOnly} = True
    ?INI:Units{PROP:ReadOnly} = True
    ?INI:ContainerDescription{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Invoice_Items',QuickWindow)         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      IF SELF.Request = InsertRecord
         INI:IID              = INV:IID
  
         ! Get next item no.
         _SQLTemp{PROP:SQL}   = 'SELECT COUNT(*) FROM _InvoiceItems WHERE IID = ' & INV:IID
         NEXT(_SQLTemp)
         INI:ItemNo           = _SQ:S1
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Invoice.Close
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Invoice_Items',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  INI:Type = 2
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
          IF CLIP(INI:Description) = '' AND INI:Type = 3
             MESSAGE('Please enter a description.', 'Update Invoice Item' , ICON:Exclamation)
             SELECT(?INI:Description)
             CYCLE
          .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Set type to 1 for reversal insert
!!! </summary>
Update_ClientsPayments PROCEDURE (p:Type, p:CPID)

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Screen_Vars      GROUP,PRE(L_SV)                       ! 
ClientName           STRING(100)                           ! 
UnAllocated_Amt      DECIMAL(10,2)                         ! 
Allocated_Amount     DECIMAL(10,2)                         ! 
Update_Alloc_Amount  DECIMAL(10,2)                         ! Amount of payment allocated to this Invoice
CPAID_Selected       ULONG                                 ! Clients Payment Allocation ID
Bulk_Option          BYTE                                  ! Bulk Option
DI_Invoice_List      STRING(1000)                          ! 
ID                   ULONG                                 ! DI or Invoice
Problems             STRING(1000)                          ! 
ToAlloc              DECIMAL(10,2)                         ! Amount of payment allocated to this Invoice
                     END                                   ! 
LOC:Orig_Amt         DECIMAL(10,2)                         ! 
LOC:CPID             ULONG                                 ! Cliets Payment ID
LOC:Loading_Problems BYTE                                  ! problems in last bulk load from numbers or from tagging
LOC:Browse_Vars      GROUP,PRE()                           ! 
L_BV:Paid_Total      DECIMAL(10,2)                         ! Paid total on invoice
                     END                                   ! 
LOC:Dont_Save        BYTE                                  ! 
BRW2::View:Browse    VIEW(ClientsPaymentsAllocation)
                       PROJECT(CLIPA:AllocationNo)
                       PROJECT(CLIPA:AllocationDate)
                       PROJECT(CLIPA:Amount)
                       PROJECT(CLIPA:Comment)
                       PROJECT(CLIPA:IID)
                       PROJECT(CLIPA:CPAID)
                       PROJECT(CLIPA:CPID)
                       JOIN(INV:PKey_IID,CLIPA:IID)
                         PROJECT(INV:DINo)
                         PROJECT(INV:Total)
                         PROJECT(INV:IID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
CLIPA:AllocationNo     LIKE(CLIPA:AllocationNo)       !List box control field - type derived from field
CLIPA:AllocationDate   LIKE(CLIPA:AllocationDate)     !List box control field - type derived from field
CLIPA:Amount           LIKE(CLIPA:Amount)             !List box control field - type derived from field
CLIPA:Comment          LIKE(CLIPA:Comment)            !List box control field - type derived from field
CLIPA:IID              LIKE(CLIPA:IID)                !List box control field - type derived from field
INV:DINo               LIKE(INV:DINo)                 !List box control field - type derived from field
INV:Total              LIKE(INV:Total)                !List box control field - type derived from field
L_BV:Paid_Total        LIKE(L_BV:Paid_Total)          !List box control field - type derived from local data
CLIPA:CPAID            LIKE(CLIPA:CPAID)              !Browse hot field - type derived from field
CLIPA:CPID             LIKE(CLIPA:CPID)               !Browse key field - type derived from field
INV:IID                LIKE(INV:IID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::CLIP:Record LIKE(CLIP:RECORD),THREAD
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Client Payments'),AT(,,417,330),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateClientsPayments'),SYSTEM
                       GROUP,AT(4,316,150,10),USE(?Group_AllocStatus)
                         PROMPT('Allocation Status:'),AT(4,316),USE(?CLIP:Status:Prompt)
                         LIST,AT(64,316,90,9),USE(CLIP:Status),COLOR(00E9E9E9h),DISABLE,DROP(5),FROM('Not Alloca' & |
  'ted|#0|Partial Allocation|#1|Fully Allocated|#2'),MSG('Status'),SKIP,TIP('Status')
                       END
                       SHEET,AT(4,4,409,308),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           GROUP,AT(9,22,262,10),USE(?GroupClient)
                             PROMPT('Client:'),AT(9,22),USE(?CLIP:CID:Prompt),TRN
                             BUTTON('...'),AT(52,22,12,10),USE(?CallLookup)
                             ENTRY(@s100),AT(69,22,199,10),USE(L_SV:ClientName),REQ,TIP('Client name')
                           END
                           GROUP,AT(9,36,124,10),USE(?Group_DateMade)
                             PROMPT('Date Made:'),AT(9,36),USE(?CLIP:DateMade:Prompt),TRN
                             BUTTON('...'),AT(52,36,12,10),USE(?Calendar)
                             SPIN(@d6),AT(69,36,64,10),USE(CLIP:DateMade),MSG('Date payment was made'),TIP('Date payme' & |
  'nt was made')
                           END
                           BUTTON(':'),AT(137,36,6,10),USE(?Button_ChangeDate),TIP('Click here to allow change of date')
                           GROUP,AT(257,256,153,14),USE(?Group_Browse)
                             BUTTON('&Insert'),AT(259,256,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                             BUTTON('&Change'),AT(311,256,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                             BUTTON('&Delete'),AT(361,256,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                           END
                           PROMPT('CPID:'),AT(285,6),USE(?CLIP:CPID:Prompt),TRN
                           STRING(@n_10),AT(365,6,,10),USE(CLIP:CPID),RIGHT(1),TRN
                           STRING(@n_10),AT(366,22,,10),USE(CLIP:CID),RIGHT(1),TRN
                           PROMPT('CID:'),AT(285,22),USE(?CLIP:DateCaptured:Prompt:2),TRN
                           PROMPT('Date Captured:'),AT(285,36),USE(?CLIP:DateCaptured:Prompt),TRN
                           STRING(@d6),AT(363,36,,10),USE(CLIP:DateCaptured),RIGHT(1),TRN
                           PROMPT('Payment Amount:'),AT(9,52),USE(?CLIP:Amount:Prompt),TRN
                           ENTRY(@n-14.2),AT(69,52,64,10),USE(CLIP:Amount),DECIMAL(12),MSG('Amount of payment'),TIP('Amount of payment')
                           PROMPT('Allocated Amt.:'),AT(285,52),USE(?Allocated_Amount:Prompt),TRN
                           ENTRY(@n-14.2),AT(349,52,60,10),USE(L_SV:Allocated_Amount),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           PROMPT(''),AT(9,66,187,10),USE(?Prompt_Reveral),TRN
                           PROMPT('Un-Allocated Amt.:'),AT(285,66),USE(?UnAllocated_Amt:Prompt),TRN
                           ENTRY(@n-14.2),AT(349,66,60,10),USE(L_SV:UnAllocated_Amt),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           LINE,AT(11,81,398,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           LIST,AT(9,86,401,148),USE(?Browse:2),HVSCROLL,FORMAT('[26R(2)|M~No.~C(0)@n6@38R(2)|M~Da' & |
  'te~C(0)@d5@46R(1)|M~Amount~C(0)@n-14.2@120L(1)|M~Comment~C(0)@s255@](209)|M~Allocati' & |
  'on~[42R(2)|M~Invoice (IID)~C(0)@n_10@40R(2)|M~DI No.~C(0)@n_10@46R(2)|M~Total~C(0)@n' & |
  '-14.2@46R(1)|M~Total Paid~C(0)@n-14.2@]|M~Invoice~'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he ClientsPaymentsAllocation file')
                           LINE,AT(95,252,230,0),USE(?Line3),COLOR(COLOR:Black)
                           PROMPT('Change Allocation Amount:'),AT(106,238),USE(?Amount:Prompt),TRN
                           ENTRY(@n-14.2),AT(195,238,60,10),USE(L_SV:Update_Alloc_Amount),RIGHT(1),MSG('Amount of payment'), |
  TIP('Amount of payment allocated to this Invoice')
                           BUTTON('Upd&ate'),AT(263,238,49,10),USE(?Button_Update),TIP('Update selected entry with' & |
  ' the allocation amount specified')
                           BUTTON('&Save'),AT(9,256,,14),USE(?Save),LEFT,KEY(CtrlAltEnter),ICON('SAVE.ICO'),DISABLE,FLAT, |
  TIP('Save Record')
                           BUTTON('&Tag Invoices'),AT(182,256,,14),USE(?Button_TagInvoices),LEFT,ICON('DocTag.ico'),FLAT
                           LINE,AT(11,273,398,0),USE(?Line1:2),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Notes:'),AT(9,278),USE(?CLIP:Notes:Prompt),TRN
                           TEXT,AT(69,278,340,30),USE(CLIP:Notes),VSCROLL,MSG('Notes for this payment'),TIP('Notes for ' & |
  'this payment')
                         END
                         TAB('&2) Bulk Allocation'),USE(?Tab2)
                           PROMPT('Bulk Option:'),AT(9,22),USE(?Bulk_Option:Prompt),TRN
                           LIST,AT(69,22,73,9),USE(L_SV:Bulk_Option),DROP(5),FROM('DI''s|#0|Invoice No.|#1'),MSG('Option'), |
  TIP('Option')
                           PROMPT('Un-Allocated Amt.:'),AT(285,22),USE(?UnAllocated_Amt:Prompt:2),TRN
                           ENTRY(@n-14.2),AT(349,22,60,10),USE(L_SV:UnAllocated_Amt,,?L_SV:UnAllocated_Amt:2),RIGHT(1), |
  COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('DI / Invoice List:'),AT(9,36),USE(?DI_Invoice_List:Prompt),TRN
                           TEXT,AT(69,36,340,130),USE(L_SV:DI_Invoice_List),VSCROLL,BOXED
                           BUTTON('  &Allocate'),AT(69,168),USE(?Button_Allocate),LEFT,ICON('PROGRESS.ICO'),FLAT
                           PROMPT('Problems:'),AT(9,208),USE(?Problems:Prompt),TRN
                           TEXT,AT(69,208,340,100),USE(L_SV:Problems),VSCROLL,BOXED
                         END
                       END
                       BUTTON('&OK'),AT(312,314,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(364,314,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(203,314,53,13),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW2::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar10           CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
Inv_Tags              shpTagClass



  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Reversal_Allocations            ROUTINE
    DATA
R:CPAID             LIKE(A_CLIPA:CPAID)
R:Problem           BYTE

    CODE
    ! Create reversal allocations for all allocations made on the payment
    CLEAR(A_CLIPA:Record,-1)
    A_CLIPA:CPID    = p:CPID
    SET(A_CLIPA:FKey_CPID_AllocationNo, A_CLIPA:FKey_CPID_AllocationNo)
    LOOP
       IF Access:ClientsPaymentsAllocationAlias.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF A_CLIPA:CPID ~= p:CPID
          BREAK
       .

       IF Access:ClientsPaymentsAllocation.TryPrimeAutoInc() ~= LEVEL:Benign
          R:Problem             = 1
          BREAK
       .
       R:CPAID                  = CLIPA:CPAID

       CLIPA:Record            :=: A_CLIPA:Record

       CLIPA:CPAID              = R:CPAID
       CLIPA:CPID               = CLIP:CPID

       CLIPA:Amount             = -A_CLIPA:Amount

       CLIPA:AllocationDate     = TODAY()
       CLIPA:AllocationTime     = CLOCK()

       IF Access:ClientsPaymentsAllocation.TryInsert() = LEVEL:Benign
          Upd_Invoice_Paid_Status(CLIPA:IID)
       ELSE
          R:Problem     = 2
          BREAK
    .  .

    IF R:Problem > 0
       MESSAGE('There was a problem adding reversal Payment Allocations for this Payment.||Not all were added.', 'Reverse Payment Allocations', ICON:Hand)
    .
    EXIT
Allocate_Bulk                       ROUTINE
    DATA
R:ID                STRING(50)

    CODE
    ! Itterates through the comma delimited list - gets DI's and / or Invoices
    L_SV:ToAlloc        = L_SV:UnAllocated_Amt

    LOOP
       IF CLIP(L_SV:DI_Invoice_List) = ''
          BREAK
       .

       R:ID             = Get_1st_Element_From_Delim_Str(L_SV:DI_Invoice_List, ',', TRUE)
       L_SV:ID          = Get_1st_Element_From_Delim_Str(R:ID, '(', TRUE)                   ! Remove any comment - see below

       IF L_SV:Bulk_Option = 0                     ! DIs
          DEL:DINo      = L_SV:ID
          IF Access:Deliveries.TryFetch(DEL:Key_DINo) = LEVEL:Benign
             ! See if we have an invoice
             INV:DID    = DEL:DID
             IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
                ! Check that this invoice is not Fully Paid / Credited already
                ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
                DO Get_Inv_Allocate
             ELSE
                IF CLIP(L_SV:Problems) = ''
                   L_SV:Problems  = L_SV:ID
                ELSE
                   L_SV:Problems  = CLIP(L_SV:Problems) & ', ' & L_SV:ID
                .
                L_SV:Problems     = CLIP(L_SV:Problems) & ' (Invoice for DI not found)'
             .
             LOC:Loading_Problems = TRUE
          ELSE
             IF CLIP(L_SV:Problems) = ''
                L_SV:Problems   = L_SV:ID
             ELSE
                L_SV:Problems   = CLIP(L_SV:Problems) & ', ' & L_SV:ID
             .
             L_SV:Problems      = CLIP(L_SV:Problems) & ' (DI not found)'
             LOC:Loading_Problems = TRUE
          .
       ELSE
          INV:IID               = L_SV:ID
          IF Access:_Invoice.TryFetch(INV:PKey_IID) = LEVEL:Benign
             DO Get_Inv_Allocate
          ELSE
             IF CLIP(L_SV:Problems) = ''
                L_SV:Problems   = L_SV:ID
             ELSE
                L_SV:Problems   = CLIP(L_SV:Problems) & ', ' & L_SV:ID
             .
             L_SV:Problems      = CLIP(L_SV:Problems) & ' (Invoice not found)'
             LOC:Loading_Problems = TRUE
    .  .  .

    ThisWindow.Reset(1)
    DISPLAY
    EXIT




Get_Inv_Allocate                  ROUTINE
    DATA
R:Allocate          LIKE(L_SV:UnAllocated_Amt)
R:Outstanding       LIKE(L_SV:UnAllocated_Amt)
R:Found             BYTE

    CODE
    ! Check this invoice is not already allocated here...
    ! October 2 - not sure how this check was intended to work.  Need to itterate through all instances of this
    ! invoice ID in the ClientsPaymentsAllocation table to be check...  or itterate through all invoices allocated
    ! to this payment to look for this IID.  One of the 2.
    R:Found                 = FALSE

    CLEAR(CLIPA:Record,-1)
    CLIPA:IID               = INV:IID
    SET(CLIPA:Fkey_IID, CLIPA:Fkey_IID)
    LOOP
       IF Access:ClientsPaymentsAllocation.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF CLIPA:IID ~= INV:IID
          BREAK
       .
       IF CLIPA:CPID = CLIP:CPID
          R:Found           = TRUE
          BREAK
    .  .


    !CLIPA:CPAID             = INV:IID
    IF R:Found = TRUE
       IF CLIP(L_SV:Problems) = ''
          L_SV:Problems     = 'DIs: ' & L_SV:ID
       ELSE
          L_SV:Problems     = CLIP(L_SV:Problems) & ', ' & L_SV:ID
       .
       L_SV:Problems        = CLIP(L_SV:Problems) & ' (Invoice already allocated here.)'
       LOC:Loading_Problems = TRUE
    ELSE
!    db.debugout('[Update_ClientsPayments - Add_From_Tags]  Get...Outstand..')

       ! Checks Invoice for amts and Creates allocation entry
       R:Outstanding           = DEFORMAT(Get_Inv_Outstanding(INV:IID))

       IF R:Outstanding <= 0.0                                         ! Nothing to allocate
          IF CLIP(L_SV:Problems) = ''
             L_SV:Problems     = 'DIs: ' & L_SV:ID
          ELSE
             L_SV:Problems     = CLIP(L_SV:Problems) & ', ' & L_SV:ID
          .
          IF L_SV:Bulk_Option  = 0                     ! DIs
             L_SV:Problems     = CLIP(L_SV:Problems) & ' (No outstanding on Invoice for DI)'
          ELSE
             L_SV:Problems     = CLIP(L_SV:Problems) & ' (No outstanding on Invoice)'
          .
          LOC:Loading_Problems = TRUE
       ELSE
          IF R:Outstanding < L_SV:ToAlloc                                 ! Can allocate full amount
             R:Allocate        = R:Outstanding
          ELSE
             R:Allocate        = L_SV:ToAlloc
          .

          ! Create allocation record
          CLEAR(CLIPA:Record)
          CLIPA:CPID               = CLIP:CPID
          IF Access:ClientsPaymentsAllocation.TryPrimeAutoInc() ~= LEVEL:Benign
             IF CLIP(L_SV:Problems) = ''
                L_SV:Problems  = 'DIs: ' & L_SV:ID
             ELSE
                L_SV:Problems  = CLIP(L_SV:Problems) & ', ' & L_SV:ID
             .
             L_SV:Problems     = CLIP(L_SV:Problems) & ' (Client Payment could not be Primed)'
             LOC:Loading_Problems = TRUE
          ELSE
             !CLIPA:CPAID
             !CLIPA:CPID            =
             !CLIPA:AllocationNo    =
             CLIPA:IID             = INV:IID
             CLIPA:Amount          = R:Allocate
             CLIPA:AllocationDate  = TODAY()
             CLIPA:AllocationTime  = CLOCK()
             CLIPA:Comment         = ''

             IF Access:ClientsPaymentsAllocation.TryInsert() = LEVEL:Benign             ! Added
                L_SV:ToAlloc      -= R:Allocate                                         ! Reduce available

                Upd_Invoice_Paid_Status(INV:IID)
             ELSE
                IF CLIP(L_SV:Problems) = ''
                   L_SV:Problems   = 'DIs: ' & L_SV:ID
                ELSE
                   L_SV:Problems   = CLIP(L_SV:Problems) & ', ' & L_SV:ID
                .
                L_SV:Problems      = CLIP(L_SV:Problems) & ' (Client Payment could not be Inserted)'
                LOC:Loading_Problems = TRUE
                Access:ClientsPaymentsAllocation.CancelAutoInc()
    .  .  .  .
    EXIT
Add_From_Tags                   ROUTINE
    DATA
R:Idx           ULONG

    CODE
    LOC:Loading_Problems        = FALSE

    IF Inv_Tags.NumberTagged() > 0
       L_SV:ToAlloc             = L_SV:UnAllocated_Amt

       LOOP
          R:Idx    += 1
          GET(Inv_Tags.TagQueue, R:Idx)
          IF ERRORCODE()
             BREAK
          .

          INV:IID               = Inv_Tags.TagQueue.Ptr

          IF Access:_Invoice.TryFetch(INV:PKey_IID) = LEVEL:Benign
             L_SV:ID            = INV:DINo              ! Used to error notes

             DO Get_Inv_Allocate
          ELSE
             IF CLIP(L_SV:Problems) = ''
                L_SV:Problems   = 'IID: ' & Inv_Tags.TagQueue.Ptr
             ELSE
                L_SV:Problems   = CLIP(L_SV:Problems) & ', IID: ' & Inv_Tags.TagQueue.Ptr
             .
             L_SV:Problems      = CLIP(L_SV:Problems) & ' (Tagged Invoice not found!)'
       .  .

       ThisWindow.Reset(1)

       IF LOC:Loading_Problems = TRUE
          MESSAGE('Some problems were encountered.||' & |
                  'Please check the Problems information box under the Bulk Allocation tab.', 'Invoice Allocation', ICON:Exclamation)
    .  .
    EXIT
Check_Payment_Allocations           ROUTINE
    CLEAR(A_CLIPA:Record,-1)
    A_CLIPA:CPID    = CLIP:CPID
    SET(A_CLIPA:FKey_CPID_AllocationNo, A_CLIPA:FKey_CPID_AllocationNo)
    LOOP
       IF Access:ClientsPaymentsAllocationAlias.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF A_CLIPA:CPID ~= CLIP:CPID
          BREAK
       .

       IF CLIP:DateMade > A_CLIPA:AllocationDate
          MESSAGE('An allocation has an Allocation Date which is earlier than the Date the payment was made.|Please correct this before saving this record.||Allocation no.: ' & A_CLIPA:AllocationNo, 'Payment Allocations', ICON:Exclamation)
          LOC:Dont_Save = TRUE
          BREAK
    .  .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Client Payments Record'
  OF InsertRecord
    ActionMessage = 'Client Payments Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Client Payments Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ClientsPayments')
  SELF.Request = GlobalRequest                    ! Store the incoming request
      CASE p:Type
      OF 1
         IF SELF.Request = InsertRecord AND p:CPID ~= 0
            CLEAR(CLIP:Record)
            IF Access:ClientsPayments.PrimeRecord() = LEVEL:Benign
         .  .
      OF 2
         IF p:CPID ~= 0
            CLEAR(CLIP:Record)
            CLIP:CPID     = p:CPID
            IF Access:ClientsPayments.TryFetch(CLIP:PKey_CPID) = LEVEL:Benign
      .  .  .
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CLIP:Status:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_BV:Paid_Total',L_BV:Paid_Total)         ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CLIP:Record,History::CLIP:Record)
  SELF.AddHistoryField(?CLIP:Status,15)
  SELF.AddHistoryField(?CLIP:DateMade,9)
  SELF.AddHistoryField(?CLIP:CPID,1)
  SELF.AddHistoryField(?CLIP:CID,2)
  SELF.AddHistoryField(?CLIP:DateCaptured,5)
  SELF.AddHistoryField(?CLIP:Amount,11)
  SELF.AddHistoryField(?CLIP:Notes,12)
  SELF.AddUpdateFile(Access:ClientsPayments)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Clients.Open                             ! File Clients used by this procedure, so make sure it's RelationManager is open
  Relate:ClientsPaymentsAlias.Open                ! File ClientsPaymentsAlias used by this procedure, so make sure it's RelationManager is open
  Relate:ClientsPaymentsAllocationAlias.Open      ! File ClientsPaymentsAllocationAlias used by this procedure, so make sure it's RelationManager is open
  Access:ClientsPayments.UseFile                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ClientsPayments
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.SaveControl = ?Save
  SELF.DisableCancelButton = 1
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:ClientsPaymentsAllocation,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    DISABLE(?CLIP:Status)
    DISABLE(?CallLookup)
    ?L_SV:ClientName{PROP:ReadOnly} = True
    DISABLE(?Calendar)
    DISABLE(?Button_ChangeDate)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    ?CLIP:Amount{PROP:ReadOnly} = True
    ?L_SV:Allocated_Amount{PROP:ReadOnly} = True
    ?L_SV:UnAllocated_Amt{PROP:ReadOnly} = True
    ?L_SV:Update_Alloc_Amount{PROP:ReadOnly} = True
    DISABLE(?Button_Update)
    DISABLE(?Button_TagInvoices)
    DISABLE(?L_SV:Bulk_Option)
    ?L_SV:UnAllocated_Amt:2{PROP:ReadOnly} = True
    DISABLE(?Button_Allocate)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.FileLoaded = 1                             ! This is a 'file loaded' browse
  BRW2.AddSortOrder(,CLIPA:FKey_CPID)             ! Add the sort order for CLIPA:FKey_CPID for sort order 1
  BRW2.AddRange(CLIPA:CPID,CLIP:CPID)             ! Add single value range limit for sort order 1
  BRW2.AddLocator(BRW2::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW2::Sort0:Locator.Init(,CLIPA:CPID,1,BRW2)    ! Initialize the browse locator using  using key: CLIPA:FKey_CPID , CLIPA:CPID
  BRW2.AppendOrder('+CLIPA:AllocationNo')         ! Append an additional sort order
  BRW2.AddField(CLIPA:AllocationNo,BRW2.Q.CLIPA:AllocationNo) ! Field CLIPA:AllocationNo is a hot field or requires assignment from browse
  BRW2.AddField(CLIPA:AllocationDate,BRW2.Q.CLIPA:AllocationDate) ! Field CLIPA:AllocationDate is a hot field or requires assignment from browse
  BRW2.AddField(CLIPA:Amount,BRW2.Q.CLIPA:Amount) ! Field CLIPA:Amount is a hot field or requires assignment from browse
  BRW2.AddField(CLIPA:Comment,BRW2.Q.CLIPA:Comment) ! Field CLIPA:Comment is a hot field or requires assignment from browse
  BRW2.AddField(CLIPA:IID,BRW2.Q.CLIPA:IID)       ! Field CLIPA:IID is a hot field or requires assignment from browse
  BRW2.AddField(INV:DINo,BRW2.Q.INV:DINo)         ! Field INV:DINo is a hot field or requires assignment from browse
  BRW2.AddField(INV:Total,BRW2.Q.INV:Total)       ! Field INV:Total is a hot field or requires assignment from browse
  BRW2.AddField(L_BV:Paid_Total,BRW2.Q.L_BV:Paid_Total) ! Field L_BV:Paid_Total is a hot field or requires assignment from browse
  BRW2.AddField(CLIPA:CPAID,BRW2.Q.CLIPA:CPAID)   ! Field CLIPA:CPAID is a hot field or requires assignment from browse
  BRW2.AddField(CLIPA:CPID,BRW2.Q.CLIPA:CPID)     ! Field CLIPA:CPID is a hot field or requires assignment from browse
  BRW2.AddField(INV:IID,BRW2.Q.INV:IID)           ! Field INV:IID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_ClientsPayments',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 2                           ! Will call: Update_ClientsPaymentsAllocation
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_ClientsPayments',1,?Browse:2,2,BRW2::PopupTextExt,Queue:Browse:2,8,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
      IF SELF.Request = ChangeRecord
         LOC:Orig_Amt                 = CLIP:Amount
         DISABLE(?Group_DateMade)
      ELSIF SELF.Request = InsertRecord
         ! (p:Type, p:CPID)
         IF p:Type = 1                        ! Reversal entry
            IF p:CPID ~= 0
               ! Check if we have a reversal entry for this already
               A_CLIP:CPID_Reversal               = p:CPID
               IF Access:ClientsPaymentsAlias.TryFetch(A_CLIP:Key_CPIDReversal) = LEVEL:Benign
                  IF Access:ClientsPayments.CancelAutoInc() ~= LEVEL:Benign
                  .
                  MESSAGE('This Payment has already been credited.||See Payment ID: ' & A_CLIP:CPID, 'Update Clients Payments - Reversal', ICON:Exclamation)
                  RETURN(LEVEL:Fatal)
               .
  
               DISABLE(?GroupClient)
  
               A_CLIP:CPID    = p:CPID
               IF Access:ClientsPaymentsAlias.TryFetch(A_CLIP:PKey_CPID) = LEVEL:Benign
                  IF A_CLIP:Type = 1                            ! This is a reversal then
                     IF Access:ClientsPayments.CancelAutoInc() ~= LEVEL:Benign
                     .
                     MESSAGE('Cannot reverse a reversal.||Payment ID ' & A_CLIP:CPID & ' is a reversal.', 'Update Clients Payments - Reversal', ICON:Exclamation)
                     RETURN(LEVEL:Fatal)
                  .
  
                  CLIP:CID                        = A_CLIP:CID
                  CLIP:CPID_Reversal              = p:CPID
  
                  CLIP:Amount                     = -A_CLIP:Amount
  
                  ?CLIP:Amount{PROP:Background}   = 0E9E9E9H
                  ?CLIP:Amount{PROP:ReadOnly}     = TRUE
                  ?CLIP:Amount{PROP:Skip}         = TRUE
  
                  DO Reversal_Allocations
            .  .
  
            CLIP:Type                = 1
            DISABLE(?Group_Browse)
            DISABLE(?Save)
  
            QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Reversal'
      .  .
      CLI:CID             = CLIP:CID
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         L_SV:ClientName  = CLI:ClientName
      .
      LOC:CPID    = CLIP:CPID
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
    Relate:ClientsPaymentsAlias.Close
    Relate:ClientsPaymentsAllocationAlias.Close
  END
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_ClientsPayments',QuickWindow)    ! Save window data to non-volatile store
  END
      Upd_ClientPayments_Status(LOC:CPID)
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Clients
      Update_ClientsPaymentsAllocation
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_TagInvoices
          Inv_Tags.ClearAllTags()
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = L_SV:ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SV:ClientName = CLI:ClientName
        CLIP:CID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?L_SV:ClientName
      IF L_SV:ClientName OR ?L_SV:ClientName{PROP:Req}
        CLI:ClientName = L_SV:ClientName
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SV:ClientName = CLI:ClientName
            CLIP:CID = CLI:CID
          ELSE
            CLEAR(CLIP:CID)
            SELECT(?L_SV:ClientName)
            CYCLE
          END
        ELSE
          CLIP:CID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
      Calendar10.SelectOnClose = True
      Calendar10.Ask('Select a Date',CLIP:DateMade)
      IF Calendar10.Response = RequestCompleted THEN
      CLIP:DateMade=Calendar10.SelectedDate
      DISPLAY(?CLIP:DateMade)
      END
      ThisWindow.Reset(True)
    OF ?Button_ChangeDate
      ThisWindow.Update()
          ENABLE(?Group_DateMade)
    OF ?CLIP:Amount
          IF SELF.Request = ChangeRecord
             IF LOC:Orig_Amt ~= CLIP:Amount
                DISABLE(?Group_Browse)
                ENABLE(?Save)
          .  .
      
      
          L_SV:UnAllocated_Amt    = CLIP:Amount - L_SV:Allocated_Amount
          DISPLAY(?L_SV:UnAllocated_Amt)
      
    OF ?L_SV:Update_Alloc_Amount
          IF L_SV:Update_Alloc_Amount < 0.0
             L_SV:Update_Alloc_Amount     = 0.0
             DISPLAY(?L_SV:Update_Alloc_Amount)
          .
    OF ?Button_Update
      ThisWindow.Update()
          IF CLIPA:CPAID = 0
             MESSAGE('Please select an Invoice Payment to allocate.', 'Update Clients Payments', ICON:Exclamation)
          ELSE
             CLIPA:CPAID          = L_SV:CPAID_Selected
             IF Access:ClientsPaymentsAllocation.TryFetch(CLIPA:PKey_CPAID) = LEVEL:Benign
                ! Check that payment doesn't exceed the invoice total
                IF L_SV:Update_Alloc_Amount > Get_Inv_Outstanding(CLIPA:IID) + CLIPA:Amount
                   MESSAGE('The Payment Allocation of ' & FORMAT(L_SV:Update_Alloc_Amount,@n12.2) & ' would be greater than the outstanding amount owed.', 'Update Clients Payments', ICON:Exclamation)
                   L_SV:Update_Alloc_Amount   = Get_Inv_Outstanding(CLIPA:IID) + CLIPA:Amount
                   DISPLAY(?L_SV:Update_Alloc_Amount)
                ELSE
                   ! Then we can allocate the new amount, otherwise we must deny this update
                   CLIPA:Amount      = L_SV:Update_Alloc_Amount
                   IF Access:ClientsPaymentsAllocation.TryUpdate() = LEVEL:Benign
                      Upd_Invoice_Paid_Status(INV:IID)
      
                      ThisWindow.Reset(1)
                   ELSE
                      MESSAGE('Invoice Payment could not be updated.', 'Update Clients Payments', ICON:Exclamation)
                .  .
             ELSE
                MESSAGE('Invoice Payment could not be fetched.', 'Update Clients Payments', ICON:Exclamation)
          .  .
    OF ?Save
      ThisWindow.Update()
          ENABLE(?Group_Browse)
          DISABLE(?Save)
    OF ?Button_TagInvoices
      ThisWindow.Update()
      Browse_Invoice(CLIP:CID, 1, Inv_Tags)
      ThisWindow.Reset
          DO Add_From_Tags
      
          BRW2.ResetFromFile()
    OF ?Button_Allocate
      ThisWindow.Update()
          DO Allocate_Bulk
    OF ?OK
      ThisWindow.Update()
          LOC:Dont_Save   = FALSE
          DO Check_Payment_Allocations
          IF LOC:Dont_Save = TRUE
             QuickWindow{PROP:AcceptAll}  = FALSE
             SELECT(?)
             CYCLE
          .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:2
          BRW2.UpdateViewRecord()
      
          L_SV:CPAID_Selected         = CLIPA:CPAID
          L_SV:Update_Alloc_Amount    = CLIPA:Amount
          DISPLAY(?L_SV:Update_Alloc_Amount)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.ResetFromView PROCEDURE

L_SV:Allocated_Amount:Sum REAL                             ! Sum variable for browse totals
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:ClientsPaymentsAllocation.SetQuickScan(1)
  SELF.Reset
  IF SELF.UseMRP
     IF SELF.View{PROP:IPRequestCount} = 0
          SELF.View{PROP:IPRequestCount} = 60
     END
  END
  LOOP
    IF SELF.UseMRP
       IF SELF.View{PROP:IPRequestCount} = 0
            SELF.View{PROP:IPRequestCount} = 60
       END
    END
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      SETCURSOR()
      RETURN
    END
    SELF.SetQueueRecord
    L_SV:Allocated_Amount:Sum += CLIPA:Amount
  END
  SELF.View{PROP:IPRequestCount} = 0
  L_SV:Allocated_Amount = L_SV:Allocated_Amount:Sum
  PARENT.ResetFromView
  Relate:ClientsPaymentsAllocation.SetQuickScan(0)
  SETCURSOR()
      L_SV:UnAllocated_Amt    = CLIP:Amount - L_SV:Allocated_Amount
      DISPLAY(?L_SV:UnAllocated_Amt)
  


BRW2.SetQueueRecord PROCEDURE

  CODE
      L_BV:Paid_Total             = Get_ClientsPay_Alloc_Amt( CLIPA:IID, 0 )
  
  PARENT.SetQueueRecord
  


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_AllocStatus, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_AllocStatus

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_ClientsPaymentsAllocation PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Invoice_Group    GROUP,PRE(L_IG)                       ! 
ClientName           STRING(35)                            ! 
ConsigneeName        STRING(35)                            ! Name of this address
Total                DECIMAL(10,2)                         ! 
DINo                 ULONG                                 ! Delivery Instruction Number
Terms                BYTE                                  ! Terms - Pre Paid, COD, Account
Owing                DECIMAL(10,2)                         ! 
Paid                 DECIMAL(10,2)                         ! Paid to date (allocated to this Invoice)
PayRemaining         DECIMAL(10,2)                         ! 
OrigRemaining        DECIMAL(10,2)                         ! 
OrigPaid             DECIMAL(10,2)                         ! 
OrigAmt              DECIMAL(10,2)                         ! 
Inv_Remaining        DECIMAL(10,2)                         ! 
                     END                                   ! 
LOC:Reversal         BYTE                                  ! 
History::CLIPA:Record LIKE(CLIPA:RECORD),THREAD
QuickWindow          WINDOW('Form Client Payment Allocation'),AT(,,203,318),FONT('Tahoma',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,IMM,MDI,HLP('UpdateClientsPaymentsAllocation'),SYSTEM
                       SHEET,AT(4,4,195,296),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('CPID:'),AT(134,6),USE(?CLIPA:CPID:Prompt),TRN
                           STRING(@n_10),AT(157,6,,10),USE(CLIPA:CPID),RIGHT(1),TRN
                           PROMPT('Allocation No.:'),AT(9,22),USE(?CLIPA:AllocationNo:Prompt),TRN
                           STRING(@n6),AT(125,22,64,10),USE(CLIPA:AllocationNo),RIGHT(1),TRN
                           PROMPT('Invoice No.:'),AT(9,36),USE(?CLIPA:IID:Prompt:2),TRN
                           BUTTON('...'),AT(106,36,13,10),USE(?CallLookup)
                           ENTRY(@n_10),AT(125,36,64,10),USE(CLIPA:IID),RIGHT(1),REQ,MSG('Invoice Number')
                           BUTTON('...'),AT(106,52,13,10),USE(?CallLookup:2)
                           ENTRY(@n_10),AT(125,52,64,10),USE(L_IG:DINo),RIGHT(1),COLOR(00E9E9E9h),MSG('Delivery In' & |
  'struction Number'),READONLY,SKIP,TIP('Delivery Instruction Number')
                           PANEL,AT(9,68,187,122),USE(?Panel1),BEVEL(-1,-1)
                           PROMPT('Client Name:'),AT(13,74),USE(?INV:ClientName:Prompt)
                           ENTRY(@s35),AT(81,74,108,10),USE(L_IG:ClientName),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP,TIP('Client name')
                           PROMPT('Consignee Name:'),AT(13,88),USE(?INV:ConsigneeName:Prompt)
                           ENTRY(@s35),AT(81,88,108,10),USE(L_IG:ConsigneeName),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Name of th' & |
  'is address'),READONLY,SKIP,TIP('Name of this address')
                           PROMPT('DI No.:'),AT(9,52),USE(?DINo:Prompt),TRN
                           PROMPT('Terms:'),AT(13,102),USE(?Terms:Prompt)
                           LIST,AT(125,102,64,10),USE(L_IG:Terms),RIGHT(1),COLOR(00E9E9E9h),DISABLE,DROP(5),FLAT,FROM('Pre Paid|#' & |
  '0|COD|#1|Account|#2'),MSG('Terms - Pre Paid, COD, Account'),TIP('Terms - Pre Paid, C' & |
  'OD, Account')
                           PROMPT('Total:'),AT(13,122),USE(?INV:Total:Prompt)
                           ENTRY(@n-14.2),AT(125,122,64,10),USE(L_IG:Total),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                           PROMPT('Paid this Invoice (allocated):'),AT(13,136),USE(?Paid:Prompt)
                           ENTRY(@n-14.2),AT(125,136,64,10),USE(L_IG:Paid),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Paid to date (allocated to this Invoice)'),READONLY, |
  SKIP,TIP('Paid to date (allocated to this Invoice)')
                           PROMPT('Invoice Remaining:'),AT(13,152),USE(?Inv_Remaining:Prompt)
                           ENTRY(@n-14.2),AT(125,152,64,10),USE(L_IG:Inv_Remaining),DECIMAL(12),COLOR(00E9E9E9h),FLAT, |
  READONLY,SKIP
                           LINE,AT(10,168,180,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Payment Unallocated:'),AT(13,174),USE(?Remaining:Prompt)
                           ENTRY(@n-14.2),AT(125,174,64,10),USE(L_IG:PayRemaining),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP
                           STRING(''),AT(9,196,112,10),USE(?String_Msg),FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI),CENTER, |
  TRN
                           CHECK(' Reversal'),AT(125,196),USE(LOC:Reversal),TRN
                           PROMPT('Allocate Amount:'),AT(9,210),USE(?CLIPA:Amount:Prompt),TRN
                           BUTTON('Allocate Max'),AT(66,210,,10),USE(?Button_Max)
                           ENTRY(@n-14.2),AT(125,210,64,10),USE(CLIPA:Amount),DECIMAL(12),MSG('Amount of payment'),TIP('Amount of ' & |
  'payment allocated to this Invoice')
                           PROMPT('Allocation Date:'),AT(9,224),USE(?CLIPA:AllocationDate:Prompt),TRN
                           SPIN(@d17),AT(125,224,64,10),USE(CLIPA:AllocationDate),RIGHT(1),MSG('Date allocation made'), |
  TIP('Date allocation made')
                           PROMPT('Comment:'),AT(9,236),USE(?CLIPA:AllocationDate:Prompt:2),TRN
                         END
                       END
                       BUTTON('&OK'),AT(98,302,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(150,302,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       TEXT,AT(9,248,180,47),USE(CLIPA:Comment),VSCROLL,BOXED,MSG('Comment')
                       BUTTON('&Help'),AT(74,2,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
New_Inv                 ROUTINE
    ThisWindow.Update

    INV:IID = CLIPA:IID
    IF Access:_Invoice.TryFetch(INV:PKey_IID) = LEVEL:Benign
        CLIPA:IID           = INV:IID
        L_IG:ClientName     = INV:ClientName
        L_IG:ConsigneeName  = INV:ConsigneeName
        L_IG:Total          = INV:Total
    ELSE
       db.debugout('***  clear Inv rec (' & CLIPA:IID & ') ***  - error: ' & Access:_Invoice.GetError() & GlobalErrors.GetError(Access:_Invoice.GetError()))
       
!        CLEAR(CLIPA:IID)
        CLEAR(L_IG:ClientName)
        CLEAR(L_IG:ConsigneeName)
        CLEAR(L_IG:Total)
    END

    IF LOC:Reversal = TRUE
       IF CLIPA:IID ~= 0
          L_IG:OrigPaid            = Get_ClientsPay_Alloc_Amt( CLIPA:IID, 0 )

          L_IG:Paid                = L_IG:OrigPaid - L_IG:OrigAmt + CLIPA:Amount

          L_IG:Owing               = L_IG:Total - L_IG:Paid

          L_IG:PayRemaining        = L_IG:OrigRemaining + L_IG:OrigAmt - CLIPA:Amount
          L_IG:Paid                = L_IG:OrigPaid - L_IG:OrigAmt + CLIPA:Amount

          L_IG:Inv_Remaining       = L_IG:Total - L_IG:Paid
       .
    ELSE
       ! Remaining will exclude any original amt on this allocation
       L_IG:OrigRemaining          = CLIP:Amount - Get_ClientsPay_Alloc_Amt( CLIP:CPID, 1 )
       ! If payment specified is greater than the amt we have left to allocate then reduce to this
       IF CLIPA:Amount > (L_IG:OrigRemaining + L_IG:OrigAmt)
          CLIPA:Amount             = L_IG:OrigRemaining + L_IG:OrigAmt
       .

       L_IG:PayRemaining           = L_IG:OrigRemaining + L_IG:OrigAmt - CLIPA:Amount

       IF CLIPA:IID ~= 0
          L_IG:OrigPaid            = Get_ClientsPay_Alloc_Amt( CLIPA:IID, 0 )

          ! If the amt paid now is greater than the amt to pay then set to this amount
          IF CLIPA:Amount > L_IG:Total - (L_IG:OrigPaid - L_IG:OrigAmt)
             CLIPA:Amount          = L_IG:Total - (L_IG:OrigPaid - L_IG:OrigAmt)
          .

          L_IG:Paid                = L_IG:OrigPaid - L_IG:OrigAmt + CLIPA:Amount

          L_IG:Owing               = L_IG:Total - L_IG:Paid

          IF CLIPA:Amount = 0.0
             ! Allocate the maximium available or max owing if smaller than available
             IF L_IG:Owing > L_IG:PayRemaining
                CLIPA:Amount       = L_IG:PayRemaining
             ELSE
                CLIPA:Amount       = L_IG:Owing
             .
          ELSE
          .

          L_IG:PayRemaining        = L_IG:OrigRemaining + L_IG:OrigAmt - CLIPA:Amount
          L_IG:Paid                = L_IG:OrigPaid - L_IG:OrigAmt + CLIPA:Amount

          L_IG:Inv_Remaining       = L_IG:Total - L_IG:Paid
       .

       IF L_IG:PayRemaining <= 0
          ?String_Msg{PROP:Text}  = 'Zero available to allocate.'
       ELSE
          ?String_Msg{PROP:Text}  = ''
    .  .

    db.debugout('[Get_ClientsPay_Alloc_Amt]  - New_Inv')
    ThisWindow.Reset(1)
    DISPLAY
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ClientsPaymentsAllocation')
      db.debugout('Update_ClientsPaymentsAllocation')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CLIPA:CPID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CLIPA:Record,History::CLIPA:Record)
  SELF.AddHistoryField(?CLIPA:CPID,2)
  SELF.AddHistoryField(?CLIPA:AllocationNo,3)
  SELF.AddHistoryField(?CLIPA:IID,4)
  SELF.AddHistoryField(?CLIPA:Amount,5)
  SELF.AddHistoryField(?CLIPA:AllocationDate,8)
  SELF.AddHistoryField(?CLIPA:Comment,10)
  SELF.AddUpdateFile(Access:ClientsPaymentsAllocation)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ClientsPayments.Open                              ! File ClientsPayments used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ClientsPaymentsAllocation
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    DISABLE(?CallLookup:2)
    ?L_IG:DINo{PROP:ReadOnly} = True
    ?L_IG:ClientName{PROP:ReadOnly} = True
    ?L_IG:ConsigneeName{PROP:ReadOnly} = True
    DISABLE(?L_IG:Terms)
    ?L_IG:Total{PROP:ReadOnly} = True
    ?L_IG:Paid{PROP:ReadOnly} = True
    ?L_IG:Inv_Remaining{PROP:ReadOnly} = True
    ?L_IG:PayRemaining{PROP:ReadOnly} = True
    DISABLE(?Button_Max)
    ?CLIPA:Amount{PROP:ReadOnly} = True
  END
      db.debugout('[Get_ClientsPay_Alloc_Amt]  - before 1.3')
  
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_ClientsPaymentsAllocation',QuickWindow) ! Restore window settings from non-volatile store
      db.debugout('[Get_ClientsPay_Alloc_Amt]  - before 1.5')
  
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ClientsPayments.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_ClientsPaymentsAllocation',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Invoice(CLIP:CID)
      Browse_Deliveries('','')
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      INV:IID = CLIPA:IID
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        CLIPA:IID = INV:IID
        L_IG:ClientName = INV:ClientName
        L_IG:ConsigneeName = INV:ConsigneeName
        L_IG:Total = INV:Total
      END
      ThisWindow.Reset(1)
          DO New_Inv
    OF ?CLIPA:IID
      IF FALSE
      
      IF CLIPA:IID OR ?CLIPA:IID{PROP:Req}
        INV:IID = CLIPA:IID
        IF Access:_Invoice.TryFetch(INV:PKey_IID)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            CLIPA:IID = INV:IID
            L_IG:ClientName = INV:ClientName
            L_IG:ConsigneeName = INV:ConsigneeName
            L_IG:Total = INV:Total
          ELSE
            CLEAR(L_IG:ClientName)
            CLEAR(L_IG:ConsigneeName)
            CLEAR(L_IG:Total)
            SELECT(?CLIPA:IID)
            CYCLE
          END
        ELSE
          L_IG:ClientName = INV:ClientName
          L_IG:ConsigneeName = INV:ConsigneeName
          L_IG:Total = INV:Total
        END
      END
      ThisWindow.Reset()
        .

    INV:IID = CLIPA:IID
    IF Access:_Invoice.TryFetch(INV:PKey_IID)
        CLIPA:IID = INV:IID
        CLIPA:IID = INV:IID
        L_IG:ClientName = INV:ClientName
        L_IG:ConsigneeName = INV:ConsigneeName
        L_IG:Total = INV:Total
        message('invoice fetched - client: ' & CLIP(INV:ClientName))
    ELSE
!        CLEAR(CLIPA:IID)
        CLEAR(L_IG:ClientName)
        CLEAR(L_IG:ConsigneeName)
        CLEAR(L_IG:Total)
    END

          DO New_Inv
    OF ?CallLookup:2
      ThisWindow.Update()
      DEL:DINo = L_IG:DINo
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_IG:DINo = DEL:DINo
      END
      ThisWindow.Reset(1)
    OF ?L_IG:DINo
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_IG:DINo OR ?L_IG:DINo{PROP:Req}
          DEL:DINo = L_IG:DINo
          IF Access:Deliveries.TryFetch(DEL:Key_DINo)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              L_IG:DINo = DEL:DINo
            ELSE
              SELECT(?L_IG:DINo)
              CYCLE
            END
          END
        END
      END
      ThisWindow.Reset()
    OF ?Button_Max
      ThisWindow.Update()
          IF L_IG:Owing > (L_IG:PayRemaining + CLIPA:Amount)
             CLIPA:Amount      += L_IG:PayRemaining
          ELSE
             CLIPA:Amount       = L_IG:Owing
          .
      
          DISPLAY
          DO New_Inv
    OF ?CLIPA:Amount
          DO New_Inv
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
          IF LOC:Reversal = TRUE
             IF CLIPA:Amount > 0.0
                MESSAGE('Cannot allocate an amount of more than zero on a reversal.', 'Amount Allocation', ICON:Hand)
                QuickWindow{PROP:AcceptAll}   = FALSE
                SELECT(?CLIPA:Amount)
                CYCLE
             .
          ELSE
             IF CLIPA:Amount <= 0.0
                MESSAGE('Cannot allocate an amount of zero or less.', 'Amount Allocation', ICON:Hand)
                QuickWindow{PROP:AcceptAll}   = FALSE
                SELECT(?CLIPA:Amount)
                CYCLE
          .  .
          ! Check allocation is not into a previous statement period - NOT DONE
          
          Upd_Invoice_Paid_Status(CLIPA:IID)
          CLIPA:StatusUpToDate    = FALSE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          db.debugout('[Get_ClientsPay_Alloc_Amt]  - CLIPA:IID: ' & CLIPA:IID)
      
          INV:IID                 = CLIPA:IID
          IF Access:_Invoice.TryFetch(INV:PKey_IID) = LEVEL:Benign
             L_IG:ClientName      = INV:ClientName
             L_IG:ConsigneeName   = INV:ConsigneeName
             L_IG:Total           = INV:Total
      
             L_IG:OrigPaid        = Get_ClientsPay_Alloc_Amt( CLIPA:IID, 0 )
      
          db.debugout('[Get_ClientsPay_Alloc_Amt]  - L_IG:OrigPaid: ' & L_IG:OrigPaid)
          .
      
          L_IG:OrigAmt            = CLIPA:Amount
          IF CLIPA:Amount < 0.0
             LOC:Reversal         = TRUE
          .
      
          db.debugout('[Get_ClientsPay_Alloc_Amt]  - before')
      
          L_IG:OrigRemaining      = CLIP:Amount - Get_ClientsPay_Alloc_Amt( CLIP:CPID, 1 )
      
          db.debugout('[Get_ClientsPay_Alloc_Amt]  - after')
      
          DO New_Inv
      
          db.debugout('[Get_ClientsPay_Alloc_Amt]  - then')
          IF SELF.Request = InsertRecord
             L_IG:DINo{PROP:Skip}     = FALSE
             L_IG:DINo{PROP:ReadOnly} = FALSE
             !L_IG:DINo{PROP:ReadOnly} = FALSE
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Invoice PROCEDURE (p:CID, p:Tagging, <shpTagClass p_Client_Tags>, p:MID)

CurrentTab           STRING(80)                            ! 
LOC:BranchName       STRING(35)                            ! Branch Name
LOC:Terms            STRING(20)                            ! Terms - Pre Paid, COD, Account
LOC:Status           STRING(20)                            ! Fully paid invoice - used for filtering - No Payments, Partially Paid, Fully Paid
LOC:Locator          STRING(35)                            ! 
LOC:Filter_Group     GROUP,PRE()                           ! 
L_FG:Invoice_Status  BYTE                                  ! used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
L_FG:Common_Date     LONG                                  ! 
                     END                                   ! 
LOC:User_Access      LONG                                  ! 
L_BI:Browse_Items    GROUP,PRE()                           ! 
L_BI:Outstanding     DECIMAL(15,2)                         ! Total Outstanding
L_BI:Credited        DECIMAL(15,2)                         ! 
L_BI:Payments        DECIMAL(15,2)                         ! 
                     END                                   ! 
LOC:Dates            GROUP,PRE(L_DG)                       ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
LOC:To_Date_Filter   DATE                                  ! 
LOC:Selected_Clients BYTE                                  ! 
LOC:Screen_Group     GROUP,PRE(LO_SG)                      ! 
Invoice_Details      BYTE                                  ! Show Invoice details
                     END                                   ! 
LOC:MID              ULONG                                 ! Manifest ID
BRW1::View:Browse    VIEW(_Invoice)
                       PROJECT(INV:IID)
                       PROJECT(INV:ClientName)
                       PROJECT(INV:ClientNo)
                       PROJECT(INV:DINo)
                       PROJECT(INV:InvoiceDate)
                       PROJECT(INV:Weight)
                       PROJECT(INV:Total)
                       PROJECT(INV:Insurance)
                       PROJECT(INV:Documentation)
                       PROJECT(INV:FuelSurcharge)
                       PROJECT(INV:TollCharge)
                       PROJECT(INV:FreightCharge)
                       PROJECT(INV:VAT)
                       PROJECT(INV:MIDs)
                       PROJECT(INV:MID)
                       PROJECT(INV:BadDebt)
                       PROJECT(INV:BranchName)
                       PROJECT(INV:VolumetricWeight)
                       PROJECT(INV:Printed)
                       PROJECT(INV:ShipperName)
                       PROJECT(INV:ConsigneeName)
                       PROJECT(INV:CR_IID)
                       PROJECT(INV:InvoiceTime)
                       PROJECT(INV:IJID)
                       PROJECT(INV:BID)
                       PROJECT(INV:DID)
                       PROJECT(INV:ICID)
                       PROJECT(INV:CID)
                       PROJECT(INV:Terms)
                       PROJECT(INV:Status)
                       JOIN(BRA:PKey_BID,INV:IID)
                         PROJECT(BRA:BID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
INV:IID                LIKE(INV:IID)                  !List box control field - type derived from field
INV:IID_Style          LONG                           !Field style
INV:ClientName         LIKE(INV:ClientName)           !List box control field - type derived from field
INV:ClientName_Style   LONG                           !Field style
INV:ClientNo           LIKE(INV:ClientNo)             !List box control field - type derived from field
INV:ClientNo_Style     LONG                           !Field style
INV:DINo               LIKE(INV:DINo)                 !List box control field - type derived from field
INV:DINo_Style         LONG                           !Field style
INV:InvoiceDate        LIKE(INV:InvoiceDate)          !List box control field - type derived from field
INV:Weight             LIKE(INV:Weight)               !List box control field - type derived from field
L_BI:Outstanding       LIKE(L_BI:Outstanding)         !List box control field - type derived from local data
L_BI:Outstanding_Style LONG                           !Field style
INV:Total              LIKE(INV:Total)                !List box control field - type derived from field
L_BI:Payments          LIKE(L_BI:Payments)            !List box control field - type derived from local data
L_BI:Payments_Style    LONG                           !Field style
L_BI:Credited          LIKE(L_BI:Credited)            !List box control field - type derived from local data
L_BI:Credited_Style    LONG                           !Field style
INV:Insurance          LIKE(INV:Insurance)            !List box control field - type derived from field
INV:Documentation      LIKE(INV:Documentation)        !List box control field - type derived from field
INV:FuelSurcharge      LIKE(INV:FuelSurcharge)        !List box control field - type derived from field
INV:TollCharge         LIKE(INV:TollCharge)           !List box control field - type derived from field
INV:FreightCharge      LIKE(INV:FreightCharge)        !List box control field - type derived from field
INV:VAT                LIKE(INV:VAT)                  !List box control field - type derived from field
INV:MIDs               LIKE(INV:MIDs)                 !List box control field - type derived from field
INV:MID                LIKE(INV:MID)                  !List box control field - type derived from field
INV:BadDebt            LIKE(INV:BadDebt)              !List box control field - type derived from field
INV:BadDebt_Icon       LONG                           !Entry's icon ID
INV:BranchName         LIKE(INV:BranchName)           !List box control field - type derived from field
LOC:Status             LIKE(LOC:Status)               !List box control field - type derived from local data
LOC:Status_Style       LONG                           !Field style
LOC:Terms              LIKE(LOC:Terms)                !List box control field - type derived from local data
INV:VolumetricWeight   LIKE(INV:VolumetricWeight)     !List box control field - type derived from field
INV:Printed            LIKE(INV:Printed)              !List box control field - type derived from field
INV:Printed_Icon       LONG                           !Entry's icon ID
INV:ShipperName        LIKE(INV:ShipperName)          !List box control field - type derived from field
INV:ConsigneeName      LIKE(INV:ConsigneeName)        !List box control field - type derived from field
INV:CR_IID             LIKE(INV:CR_IID)               !List box control field - type derived from field
INV:InvoiceTime        LIKE(INV:InvoiceTime)          !List box control field - type derived from field
INV:IJID               LIKE(INV:IJID)                 !List box control field - type derived from field
INV:BID                LIKE(INV:BID)                  !List box control field - type derived from field
INV:DID                LIKE(INV:DID)                  !List box control field - type derived from field
INV:ICID               LIKE(INV:ICID)                 !List box control field - type derived from field
INV:CID                LIKE(INV:CID)                  !List box control field - type derived from field
INV:Terms              LIKE(INV:Terms)                !Browse hot field - type derived from field
INV:Status             LIKE(INV:Status)               !Browse hot field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Invoice File'),AT(,,427,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('Browse_Invoice'),SYSTEM
                       GROUP,AT(8,20,183,10),USE(?Group_Locator)
                         PROMPT('Locator:'),AT(8,20),USE(?LOC:Locator:Prompt),TRN
                         ENTRY(@s35),AT(38,20,137,10),USE(LOC:Locator),TIP('Type here and press tab to locate')
                       END
                       LIST,AT(8,32,410,122),USE(?Browse:1),HVSCROLL,ALRT(CtrlA),ALRT(CtrlU),FORMAT('[38R(2)|FM' & |
  'Y~No. IID~C(0)@n_10@E(,0D7EBFFH,,)](37)|FM~Invoice~70L(2)|FMY~Client Name~C(0)@s100@' & |
  'E(,0D7EBFFH,,)[20R(2)|MY~No.~C(0)@n_10b@](21)|M~Client~38R(2)|MY~DI No.~C(0)@n_10b@[' & |
  '36R(2)|M~Date~C(0)@d5@]|M~Invoice~38R(2)|M~Weight~C(0)@n-14.1b@[38R(1)|MY~Outstandin' & |
  'g~L(1)@n-21.2@38R(2)|M~Total~C(0)@n-14.2@38R(1)|MY~Payments~L(1)@n-21.2@38R(1)|MY~Cr' & |
  'edited~L(1)@n-21.2@38R(2)|M~Insurance~C(0)@n-14.2@38R(2)|M~Docs.~C(0)@n-14.2@38R(2)|' & |
  'M~Fuel Surcharge~L(1)@n-14.2@38R(2)|M~Toll Charge~C(0)@n-15.2@38R(2)|M~Freight Charg' & |
  'e~L(1)@n-14.2@38R(2)|M~VAT~C(0)@n-14.2@]|M~Charges~50L(2)|M~MID(s)~C(0)@s20@[30R(2)|' & |
  'M~(Creating)~C(0)@n_10@](35)|M~MID~30R(2)|MI~Bad Debt~L(2)@p p@52L(2)|M~Branch Name~' & |
  'C(0)@s35@40L(2)|MY~Status~C(0)@s20@30L(2)|M~Terms~C(0)@s20@52R(2)|M~Volumetric Weigh' & |
  't~L(2)@n-14.1b@30R(2)|MI~Printed~L(2)@p p@60L(2)|M~Collected From~@s35@60L(2)|M~Deli' & |
  'ver(ed) To~@s35@30R(2)|M~Crd IID~C(0)@n_10b@[30R(2)|M~Time~C(0)@t7@]|M~Invoice~[30R(' & |
  '2)|M~ID~C(0)@n_10@]|M~Journal~30R(2)|M~BID~C(0)@n_10@30R(2)|M~DID~C(0)@n_10@30R(2)|M' & |
  '~ICID~C(0)@n_10@30R(2)|M~CID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the' & |
  ' _Invoice file')
                       BUTTON('&Select'),AT(160,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(214,158,49,14),USE(?View:4),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(266,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(318,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(370,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       CHECK(' Invoice Details'),AT(358,6),USE(LO_SG:Invoice_Details),MSG('Show Invoice details'), |
  TIP('Show Invoice details<0DH,0AH>Outstanding, Payments & Credited')
                       BUTTON('&Print'),AT(250,180,,14),USE(?Button_Print),LEFT,ICON('PRINT.ICO'),FLAT,HIDE
                       SHEET,AT(4,4,419,172),USE(?CurrentTab)
                         TAB('&1) By Invoice No.'),USE(?Tab:2)
                         END
                         TAB('&2) By Client'),USE(?Tab:5)
                           STRING('Tagged'),AT(302,20,115,10),USE(?String_Tagged),RIGHT(1),TRN
                           BUTTON('Select Client'),AT(9,158,,14),USE(?SelectClients),LEFT,ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                           BUTTON('Select Tag'),AT(97,158,60,14),USE(?SelectClients_All),LEFT,ICON(ICON:Tick),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                         END
                         TAB('&3) By DI No.'),USE(?Tab:6)
                         END
                         TAB('&4) By Branch'),USE(?Tab:4)
                           BUTTON('Select Branch'),AT(9,158,,14),USE(?SelectBranches),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&5) By DID'),USE(?Tab5)
                         END
                         TAB('&6) By Date'),USE(?Tab7)
                           BUTTON('&Help'),AT(87,158,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                         END
                         TAB('&7) By MID (creating)'),USE(?Tab_MID)
                           BUTTON('Select Manifest'),AT(9,158,,14),USE(?Button_SelectManifest),LEFT,ICON('WAPARENT.ICO'), |
  FLAT
                         END
                       END
                       GROUP,AT(4,184,219,10),USE(?Group_Inv_Status)
                         PROMPT('Status:'),AT(4,184),USE(?L_FG:Invoice_Status:Prompt)
                         LIST,AT(30,184,83,10),USE(L_FG:Invoice_Status),DROP(15),FROM('All|#255|No Payments|#0|P' & |
  'artially Paid|#1|Credit Notes|#2|Fully Paid - Credit|#3|Fully Paid|#4|Credit Note (s' & |
  'hown)|#5|Bad Debt (shown)|#6|Over Paid|#7'),MSG('Invoice status'),TIP('Invoice status')
                         BUTTON('Limit to Date Range'),AT(119,184,102,10),USE(?Button_DateRange),KEY(AltL),SKIP,TIP('Click here' & |
  ' (Alt-L) to limit to a specific date range')
                       END
                       BUTTON('&Print'),AT(318,180,53,14),USE(?Button_Print:2),LEFT,ICON('PRINT.ICO'),FLAT
                       BUTTON('&Close'),AT(374,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                     END

BRW1::LastSortOrder       BYTE
BRW1::SortHeader  CLASS(SortHeaderClassType) !Declare SortHeader Class
QueueResorted          PROCEDURE(STRING pString),VIRTUAL
                  END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW_Invoices         CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort4:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 4
BRW1::Sort5:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 5
BRW1::Sort8:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 7
BRW1::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW1::Sort3:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW1::Sort4:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 3
BRW1::Sort2:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 4
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

l_Client_Tags              &shpTagClass
View_Inv     VIEW(_Invoice)
    PROJECT(INV:IID)
    .

Inv_View     ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
  !------------------------------------
  !Style for ?Browse:1
  !------------------------------------
!---------------------------------------------------------------------------
Un_Tag_All                      ROUTINE
    IF p:Tagging > 0
       p_Client_Tags.ClearAllTags()
       BRW_Invoices.ResetFromBuffer()
       ?String_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()
    .
    EXIT
Tag_All                             ROUTINE
    IF p:Tagging > 0
       ! Tag All in the current sort order...
       !PUSHBIND()
       !BIND('')

       Inv_View.Init(View_Inv, Relate:_Invoice)

       CASE CHOICE(?CurrentTab)
       OF 2                        ! Delivery
       OF 3                        ! Branch
       OF 4                        ! Client                - Tag on
          Inv_View.AddSortOrder(INV:FKey_CID)
          Inv_View.AddRange(INV:CID,p:CID)
          Inv_View.AppendOrder('+INV:InvoiceDate,+INV:IID')
       OF 5                        ! Client No.
       OF 6                        ! DID
       ELSE                        ! Invoice No.
       .

       Inv_View.SetFilter('(L_FG:Invoice_Status = 255 OR (L_FG:Invoice_Status = INV:Status))')

       Inv_View.Reset()
       LOOP
          IF Inv_View.Next() ~= LEVEL:Benign
             BREAK
          .

          p_Client_Tags.MakeTag(INV:IID)
       .

       Inv_View.Kill()
   !    POPBIND()
       ?String_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()

       BRW_Invoices.ResetFromBuffer()
    .
    EXIT
Tag_From_To                  ROUTINE
    DATA
R:Forward_Backward              BYTE

    CODE
    IF p:Tagging > 0
!    R:Current_Selected_Rec_ID       = REC:Rec_ID
!
!!    MESSAGE('Selected Rec ID: ' & R:Current_Selected_Rec_ID & '|REC:TA_Date: ' & FORMAT(REC:TA_Date,@d5) & '|REC:TA_Time: ' & FORMAT(REC:TA_Time,@t4) & |
!!            '||LOC:Last_Tagged_Rec_Id: ' & LOC:Last_Tagged_Rec_Id)
!
!    ! If there is no Last Tagged then this is the only record to tag
!    IF LOC:Last_Clicked_Rec_ID ~= 0
!       LOC:Last_Tagged_Rec_Id   = LOC:Last_Clicked_Rec_ID
!    .
!
!    IF LOC:Last_Tagged_Rec_Id = 0
!       InBox_Tags.MakeTag(R:Current_Selected_Rec_ID)
!
!       LOC:Last_Tagged_Rec_Id       = R:Current_Selected_Rec_ID
!       LOC:Last_Tagged_Date         = REC:TA_Date
!       LOC:Last_Tagged_Time         = REC:TA_Time
!    ELSE
!       ! The beginning position of the loop depends on the last tagged record being before or after the
!       ! currently selected one.
!       R:Forward_Backward           = 0
!       IF REC:TA_Date >= LOC:Last_Tagged_Date
!          IF REC:TA_Date = LOC:Last_Tagged_Date
!             IF REC:TA_Time >= LOC:Last_Tagged_Time
!                IF REC:TA_Time = LOC:Last_Tagged_Time
!                   IF R:Current_Selected_Rec_ID < LOC:Last_Tagged_Rec_Id
!                      R:Forward_Backward  = 1
!                .  .
!             ELSE
!                R:Forward_Backward  = 1
!          .  .
!       ELSE
!          R:Forward_Backward        = 1
!       .
!
!
!       IF R:Forward_Backward = 0
!          CLEAR(A_REC:Record,-1)
!
!          ! Forward meaning from earliest Last Tagged Date / Time - file is in descending date / time order
!          ! So we set these to the later date / time here
!          A_REC:User_ID_Private_To      = LOC:User_ID
!          A_REC:TA_Date                 = REC:TA_Date
!          A_REC:TA_Time                 = REC:TA_Time
!
!          R:User_ID_Private_To          = LOC:User_ID
!          R:TA_Date                     = LOC:Last_Tagged_Date
!          R:TA_Time                     = LOC:Last_Tagged_Time
!
!          R:To_Rec_ID                   = LOC:Last_Tagged_Rec_Id
!       ELSE
!          CLEAR(A_REC:Record,+1)
!
!          A_REC:User_ID_Private_To      = LOC:User_ID
!          A_REC:TA_Date                 = LOC:Last_Tagged_Date
!          A_REC:TA_Time                 = LOC:Last_Tagged_Time
!
!          R:User_ID_Private_To          = LOC:User_ID
!          R:TA_Date                     = REC:TA_Date
!          R:TA_Time                     = REC:TA_Time
!          R:To_Rec_ID                   = REC:Rec_ID
!       .
!
!       ! The Key used does not have a field below the Time, so if the date and time in the From and To are the
!       ! same then the Records will be in record no. order or received order!
!       R:From_Rec_ID    = 0
!       IF (LOC:Last_Tagged_Date = REC:TA_Date) AND (LOC:Last_Tagged_Time = REC:TA_Time)
!          IF R:Current_Selected_Rec_ID ~= LOC:Last_Tagged_Rec_Id
!             ! Then the greater Rec_ID must be the To Rec_ID
!             IF LOC:Last_Tagged_Rec_Id > R:Current_Selected_Rec_ID
!                R:To_Rec_ID     = LOC:Last_Tagged_Rec_Id
!                R:From_Rec_ID   = R:Current_Selected_Rec_ID
!             ELSE
!                R:To_Rec_ID     = R:Current_Selected_Rec_ID
!                R:From_Rec_ID   = LOC:Last_Tagged_Rec_Id
!       .  .  .
!
!       SET(A_REC:SKEY_Adjust_Date_Time, A_REC:SKEY_Adjust_Date_Time)
!       LOOP
!          IF Access:Received_Msgs_Alias.TryNext() ~= LEVEL:Benign
!             BREAK
!          .
!
!!       IF R:Forward_Backward = 0
!!          MESSAGE('Set Date / Time: ' & FORMAT(REC:TA_Date,@d5) & ' / ' & FORMAT(REC:TA_Time, @t4) & '||' |
!!                   & 'Found Date / Time: ' & FORMAT(A_REC:TA_Date,@d5) & ' / ' & FORMAT(A_REC:TA_Time, @t4) |
!!                   & '||A_REC:Rec_ID: ' & REC:Rec_ID & ' - to Rec_ID: ' & R:To_Rec_ID,'Forward')
!!       ELSE
!!          MESSAGE('Set Date / Time: ' & FORMAT(LOC:Last_Tagged_Date,@d5) & ' / ' & FORMAT(LOC:Last_Tagged_Time, @t4) & '||' |
!!                   & 'Found Date / Time: ' & FORMAT(A_REC:TA_Date,@d5) & ' / ' & FORMAT(A_REC:TA_Time, @t4) |
!!                   & '||A_REC:Rec_ID: ' & REC:Rec_ID & ' - to Rec_ID: ' & R:To_Rec_ID,'Backwards')
!!       .
!
!          IF A_REC:User_ID_Private_To ~= LOC:User_ID      |
!                OR A_REC:TA_Date < R:TA_Date OR (A_REC:TA_Date = R:TA_Date AND A_REC:TA_Time < R:TA_Time)
!             BREAK
!          .
!
!          IF R:From_Rec_ID ~= 0
!             !MESSAGE('cycles R:From_Rec_ID < A_REC:Rec_ID||' & R:From_Rec_ID & ' < ' & A_REC:Rec_ID,'To Rec_ID: ' & R:To_Rec_ID)
!             IF R:From_Rec_ID > A_REC:Rec_ID
!                CYCLE
!          .  .
!
!          InBox_Tags.MakeTag(A_REC:Rec_ID)
!
!          IF R:To_Rec_ID = A_REC:Rec_ID
!             BREAK
!    .  .  .
!
!    BRW_Invoices.ResetFromBuffer()
    .
    EXIT
Tag_Toggle_this_rec       ROUTINE
    DATA
Choice_#            LONG

    CODE
    IF p:Tagging > 0
       Choice_#        = CHOICE(?Browse:1)

       !LOC:Last_Tagged_Rec_Id       = CLI:CID

       IF p_Client_Tags.IsTagged(INV:IID) = TRUE
          p_Client_Tags.ClearTag(INV:IID)
   !       db.debugout('[Select_RateMod_Clients]  UN Tagging - CLI:CID: ' & CLI:CID)
       ELSE
          p_Client_Tags.MakeTag(INV:IID)
   !       db.debugout('[Select_RateMod_Clients]  Tagging - CLI:CID: ' & CLI:CID)
       .

   !    BRW_Invoices.ResetFromBuffer()
   !    BRW_Invoices.Reset(1)
       GET(Queue:Browse:1, Choice_#)
       IF ~ERRORCODE()
          DO Style_Entry
          PUT(Queue:Browse:1)
       .
       ?String_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()
    .
    EXIT

Style_Setup                     ROUTINE
    IF p:Tagging > 0
!    IF ~OMITTED(1)
       ! Style:Normal
   !    ?list1{PROPSTYLE:FontStyle, 1}     = FONT:Regular
   !    ?list1{PROPSTYLE:TextColor, 1}     = COLOR:NONE
   !    ?list1{PROPSTYLE:BackColor, 1}     = COLOR:NONE
   !    ?list1{PROPSTYLE:TextSelected, 1}  = COLOR:NONE
   !    ?list1{PROPSTYLE:BackSelected, 1}  = COLOR:NONE

       ! Style:Header
       !?list1{PROPSTYLE:FontStyle, 1}     = FONT:Bold
       ?Browse:1{PROPSTYLE:TextColor, 1}     = COLOR:HIGHLIGHTTEXT
       ?Browse:1{PROPSTYLE:BackColor, 1}     = COLOR:Blue       !COLOR:HIGHLIGHT
       ?Browse:1{PROPSTYLE:TextSelected, 1}  = COLOR:White
       ?Browse:1{PROPSTYLE:BackSelected, 1}  = COLOR:Blue

!       SELF.Q.LOC:Day_NormalFG = -2147483634
!       SELF.Q.LOC:Day_NormalBG = -2147483635
!       SELF.Q.LOC:Day_SelectedFG = -1
!       SELF.Q.LOC:Day_SelectedBG = -1

        !?LOC:Selected{PROP:Text}    = 'Show Selected (' & RECORDS(p_Addresses) & ')'
!    ELSE
       !LOC:Omitted_1    = TRUE

       !HIDE(?LOC:Selected)
!    .
    .


    ?Browse:1{PROPSTYLE:TextColor, 2}     = -1
    ?Browse:1{PROPSTYLE:BackColor, 2}     = 0E9E9E9H
    ?Browse:1{PROPSTYLE:TextSelected, 2}  = -1
    ?Browse:1{PROPSTYLE:BackSelected, 2}  = -1
    EXIT
Style_Entry                       ROUTINE
    IF p:Tagging > 0
       IF p_Client_Tags.IsTagged(Queue:Browse:1.INV:IID) = TRUE
          Queue:Browse:1.INV:IID_Style                            = 1
          Queue:Browse:1.INV:ClientName_Style                     = 1
          Queue:Browse:1.INV:ClientNo_Style                       = 1
          Queue:Browse:1.INV:DINo_Style                           = 1
          Queue:Browse:1.LOC:Status_Style                         = 1
       ELSE
          Queue:Browse:1.INV:IID_Style                            = 0
          Queue:Browse:1.INV:ClientName_Style                     = 0
          Queue:Browse:1.INV:ClientNo_Style                       = 0
          Queue:Browse:1.INV:DINo_Style                           = 0
          Queue:Browse:1.LOC:Status_Style                         = 0
    .  .

    IF LO_SG:Invoice_Details = FALSE
       Queue:Browse:1.L_BI:Outstanding_Style                    = 2
       Queue:Browse:1.L_BI:Credited_Style                             = 2
       Queue:Browse:1.L_BI:Payments_Style                             = 2
    ELSE
       Queue:Browse:1.L_BI:Outstanding_Style                    = 0
       Queue:Browse:1.L_BI:Credited_Style                             = 0
       Queue:Browse:1.L_BI:Payments_Style                             = 0
    .
    EXIT

!INV:DINo              
!LOC:Status           
!INV:InvoiceDate      
!INV:Weight           
!LOC:Terms            
!L_BI:Outstanding     
!INV:Total            
!L_BI:Payments        
!L_BI:Credited        
!INV:Insurance        
!INV:Documentation    
!INV:FuelSurcharge    
!INV:FreightCharge    
!INV:VAT              
!LOC:BranchName       
!INV:VolumetricWeight 
!INV:Printed          
!INV:Printed_Icon     
!INV:ShipperName      
!INV:ConsigneeName    
!INV:MIDs             
!INV:InvoiceTime      
!INV:BID              
!INV:DID              
!INV:ICID             
!INV:CID              
!INV:Terms            
!INV:Status           
Check_Omitted               ROUTINE
    ! (ULONG=0, BYTE=0, <shpTagClass p_Client_Tags>, ULONG=0)
    ! (p:CID, p:Tagging, <shpTagClass p_Client_Tags>, p:MID)
    IF OMITTED(3)
       l_Client_Tags       &= NEW(shpTagClass)
       p_Client_Tags       &= l_Client_Tags

       !LOC:Local_Tagging    = TRUE
       p:Tagging            = TRUE
    .
    EXIT
Print_Invoices                  ROUTINE
    DATA

R:Type          BYTE
R:Print         BYTE
R:Idx           LONG
R:Preview       BYTE

    CODE
    BRW_Invoices.UpdateViewRecord()

    IF INV:IID ~= 0
       ThisWindow.Update

       R:Type       = POPUP('Print Invoice Continuous|Print Invoice Laser|-|Print Laser (Original)')

       R:Print      = 2
       IF p_Client_Tags.NumberTagged() > 0
          R:Print   = MESSAGE('Print tagged Invoices or Selected Invoice?','Print Option',ICON:Question, 'Tagged|Selected', 1)
       .

       IF R:Print = 1
          ! (p:Type, p:Preview, p:From, p:To, p:Un_Printed, <shpTagClass p_Client_Tags>)
          !   1       2           3       4       5                   6
          !   p:Type
          !       0 - Continuous
          !       1 - Page printer
          !   p:Un_Printed
          !       0 - All
          !       1 - Un-Printed only

          CASE MESSAGE('Would you like to preview each invoice?', 'Print Option', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
          OF BUTTON:Yes
             R:Preview  = TRUE
          OF BUTTON:No
             R:Preview  = FALSE
          .

          Print_Invoices_Multiple(R:Type - 1, R:Preview,,,, p_Client_Tags)

          CASE MESSAGE('Printing of tagged Invoices is complete.||Remove tags now?', 'Print Invoices', ICON:Question, BUTTON:Yes+BUTTON:No)
          OF BUTTON:Yes
             DO Un_Tag_All
          .
       ELSE
          IF R:Type = 1
             ! (p:ID, p:PrintType, p:ID_Options, p:Preview)
             IF INV:Status = 2 OR INV:Status = 5           ! Credit notes...
                Print_Cont(INV:IID, 1)
             ELSE
                Print_Cont(INV:IID, 0)
             .
          ELSE
             ! (ULONG p:DID,ULONG p:IID,LONG p:From,LONG p:To,BYTE p:Un_Printed,BYTE p:Preview,BYTE p:Original)
             IF R:Type = 3
                Print_Invoices(, INV:IID,,,,, TRUE)            ! IKB - need an option for Credit Notes?
             ELSE
                Print_Invoices(, INV:IID)                      ! IKB - need an option for Credit Notes?
  !             Print_Invoices(INV:IID, 0)
       .  .  .

       ThisWindow.Reset
    .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Invoice')
      LOC:User_Access = Get_User_Access(GLO:UID, 'Invoice Create / Update', 'Browse_Invoice')
      DO Check_Omitted
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Locator:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_FG:Invoice_Status',L_FG:Invoice_Status) ! Added by: BrowseBox(ABC)
  BIND('L_DG:From_Date',L_DG:From_Date)           ! Added by: BrowseBox(ABC)
  BIND('LOC:To_Date_Filter',LOC:To_Date_Filter)   ! Added by: BrowseBox(ABC)
  BIND('L_DG:To_Date',L_DG:To_Date)               ! Added by: BrowseBox(ABC)
  BIND('L_BI:Outstanding',L_BI:Outstanding)       ! Added by: BrowseBox(ABC)
  BIND('L_BI:Payments',L_BI:Payments)             ! Added by: BrowseBox(ABC)
  BIND('L_BI:Credited',L_BI:Credited)             ! Added by: BrowseBox(ABC)
  BIND('LOC:Status',LOC:Status)                   ! Added by: BrowseBox(ABC)
  BIND('LOC:Terms',LOC:Terms)                     ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                            ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:InvoiceAlias.Open                        ! File InvoiceAlias used by this procedure, so make sure it's RelationManager is open
  Access:Clients.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceComposition.UseFile              ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW_Invoices.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_Invoice,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW_Invoices.Q &= Queue:Browse:1
  BRW1::Sort3:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon INV:CID for sort order 1
  BRW_Invoices.AddSortOrder(BRW1::Sort3:StepClass,INV:FKey_CID) ! Add the sort order for INV:FKey_CID for sort order 1
  BRW_Invoices.AddRange(INV:CID,Relate:_Invoice,Relate:Clients) ! Add file relationship range limit for sort order 1
  BRW_Invoices.AppendOrder('+INV:InvoiceDate,+INV:IID') ! Append an additional sort order
  BRW_Invoices.SetFilter('((L_FG:Invoice_Status = 255 OR L_FG:Invoice_Status = INV:Status) AND ((0 = L_DG:From_Date OR INV:InvoiceDate >= L_DG:From_Date) AND (0 = LOC:To_Date_Filter OR INV:InvoiceDate <<= LOC:To_Date_Filter)))') ! Apply filter expression to browse
  BRW_Invoices.AddResetField(LO_SG:Invoice_Details) ! Apply the reset field
  BRW_Invoices.AddResetField(L_DG:From_Date)      ! Apply the reset field
  BRW_Invoices.AddResetField(L_DG:To_Date)        ! Apply the reset field
  BRW_Invoices.AddResetField(L_FG:Invoice_Status) ! Apply the reset field
  BRW1::Sort4:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon INV:DINo for sort order 2
  BRW_Invoices.AddSortOrder(BRW1::Sort4:StepClass,INV:SKey_DINo) ! Add the sort order for INV:SKey_DINo for sort order 2
  BRW_Invoices.AddLocator(BRW1::Sort4:Locator)    ! Browse has a locator for sort order 2
  BRW1::Sort4:Locator.Init(?LOC:Locator,INV:DINo,1,BRW_Invoices) ! Initialize the browse locator using ?LOC:Locator using key: INV:SKey_DINo , INV:DINo
  BRW_Invoices.AppendOrder('+INV:IID')            ! Append an additional sort order
  BRW_Invoices.SetFilter('((L_FG:Invoice_Status = 255 OR L_FG:Invoice_Status = INV:Status) AND ((0 = L_DG:From_Date OR INV:InvoiceDate >= L_DG:From_Date) AND (0 = LOC:To_Date_Filter OR INV:InvoiceDate <<= LOC:To_Date_Filter)))') ! Apply filter expression to browse
  BRW_Invoices.AddResetField(LO_SG:Invoice_Details) ! Apply the reset field
  BRW_Invoices.AddResetField(L_DG:From_Date)      ! Apply the reset field
  BRW_Invoices.AddResetField(L_DG:To_Date)        ! Apply the reset field
  BRW_Invoices.AddResetField(L_FG:Invoice_Status) ! Apply the reset field
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon INV:BID for sort order 3
  BRW_Invoices.AddSortOrder(BRW1::Sort2:StepClass,INV:FKey_BID) ! Add the sort order for INV:FKey_BID for sort order 3
  BRW_Invoices.AddRange(INV:BID,Relate:_Invoice,Relate:Branches) ! Add file relationship range limit for sort order 3
  BRW_Invoices.AddLocator(BRW1::Sort2:Locator)    ! Browse has a locator for sort order 3
  BRW1::Sort2:Locator.Init(,INV:BID,1,BRW_Invoices) ! Initialize the browse locator using  using key: INV:FKey_BID , INV:BID
  BRW_Invoices.AppendOrder('+INV:ClientName,+INV:InvoiceDate,+INV:IID') ! Append an additional sort order
  BRW_Invoices.SetFilter('((L_FG:Invoice_Status = 255 OR L_FG:Invoice_Status = INV:Status) AND ((0 = L_DG:From_Date OR INV:InvoiceDate >= L_DG:From_Date) AND (0 = LOC:To_Date_Filter OR INV:InvoiceDate <<= LOC:To_Date_Filter)))') ! Apply filter expression to browse
  BRW_Invoices.AddResetField(LO_SG:Invoice_Details) ! Apply the reset field
  BRW_Invoices.AddResetField(L_DG:From_Date)      ! Apply the reset field
  BRW_Invoices.AddResetField(L_DG:To_Date)        ! Apply the reset field
  BRW_Invoices.AddResetField(L_FG:Invoice_Status) ! Apply the reset field
  BRW_Invoices.AddSortOrder(,INV:FKey_DID)        ! Add the sort order for INV:FKey_DID for sort order 4
  BRW_Invoices.AddLocator(BRW1::Sort5:Locator)    ! Browse has a locator for sort order 4
  BRW1::Sort5:Locator.Init(,INV:DID,1,BRW_Invoices) ! Initialize the browse locator using  using key: INV:FKey_DID , INV:DID
  BRW_Invoices.AppendOrder('+INV:IID')            ! Append an additional sort order
  BRW_Invoices.SetFilter('((L_FG:Invoice_Status = 255 OR L_FG:Invoice_Status = INV:Status) AND ((0 = L_DG:From_Date OR INV:InvoiceDate >= L_DG:From_Date) AND (0 = LOC:To_Date_Filter OR INV:InvoiceDate <<= LOC:To_Date_Filter)))') ! Apply filter expression to browse
  BRW_Invoices.AddResetField(LO_SG:Invoice_Details) ! Apply the reset field
  BRW_Invoices.AddResetField(L_DG:From_Date)      ! Apply the reset field
  BRW_Invoices.AddResetField(L_DG:To_Date)        ! Apply the reset field
  BRW_Invoices.AddResetField(L_FG:Invoice_Status) ! Apply the reset field
  BRW_Invoices.AddSortOrder(,)                    ! Add the sort order for  for sort order 5
  BRW_Invoices.AppendOrder('+INV:InvoiceDate,+INV:IID') ! Append an additional sort order
  BRW_Invoices.SetFilter('((L_FG:Invoice_Status = 255 OR L_FG:Invoice_Status = INV:Status) AND ((0 = L_DG:From_Date OR INV:InvoiceDate >= L_DG:From_Date) AND (0 = LOC:To_Date_Filter OR INV:InvoiceDate <<= LOC:To_Date_Filter)))') ! Apply filter expression to browse
  BRW_Invoices.AddResetField(LO_SG:Invoice_Details) ! Apply the reset field
  BRW_Invoices.AddSortOrder(,INV:FKey_MID)        ! Add the sort order for INV:FKey_MID for sort order 6
  BRW_Invoices.AddRange(INV:MID,LOC:MID)          ! Add single value range limit for sort order 6
  BRW_Invoices.AddLocator(BRW1::Sort8:Locator)    ! Browse has a locator for sort order 6
  BRW1::Sort8:Locator.Init(,INV:MID,1,BRW_Invoices) ! Initialize the browse locator using  using key: INV:FKey_MID , INV:MID
  BRW_Invoices.AppendOrder('+INV:IID')            ! Append an additional sort order
  BRW_Invoices.SetFilter('((L_FG:Invoice_Status = 255 OR L_FG:Invoice_Status = INV:Status) AND ((0 = L_DG:From_Date OR INV:InvoiceDate >= L_DG:From_Date) AND (0 = LOC:To_Date_Filter OR INV:InvoiceDate <<= LOC:To_Date_Filter)))') ! Apply filter expression to browse
  BRW_Invoices.AddResetField(LO_SG:Invoice_Details) ! Apply the reset field
  BRW_Invoices.AddResetField(L_DG:From_Date)      ! Apply the reset field
  BRW_Invoices.AddResetField(L_DG:To_Date)        ! Apply the reset field
  BRW_Invoices.AddResetField(L_FG:Invoice_Status) ! Apply the reset field
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon INV:IID for sort order 7
  BRW_Invoices.AddSortOrder(BRW1::Sort0:StepClass,INV:PKey_IID) ! Add the sort order for INV:PKey_IID for sort order 7
  BRW_Invoices.AddLocator(BRW1::Sort0:Locator)    ! Browse has a locator for sort order 7
  BRW1::Sort0:Locator.Init(?LOC:Locator,INV:IID,1,BRW_Invoices) ! Initialize the browse locator using ?LOC:Locator using key: INV:PKey_IID , INV:IID
  BRW_Invoices.SetFilter('((L_FG:Invoice_Status = 255 OR L_FG:Invoice_Status = INV:Status) AND ((0 = L_DG:From_Date OR INV:InvoiceDate >= L_DG:From_Date) AND (0 = LOC:To_Date_Filter OR INV:InvoiceDate <<= LOC:To_Date_Filter)))') ! Apply filter expression to browse
  BRW_Invoices.AddResetField(LO_SG:Invoice_Details) ! Apply the reset field
  BRW_Invoices.AddResetField(L_DG:From_Date)      ! Apply the reset field
  BRW_Invoices.AddResetField(L_DG:To_Date)        ! Apply the reset field
  BRW_Invoices.AddResetField(L_FG:Invoice_Status) ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW_Invoices.AddField(INV:IID,BRW_Invoices.Q.INV:IID) ! Field INV:IID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:ClientName,BRW_Invoices.Q.INV:ClientName) ! Field INV:ClientName is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:ClientNo,BRW_Invoices.Q.INV:ClientNo) ! Field INV:ClientNo is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:DINo,BRW_Invoices.Q.INV:DINo) ! Field INV:DINo is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:InvoiceDate,BRW_Invoices.Q.INV:InvoiceDate) ! Field INV:InvoiceDate is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:Weight,BRW_Invoices.Q.INV:Weight) ! Field INV:Weight is a hot field or requires assignment from browse
  BRW_Invoices.AddField(L_BI:Outstanding,BRW_Invoices.Q.L_BI:Outstanding) ! Field L_BI:Outstanding is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:Total,BRW_Invoices.Q.INV:Total) ! Field INV:Total is a hot field or requires assignment from browse
  BRW_Invoices.AddField(L_BI:Payments,BRW_Invoices.Q.L_BI:Payments) ! Field L_BI:Payments is a hot field or requires assignment from browse
  BRW_Invoices.AddField(L_BI:Credited,BRW_Invoices.Q.L_BI:Credited) ! Field L_BI:Credited is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:Insurance,BRW_Invoices.Q.INV:Insurance) ! Field INV:Insurance is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:Documentation,BRW_Invoices.Q.INV:Documentation) ! Field INV:Documentation is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:FuelSurcharge,BRW_Invoices.Q.INV:FuelSurcharge) ! Field INV:FuelSurcharge is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:TollCharge,BRW_Invoices.Q.INV:TollCharge) ! Field INV:TollCharge is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:FreightCharge,BRW_Invoices.Q.INV:FreightCharge) ! Field INV:FreightCharge is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:VAT,BRW_Invoices.Q.INV:VAT) ! Field INV:VAT is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:MIDs,BRW_Invoices.Q.INV:MIDs) ! Field INV:MIDs is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:MID,BRW_Invoices.Q.INV:MID) ! Field INV:MID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:BadDebt,BRW_Invoices.Q.INV:BadDebt) ! Field INV:BadDebt is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:BranchName,BRW_Invoices.Q.INV:BranchName) ! Field INV:BranchName is a hot field or requires assignment from browse
  BRW_Invoices.AddField(LOC:Status,BRW_Invoices.Q.LOC:Status) ! Field LOC:Status is a hot field or requires assignment from browse
  BRW_Invoices.AddField(LOC:Terms,BRW_Invoices.Q.LOC:Terms) ! Field LOC:Terms is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:VolumetricWeight,BRW_Invoices.Q.INV:VolumetricWeight) ! Field INV:VolumetricWeight is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:Printed,BRW_Invoices.Q.INV:Printed) ! Field INV:Printed is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:ShipperName,BRW_Invoices.Q.INV:ShipperName) ! Field INV:ShipperName is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:ConsigneeName,BRW_Invoices.Q.INV:ConsigneeName) ! Field INV:ConsigneeName is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:CR_IID,BRW_Invoices.Q.INV:CR_IID) ! Field INV:CR_IID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:InvoiceTime,BRW_Invoices.Q.INV:InvoiceTime) ! Field INV:InvoiceTime is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:IJID,BRW_Invoices.Q.INV:IJID) ! Field INV:IJID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:BID,BRW_Invoices.Q.INV:BID) ! Field INV:BID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:DID,BRW_Invoices.Q.INV:DID) ! Field INV:DID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:ICID,BRW_Invoices.Q.INV:ICID) ! Field INV:ICID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:CID,BRW_Invoices.Q.INV:CID) ! Field INV:CID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:Terms,BRW_Invoices.Q.INV:Terms) ! Field INV:Terms is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:Status,BRW_Invoices.Q.INV:Status) ! Field INV:Status is a hot field or requires assignment from browse
  BRW_Invoices.AddField(BRA:BID,BRW_Invoices.Q.BRA:BID) ! Field BRA:BID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_Invoice',QuickWindow)      ! Restore window settings from non-volatile store
      DO Style_Setup
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      IF p:CID ~= 0
         CLI:CID  = p:CID
         IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
            SELECT(?Tab:5)
         .
      ELSIF p:MID ~= 0
         LOC:MID                = p:MID
         L_FG:Invoice_Status    = 255           ! All statuses

         DISABLE(?Insert:4)
         DISABLE(?Change:4)
         DISABLE(?Delete:4)

         DISABLE(?Tab:2)
         DISABLE(?Tab:5)
         DISABLE(?Tab:6)
         DISABLE(?Tab:4)
         DISABLE(?Tab5)
         DISABLE(?Tab7)

         DISABLE(?Button_SelectManifest)

         SELECT(?Tab_MID)
      .
      BRA:BID         = GLO:BranchID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
      .
  BRW_Invoices.AskProcedure = 1                   ! Will call: Update_Invoice
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_Invoice',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,43,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW1::SortHeader.Init(Queue:Browse:1,?Browse:1,'','',BRW1::View:Browse,INV:PKey_IID)
  BRW1::SortHeader.UseSortColors = False
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:InvoiceAlias.Close
  !Kill the Sort Header
  BRW1::SortHeader.Kill()
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_Invoice',QuickWindow)            ! Save window data to non-volatile store
  END
      IF LOC:Selected_Clients = FALSE AND p:Tagging = TRUE
         p_Client_Tags.ClearAllTags()
      .
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Invoice
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW1::SortHeader.SetAlerts()


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Print
      BRW_Invoices.UpdateViewRecord()
    OF ?Button_DateRange
            L_DG:From_Date  = GETINI('Browse_Invoice', 'From_Date', , GLO:Local_INI)
            L_DG:To_Date    = GETINI('Browse_Invoice', 'To_Date', , GLO:Local_INI)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Select:2
      ThisWindow.Update()
          DO Un_Tag_All
    OF ?LO_SG:Invoice_Details
          CLEAR(L_BI:Browse_Items)
    OF ?Button_Print
      ThisWindow.Update()
      Print_Invoices(, INV:IID)
      ThisWindow.Reset
    OF ?SelectClients
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_RateMod_Clients()
      ThisWindow.Reset
    OF ?SelectClients_All
      ThisWindow.Update()
          !p_Client_Tags   &= p_Client_Tags
          LOC:Selected_Clients  = TRUE
      
       POST(EVENT:CloseWindow)
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Branches()
      ThisWindow.Reset
    OF ?Button_SelectManifest
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_Manifests('')
      ThisWindow.Reset
          LOC:MID     = MAN:MID
          BRW_Invoices.ResetQueue(1)
    OF ?Button_DateRange
      ThisWindow.Update()
      LOC:Dates = Ask_Date_Range(L_DG:From_Date, L_DG:To_Date)
      ThisWindow.Reset
      !    IF L_DG:To_Date ~= 0
      !       L_DG:To_Date    += 1
      !    .
      
          LOC:To_Date_Filter  = L_DG:To_Date + 1
      
          IF L_DG:From_Date > 0 AND L_DG:To_Date > 0
             ?Button_DateRange{PROP:Text}    = FORMAT(L_DG:From_Date,@d5) & ' - ' & FORMAT(L_DG:To_Date,@d5)
          ELSIF L_DG:From_Date > 0
             ?Button_DateRange{PROP:Text}    = 'From ' & FORMAT(L_DG:From_Date,@d5)
          ELSIF L_DG:To_Date > 0
             ?Button_DateRange{PROP:Text}    = 'To ' & FORMAT(L_DG:To_Date,@d5)
          ELSE
             ?Button_DateRange{PROP:Text}    = 'Limit to Date Range'
          .
      
      
          PUTINI('Browse_Invoice', 'From_Date', L_DG:From_Date, GLO:Local_INI)
          PUTINI('Browse_Invoice', 'To_Date', L_DG:To_Date, GLO:Local_INI)
    OF ?Button_Print:2
      ThisWindow.Update()
      BRW_Invoices.UpdateViewRecord()
          DO Print_Invoices
      !    ThisWindow.Update
      !
      !    ! (p:ID , p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow)
      !    ! (ULONG, BYTE       , BYTE=0      , BYTE=1          , BYTE=0      ), LONG, PROC
      !    ! p:PrintType         -     0 = Invoices
      !    !                           1 = Credit Notes
      !    !                           2 = Delivery Notes
      !    !                           3 = Statements
      !
      !    CASE POPUP('Continuous Invoice Paper|Blank Paper')
      !    OF 1
      !       !     0             1               2               3                4               5
      !       ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
      !       IF INV:Status = 2 OR INV:Status = 5           ! Credit notes...
      !          Print_Cont(INV:IID, 1)
      !       ELSE
      !          Print_Cont(INV:IID, 0)
      !       .
      !    OF 2
      !       Print_Invoices(, INV:IID)            ! IKB - need an option for Credit Notes?
      !    ELSE
      !    .
      !
      !    ThisWindow.Reset
      !
    OF ?Close
      ThisWindow.Update()
          DO Un_Tag_All
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Take Sort Headers Events
  IF BRW1::SortHeader.TakeEvents()
     RETURN Level:Notify
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
        CASE EVENT()
        OF EVENT:Accepted
           !EVENT:MouseDown
    !       BRW_Invoices.UpdateViewRecord()
    
           IF KeyCode() = ShiftMouseLeft
              ! Tag all records between this one and last tagged record
              IF CHOICE(?CurrentTab) = 1
                 DO Tag_From_To
              .
           ELSIF KeyCode() = CtrlMouseLeft
              DO Tag_Toggle_this_rec
           ELSIF KeyCode() ~= MouseRight AND KeyCode() ~= CtrlMouseRight        !AND LOC:Last_Tagged_Rec_Id ~= 0
    !          InBox_Tags.ClearAllTags()                ! Clear all tags when no shift left mouse
    !          LOC:Last_Tagged_Rec_Id        = 0
    
    !          BRW_Invoices.ResetFromBuffer()
           .
    
    !       LOC:Last_Clicked_Rec_ID      = REC:Rec_ID
    !       LOC:Last_Tagged_Date         = REC:TA_Date
    !       LOC:Last_Tagged_Time         = REC:TA_Time
        .
    CASE EVENT()
    OF EVENT:AlertKey
          CASE Keycode()
          OF CtrlA
             DO Tag_All
          OF CtrlU
             DO Un_Tag_All
          .
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          ?String_Tagged{PROP:Text}   = ''
          IF p:Tagging ~= TRUE
             HIDE(?SelectClients_All)
          .
        IF LOC:User_Access > 0
           DISABLE(?Insert:4)
           DISABLE(?Change:4)
           DISABLE(?Delete:4)
      
           IF LOC:User_Access ~= 1
              DISABLE(?Button_Print:2)
              DISABLE(?Button_Print)
      
              DISABLE(?View:4)
        .  .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW_Invoices.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:4                               ! Setup the control used to initiate view only mode
    IF LOC:User_Access > 0
       SELF.InsertControl   = 0
       SELF.ChangeControl   = 0
       SELF.DeleteControl   = 0
  
       IF LOC:User_Access ~= 1
          SELF.ViewControl   = 0
    .  .


BRW_Invoices.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?CurrentTab) = 6
    RETURN SELF.SetSort(5,Force)
  ELSIF CHOICE(?CurrentTab) = 7
    RETURN SELF.SetSort(6,Force)
  ELSE
    RETURN SELF.SetSort(7,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW_Invoices.SetQueueRecord PROCEDURE

  CODE
  ! taken out, using Invoice stored field for this now.
  !    BRA:BID             = INV:BID
  !    IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
  !       LOC:BranchName   = BRA:BranchName
  !    .
  
      IF LO_SG:Invoice_Details = TRUE
         ! Invoices may be credited or have payments
         L_BI:Payments       = Get_ClientsPay_Alloc_Amt(INV:IID)
         L_BI:Credited       = -Get_Inv_Credited(INV:IID)                    ! Credited is negative
  
         ! Credit notes may have associated invoices or not??
         IF INV:CR_IID ~= 0                                                  ! Should only be for credit notes!
            IF INV:Status = 2 OR INV:Status = 5
            ELSE
               ! ??? problem, have Credit Note ID but not Credit Note!
            .
  
            ! The L_BI:Credited should be zero as Credit Notes cannot be credited.
  
            L_BI:Credited   += -Get_Inv_Credited(INV:CR_IID)                 ! This is total credited (and will include this Credit Note)
            L_BI:Payments   += Get_ClientsPay_Alloc_Amt(INV:CR_IID)
  
            A_INV:IID        = INV:CR_IID
            IF Access:InvoiceAlias.TryFetch(A_INV:PKey_IID) ~= LEVEL:Benign
               CLEAR(A_INV:Record)
            .
  
            L_BI:Outstanding = A_INV:Total - (L_BI:Payments + L_BI:Credited)
         ELSE
            L_BI:Outstanding = INV:Total - (L_BI:Payments + L_BI:Credited)
      .  .
  
  
      ! Pre Paid|COD|Account
      CLEAR(LOC:Terms)
      EXECUTE INV:Terms
         LOC:Terms   = 'COD'
         LOC:Terms   = 'Account'
      ELSE
         LOC:Terms   = 'Pre Paid'
      .
  
  
  
      !     1               2               3             4               5               6               7
      ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown, Bad Debt
      EXECUTE INV:Status + 1
         LOC:Status   = 'No Payments'
         LOC:Status   = 'Partially Paid'
         LOC:Status   = 'Credit Note'
         LOC:Status   = 'Fully Paid - Credit'
         LOC:Status   = 'Fully Paid'
         LOC:Status   = 'Credit Note (shown)'
         LOC:Status   = 'Bad Debt (shown)'
         LOC:Status   = 'Over Paid'
      ELSE
         LOC:Status   = '<unknown>'
      .
  PARENT.SetQueueRecord
  
  SELF.Q.INV:IID_Style = 0 ! 
  SELF.Q.INV:ClientName_Style = 0 ! 
  SELF.Q.INV:ClientNo_Style = 0 ! 
  SELF.Q.INV:DINo_Style = 0 ! 
  SELF.Q.L_BI:Outstanding_Style = 0 ! 
  SELF.Q.L_BI:Payments_Style = 0 ! 
  SELF.Q.L_BI:Credited_Style = 0 ! 
  IF (INV:BadDebt = 1)
    SELF.Q.INV:BadDebt_Icon = 2                            ! Set icon from icon list
  ELSE
    SELF.Q.INV:BadDebt_Icon = 1                            ! Set icon from icon list
  END
  SELF.Q.LOC:Status_Style = 0 ! 
  IF (INV:Printed = 1)
    SELF.Q.INV:Printed_Icon = 2                            ! Set icon from icon list
  ELSE
    SELF.Q.INV:Printed_Icon = 1                            ! Set icon from icon list
  END
      DO Style_Entry


BRW_Invoices.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder<>NewOrder THEN
     BRW1::SortHeader.ClearSort()
  END
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>6,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>6,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_Invoices.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     BRW1::SortHeader.RestoreHeaderText()
     BRW_Invoices.RestoreSort()
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
        BRW1::SortHeader.ResetSort()
     ELSE
        BRW1::SortHeader.SortQueue()
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Inv_Status, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Inv_Status
  SELF.SetStrategy(?String_Tagged, Resize:FixRight+Resize:FixTop, Resize:LockSize) ! Override strategy for ?String_Tagged
  SELF.RemoveControl(?Group_Locator)                       ! Remove ?Group_Locator from the resizer, it will not be moved or sized

BRW1::SortHeader.QueueResorted       PROCEDURE(STRING pString)
  CODE
    IF pString = ''
       BRW_Invoices.RestoreSort()
       BRW_Invoices.ResetSort(True)
    ELSE
       IF CHOICE(?CurrentTab) = 3
          BRW_Invoices.ReplaceSort(pString,BRW1::Sort4:Locator)
          BRW_Invoices.SetLocatorFromSort()
       ELSIF CHOICE(?CurrentTab) = 4
          BRW_Invoices.ReplaceSort(pString,BRW1::Sort2:Locator)
          BRW_Invoices.SetLocatorFromSort()
       ELSIF CHOICE(?CurrentTab) = 5
          BRW_Invoices.ReplaceSort(pString,BRW1::Sort5:Locator)
          BRW_Invoices.SetLocatorFromSort()
       ELSIF CHOICE(?CurrentTab) = 7
          BRW_Invoices.ReplaceSort(pString,BRW1::Sort8:Locator)
          BRW_Invoices.SetLocatorFromSort()
       ELSE
          BRW_Invoices.ReplaceSort(pString,BRW1::Sort0:Locator)
          BRW_Invoices.SetLocatorFromSort()
       END
    END
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Ask_MoveFloor PROCEDURE (ULONG p:MID,ULONG p:To_Floor)

LOC:Result           LONG                                  ! 
LOC:Screen_Group     GROUP,PRE(L_SG)                       ! 
To_Floor             STRING(35)                            ! Floor Name
To_Floor_ID          ULONG                                 ! Floor ID
                     END                                   ! 
LOC:Floors_At_Address QUEUE,PRE(L_FAQ)                     ! 
Floor                STRING(35)                            ! Floor Name
FID                  ULONG                                 ! Floor ID
FBNFloor             BYTE                                  ! Is this a FBN Floor
                     END                                   ! 
FDB4::View:FileDrop  VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FID)
                     END
Queue:FileDrop       QUEUE                            !
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('Move Manifested Deliveries to Floor'),AT(,,210,164),FONT('Tahoma',8,,FONT:regular), |
  DOUBLE,CENTER,GRAY,MDI
                       SHEET,AT(4,2,202,142),USE(?Sheet1)
                         TAB('To Floor'),USE(?Tab1)
                           PROMPT('Transfer to Floor:'),AT(13,22),USE(?Prompt3),TRN
                           LIST,AT(77,22,121,10),USE(L_SG:To_Floor,,?L_SG:To_Floor:2),VSCROLL,DROP(15),FORMAT('140L(2)|M~' & |
  'Floor~@s35@'),FROM(Queue:FileDrop)
                           PROMPT('This will change the Floor specified on all the Deliveries that are Manifested ' & |
  'on this manifest to the selected "To Floor".'),AT(10,97,188,40),USE(?Prompt4),FONT(,,COLOR:Black, |
  FONT:bold,CHARSET:ANSI),TRN
                         END
                       END
                       BUTTON('&OK'),AT(94,146,,14),USE(?OkButton),LEFT,ICON('waok.ico'),DEFAULT,FLAT
                       BUTTON('&Cancel'),AT(152,146,,14),USE(?CancelButton),LEFT,ICON('wacancel.ico'),FLAT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:Result)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Ask_MoveFloor')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Access:Floors.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
     IF p:MID > 0
        CLEAR(JOU:Record)
  
        MAN:MID         = p:MID
        IF Access:Manifest.TryFetch(MAN:PKey_MID) = LEVEL:Benign
           JOU:JID      = MAN:JID
           IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
              FLO:FID   = JOU:FID
           ELSE
        .  .
     ELSE
        FLO:FID   = p:To_Floor   
     .
        
           
     FLO:FID   = p:To_Floor
     IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
        L_SG:To_Floor      = FLO:Floor
        L_SG:To_Floor_ID   = FLO:FID
  
        L_FAQ:Floor        = FLO:Floor
        L_FAQ:FID          = FLO:FID
     .
  FDB4.Init(?L_SG:To_Floor:2,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:Floors,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(FLO:Key_Floor)
  FDB4.AddField(FLO:Floor,FDB4.Q.FLO:Floor) !List box control field - type derived from field
  FDB4.AddField(FLO:FID,FDB4.Q.FLO:FID) !Primary key field - type derived from field
  FDB4.AddUpdateField(FLO:FID,L_FAQ:FID)
  FDB4.AddUpdateField(FLO:Floor,L_SG:To_Floor)
  FDB4.AddUpdateField(FLO:FID,L_SG:To_Floor_ID)
  ThisWindow.AddItem(FDB4.WindowComponent)
  FDB4.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update()
          LOC:Result  = L_SG:To_Floor_ID
       POST(EVENT:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_SG:To_Floor:2
          !FLO:FID             = L_FAQ:FID
          !IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
          !.
      
          !L_SG:To_Floor       = FLO:Floor
          !L_SG:To_Floor_ID    = L_FAQ:FID
      
      !    L_SG:To_Floor_List  = L_FAQ:Floor
          !DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

