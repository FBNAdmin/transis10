   PROGRAM


NetTalk:TemplateVersion equate('10.11')
ActivateNetTalk   EQUATE(1)
  include('NetCrit.inc'),once
  include('NetMap.inc'),once
  include('NetAll.inc'),once
  include('NetTalk.inc'),once
  include('NetSimp.inc'),once
  include('NetFtp.inc'),once
  include('NetHttp.inc'),once
  include('NetWww.inc'),once
  include('NetSync.inc'),once
  include('NetWeb.inc'),once
  include('NetWebSocketClient.inc'),once
  include('NetWebSocketServer.inc'),once
  include('NetWebM.inc'),once
  include('NetWSDL.inc'),once
  include('NetEmail.inc'),once
  include('NetFile.inc'),once
  include('NetWebSms.inc'),once
  Include('NetOauth.inc'),once
  Include('NetLDAP.inc'),once
  Include('NetMaps.inc'),once
  Include('NetDrive.inc'),once
INSIGHT:Version            EQUATE('1.96')     !Deprecated - but exists for backward compatibility
INSIGHT:TemplateVersion    EQUATE('1.96')
StringTheory:TemplateVersion equate('2.66')

   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
 Include('Debuger.INC'),once

!* * * * Line Print Template Generated Code * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

OFSTRUCT    GROUP,TYPE                                                                  
cBytes       BYTE                                                                       ! Specifies the length, in bytes, of the structure
cFixedDisk   BYTE                                                                       ! Specifies whether the file is on a hard (fixed) disk. This member is nonzero if the file is on a hard disk
nErrCode     SIGNED                                                                     ! Specifies the MS-DOS error code if the OpenFile function failed
Reserved1    SIGNED                                                                     ! Reserved, Don't use
Reserved2    SIGNED                                                                     ! Reserved, Don't use
szPathName   BYTE,DIM(128)                                                              ! Specifies the path and filename of the file
            END

LF                  CSTRING(CHR(10))                                                    ! Line Feed
FF                  CSTRING(CHR(12))                                                    ! Form Feed
CR                  CSTRING(CHR(13))                                                    ! Carriage Return

lpszFilename        CSTRING(144)                                                        ! File Name
fnAttribute         SIGNED                                                              ! Attribute
hf                  SIGNED                                                              ! File Handle
hpvBuffer           STRING(500)                                                        ! String To Write
cbBuffer            SIGNED                                                              ! Characters to Write
BytesWritten        SIGNED                                                              ! Characters Written

Succeeded           EQUATE(0)                                                           ! Function Succeeded
OpenError           EQUATE(1)                                                           ! Can Not Open File or Device
WriteError          EQUATE(2)                                                           ! Can not Write to File or Device
CloseError          EQUATE(3)                                                           ! Can not Close File or Device
SeekError           EQUATE(4)                                                           ! Can not Seek File or Device

ATTR_Normal         EQUATE(0)                                                           ! File Attribute Normal
ATTR_ReadOnly       EQUATE(1)                                                           ! File Attribute Read Only
ATTR_Hidden         EQUATE(2)                                                           ! File Attribute Hidden
ATTR_System         EQUATE(3)                                                           ! File Attribute System

OF_READ             EQUATE(0000h)                                                       ! Opens the file for reading only
OF_WRITE            EQUATE(0001h)                                                       ! Opens the file for writing only
OF_READWRITE        EQUATE(0002h)                                                       ! Opens the file for reading and writing
OF_SHARE_COMPAT     EQUATE(0000h)                                                       ! Opens the file with compatibility mode, allowing any program on a given machine to open the file any number of times.
OF_SHARE_EXCLUSIVE  EQUATE(0010h)                                                       ! Opens the file with exclusive mode, denying other programs both read and write access to the file
OF_SHARE_DENY_WRITE EQUATE(0020h)                                                       ! Opens the file and denies other programs write access to the file
OF_SHARE_DENY_READ  EQUATE(0030h)                                                       ! Opens the file and denies other programs read access to the file
OF_SHARE_DENY_NONE  EQUATE(0040h)                                                       ! Opens the file without denying other programs read or write access to the file
OF_PARSE            EQUATE(0100h)                                                       ! Fills the OFSTRUCT structure but carries out no other action
OF_DELETE           EQUATE(0200h)                                                       ! Deletes the file
OF_VERIFY           EQUATE(0400h)                                                       ! Compares the time and date in the OF_STRUCT with the time and date of the specified file
OF_SEARCH           EQUATE(0400h)                                                       ! Windows searches in directories even when the file name includes a full path
OF_CANCEL           EQUATE(0800h)                                                       ! Adds a Cancel button to the OF_PROMPT dialog box. Pressing the Cancel button directs OpenFile to return a file-not-found error message
OF_CREATE           EQUATE(1000h)                                                       ! Creates a new file. If the file already exists, it is truncated to zero length
OF_PROMPT           EQUATE(2000h)                                                       ! Displays a dialog box if the requested file does not exist.
OF_EXIST            EQUATE(4000h)                                                       ! Opens the file, and then closes it. This value is used to test for file existence
OF_REOPEN           EQUATE(8000h)                                                       ! Opens the file using information in the reopen buffer

OF_STRUCT           LIKE(OFSTRUCT)                                                      ! Will Be loaded with File information. See OFSTRUCT above

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

! Generated by Clarion 10000
   INCLUDE('Insight.Inc'),once
  include('cwsynchc.inc'),once  ! added by NetTalk
   INCLUDE('MDISync.INC'),ONCE
  include('StringTheory.Inc'),ONCE
!!! Dll and Link mode flags !!!
_FUNLinkMode_    EQUATE(0)
_FUNDLLMode_     EQUATE(1)
    INCLUDE('shptagging.inc'),Once,External        ! Used in: Browse_Clients, Browse_Invoices, Browse_Manifests +...

    ! Browse_Transporter_Invoices, Browse_Transporter, 
    ! Update_TransporterPayments, Update_ManifestLoad, Update_Clients
    ! Print_Invoice_List, Print_Invoice_Summary

    INCLUDE('isqclass.inc'),Once,External
    INCLUDE('issqlcls.inc'),EXTERNAL,Once       ! Used in Operations_DI_Release
    INCLUDE('PRNPROP.CLW'),ONCE
Ulong_Q_Type          QUEUE,TYPE
Ul      ULONG
                    .
include('FormLocker.inc')

   MAP
    MODULE('TRNISDCT.DLL')
Add_Log                PROCEDURE(STRING, STRING, <STRING>, BYTE=0, BYTE=0, BYTE=1, BYTE=1, <STRING>),DLL ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt, p:Header) - global
Add_Log_h              PROCEDURE(STRING, STRING, STRING),DLL ! (p1,p2,p3)
Check_IID_On_Statement FUNCTION(ULONG),LONG,DLL            ! (p:IID)
Get_ClientsPay_Alloc_Amt FUNCTION(ULONG, BYTE=0, LONG=0),STRING,DLL ! (p:ID, p:Option, p:ToDate)
Get_DelItem_s_Totals   FUNCTION(ULONG, BYTE, <ULONG>, <*STRING>, BYTE=0, BYTE=0),ULONG,DLL ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache, p:Weight_Option) - note: Ulong returned - divide by 100 for weight   Manifest & Tripsheets also used
Get_Delivery_ManIDs    FUNCTION(ULONG, SHORT, <*STRING>, BYTE=0, ULONG=0),ULONG, PROC,DLL ! (p:DID, p:No, p:MAN_DIID_List, p:List_Option, p:DIID)
Get_InvTransporter_Credited FUNCTION(ULONG, BYTE, ULONG=0, BYTE=0, LONG=0),STRING,DLL ! (p:TIN, p:Option, p:TIN_to_Ignore, p:Type, p:ToDate)
Get_Inv_Credited       FUNCTION(ULONG, LONG=0),STRING,DLL  ! (p:IID, p:ToDate)
Get_ManLoadItem_Units  FUNCTION(ULONG),ULONG,DLL           ! (p:DIID)
Get_TransportersPay_Alloc_Amt FUNCTION(ULONG, BYTE=0, LONG=0),STRING,DLL ! (p:ID, p:Option, p:ToDate)
Get_TripItem_Del_Units FUNCTION(ULONG, BYTE),ULONG,DLL     ! (p:DIID, p:Option) - Returns delivered units of completed trips
Upd_ClientPayments_Status FUNCTION(ULONG, <*DECIMAL>),LONG,PROC,DLL ! (p:CPID, p:Amt_Allocated)
Upd_Delivery_Del_Status FUNCTION(<ULONG>,<ULONG>,BYTE=0,BYTE=1), BYTE, PROC,DLL ! (p:DID, p:DIID, p:Silent, p:NoCache)
Upd_Delivery_Man_Status FUNCTION(<ULONG>, <ULONG>, BYTE=0),BYTE,PROC,DLL ! (p:DID, p:DIID, p:Option) returns status  ** global **
Upd_InvoiceTransporter_Paid_Status FUNCTION(ULONG),BYTE,PROC,DLL ! (p:TIN)
Upd_Invoice_Paid_Status FUNCTION(ULONG, BYTE=0, BYTE=0),BYTE,PROC,DLL ! (p:IID, p:Option, p:CreditChanged)
Thread_Add_Del_Check_Global FUNCTION(STRING, STRING, STRING), LONG, PROC,DLL ! (p:Procedure, p:Thread, p:Option)   ** global **
Thread_Send_Procs_Events PROCEDURE(STRING, STRING),DLL     ! (p:Procedure, p:Event)  ** global **   ** see thread event limitation notes inside ***
Update_DBStructure     PROCEDURE(STRING),DLL               ! (p_Qry)
    END
    MODULE('FUNTRNIS.DLL')
Ask_Date_Range         FUNCTION(LONG=0, LONG=0, BYTE=0, <STRING>),STRING,DLL ! (p:From, p:To, p:Option, p:Heading)
Ask_ManifestLoad       FUNCTION(ULONG),ULONG,DLL           ! (p:MID)
Delivery_Rates         FUNCTION(ULONG, ULONG, ULONG, ULONG, LONG, ULONG, *DECIMAL, *DECIMAL, *DECIMAL, *DECIMAL, *LONG),ULONG,DLL ! (p:DID, p:LTID, p:CID, p:JID, p:DIDate, p:FIDRate, p:SelectedRate, p:Mass, p:MinimiumChargeRate, p:To_Weight, p:Effective_Date)
DI_Items_Action        FUNCTION(),BYTE,DLL                 ! 
Window_Duplicate_Client_Rates PROCEDURE(ULONG, ULONG),DLL  ! (p:CID, p:RID)
Changed_Rates          PROCEDURE,DLL                       ! 
Check_ClientRates_for_JID FUNCTION(ULONG, ULONG), ULONG,DLL ! if 1 JID, returns that, if multiple, checks that against passed - (p:CID, p:JID)
Check_ClientsRateTypes FUNCTION(ULONG, ULONG),ULONG,DLL    ! (p:CID, p:LTID)
Check_COD_Invoices     FUNCTION(ULONG, BYTE=0),ULONG, PROC,DLL ! (p:TRID, p:Option)
Check_Delivery_Invoiced FUNCTION(ULONG, BYTE=0),ULONG,DLL  ! Returns 0 or invoice ID (IID) - (p:DID, p:Option)
Check_Delivery_MID     FUNCTION(ULONG, ULONG), BYTE,DLL    ! (p:DID, p:MID)
Check_Rates            FUNCTION(ULONG),ULONG,DLL           ! currently checks CRTID - (p:CRTID)
Gen_Aging_Transporter  FUNCTION(ULONG, LONG, <*DECIMAL>, <*STRING>, BYTE=0, BYTE=0),LONG,DLL ! (p:TID, p:Date, p:Owing, p:Statement_Info, p:MonthEndDay, p:OutPut)
Gen_Invoice_s          FUNCTION(STRING, ULONG=0, BYTE=1, ULONG=0, LONG=0, <*LONG>),LONG,PROC,DLL ! Generate an invoice or multiple invoices (DID list passed) - (p:DIDs, p:BID, p:Report_Basic_Errors, p:MID, p:Inv_Date, p:Invoices_Exist)
Gen_Man_Invs           FUNCTION(ULONG, LONG),LONG,DLL      ! (p:MID, p:Inv_Date)
Gen_Remittance_Paid    FUNCTION(ULONG, ULONG, LONG, LONG, LONG, LONG, BYTE=1, BYTE=0, <*DECIMAL>),LONG,DLL ! (p:REMRID, p:TID, p:RunDate, p:RunTime, p:FromDate, p:ToDate, p:Run_Type, p:OutPut, p:Paid_Total) - from Payments Allocations
Gen_Statement          FUNCTION(ULONG, ULONG, LONG, LONG, <*DECIMAL>, BYTE=0, <*STRING>, BYTE=0, BYTE=0, BYTE=0),LONG,DLL ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
Gen_Transporter_Invoice FUNCTION(ULONG, BYTE=0, LONG),LONG,PROC,DLL ! from Manifest - (p:MID, p:Option, p:Inv_Date)
Get_Branch_Info        FUNCTION(ULONG=0, BYTE),STRING,DLL  ! (p:BID, p:Option)
Get_City_Details       FUNCTION(ULONG),STRING,DLL          ! (p:CIID)
Get_Clients_CP_Related FUNCTION(ULONG, ULONG=0, BYTE=0),LONG,DLL ! Based on Client (Journey optional) - (p:CID, p:JID, p:Mode)
Get_Clients_Related    FUNCTION(ULONG, ULONG, BYTE, BYTE=0),LONG,DLL ! 0 - Journeys, 1 - Load Types - (p:CID, p:ID, p:Option, p:Mode)
Get_ContainerType_Info FUNCTION(ULONG, BYTE),STRING,DLL    ! (p:CTID, p:Option)
Get_DelItemUnMan_Units FUNCTION(ULONG),LONG,DLL            ! Delivery Item Un-manifested units - (p:DIID)
Get_DelItems_ContainerType FUNCTION(ULONG, <*LONG>),ULONG,DLL ! uses DeliveryItemAlias - (p:DID, p:DIItemNo)
Get_DelItem_Client_Info FUNCTION(ULONG, BYTE, LONG=0, LONG=0), STRING,DLL ! (p:ID, p:Option, p:DateFrom, p:DateTo)
Get_DeliveryAdditionalCharges FUNCTION(ULONG),ULONG,DLL    ! divide by 100 - (p:DID)
Get_Delivery_Info      FUNCTION(ULONG, BYTE=0),STRING,DLL  ! (p:DELCID, p:Option)  return the totals for DI's on the passed combination
Get_Delivery_Rate      FUNCTION(ULONG, ULONG, ULONG, ULONG, ULONG, *DECIMAL, LONG=0, <*DECIMAL>, <*BYTE>, <*DECIMAL>, <*LONG>, <*ULONG>, BYTE=0),STRING,DLL ! (p:DID, p:CID, p:JID, p:CRTID, p:FID, p:Mass, p:Eff_Date, <*p:Mincharge>, <p:Setup>, <p:To_Weight>, <p:Effective_Date>, <p:RateID>, p:Quiet)
Get_DelLegs_Info       FUNCTION(ULONG, BYTE=0), STRING,DLL ! (p:DID, p:Option)
Get_DelMan_State       FUNCTION(ULONG, <*STRING>, <*STRING>, BYTE=0),BYTE,DLL ! (p:DID, p:MID, p:DIIDs, p:Option)
Get_DelMan_Weight      FUNCTION(ULONG, <ULONG>),ULONG,DLL  ! note: Ulong returned - divide by 100;  also can do single item - (p:DID, p:DIID)
Get_DelTripDel_Weight  FUNCTION(ULONG, <ULONG>),ULONG,DLL  ! note: Ulong returned - divide by 100;  also can do single item - (p:DID, p:DIID)
Get_Floor_DIs          FUNCTION(ULONG),LONG,DLL            ! (p:FID)
Get_Floor_Rate         FUNCTION(ULONG, ULONG, LONG, <*DECIMAL>, <*DECIMAL>, BYTE=0), ULONG,DLL ! (p:FID, p:JID, p:Eff_Date, p:Mass, p:Rate, p:Cache_Type)
Get_Info_SQL           FUNCTION(BYTE=0, ULONG, <*STRING>, <STRING>, LONG=1), LONG, PROC,DLL ! TripSheetDeliveries & DeliveryItems & Deliveries - (p:Option, p:ID, p:ResultString, p:SQL, p:Items)
Get_InvoicesTransporter FUNCTION(LONG, BYTE, BYTE=0, BYTE=0, BYTE=0, <ULONG>, ULONG=0, <LONG>, BYTE=0),STRING,DLL ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID, p:BID, p:ToDate, p:Extra_Inv)
Get_InvTransporter_Outstanding FUNCTION(ULONG),STRING,DLL  ! (p:TIN)
Get_Inv_Outstanding    FUNCTION(ULONG),STRING,DLL          ! (p:IID)
Get_ManDelItems_UnLoaded FUNCTION(ULONG, <*STRING>, <*STRING>),ULONG,DLL ! Note: Multi DIDs are not counted as unloaded! - (p:MID, p:Items_DIID_List, p:Multi_DIDs)
Get_Manifest_Del_MIDs  FUNCTION(ULONG, <*STRING>),ULONG,DLL ! (p:MID, p:Multi_MIDs)
Get_Manifest_DIDs      FUNCTION(ULONG, *STRING),LONG,DLL   ! List of DIDs on MID - (p:MID, p:DIDs)
Get_Manifest_Info      FUNCTION(ULONG, BYTE, BYTE=0, BYTE=0, BYTE=0, <STRING>),STRING,DLL ! (p:MID, p:Option, p:DI_Type, p:Inv_Totals, p:Output, p:Info)
Get_ManLoadItems_Info  FUNCTION(ULONG, BYTE=0, <ULONG>, <ULONG>), STRING,DLL ! (p:MLID, p:Option, p:DID, p:DIID)
Get_ManLoad_Info       FUNCTION(ULONG, BYTE, <ULONG>),STRING,DLL ! String returned  - can be limited to 1 DID - (p:MID, p:Option, p:DID)
Get_Setup_Info         FUNCTION(BYTE),STRING,DLL           ! (p:Option)
Get_Statement_Run_Info FUNCTION(ULONG),LONG,DLL            ! (p:STRID)
Get_Suburb_Details     FUNCTION(ULONG),STRING,DLL          ! (p:SUID)
Get_Transporter_Rate   FUNCTION(ULONG, ULONG, ULONG=0, *BYTE, <*ULONG>, <*DECIMAL>, <*DECIMAL>, <*ULONG>, BYTE=0, LONG=0), STRING,DLL ! (p:TID, p:JID, p:VCID, p:Setup, p:Setup_VCID, p:MinimiumLoad, p:BaseCharge, p:TRID, p:Local_Rate, p:Eff_Date)
Get_Transporter_Totals PROCEDURE(ULONG, ULONG, BYTE, *DECIMAL, *DECIMAL, *DECIMAL, *DECIMAL),DLL ! (p:TIN, p:CR_TIN, p:Status, p_b:Cost_Total, pb:Payments, pb:Credits, pb:Outstanding)
Get_TripDelItems_Incomplete FUNCTION(ULONG, <*STRING>),ULONG,DLL ! (p:TRID, p:DIIDs)
Get_TripDelItems_Info  FUNCTION(<ULONG>, BYTE=0, <ULONG>, <ULONG>, <ULONG>, <*STRING>),ULONG,DLL ! note: Ulong returned - divide by 100 for weight - (p:TRID, p:Option, p:DID, 
Get_TripDelItemUnDel_Units FUNCTION(ULONG, BYTE),LONG,DLL  ! Delivery Item un-loaded units - (p:DIID, p:Option)
Get_User_Access        FUNCTION(ULONG, STRING, STRING, <STRING>, BYTE=0, <*BYTE>),LONG,DLL ! ** global **   needs work.... - (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:Default_Action_Return)
Get_VehComp_Info       FUNCTION(ULONG, BYTE, BYTE=0, BYTE=0),STRING,DLL ! (p:VCID, p:Option, p:Truck, p:Additional)
Move_ManDeliveries     FUNCTION(ULONG, ULONG),LONG,DLL     ! Change the Floors - (p:MID, p:FID)
Add_Audit              PROCEDURE(BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0),DLL ! (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel)    ** global **
Check_StatementItems   FUNCTION(ULONG), STRING,DLL         ! (p:IID)
Get_Client_Balances    FUNCTION(ULONG, BYTE=0, *STRING),LONG,PROC,DLL ! (p:CID, p:Option, p_Bal_Group)
Check_Delivery_Release FUNCTION(<*STRING>, ULONG=0, <*STRING>),LONG,DLL ! does this delivery require releasing, if so is it? - (p_Statement_Info, p:DID, p:Reason)
Get_User_Info          FUNCTION(BYTE, *STRING, <ULONG>, <STRING>),LONG,PROC,DLL ! (p:Info, p:Value, p:UID, p:Login)
Get_UserGroup_Info     FUNCTION(BYTE, *STRING, <ULONG>, <STRING>),LONG,PROC,DLL ! (p:Info, p:Value, p:UGID, p:Group)
Get_TruckTrailer_Info  FUNCTION(ULONG, BYTE=0, LONG=0, <*LONG>),STRING,DLL ! (p:TTID, p:Type, p:Effective_Date, p:LicenseExpiryDate)
Add_Reminder           FUNCTION(LONG, LONG, BYTE=0, LONG, LONG, STRING, STRING, BYTE=0, BYTE=0, ULONG=0), LONG, PROC,DLL ! (p:RemDate, p:RemTime, p:RemOption, p:RemUID, p:RemUGID, p:Notes, p:Ref, p:Ref_Check, p:RemType, p:ID)
Get_DelItems_Info      FUNCTION(ULONG, BYTE), STRING,DLL   ! (p:DIID, p:Option)
Get_Address            FUNCTION(ULONG,BYTE=0,BYTE=0),STRING,DLL ! (p:AID, p:Type, p:Details)
Get_Client_Invoice_History FUNCTION(ULONG, LONG=0, BYTE=0, *STRING),LONG,PROC,DLL ! (p:CID, p:Date, p:Option, p_Bal_Group)
Get_Invoices           FUNCTION(LONG, BYTE, BYTE=0, BYTE=0, ULONG=0, <LONG>, BYTE=0, BYTE=0, BYTE=0, BYTE=0, <STRING>, <STRING>),STRING,DLL ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID, p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source)
Get_Client_FuelSurcharge_Calcs FUNCTION(ULONG, ULONG, *DECIMAL, LONG, <*DECIMAL>, <*LONG>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING,DLL ! (p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Cur_Eff_Date, p:X_Fuel_Cost, p:X_Eff_Date, p:M_Fuel_Base_Rate, p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base, p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per)
Rates_Modify           PROCEDURE(shpTagClass p_Client_Tags),DLL ! (shpTagClass p_Client_Tags)
Get_Client_FuelSurcharge FUNCTION(ULONG, LONG=0, <*DECIMAL>, <*ULONG>, BYTE=1),STRING,DLL ! (p:CID, p:Date, p:FuelBaseRate, p:CID_Used, p:No_Negatives)
GetSQLValue            FUNCTION(String pRequest,<*File pFile>,<*? pField>),String,DLL ! 
ConString              FUNCTION(String,String,String),String,DLL ! 
GetManifestTotals      PROCEDURE(ULong pMID, *Decimal pCost, *Decimal pVatRate, *TManifestTotals MT, Byte pOutput = 0),DLL ! 
Get_Client_Emails      FUNCTION(ULONG, BYTE=0, BYTE=0),STRING,DLL ! (p:CID, p:Type, p:Option)
Send_Email             FUNCTION(STRING, STRING, BYTE=3, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>, <ULONG>),STRING,DLL ! (p_Subject, p_Text, p_EmailAccount, p_HTML, p_Server, p_AuthUser, p_AuthPassword, p_From, p_To, p_CC, p_BCC, p_Helo, p_Attach, p_Port)
Add_Vehicle_Composition FUNCTION(ULONG, STRING, ULONG, ULONG, ULONG, STRING),LONG,DLL ! (p_TID, p_CompositionName, p_FreightLinerTTID, p_TrailerTTID, p_SuperLinkTTID, p_Capicity)
Validate_Email_Addresses FUNCTION(STRING p_EmailAddresses),STRING,DLL ! 
Save_Email_Addresses   FUNCTION(STRING p_EmailAddresses,ULONG p_CID),LONG,DLL ! 
Load_Email_Addresses   FUNCTION(ULONG p:CID),STRING,DLL    ! 
    END
    MODULE('SELTRNIS.DLL')
Select_Address         PROCEDURE,DLL                       ! 
Select_AddressAlias    PROCEDURE,DLL                       ! 
Select_TransporterInvoice PROCEDURE(BYTE=0),DLL            ! (p:Option)
Select_Journey         PROCEDURE(ULONG=0, ULONG=0, ULONG=0),DLL ! (p:CID, p:LTID, p:FID)
Select_RateMod_Clients FUNCTION(),ULONG,PROC,DLL           ! Select Clients / Mod Rates for Clients
Select_Addr_Cities     PROCEDURE,DLL                       ! 
Select_Branches        PROCEDURE,DLL                       ! 
Select_Countries       PROCEDURE,DLL                       ! 
Select_Deliveries      PROCEDURE,DLL                       ! 
Select_DeliveryStatuses PROCEDURE,DLL                      ! 
Select_Floors          PROCEDURE,DLL                       ! 
Select_Provinces       PROCEDURE,DLL                       ! 
Select_StatementRuns_Alias PROCEDURE,DLL                   ! 
Select_Transporter     PROCEDURE,DLL                       ! 
Select_Clients         PROCEDURE,DLL                       ! 
Select_Users           PROCEDURE,DLL                       ! 
Select_UserGroups      PROCEDURE,DLL                       ! 
Select_DeliveryItems   PROCEDURE,DLL                       ! 
    END
    MODULE('RPTTRNIS.DLL')
Print_Cont             FUNCTION(ULONG, BYTE, BYTE=0, BYTE=1, BYTE=0, <*Q_Type_UL_S500 p_Print_Q>, <STRING>), LONG, PROC,DLL ! (p:ID, p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow, <*Q_Type_UL_S500 p_Print_Q>)
Print_Debtor_Age_Analysis PROCEDURE(ULONG, BYTE, LONG=0, BYTE=0, BYTE=0, BYTE=0, ULONG=0, BYTE=0, BYTE=255, ULONG=0),DLL ! (p:SRID, p:Option, p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:CID, p:Credit_Limit_Option, p:ClientStatus, p:BID)
Print_Designer         PROCEDURE(ULONG),DLL                ! (p:PID)
Print_DN_POD           PROCEDURE(ULONG=0, ULONG=0, BYTE=0),DLL ! (p:MID, p:DID, p:Only_Invoiced)
Print_DN_POD_Cont      PROCEDURE(ULONG),DLL                ! (p:MID)
Print_Invoices         PROCEDURE(ULONG=0, ULONG=0, LONG=0, LONG=0, BYTE=0, BYTE=1, BYTE=0),DLL ! DID or IID passed to restrict to a Delivery or Invoice - (p:DID, p:IID, p:From, p:To, p:Un_Printed, p:Preview, p:Original)
Print_Manifest         PROCEDURE(ULONG=0, BYTE=0, BYTE=0),DLL ! (p:MID, p:Output, p:SkipPreview)
Print_Manifests_Tagged PROCEDURE(shpTagClass p_Client_Tags, BYTE),DLL ! (shpTagClass p_Client_Tags, p:Type)
Print_Manifest_Loading PROCEDURE(ULONG=0),DLL              ! (p:MID)
Print_Man_Invs         FUNCTION(ULONG, BYTE, BYTE=0, BYTE=1),LONG,PROC,DLL ! Type is for Continuoes or Laser,  Option is for COD - (p:MID, p:Type, p:Option, p:Preview)
Print_Rates            PROCEDURE(STRING, BYTE=0),DLL       ! (p:CID. p:Option)
Print_Remittances      PROCEDURE(ULONG),DLL                ! (p:RERID)
Print_Statement        PROCEDURE(ULONG=0, ULONG=0),DLL     ! (p:CID, p:STID)
Print_Statements       PROCEDURE(ULONG),DLL                ! (p:STRID)
Print_TripSheet        PROCEDURE(ULONG=0),DLL              ! (p:TRID)
Print_TripSheet_Collections PROCEDURE(ULONG=0),DLL         ! (p:TRID)
Process_Deliveries     PROCEDURE(STRING),DLL               ! (p:DIDs)
Print_Shortages_Damages PROCEDURE,DLL                      ! 
Print_Invoices_Multiple PROCEDURE(BYTE=0, BYTE=1, LONG=0, LONG=0, BYTE=0, <shpTagClass p_Client_Tags>),DLL ! (p:Type, p:Preview, p:From, p:To, p:Un_Printed, <shpTagClass p_Client_Tags>)
Get_Client_Search      FUNCTION(STRING),STRING,DLL         ! (p:ClientName)
Print_Debtor_Age_Analysis_PDF PROCEDURE(ULONG, BYTE, LONG=0, BYTE=0, BYTE=0, BYTE=0, ULONG=0, BYTE=0, BYTE=255, ULONG=0),DLL ! (p:SRID, p:Option, p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:CID, p:Credit_Limit_Option, p:ClientStatus)
Print_TripSheet_no_Preview PROCEDURE(ULONG=0),DLL          ! (p:TRID)
Get_Client_Advance_Payments FUNCTION(ULONG, LONG=2, *DECIMAL, <*DECIMAL>, <*DECIMAL>),LONG,DLL ! (p:CID, p:Payment_Status, p:Total, p:Cur, p:Other)
Process_Invoices       PROCEDURE(STRING),DLL               ! p:CID
GetMonthName           FUNCTION(Long pMonthNo),String,DLL  ! 
    END
    MODULE('M2TRNIS.DLL')
Update_FuelCost        PROCEDURE,DLL                       ! 
Update_RatesFuelCost   PROCEDURE,DLL                       ! 
Update_EToll           PROCEDURE,DLL                       ! 
    END
    MODULE('M3TRNIS.DLL')
Update_Address_Contacts PROCEDURE(ULONG=0),DLL             ! p:AID
Update_Email_Addresses PROCEDURE(BYTE=0),DLL               ! (p:Option)
Update_Countries       PROCEDURE,DLL                       ! 
Update_Provinces       PROCEDURE,DLL                       ! 
Browse_Provinces       PROCEDURE,DLL                       ! 
Update_Cities          PROCEDURE,DLL                       ! 
Browse_Cities          PROCEDURE,DLL                       ! 
Update_Suburbs         PROCEDURE,DLL                       ! 
Browse_Suburbs         PROCEDURE,DLL                       ! 
Update_Addresses       PROCEDURE(<STRING>),DLL             ! p:Type
Browse_Addresses       PROCEDURE(<STRING>, <STRING>, ULONG=0),DLL ! (p:Address_Name, p:Type, p:Branch)
Update_Accountants     PROCEDURE,DLL                       ! 
Browse_Accountants     PROCEDURE,DLL                       ! 
Browse_Address_Contacts PROCEDURE(STRING),DLL              ! p:AID
Update_Audit           PROCEDURE,DLL                       ! 
Browse_Audit           PROCEDURE,DLL                       ! 
Update_Branches        PROCEDURE,DLL                       ! 
Browse_Branches        PROCEDURE,DLL                       ! 
Update_Print_Fields    PROCEDURE(BYTE),DLL                 ! p:PrintType
Browse_Print_Fields    PROCEDURE(BYTE),DLL                 ! p:PrintType
Update_Print_Layout    PROCEDURE,DLL                       ! 
Update_Prints          PROCEDURE,DLL                       ! 
Browse_Prints          PROCEDURE,DLL                       ! 
Update_Reminders       PROCEDURE(BYTE=0, ULONG=0),DLL      ! (p:Option, p:ID)
Browse_Reminders       PROCEDURE,DLL                       ! 
Browse_Countries       PROCEDURE,DLL                       ! 
Browse_AdditionalChargeCategories PROCEDURE,DLL            ! 
Browse_ContainerOperators PROCEDURE,DLL                    ! 
Browse_ContainerTypes  PROCEDURE,DLL                       ! 
Browse_SalesReps       PROCEDURE,DLL                       ! 
Browse_Drivers         PROCEDURE(STRING, STRING),DLL       ! 
Get_Setting            FUNCTION(STRING, BYTE=0, <STRING>, <STRING>, <STRING>),STRING,DLL ! (p:Setting, p:CreateUpdate, p:DefUpdValue, p:Picture, p:Tip)
Get_Delivery_TripSheets FUNCTION(ULONG p_DID),STRING,DLL   ! (ULONG p_DID), STRING
Browse_EmailAddresses  PROCEDURE(BYTE=0),DLL               ! (p:Option)
Get_Client_ETolls      FUNCTION(ULONG, LONG=0, <*ULONG>),STRING,DLL ! (p:CID, p:Date, p:CID_Used)
Browse_Vessels         PROCEDURE,DLL                       ! 
Browse_Commodities     PROCEDURE(String),DLL               ! 
Browse_ServiceRequirements PROCEDURE(ULONG=0, ULONG=0),DLL ! 
Browse_PackagingTypes  PROCEDURE,DLL                       ! 
Browse_Remittance_Advice PROCEDURE,DLL                     ! 
Browse_ListManager     PROCEDURE,DLL                       ! 
Browse_CODAddresses    PROCEDURE,DLL                       ! 
Update_Commodities     PROCEDURE(<STRING>),DLL             ! 
Update_DeliveryStatuses PROCEDURE,DLL                      ! 
Update_Vessels         PROCEDURE,DLL                       ! 
Update_WebClientsLoginLog PROCEDURE,DLL                    ! 
Update_Statement_Run_Desc PROCEDURE,DLL                    ! 
Update_StatementItems  PROCEDURE,DLL                       ! 
Update_Remittance_Runs PROCEDURE,DLL                       ! 
Update_PackagingTypes  PROCEDURE,DLL                       ! 
Browse_Journeys        PROCEDURE(ULONG=0),DLL              ! --- filtering done on client - slow - make temp table?  whenever rates changed for client delete temp records?
Browse_LoadTypes       PROCEDURE(STRING, STRING),DLL       ! 
Browse_Statement_Run_Desc PROCEDURE,DLL                    ! 
Browse_VehicleMakeModels PROCEDURE,DLL                     ! 
Update_AdditionalCharges PROCEDURE,DLL                     ! 
Update_Rate_Updates    PROCEDURE,DLL                       ! 
    END
    MODULE('WORTRNIS.DLL')
Update_Manifest        PROCEDURE(BYTE=0),DLL               ! 
Browse_Manifests       PROCEDURE(STRING p:TID, BYTE p:Transfer=0),DLL ! 
Manifest_Emails_Setup  PROCEDURE(ULONG, BYTE=0, BYTE=0, <STRING>),DLL ! 
    END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('MANTRNIS001.CLW')
Update_WebClients      PROCEDURE   !
Window_Client_Activity PROCEDURE   !
Window_Client_ActivityJ PROCEDURE   !
Update_Clients         PROCEDURE(BYTE=0)   !(p:Option)
Update_Invoice         PROCEDURE(<STRING>)   !
Browse_Deliveries      PROCEDURE(STRING,STRING)   !
     END
     MODULE('MANTRNIS002.CLW')
Deliveries_Search      FUNCTION(*STRING),BYTE   !
Update_Invoice_Items   PROCEDURE   !
Update_ClientsPayments PROCEDURE(BYTE=0, ULONG=0)   !Set type to 1 for reversal insert
Update_ClientsPaymentsAllocation PROCEDURE   !
Browse_Invoice         PROCEDURE(ULONG=0, BYTE=0, <shpTagClass p_Client_Tags>, ULONG=0)   !
Ask_MoveFloor          FUNCTION(ULONG=0, ULONG=0),LONG   !
     END
     MODULE('MANTRNIS003.CLW')
Update_Transporter     PROCEDURE   !
Update_DeliveryLegs    PROCEDURE   !
Browse_Transporter     PROCEDURE   !
Browse_Addresses_Alias_h PROCEDURE(<STRING>, <STRING>)   !
Update_TruckTrailer    PROCEDURE   !
Update_VehicleComposition PROCEDURE(ULONG=0)   !
     END
     MODULE('MANTRNIS004.CLW')
Update_TransporterPayments PROCEDURE   !
Update_TransporterPaymentsAllocations PROCEDURE(ULONG=0)   !
Browse_Transporter_Invoices PROCEDURE(ULONG=0, <shpTagClass p_Client_Tags>, <Q_Class p_Q>)   !
Update_TransporterInvoice PROCEDURE(ULONG=0, BYTE=0, BYTE=0)   !
Start_Manifest         PROCEDURE(STRING)   !
Start_Trans_Invoice    PROCEDURE(STRING)   !
Browse_VehicleComposition PROCEDURE   !
     END
     MODULE('IKB_LIB.LIB')
       INCLUDE('IKB_LIB.INC')
     END
     MODULE('MANTRNIS005.CLW')
Update_TransporterRates PROCEDURE   !
Select_VehicleComposition PROCEDURE   !
Print_Manifest_Invoices PROCEDURE(ULONG pMID)   !Report the _Invoice File
Update_ClientsPayments_h PROCEDURE(STRING)   !
Update_Invoice_h       PROCEDURE(STRING, STRING)   !
Update_DeliveryComposition PROCEDURE   !
Browse_DeliveryCompositions FUNCTION(),ULONG   !
Update_DeliveryProgress PROCEDURE   !
     END
     MODULE('MANTRNIS006.CLW')
Update_DeliveryItems   PROCEDURE(BYTE=0, ULONG=0)   !
Update_DeliveryItems_h PROCEDURE(ULONG=0)   !
Browse_Floors          PROCEDURE(STRING)   !
Update_Floors          PROCEDURE(BYTE=0)   !
Update_ContainerParkRates PROCEDURE   !
Browse_DI_Progress     PROCEDURE   !
Browse_ClientsRateTypes PROCEDURE(ULONG=0, ULONG=0)   !
Update_ClientsRateTypes PROCEDURE   !
     END
     MODULE('MANTRNIS007.CLW')
Update_DeliveriesAdditionalCharges PROCEDURE(ULONG)   !
Browse_DeliveryAdditionalCharges_Delivery PROCEDURE(ULONG, ULONG)   !
Update_FuelSurcharge   PROCEDURE   !
Update_Rates           PROCEDURE(ULONG=0, BYTE=0, ULONG=0)   !
Update_Clients_ContainerParkRates PROCEDURE   !
Update_Statements      PROCEDURE   !
Browse_WebUsers        PROCEDURE   !
Main_Main              PROCEDURE   !
     END
     MODULE('MANTRNIS008.CLW')
Browse_TripSheets      PROCEDURE   !
TripSheet_Load_DI      FUNCTION(ULONG, ULONG, ULONG, ULONG=0, BYTE=0), LONG   !
Update_TripSheet       PROCEDURE   !**** changes such as additional calls to procedures using templates, needs checking of RUN method
Update_TripSheetDeliveries_Load PROCEDURE(ULONG=0)   !
Update_TripSheetDeliveries_h PROCEDURE(ULONG=0)   !
Update_Shortages_Damages PROCEDURE(BYTE=0)   !
Browse_Drivers_Alias_h PROCEDURE(STRING,STRING)   !
     END
     MODULE('MANTRNIS009.CLW')
TripSheet_BulkPOD      PROCEDURE(ULONG=0)   !
Browse_Transporter_Payments PROCEDURE   !
Update_Statement_Runs  PROCEDURE   !
Browse_Statements      PROCEDURE   !
Browse_StatementRuns   PROCEDURE   !
Gen_Statements         PROCEDURE   !Window
Browse_Remittance_Runs PROCEDURE   !
Gen_Remittances        PROCEDURE   !
     END
     MODULE('MANTRNIS010.CLW')
Browse_Rates_Update    PROCEDURE   !
Browse_DeliveryStatuses PROCEDURE   !
Browse_Client_Payments PROCEDURE   !
Upd_DeliveryComp_Wizard PROCEDURE   !             ***   depricated
Update__Container_Rates PROCEDURE(BYTE=0)   !
Print_Debtor_Age_Analysis_Window PROCEDURE(STRING)   !
DIs_Processed          PROCEDURE   !
Reminding_Window       PROCEDURE(STRING, STRING)   !
     END
     MODULE('MANTRNIS011.CLW')
Reminding              PROCEDURE   !Reminding
Create_Journal         PROCEDURE   !
Clients_Rates_Mod      PROCEDURE   !
Create_Credit_Note     PROCEDURE   !(also Bad Debt)
Print_Transporter_Invoices PROCEDURE   !
Print_Reciepts         PROCEDURE   !
Print_Reciepts_with_Headings PROCEDURE   !
Print_Reciepts_Extended PROCEDURE   !**** not used ****
     END
     MODULE('MANTRNIS012.CLW')
Print_Invoices_List    PROCEDURE   !
Print_Invoice_Summary  PROCEDURE   !
Print_Transporter_Invoices_VAT PROCEDURE   !
Print_Client_DeliveryReport PROCEDURE   !===========
Print_UndeliveredPODs  PROCEDURE   !===========
Browse_Shortages_Damages PROCEDURE   !
Print_Delivery_Status  PROCEDURE   !
Operations_DI_Release  PROCEDURE   !
     END
     MODULE('MANTRNIS013.CLW')
Window_Journey_Activity PROCEDURE   !
Start_Print_SalesRep   PROCEDURE   !Sales Rep Report settings
Clients_OverTime       PROCEDURE   !
Delivery_Tracking_Emails_Setup PROCEDURE   !came from Manifest_Emails
Update_Deliveries_with_email_etc_old_perhaps_____ PROCEDURE(BYTE=0)   !
Manifest_Emails        PROCEDURE   !Using in silent mode and none silent - NOT USED YET
     END
     MODULE('MANTRNIS031.CLW')
Update_DeliveryItems_old_change_to_commodity PROCEDURE(BYTE=0, ULONG=0)   !
TripSheet_BulkPOD_Win  FUNCTION(ULONG),LONG   !
     END
     MODULE('MANTRNIS032.CLW')
Browse_TruckTrailer    PROCEDURE(<BYTE>, <BYTE>)   !
Delivery_PODs          PROCEDURE(ULONG p:DID)   !
     END
     MODULE('MANTRNIS033.CLW')
Update_Deliveries      PROCEDURE(BYTE=0)   !
     END
     MODULE('MANTRNIS034.CLW')
Update_TripSheetDeliveries PROCEDURE(<STRING>)   !
     END
     MODULE('MANTRNIS035.CLW')
Delivery_Tracking_Emails_Setup_old_before_tabs PROCEDURE   !came from Manifest_Emails
     END
     MODULE('MANTRNIS036.CLW')
Browse_Clients         PROCEDURE(<shpTagClass p_Client_Tags>, BYTE p_Option=0, <STRING p_ClientName>)   !
     END
     
     !* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
          LinePrint(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>),BYTE,PROC          ! Declare LinePrint Function
          DeleteFile(STRING FileToDelete),BYTE,PROC                                          ! Declare DeleteFile Function
            MODULE('')                                                                       ! MODULE Start
             OpenFile(*CSTRING,*OFSTRUCT,SIGNED),SIGNED,PASCAL,RAW                           ! Open/Create/Delete File
             _llseek(SIGNED,LONG,SIGNED),LONG,PASCAL                                         ! Control File Pointer
             _lwrite(SIGNED,*STRING,UNSIGNED),UNSIGNED,PASCAL,RAW                           ! Write to File
             _lclose(SIGNED),SIGNED,PASCAL                                                   ! Close File
            END                                                                              ! Terminate Module
     
     !* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
    ! Declare functions defined in this DLL
MANTRNIS:Init          PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>)
MANTRNIS:Kill          PROCEDURE
    ! Declare init functions defined in a different dll
     MODULE('TRNISDCT.DLL')
TrnISDct:Init          PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>),DLL
TrnISDct:Kill          PROCEDURE,DLL
     END
     MODULE('FUNTRNIS.DLL')
FunTrnIS:Init          PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>),DLL
FunTrnIS:Kill          PROCEDURE,DLL
     END
     MODULE('SELTRNIS.DLL')
SelTrnIS:Init          PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>),DLL
SelTrnIS:Kill          PROCEDURE,DLL
     END
     MODULE('RPTTRNIS.DLL')
RptTrnIS:Init          PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>),DLL
RptTrnIS:Kill          PROCEDURE,DLL
     END
     MODULE('M2TRNIS.DLL')
M2TrnIS:Init           PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>),DLL
M2TrnIS:Kill           PROCEDURE,DLL
     END
     MODULE('M3TRNIS.DLL')
M3TRNIS:Init           PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>),DLL
M3TRNIS:Kill           PROCEDURE,DLL
     END
     MODULE('WORTRNIS.DLL')
WorTrnis:Init          PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>),DLL
WorTrnis:Kill          PROCEDURE,DLL
     END
   END

GLO:BranchID         ULONG,EXTERNAL,DLL(_ABCDllMode_)
GLO:ReplicatedDatabaseID LONG,EXTERNAL,DLL(_ABCDllMode_)
GLO:DBOwner          STRING(255),EXTERNAL,DLL(_ABCDllMode_)
GLO:Global_INI       CSTRING('.\TransIS.INI<0>{241}'),EXTERNAL,DLL(_ABCDllMode_)
GLO:Local_INI        CSTRING('TransISL.INI<0>{242}'),EXTERNAL,DLL(_ABCDllMode_)
GLO:Rep_ID           LONG,EXTERNAL,DLL(_ABCDllMode_)
GLO:ClosingDown      BYTE,EXTERNAL,DLL(_ABCDllMode_)
GLO:Global_Controls  GROUP,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Reminding_Waiting      BYTE
Frame_Size             GROUP,PRE(GLO)
Width                    LONG
Height                   LONG
                       END
                     END
GLO:Development_Group GROUP,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Testing_Mode           BYTE(1)
                     END
GLO:Loaded_IDs_Group GROUP,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Setup_Loaded_ID        ULONG(1)
Get_Delivery_ManIDs_Loaded_ID LONG(1)
Rates_Caching_No       ULONG
Reminder_Inc           LONG
                     END
GLO:Thread_Q         QUEUE,PRE(GL_TQ),EXTERNAL,DLL(_ABCDllMode_)
ProcedureName          STRING(150)
Thread                 SIGNED
Q_ID                   ULONG
                     END
GLO:User_Group       GROUP,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
UID                    ULONG
UGIDs                  STRING(100)
Login                  STRING(20)
AccessLevel            BYTE
LoggedInDate           DATE
LoggedInTime           TIME
                     END
GLO:String_Q_Type    QUEUE,PRE(GLO_SQT),EXTERNAL,DLL(_ABCDllMode_)
Str1                   STRING(1000)
Str2                   STRING(1000)
Str3                   STRING(1000)
                     END
GLO:Clock_Audit      TIME,EXTERNAL,DLL(_ABCDllMode_)
SilentRunning        BYTE(0)                               ! Set true when application is running in 'silent mode'

!region File Declaration
Clients              FILE,DRIVER('MSSQL'),PRE(CLI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Clients (debtors)   
PKey_CID                 KEY(CLI:CID),NOCASE,OPT,PRIMARY   ! By Client ID        
Key_ClientNo             KEY(CLI:ClientNo),NOCASE          ! By Client No.       
Key_ClientName           KEY(CLI:ClientName)               ! By Client Name      
Key_ClientSearch         KEY(CLI:ClientSearch)             ! By Client Search    
FKey_BID                 KEY(CLI:BID),DUP,NOCASE,OPT       ! By Branch ID        
FKey_AID                 KEY(CLI:AID),DUP,NOCASE,OPT       ! By Address ID       
FKey_ACID                KEY(CLI:ACID),DUP,NOCASE,OPT      ! By Accountant ID    
FKey_SRID                KEY(CLI:SRID),DUP,NOCASE          !                     
Record                   RECORD,PRE()
CID                         ULONG                          ! Client ID           
ClientNo                    ULONG                          ! Client No.          
ClientName                  STRING(100)                    !                     
ClientSearch                STRING(100)                    ! Search string - client name without punctuation or case (Not dispayed)
BID                         ULONG                          ! Branch ID           
AID                         ULONG                          ! Address ID          
GenerateInvoice             BYTE                           ! Generate an invoice when DI created
MinimiumCharge              DECIMAL(8,2)                   !                     
Rate                        DECIMAL(8,2)                   ! Rate for this debtor
DocumentCharge              DECIMAL(8,2)                   ! Document Charge     
ACID                        ULONG                          ! Accountant ID       
OpsManager                  STRING(35)                     ! Ops Manager         
InsuranceRequired           BYTE                           ! Insurance Required  
InsuranceType               BYTE                           ! Insurance Type      
InsurancePercent            DECIMAL(9,6)                   ! Insruance Percentage
Discounts                   GROUP                          ! Discounts           
OnInvoice                     DECIMAL(5,2)                 ! On Invoice          
Days30                        DECIMAL(5,2)                 ! 30 Days             
Days60                        DECIMAL(5,2)                 ! 60 Days             
Days90                        DECIMAL(5,2)                 ! 90 Days             
                            END                            !                     
PaymentPeriod               USHORT                         ! Payment required in period (in days, 0 is COD/COP)
VolumetricRatio             DECIMAL(8,2)                   ! x square cube weighs this amount
AdviceOfDispatch            BYTE                           ! Advise on dispatch  
DeliveryNotes               BYTE                           ! Delivery notes - Individual or Summary
Notes                       STRING(500)                    !                     
InvoiceMessage              STRING(255)                    !                     
Status                      BYTE                           ! Normal, On Hold, Closed, Dormant
FaxConfirmation             BYTE                           ! Fax confirmation of parcel status required
SRID                        ULONG                          ! Sales Rep ID        
DateAndTimeSalesRepEarnsUntil STRING(8)                    !                     
DateAndTimeSalesRepEarnsUntil_GROUP GROUP,OVER(DateAndTimeSalesRepEarnsUntil) !                     
SalesRepEarnsUntil            DATE                         ! Date up till when the sales rep. earns commision on this client
SalesRepEarnsUntilTIME        TIME                         !                     
                            END                            !                     
VATNo                       STRING(20)                     ! VAT No.             
Terms                       BYTE                           ! Terms - Pre Paid, COD, Account, On Statement
AccountLimit                DECIMAL(12,2)                  ! Account limit       
DateTimeOpened              STRING(8)                      ! Date & Time Opened  
DateTimeOpened_Group        GROUP,OVER(DateTimeOpened)     !                     
DateOpened                    DATE                         !                     
TimeOpened                    TIME                         !                     
                            END                            !                     
Balances                    GROUP                          !                     
UpdatedDateTime               STRING(8)                    !                     
UpdatedDateTime_Group         GROUP,OVER(UpdatedDateTime)  !                     
UpdatedDate                     DATE                       !                     
UpdatedTime                     TIME                       !                     
                              END                          !                     
BalanceCurrent                DECIMAL(12,2)                !                     
Balance30Days                 DECIMAL(12,2)                !                     
Balance60Days                 DECIMAL(12,2)                !                     
Balance90Days                 DECIMAL(12,2)                !                     
                            END                            !                     
FuelSurchargeActive         BYTE                           !                     
PODMessage                  CSTRING(256)                   !                     
LiabilityCover              BYTE                           !                     
LiabilityCrossBorder        BYTE                           !                     
TollChargeActive            BYTE                           !                     
                         END
                     END                       

Reminders            FILE,DRIVER('MSSQL'),PRE(REM),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Reminders           
PKey_RID                 KEY(REM:RID),NOCASE,OPT,PRIMARY   ! By Reminder         
Key_ID                   KEY(REM:ID),DUP,NOCASE,OPT        ! By ID               
SKey_Type_ID             KEY(REM:ReminderType,REM:ID),DUP,NOCASE,OPT ! By Type && ID       
FKey_UID                 KEY(REM:UID),DUP,NOCASE,OPT       ! By User             
FKey_UGID                KEY(REM:UGID),DUP,NOCASE,OPT      ! By User Group       
SKey_Reference           KEY(REM:Reference),DUP,NOCASE,OPT ! By Reference        
Record                   RECORD,PRE()
RID                         ULONG                          ! Reminder ID         
ReminderType                BYTE                           ! General, Client, Truck / Trailer
ID                          ULONG                          ! Link ID             
Active                      BYTE                           ! Active Reminder     
Popup                       BYTE                           ! Popup when due      
ReminderDateTime            STRING(8)                      !                     
ReminderDateTimeGroup       GROUP,OVER(ReminderDateTime)   !                     
ReminderDate                  DATE                         !                     
ReminderTime                  TIME                         !                     
                            END                            !                     
RemindGroup                 GROUP                          !                     
RemindOption                  BYTE                         ! All, Group & User, User, Group
UID                           ULONG                        ! User ID             
UGID                          ULONG                        ! User Group ID       
                            END                            !                     
Notes                       CSTRING(500)                   !                     
CreatedGroup                GROUP                          !                     
Created_UID                   ULONG                        ! User ID             
Created_DateTime              STRING(8)                    !                     
Created_DateTimeGroup         GROUP,OVER(Created_DateTime) !                     
Created_Date                    DATE                       !                     
Created_Time                    TIME                       !                     
                              END                          !                     
                            END                            !                     
Reference                   CSTRING(36)                    ! Reference for this Reminder
SystemGenerated             BYTE                           ! This reminder was System Generated
Reoccur                     GROUP                          !                     
Reoccurs                      BYTE                         ! This reminder reoccurs
Reoccur_Option                BYTE                         !                     
Reoccur_Period                LONG                         !                     
                            END                            !                     
                         END
                     END                       

WebClients           FILE,DRIVER('MSSQL'),PRE(WCLI),BINDABLE,CREATE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PK_WebClients            KEY(WCLI:WebClientID),PRIMARY     !                     
FKey_CID                 KEY(WCLI:CID),DUP,NAME('_WA_Sys_CID_2334397B') ! By Client           
Key_ClientLogin          KEY(WCLI:ClientLogin),DUP,NAME('_WA_Sys_ClientLogin_2334397B') ! By Client Login     
Record                   RECORD,PRE()
WebClientID                 LONG                           !                     
CID                         ULONG                          ! Client ID           
ClientLogin                 STRING(20)                     !                     
ClientPassword              STRING(20)                     !                     
LoginName                   STRING(50)                     !                     
AccessLevel                 BYTE                           ! 1 admin access, 10 client access
                         END
                     END                       

_View_Rates_Client_Journeys FILE,DRIVER('MSSQL'),PRE(V_RCJ),BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
CKey_CID_JID             KEY(V_RCJ:CID,V_RCJ:JID),DUP,NOCASE,OPT ! By Client && Journey
Record                   RECORD,PRE()
CID                         LONG                           !                     
JID                         LONG                           !                     
                         END
                     END                       

AdditionalCharges    FILE,DRIVER('MSSQL'),PRE(ACCA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Additional Charge Categories
PKey_ACCID               KEY(ACCA:ACCID),NOCASE,OPT,PRIMARY ! By Additional Charge Category ID
Key_Description          KEY(ACCA:Description),NOCASE      ! By Description      
Record                   RECORD,PRE()
ACCID                       ULONG                          ! Additional Charge Category ID
Description                 STRING(35)                     ! Description         
                         END
                     END                       

Clients_ContainerParkDiscounts FILE,DRIVER('MSSQL'),PRE(CLI_CP),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Discounts for clients for container parks
PKey_CPDID               KEY(CLI_CP:CPDID),NOCASE,OPT,PRIMARY ! By Container Park Discount ID
FKey_CID                 KEY(CLI_CP:CID),DUP,NOCASE,OPT    ! By Client           
FKey_FID                 KEY(CLI_CP:FID),DUP,NOCASE,OPT    ! By Floor            
FKey_RUBID               KEY(CLI_CP:RUBID),DUP,NOCASE,OPT  ! By Rate Update ID   
Record                   RECORD,PRE()
CPDID                       ULONG                          ! Container Park Discount ID
CID                         ULONG                          ! Client ID           
FID                         ULONG                          ! Floor ID            
ContainerParkRateDiscount   DECIMAL(5,2)                   ! Discount % that is applied to the container park rates for this client
ContainerParkMinimium       DECIMAL(9,2)                   ! Minimium charge for this client (for this container park)
Effective_DateAndTime       STRING(8)                      !                     
Effective_DateAndTime_GROUP GROUP,OVER(Effective_DateAndTime) !                     
Effective_Date                DATE                         ! Effective from this date
Effective_TIME                TIME                         ! Not used!           
                            END                            !                     
RUBID                       ULONG                          ! Rate Update Batch ID
                         END
                     END                       

VehicleMakeModel     FILE,DRIVER('MSSQL'),PRE(VMM),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Vehicle Makes & Models
PKey_VMMID               KEY(VMM:VMMID),NOCASE,OPT,PRIMARY ! By Vehicle Make && Model ID
Key_MakeModel            KEY(VMM:MakeModel),NOCASE         ! By Make && Model    
Record                   RECORD,PRE()
VMMID                       ULONG                          ! Vehicle Make & Model ID
MakeModel                   STRING(35)                     ! Make & Model        
Type                        BYTE                           ! Type of vehicle - Horse, Trailer, Rigid
Capacity                    DECIMAL(6)                     ! In Kgs              
                         END
                     END                       

_View_ContainerParkRates_Client FILE,DRIVER('MSSQL'),PRE(V_CPRC),BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
Key_CID                  KEY(V_CPRC:CID),DUP,NOCASE,OPT    ! By Client           
Key_JID                  KEY(V_CPRC:JID),DUP,NOCASE,OPT    ! By Journey          
CKey_JID_CID             KEY(V_CPRC:JID,V_CPRC:CID),DUP,NOCASE,OPT ! By Journey && Client
Record                   RECORD,PRE()
JID                         LONG                           !                     
CID                         LONG                           !                     
FID                         ULONG                          ! Floor ID            
                         END
                     END                       

Addresses            FILE,DRIVER('MSSQL'),PRE(ADD),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Addresses           
PKey_AID                 KEY(ADD:AID),NOCASE,OPT,PRIMARY   ! By AID              
Key_Name                 KEY(ADD:AddressName),NOCASE       ! By Name             
FKey_SUID                KEY(ADD:SUID),DUP,NOCASE,OPT      ! By Suburb           
FKey_BID                 KEY(ADD:BID),DUP,NOCASE,OPT       ! By Branch           
Record                   RECORD,PRE()
AID                         ULONG                          ! Address ID - note once used certain information should not be changeable, such as the suburb
BID                         ULONG                          ! Branch ID           
AddressName                 STRING(35)                     ! Name of this address
AddressNameSuburb           STRING(50)                     ! Address Name & Suburb
Line1                       STRING(35)                     ! Address line 1      
Line2                       STRING(35)                     ! Address line 2      
SUID                        ULONG                          ! Note: to keep system integrity this should not be changeable once used
PhoneNo                     STRING(20)                     ! Phone no.           
PhoneNo2                    STRING(20)                     ! Phone no. 2         
Fax                         CSTRING(61)                    ! Fax                 
ShowForAllBranches          BYTE                           ! Show this address for all branches
Used_Group                  GROUP                          !                     
Branch                        BYTE                         ! This is used for a Branch
Client                        BYTE                         ! This is used for a Client
Accountant                    BYTE                         ! This is used for an Accountant
Delivery                      BYTE                         ! This is used for a Delivery
ContainerTurnIn               BYTE                         ! This is used for a Container Turn In
Transporter                   BYTE                         ! This is used for a Transporter
Journey                       BYTE                         ! This is used for a Journey
                            END                            !                     
Archived                    BYTE                           ! Mark Address as Archived
Archived_DateTime           STRING(8)                      !                     
Archived_DateTime_Group     GROUP,OVER(Archived_DateTime)  !                     
Archived_Date                 DATE                         !                     
Archived_Time                 TIME                         !                     
                            END                            !                     
                         END
                     END                       

Application_Section_Usage FILE,DRIVER('MSSQL'),PRE(APPSU),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Application Sections are used on these Procedures
PKey_ASUID               KEY(APPSU:ASUID),NOCASE,OPT,PRIMARY ! By App. Section Usage ID
FKey_ASID_ProcedureName  KEY(APPSU:ASID,APPSU:ProcedureName),DUP,NOCASE,OPT ! By Application Section && Procedure Name
Key_ProcedureName        KEY(APPSU:ProcedureName),NOCASE,OPT ! By Procedure Name   
Record                   RECORD,PRE()
ASUID                       ULONG                          !                     
ASID                        ULONG                          !                     
ProcedureName               STRING(100)                    ! Procedure Name      
LastCalledDateTime          STRING(8)                      !                     
LastCalledDateTime_Group    GROUP,OVER(LastCalledDateTime) !                     
LastCalledDate                DATE                         !                     
LastCalledTime                TIME                         !                     
                            END                            !                     
                         END
                     END                       

ApplicationSections_Extras FILE,DRIVER('MSSQL'),PRE(APPSE),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Application Sections Extras
PKey_ASEID               KEY(APPSE:ASEID),NOCASE,OPT,PRIMARY ! By App. Sec. Extra  
FKey_ASID                KEY(APPSE:ASID),DUP,NOCASE,OPT    ! By Application Section
SKey_ASID_ExtraSection   KEY(APPSE:ASID,APPSE:ExtraSection),NOCASE,OPT ! By Extra Section    
Record                   RECORD,PRE()
ASEID                       ULONG                          !                     
ASID                        ULONG                          !                     
ExtraSection                STRING(100)                    !                     
DefaultAction               BYTE                           ! Default action for security is - Allow, View Only, No Access
                         END
                     END                       

EmailAddresses       FILE,DRIVER('MSSQL'),PRE(EMAI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_EAID                KEY(EMAI:EAID),NOCASE,OPT,PRIMARY ! By Email Address ID 
FKey_AID                 KEY(EMAI:AID),DUP,NOCASE,OPT      ! By Address ID       
FKey_CID                 KEY(EMAI:CID),DUP,NOCASE,OPT      ! By Client           
SKey_EmailAddress_CID    KEY(EMAI:EmailAddress,EMAI:CID),DUP,NOCASE !                     
Record                   RECORD,PRE()
EAID                        ULONG                          ! Email Address ID    
AID                         ULONG                          ! Address ID - note once used certain information should not be changeable, such as the suburb
EmailName                   STRING(35)                     ! Name of contact     
EmailAddress                STRING(255)                    ! Email Address       
RateLetter                  BYTE                           ! This email address gets sent rate letters
CID                         ULONG                          ! Client ID           
DefaultAddress              BYTE                           ! Default Email Address
Operations                  BYTE                           ! This email address is for the client Operations team
OperationsReference         STRING(35)                     ! A reference for the Operations team
DefaultOnDI                 BYTE                           ! Always use this email address on new DIs
                         END
                     END                       

Add_Suburbs          FILE,DRIVER('MSSQL'),PRE(SUBU),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Suburbs             
PKey_SUID                KEY(SUBU:SUID),NOCASE,OPT,PRIMARY ! By Suburb ID        
Key_Suburb               KEY(SUBU:Suburb,SUBU:PostalCode),NOCASE ! By Suburb           
FKey_CIID                KEY(SUBU:CIID),DUP,NOCASE,OPT     ! By City             
Record                   RECORD,PRE()
SUID                        ULONG                          ! Suburb ID           
CIID                        ULONG                          ! City ID             
Suburb                      STRING(50)                     ! Suburb              
PostalCode                  STRING(10)                     !                     
                         END
                     END                       

LoadTypes2           FILE,DRIVER('MSSQL'),PRE(LOAD2),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Load Types          
PKey_LTID                KEY(LOAD2:LTID),NOCASE,OPT,PRIMARY ! By Load Type ID     
Key_LoadType             KEY(LOAD2:LoadType),NOCASE        ! By Load Type        
FKey_FID                 KEY(LOAD2:FID),DUP,NOCASE,OPT     ! By Floor            
Record                   RECORD,PRE()
LTID                        ULONG                          ! Type ID             
LoadType                    STRING(100)                    ! Load Type           
LoadOption                  BYTE                           ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
TurnIn                      BYTE                           ! Container Turn In required for this Load Type
Hazchem                     BYTE                           !                     
ContainerParkStandard       BYTE                           ! Container Park Standard Rates
FID                         ULONG                          ! Floor ID            
                         END
                     END                       

TransporterPayments  FILE,DRIVER('MSSQL'),PRE(TRAP),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Transporter Payments
SKey_TPID_Reversal       KEY(TRAP:TPID_Reversal),DUP,NOCASE,OPT ! By Transporter Reversal ID
PKey_TPID                KEY(TRAP:TPID),NOCASE,OPT,PRIMARY ! By Transporter Payment ID
FKey_TID                 KEY(TRAP:TID),DUP,NOCASE,OPT      ! By Transporter      
Record                   RECORD,PRE()
TPID                        ULONG                          !                     
TID                         ULONG                          ! Transporter ID      
DateAndTimeCaptured         STRING(8)                      !                     
DateAndTimeCaptured_GROUP   GROUP,OVER(DateAndTimeCaptured) !                     
DateCaptured                  DATE                         ! Date captured into the system
TimeCaptured                  TIME                         !                     
                            END                            !                     
DateAndTimeMade             STRING(8)                      !                     
DateAndTimeMade_GROUP       GROUP,OVER(DateAndTimeMade)    !                     
DateMade                      DATE                         ! Date payment was made
TimeMade                      TIME                         !                     
                            END                            !                     
Amount                      DECIMAL(10,2)                  ! Amount of payment   
Notes                       STRING(255)                    ! Notes for this payment
Type                        BYTE                           ! Payment, Reversal   
TPID_Reversal               ULONG                          ! Reversal            
                         END
                     END                       

Branches             FILE,DRIVER('MSSQL'),PRE(BRA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Branches            
PKey_BID                 KEY(BRA:BID),NOCASE,OPT,PRIMARY   ! By Branch ID        
Key_BranchName           KEY(BRA:BranchName),NOCASE        ! By Branch Name      
FKey_AID                 KEY(BRA:AID),DUP,NOCASE,OPT       ! By Address ID       
FKey_FID                 KEY(BRA:FID),DUP,NOCASE,OPT       ! By Floor            
Record                   RECORD,PRE()
BID                         ULONG                          ! Branch ID           
BranchName                  STRING(35)                     ! Branch Name         
AID                         ULONG                          ! Address ID          
FID                         ULONG                          ! Floor ID            
GeneralRatesClientID        ULONG                          ! Client ID           
GeneralRatesTransporterID   ULONG                          ! Transporter ID      
GeneralFuelSurchargeClientID ULONG                         ! Client ID           
GeneralTollChargeClientID   ULONG                          ! Client that will be used for Toll Charge
                         END
                     END                       

Add_Cities           FILE,DRIVER('MSSQL'),PRE(CITI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Cities              
PKey_CIID                KEY(CITI:CIID),NOCASE,OPT,PRIMARY ! By City ID          
Key_City                 KEY(CITI:City),NOCASE             ! By City             
FKey_PRID_City           KEY(CITI:PRID,CITI:City),DUP,NOCASE,OPT ! By Province && City 
Record                   RECORD,PRE()
CIID                        ULONG                          ! City ID             
PRID                        ULONG                          ! Province ID         
City                        STRING(35)                     ! City                
                         END
                     END                       

Add_Provinces        FILE,DRIVER('MSSQL'),PRE(PROV),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Provinces           
PKey_PRID                KEY(PROV:PRID),NOCASE,OPT,PRIMARY ! By Province ID      
Key_Province             KEY(PROV:Province),NOCASE         ! By Province         
FKey_COID_Province       KEY(PROV:COID,PROV:Province),DUP,NOCASE,OPT ! By Country && Province
Record                   RECORD,PRE()
PRID                        ULONG                          ! Province ID         
COID                        ULONG                          ! Country ID          
Province                    STRING(35)                     ! Province            
                         END
                     END                       

Accountants          FILE,DRIVER('MSSQL'),PRE(ACCO),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Accountants         
PKey_ACID                KEY(ACCO:ACID),NOCASE,OPT,PRIMARY ! By ACID             
Key_AccountantName       KEY(ACCO:AccountantName),NOCASE   ! By Accountant Name  
FKey_AID                 KEY(ACCO:AID),DUP,NOCASE,OPT      ! By Address ID       
Record                   RECORD,PRE()
ACID                        ULONG                          ! Accountant ID       
AccountantName              STRING(35)                     ! Accountants Name    
AID                         ULONG                          ! Address ID          
                         END
                     END                       

_View_Rates_Clients_LoadTypes FILE,DRIVER('MSSQL'),PRE(V_RATC_L),BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
CKey_CID_LTID            KEY(V_RATC_L:CID,V_RATC_L:LTID),DUP,NOCASE,OPT ! By Client && Load Type
Record                   RECORD,PRE()
LTID                        LONG                           !                     
CID                         LONG                           !                     
                         END
                     END                       

Transporter          FILE,DRIVER('MSSQL'),PRE(TRA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Transporter         
PKey_TID                 KEY(TRA:TID),NOCASE,OPT,PRIMARY   ! By TID              
Key_TransporterName      KEY(TRA:TransporterName),NOCASE   ! By Transporter Name 
FKey_BID                 KEY(TRA:BID),DUP,NOCASE,OPT       ! By Branch ID        
FKey_AID                 KEY(TRA:AID),DUP,NOCASE,OPT       ! By Address ID       
FKey_ACID                KEY(TRA:ACID),DUP,NOCASE,OPT      ! By Accountant ID    
Record                   RECORD,PRE()
TID                         ULONG                          ! Transporter ID      
TransporterName             STRING(35)                     ! Transporters Name   
BID                         ULONG                          ! Branch ID           
AID                         ULONG                          ! Address ID          
ACID                        ULONG                          ! Accountant ID       
OpsManager                  STRING(35)                     !                     
VATNo                       STRING(20)                     ! VAT No.             
Linked_CID                  ULONG                          ! Client ID that this Transporter is linked to
ChargesVAT                  BYTE                           ! This Transporter charges / pays VAT
Broking                     BYTE                           ! This is a broking transporter
Archived                    BYTE                           !                     
Archived_DateTime           STRING(8)                      !                     
Archived_DateTime_Group     GROUP,OVER(Archived_DateTime)  !                     
Archived_Date                 DATE                         !                     
Archived_Time                 TIME                         !                     
                            END                            !                     
Comments                    CSTRING(1001)                  !                     
Status                      BYTE                           ! Normal, Pending, Do Not Use
                         END
                     END                       

Container_Parks      FILE,DRIVER('MSSQL'),PRE(CON),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_CPID                KEY(CON:CPID),NOCASE,OPT,PRIMARY  ! By CPID             
Key_ContainerPark        KEY(CON:ContainerPark),NOCASE     ! By Container Park   
FKey_FID                 KEY(CON:FID),DUP,NOCASE,OPT       ! By Floor            
Record                   RECORD,PRE()
CPID                        ULONG                          ! Container Park IDs  
ContainerPark               STRING(35)                     ! Container Park      
FID                         ULONG                          ! Floor ID            
                         END
                     END                       

LFM_CFile            FILE,DRIVER('MSSQL'),PRE(CFG),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! List Manager   (No replication)
key_Main                 KEY(CFG:AppName,CFG:ProcId,CFG:UserId,CFG:CtrlId,CFG:FormatId),NOCASE,OPT,PRIMARY !                     
Record                   RECORD,PRE()
AppName                     STRING(255)                    !                     
ProcId                      STRING(255)                    !                     
UserId                      SHORT                          !                     
CtrlId                      SHORT                          !                     
FormatId                    SHORT                          !                     
FormatName                  STRING(30)                     !                     
Flag                        BYTE                           !                     
Format                      STRING(5120)                   !                     
VarLine                     STRING(2048)                   !                     
                         END
                     END                       

ClientsPaymentsAllocation FILE,DRIVER('MSSQL'),PRE(CLIPA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Client Payments Invoice Allocation
PKey_CPAID               KEY(CLIPA:CPAID),NOCASE,OPT,PRIMARY ! By Clients Payment Allocation ID
FKey_CPID_AllocationNo   KEY(CLIPA:CPID,CLIPA:AllocationNo),NOCASE,OPT ! By Clients Payment & Allocation Number
Fkey_IID                 KEY(CLIPA:IID),DUP,NOCASE,OPT     ! By Invoice          
FKey_CPID                KEY(CLIPA:CPID),DUP,NOCASE,OPT    ! By Clients Payment  
Record                   RECORD,PRE()
CPAID                       ULONG                          ! Clients Payment Allocation ID
CPID                        ULONG                          ! Cliets Payment ID   
AllocationNo                SHORT                          ! Allocation number   
IID                         ULONG                          ! Invoice Number      
Amount                      DECIMAL(10,2)                  ! Amount of payment allocated to this Invoice
AllocationDateAndTime       STRING(8)                      !                     
AllocationDateAndTime_GROUP GROUP,OVER(AllocationDateAndTime) !                     
AllocationDate                DATE                         ! Date allocation made
AllocationTime                TIME                         !                     
                            END                            !                     
Comment                     CSTRING(256)                   ! Comment             
StatusUpToDate              BYTE                           ! The status is up to date
                         END
                     END                       

Users                FILE,DRIVER('MSSQL'),PRE(USE),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Users               
PKey_UID                 KEY(USE:UID),NOCASE,OPT,PRIMARY   ! By User             
Key_Login                KEY(USE:Login),NOCASE             ! By Login            
SKey_Name_Surname        KEY(USE:Name,USE:Surname),DUP,NOCASE,OPT ! By Name && Surname  
Record                   RECORD,PRE()
UID                         ULONG                          ! User ID             
Login                       STRING(20)                     ! User Login          
Password                    STRING(35)                     ! User Password       
Name                        STRING(35)                     ! User Name           
Surname                     STRING(35)                     ! User Surname        
AccessLevel                 BYTE                           ! Users general access level
                         END
                     END                       

_FuelCost            FILE,DRIVER('MSSQL'),PRE(FUE),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Fuel Cost           
PKey_FCID                KEY(FUE:FCID),NOCASE,OPT,PRIMARY  ! By Fuel Cost        
SKey_EffectiveDate       KEY(FUE:EffectiveDate),DUP,NOCASE,OPT ! By Effective Date   
Record                   RECORD,PRE()
FCID                        ULONG                          ! Fuel Cost ID        
FuelCost                    DECIMAL(10,3)                  ! Cost per litre      
FuelBaseRate                DECIMAL(10,4)                  ! % cost of total cost
Base_FCID                   ULONG                          ! Fuel Cost ID        
EffectiveDate_Str           STRING(8)                      !                     
EffectiveDate_Group         GROUP,OVER(EffectiveDate_Str)  !                     
EffectiveDate                 DATE                         !                     
EffectiveTime                 TIME                         !                     
                            END                            !                     
FuelCostType                BYTE                           ! Type of this Fuel Cost
                         END
                     END                       

ContainerTypes       FILE,DRIVER('MSSQL'),PRE(CTYP),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Container Types     
PKey_CTID                KEY(CTYP:CTID),NOCASE,OPT,PRIMARY ! By Container Type ID
Key_Type                 KEY(CTYP:ContainerType),NOCASE    ! By Container Type   
Record                   RECORD,PRE()
CTID                        ULONG                          ! Container Type ID   
ContainerType               STRING(35)                     ! Type                
Size                        BYTE                           !                     
OpenTop                     BYTE                           ! Open Top            
OverHeight                  BYTE                           !                     
FlatRack                    BYTE                           ! Flat Rack - dimensions will be needed
Reefer                      BYTE                           !                     
                         END
                     END                       

DeliveryItems_Components FILE,DRIVER('MSSQL'),PRE(DELIC),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Delivery Item Components
PKey_DICID               KEY(DELIC:DICID),NOCASE,OPT,PRIMARY ! By ID               
FKey_DIID                KEY(DELIC:DIID),DUP,NOCASE,OPT    ! By Delivery Item    
Record                   RECORD,PRE()
DICID                       ULONG                          !                     
DIID                        ULONG                          ! Delivery Item ID    
Length                      DECIMAL(6)                     ! Length in cm        
Breadth                     DECIMAL(6)                     ! Breadth in cm       
Height                      DECIMAL(6)                     ! Height in cm        
Volume                      DECIMAL(8,3)                   ! Volume for manual entry (metres cubed)
Volume_Unit                 DECIMAL(8,3)                   ! Volume of 1 unit    
Units                       USHORT                         ! Number of units     
Weight                      DECIMAL(8,2)                   ! In kg's             
VolumetricWeight            DECIMAL(8,2)                   ! Weight based on Volumetric calculation (in kgs)
                         END
                     END                       

DeliveryItems        FILE,DRIVER('MSSQL'),PRE(DELI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Delivery Items      
PKey_DIID                KEY(DELI:DIID),NOCASE,OPT,PRIMARY ! By Delivery Item ID 
FKey_DID_ItemNo          KEY(DELI:DID,DELI:ItemNo),NOCASE,OPT ! By Delivery ID && Item No.
FKey_CMID                KEY(DELI:CMID),DUP,NOCASE,OPT     ! By Commodity        
FKey_COID                KEY(DELI:COID),DUP,NOCASE,OPT     ! By Container Operator
FKey_CTID                KEY(DELI:CTID),DUP,NOCASE,OPT     ! By Container Type   
FKey_ContainerReturnAID  KEY(DELI:ContainerReturnAID),DUP,NOCASE,OPT ! By Container Return Address
FKey_PTID                KEY(DELI:PTID),DUP,NOCASE,OPT     ! By Packaging Type ID
Record                   RECORD,PRE()
DIID                        ULONG                          ! Delivery Item ID    
DID                         ULONG                          ! Delivery ID         
ItemNo                      SHORT                          ! Item Number         
CMID                        ULONG                          ! Commodity ID        
Type                        BYTE                           ! Type of Item - Container or Loose
Container_Group             GROUP                          ! Container fields    
ShowOnInvoice                 BYTE                         ! Show these container details on the Invoice generated from this DI
COID                          ULONG                        ! Container Operator ID
CTID                          ULONG                        ! Container Type ID   
ContainerNo                   CSTRING(36)                  !                     
ContainerReturnAID            ULONG                        ! Address ID - note once used certain information should not be changeable, such as the suburb
ContainerVessel               CSTRING(36)                  ! Vessel this container arrived on
SealNo                        CSTRING(36)                  ! Container Seal no.  
DateAndTimeETA                STRING(8)                    !                     
DateAndTimeETA_GROUP          GROUP,OVER(DateAndTimeETA)   !                     
ETA                             DATE                       ! Estimated time of arrival for this Vessel
ETA_TIME                        TIME                       ! Not used!           
                              END                          !                     
                            END                            !                     
Loose_Group                 GROUP                          !                     
ByContainer                   BYTE                         ! Are the items arriving by container. If so allow capturing of container info minus Return Address
Length                        DECIMAL(5,3)                 ! Length in metres    
Breadth                       DECIMAL(5,3)                 ! Breadth in metres   
Height                        DECIMAL(5,3)                 ! Height in metres    
Volume                        DECIMAL(8,3)                 ! Volume for manual entry (metres cubed)
Volume_Unit                   DECIMAL(8,3)                 ! Volume of 1 unit    
Units                         USHORT                       ! Number of units     
PTID                          ULONG                        ! Packaging Type ID   
                            END                            !                     
Weight                      DECIMAL(8,2)                   ! In kg's             
VolumetricRatio             DECIMAL(8,2)                   ! x square cubes weigh this amount
VolumetricWeight            DECIMAL(8,2)                   ! Weight based on Volumetric calculation (in kgs)
Delivered_Group             GROUP                          !                     
DeliveredUnits                USHORT                       ! Units delivered     
                            END                            !                     
                         END
                     END                       

_View_Rates_Client   FILE,DRIVER('MSSQL'),PRE(V_RATC),BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
Key_CID                  KEY(V_RATC:CID),DUP,NOCASE,OPT    ! By Client           
CKey_JID_CID             KEY(V_RATC:JID,V_RATC:CID),DUP,NOCASE,OPT ! By Journey & Client 
Record                   RECORD,PRE()
CID                         LONG                           !                     
JID                         LONG                           !                     
                         END
                     END                       

DeliveryComposition  FILE,DRIVER('MSSQL'),PRE(DELC),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Delivery Composition
PKey_DELCID              KEY(DELC:DELCID),NOCASE,OPT,PRIMARY ! By DC ID            
FKey_CID                 KEY(DELC:CID),DUP,NOCASE,OPT      ! By Client           
Record                   RECORD,PRE()
DELCID                      ULONG                          ! Delivery Composition ID
CID                         ULONG                          ! Client ID           
Reference                   CSTRING(36)                    ! Reference for this Delivery Composition
DateTime                    STRING(8)                      ! Date & Time         
DateTime_Group              GROUP,OVER(DateTime)           !                     
DateAdded                     DATE                         !                     
TimeAdded                     TIME                         !                     
                            END                            !                     
TotalsGroup                 GROUP                          !                     
Items                         USHORT                       ! Total no. of items  
Weight                        DECIMAL(15,2)                ! Total weight        
Volume                        DECIMAL(12,3)                ! Total volume        
Deliveries                    USHORT                       ! Total deliveries    
                            END                            !                     
DeliveriesLoaded            BYTE                           ! All deliveries have been loaded
Manifested                  BYTE                           ! All deliveries have been manifested
Delivered                   BYTE                           ! All deliveries have been delivered
                         END
                     END                       

Commodities          FILE,DRIVER('MSSQL'),PRE(COM),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Commodities         
PKey_CMID                KEY(COM:CMID),NOCASE,OPT,PRIMARY  ! By Commodity ID     
Key_Commodity            KEY(COM:Commodity),NOCASE         ! By Commodity        
Record                   RECORD,PRE()
CMID                        ULONG                          ! Commodity ID        
Commodity                   STRING(35)                     ! Commodity           
                         END
                     END                       

UserGroups           FILE,DRIVER('MSSQL'),PRE(USEG),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_UGID                KEY(USEG:UGID),NOCASE,OPT,PRIMARY ! By User Group ID    
Key_GroupName            KEY(USEG:GroupName),NOCASE        ! By Group Name       
Record                   RECORD,PRE()
UGID                        ULONG                          ! User Group ID       
GroupName                   STRING(35)                     ! Name of this group of users
                         END
                     END                       

DeliveriesAdditionalCharges FILE,DRIVER('MSSQL'),PRE(DELA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Deliveries Additional Charges (amounts used only to accumulate to Delivery Add. Charge amt)
PKey_DAID                KEY(DELA:DAID),NOCASE,OPT,PRIMARY ! By Additional Charge ID
FKey_DID                 KEY(DELA:DID),DUP,NOCASE,OPT      ! By Delivery ID      
FKey_ACID                KEY(DELA:ACID),DUP,NOCASE,OPT     ! By Additional Charge
Record                   RECORD,PRE()
DAID                        ULONG                          ! Delivery Additional ID
DID                         ULONG                          ! Delivery ID         
ACID                        ULONG                          ! Additional Charges ID
AdditionalCharge            STRING(35)                     ! Description         
Charge                      DECIMAL(11,2)                  !                     
                         END
                     END                       

Journeys             FILE,DRIVER('MSSQL'),PRE(JOU),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Journeys            
PKey_JID                 KEY(JOU:JID),NOCASE,OPT,PRIMARY   ! By Journey ID       
Key_Journey              KEY(JOU:Journey),NOCASE           ! By Journey          
FKey_BID                 KEY(JOU:BID),DUP,NOCASE,OPT       ! By Branch           
FKey_FID                 KEY(JOU:FID),DUP,NOCASE,OPT       ! By Floor            
FKey_FID2                KEY(JOU:FID2),DUP,NOCASE          !                     
Record                   RECORD,PRE()
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
Journey                     CSTRING(701)                   ! Description         
Description                 CSTRING(1501)                  !                     
BID                         ULONG                          ! Branch ID           
FID                         ULONG                          ! Floor ID            
FID2                        ULONG                          ! To/From Floor       
EToll                       BYTE                           ! Is an E-Toll normally incurred on this Journey?
                         END
                     END                       

ClientsPayments      FILE,DRIVER('MSSQL'),PRE(CLIP),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Client Payments     
PKey_CPID                KEY(CLIP:CPID),NOCASE,OPT,PRIMARY ! By CPID             
FKey_CID                 KEY(CLIP:CID),DUP,NOCASE,OPT      ! By Client           
Key_CPIDReversal         KEY(CLIP:CPID_Reversal),DUP,NOCASE,OPT ! By Client Payment Reversed
Record                   RECORD,PRE()
CPID                        ULONG                          ! Cliets Payment ID   
CID                         ULONG                          ! Client ID           
DateAndTimeCaptured         STRING(8)                      !                     
DateAndTimeCaptured_GROUP   GROUP,OVER(DateAndTimeCaptured) !                     
DateCaptured                  DATE                         ! Date captured into the system
TimeCaptured                  TIME                         !                     
                            END                            !                     
DateAndTimeMade             STRING(8)                      !                     
DateAndTimeMade_GROUP       GROUP,OVER(DateAndTimeMade)    !                     
DateMade                      DATE                         ! Date payment was made
TimeMade                      TIME                         !                     
                            END                            !                     
Amount                      DECIMAL(10,2)                  ! Amount of payment   
Notes                       STRING(255)                    ! Notes for this payment
Type                        BYTE                           ! Payment, Reversal   
CPID_Reversal               ULONG                          ! Reversal            
Status                      BYTE                           ! Status - Not Allocated, Partial Allocation, Fully Allocated
StatusUpToDate              BYTE                           ! The status is up to date
                         END
                     END                       

Floors               FILE,DRIVER('MSSQL'),PRE(FLO),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Floors are locations or stores
PKey_FID                 KEY(FLO:FID),NOCASE,OPT,PRIMARY   ! By Floor ID         
Key_Floor                KEY(FLO:Floor),NOCASE             ! By Floor            
FKey_AID                 KEY(FLO:AID),DUP,NOCASE,OPT       ! By Address          
Record                   RECORD,PRE()
FID                         ULONG                          ! Floor ID            
Floor                       STRING(35)                     ! Floor Name          
FBNFloor                    BYTE                           ! Is this a FBN Floor 
AID                         ULONG                          ! Address ID - note once used certain information should not be changeable, such as the suburb
Print_Rates                 BYTE                           ! Print this Floors rates on rate letters
TripSheetLoading            BYTE                           ! Show this Floor on the Trip Sheet Loading
                         END
                     END                       

DeliveryStatuses     FILE,DRIVER('MSSQL'),PRE(DELS),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_DSID                KEY(DELS:DSID),NOCASE,OPT,PRIMARY ! By Delivery Status ID
Key_Status               KEY(DELS:DeliveryStatus),NOCASE   ! By Status           
Record                   RECORD,PRE()
DSID                        ULONG                          ! Delivery Status ID  
DeliveryStatus              STRING(35)                     !                     
ReportColumn                BYTE                           ! Select the report column that you want this status date to show under
                         END
                     END                       

AddressContacts      FILE,DRIVER('MSSQL'),PRE(ADDC),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Address Contacts    
PKey_ACID                KEY(ADDC:ACID),NOCASE,OPT,PRIMARY ! By Address Contacts ID
FKey_AID                 KEY(ADDC:AID),DUP,NOCASE,OPT      ! By Address          
Key_ContactName          KEY(ADDC:ContactName,ADDC:AID),NOCASE ! By Contact Name     
Record                   RECORD,PRE()
ACID                        ULONG                          ! Address Contact ID  
AID                         ULONG                          ! Address ID - note once used certain information should not be changeable, such as the suburb
ContactName                 STRING(35)                     ! Contacts Name       
PrimaryContact              BYTE                           ! Is this contact the primary contact?
                         END
                     END                       

ClientsRateTypes     FILE,DRIVER('MSSQL'),PRE(CRT),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Clients Rate Types (Load Types)
PKey_CRTID               KEY(CRT:CRTID),NOCASE,OPT,PRIMARY ! By Client Rate Type ID
Key_ClientRateType       KEY(CRT:ClientRateType),DUP,NOCASE ! By Client Rate Type 
FKey_CID                 KEY(CRT:CID),DUP,NOCASE,OPT       ! By Client           
FKey_LTID                KEY(CRT:LTID),DUP,NOCASE,OPT      ! By Load Type        
SKey_CID_ClientRateType  KEY(CRT:CID,CRT:ClientRateType),NOCASE,OPT ! By Client && Rate Type
Record                   RECORD,PRE()
CRTID                       ULONG                          ! Client Rate Type ID 
CID                         ULONG                          ! Client ID           
ClientRateType              STRING(100)                    ! Load Type           
LTID                        ULONG                          ! Type ID             
                         END
                     END                       

Vessels              FILE,DRIVER('MSSQL'),PRE(VES),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Vessels             
PKey_VEID                KEY(VES:VEID),NOCASE,OPT,PRIMARY  ! By Vessel ID        
Key_Vessel               KEY(VES:Vessel),NOCASE,OPT        ! By Vessel           
Record                   RECORD,PRE()
VEID                        ULONG                          ! Vessel ID           
Vessel                      STRING(50)                     ! Vessel Name         
                         END
                     END                       

Delivery_CODAddresses FILE,DRIVER('MSSQL'),PRE(DCADD),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Delivery COD / Pre Paid Addresses
PKey_DC_ID               KEY(DCADD:DC_ID),NOCASE,OPT,PRIMARY ! By Delivery COD Address ID
Key_AddressName          KEY(DCADD:AddressName),NOCASE     ! By Address Name     
Record                   RECORD,PRE()
DC_ID                       ULONG                          ! Delivery COD Addresses ID
AddressName                 STRING(35)                     ! Name of this address
Line1                       STRING(35)                     ! Address line 1      
Line2                       STRING(35)                     ! Address line 2      
Line3                       STRING(35)                     !                     
Line4                       STRING(35)                     !                     
Line5                       STRING(35)                     !                     
VATNo                       STRING(20)                     ! VAT No.             
                         END
                     END                       

ContainerOperators   FILE,DRIVER('MSSQL'),PRE(CONO),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Container Operators 
PKey_COID                KEY(CONO:COID),NOCASE,OPT,PRIMARY ! By Container Operator ID
Key_Operator             KEY(CONO:ContainerOperator),NOCASE ! By Operator         
Record                   RECORD,PRE()
COID                        ULONG                          ! Container Operator ID
ContainerOperator           STRING(35)                     ! Container Operator  
                         END
                     END                       

Drivers              FILE,DRIVER('MSSQL'),PRE(DRI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Drivers             
FKey_BID                 KEY(DRI:BID),DUP,NOCASE,OPT       ! By Branch           
PKey_DRID                KEY(DRI:DRID),NOCASE,OPT,PRIMARY  ! By Driver ID        
Key_FirstNameSurname     KEY(DRI:FirstName,DRI:Surname),NOCASE ! By First Name && Surname
SKey_FirstNameSurname    KEY(DRI:FirstNameSurname),DUP,NOCASE,OPT ! By First Name && Surname (c)
Record                   RECORD,PRE()
DRID                        ULONG                          ! Drivers ID          
FirstName                   STRING(35)                     ! First Name          
Surname                     STRING(35)                     ! Surname             
EmployeeNo                  STRING(20)                     ! Employee No.        
Type                        BYTE                           ! Driver or Assistant 
FirstNameSurname            STRING(70)                     ! Firstname & Surname 
Category                    BYTE                           ! Driver Category     
BID                         ULONG                          ! Branch ID           
Archived                    BYTE                           ! Mark driver as not active
Archived_DateTime           STRING(8)                      !                     
Archived_DateTime_Group     GROUP,OVER(Archived_DateTime)  !                     
Archived_Date                 DATE                         !                     
Archived_Time                 TIME                         !                     
                            END                            !                     
                         END
                     END                       

RemindersUsers       FILE,DRIVER('MSSQL'),PRE(REU),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Users Reminders options
PKey_RUID                KEY(REU:RUID),NOCASE,PRIMARY      ! By Reminders Users ID
FKey_RID                 KEY(REU:RID),DUP,NOCASE,OPT       ! By Reminder         
FKey_UID                 KEY(REU:UID),DUP,NOCASE,OPT       ! By User             
SKey_UID_RID             KEY(REU:UID,REU:RID),DUP,NOCASE,OPT ! By User && Reminder 
Record                   RECORD,PRE()
RUID                        ULONG                          ! Reminders Users ID  
RID                         ULONG                          ! Reminder ID         
UID                         ULONG                          ! User ID             
NoAction                    BYTE                           ! This user will take no action on this reminder
ReminderDateTime            STRING(8)                      !                     
ReminderDateTimeGroup       GROUP,OVER(ReminderDateTime)   !                     
ReminderDate                  DATE                         !                     
ReminderTime                  TIME                         !                     
                            END                            !                     
                         END
                     END                       

_InvoiceItems        FILE,DRIVER('MSSQL'),PRE(INI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Invoice Items       
PKey_ITID                KEY(INI:ITID),NOCASE,OPT,PRIMARY  ! By Item ID          
FKey_IID                 KEY(INI:IID),DUP,NOCASE,OPT       ! By Invoice          
FKey_DIID                KEY(INI:DIID),DUP,NOCASE,OPT      ! By Delivery Item    
FKey_CMID                KEY(INI:CMID),DUP,NOCASE,OPT      ! By Commodity        
Record                   RECORD,PRE()
ITID                        ULONG                          ! Invoice Item ID     
IID                         ULONG                          ! Invoice ID          
DIID                        ULONG                          ! Delivery Item ID    
ItemNo                      USHORT                         ! Item Number         
Type                        BYTE                           ! Type of Item - Container or Loose
CMID                        ULONG                          ! Commodity ID        
Commodity                   CSTRING(36)                    ! Commodity           
Description                 CSTRING(151)                   ! Description - populate with fields from Delivery Item rec.
Units                       USHORT                         !                     
ContainerDescription        CSTRING(151)                   !                     
                         END
                     END                       

DeliveryLegs         FILE,DRIVER('MSSQL'),PRE(DELL),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Extra Delivery Legs - see comments
PKey_DLID                KEY(DELL:DLID),NOCASE,OPT,PRIMARY ! By Delivery Leg ID  
FKey_DID_Leg             KEY(DELL:DID,DELL:Leg),DUP,NOCASE,OPT ! By Delivery ID && Leg
FKey_TID                 KEY(DELL:TID),DUP,NOCASE,OPT      ! By Transporter ID   
FKey_JID                 KEY(DELL:JID),DUP,NOCASE,OPT      ! By Journey          
FKey_CollectionAID       KEY(DELL:CollectionAID),DUP,NOCASE,OPT ! By Collection AID   
FKey_DeliveryAID         KEY(DELL:DeliveryAID),DUP,NOCASE,OPT ! By Delivery AID     
Record                   RECORD,PRE()
DLID                        ULONG                          ! Delivery Leg ID     
DID                         ULONG                          ! Delivery ID         
Leg                         SHORT                          ! Leg Number          
TID                         ULONG                          ! Transporter ID      
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
CollectionAID               ULONG                          ! Collection Address ID
DeliveryAID                 ULONG                          ! Delivery Address ID 
ChargesGroup                GROUP                          !                     
Cost                          DECIMAL(9,2)                 ! Cost for this leg (from Transporter)
VATRate                       DECIMAL(5,2)                 ! VAT rate            
                            END                            !                     
                         END
                     END                       

Shortages_Damages    FILE,DRIVER('MSSQL'),PRE(SHO),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Shortages & Damages Reports
PKey_SDID                KEY(SHO:SDID),NOCASE,OPT,PRIMARY  ! By Shortages && Damages ID
FKey_DID                 KEY(SHO:DID),DUP,NOCASE,OPT       ! By Delivery         
FKey_TRID                KEY(SHO:TRID),DUP,NOCASE,OPT      ! By Tripsheet        
FKey_IID                 KEY(SHO:IID),DUP,NOCASE,OPT       ! By Invoice          
FKey_C_BID               KEY(SHO:CollectionBID),DUP,NOCASE,OPT ! By Collection Branch
FKey_TripSheet_DRID      KEY(SHO:TripSheetDriverID),DUP,NOCASE,OPT ! By Tripsheet Driver 
FKey_Delivery_DRID       KEY(SHO:DeliveryDriverID),DUP,NOCASE,OPT ! By Delivery Driver  
Record                   RECORD,PRE()
SDID                        ULONG                          ! SD ID               
General_Group               GROUP                          !                     
DID                           ULONG                        ! Delivery ID         
DINo                          ULONG                        ! Delivery Instruction Number
Items                         STRING(35)                   ! Delivery items that were a problem (item nos.)
TRID                          ULONG                        ! Tripsheet ID        
TripsheetItems                STRING(35)                   ! Tripsheet Items     
IID                           ULONG                        ! Invoice Number      
POD_IID                       ULONG                        ! POD No. - same as invoice except for Journal debit of invoice
DeliveryDepot                 STRING(35)                   ! Delivery Depot      
CollectionBID                 ULONG                        ! Branch ID           
TripSheetDriverID             ULONG                        ! Drivers ID          
FLDriver1                     STRING(35)                   ! F/L Driver 1        
Supervisor1                   STRING(35)                   !                     
DeliveryDriverID              ULONG                        ! Drivers ID          
FLDriver2                     STRING(35)                   ! F/L Driver 2        
Supervisor2                   STRING(35)                   !                     
DI_DateTime                   STRING(8)                    !                     
DI_DateTime_Group             GROUP,OVER(DI_DateTime)      !                     
DI_Date                         DATE                       !                     
DI_Time                         TIME                       !                     
                              END                          !                     
Create_DateTime               STRING(8)                    !                     
Create_DateTime_Group         GROUP,OVER(Create_DateTime)  !                     
Create_Date                     DATE                       !                     
Create_Time                     TIME                       !                     
                              END                          !                     
ConsigneeCID                  ULONG                        ! Client ID           
ConsigneeAID                  ULONG                        ! Address entry ID, Consignee
RemarksOnPOD                  STRING(255)                  !                     
ReportNo                      STRING(35)                   ! CRO or DUPR No. (attach copy)
ReportComments                STRING(255)                  ! Shortage or Damage Comments on CRO or DUPR
                            END                            !                     
ConditionUplift_Group       GROUP                          !                     
Damaged                       BYTE                         !                     
Unpacked                      BYTE                         !                     
Short                         BYTE                         !                     
Wet                           BYTE                         !                     
Taped                         BYTE                         !                     
Palletised                    BYTE                         !                     
Loose                         BYTE                         !                     
Other                         BYTE                         !                     
ConditionComment              STRING(255)                  ! Condition of Cargo when uplifted
                            END                            !                     
Driver_Group                GROUP                          !                     
DriversNotationDI             STRING(255)                  !                     
DriverSigned                  BYTE                         ! Did the driver sign the clients documents
DriverCommentsEntered         BYTE                         ! Did the driver insert comments on the clients document
DriverComments                STRING(255)                  ! Driver Comments on Clients Documents
                            END                            !                     
Depot_Group                 GROUP                          ! FBN Depot           
DepotConditionReceivedDispatch STRING(255)                 ! Condition of goods when received by dispatch
DepotConditionReceived        BYTE                         ! Did the consignment arrive at the FBN depot in the same condition as the Driver received it
DepotSupervisorNotation       BYTE                         ! If not received in uplift condition by depot, did the supervisor make a notation on the drivers DI
DepotRemarks                  STRING(255)                  ! Remarks             
DepotWarningIssuedDriver      BYTE                         ! If goods were not received by the depot in uplift condition was a warning issued to the driver
                            END                            !                     
Destination_Depot_Group     GROUP                          ! FBN Destination     
DestConditionReceived         BYTE                         ! Is the condition the same as received by dispatch depot
DestinationCondition_Group    GROUP                        !                     
DestShort                       BYTE                       !                     
DestDamaged                     BYTE                       !                     
DestUnPacked                    BYTE                       !                     
DestLoose                       BYTE                       !                     
DestTaped                       BYTE                       !                     
DestPalletised                  BYTE                       !                     
                              END                          !                     
DestFurtherDamages            BYTE                         ! Were there further damages
DestFurtherDamages_Repacked   BYTE                         ! If further damages, was cargo Repacked
DestFurtherDamages_Retaped    BYTE                         ! If further damages, was cargo Retaped
DestFurtherDamages_Remarks    STRING(255)                  !                     
DestFurtherDamages_Materials_Straps BYTE                   !                     
DestFurtherDamages_Materials_ClearTape BYTE                !                     
DestFurtherDamages_Materials_B_Tape BYTE                   !                     
DestFurtherDamages_Materials_Boxes BYTE                    !                     
DestFurtherDamages_Materials_Other STRING(35)              !                     
DestClientReportedDamages     BYTE                         !                     
DestClientReportedDamagesComments CSTRING(255)             !                     
                            END                            !                     
Final_Destination_Condition_Group GROUP                    !                     
FinalDestination_Condition    STRING(255)                  ! Condition of Cargo at final destination
FinalDestination_Reported     STRING(255)                  ! Any damages reported and noted by client
FinalDestination_DriversCommentsOffLoading STRING(255)     ! Drivers comments regarding the off loading at client
                            END                            !                     
Office_Group                GROUP                          !                     
DriverAdvised_POD_Endorsed    BYTE                         ! Did the driver advise the office that the POD was endorsed
DriverAdvised_Person          STRING(35)                   ! Who the driver advised of endorsed POD
ClientPhoned                  BYTE                         ! Did the client phone any FBN staff member
ClientPhoned_Information      STRING(255)                  ! Information from client
WhatActionShouldHaveBeenTaken STRING(255)                  ! In your own words what action should have been taken to prevent this incident
HaveWarningsBeenIssued        BYTE                         ! Have any warnings been issued to FBN staff
WarningIssuedTo               STRING(35)                   !                     
StolenGoods_ReportedToPolice  BYTE                         ! If goods were stolen has this been reported to the police
PoliceReference               STRING(35)                   !                     
WhoThoughtReponsible          STRING(35)                   ! Who do you think is responsible
AnyOtherRelevantInformation   STRING(255)                  !                     
                            END                            !                     
FinalComments_Group         GROUP                          !                     
InvestigationCompleted_DateTime STRING(8)                  !                     
InvestigationCompleted_DateTime_Group GROUP,OVER(InvestigationCompleted_DateTime) !                     
DateInvestigationCompleted      DATE                       !                     
InvestigationCompleted_Time     TIME                       !                     
                              END                          !                     
ConsignmentCollectedBy        STRING(35)                   ! (Driver)            
SupervisorSignedForCargo      STRING(35)                   ! Supervisor who signed for cargo
ConsignmentDeliveredBy        STRING(35)                   ! (Driver)            
SupervisorSignedForCargoDelivered STRING(35)               ! Supervisor who signed for cargo to be delivered
CauseOfDamage                 STRING(50)                   !                     
BranchResponsible             STRING(35)                   ! for damage          
LocalDriverResponsible        STRING(35)                   ! for damage          
LongDistanceDriverResponsible STRING(35)                   ! for damage          
ClientResponsible             STRING(35)                   ! for damage          
SupervisorAcceptedDamagedCargo STRING(35)                  !                     
SupervisorInvestigatingDamages STRING(35)                  !                     
CorrectiveActionTaken         CSTRING(1001)                !                     
WarningsIssued1               STRING(50)                   !                     
WarningsIssued2               STRING(50)                   !                     
WarningsIssued3               STRING(50)                   !                     
                            END                            !                     
                         END
                     END                       

_Statements          FILE,DRIVER('MSSQL'),PRE(STA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_STID                KEY(STA:STID),NOCASE,OPT,PRIMARY  ! By Statement ID     
FKey_CID                 KEY(STA:CID),DUP,NOCASE,OPT       ! By Client           
FKey_BID                 KEY(STA:BID),DUP,NOCASE,OPT       ! By Branch           
FKey_STRID               KEY(STA:STRID),DUP,NOCASE,OPT     ! By Statement Run ID 
Record                   RECORD,PRE()
STID                        ULONG                          ! Statement ID        
STRID                       ULONG                          ! Statement Run ID    
StatementDateAndTime        STRING(8)                      !                     
StatementDateAndTime_GROUP  GROUP,OVER(StatementDateAndTime) !                     
StatementDate                 DATE                         ! Statement Date      
StatementTime                 TIME                         !                     
                            END                            !                     
CID                         ULONG                          ! Client ID           
BID                         ULONG                          ! Branch ID           
Days90                      DECIMAL(10,2)                  ! 90 Days             
Days60                      DECIMAL(10,2)                  ! 60 Days             
Days30                      DECIMAL(10,2)                  ! 30 Days             
Current                     DECIMAL(10,2)                  ! Current             
Total                       DECIMAL(10,2)                  ! Total               
Paid                        DECIMAL(10,2)                  ! Paid since last statement
Paid_STID                   ULONG                          ! Paid since this Statement ID
                         END
                     END                       

ServiceRequirements  FILE,DRIVER('MSSQL'),PRE(SERI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Service Requirements
PKey_SID                 KEY(SERI:SID),NOCASE,OPT,PRIMARY  ! By Service Requirement ID
Key_ServiceRequirement   KEY(SERI:ServiceRequirement),NOCASE ! By Service Requirment
Record                   RECORD,PRE()
SID                         ULONG                          ! Service Requirement ID
ServiceRequirement          STRING(35)                     ! Service Requirement 
Broking                     BYTE                           !                     
                         END
                     END                       

_Invoice             FILE,DRIVER('MSSQL'),PRE(INV),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Invoice             
PKey_IID                 KEY(INV:IID),NOCASE,OPT,PRIMARY   ! By Invoice ID       
FKey_ICID                KEY(INV:ICID),DUP,NOCASE,OPT      ! By Invoice Composition
FKey_BID                 KEY(INV:BID),DUP,NOCASE,OPT       ! By Branch           
FKey_CID                 KEY(INV:CID),DUP,NOCASE,OPT       ! By Client           
FKey_DID                 KEY(INV:DID),DUP,NOCASE,OPT       ! By Delivery         
SKey_DINo                KEY(INV:DINo),DUP,NOCASE,OPT      ! By DI No.           
Key_POD_IID              KEY(INV:POD_IID),DUP,NOCASE,OPT   ! By POD              
Key_CR_IID               KEY(INV:CR_IID),DUP,NOCASE,OPT    ! By Credited Invoice 
FKey_MID                 KEY(INV:MID),DUP,NOCASE,OPT       ! By Generating MID   
FKey_IJID                KEY(INV:IJID),DUP,NOCASE,OPT      ! By Journal          
SKey_Date_Time           KEY(INV:InvoiceDateAndTime),DUP   !                     
FKey_CID_Date            KEY(INV:CID,INV:InvoiceDateAndTime),DUP !                     
Record                   RECORD,PRE()
IID                         ULONG                          ! Invoice Number      
POD_IID                     ULONG                          ! POD No. - same as invoice except for Journal debit/credit of invoice
CR_IID                      ULONG                          ! Credit of Invoice, Invoice IID of credited Invoice
BID                         ULONG                          ! Branch ID           
BranchName                  CSTRING(36)                    ! Branch Name         
CID                         ULONG                          ! Client ID           
Client_Group                GROUP                          ! Clients details at invoice generation
ClientNo                      ULONG                        ! Client No.          
ClientName                    CSTRING(101)                 !                     
ClientLine1                   CSTRING(36)                  ! Address line 1      
ClientLine2                   CSTRING(36)                  ! Address line 2      
ClientSuburb                  CSTRING(51)                  ! Suburb              
ClientPostalCode              CSTRING(11)                  !                     
VATNo                         CSTRING(21)                  ! VAT No.             
InvoiceMessage                CSTRING(256)                 !                     
                            END                            !                     
DID                         ULONG                          ! Delivery ID         
DINo                        ULONG                          ! Delivery Instruction Number
ClientReference             CSTRING(61)                    ! Client Reference    
MIDs                        CSTRING(101)                   ! List of Manifest IDs that the delivery is currently manifested on
Terms                       BYTE                           ! Terms - Pre Paid, COD, Account
ICID                        ULONG                          ! Invoice Composition ID
Shipper_Group               GROUP                          !                     
ShipperName                   CSTRING(36)                  ! Name of this address
ShipperLine1                  CSTRING(36)                  ! Address line 1      
ShipperLine2                  CSTRING(36)                  ! Address line 2      
ShipperSuburb                 CSTRING(51)                  ! Suburb              
ShipperPostalCode             CSTRING(11)                  !                     
                            END                            !                     
Consignee_Group             GROUP                          !                     
ConsigneeName                 CSTRING(36)                  ! Name of this address
ConsigneeLine1                CSTRING(36)                  ! Address line 1      
ConsigneeLine2                CSTRING(36)                  ! Address line 2      
ConsigneeSuburb               CSTRING(51)                  ! Suburb              
ConsigneePostalCode           CSTRING(11)                  !                     
                            END                            !                     
InvoiceDateAndTime          STRING(8)                      !                     
InvoiceDateAndTime_GROUP    GROUP,OVER(InvoiceDateAndTime) !                     
InvoiceDate                   DATE                         ! Invoice Date        
InvoiceTime                   TIME                         ! Invoice Time - generated time
                            END                            !                     
Printed                     BYTE                           ! Printed - every other copy must specify "copy invoice"
Charges_Group               GROUP                          !                     
Insurance                     DECIMAL(10,2)                !                     
Documentation                 DECIMAL(10,2)                !                     
FuelSurcharge                 DECIMAL(10,2)                !                     
AdditionalCharge              DECIMAL(11,2)                ! Additional charges  
FreightCharge                 DECIMAL(11,2)                !                     
VAT                           DECIMAL(10,2)                ! VAT                 
Total                         DECIMAL(11,2)                !                     
TollCharge                    DECIMAL(11,2)                !                     
                            END                            !                     
VATRate                     DECIMAL(5,2)                   ! VAT rate            
Weight                      DECIMAL(8,2)                   ! In kg's             
VolumetricWeight            DECIMAL(8,2)                   ! Weight based on Volumetric calculation (in kgs)
Volume                      DECIMAL(8,3)                   ! Volume for manual entry (metres cubed)
BadDebt                     BYTE                           ! This is a Bad Debt Credit Note
Status                      BYTE                           ! used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note (shown), Bad Debt (shown), Over Paid
StatusUpToDate              BYTE                           ! The status is up to date
MID                         ULONG                          ! Generating MID      
UID                         ULONG                          ! User ID             
DC_ID                       ULONG                          ! Delivery COD Addresses ID
IJID                        ULONG                          ! Invoice Journal ID  
Created_DateTime            STRING(8)                      ! Created Date & Time 
Created_DateTime_Group      GROUP,OVER(Created_DateTime)   !                     
Created_Date                  DATE                         !                     
Created_Time                  TIME                         !                     
                            END                            !                     
                         END
                     END                       

ManifestLoad         FILE,DRIVER('MSSQL'),PRE(MAL),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Manifest Truck / Trailer Load
PKey_MLID                KEY(MAL:MLID),NOCASE,OPT,PRIMARY  ! By Manifest Load ID 
FKey_MID                 KEY(MAL:MID),DUP,NOCASE,OPT       ! By Manifest         
FKey_TTID                KEY(MAL:TTID),DUP,NOCASE,OPT      ! By Truck or Trailer 
SKey_MID_TTID            KEY(MAL:MID,MAL:TTID),NOCASE      ! By Manifest && Truck /  Trailer
Record                   RECORD,PRE()
MLID                        ULONG                          ! Manifest Load ID    
MID                         ULONG                          ! Manifest ID         
TTID                        ULONG                          ! Track or Trailer ID 
                         END
                     END                       

ManifestLoadDeliveries FILE,DRIVER('MSSQL'),PRE(MALD),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Manifest Load Deliveries
PKey_MLDID               KEY(MALD:MLDID),NOCASE,OPT,PRIMARY ! By Manifest Load Delivery Items ID
FSKey_MLID_DIID          KEY(MALD:MLID,MALD:DIID),NOCASE,OPT ! By Manifest Load && Delivery Item
FKey_DIID                KEY(MALD:DIID),DUP,NOCASE,OPT     ! By Delivery Items ID
Record                   RECORD,PRE()
MLDID                       ULONG                          ! Manifest Load Delivery Items ID
MLID                        ULONG                          ! Manifest Load ID    
DIID                        ULONG                          ! Delivery Item ID    
UnitsLoaded                 USHORT                         ! Number of units     
                         END
                     END                       

TransporterPaymentsAllocations FILE,DRIVER('MSSQL'),PRE(TRAPA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Transporter Payments Allocations
FKey_TIN                 KEY(TRAPA:TIN),DUP,NOCASE,OPT     ! By Transporter Invoice
PKey_TRPAID              KEY(TRAPA:TRPAID),NOCASE,OPT,PRIMARY ! By TRPAID           
FKey_TPID_AllocationNo   KEY(TRAPA:TPID,TRAPA:AllocationNo),NOCASE,OPT ! By Transporter Payment && Allocation No.
FKey_MID                 KEY(TRAPA:MID),DUP,NOCASE,OPT     ! By Manifest         
Record                   RECORD,PRE()
TRPAID                      ULONG                          ! Trasnporter Payment Allocation ID
TPID                        ULONG                          !                     
AllocationNo                SHORT                          ! Allocation number   
AllocationDateAndTime       STRING(8)                      !                     
AllocationDateAndTime_GROUP GROUP,OVER(AllocationDateAndTime) !                     
AllocationDate                DATE                         ! Date allocation made
AllocationTime                TIME                         !                     
                            END                            !                     
Amount                      DECIMAL(10,2)                  ! Amount of payment allocated to this Invoice
Comment                     STRING(255)                    ! Comment             
MID                         ULONG                          ! Manifest ID         
TIN                         ULONG                          ! Transporter Invoice No.
StatusUpToDate              BYTE                           ! The status is up to date
                         END
                     END                       

TripSheets           FILE,DRIVER('MSSQL'),PRE(TRI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Trip Sheets         
PKey_TID                 KEY(TRI:TRID),NOCASE,OPT,PRIMARY  ! By Tripsheet ID     
FKey_BID                 KEY(TRI:BID),DUP,NOCASE,OPT       ! By Branch           
FKey_VCID                KEY(TRI:VCID),DUP,NOCASE,OPT      ! By Vehicle Compositions
FKey_DRID                KEY(TRI:DRID),DUP,NOCASE,OPT      ! By Driver           
FKey_Assistant_DRID      KEY(TRI:Assistant_DRID),DUP,NOCASE,OPT ! By Assistant Driver 
FKey_SuburbID            KEY(TRI:SUID),DUP,NOCASE,OPT      ! By Suburb           
FKey_TID                 KEY(TRI:TID),DUP,NOCASE,OPT       ! By Transporter      
Record                   RECORD,PRE()
TRID                        ULONG                          ! Tripsheet ID        
BID                         ULONG                          ! Branch ID           
VCID                        ULONG                          ! Vehicle Composition ID
DepartDateAndTime           STRING(8)                      !                     
DepartDateAndTime_GROUP     GROUP,OVER(DepartDateAndTime)  !                     
DepartDate                    DATE                         !                     
DepartTime                    TIME                         !                     
                            END                            !                     
ReturnedDateAndTime         STRING(8)                      !                     
ReturnedDateAndTime_GROUP   GROUP,OVER(ReturnedDateAndTime) !                     
ReturnedDate                  DATE                         !                     
ReturnedTime                  TIME                         !                     
                            END                            !                     
Notes                       CSTRING(501)                   ! Notes               
State                       BYTE                           ! State of this Trip Sheet
DRID                        ULONG                          ! Drivers ID          
Assistant_DRID              ULONG                          ! Drivers ID          
FID                         ULONG                          ! Floor ID - delivered to for all items on this tripsheet
Collections                 BYTE                           ! Collections         
SUID                        ULONG                          ! Suburb ID           
TID                         ULONG                          ! Transporter ID      
                         END
                     END                       

Manifest             FILE,DRIVER('MSSQL'),PRE(MAN),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Manifest            
PKey_MID                 KEY(MAN:MID),NOCASE,OPT,PRIMARY   ! By Manifest ID      
FKey_BID                 KEY(MAN:BID),DUP,NOCASE,OPT       ! By Branch           
FKey_TID                 KEY(MAN:TID),DUP,NOCASE,OPT       ! By Transporter      
FKey_VCID                KEY(MAN:VCID),DUP,NOCASE,OPT      ! By Vehicle Composition
FKey_JID                 KEY(MAN:JID),DUP,NOCASE,OPT       ! By Journey          
FKey_CAID                KEY(MAN:CollectionAID),DUP,NOCASE,OPT ! By Collection Address
FKey_DAID                KEY(MAN:DeliveryAID),DUP,NOCASE,OPT ! By Delivery Address 
FKey_DRID                KEY(MAN:DRID),DUP,NOCASE,OPT      ! By Driver           
SKey_CreatedDate         KEY(MAN:CreatedDate),DUP,NOCASE,OPT ! By Created Date     
Record                   RECORD,PRE()
MID                         ULONG                          ! Manifest ID         
BID                         ULONG                          ! Branch ID           
TID                         ULONG                          ! Transporter ID      
VCID                        ULONG                          ! Vehicle Composition ID
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
CollectionAID               ULONG                          ! Collection Address ID
DeliveryAID                 ULONG                          ! Delivery Address ID 
Cost                        DECIMAL(10,2)                  !                     
Rate                        DECIMAL(10,4)                  ! Rate per Kg - this can be calculated from the total mass and the cost
VATRate                     DECIMAL(5,2)                   ! VAT rate            
State                       BYTE                           ! State of this manifest
CreatedDateAndTime          STRING(8)                      !                     
CreatedDateAndTime_GROUP    GROUP,OVER(CreatedDateAndTime) !                     
CreatedDate                   DATE                         ! Created Date        
CreatedTime                   TIME                         !                     
                            END                            !                     
DepartDateAndTime           STRING(8)                      !                     
DepartDateAndTime_GROUP     GROUP,OVER(DepartDateAndTime)  !                     
DepartDate                    DATE                         ! Departed Date       
DepartTime                    TIME                         ! Departed Time       
                            END                            !                     
ETADateAndTime              STRING(8)                      !                     
ETADateAndTime_GROUP        GROUP,OVER(ETADateAndTime)     !                     
ETADate                       DATE                         ! Estimated arrival date
ETATime                       TIME                         ! Estimated arrival time
                            END                            !                     
DRID                        ULONG                          ! Drivers ID          
Broking                     BYTE                           ! Broking Manifest    
TTID_FreightLiner           ULONG                          ! Track or Trailer ID 
TTID_Trailer                ULONG                          ! Track or Trailer ID 
TTID_SuperLink              ULONG                          ! Track or Trailer ID 
Tracking_BID                ULONG                          ! This is the Branch that sees the Manifest in Arrivals after it goes on-route
                         END
                     END                       

ApplicationSections  FILE,DRIVER('MSSQL'),PRE(APP),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Application Sections
PKey_ASID                KEY(APP:ASID),NOCASE,OPT,PRIMARY  ! By PS ID            
Key_ApplicationSection   KEY(APP:ApplicationSection),NOCASE,OPT ! By Application Section
Record                   RECORD,PRE()
ASID                        ULONG                          !                     
ApplicationSection          STRING(35)                     ! Application Section Name
DefaultAction               BYTE                           ! Default action for security is - Allow, View Only, No Access
                         END
                     END                       

UsersGroups          FILE,DRIVER('MSSQL'),PRE(USEBG),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Groups the users belong to
PKey_UBGID               KEY(USEBG:UBGID),NOCASE,OPT,PRIMARY ! By User Belongs Group ID
FKey_UGID                KEY(USEBG:UGID),DUP,NOCASE,OPT    ! By User Group       
Key_UID                  KEY(USEBG:UID),DUP,NOCASE,OPT     ! By User             
Record                   RECORD,PRE()
UBGID                       ULONG                          ! User Belongs Groups ID
UID                         ULONG                          ! User ID             
UGID                        ULONG                          ! ID                  
                         END
                     END                       

UsersAccesses        FILE,DRIVER('MSSQL'),PRE(USAC),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! User Accesses       
PKey_UAID                KEY(USAC:UAID),NOCASE,OPT,PRIMARY ! By Users Accesses ID
FKey_UID                 KEY(USAC:UID),DUP,NOCASE,OPT      ! By User             
FKey_ASID                KEY(USAC:ASID),DUP,NOCASE,OPT     ! By Application Section
SKey_UID_ASID            KEY(USAC:UID,USAC:ASID),NOCASE,OPT ! By User && Application Section
Record                   RECORD,PRE()
UAID                        ULONG                          !                     
UID                         ULONG                          ! User ID             
ASID                        ULONG                          !                     
AccessOption                BYTE                           ! Option - Allow, View Only, No Access
                         END
                     END                       

UserGroupAccesses    FILE,DRIVER('MSSQL'),PRE(UGAC),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! UserGroup Accesses  
PKey_UGAID               KEY(UGAC:UGAID),NOCASE,OPT,PRIMARY ! By User Group Accesses ID
FKey_UGID                KEY(UGAC:UGID),DUP,NOCASE,OPT     ! By User Group       
FKey_ASID                KEY(UGAC:ASID),DUP,NOCASE,OPT     ! By Application Section
SKey_UGID_ASID           KEY(UGAC:UGID,UGAC:ASID),NOCASE,OPT ! By User Group && Application Section
Record                   RECORD,PRE()
UGAID                       ULONG                          !                     
UGID                        ULONG                          ! User Group ID       
ASID                        ULONG                          !                     
AccessOption                BYTE                           ! Option - Allow, View Only, No Access
                         END
                     END                       

UsersAccessesExtra   FILE,DRIVER('MSSQL'),PRE(USACE),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_UAEID               KEY(USACE:UAEID),NOCASE,OPT,PRIMARY ! By User Accesses Extra
FKey_UAID                KEY(USACE:UAID),DUP,NOCASE,OPT    ! By User Accesses    
FKey_UID                 KEY(USACE:UID),DUP,NOCASE,OPT     ! By User             
FKey_ASID                KEY(USACE:ASID),DUP,NOCASE,OPT    ! By Application Section
FKey_ASEID               KEY(USACE:ASEID),DUP,NOCASE,OPT   ! By App. Section Extra
Record                   RECORD,PRE()
UAEID                       ULONG                          !                     
UAID                        ULONG                          !                     
UID                         ULONG                          ! User ID             
ASID                        ULONG                          !                     
ASEID                       ULONG                          !                     
AccessOption                BYTE                           ! Option - Allow, View Only, No Access
                         END
                     END                       

UserGroupAccessesExtra FILE,DRIVER('MSSQL'),PRE(UGACE),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_UGAEID              KEY(UGACE:UGAEID),NOCASE,OPT,PRIMARY ! By User Group Accesses Extra
FKey_UGAID               KEY(UGACE:UGAID),DUP,NOCASE,OPT   ! By User Group Accesses
FKey_UGID                KEY(UGACE:UGID),DUP,NOCASE,OPT    ! By User Group       
FKey_ASID                KEY(UGACE:ASID),DUP,NOCASE,OPT    ! By Application Section
FKey_ASEID               KEY(UGACE:ASEID),DUP,NOCASE,OPT   ! By App. Section Extra
Record                   RECORD,PRE()
UGAEID                      ULONG                          !                     
UGAID                       ULONG                          !                     
UGID                        ULONG                          ! User Group ID       
ASID                        ULONG                          !                     
ASEID                       ULONG                          !                     
AccessOption                BYTE                           ! Option - Allow, View Only, No Access
                         END
                     END                       

__RatesContainer     FILE,DRIVER('MSSQL'),PRE(CORA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Container Rates ---------- old
PKey_CRID                KEY(CORA:CRID),NOCASE,OPT,PRIMARY ! By Container Rates ID
FKey_CID                 KEY(CORA:CID),DUP,NOCASE,OPT      ! By Client           
FKey_JID                 KEY(CORA:JID),DUP,NOCASE,OPT      ! By Journey          
FKey_CTID                KEY(CORA:CTID),DUP,NOCASE,OPT     ! By Container Type   
CKey_CID_JID_CTID_EffDate_ToMass KEY(CORA:CID,CORA:JID,CORA:CTID,-CORA:Effective_Date,CORA:ToMass),NOCASE,OPT ! By Client, Journey, Container Type, Effective Date && To Mass
Record                   RECORD,PRE()
CRID                        ULONG                          ! Container Rate ID   
CID                         ULONG                          ! Client ID           
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
CTID                        ULONG                          ! Container Type ID   
ToMass                      DECIMAL(9)                     ! Up to this mass in Kgs
RatePerKg                   DECIMAL(8,2)                   ! Rate per Kg         
MinimiumCharge              DECIMAL(10,2)                  !                     
Effective_DateAndTime       STRING(8)                      !                     
Effective_DateAndTime_GROUP GROUP,OVER(Effective_DateAndTime) !                     
Effective_Date                DATE                         ! Effective from date 
Effective_TIME                TIME                         ! Not used!           
                            END                            !                     
                         END
                     END                       

SalesRepsBands       FILE,DRIVER('MSSQL'),PRE(SRBA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Sales Rep. Earning Bands
PKey_SRBID               KEY(SRBA:SRBID),NOCASE,OPT,PRIMARY ! By Sales Rep. Band ID
FKey_SRID                KEY(SRBA:SRID),DUP,NOCASE,OPT     ! By Sales Rep.       
Record                   RECORD,PRE()
SRBID                       ULONG                          ! Sales Rep Band ID   
SRID                        ULONG                          ! Sales Rep ID        
ToTons                      ULONG                          ! To Tons             
Commision                   DECIMAL(5,2)                   ! Commision percentage
                         END
                     END                       

Add_Countries        FILE,DRIVER('MSSQL'),PRE(COUN),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Countries           
PKey_COID                KEY(COUN:COID),NOCASE,OPT,PRIMARY ! By Country ID       
Key_Country              KEY(COUN:Country),NOCASE          ! By Country          
Record                   RECORD,PRE()
COID                        ULONG                          ! Country ID          
Country                     STRING(50)                     ! Country             
                         END
                     END                       

Deliveries           FILE,DRIVER('MSSQL'),PRE(DEL),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Deliveries          
PKey_DID                 KEY(DEL:DID),NOCASE,OPT,PRIMARY   ! By Delivery ID      
Key_DINo                 KEY(DEL:DINo),NOCASE,OPT          ! By Delivery Instruction No.
SKey_ClientReference     KEY(DEL:ClientReference),DUP,NOCASE ! By Client Reference 
FKey_BID                 KEY(DEL:BID),DUP,NOCASE,OPT       ! By Branch ID        
FKey_CID                 KEY(DEL:CID),DUP,NOCASE,OPT       ! By Client ID        
FKey_JID                 KEY(DEL:JID),DUP,NOCASE,OPT       ! By Journey          
FKey_CollectionAID       KEY(DEL:CollectionAID),DUP,NOCASE,OPT ! By Collection Address ID
FKey_DeliveryAID         KEY(DEL:DeliveryAID),DUP,NOCASE,OPT ! By Delivery Address ID
FKey_LTID                KEY(DEL:LTID),DUP,NOCASE,OPT      ! By Load Type        
FKey_SID                 KEY(DEL:SID),DUP,NOCASE,OPT       ! By Service ID       
FKey_TripSheetInTID      KEY(DEL:TripSheetInTID),DUP,NOCASE,OPT ! By Tripsheet In TID 
FKey_TripSheetOutTID     KEY(DEL:TripSheetOutTID),DUP,NOCASE,OPT ! By Tripsheet Out TID
FKey_FID                 KEY(DEL:FID),DUP,NOCASE,OPT       ! By Floor            
FKey_DCID                KEY(DEL:DC_ID),DUP,NOCASE,OPT     ! By Delivery COD Address
FKey_DELCID              KEY(DEL:DELCID),DUP,NOCASE,OPT    ! By Delivery Composition
FKey_CollectedDRID       KEY(DEL:CollectedByDRID),DUP,NOCASE,OPT ! By Collected by Driver
FKey_CRTID               KEY(DEL:CRTID),DUP,NOCASE,OPT     ! By Client Rate Type 
FKey_UID                 KEY(DEL:UID),DUP,NOCASE,OPT       ! By User             
Record                   RECORD,PRE()
DID                         ULONG                          ! Delivery ID         
DINo                        ULONG                          ! Delivery Instruction Number
DIDateAndTime               STRING(8)                      !                     
DIDateAndTime_GROUP         GROUP,OVER(DIDateAndTime)      !                     
DIDate                        DATE                         ! DI Date             
DITime                        TIME                         ! Not used!           
                            END                            !                     
BID                         ULONG                          ! Branch ID           
CID                         ULONG                          ! Client ID           
ClientReference             CSTRING(61)                    ! Client Reference    
JID                         ULONG                          ! Journey ID          
CollectionAID               ULONG                          ! Collection Address ID
DeliveryAID                 ULONG                          ! Delivery Address ID 
CRTID                       ULONG                          ! Client Rate Type ID 
LTID                        ULONG                          ! Load Type ID        
SID                         ULONG                          ! Service Requirement ID
TripSheetInTID              ULONG                          ! Tripsheet ID        
TripSheetOutTID             ULONG                          ! Tripsheet ID        
MultipleLoadDID             ULONG                          ! Delivery ID this DI is linked to - multipart DI, more that 1 load required to deliver
Charges_Group               GROUP                          !                     
Rate                          DECIMAL(10,4)                ! Rate for DI (debtors journey rate possibly adjusted) - Charge will be Rate multiplied by the total weight in kgs.
DocumentCharge                DECIMAL(8,2)                 ! Document Charge applied to this DI
FuelSurcharge                 DECIMAL(8,2)                 ! Fuel Surcharge applied to this DI
Charge                        DECIMAL(11,2)                ! Charge for the DI- Excludes VAT, Docs, Fuel, Insurance and Additional Charges charges
AdditionalCharge_Calculate    BYTE                         ! Calculate the Additional Charge
AdditionalCharge              DECIMAL(11,2)                ! Additional charges  
VATRate                       DECIMAL(5,2)                 ! VAT Rate            
TollRate                      DECIMAL(10,4)                ! Rate Toll charged at as a percent of the Freight charge
TollCharge                    DECIMAL(11,2)                !                     
                            END                            !                     
Insure                      BYTE                           ! Insure the goods on this DI.  Note this is in addition to any insurance that may be part of this clients rate.
TotalConsignmentValue       DECIMAL(11,2)                  ! Total value of the cargo on this DI - required if additional insurance required
InsuranceRate               DECIMAL(9,6)                   ! Insurance rate per ton
FID                         ULONG                          ! Floor ID - if not the current branches floor then allow progress entries
FIDRate                     ULONG                          ! Floor ID used for Container Park Rates - doesn't change
SpecialDeliveryInstructions CSTRING(256)                   ! This will print on the DI and Delivery Note (& POD)
Notes                       CSTRING(256)                   ! General Notes       
ReceivedDateAndTime         STRING(8)                      !                     
ReceivedDateAndTime_GROUP   GROUP,OVER(ReceivedDateAndTime) !                     
ReceivedDate                  DATE                         ! Received Date       
ReceivedTime                  TIME                         ! Not used!           
                            END                            !                     
MultipleManifestsAllowed    BYTE                           ! Allow this DI to be manifested on multiple manifests
Manifested                  BYTE                           ! Manifested status - this has maintenance function - Not Manifested, Partially Manifested, Partially Manifested Multiple, Fully Manifested, Fully Manifested Multiple
Delivered                   BYTE                           ! Delivered Status - this has maintenance function - Not Delivered, Partially Delivered, Delivered, Delivered (manual)
DC_ID                       ULONG                          ! Delivery COD Addresses ID
Terms                       BYTE                           ! Terms - Pre Paid, COD, Account, On Statement
DELCID                      ULONG                          ! Delivery Composition ID
CreatedDateTime             STRING(8)                      !                     
CreatedDateTime_Group       GROUP,OVER(CreatedDateTime)    !                     
CreatedDate                   DATE                         ! Entry created on    
CreatedTime                   TIME                         ! Entry created at    
                            END                            !                     
CollectedByDRID             ULONG                          ! Drivers ID          
UID                         ULONG                          ! User ID - last worked on this DI
ReleasedUID                 ULONG                          ! If requiring release, this is the User that has released it
VATRate_OverriddenUserID    ULONG                          ! The VAT rate has been overridden by this user
NoticeEmailAddresses        STRING(1024)                   ! Email addresses to send notices to of progress of DI
Tolls                       BYTE                           ! Are tolls to be charged on this DI?
                         END
                     END                       

WebClientsLoginLog   FILE,DRIVER('MSSQL'),PRE(WLOG),BINDABLE,CREATE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PK_WebClientsLoginLog    KEY(WLOG:LoginLogID),PRIMARY      !                     
FKey_WebClientID         KEY(WLOG:WebClientID),DUP,NAME('_WA_Sys_WebClientID_214BF109') ! By Web Client ID    
Record                   RECORD,PRE()
LoginLogID                  LONG                           !                     
WebClientID                 LONG                           !                     
ClientLogin                 STRING(20)                     !                     
FailedPassword              STRING(20)                     !                     
LoginIP                     STRING(15)                     !                     
LoginDateTime               STRING(8)                      !                     
LoginDateTime_GROUP         GROUP,OVER(LoginDateTime)      !                     
LoginDateTime_DATE            DATE                         !                     
LoginDateTime_TIME            TIME                         !                     
                            END                            !                     
OffLine                     BYTE                           !                     
                         END
                     END                       

TruckTrailer         FILE,DRIVER('MSSQL'),PRE(TRU),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Trucks & Trailers   
PKey_TTID                KEY(TRU:TTID),NOCASE,OPT,PRIMARY  ! By TTID             
FKey_TID                 KEY(TRU:TID),DUP,NOCASE,OPT       ! By Tansporter       
FKey_VMMID               KEY(TRU:VMMID),DUP,NOCASE,OPT     ! By Vehicle Make && Model
Key_Registration         KEY(TRU:Registration),NOCASE,OPT  ! By Registration     
FKey_DRID                KEY(TRU:DRID),DUP,NOCASE,OPT      ! By Driver           
Record                   RECORD,PRE()
TTID                        ULONG                          ! Track or Trailer ID 
TID                         ULONG                          ! Transporter ID      
VMMID                       ULONG                          ! Vehicle Make & Model ID
Type                        BYTE                           ! Type of vehicle - Horse, Trailer, Rigid
DRID                        ULONG                          ! Driver - Note these only apply to Horse & Combined types
Registration                STRING(20)                     !                     
Capacity                    DECIMAL(6)                     ! In Kgs              
LicenseInfoGroup            GROUP                          !                     
LicenseExpiryDateTime         STRING(8)                    !                     
LicenseExpiryGroup            GROUP,OVER(LicenseExpiryDateTime) !                     
LicenseExpiryDate               DATE                       ! License expires on this date
LicenseExpiryTime               TIME                       !                     
                              END                          !                     
LicenseInfo                   STRING(250)                  !                     
                            END                            !                     
                         END
                     END                       

__RateUpdates        FILE,DRIVER('MSSQL'),PRE(RATU),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Batch Rate Updates  
PKey_RUBID               KEY(RATU:RUBID),NOCASE,OPT,PRIMARY ! By RUBID            
Record                   RECORD,PRE()
RUBID                       ULONG                          ! Rate Update Batch ID
UID                         ULONG                          ! User ID             
Effective_DateTime          STRING(8)                      !                     
Effective_DateTimeGroup     GROUP,OVER(Effective_DateTime) !                     
Effective_Date                DATE                         !                     
Effective_Time                TIME                         !                     
                            END                            !                     
Run_DateTime                STRING(8)                      !                     
Run_DateTimeGroup           GROUP,OVER(Run_DateTime)       !                     
RunDate                       DATE                         !                     
RunTime                       TIME                         !                     
                            END                            !                     
Rates_Option                BYTE                           !                     
IncreaseDecrease            BYTE                           ! Increase or Decrease
Set_Percent                 BYTE                           !                     
Percentage                  DECIMAL(6,2)                   !                     
MinimiumCharge              BYTE                           ! Percentage, No Change
AdHoc                       BYTE                           ! Ad Hoc option - Don't Adjust, Adjust (leave Ad Hoc), Adjust (change to non Ad Hoc)
Round_To_Cents              BYTE                           !                     
Round_To_Rands              BYTE                           !                     
Results                     GROUP                          !                     
ClientsUpdated                ULONG                        !                     
RatesAdded                    ULONG                        !                     
CompletedStatus               BYTE                         ! Completed Successfully
                            END                            !                     
                         END
                     END                       

TripSheetDeliveries  FILE,DRIVER('MSSQL'),PRE(TRDI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Trip Sheet Delivery Items
PKey_TDID                KEY(TRDI:TDID),NOCASE,OPT,PRIMARY ! By Trip Sheet Delivery ID
FKey_TRID                KEY(TRDI:TRID),DUP,NOCASE,OPT     ! By Tripsheet ID     
FKey_DIID                KEY(TRDI:DIID),DUP,NOCASE,OPT     ! By Delivery Item ID 
CKey_TRID_DIID           KEY(TRDI:TRID,TRDI:DIID),DUP,NOCASE,OPT ! By Trip Sheet && Delivery Item
Record                   RECORD,PRE()
TDID                        ULONG                          ! Trip Sheet Delivery Items ID
TRID                        ULONG                          ! Tripsheet ID        
DIID                        ULONG                          ! Delivery Item ID    
UnitsLoaded                 USHORT                         ! Number of units     
Delivery_Group              GROUP                          !                     
DeliveredDateAndTime          STRING(8)                    !                     
DeliveredDateAndTime_GROUP    GROUP,OVER(DeliveredDateAndTime) !                     
DeliveredDate                   DATE                       ! Date delivered      
DeliveredTime                   TIME                       ! Time delivered      
                              END                          !                     
UnitsDelivered                USHORT                       !                     
UnitsNotAccepted              USHORT                       ! Units that were not accepted for delivery
ClearSignature                BYTE                         ! Does the POD have a clear signature
Notes                         CSTRING(256)                 ! Notes               
                            END                            !                     
                         END
                     END                       

_InvoiceTransporter  FILE,DRIVER('MSSQL'),PRE(INT),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Transporter Invoices
PKey_TIN                 KEY(INT:TIN),NOCASE,OPT,PRIMARY   ! By Transporter Invoice No.
FKey_TID                 KEY(INT:TID),DUP,NOCASE,OPT       ! By Transporter      
FKey_BID                 KEY(INT:BID),DUP,NOCASE,OPT       ! By Branch           
Fkey_MID                 KEY(INT:MID),DUP,NOCASE,OPT       ! By Manifest         
FKey_UID                 KEY(INT:UID),DUP,NOCASE,OPT       ! By User             
FKey_DID                 KEY(INT:DID),DUP,NOCASE,OPT       ! By Delivery         
FKey_DLID                KEY(INT:DLID),DUP,NOCASE,OPT      ! By Delivery Leg     
Key_CR_TIN               KEY(INT:CR_TIN),DUP,NOCASE,OPT    ! By Credited Invoice 
SKey_MID_DID             KEY(INT:MID,INT:DID),DUP,NOCASE,OPT ! By Manifest && Delivery
SKey_MID_Manifest        KEY(INT:MID,INT:Manifest),DUP,NOCASE,OPT ! By MID && Manifest - manifest invoice for this MID
Record                   RECORD,PRE()
TIN                         ULONG                          ! Transporter Invoice No.
CR_TIN                      ULONG                          ! Transporter Invoice No.
TID                         ULONG                          ! Transporter ID      
BID                         ULONG                          ! Branch ID           
MID                         ULONG                          ! Manifest ID         
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
DeliveryGroup               GROUP                          ! used when a Delivery has an extra leg, generating MID is recorded in this case
DID                           ULONG                        ! Delivery ID         
DINo                          ULONG                        ! Delivery Instruction Number
DLID                          ULONG                        ! Delivery Leg ID     
Leg                           SHORT                        ! Leg Number          
CollectionAID                 ULONG                        ! Collection Address ID
DeliveryAID                   ULONG                        ! Delivery Address ID 
                            END                            !                     
ChargesGroup                GROUP                          !                     
Cost                          DECIMAL(10,2)                !                     
VAT                           DECIMAL(10,2)                ! VAT amount          
Rate                          DECIMAL(10,4)                ! Rate per Kg - this can be calculated from the total mass and the cost
VATRate                       DECIMAL(5,2)                 ! VAT rate            
                            END                            !                     
InvoiceDateTime             STRING(8)                      !                     
InvoiceDateTimeGroup        GROUP,OVER(InvoiceDateTime)    !                     
InvoiceDate                   DATE                         !                     
InvoiceTime                   TIME                         !                     
                            END                            !                     
CreatedDateTime             STRING(8)                      ! Created on this date at this time
CreatedDateTimeGroup        GROUP,OVER(CreatedDateTime)    !                     
CreatedDate                   DATE                         !                     
CreatedTime                   TIME                         !                     
                            END                            !                     
UID                         ULONG                          ! User ID             
Broking                     BYTE                           ! Broking Invoice (from Broking Manifest or Delivery Leg)
Manifest                    BYTE                           ! This is the Manifest invoice
Status                      BYTE                           ! used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid
StatusUpToDate              BYTE                           ! The status is up to date
Comment                     CSTRING(256)                   !                     
VAT_Specified               BYTE                           ! The user has chosen to specify the VAT amount
ExtraInv                    BYTE                           ! Is this Invoice an Extra Invoice
Creditors_Group             GROUP                          ! Information for creditors relating to this invoice
CIN                           CSTRING(31)                  ! Creditors Invoice No. - 3rd party reference
CIN_DateTimeReceived          STRING(8)                    !                     
CIN_DateTimeReceived_Group    GROUP,OVER(CIN_DateTimeReceived) !                     
CIN_DateReceived                DATE                       !                     
CIN_TimeReceived                TIME                       ! no use              
                              END                          !                     
                            END                            !                     
                         END
                     END                       

_InvoiceJournals     FILE,DRIVER('MSSQL'),PRE(IJOU),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_IJID                KEY(IJOU:IJID),NOCASE,OPT,PRIMARY ! By Journal          
SKey_DateTime            KEY(IJOU:Journal_Date),DUP,NOCASE,OPT ! By Date && Time     
Record                   RECORD,PRE()
IJID                        ULONG                          ! Invoice Journal ID  
Journal_DateTime            STRING(8)                      !                     
Journal_DateTime_Group      GROUP,OVER(Journal_DateTime)   !                     
Journal_Date                  DATE                         !                     
Journal_Time                  TIME                         !                     
                            END                            !                     
UID                         ULONG                          ! User ID             
Notes                       CSTRING(255)                   !                     
                         END
                     END                       

PackagingTypes       FILE,DRIVER('MSSQL'),PRE(PACK),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Packaging Types     
PKey_PTID                KEY(PACK:PTID),NOCASE,OPT,PRIMARY ! By Packaging Type ID
Key_Packaging            KEY(PACK:Packaging),NOCASE        ! By Packaging        
Record                   RECORD,PRE()
PTID                        ULONG                          ! Packaging Type ID   
Packaging                   STRING(35)                     !                     
Archived                    BYTE                           ! Mark Packaging Type as Archived
Archived_DateTime           STRING(8)                      !                     
Archived_DateTime_Group     GROUP,OVER(Archived_DateTime)  !                     
Archived_Date                 DATE                         !                     
Archived_Time                 TIME                         !                     
                            END                            !                     
                         END
                     END                       

DeliveryProgress     FILE,DRIVER('MSSQL'),PRE(DELP),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Delivery Progress   
PKey_DPID                KEY(DELP:DPID),NOCASE,OPT,PRIMARY ! By Delivery Progress ID
FKey_DID                 KEY(DELP:DID),DUP,NOCASE,OPT      ! By Delivery ID      
FKey_CalledAID           KEY(DELP:CalledAID),DUP,NOCASE,OPT ! By Called Address   
FKey_ACID                KEY(DELP:ACID),DUP,NOCASE,OPT     ! By Address Contact  
FKey_DSID                KEY(DELP:DSID),DUP,NOCASE,OPT     ! By Delivery Status  
Record                   RECORD,PRE()
DPID                        ULONG                          ! Delivery Progress ID
DID                         ULONG                          ! Delivery ID         
CalledAID                   ULONG                          ! Address ID - note once used certain information should not be changeable, such as the suburb
ACID                        ULONG                          ! Address Contact ID  
DSID                        ULONG                          ! Delivery Status ID  
StatusDateAndTime           STRING(8)                      !                     
StatusDateAndTime_GROUP     GROUP,OVER(StatusDateAndTime)  !                     
StatusDate                    DATE                         ! Date of status entry
StatusTime                    TIME                         ! Time of status entry
                            END                            !                     
ActionDateAndTime           STRING(8)                      !                     
ActionDateAndTime_GROUP     GROUP,OVER(ActionDateAndTime)  !                     
ActionDate                    DATE                         !                     
ActionTime                    TIME                         !                     
                            END                            !                     
                         END
                     END                       

VehicleComposition   FILE,DRIVER('MSSQL'),PRE(VCO),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Vehicle Composition 
PKey_VCID                KEY(VCO:VCID),NOCASE,OPT,PRIMARY  ! By Vehicle Composition ID
FKey_TID                 KEY(VCO:TID),DUP,NOCASE,OPT       ! By Transporter      
Key_Name                 KEY(VCO:CompositionName),NOCASE   ! By Name             
FKey_TID0                KEY(VCO:TTID0),NOCASE             ! By Truck            
Record                   RECORD,PRE()
VCID                        ULONG                          ! Vehicle Composition ID
TID                         ULONG                          ! Transporter ID      
CompositionName             STRING(35)                     !                     
TTID0                       ULONG                          ! Track or Trailer ID 
TTID1                       ULONG                          ! Track or Trailer ID 
TTID2                       ULONG                          ! Track or Trailer ID 
TTID3                       ULONG                          ! Track or Trailer ID 
Capacity                    DECIMAL(7)                     ! Total capacity not to exceed this amount
ShowForAllTransporters      BYTE                           ! Show this Vehicle Composition for all Transporters
Archived                    BYTE                           ! Is this vehicle composition archived?
Archived_DateTime           STRING(8)                      !                     
Archived_DateTime_Group     GROUP,OVER(Archived_DateTime)  !                     
Archived_Date                 DATE                         !                     
Archived_Time                 TIME                         !                     
                            END                            !                     
                         END
                     END                       

SalesReps            FILE,DRIVER('MSSQL'),PRE(SAL),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Sales Reps          
PKey_SRID                KEY(SAL:SRID),NOCASE,OPT,PRIMARY  ! By Sales Rep. ID    
Key_SalesRep             KEY(SAL:SalesRep),NOCASE          ! By Sales Rep.       
Record                   RECORD,PRE()
SRID                        ULONG                          ! Sales Rep ID        
SalesRep                    STRING(35)                     ! Sales Rep. Name     
                         END
                     END                       

_RemittanceItems     FILE,DRIVER('MSSQL'),PRE(REMIT),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_REMIID              KEY(REMIT:REMIID),NOCASE,OPT,PRIMARY ! By Remittance Item ID
FKey_REMID               KEY(REMIT:REMID),DUP,NOCASE,OPT   ! By Remittance       
FKey_TIN                 KEY(REMIT:TIN),DUP,NOCASE,OPT     ! By Invoice          
Record                   RECORD,PRE()
REMIID                      ULONG                          ! Remittance Item ID  
REMID                       ULONG                          ! Remittance ID       
InvoiceDateAndTime          STRING(8)                      !                     
InvoiceDateAndTime_GROUP    GROUP,OVER(InvoiceDateAndTime) !                     
InvoiceDate                   DATE                         !                     
InvoiceTime                   TIME                         !                     
                            END                            !                     
TIN                         ULONG                          ! Transporter Invoice No.
MID                         ULONG                          ! Manifest ID         
AmountPaid                  DECIMAL(10,2)                  !                     
                         END
                     END                       

_Remittance          FILE,DRIVER('MSSQL'),PRE(REMI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_REMID               KEY(REMI:REMID),NOCASE,OPT,PRIMARY ! By Remittance ID    
FKey_TID                 KEY(REMI:TID),DUP,NOCASE,OPT      ! By Transporter      
FKey_BID                 KEY(REMI:BID),DUP,NOCASE,OPT      ! By Branch           
FKey_RERID               KEY(REMI:RERID),DUP,NOCASE,OPT    ! By Remittance Run ID
Record                   RECORD,PRE()
REMID                       ULONG                          ! Remittance ID       
RERID                       ULONG                          ! Remittance Run ID   
RemittanceDateAndTime       STRING(8)                      !                     
RemittanceDateAndTime_GROUP GROUP,OVER(RemittanceDateAndTime) !                     
RemittanceDate                DATE                         ! Statement Date      
RemittanceTime                TIME                         !                     
                            END                            !                     
TID                         ULONG                          ! Transporter ID      
BID                         ULONG                          ! Branch ID           
Days90                      DECIMAL(10,2)                  ! 90 Days             
Days60                      DECIMAL(10,2)                  ! 60 Days             
Days30                      DECIMAL(10,2)                  ! 30 Days             
Current                     DECIMAL(10,2)                  ! Current             
Total                       DECIMAL(10,2)                  ! Total               
                         END
                     END                       

_Remittance_Runs     FILE,DRIVER('MSSQL'),PRE(REMR),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_RERID               KEY(REMR:RERID),NOCASE,OPT,PRIMARY ! By Remittance Run ID
Key_DateTime             KEY(-REMR:RunDateTime),DUP,NOCASE ! By Run Date && Time 
FKey_UID                 KEY(REMR:UID),DUP,NOCASE,OPT      ! By User             
FKey_RunDesc             KEY(REMR:RunDescription),DUP,NOCASE,OPT ! By Run Description  
Record                   RECORD,PRE()
RERID                       ULONG                          ! Remittance Run ID   
RunDescription              STRING(35)                     ! Run Description     
RunDateTime                 STRING(8)                      !                     
RunDateTime_Group           GROUP,OVER(RunDateTime)        !                     
RunDate                       DATE                         ! Statement Run Date  
RunTime                       TIME                         !                     
                            END                            !                     
EntryDateTime               STRING(8)                      !                     
EntryDateTime_Group         GROUP,OVER(EntryDateTime)      !                     
EntryDate                     DATE                         ! Run done at         
EntryTime                     TIME                         !                     
                            END                            !                     
Complete                    BYTE                           ! Complete            
Type                        BYTE                           ! Transporter Monthly, Transporter Adhoc, Internal Adhoc (not on web)
UID                         ULONG                          ! User ID             
PaymentsFromDateTime        STRING(8)                      !                     
PaymentsFromDateTime_Group  GROUP,OVER(PaymentsFromDateTime) !                     
PaymentsFromDate              DATE                         !                     
PaymentsFromTime              TIME                         !                     
                            END                            !                     
PaymentsToDateTime          STRING(8)                      !                     
PaymentsToDateTime_Group    GROUP,OVER(PaymentsToDateTime) !                     
PaymentsToDate                DATE                         !                     
PaymentsToTime                TIME                         !                     
                            END                            !                     
                         END
                     END                       

_InvoiceComposition  FILE,DRIVER('MSSQL'),PRE(INCO),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Invoice Composition 
PKey_ICID                KEY(INCO:ICID),NOCASE,OPT,PRIMARY ! By ICID             
Record                   RECORD,PRE()
ICID                        ULONG                          ! Invoice Composition ID
                         END
                     END                       

_StatementItems      FILE,DRIVER('MSSQL'),PRE(STAI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_STIID               KEY(STAI:STIID),NOCASE,OPT,PRIMARY ! By Statement Item ID
FKey_STID                KEY(STAI:STID),DUP,NOCASE,OPT     ! By Statement        
FKey_IID                 KEY(STAI:IID),DUP,NOCASE,OPT      ! By Invoice          
Record                   RECORD,PRE()
STIID                       ULONG                          ! Statement Item ID   
STID                        ULONG                          ! Statement ID        
InvoiceDateAndTime          STRING(8)                      !                     
InvoiceDateAndTime_GROUP    GROUP,OVER(InvoiceDateAndTime) !                     
InvoiceDate                   DATE                         !                     
InvoiceTime                   TIME                         !                     
                            END                            !                     
IID                         ULONG                          ! Invoice Number      
DID                         ULONG                          ! Delivery ID         
DINo                        ULONG                          ! Delivery Instruction Number
Debit                       DECIMAL(10,2)                  !                     
Credit                      DECIMAL(10,2)                  !                     
Amount                      DECIMAL(10,2)                  !                     
                         END
                     END                       

_Statement_Runs      FILE,DRIVER('MSSQL'),PRE(STAR),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_STRID               KEY(STAR:STRID),NOCASE,OPT,PRIMARY ! By Statement Run ID 
Key_DateTime             KEY(-STAR:RunDateTime),DUP,NOCASE ! By Run Date && Time 
FKey_UID                 KEY(STAR:UID),DUP,NOCASE,OPT      ! By User             
FKey_RunDesc             KEY(STAR:RunDescription),DUP,NOCASE,OPT ! By Run Description  
Record                   RECORD,PRE()
STRID                       ULONG                          ! Statement Run ID    
RunDescription              STRING(35)                     ! Run Description     
RunDateTime                 STRING(8)                      !                     
RunDateTime_Group           GROUP,OVER(RunDateTime)        !                     
RunDate                       DATE                         ! Statement Run Date  
RunTime                       TIME                         !                     
                            END                            !                     
EntryDateTime               STRING(8)                      !                     
EntryDateTime_Group         GROUP,OVER(EntryDateTime)      !                     
EntryDate                     DATE                         ! Run done at         
EntryTime                     TIME                         !                     
                            END                            !                     
Complete                    BYTE                           ! Complete            
Type                        BYTE                           ! Client Monthly, Client Adhoc, Internal Adhoc (not on web)
UID                         ULONG                          ! User ID             
Paid_STRID                  ULONG                          ! For purposes of calculating Paid in last period, this Run ID was used
                         END
                     END                       

_Statement_Run_Desc  FILE,DRIVER('MSSQL'),PRE(STDES),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Statement Run Descriptions
PKey_SRDID               KEY(STDES:SRDID),NOCASE,OPT,PRIMARY ! By ID               
Key_RunDesc              KEY(STDES:RunDescription),NOCASE,OPT ! By Run Description  
Record                   RECORD,PRE()
SRDID                       ULONG                          !                     
RunDescription              STRING(35)                     ! Run Description     
                         END
                     END                       

__RatesTransporter   FILE,DRIVER('MSSQL'),PRE(TRRA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_TRID                KEY(TRRA:TRID),NOCASE,OPT,PRIMARY ! By Transporter Rate ID
FKey_TID                 KEY(TRRA:TID),DUP,NOCASE,OPT      ! By Transporter ID   
FKey_VCID                KEY(TRRA:VCID),DUP,NOCASE,OPT     ! By Vehicle Composition
FKey_JID                 KEY(TRRA:JID),DUP,NOCASE,OPT      ! By Journey          
CKey_TID_JID_VCID_EffDate KEY(TRRA:TID,TRRA:JID,TRRA:VCID,-TRRA:Effective_Date),NOCASE,OPT ! By Transporter, Journey, Vehicle Composition && Effective Date Combination
Record                   RECORD,PRE()
TRID                        ULONG                          ! Transporter Rate ID 
TID                         ULONG                          ! Transporter ID      
VCID                        ULONG                          ! Vehicle Composition ID
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
MinimiumLoad                DECIMAL(8)                     ! In Kgs              
BaseCharge                  DECIMAL(9,2)                   !                     
PerRate                     DECIMAL(10,4)                  ! Above Minimium Load 
Effective_DateAndTime       STRING(8)                      !                     
Effective_DateAndTime_GROUP GROUP,OVER(Effective_DateAndTime) !                     
Effective_Date                DATE                         ! Effective from date 
Effective_TIME                TIME                         ! not used!           
                            END                            !                     
                         END
                     END                       

__RatesBreakbulk     FILE,DRIVER('MSSQL'),PRE(BBRA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Breakbulk Rates ---------- old
PKey_BBID                KEY(BBRA:BBID),NOCASE,OPT,PRIMARY ! By Breakbulk Rate ID
FKey_CID                 KEY(BBRA:CID),DUP,NOCASE,OPT      ! By Client           
FKey_JID                 KEY(BBRA:JID),DUP,NOCASE,OPT      ! By Journey          
CKey_CID_JID_EffDate_ToMass KEY(BBRA:CID,BBRA:JID,-BBRA:Effective_Date,BBRA:ToMass),NOCASE,OPT ! By Client, Journey, Effective Date && To Mass
Record                   RECORD,PRE()
BBID                        ULONG                          ! Breakbulk Rates ID  
CID                         ULONG                          ! Client ID           
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
ToMass                      DECIMAL(9)                     ! Up to this mass in Kgs
RatePerKg                   DECIMAL(8,2)                   ! Rate per Kg         
MinimiumCharge              DECIMAL(10,2)                  !                     
Effective_DateAndTime       STRING(8)                      !                     
Effective_DateAndTime_GROUP GROUP,OVER(Effective_DateAndTime) !                     
Effective_Date                DATE                         ! Effective from this date
Effective_TIME                TIME                         ! Not used!           
                            END                            !                     
                         END
                     END                       

__RatesFuelSurcharge FILE,DRIVER('MSSQL'),PRE(FSRA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Clients Fuel Surcharge Rates
PKey_FCID                KEY(FSRA:FCID),NOCASE,OPT,PRIMARY ! By Fuel Surcharge ID
FKey_CID                 KEY(FSRA:CID),DUP,NOCASE,OPT      ! By Client           
CKey_CID_EffDate         KEY(FSRA:CID,-FSRA:Effective_Date),DUP,OPT ! By Client && Effective Date
FKey_RUBID               KEY(FSRA:RUBID),DUP,NOCASE,OPT    ! By Rate Update ID   
Record                   RECORD,PRE()
FCID                        ULONG                          ! Fuel Surcharges ID  
CID                         ULONG                          ! Client ID           
FuelSurcharge               DECIMAL(5,2)                   ! Fuel Surcharge - % of invoice before VAT, set to default when creating client
Effective_DateAndTime       STRING(8)                      !                     
Effective_DateAndTime_GROUP GROUP,OVER(Effective_DateAndTime) !                     
Effective_Date                DATE                         ! Effective from this date
Effective_TIME                TIME                         ! Not used!           
                            END                            !                     
RUBID                       ULONG                          ! Rate Update Batch ID
                         END
                     END                       

__RatesContainerPark FILE,DRIVER('MSSQL'),PRE(CPRA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Container Park Rates
PKey_CPID                KEY(CPRA:CPID),NOCASE,OPT,PRIMARY ! By Container Park Rates ID
FKey_FID                 KEY(CPRA:FID),DUP,NOCASE,OPT      ! By Floor            
FKey_JID                 KEY(CPRA:JID),DUP,NOCASE,OPT      ! By Journey          
CKey_FID_JID_EffDate_ToMass KEY(CPRA:FID,CPRA:JID,-CPRA:Effective_Date,CPRA:ToMass),NOCASE,OPT ! By Floor, Journey, Effective Date && To Mass
FKey_RUBID               KEY(CPRA:RUBID),DUP,NOCASE,OPT    ! By Rate Update ID   
Record                   RECORD,PRE()
CPID                        ULONG                          ! Container Park Rate ID
FID                         ULONG                          ! Floor ID            
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
ToMass                      DECIMAL(9)                     ! Up to this mass in Kgs
RatePerKg                   DECIMAL(10,4)                  ! Rate per Kg         
Effective_DateAndTime       STRING(8)                      !                     
Effective_DateAndTime_GROUP GROUP,OVER(Effective_DateAndTime) !                     
Effective_Date                DATE                         ! Effective from this date
Effective_TIME                TIME                         ! Not used!           
                            END                            !                     
RUBID                       ULONG                          ! Rate Update Batch ID
                         END
                     END                       

__RatesConsolidated  FILE,DRIVER('MSSQL'),PRE(CSRA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Consolidated Rates ---------- old
PKey_COID                KEY(CSRA:COID),NOCASE,OPT,PRIMARY ! By Consolidated Rate ID
FKey_CID                 KEY(CSRA:CID),DUP,NOCASE,OPT      ! By Client           
FKey_JID                 KEY(CSRA:JID),DUP,NOCASE,OPT      ! By Journey          
CKey_CID_JID_EffDate_ToMass KEY(CSRA:CID,CSRA:JID,-CSRA:Effective_Date,CSRA:ToMass),NOCASE,OPT ! By Client, Journey, Effective Date && To Mass
Record                   RECORD,PRE()
COID                        ULONG                          ! Consolidated Rate ID
CID                         ULONG                          ! Client ID           
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
ToMass                      DECIMAL(9)                     ! Up to this mass in Kgs
RatePerKg                   DECIMAL(8,2)                   ! Rate per Kg         
MinimiumCharge              DECIMAL(10,2)                  !                     
Effective_DateAndTime       STRING(8)                      !                     
Effective_DateAndTime_GROUP GROUP,OVER(Effective_DateAndTime) !                     
Effective_Date                DATE                         ! Effective from this date
Effective_Time                TIME                         ! Not used!           
                            END                            !                     
                         END
                     END                       

__Rates              FILE,DRIVER('MSSQL'),PRE(RAT),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Rates               
PKey_CRID                KEY(RAT:RID),NOCASE,OPT,PRIMARY   ! By Container Rates ID
FKey_CID                 KEY(RAT:CID),DUP,NOCASE,OPT       ! By Client           
FKey_JID                 KEY(RAT:JID),DUP,NOCASE,OPT       ! By Journey          
FKey_LTID                KEY(RAT:LTID),DUP,NOCASE,OPT      ! By Load Type        
FKey_CTID                KEY(RAT:CTID),DUP,NOCASE,OPT      ! By Container Type   
CKey_CRTID_JID_CTID_EffDate_ToMass KEY(RAT:CRTID,RAT:JID,RAT:CTID,-RAT:Effective_Date,RAT:ToMass),NOCASE,OPT ! By Client Rate Type, Journey, Container Type, Effective Date && To Mass
FKey_RUBID               KEY(RAT:RUBID),DUP,NOCASE,OPT     ! By Batch            
FKey_CRTID               KEY(RAT:CRTID),DUP,NOCASE,OPT     ! By Client Rate      
Record                   RECORD,PRE()
RID                         ULONG                          ! Rate ID             
CID                         ULONG                          ! Client ID           
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
LTID                        ULONG                          ! Load Type ID        
CTID                        ULONG                          ! Container Type ID   
ToMass                      DECIMAL(9)                     ! Up to this mass in Kgs
RatePerKg                   DECIMAL(10,4)                  ! Rate per Kg         
MinimiumCharge              DECIMAL(10,2)                  !                     
Effective_DateAndTime       STRING(8)                      !                     
Effective_DateAndTime_GROUP GROUP,OVER(Effective_DateAndTime) !                     
Effective_Date                DATE                         ! Effective from date 
Effective_TIME                TIME                         ! Not used!           
                            END                            !                     
Added_DateTime              STRING(8)                      !                     
Added_DateTime_Group        GROUP,OVER(Added_DateTime)     !                     
Added_Date                    DATE                         !                     
Added_Time                    TIME                         !                     
                            END                            !                     
AdHoc                       BYTE                           ! Ad Hoc rate (not in rate letter)
RUBID                       ULONG                          ! Rate Update Batch ID
CRTID                       ULONG                          ! Client Rate Type ID 
                         END
                     END                       

__RatesAdditionalCharges FILE,DRIVER('MSSQL'),PRE(CARA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Clients Additional Charge Rates
PKey_ACID                KEY(CARA:ACID),NOCASE,OPT,PRIMARY ! By Additional Charge ID
FKey_ACCID               KEY(CARA:ACCID),DUP,NOCASE,OPT    ! By Additional Charge Category
FKey_CID                 KEY(CARA:CID),DUP,NOCASE,OPT      ! By Client           
CKey_CID_EffDate         KEY(CARA:CID,-CARA:Effective_Date),DUP,NOCASE,OPT ! By Client && Effective Date
FKey_RUBID               KEY(CARA:RUBID),DUP,NOCASE,OPT    ! By Rate Update ID   
Record                   RECORD,PRE()
ACID                        ULONG                          ! Additional Charges ID
ACCID                       ULONG                          ! Additional Charge Category ID
CID                         ULONG                          ! Client ID           
Charge                      DECIMAL(11,2)                  ! Charge amount       
Effective_DateAndTime       STRING(8)                      !                     
Effective_DateAndTime_GROUP GROUP,OVER(Effective_DateAndTime) !                     
Effective_Date                DATE                         ! Effective from this date
Effective_TIME                TIME                         ! Not used!           
                            END                            !                     
RUBID                       ULONG                          ! Rate Update Batch ID
                         END
                     END                       

__RatesFuelCost      FILE,DRIVER('MSSQL'),PRE(FCRA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Clients Fuel Cost   
PKey_RFCID               KEY(FCRA:RFCID),NOCASE,OPT,PRIMARY ! By Rates Fuel Cost ID
FKey_CID                 KEY(FCRA:CID),DUP,NOCASE,OPT      ! By Client           
CKey_CID_EffDateDesc     KEY(FCRA:CID,-FCRA:Effective_Date),DUP,OPT ! By Client && Effective Date (desc)
FKey_RUBID               KEY(FCRA:RUBID),DUP,NOCASE,OPT    ! By Rate Update ID   
FKey_FCID                KEY(FCRA:FCID),DUP,NOCASE,OPT     ! By Fuel Cost        
Record                   RECORD,PRE()
RFCID                       ULONG                          ! Fuel Surcharges ID  
CID                         ULONG                          ! Client ID           
FuelCost                    DECIMAL(10,3)                  ! Fuel Cost           
FCID                        ULONG                          ! Fuel Cost ID - used for Base Rate
FCID_CostBase               ULONG                          ! Fuel Cost ID - used for Cost Base
FuelBaseRate                DECIMAL(10,4)                  ! % cost of total cost
FuelSurcharge               DECIMAL(10,3)                  !                     
Effective_DateAndTime       STRING(8)                      !                     
Effective_DateAndTime_GROUP GROUP,OVER(Effective_DateAndTime) !                     
Effective_Date                DATE                         ! Effective from this date
Effective_TIME                TIME                         ! Not used!           
                            END                            !                     
RUBID                       ULONG                          ! Rate Update Batch ID
CostBase_DateTime           STRING(8)                      !                     
CostBase_DateTime_Group     GROUP,OVER(CostBase_DateTime)  !                     
CostBase_Date                 DATE                         !                     
CostBase_Time                 TIME                         ! not used            
                            END                            !                     
CostChange_DateTime         STRING(8)                      !                     
CostChange_DateTime_Group   GROUP,OVER(CostChange_DateTime) !                     
CostChange_Date               DATE                         !                     
CostChange_Time               TIME                         ! not used            
                            END                            !                     
CostBase_X                  DECIMAL(14,3)                  !                     
                         END
                     END                       

_SQLTemp             FILE,DRIVER('MSSQL'),PRE(_SQ),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
Record                   RECORD,PRE()
S1                          STRING(255)                    !                     
S2                          STRING(255)                    !                     
SDateTime                   STRING(8)                      !                     
SDateTime_Group             GROUP,OVER(SDateTime)          !                     
SDate                         DATE                         !                     
STime                         TIME                         !                     
                            END                            !                     
S3                          STRING(255)                    !                     
S4                          STRING(255)                    !                     
                         END
                     END                       

__RatesToll          FILE,DRIVER('MSSQL'),PRE(TOL),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Clients Toll Rates  
PKey_TRID                KEY(TOL:TRID),NOCASE,OPT,PRIMARY  ! By Toll Rate ID     
FKey_CID                 KEY(TOL:CID),DUP,NOCASE,OPT       ! By Client           
CKey_CID_EffDate         KEY(TOL:CID,-TOL:Effective_Date),DUP,OPT ! By Client && Effective Date
FKey_RUBID               KEY(TOL:RUBID),DUP,NOCASE,OPT     ! By Rate Update ID   
Record                   RECORD,PRE()
TRID                        ULONG                          ! Toll Rate ID        
CID                         ULONG                          ! Client ID           
TollRate                    DECIMAL(5,2)                   ! Toll Rate - % of Freight before VAT
Effective_DateAndTime       STRING(8)                      !                     
Effective_DateAndTime_GROUP GROUP,OVER(Effective_DateAndTime) !                     
Effective_Date                DATE                         ! Effective from this date
Effective_TIME                TIME                         ! Not used!           
                            END                            !                     
RUBID                       ULONG                          ! Rate Update Batch ID
                         END
                     END                       

NotificationsSetup   FILE,DRIVER('MSSQL'),PRE(NOT),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_NOTID               KEY(NOT:NOT_ID),NOCASE,OPT,PRIMARY ! By ID               
Key_Stage_Type           KEY(NOT:NoticeStage,NOT:LoadType),NOCASE,OPT ! By Stage && Type    
Record                   RECORD,PRE()
NOT_ID                      ULONG                          !                     
NoticeStage                 BYTE                           ! On Route (M) / Transferred (M) / Out on Delivery (TS) / Delivered (TS)
LoadType                    BYTE                           ! Overnight / Broking / Local Delivery
Subject                     CSTRING(255)                   !                     
Template                    CSTRING(8000)                  !                     
Tip                         CSTRING(256)                   ! Tip of how to set this setting
DoNotGenerate               BYTE                           !                     
                         END
                     END                       

ClientsAlias         FILE,DRIVER('MSSQL'),PRE(A_CLI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_CID                 KEY(A_CLI:CID),NOCASE,OPT,PRIMARY ! By Client ID        
Key_ClientNo             KEY(A_CLI:ClientNo),NOCASE        ! By Client No.       
Key_ClientName           KEY(A_CLI:ClientName)             ! By Client Name      
Key_ClientSearch         KEY(A_CLI:ClientSearch)           ! By Client Search    
FKey_BID                 KEY(A_CLI:BID),DUP,NOCASE,OPT     ! By Branch ID        
FKey_AID                 KEY(A_CLI:AID),DUP,NOCASE,OPT     ! By Address ID       
FKey_ACID                KEY(A_CLI:ACID),DUP,NOCASE,OPT    ! By Accountant ID    
FKey_SRID                KEY(A_CLI:SRID),DUP,NOCASE        !                     
Record                   RECORD,PRE()
CID                         ULONG                          ! Client ID           
ClientNo                    ULONG                          ! Client No.          
ClientName                  STRING(100)                    !                     
ClientSearch                STRING(100)                    ! Search string - client name without punctuation or case (Not dispayed)
BID                         ULONG                          ! Branch ID           
AID                         ULONG                          ! Address ID          
GenerateInvoice             BYTE                           ! Generate an invoice when DI created
MinimiumCharge              DECIMAL(8,2)                   !                     
Rate                        DECIMAL(8,2)                   ! Rate for this debtor
DocumentCharge              DECIMAL(8,2)                   ! Document Charge     
ACID                        ULONG                          ! Accountant ID       
OpsManager                  STRING(35)                     ! Ops Manager         
InsuranceRequired           BYTE                           ! Insurance Required  
InsuranceType               BYTE                           ! Insurance Type      
InsurancePercent            DECIMAL(9,6)                   ! Insruance Percentage
Discounts                   GROUP                          ! Discounts           
OnInvoice                     DECIMAL(5,2)                 ! On Invoice          
Days30                        DECIMAL(5,2)                 ! 30 Days             
Days60                        DECIMAL(5,2)                 ! 60 Days             
Days90                        DECIMAL(5,2)                 ! 90 Days             
                            END                            !                     
PaymentPeriod               USHORT                         ! Payment required in period (in days, 0 is COD/COP)
VolumetricRatio             DECIMAL(8,2)                   ! x square cube weighs this amount
AdviceOfDispatch            BYTE                           ! Advise on dispatch  
DeliveryNotes               BYTE                           ! Delivery notes - Individual or Summary
Notes                       STRING(500)                    !                     
InvoiceMessage              STRING(255)                    !                     
Status                      BYTE                           ! Normal, On Hold, Closed, Dormant
FaxConfirmation             BYTE                           ! Fax confirmation of parcel status required
SRID                        ULONG                          ! Sales Rep ID        
DateAndTimeSalesRepEarnsUntil STRING(8)                    !                     
DateAndTimeSalesRepEarnsUntil_GROUP GROUP,OVER(DateAndTimeSalesRepEarnsUntil) !                     
SalesRepEarnsUntil            DATE                         ! Date up till when the sales rep. earns commision on this client
SalesRepEarnsUntilTIME        TIME                         !                     
                            END                            !                     
VATNo                       STRING(20)                     ! VAT No.             
Terms                       BYTE                           ! Terms - Pre Paid, COD, Account, On Statement
AccountLimit                DECIMAL(12,2)                  ! Account limit       
DateTimeOpened              STRING(8)                      ! Date & Time Opened  
DateTimeOpened_Group        GROUP,OVER(DateTimeOpened)     !                     
DateOpened                    DATE                         !                     
TimeOpened                    TIME                         !                     
                            END                            !                     
Balances                    GROUP                          !                     
UpdatedDateTime               STRING(8)                    !                     
UpdatedDateTime_Group         GROUP,OVER(UpdatedDateTime)  !                     
UpdatedDate                     DATE                       !                     
UpdatedTime                     TIME                       !                     
                              END                          !                     
BalanceCurrent                DECIMAL(12,2)                !                     
Balance30Days                 DECIMAL(12,2)                !                     
Balance60Days                 DECIMAL(12,2)                !                     
Balance90Days                 DECIMAL(12,2)                !                     
                            END                            !                     
FuelSurchargeActive         BYTE                           !                     
PODMessage                  CSTRING(256)                   !                     
LiabilityCover              BYTE                           !                     
LiabilityCrossBorder        BYTE                           !                     
TollChargeActive            BYTE                           !                     
                         END
                     END                       

RemindersAlias       FILE,DRIVER('MSSQL'),PRE(A_REM),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_RID                 KEY(A_REM:RID),NOCASE,OPT,PRIMARY ! By Reminder         
Key_ID                   KEY(A_REM:ID),DUP,NOCASE,OPT      ! By ID               
SKey_Type_ID             KEY(A_REM:ReminderType,A_REM:ID),DUP,NOCASE,OPT ! By Type && ID       
FKey_UID                 KEY(A_REM:UID),DUP,NOCASE,OPT     ! By User             
FKey_UGID                KEY(A_REM:UGID),DUP,NOCASE,OPT    ! By User Group       
SKey_Reference           KEY(A_REM:Reference),DUP,NOCASE,OPT ! By Reference        
Record                   RECORD,PRE()
RID                         ULONG                          ! Reminder ID         
ReminderType                BYTE                           ! General, Client, Truck / Trailer
ID                          ULONG                          ! Link ID             
Active                      BYTE                           ! Active Reminder     
Popup                       BYTE                           ! Popup when due      
ReminderDateTime            STRING(8)                      !                     
ReminderDateTimeGroup       GROUP,OVER(ReminderDateTime)   !                     
ReminderDate                  DATE                         !                     
ReminderTime                  TIME                         !                     
                            END                            !                     
RemindGroup                 GROUP                          !                     
RemindOption                  BYTE                         ! All, Group & User, User, Group
UID                           ULONG                        ! User ID             
UGID                          ULONG                        ! User Group ID       
                            END                            !                     
Notes                       CSTRING(500)                   !                     
CreatedGroup                GROUP                          !                     
Created_UID                   ULONG                        ! User ID             
Created_DateTime              STRING(8)                    !                     
Created_DateTimeGroup         GROUP,OVER(Created_DateTime) !                     
Created_Date                    DATE                       !                     
Created_Time                    TIME                       !                     
                              END                          !                     
                            END                            !                     
Reference                   CSTRING(36)                    ! Reference for this Reminder
SystemGenerated             BYTE                           ! This reminder was System Generated
Reoccur                     GROUP                          !                     
Reoccurs                      BYTE                         ! This reminder reoccurs
Reoccur_Option                BYTE                         !                     
Reoccur_Period                LONG                         !                     
                            END                            !                     
                         END
                     END                       

AddressAlias         FILE,DRIVER('MSSQL'),PRE(A_ADD),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Alias of the Addresses
PKey_AID                 KEY(A_ADD:AID),NOCASE,OPT,PRIMARY ! By AID              
Key_Name                 KEY(A_ADD:AddressName),NOCASE     ! By Name             
FKey_SUID                KEY(A_ADD:SUID),DUP,NOCASE,OPT    ! By Suburb           
FKey_BID                 KEY(A_ADD:BID),DUP,NOCASE,OPT     ! By Branch           
Record                   RECORD,PRE()
AID                         ULONG                          ! Address ID - note once used certain information should not be changeable, such as the suburb
BID                         ULONG                          ! Branch ID           
AddressName                 STRING(35)                     ! Name of this address
AddressNameSuburb           STRING(50)                     ! Address Name & Suburb
Line1                       STRING(35)                     ! Address line 1      
Line2                       STRING(35)                     ! Address line 2      
SUID                        ULONG                          ! Note: to keep system integrity this should not be changeable once used
PhoneNo                     STRING(20)                     ! Phone no.           
PhoneNo2                    STRING(20)                     ! Phone no. 2         
Fax                         CSTRING(61)                    ! Fax                 
ShowForAllBranches          BYTE                           ! Show this address for all branches
Used_Group                  GROUP                          !                     
Branch                        BYTE                         ! This is used for a Branch
Client                        BYTE                         ! This is used for a Client
Accountant                    BYTE                         ! This is used for an Accountant
Delivery                      BYTE                         ! This is used for a Delivery
ContainerTurnIn               BYTE                         ! This is used for a Container Turn In
Transporter                   BYTE                         ! This is used for a Transporter
Journey                       BYTE                         ! This is used for a Journey
                            END                            !                     
Archived                    BYTE                           ! Mark Address as Archived
Archived_DateTime           STRING(8)                      !                     
Archived_DateTime_Group     GROUP,OVER(Archived_DateTime)  !                     
Archived_Date                 DATE                         !                     
Archived_Time                 TIME                         !                     
                            END                            !                     
                         END
                     END                       

LoadTypes2Alias      FILE,DRIVER('MSSQL'),PRE(A_LOAD2),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_LTID                KEY(A_LOAD2:LTID),NOCASE,OPT,PRIMARY ! By Load Type ID     
Key_LoadType             KEY(A_LOAD2:LoadType),NOCASE      ! By Load Type        
FKey_FID                 KEY(A_LOAD2:FID),DUP,NOCASE,OPT   ! By Floor            
Record                   RECORD,PRE()
LTID                        ULONG                          ! Type ID             
LoadType                    STRING(100)                    ! Load Type           
LoadOption                  BYTE                           ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
TurnIn                      BYTE                           ! Container Turn In required for this Load Type
Hazchem                     BYTE                           !                     
ContainerParkStandard       BYTE                           ! Container Park Standard Rates
FID                         ULONG                          ! Floor ID            
                         END
                     END                       

TransporterPaymentsAlias FILE,DRIVER('MSSQL'),PRE(A_TRAP),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
SKey_TPID_Reversal       KEY(A_TRAP:TPID_Reversal),DUP,NOCASE,OPT ! By Transporter Reversal ID
PKey_TPID                KEY(A_TRAP:TPID),NOCASE,OPT,PRIMARY ! By Transporter Payment ID
FKey_TID                 KEY(A_TRAP:TID),DUP,NOCASE,OPT    ! By Transporter      
Record                   RECORD,PRE()
TPID                        ULONG                          !                     
TID                         ULONG                          ! Transporter ID      
DateAndTimeCaptured         STRING(8)                      !                     
DateAndTimeCaptured_GROUP   GROUP,OVER(DateAndTimeCaptured) !                     
DateCaptured                  DATE                         ! Date captured into the system
TimeCaptured                  TIME                         !                     
                            END                            !                     
DateAndTimeMade             STRING(8)                      !                     
DateAndTimeMade_GROUP       GROUP,OVER(DateAndTimeMade)    !                     
DateMade                      DATE                         ! Date payment was made
TimeMade                      TIME                         !                     
                            END                            !                     
Amount                      DECIMAL(10,2)                  ! Amount of payment   
Notes                       STRING(255)                    ! Notes for this payment
Type                        BYTE                           ! Payment, Reversal   
TPID_Reversal               ULONG                          ! Reversal            
                         END
                     END                       

TransporterAlias     FILE,DRIVER('MSSQL'),PRE(A_TRA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_TID                 KEY(A_TRA:TID),NOCASE,OPT,PRIMARY ! By TID              
Key_TransporterName      KEY(A_TRA:TransporterName),NOCASE ! By Transporter Name 
FKey_BID                 KEY(A_TRA:BID),DUP,NOCASE,OPT     ! By Branch ID        
FKey_AID                 KEY(A_TRA:AID),DUP,NOCASE,OPT     ! By Address ID       
FKey_ACID                KEY(A_TRA:ACID),DUP,NOCASE,OPT    ! By Accountant ID    
Record                   RECORD,PRE()
TID                         ULONG                          ! Transporter ID      
TransporterName             STRING(35)                     ! Transporters Name   
BID                         ULONG                          ! Branch ID           
AID                         ULONG                          ! Address ID          
ACID                        ULONG                          ! Accountant ID       
OpsManager                  STRING(35)                     !                     
VATNo                       STRING(20)                     ! VAT No.             
Linked_CID                  ULONG                          ! Client ID that this Transporter is linked to
ChargesVAT                  BYTE                           ! This Transporter charges / pays VAT
Broking                     BYTE                           ! This is a broking transporter
Archived                    BYTE                           !                     
Archived_DateTime           STRING(8)                      !                     
Archived_DateTime_Group     GROUP,OVER(Archived_DateTime)  !                     
Archived_Date                 DATE                         !                     
Archived_Time                 TIME                         !                     
                            END                            !                     
Comments                    CSTRING(1001)                  !                     
Status                      BYTE                           ! Normal, Pending, Do Not Use
                         END
                     END                       

ClientsPaymentsAllocationAlias FILE,DRIVER('MSSQL'),PRE(A_CLIPA),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_CPAID               KEY(A_CLIPA:CPAID),NOCASE,OPT,PRIMARY ! By Clients Payment Allocation ID
FKey_CPID_AllocationNo   KEY(A_CLIPA:CPID,A_CLIPA:AllocationNo),NOCASE,OPT ! By Clients Payment & Allocation Number
Fkey_IID                 KEY(A_CLIPA:IID),DUP,NOCASE,OPT   ! By Invoice          
FKey_CPID                KEY(A_CLIPA:CPID),DUP,NOCASE,OPT  ! By Clients Payment  
Record                   RECORD,PRE()
CPAID                       ULONG                          ! Clients Payment Allocation ID
CPID                        ULONG                          ! Cliets Payment ID   
AllocationNo                SHORT                          ! Allocation number   
IID                         ULONG                          ! Invoice Number      
Amount                      DECIMAL(10,2)                  ! Amount of payment allocated to this Invoice
AllocationDateAndTime       STRING(8)                      !                     
AllocationDateAndTime_GROUP GROUP,OVER(AllocationDateAndTime) !                     
AllocationDate                DATE                         ! Date allocation made
AllocationTime                TIME                         !                     
                            END                            !                     
Comment                     CSTRING(256)                   ! Comment             
StatusUpToDate              BYTE                           ! The status is up to date
                         END
                     END                       

DeliveryItemAlias    FILE,DRIVER('MSSQL'),PRE(A_DELI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Alias of the Delivery Items
PKey_DIID                KEY(A_DELI:DIID),NOCASE,OPT,PRIMARY ! By Delivery Item ID 
FKey_DID_ItemNo          KEY(A_DELI:DID,A_DELI:ItemNo),NOCASE,OPT ! By Delivery ID && Item No.
FKey_CMID                KEY(A_DELI:CMID),DUP,NOCASE,OPT   ! By Commodity        
FKey_COID                KEY(A_DELI:COID),DUP,NOCASE,OPT   ! By Container Operator
FKey_CTID                KEY(A_DELI:CTID),DUP,NOCASE,OPT   ! By Container Type   
FKey_ContainerReturnAID  KEY(A_DELI:ContainerReturnAID),DUP,NOCASE,OPT ! By Container Return Address
FKey_PTID                KEY(A_DELI:PTID),DUP,NOCASE,OPT   ! By Packaging Type ID
Record                   RECORD,PRE()
DIID                        ULONG                          ! Delivery Item ID    
DID                         ULONG                          ! Delivery ID         
ItemNo                      SHORT                          ! Item Number         
CMID                        ULONG                          ! Commodity ID        
Type                        BYTE                           ! Type of Item - Container or Loose
Container_Group             GROUP                          ! Container fields    
ShowOnInvoice                 BYTE                         ! Show these container details on the Invoice generated from this DI
COID                          ULONG                        ! Container Operator ID
CTID                          ULONG                        ! Container Type ID   
ContainerNo                   CSTRING(36)                  !                     
ContainerReturnAID            ULONG                        ! Address ID - note once used certain information should not be changeable, such as the suburb
ContainerVessel               CSTRING(36)                  ! Vessel this container arrived on
SealNo                        CSTRING(36)                  ! Container Seal no.  
DateAndTimeETA                STRING(8)                    !                     
DateAndTimeETA_GROUP          GROUP,OVER(DateAndTimeETA)   !                     
ETA                             DATE                       ! Estimated time of arrival for this Vessel
ETA_TIME                        TIME                       ! Not used!           
                              END                          !                     
                            END                            !                     
Loose_Group                 GROUP                          !                     
ByContainer                   BYTE                         ! Are the items arriving by container. If so allow capturing of container info minus Return Address
Length                        DECIMAL(5,3)                 ! Length in metres    
Breadth                       DECIMAL(5,3)                 ! Breadth in metres   
Height                        DECIMAL(5,3)                 ! Height in metres    
Volume                        DECIMAL(8,3)                 ! Volume for manual entry (metres cubed)
Volume_Unit                   DECIMAL(8,3)                 ! Volume of 1 unit    
Units                         USHORT                       ! Number of units     
PTID                          ULONG                        ! Packaging Type ID   
                            END                            !                     
Weight                      DECIMAL(8,2)                   ! In kg's             
VolumetricRatio             DECIMAL(8,2)                   ! x square cubes weigh this amount
VolumetricWeight            DECIMAL(8,2)                   ! Weight based on Volumetric calculation (in kgs)
Delivered_Group             GROUP                          !                     
DeliveredUnits                USHORT                       ! Units delivered     
                            END                            !                     
                         END
                     END                       

DeliveryItemAlias2   FILE,DRIVER('MSSQL'),PRE(A_DELI2),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_DIID                KEY(A_DELI2:DIID),NOCASE,OPT,PRIMARY ! By Delivery Item ID 
FKey_DID_ItemNo          KEY(A_DELI2:DID,A_DELI2:ItemNo),NOCASE,OPT ! By Delivery ID && Item No.
FKey_CMID                KEY(A_DELI2:CMID),DUP,NOCASE,OPT  ! By Commodity        
FKey_COID                KEY(A_DELI2:COID),DUP,NOCASE,OPT  ! By Container Operator
FKey_CTID                KEY(A_DELI2:CTID),DUP,NOCASE,OPT  ! By Container Type   
FKey_ContainerReturnAID  KEY(A_DELI2:ContainerReturnAID),DUP,NOCASE,OPT ! By Container Return Address
FKey_PTID                KEY(A_DELI2:PTID),DUP,NOCASE,OPT  ! By Packaging Type ID
Record                   RECORD,PRE()
DIID                        ULONG                          ! Delivery Item ID    
DID                         ULONG                          ! Delivery ID         
ItemNo                      SHORT                          ! Item Number         
CMID                        ULONG                          ! Commodity ID        
Type                        BYTE                           ! Type of Item - Container or Loose
Container_Group             GROUP                          ! Container fields    
ShowOnInvoice                 BYTE                         ! Show these container details on the Invoice generated from this DI
COID                          ULONG                        ! Container Operator ID
CTID                          ULONG                        ! Container Type ID   
ContainerNo                   CSTRING(36)                  !                     
ContainerReturnAID            ULONG                        ! Address ID - note once used certain information should not be changeable, such as the suburb
ContainerVessel               CSTRING(36)                  ! Vessel this container arrived on
SealNo                        CSTRING(36)                  ! Container Seal no.  
DateAndTimeETA                STRING(8)                    !                     
DateAndTimeETA_GROUP          GROUP,OVER(DateAndTimeETA)   !                     
ETA                             DATE                       ! Estimated time of arrival for this Vessel
ETA_TIME                        TIME                       ! Not used!           
                              END                          !                     
                            END                            !                     
Loose_Group                 GROUP                          !                     
ByContainer                   BYTE                         ! Are the items arriving by container. If so allow capturing of container info minus Return Address
Length                        DECIMAL(5,3)                 ! Length in metres    
Breadth                       DECIMAL(5,3)                 ! Breadth in metres   
Height                        DECIMAL(5,3)                 ! Height in metres    
Volume                        DECIMAL(8,3)                 ! Volume for manual entry (metres cubed)
Volume_Unit                   DECIMAL(8,3)                 ! Volume of 1 unit    
Units                         USHORT                       ! Number of units     
PTID                          ULONG                        ! Packaging Type ID   
                            END                            !                     
Weight                      DECIMAL(8,2)                   ! In kg's             
VolumetricRatio             DECIMAL(8,2)                   ! x square cubes weigh this amount
VolumetricWeight            DECIMAL(8,2)                   ! Weight based on Volumetric calculation (in kgs)
Delivered_Group             GROUP                          !                     
DeliveredUnits                USHORT                       ! Units delivered     
                            END                            !                     
                         END
                     END                       

JourneysAlias        FILE,DRIVER('MSSQL'),PRE(A_JOU),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_JID                 KEY(A_JOU:JID),NOCASE,OPT,PRIMARY ! By Journey ID       
Key_Journey              KEY(A_JOU:Journey),NOCASE         ! By Journey          
FKey_BID                 KEY(A_JOU:BID),DUP,NOCASE,OPT     ! By Branch           
FKey_FID                 KEY(A_JOU:FID),DUP,NOCASE,OPT     ! By Floor            
FKey_FID2                KEY(A_JOU:FID2),DUP,NOCASE        !                     
Record                   RECORD,PRE()
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
Journey                     CSTRING(701)                   ! Description         
Description                 CSTRING(1501)                  !                     
BID                         ULONG                          ! Branch ID           
FID                         ULONG                          ! Floor ID            
FID2                        ULONG                          ! To/From Floor       
EToll                       BYTE                           ! Is an E-Toll normally incurred on this Journey?
                         END
                     END                       

ClientsPaymentsAlias FILE,DRIVER('MSSQL'),PRE(A_CLIP),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Alias of the client payments
PKey_CPID                KEY(A_CLIP:CPID),NOCASE,OPT,PRIMARY ! By CPID             
FKey_CID                 KEY(A_CLIP:CID),DUP,NOCASE,OPT    ! By Client           
Key_CPIDReversal         KEY(A_CLIP:CPID_Reversal),DUP,NOCASE,OPT ! By Client Payment Reversed
Record                   RECORD,PRE()
CPID                        ULONG                          ! Cliets Payment ID   
CID                         ULONG                          ! Client ID           
DateAndTimeCaptured         STRING(8)                      !                     
DateAndTimeCaptured_GROUP   GROUP,OVER(DateAndTimeCaptured) !                     
DateCaptured                  DATE                         ! Date captured into the system
TimeCaptured                  TIME                         !                     
                            END                            !                     
DateAndTimeMade             STRING(8)                      !                     
DateAndTimeMade_GROUP       GROUP,OVER(DateAndTimeMade)    !                     
DateMade                      DATE                         ! Date payment was made
TimeMade                      TIME                         !                     
                            END                            !                     
Amount                      DECIMAL(10,2)                  ! Amount of payment   
Notes                       STRING(255)                    ! Notes for this payment
Type                        BYTE                           ! Payment, Reversal   
CPID_Reversal               ULONG                          ! Reversal            
Status                      BYTE                           ! Status - Not Allocated, Partial Allocation, Fully Allocated
StatusUpToDate              BYTE                           ! The status is up to date
                         END
                     END                       

FloorsAlias          FILE,DRIVER('MSSQL'),PRE(A_FLO),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Alias of the Floors table
PKey_FID                 KEY(A_FLO:FID),NOCASE,OPT,PRIMARY ! By Floor ID         
Key_Floor                KEY(A_FLO:Floor),NOCASE           ! By Floor            
FKey_AID                 KEY(A_FLO:AID),DUP,NOCASE,OPT     ! By Address          
Record                   RECORD,PRE()
FID                         ULONG                          ! Floor ID            
Floor                       STRING(35)                     ! Floor Name          
FBNFloor                    BYTE                           ! Is this a FBN Floor 
AID                         ULONG                          ! Address ID - note once used certain information should not be changeable, such as the suburb
Print_Rates                 BYTE                           ! Print this Floors rates on rate letters
TripSheetLoading            BYTE                           ! Show this Floor on the Trip Sheet Loading
                         END
                     END                       

DriversAlias         FILE,DRIVER('MSSQL'),PRE(A_DRI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
FKey_BID                 KEY(A_DRI:BID),DUP,NOCASE,OPT     ! By Branch           
PKey_DRID                KEY(A_DRI:DRID),NOCASE,OPT,PRIMARY ! By Driver ID        
Key_FirstNameSurname     KEY(A_DRI:FirstName,A_DRI:Surname),NOCASE ! By First Name && Surname
SKey_FirstNameSurname    KEY(A_DRI:FirstNameSurname),DUP,NOCASE,OPT ! By First Name && Surname (c)
Record                   RECORD,PRE()
DRID                        ULONG                          ! Drivers ID          
FirstName                   STRING(35)                     ! First Name          
Surname                     STRING(35)                     ! Surname             
EmployeeNo                  STRING(20)                     ! Employee No.        
Type                        BYTE                           ! Driver or Assistant 
FirstNameSurname            STRING(70)                     ! Firstname & Surname 
Category                    BYTE                           ! Driver Category     
BID                         ULONG                          ! Branch ID           
Archived                    BYTE                           ! Mark driver as not active
Archived_DateTime           STRING(8)                      !                     
Archived_DateTime_Group     GROUP,OVER(Archived_DateTime)  !                     
Archived_Date                 DATE                         !                     
Archived_Time                 TIME                         !                     
                            END                            !                     
                         END
                     END                       

InvoiceItemsAlias    FILE,DRIVER('MSSQL'),PRE(A_INI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_ITID                KEY(A_INI:ITID),NOCASE,OPT,PRIMARY ! By Item ID          
FKey_IID                 KEY(A_INI:IID),DUP,NOCASE,OPT     ! By Invoice          
FKey_DIID                KEY(A_INI:DIID),DUP,NOCASE,OPT    ! By Delivery Item    
FKey_CMID                KEY(A_INI:CMID),DUP,NOCASE,OPT    ! By Commodity        
Record                   RECORD,PRE()
ITID                        ULONG                          ! Invoice Item ID     
IID                         ULONG                          ! Invoice ID          
DIID                        ULONG                          ! Delivery Item ID    
ItemNo                      USHORT                         ! Item Number         
Type                        BYTE                           ! Type of Item - Container or Loose
CMID                        ULONG                          ! Commodity ID        
Commodity                   CSTRING(36)                    ! Commodity           
Description                 CSTRING(151)                   ! Description - populate with fields from Delivery Item rec.
Units                       USHORT                         !                     
ContainerDescription        CSTRING(151)                   !                     
                         END
                     END                       

DeliveryLegsAlias    FILE,DRIVER('MSSQL'),PRE(A_DELL),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_DLID                KEY(A_DELL:DLID),NOCASE,OPT,PRIMARY ! By Delivery Leg ID  
FKey_DID_Leg             KEY(A_DELL:DID,A_DELL:Leg),DUP,NOCASE,OPT ! By Delivery ID && Leg
FKey_TID                 KEY(A_DELL:TID),DUP,NOCASE,OPT    ! By Transporter ID   
FKey_JID                 KEY(A_DELL:JID),DUP,NOCASE,OPT    ! By Journey          
FKey_CollectionAID       KEY(A_DELL:CollectionAID),DUP,NOCASE,OPT ! By Collection AID   
FKey_DeliveryAID         KEY(A_DELL:DeliveryAID),DUP,NOCASE,OPT ! By Delivery AID     
Record                   RECORD,PRE()
DLID                        ULONG                          ! Delivery Leg ID     
DID                         ULONG                          ! Delivery ID         
Leg                         SHORT                          ! Leg Number          
TID                         ULONG                          ! Transporter ID      
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
CollectionAID               ULONG                          ! Collection Address ID
DeliveryAID                 ULONG                          ! Delivery Address ID 
ChargesGroup                GROUP                          !                     
Cost                          DECIMAL(9,2)                 ! Cost for this leg (from Transporter)
VATRate                       DECIMAL(5,2)                 ! VAT rate            
                            END                            !                     
                         END
                     END                       

InvoiceAlias         FILE,DRIVER('MSSQL'),PRE(A_INV),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Invoice Alias       
PKey_IID                 KEY(A_INV:IID),NOCASE,OPT,PRIMARY ! By Invoice ID       
FKey_ICID                KEY(A_INV:ICID),DUP,NOCASE,OPT    ! By Invoice Composition
FKey_BID                 KEY(A_INV:BID),DUP,NOCASE,OPT     ! By Branch           
FKey_CID                 KEY(A_INV:CID),DUP,NOCASE,OPT     ! By Client           
FKey_DID                 KEY(A_INV:DID),DUP,NOCASE,OPT     ! By Delivery         
SKey_DINo                KEY(A_INV:DINo),DUP,NOCASE,OPT    ! By DI No.           
Key_POD_IID              KEY(A_INV:POD_IID),DUP,NOCASE,OPT ! By POD              
Key_CR_IID               KEY(A_INV:CR_IID),DUP,NOCASE,OPT  ! By Credited Invoice 
FKey_MID                 KEY(A_INV:MID),DUP,NOCASE,OPT     ! By Generating MID   
FKey_IJID                KEY(A_INV:IJID),DUP,NOCASE,OPT    ! By Journal          
SKey_Date_Time           KEY(A_INV:InvoiceDateAndTime),DUP !                     
FKey_CID_Date            KEY(A_INV:CID,A_INV:InvoiceDateAndTime),DUP !                     
Record                   RECORD,PRE()
IID                         ULONG                          ! Invoice Number      
POD_IID                     ULONG                          ! POD No. - same as invoice except for Journal debit/credit of invoice
CR_IID                      ULONG                          ! Credit of Invoice, Invoice IID of credited Invoice
BID                         ULONG                          ! Branch ID           
BranchName                  CSTRING(36)                    ! Branch Name         
CID                         ULONG                          ! Client ID           
Client_Group                GROUP                          ! Clients details at invoice generation
ClientNo                      ULONG                        ! Client No.          
ClientName                    CSTRING(101)                 !                     
ClientLine1                   CSTRING(36)                  ! Address line 1      
ClientLine2                   CSTRING(36)                  ! Address line 2      
ClientSuburb                  CSTRING(51)                  ! Suburb              
ClientPostalCode              CSTRING(11)                  !                     
VATNo                         CSTRING(21)                  ! VAT No.             
InvoiceMessage                CSTRING(256)                 !                     
                            END                            !                     
DID                         ULONG                          ! Delivery ID         
DINo                        ULONG                          ! Delivery Instruction Number
ClientReference             CSTRING(61)                    ! Client Reference    
MIDs                        CSTRING(101)                   ! List of Manifest IDs that the delivery is currently manifested on
Terms                       BYTE                           ! Terms - Pre Paid, COD, Account
ICID                        ULONG                          ! Invoice Composition ID
Shipper_Group               GROUP                          !                     
ShipperName                   CSTRING(36)                  ! Name of this address
ShipperLine1                  CSTRING(36)                  ! Address line 1      
ShipperLine2                  CSTRING(36)                  ! Address line 2      
ShipperSuburb                 CSTRING(51)                  ! Suburb              
ShipperPostalCode             CSTRING(11)                  !                     
                            END                            !                     
Consignee_Group             GROUP                          !                     
ConsigneeName                 CSTRING(36)                  ! Name of this address
ConsigneeLine1                CSTRING(36)                  ! Address line 1      
ConsigneeLine2                CSTRING(36)                  ! Address line 2      
ConsigneeSuburb               CSTRING(51)                  ! Suburb              
ConsigneePostalCode           CSTRING(11)                  !                     
                            END                            !                     
InvoiceDateAndTime          STRING(8)                      !                     
InvoiceDateAndTime_GROUP    GROUP,OVER(InvoiceDateAndTime) !                     
InvoiceDate                   DATE                         ! Invoice Date        
InvoiceTime                   TIME                         ! Invoice Time - generated time
                            END                            !                     
Printed                     BYTE                           ! Printed - every other copy must specify "copy invoice"
Charges_Group               GROUP                          !                     
Insurance                     DECIMAL(10,2)                !                     
Documentation                 DECIMAL(10,2)                !                     
FuelSurcharge                 DECIMAL(10,2)                !                     
AdditionalCharge              DECIMAL(11,2)                ! Additional charges  
FreightCharge                 DECIMAL(11,2)                !                     
VAT                           DECIMAL(10,2)                ! VAT                 
Total                         DECIMAL(11,2)                !                     
TollCharge                    DECIMAL(11,2)                !                     
                            END                            !                     
VATRate                     DECIMAL(5,2)                   ! VAT rate            
Weight                      DECIMAL(8,2)                   ! In kg's             
VolumetricWeight            DECIMAL(8,2)                   ! Weight based on Volumetric calculation (in kgs)
Volume                      DECIMAL(8,3)                   ! Volume for manual entry (metres cubed)
BadDebt                     BYTE                           ! This is a Bad Debt Credit Note
Status                      BYTE                           ! used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note (shown), Bad Debt (shown), Over Paid
StatusUpToDate              BYTE                           ! The status is up to date
MID                         ULONG                          ! Generating MID      
UID                         ULONG                          ! User ID             
DC_ID                       ULONG                          ! Delivery COD Addresses ID
IJID                        ULONG                          ! Invoice Journal ID  
Created_DateTime            STRING(8)                      ! Created Date & Time 
Created_DateTime_Group      GROUP,OVER(Created_DateTime)   !                     
Created_Date                  DATE                         !                     
Created_Time                  TIME                         !                     
                            END                            !                     
                         END
                     END                       

TripSheetsAlias      FILE,DRIVER('MSSQL'),PRE(A_TRI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_TID                 KEY(A_TRI:TRID),NOCASE,OPT,PRIMARY ! By Tripsheet ID     
FKey_BID                 KEY(A_TRI:BID),DUP,NOCASE,OPT     ! By Branch           
FKey_VCID                KEY(A_TRI:VCID),DUP,NOCASE,OPT    ! By Vehicle Compositions
FKey_DRID                KEY(A_TRI:DRID),DUP,NOCASE,OPT    ! By Driver           
FKey_Assistant_DRID      KEY(A_TRI:Assistant_DRID),DUP,NOCASE,OPT ! By Assistant Driver 
FKey_SuburbID            KEY(A_TRI:SUID),DUP,NOCASE,OPT    ! By Suburb           
FKey_TID                 KEY(A_TRI:TID),DUP,NOCASE,OPT     ! By Transporter      
Record                   RECORD,PRE()
TRID                        ULONG                          ! Tripsheet ID        
BID                         ULONG                          ! Branch ID           
VCID                        ULONG                          ! Vehicle Composition ID
DepartDateAndTime           STRING(8)                      !                     
DepartDateAndTime_GROUP     GROUP,OVER(DepartDateAndTime)  !                     
DepartDate                    DATE                         !                     
DepartTime                    TIME                         !                     
                            END                            !                     
ReturnedDateAndTime         STRING(8)                      !                     
ReturnedDateAndTime_GROUP   GROUP,OVER(ReturnedDateAndTime) !                     
ReturnedDate                  DATE                         !                     
ReturnedTime                  TIME                         !                     
                            END                            !                     
Notes                       CSTRING(501)                   ! Notes               
State                       BYTE                           ! State of this Trip Sheet
DRID                        ULONG                          ! Drivers ID          
Assistant_DRID              ULONG                          ! Drivers ID          
FID                         ULONG                          ! Floor ID - delivered to for all items on this tripsheet
Collections                 BYTE                           ! Collections         
SUID                        ULONG                          ! Suburb ID           
TID                         ULONG                          ! Transporter ID      
                         END
                     END                       

DeliveriesAlias      FILE,DRIVER('MSSQL'),PRE(A_DEL),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_DID                 KEY(A_DEL:DID),NOCASE,OPT,PRIMARY ! By Delivery ID      
Key_DINo                 KEY(A_DEL:DINo),NOCASE,OPT        ! By Delivery Instruction No.
SKey_ClientReference     KEY(A_DEL:ClientReference),DUP,NOCASE ! By Client Reference 
FKey_BID                 KEY(A_DEL:BID),DUP,NOCASE,OPT     ! By Branch ID        
FKey_CID                 KEY(A_DEL:CID),DUP,NOCASE,OPT     ! By Client ID        
FKey_JID                 KEY(A_DEL:JID),DUP,NOCASE,OPT     ! By Journey          
FKey_CollectionAID       KEY(A_DEL:CollectionAID),DUP,NOCASE,OPT ! By Collection Address ID
FKey_DeliveryAID         KEY(A_DEL:DeliveryAID),DUP,NOCASE,OPT ! By Delivery Address ID
FKey_LTID                KEY(A_DEL:LTID),DUP,NOCASE,OPT    ! By Load Type        
FKey_SID                 KEY(A_DEL:SID),DUP,NOCASE,OPT     ! By Service ID       
FKey_TripSheetInTID      KEY(A_DEL:TripSheetInTID),DUP,NOCASE,OPT ! By Tripsheet In TID 
FKey_TripSheetOutTID     KEY(A_DEL:TripSheetOutTID),DUP,NOCASE,OPT ! By Tripsheet Out TID
FKey_FID                 KEY(A_DEL:FID),DUP,NOCASE,OPT     ! By Floor            
FKey_DCID                KEY(A_DEL:DC_ID),DUP,NOCASE,OPT   ! By Delivery COD Address
FKey_DELCID              KEY(A_DEL:DELCID),DUP,NOCASE,OPT  ! By Delivery Composition
FKey_CollectedDRID       KEY(A_DEL:CollectedByDRID),DUP,NOCASE,OPT ! By Collected by Driver
FKey_CRTID               KEY(A_DEL:CRTID),DUP,NOCASE,OPT   ! By Client Rate Type 
FKey_UID                 KEY(A_DEL:UID),DUP,NOCASE,OPT     ! By User             
Record                   RECORD,PRE()
DID                         ULONG                          ! Delivery ID         
DINo                        ULONG                          ! Delivery Instruction Number
DIDateAndTime               STRING(8)                      !                     
DIDateAndTime_GROUP         GROUP,OVER(DIDateAndTime)      !                     
DIDate                        DATE                         ! DI Date             
DITime                        TIME                         ! Not used!           
                            END                            !                     
BID                         ULONG                          ! Branch ID           
CID                         ULONG                          ! Client ID           
ClientReference             CSTRING(61)                    ! Client Reference    
JID                         ULONG                          ! Journey ID          
CollectionAID               ULONG                          ! Collection Address ID
DeliveryAID                 ULONG                          ! Delivery Address ID 
CRTID                       ULONG                          ! Client Rate Type ID 
LTID                        ULONG                          ! Load Type ID        
SID                         ULONG                          ! Service Requirement ID
TripSheetInTID              ULONG                          ! Tripsheet ID        
TripSheetOutTID             ULONG                          ! Tripsheet ID        
MultipleLoadDID             ULONG                          ! Delivery ID this DI is linked to - multipart DI, more that 1 load required to deliver
Charges_Group               GROUP                          !                     
Rate                          DECIMAL(10,4)                ! Rate for DI (debtors journey rate possibly adjusted) - Charge will be Rate multiplied by the total weight in kgs.
DocumentCharge                DECIMAL(8,2)                 ! Document Charge applied to this DI
FuelSurcharge                 DECIMAL(8,2)                 ! Fuel Surcharge applied to this DI
Charge                        DECIMAL(11,2)                ! Charge for the DI- Excludes VAT, Docs, Fuel, Insurance and Additional Charges charges
AdditionalCharge_Calculate    BYTE                         ! Calculate the Additional Charge
AdditionalCharge              DECIMAL(11,2)                ! Additional charges  
VATRate                       DECIMAL(5,2)                 ! VAT Rate            
TollRate                      DECIMAL(10,4)                ! Rate Toll charged at as a percent of the Freight charge
TollCharge                    DECIMAL(11,2)                !                     
                            END                            !                     
Insure                      BYTE                           ! Insure the goods on this DI.  Note this is in addition to any insurance that may be part of this clients rate.
TotalConsignmentValue       DECIMAL(11,2)                  ! Total value of the cargo on this DI - required if additional insurance required
InsuranceRate               DECIMAL(9,6)                   ! Insurance rate per ton
FID                         ULONG                          ! Floor ID - if not the current branches floor then allow progress entries
FIDRate                     ULONG                          ! Floor ID used for Container Park Rates - doesn't change
SpecialDeliveryInstructions CSTRING(256)                   ! This will print on the DI and Delivery Note (& POD)
Notes                       CSTRING(256)                   ! General Notes       
ReceivedDateAndTime         STRING(8)                      !                     
ReceivedDateAndTime_GROUP   GROUP,OVER(ReceivedDateAndTime) !                     
ReceivedDate                  DATE                         ! Received Date       
ReceivedTime                  TIME                         ! Not used!           
                            END                            !                     
MultipleManifestsAllowed    BYTE                           ! Allow this DI to be manifested on multiple manifests
Manifested                  BYTE                           ! Manifested status - this has maintenance function - Not Manifested, Partially Manifested, Partially Manifested Multiple, Fully Manifested, Fully Manifested Multiple
Delivered                   BYTE                           ! Delivered Status - this has maintenance function - Not Delivered, Partially Delivered, Delivered, Delivered (manual)
DC_ID                       ULONG                          ! Delivery COD Addresses ID
Terms                       BYTE                           ! Terms - Pre Paid, COD, Account, On Statement
DELCID                      ULONG                          ! Delivery Composition ID
CreatedDateTime             STRING(8)                      !                     
CreatedDateTime_Group       GROUP,OVER(CreatedDateTime)    !                     
CreatedDate                   DATE                         ! Entry created on    
CreatedTime                   TIME                         ! Entry created at    
                            END                            !                     
CollectedByDRID             ULONG                          ! Drivers ID          
UID                         ULONG                          ! User ID - last worked on this DI
ReleasedUID                 ULONG                          ! If requiring release, this is the User that has released it
VATRate_OverriddenUserID    ULONG                          ! The VAT rate has been overridden by this user
NoticeEmailAddresses        STRING(1024)                   ! Email addresses to send notices to of progress of DI
Tolls                       BYTE                           ! Are tolls to be charged on this DI?
                         END
                     END                       

TruckTrailerAlias    FILE,DRIVER('MSSQL'),PRE(A_TRU),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_TTID                KEY(A_TRU:TTID),NOCASE,OPT,PRIMARY ! By TTID             
FKey_TID                 KEY(A_TRU:TID),DUP,NOCASE,OPT     ! By Tansporter       
FKey_VMMID               KEY(A_TRU:VMMID),DUP,NOCASE,OPT   ! By Vehicle Make && Model
Key_Registration         KEY(A_TRU:Registration),NOCASE,OPT ! By Registration     
FKey_DRID                KEY(A_TRU:DRID),DUP,NOCASE,OPT    ! By Driver           
Record                   RECORD,PRE()
TTID                        ULONG                          ! Track or Trailer ID 
TID                         ULONG                          ! Transporter ID      
VMMID                       ULONG                          ! Vehicle Make & Model ID
Type                        BYTE                           ! Type of vehicle - Horse, Trailer, Rigid
DRID                        ULONG                          ! Driver - Note these only apply to Horse & Combined types
Registration                STRING(20)                     !                     
Capacity                    DECIMAL(6)                     ! In Kgs              
LicenseInfoGroup            GROUP                          !                     
LicenseExpiryDateTime         STRING(8)                    !                     
LicenseExpiryGroup            GROUP,OVER(LicenseExpiryDateTime) !                     
LicenseExpiryDate               DATE                       ! License expires on this date
LicenseExpiryTime               TIME                       !                     
                              END                          !                     
LicenseInfo                   STRING(250)                  !                     
                            END                            !                     
                         END
                     END                       

TripSheetDeliveriesAlias FILE,DRIVER('MSSQL'),PRE(A_TRDI),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_TDID                KEY(A_TRDI:TDID),NOCASE,OPT,PRIMARY ! By Trip Sheet Delivery ID
FKey_TRID                KEY(A_TRDI:TRID),DUP,NOCASE,OPT   ! By Tripsheet ID     
FKey_DIID                KEY(A_TRDI:DIID),DUP,NOCASE,OPT   ! By Delivery Item ID 
CKey_TRID_DIID           KEY(A_TRDI:TRID,A_TRDI:DIID),DUP,NOCASE,OPT ! By Trip Sheet && Delivery Item
Record                   RECORD,PRE()
TDID                        ULONG                          ! Trip Sheet Delivery Items ID
TRID                        ULONG                          ! Tripsheet ID        
DIID                        ULONG                          ! Delivery Item ID    
UnitsLoaded                 USHORT                         ! Number of units     
Delivery_Group              GROUP                          !                     
DeliveredDateAndTime          STRING(8)                    !                     
DeliveredDateAndTime_GROUP    GROUP,OVER(DeliveredDateAndTime) !                     
DeliveredDate                   DATE                       ! Date delivered      
DeliveredTime                   TIME                       ! Time delivered      
                              END                          !                     
UnitsDelivered                USHORT                       !                     
UnitsNotAccepted              USHORT                       ! Units that were not accepted for delivery
ClearSignature                BYTE                         ! Does the POD have a clear signature
Notes                         CSTRING(256)                 ! Notes               
                            END                            !                     
                         END
                     END                       

InvoiceTransporterAlias FILE,DRIVER('MSSQL'),PRE(A_INT),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) !                     
PKey_TIN                 KEY(A_INT:TIN),NOCASE,OPT,PRIMARY ! By Transporter Invoice No.
FKey_TID                 KEY(A_INT:TID),DUP,NOCASE,OPT     ! By Transporter      
FKey_BID                 KEY(A_INT:BID),DUP,NOCASE,OPT     ! By Branch           
Fkey_MID                 KEY(A_INT:MID),DUP,NOCASE,OPT     ! By Manifest         
FKey_UID                 KEY(A_INT:UID),DUP,NOCASE,OPT     ! By User             
FKey_DID                 KEY(A_INT:DID),DUP,NOCASE,OPT     ! By Delivery         
FKey_DLID                KEY(A_INT:DLID),DUP,NOCASE,OPT    ! By Delivery Leg     
Key_CR_TIN               KEY(A_INT:CR_TIN),DUP,NOCASE,OPT  ! By Credited Invoice 
SKey_MID_DID             KEY(A_INT:MID,A_INT:DID),DUP,NOCASE,OPT ! By Manifest && Delivery
SKey_MID_Manifest        KEY(A_INT:MID,A_INT:Manifest),DUP,NOCASE,OPT ! By MID && Manifest - manifest invoice for this MID
Record                   RECORD,PRE()
TIN                         ULONG                          ! Transporter Invoice No.
CR_TIN                      ULONG                          ! Transporter Invoice No.
TID                         ULONG                          ! Transporter ID      
BID                         ULONG                          ! Branch ID           
MID                         ULONG                          ! Manifest ID         
JID                         ULONG                          ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
DeliveryGroup               GROUP                          ! used when a Delivery has an extra leg, generating MID is recorded in this case
DID                           ULONG                        ! Delivery ID         
DINo                          ULONG                        ! Delivery Instruction Number
DLID                          ULONG                        ! Delivery Leg ID     
Leg                           SHORT                        ! Leg Number          
CollectionAID                 ULONG                        ! Collection Address ID
DeliveryAID                   ULONG                        ! Delivery Address ID 
                            END                            !                     
ChargesGroup                GROUP                          !                     
Cost                          DECIMAL(10,2)                !                     
VAT                           DECIMAL(10,2)                ! VAT amount          
Rate                          DECIMAL(10,4)                ! Rate per Kg - this can be calculated from the total mass and the cost
VATRate                       DECIMAL(5,2)                 ! VAT rate            
                            END                            !                     
InvoiceDateTime             STRING(8)                      !                     
InvoiceDateTimeGroup        GROUP,OVER(InvoiceDateTime)    !                     
InvoiceDate                   DATE                         !                     
InvoiceTime                   TIME                         !                     
                            END                            !                     
CreatedDateTime             STRING(8)                      ! Created on this date at this time
CreatedDateTimeGroup        GROUP,OVER(CreatedDateTime)    !                     
CreatedDate                   DATE                         !                     
CreatedTime                   TIME                         !                     
                            END                            !                     
UID                         ULONG                          ! User ID             
Broking                     BYTE                           ! Broking Invoice (from Broking Manifest or Delivery Leg)
Manifest                    BYTE                           ! This is the Manifest invoice
Status                      BYTE                           ! used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid
StatusUpToDate              BYTE                           ! The status is up to date
Comment                     CSTRING(256)                   !                     
VAT_Specified               BYTE                           ! The user has chosen to specify the VAT amount
ExtraInv                    BYTE                           ! Is this Invoice an Extra Invoice
Creditors_Group             GROUP                          ! Information for creditors relating to this invoice
CIN                           CSTRING(31)                  ! Creditors Invoice No. - 3rd party reference
CIN_DateTimeReceived          STRING(8)                    !                     
CIN_DateTimeReceived_Group    GROUP,OVER(CIN_DateTimeReceived) !                     
CIN_DateReceived                DATE                       !                     
CIN_TimeReceived                TIME                       ! no use              
                              END                          !                     
                            END                            !                     
                         END
                     END                       

Statement_Runs_Alias FILE,DRIVER('MSSQL'),PRE(A_STAR),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode) ! Statement Run Alias 
PKey_STRID               KEY(A_STAR:STRID),NOCASE,OPT,PRIMARY ! By Statement Run ID 
Key_DateTime             KEY(-A_STAR:RunDateTime),DUP,NOCASE ! By Run Date && Time 
FKey_UID                 KEY(A_STAR:UID),DUP,NOCASE,OPT    ! By User             
FKey_RunDesc             KEY(A_STAR:RunDescription),DUP,NOCASE,OPT ! By Run Description  
Record                   RECORD,PRE()
STRID                       ULONG                          ! Statement Run ID    
RunDescription              STRING(35)                     ! Run Description     
RunDateTime                 STRING(8)                      !                     
RunDateTime_Group           GROUP,OVER(RunDateTime)        !                     
RunDate                       DATE                         ! Statement Run Date  
RunTime                       TIME                         !                     
                            END                            !                     
EntryDateTime               STRING(8)                      !                     
EntryDateTime_Group         GROUP,OVER(EntryDateTime)      !                     
EntryDate                     DATE                         ! Run done at         
EntryTime                     TIME                         !                     
                            END                            !                     
Complete                    BYTE                           ! Complete            
Type                        BYTE                           ! Client Monthly, Client Adhoc, Internal Adhoc (not on web)
UID                         ULONG                          ! User ID             
Paid_STRID                  ULONG                          ! For purposes of calculating Paid in last period, this Run ID was used
                         END
                     END                       

!endregion

  Include('TransIS.equ')
  Include('TransISTypes.clw')
  Include('DatePeriods.inc')
db Debuger
Q_Type_UL_S500      QUEUE,TYPE
UL1_        ULONG
S500        STRING(500)
                    .
GlobalInsight Class(Insight),type
Init             PROCEDURE (Long par:Control, Long par:Type=1, Real par:Points=1),Long,VIRTUAL
              End
!--------------------------------------------------------------------------
! -----------Tintools-------------
!--------------------------------------------------------------------------
TinToolsViewRecord  EQUATE(15)
TinToolsCopyRecord  EQUATE(16)
!--------------------------------------------------------------------------
! -----------Tintools-------------
!--------------------------------------------------------------------------
Access:Clients       &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Clients
Relate:Clients       &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Clients
Access:Reminders     &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Reminders
Relate:Reminders     &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Reminders
Access:WebClients    &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for WebClients
Relate:WebClients    &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for WebClients
Access:_View_Rates_Client_Journeys &FileManager,THREAD,EXTERNAL,DLL ! FileManager for _View_Rates_Client_Journeys
Relate:_View_Rates_Client_Journeys &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for _View_Rates_Client_Journeys
Access:AdditionalCharges &FileManager,THREAD,EXTERNAL,DLL  ! FileManager for AdditionalCharges
Relate:AdditionalCharges &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for AdditionalCharges
Access:Clients_ContainerParkDiscounts &FileManager,THREAD,EXTERNAL,DLL ! FileManager for Clients_ContainerParkDiscounts
Relate:Clients_ContainerParkDiscounts &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for Clients_ContainerParkDiscounts
Access:VehicleMakeModel &FileManager,THREAD,EXTERNAL,DLL   ! FileManager for VehicleMakeModel
Relate:VehicleMakeModel &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for VehicleMakeModel
Access:_View_ContainerParkRates_Client &FileManager,THREAD,EXTERNAL,DLL ! FileManager for _View_ContainerParkRates_Client
Relate:_View_ContainerParkRates_Client &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for _View_ContainerParkRates_Client
Access:Addresses     &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Addresses
Relate:Addresses     &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Addresses
Access:Application_Section_Usage &FileManager,THREAD,EXTERNAL,DLL ! FileManager for Application_Section_Usage
Relate:Application_Section_Usage &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for Application_Section_Usage
Access:ApplicationSections_Extras &FileManager,THREAD,EXTERNAL,DLL ! FileManager for ApplicationSections_Extras
Relate:ApplicationSections_Extras &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for ApplicationSections_Extras
Access:EmailAddresses &FileManager,THREAD,EXTERNAL,DLL     ! FileManager for EmailAddresses
Relate:EmailAddresses &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for EmailAddresses
Access:Add_Suburbs   &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Add_Suburbs
Relate:Add_Suburbs   &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Add_Suburbs
Access:LoadTypes2    &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for LoadTypes2
Relate:LoadTypes2    &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for LoadTypes2
Access:TransporterPayments &FileManager,THREAD,EXTERNAL,DLL ! FileManager for TransporterPayments
Relate:TransporterPayments &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for TransporterPayments
Access:Branches      &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Branches
Relate:Branches      &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Branches
Access:Add_Cities    &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Add_Cities
Relate:Add_Cities    &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Add_Cities
Access:Add_Provinces &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Add_Provinces
Relate:Add_Provinces &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Add_Provinces
Access:Accountants   &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Accountants
Relate:Accountants   &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Accountants
Access:_View_Rates_Clients_LoadTypes &FileManager,THREAD,EXTERNAL,DLL ! FileManager for _View_Rates_Clients_LoadTypes
Relate:_View_Rates_Clients_LoadTypes &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for _View_Rates_Clients_LoadTypes
Access:Transporter   &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Transporter
Relate:Transporter   &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Transporter
Access:Container_Parks &FileManager,THREAD,EXTERNAL,DLL    ! FileManager for Container_Parks
Relate:Container_Parks &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for Container_Parks
Access:ClientsPaymentsAllocation &FileManager,THREAD,EXTERNAL,DLL ! FileManager for ClientsPaymentsAllocation
Relate:ClientsPaymentsAllocation &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for ClientsPaymentsAllocation
Access:Users         &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Users
Relate:Users         &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Users
Access:_FuelCost     &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for _FuelCost
Relate:_FuelCost     &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for _FuelCost
Access:ContainerTypes &FileManager,THREAD,EXTERNAL,DLL     ! FileManager for ContainerTypes
Relate:ContainerTypes &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for ContainerTypes
Access:DeliveryItems_Components &FileManager,THREAD,EXTERNAL,DLL ! FileManager for DeliveryItems_Components
Relate:DeliveryItems_Components &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for DeliveryItems_Components
Access:DeliveryItems &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for DeliveryItems
Relate:DeliveryItems &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for DeliveryItems
Access:_View_Rates_Client &FileManager,THREAD,EXTERNAL,DLL ! FileManager for _View_Rates_Client
Relate:_View_Rates_Client &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for _View_Rates_Client
Access:DeliveryComposition &FileManager,THREAD,EXTERNAL,DLL ! FileManager for DeliveryComposition
Relate:DeliveryComposition &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for DeliveryComposition
Access:Commodities   &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Commodities
Relate:Commodities   &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Commodities
Access:UserGroups    &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for UserGroups
Relate:UserGroups    &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for UserGroups
Access:DeliveriesAdditionalCharges &FileManager,THREAD,EXTERNAL,DLL ! FileManager for DeliveriesAdditionalCharges
Relate:DeliveriesAdditionalCharges &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for DeliveriesAdditionalCharges
Access:Journeys      &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Journeys
Relate:Journeys      &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Journeys
Access:ClientsPayments &FileManager,THREAD,EXTERNAL,DLL    ! FileManager for ClientsPayments
Relate:ClientsPayments &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for ClientsPayments
Access:Floors        &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Floors
Relate:Floors        &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Floors
Access:DeliveryStatuses &FileManager,THREAD,EXTERNAL,DLL   ! FileManager for DeliveryStatuses
Relate:DeliveryStatuses &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for DeliveryStatuses
Access:AddressContacts &FileManager,THREAD,EXTERNAL,DLL    ! FileManager for AddressContacts
Relate:AddressContacts &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for AddressContacts
Access:ClientsRateTypes &FileManager,THREAD,EXTERNAL,DLL   ! FileManager for ClientsRateTypes
Relate:ClientsRateTypes &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for ClientsRateTypes
Access:Vessels       &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Vessels
Relate:Vessels       &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Vessels
Access:Delivery_CODAddresses &FileManager,THREAD,EXTERNAL,DLL ! FileManager for Delivery_CODAddresses
Relate:Delivery_CODAddresses &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for Delivery_CODAddresses
Access:ContainerOperators &FileManager,THREAD,EXTERNAL,DLL ! FileManager for ContainerOperators
Relate:ContainerOperators &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for ContainerOperators
Access:Drivers       &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Drivers
Relate:Drivers       &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Drivers
Access:RemindersUsers &FileManager,THREAD,EXTERNAL,DLL     ! FileManager for RemindersUsers
Relate:RemindersUsers &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for RemindersUsers
Access:_InvoiceItems &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for _InvoiceItems
Relate:_InvoiceItems &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for _InvoiceItems
Access:DeliveryLegs  &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for DeliveryLegs
Relate:DeliveryLegs  &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for DeliveryLegs
Access:Shortages_Damages &FileManager,THREAD,EXTERNAL,DLL  ! FileManager for Shortages_Damages
Relate:Shortages_Damages &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for Shortages_Damages
Access:_Statements   &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for _Statements
Relate:_Statements   &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for _Statements
Access:ServiceRequirements &FileManager,THREAD,EXTERNAL,DLL ! FileManager for ServiceRequirements
Relate:ServiceRequirements &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for ServiceRequirements
Access:_Invoice      &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for _Invoice
Relate:_Invoice      &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for _Invoice
Access:ManifestLoad  &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for ManifestLoad
Relate:ManifestLoad  &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for ManifestLoad
Access:ManifestLoadDeliveries &FileManager,THREAD,EXTERNAL,DLL ! FileManager for ManifestLoadDeliveries
Relate:ManifestLoadDeliveries &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for ManifestLoadDeliveries
Access:TransporterPaymentsAllocations &FileManager,THREAD,EXTERNAL,DLL ! FileManager for TransporterPaymentsAllocations
Relate:TransporterPaymentsAllocations &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for TransporterPaymentsAllocations
Access:TripSheets    &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for TripSheets
Relate:TripSheets    &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for TripSheets
Access:Manifest      &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Manifest
Relate:Manifest      &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Manifest
Access:ApplicationSections &FileManager,THREAD,EXTERNAL,DLL ! FileManager for ApplicationSections
Relate:ApplicationSections &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for ApplicationSections
Access:UsersGroups   &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for UsersGroups
Relate:UsersGroups   &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for UsersGroups
Access:UsersAccesses &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for UsersAccesses
Relate:UsersAccesses &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for UsersAccesses
Access:UserGroupAccesses &FileManager,THREAD,EXTERNAL,DLL  ! FileManager for UserGroupAccesses
Relate:UserGroupAccesses &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for UserGroupAccesses
Access:UsersAccessesExtra &FileManager,THREAD,EXTERNAL,DLL ! FileManager for UsersAccessesExtra
Relate:UsersAccessesExtra &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for UsersAccessesExtra
Access:UserGroupAccessesExtra &FileManager,THREAD,EXTERNAL,DLL ! FileManager for UserGroupAccessesExtra
Relate:UserGroupAccessesExtra &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for UserGroupAccessesExtra
Access:__RatesContainer &FileManager,THREAD,EXTERNAL,DLL   ! FileManager for __RatesContainer
Relate:__RatesContainer &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for __RatesContainer
Access:SalesRepsBands &FileManager,THREAD,EXTERNAL,DLL     ! FileManager for SalesRepsBands
Relate:SalesRepsBands &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for SalesRepsBands
Access:Add_Countries &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Add_Countries
Relate:Add_Countries &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Add_Countries
Access:Deliveries    &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for Deliveries
Relate:Deliveries    &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for Deliveries
Access:WebClientsLoginLog &FileManager,THREAD,EXTERNAL,DLL ! FileManager for WebClientsLoginLog
Relate:WebClientsLoginLog &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for WebClientsLoginLog
Access:TruckTrailer  &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for TruckTrailer
Relate:TruckTrailer  &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for TruckTrailer
Access:__RateUpdates &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for __RateUpdates
Relate:__RateUpdates &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for __RateUpdates
Access:TripSheetDeliveries &FileManager,THREAD,EXTERNAL,DLL ! FileManager for TripSheetDeliveries
Relate:TripSheetDeliveries &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for TripSheetDeliveries
Access:_InvoiceTransporter &FileManager,THREAD,EXTERNAL,DLL ! FileManager for _InvoiceTransporter
Relate:_InvoiceTransporter &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for _InvoiceTransporter
Access:_InvoiceJournals &FileManager,THREAD,EXTERNAL,DLL   ! FileManager for _InvoiceJournals
Relate:_InvoiceJournals &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for _InvoiceJournals
Access:PackagingTypes &FileManager,THREAD,EXTERNAL,DLL     ! FileManager for PackagingTypes
Relate:PackagingTypes &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for PackagingTypes
Access:DeliveryProgress &FileManager,THREAD,EXTERNAL,DLL   ! FileManager for DeliveryProgress
Relate:DeliveryProgress &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for DeliveryProgress
Access:VehicleComposition &FileManager,THREAD,EXTERNAL,DLL ! FileManager for VehicleComposition
Relate:VehicleComposition &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for VehicleComposition
Access:SalesReps     &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for SalesReps
Relate:SalesReps     &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for SalesReps
Access:_RemittanceItems &FileManager,THREAD,EXTERNAL,DLL   ! FileManager for _RemittanceItems
Relate:_RemittanceItems &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for _RemittanceItems
Access:_Remittance   &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for _Remittance
Relate:_Remittance   &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for _Remittance
Access:_Remittance_Runs &FileManager,THREAD,EXTERNAL,DLL   ! FileManager for _Remittance_Runs
Relate:_Remittance_Runs &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for _Remittance_Runs
Access:_InvoiceComposition &FileManager,THREAD,EXTERNAL,DLL ! FileManager for _InvoiceComposition
Relate:_InvoiceComposition &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for _InvoiceComposition
Access:_StatementItems &FileManager,THREAD,EXTERNAL,DLL    ! FileManager for _StatementItems
Relate:_StatementItems &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for _StatementItems
Access:_Statement_Runs &FileManager,THREAD,EXTERNAL,DLL    ! FileManager for _Statement_Runs
Relate:_Statement_Runs &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for _Statement_Runs
Access:_Statement_Run_Desc &FileManager,THREAD,EXTERNAL,DLL ! FileManager for _Statement_Run_Desc
Relate:_Statement_Run_Desc &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for _Statement_Run_Desc
Access:__RatesTransporter &FileManager,THREAD,EXTERNAL,DLL ! FileManager for __RatesTransporter
Relate:__RatesTransporter &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for __RatesTransporter
Access:__RatesBreakbulk &FileManager,THREAD,EXTERNAL,DLL   ! FileManager for __RatesBreakbulk
Relate:__RatesBreakbulk &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for __RatesBreakbulk
Access:__RatesFuelSurcharge &FileManager,THREAD,EXTERNAL,DLL ! FileManager for __RatesFuelSurcharge
Relate:__RatesFuelSurcharge &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for __RatesFuelSurcharge
Access:__RatesContainerPark &FileManager,THREAD,EXTERNAL,DLL ! FileManager for __RatesContainerPark
Relate:__RatesContainerPark &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for __RatesContainerPark
Access:__RatesConsolidated &FileManager,THREAD,EXTERNAL,DLL ! FileManager for __RatesConsolidated
Relate:__RatesConsolidated &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for __RatesConsolidated
Access:__Rates       &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for __Rates
Relate:__Rates       &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for __Rates
Access:__RatesAdditionalCharges &FileManager,THREAD,EXTERNAL,DLL ! FileManager for __RatesAdditionalCharges
Relate:__RatesAdditionalCharges &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for __RatesAdditionalCharges
Access:__RatesFuelCost &FileManager,THREAD,EXTERNAL,DLL    ! FileManager for __RatesFuelCost
Relate:__RatesFuelCost &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for __RatesFuelCost
Access:_SQLTemp      &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for _SQLTemp
Relate:_SQLTemp      &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for _SQLTemp
Access:__RatesToll   &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for __RatesToll
Relate:__RatesToll   &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for __RatesToll
Access:NotificationsSetup &FileManager,THREAD,EXTERNAL,DLL ! FileManager for NotificationsSetup
Relate:NotificationsSetup &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for NotificationsSetup
Access:ClientsAlias  &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for ClientsAlias
Relate:ClientsAlias  &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for ClientsAlias
Access:RemindersAlias &FileManager,THREAD,EXTERNAL,DLL     ! FileManager for RemindersAlias
Relate:RemindersAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for RemindersAlias
Access:AddressAlias  &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for AddressAlias
Relate:AddressAlias  &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for AddressAlias
Access:LoadTypes2Alias &FileManager,THREAD,EXTERNAL,DLL    ! FileManager for LoadTypes2Alias
Relate:LoadTypes2Alias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for LoadTypes2Alias
Access:TransporterPaymentsAlias &FileManager,THREAD,EXTERNAL,DLL ! FileManager for TransporterPaymentsAlias
Relate:TransporterPaymentsAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for TransporterPaymentsAlias
Access:TransporterAlias &FileManager,THREAD,EXTERNAL,DLL   ! FileManager for TransporterAlias
Relate:TransporterAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for TransporterAlias
Access:ClientsPaymentsAllocationAlias &FileManager,THREAD,EXTERNAL,DLL ! FileManager for ClientsPaymentsAllocationAlias
Relate:ClientsPaymentsAllocationAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for ClientsPaymentsAllocationAlias
Access:DeliveryItemAlias &FileManager,THREAD,EXTERNAL,DLL  ! FileManager for DeliveryItemAlias
Relate:DeliveryItemAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for DeliveryItemAlias
Access:DeliveryItemAlias2 &FileManager,THREAD,EXTERNAL,DLL ! FileManager for DeliveryItemAlias2
Relate:DeliveryItemAlias2 &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for DeliveryItemAlias2
Access:JourneysAlias &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for JourneysAlias
Relate:JourneysAlias &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for JourneysAlias
Access:ClientsPaymentsAlias &FileManager,THREAD,EXTERNAL,DLL ! FileManager for ClientsPaymentsAlias
Relate:ClientsPaymentsAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for ClientsPaymentsAlias
Access:FloorsAlias   &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for FloorsAlias
Relate:FloorsAlias   &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for FloorsAlias
Access:DriversAlias  &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for DriversAlias
Relate:DriversAlias  &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for DriversAlias
Access:InvoiceItemsAlias &FileManager,THREAD,EXTERNAL,DLL  ! FileManager for InvoiceItemsAlias
Relate:InvoiceItemsAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for InvoiceItemsAlias
Access:DeliveryLegsAlias &FileManager,THREAD,EXTERNAL,DLL  ! FileManager for DeliveryLegsAlias
Relate:DeliveryLegsAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for DeliveryLegsAlias
Access:InvoiceAlias  &FileManager,THREAD,EXTERNAL,DLL      ! FileManager for InvoiceAlias
Relate:InvoiceAlias  &RelationManager,THREAD,EXTERNAL,DLL  ! RelationManager for InvoiceAlias
Access:TripSheetsAlias &FileManager,THREAD,EXTERNAL,DLL    ! FileManager for TripSheetsAlias
Relate:TripSheetsAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for TripSheetsAlias
Access:DeliveriesAlias &FileManager,THREAD,EXTERNAL,DLL    ! FileManager for DeliveriesAlias
Relate:DeliveriesAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for DeliveriesAlias
Access:TruckTrailerAlias &FileManager,THREAD,EXTERNAL,DLL  ! FileManager for TruckTrailerAlias
Relate:TruckTrailerAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for TruckTrailerAlias
Access:TripSheetDeliveriesAlias &FileManager,THREAD,EXTERNAL,DLL ! FileManager for TripSheetDeliveriesAlias
Relate:TripSheetDeliveriesAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for TripSheetDeliveriesAlias
Access:InvoiceTransporterAlias &FileManager,THREAD,EXTERNAL,DLL ! FileManager for InvoiceTransporterAlias
Relate:InvoiceTransporterAlias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for InvoiceTransporterAlias
Access:Statement_Runs_Alias &FileManager,THREAD,EXTERNAL,DLL ! FileManager for Statement_Runs_Alias
Relate:Statement_Runs_Alias &RelationManager,THREAD,EXTERNAL,DLL ! RelationManager for Statement_Runs_Alias

GlobalRequest        BYTE,EXTERNAL,DLL,THREAD              ! Exported from a dll, set when a browse calls a form, to let it know action to perform
GlobalResponse       BYTE,EXTERNAL,DLL,THREAD              ! Exported from a dll, set to the response from the form
VCRRequest           LONG,EXTERNAL,DLL,THREAD              ! Exported from a dll, set to the request from the VCR buttons
FuzzyMatcher         FuzzyClass                            ! Global fuzzy matcher
LocalErrorStatus     ErrorStatusClass,THREAD
LocalErrors          ErrorClass
LocalINIMgr          INIClass
GlobalErrors         &ErrorClass
INIMgr               &INIClass
DLLInitializer       CLASS                                 ! An object of this type is used to initialize the dll, it is created in the generated bc module
Construct              PROCEDURE
Destruct               PROCEDURE
                     END

  CODE
DLLInitializer.Construct PROCEDURE


  CODE
  LocalErrors.Init(LocalErrorStatus)
  LocalINIMgr.Init('.\MANTRNIS.INI', NVD_INI)              ! Initialize the local INI manager to use windows INI file
  INIMgr &= LocalINIMgr
  IF GlobalErrors &= NULL
    GlobalErrors &= LocalErrors                            ! Assign local managers to global managers
  END
  FuzzyMatcher.Init                                        ! Initilaize the browse 'fuzzy matcher'
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)            ! Configure case matching
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)          ! Configure 'word only' matching
                             ! Begin Generated by NetTalk Extension Template
    if ~command ('/netnolog') and (command ('/nettalklog') or command ('/nettalklogerrors') or command ('/neterrors') or command ('/netall'))
      NetDebugTrace ('[Nettalk Template] NetTalk Template version 10.11')
      NetDebugTrace ('[Nettalk Template] NetTalk Template using Clarion ' & 10000)
      NetDebugTrace ('[Nettalk Template] NetTalk Object version ' & NETTALK:VERSION )
      NetDebugTrace ('[Nettalk Template] ABC Template Chain')
    end
                             ! End Generated by Extension Template
     db.init('MANTRNIS',1,50)  ! intiialize the debuger class 
  

!* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

LinePrint FUNCTION(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>)                ! LinePrint Function

   CODE                                                                                 ! Fuction Code Starts Here
    IF OMITTED(2)                                                                       ! If Device Name is Omitted
       IF SUB(PRINTER{07B29H},1,2) = '\\' 
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})))               ! Use Default Device
       ELSE
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})) - 1)           ! Use Default Device
       END 
    ELSE                                                                                ! Otherwise
       lpszFilename = CLIP(DeviceName)                                                  ! Use passed Device Name
    END                                                                                 ! Terminate IF

    IF (OMITTED(3) OR CRLF = True) AND CLIP(StringToPrint) <> FF                        ! If CRLF parameter is set or omitted and user did not pass FF
        hpvBuffer = StringToPrint & CR & LF                                             ! Print String with CR & LF
    ELSE                                                                                ! Otherwise
       hpvBuffer = StringToPrint                                                        ! Print Text As Is
    END                                                                                 ! Terminate IF

    cbBuffer = LEN(CLIP(hpvBuffer))                                                     ! Check Length of the Data to be Printed

     hf = OpenFile(lpszFilename,OF_STRUCT,OF_WRITE)                                     ! Open file and obtain file handle
     IF hf = -1                                                                         ! If File does not exist
      hf = OpenFile(lpszFilename,OF_STRUCT,OF_CREATE)                                   ! Create file and obtain file handle
      IF hf = -1 THEN RETURN(OpenError).                                                ! If Error then return OpenError
     END                                                                                ! Terminate IF

    IF SUB(lpszFilename,1,3) <> 'COM' AND |                                             ! If user prints to a file
       SUB(lpszFilename,1,3) <> 'LPT' AND |                                            
       SUB(lpszFilename,1,2) <> '\\'
       IF _llseek(hf,0,2) = -1 THEN RETURN(4).                                          ! Set file pointer to the end of the file (Append lines)
    END                                                                                 ! Terminate IF

    BytesWritten = _lwrite(hf,hpvBuffer,cbBuffer)                                       ! Write to the file
    IF BytesWritten < cbBuffer THEN RETURN(WriteError).                                 ! IF Writing to a device or file is not possible, return Write Error
    IF _lclose(hf) THEN RETURN(CloseError) ELSE RETURN(Succeeded).                      ! If error in closing device or a file, return CloseError otherwise return Succeeded


DeleteFile FUNCTION(STRING FileToDelete)                                                ! DeleteFile Function

    CODE                                                                                ! Function Code Starts Here
    lpszFilename = CLIP(FileToDelete)                                                   ! Put file name in the buffer
    hf = OpenFile(lpszFilename,OF_STRUCT,OF_DELETE)                                     ! and delete it
     IF hf = -1 THEN RETURN(OpenError) ELSE RETURN(Succeeded).                          ! If error, return OpenError otherwise Return Succeeded

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
!These procedures are used to initialize the DLL. It must be called by the main executable when it starts up
MANTRNIS:Init PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>)
MANTRNIS:Init_Called    BYTE,STATIC

  CODE
  IF MANTRNIS:Init_Called
     RETURN
  ELSE
     MANTRNIS:Init_Called = True
  END
  IF ~curGlobalErrors &= NULL
    GlobalErrors &= curGlobalErrors
  END
  IF ~curINIMgr &= NULL
    INIMgr &= curINIMgr
  END
  Access:Clients.SetErrors(GlobalErrors)
  Access:Reminders.SetErrors(GlobalErrors)
  Access:WebClients.SetErrors(GlobalErrors)
  Access:_View_Rates_Client_Journeys.SetErrors(GlobalErrors)
  Access:AdditionalCharges.SetErrors(GlobalErrors)
  Access:Clients_ContainerParkDiscounts.SetErrors(GlobalErrors)
  Access:VehicleMakeModel.SetErrors(GlobalErrors)
  Access:_View_ContainerParkRates_Client.SetErrors(GlobalErrors)
  Access:Addresses.SetErrors(GlobalErrors)
  Access:Application_Section_Usage.SetErrors(GlobalErrors)
  Access:ApplicationSections_Extras.SetErrors(GlobalErrors)
  Access:EmailAddresses.SetErrors(GlobalErrors)
  Access:Add_Suburbs.SetErrors(GlobalErrors)
  Access:LoadTypes2.SetErrors(GlobalErrors)
  Access:TransporterPayments.SetErrors(GlobalErrors)
  Access:Branches.SetErrors(GlobalErrors)
  Access:Add_Cities.SetErrors(GlobalErrors)
  Access:Add_Provinces.SetErrors(GlobalErrors)
  Access:Accountants.SetErrors(GlobalErrors)
  Access:_View_Rates_Clients_LoadTypes.SetErrors(GlobalErrors)
  Access:Transporter.SetErrors(GlobalErrors)
  Access:Container_Parks.SetErrors(GlobalErrors)
  Access:ClientsPaymentsAllocation.SetErrors(GlobalErrors)
  Access:Users.SetErrors(GlobalErrors)
  Access:_FuelCost.SetErrors(GlobalErrors)
  Access:ContainerTypes.SetErrors(GlobalErrors)
  Access:DeliveryItems_Components.SetErrors(GlobalErrors)
  Access:DeliveryItems.SetErrors(GlobalErrors)
  Access:_View_Rates_Client.SetErrors(GlobalErrors)
  Access:DeliveryComposition.SetErrors(GlobalErrors)
  Access:Commodities.SetErrors(GlobalErrors)
  Access:UserGroups.SetErrors(GlobalErrors)
  Access:DeliveriesAdditionalCharges.SetErrors(GlobalErrors)
  Access:Journeys.SetErrors(GlobalErrors)
  Access:ClientsPayments.SetErrors(GlobalErrors)
  Access:Floors.SetErrors(GlobalErrors)
  Access:DeliveryStatuses.SetErrors(GlobalErrors)
  Access:AddressContacts.SetErrors(GlobalErrors)
  Access:ClientsRateTypes.SetErrors(GlobalErrors)
  Access:Vessels.SetErrors(GlobalErrors)
  Access:Delivery_CODAddresses.SetErrors(GlobalErrors)
  Access:ContainerOperators.SetErrors(GlobalErrors)
  Access:Drivers.SetErrors(GlobalErrors)
  Access:RemindersUsers.SetErrors(GlobalErrors)
  Access:_InvoiceItems.SetErrors(GlobalErrors)
  Access:DeliveryLegs.SetErrors(GlobalErrors)
  Access:Shortages_Damages.SetErrors(GlobalErrors)
  Access:_Statements.SetErrors(GlobalErrors)
  Access:ServiceRequirements.SetErrors(GlobalErrors)
  Access:_Invoice.SetErrors(GlobalErrors)
  Access:ManifestLoad.SetErrors(GlobalErrors)
  Access:ManifestLoadDeliveries.SetErrors(GlobalErrors)
  Access:TransporterPaymentsAllocations.SetErrors(GlobalErrors)
  Access:TripSheets.SetErrors(GlobalErrors)
  Access:Manifest.SetErrors(GlobalErrors)
  Access:ApplicationSections.SetErrors(GlobalErrors)
  Access:UsersGroups.SetErrors(GlobalErrors)
  Access:UsersAccesses.SetErrors(GlobalErrors)
  Access:UserGroupAccesses.SetErrors(GlobalErrors)
  Access:UsersAccessesExtra.SetErrors(GlobalErrors)
  Access:UserGroupAccessesExtra.SetErrors(GlobalErrors)
  Access:__RatesContainer.SetErrors(GlobalErrors)
  Access:SalesRepsBands.SetErrors(GlobalErrors)
  Access:Add_Countries.SetErrors(GlobalErrors)
  Access:Deliveries.SetErrors(GlobalErrors)
  Access:WebClientsLoginLog.SetErrors(GlobalErrors)
  Access:TruckTrailer.SetErrors(GlobalErrors)
  Access:__RateUpdates.SetErrors(GlobalErrors)
  Access:TripSheetDeliveries.SetErrors(GlobalErrors)
  Access:_InvoiceTransporter.SetErrors(GlobalErrors)
  Access:_InvoiceJournals.SetErrors(GlobalErrors)
  Access:PackagingTypes.SetErrors(GlobalErrors)
  Access:DeliveryProgress.SetErrors(GlobalErrors)
  Access:VehicleComposition.SetErrors(GlobalErrors)
  Access:SalesReps.SetErrors(GlobalErrors)
  Access:_RemittanceItems.SetErrors(GlobalErrors)
  Access:_Remittance.SetErrors(GlobalErrors)
  Access:_Remittance_Runs.SetErrors(GlobalErrors)
  Access:_InvoiceComposition.SetErrors(GlobalErrors)
  Access:_StatementItems.SetErrors(GlobalErrors)
  Access:_Statement_Runs.SetErrors(GlobalErrors)
  Access:_Statement_Run_Desc.SetErrors(GlobalErrors)
  Access:__RatesTransporter.SetErrors(GlobalErrors)
  Access:__RatesBreakbulk.SetErrors(GlobalErrors)
  Access:__RatesFuelSurcharge.SetErrors(GlobalErrors)
  Access:__RatesContainerPark.SetErrors(GlobalErrors)
  Access:__RatesConsolidated.SetErrors(GlobalErrors)
  Access:__Rates.SetErrors(GlobalErrors)
  Access:__RatesAdditionalCharges.SetErrors(GlobalErrors)
  Access:__RatesFuelCost.SetErrors(GlobalErrors)
  Access:_SQLTemp.SetErrors(GlobalErrors)
  Access:__RatesToll.SetErrors(GlobalErrors)
  Access:NotificationsSetup.SetErrors(GlobalErrors)
  Access:ClientsAlias.SetErrors(GlobalErrors)
  Access:RemindersAlias.SetErrors(GlobalErrors)
  Access:AddressAlias.SetErrors(GlobalErrors)
  Access:LoadTypes2Alias.SetErrors(GlobalErrors)
  Access:TransporterPaymentsAlias.SetErrors(GlobalErrors)
  Access:TransporterAlias.SetErrors(GlobalErrors)
  Access:ClientsPaymentsAllocationAlias.SetErrors(GlobalErrors)
  Access:DeliveryItemAlias.SetErrors(GlobalErrors)
  Access:DeliveryItemAlias2.SetErrors(GlobalErrors)
  Access:JourneysAlias.SetErrors(GlobalErrors)
  Access:ClientsPaymentsAlias.SetErrors(GlobalErrors)
  Access:FloorsAlias.SetErrors(GlobalErrors)
  Access:DriversAlias.SetErrors(GlobalErrors)
  Access:InvoiceItemsAlias.SetErrors(GlobalErrors)
  Access:DeliveryLegsAlias.SetErrors(GlobalErrors)
  Access:InvoiceAlias.SetErrors(GlobalErrors)
  Access:TripSheetsAlias.SetErrors(GlobalErrors)
  Access:DeliveriesAlias.SetErrors(GlobalErrors)
  Access:TruckTrailerAlias.SetErrors(GlobalErrors)
  Access:TripSheetDeliveriesAlias.SetErrors(GlobalErrors)
  Access:InvoiceTransporterAlias.SetErrors(GlobalErrors)
  Access:Statement_Runs_Alias.SetErrors(GlobalErrors)
  TrnISDct:Init(curGlobalErrors, curINIMgr)                ! Initialise dll - (ABC) -
  FunTrnIS:Init(curGlobalErrors, curINIMgr)                ! Initialise dll - (ABC) -
  SelTrnIS:Init(curGlobalErrors, curINIMgr)                ! Initialise dll - (ABC) -
  RptTrnIS:Init(curGlobalErrors, curINIMgr)                ! Initialise dll - (ABC) -
  M2TrnIS:Init(curGlobalErrors, curINIMgr)                 ! Initialise dll - (ABC) -
  M3TRNIS:Init(curGlobalErrors, curINIMgr)                 ! Initialise dll - (ABC) -
  WorTrnis:Init(curGlobalErrors, curINIMgr)                ! Initialise dll - (ABC) -

!This procedure is used to shutdown the DLL. It must be called by the main executable before it closes down

MANTRNIS:Kill PROCEDURE
MANTRNIS:Kill_Called    BYTE,STATIC

  CODE
  IF MANTRNIS:Kill_Called
     RETURN
  ELSE
     MANTRNIS:Kill_Called = True
  END
  TrnISDct:Kill()                                          ! Kill dll - (ABC) -
  FunTrnIS:Kill()                                          ! Kill dll - (ABC) -
  SelTrnIS:Kill()                                          ! Kill dll - (ABC) -
  RptTrnIS:Kill()                                          ! Kill dll - (ABC) -
  M2TrnIS:Kill()                                           ! Kill dll - (ABC) -
  M3TRNIS:Kill()                                           ! Kill dll - (ABC) -
  WorTrnis:Kill()                                          ! Kill dll - (ABC) -
GlobalInsight.Init   Procedure (Long par:Control, Long par:Type=1, Real par:Points=1)
ReturnValue  Long
  code
  ReturnValue = Parent.Init(par:Control,par:Type,par:Points)
  Self.LegendFontHeight = 8
  Self.LegendAngle = 0
  Self.LegendStacked = 0
  Self.SetStyle(1,1)   ! Sets Data Style
  Self.SetStyle(1,2)   ! Sets Axis Style
  Self.HeaderFontHeight = 12
  Self.XNameFontHeight = 8
  Self.XFontHeight = 8
      !GlobalChartXAxisFontColor = -1
  Self.YNameFontHeight = 8
  Self.YFontHeight = 8
  Self.DataFontHeight = 8
  return ReturnValue

  

DLLInitializer.Destruct PROCEDURE

  CODE
                             ! Begin Generated by NetTalk Extension Template
    NetCloseCallBackWindow() ! Tell NetTalk DLL to shutdown it's WinSock Call Back Window
  
    if ~command ('/netnolog') and (command ('/nettalklog') or command ('/nettalklogerrors') or command ('/neterrors') or command ('/netall'))
      NetDebugTrace ('[Nettalk Template] NetTalk Template version 10.11')
      NetDebugTrace ('[Nettalk Template] NetTalk Template using Clarion ' & 10000)
      NetDebugTrace ('[Nettalk Template] Closing Down NetTalk (Object) version ' & NETTALK:VERSION)
    end
                             ! End Generated by Extension Template
     db.kill
  FuzzyMatcher.Kill                                        ! Destroy fuzzy matcher
  LocalINIMgr.Kill                                         ! Kill local managers and assign NULL to global refernces
  INIMgr &= NULL                                           ! It is an error to reference these object after this point
  GlobalErrors &= NULL


