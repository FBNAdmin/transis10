

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS007.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_DeliveriesAdditionalCharges PROCEDURE (p:CID)

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Scr_Fields       GROUP,PRE(L_SF)                       ! 
ACCID                ULONG                                 ! Additional Charge Category ID
ClientName           STRING(35)                            ! 
CID                  ULONG                                 ! Client ID
Today                DATE                                  ! 
ShowAllClients       BYTE                                  ! Show all clients that have this additional charge type
                     END                                   ! 
FDB5::View:FileDrop  VIEW(__RatesAdditionalCharges)
                       PROJECT(CARA:Charge)
                       PROJECT(CARA:ACID)
                       PROJECT(CARA:CID)
                       JOIN(CLI:PKey_CID,CARA:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:FileDrop       QUEUE                            !
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CARA:Charge            LIKE(CARA:Charge)              !List box control field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !List box control field - type derived from field
CARA:ACID              LIKE(CARA:ACID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DELA:Record LIKE(DELA:RECORD),THREAD
QuickWindow          WINDOW('Form Delivery Additional Charge'),AT(,,236,101),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('UpdateDeliveriesAdditionalCharges'),SYSTEM
                       SHEET,AT(4,4,228,78),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Additional Charge:'),AT(9,20),USE(?DELA:AdditionalCharge:Prompt),TRN
                           BUTTON('...'),AT(74,20,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(93,20,136,10),USE(DELA:AdditionalCharge),MSG('Description'),TIP('Description')
                           CHECK(' Show All Clients'),AT(93,36),USE(L_SF:ShowAllClients),MSG('Show all clients tha' & |
  't have this additional charge type'),TIP('Show all clients that have this additional' & |
  ' charge type'),TRN
                           PROMPT('Client Additional Charges:'),AT(9,50),USE(?DELA:AdditionalCharge:Prompt:2),TRN
                           LIST,AT(93,50,136,10),USE(L_SF:ClientName),VSCROLL,DROP(15,180),FORMAT('100L(2)|M~Clien' & |
  't Name~@s35@60R(2)|M~Charge~L@n-15.2@40R(2)|M~CID~L@n_10@'),FROM(Queue:FileDrop)
                           PROMPT('Charge:'),AT(9,66),USE(?DELA:Charge:Prompt),TRN
                           ENTRY(@n-15.2),AT(93,66,68,10),USE(DELA:Charge),DECIMAL(12)
                         END
                       END
                       BUTTON('&OK'),AT(130,84,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(184,84,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,84,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB5                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record Delivery Add. Charges'
  OF InsertRecord
    ActionMessage = 'Delivery Add. Charges Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Delivery Add. Charges Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_DeliveriesAdditionalCharges')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DELA:AdditionalCharge:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_SF:ShowAllClients',L_SF:ShowAllClients)          ! Added by: FileDrop(ABC)
  BIND('L_SF:CID',L_SF:CID)                                ! Added by: FileDrop(ABC)
  BIND('L_SF:Today',L_SF:Today)                            ! Added by: FileDrop(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DELA:Record,History::DELA:Record)
  SELF.AddHistoryField(?DELA:AdditionalCharge,4)
  SELF.AddHistoryField(?DELA:Charge,5)
  SELF.AddUpdateFile(Access:DeliveriesAdditionalCharges)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:AdditionalCharges.SetOpenRelated()
  Relate:AdditionalCharges.Open                            ! File AdditionalCharges used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveriesAdditionalCharges
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?DELA:AdditionalCharge{PROP:ReadOnly} = True
    DISABLE(?L_SF:ClientName)
    ?DELA:Charge{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_DeliveriesAdditionalCharges',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      L_SF:CID    = p:CID
  
      L_SF:Today  = TODAY()
  FDB5.Init(?L_SF:ClientName,Queue:FileDrop.ViewPosition,FDB5::View:FileDrop,Queue:FileDrop,Relate:__RatesAdditionalCharges,ThisWindow)
  FDB5.Q &= Queue:FileDrop
  FDB5.AddSortOrder(CARA:FKey_ACCID)
  FDB5.AddRange(CARA:ACCID,L_SF:ACCID)
  FDB5.SetFilter('(L_SF:ShowAllClients = 1 OR (L_SF:CID = CARA:CID))  AND (CARA:Effective_Date << L_SF:Today)')
  FDB5.AddField(CLI:ClientName,FDB5.Q.CLI:ClientName) !List box control field - type derived from field
  FDB5.AddField(CARA:Charge,FDB5.Q.CARA:Charge) !List box control field - type derived from field
  FDB5.AddField(CLI:CID,FDB5.Q.CLI:CID) !List box control field - type derived from field
  FDB5.AddField(CARA:ACID,FDB5.Q.CARA:ACID) !Primary key field - type derived from field
  FDB5.AddUpdateField(CARA:Charge,DELA:Charge)
  FDB5.AddUpdateField(ACCA:ACCID,L_SF:ACCID)
  ThisWindow.AddItem(FDB5.WindowComponent)
  FDB5.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AdditionalCharges.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_DeliveriesAdditionalCharges',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_AdditionalChargeCategories
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      ACCA:Description = DELA:AdditionalCharge
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        DELA:AdditionalCharge = ACCA:Description
        L_SF:ACCID = ACCA:ACCID
      END
      ThisWindow.Reset(1)
      FDB5.ResetQueue(1)
    OF ?DELA:AdditionalCharge
      IF DELA:AdditionalCharge OR ?DELA:AdditionalCharge{PROP:Req}
        ACCA:Description = DELA:AdditionalCharge
        IF Access:AdditionalCharges.TryFetch(ACCA:Key_Description)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            DELA:AdditionalCharge = ACCA:Description
            L_SF:ACCID = ACCA:ACCID
          ELSE
            CLEAR(L_SF:ACCID)
            SELECT(?DELA:AdditionalCharge)
            CYCLE
          END
        ELSE
          L_SF:ACCID = ACCA:ACCID
        END
      END
      ThisWindow.Reset()
      FDB5.ResetQueue(1)
    OF ?L_SF:ShowAllClients
      FDB5.ResetQueue(1)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_DeliveryAdditionalCharges_Delivery PROCEDURE (p:DID, p:CID)

CurrentTab           STRING(80)                            ! 
LOC:CID              ULONG                                 ! Client ID
BRW1::View:Browse    VIEW(DeliveriesAdditionalCharges)
                       PROJECT(DELA:AdditionalCharge)
                       PROJECT(DELA:Charge)
                       PROJECT(DELA:DAID)
                       PROJECT(DELA:DID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DELA:AdditionalCharge  LIKE(DELA:AdditionalCharge)    !List box control field - type derived from field
DELA:Charge            LIKE(DELA:Charge)              !List box control field - type derived from field
DELA:DAID              LIKE(DELA:DAID)                !Primary key field - type derived from field
DELA:DID               LIKE(DELA:DID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Delivery Additional Charges File'),AT(,,330,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_DeliveryAdditionalCharges_Delivery'),SYSTEM
                       LIST,AT(8,30,314,124),USE(?Browse:1),HVSCROLL,FORMAT('140L(2)|M~Additional Charge~@s35@' & |
  '68R(1)|M~Charge~C(0)@n-15.2@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Deliveries' & |
  'AdditionalCharges file')
                       BUTTON('&Select'),AT(61,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(114,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(167,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(220,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(273,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,322,172),USE(?CurrentTab)
                         TAB('&1) By Delivery'),USE(?Tab:2)
                           BUTTON('Select Deliveries'),AT(9,158,49,14),USE(?SelectDeliveries),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,HIDE,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(277,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_DeliveryAdditionalCharges_Delivery')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Deliveries.Open                          ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:DeliveriesAdditionalCharges,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,DELA:FKey_DID)               ! Add the sort order for DELA:FKey_DID for sort order 1
  BRW1.AddRange(DELA:DID,Relate:DeliveriesAdditionalCharges,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW1.AddField(DELA:AdditionalCharge,BRW1.Q.DELA:AdditionalCharge) ! Field DELA:AdditionalCharge is a hot field or requires assignment from browse
  BRW1.AddField(DELA:Charge,BRW1.Q.DELA:Charge)   ! Field DELA:Charge is a hot field or requires assignment from browse
  BRW1.AddField(DELA:DAID,BRW1.Q.DELA:DAID)       ! Field DELA:DAID is a hot field or requires assignment from browse
  BRW1.AddField(DELA:DID,BRW1.Q.DELA:DID)         ! Field DELA:DID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_DeliveryAdditionalCharges_Delivery',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      IF p:DID ~= DEL:DID
         DEL:DID  = p:DID
         IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
      .  .
  
      LOC:CID = p:CID
  BRW1.AskProcedure = 1                           ! Will call: Update_DeliveriesAdditionalCharges(LOC:CID)
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_DeliveryAdditionalCharges_Delivery',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,2,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_DeliveryAdditionalCharges_Delivery',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_DeliveriesAdditionalCharges(LOC:CID)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectDeliveries
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Deliveries()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_FuelSurcharge PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Locals           GROUP,PRE(LO)                         ! 
Effective_Date       DATE                                  ! Effective from this date
ClientName           STRING(100)                           ! 
                     END                                   ! 
History::FSRA:Record LIKE(FSRA:RECORD),THREAD
QuickWindow          WINDOW('Form Rates Fuel Surcharge'),AT(,,243,115),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_FuelSurcharge'),SYSTEM
                       SHEET,AT(4,4,235,92),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('CID:'),AT(173,36),USE(?FSRA:CID:Prompt),TRN
                           STRING(@n_10),AT(189,36,,10),USE(FSRA:CID),RIGHT(1),TRN
                           PROMPT('Client:'),AT(9,22),USE(?ClientName:Prompt),TRN
                           BUTTON('...'),AT(62,22,12,10),USE(?CallLookup)
                           ENTRY(@s100),AT(78,22,155,10),USE(LO:ClientName),REQ,TIP('Client name')
                           PROMPT('Fuel Surcharge:'),AT(9,64),USE(?FSRA:FuelSurcharge:Prompt),TRN
                           ENTRY(@n-7.2),AT(78,64,67,10),USE(FSRA:FuelSurcharge),RIGHT(1),MSG('Fuel Surcharge'),TIP('Fuel Surcharge')
                           PROMPT('Effective Date:'),AT(9,78),USE(?FSRA:Effective_Date:Prompt),TRN
                           BUTTON('...'),AT(62,78,12,10),USE(?Calendar)
                           SPIN(@d5),AT(78,78,67,10),USE(FSRA:Effective_Date),RIGHT(1),DISABLE,MSG('Effective from' & |
  ' this date'),REQ,TIP('Effective from this date')
                         END
                       END
                       BUTTON('&OK'),AT(138,98,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(190,98,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(2,98,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar5            CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Fuel Surcharge Record'
  OF InsertRecord
    GlobalErrors.Throw(Msg:InsertIllegal)
    RETURN
  OF ChangeRecord
    GlobalErrors.Throw(Msg:UpdateIllegal)
    RETURN
  OF DeleteRecord
    GlobalErrors.Throw(Msg:DeleteIllegal)
    RETURN
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_FuelSurcharge')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?FSRA:CID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(FSRA:Record,History::FSRA:Record)
  SELF.AddHistoryField(?FSRA:CID,2)
  SELF.AddHistoryField(?FSRA:FuelSurcharge,3)
  SELF.AddHistoryField(?FSRA:Effective_Date,6)
  SELF.AddUpdateFile(Access:__RatesFuelSurcharge)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:__RatesFuelSurcharge
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.InsertAction = Insert:None                        ! Inserts not allowed
    SELF.DeleteAction = Delete:None                        ! Deletes not allowed
    SELF.ChangeAction = Change:None                        ! Changes not allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?LO:ClientName{PROP:ReadOnly} = True
    ?FSRA:FuelSurcharge{PROP:ReadOnly} = True
    DISABLE(?Calendar)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_FuelSurcharge',QuickWindow)         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_FuelSurcharge',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Select_RateMod_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = LO:ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LO:ClientName = CLI:ClientName
        FSRA:CID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?LO:ClientName
      IF LO:ClientName OR ?LO:ClientName{PROP:Req}
        CLI:ClientName = LO:ClientName
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LO:ClientName = CLI:ClientName
            FSRA:CID = CLI:CID
          ELSE
            CLEAR(FSRA:CID)
            SELECT(?LO:ClientName)
            CYCLE
          END
        ELSE
          FSRA:CID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',FSRA:Effective_Date)
      IF Calendar5.Response = RequestCompleted THEN
      FSRA:Effective_Date=Calendar5.SelectedDate
      DISPLAY(?FSRA:Effective_Date)
      END
      ThisWindow.Reset(True)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF SELF.Request = InsertRecord
             FSRA:CID         = CLI:CID
          .
      
          CLI:CID             = FSRA:CID
          IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
             LO:ClientName    = CLI:ClientName
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Rates PROCEDURE (p:LTID, p:Disable_Client, p:JID)

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
ClientName           STRING(35)                            ! 
Journey              CSTRING(701)                          ! Description
LOC:ClientRateType   STRING(100)                           ! Load Type
ContainerType        STRING(35)                            ! Type
LOC:LTID             ULONG                                 ! Type ID
LOC:LoadType         STRING(100)                           ! Load Type
LOC:LoadOption       BYTE                                  ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
LOC:ClientRateType2  STRING(100)                           ! Load Type
FDB4::View:FileDrop  VIEW(Clients)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:CID)
                     END
FDB6::View:FileDrop  VIEW(Journeys)
                       PROJECT(JOU:Journey)
                       PROJECT(JOU:Description)
                       PROJECT(JOU:JID)
                     END
FDB7::View:FileDrop  VIEW(ContainerTypes)
                       PROJECT(CTYP:ContainerType)
                       PROJECT(CTYP:CTID)
                     END
FDB9::View:FileDrop  VIEW(ClientsRateTypes)
                       PROJECT(CRT:ClientRateType)
                       PROJECT(CRT:CID)
                       PROJECT(CRT:CRTID)
                     END
FDB11::View:FileDrop VIEW(LoadTypes2)
                       PROJECT(LOAD2:LoadType)
                       PROJECT(LOAD2:LoadOption)
                       PROJECT(LOAD2:LTID)
                     END
Queue:FileDrop       QUEUE                            !
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
JOU:Description        LIKE(JOU:Description)          !List box control field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:2     QUEUE                            !
CTYP:ContainerType     LIKE(CTYP:ContainerType)       !List box control field - type derived from field
CTYP:CTID              LIKE(CTYP:CTID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:3     QUEUE                            !
CRT:ClientRateType     LIKE(CRT:ClientRateType)       !List box control field - type derived from field
CRT:CID                LIKE(CRT:CID)                  !Browse hot field - type derived from field
CRT:CRTID              LIKE(CRT:CRTID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:4     QUEUE                            !
LOAD2:LoadType         LIKE(LOAD2:LoadType)           !List box control field - type derived from field
LOAD2:LoadOption       LIKE(LOAD2:LoadOption)         !Browse hot field - type derived from field
LOAD2:LTID             LIKE(LOAD2:LTID)               !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::RAT:Record  LIKE(RAT:RECORD),THREAD
QuickWindow          WINDOW('Update the Rates File'),AT(,,221,235),FONT('Tahoma',8),DOUBLE,GRAY,IMM,MDI,HLP('Update__Ra' & |
  'tesContainer'),SYSTEM
                       SHEET,AT(4,4,213,212),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('JID:'),AT(82,5),USE(?RAT:JID:Prompt)
                           STRING(@n_10),AT(98,5),USE(RAT:JID),RIGHT(1)
                           GROUP,AT(9,88,204,10),USE(?Group_Cont_Type)
                             PROMPT('Container Type:'),AT(9,88),USE(?RAT:ToMass:Prompt:4),TRN
                             LIST,AT(86,88,126,10),USE(ContainerType),VSCROLL,DROP(15),FORMAT('140L(2)|M~Type~@s35@'),FROM(Queue:FileDrop:2)
                           END
                           PROMPT('Client:'),AT(9,22),USE(?RAT:ToMass:Prompt:2),TRN
                           LIST,AT(86,24,126,9),USE(ClientName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Client Name~@s35@'), |
  FROM(Queue:FileDrop)
                           PROMPT('Load Type:'),AT(9,41),USE(?RAT:ToMass:Prompt:6),TRN
                           LIST,AT(86,40,126,10),USE(LOC:LoadType),VSCROLL,DROP(15),FORMAT('120L(2)|M~Load Type~@s100@'), |
  FROM(Queue:FileDrop:4)
                           PROMPT('Journey:'),AT(9,54),USE(?RAT:ToMass:Prompt:3),TRN
                           BUTTON('...'),AT(69,54,13,10),USE(?Button_Browse_Journey),TIP('Browse Journies')
                           LIST,AT(86,54,126,10),USE(Journey),HVSCROLL,DROP(15,250),FORMAT('150L(2)|M~Journey~@s25' & |
  '5@1020L(2)|M~Description~@s255@'),FROM(Queue:FileDrop:1)
                           PROMPT('Client Rate Type:'),AT(9,68),USE(?RAT:ToMass:Prompt:5),TRN
                           BUTTON('...'),AT(69,68,13,10),USE(?Button_Browse_ClientRateTypes),TIP('Browse Journies')
                           LIST,AT(86,68,126,10),USE(LOC:ClientRateType),HVSCROLL,DROP(15,250),FORMAT('140L(2)|M~C' & |
  'lient Rate Types~@s100@'),FROM(Queue:FileDrop:3)
                           PROMPT('To Mass:'),AT(9,108),USE(?RAT:ToMass:Prompt),TRN
                           BUTTON('Set No Limit'),AT(86,108,,10),USE(?Button_Set_No_Limit)
                           SPIN(@n-12.0),AT(141,108,71,10),USE(RAT:ToMass),RIGHT(2),MSG('Up to this mass in Kgs'),TIP('Up to this' & |
  ' mass in Kgs')
                           PROMPT('Rate Per Kg:'),AT(9,124),USE(?RAT:RatePerKg:Prompt),TRN
                           ENTRY(@n-13.4),AT(141,124,71,10),USE(RAT:RatePerKg),RIGHT(1),MSG('Rate per Kg'),TIP('Rate per Kg')
                           PROMPT('Minimium Charge:'),AT(9,138),USE(?RAT:MinimiumCharge:Prompt),TRN
                           ENTRY(@n-14.2),AT(141,138,71,10),USE(RAT:MinimiumCharge),RIGHT(1)
                           PROMPT('Effective Date:'),AT(9,154),USE(?RAT:Effective_Date:Prompt),TRN
                           BUTTON('...'),AT(126,154,12,10),USE(?Calendar)
                           SPIN(@d5),AT(141,154,71,10),USE(RAT:Effective_Date),RIGHT(1),MSG('Effective from date'),REQ, |
  TIP('Effective from date')
                           PROMPT('Added Date:'),AT(9,172),USE(?RAT:Added_Date:Prompt),TRN
                           ENTRY(@d5),AT(141,172,71,10),USE(RAT:Added_Date),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Added Time:'),AT(9,186),USE(?RAT:Added_Time:Prompt),TRN
                           ENTRY(@t7),AT(141,186,71,10),USE(RAT:Added_Time),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP
                           CHECK(' Ad Hoc'),AT(141,202),USE(RAT:AdHoc),MSG('Ad Hoc rate (not in rate letter)'),TIP('Ad Hoc rat' & |
  'e (not in rate letter)'),TRN
                         END
                       END
                       PROMPT('CRTID:'),AT(148,5),USE(?RAT:LTID:Prompt)
                       STRING(@n_10),AT(173,5),USE(RAT:CRTID),RIGHT(1)
                       BUTTON('&OK'),AT(108,220,,14),USE(?OK),LEFT,ICON('waok.ico'),DEFAULT,FLAT
                       BUTTON('Cancel'),AT(164,220,,14),USE(?Cancel),LEFT,ICON('wacancel.ico'),FLAT
                       BUTTON('Help'),AT(8,198,45,14),USE(?Help),HIDE,STD(STD:Help)
                       PROMPT('RUBID:'),AT(4,223),USE(?RAT:RUBID:Prompt)
                       STRING(@n_10),AT(34,223),USE(RAT:RUBID),RIGHT(1)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
ChangeAction           PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
InsertAction           PROCEDURE(),BYTE,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeUpdate            PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB_Journey          CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:2              !Reference to display queue
                     END

FDB_ClientRateTypes  CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:3              !Reference to display queue
                     END

FDB11                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:4              !Reference to display queue
                     END

Calendar14           CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
New_ClientRateType              ROUTINE
    ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
    IF LOC:LoadOption = 2
       ENABLE(?Group_Cont_Type)
    ELSE
       DISABLE(?Group_Cont_Type)
       CLEAR(RAT:CTID)
       CLEAR(ContainerType)
    .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Container Rates Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Container Rates Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.ChangeAction PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ChangeAction()
      IF ReturnValue = LEVEL:Benign
         Changed_Rates()
      .
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Rates')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RAT:JID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(RAT:Record,History::RAT:Record)
  SELF.AddHistoryField(?RAT:JID,3)
  SELF.AddHistoryField(?RAT:ToMass,6)
  SELF.AddHistoryField(?RAT:RatePerKg,7)
  SELF.AddHistoryField(?RAT:MinimiumCharge,8)
  SELF.AddHistoryField(?RAT:Effective_Date,11)
  SELF.AddHistoryField(?RAT:Added_Date,15)
  SELF.AddHistoryField(?RAT:Added_Time,16)
  SELF.AddHistoryField(?RAT:AdHoc,17)
  SELF.AddHistoryField(?RAT:CRTID,19)
  SELF.AddHistoryField(?RAT:RUBID,18)
  SELF.AddUpdateFile(Access:__Rates)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:__RatesContainer.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ClientsRateTypes.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LoadTypes2.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:__Rates
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
      ! Load the Client, Journey and Load Types locals
      ClientName      = CLI:ClientName
  
      CORA:CID        = CLI:CID
  
  
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Rates',QuickWindow)                 ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      IF SELF.Request = InsertRecord
         CORA:Effective_Date  = GETINI('ContainerRates', 'Last_Used_Eff_Date', , '.\TransIS.INI')
      .
  FDB4.Init(?ClientName,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:Clients,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(CLI:Key_ClientName)
  FDB4.AddField(CLI:ClientName,FDB4.Q.CLI:ClientName) !List box control field - type derived from field
  FDB4.AddField(CLI:CID,FDB4.Q.CLI:CID) !Primary key field - type derived from field
  FDB4.AddUpdateField(CLI:CID,CORA:CID)
  ThisWindow.AddItem(FDB4.WindowComponent)
  FDB4.DefaultFill = 0
  FDB_Journey.Init(?Journey,Queue:FileDrop:1.ViewPosition,FDB6::View:FileDrop,Queue:FileDrop:1,Relate:Journeys,ThisWindow)
  FDB_Journey.Q &= Queue:FileDrop:1
  FDB_Journey.AddSortOrder(JOU:Key_Journey)
  FDB_Journey.AddField(JOU:Journey,FDB_Journey.Q.JOU:Journey) !List box control field - type derived from field
  FDB_Journey.AddField(JOU:Description,FDB_Journey.Q.JOU:Description) !List box control field - type derived from field
  FDB_Journey.AddField(JOU:JID,FDB_Journey.Q.JOU:JID) !Primary key field - type derived from field
  FDB_Journey.AddUpdateField(JOU:JID,RAT:JID)
  ThisWindow.AddItem(FDB_Journey.WindowComponent)
  FDB_Journey.DefaultFill = 0
  FDB7.Init(?ContainerType,Queue:FileDrop:2.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop:2,Relate:ContainerTypes,ThisWindow)
  FDB7.Q &= Queue:FileDrop:2
  FDB7.AddSortOrder(CTYP:Key_Type)
  FDB7.AddField(CTYP:ContainerType,FDB7.Q.CTYP:ContainerType) !List box control field - type derived from field
  FDB7.AddField(CTYP:CTID,FDB7.Q.CTYP:CTID) !Primary key field - type derived from field
  FDB7.AddUpdateField(CTYP:CTID,RAT:CTID)
  ThisWindow.AddItem(FDB7.WindowComponent)
  FDB7.DefaultFill = 0
  FDB_ClientRateTypes.Init(?LOC:ClientRateType,Queue:FileDrop:3.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop:3,Relate:ClientsRateTypes,ThisWindow)
  FDB_ClientRateTypes.Q &= Queue:FileDrop:3
  FDB_ClientRateTypes.AddSortOrder(CRT:FKey_LTID)
  FDB_ClientRateTypes.AppendOrder('CRT:ClientRateType')
  FDB_ClientRateTypes.AddRange(CRT:LTID,LOC:LTID)
  FDB_ClientRateTypes.SetFilter('CRT:CID = CLI:CID')
  FDB_ClientRateTypes.AddField(CRT:ClientRateType,FDB_ClientRateTypes.Q.CRT:ClientRateType) !List box control field - type derived from field
  FDB_ClientRateTypes.AddField(CRT:CID,FDB_ClientRateTypes.Q.CRT:CID) !Browse hot field - type derived from field
  FDB_ClientRateTypes.AddField(CRT:CRTID,FDB_ClientRateTypes.Q.CRT:CRTID) !Primary key field - type derived from field
  FDB_ClientRateTypes.AddUpdateField(CRT:CRTID,RAT:CRTID)
  ThisWindow.AddItem(FDB_ClientRateTypes.WindowComponent)
  FDB_ClientRateTypes.DefaultFill = 0
  FDB11.Init(?LOC:LoadType,Queue:FileDrop:4.ViewPosition,FDB11::View:FileDrop,Queue:FileDrop:4,Relate:LoadTypes2,ThisWindow)
  FDB11.Q &= Queue:FileDrop:4
  FDB11.AddSortOrder(LOAD2:Key_LoadType)
  FDB11.AddField(LOAD2:LoadType,FDB11.Q.LOAD2:LoadType) !List box control field - type derived from field
  FDB11.AddField(LOAD2:LoadOption,FDB11.Q.LOAD2:LoadOption) !Browse hot field - type derived from field
  FDB11.AddField(LOAD2:LTID,FDB11.Q.LOAD2:LTID) !Primary key field - type derived from field
  FDB11.AddUpdateField(LOAD2:LTID,LOC:LTID)
  FDB11.AddUpdateField(LOAD2:LTID,RAT:LTID)
  FDB11.AddUpdateField(LOAD2:LoadOption,LOC:LoadOption)
  ThisWindow.AddItem(FDB11.WindowComponent)
  FDB11.DefaultFill = 0
      IF p:Disable_Client = TRUE
         DISABLE(?ClientName)
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.InsertAction PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.InsertAction()
      IF ReturnValue = LEVEL:Benign
         Changed_Rates()
      .
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Rates',QuickWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeUpdate PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.PrimeUpdate()
      IF SELF.Request = DeleteRecord
         Changed_Rates()
      .
  
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LOC:LoadType
      FDB_ClientRateTypes.ResetQueue(1)
          DO New_ClientRateType
    OF ?Button_Browse_Journey
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Journey()
      ThisWindow.Reset
          Journey     = JOU:Journey
          CORA:JID    = JOU:JID
          DISPLAY
      FDB_Journey.ResetQueue(1)
          JOU:Journey = Journey
          JOU:JID     = CORA:JID
      
          RAT:JID     = JOU:JID
          DISPLAY
    OF ?Button_Browse_ClientRateTypes
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_ClientsRateTypes()
      ThisWindow.Reset
   ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
    IF LOC:LoadOption = 1
       CLEAR(CRT:Record)
       CLEAR(LOC:ClientRateType)

       CLEAR(LOAD2:Record)
       CLEAR(LOC:LTID)
       CLEAR(LOC:LoadType)

       MESSAGE('Cannot set Rates for Container Park Load Types.||See Container Park Discounts', 'Load Type Rates', ICON:Hand)
       CYCLE
    .

    LOC:ClientRateType2 = CRT:ClientRateType
    !LOC:LoadOption      = LOAD2:LoadOption
    RAT:CRTID           = CRT:CRTID
    DISPLAY
      FDB_ClientRateTypes.ResetQueue(1)
    CRT:ClientRateType  = LOC:ClientRateType2
    CRT:CRTID           = RAT:CRTID

      
    DISPLAY
          DO New_ClientRateType
      
    OF ?Button_Set_No_Limit
      ThisWindow.Update()
          RAT:ToMass      = 50000
          DISPLAY(?RAT:ToMass)
    OF ?Calendar
      ThisWindow.Update()
      Calendar14.SelectOnClose = True
      Calendar14.Ask('Select a Date',RAT:Effective_Date)
      IF Calendar14.Response = RequestCompleted THEN
      RAT:Effective_Date=Calendar14.SelectedDate
      DISPLAY(?RAT:Effective_Date)
      END
      ThisWindow.Reset(True)
    OF ?OK
      ThisWindow.Update()
          IF RAT:CID = 0
             SELECT(?ClientName)
             Stop_#   = TRUE
          .
          IF RAT:CRTID = 0
             SELECT(?LOC:ClientRateType)
             Stop_#   = TRUE
          .
          IF RAT:JID = 0
             SELECT(?Journey)
             Stop_#   = TRUE
          .
          IF RAT:LTID = 0
             SELECT(?LOC:LoadType)
             Stop_#   = TRUE
          .
      
      
          IF Stop_# = TRUE
             QuickWindow{PROP:AcceptAll}  = FALSE
             CYCLE
          .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LOC:LoadType
      FDB_ClientRateTypes.ResetQueue(1)
          DO New_ClientRateType
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
    IF SELF.Request = InsertRecord
       RAT:JID              = p:JID
       RAT:LTID             = p:LTID

       SELECT(?RAT:ToMass)
    .

    JOU:JID                 = RAT:JID
    IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
       Journey              = JOU:Journey
    .

    CRT:CRTID               = RAT:CRTID
    IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
       LOC:ClientRateType   = CRT:ClientRateType

       LOC:LTID             = CRT:LTID
       LOAD2:LTID           = CRT:LTID
       IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
          LOC:LoadOption    = LOAD2:LoadOption
          LOC:LoadType      = LOAD2:LoadType
       .

       DO New_ClientRateType
    .

    CTYP:CTID               = RAT:CTID
    IF Access:ContainerTypes.TryFetch(CTYP:PKey_CTID) = LEVEL:Benign
       ContainerType        = CTYP:ContainerType
    .


    LOAD2:LTID              = RAT:LTID
    IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
       LOC:LoadType         = LOAD2:LoadType
    .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          DO New_ClientRateType
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Clients_ContainerParkRates PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Scr_Fields       GROUP,PRE(L_SF)                       ! 
Clientname           STRING(35)                            ! 
Floor                STRING(35)                            ! Floor Name
Journey              STRING(70)                            ! Description
                     END                                   ! 
L_SF:ClientRatePerKg DECIMAL(10,4)                         ! 
BRW9::View:Browse    VIEW(__RatesContainerPark)
                       PROJECT(CPRA:ToMass)
                       PROJECT(CPRA:RatePerKg)
                       PROJECT(CPRA:Effective_Date)
                       PROJECT(CPRA:CPID)
                       PROJECT(CPRA:FID)
                       PROJECT(CPRA:JID)
                       JOIN(JOU:PKey_JID,CPRA:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
CPRA:ToMass            LIKE(CPRA:ToMass)              !List box control field - type derived from field
CPRA:RatePerKg         LIKE(CPRA:RatePerKg)           !List box control field - type derived from field
L_SF:ClientRatePerKg   LIKE(L_SF:ClientRatePerKg)     !List box control field - type derived from local data
CPRA:Effective_Date    LIKE(CPRA:Effective_Date)      !List box control field - type derived from field
CPRA:CPID              LIKE(CPRA:CPID)                !Primary key field - type derived from field
CPRA:FID               LIKE(CPRA:FID)                 !Browse key field - type derived from field
CPRA:JID               LIKE(CPRA:JID)                 !Browse key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::CLI_CP:Record LIKE(CLI_CP:RECORD),THREAD
BRW9::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW9::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW9::PopupChoice    SIGNED                       ! Popup current choice
BRW9::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW9::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Clients Container Park Discounts'),AT(,,240,273),FONT('Tahoma',8,,FONT:regular), |
  DOUBLE,CENTER,GRAY,IMM,MDI,HLP('Update_Clients_ContainerParkRates'),SYSTEM
                       SHEET,AT(4,4,232,116),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Client:'),AT(9,22),USE(?Clientname:Prompt),TRN
                           BUTTON('...'),AT(102,22,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(121,22,111,10),USE(L_SF:Clientname),REQ,TIP('Client name')
                           BUTTON('...'),AT(102,38,12,10),USE(?CallLookup:2)
                           PROMPT('Floor:'),AT(6,38),USE(?Floor:Prompt),TRN
                           ENTRY(@s35),AT(121,38,111,10),USE(L_SF:Floor),MSG('Floor Name'),REQ,TIP('Floor Name')
                           PROMPT(''),AT(10,54,215,20),USE(?Prompt_FloorRates),FONT(,,,FONT:bold,CHARSET:ANSI),CENTER, |
  TRN
                           PROMPT('CID:'),AT(177,6),USE(?CLI_CP:CID:Prompt),TRN
                           STRING(@n_10),AT(193,6,,10),USE(CLI_CP:CID),RIGHT(1),TRN
                           PROMPT('Container Park Rate Discount:'),AT(9,76),USE(?CLI_CP:ContainerParkRateDiscount:Prompt), |
  TRN
                           ENTRY(@n7.2),AT(121,76,60,10),USE(CLI_CP:ContainerParkRateDiscount),DECIMAL(12),MSG('Discount %' & |
  ' that is applied to the container park rates for this client'),TIP('Discount % that ' & |
  'is applied to the container park rates for this client')
                           PROMPT('Container Park Minimium:'),AT(9,90),USE(?CLI_CP:ContainerParkMinimium:Prompt),TRN
                           ENTRY(@n-13.2),AT(121,90,60,10),USE(CLI_CP:ContainerParkMinimium),DECIMAL(12),MSG('Minimium c' & |
  'harge for this client (for this container park)'),TIP('Minimium charge for this clie' & |
  'nt (for this container park)')
                           BUTTON('...'),AT(102,106,12,10),USE(?Calendar)
                           PROMPT('Effective Date:'),AT(9,106),USE(?CLI_CP:Effective_Date:Prompt),TRN
                           SPIN(@d5),AT(121,106,60,10),USE(CLI_CP:Effective_Date),RIGHT(1),MSG('Effective from this date'), |
  REQ,TIP('Effective from this date')
                         END
                       END
                       SHEET,AT(4,126,232,130),USE(?Sheet2),WIZARD
                         TAB('Tab 2'),USE(?Tab2)
                           LIST,AT(9,130,223,122),USE(?List),HVSCROLL,FORMAT('50L(1)|M~Journey~C(0)@s70@38R(1)|M~T' & |
  'o Mass~C(0)@n-12.0@44R(1)|M~Rate Per Kg~C(0)@n-13.4@44R(1)|M~Client Rate~C(0)@n-13.4' & |
  '@60L(1)|M~Effective Date~C(0)@d5@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('&OK'),AT(132,258,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(186,258,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,256,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW9::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
ChangeAction           PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
InsertAction           PROCEDURE(),BYTE,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeUpdate            PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW9                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                      ! Default Locator
Calendar12           CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Floor_Rates             ROUTINE
    ! Check if the Selected Floor has rates setup
    CLEAR(CPRA:Record,-1)
    CPRA:FID            = CLI_CP:FID
    !CPRA:JID            =
    CPRA:Effective_Date = TODAY()
    SET(CPRA:CKey_FID_JID_EffDate_ToMass, CPRA:CKey_FID_JID_EffDate_ToMass)
    LOOP
       IF Access:__RatesContainerPark.TryNext() ~= LEVEL:Benign
          CLEAR(CPRA:Record)
          BREAK
       .
       IF CPRA:FID ~= CLI_CP:FID
          CLEAR(CPRA:Record)
          BREAK
       .

       BREAK
    .
    IF CPRA:CPID = 0
       ?Prompt_FloorRates{PROP:TEXT}   = 'Warning - there are no Rates for this Floor.'
    .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.ChangeAction PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ChangeAction()
    IF ReturnValue = LEVEL:Benign
       Changed_Rates()
    .
  
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Clients_ContainerParkRates')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Clientname:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_SF:ClientRatePerKg',L_SF:ClientRatePerKg) ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CLI_CP:Record,History::CLI_CP:Record)
  SELF.AddHistoryField(?CLI_CP:CID,2)
  SELF.AddHistoryField(?CLI_CP:ContainerParkRateDiscount,4)
  SELF.AddHistoryField(?CLI_CP:ContainerParkMinimium,5)
  SELF.AddHistoryField(?CLI_CP:Effective_Date,8)
  SELF.AddUpdateFile(Access:Clients_ContainerParkDiscounts)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Clients.Open                             ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:Floors.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesContainerPark.UseFile             ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Clients_ContainerParkDiscounts
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:__RatesContainerPark,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?L_SF:Clientname{PROP:ReadOnly} = True
    DISABLE(?CallLookup:2)
    ?L_SF:Floor{PROP:ReadOnly} = True
    ?CLI_CP:ContainerParkRateDiscount{PROP:ReadOnly} = True
    ?CLI_CP:ContainerParkMinimium{PROP:ReadOnly} = True
    DISABLE(?Calendar)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,CPRA:CKey_FID_JID_EffDate_ToMass) ! Add the sort order for CPRA:CKey_FID_JID_EffDate_ToMass for sort order 1
  BRW9.AddRange(CPRA:FID,CLI_CP:FID)              ! Add single value range limit for sort order 1
  BRW9.AddLocator(BRW9::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW9::Sort0:Locator.Init(,CPRA:JID,1,BRW9)      ! Initialize the browse locator using  using key: CPRA:CKey_FID_JID_EffDate_ToMass , CPRA:JID
  BRW9.AddResetField(CLI_CP:ContainerParkRateDiscount) ! Apply the reset field
  BRW9.AddField(JOU:Journey,BRW9.Q.JOU:Journey)   ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW9.AddField(CPRA:ToMass,BRW9.Q.CPRA:ToMass)   ! Field CPRA:ToMass is a hot field or requires assignment from browse
  BRW9.AddField(CPRA:RatePerKg,BRW9.Q.CPRA:RatePerKg) ! Field CPRA:RatePerKg is a hot field or requires assignment from browse
  BRW9.AddField(L_SF:ClientRatePerKg,BRW9.Q.L_SF:ClientRatePerKg) ! Field L_SF:ClientRatePerKg is a hot field or requires assignment from browse
  BRW9.AddField(CPRA:Effective_Date,BRW9.Q.CPRA:Effective_Date) ! Field CPRA:Effective_Date is a hot field or requires assignment from browse
  BRW9.AddField(CPRA:CPID,BRW9.Q.CPRA:CPID)       ! Field CPRA:CPID is a hot field or requires assignment from browse
  BRW9.AddField(CPRA:FID,BRW9.Q.CPRA:FID)         ! Field CPRA:FID is a hot field or requires assignment from browse
  BRW9.AddField(CPRA:JID,BRW9.Q.CPRA:JID)         ! Field CPRA:JID is a hot field or requires assignment from browse
  BRW9.AddField(JOU:JID,BRW9.Q.JOU:JID)           ! Field JOU:JID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_Clients_ContainerParkRates',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW9.AddToolbarTarget(Toolbar)                  ! Browse accepts toolbar control
  BRW9.ToolbarItem.HelpButton = ?Help
  BRW9::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW9::FormatManager.Init('MANTRNIS','Update_Clients_ContainerParkRates',1,?List,9,BRW9::PopupTextExt,Queue:Browse,5,LFM_CFile,LFM_CFile.Record)
  BRW9::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.InsertAction PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.InsertAction()
    IF ReturnValue = LEVEL:Benign
       Changed_Rates()
    .
  
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  ! List Format Manager destructor
  BRW9::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_Clients_ContainerParkRates',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeUpdate PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.PrimeUpdate()
      IF SELF.Request = DeleteRecord
         Changed_Rates()
      .
  
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Clients
      Browse_Floors('')
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = L_SF:Clientname
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SF:Clientname = CLI:ClientName
        CLI_CP:CID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?L_SF:Clientname
      IF L_SF:Clientname OR ?L_SF:Clientname{PROP:Req}
        CLI:ClientName = L_SF:Clientname
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SF:Clientname = CLI:ClientName
            CLI_CP:CID = CLI:CID
          ELSE
            CLEAR(CLI_CP:CID)
            SELECT(?L_SF:Clientname)
            CYCLE
          END
        ELSE
          CLI_CP:CID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update()
      FLO:Floor = L_SF:Floor
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_SF:Floor = FLO:Floor
        CLI_CP:FID = FLO:FID
      END
      ThisWindow.Reset(1)
          DO Floor_Rates
    OF ?L_SF:Floor
      IF L_SF:Floor OR ?L_SF:Floor{PROP:Req}
        FLO:Floor = L_SF:Floor
        IF Access:Floors.TryFetch(FLO:Key_Floor)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            L_SF:Floor = FLO:Floor
            CLI_CP:FID = FLO:FID
          ELSE
            CLEAR(CLI_CP:FID)
            SELECT(?L_SF:Floor)
            CYCLE
          END
        ELSE
          CLI_CP:FID = FLO:FID
        END
      END
      ThisWindow.Reset()
          DO Floor_Rates
    OF ?Calendar
      ThisWindow.Update()
      Calendar12.SelectOnClose = True
      Calendar12.Ask('Select a Date',CLI_CP:Effective_Date)
      IF Calendar12.Response = RequestCompleted THEN
      CLI_CP:Effective_Date=Calendar12.SelectedDate
      DISPLAY(?CLI_CP:Effective_Date)
      END
      ThisWindow.Reset(True)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF SELF.Request = InsertRecord
             CLI_CP:CID   = CLI:CID
             CLI_CP:FID   = FLO:FID
          .
      
          CLI:CID     = CLI_CP:CID
          IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
             L_SF:Clientname  = CLI:ClientName
          .
      
      
          FLO:FID     = CLI_CP:FID
          IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
             L_SF:Floor       = FLO:Floor
          .
      
      
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW9.SetQueueRecord PROCEDURE

  CODE
      L_SF:ClientRatePerKg    = CPRA:RatePerKg - (CPRA:RatePerKg * (CLI_CP:ContainerParkRateDiscount / 100))
  
      ! 10 - (10 * 0.10) = 9
  PARENT.SetQueueRecord
  


BRW9.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW9::LastSortOrder <> NewOrder THEN
     BRW9::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW9::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW9.TakeNewSelection PROCEDURE

  CODE
  IF BRW9::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW9::PopupTextExt = ''
        BRW9::PopupChoiceExec = True
        BRW9::FormatManager.MakePopup(BRW9::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW9::PopupTextExt = '|-|' & CLIP(BRW9::PopupTextExt)
        END
        BRW9::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW9::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW9::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW9::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW9::PopupChoiceOn AND BRW9::PopupChoiceExec THEN
     BRW9::PopupChoiceExec = False
     BRW9::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW9::PopupTextExt)
     IF BRW9::FormatManager.DispatchChoice(BRW9::PopupChoice)
     ELSE
     END
  END

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Statements PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Screen_Vars      GROUP,PRE(L_SV)                       ! 
Client               STRING(35)                            ! 
BranchName           STRING(35)                            ! Branch Name
                     END                                   ! 
BRW2::View:Browse    VIEW(_StatementItems)
                       PROJECT(STAI:InvoiceDate)
                       PROJECT(STAI:IID)
                       PROJECT(STAI:Debit)
                       PROJECT(STAI:Credit)
                       PROJECT(STAI:Amount)
                       PROJECT(STAI:DINo)
                       PROJECT(STAI:STIID)
                       PROJECT(STAI:STID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
STAI:InvoiceDate       LIKE(STAI:InvoiceDate)         !List box control field - type derived from field
STAI:IID               LIKE(STAI:IID)                 !List box control field - type derived from field
STAI:Debit             LIKE(STAI:Debit)               !List box control field - type derived from field
STAI:Credit            LIKE(STAI:Credit)              !List box control field - type derived from field
STAI:Amount            LIKE(STAI:Amount)              !List box control field - type derived from field
STAI:DINo              LIKE(STAI:DINo)                !List box control field - type derived from field
STAI:STIID             LIKE(STAI:STIID)               !List box control field - type derived from field
STAI:STID              LIKE(STAI:STID)                !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::STA:Record  LIKE(STA:RECORD),THREAD
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Statement'),AT(,,430,338),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY,IMM, |
  MDI,HLP('Update_Statements')
                       SHEET,AT(4,4,422,312),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('STID:'),AT(341,6),USE(?STA:STID:Prompt),TRN
                           STRING(@n_10),AT(365,6,,10),USE(STA:STID),RIGHT(1),TRN
                           PROMPT('Statement Date:'),AT(9,20),USE(?STA:StatementDate:Prompt),TRN
                           ENTRY(@d17),AT(66,22,64,10),USE(STA:StatementDate),COLOR(00E9E9E9h),FLAT,MSG('Statement Date'), |
  SKIP,TIP('Statement Date')
                           PROMPT('Time:'),AT(185,22,28,10),USE(?STA:StatementTime:Prompt),RIGHT,TRN
                           ENTRY(@t7),AT(217,22,64,10),USE(STA:StatementTime),COLOR(00E9E9E9h),FLAT,SKIP
                           PROMPT('Client Name:'),AT(9,34),USE(?Client:Prompt),TRN
                           ENTRY(@s35),AT(66,36,142,10),USE(L_SV:Client),COLOR(00E9E9E9h),FLAT,REQ,SKIP,TIP('Client name')
                           PROMPT('CID:'),AT(217,36),USE(?STA:CID:Prompt),TRN
                           STRING(@n_10),AT(237,36,44,10),USE(STA:CID),RIGHT(1),TRN
                           PROMPT('Branch:'),AT(9,46),USE(?BramchName:Prompt),TRN
                           ENTRY(@s35),AT(66,48,142,10),USE(L_SV:BranchName),COLOR(00E9E9E9h),FLAT,SKIP
                           PROMPT('BID:'),AT(217,48),USE(?STA:BID:Prompt),TRN
                           STRING(@n_10),AT(237,48,44,10),USE(STA:BID),RIGHT(1),TRN
                           PROMPT('Current:'),AT(322,22,28,10),USE(?STA:Current:Prompt),LEFT,TRN
                           ENTRY(@n-14.2),AT(357,22,64,10),USE(STA:Current),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Current'), |
  SKIP,TIP('Current')
                           PROMPT('30 Days:'),AT(322,36,28,10),USE(?STA:Days30:Prompt),LEFT,TRN
                           ENTRY(@n-14.2),AT(357,36,64,10),USE(STA:Days30),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('30 Das'), |
  SKIP,TIP('30 Das')
                           PROMPT('60 Days:'),AT(322,64),USE(?STA:Days60:Prompt),TRN
                           ENTRY(@n-14.2),AT(357,64,64,10),USE(STA:Days60),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('60 Days'), |
  SKIP,TIP('60 Days')
                           PROMPT('90 Days:'),AT(322,50),USE(?STA:Days90:Prompt),TRN
                           ENTRY(@n-14.2),AT(357,50,64,10),USE(STA:Days90),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('90 Days'), |
  SKIP,TIP('90 Days')
                           PROMPT('Total:'),AT(322,78),USE(?STA:Total:Prompt),TRN
                           ENTRY(@n-14.2),AT(357,78,64,10),USE(STA:Total),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total'), |
  SKIP,TIP('Total')
                           LINE,AT(7,94,413,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           LIST,AT(9,100,413,193),USE(?Browse:2),HVSCROLL,ALRT(MouseLeft2),FORMAT('48R(2)|M~Invoic' & |
  'e Date~C(0)@d17@44R(2)|M~Invoice No.~C(0)@n_10@54R(1)|M~Debit~C(0)@n-14.2@54R(1)|M~C' & |
  'redit~C(0)@n-14.2@54R(1)|M~Amount~C(0)@n-14.2@38R(2)|M~DI No.~C(0)@n_10@36R(2)|M~STI' & |
  'ID~C(0)@n_10@30R(2)|M~STID~C(0)@n_10@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the _' & |
  'StatementItems file')
                           BUTTON(' Drill down to Invoice'),AT(9,298,,14),USE(?Button_Drill),LEFT,ICON('Gray_D.ico'), |
  FLAT,TIP('Opens Invoice in new thread'),TRN
                           BUTTON('&Insert'),AT(266,298,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(321,298,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(373,298,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON(' Print'),AT(4,320,,14),USE(?Button_Print),LEFT,ICON(ICON:Print1),FLAT
                       BUTTON('&OK'),AT(328,320,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(378,320,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(132,320,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW2::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Statement Record'
  OF InsertRecord
    GlobalErrors.Throw(Msg:InsertIllegal)
    RETURN
  OF ChangeRecord
    GlobalErrors.Throw(Msg:UpdateIllegal)
    RETURN
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Statements')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STA:STID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(STA:Record,History::STA:Record)
  SELF.AddHistoryField(?STA:STID,1)
  SELF.AddHistoryField(?STA:StatementDate,5)
  SELF.AddHistoryField(?STA:StatementTime,6)
  SELF.AddHistoryField(?STA:CID,7)
  SELF.AddHistoryField(?STA:BID,8)
  SELF.AddHistoryField(?STA:Current,12)
  SELF.AddHistoryField(?STA:Days30,11)
  SELF.AddHistoryField(?STA:Days60,10)
  SELF.AddHistoryField(?STA:Days90,9)
  SELF.AddHistoryField(?STA:Total,13)
  SELF.AddUpdateFile(Access:_Statements)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                            ! File Branches used by this procedure, so make sure it's RelationManager is open
  Access:_Statements.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_Statements
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.InsertAction = Insert:None               ! Inserts not allowed
    SELF.ChangeAction = Change:None               ! Changes not allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:_StatementItems,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    ?STA:StatementDate{PROP:ReadOnly} = True
    ?STA:StatementTime{PROP:ReadOnly} = True
    ?L_SV:Client{PROP:ReadOnly} = True
    ?L_SV:BranchName{PROP:ReadOnly} = True
    ?STA:Current{PROP:ReadOnly} = True
    ?STA:Days30{PROP:ReadOnly} = True
    ?STA:Days60{PROP:ReadOnly} = True
    ?STA:Days90{PROP:ReadOnly} = True
    ?STA:Total{PROP:ReadOnly} = True
    DISABLE(?Button_Drill)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    DISABLE(?Button_Print)
  END
    ENABLE(?Button_Drill)
    ENABLE(?Button_Print)
  
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon STAI:STID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,STAI:FKey_STID) ! Add the sort order for STAI:FKey_STID for sort order 1
  BRW2.AddRange(STAI:STID,Relate:_StatementItems,Relate:_Statements) ! Add file relationship range limit for sort order 1
  BRW2.AppendOrder('+STAI:STIID,+STAI:InvoiceDate') ! Append an additional sort order
  BRW2.AddField(STAI:InvoiceDate,BRW2.Q.STAI:InvoiceDate) ! Field STAI:InvoiceDate is a hot field or requires assignment from browse
  BRW2.AddField(STAI:IID,BRW2.Q.STAI:IID)         ! Field STAI:IID is a hot field or requires assignment from browse
  BRW2.AddField(STAI:Debit,BRW2.Q.STAI:Debit)     ! Field STAI:Debit is a hot field or requires assignment from browse
  BRW2.AddField(STAI:Credit,BRW2.Q.STAI:Credit)   ! Field STAI:Credit is a hot field or requires assignment from browse
  BRW2.AddField(STAI:Amount,BRW2.Q.STAI:Amount)   ! Field STAI:Amount is a hot field or requires assignment from browse
  BRW2.AddField(STAI:DINo,BRW2.Q.STAI:DINo)       ! Field STAI:DINo is a hot field or requires assignment from browse
  BRW2.AddField(STAI:STIID,BRW2.Q.STAI:STIID)     ! Field STAI:STIID is a hot field or requires assignment from browse
  BRW2.AddField(STAI:STID,BRW2.Q.STAI:STID)       ! Field STAI:STID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_Statements',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1                           ! Will call: Update_StatementItems
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_Statements',1,?Browse:2,2,BRW2::PopupTextExt,Queue:Browse:2,8,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_Statements',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_StatementItems
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Drill
      BRW2.UpdateViewRecord()
          START(Update_Invoice_h,, STAI:IID, ViewRecord)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Print
      ThisWindow.Update()
          CASE POPUP('Print Statements Continuous|Print Statements Laser')
          OF 1
             ! (p:ID, p:PrintType, p:ID_Options, p:Preview)
             Print_Cont(STA:STID, 3)
          OF 2
             Print_Statement(STA:CID, STA:STID)
          .
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:2
    CASE EVENT()
    OF EVENT:AlertKey
          IF KeyCode() = MouseLeft2
             POST(EVENT:Accepted, ?Button_Drill)
          .
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          CLI:CID             = STA:CID
          IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
             L_SV:Client      = CLI:ClientName
          .
      
          BRA:BID             = STA:BID
          IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
             L_SV:BranchName  = BRA:BranchName
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_WebUsers PROCEDURE 

CurrentTab           STRING(80)                            ! 
BRW1::View:Browse    VIEW(WebClients)
                       PROJECT(WCLI:ClientLogin)
                       PROJECT(WCLI:ClientPassword)
                       PROJECT(WCLI:AccessLevel)
                       PROJECT(WCLI:LoginName)
                       PROJECT(WCLI:WebClientID)
                       PROJECT(WCLI:CID)
                       JOIN(CLI:PKey_CID,WCLI:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
WCLI:ClientLogin       LIKE(WCLI:ClientLogin)         !List box control field - type derived from field
WCLI:ClientPassword    LIKE(WCLI:ClientPassword)      !List box control field - type derived from field
WCLI:AccessLevel       LIKE(WCLI:AccessLevel)         !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
WCLI:LoginName         LIKE(WCLI:LoginName)           !List box control field - type derived from field
WCLI:WebClientID       LIKE(WCLI:WebClientID)         !List box control field - type derived from field
WCLI:CID               LIKE(WCLI:CID)                 !Browse key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Web Clients File'),AT(,,356,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_WebUsers'),SYSTEM
                       LIST,AT(8,30,340,124),USE(?Browse:1),HVSCROLL,FORMAT('60L(2)|M~Client Login~@s20@58L(2)' & |
  '|M~Client Password~@s20@50R(2)|M~Access Level~C(0)@n3@140L(2)|M~Client Name~@s35@80L' & |
  '(2)|M~Login Name~@s50@48R(2)|M~Web Client ID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he WebClients file')
                       BUTTON('&Select'),AT(87,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(140,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(193,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(246,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(299,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,348,172),USE(?CurrentTab)
                         TAB('&1) By Client Login'),USE(?Tab:4)
                         END
                         TAB('&2) By Client'),USE(?Tab:3)
                           BUTTON('Select Clients'),AT(9,158,,14),USE(?SelectClients),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&3) Web Client ID'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(303,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_WebUsers')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Clients.Open                             ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:WebClients,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,WCLI:FKey_CID)               ! Add the sort order for WCLI:FKey_CID for sort order 1
  BRW1.AddRange(WCLI:CID,Relate:WebClients,Relate:Clients) ! Add file relationship range limit for sort order 1
  BRW1.AddSortOrder(,WCLI:PK_WebClients)          ! Add the sort order for WCLI:PK_WebClients for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,WCLI:WebClientID,,BRW1) ! Initialize the browse locator using  using key: WCLI:PK_WebClients , WCLI:WebClientID
  BRW1.AddSortOrder(,WCLI:Key_ClientLogin)        ! Add the sort order for WCLI:Key_ClientLogin for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(,WCLI:ClientLogin,,BRW1) ! Initialize the browse locator using  using key: WCLI:Key_ClientLogin , WCLI:ClientLogin
  BRW1.AddField(WCLI:ClientLogin,BRW1.Q.WCLI:ClientLogin) ! Field WCLI:ClientLogin is a hot field or requires assignment from browse
  BRW1.AddField(WCLI:ClientPassword,BRW1.Q.WCLI:ClientPassword) ! Field WCLI:ClientPassword is a hot field or requires assignment from browse
  BRW1.AddField(WCLI:AccessLevel,BRW1.Q.WCLI:AccessLevel) ! Field WCLI:AccessLevel is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientName,BRW1.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW1.AddField(WCLI:LoginName,BRW1.Q.WCLI:LoginName) ! Field WCLI:LoginName is a hot field or requires assignment from browse
  BRW1.AddField(WCLI:WebClientID,BRW1.Q.WCLI:WebClientID) ! Field WCLI:WebClientID is a hot field or requires assignment from browse
  BRW1.AddField(WCLI:CID,BRW1.Q.WCLI:CID)         ! Field WCLI:CID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:CID,BRW1.Q.CLI:CID)           ! Field CLI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_WebUsers',QuickWindow)     ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                           ! Will call: Update_WebClients
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_WebUsers',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,6,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_WebUsers',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_WebClients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectClients
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_RateMod_Clients()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>2,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>2,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Main_Main            PROCEDURE                             ! Declare Procedure

  CODE
