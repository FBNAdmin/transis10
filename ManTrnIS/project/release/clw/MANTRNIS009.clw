

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS009.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
TripSheet_BulkPOD PROCEDURE (p:TRID)

LOC:Options          GROUP,PRE(L_OG)                       ! 
Branch               STRING(35)                            ! Branch Name
BID                  ULONG                                 ! Branch ID
                     END                                   ! 
LOC:Count            ULONG                                 ! 
LOC:Returned         LONG                                  ! 
LOC:Del_Group        GROUP,PRE(L_DG)                       ! 
Date                 LONG                                  ! 
Time                 LONG                                  ! 
                     END                                   ! 
LOC:Screen_Group     GROUP,PRE(L_SG)                       ! 
TransporterName      STRING(35)                            ! Transporters Name
CompositionName      STRING(35)                            ! 
                     END                                   ! 
LOC:FID              ULONG                                 ! Floor ID
Window               WINDOW('POD Bulk Capture (Trip Sheets Returned)'),AT(,,316,153),FONT('Tahoma',8,,FONT:regular), |
  DOUBLE,CENTER,GRAY,MDI
                       PANEL,AT(7,4,302,113),USE(?Panel1),BEVEL(-1,-1)
                       PROMPT('Transporter:'),AT(12,24),USE(?TransporterName:Prompt)
                       ENTRY(@s35),AT(66,24,236,10),USE(L_SG:TransporterName),COLOR(00E9E9E9h),MSG('Transporters Name'), |
  READONLY,SKIP,TIP('Transporters Name')
                       PROMPT('TRID:'),AT(12,10),USE(?TRI:TRID:Prompt:2)
                       ENTRY(@N_10),AT(66,10,60,10),USE(TRI:TRID),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP,MSG('Tripsheet ID')
                       PROMPT('Composition:'),AT(12,38),USE(?CompositionName:Prompt)
                       ENTRY(@s35),AT(66,38,236,10),USE(L_SG:CompositionName),COLOR(00E9E9E9h),READONLY,SKIP
                       PROMPT('Notes:'),AT(12,52),USE(?TRI:Notes:Prompt)
                       TEXT,AT(66,52,236,30),USE(TRI:Notes),VSCROLL,BOXED,COLOR(00E9E9E9h),MSG('Notes'),READONLY, |
  SKIP,TIP('Notes')
                       PROMPT('Depart Date:'),AT(12,88),USE(?TRI:DepartDate:Prompt)
                       ENTRY(@d6),AT(66,88,60,10),USE(TRI:DepartDate),COLOR(00E9E9E9h),READONLY,SKIP
                       PROMPT('Depart Time:'),AT(188,88),USE(?TRI:DepartTime:Prompt)
                       ENTRY(@t7),AT(242,88,60,10),USE(TRI:DepartTime),COLOR(00E9E9E9h),READONLY,SKIP
                       PROMPT('Returned Date:'),AT(12,102),USE(?TRI:ReturnedDate:Prompt)
                       ENTRY(@d6),AT(66,102,60,10),USE(TRI:ReturnedDate),COLOR(00E9E9E9h),READONLY,SKIP
                       PROMPT('Returned Time:'),AT(188,102),USE(?TRI:ReturnedTime:Prompt)
                       ENTRY(@t7),AT(242,102,60,10),USE(TRI:ReturnedTime),COLOR(00E9E9E9h),READONLY,SKIP
                       PANEL,AT(7,120,239,30),USE(?Panel2)
                       BUTTON('&Cancel'),AT(254,136,,14),USE(?CancelButton),LEFT,ICON('wacancel.ico'),FLAT
                       PROMPT('Currently capturing Trip Sheet items from the above Trip Sheet.'),AT(11,124,231,20), |
  USE(?Prompt9)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
View_TripS              VIEW(TripSheets)
    .

TripSView       ViewManager




View_TripDels   VIEW(TripSheetDeliveriesAlias)
    PROJECT(A_TRDI:TDID, A_TRDI:DIID)
    .

TripDelsView    ViewManager
Loop_Class              CLASS

Construct       PROCEDURE()
Destruct        PROCEDURE()

ProcessL        PROCEDURE()
ProcessTD       PROCEDURE(),BYTE

    .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
!                                                   old
!Loop_TripSheets                         ROUTINE       
!    PUSHBIND()
!    BIND('TRI:State', TRI:State)
!
!    TripSView.Init(View_TripS, Relate:TripSheets)
!    TripSView.AddSortOrder(TRI:FKey_BID)
!    !TripSView.AppendOrder(
!    TripSView.AddRange(TRI:BID, GLO:BranchID)
!    TripSView.SetFilter('TRI:State = 3 OR TRI:State = 2')
!
!    TripSView.Reset()
!    LOOP
!       IF TripSView.Next() ~= LEVEL:Benign
!          BREAK
!       .
!
!       LOC:Count    += 1
!       ! Do the stuff, once complete for all items, set State = 4 - finalised
!
!       ! Popup screen asking for Returned Dates
!       LOC:Returned  = TripSheet_BulkPOD_Win(TRI:TRID)
!       IF LOC:Returned < 0
!          BREAK                                             ! User cancelled
!       .
!
!       L_DG:Date    = TRI:ReturnedDate
!       L_DG:Time    = TRI:ReturnedTime
!
!    db.debugout('TRI:TRID: ' & TRI:TRID)
!
!       ! Loop through the Deliveries
!       TripDelsView.Init(View_TripDels, Relate:TripSheetDeliveriesAlias)
!       TripDelsView.AddSortOrder(A_TRDI:FKey_TRID)
!       TripDelsView.AppendOrder(A_TRDI:DIID)
!       TripDelsView.AddRange(A_TRDI:TRID, TRI:TRID)
!       !TripDelsView.SetFilter()
!       TripDelsView.Reset()
!       LOOP
!    db.debugout('A_TRDI:TDID: ' & A_TRDI:TDID)
!
!          IF TripDelsView.Next() ~= LEVEL:Benign
!             BREAK
!          .
!
!    db.debugout('2. A_TRDI:TDID: ' & A_TRDI:TDID)
!
!          TRDI:TDID     = A_TRDI:TDID
!          IF Access:TripSheetDeliveries.TryFetch(TRDI:PKey_TDID) ~= LEVEL:Benign
!             MESSAGE('Could not fetch the Trip Sheet Deliverey: ' & A_TRDI:TDID, 'Trip Sheet POD', ICON:Hand)
!             CYCLE
!          .
!
!          GlobalRequest     = ChangeRecord
!          Update_TripSheetDeliveries(LOC:Del_Group)
!          IF GlobalResponse = RequestCancelled
!             ! Do what?
!             CASE MESSAGE('Would you like to stop updating the Trip Sheet Deliveries?', 'Trip Sheet POD', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
!             OF BUTTON:Yes
!                BREAK
!       .  .  .
!
!    db.debugout('Kill dels view')
!       TripDelsView.Kill()
!    .
!
!    db.debugout('Kill trip view')
!    TripSView.Kill()
!
!    db.debugout('pop bind')
!
!    POPBIND()
!
!    db.debugout('exit')
!
!    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('TripSheet_BulkPOD')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
      BIND('TRI:State', TRI:State)
      BIND('TRI:TRID', TRI:TRID)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:TripSheetDeliveriesAlias.Open                     ! File TripSheetDeliveriesAlias used by this procedure, so make sure it's RelationManager is open
  Relate:TripSheetsAlias.Open                              ! File TripSheetsAlias used by this procedure, so make sure it's RelationManager is open
  Access:TripSheets.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleComposition.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TripSheetDeliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItemAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
      BRA:BID         = GLO:BranchID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         L_OG:Branch  = BRA:BranchName
         L_OG:BID     = GLO:BranchID
      .
  Do DefineListboxStyle
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      UNBIND('TRI:TRID')
      UNBIND('TRI:State')
  
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:TripSheetDeliveriesAlias.Close
    Relate:TripSheetsAlias.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CancelButton
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          Loop_Class.ProcessL()
      
          IF LOC:Count <= 0
             MESSAGE('There are no Trip Sheets to finalise.', 'Trip Sheet POD Capture', ICON:Asterisk)
          .
      
          POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Loop_Class.ProcessL                 PROCEDURE()
L_Cancel_Item   BYTE
LOC:TRID        LIKE(TRI:TRID)

    CODE
    TripSView.Reset()
    LOOP
       IF L_Cancel_Item = TRUE
          CASE MESSAGE('Trip Sheet item update cancelled.||Reset this Trip Sheet status to Transferred?  (from Finalised)', 'Trip Sheet POD', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
          OF BUTTON:Yes
             A_TRI:TRID     = TRI:TRID
             IF Access:TripSheetsAlias.TryFetch(A_TRI:PKey_TID) = LEVEL:Benign
                A_TRI:State = 3
                IF Access:TripSheetsAlias.TryUpdate() = LEVEL:Benign
       .  .  .  .

       IF TripSView.Next() ~= LEVEL:Benign
          BREAK
       .
       LOC:TRID     = TRI:TRID

       IF L_Cancel_Item = TRUE
          CASE MESSAGE('Trip Sheet item update cancelled.||Cancel processing of the next Trip Sheet?', 'Trip Sheet POD', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
          OF BUTTON:Yes
             BREAK                                          ! User cancelled
       .  .

       LOC:Count   += 1
       ! Do the stuff, once complete for all items, set State = 4 - finalised (at the moment this happens before complete???

       VCO:VCID     = TRI:VCID
       IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
          L_SG:CompositionName  = VCO:CompositionName
       .

       !TRA:TID      = VCO:TID
       TRA:TID      = TRI:TID
       IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
          L_SG:TransporterName  = TRA:TransporterName
       .
       DISPLAY

       ! Popup screen asking for Returned Dates - TRI:State is set in this function TripSheet_BulkPOD_Win
       LOC:Returned = TripSheet_BulkPOD_Win(TRI:TRID)
       IF LOC:Returned < 0
          IF p:TRID ~= 0                                       ! If only one then no question
             BREAK
          ELSE
             CASE MESSAGE('Continue with next Trip Sheet?', 'Trip Sheet POD', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No)
             OF BUTTON:No
                BREAK                                          ! User cancelled
             OF BUTTON:Yes
                CYCLE                                          ! Get next Trip Sheet
       .  .  .

       TRI:TRID         = LOC:TRID
       IF Access:TripSheets.TryFetch(TRI:PKey_TID) = LEVEL:Benign
       .
       TRI:State        = 3                                 ! Reset to not finalised for entry purposes

       CLEAR(LOC:Screen_Group)
       L_Cancel_Item    = FALSE

       L_DG:Date        = TRI:ReturnedDate
       L_DG:Time        = TRI:ReturnedTime
       LOC:FID          = TRI:FID

       L_Cancel_Item    = SELF.ProcessTD()
    .
    RETURN



Loop_Class.ProcessTD                 PROCEDURE()
L_Cancel_Item   BYTE
LOC:DIID        LIKE(A_TRDI:DIID)

    CODE
    ! Loop through the Deliveries
    !TripDelsView.SetFilter()

    TripDelsView.Reset()
    LOOP UNTIL L_Cancel_Item = TRUE
       IF TripDelsView.Next() ~= LEVEL:Benign
          BREAK
       .

       LOC:DIID      = A_TRDI:DIID

       TRDI:TDID     = A_TRDI:TDID
       IF Access:TripSheetDeliveries.TryFetch(TRDI:PKey_TDID) ~= LEVEL:Benign
          MESSAGE('Could not fetch the Trip Sheet Delivery: ' & A_TRDI:TDID, 'Trip Sheet POD', ICON:Hand)
          CYCLE
       .

       GlobalRequest     = ChangeRecord
       Update_TripSheetDeliveries(LOC:Del_Group)
       IF GlobalResponse = RequestCancelled
          ! Do what?
          CASE MESSAGE('Would you like to stop updating the Trip Sheet Deliveries?', 'Trip Sheet POD', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
          OF BUTTON:Yes
             L_Cancel_Item  = TRUE
          .
       ELSE
          ! Change the Floor that the entire DI is on....
          ! Or should this be done only when all have been moved?  In theory
          ! they could go to different floors too...
          A_DELI:DIID       = LOC:DIID
          CLEAR(A_DELI:Record)
          IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) = LEVEL:Benign
             DEL:DID        = A_DELI:DID
             IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
                DEL:FID     = LOC:FID
                IF Access:Deliveries.TryUpdate() = LEVEL:Benign
    .  .  .  .  .
    RETURN(L_Cancel_Item)
Loop_Class.Construct                PROCEDURE()
    CODE
    TripSView.Init(View_TripS, Relate:TripSheets)
    TripSView.AddSortOrder(TRI:FKey_BID)
    !TripSView.AppendOrder(

    IF p:TRID = 0
       BRA:BID = GLO:BranchID
       IF Access:Branches.TryFetch(BRA:PKey_BID) = Level:Benign.
            
       CASE MESSAGE('Would you like to process Trip Sheets for your branch, ' & CLIP(BRA:BranchName) & ' (BID: ' & GLO:BranchID & '), or all Branches?', 'Branch', ICON:Question, 'BID: ' & GLO:BranchID &'|All', 1)
       OF 1
          TripSView.AddRange(TRI:BID, GLO:BranchID)
    .  .

    TripSView.SetFilter('TRI:State = 3 OR TRI:State = 2', '1')          ! 0 Loading, 1 Loaded, 2 On Route, 3 Transferred, 4 Finalised

    IF p:TRID ~= 0
       TripSView.SetFilter('TRI:TRID = ' & p:TRID, '2')
    .


    TripDelsView.Init(View_TripDels, Relate:TripSheetDeliveriesAlias)
    TripDelsView.AddSortOrder(A_TRDI:FKey_TRID)
    TripDelsView.AppendOrder('A_TRDI:TDID')            !A_TRDI:DIID')
    TripDelsView.AddRange(A_TRDI:TRID, TRI:TRID)
    RETURN


Loop_Class.Destruct                 PROCEDURE()
    CODE
    TripDelsView.Kill()
    TripSView.Kill()
    RETURN
!                   old
!Loop_Class.ProcessL        PROCEDURE()
!L_Cancel_Item   BYTE
!
!    CODE
!    TripSView.Init(View_TripS, Relate:TripSheets)
!    TripSView.AddSortOrder(TRI:FKey_BID)
!    !TripSView.AppendOrder(
!    TripSView.AddRange(TRI:BID, GLO:BranchID)
!    TripSView.SetFilter('TRI:State = 3 OR TRI:State = 2')
!
!       TripDelsView.Init(View_TripDels, Relate:TripSheetDeliveriesAlias)
!       TripDelsView.AddSortOrder(A_TRDI:FKey_TRID)
!       TripDelsView.AppendOrder('A_TRDI:DIID')
!       TripDelsView.AddRange(A_TRDI:TRID, TRI:TRID)
!
!    TripSView.Reset()
!    LOOP
!       IF L_Cancel_Item = TRUE
!          CASE MESSAGE('Trip Sheet item update cancelled.||Reset this Trip Sheet status to Transferred?  (from Finalised)', 'Trip Sheet POD', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
!          OF BUTTON:Yes
!             A_TRI:TRID     = TRI:TRID
!             IF Access:TripSheetsAlias.TryFetch(A_TRI:PKey_TID) = LEVEL:Benign
!                A_TRI:State = 3
!                IF Access:TripSheetsAlias.TryUpdate() = LEVEL:Benign
!       .  .  .  .
!
!       IF TripSView.Next() ~= LEVEL:Benign
!          BREAK
!       .
!
!       IF L_Cancel_Item = TRUE
!          CASE MESSAGE('Trip Sheet item update cancelled.||Cancel processing of the next Trip Sheet?', 'Trip Sheet POD', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
!          OF BUTTON:Yes
!             BREAK                                          ! User cancelled
!       .  .
!
!       LOC:Count    += 1
!       ! Do the stuff, once complete for all items, set State = 4 - finalised (at the moment this happens before complete???
!
!       VCO:VCID = TRI:VCID
!       IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
!          L_SG:CompositionName  = VCO:CompositionName
!       .
!       TRA:TID  = VCO:TID
!       IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
!          L_SG:TransporterName  = TRA:TransporterName
!       .
!       DISPLAY
!
!       ! Popup screen asking for Returned Dates
!       LOC:Returned  = TripSheet_BulkPOD_Win(TRI:TRID)
!       IF LOC:Returned < 0
!          CASE MESSAGE('Continue with next Trip Sheet?', 'Trip Sheet POD', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No)
!          OF BUTTON:No
!             BREAK                                          ! User cancelled
!          OF BUTTON:Yes
!             CYCLE                                          ! Get next Trip Sheet
!       .  .
!
!       IF Access:TripSheets.TryFetch(TRI:PKey_TID) = LEVEL:Benign
!       .
!       TRI:State    = 3                                     ! Reset to not finalised for entry purposes
!
!       CLEAR(LOC:Screen_Group)
!       L_Cancel_Item    = FALSE
!
!       L_DG:Date    = TRI:ReturnedDate
!       L_DG:Time    = TRI:ReturnedTime
!       LOC:FID      = TRI:FID
!
!       ! Loop through the Deliveries
!       !TripDelsView.SetFilter()
!       TripDelsView.Reset()
!       LOOP UNTIL L_Cancel_Item = TRUE
!          IF TripDelsView.Next() ~= LEVEL:Benign
!             BREAK
!          .
!
!          TRDI:TDID     = A_TRDI:TDID
!          IF Access:TripSheetDeliveries.TryFetch(TRDI:PKey_TDID) ~= LEVEL:Benign
!             MESSAGE('Could not fetch the Trip Sheet Delivery: ' & A_TRDI:TDID, 'Trip Sheet POD', ICON:Hand)
!             CYCLE
!          .
!
!          GlobalRequest     = ChangeRecord
!          Update_TripSheetDeliveries(LOC:Del_Group)
!          IF GlobalResponse = RequestCancelled
!             ! Do what?
!             CASE MESSAGE('Would you like to stop updating the Trip Sheet Deliveries?', 'Trip Sheet POD', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
!             OF BUTTON:Yes
!                L_Cancel_Item   = TRUE
!             .
!          ELSE
!             ! Change the Floor that the entire DI is on....
!             A_DELI:DIID    = A_TRDI:DIID
!             CLEAR(A_DELI:Record)
!             IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) = LEVEL:Benign
!                DEL:DID     = A_DELI:DID
!                IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
!                   DEL:FID = LOC:FID
!                   IF Access:Deliveries.TryUpdate() = LEVEL:Benign
!          .  .  .  .
!    .  .
!
!       TripDelsView.Kill()
!    TripSView.Kill()
!    RETURN
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Transporter_Payments PROCEDURE 

CurrentTab           STRING(80)                            ! 
BRW1::View:Browse    VIEW(TransporterPayments)
                       PROJECT(TRAP:TPID)
                       PROJECT(TRAP:DateMade)
                       PROJECT(TRAP:Amount)
                       PROJECT(TRAP:Notes)
                       PROJECT(TRAP:DateCaptured)
                       PROJECT(TRAP:TimeCaptured)
                       PROJECT(TRAP:Type)
                       PROJECT(TRAP:TID)
                       PROJECT(TRAP:TPID_Reversal)
                       JOIN(TRA:PKey_TID,TRAP:TID)
                         PROJECT(TRA:TransporterName)
                         PROJECT(TRA:TID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
TRAP:TPID              LIKE(TRAP:TPID)                !List box control field - type derived from field
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
TRAP:DateMade          LIKE(TRAP:DateMade)            !List box control field - type derived from field
TRAP:Amount            LIKE(TRAP:Amount)              !List box control field - type derived from field
TRAP:Notes             LIKE(TRAP:Notes)               !List box control field - type derived from field
TRAP:DateCaptured      LIKE(TRAP:DateCaptured)        !List box control field - type derived from field
TRAP:TimeCaptured      LIKE(TRAP:TimeCaptured)        !List box control field - type derived from field
TRAP:Type              LIKE(TRAP:Type)                !List box control field - type derived from field
TRAP:TID               LIKE(TRAP:TID)                 !List box control field - type derived from field
TRAP:TPID_Reversal     LIKE(TRAP:TPID_Reversal)       !List box control field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Transporter Payments File'),AT(,,419,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MAX,MDI,HLP('Transporter_Payments'),SYSTEM
                       LIST,AT(8,30,402,124),USE(?Browse:1),HVSCROLL,FORMAT('30R(2)|FM~TPID~C(0)@n_10@80L(2)|F' & |
  'M~Transporter~C(0)@s35@40R(2)|M~Date Made~C(0)@d5@54R(1)|M~Amount~C(0)@n-14.2@100L(2' & |
  ')|M~Notes~@s255@[38R(2)|M~Date~C(0)@d5b@36R(2)|M~Time~C(0)@t7@]|M~Captured~20R(2)|M~' & |
  'Type~C(0)@n3@30R(2)|M~TID~C(0)@n_10@40R(2)|M~TPID Reversal~C(0)@n_10@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the TransporterPayments file')
                       BUTTON('&Select'),AT(150,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(204,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(256,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(310,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(362,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,412,172),USE(?CurrentTab)
                         TAB('&1) By Transporter Payment ID'),USE(?Tab:2)
                         END
                         TAB('&2) By Transporter'),USE(?Tab:3)
                           BUTTON('Select Transporter'),AT(9,158,,14),USE(?SelectTransporter),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(367,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Transporter_Payments')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                         ! File Transporter used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TransporterPayments,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,TRAP:FKey_TID)               ! Add the sort order for TRAP:FKey_TID for sort order 1
  BRW1.AddRange(TRAP:TID,Relate:TransporterPayments,Relate:Transporter) ! Add file relationship range limit for sort order 1
  BRW1.AddSortOrder(,TRAP:PKey_TPID)              ! Add the sort order for TRAP:PKey_TPID for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,TRAP:TPID,1,BRW1)     ! Initialize the browse locator using  using key: TRAP:PKey_TPID , TRAP:TPID
  BRW1.AddField(TRAP:TPID,BRW1.Q.TRAP:TPID)       ! Field TRAP:TPID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:TransporterName,BRW1.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW1.AddField(TRAP:DateMade,BRW1.Q.TRAP:DateMade) ! Field TRAP:DateMade is a hot field or requires assignment from browse
  BRW1.AddField(TRAP:Amount,BRW1.Q.TRAP:Amount)   ! Field TRAP:Amount is a hot field or requires assignment from browse
  BRW1.AddField(TRAP:Notes,BRW1.Q.TRAP:Notes)     ! Field TRAP:Notes is a hot field or requires assignment from browse
  BRW1.AddField(TRAP:DateCaptured,BRW1.Q.TRAP:DateCaptured) ! Field TRAP:DateCaptured is a hot field or requires assignment from browse
  BRW1.AddField(TRAP:TimeCaptured,BRW1.Q.TRAP:TimeCaptured) ! Field TRAP:TimeCaptured is a hot field or requires assignment from browse
  BRW1.AddField(TRAP:Type,BRW1.Q.TRAP:Type)       ! Field TRAP:Type is a hot field or requires assignment from browse
  BRW1.AddField(TRAP:TID,BRW1.Q.TRAP:TID)         ! Field TRAP:TID is a hot field or requires assignment from browse
  BRW1.AddField(TRAP:TPID_Reversal,BRW1.Q.TRAP:TPID_Reversal) ! Field TRAP:TPID_Reversal is a hot field or requires assignment from browse
  BRW1.AddField(TRA:TID,BRW1.Q.TRA:TID)           ! Field TRA:TID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_Transporter_Payments',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                           ! Will call: Update_TransporterPayments
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_Transporter_Payments',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,10,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Transporter.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_Transporter_Payments',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_TransporterPayments
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectTransporter
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Transporter()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>1,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>1,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Statement_Runs PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
BRW2::View:Browse    VIEW(_Statements)
                       PROJECT(STA:STID)
                       PROJECT(STA:StatementDate)
                       PROJECT(STA:StatementTime)
                       PROJECT(STA:BID)
                       PROJECT(STA:Days90)
                       PROJECT(STA:Days60)
                       PROJECT(STA:Days30)
                       PROJECT(STA:Current)
                       PROJECT(STA:Total)
                       PROJECT(STA:Paid)
                       PROJECT(STA:Paid_STID)
                       PROJECT(STA:CID)
                       PROJECT(STA:STRID)
                       JOIN(CLI:PKey_CID,STA:CID)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
STA:STID               LIKE(STA:STID)                 !List box control field - type derived from field
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
STA:StatementDate      LIKE(STA:StatementDate)        !List box control field - type derived from field
STA:StatementTime      LIKE(STA:StatementTime)        !List box control field - type derived from field
STA:BID                LIKE(STA:BID)                  !List box control field - type derived from field
STA:Days90             LIKE(STA:Days90)               !List box control field - type derived from field
STA:Days60             LIKE(STA:Days60)               !List box control field - type derived from field
STA:Days30             LIKE(STA:Days30)               !List box control field - type derived from field
STA:Current            LIKE(STA:Current)              !List box control field - type derived from field
STA:Total              LIKE(STA:Total)                !List box control field - type derived from field
STA:Paid               LIKE(STA:Paid)                 !List box control field - type derived from field
STA:Paid_STID          LIKE(STA:Paid_STID)            !List box control field - type derived from field
STA:CID                LIKE(STA:CID)                  !List box control field - type derived from field
STA:STRID              LIKE(STA:STRID)                !List box control field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::STAR:Record LIKE(STAR:RECORD),THREAD
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Statement Runs'),AT(,,473,325),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('Update_Statement_Runs'),SYSTEM
                       SHEET,AT(4,4,466,302),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('STRID:'),AT(9,20),USE(?STAR:STRID:Prompt),TRN
                           STRING(@n_10),AT(77,20,104,10),USE(STAR:STRID),TRN
                           PROMPT('Run Description:'),AT(9,34),USE(?STAR:RunDescription:Prompt),TRN
                           ENTRY(@s35),AT(77,34,144,10),USE(STAR:RunDescription),MSG('Run Description'),TIP('Run Description')
                           PROMPT('Run Date:'),AT(9,48),USE(?STAR:RunDate:Prompt),TRN
                           ENTRY(@d17),AT(77,48,104,10),USE(STAR:RunDate),MSG('Statement Run Date'),TIP('Statement Run Date')
                           PROMPT('Run Time:'),AT(9,62),USE(?STAR:RunTime:Prompt),TRN
                           ENTRY(@t7),AT(77,62,104,10),USE(STAR:RunTime)
                           PROMPT('Entry Date:'),AT(9,76),USE(?STAR:EntryDate:Prompt),TRN
                           ENTRY(@d17),AT(77,76,104,10),USE(STAR:EntryDate),MSG('Run done at'),TIP('Run done at')
                           PROMPT('Entry Time:'),AT(9,90),USE(?STAR:EntryTime:Prompt),TRN
                           ENTRY(@t7),AT(77,90,104,10),USE(STAR:EntryTime)
                           CHECK(' Complete'),AT(77,104,70),USE(STAR:Complete),MSG('Complete'),TIP('Complete'),TRN
                         END
                         TAB('&2) Statements'),USE(?Tab:2)
                           LIST,AT(9,20,457,265),USE(?Browse:2),HVSCROLL,FORMAT('40R(2)|M~STID~C(0)@n_10@40R(2)|M~' & |
  'Client No.~C(0)@n_10b@80L(2)|M~Client Name~C(0)@s100@[50R(2)|M~Date~C(0)@d17@60R(2)|' & |
  'M~Time~C(0)@t8@](87)|M~Statement~40R(2)|M~BID~C(0)@n_10@50R(1)|M~90 Days~C(0)@n-14.2' & |
  '@50R(1)|M~60 Days~C(0)@n-14.2@50R(1)|M~30 Days~C(0)@n-14.2@50R(1)|M~Current~C(0)@n-1' & |
  '4.2@50R(1)|M~Total~C(0)@n-14.2@50R(1)|M~Paid~C(0)@n-14.2@40R(1)|M~Paid STID~C(0)@n_1' & |
  '0@40R(2)|M~CID~C(0)@n_10@40R(2)|M~STR ID~C(0)@n_10@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he _Statements file')
                           BUTTON('&Print'),AT(9,288,,14),USE(?Button_Print),LEFT,ICON(ICON:Print1),FLAT,TIP('Print sele' & |
  'cted Statement')
                           BUTTON('&View'),AT(255,288,53,14),USE(?View),LEFT,ICON('WAVIEW.ICO'),FLAT
                           BUTTON('&Insert'),AT(311,288,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(363,288,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(417,288,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(366,308,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(420,308,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(2,308,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW2::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Statement Run Record'
  OF InsertRecord
    GlobalErrors.Throw(Msg:InsertIllegal)
    RETURN
  OF ChangeRecord
    GlobalErrors.Throw(Msg:UpdateIllegal)
    RETURN
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Statement_Runs')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STAR:STRID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(STAR:Record,History::STAR:Record)
  SELF.AddHistoryField(?STAR:STRID,1)
  SELF.AddHistoryField(?STAR:RunDescription,2)
  SELF.AddHistoryField(?STAR:RunDate,5)
  SELF.AddHistoryField(?STAR:RunTime,6)
  SELF.AddHistoryField(?STAR:EntryDate,9)
  SELF.AddHistoryField(?STAR:EntryTime,10)
  SELF.AddHistoryField(?STAR:Complete,11)
  SELF.AddUpdateFile(Access:_Statement_Runs)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:_Statement_Runs.SetOpenRelated()
  Relate:_Statement_Runs.Open                     ! File _Statement_Runs used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_Statement_Runs
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.InsertAction = Insert:None               ! Inserts not allowed
    SELF.ChangeAction = Change:None               ! Changes not allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:_Statements,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    ?STAR:RunDescription{PROP:ReadOnly} = True
    ?STAR:RunDate{PROP:ReadOnly} = True
    ?STAR:RunTime{PROP:ReadOnly} = True
    ?STAR:EntryDate{PROP:ReadOnly} = True
    ?STAR:EntryTime{PROP:ReadOnly} = True
    DISABLE(?Button_Print)
    DISABLE(?View)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
    ENABLE(?View)
    ENABLE(?Button_Print)
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,STA:FKey_STRID)              ! Add the sort order for STA:FKey_STRID for sort order 1
  BRW2.AddRange(STA:STRID,Relate:_Statements,Relate:_Statement_Runs) ! Add file relationship range limit for sort order 1
  BRW2.AppendOrder('+CLI:ClientName,+STA:STID')   ! Append an additional sort order
  BRW2.AddField(STA:STID,BRW2.Q.STA:STID)         ! Field STA:STID is a hot field or requires assignment from browse
  BRW2.AddField(CLI:ClientNo,BRW2.Q.CLI:ClientNo) ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW2.AddField(CLI:ClientName,BRW2.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW2.AddField(STA:StatementDate,BRW2.Q.STA:StatementDate) ! Field STA:StatementDate is a hot field or requires assignment from browse
  BRW2.AddField(STA:StatementTime,BRW2.Q.STA:StatementTime) ! Field STA:StatementTime is a hot field or requires assignment from browse
  BRW2.AddField(STA:BID,BRW2.Q.STA:BID)           ! Field STA:BID is a hot field or requires assignment from browse
  BRW2.AddField(STA:Days90,BRW2.Q.STA:Days90)     ! Field STA:Days90 is a hot field or requires assignment from browse
  BRW2.AddField(STA:Days60,BRW2.Q.STA:Days60)     ! Field STA:Days60 is a hot field or requires assignment from browse
  BRW2.AddField(STA:Days30,BRW2.Q.STA:Days30)     ! Field STA:Days30 is a hot field or requires assignment from browse
  BRW2.AddField(STA:Current,BRW2.Q.STA:Current)   ! Field STA:Current is a hot field or requires assignment from browse
  BRW2.AddField(STA:Total,BRW2.Q.STA:Total)       ! Field STA:Total is a hot field or requires assignment from browse
  BRW2.AddField(STA:Paid,BRW2.Q.STA:Paid)         ! Field STA:Paid is a hot field or requires assignment from browse
  BRW2.AddField(STA:Paid_STID,BRW2.Q.STA:Paid_STID) ! Field STA:Paid_STID is a hot field or requires assignment from browse
  BRW2.AddField(STA:CID,BRW2.Q.STA:CID)           ! Field STA:CID is a hot field or requires assignment from browse
  BRW2.AddField(STA:STRID,BRW2.Q.STA:STRID)       ! Field STA:STRID is a hot field or requires assignment from browse
  BRW2.AddField(CLI:CID,BRW2.Q.CLI:CID)           ! Field CLI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_Statement_Runs',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1                           ! Will call: Update_Statements
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_Statement_Runs',1,?Browse:2,2,BRW2::PopupTextExt,Queue:Browse:2,15,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Statement_Runs.Close
  END
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_Statement_Runs',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Statements
    ReturnValue = GlobalResponse
  END
     IF SELF.Request = ViewRecord AND Request = ViewRecord
       GlobalRequest = Request
       Update_Statements
       ReturnValue = GlobalResponse
     END
  
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Print
      ThisWindow.Update()
      BRW2.UpdateViewRecord()
          IF STA:STID ~= 0
             ThisWindow.Update
      
             CASE POPUP('Print Statements Continuous|Print Statements Laser')
             OF 1
                ! (p:ID, p:PrintType, p:ID_Options, p:Preview)
                Print_Cont(STA:STID, 3)
             OF 2
                Print_Statement(CLI:CID, STA:STID)
             .
      
             ThisWindow.Reset
          .
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END
  SELF.ViewControl = ?View                                 ! Setup the control used to initiate view only mode


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Statements PROCEDURE 

CurrentTab           STRING(80)                            ! 
LOC:Locator          STRING(50)                            ! 
LOC:Statement_Type   BYTE                                  ! Client, Adhoc
LOC:StatementItems_Q QUEUE,PRE(L_SIQ)                      ! 
IID                  ULONG                                 ! Invoice Number
                     END                                   ! 
BRW1::View:Browse    VIEW(_Statements)
                       PROJECT(STA:STID)
                       PROJECT(STA:StatementDate)
                       PROJECT(STA:StatementTime)
                       PROJECT(STA:Total)
                       PROJECT(STA:Days90)
                       PROJECT(STA:Days60)
                       PROJECT(STA:Days30)
                       PROJECT(STA:Current)
                       PROJECT(STA:CID)
                       PROJECT(STA:BID)
                       PROJECT(STA:STRID)
                       JOIN(STAR:PKey_STRID,STA:STRID)
                         PROJECT(STAR:Type)
                         PROJECT(STAR:STRID)
                       END
                       JOIN(CLI:PKey_CID,STA:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
STA:STID               LIKE(STA:STID)                 !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
STA:StatementDate      LIKE(STA:StatementDate)        !List box control field - type derived from field
STA:StatementTime      LIKE(STA:StatementTime)        !List box control field - type derived from field
STA:Total              LIKE(STA:Total)                !List box control field - type derived from field
STA:Days90             LIKE(STA:Days90)               !List box control field - type derived from field
STA:Days60             LIKE(STA:Days60)               !List box control field - type derived from field
STA:Days30             LIKE(STA:Days30)               !List box control field - type derived from field
STA:Current            LIKE(STA:Current)              !List box control field - type derived from field
STA:CID                LIKE(STA:CID)                  !List box control field - type derived from field
STA:BID                LIKE(STA:BID)                  !List box control field - type derived from field
STAR:Type              LIKE(STAR:Type)                !Browse hot field - type derived from field
STA:STRID              LIKE(STA:STRID)                !Browse key field - type derived from field
STAR:STRID             LIKE(STAR:STRID)               !Related join file key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Statements File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('Browse_Statements'),SYSTEM
                       GROUP,AT(95,183,117,10),USE(?Group_StateType)
                         PROMPT('Statement Type:'),AT(95,183),USE(?LOC:Statement_Type:Prompt)
                         LIST,AT(154,183,60,10),USE(LOC:Statement_Type),DROP(5),FROM('Client|#0|Ad Hoc|#1|All|#255'), |
  MSG('Client, Adhoc'),TIP('Client, Adhoc')
                       END
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('30R(2)|M~ST ID (Statement ID)~L(1' & |
  ')@n_10@60L(2)|M~Client~C(0)@s35@34R(2)|M~Client No.~C(0)@n_10b@38R(2)|M~Date~C(0)@d5' & |
  '@38R(2)|M~Time~C(0)@t7@50R(2)|M~Total~C(0)@n-14.2@50R(1)|M~90 Days~C(0)@n-14.2@50R(1' & |
  ')|M~60 Days~C(0)@n-14.2@50R(1)|M~30 Days~C(0)@n-14.2@50R(1)|M~Current~C(0)@n-14.2@30' & |
  'R(2)|M~CID~C(0)@n_10@30R(2)|M~BID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he _Statements file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Statement ID'),USE(?Tab:2)
                           PROMPT('Locator:'),AT(9,18),USE(?LOC:Locator:Prompt),TRN
                           STRING(@s50),AT(37,18),USE(LOC:Locator),TRN
                         END
                         TAB('&2) By Client'),USE(?Tab:3)
                           BUTTON('Select Client'),AT(9,158,,14),USE(?SelectClients),LEFT,ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                         END
                         TAB('&3) By Branch'),USE(?Tab:4)
                         END
                         TAB('&4) By Statement Run'),USE(?Tab4)
                           BUTTON('Statement Run'),AT(9,159,,14),USE(?Button_Statement_Run),LEFT,ICON('WAPARENT.ICO'), |
  FLAT
                         END
                         TAB('&5) By Client No.'),USE(?Tab5)
                         END
                       END
                       BUTTON('&Print'),AT(248,180,,14),USE(?Button_Print),LEFT,ICON('PRINT.ICO'),FLAT
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,4,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 4
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ProgressWindow WINDOW('Progress...'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI), |
         CENTER,TIMER(1),GRAY,DOUBLE,MDI
       PROGRESS,USE(?Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
     END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Make_STID_List              ROUTINE
    FREE(LOC:StatementItems_Q)

    BRW1.UpdateViewRecord()
    IF STA:STID ~= 0
       STAI:STID   = STA:STID
       SET(STAI:FKey_STID,STAI:FKey_STID)
       LOOP
          IF Access:_StatementItems.TryNext() ~= LEVEL:Benign
             BREAK
          .
          IF STAI:STID ~= STA:STID
             BREAK
          .

          L_SIQ:IID     = STAI:IID
          ADD(LOC:StatementItems_Q)
    .  .
    EXIT
Run_STID_List                ROUTINE
    IF RECORDS(LOC:StatementItems_Q) > 0
       SETCURSOR(CURSOR:Wait)
       OPEN(ProgressWindow)
       DISPLAY
       ?Progress:Thermometer{PROP:RangeHigh}    = RECORDS(LOC:StatementItems_Q)
       ?Progress:UserString{PROP:Text}          = 'Busy, please wait...'

       Idx_#    = 0
       LOOP
          Idx_# += 1
          GET(LOC:StatementItems_Q, Idx_#)
          IF ERRORCODE()
             BREAK
          .

          ?Progress:Thermometer{PROP:Progress}   = Idx_#
          DISPLAY

          Upd_Invoice_Paid_Status(L_SIQ:IID, 0)
       .
       CLOSE(ProgressWindow)
       SETCURSOR()
    .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Statements')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Statement_Type:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Statement_Type',LOC:Statement_Type)   ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Clients.Open                             ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:_Statement_Runs.UseFile                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_StatementItems.UseFile                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_Statements,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,STA:FKey_CID)                ! Add the sort order for STA:FKey_CID for sort order 1
  BRW1.AddRange(STA:CID,Relate:_Statements,Relate:Clients) ! Add file relationship range limit for sort order 1
  BRW1.AppendOrder('+CLI:ClientName,+STA:StatementDate,+STA:STID') ! Append an additional sort order
  BRW1.AddSortOrder(,STA:FKey_BID)                ! Add the sort order for STA:FKey_BID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,STA:BID,1,BRW1)       ! Initialize the browse locator using  using key: STA:FKey_BID , STA:BID
  BRW1.AppendOrder('+CLI:ClientName,+STA:StatementDate,+STA:STID') ! Append an additional sort order
  BRW1.AddSortOrder(,STA:FKey_STRID)              ! Add the sort order for STA:FKey_STRID for sort order 3
  BRW1.AddRange(STA:STRID,Relate:_Statements,Relate:_Statement_Runs) ! Add file relationship range limit for sort order 3
  BRW1.AddLocator(BRW1::Sort3:Locator)            ! Browse has a locator for sort order 3
  BRW1::Sort3:Locator.Init(,STA:STRID,1,BRW1)     ! Initialize the browse locator using  using key: STA:FKey_STRID , STA:STRID
  BRW1.AppendOrder('+CLI:ClientName,+STA:STID')   ! Append an additional sort order
  BRW1.AddSortOrder(,)                            ! Add the sort order for  for sort order 4
  BRW1.AppendOrder('+CLI:ClientNo,+STA:StatementDate,+STA:STID') ! Append an additional sort order
  BRW1.AddSortOrder(,STA:PKey_STID)               ! Add the sort order for STA:PKey_STID for sort order 5
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 5
  BRW1::Sort0:Locator.Init(?LOC:Locator,STA:STID,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: STA:PKey_STID , STA:STID
  BRW1.SetFilter('(LOC:Statement_Type = 255 OR LOC:Statement_Type = STAR:Type)') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Statement_Type)          ! Apply the reset field
  BRW1.AddField(STA:STID,BRW1.Q.STA:STID)         ! Field STA:STID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientName,BRW1.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientNo,BRW1.Q.CLI:ClientNo) ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW1.AddField(STA:StatementDate,BRW1.Q.STA:StatementDate) ! Field STA:StatementDate is a hot field or requires assignment from browse
  BRW1.AddField(STA:StatementTime,BRW1.Q.STA:StatementTime) ! Field STA:StatementTime is a hot field or requires assignment from browse
  BRW1.AddField(STA:Total,BRW1.Q.STA:Total)       ! Field STA:Total is a hot field or requires assignment from browse
  BRW1.AddField(STA:Days90,BRW1.Q.STA:Days90)     ! Field STA:Days90 is a hot field or requires assignment from browse
  BRW1.AddField(STA:Days60,BRW1.Q.STA:Days60)     ! Field STA:Days60 is a hot field or requires assignment from browse
  BRW1.AddField(STA:Days30,BRW1.Q.STA:Days30)     ! Field STA:Days30 is a hot field or requires assignment from browse
  BRW1.AddField(STA:Current,BRW1.Q.STA:Current)   ! Field STA:Current is a hot field or requires assignment from browse
  BRW1.AddField(STA:CID,BRW1.Q.STA:CID)           ! Field STA:CID is a hot field or requires assignment from browse
  BRW1.AddField(STA:BID,BRW1.Q.STA:BID)           ! Field STA:BID is a hot field or requires assignment from browse
  BRW1.AddField(STAR:Type,BRW1.Q.STAR:Type)       ! Field STAR:Type is a hot field or requires assignment from browse
  BRW1.AddField(STA:STRID,BRW1.Q.STA:STRID)       ! Field STA:STRID is a hot field or requires assignment from browse
  BRW1.AddField(STAR:STRID,BRW1.Q.STAR:STRID)     ! Field STAR:STRID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:CID,BRW1.Q.CLI:CID)           ! Field CLI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_Statements',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                           ! Will call: Update_Statements
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_Statements',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,12,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_Statements',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Statements
    ReturnValue = GlobalResponse
  END
    IF Request = DeleteRecord AND ReturnValue = RequestCompleted
       DO Run_STID_List
    .
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Delete:4
          ! Make a list of the IIDs from this Statement - to run the upd on them
          DO Make_STID_List
    OF ?Button_Print
      BRW1.UpdateViewRecord()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectClients
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_RateMod_Clients()
      ThisWindow.Reset
    OF ?Button_Statement_Run
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_StatementRuns()
      ThisWindow.Reset
        LOC:Statement_Type  = STAR:Type
      
    OF ?Button_Print
      ThisWindow.Update()
          IF STA:STID ~= 0
             ThisWindow.Update
      
             CASE POPUP('Print Statements Continuous|Print Statements Laser')
             OF 1
                Print_Cont(STA:STID, 3)
             OF 2
                Print_Statement(, STA:STID)
             .
      
             ThisWindow.Reset
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF GLO:AccessLevel >= 10
          ELSE
             DISABLE(?Insert:4)
             DISABLE(?Change:4)
             DISABLE(?Delete:4)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
    IF GLO:AccessLevel >= 10
    ELSE
       SELF.InsertControl = 0
       SELF.ChangeControl = 0
       SELF.DeleteControl = 0
    .
  
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>4,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>4,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_StateType, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_StateType

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_StatementRuns PROCEDURE 

CurrentTab           STRING(80)                            ! 
LOC:Statement_No     ULONG                                 ! 
LOC:Type             STRING(20)                            ! Client Monthly, Client Adhoc, Internal Adhoc (not on web)
BRW1::View:Browse    VIEW(_Statement_Runs)
                       PROJECT(STAR:STRID)
                       PROJECT(STAR:RunDescription)
                       PROJECT(STAR:RunDate)
                       PROJECT(STAR:RunTime)
                       PROJECT(STAR:EntryDate)
                       PROJECT(STAR:EntryTime)
                       PROJECT(STAR:Complete)
                       PROJECT(STAR:Type)
                       PROJECT(STAR:UID)
                       PROJECT(STAR:RunDateTime)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
STAR:STRID             LIKE(STAR:STRID)               !List box control field - type derived from field
STAR:RunDescription    LIKE(STAR:RunDescription)      !List box control field - type derived from field
LOC:Type               LIKE(LOC:Type)                 !List box control field - type derived from local data
STAR:RunDate           LIKE(STAR:RunDate)             !List box control field - type derived from field
STAR:RunTime           LIKE(STAR:RunTime)             !List box control field - type derived from field
LOC:Statement_No       LIKE(LOC:Statement_No)         !List box control field - type derived from local data
STAR:EntryDate         LIKE(STAR:EntryDate)           !List box control field - type derived from field
STAR:EntryTime         LIKE(STAR:EntryTime)           !List box control field - type derived from field
STAR:Complete          LIKE(STAR:Complete)            !List box control field - type derived from field
STAR:Type              LIKE(STAR:Type)                !Browse hot field - type derived from field
STAR:UID               LIKE(STAR:UID)                 !Browse key field - type derived from field
STAR:RunDateTime       LIKE(STAR:RunDateTime)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Statement Runs File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_StatementRuns'),SYSTEM
                       LIST,AT(8,20,342,136),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~Run ID~C(0)@n_10@70L(2)|' & |
  'M~Run Description~C(0)@s35@40L(2)|M~Type~C(0)@s20@[48R(2)|M~Date~C(0)@d6@38R(2)|M~Ru' & |
  'n Time~C(0)@t7@]|M~Run~[56R(2)|M~Statements~C(0)@n13@](41)|M~No.~[48R(2)|M~Entry Dat' & |
  'e~C(0)@d6@38R(2)|M~Entry Time~C(0)@t7@]|M~Run On / At~36R(2)|M~Complete~C(0)@n3@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the _Statement_Runs file')
                       BUTTON('&Select'),AT(8,160,53,12),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Run Date && Time'),USE(?Tab:3)
                           BUTTON('&View'),AT(118,160,53,12),USE(?View),LEFT,ICON('WAVIEW.ICO'),FLAT
                           BUTTON('&Insert'),AT(181,160,,12),USE(?Insert),LEFT,ICON('wainsert.ico'),FLAT
                           BUTTON('&Change'),AT(237,160,,12),USE(?Change),LEFT,ICON('wachange.ico'),FLAT
                           BUTTON('&Delete'),AT(297,160,,12),USE(?Delete),LEFT,ICON('wadelete.ico'),FLAT
                         END
                         TAB('&2) By Statement Run ID'),USE(?Tab:2)
                         END
                         TAB('&3) By User'),USE(?Tab:4)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('Generate Statements'),AT(4,180,,14),USE(?Buttin_GenState),LEFT,ICON('Icon0113.ico'), |
  FLAT
                       BUTTON('Print'),AT(106,180,,14),USE(?Button_Print),LEFT,ICON(ICON:Print1),FLAT,TIP('Print Stat' & |
  'ements from this Run')
                       BUTTON('&Help'),AT(306,2,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_StatementRuns')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Type',LOC:Type)                       ! Added by: BrowseBox(ABC)
  BIND('LOC:Statement_No',LOC:Statement_No)       ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:_Statement_Runs.SetOpenRelated()
  Relate:_Statement_Runs.Open                     ! File _Statement_Runs used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_Statement_Runs,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,STAR:PKey_STRID)             ! Add the sort order for STAR:PKey_STRID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)            ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,STAR:STRID,1,BRW1)    ! Initialize the browse locator using  using key: STAR:PKey_STRID , STAR:STRID
  BRW1.AddSortOrder(,STAR:FKey_UID)               ! Add the sort order for STAR:FKey_UID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,STAR:UID,1,BRW1)      ! Initialize the browse locator using  using key: STAR:FKey_UID , STAR:UID
  BRW1.AddSortOrder(,STAR:Key_DateTime)           ! Add the sort order for STAR:Key_DateTime for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(,STAR:RunDateTime,1,BRW1) ! Initialize the browse locator using  using key: STAR:Key_DateTime , STAR:RunDateTime
  BRW1.AddField(STAR:STRID,BRW1.Q.STAR:STRID)     ! Field STAR:STRID is a hot field or requires assignment from browse
  BRW1.AddField(STAR:RunDescription,BRW1.Q.STAR:RunDescription) ! Field STAR:RunDescription is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Type,BRW1.Q.LOC:Type)         ! Field LOC:Type is a hot field or requires assignment from browse
  BRW1.AddField(STAR:RunDate,BRW1.Q.STAR:RunDate) ! Field STAR:RunDate is a hot field or requires assignment from browse
  BRW1.AddField(STAR:RunTime,BRW1.Q.STAR:RunTime) ! Field STAR:RunTime is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Statement_No,BRW1.Q.LOC:Statement_No) ! Field LOC:Statement_No is a hot field or requires assignment from browse
  BRW1.AddField(STAR:EntryDate,BRW1.Q.STAR:EntryDate) ! Field STAR:EntryDate is a hot field or requires assignment from browse
  BRW1.AddField(STAR:EntryTime,BRW1.Q.STAR:EntryTime) ! Field STAR:EntryTime is a hot field or requires assignment from browse
  BRW1.AddField(STAR:Complete,BRW1.Q.STAR:Complete) ! Field STAR:Complete is a hot field or requires assignment from browse
  BRW1.AddField(STAR:Type,BRW1.Q.STAR:Type)       ! Field STAR:Type is a hot field or requires assignment from browse
  BRW1.AddField(STAR:UID,BRW1.Q.STAR:UID)         ! Field STAR:UID is a hot field or requires assignment from browse
  BRW1.AddField(STAR:RunDateTime,BRW1.Q.STAR:RunDateTime) ! Field STAR:RunDateTime is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_StatementRuns',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                           ! Will call: Update_Statement_Runs
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_StatementRuns',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,9,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Statement_Runs.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_StatementRuns',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Statement_Runs
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Print
      BRW1.UpdateViewRecord()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Buttin_GenState
      ThisWindow.Update()
      Gen_Statements()
      ThisWindow.Reset
      BRW1.ResetFromFile()
    OF ?Button_Print
      ThisWindow.Update()
      Print_Statements(STAR:STRID)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF GLO:AccessLevel >= 10
          ELSE
             DISABLE(?Insert)
             DISABLE(?Change)
             DISABLE(?Delete)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END
  SELF.ViewControl = ?View                                 ! Setup the control used to initiate view only mode
    IF GLO:AccessLevel >= 10
    ELSE
       SELF.InsertControl=0
       SELF.ChangeControl=0
       SELF.DeleteControl=0
    .


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! Client Monthly|Client Adhoc|Internal Adhoc (not on web)
      EXECUTE STAR:Type + 1
         LOC:Type     = 'Client Monthly'
         LOC:Type     = 'Client Ad Hoc'
         LOC:Type     = 'Internal Adhoc'
      ELSE
         LOC:Type     = ''
      .
  
  
  
      LOC:Statement_No    = Get_Statement_Run_Info(STAR:STRID)
  PARENT.SetQueueRecord
  


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>2,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>2,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
Gen_Statements PROCEDURE 

LOC:Locals           GROUP,PRE(L_LS)                       ! 
Date                 LONG                                  ! Date for Statement generation
StatementTime        LONG                                  ! 
Timer                ULONG(1)                              ! 
Loops                ULONG(100)                            ! 
Time                 LONG                                  ! 
Count                ULONG                                 ! 
Amount               DECIMAL(10,2)                         ! 
Type                 BYTE                                  ! Client Monthly, Client Adhoc, Internal Adhoc (not on web)
RunDescription       STRING(35)                            ! Run Description
SingleClient         BYTE                                  ! Generate statement for a single client
CID                  ULONG                                 ! Client ID
ClientName           STRING(100)                           ! 
ClientNo             ULONG                                 ! Client No.
STRID                ULONG                                 ! Statement Run ID
Paid_STRID           ULONG                                 ! For purposes of calculating Paid in last period, this Run ID was used
Get_Previous_Run     BYTE(1)                               ! 
Payments_Group       GROUP,PRE()                           ! 
PaymentsFrom         DATE                                  ! 
PaymentsFromTime     TIME                                  ! 
PaymentsTo           DATE                                  ! 
PaymentsToTime       TIME                                  ! 
                     END                                   ! 
                     END                                   ! 
LOC:Totals           GROUP,PRE(L_TO)                       ! 
Statements           ULONG                                 ! 
Owing                DECIMAL(14,2)                         ! 
                     END                                   ! 
FDCB6::View:FileDropCombo VIEW(_Statement_Run_Desc)
                       PROJECT(STDES:RunDescription)
                       PROJECT(STDES:SRDID)
                     END
Queue:FileDropCombo  QUEUE                            !
STDES:RunDescription   LIKE(STDES:RunDescription)     !List box control field - type derived from field
STDES:SRDID            LIKE(STDES:SRDID)              !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Generating Statements'),AT(,,448,239),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,IMM,MDI,HLP('Gen_Statements'),TIMER(1)
                       SHEET,AT(4,4,440,215),USE(?Sheet1)
                         TAB('Run'),USE(?Tab1)
                           GROUP,AT(210,48,225,94),USE(?Group_Previous_Statement)
                             PROMPT('Please select a previous Statement Run to use to calculate Last Period Payments from.'), |
  AT(210,48,225,20),USE(?Prompt13),TRN
                             PROMPT('Statement Run ID:'),AT(210,72,59,10),USE(?Paid_STRID:Prompt),TRN
                             ENTRY(@n_10b),AT(274,73,56,10),USE(L_LS:Paid_STRID),RIGHT,COLOR(00E9E9E9h),FLAT,MSG('For purpos' & |
  'es of calculating Paid in last period, this Run ID was used'),READONLY,SKIP,TIP('For purpos' & |
  'es of calculating Paid in last period, this Run ID was used')
                             BUTTON('...'),AT(338,72,12,10),USE(?CallLookup:2)
                             PROMPT('Run Description:'),AT(210,88,53,10),USE(?A_STAR:RunDescription:Prompt),TRN
                             ENTRY(@s35),AT(274,89,117,10),USE(A_STAR:RunDescription),COLOR(00E9E9E9h),FLAT,MSG('Run Description'), |
  READONLY,SKIP,TIP('Run Description')
                             PROMPT('Run Date:'),AT(210,106,33,10),USE(?A_STAR:RunDate:Prompt),TRN
                             ENTRY(@d17),AT(274,105,56,10),USE(A_STAR:RunDate),COLOR(00E9E9E9h),FLAT,MSG('Statement Run Date'), |
  READONLY,SKIP,TIP('Statement Run Date')
                             PROMPT('Run Time:'),AT(338,106,33,10),USE(?A_STAR:RunTime:Prompt),TRN
                             ENTRY(@t7),AT(378,105,56,10),USE(A_STAR:RunTime),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                             PROMPT('Entry Date:'),AT(210,118,35,10),USE(?A_STAR:EntryDate:Prompt),TRN
                             ENTRY(@d17),AT(274,118,56,10),USE(A_STAR:EntryDate),COLOR(00E9E9E9h),FLAT,MSG('Run done at'), |
  READONLY,SKIP,TIP('Run done at')
                             PROMPT('Entry Time:'),AT(338,118,35,10),USE(?A_STAR:EntryTime:Prompt),TRN
                             ENTRY(@t7),AT(378,118,56,10),USE(A_STAR:EntryTime),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                             PROMPT('Type:'),AT(210,132,18,10),USE(?A_STAR:Type:Prompt),TRN
                             LIST,AT(274,131,117,10),USE(A_STAR:Type),COLOR(00E9E9E9h),DISABLE,DROP(5),FLAT,FROM('Client Mon' & |
  'thly|#0|Client Adhoc|#1|Internal Adhoc (not on web)|#20'),MSG('Client, Adhoc'),SKIP, |
  TIP('Client, Adhoc')
                           END
                           GROUP,AT(30,22,153,122),USE(?Group1)
                             COMBO(@s35),AT(78,28,117,10),USE(L_LS:RunDescription),HVSCROLL,DROP(15),FORMAT('140L(2)|M~' & |
  'Run Description~@s35@'),FROM(Queue:FileDropCombo),IMM
                             PROMPT('Run Description:'),AT(14,28),USE(?Prompt4),TRN
                             PROMPT('Statement Date:'),AT(14,44),USE(?Date:Prompt),TRN
                             SPIN(@d6b),AT(78,44,65,10),USE(L_LS:Date),RIGHT(1),MSG('Date for Statement generation'),TIP('Date for S' & |
  'tatement generation')
                             BUTTON('...'),AT(149,44,12,10),USE(?Calendar:3)
                             PROMPT('Statement Time:'),AT(14,58),USE(?StatementTime:Prompt),TRN
                             ENTRY(@t4b),AT(78,57,65,10),USE(L_LS:StatementTime),RIGHT(1)
                             PROMPT('Type:'),AT(14,74),USE(?STAR:Type:Prompt),TRN
                             LIST,AT(78,73,117,10),USE(L_LS:Type),DROP(5),FROM('Client Monthly|#0|Client Adhoc|#1|In' & |
  'ternal Adhoc (not on web)|#20'),MSG('Client, Adhoc'),TIP('Client, Adhoc')
                             CHECK(' Single Client'),AT(78,102),USE(L_LS:SingleClient),MSG('Generate statement for a' & |
  ' single client'),TIP('Generate statement for a single client'),TRN
                             GROUP,AT(41,118,157,26),USE(?Group_ClientName)
                               PROMPT('Client Name:'),AT(14,116),USE(?ClientName:Prompt),TRN
                               BUTTON('...'),AT(61,116,12,10),USE(?CallLookup)
                               ENTRY(@s100),AT(78,115,117,10),USE(L_LS:ClientName),REQ,TIP('Client name')
                               PROMPT('Client No.:'),AT(14,131),USE(?L_LS:ClientNo:Prompt),TRN
                               ENTRY(@n_10b),AT(78,131,65,10),USE(L_LS:ClientNo),RIGHT(1),COLOR(00E9E9E9h),MSG('Client No.'), |
  READONLY,SKIP,TIP('Client No.')
                             END
                           END
                           LINE,AT(201,23,0,126),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           CHECK(' Use the Previous Statement Run to set Payment From/To dates'),AT(210,28),USE(L_LS:Get_Previous_Run), |
  TIP('Get Previous Run info. automatically or set the Payments From and Payments To da' & |
  'tes manually'),TRN
                           LINE,AT(13,94,183,0),USE(?Line2),COLOR(COLOR:Black),LINEWIDTH(2)
                           LINE,AT(14,147,420,0),USE(?Line3),COLOR(COLOR:Black),LINEWIDTH(2)
                           PANEL,AT(17,180,413,33),USE(?Panel1),BEVEL(-1,-1)
                           PROMPT('Statement Run ID:'),AT(33,198),USE(?_ST:STRID:Prompt),TRN
                           STRING(@n_10),AT(97,198),USE(L_LS:STRID),RIGHT(1)
                           PROMPT('Statements:'),AT(182,198),USE(?L_TO:Statements:Prompt),TRN
                           ENTRY(@n13),AT(226,198,41,10),USE(L_TO:Statements),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP
                           PROMPT('Owing:'),AT(322,198),USE(?L_TO:Owing:Prompt),TRN
                           ENTRY(@n-19.2),AT(350,198,65,10),USE(L_TO:Owing),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                           GROUP,AT(14,158,338,12),USE(?Group_Payments)
                             PROMPT('Payments From:'),AT(14,158),USE(?L_LS:PaymentsFrom:Prompt),TRN
                             SPIN(@d5),AT(78,158,65,10),USE(L_LS:PaymentsFrom)
                             BUTTON('...'),AT(149,158,12,10),USE(?Calendar)
                             PROMPT('Payments To:'),AT(210,158),USE(?PaymentsTo:Prompt),TRN
                             SPIN(@d5),AT(270,158,65,10),USE(L_LS:PaymentsTo)
                             BUTTON('...'),AT(341,158,12,10),USE(?Calendar:2)
                           END
                         END
                       END
                       PROGRESS,AT(28,186,194,8),USE(?Progress1),RANGE(0,100)
                       PROMPT(''),AT(226,186,194,10),USE(?Prompt_Note),CENTER,TRN
                       BUTTON('&Cancel'),AT(394,222,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(2,222,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('&Go'),AT(342,222,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)             ! File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

Calendar8            CalendarClass
Calendar9            CalendarClass
Calendar10           CalendarClass
View_Client         VIEW(Clients)
    PROJECT(CLI:CID, CLI:ClientNo, CLI:ClientName)
    .


Client_View         ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Get_Previous            ROUTINE
    IF L_LS:Get_Previous_Run = TRUE
       ! Client Monthly, Client Adhoc, Internal Adhoc (not on web)
       ! Find the one from last month that is of this type

       _SQLTemp{PROP:SQL}  = 'SELECT STRID, 1, RunDateTime FROM _Statement_Runs WHERE Type = ' & L_LS:Type & ' ORDER BY RunDateTime DESC'
       IF ERRORCODE()
          MESSAGE('SQL error: ' & CLIP(ERROR()) & '||F Err: ' & CLIP(FILEERROR()),'Gen Statement - Get Previous', ICON:Hand)
       .
       LOOP
          NEXT(_SQLTemp)
          IF ERRORCODE()             ! None of this type
             BREAK
          .

          !IF YEAR(_SQ:SDate) <= YEAR(L_LS:Date) AND MONTH(_SQ:SDate) < MONTH(L_LS:Date)
             ! We have a suitable entry - get it and exit
             A_STAR:STRID           = _SQ:S1
             IF Access:Statement_Runs_Alias.TryFetch(A_STAR:PKey_STRID) = LEVEL:Benign
                L_LS:Paid_STRID     = A_STAR:STRID

                L_LS:PaymentsFrom   = A_STAR:RunDate
                L_LS:PaymentsTo     = L_LS:Date
             .
             BREAK
       .  !.
       DISPLAY
    .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Gen_Statements')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt13
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
    BIND('CLI:CID',CLI:CID)
    BIND('L_LS:CID',L_LS:CID)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Relate:Statement_Runs_Alias.Open                         ! File Statement_Runs_Alias used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Access:_Statement_Runs.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
      QuickWindow{PROP:Timer}  = 0
  Do DefineListboxStyle
      L_LS:Get_Previous_Run   = GETINI('Gen_Statements', 'Get_Previous_Run', , GLO:Global_INI)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
      L_LS:Date           = TODAY()
      L_LS:StatementTime  = (INT(CLOCK() / 100 / 60) * 100 * 60) + 1
  IF ?L_LS:SingleClient{Prop:Checked}
    ENABLE(?Group_ClientName)
  END
  IF NOT ?L_LS:SingleClient{PROP:Checked}
    DISABLE(?Group_ClientName)
  END
  IF ?L_LS:Get_Previous_Run{Prop:Checked}
    DISABLE(?Group_Payments)
    ENABLE(?Group_Previous_Statement)
  END
  IF NOT ?L_LS:Get_Previous_Run{PROP:Checked}
    ENABLE(?Group_Payments)
    DISABLE(?Group_Previous_Statement)
  END
  FDCB6.Init(L_LS:RunDescription,?L_LS:RunDescription,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:_Statement_Run_Desc,ThisWindow,GlobalErrors,1,1,0)
  FDCB6.AskProcedure = 3
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder()
  FDCB6.AddField(STDES:RunDescription,FDCB6.Q.STDES:RunDescription) !List box control field - type derived from field
  FDCB6.AddField(STDES:SRDID,FDCB6.Q.STDES:SRDID) !Primary key field - type derived from field
  FDCB6.AddUpdateField(STDES:RunDescription,L_LS:RunDescription)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    UNBIND('CLI:CID')
    UNBIND('L_LS:CID')
  
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
    Relate:Statement_Runs_Alias.Close
    Relate:_SQLTemp.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_StatementRuns_Alias
      Browse_Clients
      Update_Statement_Run_Desc
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_LS:Paid_STRID
      IF L_LS:Paid_STRID OR ?L_LS:Paid_STRID{PROP:Req}
        A_STAR:STRID = L_LS:Paid_STRID
        IF Access:Statement_Runs_Alias.TryFetch(A_STAR:PKey_STRID)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_LS:Paid_STRID = A_STAR:STRID
          ELSE
            SELECT(?L_LS:Paid_STRID)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update()
      A_STAR:STRID = L_LS:Paid_STRID
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_LS:Paid_STRID = A_STAR:STRID
      END
      ThisWindow.Reset(1)
    OF ?L_LS:RunDescription
          DO Get_Previous
      FDCB6.TakeAccepted()
    OF ?Calendar:3
      ThisWindow.Update()
      Calendar10.SelectOnClose = True
      Calendar10.Ask('Select a Date',L_LS:Date)
      IF Calendar10.Response = RequestCompleted THEN
      L_LS:Date=Calendar10.SelectedDate
      DISPLAY(?L_LS:Date)
      END
      ThisWindow.Reset(True)
    OF ?L_LS:Type
          DO Get_Previous
    OF ?L_LS:SingleClient
      IF ?L_LS:SingleClient{PROP:Checked}
        ENABLE(?Group_ClientName)
      END
      IF NOT ?L_LS:SingleClient{PROP:Checked}
        DISABLE(?Group_ClientName)
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = L_LS:ClientName
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_LS:ClientName = CLI:ClientName
        L_LS:CID = CLI:CID
        L_LS:ClientNo = CLI:ClientNo
      END
      ThisWindow.Reset(1)
    OF ?L_LS:ClientName
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_LS:ClientName OR ?L_LS:ClientName{PROP:Req}
          CLI:ClientName = L_LS:ClientName
          IF Access:Clients.TryFetch(CLI:Key_ClientName)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              L_LS:ClientName = CLI:ClientName
              L_LS:CID = CLI:CID
              L_LS:ClientNo = CLI:ClientNo
            ELSE
              CLEAR(L_LS:CID)
              CLEAR(L_LS:ClientNo)
              SELECT(?L_LS:ClientName)
              CYCLE
            END
          ELSE
            L_LS:CID = CLI:CID
            L_LS:ClientNo = CLI:ClientNo
          END
        END
      END
      ThisWindow.Reset()
    OF ?L_LS:Get_Previous_Run
      IF ?L_LS:Get_Previous_Run{PROP:Checked}
        DISABLE(?Group_Payments)
        ENABLE(?Group_Previous_Statement)
      END
      IF NOT ?L_LS:Get_Previous_Run{PROP:Checked}
        ENABLE(?Group_Payments)
        DISABLE(?Group_Previous_Statement)
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
      Calendar8.SelectOnClose = True
      Calendar8.Ask('Select a Date',L_LS:PaymentsFrom)
      IF Calendar8.Response = RequestCompleted THEN
      L_LS:PaymentsFrom=Calendar8.SelectedDate
      DISPLAY(?L_LS:PaymentsFrom)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar9.SelectOnClose = True
      Calendar9.Ask('Select a Date',L_LS:PaymentsTo)
      IF Calendar9.Response = RequestCompleted THEN
      L_LS:PaymentsTo=Calendar9.SelectedDate
      DISPLAY(?L_LS:PaymentsTo)
      END
      ThisWindow.Reset(True)
    OF ?Cancel
      ThisWindow.Update()
          IF QuickWindow{PROP:Timer} ~= 0
             CASE MESSAGE('Statements are being generated, cancelling now will possibly leave some Client without statements for this run.||Do you want to cancel?', '', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:No
                CYCLE
          .  .
       POST(EVENT:CloseWindow)
    OF ?Ok
      ThisWindow.Update()
          ! Validations
          IF CLIP(L_LS:RunDescription) = ''
             MESSAGE('Please select or enter a Run Description.', 'Statement Run', ICON:Exclamation)
             SELECT(?L_LS:RunDescription)
             CYCLE
          .
      
          IF L_LS:SingleClient = TRUE
             IF L_LS:CID = 0
                MESSAGE('For single client, please select a client.', 'Generate Statements', ICON:Exclamation)
                SELECT(?L_LS:ClientName)
                CYCLE
          .  .
          PUTINI('Gen_Statements', 'Get_Previous_Run', L_LS:Get_Previous_Run, GLO:Global_INI)
          ! Add Statement Run record
          IF Access:_Statement_Runs.PrimeRecord() ~= LEVEL:Benign
             CYCLE
          ELSE
             !
             STAR:RunDate         = L_LS:Date
             STAR:RunTime         = L_LS:StatementTime
             STAR:RunDescription  = L_LS:RunDescription
      
             STAR:EntryDate       = TODAY()
             STAR:EntryTime       = CLOCK()
      
             STAR:Type            = L_LS:Type
      
             IF Access:_Statement_Runs.Insert() ~= LEVEL:Benign
                CYCLE
             .
      
             L_LS:STRID           = STAR:STRID
          .
      
      
          DISABLE(?Group1)
          DISABLE(?Ok)
      
          Client_View.Init(View_Client, Relate:Clients)
          Client_View.AddSortOrder(CLI:Key_ClientNo)
          !Client_View.AppendOrder()
          !Client_View.AddRange(CLI:ClientNo, )
      
          ?Progress1{PROP:RangeHigh}  = RECORDS(Clients)
      
          IF L_LS:SingleClient = TRUE
             ?Progress1{PROP:RangeHigh}  = 1
             Client_View.SetFilter('CLI:CID = L_LS:CID', '1')
          .
      
          Client_View.Reset()
      
          QuickWindow{PROP:Timer}     = 1
          L_LS:Timer                  = 1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_LS:RunDescription
          DO Get_Previous
    OF ?L_LS:Type
          DO Get_Previous
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
          QuickWindow{PROP:Timer} = 0
      
          L_LS:Time               = CLOCK()
          L_LS:Count              = 0
      
          LOOP L_LS:Loops TIMES
             IF CLOCK() - L_LS:Time > 100
                IF L_LS:Count > 0
                   L_LS:Loops     = L_LS:Count
                ELSE
                   L_LS:Loops     = 1
                .
                BREAK
             .
      
             IF Client_View.Next() ~= LEVEL:Benign
                Client_View.Kill()
      
                L_LS:Timer        = 0
                IF L_TO:Statements > 0
                   MESSAGE('Statements have been generated.|||Generated: ' & L_TO:Statements & '||Total Owing: ' & FORMAT(L_TO:Owing,@n12.2) , 'Statement Generation', ICON:Asterisk)
                ELSE
                   MESSAGE('No statements generated.' , 'Statement Generation', ICON:Asterisk)
                .
      
                CLEAR(LOC:Totals)
                ?Prompt_Note{PROP:Text}     = ''
                ?Progress1{PROP:Progress}   = 0
      
                ENABLE(?Group1)
                ENABLE(?Ok)
      
                !IF L_LS:SingleClient = FALSE
                !   POST(EVENT:CloseWindow)
                !.
      
                BREAK
             .
      
             L_LS:Count          += 1
      
             CLEAR(L_LS:Amount)
                           ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
             IF Gen_Statement(STAR:STRID, CLI:CID, L_LS:Date, L_LS:StatementTime, L_LS:Amount,,, L_LS:Type) > 0
                L_TO:Statements     += 1
             .
      
             L_TO:Owing          += L_LS:Amount
      
             ?Prompt_Note{PROP:Text}     = CLIP(CLI:ClientName) & '..  Owing: ' & FORMAT(L_LS:Amount, @n12.2)
             ?Progress1{PROP:Progress}   = L_TO:Statements
          .
      
          IF CLOCK() - L_LS:Time < 80
             L_LS:Loops           = L_LS:Loops * 1.1
          .
      
          DISPLAY
          QuickWindow{PROP:Timer}     = L_LS:Timer
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Remittance_Runs PROCEDURE 

CurrentTab           STRING(80)                            ! 
LOC:Type             STRING(35)                            ! Transporter Monthly, Transporter Adhoc, Internal Adhoc (not on web)
BRW1::View:Browse    VIEW(_Remittance_Runs)
                       PROJECT(REMR:RERID)
                       PROJECT(REMR:RunDescription)
                       PROJECT(REMR:RunDate)
                       PROJECT(REMR:RunTime)
                       PROJECT(REMR:EntryDate)
                       PROJECT(REMR:EntryTime)
                       PROJECT(REMR:PaymentsFromDate)
                       PROJECT(REMR:PaymentsToDate)
                       PROJECT(REMR:Complete)
                       PROJECT(REMR:Type)
                       PROJECT(REMR:RunDateTime)
                       PROJECT(REMR:UID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
REMR:RERID             LIKE(REMR:RERID)               !List box control field - type derived from field
REMR:RunDescription    LIKE(REMR:RunDescription)      !List box control field - type derived from field
LOC:Type               LIKE(LOC:Type)                 !List box control field - type derived from local data
REMR:RunDate           LIKE(REMR:RunDate)             !List box control field - type derived from field
REMR:RunTime           LIKE(REMR:RunTime)             !List box control field - type derived from field
REMR:EntryDate         LIKE(REMR:EntryDate)           !List box control field - type derived from field
REMR:EntryTime         LIKE(REMR:EntryTime)           !List box control field - type derived from field
REMR:PaymentsFromDate  LIKE(REMR:PaymentsFromDate)    !List box control field - type derived from field
REMR:PaymentsToDate    LIKE(REMR:PaymentsToDate)      !List box control field - type derived from field
REMR:Complete          LIKE(REMR:Complete)            !List box control field - type derived from field
REMR:Type              LIKE(REMR:Type)                !Browse hot field - type derived from field
REMR:RunDateTime       LIKE(REMR:RunDateTime)         !Browse key field - type derived from field
REMR:UID               LIKE(REMR:UID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Remittance Runs File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_Remittance_Runs'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('30R(2)|M~RER ID~C(0)@n_10@70L(2)|' & |
  'M~Run Description~@s35@50L(2)|M~Type~@s35@[38R(2)|M~Date~C(0)@d5b@38R(2)|M~Time~C(0)' & |
  '@t7@]|M~Run~[38R(2)|M~Date~C(0)@d5@38R(2)|M~Time~C(0)@t7@]|M~Entry~[47R(2)|M~From Da' & |
  'te~C(0)@d6b@47R(2)|M~To Date~C(0)@d6b@]|M~Payments~36R(2)|M~Complete~C(0)@n3@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the _Remittance_Runs file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       BUTTON('Generate Remittance'),AT(3,180,,14),USE(?Button_Gen_Remittance),LEFT,ICON('Icon0113.ico'), |
  FLAT
                       BUTTON('Print'),AT(108,180,,14),USE(?Button_Print),LEFT,ICON(ICON:Print1),FLAT
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Remittance Run ID'),USE(?Tab:2)
                         END
                         TAB('&2) By Run Date && Time'),USE(?Tab:3)
                         END
                         TAB('&3) By User'),USE(?Tab:4)
                         END
                         TAB('&4) By Run Description'),USE(?Tab:5)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,4,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 4
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Remittance_Runs')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Type',LOC:Type)                       ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:_Remittance_Runs.Open                    ! File _Remittance_Runs used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_Remittance_Runs,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,REMR:Key_DateTime)           ! Add the sort order for REMR:Key_DateTime for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)            ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,REMR:RunDateTime,1,BRW1) ! Initialize the browse locator using  using key: REMR:Key_DateTime , REMR:RunDateTime
  BRW1.AddSortOrder(,REMR:FKey_UID)               ! Add the sort order for REMR:FKey_UID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,REMR:UID,1,BRW1)      ! Initialize the browse locator using  using key: REMR:FKey_UID , REMR:UID
  BRW1.AddSortOrder(,REMR:FKey_RunDesc)           ! Add the sort order for REMR:FKey_RunDesc for sort order 3
  BRW1.AddLocator(BRW1::Sort3:Locator)            ! Browse has a locator for sort order 3
  BRW1::Sort3:Locator.Init(,REMR:RunDescription,1,BRW1) ! Initialize the browse locator using  using key: REMR:FKey_RunDesc , REMR:RunDescription
  BRW1.AddSortOrder(,REMR:PKey_RERID)             ! Add the sort order for REMR:PKey_RERID for sort order 4
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 4
  BRW1::Sort0:Locator.Init(,REMR:RERID,1,BRW1)    ! Initialize the browse locator using  using key: REMR:PKey_RERID , REMR:RERID
  BRW1.AddField(REMR:RERID,BRW1.Q.REMR:RERID)     ! Field REMR:RERID is a hot field or requires assignment from browse
  BRW1.AddField(REMR:RunDescription,BRW1.Q.REMR:RunDescription) ! Field REMR:RunDescription is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Type,BRW1.Q.LOC:Type)         ! Field LOC:Type is a hot field or requires assignment from browse
  BRW1.AddField(REMR:RunDate,BRW1.Q.REMR:RunDate) ! Field REMR:RunDate is a hot field or requires assignment from browse
  BRW1.AddField(REMR:RunTime,BRW1.Q.REMR:RunTime) ! Field REMR:RunTime is a hot field or requires assignment from browse
  BRW1.AddField(REMR:EntryDate,BRW1.Q.REMR:EntryDate) ! Field REMR:EntryDate is a hot field or requires assignment from browse
  BRW1.AddField(REMR:EntryTime,BRW1.Q.REMR:EntryTime) ! Field REMR:EntryTime is a hot field or requires assignment from browse
  BRW1.AddField(REMR:PaymentsFromDate,BRW1.Q.REMR:PaymentsFromDate) ! Field REMR:PaymentsFromDate is a hot field or requires assignment from browse
  BRW1.AddField(REMR:PaymentsToDate,BRW1.Q.REMR:PaymentsToDate) ! Field REMR:PaymentsToDate is a hot field or requires assignment from browse
  BRW1.AddField(REMR:Complete,BRW1.Q.REMR:Complete) ! Field REMR:Complete is a hot field or requires assignment from browse
  BRW1.AddField(REMR:Type,BRW1.Q.REMR:Type)       ! Field REMR:Type is a hot field or requires assignment from browse
  BRW1.AddField(REMR:RunDateTime,BRW1.Q.REMR:RunDateTime) ! Field REMR:RunDateTime is a hot field or requires assignment from browse
  BRW1.AddField(REMR:UID,BRW1.Q.REMR:UID)         ! Field REMR:UID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_Remittance_Runs',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                           ! Will call: Update_Remittance_Runs
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_Remittance_Runs',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,10,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Remittance_Runs.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_Remittance_Runs',QuickWindow)    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Remittance_Runs
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Print
      BRW1.UpdateViewRecord()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Gen_Remittance
      ThisWindow.Update()
      Gen_Remittances()
      ThisWindow.Reset
      BRW1.ResetFromFile()
    OF ?Button_Print
      ThisWindow.Update()
      Print_Remittances(REMR:RERID)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! Transporter Monthly|Transporter Adhoc|Internal Adhoc (not on web)
      EXECUTE REMR:Type + 1
         LOC:Type     = 'Transporter Monthly'
         LOC:Type     = 'Transporter Adhoc'
         LOC:Type     = 'Internal Adhoc (not on web)'
      .
  PARENT.SetQueueRecord
  


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>3,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>3,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Gen_Remittances PROCEDURE 

LOC:Locals           GROUP,PRE(L_LS)                       ! 
Type                 BYTE                                  ! Transporter Monthly, Transporter Adhoc, Internal Adhoc (not on web)
Date                 LONG                                  ! Date for Statement generation
RemittanceTime       LONG                                  ! 
Timer                ULONG(1)                              ! 
Loops                ULONG(100)                            ! 
Time                 LONG                                  ! 
Count                ULONG                                 ! 
Amount               DECIMAL(10,2)                         ! 
RunDescription       STRING(35)                            ! Run Description
SingleTransporter    BYTE                                  ! Generate statement for a single Transporter
CID                  ULONG                                 ! Client ID
TransporterName      STRING(35)                            ! Transporters Name
TID                  ULONG                                 ! Transporter ID
REMID                ULONG                                 ! Remittance ID
Payments_Group       GROUP,PRE()                           ! 
PaymentsFrom         DATE                                  ! 
PaymentsFromTime     TIME                                  ! 
PaymentsTo           DATE                                  ! 
PaymentsToTime       TIME                                  ! 
                     END                                   ! 
                     END                                   ! 
LOC:Totals           GROUP,PRE(L_TO)                       ! 
Remittances          ULONG                                 ! 
Paid                 DECIMAL(14,2)                         ! 
                     END                                   ! 
FDCB6::View:FileDropCombo VIEW(_Statement_Run_Desc)
                       PROJECT(STDES:RunDescription)
                       PROJECT(STDES:SRDID)
                     END
Queue:FileDropCombo  QUEUE                            !
STDES:RunDescription   LIKE(STDES:RunDescription)     !List box control field - type derived from field
STDES:SRDID            LIKE(STDES:SRDID)              !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Generate Remittance'),AT(,,383,217),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  IMM,MDI,HLP('Gen_Statements'),TIMER(1)
                       SHEET,AT(4,4,375,196),USE(?Sheet1)
                         TAB('Run'),USE(?Tab1)
                           GROUP,AT(30,22,153,109),USE(?Group1)
                             COMBO(@s35),AT(94,28,117,10),USE(L_LS:RunDescription),HVSCROLL,DROP(15),FORMAT('140L(2)|M~' & |
  'Run Description~@s35@'),FROM(Queue:FileDropCombo),IMM
                             PROMPT('Run Description:'),AT(14,28),USE(?Prompt4),TRN
                             PROMPT('Remittance Date:'),AT(14,44),USE(?Date:Prompt),TRN
                             SPIN(@d6b),AT(94,44,65,10),USE(L_LS:Date),RIGHT(1),MSG('Date for Remittance generation'),TIP('Date for R' & |
  'emittance generation')
                             PROMPT('Remittance Time:'),AT(14,58),USE(?StatementTime:Prompt),TRN
                             ENTRY(@t4b),AT(94,58,65,10),USE(L_LS:RemittanceTime),RIGHT(1)
                             CHECK(' Single Transporter'),AT(94,84),USE(L_LS:SingleTransporter),MSG('Generate statem' & |
  'ent for a single client'),TIP('Generate statement for a single client'),TRN
                             GROUP,AT(41,106,157,26),USE(?Group_ClientName)
                               PROMPT('Transporter Name:'),AT(14,96),USE(?ClientName:Prompt),TRN
                               BUTTON('...'),AT(78,96,12,10),USE(?CallLookup)
                               ENTRY(@s35),AT(94,96,117,10),USE(L_LS:TransporterName),REQ,TIP('Client name')
                               PROMPT('Transporter ID (TID):'),AT(14,110),USE(?L_LS:ClientNo:Prompt),TRN
                               ENTRY(@n_10),AT(94,110,65,10),USE(L_LS:TID),RIGHT(1),COLOR(00E9E9E9h),MSG('Client No.'),READONLY, |
  SKIP,TIP('Client No.')
                             END
                           END
                           LINE,AT(218,23,0,107),USE(?Line4),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Type:'),AT(233,70),USE(?Type:Prompt),TRN
                           LIST,AT(262,70,105,10),USE(L_LS:Type),DROP(5),FROM('Transporter Monthly|#0|Transporter ' & |
  'Adhoc|#1|Internal Adhoc (not on web)|#20'),MSG('Transporter, Adhoc'),TIP('Transporter, Adhoc')
                           LINE,AT(13,76,207,0),USE(?Line2),COLOR(COLOR:Black),LINEWIDTH(2)
                           LINE,AT(14,129,355,0),USE(?Line3),COLOR(COLOR:Black),LINEWIDTH(2)
                           PANEL,AT(14,162,355,33),USE(?Panel1),BEVEL(-1,-1)
                           PROMPT('Remittance Run ID:'),AT(30,180),USE(?_ST:STRID:Prompt),TRN
                           STRING(@n_10),AT(94,180),USE(L_LS:REMID),RIGHT(1),TRN
                           PROMPT('Remittances:'),AT(169,180),USE(?L_TO:Statements:Prompt),TRN
                           ENTRY(@n13),AT(213,180,41,10),USE(L_TO:Remittances),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP
                           PROMPT('Paid:'),AT(265,180),USE(?L_TO:Owing:Prompt),TRN
                           ENTRY(@n-19.2),AT(285,180,65,10),USE(L_TO:Paid),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                           GROUP,AT(14,140,338,12),USE(?Group_Payments)
                             PROMPT('Payments From:'),AT(14,142),USE(?L_LS:PaymentsFrom:Prompt),TRN
                             SPIN(@d5),AT(78,142,65,10),USE(L_LS:PaymentsFrom)
                             BUTTON('...'),AT(149,142,12,10),USE(?Calendar)
                             PROMPT('Payments To:'),AT(210,142),USE(?PaymentsTo:Prompt),TRN
                             SPIN(@d5),AT(270,142,65,10),USE(L_LS:PaymentsTo)
                             BUTTON('...'),AT(341,142,12,10),USE(?Calendar:2)
                           END
                         END
                       END
                       PROGRESS,AT(26,168,194,8),USE(?Progress1),RANGE(0,100)
                       PROMPT(''),AT(224,168,131,10),USE(?Prompt_Note),CENTER,TRN
                       BUTTON('&Cancel'),AT(330,202,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(2,202,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('&Go'),AT(278,202,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)             ! File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

Calendar8            CalendarClass
Calendar9            CalendarClass
View_Client         VIEW(Transporter)
    PROJECT(TRA:TID, TRA:TransporterName)

    .


Client_View         ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Gen_Remittances')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?L_LS:RunDescription
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
    BIND('TRA:TID', TRA:TID)
    BIND('L_LS:TID', L_LS:TID)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Statement_Runs_Alias.Open                         ! File Statement_Runs_Alias used by this procedure, so make sure it's RelationManager is open
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                                  ! File Transporter used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Access:_Remittance_Runs.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
      QuickWindow{PROP:Timer}  = 0
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Gen_Remittances',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      L_LS:Date           = TODAY()
      L_LS:RemittanceTime = (INT(CLOCK() / 100 / 60) * 100 * 60) + 1
  FDCB6.Init(L_LS:RunDescription,?L_LS:RunDescription,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:_Statement_Run_Desc,ThisWindow,GlobalErrors,1,1,0)
  FDCB6.AskProcedure = 2
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder()
  FDCB6.AddField(STDES:RunDescription,FDCB6.Q.STDES:RunDescription) !List box control field - type derived from field
  FDCB6.AddField(STDES:SRDID,FDCB6.Q.STDES:SRDID) !Primary key field - type derived from field
  FDCB6.AddUpdateField(STDES:RunDescription,L_LS:RunDescription)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Statement_Runs_Alias.Close
    Relate:Transporter.Close
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('Gen_Remittances',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Transporter
      Update_Statement_Run_Desc
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_LS:RunDescription
      FDCB6.TakeAccepted()
    OF ?CallLookup
      ThisWindow.Update()
      TRA:TransporterName = L_LS:TransporterName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_LS:TransporterName = TRA:TransporterName
        L_LS:TID = TRA:TID
      END
      ThisWindow.Reset(1)
    OF ?L_LS:TransporterName
      IF L_LS:TransporterName OR ?L_LS:TransporterName{PROP:Req}
        TRA:TransporterName = L_LS:TransporterName
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_LS:TransporterName = TRA:TransporterName
            L_LS:TID = TRA:TID
          ELSE
            CLEAR(L_LS:TID)
            SELECT(?L_LS:TransporterName)
            CYCLE
          END
        ELSE
          L_LS:TID = TRA:TID
        END
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
      Calendar8.SelectOnClose = True
      Calendar8.Ask('Select a Date',L_LS:PaymentsFrom)
      IF Calendar8.Response = RequestCompleted THEN
      L_LS:PaymentsFrom=Calendar8.SelectedDate
      DISPLAY(?L_LS:PaymentsFrom)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar9.SelectOnClose = True
      Calendar9.Ask('Select a Date',L_LS:PaymentsTo)
      IF Calendar9.Response = RequestCompleted THEN
      L_LS:PaymentsTo=Calendar9.SelectedDate
      DISPLAY(?L_LS:PaymentsTo)
      END
      ThisWindow.Reset(True)
    OF ?Cancel
      ThisWindow.Update()
          IF QuickWindow{PROP:Timer} ~= 0
             CASE MESSAGE('Remittances are being generated, cancelling now will possibly leave some Transporters without remittance for this run.||Do you want to cancel?', 'Remittance Run', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:No
                CYCLE
          .  .
       POST(EVENT:CloseWindow)
    OF ?Ok
      ThisWindow.Update()
          ! Validations
          IF CLIP(L_LS:RunDescription) = ''
             MESSAGE('Please enter a Run Description.', 'Remittance Run', ICON:Exclamation)
             SELECT(?L_LS:RunDescription)
             CYCLE
          .
      
          IF L_LS:SingleTransporter = TRUE
             IF L_LS:TID = 0
                MESSAGE('For single Transporter, please select a Transporter.', 'Generate Remittances', ICON:Exclamation)
                SELECT(?L_LS:TransporterName)
                CYCLE
          .  .
      
      
          IF L_LS:PaymentsFrom > L_LS:PaymentsTo
             MESSAGE('The From date is later than the To date.', 'Generate Remittances', ICON:Exclamation)
             SELECT(?L_LS:PaymentsTo)
             CYCLE
          .
      
      
          IF L_LS:PaymentsFrom <= 0 OR L_LS:PaymentsTo <= 0
             CASE MESSAGE('You have not specified either the From date or the To date.||Proceed', 'Generate Remittances', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:No
                SELECT(?L_LS:PaymentsTo)
                CYCLE
          .  .
          ! Save dates
          PUTINI('Gen_Remittances', 'PaymentsFrom', L_LS:PaymentsFrom, GLO:Local_INI)
          PUTINI('Gen_Remittances', 'PaymentsTo', L_LS:PaymentsTo, GLO:Local_INI)
          
          ! Add Statement Run record
          IF Access:_Statement_Runs.PrimeRecord() ~= LEVEL:Benign
             CYCLE
          ELSE
             !
             REMR:RunDate         = L_LS:Date
             REMR:RunTime         = CLOCK()             !L_LS:StatementTime
             REMR:RunDescription  = L_LS:RunDescription
      
             REMR:EntryDate       = TODAY()
             REMR:EntryTime       = CLOCK()
      
             REMR:Type            = L_LS:Type
      
             IF Access:_Remittance_Runs.Insert() ~= LEVEL:Benign
                CYCLE
             .
      
             L_LS:REMID           = REMR:RERID
          .
      
      
          DISABLE(?Group1)
          DISABLE(?Ok)
      
          Client_View.Init(View_Client, Relate:Transporter)
          Client_View.AddSortOrder(TRA:PKey_TID)
          !Client_View.AppendOrder()
          !Client_View.AddRange(CLI:ClientNo, )
      
          ?Progress1{PROP:RangeHigh}  = RECORDS(Transporter)
      
          IF L_LS:SingleTransporter = TRUE
             ?Progress1{PROP:RangeHigh}  = 1
             Client_View.SetFilter('TRA:TID = L_LS:TID', '1')
          .
      
          Client_View.Reset()
      
          QuickWindow{PROP:Timer}     = 1
          L_LS:Timer                  = 1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          !OPEN(Client_View)
          !SET(Client_View)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
          QuickWindow{PROP:Timer} = 0
      
          L_LS:Time               = CLOCK()
          L_LS:Count              = 0
      
          LOOP L_LS:Loops TIMES
             IF CLOCK() - L_LS:Time > 100
                IF L_LS:Count > 0
                   L_LS:Loops     = L_LS:Count
                ELSE
                   L_LS:Loops     = 1
                .
                BREAK
             .
      
             IF Client_View.Next() ~= LEVEL:Benign
                L_LS:Timer        = 0
                IF L_TO:Remittances > 0
                   MESSAGE('Remittances have been generated.|||Generated: ' & L_TO:Remittances & '||Total Paid: ' & FORMAT(L_TO:Paid,@n12.2) , 'Remittance Generation', ICON:Asterisk)
                ELSE
                   MESSAGE('No Remittances generated.' , 'Remittance Generation', ICON:Asterisk)
                .
      
                Client_View.Kill()
      
                ENABLE(?Group1)
                ENABLE(?Ok)
      
                !IF L_LS:SingleClient = FALSE
                !   POST(EVENT:CloseWindow)
                !.
      
                BREAK
             .
      
             L_LS:Count          += 1
      
             CLEAR(L_LS:Amount)
                                 ! (p:REMRID, p:TID, p:RunDate, p:RunTime, p:FromDate, p:ToDate, p:Run_Type, p:OutPut, p:Paid_Total)
             IF Gen_Remittance_Paid(REMR:RERID, TRA:TID, L_LS:Date, L_LS:Time, L_LS:PaymentsFrom, L_LS:PaymentsTo,,, L_LS:Amount) > 0
                L_TO:Remittances     += 1
             .
      
             L_TO:Paid           += L_LS:Amount
          .
      
          IF CLOCK() - L_LS:Time < 80
             L_LS:Loops           = L_LS:Loops * 1.1
          .
      
          ?Prompt_Note{PROP:Text}     = CLIP(TRA:TransporterName) & '..  Paid: ' & FORMAT(L_LS:Amount, @n12.2)
          ?Progress1{PROP:Progress}   = L_TO:Remittances
      
          DISPLAY
          QuickWindow{PROP:Timer}     = L_LS:Timer
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

