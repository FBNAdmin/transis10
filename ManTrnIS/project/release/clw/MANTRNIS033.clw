

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS033.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Deliveries PROCEDURE (p:Option)

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Screen_Group     GROUP,PRE(L_SG)                       ! 
Manifested_Units     ULONG                                 ! 
Un_Manifested_Units  ULONG                                 ! 
Value                DECIMAL(11,2)                         ! 
Value_Type           BYTE                                  ! 
VATRate_Previous     DECIMAL(5,2)                          ! Before any possible user change
Tripsheets_Info      STRING(255)                           ! Used as to get TRID's and Delivered Date / Times
SpecialDeliveryCharsLeft LONG(36*4)                        ! 
                     END                                   ! 
LOC:Lookup_Vars      GROUP,PRE(L_LG)                       ! 
COD_Address          STRING(35)                            ! Name of this address
Driver_Name          STRING(70)                            ! Driver Name
Journey              STRING(70)                            ! Description
Journey_TollCharged  BYTE                                  ! 
TollRate             DECIMAL(10,4)                         ! Rate Toll charged at as a percent of the Freight charge
ClientName           STRING(100)                           ! 
ClientNo             ULONG                                 ! Client No.
From_Address         STRING(35)                            ! Name of this address
To_Address           STRING(35)                            ! Name of this address
LoadType             STRING(100)                           ! Load Type
ServiceRequirement   STRING(35)                            ! Service Requirement
Floor                STRING(35)                            ! Floor Name
FloorRate            STRING(35)                            ! Floor Name
ClientRateType       STRING(100)                           ! Load Type
ClientRateType_CID   ULONG                                 ! Client ID
                     END                                   ! 
LOC:Load_Type_Group  GROUP,PRE(L_LT)                       ! 
TurnIn               BYTE                                  ! Container Turn In required for this Load Type
LoadOption           BYTE                                  ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
Hazchem              BYTE                                  ! 
ContainerParkStandard BYTE                                 ! Container Park Standard Rates
FID                  ULONG                                 ! Floor ID
                     END                                   ! 
LOC:Last_Selected    LONG                                  ! 
LOC:Request          LONG                                  ! 
LOC:Manifested       BYTE                                  ! 
LOC:Original_Request LONG                                  ! 
LOC:Client_Group     GROUP,PRE(L_CG)                       ! 
MinChargeChecked     BYTE                                  ! 
MinimiumCharge       DECIMAL(8,2)                          ! 
UseMinCharge         BYTE                                  ! 
GenerateInvoice      BYTE                                  ! Generate an invoice when DI created
ClientTerms          BYTE                                  ! Terms - Pre Paid, COD, Account, On Statement
ClientStatus         BYTE                                  ! Normal, On Hold, Closed
                     END                                   ! 
LOC:Charge_Group     GROUP,PRE(L_CC)                       ! 
TotalWeight          DECIMAL(10,2)                         ! 
TotalUnits           ULONG                                 ! Number of units
Charge               DECIMAL(9,2)                          ! Charge for the DI (additional legs have own charges specified)
InsuanceAmount       DECIMAL(10,2)                         ! 
FuelSurchargePer     DECIMAL(12,4)                         ! 
MinChargeRateChecked BYTE                                  ! 
MinimiumChargeRate   DECIMAL(10,4)                         ! Rate per Kg
UseMinRateCharge     BYTE                                  ! 
Leg_Costs            DECIMAL(9,2)                          ! 
TotalCharge          DECIMAL(9,2)                          ! Charge for the DI (additional legs have own charges specified)
Rate_Checked         BYTE                                  ! Once rate checked tell user - only once
VAT                  DECIMAL(9,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
TotalCharge_VAT      DECIMAL(9,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
                     END                                   ! 
LOC:Last_Group       GROUP,PRE(L_LG)                       ! 
Charge_Specified     BYTE                                  ! User has specified a charge - no longer calculated (still check min)
Rate_Specified       BYTE                                  ! User typed a rate
Last_Rate            DECIMAL(10,4)                         ! Rate for DI (debtors journey rate possibly adjusted) - Charge will be Rate multiplied by the total weight in kgs.
Next_Rate            DECIMAL(10,4)                         ! Rate for DI (debtors journey rate possibly adjusted) - Charge will be Rate multiplied by the total weight in kgs.
No_Items             ULONG                                 ! 
Last_No_Items        ULONG                                 ! 
CID                  ULONG                                 ! Client ID
JID                  ULONG                                 ! Journey ID
LTID                 ULONG                                 ! Load Type ID
CRTID                ULONG                                 ! Client Rate Type ID
PODMessage           STRING(255)                           ! 
                     END                                   ! 
LOC:Rate_Group       GROUP,PRE(L_RG)                       ! 
Setup_Rate           BYTE                                  ! 
To_Weight            DECIMAL(9)                            ! Up to this mass in Kgs
Effective_Date       LONG                                  ! Effective from this date
Selected_Rate        DECIMAL(10,4)                         ! Rate per Kg
ID                   ULONG                                 ! Rate / Container Park ID
Returned_Rate        DECIMAL(10,4)                         ! Rate per Kg
                     END                                   ! 
LOC:Items_Group      GROUP,PRE()                           ! 
L_IG:MID             STRING(50)                            ! Manifest ID
L_IG:Last_MID        ULONG                                 ! Manifest ID
                     END                                   ! 
LOC:DeliveryComposition_Ret_Group GROUP,PRE(L_DC)          ! Totals so far
Items                ULONG                                 ! Total no. of items
Weight               DECIMAL(15,2)                         ! Total weight
Volume               DECIMAL(12,3)                         ! Total volume
Deliveries           ULONG                                 ! Total deliveries
First_DID            ULONG                                 ! Delivery ID
                     END                                   ! 
LOC:DeliveryComposition_Group_2 GROUP,PRE(L_DC2)           ! 
Start_of_Insert      BYTE                                  ! set if new record and user not set anything yet - does default populate
                     END                                   ! 
LOC:Rate_Group_Ret   GROUP,PRE(L_RG)                       ! for retrieving rates
RatePerKg            DECIMAL(10,4)                         ! Rate per Kg
                     END                                   ! 
LOC:GeneralRatesClientID ULONG                             ! Client ID
LOC:Delivery_Leg_Vars GROUP,PRE()                          ! 
L_DLG:VAT            DECIMAL(12,2)                         ! 
L_DLG:Total          DECIMAL(12,2)                         ! 
                     END                                   ! 
LOC:Form_Control_Group GROUP,PRE(L_FCG)                    ! 
DI_Date_Checked      BYTE                                  ! set to 1 on 1st fail, 2nd asks use if correct
                     END                                   ! 
LOC:ReleaseReason    STRING(35)                            ! 
LOC:Str              STRING(600)                           ! 
BRW4::View:Browse    VIEW(DeliveryItems)
                       PROJECT(DELI:ItemNo)
                       PROJECT(DELI:Weight)
                       PROJECT(DELI:ContainerNo)
                       PROJECT(DELI:ContainerVessel)
                       PROJECT(DELI:ETA)
                       PROJECT(DELI:Length)
                       PROJECT(DELI:Breadth)
                       PROJECT(DELI:Height)
                       PROJECT(DELI:DIID)
                       PROJECT(DELI:DID)
                       PROJECT(DELI:ContainerReturnAID)
                       PROJECT(DELI:PTID)
                       PROJECT(DELI:CMID)
                       PROJECT(DELI:CTID)
                       PROJECT(DELI:COID)
                       JOIN(ADD:PKey_AID,DELI:ContainerReturnAID)
                         PROJECT(ADD:AddressName)
                         PROJECT(ADD:AID)
                       END
                       JOIN(PACK:PKey_PTID,DELI:PTID)
                         PROJECT(PACK:PTID)
                       END
                       JOIN(COM:PKey_CMID,DELI:CMID)
                         PROJECT(COM:Commodity)
                         PROJECT(COM:CMID)
                       END
                       JOIN(CTYP:PKey_CTID,DELI:CTID)
                         PROJECT(CTYP:ContainerType)
                         PROJECT(CTYP:CTID)
                       END
                       JOIN(CONO:PKey_COID,DELI:COID)
                         PROJECT(CONO:ContainerOperator)
                         PROJECT(CONO:COID)
                       END
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:ItemsContainers
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
DELI:Weight            LIKE(DELI:Weight)              !List box control field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !List box control field - type derived from field
DELI:ContainerVessel   LIKE(DELI:ContainerVessel)     !List box control field - type derived from field
DELI:ETA               LIKE(DELI:ETA)                 !List box control field - type derived from field
CTYP:ContainerType     LIKE(CTYP:ContainerType)       !List box control field - type derived from field
CONO:ContainerOperator LIKE(CONO:ContainerOperator)   !List box control field - type derived from field
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
DELI:Length            LIKE(DELI:Length)              !List box control field - type derived from field
DELI:Breadth           LIKE(DELI:Breadth)             !List box control field - type derived from field
DELI:Height            LIKE(DELI:Height)              !List box control field - type derived from field
L_IG:MID               LIKE(L_IG:MID)                 !List box control field - type derived from local data
DELI:DIID              LIKE(DELI:DIID)                !Primary key field - type derived from field
DELI:DID               LIKE(DELI:DID)                 !Browse key field - type derived from field
ADD:AID                LIKE(ADD:AID)                  !Related join file key field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Related join file key field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Related join file key field - type derived from field
CTYP:CTID              LIKE(CTYP:CTID)                !Related join file key field - type derived from field
CONO:COID              LIKE(CONO:COID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW20::View:Browse   VIEW(DeliveryItemAlias2)
                       PROJECT(A_DELI2:ItemNo)
                       PROJECT(A_DELI2:Units)
                       PROJECT(A_DELI2:Weight)
                       PROJECT(A_DELI2:Volume)
                       PROJECT(A_DELI2:VolumetricWeight)
                       PROJECT(A_DELI2:Length)
                       PROJECT(A_DELI2:Breadth)
                       PROJECT(A_DELI2:Height)
                       PROJECT(A_DELI2:DIID)
                       PROJECT(A_DELI2:DID)
                       PROJECT(A_DELI2:PTID)
                       PROJECT(A_DELI2:CMID)
                       JOIN(PACK:PKey_PTID,A_DELI2:PTID)
                         PROJECT(PACK:Packaging)
                         PROJECT(PACK:PTID)
                       END
                       JOIN(COM:PKey_CMID,A_DELI2:CMID)
                         PROJECT(COM:Commodity)
                         PROJECT(COM:CMID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?Browse:ItemsLoose
A_DELI2:ItemNo         LIKE(A_DELI2:ItemNo)           !List box control field - type derived from field
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
PACK:Packaging         LIKE(PACK:Packaging)           !List box control field - type derived from field
A_DELI2:Units          LIKE(A_DELI2:Units)            !List box control field - type derived from field
A_DELI2:Weight         LIKE(A_DELI2:Weight)           !List box control field - type derived from field
A_DELI2:Volume         LIKE(A_DELI2:Volume)           !List box control field - type derived from field
A_DELI2:VolumetricWeight LIKE(A_DELI2:VolumetricWeight) !List box control field - type derived from field
A_DELI2:Length         LIKE(A_DELI2:Length)           !List box control field - type derived from field
A_DELI2:Breadth        LIKE(A_DELI2:Breadth)          !List box control field - type derived from field
A_DELI2:Height         LIKE(A_DELI2:Height)           !List box control field - type derived from field
L_IG:MID               LIKE(L_IG:MID)                 !List box control field - type derived from local data
A_DELI2:DIID           LIKE(A_DELI2:DIID)             !List box control field - type derived from field
A_DELI2:DID            LIKE(A_DELI2:DID)              !Browse key field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Related join file key field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(DeliveryLegs)
                       PROJECT(DELL:Leg)
                       PROJECT(DELL:Cost)
                       PROJECT(DELL:VATRate)
                       PROJECT(DELL:DLID)
                       PROJECT(DELL:DID)
                       PROJECT(DELL:JID)
                       PROJECT(DELL:TID)
                       JOIN(JOU:PKey_JID,DELL:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                       JOIN(TRA:PKey_TID,DELL:TID)
                         PROJECT(TRA:TransporterName)
                         PROJECT(TRA:TID)
                       END
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?Browse:6
DELL:Leg               LIKE(DELL:Leg)                 !List box control field - type derived from field
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
L_DLG:Total            LIKE(L_DLG:Total)              !List box control field - type derived from local data
DELL:Cost              LIKE(DELL:Cost)                !List box control field - type derived from field
L_DLG:VAT              LIKE(L_DLG:VAT)                !List box control field - type derived from local data
DELL:VATRate           LIKE(DELL:VATRate)             !List box control field - type derived from field
DELL:DLID              LIKE(DELL:DLID)                !Primary key field - type derived from field
DELL:DID               LIKE(DELL:DID)                 !Browse key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(DeliveryProgress)
                       PROJECT(DELP:StatusDate)
                       PROJECT(DELP:ActionDate)
                       PROJECT(DELP:StatusTime)
                       PROJECT(DELP:ActionTime)
                       PROJECT(DELP:DPID)
                       PROJECT(DELP:DID)
                       PROJECT(DELP:ACID)
                       PROJECT(DELP:DSID)
                       JOIN(ADDC:PKey_ACID,DELP:ACID)
                         PROJECT(ADDC:ContactName)
                         PROJECT(ADDC:ACID)
                       END
                       JOIN(DELS:PKey_DSID,DELP:DSID)
                         PROJECT(DELS:DeliveryStatus)
                         PROJECT(DELS:DSID)
                       END
                     END
Queue:Browse:8       QUEUE                            !Queue declaration for browse/combo box using ?Browse:8
DELS:DeliveryStatus    LIKE(DELS:DeliveryStatus)      !List box control field - type derived from field
ADDC:ContactName       LIKE(ADDC:ContactName)         !List box control field - type derived from field
DELP:StatusDate        LIKE(DELP:StatusDate)          !List box control field - type derived from field
DELP:ActionDate        LIKE(DELP:ActionDate)          !List box control field - type derived from field
DELP:StatusTime        LIKE(DELP:StatusTime)          !List box control field - type derived from field
DELP:ActionTime        LIKE(DELP:ActionTime)          !List box control field - type derived from field
DELP:DPID              LIKE(DELP:DPID)                !Primary key field - type derived from field
DELP:DID               LIKE(DELP:DID)                 !Browse key field - type derived from field
ADDC:ACID              LIKE(ADDC:ACID)                !Related join file key field - type derived from field
DELS:DSID              LIKE(DELS:DSID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB27::View:FileDrop VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FID)
                     END
Queue:FileDrop       QUEUE                            !
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DEL:Record  LIKE(DEL:RECORD),THREAD
BRW4::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW4::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW4::PopupChoice    SIGNED                       ! Popup current choice
BRW4::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW4::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW20::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW20::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW20::PopupChoice   SIGNED                       ! Popup current choice
BRW20::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW20::PopupChoiceExec BYTE(0)                    ! Popup executed
BRW6::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW6::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW6::PopupChoice    SIGNED                       ! Popup current choice
BRW6::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW6::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW8::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW8::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW8::PopupChoice    SIGNED                       ! Popup current choice
BRW8::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW8::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Deliveries'),AT(,,629,369),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdateDeliveries'),SYSTEM,TIMER(50)
                       SHEET,AT(4,2,621,342),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           STRING(@n_10),AT(365,22,43),USE(DEL:DID),RIGHT(1),TRN
                           PROMPT('DID:'),AT(349,22),USE(?DEL:BID:Prompt:2),TRN
                           PROMPT('DI No.:'),AT(9,22),USE(?DEL:DINo:Prompt),TRN
                           ENTRY(@n_10b),AT(65,22,70,10),USE(DEL:DINo),RIGHT(1),MSG('Delivery Instruction Number'),REQ, |
  TIP('Delivery Instruction Number')
                           STRING('Branch'),AT(142,22,59,10),USE(?String_Branch),TRN
                           PROMPT('DI Date:'),AT(217,22),USE(?DEL:DIDate:Prompt),TRN
                           SPIN(@d6),AT(270,22,70,10),USE(DEL:DIDate),RIGHT(1),MSG('DI Date'),REQ,TIP('DI Date')
                           BUTTON('...'),AT(254,22,12,10),USE(?Calendar)
                           PROMPT('Client:'),AT(9,36),USE(?LOC:ClientName:Prompt),TRN
                           BUTTON('...'),AT(49,36,12,10),USE(?CallLookup_Client),KEY(F2Key),TIP('Press F2 to access')
                           ENTRY(@s100),AT(65,36,93,10),USE(L_LG:ClientName),REQ,TIP('Client name')
                           BUTTON('www'),AT(35,36,12,10),USE(?Button_ClientEmails),FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI), |
  LEFT,ICON('Icon0443.ico'),TIP('Client email addresses')
                           ENTRY(@n_10b),AT(161,36,41,10),USE(L_LG:ClientNo),RIGHT(1),MSG('Client ID'),REQ,TIP('Client ID')
                           PROMPT('Client Ref.:'),AT(217,36),USE(?DEL:ClientReference:Prompt),TRN
                           ENTRY(@s60),AT(270,36,70,10),USE(DEL:ClientReference),LEFT(1),MSG('Client Reference'),TIP('Client Reference')
                           PROMPT('CID:'),AT(349,36),USE(?DEL:CID:Prompt),TRN
                           STRING(@n_10),AT(365,36,43),USE(DEL:CID),RIGHT(1),TRN
                           GROUP,AT(9,52,193,10),USE(?Group_LoadType)
                             PROMPT('Load Type:'),AT(9,52),USE(?LoadType:Prompt),TRN
                             BUTTON('...'),AT(49,52,12,10),USE(?CallLookup_LoadType),KEY(F3Key),TIP('Press F3 to access')
                             ENTRY(@s100),AT(65,52,137,10),USE(L_LG:LoadType),MSG('Load Type'),REQ,TIP('Load Type')
                           END
                           BUTTON(':'),AT(209,52,5,10),USE(?Button_ListRates),TIP('List the Clients Rates')
                           PROMPT('Rate Type:'),AT(217,52),USE(?ClientRateType:Prompt),TRN
                           BUTTON('...'),AT(254,52,12,10),USE(?CallLookup)
                           ENTRY(@s100),AT(270,52,137,10),USE(L_LG:ClientRateType),MSG('Client Rate Type'),REQ,TIP('Client Rate Type')
                           PROMPT('Journey:'),AT(9,68),USE(?Journey:Prompt),TRN
                           BUTTON('...'),AT(49,68,12,10),USE(?CallLookup_Journey),KEY(F4Key),TIP('Press F4 to access')
                           ENTRY(@s70),AT(65,68,137,10),USE(L_LG:Journey),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  MSG('Journey'),REQ,TIP('Journey')
                           PROMPT('Floor:'),AT(217,68),USE(?Floor:Prompt),TRN
                           BUTTON('...'),AT(254,68,12,10),USE(?CallLookup_Floor)
                           ENTRY(@s35),AT(270,68,137,10),USE(L_LG:Floor),MSG('Floor Name'),REQ,TIP('Floor Name')
                           BUTTON('...'),AT(49,82,12,10),USE(?CallLookup_CollectAdd),KEY(F5Key),TIP('Press F5 to access')
                           PROMPT('Collect:'),AT(9,82),USE(?From_Address:Prompt),TRN
                           ENTRY(@s35),AT(65,82,137,10),USE(L_LG:From_Address),MSG('Collection Address Name'),REQ,TIP('Collection' & |
  ' Address Name')
                           BUTTON('...'),AT(254,82,12,10),USE(?CallLookup_ToAdd),KEY(F6Key)
                           PROMPT('Deliver:'),AT(217,82),USE(?To_Address:Prompt),TRN
                           ENTRY(@s35),AT(270,82,137,10),USE(L_LG:To_Address),MSG('Delivery Address Name'),REQ,TIP('Delivery A' & |
  'ddress Name')
                           BUTTON('www'),AT(49,22,12,10),USE(?Button_Release),FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI), |
  LEFT,ICON('dotred.ico'),HIDE,TIP('Release this DI')
                           PROMPT('Driver:'),AT(9,98),USE(?Driver:Prompt),TRN
                           BUTTON('...'),AT(49,98,12,10),USE(?CallLookup_Driver_Name),KEY(F7Key),TIP('Press F7 to access')
                           ENTRY(@s70),AT(65,98,93,10),USE(L_LG:Driver_Name),MSG('Name of Driver'),TIP('Name of Dr' & |
  'iver collected by')
                           BUTTON('COD'),AT(169,98,,10),USE(?Button_COD_Address),FONT(,,,FONT:bold,CHARSET:ANSI),KEY(F8Key), |
  DISABLE,TIP('Specify a COD Address - F8 key<0DH,0AH>This will be used on the Invoice')
                           PROMPT('Service:'),AT(217,98),USE(?ServiceRequirement:Prompt),TRN
                           BUTTON('...'),AT(254,98,12,10),USE(?CallLookup_ServiceReq),KEY(F9Key),TIP('Press F9 to access')
                           ENTRY(@s35),AT(270,98,137,10),USE(L_LG:ServiceRequirement),MSG('Service Requirement'),REQ, |
  TIP('Service Requirement')
                           STRING(@n_10),AT(76,300,33,10),USE(DEL:BID),RIGHT(1),HIDE,TRN
                           PROMPT('BID:'),AT(60,300),USE(?DEL:BID:Prompt),HIDE,TRN
                           PROMPT('Email Addresses:'),AT(9,115),USE(?DEL:NoticeEmailAddresses:Prompt)
                           BUTTON('...'),AT(68,114,12,10),USE(?Lookup_EmailAddresses),KEY(F12Key),TIP('Press F12 to access')
                           TEXT,AT(10,144,167,34),USE(DEL:SpecialDeliveryInstructions),BOXED,MSG('This will print ' & |
  'on the DI and Delivery Note (& POD)'),TIP('This will print on the DI and Delivery No' & |
  'te (& POD)')
                           PROMPT('Special Delivery Instructions:'),AT(9,132),USE(?DEL:SpecialDeliveryInstructions:Prompt), |
  TRN
                           TEXT,AT(218,144,219,50),USE(DEL:Notes),VSCROLL,BOXED,MSG('General Notes'),TIP('General Notes')
                           PROMPT('Notes:'),AT(217,132),USE(?DEL:Notes:Prompt),TRN
                           ENTRY(@s255),AT(85,115,528,10),USE(DEL:NoticeEmailAddresses),MSG('Email addresses to se' & |
  'nd notices to of progress of DI'),TIP('Email addresses to send notices to of progress of DI')
                           LINE,AT(11,201,612,0),USE(?Line_diitem_div),COLOR(COLOR:Black),LINEWIDTH(2)
                           SHEET,AT(7,204,613,108),USE(?Sheet_Items),DISABLE,BELOW
                             TAB('General'),USE(?Tab_Loose)
                               LIST,AT(11,207,603,84),USE(?Browse:ItemsLoose),HVSCROLL,KEY(F10Key),FORMAT('32R(2)|M~It' & |
  'em No.~C(0)@n6@60L(2)|M~Commodity~C(0)@s35@60L(2)|M~Packaging~C(0)@s35@26R(2)|M~Unit' & |
  's~C(0)@n6@44R(2)|M~Weight~C(0)@n-11.2@44R(2)|M~Volume~C(0)@n-11.3@62R(2)|M~Volumetri' & |
  'c Weight~C(0)@n-11.2@28R(2)|M~Length~C(0)@n6@30R(2)|M~Breadth~C(0)@n6@30R(2)|M~Heigh' & |
  't~C(0)@n6.2@60L(2)|M~MID List~C(0)@s50@40R(2)|M~DIID~C(0)@n_10@'),FROM(Queue:Browse),IMM, |
  MSG('Browsing the DeliveryProgress file')
                               BUTTON('&View'),AT(399,300,53,14),USE(?View:2),LEFT,ICON('WAview.ICO'),FLAT,TIP('View (General)')
                               BUTTON('&Insert'),AT(459,300,49,14),USE(?Insert),LEFT,ICON('WAINSERT.ICO'),FLAT
                               BUTTON('&Change'),AT(512,300,49,14),USE(?Change),LEFT,ICON('WACHANGE.ICO'),FLAT
                               BUTTON('&Delete'),AT(566,300,49,14),USE(?Delete),LEFT,ICON('WADELETE.ICO'),FLAT
                             END
                             TAB('Containers'),USE(?Tab_Containers)
                               LIST,AT(11,207,603,86),USE(?Browse:ItemsContainers),HVSCROLL,FORMAT('32R(2)|M~Item No.~' & |
  'C(0)@n6@50L(2)|M~Commodity~C(0)@s35@44R(2)|M~Weight~C(0)@n-11.2@50L(2)|M~Container N' & |
  'o.~C(0)@s35@50L(2)|M~Vessel~C(0)@s35@38R(2)|M~ETA~C(0)@d5@54L(2)|M~Container Type~C(' & |
  '0)@s35@66L(2)|M~Container Operator~C(0)@s35@56L(2)|M~Turn In Address~C(0)@s35@28R(2)' & |
  '|M~Length~C(0)@n6@30R(2)|M~Breadth~C(0)@n6@26R(2)|M~Height~C(0)@n6@40L(2)|M~L IG : M' & |
  'ID~C(0)@s50@'),FROM(Queue:Browse:4),IMM,MSG('Browsing the DeliveryProgress file')
                               BUTTON('&View'),AT(401,299,53,14),USE(?View),LEFT,ICON('WAview.ICO'),FLAT,TIP('View (Containers)')
                               BUTTON('&Insert'),AT(458,299,49,14),USE(?Insert:5),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record (c)')
                               BUTTON('&Change'),AT(513,299,49,14),USE(?Change:5),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record (c)')
                               BUTTON('&Delete'),AT(569,299,49,14),USE(?Delete:5),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record (c)')
                             END
                           END
                           BUTTON('&Tabs'),AT(163,298,48,14),USE(?Button_Wizard),LEFT,ICON('�<02H,011H,07FH>'),FLAT,SKIP
                           BUTTON('View Associated Docs.'),AT(534,22),USE(?BUTTON_viewdocs),TIP('View DI associate' & |
  'd documents')
                           PROMPT('Chars Left:'),AT(10,180),USE(?L_SG:SpecialDeliveryCharsLeft:Prompt)
                           STRING(@n-14),AT(109,180),USE(L_SG:SpecialDeliveryCharsLeft),RIGHT(1)
                           BUTTON('Manifest'),AT(534,40,86,14),USE(?BUTTON_Manifest),DISABLE
                           BUTTON('Tripsheet'),AT(534,57,86,14),USE(?BUTTON_Tripsheet),DISABLE
                           BUTTON('I&nvoice'),AT(534,75,86,14),USE(?Button_View_Invoice),DISABLE
                         END
                         TAB('&2) Charges'),USE(?Tab_Charges)
                           CHECK(' &Insure'),AT(85,22,43,10),USE(DEL:Insure),MSG('Insure'),TIP('Insure the goods o' & |
  'n this DI.<0DH,0AH>Note this is in addition to any insurance that may be part of thi' & |
  's clients rate.'),TRN
                           PROMPT('Clients Terms:'),AT(282,22),USE(?L_CG:ClientTerms:Prompt),TRN
                           LIST,AT(337,22,71,10),USE(L_CG:ClientTerms),DISABLE,DROP(5),FROM('Pre Paid|#0|COD|#1|Ac' & |
  'count|#2|On Statement|#3'),MSG('Terms - Pre Paid, COD, Account'),TIP('Terms - Pre Pa' & |
  'id, COD, Account')
                           GROUP,AT(9,20,176,25),USE(?Group_Insured)
                             PROMPT('Consignment Value:'),AT(9,22),USE(?DEL:TotalConsignmentValue:Prompt),TRN
                             ENTRY(@n-15.2),AT(133,22,52,10),USE(DEL:TotalConsignmentValue),DECIMAL(12),MSG('Total valu' & |
  'e of the cargo on this DI - required if additional insurance required'),TIP('Total valu' & |
  'e of the cargo on this DI - required if additional insurance required')
                             PROMPT('Insurance Rate:'),AT(9,34),USE(?DEL:InsuranceRate:Prompt),TRN
                             ENTRY(@N-11.6~%~),AT(133,34,52,10),USE(DEL:InsuranceRate),RIGHT(1),MSG('Insurance rate per ton'), |
  TIP('Insurance rate per ton')
                           END
                           PROMPT('Insuance Amount:'),AT(9,50),USE(?InsuanceAmount:Prompt),TRN
                           ENTRY(@n-14.2),AT(133,50,52,10),USE(L_CC:InsuanceAmount),DECIMAL(12),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           CHECK(' Generate Invoice'),AT(337,36),USE(L_CG:GenerateInvoice),MSG('Generate an invoic' & |
  'e when DI created'),TIP('Generate an invoice when DI created'),TRN
                           PROMPT('+'),AT(190,50),USE(?Prompt25),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           BUTTON('Reverse Calculation'),AT(217,50,75,10),USE(?Button_ReverseCalc),TIP('You specif' & |
  'y the Insurance Amount - we''ll calculate the rate')
                           PROMPT('&Rate:'),AT(9,82),USE(?DEL:Rate:Prompt),TRN
                           ENTRY(@n-11.3),AT(133,82,52,10),USE(DEL:Rate),RIGHT(1),MSG('Rate used on this DI'),TIP('Rate used ' & |
  'on this DI<0DH,0AH>Charge will be this rate multiplied by the total weight in kgs.')
                           BUTTON('&Get Rate'),AT(74,82,53,14),USE(?Button_GetRate)
                           BUTTON('&List Rates'),AT(74,96,53,14),USE(?Button_ShowRates)
                           PROMPT('Total Weight:'),AT(282,82),USE(?L_CC:TotalWeight:Prompt),TRN
                           ENTRY(@n-14.1),AT(350,82,52,10),USE(L_CC:TotalWeight),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP
                           GROUP,AT(258,100,143,10),USE(?Group_RateFloor),DISABLE
                             PROMPT('Rate Floor:'),AT(278,100),USE(?FloorRate:Prompt:2),TRN
                             LIST,AT(330,100,71,10),USE(L_LG:FloorRate),VSCROLL,DROP(15,100),FORMAT('140L(2)|M~Floor~@s35@'), |
  FROM(Queue:FileDrop)
                           END
                           PANEL,AT(133,96,275,46),USE(?Panel1),BEVEL(-1,-1),FILL(00E9E9E9h)
                           STRING('Rate Information'),AT(137,100,138,10),USE(?String_Rate_Info),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           CHECK(' This is a Setup Rate'),AT(190,82),USE(L_RG:Setup_Rate),DISABLE,TRN
                           PROMPT('Rate Upper Weight:'),AT(278,114),USE(?To_Weight:Prompt),TRN
                           SPIN(@n-12.0),AT(350,114,52,10),USE(L_RG:To_Weight),RIGHT(1),COLOR(00E9E9E9h),DISABLE,MSG('Up to this' & |
  ' mass in Kgs'),TIP('Up to this mass in Kgs')
                           PROMPT('Rate Effective Date:'),AT(137,114),USE(?Effective_Date:Prompt),TRN
                           SPIN(@d5),AT(217,114,52,10),USE(L_RG:Effective_Date),RIGHT(1),COLOR(00E9E9E9h),DISABLE,MSG('Effective ' & |
  'from this date'),READONLY,SKIP,TIP('Effective from this date')
                           PROMPT('Min. Charge Client:'),AT(137,128),USE(?L_CG:MinimiumCharge:Prompt),TRN
                           ENTRY(@n-11.2),AT(217,128,52,10),USE(L_CG:MinimiumCharge),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           PROMPT('Min. Charge - Rate:'),AT(278,128),USE(?L_CC:MinimiumChargeRate:Prompt),TRN
                           ENTRY(@n-14.3),AT(350,128,52,10),USE(L_CC:MinimiumChargeRate),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           BUTTON('Additional Charges'),AT(217,152,75,10),USE(?Button_AdditionalCharges)
                           CHECK(' Calculate Additional Charge'),AT(302,152),USE(DEL:AdditionalCharge_Calculate),MSG('Calculate ' & |
  'the Additional Charge'),TIP('Calculate the Additional Charge'),TRN
                           PROMPT('Additional Charge Total:'),AT(9,152),USE(?DEL:AdditionalCharge:Prompt),TRN
                           ENTRY(@n-15.2),AT(133,152,52,10),USE(DEL:AdditionalCharge),DECIMAL(12),MSG('Addi'),TIP('Addi')
                           PROMPT('&Charge (based on rate):'),AT(9,170),USE(?DEL:Charge:Prompt),TRN
                           ENTRY(@n-13.2),AT(133,170,52,10),USE(DEL:Charge),RIGHT(1),MSG('Charge for the DI'),TIP('Charge for' & |
  ' the DI<0DH,0AH>Excludes VAT, Docs, Fuel and Insurance charges')
                           PROMPT('+'),AT(190,170),USE(?Prompt25:3),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           PROMPT('Leg costs'),AT(217,170,191,10),USE(?Prompt_LegCosts),FONT(,,COLOR:Black,FONT:bold, |
  CHARSET:ANSI),HIDE,TRN
                           BUTTON('<<>'),AT(334,204,12,10),USE(?Button_ChangeVAT),TIP('Change the VAT Rate')
                           PROMPT('Document Charge:'),AT(9,190),USE(?DEL:DocumentCharge:Prompt),TRN
                           ENTRY(@n-11.2),AT(133,190,52,10),USE(DEL:DocumentCharge),RIGHT(1),COLOR(00E9E9E9h),MSG('Document Charge'), |
  READONLY,SKIP,TIP('Document Charge applied to this DI<0DH,0AH>If you need to change t' & |
  'his please ask a supervisor')
                           PROMPT('+'),AT(190,190),USE(?Prompt25:2),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           BUTTON('Change'),AT(85,190,40,10),USE(?Button_Change_Doc),DISABLE
                           PROMPT('Fuel Surcharge:'),AT(9,204),USE(?DEL:FuelSurcharge:Prompt),TRN
                           PROMPT('VAT Rate:'),AT(285,204),USE(?DEL:VATRate:Prompt),TRN
                           ENTRY(@n7.2),AT(350,204,52,10),USE(DEL:VATRate),DECIMAL(12),COLOR(00E9E9E9h),MSG('VAT Rate'), |
  READONLY,SKIP,TIP('VAT Rate for this delivery')
                           ENTRY(@n-14.2~%~),AT(85,204,40,10),USE(L_CC:FuelSurchargePer),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           ENTRY(@N-11.2),AT(133,204,52,10),USE(DEL:FuelSurcharge),RIGHT(1),COLOR(00E9E9E9h),MSG('Fuel Surcharge'), |
  READONLY,SKIP,TIP('Fuel Surcharge applied to this DI')
                           PROMPT('+'),AT(190,204),USE(?Prompt25:4),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           PROMPT('Toll Rate:'),AT(9,218),USE(?DEL:TollRate:Prompt)
                           ENTRY(@n-9.2~%~),AT(85,218,40,10),USE(DEL:TollRate),RIGHT(1),COLOR(00E9E9E9h),MSG('Rate Toll ' & |
  'charged at as a percent of the Freight charge'),READONLY,SKIP,TIP('Rate Toll charged' & |
  ' at as a percent of the Freight charge')
                           ENTRY(@n-11.2),AT(133,218,52,10),USE(DEL:TollCharge),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('+'),AT(190,218),USE(?Prompt25:6),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           PROMPT('Total Charge (excl):'),AT(9,236),USE(?TotalCharge:Prompt),FONT(,,COLOR:Black,FONT:bold, |
  CHARSET:ANSI),TRN
                           ENTRY(@n-13.2),AT(133,236,52,10),USE(L_CC:TotalCharge),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Charge for the DI (additional legs have own charg' & |
  'es specified)'),READONLY,SKIP,TIP('Charge for the DI (additional legs have own charg' & |
  'es specified)<0DH,0AH>Excludes VAT')
                           PROMPT('='),AT(190,236),USE(?Prompt25:5),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           PROMPT('VAT:'),AT(285,218),USE(?VAT:Prompt),TRN
                           ENTRY(@n-13.2),AT(350,218,52,10),USE(L_CC:VAT),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Charge for the DI'), |
  READONLY,SKIP,TIP('Charge for the DI<0DH,0AH>Excludes VAT, Docs, Fuel and Insurance charges')
                           PROMPT('Total Charge (incl):'),AT(285,236),USE(?TotalCharge:Prompt:2),TRN
                           ENTRY(@n-13.2),AT(350,236,52,10),USE(L_CC:TotalCharge_VAT),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Charge for the DI'),READONLY,SKIP,TIP('Charge for the DI<0DH,0AH>Excludes VAT, D' & |
  'ocs, Fuel and Insurance charges')
                         END
                         TAB('&3) Delivery Legs, Progress, Special Instr. && Notes'),USE(?Tab:6)
                           PROMPT('Delivery Legs'),AT(189,20),USE(?Prompt19:2),TRN
                           LINE,AT(181,20,0,148),USE(?Line2),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Delivery Progress'),AT(9,20),USE(?Prompt19),TRN
                           LIST,AT(9,32,167,70),USE(?Browse:8),HVSCROLL,FORMAT('60L(2)|M~Status~C(0)@s35@52L(2)|M~' & |
  'Contact Name~C(0)@s35@42R(2)|M~Status Date~C(0)@d5@42R(2)|M~Action Date~C(0)@d5b@42R' & |
  '(2)|M~Status Time~C(0)@t7@42R(2)|M~Action Time~C(0)@t7@'),FROM(Queue:Browse:8),IMM,MSG('Browsing t' & |
  'he DeliveryProgress file')
                           BUTTON('&Insert'),AT(21,104,49,14),USE(?Insert:9),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(73,104,49,14),USE(?Change:9),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(126,104,49,14),USE(?Delete:9),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                           PROMPT('Branch:'),AT(9,130),USE(?DEL:Manifested:Prompt:2),TRN
                           BUTTON('Change'),AT(41,142,50,10),USE(?Button_ChangeBranch),TIP('Change the Branch on t' & |
  'his DI & any associated Invoice')
                           PROMPT('Delivered:'),AT(189,152),USE(?DEL:Delivered:Prompt),TRN
                           LIST,AT(250,152,101,10),USE(DEL:Delivered),DISABLE,DROP(15),FROM('Not Delivered|#0|Part' & |
  'ially Delivered|#1|Delivered|#2|Delivered (manual)|#3'),MSG('Delivered Status'),TIP('Delivered Status')
                           STRING('User'),AT(41,154,101,10),USE(?String_User),TRN
                           PROMPT('User:'),AT(9,154),USE(?Prompt63),TRN
                           STRING('Branch'),AT(41,130,101,10),USE(?String_Branch:2),TRN
                           LIST,AT(189,32,219,70),USE(?Browse:6),HVSCROLL,FORMAT('18R(2)|M~Leg~C(0)@n6@60L(2)|M~Tr' & |
  'ansporter~C(0)@s35@60L(2)|M~Journey~C(0)@s70@38R(1)|M~Total~C(0)@n-17.2@50R(1)|M~Cos' & |
  't~C(0)@n-13.2@38R(1)|M~VAT~C(0)@n-17.2@38R(1)|M~VAT Rate~C(0)@n-7.2@'),FROM(Queue:Browse:6), |
  IMM,MSG('Browsing the DeliveryProgress file')
                           BUTTON('&Insert'),AT(253,104,49,14),USE(?Insert:7),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(305,104,49,14),USE(?Change:7),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(358,104,49,14),USE(?Delete:7),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                           LINE,AT(7,121,225,0),USE(?Line3:4),COLOR(COLOR:Black),LINEWIDTH(2)
                           LINE,AT(183,121,225,0),USE(?Line3:2),COLOR(COLOR:Black),LINEWIDTH(2)
                           CHECK(' &Multiple Manifests Allowed'),AT(250,126),USE(DEL:MultipleManifestsAllowed),MSG('Allow this' & |
  ' DI to be manifested on multiple manifests'),TIP('Allow this DI to be manifested on ' & |
  'multiple manifests'),TRN
                           PROMPT('Manifested:'),AT(189,138),USE(?DEL:Manifested:Prompt),TRN
                           LIST,AT(250,138,101,10),USE(DEL:Manifested),VSCROLL,DISABLE,DROP(15),FROM('Not Manifest' & |
  'ed|#0|Partially Manifested|#1|Partially Manifested Multiple|#2|Fully Manifested|#3|F' & |
  'ully Manifested Multiple|#4'),MSG('Manifested status'),TIP('Manifested status')
                           BUTTON('&Update'),AT(357,138,50,25),USE(?Button_UpdateManifested)
                           LINE,AT(10,166,174,0),USE(?Line3),COLOR(COLOR:Black),LINEWIDTH(2)
                           LINE,AT(183,166,225,0),USE(?Line3:3),COLOR(COLOR:Black),LINEWIDTH(2)
                         END
                         TAB('&4) Multi Part'),USE(?Tab_Multi)
                           GROUP,AT(17,112,311,24),USE(?Group_Captured)
                             PROMPT('Items:'),AT(229,112),USE(?DELC:Items:Prompt:2),TRN
                             ENTRY(@n13),AT(290,112,65,10),USE(L_DC:Items),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total no. of items'), |
  SKIP,TIP('Total no. of items')
                             PROMPT('Weight:'),AT(17,112),USE(?DELC:Weight:Prompt:2),TRN
                             ENTRY(@n-21.2),AT(78,112,65,10),USE(L_DC:Weight),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total weight'), |
  SKIP,TIP('Total weight')
                             PROMPT('Deliveries:'),AT(229,126),USE(?DELC:Deliveries:Prompt:2),TRN
                             ENTRY(@n13),AT(290,126,65,10),USE(L_DC:Deliveries),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total no. ' & |
  'of deliveries'),SKIP,TIP('Total no. of deliveries')
                             PROMPT('Volume:'),AT(17,126),USE(?DELC:Volume:Prompt:2),TRN
                             ENTRY(@n-16.3),AT(78,126,65,10),USE(L_DC:Volume),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total volume'), |
  SKIP,TIP('Total volume'),TRN
                           END
                           GROUP,AT(17,54,311,24),USE(?Group_MultiTotals)
                             PROMPT('Weight:'),AT(17,54),USE(?DELC:Weight:Prompt),TRN
                             ENTRY(@n-21.2),AT(78,54,65,10),USE(DELC:Weight),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total weight'), |
  SKIP,TIP('Total weight')
                             PROMPT('Volume:'),AT(17,68),USE(?DELC:Volume:Prompt),TRN
                             ENTRY(@n-16.3),AT(78,68,65,10),USE(DELC:Volume),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total volume'), |
  SKIP,TIP('Total volume')
                           END
                           PANEL,AT(10,38,396,106),USE(?Panel2),BEVEL(-1,-1)
                           PROMPT('Multi-Part Delivery'),AT(78,42),USE(?Prompt50),FONT(,,,FONT:bold+FONT:underline,CHARSET:ANSI), |
  TRN
                           LINE,AT(18,87,382,0),USE(?Line5),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Deliveries Captured To Date'),AT(78,98),USE(?Prompt50:2),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           BUTTON('Specify Multi-Part'),AT(78,20,,12),USE(?Button_Specify)
                           PROMPT('This Delivery is part of a Multi Part delivery.'),AT(153,22,251,10),USE(?Prompt_Multi), |
  FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         END
                         TAB('DB'),USE(?Tab_DB),HIDE
                           BUTTON('Display'),AT(65,29,95,14),USE(?Button_DB_Display)
                           PROMPT('CRTID:'),AT(65,51),USE(?DEL:CRTID:Prompt),TRN
                           STRING(@n_10),AT(117,51),USE(DEL:CRTID),RIGHT(1),TRN
                           PROMPT('LTID:'),AT(65,63),USE(?DEL:LTID:Prompt),TRN
                           STRING(@n_10),AT(117,63),USE(DEL:LTID),RIGHT(1),TRN
                         END
                       END
                       BUTTON(':'),AT(528,4,9,10),USE(?Button_Value),FONT(,8),TIP('Set Charge option to show here')
                       ENTRY(@n-15.2),AT(569,4,50,10),USE(L_SG:Value),FONT(,,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Charge for the DI (additional legs have own charg' & |
  'es specified)'),READONLY,SKIP,TIP('Charge for the DI (additional legs have own charg' & |
  'es specified)<0DH,0AH>Excludes VAT, Docs and Fuel charges')
                       PROMPT(''),AT(7,316,615,20),USE(?Prompt_Manifest),FONT(,,COLOR:Red,,CHARSET:ANSI)
                       PROMPT('Charge:'),AT(541,4),USE(?Prompt_Value),TRN
                       BUTTON('&OK'),AT(513,346,49,20),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('Cancel'),AT(567,346,,20),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('Update PODs'),AT(4,351),USE(?BUTTON_UpdatePods)
                     END

BRW4::LastSortOrder       BYTE
BRW20::LastSortOrder       BYTE
BRW6::LastSortOrder       BYTE
BRW8::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW_DeliveryItems    CLASS(BrowseClass)                    ! Browse using ?Browse:ItemsContainers
Q                      &Queue:Browse:4                !Reference to browse queue
AskRecord              PROCEDURE(BYTE Request),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW4::Sort0:StepClass StepLongClass                        ! Default Step Manager
BRW_DelItemsAlias    CLASS(BrowseClass)                    ! Browse using ?Browse:ItemsLoose
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW20::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW6                 CLASS(BrowseClass)                    ! Browse using ?Browse:6
Q                      &Queue:Browse:6                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW6::Sort0:StepClass StepLongClass                        ! Default Step Manager
BRW8                 CLASS(BrowseClass)                    ! Browse using ?Browse:8
Q                      &Queue:Browse:8                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB27                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Calendar22           CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
!ManLD           VIEW(ManifestLoadDeliveries)
!    PROJECT(MALD:DIID, MALD:MLID)
!       JOIN(MAL:PKey_MLID, MALD:MLID)
!       PROJECT(MAL:MLID, MAL:MID)
!    .  .
View_Inv        VIEW(_Invoice)
    PROJECT(INV:IID)
    .

Inv_View        ViewManager
                     MAP

Validate_EmailNotficicationAddresses   PROCEDURE(), LONG
                     .

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
New_Journey                 ROUTINE
    CLEAR(L_CC:Rate_Checked)

    !MESSAGE('new journey Jour: ' & JOU:JID & '      Del JID: ' & DEL:JID)

    JOU:JID    = DEL:JID
    IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
       L_LG:Journey              = JOU:Journey
       L_LG:Journey_TollCharged  = JOU:EToll
    .
               
    ! Here we should check that the journey has actually changed.
    DO Charge_Calc
    DISPLAY

    EXIT
New_Load_Type           ROUTINE
    CLEAR(L_CC:Rate_Checked)

    LOAD2:LTID                  = DEL:LTID
    IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
       ENABLE(?Sheet_Items)

       LOC:Load_Type_Group     :=: LOAD2:Record
!       IF L_LT:ContainerOption = 1 OR L_LT:ContainerOption = 2
!          SELECT(?Tab_Containers)
!       ELSE
!          SELECT(?Tab_Loose)
!       .

       ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
       IF L_LT:ContainerParkStandard = TRUE
          DEL:FIDRate           = LOAD2:FID
          FLO:FID               = DEL:FIDRate
          IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
             L_LG:FloorRate     = FLO:Floor
             DISPLAY(?L_LG:FloorRate)
       .  .
          
       ! Check how many CRTIDs are available for this Load Type LTID for this Client - 3 Oct 12
       DEL:CRTID    = Check_ClientsRateTypes(DEL:CID, DEL:LTID)
       IF DEL:CRTID = 0 AND L_LG:LTID <> LOAD2:LTID       ! we have none and we did change load types, then set to zero
          ! MESSAGE('crtid is 0..')
          
          CLEAR(DEL:CRTID)
          CLEAR(L_LG:ClientRateType)
          CLEAR(L_LG:ClientRateType_CID)
       .
       IF DEL:CRTID ~= 0
          CRT:CRTID         = DEL:CRTID
          IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
             L_LG:ClientRateType        = CRT:ClientRateType
             L_LG:ClientRateType_CID    = CRT:CID
       .  .
          
    ELSE
       DISABLE(?Sheet_Items)
    .

    L_LG:LTID = LOAD2:LTID            ! set last load type seen here
          
    DO Floor_Changed
    DO Charge_Calc
    EXIT
New_Client                    ROUTINE
    ! Check if Client is On Hold
    IF L_CG:ClientStatus = 1
       MESSAGE('This Client is on Hold!', 'Update Deliveries - New Client', ICON:Exclamation)
    ELSIF L_CG:ClientStatus = 2
       MESSAGE('This Client account has been closed!', 'Update Deliveries - New Client', ICON:Hand)
       CLEAR(DEL:CID)
       CLEAR(L_LG:ClientName)
       CLEAR(L_LG:ClientNo)
       SELECT(?L_LG:ClientName)
       EXIT
	.
	
    ! Pre Paid, COD, Account
    IF DEL:Terms < 2
       !DISABLE(?L_LG:To_Address)
       !?L_LG:To_Address{PROP:Req}   = FALSE
       !ENABLE(?Group_COD_Add)
       ENABLE(?Button_COD_Address)
       ?Button_COD_Address{PROP:FontColor}  = COLOR:Red
    ELSE
       !ENABLE(?L_LG:To_Address)
       !?L_LG:To_Address{PROP:Req}   = TRUE
       !DISABLE(?Group_COD_Add)
       DISABLE(?Button_COD_Address)
       ?Button_COD_Address{PROP:FontColor}  = COLOR:None
       CLEAR(DEL:DC_ID)
    .


    IF L_LG:CID ~= DEL:CID                                  ! Client has changed.  This does not execute on Change, Delete unless client changed
       CLEAR(L_CG:MinChargeChecked)
       CLEAR(L_CG:UseMinCharge)
	    CLEAR(L_CC:Rate_Checked)
		
       IF QuickWindow{PROP:AcceptAll}  = FALSE
		   IF CLIP(L_LG:PODMessage) <> ''
		      MESSAGE(CLIP(CLI:PODMessage), 'POD Message', ICON:Exclamation)			 
		   .

          ! Check if 1 Journey - if so load these now as defaults
          DEL:JID              = Check_ClientRates_for_JID(DEL:CID, GETINI('Manifest', 'Default_Manifest_JID', 0, GLO:Local_INI))

          JOU:JID              = DEL:JID
          !MESSAGE('Top - before Journey lookup - DEL:JID: ' & DEL:JID)
          IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
             L_LG:Journey              = JOU:Journey
             L_LG:Journey_TollCharged  = JOU:EToll
          ELSE
             CLEAR(L_LG:Journey)
             CLEAR(L_LG:Journey_TollCharged)
          .

          ! Check if 1 Load Type if so load these now as defaults
          Count_#              = 0
          CLEAR(V_RATC_L:Record)
          V_RATC_L:CID         = DEL:CID
          SET(V_RATC_L:CKey_CID_LTID, V_RATC_L:CKey_CID_LTID)
          LOOP  
             IF Access:_View_Rates_Clients_LoadTypes.TryNext() ~= LEVEL:Benign
                BREAK
             .
             IF V_RATC_L:CID ~= DEL:CID
                BREAK
             .

             IF DEL:LTID ~= V_RATC_L:LTID   ! 1st time and then when differs
                Count_#   += 1
             .            
             
             DEL:LTID   = V_RATC_L:LTID
          .
          IF Count_# > 1
             CLEAR(DEL:LTID)
             CLEAR(L_LG:LoadType)          
          .

          IF DEL:LTID ~= 0             
             LOAD2:LTID        = DEL:LTID
             IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
                L_LG:LoadType  = LOAD2:LoadType
             .
             DO New_Load_Type
          .  

          ! Check Journeys available       
          Count_#              = 0
          CLEAR(V_RCJ:Record)
          V_RCJ:CID            = DEL:CID
          SET(V_RCJ:CKey_CID_JID, V_RCJ:CKey_CID_JID)
          LOOP  
             IF Access:_View_Rates_Client_Journeys.TryNext() ~= LEVEL:Benign
                BREAK
             .
             IF V_RCJ:CID ~= DEL:CID
                BREAK
             .

             IF DEL:JID ~= V_RCJ:JID   ! 1st time and then when differs
                Count_#   += 1
             .            
             
             DEL:JID   = V_RCJ:JID
          .
          IF Count_# > 1
             !MESSAGE('count: ' & Count_#)
             CLEAR(DEL:JID)
             CLEAR(L_LG:Journey)
             CLEAR(L_LG:Journey_TollCharged)
          .

          IF DEL:JID ~= 0
             JOU:JID        = DEL:JID
             !MESSAGE('Journey lookup - DEL:JID: ' & DEL:JID)
             IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
                L_LG:Journey              = JOU:Journey
                L_LG:Journey_TollCharged  = JOU:EToll
          .  .

          L_CC:FuelSurchargePer     = Get_Client_FuelSurcharge(DEL:CID, DEL:DIDate)
          L_LG:TollRate             = Get_Client_ETolls(DEL:CID, DEL:DIDate)     
       .

       ! Check how many CRTIDs are available for this Load Type LTID for this Client
       DEL:CRTID    = Check_ClientsRateTypes(DEL:CID, DEL:LTID)
       IF DEL:CRTID = 0
          CLEAR(DEL:CRTID)
          CLEAR(L_LG:ClientRateType)
          CLEAR(L_LG:ClientRateType_CID)
       .
       IF DEL:CRTID ~= 0
          CRT:CRTID         = DEL:CRTID
          IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
             L_LG:ClientRateType        = CRT:ClientRateType
             L_LG:ClientRateType_CID    = CRT:CID
       .  .
       
       ! Override the clients percentage with the current rate
       IF DEL:FuelSurcharge ~= 0.0
          ! If we have data, set to this percentage rather
          L_CC:FuelSurchargePer = (DEL:FuelSurcharge / DEL:Charge) * 100
       .
       IF DEL:TollCharge ~= 0.0
          DEL:TollRate          = (DEL:TollCharge / DEL:Charge) * 100
       .
         
       DEL:NoticeEmailAddresses  = Load_Email_Addresses(DEL:CID)  
            
            
       DO Charge_Calc
    .
    L_LG:CID                = DEL:CID
    EXIT
New_Client_Old                    ROUTINE
    ! Check if Client is On Hold
    IF L_CG:ClientStatus = 1
       MESSAGE('This Client is on Hold!', 'Update Deliveries - New Client', ICON:Exclamation)
    ELSIF L_CG:ClientStatus = 2
       MESSAGE('This Client account has been closed!', 'Update Deliveries - New Client', ICON:Hand)
       CLEAR(DEL:CID)
       CLEAR(L_LG:ClientName)
       CLEAR(L_LG:ClientNo)
       SELECT(?L_LG:ClientName)
       EXIT
	.
	
    ! Pre Paid, COD, Account
    IF DEL:Terms < 2
       !DISABLE(?L_LG:To_Address)
       !?L_LG:To_Address{PROP:Req}   = FALSE
       !ENABLE(?Group_COD_Add)
       ENABLE(?Button_COD_Address)
       ?Button_COD_Address{PROP:FontColor}  = COLOR:Red
    ELSE
       !ENABLE(?L_LG:To_Address)
       !?L_LG:To_Address{PROP:Req}   = TRUE
       !DISABLE(?Group_COD_Add)
       DISABLE(?Button_COD_Address)
       ?Button_COD_Address{PROP:FontColor}  = COLOR:None
       CLEAR(DEL:DC_ID)
    .


    IF L_LG:CID ~= DEL:CID                                  ! Client has changed.  This does not execute on Change, Delete unless client changed
       CLEAR(L_CG:MinChargeChecked)
       CLEAR(L_CG:UseMinCharge)
	   CLEAR(L_CC:Rate_Checked)
		
       IF QuickWindow{PROP:AcceptAll}  = FALSE
		  IF CLIP(L_LG:PODMessage) <> ''
			 MESSAGE(CLIP(CLI:PODMessage), 'POD Message', ICON:Exclamation)			 
		  .

          ! Check if 1 Journey - if so load these now as defaults
          DEL:JID              = Check_ClientRates_for_JID(DEL:CID, GETINI('Manifest', 'Default_Manifest_JID', 0, GLO:Local_INI))

          JOU:JID              = DEL:JID
          IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
             L_LG:Journey      = JOU:Journey
          ELSE
             CLEAR(L_LG:Journey)
          .


          ! Check if 1 Load Type if so load these now as defaults
          Count_#              = 0
          CLEAR(V_RCJ:Record)
          V_RCJ:CID            = DEL:CID
          SET(V_RCJ:CKey_CID_JID, V_RCJ:CKey_CID_JID)
          LOOP
             IF Access:_View_Rates_Client_Journeys.TryNext() ~= LEVEL:Benign
                BREAK
             .
             IF V_RCJ:CID ~= DEL:CID
                BREAK
             .

             IF DEL:LTID ~= V_RATC_L:LTID   ! 1st time and then when differs
                Count_#   += 1
             .            
             
             DEL:LTID   = V_RATC_L:LTID
          .
          IF Count_# > 1
             CLEAR(DEL:LTID)
             CLEAR(L_LG:LoadType)
          ELSIF Count_# = 1
             DEL:LTID          = V_RATC_L:LTID
          .

          IF DEL:LTID ~= 0
             LOAD2:LTID        = DEL:LTID
             IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
                L_LG:LoadType  = LOAD2:LoadType
          .  .

          L_CC:FuelSurchargePer     = Get_Client_FuelSurcharge(DEL:CID, DEL:DIDate)

          
       .

       ! Check how many CRTIDs are available for this Load Type LTID for this Client
       DEL:CRTID    = Check_ClientsRateTypes(DEL:CID, DEL:LTID)
       IF DEL:CRTID = 0
          CLEAR(DEL:CRTID)
          CLEAR(L_LG:ClientRateType)
          CLEAR(L_LG:ClientRateType_CID)
       .
       IF DEL:CRTID ~= 0
          CRT:CRTID         = DEL:CRTID
          IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
             L_LG:ClientRateType        = CRT:ClientRateType
             L_LG:ClientRateType_CID    = CRT:CID
       .  .

       ! Override the clients percentage with the current rate
       IF DEL:FuelSurcharge ~= 0.0
          ! If we have data, set to this percentage rather
          L_CC:FuelSurchargePer = (DEL:FuelSurcharge / DEL:Charge) * 100
       .

       DO Charge_Calc
    .
    L_LG:CID                = DEL:CID
    EXIT
Charge_Calc                  ROUTINE
    DATA
R:Order     BYTE
R:Charge    LIKE(L_CC:Charge)

    CODE
    IF ThisWindow.Request ~= ViewRecord

!                                      ! (p:DID, p:SelectedRate, p:Mass, p:MinimiumChargeRate, p:To_Weight, p:Effective_Date)
!    IF Delivery_Rates(DEL:DID, L_RG:Returned_Rate, L_CC:TotalWeight, L_CC:MinimiumChargeRate, L_RG:To_Weight, L_RG:Effective_Date) > 0
!       DEL:Rate            = L_RG:Returned_Rate
!
!       L_RG:Selected_Rate  = DEL:Rate
!
!       L_LG:Rate_Specified = TRUE
!    .
!

!     db.debugout('[Update_Deliveries]  Charge_Calc - Start')

       ! We only want to do this if there are some items captured.
       IF Get_DelItem_s_Totals(DEL:DID, 3,,, TRUE) > 0         ! No cache = TRUE
   !       IF ThisWindow{PROP:AcceptAll} ~= TRUE
   !  db.debugout('Update_Deliveries - Charge_Calc - after Get_DelItems.. 3 - DEL:DID: ' & DEL:DID)

   !          ThisWindow.Update

!     db.debugout('[Update_Deliveries]  Charge_Calc - after Get_DelItems.. 0 after update')

             L_CC:TotalWeight                  = Get_DelItem_s_Totals(DEL:DID,0,,,TRUE) / 100  ! Returns a Ulong
             L_CC:TotalUnits                   = Get_DelItem_s_Totals(DEL:DID,3,,,TRUE)        ! Get items (used in del comb)

!     db.debugout('[Update_Deliveries]  Charge_Calc - after Get_DelItems.. 0 before Reset')

   !          ThisWindow.Reset(1)

             IF L_LG:Charge_Specified = TRUE                    ! Make current rate match
                DEL:Rate                       = DEL:Charge / L_CC:TotalWeight
             .

             DO Rate_Calc                                                                ! Uses the L_CC:TotalWeight

!     db.debugout('[Update_Deliveries]  Charge_Calc - after Rate_Calc.. 0 - Charge_Specified: ' & L_LG:Charge_Specified & ', DEL:Rate: ' & DEL:Rate)

             ! Calc begins......
             IF L_LG:Charge_Specified = FALSE                   ! User has not set a charge
                L_CC:Charge                    = L_CC:TotalWeight * DEL:Rate
             ELSE
                L_CC:Charge                    = DEL:Charge
             .

             R:Charge      = L_CC:Charge

             LOOP 2 TIMES
                R:Order    += 1

                IF R:Order = 3         ! only 2nd loop - 1 not checked
                   R:Order = 1
                ELSE
                   IF R:Charge < L_CG:MinimiumCharge
                      IF R:Charge < L_CC:MinimiumChargeRate
                         ! The only condition that it matters
                         IF L_CG:MinimiumCharge < L_CC:MinimiumChargeRate
                            R:Order   = 2         ! Do bigger question 1st, if yes then will skip question 2!
                .  .  .  .

                IF R:Order = 1
                   ! If we are using a minimium weight then we cannot just set the Charge
                   ! Clients Minimium Charge
                   IF R:Charge < L_CG:MinimiumCharge
                      ! Checked is reset when Client is changed.
                      IF L_CG:MinChargeChecked = FALSE
                         CASE MESSAGE('The Delivery Charge is less then the Clients Minimum Charge.||Would you like to set the Charge to the Minimum Charge for this client of: ' & CLIP(LEFT(FORMAT(L_CG:MinimiumCharge,@n-12.2))), 'Charge Calc.', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                         OF BUTTON:Yes
                            DEL:Charge               = L_CG:MinimiumCharge
                            L_CG:UseMinCharge        = TRUE
                            R:Charge                 = DEL:Charge
                         .
                         L_CG:MinChargeChecked       = TRUE
                      .
                   ELSE
                      IF DEL:Charge = L_CG:MinimiumCharge
                      ELSE
                         L_CG:UseMinCharge           = FALSE
                         L_CG:MinChargeChecked       = FALSE
                .  .  .

                IF R:Order = 2
                   IF R:Charge < L_CC:MinimiumChargeRate
                      ! Checked is reset when Rate changes.
                      IF L_CC:MinChargeRateChecked = FALSE
                         CASE MESSAGE('The Delivery Charge is less then the Rates Minimum Charge.||Would you like to set the Charge to the Minimum Charge for this Rate of: ' & CLIP(LEFT(FORMAT(L_CC:MinimiumChargeRate,@n-12.2))), 'Charge Calc.', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                         OF BUTTON:Yes
                            DEL:Charge               = L_CC:MinimiumChargeRate
                            L_CC:UseMinRateCharge    = TRUE
                            R:Charge                 = DEL:Charge
                         .
                         L_CC:MinChargeRateChecked   = TRUE
                      .
                   ELSE
                      IF DEL:Charge = L_CC:MinimiumChargeRate
                      ELSE
                         L_CC:UseMinRateCharge       = FALSE
                         L_CC:MinChargeRateChecked   = FALSE
             .  .  .  .

             IF (L_CC:UseMinRateCharge = TRUE OR L_CG:UseMinCharge = TRUE)
                DEL:Rate                      = DEL:Charge / L_CC:TotalWeight
             ELSE
                IF L_LG:Charge_Specified = FALSE                   ! User has not set a charge
                   DEL:Charge                 = L_CC:Charge
                ELSE
                   DEL:Rate                   = DEL:Charge / L_CC:TotalWeight
       .  .  .  !.

       IF DEL:Insure = TRUE
          L_CC:InsuanceAmount              = DEL:TotalConsignmentValue * (DEL:InsuranceRate / 100)
       .

       IF L_CC:FuelSurchargePer > 0
          DEL:FuelSurcharge                = DEL:Charge * (L_CC:FuelSurchargePer / 100)
       .
          
       CLEAR(DEL:TollCharge)
       CLEAR(DEL:TollRate)
       IF L_LG:Journey_TollCharged = TRUE 
          DEL:TollRate                     = L_LG:TollRate
          DEL:TollCharge                   = DEL:Charge * (DEL:TollRate / 100)
       .
                  
       L_CC:TotalCharge                    = DEL:Charge + DEL:AdditionalCharge + DEL:DocumentCharge + DEL:FuelSurcharge + DEL:TollCharge + L_CC:InsuanceAmount

       L_CC:VAT                = L_CC:TotalCharge * (DEL:VATRate / 100)
       L_CC:TotalCharge_VAT    = L_CC:TotalCharge + L_CC:VAT

       DISPLAY

     db.debugout('[Update_Deliveries]  Charge_Calc - End.. 0 - DEL:DID: ' & DEL:DID)
    .

    DO Value_Type
    EXIT
Rate_Calc                      ROUTINE
    DATA
R:MinimiumChargeRate        LIKE(L_CC:MinimiumChargeRate)
R:To_Weight                 LIKE(L_RG:To_Weight)
R:Effective_Date            LIKE(L_RG:Effective_Date)
R:Setup_Rate                LIKE(L_RG:Setup_Rate)

    CODE
    ! Dont do calc when OK on window.............   sure ??
    IF QuickWindow{PROP:AcceptAll} = FALSE AND L_CC:Rate_Checked = FALSE AND ThisWindow.Request ~= ViewRecord
  db.debugout('[Update_Deliveries]  Rate_Calc - Start')
       ! Calculate the rate when?
       ! When inserting records or when something changes?
       ! Warn if something changes and the rate would change too.

       IF DEL:CID ~= 0 AND DEL:JID ~= 0 AND DEL:LTID ~= 0 AND L_CC:TotalWeight > 0.0
          CLEAR(L_CC:MinChargeRateChecked)
          CLEAR(L_CC:UseMinRateCharge)
          L_CC:Rate_Checked        = TRUE

                                                    ! (p:DID, p:CID, p:JID, p:CRTID, p:FID, p:Mass, p:Eff_Date, <*p:Mincharge>, |
                                                    ! <p:Setup>, <p:To_Weight>, <p:Effective_Date>, <p:RateID>)
           LOC:Rate_Group_Ret       = Get_Delivery_Rate(DEL:DID, DEL:CID, DEL:JID, DEL:CRTID, DEL:FIDRate, L_CC:TotalWeight,  |
                                                        DEL:DIDate, R:MinimiumChargeRate, R:Setup_Rate,               |
                                                        R:To_Weight, R:Effective_Date, L_RG:ID) ! was L_RG:Setup_Rate
          L_LG:Next_Rate           = L_RG:RatePerKg

          ! Note L_RG:ID can be the CP Rate, CP Client Discount Rate, or the Rate ID.
          IF L_RG:ID = 0 AND L_LG:Rate_Specified = FALSE
             MESSAGE('No rate was found.', 'Rate Calculation', ICON:Exclamation)
          .

    db.debugout('[Update_Deliveries]  Checking rate - found rate: ' & L_LG:Next_Rate & ',  current rate: ' & DEL:Rate & ',  Charge_Specified: ' & L_LG:Charge_Specified)

          IF L_LG:Next_Rate = 0.0
             SELECT(?DEL:Rate)
          .

          ! If we previously had a Rate and the New Rate will differ then warn the user
          !IF L_LG:Last_Rate ~= L_LG:Next_Rate AND (DEL:Rate ~= 0.0 AND (L_LG:Next_Rate ~= 0.0 OR LOC:Request = InsertRecord))
          IF DEL:Rate ~= L_LG:Next_Rate AND (DEL:Rate ~= 0.0 AND (L_LG:Next_Rate ~= 0.0 OR LOC:Request = InsertRecord))
             ! AND L_LG:Last_No_Items ~= L_LG:No_Items
             IF L_LG:Rate_Specified = FALSE
                ! Specified in the messages text means client rate specified, not user selected specified.
                CASE MESSAGE('The Rate used on this delivery (' & DEL:Rate & ') differs to the specified rate (' & L_LG:Next_Rate & ').' |
                             & '||Would you like to update the Delivery Rate with the new rate?', |
                             'Rate', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                OF BUTTON:Yes
                   L_RG:Selected_Rate       = 0.0
                   L_LG:Rate_Specified      = FALSE
                   L_LG:Charge_Specified    = FALSE

                   DEL:Rate                 = L_LG:Next_Rate

                   L_CC:MinimiumChargeRate  = R:MinimiumChargeRate
                   L_RG:To_Weight           = R:To_Weight     
                   L_RG:Effective_Date      = R:Effective_Date
                   L_RG:Setup_Rate          = R:Setup_Rate

                   DISPLAY
             .  .

             L_LG:Last_Rate        = L_LG:Next_Rate
             L_LG:Last_No_Items    = L_LG:No_Items

             !L_LG:CID              = DEL:CID
             !L_LG:JID              = DEL:JID
             !L_LG:LTID             = DEL:LTID
          ELSIF LOC:Request = InsertRecord AND DEL:Rate = 0.0 AND L_LG:Rate_Specified = FALSE
             L_CC:MinimiumChargeRate    = R:MinimiumChargeRate
             L_RG:To_Weight             = R:To_Weight
             L_RG:Effective_Date        = R:Effective_Date
             L_RG:Setup_Rate            = R:Setup_Rate

             DEL:Rate                   = L_LG:Next_Rate
             DISPLAY(?DEL:Rate)
          .

          IF L_RG:Selected_Rate = DEL:Rate AND L_RG:Selected_Rate ~= L_LG:Next_Rate
             ?String_Rate_Info{PROP:Text}   = 'Selected Rate used.'
          ELSIF L_RG:Setup_Rate = TRUE
             ?String_Rate_Info{PROP:Text}   = 'Default Rate used.'
          ELSIF L_RG:ID ~= 0
             ?String_Rate_Info{PROP:Text}   = 'Client Rate used.'
          ELSE
             ?String_Rate_Info{PROP:Text}   = 'No Rate found.'
       .  .

  db.debugout('[Update_Deliveries]  Rate_Calc - End')
    .
    EXIT
Validate_Col_Add            ROUTINE
    ADD:AID             = DEL:CollectionAID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       ?L_LG:From_Address{PROP:Tip}     = 'Collection Address'
       IF ADD:Client = TRUE AND ADD:Delivery = FALSE
          MESSAGE('The Collection address specified is a Client address and not a Delivery address.||Please select another address.', 'Collection Address', ICON:Exclamation)
          CLEAR(DEL:CollectionAID)
          CLEAR(L_LG:From_Address)
          SELECT(?L_LG:From_Address)
       ELSE
          DO Collection_Add_Tip
    .  .
    EXIT



Collection_Add_Tip            ROUTINE
    DATA
R_Return_Group           GROUP,PRE(R)
Suburb                     STRING(50) !Suburb
PostalCode                 STRING(10)
Country                    STRING(50) !Country
Province                   STRING(35) !Province
City                       STRING(35) !City
Found                      BYTE
                         END

R:Details               STRING(250)
    CODE
    R_Return_Group    = Get_Suburb_Details(ADD:SUID)
    IF R:Found = TRUE
       R:Details         = 'Collection Address (' & CLIP(R:City) & ')<10>'
       IF CLIP(ADD:Line1) ~= ''
          R:Details      = CLIP(R:Details) & CLIP(ADD:Line1) & '<10>'
       .
       IF CLIP(ADD:Line2) ~= ''
          R:Details      = CLIP(R:Details) & CLIP(ADD:Line2) & '<10>'
       .
       R:Details         = CLIP(R:Details) & CLIP(R:PostalCode) & '<10>' & |
                                             CLIP(R:Province)
    ELSE
       R:Details         = 'Collection Address<10>(Suburb info. missing - SUID: ' & ADD:SUID & ',  ColAID: ' & DEL:CollectionAID & ')'
    .
    ?L_LG:From_Address{PROP:Tip}     = CLIP(R:Details)
    EXIT
Validate_Del_Add            ROUTINE
    ADD:AID             = DEL:DeliveryAID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       ?L_LG:To_Address{PROP:Tip}           = 'Delivery Address'

       IF ADD:Client = TRUE AND ADD:Delivery = FALSE
          MESSAGE('The Delivery address specified is a Client address and not a Delivery address.||Please select another address.', 'Collection Address', ICON:Exclamation)
          CLEAR(DEL:DeliveryAID)
          CLEAR(L_LG:To_Address)
          SELECT(?L_LG:To_Address)
       ELSE
          DO Delivery_Add_Tip
    .  .
    EXIT


Delivery_Add_Tip            ROUTINE
    DATA
R_Return_Group           GROUP,PRE(R)
Suburb                     STRING(50) !Suburb
PostalCode                 STRING(10)
Country                    STRING(50) !Country
Province                   STRING(35) !Province
City                       STRING(35) !City
Found                      BYTE
                         END

R:Details               STRING(250)

    CODE
    R_Return_Group    = Get_Suburb_Details(ADD:SUID)

    IF R:Found = TRUE
       R:Details         = 'Delivery Address (' & CLIP(R:City) & ')<10>'
       IF CLIP(ADD:Line1) ~= ''
          R:Details      = CLIP(R:Details) & CLIP(ADD:Line1) & '<10>'
       .
       IF CLIP(ADD:Line2) ~= ''
          R:Details      = CLIP(R:Details) & CLIP(ADD:Line2) & '<10>'
       .
       R:Details         = CLIP(R:Details) & CLIP(R:PostalCode) & '<10>' & |
                                             CLIP(R:Province)
    ELSE
       R:Details         = 'Delivery Address<10>(Suburb info. missing - SUID: ' & ADD:SUID & ',  DelAID: ' & DEL:DeliveryAID & ')'
    .
    ?L_LG:To_Address{PROP:Tip}     = CLIP(R:Details)
    EXIT
Floor_Changed               ROUTINE
    ! Check load type is Container Park or not
    ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery

    IF L_LT:ContainerParkStandard = TRUE
       ! Load type is Container Park Special
    ELSIF L_LT:LoadOption = 1                   ! Container Park
       ENABLE(?Group_RateFloor)
       IF DEL:FIDRate = 0
          DEL:FIDRate       = DEL:FID
          CLEAR(L_LG:FloorRate)
       .

       IF CLIP(L_LG:FloorRate) = ''
          ! Load String
          FLO:FID           = DEL:FIDRate
          IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
             L_LG:FloorRate = FLO:Floor
       .  .
    ELSE
       CLEAR(DEL:FIDRate)
       DISABLE(?Group_RateFloor)
    .
    EXIT
Get_Rate_Button           ROUTINE
    DATA
R:MinimiumChargeRate        LIKE(L_CC:MinimiumChargeRate)
R:To_Weight                 LIKE(L_RG:To_Weight)
R:Effective_Date            LIKE(L_RG:Effective_Date)
R:Setup_Rate                LIKE(L_RG:Setup_Rate)

    CODE
    IF DEL:CID ~= 0 AND DEL:JID ~= 0 AND DEL:LTID ~= 0
       CLEAR(L_CC:MinChargeRateChecked)
       CLEAR(L_CC:UseMinRateCharge)
                                        ! (p:DID, p:CID, p:JID, p:CRTID, p:FID, p:Mass, p:Eff_Date, <*p:Mincharge>, |
                                        ! <p:Setup>, <p:To_Weight>, <p:Effective_Date>, <p:RateID>)
       LOC:Rate_Group_Ret = Get_Delivery_Rate(DEL:DID, DEL:CID, DEL:JID, DEL:CRTID, DEL:FIDRate, L_CC:TotalWeight, DEL:DIDate, |
                                              R:MinimiumChargeRate, R:Setup_Rate, R:To_Weight, |
                                              R:Effective_Date, L_RG:ID)
       L_LG:Next_Rate     = L_RG:RatePerKg

       IF L_RG:ID = 0
          MESSAGE('No rate was found.', 'Rate Calculation', ICON:Exclamation)
       ELSE
          CASE MESSAGE('The calculated rate would be: ' & L_LG:Next_Rate & '|With a minimium Charge of: ' & L_CC:MinimiumChargeRate    |
                       & '||Would you like to update the rate and charges now?', |
                       'Rate Calculation', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
          OF BUTTON:Yes
             CLEAR(L_CC:Rate_Checked)
             CLEAR(L_LG:Charge_Specified)
             CLEAR(L_LG:Rate_Specified)

             DEL:Rate               = L_LG:Next_Rate
             L_LG:Last_Rate         = L_LG:Next_Rate

             L_RG:Selected_Rate     = 0.0
             L_LG:Rate_Specified    = FALSE

             L_CC:MinimiumChargeRate  = R:MinimiumChargeRate
             L_RG:To_Weight           = R:To_Weight     
             L_RG:Effective_Date      = R:Effective_Date
             L_RG:Setup_Rate          = R:Setup_Rate

             DO Charge_Calc
    .  .  .
    EXIT
Date_Changed                ROUTINE                 ! Fuel Surcharge loaded here
    IF QuickWindow{PROP:AcceptAll}  = FALSE
       L_CC:FuelSurchargePer     = Get_Client_FuelSurcharge(DEL:CID, DEL:DIDate)
       DO Charge_Calc
       DISPLAY
    .
    EXIT
Value_Type                      ROUTINE
    EXECUTE L_SG:Value_Type
       L_SG:Value               = DEL:Charge
       L_SG:Value               = L_CC:TotalCharge
       L_SG:Value               = L_CC:TotalCharge_VAT
    .
    EXECUTE L_SG:Value_Type
       ?L_SG:Value{PROP:Tip}    = 'Delivery Charge (excl. VAT, Additional Charges, Docs., Fuel Surcharge & Insurance)'
       ?L_SG:Value{PROP:Tip}    = 'Total Charge (excl. VAT & incl. Additional Charges, Docs., Fuel Surcharge & Insurance)'
       ?L_SG:Value{PROP:Tip}    = 'Total Charge (all incl.)'
    .
    DISPLAY(?L_SG:Value)
    EXIT
Load_Lookups                ROUTINE
    ! Load lookup fields & additionals
    JOU:JID             = DEL:JID
    IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
!       db.debugout('[Update_Deliveries]  DEL:JID: ' & DEL:JID & ' - JOU:Journey: ' & JOU:Journey)
       L_LG:Journey               = JOU:Journey
       L_LG:Journey_TollCharged   = JOU:EToll
    .

    CLI:CID                     = DEL:CID
    IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
       L_LG:ClientName          = CLI:ClientName

       ! What if these change on the Client record???
       L_CG:MinimiumCharge      = CLI:MinimiumCharge          ! This is also specified on some rates???

       IF LOC:Request ~= ChangeRecord AND LOC:Request ~= ViewRecord
          L_CC:FuelSurchargePer = Get_Client_FuelSurcharge(CLI:CID, DEL:DIDate)         !CLI:FuelSurcharge
          L_LG:TollRate         = Get_Client_ETolls(CLI:CID, DEL:DIDate) 
          DO Charge_Calc
       ELSIF DEL:FuelSurcharge ~= 0.0 OR DEL:TollCharge ~= 0.0
          ! If we have data, set to this percentage rather
          L_CC:FuelSurchargePer = (DEL:FuelSurcharge / DEL:Charge) * 100
          DEL:TollRate          = (DEL:TollCharge / DEL:Charge) * 100
          L_LG:TollRate         = DEL:TollRate
       .

       L_CG:GenerateInvoice     = CLI:GenerateInvoice

       L_LG:ClientNo            = CLI:ClientNo

       IF LOC:Request = InsertRecord
          DEL:DocumentCharge    = CLI:DocumentCharge
    .  .

    ADD:AID             = DEL:CollectionAID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       L_LG:From_Address = ADD:AddressName
    ELSE
       CLEAR(ADD:Record)
    .
    DO Collection_Add_Tip

    ADD:AID             = DEL:DeliveryAID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       L_LG:To_Address  = ADD:AddressName
    ELSE
       CLEAR(ADD:Record)
    .
    DO Delivery_Add_Tip

    LOAD2:LTID          = DEL:LTID
    IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
       L_LG:LoadType    = LOAD2:LoadType
    .

    CRT:CRTID           = DEL:CRTID
    IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
       L_LG:ClientRateType        = CRT:ClientRateType
       L_LG:ClientRateType_CID    = CRT:CID
    .

    SERI:SID            = DEL:SID
    IF Access:ServiceRequirements.TryFetch(SERI:PKey_SID) = LEVEL:Benign
       L_LG:ServiceRequirement  = SERI:ServiceRequirement
    .

    FLO:FID             = DEL:FID
    IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
       L_LG:Floor       = FLO:Floor
    .

    DCADD:DC_ID         = DEL:DC_ID
    IF Access:Delivery_CODAddresses.TryFetch(DCADD:PKey_DC_ID) = LEVEL:Benign
       L_LG:COD_Address = DCADD:AddressName
    .

    ! Pre Paid, COD, Account
    IF DEL:Terms < 2
       ENABLE(?Button_COD_Address)
    ELSE
       DISABLE(?Button_COD_Address)
    .

    ! Load Type
    LOAD2:LTID              = DEL:LTID
    IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
       LOC:Load_Type_Group :=: LOAD2:Record
    .

    EXIT
! ----------------------------------------------------------------      Delivery Compositions
Get_DelComp                 ROUTINE
    DATA
R:DELCID            LIKE(DEL:DELCID)
R:DELCID_Ext        LIKE(DEL:DELCID)

    CODE
    R:DELCID_Ext    = DEL:DELCID

    ! User will know from the button what action will be performed.
    ThisWindow.Update
    GlobalRequest   = SelectRecord
    R:DELCID        = Browse_DeliveryCompositions()

    IF DEL:DELCID ~= 0 AND DEL:DELCID ~= R:DELCID
       ! Previous DELCID not equal to chosen one - warn user
       CASE MESSAGE('You have selected a different Multi-Part Delivery to the previous one specified for this Delivery.||' & |
                    'Is this correct?', 'Multi-Part Delivery', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
       OF BUTTON:No
          CLEAR(R:DELCID)
       OF BUTTON:Yes
          IF R:DELCID = 0
             DEL:DELCID = 0                                 ! The other cases are delt with below
    .  .  .

    IF R:DELCID ~= 0
       DEL:DELCID           = R:DELCID
       DO Check_DelComp

       IF DEL:DELCID ~= 0
          IF L_DC2:Start_of_Insert = FALSE
             CASE MESSAGE('Would you like to use the previous Deliveries on this Multi-Part to create this delivery?', |
                        'Multi-Part Delivery', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
             OF BUTTON:Yes
                L_DC2:Start_of_Insert   = TRUE
          .  .

          IF L_DC2:Start_of_Insert = TRUE
             IF L_DC:First_DID = 0
                DELC:DELCID = R:DELCID
                IF Access:DeliveryComposition.TryFetch(DELC:PKey_DELCID) = LEVEL:Benign
                   DEL:CID                             = DELC:CID
                   DEL:ClientReference                 = DELC:Reference

                   CLI:CID                             = DEL:CID
                   IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
                      DEL:Terms                        = CLI:Terms
                      L_CG:ClientTerms                 = CLI:Terms
                      L_CG:ClientStatus                = CLI:Status
                   .

                   DO New_Client
                   IF DEL:CID = 0
                      ! Cancel???
                   ELSE
                      DO Load_Lookups

                      MESSAGE('This is the first DI in this Multi-Part Delivery.||Next, please specify the Delivery details.', 'Insert Delivery', ICON:Asterisk)
                      SELECT(?Tab:1)
                .  .
             ELSE
                ! Set Delivery fields from previous Delivery and create new item for user completion
                A_DEL:DID      = L_DC:First_DID
                IF Access:DeliveriesAlias.TryFetch(A_DEL:PKey_DID) = LEVEL:Benign
                   DEL:CID                             = A_DEL:CID
                   DEL:ClientReference                 = A_DEL:ClientReference
                   DEL:JID                             = A_DEL:JID
                   DEL:CollectionAID                   = A_DEL:CollectionAID
                   DEL:DeliveryAID                     = A_DEL:DeliveryAID
                   DEL:CRTID                           = A_DEL:CRTID
                   DEL:LTID                            = A_DEL:LTID
                   DEL:SID                             = A_DEL:SID

                   DEL:MultipleLoadDID                 = L_DC:First_DID                            ! ??

                   DEL:Rate                            = A_DEL:Rate
                   DEL:DocumentCharge                  = A_DEL:DocumentCharge
                   DEL:AdditionalCharge_Calculate      = A_DEL:AdditionalCharge_Calculate
                   DEL:AdditionalCharge                = A_DEL:AdditionalCharge
                   DEL:VATRate                         = A_DEL:VATRate
                   DEL:Insure                          = A_DEL:Insure
                   DEL:InsuranceRate                   = A_DEL:InsuranceRate
                   DEL:FID                             = A_DEL:FID
                   DEL:FIDRate                         = A_DEL:FIDRate
                   DEL:SpecialDeliveryInstructions     = A_DEL:SpecialDeliveryInstructions
                   DEL:Notes                           = A_DEL:Notes
                   DEL:MultipleManifestsAllowed        = A_DEL:MultipleManifestsAllowed
                   DEL:DC_ID                           = A_DEL:DC_ID
                   DEL:Terms                           = A_DEL:Terms

                   DEL:VATRate_OverriddenUserID        = A_DEL:VATRate_OverriddenUserID            ! ??

                   CLI:CID                             = DEL:CID
                   IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
                      DEL:Terms                        = CLI:Terms
                      L_CG:ClientTerms                 = CLI:Terms
                      L_CG:ClientStatus                = CLI:Status
                   .
                   !DO New_Client
                   !IF DEL:CID = 0

                   DO Load_Lookups

                   DO New_Load_Type

                   MESSAGE('Next, please specify the Delivery Item details.', 'Insert Delivery', ICON:Asterisk)

                   ! Call Item insert...       note L_DC:First_DID is used in parameter
                   POST(EVENT:Accepted, ?Insert)
             .  .
          ELSE
          .
       ELSE
          CLEAR(L_DC:First_DID)

!          IF R:DELCID_Ext ~= 0
!             CASE MESSAGE('This Delivery was already specified on this Multi-Part, you have selected not to add it ' & |
!                    'due to a condition being exceeded.||Would you like to remove this Delivery from the Multi-Part?', |
!                    'Multi-Part Delivery', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
!             OF BUTTON:Yes
!                DEL:DELCID  = 0
    .  .  !.  .

    DO DelComp_SetButton

    CLEAR(L_DC2:Start_of_Insert)
    DISPLAY
    EXIT
Check_DelComp             ROUTINE
    DATA
R:Deliveries    LIKE(L_DC:Deliveries)
R:Items         LIKE(L_DC:Items)
R:Weight        LIKE(L_DC:Weight)

R:Add           BYTE

R:Msg           STRING(255)

    CODE
    IF DEL:DELCID ~= 0
       R:Add            = TRUE

       ! Check that this Composition is not completely loaded already
       LOC:DeliveryComposition_Ret_Group = Get_Delivery_Info(DEL:DELCID)

       R:Deliveries     = L_DC:Deliveries
       R:Items          = L_DC:Items
       R:Weight         = L_DC:Weight

       IF LOC:Request ~= InsertRecord
          R:Deliveries -= 1
          R:Items      -= L_CC:TotalUnits
          R:Weight     -= L_CC:TotalWeight
       .

       ! All specified deliveries done
!       IF (R:Deliveries + 1) > DELC:Deliveries
!          CASE MESSAGE('This Delivery Composition already has the specified number of Deliveries.||Continue and add this one?', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!          OF BUTTON:No
!             R:Add      = FALSE
!          .
!       ELSIF (R:Items + L_CC:TotalUnits) > DELC:Items
!          CASE MESSAGE('This Delivery Composition already has the specified number of Items.||Continue and add these items? (Items: ' & L_CC:TotalUnits & ')', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!          OF BUTTON:No
!             R:Add      = FALSE
!          .

       IF LOC:Request = InsertRecord
          R:Msg     = 'Adding this DI to the selected Multi-Part Delivery would exceed the specified weight'
       ELSE
          R:Msg     = 'The selected Multi-Part Delivery exceeds the specified weight'
       .

       IF (R:Weight + L_CC:TotalWeight) > DELC:Weight
          CASE MESSAGE(CLIP(R:Msg) & ' by ' & |
                    FORMAT(DELC:Weight - R:Weight - L_CC:TotalWeight, @n12.2) & '.||Continue and add this Delivery weight? (Weight: ' & |
                    FORMAT(L_CC:TotalWeight,@n12.2) & ')', 'Multi-Part Delivery', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
          OF BUTTON:No                                                                                                                         
             R:Add          = FALSE
       .  .

       IF R:Add = TRUE
          ! Add in this deliveries info then
          L_DC:Deliveries  += 1                 !R:Deliveries          !1
          L_DC:Items       += L_CC:TotalUnits   !R:Items               !L_CC:TotalUnits
          L_DC:Weight      += L_CC:TotalWeight  !R:Weight              !L_CC:TotalWeight
       ELSE
          CLEAR(L_DC:First_DID)
          DEL:DELCID        = 0
    .  .

    DO DelComp_SetButton

    ThisWindow.Reset
    DISPLAY
    EXIT
DelComp_SetButton        ROUTINE
    IF DEL:DELCID = 0
       ?Button_Specify{PROP:Text} = 'Specify Multi-Part'
       HIDE(?Prompt_Multi)
       ?Tab_Multi{PROP:Font,4}    = FONT:Regular
    ELSE
       ?Button_Specify{PROP:Text} = 'Change Multi-Part'
       UNHIDE(?Prompt_Multi)
       ?Tab_Multi{PROP:Font,4}    = FONT:Bold
    .

    IF ?Button_Specify{PROP:Text} = 'Specify Multi-Part'
       CLEAR(LOC:DeliveryComposition_Ret_Group)
       CLEAR(LOC:DeliveryComposition_Group_2)
    .
    EXIT
!                           old
!Get_DelComp                 ROUTINE
!    DATA
!R:DELCID        LIKE(DEL:DELCID)
!
!R:Deliveries    LIKE(L_DC:Deliveries)
!R:Items         LIKE(L_DC:Items)
!R:Weight        LIKE(L_DC:Weight)
!
!R:Add           BYTE
!
!    CODE
!    ! User will know from the button what action will be performed.
!    ThisWindow.Update
!    GlobalRequest   = SelectRecord
!    R:DELCID        = Browse_DeliveryCompositions()
!
!    IF DEL:DELCID ~= 0 AND DEL:DELCID ~= R:DELCID
!       ! Previous DELCID not equal to chosen one - warn user
!       CASE MESSAGE('You have selected a different Delivery Composition to the previous one specified for this Delivery.||' & |
!                    'Is this correct?', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
!       OF BUTTON:No
!          CLEAR(R:DELCID)
!       OF BUTTON:Yes
!          IF R:DELCID = 0
!             DEL:DELCID = 0                                 ! The other cases are delt with below
!    .  .  .
!
!    IF R:DELCID ~= 0
!       R:Add            = TRUE
!
!       ! Check that this Composition is not completely loaded already
!       LOC:DeliveryComposition_Ret_Group = Get_Delivery_Info(R:DELCID)
!
!       R:Deliveries     = L_DC:Deliveries
!       R:Items          = L_DC:Items
!       R:Weight         = L_DC:Weight
!       IF DEL:DELCID = R:DELCID                             ! Previously part of this delivery
!          R:Deliveries -= 1
!          R:Items      -= L_CC:TotalUnits
!          R:Weight     -= L_CC:TotalWeight
!       .
!
!       ! All specified deliveries done
!!       IF (R:Deliveries + 1) > DELC:Deliveries
!!          CASE MESSAGE('This Delivery Composition already has the specified number of Deliveries.||Continue and add this one?', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!!          OF BUTTON:No
!!             R:Add      = FALSE
!!          .
!!       ELSIF (R:Items + L_CC:TotalUnits) > DELC:Items
!!          CASE MESSAGE('This Delivery Composition already has the specified number of Items.||Continue and add these items? (Items: ' & L_CC:TotalUnits & ')', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!!          OF BUTTON:No
!!             R:Add      = FALSE
!!          .
!       IF (R:Weight + L_CC:TotalWeight) > DELC:Weight
!          CASE MESSAGE('This Delivery Composition already has the specified weight.||Continue and add this Delivery weight? (Weight: ' & FORMAT(L_CC:TotalWeight,@n12.2) & ')', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!          OF BUTTON:No                                                                                                                         
!             R:Add      = FALSE
!          .
!       ELSE
!          !
!       .
!
!       IF R:Add = TRUE
!          DEL:DELCID        = R:DELCID
!
!          ! Add in this deliveries info then
!          L_DC:Deliveries  += 1                 !R:Deliveries          !1
!          L_DC:Items       += L_CC:TotalUnits   !R:Items               !L_CC:TotalUnits
!          L_DC:Weight      += L_CC:TotalWeight  !R:Weight              !L_CC:TotalWeight
!
!          IF L_DC2:Start_of_Insert = TRUE
!             IF L_DC:First_DID = 0
!                DELC:DELCID = R:DELCID
!                IF Access:DeliveryComposition.TryFetch(DELC:PKey_DELCID) = LEVEL:Benign
!                   DEL:CID                             = DELC:CID
!                   DEL:ClientReference                 = DELC:Reference
!
!                   CLI:CID                             = DEL:CID
!                   IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
!                      DEL:Terms                        = CLI:Terms
!                      L_CG:ClientTerms                 = CLI:Terms
!                      L_CG:ClientStatus                = CLI:Status
!                   .
!
!                   DO New_Client
!
!                   DO Load_Lookups
!
!                   MESSAGE('This is the first DI in this Multi Part Delivery.||Next, please specify the Delivery details.', 'Insert Delivery', ICON:Asterisk)
!                   SELECT(?Tab:1)
!                .
!             ELSE
!                ! Set Delivery fields from previous Delivery and create new item for user completion
!                A_DEL:DID      = L_DC:First_DID
!                IF Access:DeliveriesAlias.TryFetch(A_DEL:PKey_DID) = LEVEL:Benign
!                   DEL:CID                             = A_DEL:CID
!                   DEL:ClientReference                 = A_DEL:ClientReference
!                   DEL:JID                             = A_DEL:JID
!                   DEL:CollectionAID                   = A_DEL:CollectionAID
!                   DEL:DeliveryAID                     = A_DEL:DeliveryAID
!                   DEL:CRTID                           = A_DEL:CRTID
!                   DEL:LTID                            = A_DEL:LTID
!                   DEL:SID                             = A_DEL:SID
!
!                   DEL:MultipleLoadDID                 = L_DC:First_DID                            ! ??
!
!                   DEL:Rate                            = A_DEL:Rate
!                   DEL:DocumentCharge                  = A_DEL:DocumentCharge
!                   DEL:AdditionalCharge_Calculate      = A_DEL:AdditionalCharge_Calculate
!                   DEL:AdditionalCharge                = A_DEL:AdditionalCharge
!                   DEL:VATRate                         = A_DEL:VATRate
!                   DEL:Insure                          = A_DEL:Insure
!                   DEL:InsuranceRate                   = A_DEL:InsuranceRate
!                   DEL:FID                             = A_DEL:FID
!                   DEL:FIDRate                         = A_DEL:FIDRate
!                   DEL:SpecialDeliveryInstructions     = A_DEL:SpecialDeliveryInstructions
!                   DEL:Notes                           = A_DEL:Notes
!                   DEL:MultipleManifestsAllowed        = A_DEL:MultipleManifestsAllowed
!                   DEL:DC_ID                           = A_DEL:DC_ID
!                   DEL:Terms                           = A_DEL:Terms
!
!                   DEL:VATRate_OverriddenUserID        = A_DEL:VATRate_OverriddenUserID            ! ??
!
!                   DO Load_Lookups
!
!                   DO New_Load_Type
!
!                   MESSAGE('Next, please specify the Delivery Item details.', 'Insert Delivery', ICON:Asterisk)
!
!                   ! Call Item insert...       note L_DC:First_DID is used in parameter
!                   POST(EVENT:Accepted, ?Insert)
!          .  .  .
!       ELSE
!          CLEAR(L_DC:First_DID)
!
!          IF DEL:DELCID ~= 0
!             CASE MESSAGE('This Delivery was already specified on this Composition, you have selected not to add it due to a condition being exceeded.||Would you like to remove this Delivery from the Composition?', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!             OF BUTTON:Yes
!                DEL:DELCID  = 0
!                CLEAR(LOC:DeliveryComposition_Ret_Group)
!                CLEAR(LOC:DeliveryComposition_Group_2)
!    .  .  .  .
!
!    ThisWindow.Reset
!    IF DEL:DELCID = 0
!       ?Button_Specify{PROP:Text} = 'Specify Multi-Part'
!       HIDE(?Prompt_Multi)
!       ?Tab_Multi{PROP:Font,4}    = FONT:Regular
!    ELSE
!       ?Button_Specify{PROP:Text} = 'Change Multi-Part'
!       UNHIDE(?Prompt_Multi)
!       ?Tab_Multi{PROP:Font,4}    = FONT:Bold
!    .
!
!    IF ?Button_Specify{PROP:Text} = 'Specify Multi-Part'
!       CLEAR(LOC:DeliveryComposition_Ret_Group)
!       CLEAR(LOC:DeliveryComposition_Group_2)
!    .
!
!    CLEAR(L_DC2:Start_of_Insert)
!    DISPLAY
!    EXIT
! ----------------------------------------------------------------      
Apply_User_Permissions    ROUTINE               ! Special user permissions
    IF ThisWindow.Request = ViewRecord AND LOC:Original_Request = ChangeRecord
       ! (ULONG, STRING, STRING, <STRING>, BYTE=0, <*BYTE>),LONG
       ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned)

       ! Returns
       !   0   - Allow (default)
       !   1   - Disable
       !   2   - Hide
       !   100 - Allow                 - Default action
       !   101 - Disallow              - Default action

       CASE Get_User_Access(GLO:UID, 'Deliveries', 'Update_Deliveries', 'Change_Mode', 1)
       OF 0 OROF 100
          ! This user is allowed to change applications at this stage... only super user!
          ThisWindow.Request = ChangeRecord
       OF 2
       ELSE
    .  .

    CASE Get_User_Access(GLO:UID, 'Deliveries', 'Update_Deliveries', 'DI-Docs', 1)
    OF 0 OROF 100
       ! This user is allowed to change applications at this stage... only super user!
       
    OF 2
    ELSE
      DISABLE(?BUTTON_viewdocs)
    .
    EXIT
Check_Invoiced              ROUTINE
    ! Check that we dont already have an invoice for this DI

    Inv_View.Init(View_Inv, Relate:_Invoice)
    Inv_View.AddSortOrder(INV:FKey_DID)
    !Inv_View.AppendOrder()
    Inv_View.AddRange(INV:DID, DEL:DID)
    !Inv_View.SetFilter()

    Inv_View.Reset()
    LOOP
       IF Inv_View.Next() ~= LEVEL:Benign
          BREAK
       .

       IF INV:CR_IID = 0
          ?Prompt_Manifest{PROP:Text}  = 'Delivery Invoiced (Inv. no.: ' & INV:IID & ') - No editing.'
!       ELSE
!          CLEAR(INV:Record)
          BREAK
    .  .

    Inv_View.Kill()
    EXIT
Load_WebDocs         ROUTINE
   ! fbn-dis.herokuapp.com/di/######
   
   ! (whandle, URL, p:Params, p:LauchDir)
   ! (unsigned, STRING, <STRING>, <STRING>),LONG,PROC
   
   ISExecute(QuickWindow{PROP:Handle} , 'http://fbn-dis.herokuapp.com/di/' & DEL:DINo)
   EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Deliveries Record '
  OF InsertRecord
    ActionMessage = 'Deliveries Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Deliveries Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Deliveries')
      LOC:Original_Request    = GlobalRequest
  SELF.Request = GlobalRequest                    ! Store the incoming request
      ! p:Option
      ! (BYTE=0)
      !           0   - Normal mode
      !           1   - Admin editing mode
      !           2   - Multi-part delivery (insert only)
  
      IF SELF.Request = InsertRecord
      ELSE                                                            ! p:Option used here
         IF p:Option = 2                                              ! Invalid, only valid for Insert
            p:Option  = 0
            MESSAGE('Option 2 is specified but this is only available for Inserts, form is currently not in insert mode.', 'Update_Deliveries', ICON:Hand)
         .
  
         ! Check for Change / Delete actions - see if we do not allow them to do these
  
         ! Get no of manifested units from this delivery
         L_SG:Manifested_Units    = Get_DelItem_s_Totals(DEL:DID, 2,,,TRUE)
         IF L_SG:Manifested_Units > 0
            ! No editing of this DI is allowed!
            ! unless...
            IF p:Option = 1
            ELSE
               SELF.Request       = ViewRecord
            .
            LOC:Manifested        = TRUE
         ELSIF DEL:Manifested > 0
            Upd_Delivery_Man_Status(DEL:DID,,1)                       ! Update the status - DEL:Manifested
            MESSAGE('The Manifested status of this Delivery was wrong, this has now beeen updated.', 'Update Deliveries', ICON:Asterisk)
      .  .
  
      CLEAR(INV:Record)
      IF SELF.Request = ChangeRecord OR SELF.Request = ViewRecord
         ! Check that we dont already have an invoice for this DI
         INV:DID          = DEL:DID
         IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
            ! We do have an invoice - set View mode
            IF p:Option = 1
            ELSE
               SELF.Request  = ViewRecord
            .
         ELSE
            CLEAR(INV:Record)
      .  .
  
      LOC:Request                 = SELF.Request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DEL:DID
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_IG:MID',L_IG:MID)                       ! Added by: BrowseBox(ABC)
  BIND('L_DLG:Total',L_DLG:Total)                 ! Added by: BrowseBox(ABC)
  BIND('L_DLG:VAT',L_DLG:VAT)                     ! Added by: BrowseBox(ABC)
        BIND('MALD:DIID', MALD:DIID)
        BIND('A_DELI:DIID', A_DELI:DIID)
        BIND('DELI:DIID', DELI:DIID)
  
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DEL:Record,History::DEL:Record)
  SELF.AddHistoryField(?DEL:DID,1)
  SELF.AddHistoryField(?DEL:DINo,2)
  SELF.AddHistoryField(?DEL:DIDate,5)
  SELF.AddHistoryField(?DEL:ClientReference,9)
  SELF.AddHistoryField(?DEL:CID,8)
  SELF.AddHistoryField(?DEL:BID,7)
  SELF.AddHistoryField(?DEL:SpecialDeliveryInstructions,34)
  SELF.AddHistoryField(?DEL:Notes,35)
  SELF.AddHistoryField(?DEL:NoticeEmailAddresses,54)
  SELF.AddHistoryField(?DEL:Insure,29)
  SELF.AddHistoryField(?DEL:TotalConsignmentValue,30)
  SELF.AddHistoryField(?DEL:InsuranceRate,31)
  SELF.AddHistoryField(?DEL:Rate,20)
  SELF.AddHistoryField(?DEL:AdditionalCharge_Calculate,24)
  SELF.AddHistoryField(?DEL:AdditionalCharge,25)
  SELF.AddHistoryField(?DEL:Charge,23)
  SELF.AddHistoryField(?DEL:DocumentCharge,21)
  SELF.AddHistoryField(?DEL:VATRate,26)
  SELF.AddHistoryField(?DEL:FuelSurcharge,22)
  SELF.AddHistoryField(?DEL:TollRate,27)
  SELF.AddHistoryField(?DEL:TollCharge,28)
  SELF.AddHistoryField(?DEL:Delivered,42)
  SELF.AddHistoryField(?DEL:MultipleManifestsAllowed,40)
  SELF.AddHistoryField(?DEL:Manifested,41)
  SELF.AddHistoryField(?DEL:CRTID,13)
  SELF.AddHistoryField(?DEL:LTID,14)
  SELF.AddUpdateFile(Access:Deliveries)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:AddressAlias.SetOpenRelated()
  Relate:AddressAlias.Open                        ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  Relate:DeliveriesAlias.Open                     ! File DeliveriesAlias used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                            ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Relate:_View_ContainerParkRates_Client.Open     ! File _View_ContainerParkRates_Client used by this procedure, so make sure it's RelationManager is open
  Relate:_View_Rates_Client.Open                  ! File _View_Rates_Client used by this procedure, so make sure it's RelationManager is open
  Relate:_View_Rates_Client_Journeys.Open         ! File _View_Rates_Client_Journeys used by this procedure, so make sure it's RelationManager is open
  Relate:_View_Rates_Clients_LoadTypes.Open       ! File _View_Rates_Clients_LoadTypes used by this procedure, so make sure it's RelationManager is open
  Access:EmailAddresses.UseFile                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ClientsRateTypes.UseFile                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ServiceRequirements.UseFile              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Floors.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoadDeliveries.UseFile           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoad.UseFile                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Delivery_CODAddresses.UseFile            ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Invoice.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__Rates.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients_ContainerParkDiscounts.UseFile   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesContainerPark.UseFile             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LoadTypes2.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Branches.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TripSheets.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Deliveries
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW_DeliveryItems.Init(?Browse:ItemsContainers,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:DeliveryItems,SELF) ! Initialize the browse manager
  BRW_DelItemsAlias.Init(?Browse:ItemsLoose,Queue:Browse.ViewPosition,BRW20::View:Browse,Queue:Browse,Relate:DeliveryItemAlias2,SELF) ! Initialize the browse manager
  BRW6.Init(?Browse:6,Queue:Browse:6.ViewPosition,BRW6::View:Browse,Queue:Browse:6,Relate:DeliveryLegs,SELF) ! Initialize the browse manager
  BRW8.Init(?Browse:8,Queue:Browse:8.ViewPosition,BRW8::View:Browse,Queue:Browse:8,Relate:DeliveryProgress,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
      IF SELF.Request = ChangeRecord OR SELF.Request = ViewRecord
         DO Check_Invoiced
      .
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    ?DEL:DINo{PROP:ReadOnly} = True
    DISABLE(?Calendar)
    DISABLE(?CallLookup_Client)
    ?L_LG:ClientName{PROP:ReadOnly} = True
    DISABLE(?Button_ClientEmails)
    ?L_LG:ClientNo{PROP:ReadOnly} = True
    ?DEL:ClientReference{PROP:ReadOnly} = True
    DISABLE(?CallLookup_LoadType)
    ?L_LG:LoadType{PROP:ReadOnly} = True
    DISABLE(?Button_ListRates)
    DISABLE(?CallLookup)
    ?L_LG:ClientRateType{PROP:ReadOnly} = True
    DISABLE(?CallLookup_Journey)
    ?L_LG:Journey{PROP:ReadOnly} = True
    DISABLE(?CallLookup_Floor)
    ?L_LG:Floor{PROP:ReadOnly} = True
    DISABLE(?CallLookup_CollectAdd)
    ?L_LG:From_Address{PROP:ReadOnly} = True
    DISABLE(?CallLookup_ToAdd)
    ?L_LG:To_Address{PROP:ReadOnly} = True
    DISABLE(?Button_Release)
    DISABLE(?CallLookup_Driver_Name)
    ?L_LG:Driver_Name{PROP:ReadOnly} = True
    DISABLE(?Button_COD_Address)
    DISABLE(?CallLookup_ServiceReq)
    ?L_LG:ServiceRequirement{PROP:ReadOnly} = True
    DISABLE(?Lookup_EmailAddresses)
    ?DEL:NoticeEmailAddresses{PROP:ReadOnly} = True
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
    DISABLE(?Insert:5)
    DISABLE(?Change:5)
    DISABLE(?Delete:5)
    DISABLE(?Button_Wizard)
    DISABLE(?BUTTON_viewdocs)
    DISABLE(?BUTTON_Manifest)
    DISABLE(?BUTTON_Tripsheet)
    DISABLE(?Button_View_Invoice)
    DISABLE(?L_CG:ClientTerms)
    ?DEL:TotalConsignmentValue{PROP:ReadOnly} = True
    ?DEL:InsuranceRate{PROP:ReadOnly} = True
    ?L_CC:InsuanceAmount{PROP:ReadOnly} = True
    DISABLE(?Button_ReverseCalc)
    ?DEL:Rate{PROP:ReadOnly} = True
    DISABLE(?Button_GetRate)
    DISABLE(?Button_ShowRates)
    ?L_CC:TotalWeight{PROP:ReadOnly} = True
    ?L_LG:FloorRate{PROP:ReadOnly} = True
    ?L_CG:MinimiumCharge{PROP:ReadOnly} = True
    ?L_CC:MinimiumChargeRate{PROP:ReadOnly} = True
    DISABLE(?Button_AdditionalCharges)
    ?DEL:AdditionalCharge{PROP:ReadOnly} = True
    ?DEL:Charge{PROP:ReadOnly} = True
    DISABLE(?Button_ChangeVAT)
    ?DEL:DocumentCharge{PROP:ReadOnly} = True
    DISABLE(?Button_Change_Doc)
    ?DEL:VATRate{PROP:ReadOnly} = True
    ?L_CC:FuelSurchargePer{PROP:ReadOnly} = True
    ?DEL:FuelSurcharge{PROP:ReadOnly} = True
    ?DEL:TollRate{PROP:ReadOnly} = True
    ?DEL:TollCharge{PROP:ReadOnly} = True
    ?L_CC:TotalCharge{PROP:ReadOnly} = True
    ?L_CC:VAT{PROP:ReadOnly} = True
    ?L_CC:TotalCharge_VAT{PROP:ReadOnly} = True
    DISABLE(?Insert:9)
    DISABLE(?Change:9)
    DISABLE(?Delete:9)
    DISABLE(?Button_ChangeBranch)
    DISABLE(?DEL:Delivered)
    DISABLE(?Insert:7)
    DISABLE(?Change:7)
    DISABLE(?Delete:7)
    DISABLE(?DEL:Manifested)
    DISABLE(?Button_UpdateManifested)
    ?L_DC:Weight{PROP:ReadOnly} = True
    ?L_DC:Volume{PROP:ReadOnly} = True
    ?DELC:Weight{PROP:ReadOnly} = True
    ?DELC:Volume{PROP:ReadOnly} = True
    DISABLE(?Button_Specify)
    DISABLE(?Button_DB_Display)
    DISABLE(?Button_Value)
    ?L_SG:Value{PROP:ReadOnly} = True
    DISABLE(?BUTTON_UpdatePods)
  END
      IF SELF.Request = ViewRecord
         ?DEL:SpecialDeliveryInstructions{PROP:ReadOnly}  = TRUE
         ?DEL:Notes{PROP:ReadOnly}                        = TRUE
  
         ENABLE(?Button_Value)
         ENABLE(?BUTTON_UpdatePods)
         ENABLE(?BUTTON_viewdocs)
      .
      DO Apply_User_Permissions
      IF INV:DID = DEL:DID          ! Invoiced
         ENABLE(?Button_View_Invoice)
      .
  
      ENABLE(?View)
      ENABLE(?View:2)
  
      ENABLE(?Button_UpdateManifested)
      ENABLE(?Button_ChangeBranch)
  
  
  
  
      IF SELF.Request = ChangeRecord OR SELF.Request = ViewRecord
         ! Check, if not delivered then if Release required, show button
         ! Not Delivered|Partially Delivered|Delivered          
  
         CASE Get_User_Access(GLO:UID, 'Menus', 'Update_Deliveries', 'DI Release', 1)     ! Disable by default
         OF 0 OROF 100
            !ENABLE(?BrowseOperationsDeliveriestoRelease)
            IF DEL:ReleasedUID ~= 0
               User_" = ''
               Get_User_Info(1, User_", DEL:ReleasedUID)
               ?Button_Release{PROP:Tip}     = 'This DI was released by ' & CLIP(User_")
  
               ?Button_Release{PROP:Icon}    = '~dotgreen.ico'
               UNHIDE(?Button_Release)
            ELSE
               IF INV:DID = DEL:DID          ! Invoiced - we will only check the Release requirement when invoiced & not delivered
                  IF DEL:Delivered < 2
                     ! (<*STRING>, ULONG=0, <*STRING>),LONG
                     ! (p_Statement_Info, p:DID, p_Release_Status_Str)
                     CASE Check_Delivery_Release(, DEL:DID, LOC:ReleaseReason)
                     OF 1                                      ! Released - shouldnt be here, see DEL:ReleadedUID
                     OF 0
                        ! No release needed
                        ?Button_Release{PROP:Icon} = '~dotyellow.ico'
                        ?Button_Release{PROP:Tip}  = 'This DI does not require releasing'
                        UNHIDE(?Button_Release)
                     ELSE
                        ?Button_Release{PROP:Tip}  = 'To release this DI click here (' & CLIP(LOC:ReleaseReason) & ')'
                        UNHIDE(?Button_Release)
            .  .  .  .
         OF 2
            !HIDE(?BrowseOperationsDeliveriestoRelease)
         ELSE
            !DISABLE(?BrowseOperationsDeliveriestoRelease)
         .
  
         IF ?Button_Release{PROP:Hide} = FALSE
            ENABLE(?Button_Release)
      .  .
  
  BRW_DeliveryItems.Q &= Queue:Browse:4
  BRW4::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon DELI:ItemNo for sort order 1
  BRW_DeliveryItems.AddSortOrder(BRW4::Sort0:StepClass,DELI:FKey_DID_ItemNo) ! Add the sort order for DELI:FKey_DID_ItemNo for sort order 1
  BRW_DeliveryItems.AddRange(DELI:DID,Relate:DeliveryItems,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW_DeliveryItems.AddLocator(BRW4::Sort0:Locator) ! Browse has a locator for sort order 1
  BRW4::Sort0:Locator.Init(,DELI:ItemNo,1,BRW_DeliveryItems) ! Initialize the browse locator using  using key: DELI:FKey_DID_ItemNo , DELI:ItemNo
  BRW_DeliveryItems.AddField(DELI:ItemNo,BRW_DeliveryItems.Q.DELI:ItemNo) ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(COM:Commodity,BRW_DeliveryItems.Q.COM:Commodity) ! Field COM:Commodity is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:Weight,BRW_DeliveryItems.Q.DELI:Weight) ! Field DELI:Weight is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:ContainerNo,BRW_DeliveryItems.Q.DELI:ContainerNo) ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:ContainerVessel,BRW_DeliveryItems.Q.DELI:ContainerVessel) ! Field DELI:ContainerVessel is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:ETA,BRW_DeliveryItems.Q.DELI:ETA) ! Field DELI:ETA is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(CTYP:ContainerType,BRW_DeliveryItems.Q.CTYP:ContainerType) ! Field CTYP:ContainerType is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(CONO:ContainerOperator,BRW_DeliveryItems.Q.CONO:ContainerOperator) ! Field CONO:ContainerOperator is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(ADD:AddressName,BRW_DeliveryItems.Q.ADD:AddressName) ! Field ADD:AddressName is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:Length,BRW_DeliveryItems.Q.DELI:Length) ! Field DELI:Length is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:Breadth,BRW_DeliveryItems.Q.DELI:Breadth) ! Field DELI:Breadth is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:Height,BRW_DeliveryItems.Q.DELI:Height) ! Field DELI:Height is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(L_IG:MID,BRW_DeliveryItems.Q.L_IG:MID) ! Field L_IG:MID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:DIID,BRW_DeliveryItems.Q.DELI:DIID) ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:DID,BRW_DeliveryItems.Q.DELI:DID) ! Field DELI:DID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(ADD:AID,BRW_DeliveryItems.Q.ADD:AID) ! Field ADD:AID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(PACK:PTID,BRW_DeliveryItems.Q.PACK:PTID) ! Field PACK:PTID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(COM:CMID,BRW_DeliveryItems.Q.COM:CMID) ! Field COM:CMID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(CTYP:CTID,BRW_DeliveryItems.Q.CTYP:CTID) ! Field CTYP:CTID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(CONO:COID,BRW_DeliveryItems.Q.CONO:COID) ! Field CONO:COID is a hot field or requires assignment from browse
  BRW_DelItemsAlias.Q &= Queue:Browse
  BRW_DelItemsAlias.AddSortOrder(,A_DELI2:FKey_DID_ItemNo) ! Add the sort order for A_DELI2:FKey_DID_ItemNo for sort order 1
  BRW_DelItemsAlias.AddRange(A_DELI2:DID,DEL:DID) ! Add single value range limit for sort order 1
  BRW_DelItemsAlias.AddLocator(BRW20::Sort0:Locator) ! Browse has a locator for sort order 1
  BRW20::Sort0:Locator.Init(,A_DELI2:ItemNo,1,BRW_DelItemsAlias) ! Initialize the browse locator using  using key: A_DELI2:FKey_DID_ItemNo , A_DELI2:ItemNo
  BRW_DelItemsAlias.AddField(A_DELI2:ItemNo,BRW_DelItemsAlias.Q.A_DELI2:ItemNo) ! Field A_DELI2:ItemNo is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(COM:Commodity,BRW_DelItemsAlias.Q.COM:Commodity) ! Field COM:Commodity is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(PACK:Packaging,BRW_DelItemsAlias.Q.PACK:Packaging) ! Field PACK:Packaging is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:Units,BRW_DelItemsAlias.Q.A_DELI2:Units) ! Field A_DELI2:Units is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:Weight,BRW_DelItemsAlias.Q.A_DELI2:Weight) ! Field A_DELI2:Weight is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:Volume,BRW_DelItemsAlias.Q.A_DELI2:Volume) ! Field A_DELI2:Volume is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:VolumetricWeight,BRW_DelItemsAlias.Q.A_DELI2:VolumetricWeight) ! Field A_DELI2:VolumetricWeight is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:Length,BRW_DelItemsAlias.Q.A_DELI2:Length) ! Field A_DELI2:Length is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:Breadth,BRW_DelItemsAlias.Q.A_DELI2:Breadth) ! Field A_DELI2:Breadth is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:Height,BRW_DelItemsAlias.Q.A_DELI2:Height) ! Field A_DELI2:Height is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(L_IG:MID,BRW_DelItemsAlias.Q.L_IG:MID) ! Field L_IG:MID is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:DIID,BRW_DelItemsAlias.Q.A_DELI2:DIID) ! Field A_DELI2:DIID is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:DID,BRW_DelItemsAlias.Q.A_DELI2:DID) ! Field A_DELI2:DID is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(PACK:PTID,BRW_DelItemsAlias.Q.PACK:PTID) ! Field PACK:PTID is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(COM:CMID,BRW_DelItemsAlias.Q.COM:CMID) ! Field COM:CMID is a hot field or requires assignment from browse
  BRW6.Q &= Queue:Browse:6
  BRW6::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon DELL:Leg for sort order 1
  BRW6.AddSortOrder(BRW6::Sort0:StepClass,DELL:FKey_DID_Leg) ! Add the sort order for DELL:FKey_DID_Leg for sort order 1
  BRW6.AddRange(DELL:DID,Relate:DeliveryLegs,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW6.AddLocator(BRW6::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW6::Sort0:Locator.Init(,DELL:Leg,1,BRW6)      ! Initialize the browse locator using  using key: DELL:FKey_DID_Leg , DELL:Leg
  BRW6.AddField(DELL:Leg,BRW6.Q.DELL:Leg)         ! Field DELL:Leg is a hot field or requires assignment from browse
  BRW6.AddField(TRA:TransporterName,BRW6.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW6.AddField(JOU:Journey,BRW6.Q.JOU:Journey)   ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW6.AddField(L_DLG:Total,BRW6.Q.L_DLG:Total)   ! Field L_DLG:Total is a hot field or requires assignment from browse
  BRW6.AddField(DELL:Cost,BRW6.Q.DELL:Cost)       ! Field DELL:Cost is a hot field or requires assignment from browse
  BRW6.AddField(L_DLG:VAT,BRW6.Q.L_DLG:VAT)       ! Field L_DLG:VAT is a hot field or requires assignment from browse
  BRW6.AddField(DELL:VATRate,BRW6.Q.DELL:VATRate) ! Field DELL:VATRate is a hot field or requires assignment from browse
  BRW6.AddField(DELL:DLID,BRW6.Q.DELL:DLID)       ! Field DELL:DLID is a hot field or requires assignment from browse
  BRW6.AddField(DELL:DID,BRW6.Q.DELL:DID)         ! Field DELL:DID is a hot field or requires assignment from browse
  BRW6.AddField(JOU:JID,BRW6.Q.JOU:JID)           ! Field JOU:JID is a hot field or requires assignment from browse
  BRW6.AddField(TRA:TID,BRW6.Q.TRA:TID)           ! Field TRA:TID is a hot field or requires assignment from browse
  BRW8.Q &= Queue:Browse:8
  BRW8::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon DELP:DID for sort order 1
  BRW8.AddSortOrder(BRW8::Sort0:StepClass,DELP:FKey_DID) ! Add the sort order for DELP:FKey_DID for sort order 1
  BRW8.AddRange(DELP:DID,Relate:DeliveryProgress,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW8.AddField(DELS:DeliveryStatus,BRW8.Q.DELS:DeliveryStatus) ! Field DELS:DeliveryStatus is a hot field or requires assignment from browse
  BRW8.AddField(ADDC:ContactName,BRW8.Q.ADDC:ContactName) ! Field ADDC:ContactName is a hot field or requires assignment from browse
  BRW8.AddField(DELP:StatusDate,BRW8.Q.DELP:StatusDate) ! Field DELP:StatusDate is a hot field or requires assignment from browse
  BRW8.AddField(DELP:ActionDate,BRW8.Q.DELP:ActionDate) ! Field DELP:ActionDate is a hot field or requires assignment from browse
  BRW8.AddField(DELP:StatusTime,BRW8.Q.DELP:StatusTime) ! Field DELP:StatusTime is a hot field or requires assignment from browse
  BRW8.AddField(DELP:ActionTime,BRW8.Q.DELP:ActionTime) ! Field DELP:ActionTime is a hot field or requires assignment from browse
  BRW8.AddField(DELP:DPID,BRW8.Q.DELP:DPID)       ! Field DELP:DPID is a hot field or requires assignment from browse
  BRW8.AddField(DELP:DID,BRW8.Q.DELP:DID)         ! Field DELP:DID is a hot field or requires assignment from browse
  BRW8.AddField(ADDC:ACID,BRW8.Q.ADDC:ACID)       ! Field ADDC:ACID is a hot field or requires assignment from browse
  BRW8.AddField(DELS:DSID,BRW8.Q.DELS:DSID)       ! Field DELS:DSID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_Deliveries',QuickWindow)   ! Restore window settings from non-volatile store
      L_SG:Value_Type     = GETINI('Delivery', 'Value_Type', 0, GLO:Local_INI)
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      IF SELF.Request = InsertRecord
         ! Set the floor
         DEL:FID                  = Get_Branch_Info(DEL:BID, 3)
  
         DEL:VATRate              = Get_Setup_Info(4)
  
         DEL:SID                  = GETINI('Delivery', 'Default_DI_SID', 0, GLO:Local_INI)
      ELSE
         L_LG:CID                 = DEL:CID                                       ! Set our existing CID
  
         L_LG:Charge_Specified    = TRUE
         ENABLE(?Sheet_Items)
      .
  
      L_SG:VATRate_Previous       = DEL:VATRate                                   ! Record the VAT rate on entry
  
      LOC:GeneralRatesClientID    = Get_Branch_Info(,4)
      DO Load_Lookups                 ! Load lookup fields & additionals
      IF SELF.Request ~= InsertRecord
         ! Set screen values
         L_CC:TotalWeight     = Get_DelItem_s_Totals(DEL:DID,0,,,TRUE) / 100         ! Returns a Ulong
         L_CC:TotalUnits      = Get_DelItem_s_Totals(DEL:DID,3,,,TRUE)
      .
  
      L_CC:TotalCharge        = DEL:Charge + DEL:AdditionalCharge + DEL:DocumentCharge + DEL:FuelSurcharge + DEL:TollCharge + L_CC:InsuanceAmount
      !L_CC:TotalCharge        = DEL:Charge + DEL:DocumentCharge + L_CC:InsuanceAmount + DEL:FuelSurcharge
      L_LG:Last_Rate          = DEL:Rate
  
      L_CC:VAT                = L_CC:TotalCharge * (DEL:VATRate / 100)
      L_CC:TotalCharge_VAT    = L_CC:TotalCharge + L_CC:VAT
  
      IF LOC:Manifested = TRUE                                          ! p:Option used here
         L_SG:Un_Manifested_Units   = Get_DelItem_s_Totals(DEL:DID, 1,,,TRUE)
         
         Get_Delivery_ManIDs(DEL:DID, 0, MIDs_", 1)
         
         MAN:MID = Get_1st_Element_From_Delim_Str(MIDs_", ',', false)
         IF Access:Manifest.Fetch(MAN:PKey_MID) = Level:Benign            
            ENABLE(?BUTTON_Manifest)
         .
         
  
         IF L_SG:Un_Manifested_Units > 0
            ?Prompt_Manifest{PROP:Text}  = 'Delivery Manifested (Un: ' & L_SG:Un_Manifested_Units & ' / Man: ' & L_SG:Manifested_Units & ') - No editing. (MID''s: ' & CLIP(MIDs_") & ')'
         ELSE
            !?Prompt_Manifest{PROP:Text}  = 'Delivery has been fully Manifested (Man. no.: ' &  & ') - No editing.'
            IF INV:IID ~= 0
               IF p:Option = 1
                  ?Prompt_Manifest{PROP:Text}  = 'Delivery fully Manifested - ADMIN editing. (Inv.: ' & INV:IID & ', MID''s: ' & CLIP(MIDs_") & ')'
               ELSE
                  ?Prompt_Manifest{PROP:Text}  = 'Delivery fully Manifested - No editing. (Inv.: ' & INV:IID & ', MID''s: ' & CLIP(MIDs_") & ')'
               .
            ELSE
               IF p:Option = 1
                  ?Prompt_Manifest{PROP:Text}  = 'Delivery fully Manifested - ADMIN editing. (MID''s: ' & CLIP(MIDs_") & ')'
               ELSE
                  ?Prompt_Manifest{PROP:Text}  = 'Delivery fully Manifested - No editing. (MID''s: ' & CLIP(MIDs_") & ')'
         .  .  .
  
         ! Get_TripDelItems_Info
         ! Check that they have been delivered
         !
         ! (p:TRID, p:Option, p:DID, p:TDID, p:DIID, p:Del_List)
         ! (<ULONG>, BYTE=0, <ULONG>, <ULONG>, <ULONG>, <*STRING>),ULONG
         !   1         2        3        4       5           6
         ! p:Del_List
         !   Either list of Dates & Times or TRID's
         ! p:Option
         ! TRID passed
         !   0. Weight (real weight - not volumetric)
         !   1. Units
         !       if p:DID is provided then the result is limited to the DID passed
         !       if p:TDID is provided then exclude this from weight
         ! DID passed only
         !   2. List of Delivery Dates, no of entries returned - for DIID!
         !   3. List of Delivered Date & Times
         !   4. List of TRIDs
  
         CLEAR(L_SG:Tripsheets_Info)
         IF Get_TripDelItems_Info(, 4, DEL:DID,,, L_SG:Tripsheets_Info) > 0
         
            TRI:TRID = Get_1st_Element_From_Delim_Str(L_SG:Tripsheets_Info, ',', false)
            IF (TRI:TRID) 
               ENABLE(?BUTTON_Tripsheet)
            .   
         
            ?Prompt_Manifest{PROP:Text}   = CLIP(?Prompt_Manifest{PROP:Text}) & ', Delivered. (TRID''s: ' & CLIP(L_SG:Tripsheets_Info)
  
            CLEAR(L_SG:Tripsheets_Info)
            IF Get_TripDelItems_Info(, 3, DEL:DID,,, L_SG:Tripsheets_Info) > 0
            .
            L_SG:Tripsheets_Info          = Get_1st_Element_From_Delim_Str(L_SG:Tripsheets_Info, ',')
            ?Prompt_Manifest{PROP:Text}   = CLIP(?Prompt_Manifest{PROP:Text}) & ', Last Del.: ' & CLIP(L_SG:Tripsheets_Info) & ')'
         .
  
         DISABLE(?DEL:Insure)
      .
  IF ?DEL:Insure{Prop:Checked}
    ENABLE(?Group_Insured)
  END
  IF NOT ?DEL:Insure{PROP:Checked}
    DISABLE(?Group_Insured)
  END
  BRW_DeliveryItems.AskProcedure = 10             ! Will call: Update_DeliveryItems(, L_DC:First_DID)
  BRW_DelItemsAlias.AskProcedure = 11             ! Will call: Update_DeliveryItems_h(L_DC:First_DID)
  BRW6.AskProcedure = 12                          ! Will call: Update_DeliveryLegs
  BRW8.AskProcedure = 13                          ! Will call: Update_DeliveryProgress
  FDB27.Init(?L_LG:FloorRate,Queue:FileDrop.ViewPosition,FDB27::View:FileDrop,Queue:FileDrop,Relate:Floors,ThisWindow)
  FDB27.Q &= Queue:FileDrop
  FDB27.AddSortOrder(FLO:Key_Floor)
  FDB27.AddField(FLO:Floor,FDB27.Q.FLO:Floor) !List box control field - type derived from field
  FDB27.AddField(FLO:FID,FDB27.Q.FLO:FID) !Primary key field - type derived from field
  FDB27.AddUpdateField(FLO:FID,DEL:FIDRate)
  ThisWindow.AddItem(FDB27.WindowComponent)
  BRW_DelItemsAlias.AddToolbarTarget(Toolbar)     ! Browse accepts toolbar control
      ! User stuff
      IF GLO:AccessLevel >= 100
         UNHIDE(?Tab_DB)
      .
      IF GLO:AccessLevel >= 10
         ENABLE(?Button_Change_Doc)
      .
  
  
      !   (p:Info, p:Value, p:UID, p:Login)
      !   (BYTE, *STRING, <ULONG>, <STRING>),LONG,PROC
      !   p:Info  1   - Login
      !           2   - Password
      !           3   - User Name + Surname
      !           4   - Name
      !           5   - Surname
      !           6   - Access Level
      !           7   - UID
      User_"  = ''
      Get_User_Info(1, User_", DEL:UID)
      ?String_User{PROP:Text} = CLIP(User_")
      DO Value_Type
    ?Button_ClientEmails{PROP:Tip} = 'Clients Operations emails: ' & CLIP(Get_Client_Emails(DEL:CID,2,1))
  BRW4::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW4::FormatManager.Init('MANTRNIS','Update_Deliveries',1,?Browse:ItemsContainers,4,BRW4::PopupTextExt,Queue:Browse:4,13,LFM_CFile,LFM_CFile.Record)
  BRW4::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW20::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW20::FormatManager.Init('MANTRNIS','Update_Deliveries',1,?Browse:ItemsLoose,20,BRW20::PopupTextExt,Queue:Browse,12,LFM_CFile,LFM_CFile.Record)
  BRW20::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW6::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW6::FormatManager.Init('MANTRNIS','Update_Deliveries',1,?Browse:6,6,BRW6::PopupTextExt,Queue:Browse:6,7,LFM_CFile,LFM_CFile.Record)
  BRW6::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW8::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW8::FormatManager.Init('MANTRNIS','Update_Deliveries',1,?Browse:8,8,BRW8::PopupTextExt,Queue:Browse:8,6,LFM_CFile,LFM_CFile.Record)
  BRW8::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
        UNBIND('MALD:DIID')
        UNBIND('A_DELI:DIID')
        UNBIND('DELI:DIID')
  
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressAlias.Close
    Relate:DeliveriesAlias.Close
    Relate:_SQLTemp.Close
    Relate:_View_ContainerParkRates_Client.Close
    Relate:_View_Rates_Client.Close
    Relate:_View_Rates_Client_Journeys.Close
    Relate:_View_Rates_Clients_LoadTypes.Close
  END
  ! List Format Manager destructor
  BRW4::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW20::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW6::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW8::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_Deliveries',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  DEL:BID = GLO:BranchID
  DEL:DIDate = TODAY()
  DEL:DITime = CLOCK()
  DEL:MultipleManifestsAllowed = 1
  PARENT.PrimeFields


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  DELC:DELCID = DEL:DELCID                                 ! Assign linking field value
  Access:DeliveryComposition.Fetch(DELC:PKey_DELCID)
  PARENT.Reset(Force)
      IF SELF.Request = ViewRecord
         ENABLE(?View:2)
         ENABLE(?View)
      .


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Clients(, , L_LG:ClientName)
      Browse_LoadTypes('V', DEL:CID)
      Browse_ClientsRateTypes(DEL:CID, DEL:LTID)
      Select_Journey(DEL:CID, DEL:LTID, DEL:FIDRate)
      Browse_Floors('')
      Browse_Addresses(, 'DC')
      Browse_Addresses_Alias_h(, 'DC')
      Browse_Drivers('','')
      Browse_ServiceRequirements
      Update_DeliveryItems(, L_DC:First_DID)
      Update_DeliveryItems_h(L_DC:First_DID)
      Update_DeliveryLegs
      Update_DeliveryProgress
    END
    ReturnValue = GlobalResponse
  END
    IF (Number = 10 OR Number = 11)         ! Update_DeliveryItems AND Update_DeliveryItems_h
       IF SELF.Request = ViewRecord
          CASE Number
          OF 10
             IF DELI:DIID = 0
                MESSAGE('Please select a Delivery Item.', 'Select Delivery Item', ICON:Exclamation)
             ELSE
                GlobalRequest  = ViewRecord
                Update_DeliveryItems
             .
          OF 11
             IF A_DELI:DIID = 0
                MESSAGE('Please select a Delivery Item.', 'Select Delivery Item', ICON:Exclamation)
             ELSE
                GlobalRequest  = ViewRecord
                Update_DeliveryItems_h
          .  .
       ELSE
    db.debugout('[Update_Deliveries]  RUN() - After Run - Number: ' & Number)
  
          DO Charge_Calc
  
          ! This is needed - otherwise missing record errors from ABC class.. 8 Dec 04
          ! BRW_DelItemsAlias.ResetFromFile() done in event
          POST(EVENT:User)
  
    db.debugout('[Update_Deliveries]  RUN() - After Run - After Charge calc - Number: ' & Number)
    .  .
  
    CLEAR(L_DC:First_DID)           ! When it is set, it is to be used only once
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DEL:DINo
          ! Check this DI No.
          IF SELF.Request = InsertRecord
             CLEAR(A_DEL:Record)
             A_DEL:DINo  = DEL:DINo
             IF Access:DeliveriesAlias.TryFetch(A_DEL:Key_DINo) = LEVEL:Benign
                ! Maybe this is this record though, check the internal ID
                IF A_DEL:DID = DEL:DID
                   ! This is our current record!
                ELSE
                   MESSAGE('There already exists a DI with this DI No.', 'DI Exists', ICON:Exclamation)
                   SELECT(?DEL:DINo)
                   CYCLE
          .  .  .
    OF ?DEL:DIDate
          DO Date_Changed
    OF ?Calendar
      ThisWindow.Update()
      Calendar22.SelectOnClose = True
      Calendar22.Ask('Select a Date',DEL:DIDate)
      IF Calendar22.Response = RequestCompleted THEN
      DEL:DIDate=Calendar22.SelectedDate
      DISPLAY(?DEL:DIDate)
      END
      ThisWindow.Reset(True)
    OF ?CallLookup_Client
      ThisWindow.Update()
      CLI:ClientName = L_LG:ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_LG:ClientName = CLI:ClientName
        DEL:CID = CLI:CID
        DEL:DocumentCharge = CLI:DocumentCharge
        DEL:Terms = CLI:Terms
        L_CG:MinimiumCharge = CLI:MinimiumCharge
        L_CG:ClientTerms = CLI:Terms
        L_LG:ClientNo = CLI:ClientNo
        L_CG:ClientStatus = CLI:Status
        L_LG:PODMessage = CLI:PODMessage
      END
      ThisWindow.Reset(1)
          DO New_Client
    OF ?L_LG:ClientName
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_LG:ClientName OR ?L_LG:ClientName{PROP:Req}
          CLI:ClientName = L_LG:ClientName
          IF Access:Clients.TryFetch(CLI:Key_ClientName)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              L_LG:ClientName = CLI:ClientName
              DEL:CID = CLI:CID
              DEL:DocumentCharge = CLI:DocumentCharge
              DEL:Terms = CLI:Terms
              L_CG:MinimiumCharge = CLI:MinimiumCharge
              L_CG:ClientTerms = CLI:Terms
              L_LG:ClientNo = CLI:ClientNo
              L_CG:ClientStatus = CLI:Status
              L_LG:PODMessage = CLI:PODMessage
            ELSE
              CLEAR(DEL:CID)
              CLEAR(DEL:DocumentCharge)
              CLEAR(DEL:Terms)
              CLEAR(L_CG:MinimiumCharge)
              CLEAR(L_CG:ClientTerms)
              CLEAR(L_LG:ClientNo)
              CLEAR(L_CG:ClientStatus)
              CLEAR(L_LG:PODMessage)
              SELECT(?L_LG:ClientName)
              CYCLE
            END
          ELSE
            DEL:CID = CLI:CID
            DEL:DocumentCharge = CLI:DocumentCharge
            DEL:Terms = CLI:Terms
            L_CG:MinimiumCharge = CLI:MinimiumCharge
            L_CG:ClientTerms = CLI:Terms
            L_LG:ClientNo = CLI:ClientNo
            L_CG:ClientStatus = CLI:Status
            L_LG:PODMessage = CLI:PODMessage
          END
        END
      END
      ThisWindow.Reset()
          DO New_Client
    OF ?Button_ClientEmails
      ThisWindow.Update()
        ! Load the client form with Emails list showing only...        
            CLI:CID = DEL:CID
            IF Access:Clients.TryFetch(CLI:PKey_CID) = Level:Benign
              GlobalRequest = ChangeRecord
              
              Update_Clients(2)
              
              ?Button_ClientEmails{PROP:Tip} = 'Clients Operations emails: ' & CLIP(Get_Client_Emails(DEL:CID, 2, 1))
            .
            
    OF ?L_LG:ClientNo
          ! Lookup the client
          IF L_LG:ClientNo ~= 0
             CLI:ClientNo         = L_LG:ClientNo
             IF Access:Clients.TryFetch(CLI:Key_ClientNo) = LEVEL:Benign
                L_LG:ClientName   = CLI:ClientName
      
                POST(EVENT:Accepted, ?L_LG:ClientName)      ! Assign all other fields!
             ELSE
                MESSAGE('Cannot find a client with the Client No. specified: ' & L_LG:ClientNo, 'Update Deliveries', ICON:Exclamation)
      
                CLEAR(L_LG:ClientNo)
                SELECT(?L_LG:ClientNo)
                DISPLAY
                CYCLE
          .  .
    OF ?CallLookup_LoadType
      ThisWindow.Update()
      LOAD2:LoadType = L_LG:LoadType
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_LG:LoadType = LOAD2:LoadType
        DEL:LTID = LOAD2:LTID
      END
      ThisWindow.Reset(1)
          DO New_Load_Type
    OF ?L_LG:LoadType
      IF L_LG:LoadType OR ?L_LG:LoadType{PROP:Req}
        LOAD2:LoadType = L_LG:LoadType
        IF Access:LoadTypes2.TryFetch(LOAD2:Key_LoadType)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            L_LG:LoadType = LOAD2:LoadType
            DEL:LTID = LOAD2:LTID
          ELSE
            CLEAR(DEL:LTID)
            SELECT(?L_LG:LoadType)
            CYCLE
          END
        ELSE
          DEL:LTID = LOAD2:LTID
        END
      END
      ThisWindow.Reset()
          DO New_Load_Type
    OF ?Button_ListRates
      ThisWindow.Update()
                         ! (p:DID  , p:LTID  , p:CID  , p:JID  , p:DIDate  , p:FIDRate  , p:SelectedRate    , p:Mass          , p:MinimiumChargeRate   , p:To_Weight   , p:Effective_Date)
          IF Delivery_Rates(DEL:DID, DEL:LTID, DEL:CID, DEL:JID, DEL:DIDate, DEL:FIDRate, L_RG:Returned_Rate, L_CC:TotalWeight, L_CC:MinimiumChargeRate, L_RG:To_Weight, L_RG:Effective_Date) > 0
             db.debugout('[Update_Deliveries]   2 L_CC:MinimiumChargeRate: ' & L_CC:MinimiumChargeRate & ',  L_RG:To_Weight: ' & L_RG:To_Weight & ',  L_RG:Effective_Date: ' & FORMAT(L_RG:Effective_Date,@d5))
      
             DEL:Rate            = L_RG:Returned_Rate
      
             L_RG:Selected_Rate  = DEL:Rate
      
             L_LG:Rate_Specified = TRUE
      
      !       POST(EVENT:Accepted, ?DEL:Rate)
          .
      
      !    ThisWindow.Reset
    OF ?CallLookup
      ThisWindow.Update()
      CRT:ClientRateType = L_LG:ClientRateType
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        L_LG:ClientRateType = CRT:ClientRateType
        DEL:CRTID = CRT:CRTID
        L_LG:ClientRateType_CID = CRT:CID
      END
      ThisWindow.Reset(1)
    OF ?L_LG:ClientRateType
         CRT:CID = DEL:CID
      
         IF L_LG:ClientRateType OR ?L_LG:ClientRateType{Prop:Req}
            CRT:ClientRateType = L_LG:ClientRateType
            IF Access:ClientsRateTypes.TryFetch(CRT:SKey_CID_ClientRateType) ~= LEVEL:Benign
               CRT:CID   = LOC:GeneralRatesClientID         ! Try the setup client then
         .  .
      
        db.debugout('[Update_Deliveries]  1.  DEL:CID: ' & DEL:CID & ',  CRT:CID: '& CRT:CID & ',  L_LG:ClientRateType_CID: ' & L_LG:ClientRateType_CID & ',  LOC:GeneralRatesClientID: ' & LOC:GeneralRatesClientID)
      
      
      IF L_LG:ClientRateType OR ?L_LG:ClientRateType{PROP:Req}
        CRT:ClientRateType = L_LG:ClientRateType
        IF Access:ClientsRateTypes.TryFetch(CRT:SKey_CID_ClientRateType)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            L_LG:ClientRateType = CRT:ClientRateType
            DEL:CRTID = CRT:CRTID
            L_LG:ClientRateType_CID = CRT:CID
          ELSE
            CLEAR(DEL:CRTID)
            CLEAR(L_LG:ClientRateType_CID)
            SELECT(?L_LG:ClientRateType)
            CYCLE
          END
        ELSE
          DEL:CRTID = CRT:CRTID
          L_LG:ClientRateType_CID = CRT:CID
        END
      END
      ThisWindow.Reset()
          ! Check that the Client Rate is applicable to this Client - or Setup client Rate Type
          IF L_LG:ClientRateType_CID ~= DEL:CID AND L_LG:ClientRateType_CID ~= LOC:GeneralRatesClientID
                db.debugout('[Update_Deliveries]   DEL:CID: ' & DEL:CID & ',  L_LG:ClientRateType_CID: ' & L_LG:ClientRateType_CID & ', LOC:GeneralRatesClientID: ' & LOC:GeneralRatesClientID)
      
             CLEAR(CRT:Record)
             CLEAR(L_LG:ClientRateType)
             CLEAR(DEL:CRTID)
      
             SELECT(?L_LG:ClientRateType)
          .
    OF ?CallLookup_Journey
      ThisWindow.Update()
      JOU:Journey = L_LG:Journey
      IF SELF.Run(4,SelectRecord) = RequestCompleted
        L_LG:Journey = JOU:Journey
        DEL:JID = JOU:JID
      END
      ThisWindow.Reset(1)
          DO New_Journey
      
          IF DEL:CollectionAID ~= 0 AND DEL:DeliveryAID ~= 0
             SELECT(?CallLookup_CollectAdd)
          .
    OF ?L_LG:Journey
      IF L_LG:Journey OR ?L_LG:Journey{PROP:Req}
        JOU:Journey = L_LG:Journey
        IF Access:Journeys.TryFetch(JOU:Key_Journey)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            L_LG:Journey = JOU:Journey
            DEL:JID = JOU:JID
          ELSE
            CLEAR(DEL:JID)
            SELECT(?L_LG:Journey)
            CYCLE
          END
        ELSE
          DEL:JID = JOU:JID
        END
      END
      ThisWindow.Reset()
          DO New_Journey
    OF ?CallLookup_Floor
      ThisWindow.Update()
      FLO:Floor = L_LG:Floor
      IF SELF.Run(5,SelectRecord) = RequestCompleted
        L_LG:Floor = FLO:Floor
        DEL:FID = FLO:FID
      END
      ThisWindow.Reset(1)
          DO Floor_Changed
    OF ?L_LG:Floor
      IF L_LG:Floor OR ?L_LG:Floor{PROP:Req}
        FLO:Floor = L_LG:Floor
        IF Access:Floors.TryFetch(FLO:Key_Floor)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            L_LG:Floor = FLO:Floor
            DEL:FID = FLO:FID
          ELSE
            CLEAR(DEL:FID)
            SELECT(?L_LG:Floor)
            CYCLE
          END
        ELSE
          DEL:FID = FLO:FID
        END
      END
      ThisWindow.Reset()
          DO Floor_Changed
    OF ?CallLookup_CollectAdd
      ThisWindow.Update()
      ADD:AddressName = L_LG:From_Address
      IF SELF.Run(6,SelectRecord) = RequestCompleted
        L_LG:From_Address = ADD:AddressName
        DEL:CollectionAID = ADD:AID
      END
      ThisWindow.Reset(1)
          DO Validate_Col_Add
    OF ?L_LG:From_Address
      IF L_LG:From_Address OR ?L_LG:From_Address{PROP:Req}
        ADD:AddressName = L_LG:From_Address
        IF Access:Addresses.TryFetch(ADD:Key_Name)
          IF SELF.Run(6,SelectRecord) = RequestCompleted
            L_LG:From_Address = ADD:AddressName
            DEL:CollectionAID = ADD:AID
          ELSE
            CLEAR(DEL:CollectionAID)
            SELECT(?L_LG:From_Address)
            CYCLE
          END
        ELSE
          DEL:CollectionAID = ADD:AID
        END
      END
      ThisWindow.Reset()
          DO Validate_Col_Add
    OF ?CallLookup_ToAdd
      ThisWindow.Update()
      A_ADD:AddressName = L_LG:To_Address
      IF SELF.Run(7,SelectRecord) = RequestCompleted
        L_LG:To_Address = A_ADD:AddressName
        DEL:DeliveryAID = A_ADD:AID
      END
      ThisWindow.Reset(1)
          DO Validate_Del_Add
    OF ?L_LG:To_Address
      IF L_LG:To_Address OR ?L_LG:To_Address{PROP:Req}
        A_ADD:AddressName = L_LG:To_Address
        IF Access:AddressAlias.TryFetch(A_ADD:Key_Name)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            L_LG:To_Address = A_ADD:AddressName
            DEL:DeliveryAID = A_ADD:AID
          ELSE
            CLEAR(DEL:DeliveryAID)
            SELECT(?L_LG:To_Address)
            CYCLE
          END
        ELSE
          DEL:DeliveryAID = A_ADD:AID
        END
      END
      ThisWindow.Reset()
          DO Validate_Del_Add
    OF ?Button_Release
      ThisWindow.Update()
          IF DEL:ReleasedUID = 0
             CASE MESSAGE('Would you like to release this DI?||Note other changes you may have made will be saved.', 'Release DI', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:Yes
                ?Button_Release{PROP:Icon} = '~dotgreen.ico'
                DEL:ReleasedUID   = GLO:UID
      
      !          User_" = ''                  - we are exiting afterwards now - so no need for this
      !          Get_User_Info(1, User_", DEL:ReleasedUID)
      !          ?Button_Release{PROP:Tip}     = 'This DI was released by ' & CLIP(User_")
      
                IF Access:Deliveries.TryUpdate() = LEVEL:Benign
                   ThisWindow.CancelAction        = Cancel:Cancel
                   POST(EVENT:Accepted, ?Cancel)                      ! Saved, so exit
             .  .
          ELSE
             CASE MESSAGE('Would you like to cancel the DI release?||Note other changes you may have made will be saved.', 'Cancel DI Release', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:Yes
                ?Button_Release{PROP:Icon}    = '~dotred.ico'
                DEL:ReleasedUID               = 0
                ?Button_Release{PROP:Tip}     = 'To release this DI click here'
      
                IF Access:Deliveries.TryUpdate() = LEVEL:Benign
          .  .  .
    OF ?CallLookup_Driver_Name
      ThisWindow.Update()
      DRI:FirstNameSurname = L_LG:Driver_Name
      IF SELF.Run(8,SelectRecord) = RequestCompleted
        L_LG:Driver_Name = DRI:FirstNameSurname
        DEL:CollectedByDRID = DRI:DRID
      END
      ThisWindow.Reset(1)
    OF ?L_LG:Driver_Name
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_LG:Driver_Name OR ?L_LG:Driver_Name{PROP:Req}
          DRI:FirstNameSurname = L_LG:Driver_Name
          IF Access:Drivers.TryFetch(DRI:SKey_FirstNameSurname)
            IF SELF.Run(8,SelectRecord) = RequestCompleted
              L_LG:Driver_Name = DRI:FirstNameSurname
              DEL:CollectedByDRID = DRI:DRID
            ELSE
              CLEAR(DEL:CollectedByDRID)
              SELECT(?L_LG:Driver_Name)
              CYCLE
            END
          ELSE
            DEL:CollectedByDRID = DRI:DRID
          END
        END
      END
      ThisWindow.Reset()
    OF ?Button_COD_Address
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_CODAddresses()
      ThisWindow.Reset
          IF GlobalResponse = RequestCompleted
             DEL:DC_ID       = DCADD:DC_ID
      
             ! Set the Tip to the COD Address
             DCADD:DC_ID     = DEL:DC_ID
             IF Access:Delivery_CODAddresses.TryFetch(DCADD:PKey_DC_ID) = LEVEL:Benign
                ?Button_COD_Address{PROP:Tip}       = 'COD Address specified is:<13,10>' & CLIP(DCADD:AddressName) & '<13,10>' & CLIP(DCADD:Line1) & |
                                                 CLIP(DCADD:Line2) & '<13,10>' & CLIP(DCADD:Line3) & '<13,10>' & CLIP(DCADD:Line4) & |
                                                 '<13,10>' & CLIP(DCADD:Line5) & '<13,10>VAT No.: ' & CLIP(DCADD:VATNo)
                ?Button_COD_Address{PROP:FontColor}  = COLOR:None
             ELSE
                ?Button_COD_Address{PROP:Tip}       = 'Specify a COD Address<13,10>This will be used on the Invoice'
                ?Button_COD_Address{PROP:FontColor}  = COLOR:Red
          .  .
    OF ?CallLookup_ServiceReq
      ThisWindow.Update()
      SERI:ServiceRequirement = L_LG:ServiceRequirement
      IF SELF.Run(9,SelectRecord) = RequestCompleted
        L_LG:ServiceRequirement = SERI:ServiceRequirement
        DEL:SID = SERI:SID
      END
      ThisWindow.Reset(1)
    OF ?L_LG:ServiceRequirement
      IF L_LG:ServiceRequirement OR ?L_LG:ServiceRequirement{PROP:Req}
        SERI:ServiceRequirement = L_LG:ServiceRequirement
        IF Access:ServiceRequirements.TryFetch(SERI:Key_ServiceRequirement)
          IF SELF.Run(9,SelectRecord) = RequestCompleted
            L_LG:ServiceRequirement = SERI:ServiceRequirement
            DEL:SID = SERI:SID
          ELSE
            CLEAR(DEL:SID)
            SELECT(?L_LG:ServiceRequirement)
            CYCLE
          END
        ELSE
          DEL:SID = SERI:SID
        END
      END
      ThisWindow.Reset()
    OF ?Lookup_EmailAddresses
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_EmailAddresses(2)
      ThisWindow.Reset
      IF GlobalResponse = RequestCompleted
        ! we should have an email address
        ! (p:Add, p:List, p:Delim, p:Option, p:Prefix, p:Beginning, p:IgnoreIfExists)
        Add_to_List(EMAI:EmailAddress, DEL:NoticeEmailAddresses, ', ',,,, 1)  
        
        DISPLAY(?DEL:NoticeEmailAddresses)
       
        ! message('added: ' & EMAI:EmailAddress )
      .
    OF ?DEL:SpecialDeliveryInstructions
      UPDATE(?DEL:SpecialDeliveryInstructions)
      L_SG:SpecialDeliveryCharsLeft = 4*36 - LEN(CLIP(DEL:SpecialDeliveryInstructions))
      IF L_SG:SpecialDeliveryCharsLeft < 0
         DEL:SpecialDeliveryInstructions = SUB(DEL:SpecialDeliveryInstructions,1,4*36)
         L_SG:SpecialDeliveryCharsLeft = 4*36 - LEN(CLIP(DEL:SpecialDeliveryInstructions))   
      .
      DISPLAY(L_SG:SpecialDeliveryCharsLeft)
    OF ?Delete
      ThisWindow.Update()
          IF RECORDS(Queue:Browse:4) OR RECORDS(Queue:Browse)
             ! We don't want Load Type changed when we already have Items with the current specified Load Type
             DISABLE(?Group_LoadType)
          ELSE
             ENABLE(?Group_LoadType)
          .
    OF ?Delete:5
      ThisWindow.Update()
          IF RECORDS(Queue:Browse:4) OR RECORDS(Queue:Browse)
             ! We don't want Load Type changed when we already have Items with the current specified Load Type
             DISABLE(?Group_LoadType)
          ELSE
             ENABLE(?Group_LoadType)
          .
    OF ?Button_Wizard
      ThisWindow.Update()
          IF ?Sheet_Items{PROP:Wizard} = TRUE
             ?Sheet_Items{PROP:Wizard}    = FALSE
             ?Sheet_Items{PROP:NoSheet}   = FALSE
          ELSE
             ?Sheet_Items{PROP:Wizard}    = TRUE
             ?Sheet_Items{PROP:NoSheet}   = TRUE
          .
    OF ?BUTTON_viewdocs
      ThisWindow.Update()
         ! load web page
         DO Load_WebDocs
         ! fbn-dis.herokuapp.com/di/######
         
         ! (whandle, URL, p:Params, p:LauchDir)
         ! (unsigned, STRING, <STRING>, <STRING>),LONG,PROC
         
         !ISExecute(Window{PROP:Handle} , 'http://fbn-dis.herokuapp.com/di/' & DEL:DINo)
    OF ?BUTTON_Manifest
      ThisWindow.Update()
      Update_Manifest()
      ThisWindow.Reset
    OF ?BUTTON_Tripsheet
      ThisWindow.Update()
      GlobalRequest = ViewRecord
      Update_TripSheet()
      ThisWindow.Reset
    OF ?Button_View_Invoice
      ThisWindow.Update()
          IF DEL:DID ~= 0
             INV:DID          = DEL:DID
             IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
                GlobalRequest    = ViewRecord
                Update_Invoice()
          .  .
    OF ?DEL:Insure
      IF ?DEL:Insure{PROP:Checked}
        ENABLE(?Group_Insured)
      END
      IF NOT ?DEL:Insure{PROP:Checked}
        DISABLE(?Group_Insured)
      END
      ThisWindow.Reset()
          DO Charge_Calc
    OF ?DEL:TotalConsignmentValue
          DO Charge_Calc
    OF ?DEL:InsuranceRate
          DO Charge_Calc
    OF ?L_CC:InsuanceAmount
          DEL:InsuranceRate   = (L_CC:InsuanceAmount / DEL:TotalConsignmentValue) * 100
          DISPLAY(?DEL:InsuranceRate)
      
          DO Charge_Calc
    OF ?Button_ReverseCalc
      ThisWindow.Update()
          ?L_CC:InsuanceAmount{PROP:ReadOnly}     = FALSE
          ?L_CC:InsuanceAmount{PROP:Background}   = -1
          ?L_CC:InsuanceAmount{PROP:Skip}         = FALSE
      
    OF ?DEL:Rate
          IF QuickWindow{PROP:AcceptAll} = FALSE
             CLEAR(L_CC:Rate_Checked)
      
             CLEAR(L_CC:MinChargeRateChecked)
             CLEAR(L_CC:UseMinRateCharge)
         !    CLEAR(L_CG:MinChargeChecked)
         !    CLEAR(L_CG:MinimiumCharge)
      
             L_LG:Charge_Specified   = FALSE
             L_LG:Rate_Specified     = TRUE
             DO Charge_Calc
          .
    OF ?Button_GetRate
      ThisWindow.Update()
          DO Get_Rate_Button
    OF ?Button_ShowRates
      ThisWindow.Update()
                         ! (p:DID  , p:LTID  , p:CID  , p:JID  , p:DIDate  , p:FIDRate  , p:SelectedRate    , p:Mass          , p:MinimiumChargeRate   , p:To_Weight   , p:Effective_Date)
          IF Delivery_Rates(DEL:DID, DEL:LTID, DEL:CID, DEL:JID, DEL:DIDate, DEL:FIDRate, L_RG:Returned_Rate, L_CC:TotalWeight, L_CC:MinimiumChargeRate, L_RG:To_Weight, L_RG:Effective_Date) > 0
             db.debugout('[Update_Deliveries]   L_CC:MinimiumChargeRate: ' & L_CC:MinimiumChargeRate & ',  L_RG:To_Weight: ' & L_RG:To_Weight & ',  L_RG:Effective_Date: ' & FORMAT(L_RG:Effective_Date,@d5))
      
             DEL:Rate            = L_RG:Returned_Rate
      
             L_RG:Selected_Rate  = DEL:Rate
      
      !       ThisWindow.Reset
      
             !IF DEL:Rate ~= 0.0    - we know it is specified - the IF!
      
             L_LG:Rate_Specified = TRUE
      
             !.
      
             POST(EVENT:Accepted, ?DEL:Rate)
          .
      
          ThisWindow.Reset
    OF ?Button_AdditionalCharges
      ThisWindow.Update()
      Browse_DeliveryAdditionalCharges_Delivery(DEL:DID, DEL:CID)
      ThisWindow.Reset
          IF DEL:CID ~= 0 AND DEL:AdditionalCharge_Calculate = TRUE
             DEL:AdditionalCharge     = Get_DeliveryAdditionalCharges(DEL:DID) / 100
             DISPLAY(?DEL:AdditionalCharge)
          .
    OF ?DEL:AdditionalCharge_Calculate
          IF DEL:CID ~= 0 AND DEL:AdditionalCharge_Calculate = TRUE
             DEL:AdditionalCharge     = Get_DeliveryAdditionalCharges(DEL:DID) / 100
             DISPLAY(?DEL:AdditionalCharge)
          .
    OF ?DEL:AdditionalCharge
          DO Charge_Calc
    OF ?DEL:Charge
          IF QuickWindow{PROP:AcceptAll} = FALSE       ! Not on accept all then - user specified
             L_LG:Charge_Specified    = TRUE
          .
      
          DO Charge_Calc
    OF ?Button_ChangeVAT
      ThisWindow.Update()
          IF ?DEL:VATRate{PROP:Skip} = FALSE
             ?DEL:VATRate{PROP:Skip}         = TRUE
             ?DEL:VATRate{PROP:ReadOnly}     = TRUE
      
             ?DEL:VATRate{PROP:Background}   = 0E9E9E9H
          ELSE
             ?DEL:VATRate{PROP:Skip}         = FALSE
             ?DEL:VATRate{PROP:ReadOnly}     = FALSE
      
             ?DEL:VATRate{PROP:Background}   = -1
          .
    OF ?DEL:DocumentCharge
          DO Charge_Calc
    OF ?Button_Change_Doc
      ThisWindow.Update()
          ?DEL:DocumentCharge{PROP:ReadOnly}      = FALSE
          ?DEL:DocumentCharge{PROP:Skip}          = FALSE
          ?DEL:DocumentCharge{PROP:Background}    = -1
    OF ?DEL:VATRate
          DO Charge_Calc
    OF ?L_CC:FuelSurchargePer
          DEL:FuelSurcharge   = DEL:Charge * (L_CC:FuelSurchargePer / 100)
          DISPLAY(?DEL:FuelSurcharge)
          DO Charge_Calc
    OF ?DEL:FuelSurcharge
          ! If we have an amt entered by the user then re-calc the percent that this represents...
      
          L_CC:FuelSurchargePer   = (DEL:FuelSurcharge / DEL:Charge) * 100
      
          !IF L_CC:FuelSurchargePer > 0
          !   DEL:FuelSurcharge    = DEL:Charge * (L_CC:FuelSurchargePer / 100)
          !.
      
          DO Charge_Calc
    OF ?Button_ChangeBranch
      ThisWindow.Update()
          ! Check if this DI has an invoice
          Stop_#  = FALSE
      
          INV:DID = DEL:DID
          IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
             CASE MESSAGE('This DI already has an Invoice.||Do you want to change the Branch on both the DI and the Invoice?', 'DI Branch Change', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:No
                Stop_#    = TRUE
             .
          ELSE
             CLEAR(INV:Record)
          .
      
          IF Stop_# = FALSE
             GlobalRequest    = SelectRecord
             Select_Branches()
      
             IF BRA:BID ~= DEL:BID
                IF DEL:Manifested = TRUE OR INV:IID ~= 0
                   ! Check the Manifest branch is not the same?
                   MAN:MID        = INV:MID
                   IF Access:Manifest.TryFetch(MAN:PKey_MID) = LEVEL:Benign
                      IF MAN:BID ~= BRA:BID
                         CASE MESSAGE('The Manifest that this DI is on has a different Branch to the new selected branch.||Do you still want to change the DI & Invoice branch?||The Manifest Branch will not be changed.', 'DI Branch Change', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
                         OF BUTTON:No
                            Stop_#    = TRUE
                .  .  .  .
      
                IF Stop_# = FALSE
                   IF INV:IID ~= 0
                      ! Change the Invoice branch...
                      INV:BID         = BRA:BID
                      INV:BranchName  = BRA:BranchName
                      IF Access:_Invoice.Update() = LEVEL:Benign
                   .  .
      
                   IF SELF.Request = ViewRecord
                      DEL:BID = BRA:BID
                      IF Access:Deliveries.Update() = LEVEL:Benign
                      .
                   ELSE
                      DEL:BID = BRA:BID
                      MESSAGE('The Branch has been changed but the DI is in edit mode so this change will not be saved until you save the DI.', 'DI Branch Change', ICON:Exclamation)
                   .
      
                   ?String_Branch{PROP:Text}    = BRA:BranchName
                   ?String_Branch:2{PROP:Text}  = BRA:BranchName
      
                   DISPLAY
                .
             ELSE
                MESSAGE('You selected the same branch that is already specified on this DI.','DI Branch Change',ICON:Asterisk)
          .  .
    OF ?Button_UpdateManifested
      ThisWindow.Update()
         ThisWindow.Update
         DEL:Manifested = Upd_Delivery_Man_Status(DEL:DID,,1)
         DEL:Delivered  = Upd_Delivery_Del_Status(DEL:DID)
         ThisWindow.Reset
      
    OF ?Button_Specify
      ThisWindow.Update()
          DO Get_DelComp
      
    OF ?Button_DB_Display
      ThisWindow.Update()
          DISPLAY
    OF ?Button_Value
      ThisWindow.Update()
          L_SG:Value_Type  = POPUP('Delivery Charge|Total Charge excl.|Total Charge incl.', ?L_SG:Value{PROP:XPos}, ?L_SG:Value{PROP:YPos}, 1)
          PUTINI('Delivery', 'Value_Type', L_SG:Value_Type, GLO:Local_INI)
      
      
          DO Value_Type
    OF ?OK
      ThisWindow.Update()
          IF SELF.Request <> ViewRecord
             IF L_CG:ClientStatus = 1
                !MESSAGE('This Client is on Hold!', 'Update Deliveries - New Client', ICON:Exclamation)
             ELSIF L_CG:ClientStatus = 2
                QuickWindow{PROP:AcceptAll}    = FALSE
                MESSAGE('Client account, ' & CLIP(L_LG:ClientName) & ' (no. ' & L_LG:ClientNo & ') has been closed!||Please select a valid client.', 'Update Deliveries', ICON:Hand)
                CLEAR(DEL:CID)
                CLEAR(L_LG:ClientName)
                CLEAR(L_LG:ClientNo)
                SELECT(?L_LG:ClientName)
                CYCLE
          .  .
         IF SELF.Request <> ViewRecord
            IF RECORDS(Queue:Browse) <= 0 AND RECORDS(Queue:Browse:4) <= 0
               CASE MESSAGE('You have not added any items to this delivery.||Would you still like to exit?', 'Update Delivery', ICON:Exclamation, BUTTON:Yes+BUTTON:No,BUTTON:No)
               OF BUTTON:No
                  QuickWindow{PROP:AcceptAll}    = FALSE
                  SELECT(?Sheet_Items)
                  CYCLE
               .
            ELSE
               DO Charge_Calc
      
               IF L_CC:TotalCharge = 0.0 OR DEL:Charge = 0.0
                  CASE MESSAGE('The Total Charge is: ' & CLIP(LEFT(FORMAT(L_CC:TotalCharge, @n-12.2))) & '|The Charge is: ' & CLIP(LEFT(FORMAT(DEL:Charge, @n-12.2))) &'|||Do you want to continue?', 'Update Delivery', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
                  OF BUTTON:No
                     QuickWindow{PROP:AcceptAll} = FALSE
                     SELECT(?DEL:Rate)
                     CYCLE
            .  .  .
      
            IF DEL:Terms = 1
               IF DEL:DC_ID = 0
                  CASE MESSAGE('This is a COD delivery would you like to enter a COD Address?', 'Update Delivery', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                  OF BUTTON:Yes
                     QuickWindow{PROP:AcceptAll} = FALSE
                     SELECT(?Button_COD_Address)
                     CYCLE
            .  .  .
      
            IF DEL:CID = 0
               QuickWindow{PROP:AcceptAll} = FALSE
               SELECT(?L_LG:ClientName)
               CYCLE
            .
      
            IF DEL:DIDate > TODAY() OR DEL:DIDate < TODAY() - 7
               L_FCG:DI_Date_Checked += 1
               IF L_FCG:DI_Date_Checked = 1
                  MESSAGE('Are you sure that the DI Date is correct?||Please check it.||Todays date is: ' & FORMAT(TODAY(),@d6) & '  (' & Week_Day(TODAY()) & ')', 'Update Delivery', ICON:Exclamation)
                  QuickWindow{PROP:AcceptAll} = FALSE
                  SELECT(?DEL:DIDate)
                  CYCLE
               ELSE
                  CASE MESSAGE('Please confirm that the DI Date is correct.||Todays date is: ' & FORMAT(TODAY(),@d6) & '  (' & Week_Day(TODAY()) & ')', 'Update Delivery', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
                  OF BUTTON:No
                     QuickWindow{PROP:AcceptAll} = FALSE
                     SELECT(?DEL:DIDate)
                     CYCLE
            .  .  .
      
            ! Check Multi Part Delivery
            IF DEL:DELCID ~= 0
               DO Check_DelComp
               IF DEL:DELCID = 0
                  QuickWindow{PROP:AcceptAll} = FALSE
                  SELECT(?Tab_Multi)
                  CYCLE
            .  .         
            
           IF DEL:CollectionAID = DEL:DeliveryAID
                MESSAGE('The Collection address cannot be the same as the Delivery address.||Please select a new Collection or Delivery address.','Update Delivery',ICON:Exclamation)
                QuickWindow{PROP:AcceptAll} = FALSE
                SELECT(?L_LG:From_Address)
                CYCLE                        
           .            
                 
           IF Validate_EmailNotficicationAddresses() = TRUE
               QuickWindow{PROP:AcceptAll} = FALSE
               SELECT(?DEL:NoticeEmailAddresses)
               CYCLE                        
        .  .  
          ! Set variables on successful validation
          IF SELF.Request <> ViewRecord
             DEL:UID     = GLO:UID
      
             IF L_SG:VATRate_Previous ~= DEL:VATRate
      			DEL:VATRate_OverriddenUserID  = TRUE
      		.
      		
             ! Check if there was / is a POD Message for the Client - if there is then add it to the 
             ! DEL:SpecialDeliveryInstructions field
                     
      		! (p:Add , p:List , p:Delim, p:Option, p:Prefix, p:Beginning)
          	! (STRING, *STRING, STRING , BYTE=0  , <STRING>, BYTE=0)
      		LOC:Str				= DEL:SpecialDeliveryInstructions
      		IF INSTRING(L_LG:PODMessage, LOC:Str, 1, 1) <= 0
      		   Add_to_List(L_LG:PODMessage, LOC:Str, ', ')				
      		   DEL:SpecialDeliveryInstructions		= LOC:Str
      		.
      				
      		!MESSAGE(DEL:SpecialDeliveryInstructions)
      	    ! Save Email Addresses into Email Addresses as ops
             Save_Email_Addresses(DEL:NoticeEmailAddresses, DEL:CID)
          .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    OF ?BUTTON_UpdatePods
      ThisWindow.Update()
      Delivery_PODs(DEL:DID)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeCompleted()
      IF ReturnValue = LEVEL:Benign
         ! We have completed processing and validation.. ??
         ! If we are inserting, check if we should invoice this now
         IF L_CG:GenerateInvoice = TRUE
            IF Check_Delivery_Invoiced(DEL:DID) = 0
               Gen_Invoice_s(DEL:DID)
            .
  
            ! Check to see if the Invoice has been printed before, if not then ask user if theyd like to now
            IF Check_Delivery_Invoiced(DEL:DID, 1) = 0    ! Not printed
               CASE MESSAGE('Invoice has been generated.||Print it now?', 'Update Delivery - Invoice Print', ICON:Question, |
                              BUTTON:Yes+BUTTON:No, BUTTON:Yes)
               OF BUTTON:Yes
                  Print_Cont(DEL:DID, 0, 1)                  ! Use a DID to print the Invoice    
         .  .  . 
      
      .
  
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
          IF CHOICE(?CurrentTab) = 2
        !  db.debugout('[Update_Deliveries]  CurrentTab - 2 - start')
             ! (p:DID, p:Option)
             ! p:Option          0. = Number
             !                   1. = Total Cost
             !                   2. = Total Cost inc.
             !                   3. = TIDs
      
             IF Get_DelLegs_Info(DEL:DID, 0) = 0
                HIDE(?Prompt_LegCosts)
             ELSE
                ?Prompt_LegCosts{PROP:Text}   = 'Total costs for Legs: R ' & CLIP(LEFT(FORMAT(Get_DelLegs_Info(DEL:DID, 2), @n12.2)))
                UNHIDE(?Prompt_LegCosts)
             .
        !  db.debugout('[Update_Deliveries]  CurrentTab - 2 - end')
          .  !.
    OF ?DEL:DIDate
          DO Date_Changed
    OF ?Browse:ItemsContainers
          IF RECORDS(Queue:Browse) <= 0 AND RECORDS(Queue:Browse:4) <= 0
             POST(EVENT:Accepted, ?Insert:5)
          .
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all Selected events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?Browse:ItemsLoose
          IF RECORDS(Queue:Browse) <= 0 AND RECORDS(Queue:Browse:4) <= 0
             POST(EVENT:Accepted, ?Insert)
          .
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseDown
        GLO:ClosingDown   = TRUE
    OF EVENT:OpenWindow
          LOAD2:LTID                  = DEL:LTID
          IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
             ENABLE(?Sheet_Items)
      
             LOC:Load_Type_Group    :=: LOAD2:Record

             ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
             IF L_LT:LoadOption = 1 OR L_LT:LoadOption = 2 OR L_LT:LoadOption = 4
                SELECT(?Tab_Containers)
             ELSE
                SELECT(?Tab_Loose)
             .
      
             DO Floor_Changed
          ELSE
             IF SELF.Request ~= InsertRecord
                DISABLE(?Sheet_Items)
          .  .
          ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
          IF L_LT:LoadOption = 0 OR L_LT:LoadOption = 1 OR L_LT:LoadOption = 2 OR L_LT:LoadOption = 4 OR L_LT:ContainerParkStandard = TRUE
          ELSE
             SELECT(?Tab_Loose)
      
             IF SELF.Request = InsertRecord
                SELECT(FIRSTFIELD())
             ELSE
                SELECT(?Sheet_Items)
          .  .
      
      
             ?Sheet_Items{PROP:Wizard}    = TRUE
             ?Sheet_Items{PROP:NoSheet}   = TRUE
          ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
          IF L_LT:LoadOption = 1 OR L_LT:LoadOption = 2 OR L_LT:LoadOption = 4 OR L_LT:ContainerParkStandard = TRUE
             SELECT(?Tab_Containers)
          ELSE
             SELECT(?Tab_Loose)
          .
          ?String_Rate_Info{PROP:Text}    = ''
          IF DEL:DC_ID ~= 0
             DCADD:DC_ID = DEL:DC_ID
             IF Access:Delivery_CODAddresses.TryFetch(DCADD:PKey_DC_ID) = LEVEL:Benign
                ?Button_COD_Address{PROP:Tip} = 'COD Address specified is:<13,10>' & CLIP(DCADD:AddressName) & '<13,10>' & CLIP(DCADD:Line1) & |
                                                 CLIP(DCADD:Line2) & '<13,10>' & CLIP(DCADD:Line3) & '<13,10>' & CLIP(DCADD:Line4) & |
                                                 '<13,10>' & CLIP(DCADD:Line5) & '<13,10>VAT No.: ' & CLIP(DCADD:VATNo)
          .  .
          BRA:BID     = DEL:BID
          IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
             ?String_Branch{PROP:Text}    = BRA:BranchName
             ?String_Branch:2{PROP:Text}  = BRA:BranchName
          .
    OF EVENT:Timer
      CASE FOCUS() 
      OF ?DEL:SpecialDeliveryInstructions
         POST(EVENT:Accepted,?DEL:SpecialDeliveryInstructions)
      .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF (RECORDS(Queue:Browse:4) OR RECORDS(Queue:Browse)) AND SELF.Request ~= InsertRecord
             ! We don't want Load Type changed when we already have Items with the current specified Load Type
             DISABLE(?Group_LoadType)
          .
          ! Check if this part of a multi part delivery or not
          IF DEL:DELCID = 0
             ?Button_Specify{PROP:Text}       = 'Specify Multi-Part'
             HIDE(?Prompt_Multi)
          ELSE
             ?Button_Specify{PROP:Text}       = 'Change Multi-Part'
      
             UNHIDE(?Prompt_Multi)
             LOC:DeliveryComposition_Ret_Group = Get_Delivery_Info(DEL:DELCID)
      
             ?Tab_Multi{PROP:Font,4}          = FONT:Bold
          .
      
      
          ! DEL:DINo
          IF SELF.Request = InsertRecord
             SELECT(?DEL:DINo, ?DEL:DINo{PROP:SelStart}, ?DEL:DINo{PROP:SelEnd})
          .
          IF SELF.Request = InsertRecord                          ! p:Option is used here
             IF p:Option = 2
                ! 1st thing we want to do is request the user to choose / specify the Delivery Composition
                MESSAGE('Next, please specify the Multi-Part Delivery Composition that this DI is to be part of.', 'Insert Delivery', ICON:Asterisk)
      
                L_DC2:Start_of_Insert  = TRUE
      
                SELECT(?Tab_Multi)
                POST(EVENT:Accepted, ?Button_Specify)
             .
          ELSE
             ?DEL:DIDate{PROP:Tip}    = 'Created on ' & FORMAT(DEL:CreatedDate,@d5b) & ' @ ' & FORMAT(DEL:CreatedTime,@t4b)
          .
    ELSE
      CASE EVENT()
      OF EVENT:User
         BRW_DelItemsAlias.ResetFromFile()
  
         IF L_CC:TotalUnits > 0          ! If we have items, then go to the charges Tab...
            IF DI_Items_Action() = 2
               SELECT(?Tab_Charges)
            ELSE
      .  .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Validate_EmailNotficicationAddresses   PROCEDURE() !, LONG
L_Res       STRING(255)

L_Res_Cur   STRING(10)
L_Add       STRING(255)

L_Addresses  LIKE(DEL:NoticeEmailAddresses)
L_AddressesToCheck                        LIKE(DEL:NoticeEmailAddresses)

L_Ret       LONG

CODE
   !(STRING p_EmailAddresses),STRING
   L_Res    = Validate_Email_Addresses(DEL:NoticeEmailAddresses)
   IF INSTRING('0', L_Res, 1, 1) > 0
      
      db.Debugout('[DI - Validate_EmailNotficicationAddresses] =========== L_Res: ' & CLIP(L_Res))  
      
      ! We have at least one bad address
      L_Addresses    = DEL:NoticeEmailAddresses
      LOOP
         IF CLIP(L_Res) = ''
            BREAK
         .

         L_Res_Cur   = LEFT(Get_1st_Element_From_Delim_Str(L_res, ',', 1))
         L_Add       = LEFT(Get_1st_Element_From_Delim_Str(L_Addresses, ',', 1))

         IF L_Res_Cur = 0                                            ! No match, bad address
            Add_to_List('"' & CLIP(L_Add) & '"', L_AddressesToCheck, ',')
      .  .
   
      MESSAGE('Please check the following Email addresses, they appear to be incorrect.||' & CLIP(L_AddressesToCheck), 'Notification Email Addresses', ICON:Asterisk)
      L_Ret    = TRUE
   .
      
   RETURN(L_Ret)
      

BRW_DeliveryItems.AskRecord PROCEDURE(BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
      db.debugout('[Update_Deliveries]  BRW4 Ask Record')
  ReturnValue = PARENT.AskRecord(Request)
  RETURN ReturnValue


BRW_DeliveryItems.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:5
    SELF.ChangeControl=?Change:5
    SELF.DeleteControl=?Delete:5
  END
  SELF.ViewControl = ?View                                 ! Setup the control used to initiate view only mode


BRW_DeliveryItems.SetQueueRecord PROCEDURE

  CODE
      ! (p:DID, p:No, p:MAN_DIID_List, p:List_Option, p:DIID)
      ! (ULONG, SHORT, <*STRING>, BYTE=0, ULONG=0),ULONG, PROC
      ! Look through all the Items for the Delivery and enumerate the MAN:MIDs into a Q
      ! p:No
      !   0. Return number of different MIDs (for this DID)
      !   x. Return this number MID - return the nth MID (for this DID)
      !
      ! p:List_Option
      !   0. Return list with MID, DIID, MID, DIID
      !   x. Return list with MID, MID
      !
      ! If not omitted - p:MAN_DIID_List
      !       Load it with MID,DIID,MID,DIID - which is the MIDs and the 1st DIID on each
      !
      ! A delivery item can be loaded on different trailers - i.e. different ManLoads
      ! Item can appear on more than one ManifestLoad but a single ManifestLoad can only have 1 instance of the Item
      !
      ! If p:DIID provided then only get for this DIID
  
  
      db.debugout('[Update_Deliveries]  Del Items - start')
  
      CLEAR(L_IG:MID)
      Get_Delivery_ManIDs(DEL:DID, 0, L_IG:MID, , DELI:DIID)
  
      db.debugout('[Update_Deliveries]  Del Items - end')
  
  
  
  
  
  
  
  
  !    OPEN(ManLD)
  !    ManLD{PROP:Filter}  = 'MALD:DIID = DELI:DIID'
  !    ManLD{PROP:Order}   = 'MAL:MID,MAL:MLID'
  !    SET(ManLD)
  !
  !    LOOP
  !       NEXT(ManLD)
  !       IF ERRORCODE()
  !          BREAK
  !       .
  !
  !       IF L_IG:Last_MID = MAL:MID
  !          CYCLE
  !       .
  !       L_IG:Last_MID = MAL:MID
  !
  !       IF CLIP(L_IG:MID) = ''
  !          L_IG:MID  = MAL:MID
  !       ELSE
  !          L_IG:MID  = CLIP(L_IG:MID) & ', ' & MAL:MID
  !    .  .
  !
  !    CLOSE(ManLD)
  
  
  
  PARENT.SetQueueRecord
  


BRW_DeliveryItems.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW4::LastSortOrder <> NewOrder THEN
     BRW4::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW4::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_DeliveryItems.TakeNewSelection PROCEDURE

  CODE
  IF BRW4::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW4::PopupTextExt = ''
        BRW4::PopupChoiceExec = True
        BRW4::FormatManager.MakePopup(BRW4::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW4::PopupTextExt = '|-|' & CLIP(BRW4::PopupTextExt)
        END
        BRW4::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW4::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW4::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW4::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW4::PopupChoiceOn AND BRW4::PopupChoiceExec THEN
     BRW4::PopupChoiceExec = False
     BRW4::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW4::PopupTextExt)
     IF BRW4::FormatManager.DispatchChoice(BRW4::PopupChoice)
     ELSE
     END
  END


BRW_DelItemsAlias.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END
  SELF.ViewControl = ?View:2                               ! Setup the control used to initiate view only mode


BRW_DelItemsAlias.ResetFromView PROCEDURE

L_LG:No_Items:Cnt    LONG                                  ! Count variable for browse totals
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:DeliveryItemAlias2.SetQuickScan(1)
  SELF.Reset
  IF SELF.UseMRP
     IF SELF.View{PROP:IPRequestCount} = 0
          SELF.View{PROP:IPRequestCount} = 60
     END
  END
  LOOP
    IF SELF.UseMRP
       IF SELF.View{PROP:IPRequestCount} = 0
            SELF.View{PROP:IPRequestCount} = 60
       END
    END
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      SETCURSOR()
      RETURN
    END
    SELF.SetQueueRecord
    L_LG:No_Items:Cnt += 1
  END
  SELF.View{PROP:IPRequestCount} = 0
  L_LG:No_Items = L_LG:No_Items:Cnt
  PARENT.ResetFromView
  Relate:DeliveryItemAlias2.SetQuickScan(0)
  SETCURSOR()


BRW_DelItemsAlias.SetQueueRecord PROCEDURE

  CODE
      ! (p:DID, p:No, p:MAN_DIID_List, p:List_Option, p:DIID)
      db.debugout('[Update_Deliveries]  Del Items Alias - start')
  
      Get_Delivery_ManIDs(DEL:DID, 0, L_IG:MID, , A_DELI2:DIID)
  
      db.debugout('[Update_Deliveries]  Del Items Alias - end')
  
  !    OPEN(ManLD)
  !    ManLD{PROP:Filter}  = 'MALD:DIID = A_DELI:DIID'
  !    ManLD{PROP:Order}   = 'MAL:MID,MAL:MLID'
  !    SET(ManLD)
  !
  !    LOOP
  !       NEXT(ManLD)
  !       IF ERRORCODE()
  !          BREAK
  !       .
  !
  !       IF L_IG:Last_MID = MAL:MID
  !          CYCLE
  !       .
  !       L_IG:Last_MID = MAL:MID
  !
  !       IF CLIP(L_IG:MID) = ''
  !          L_IG:MID  = MAL:MID
  !       ELSE
  !          L_IG:MID  = CLIP(L_IG:MID) & ', ' & MAL:MID
  !    .  .
  !
  !    CLOSE(ManLD)
  PARENT.SetQueueRecord
  


BRW_DelItemsAlias.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW20::LastSortOrder <> NewOrder THEN
     BRW20::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW20::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_DelItemsAlias.TakeNewSelection PROCEDURE

  CODE
  IF BRW20::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW20::PopupTextExt = ''
        BRW20::PopupChoiceExec = True
        BRW20::FormatManager.MakePopup(BRW20::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW20::PopupTextExt = '|-|' & CLIP(BRW20::PopupTextExt)
        END
        BRW20::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW20::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW20::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW20::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW20::PopupChoiceOn AND BRW20::PopupChoiceExec THEN
     BRW20::PopupChoiceExec = False
     BRW20::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW20::PopupTextExt)
     IF BRW20::FormatManager.DispatchChoice(BRW20::PopupChoice)
     ELSE
     END
  END


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:7
    SELF.ChangeControl=?Change:7
    SELF.DeleteControl=?Delete:7
  END


BRW6.SetQueueRecord PROCEDURE

  CODE
      L_DLG:VAT   = DELL:Cost * (DELL:VATRate / 100)
      L_DLG:Total = DELL:Cost + L_DLG:VAT
  PARENT.SetQueueRecord
  


BRW6.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW6::LastSortOrder <> NewOrder THEN
     BRW6::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW6::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW6.TakeNewSelection PROCEDURE

  CODE
  IF BRW6::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW6::PopupTextExt = ''
        BRW6::PopupChoiceExec = True
        BRW6::FormatManager.MakePopup(BRW6::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW6::PopupTextExt = '|-|' & CLIP(BRW6::PopupTextExt)
        END
        BRW6::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW6::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW6::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW6::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW6::PopupChoiceOn AND BRW6::PopupChoiceExec THEN
     BRW6::PopupChoiceExec = False
     BRW6::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW6::PopupTextExt)
     IF BRW6::FormatManager.DispatchChoice(BRW6::PopupChoice)
     ELSE
     END
  END


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:9
    SELF.ChangeControl=?Change:9
    SELF.DeleteControl=?Delete:9
  END


BRW8.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW8::LastSortOrder <> NewOrder THEN
     BRW8::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW8::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  IF BRW8::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW8::PopupTextExt = ''
        BRW8::PopupChoiceExec = True
        BRW8::FormatManager.MakePopup(BRW8::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW8::PopupTextExt = '|-|' & CLIP(BRW8::PopupTextExt)
        END
        BRW8::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW8::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW8::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW8::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW8::PopupChoiceOn AND BRW8::PopupChoiceExec THEN
     BRW8::PopupChoiceExec = False
     BRW8::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW8::PopupTextExt)
     IF BRW8::FormatManager.DispatchChoice(BRW8::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.RemoveControl(?Group_LoadType)                      ! Remove ?Group_LoadType from the resizer, it will not be moved or sized


FDB27.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      ! Check that there are Rates for this Floor - only show Floors with Rates
      ! (p:FID, p:JID, p:Eff_Date, p:Mass, p:Rate)
  
      ! Only do this if we are using Container Park Rates
      ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
      IF L_LT:LoadOption = 1
         IF Get_Floor_Rate(FLO:FID, DEL:JID, DEL:DIDate) <= 0
            ReturnValue    = Record:Filtered
            RETURN(ReturnValue)
      .  .
  
  
  
  ReturnValue = PARENT.ValidateRecord()
  RETURN ReturnValue

