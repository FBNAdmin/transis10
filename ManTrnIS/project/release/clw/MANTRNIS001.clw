

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQUERY.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_WebClients PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:ClientName       STRING(35)                            ! 
LOC:ClientNo         ULONG                                 ! Client No.
BRW2::View:Browse    VIEW(WebClientsLoginLog)
                       PROJECT(WLOG:LoginLogID)
                       PROJECT(WLOG:ClientLogin)
                       PROJECT(WLOG:FailedPassword)
                       PROJECT(WLOG:LoginIP)
                       PROJECT(WLOG:LoginDateTime_DATE)
                       PROJECT(WLOG:LoginDateTime_TIME)
                       PROJECT(WLOG:OffLine)
                       PROJECT(WLOG:WebClientID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
WLOG:LoginLogID        LIKE(WLOG:LoginLogID)          !List box control field - type derived from field
WLOG:ClientLogin       LIKE(WLOG:ClientLogin)         !List box control field - type derived from field
WLOG:FailedPassword    LIKE(WLOG:FailedPassword)      !List box control field - type derived from field
WLOG:LoginIP           LIKE(WLOG:LoginIP)             !List box control field - type derived from field
WLOG:LoginDateTime_DATE LIKE(WLOG:LoginDateTime_DATE) !List box control field - type derived from field
WLOG:LoginDateTime_TIME LIKE(WLOG:LoginDateTime_TIME) !List box control field - type derived from field
WLOG:OffLine           LIKE(WLOG:OffLine)             !List box control field - type derived from field
WLOG:WebClientID       LIKE(WLOG:WebClientID)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::WCLI:Record LIKE(WCLI:RECORD),THREAD
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Web Clients'),AT(,,288,158),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdateWebClients'),SYSTEM
                       SHEET,AT(4,4,280,134),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Client Name:'),AT(9,22),USE(?LOC:ClientName:Prompt),TRN
                           BUTTON('...'),AT(53,22,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(69,22,147,10),USE(LOC:ClientName),TIP('Client name')
                           PROMPT('Web Client ID:'),AT(185,4),USE(?WCLI:WebClientID:Prompt),TRN
                           ENTRY(@n_10),AT(235,4,49,10),USE(WCLI:WebClientID),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP
                           PROMPT('CID:'),AT(221,22),USE(?WCLI:CID:Prompt),TRN
                           STRING(@n_10),AT(233,22,49,10),USE(WCLI:CID),RIGHT(1),TRN
                           PROMPT('Client No.:'),AT(9,36),USE(?LOC:ClientNo:Prompt),TRN
                           ENTRY(@n_10b),AT(69,36,84,10),USE(LOC:ClientNo),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Client No.'), |
  READONLY,SKIP,TIP('Client No.')
                           BUTTON('Set Admin Login'),AT(218,36,63,12),USE(?Button_SetAdminLogin)
                           PROMPT('Client Login:'),AT(9,64),USE(?WCLI:ClientLogin:Prompt),TRN
                           ENTRY(@s20),AT(69,64,84,10),USE(WCLI:ClientLogin)
                           PROMPT('Client Password:'),AT(9,78),USE(?WCLI:ClientPassword:Prompt),TRN
                           ENTRY(@s20),AT(69,78,84,10),USE(WCLI:ClientPassword)
                           PROMPT('Login Name:'),AT(9,96),USE(?WCLI:LoginName:Prompt),TRN
                           ENTRY(@s50),AT(69,96,204,10),USE(WCLI:LoginName)
                           PROMPT('Access Level:'),AT(9,119),USE(?WCLI:AccessLevel:Prompt),TRN
                           SPIN(@n3),AT(69,119,60,10),USE(WCLI:AccessLevel),RIGHT(1),TIP('1 admin access, 10 client access'),MSG('1 admin access, 10 client access')
                         END
                         TAB('&2) Web Clients Login Log'),USE(?Tab:2)
                           LIST,AT(9,20,272,97),USE(?Browse:2),HVSCROLL,FORMAT('48R(2)|M~Login Log ID~C(0)@n_10@60' & |
  'L(2)|M~Client Login~@s20@56L(2)|M~Failed Password~@s20@64L(2)|M~Login IP~@s15@40L(2)' & |
  '|M~Login Date~C(0)@d5b@38L(2)|M~Login Time~C(0)@t7@30R(2)|M~Off Line~C(0)@n3@40R(2)|' & |
  'M~Web Client ID~C(0)@n_10@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the WebClientsLo' & |
  'ginLog file')
                           BUTTON('&View'),AT(227,120,,14),USE(?View),LEFT,ICON('waview.ico'),FLAT
                           BUTTON('&Insert'),AT(9,120,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,HIDE,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(61,120,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,HIDE,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(113,120,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,HIDE,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(180,140,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(234,140,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,140,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW2::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_WebClients')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:ClientName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(WCLI:Record,History::WCLI:Record)
  SELF.AddHistoryField(?WCLI:WebClientID,1)
  SELF.AddHistoryField(?WCLI:CID,2)
  SELF.AddHistoryField(?WCLI:ClientLogin,3)
  SELF.AddHistoryField(?WCLI:ClientPassword,4)
  SELF.AddHistoryField(?WCLI:LoginName,5)
  SELF.AddHistoryField(?WCLI:AccessLevel,6)
  SELF.AddUpdateFile(Access:WebClients)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Clients.Open                             ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:WebClients.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:WebClients
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:WebClientsLoginLog,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?LOC:ClientName{PROP:ReadOnly} = True
    ?WCLI:WebClientID{PROP:ReadOnly} = True
    ?LOC:ClientNo{PROP:ReadOnly} = True
    DISABLE(?Button_SetAdminLogin)
    ?WCLI:ClientLogin{PROP:ReadOnly} = True
    ?WCLI:ClientPassword{PROP:ReadOnly} = True
    ?WCLI:LoginName{PROP:ReadOnly} = True
    DISABLE(?View)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,WLOG:FKey_WebClientID)       ! Add the sort order for WLOG:FKey_WebClientID for sort order 1
  BRW2.AddRange(WLOG:WebClientID,Relate:WebClientsLoginLog,Relate:WebClients) ! Add file relationship range limit for sort order 1
  BRW2.AddField(WLOG:LoginLogID,BRW2.Q.WLOG:LoginLogID) ! Field WLOG:LoginLogID is a hot field or requires assignment from browse
  BRW2.AddField(WLOG:ClientLogin,BRW2.Q.WLOG:ClientLogin) ! Field WLOG:ClientLogin is a hot field or requires assignment from browse
  BRW2.AddField(WLOG:FailedPassword,BRW2.Q.WLOG:FailedPassword) ! Field WLOG:FailedPassword is a hot field or requires assignment from browse
  BRW2.AddField(WLOG:LoginIP,BRW2.Q.WLOG:LoginIP) ! Field WLOG:LoginIP is a hot field or requires assignment from browse
  BRW2.AddField(WLOG:LoginDateTime_DATE,BRW2.Q.WLOG:LoginDateTime_DATE) ! Field WLOG:LoginDateTime_DATE is a hot field or requires assignment from browse
  BRW2.AddField(WLOG:LoginDateTime_TIME,BRW2.Q.WLOG:LoginDateTime_TIME) ! Field WLOG:LoginDateTime_TIME is a hot field or requires assignment from browse
  BRW2.AddField(WLOG:OffLine,BRW2.Q.WLOG:OffLine) ! Field WLOG:OffLine is a hot field or requires assignment from browse
  BRW2.AddField(WLOG:WebClientID,BRW2.Q.WLOG:WebClientID) ! Field WLOG:WebClientID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_WebClients',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 2                           ! Will call: Update_WebClientsLoginLog
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_WebClients',1,?Browse:2,2,BRW2::PopupTextExt,Queue:Browse:2,8,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_WebClients',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Clients
      Update_WebClientsLoginLog
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = LOC:ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:ClientName = CLI:ClientName
        LOC:ClientNo = CLI:ClientNo
        WCLI:CID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?LOC:ClientName
      IF LOC:ClientName OR ?LOC:ClientName{PROP:Req}
        CLI:ClientName = LOC:ClientName
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:ClientName = CLI:ClientName
            LOC:ClientNo = CLI:ClientNo
            WCLI:CID = CLI:CID
          ELSE
            CLEAR(LOC:ClientNo)
            CLEAR(WCLI:CID)
            SELECT(?LOC:ClientName)
            CYCLE
          END
        ELSE
          LOC:ClientNo = CLI:ClientNo
          WCLI:CID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?Button_SetAdminLogin
      ThisWindow.Update()
          WCLI:CID    = 0
          CLEAR(LOC:ClientName)
          CLEAR(LOC:ClientNo)
      
          DISPLAY
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          CLI:CID     = WCLI:CID
          IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
             LOC:ClientNo     = CLI:ClientNo
             LOC:ClientName   = CLI:ClientName
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END
  SELF.ViewControl = ?View                                 ! Setup the control used to initiate view only mode


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

    
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Window_Client_Activity PROCEDURE 

LOC:Client_Q         QUEUE,PRE(L_CQ)                       ! 
ClientName           STRING(100)                           ! 
ClientNo             ULONG                                 ! Client No.
DIs                  ULONG                                 ! Number of DIs
Weight               DECIMAL(15,2)                         ! 
Value                DECIMAL(15,2)                         ! 
Ratio                DECIMAL(7,2)                          ! Value to Weight
PointNo              LONG                                  ! 
CID                  ULONG                                 ! Client ID
                     END                                   ! 
LOC:Options          GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
Sort_By              BYTE                                  ! Sort Top Entries By
Top_Entries          USHORT(10)                            ! 
                     END                                   ! 
LOC:SQL_OG           GROUP,PRE(L_OG)                       ! 
SQL                  STRING(255)                           ! 
SQL_Results          STRING(150)                           ! 
Progress             LONG                                  ! 
                     END                                   ! 
ClickedOnPointName   STRING(255)                           ! 
ClickedOnPointNumber REAL                                  ! 
ClickedOnSetNumber   LONG                                  ! 
LOC:Client_Q_Graph   QUEUE,PRE(L_CQG)                      ! 
ClientName           STRING(100)                           ! 
ClientNo             ULONG                                 ! Client No.
DIs                  ULONG                                 ! Number of DIs
Weight               DECIMAL(15,2)                         ! 
Value                DECIMAL(15,2)                         ! 
Ratio                DECIMAL(7,2)                          ! Value to Weight
PointNo              LONG                                  ! 
CID                  ULONG                                 ! Client ID
                     END                                   ! 
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q1
ThisGraph1    Class(GlobalInsight)
ValidateRecord  PROCEDURE(long graphID),Long ,VIRTUAL
SetPointName    PROCEDURE (Long graphSet),String ,VIRTUAL
Reset           PROCEDURE (Byte graphForce=0),VIRTUAL
              End
ThisGraph1:Popup  Class(PopupClass)
               End
ThisGraph1:ClickedOnPointName   String(255)
ThisGraph1:ClickedOnPointNumber Real
ThisGraph1:ClickedOnSetNumber   Long
ThisGraph1:Color                Group(iColorGroupType), PRE(ThisGraph1:Color)
                                End
ThisGraph1:FileName              String(255)
!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
QuickWindow          WINDOW('Client Activity Graph'),AT(,,405,259),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('Window_Client_Activity'),SYSTEM
                       SHEET,AT(3,2,399,236),USE(?Sheet1)
                         TAB('Options'),USE(?Tab1)
                           GROUP,AT(27,30,199,132),USE(?Group_Top),TRN
                             PROMPT('From Date:'),AT(42,44),USE(?LO:From_Date:Prompt),TRN
                             SPIN(@d5b),AT(91,44,60,10),USE(LO:From_Date),RIGHT(1)
                             BUTTON('...'),AT(159,44,12,10),USE(?Calendar)
                             PROMPT('To Date:'),AT(42,62),USE(?LO:To_Date:Prompt),TRN
                             SPIN(@d5b),AT(91,62,60,10),USE(LO:To_Date),RIGHT(1)
                             BUTTON('...'),AT(159,62,12,10),USE(?Calendar:2)
                             PROMPT('Top Entries:'),AT(42,90),USE(?Top_Entries:Prompt),TRN
                             SPIN(@n6),AT(91,90,60,10),USE(LO:Top_Entries),RIGHT(1)
                             BUTTON('L&oad Top Entries'),AT(159,90,,14),USE(?Button_Load),TIP('Re-Load the Graph wit' & |
  'h Top Entries (doesn''t renew data)')
                             PROMPT('Sort By:'),AT(42,124),USE(?Sort_By:Prompt),TRN
                             LIST,AT(91,124,60,10),USE(LO:Sort_By),DROP(5),FROM('Value|#0|Ratio (R''s per Ton)|#1|We' & |
  'ight|#2|No. DIs|#3'),MSG('Sort Top Entries By'),TIP('Sort Top Entries By')
                           END
                           BUTTON('&Load Graph Data'),AT(307,218,,14),USE(?Ok:2),LEFT,ICON('WAOK.ICO'),FLAT,TIP('Get data f' & |
  'rom database')
                           PROGRESS,AT(138,220,151,8),USE(?Progress1),HIDE,RANGE(0,100)
                         END
                         TAB('Graph'),USE(?Tab_Graph)
                           REGION,AT(6,20,391,214),USE(?Insight),BEVEL(1,-1),IMM
                         END
                         TAB('Graphed Data'),USE(?Tab3)
                           LIST,AT(6,18,391,216),USE(?List3),HVSCROLL,FORMAT('80L(2)|M~Client Name~@s100@40R(2)|M~' & |
  'Client No.~L@n_10b@52R(2)|M~DI s~L@n13@60R(2)|M~Weight~L@n-21.2@60R(2)|M~Value~L@n-2' & |
  '1.2@40R(2)|M~Ratio~L@n-10.2@56R(2)|M~Point No.~L@n-14@'),FROM(LOC:Client_Q_Graph)
                         END
                         TAB('All Data'),USE(?Tab4)
                           LIST,AT(6,18,391,216),USE(?List1),HVSCROLL,FORMAT('80L(2)|M~Client Name~@s100@40R(2)|M~' & |
  'Client No.~L@n_10b@52R(2)|M~DI s~L@n13@50R(2)|M~Weight~L@n-21.2@50R(2)|M~Value~L@n-2' & |
  '1.2@40R(2)|M~Ratio~L@n-10.2@56R(2)|M~Point No.~L@n-14@'),FROM(LOC:Client_Q)
                         END
                       END
                       BUTTON('&Cancel'),AT(354,242,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(46,242,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q2

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar7            CalendarClass
Calendar8            CalendarClass
View_Clients        VIEW(Clients)
    .

Cli_View            ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Process_Clients                     ROUTINE
    SETCURSOR(CURSOR:WAIT)
    ?Progress1{PROP:RangeHigh}  = RECORDS(Clients)

    FREE(LOC:Client_Q)
    L_OG:Progress   = 0

    Cli_View.Init(View_Clients, Relate:Clients)
    Cli_View.AddSortOrder(CLI:PKey_CID)

    !Cli_View.AppendOrder()
    !Cli_View.AddRange()
    !Cli_View.SetFilter()

    Cli_View.Reset()
    LOOP
       IF Cli_View.Next() ~= LEVEL:Benign
          BREAK
       .
       L_OG:Progress           += 1
       ?Progress1{PROP:Progress}  = L_OG:Progress

       L_OG:SQL                 = 'SELECT COUNT(*), SUM(DocumentCharge) + SUM(FuelSurcharge) + SUM(Charge) + SUM(AdditionalCharge) FROM Deliveries WHERE CID = ' & CLI:CID & ' AND DIDateAndTime >= <39>' & FORMAT(LO:From_Date,@d8) & '<39> AND DIDateAndTime < <39>' & FORMAT(LO:To_Date+1,@d8) & '<39>'

       Get_Info_SQL(4, CLI:CID, L_OG:SQL_Results, L_OG:SQL, 2)

       L_CQ:DIs                 = Get_1st_Element_From_Delim_Str(L_OG:SQL_Results, ',', TRUE)
       L_CQ:Value               = Get_1st_Element_From_Delim_Str(L_OG:SQL_Results, ',', TRUE)

       L_CQ:Weight              = Get_DelItem_Client_Info(CLI:CID, 2, LO:From_Date, LO:To_Date) / 1000     ! Tons

       L_CQ:Ratio               = L_CQ:Value / L_CQ:Weight

       L_CQ:ClientName          = CLI:ClientName
       L_CQ:ClientNo            = CLI:ClientNo

       L_CQ:CID                 = CLI:CID

       IF L_CQ:Value ~= 0.0
          ADD(LOC:Client_Q)
    .  .
    Cli_View.Kill()


    DO Process_Graph_Q

    SETCURSOR()
    EXIT
Process_Graph_Q                 ROUTINE
    FREE(LOC:Client_Q_Graph)

    !Value|Ratio (R's per Ton)|Weight|No. DIs

    ! Sort the Q from highest to lowest
    EXECUTE LO:Sort_By + 1
       SORT(LOC:Client_Q, -L_CQ:Value, -L_CQ:Weight, -L_CQ:DIs, L_CQ:ClientName)
       SORT(LOC:Client_Q, -L_CQ:Ratio, -L_CQ:Weight, -L_CQ:DIs, L_CQ:ClientName)
       SORT(LOC:Client_Q, -L_CQ:Weight, -L_CQ:Value, -L_CQ:DIs, L_CQ:ClientName)
       SORT(LOC:Client_Q, -L_CQ:DIs, -L_CQ:Value, -L_CQ:DIs, L_CQ:ClientName)
    .


    LOOP I_# = 1 TO RECORDS(LOC:Client_Q)
       IF RECORDS(LOC:Client_Q_Graph) >= LO:Top_Entries
          BREAK
       .

       GET(LOC:Client_Q, I_#)
       IF ERRORCODE()
          BREAK
       .

       LOC:Client_Q_Graph   :=: LOC:Client_Q
       ADD(LOC:Client_Q_Graph)
    .

    ! Number the Q entries
    LOOP I_# = 1 TO RECORDS(LOC:Client_Q_Graph)
       GET(LOC:Client_Q_Graph, I_#)
       IF ERRORCODE()
          BREAK
       .
       L_CQG:PointNo = I_#
       PUT(LOC:Client_Q_Graph)
    .

    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Window_Client_Activity')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LO:From_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Window_Client_Activity',QuickWindow)       ! Restore window settings from non-volatile store
      LO:From_Date    = GETINI('Window_Client_Activity', 'From_Date', TODAY() - 100, GLO:Local_INI)
      LO:To_Date      = GETINI('Window_Client_Activity', 'To_Date', TODAY(), GLO:Local_INI)
      ThisGraph1.xFontAngle  = 90
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
    ! Start Of Insight InitStuff Group
  Bind('ThisGraph1:ClickedOnPointNumber',ThisGraph1:ClickedOnPointNumber) ! Insight ClickedOnVariable
  Bind('ThisGraph1:ClickedOnPointName',ThisGraph1:ClickedOnPointName)       ! Insight ClickedOnVariable
  Bind('ThisGraph1:ClickedOnSetNumber',ThisGraph1:ClickedOnSetNumber)     ! Insight ClickedOnVariable
      ! ClickedOnPointName - STRING(255) -
      ! ClickedOnPointNumber - REAL -
      ! ClickedOnSetNumber - LONG -
  if ThisGraph1.Init(?Insight,Insight:Line).
  if not ThisGraph1:Color:Fetched then ThisGraph1.GetWindowsColors(ThisGraph1:Color).
  QuickWindow{prop:buffer} = 1
  ThisGraph1.LegendPosition = 8
  ThisGraph1.LegendText = 0
  ThisGraph1.LegendValue = 0
  ThisGraph1.LegendPercent = 0
  ThisGraph1.Stacked=0
  ThisGraph1.BlackAndWhite=0
  ThisGraph1.BackgroundPicture=''
  ThisGraph1.BackgroundColor=Color:Silver
  ThisGraph1.BackgroundShadeColor=Color:None
  ThisGraph1.BorderColor=Color:Black
  ThisGraph1.MinPointWidth=0
  ThisGraph1.PaperZoom=4
  ThisGraph1.ActiveInvisible=0
  ThisGraph1.PrintPortrait=0
  ThisGraph1.RightBorder=10
  ThisGraph1.MaxXGridTicks=16
  ThisGraph1.MaxYGridTicks=16
  ThisGraph1.AutoShade=1
  ThisGraph1.TopShade=0
  ThisGraph1.RightShade=0
  ThisGraph1.LongShade=0
  ThisGraph1.Pattern=Insight:None
  ThisGraph1.Float=0
  ThisGraph1.ZCluster=1
  ThisGraph1.Fill=0
  ThisGraph1.FillToZero=0
  ThisGraph1.SquareWave=0
  ThisGraph1.InnerRadius=0
  ThisGraph1.MaxPieRadius=0
  ThisGraph1.Shape=Insight:Auto
  ThisGraph1.PieAngle=0
  ThisGraph1.WorkSpaceWidth=0
  ThisGraph1.WorkSpaceHeight=0
  ThisGraph1.PieLabelLineColor=Color:None
  ThisGraph1.AspectRatio=1
  ThisGraph1.PieLabelLines=0
  ThisGraph1.StackLines=1
  ThisGraph1.LineFromZero=0
  ThisGraph1.LineWidth = 1
      ! Fonts
  ThisGraph1.LegendAngle   = 0
  ThisGraph1.ShowXLabelsEvery = 1
  ThisGraph1.DisplayOnX = 2   ! 0= don't display, 1= display number, 2= display name.
  ThisGraph1.SeparateYAxis = 1
  ThisGraph1.AutoXLabels = 0
  ThisGraph1.SpreadXLabels       = 0
  ThisGraph1.AutoXGridTicks = 1
  ThisGraph1.AutoScale = Scale:Low + Scale:High
  ThisGraph1.AutoYGridTicks = 0
  ThisGraph1.Depth = 0
          
  ThisGraph1.AddItem(1,1,LOC:Client_Q_Graph,Insight:GraphField,L_CQG:DIs,,,,,L_CQG:PointNo,)
  ThisGraph1.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  
  ThisGraph1.SetSetType(1,Insight:None)
  If ThisGraph1.GetSet(1) = 0 then ThisGraph1.AddSetQ(1).
  ThisGraph1.SetQ.SquareWave = -1
  ThisGraph1.SetQ.Fill       = -1
  ThisGraph1.SetQ.FillToZero = -1
  ThisGraph1.SetQ.PointWidth = 66
  ThisGraph1.SetQ.Points     = 0
  Put(ThisGraph1.SetQ)
          
  ThisGraph1.AddItem(2,2,LOC:Client_Q_Graph,Insight:GraphField,L_CQG:Ratio,,,,,L_CQG:PointNo,)
  ThisGraph1.SetSetYAxis(2,Scale:Low + Scale:High,,,)
  
  ThisGraph1.SetSetType(2,Insight:None)
  If ThisGraph1.GetSet(2) = 0 then ThisGraph1.AddSetQ(2).
  ThisGraph1.SetQ.SquareWave = -1
  ThisGraph1.SetQ.Fill       = -1
  ThisGraph1.SetQ.FillToZero = -1
  ThisGraph1.SetQ.PointWidth = 66
  ThisGraph1.SetQ.Points     = 0
  Put(ThisGraph1.SetQ)
          
  ThisGraph1.AddItem(3,3,LOC:Client_Q_Graph,Insight:GraphField,L_CQG:Value,,,,,L_CQG:PointNo,)
  ThisGraph1.SetSetYAxis(3,Scale:Low + Scale:High,,,)
  
  ThisGraph1.SetSetType(3,Insight:None)
  If ThisGraph1.GetSet(3) = 0 then ThisGraph1.AddSetQ(3).
  ThisGraph1.SetQ.SquareWave = -1
  ThisGraph1.SetQ.Fill       = -1
  ThisGraph1.SetQ.FillToZero = -1
  ThisGraph1.SetQ.PointWidth = 66
  ThisGraph1.SetQ.Points     = 0
  Put(ThisGraph1.SetQ)
          
  ThisGraph1.AddItem(4,4,LOC:Client_Q_Graph,Insight:GraphField,L_CQG:Weight,,,,,L_CQG:PointNo,)
  ThisGraph1.SetSetYAxis(4,Scale:Low + Scale:High,,,)
  
  ThisGraph1.SetSetType(4,Insight:None)
  If ThisGraph1.GetSet(4) = 0 then ThisGraph1.AddSetQ(4).
  ThisGraph1.SetQ.SquareWave = -1
  ThisGraph1.SetQ.Fill       = -1
  ThisGraph1.SetQ.FillToZero = -1
  ThisGraph1.SetQ.PointWidth = 66
  ThisGraph1.SetQ.Points     = 0
  Put(ThisGraph1.SetQ)
  !Mouse
  ThisGraph1.setMouseMoveParameters(1, '')
  if ThisGraph1.SavedGraphType<>0 then ThisGraph1.Type = ThisGraph1.SavedGraphType.
  ThisGraph1:Popup.Init()
  ThisGraph1:Popup.AddItem('Zoom Out','ZoomOut','',1)
  ThisGraph1:Popup.AddItemEvent('ZoomOut',INSIGHT:ZoomOut,?Insight)
  ThisGraph1:Popup.AddItem('Zoom In','ZoomIn','',1)
  ThisGraph1:Popup.AddItemEvent('ZoomIn',INSIGHT:ZoomIn,?Insight)
  ThisGraph1:Popup.AddItem('Cycle Sets','CycleSets','',1)
  ThisGraph1:Popup.AddItemEvent('CycleSets',INSIGHT:CycleSets,?Insight)
  ThisGraph1:Popup.AddItem('Copy','Copy','',1)
  ThisGraph1:Popup.AddItemEvent('Copy',INSIGHT:Copy,?Insight)
  ThisGraph1:Popup.AddItem('Save As...','SaveAs','',1)
  ThisGraph1:Popup.AddItemEvent('SaveAs',INSIGHT:SaveAs,?Insight)
  ThisGraph1:Popup.AddItem('Print Graph','Print','',1)
  ThisGraph1:Popup.AddItemEvent('Print',INSIGHT:PrintGraph,?Insight)
  ThisGraph1:Popup.AddItem('Graph Type','SetGraphType','',1)
  ThisGraph1:Popup.AddItem('Line','LineType','SetGraphType',2)
  ThisGraph1:Popup.AddItem('Bar','BarType','SetGraphType',2)
  ThisGraph1:Popup.AddItem('Pareto','ParetoType','SetGraphType',2)
  ThisGraph1:Popup.AddItemEvent('LineType',INSIGHT:SelectGraphType+Insight:Line,?Insight)
  ThisGraph1:Popup.AddItemEvent('BarType',INSIGHT:SelectGraphType+Insight:Bar,?Insight)
  ThisGraph1:Popup.AddItemEvent('ParetoType',INSIGHT:SelectGraphType+Insight:Pareto,?Insight)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Window_Client_Activity',QuickWindow)    ! Save window data to non-volatile store
  END
   ThisGraph1.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     ThisGraph1.Reset()
     Post(Event:accepted,?Insight)


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar7.SelectOnClose = True
      Calendar7.Ask('Select a Date',LO:From_Date)
      IF Calendar7.Response = RequestCompleted THEN
      LO:From_Date=Calendar7.SelectedDate
      DISPLAY(?LO:From_Date)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar8.SelectOnClose = True
      Calendar8.Ask('Select a Date',LO:To_Date)
      IF Calendar8.Response = RequestCompleted THEN
      LO:To_Date=Calendar8.SelectedDate
      DISPLAY(?LO:To_Date)
      END
      ThisWindow.Reset(True)
    OF ?Button_Load
      ThisWindow.Update()
          DO Process_Graph_Q
      
          ThisGraph1.ResetQueue()
      
          SELECT(?Tab_Graph)
    OF ?Ok:2
      ThisWindow.Update()
          IF LO:From_Date = 0
             SELECT(?LO:From_Date)
             CYCLE
          .
          IF LO:To_Date = 0
             SELECT(?LO:To_Date)
             CYCLE
          .
          IF LO:To_Date < LO:From_Date
             SELECT(?LO:From_Date)
             CYCLE
          .
      
          PUTINI('Window_Client_Activity', 'From_Date', LO:From_Date, GLO:Local_INI)
          PUTINI('Window_Client_Activity', 'To_Date', LO:To_Date, GLO:Local_INI)
      
          UNHIDE(?Progress1)
          DO Process_Clients
          HIDE(?Progress1)
          SELECT(?Tab_Graph)
    OF ?Insight
      If ((?Insight{prop:visible} or ThisGraph1.ActiveInvisible) and not 0{prop:acceptall})
        ThisGraph1.Draw()
      End
    OF ?Cancel
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Insight
    CASE EVENT()
    Of INSIGHT:ResetAndDraw
      ThisGraph1.Reset()
      Post(event:accepted,?Insight)
    Of INSIGHT:PrintGraph
      ThisGraph1.PrintGraph('',255)
    Of INSIGHT:Copy
         ThisGraph1.ToClipBoard()
    Of INSIGHT:SaveAs
      if FileDialog('Save As...',ThisGraph1:FileName,'Windows Bitmap|*.bmp|Portable Network Graphics|*.png',10010011b) = 1
        ThisGraph1.SaveAs(ThisGraph1:FileName)
      End
    Of INSIGHT:ZoomIn
      ThisGraph1.Zoom(,-25,ThisGraph1:ClickedOnPointNumber)
      if ThisGraph1.zoom < 5 then ThisGraph1:Popup.SetItemEnable('ZoomIn',0).
      if ThisGraph1.zoom < 100 then ThisGraph1:Popup.SetItemEnable('ZoomOut',1).
    Of INSIGHT:ZoomOut
      ThisGraph1.Zoom(,+25,ThisGraph1:ClickedOnPointNumber)
      if ThisGraph1.zoom = 100 then ThisGraph1:Popup.SetItemEnable('ZoomOut',0).
      if ThisGraph1.zoom > 4 then ThisGraph1:Popup.SetItemEnable('ZoomIn',1).
    Of INSIGHT:CycleSets
      ThisGraph1.SwapSets(1)
      ThisGraph1.Draw()
    Of INSIGHT:SelectGraphType + Insight:Bar to INSIGHT:SelectGraphType + Insight:Gantt
      ThisGraph1.Type = event() - INSIGHT:SelectGraphType
      ThisGraph1.Draw()
    OF EVENT:MouseUp
        If ThisGraph1.GetPoint(ThisGraph1:ClickedOnSetNumber,ThisGraph1:ClickedOnPointNumber) = true
          ThisGraph1:ClickedOnPointName = ThisGraph1.GetPointName(ThisGraph1:ClickedOnSetNumber,ThisGraph1:ClickedOnPointNumber)
        End
      Case Keycode()
      Of MouseRight !Up
        ThisGraph1:Popup.Ask()
      End
    OF EVENT:MouseMove
              IF ThisGraph1.GetPointName() <> ''
      ?Insight{prop:tip} = ThisGraph1.GetPointSummary(1,'<10>')
              ELSE
      ?Insight{prop:tip} = ''
              END
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Top, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Top

! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph1.ValidateRecord  PROCEDURE (long graphID)
ReturnValue  Long  ! set to > 0 to reject record
  Code
  ReturnValue = Parent.ValidateRecord(GraphID)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph1.SetPointName     PROCEDURE(Long graphID)
ReturnValue  String(255)
  Code
  ReturnValue = SUB(L_CQG:ClientNo & ' ' & L_CQG:ClientName,1,10)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph1.Reset     PROCEDURE (Byte graphForce=0)
  Code
  self.XAxisName = 'Clients'
      
  Self.SetSetPoints(1,0)
  Self.SetSetDescription(1,'DIs')
  Self.SetSetDataLabels(1,0,'', 1,0)
  ThisGraph1.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  
      
  Self.SetSetPoints(2,0)
  Self.SetSetDescription(2,'Rate')
  Self.SetSetDataLabels(2,0,'', 1,0)
  ThisGraph1.SetSetYAxis(2,Scale:Low + Scale:High,,,)
  
      
  Self.SetSetPoints(3,0)
  Self.SetSetDescription(3,'Value')
  Self.SetSetDataLabels(3,0,'', 1,0)
  ThisGraph1.SetSetYAxis(3,Scale:Low + Scale:High,,,)
  
      
  Self.SetSetPoints(4,0)
  Self.SetSetDescription(4,'Weight')
  Self.SetSetDataLabels(4,0,'', 1,0)
  ThisGraph1.SetSetYAxis(4,Scale:Low + Scale:High,,,)
  
  Parent.Reset(graphForce)
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Window_Client_ActivityJ PROCEDURE 

LOC:Options          GROUP,PRE(L_OG)                       ! 
CID                  ULONG                                 ! Client ID
ClientName           STRING(100)                           ! 
ClientNo             ULONG                                 ! Client No.
FromDate             DATE                                  ! 
ToDate               DATE                                  ! 
JIDs_Excluded        STRING(1000)                          ! 
                     END                                   ! 
LOC:Journey_Q        QUEUE,PRE(L_JQ)                       ! 
Journey              STRING(70)                            ! Description
Kgs                  DECIMAL(11)                           ! Kgs
DIs                  ULONG                                 ! No. of DIs
JID                  ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
Mark                 LONG                                  ! 
                     END                                   ! 
LOC:Work_Group       GROUP,PRE(L_WG)                       ! 
Graph_Title          STRING(100)                           ! 
Idx                  LONG                                  ! 
Timer                LONG                                  ! 
LTimes               LONG(10)                              ! 
Time                 LONG                                  ! 
Count                LONG                                  ! 
Progress             LONG                                  ! 
Prog_Bar             LONG                                  ! 
Where                STRING(500)                           ! 
                     END                                   ! 
LOC:J_Q1             QUEUE,PRE(L_J1)                       ! 
Journey              STRING(70)                            ! Description
Mark                 LONG                                  ! 
JID                  ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
                     END                                   ! 
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q1
ThisGraph4    Class(GlobalInsight)
ValidateRecord  PROCEDURE(long graphID),Long ,VIRTUAL
Reset           PROCEDURE (Byte graphForce=0),VIRTUAL
              End
ThisGraph4:Popup  Class(PopupClass)
               End
ThisGraph4:ClickedOnPointName   String(255)
ThisGraph4:ClickedOnPointNumber Real
ThisGraph4:ClickedOnSetNumber   Long
ThisGraph4:Color                Group(iColorGroupType), PRE(ThisGraph4:Color)
                                End
ThisGraph4:FileName              String(255)
!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
Window               WINDOW('Client Activity'),AT(,,397,223),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  MAX,MDI,TIMER(10),IMM
                       SHEET,AT(4,2,390,198),USE(?Sheet1)
                         TAB('Options'),USE(?Tab1)
                           GROUP,AT(13,28,375,166),USE(?Group1),TRN
                             PROMPT('Client Name:'),AT(13,28),USE(?ClientName:Prompt),TRN
                             BUTTON('...'),AT(65,28,12,10),USE(?CallLookup)
                             ENTRY(@s100),AT(83,28,215,10),USE(L_OG:ClientName),TIP('Client name')
                             PROMPT('Client No.:'),AT(13,44),USE(?ClientNo:Prompt),TRN
                             ENTRY(@n_10b),AT(83,44,60,10),USE(L_OG:ClientNo),RIGHT(1),COLOR(00E9E9E9h),MSG('Client No.'), |
  READONLY,SKIP,TIP('Client No.')
                             PROMPT('Leave client blank for all.'),AT(153,44),USE(?Prompt5:2),TRN
                             BUTTON('&Clear'),AT(264,44,34,10),USE(?Button_Clear)
                             PROMPT('From Date:'),AT(13,64),USE(?FromDate:Prompt),TRN
                             BUTTON('...'),AT(65,64,12,10),USE(?Calendar)
                             ENTRY(@d6b),AT(83,64,60,10),USE(L_OG:FromDate)
                             PROMPT('Leave dates blank for all.'),AT(153,64),USE(?Prompt5),TRN
                             PROMPT('To Date:'),AT(13,80),USE(?ToDate:Prompt),TRN
                             BUTTON('...'),AT(65,80,12,10),USE(?Calendar:2)
                             ENTRY(@d6b),AT(83,80,60,10),USE(L_OG:ToDate)
                           END
                           LIST,AT(13,102,154,92),USE(?List_J1),VSCROLL,FORMAT('280L(2)|M~Journeys to Exclude~@s70@'), |
  FROM(LOC:J_Q1),MARK(L_J1:Mark)
                           BUTTON('&OK'),AT(347,174,41,20),USE(?OkButton:2),DEFAULT
                         END
                         TAB('Activity'),USE(?Tab_Activity)
                           REGION,AT(9,18,382,179),USE(?Insight),BEVEL(1,-1),IMM
                         END
                         TAB('Data'),USE(?Tab3)
                           LIST,AT(9,18,343,161),USE(?List_J),VSCROLL,FORMAT('216L(2)|M~Journey~@s70@60D(2)|M~Kgs~' & |
  'L@n-15.0@52D(2)|M~DI s~L@n13@'),FROM(LOC:Journey_Q),MARK(L_JQ:Mark)
                           BUTTON('Add Marked to Excluded'),AT(261,184,,11),USE(?Button_Add_Excl)
                         END
                       END
                       PROGRESS,AT(7,208,185,8),USE(?Progress1),HIDE,RANGE(0,100)
                       BUTTON('&Cancel'),AT(358,206,36,14),USE(?CancelButton)
                     END
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q2

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Calendar2            CalendarClass
Calendar3            CalendarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

DI_View         VIEW(Deliveries)
    PROJECT(DEL:DID, DEL:JID)
!       JOIN(JOU:PKey_JID, DEL:JID)
!       PROJECT(JOU:Journey)
    .  !.



DI_VM       ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_Data               ROUTINE
    SETCURSOR(CURSOR:Wait)

    PUTINI('Window_Client_ActivityJ', 'FromDate', L_OG:FromDate, GLO:Local_INI)
    PUTINI('Window_Client_ActivityJ', 'ToDate', L_OG:ToDate, GLO:Local_INI)

    FREE(LOC:Journey_Q)
    CLEAR(L_WG:Where)
!DI_View         VIEW(Deliveries)
!    PROJECT(DEL:DID)
!       JOIN(JOU:PKey_JID, DEL:JID)
!       PROJECT(JOU:Journey)
!    .  .

    BIND('DEL:DIDate', DEL:DIDate)
    BIND('L_OG:FromDate', L_OG:FromDate)
    BIND('L_OG:ToDate', L_OG:ToDate)

    DI_VM.Init(DI_View, Relate:Deliveries)
    DI_VM.AddSortOrder(DEL:FKey_CID)
    IF L_OG:CID ~= 0
       DI_VM.AddRange(DEL:CID, L_OG:CID)

       ! (p:Add, p:List, p:Delim, p:Option, p:Prefix)
       ! (STRING, *STRING, STRING, BYTE=0, <STRING>)
       Add_To_List('CID = ' & L_OG:CID, L_WG:Where, ' AND ')

       L_WG:Graph_Title = 'Activity for (' & L_OG:ClientNo & '): ' & CLIP(L_OG:ClientName)
    ELSE
       L_WG:Graph_Title = 'Client Activity'
    .

    !DI_VM.AppendOrder()
    IF L_OG:FromDate > 0
       DI_VM.SetFilter('DEL:DIDate >= L_OG:FromDate','1')

       ! SQL_Get_DateT_G
       ! Returns SQL Date / Time string - (p:Date, p:Time, p:Encompass)
       ! (LONG, LONG=0, BYTE=0),STRING
       Add_To_List('DIDateAndTime >= ' & SQL_Get_DateT_G(L_OG:FromDate,,1), L_WG:Where, ' AND ')
    .
    IF L_OG:ToDate > 0
       DI_VM.SetFilter('DEL:DIDate < L_OG:ToDate + 1','2')
       Add_To_List('DIDateAndTime < ' & SQL_Get_DateT_G(L_OG:ToDate + 1,,1), L_WG:Where, ' AND ')
    .

    DO Get_JIDs_Excl
    IF CLIP(L_OG:JIDs_Excluded) ~= ''
       DI_VM.SetFilter('SQL(A.JID NOT IN (' & CLIP(L_OG:JIDs_Excluded) & '))','3')
       Add_To_List('JID NOT IN (' & CLIP(L_OG:JIDs_Excluded) & ')', L_WG:Where, ' AND ')
    .


    _SQLTemp{PROP:SQL}  = 'SELECT COUNT(*) FROM Deliveries WHERE ' & CLIP(L_WG:Where)
    NEXT(_SQLTemp)
    IF ERRORCODE()
       MESSAGE('Count error: ' & ERROR() & '|File Error: ' & FILEERROR())
    ELSE
       L_WG:Count   = _SQ:S1
    .

    L_WG:Progress   = 0

    ?Progress1{PROP:RangeHigh}  = 100
    ?Progress1{PROP:Progress}   = 0
    ?Progress1{PROP:Hide}       = FALSE

    DI_VM.Reset()

    L_WG:Timer  = 10
    Window{PROP:Timer} = L_WG:Timer
    EXIT
Load_Complete         ROUTINE
    DI_VM.Kill()

    UNBIND('DEL:DIDate')
    UNBIND('L_OG:FromDate')
    UNBIND('L_OG:ToDate')

    DO Load_Journeys
    SETCURSOR()

    L_WG:Timer  = 0
    ?Progress1{PROP:Hide}   = TRUE
    SELECT(?Tab_Activity)
    EXIT
! ---------------------------------------
Load_Journeys         ROUTINE
    L_WG:Idx    = 0
    LOOP
       L_WG:Idx += 1
       GET(LOC:Journey_Q, L_WG:Idx)
       IF ERRORCODE()
          BREAK
       .

       JOU:JID          = L_JQ:JID
       IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
          L_JQ:Journey  = JOU:Journey
          PUT(LOC:Journey_Q)
    .  .
    EXIT
Get_JIDs_Excl          ROUTINE
    CLEAR(L_OG:JIDs_Excluded)

    Idx_#   = 0
    LOOP
       Idx_#    += 1
       GET(LOC:J_Q1, Idx_#)
       IF ERRORCODE()
          BREAK
       .

       IF L_J1:Mark = TRUE
          L_OG:JIDs_Excluded    = CLIP(L_OG:JIDs_Excluded) & ',' & L_J1:JID
    .  .

    IF CLIP(L_OG:JIDs_Excluded) ~= ''
       L_OG:JIDs_Excluded       = SUB( CLIP(L_OG:JIDs_Excluded), 2, LEN(CLIP(L_OG:JIDs_Excluded)) - 1)

       PUTINI('Window_Client_ActivityJ', 'JIDs_Excluded', L_OG:JIDs_Excluded, GLO:Local_INI)
    .
    EXIT
! ---------------------------------------
Load_Journeys_Sel               ROUTINE
    L_OG:JIDs_Excluded  = GETINI('Window_Client_ActivityJ', 'JIDs_Excluded', '', GLO:Local_INI)

    FREE(LOC:J_Q1)
    CLEAR(LOC:J_Q1)

    CLEAR(JOU:Record,-1)
    SET(JOU:Key_Journey)
    LOOP
       IF Access:Journeys.TryNext() ~= LEVEL:Benign
          BREAK
       .

       CLEAR(LOC:J_Q1)
       L_J1:Journey = JOU:Journey
       L_J1:JID     = JOU:JID

       ! (p:Delim_List, p:Delim, p:Element, p:Chop_Out, p:Spaced)
       ! (*STRING, STRING, STRING, BYTE=0, BYTE=1),LONG
       IF Match_Element_In_Delim_Str(L_OG:JIDs_Excluded, ',', CLIP(L_J1:JID))
          L_J1:Mark = TRUE
       .

       ADD(LOC:J_Q1)
    .
    EXIT
Add_Run_Marked_Excl         ROUTINE
    Idx_#       = 0
    Upd_#       = 0
    LOOP
       Idx_#   += 1
       GET(LOC:Journey_Q, Idx_#)
       IF ERRORCODE()
          BREAK
       .

       IF L_JQ:Mark = TRUE
          L_J1:JID  = L_JQ:JID
          GET(LOC:J_Q1, L_J1:JID)
          IF ~ERRORCODE()
             L_J1:Mark  = TRUE
             PUT(LOC:J_Q1)
             Upd_#  += 1
    .  .  .

    IF Upd_# > 0
       MESSAGE('Updated Excluded Journeys.|Excluded Journeys list will be saved after next run.', 'Excluded Journeys', ICON:Asterisk)
    .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Window_Client_ActivityJ')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ClientName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Window_Client_ActivityJ',Window)           ! Restore window settings from non-volatile store
      L_OG:ClientName     = CLI:ClientName            ! Current buffer on this thread
      L_OG:CID            = CLI:CID
      L_OG:ClientNo       = CLI:ClientNo
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
    ! Start Of Insight InitStuff Group
  Bind('ThisGraph4:ClickedOnPointNumber',ThisGraph4:ClickedOnPointNumber) ! Insight ClickedOnVariable
  Bind('ThisGraph4:ClickedOnPointName',ThisGraph4:ClickedOnPointName)       ! Insight ClickedOnVariable
  Bind('ThisGraph4:ClickedOnSetNumber',ThisGraph4:ClickedOnSetNumber)     ! Insight ClickedOnVariable
  if ThisGraph4.Init(?Insight,Insight:Bar).
  if not ThisGraph4:Color:Fetched then ThisGraph4.GetWindowsColors(ThisGraph4:Color).
  Window{prop:buffer} = 1
  ThisGraph4.Stacked=0
  ThisGraph4.BlackAndWhite=0
  ThisGraph4.BackgroundPicture=''
  ThisGraph4.BackgroundColor=Color:Silver
  ThisGraph4.BackgroundShadeColor=Color:None
  ThisGraph4.BorderColor=Color:Black
  ThisGraph4.MinPointWidth=0
  ThisGraph4.PaperZoom=4
  ThisGraph4.ActiveInvisible=0
  ThisGraph4.PrintPortrait=0
  ThisGraph4.RightBorder=10
  ThisGraph4.MaxXGridTicks=16
  ThisGraph4.MaxYGridTicks=16
  ThisGraph4.WorkSpaceWidth=0
  ThisGraph4.WorkSpaceHeight=0
  ThisGraph4.AutoShade=1
  ThisGraph4.TopShade=0
  ThisGraph4.RightShade=0
  ThisGraph4.LongShade=0
  ThisGraph4.Pattern=Insight:None
  ThisGraph4.Float=0
  ThisGraph4.ZCluster=0
  ThisGraph4.Fill=0
  ThisGraph4.FillToZero=0
  ThisGraph4.SquareWave=0
  ThisGraph4.StackLines=1
  ThisGraph4.InnerRadius=0
  ThisGraph4.MaxPieRadius=0
  ThisGraph4.PieAngle=0
  ThisGraph4.PieLabelLineColor=Color:None
  ThisGraph4.AspectRatio=1
  ThisGraph4.PieLabelLines=0
  ThisGraph4.Shape=Insight:Auto
  ThisGraph4.LineFromZero=0
  ThisGraph4.LineWidth = 1
      ! Fonts
  ThisGraph4.LegendAngle   = 0
  ThisGraph4.ShowXLabelsEvery = 1
  ThisGraph4.DisplayOnX = 2   ! 0= don't display, 1= display number, 2= display name.
  ThisGraph4.SeparateYAxis = 1
  ThisGraph4.AutoXLabels = 0
  ThisGraph4.SpreadXLabels       = 0
  ThisGraph4.AutoXGridTicks = 1
  ThisGraph4.AutoScale = Scale:Low + Scale:High
  ThisGraph4.AutoYGridTicks = 0
  ThisGraph4.ShowDataLabels = 1
  ThisGraph4.DataLabelFormat = '@N13'
  ThisGraph4.ShowDataLabelsEvery = 1
  ThisGraph4.Depth = 200 / 10
          
  ThisGraph4.AddItem(1,1,LOC:Journey_Q,Insight:GraphField,L_JQ:Kgs,,,,,,L_JQ:Journey)
  ThisGraph4.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  
  ThisGraph4.SetSetType(1,Insight:None)
  If ThisGraph4.GetSet(1) = 0 then ThisGraph4.AddSetQ(1).
  ThisGraph4.SetQ.SquareWave = -1
  ThisGraph4.SetQ.Fill       = -1
  ThisGraph4.SetQ.FillToZero = -1
  ThisGraph4.SetQ.PointWidth = 66
  ThisGraph4.SetQ.Points     = 0
  Put(ThisGraph4.SetQ)
  !Mouse
  ThisGraph4.setMouseMoveParameters(1, '@N13')
  if ThisGraph4.SavedGraphType<>0 then ThisGraph4.Type = ThisGraph4.SavedGraphType.
  ThisGraph4:Popup.Init()
  ThisGraph4:Popup.AddItem('Copy','Copy','',1)
  ThisGraph4:Popup.AddItemEvent('Copy',INSIGHT:Copy,?Insight)
  ThisGraph4:Popup.AddItem('Save As...','SaveAs','',1)
  ThisGraph4:Popup.AddItemEvent('SaveAs',INSIGHT:SaveAs,?Insight)
  ThisGraph4:Popup.AddItem('Print Graph','Print','',1)
  ThisGraph4:Popup.AddItemEvent('Print',INSIGHT:PrintGraph,?Insight)
  ThisGraph4:Popup.AddItem('Graph Type','SetGraphType','',1)
  ThisGraph4:Popup.AddItem('Line','LineType','SetGraphType',2)
  ThisGraph4:Popup.AddItem('Bar','BarType','SetGraphType',2)
  ThisGraph4:Popup.AddItem('Pareto','ParetoType','SetGraphType',2)
  ThisGraph4:Popup.AddItemEvent('LineType',INSIGHT:SelectGraphType+Insight:Line,?Insight)
  ThisGraph4:Popup.AddItemEvent('BarType',INSIGHT:SelectGraphType+Insight:Bar,?Insight)
  ThisGraph4:Popup.AddItemEvent('ParetoType',INSIGHT:SelectGraphType+Insight:Pareto,?Insight)
  SELF.SetAlerts()
      DO Load_Journeys_Sel
  
      L_OG:FromDate   = GETINI('Window_Client_ActivityJ', 'FromDate', , GLO:Local_INI)
      L_OG:ToDate     = GETINI('Window_Client_ActivityJ', 'ToDate', , GLO:Local_INI)
  
      Window{PROP:Timer}     = 0
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('Window_Client_ActivityJ',Window)        ! Save window data to non-volatile store
  END
   ThisGraph4.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     ThisGraph4.Reset()
     Post(Event:accepted,?Insight)


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Select_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = L_OG:ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_OG:ClientName = CLI:ClientName
        L_OG:ClientNo = CLI:ClientNo
        L_OG:CID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?L_OG:ClientName
          IF CLIP(L_OG:ClientName) = ''
             CLEAR(L_OG:CID)
             CLEAR(L_OG:ClientNo)
             DISPLAY
          .
      IF L_OG:ClientName OR ?L_OG:ClientName{PROP:Req}
        CLI:ClientName = L_OG:ClientName
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_OG:ClientName = CLI:ClientName
            L_OG:ClientNo = CLI:ClientNo
            L_OG:CID = CLI:CID
          ELSE
            CLEAR(L_OG:ClientNo)
            CLEAR(L_OG:CID)
            SELECT(?L_OG:ClientName)
            CYCLE
          END
        ELSE
          L_OG:ClientNo = CLI:ClientNo
          L_OG:CID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?Button_Clear
      ThisWindow.Update()
          CLEAR(L_OG:CID)
          CLEAR(L_OG:ClientName)
          CLEAR(L_OG:ClientNo)
      
          DISPLAY
    OF ?Calendar
      ThisWindow.Update()
      Calendar2.SelectOnClose = True
      Calendar2.Ask('Select a Date',L_OG:FromDate)
      IF Calendar2.Response = RequestCompleted THEN
      L_OG:FromDate=Calendar2.SelectedDate
      DISPLAY(?L_OG:FromDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar3.SelectOnClose = True
      Calendar3.Ask('Select a Date',L_OG:ToDate)
      IF Calendar3.Response = RequestCompleted THEN
      L_OG:ToDate=Calendar3.SelectedDate
      DISPLAY(?L_OG:ToDate)
      END
      ThisWindow.Reset(True)
    OF ?OkButton:2
      ThisWindow.Update()
          DO Load_Data
    OF ?Insight
      If ((?Insight{prop:visible} or ThisGraph4.ActiveInvisible) and not 0{prop:acceptall})
        ThisGraph4.Draw()
      End
    OF ?Button_Add_Excl
      ThisWindow.Update()
          DO Add_Run_Marked_Excl
    OF ?CancelButton
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Insight
    CASE EVENT()
    Of INSIGHT:ResetAndDraw
      ThisGraph4.Reset()
      Post(event:accepted,?Insight)
    Of INSIGHT:PrintGraph
      ThisGraph4.PrintGraph('',255)
    Of INSIGHT:Copy
         ThisGraph4.ToClipBoard()
    Of INSIGHT:SaveAs
      if FileDialog('Save As...',ThisGraph4:FileName,'Windows Bitmap|*.bmp|Portable Network Graphics|*.png',10010011b) = 1
        ThisGraph4.SaveAs(ThisGraph4:FileName)
      End
    Of INSIGHT:SelectGraphType + Insight:Bar to INSIGHT:SelectGraphType + Insight:Gantt
      ThisGraph4.Type = event() - INSIGHT:SelectGraphType
      ThisGraph4.Draw()
    OF EVENT:MouseUp
        If ThisGraph4.GetPoint(ThisGraph4:ClickedOnSetNumber,ThisGraph4:ClickedOnPointNumber) = true
          ThisGraph4:ClickedOnPointName = ThisGraph4.GetPointName(ThisGraph4:ClickedOnSetNumber,ThisGraph4:ClickedOnPointNumber)
        End
      Case Keycode()
      Of MouseRight !Up
        ThisGraph4:Popup.Ask()
      End
    OF EVENT:MouseMove
              IF ThisGraph4.GetPointName() <> ''
      ?Insight{prop:tip} = ThisGraph4.GetPointSummary(1,'<10>')
              ELSE
      ?Insight{prop:tip} = ''
              END
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
          Window{PROP:Timer} = 0
      
          L_WG:Time   = CLOCK()
      
          LOOP L_WG:LTimes TIMES
             IF CLOCK() - L_WG:Time > 100
                L_WG:LTimes   = L_WG:LTimes * 0.9
                BREAK
             .
      
             L_WG:Progress    += 1
             IF INT((L_WG:Progress / L_WG:Count) * 100) ~= L_WG:Prog_Bar
                L_WG:Prog_Bar = INT((L_WG:Progress / L_WG:Count) * 100)
                ?Progress1{PROP:Progress}  = L_WG:Prog_Bar
             .
      
             IF DI_VM.Next() ~= LEVEL:Benign
                DO Load_Complete
                BREAK
             .
      
             CLEAR(LOC:Journey_Q)
             L_JQ:JID     = DEL:JID
             GET(LOC:Journey_Q, L_JQ:JID)
             IF ERRORCODE()
                CLEAR(LOC:Journey_Q)
                L_JQ:JID  = DEL:JID
                ADD(LOC:Journey_Q)
             .
      
             ! Get_DelItem_s_Totals
             ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache)
             ! (ULONG, BYTE, <ULONG>, <*STRING>, BYTE=0),ULONG
             !
             ! p:Option
             !   0. Weight of all DID items - Volumetric considered
             !   1. Total Items not loaded on the DID    - Manifest
             !   2. Total Items loaded on the DID        - Manifest
             !   3. Total Items on the DID
             !   4. Total Items not loaded on the DID    - Tripsheet
             !   5. Total Items loaded on the DID        - Tripsheet
             !   6. Weight of all DID items - real weight
             !   7. Total Items not delivered on the DID - Tripsheet     (similar to 4)
             !
             ! p:DIID is passed to check a single DIID
             ! p:Items_DIID_List is passed to return a list of DIIDs for the given Option
             !
             ! Cached entries are renewed every 1.5 seconds
      
             L_JQ:Kgs    += Get_DelItem_s_Totals(DEL:DID,0,,,TRUE) / 100  ! Returns a Ulong
             L_JQ:DIs    += 1
      
             PUT(LOC:Journey_Q)
          .
      
      
          IF CLOCK() - L_WG:Time < 50
             L_WG:LTimes   = L_WG:LTimes * 1.2
          .
          IF L_WG:LTimes = 0
             L_WG:LTimes   = 1
          .
      
          Window{PROP:Timer} = L_WG:Timer
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph4.ValidateRecord  PROCEDURE (long graphID)
ReturnValue  Long  ! set to > 0 to reject record
  Code
  ReturnValue = Parent.ValidateRecord(GraphID)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph4.Reset     PROCEDURE (Byte graphForce=0)
  Code
  self.HeaderName = L_WG:Graph_Title
  self.XAxisName = 'Journeys'
  self.YAxisName = 'Kgs'
  Sort(LOC:Journey_Q,L_JQ:Journey)
      
  Self.SetSetPoints(1,0)
  Self.SetSetDescription(1,'Kgs')
  Self.SetSetDataLabels(1,0,'', 1,0)
  ThisGraph4.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  
  Parent.Reset(graphForce)
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group1
  SELF.SetStrategy(?Progress1, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Progress1

!!! <summary>
!!! Generated from procedure template - Window
!!! (p:Option)
!!! </summary>
Update_Clients PROCEDURE (p:Option)

FormLocker           Class(TFormLocker)
CanBeUnlocked               Procedure(LONG pCtrl),BYTE,Derived 
        ! In this function you should add code if some fields are restricted to enable by Security
                     END
CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:AddressName      STRING(35)                            ! Name of this address
LOC:SalesRep         STRING(35)                            ! Sales Rep. Name
LOC:AccountantName   STRING(35)                            ! Accountants Name
LOC:Ton_Volume       DECIMAL(8,2)                          ! A Ton is equivalent to this volume
LOC:Rates_Group      GROUP,PRE()                           ! 
IncludePast          BYTE                                  ! Include past rates no longer effective
L_JG:Journey         STRING(70)                            ! Description
L_JG:JID             ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
L_LG:LoadType        STRING(35)                            ! Load Type
L_LG:LTID            ULONG                                 ! Load Type ID
EToll_CID            ULONG                                 ! CID to filter CID by
                     END                                   ! 
EToll_Unique         BYTE                                  ! 
LOC:Changed_Indicator ULONG                                ! When we change a browse change this to force update
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
LOC:Del_Group        GROUP,PRE()                           ! 
L_DG:Weight          DECIMAL(12,2)                         ! In kg's
L_DG:Units           USHORT                                ! Number of units
                     END                                   ! 
L_SF:ClientRatePerKg DECIMAL(10,4)                         ! Rate per Kg
LOC:ClientRateType_Used ULONG                              ! 
LOC:Invoice_Brws_Group GROUP,PRE()                         ! 
Locator              STRING(20)                            ! 
LI_BG:Delivery_Dates STRING(150)                           ! 
LI_BG:Commodities    STRING(150)                           ! 
LI_BG:Packagings     STRING(150)                           ! 
LI_BG:Status         STRING(30)                            ! 
LI_BG:Outstanding    BYTE(255)                             ! Invoice status
LI_BG:Change_Indicator BYTE                                ! 
LI_BG:DI_ContainerNos STRING(255)                          ! 
                     END                                   ! 
L_BI:Browse_Items    GROUP,PRE()                           ! 
L_BI:Outstanding     DECIMAL(15,2)                         ! Total Outstanding
L_BI:Credited        DECIMAL(15,2)                         ! 
L_BI:Payments        DECIMAL(15,2)                         ! 
                     END                                   ! 
L_BI:Deliveries_Commodities_Packaging BYTE                 ! Lookup Delivery Dates, Commodities & Packaging info.
LOC:Reminders_Group  GROUP,PRE(L_RG)                       ! 
Show_Active          BYTE                                  ! Show active
                     END                                   ! 
LOC:Client_Contacts  STRING(150)                           ! 
LOC:InvoiceStatus_Q  QUEUE,PRE(LQ)                         ! 
Str                  STRING(20)                            ! 
Value                BYTE                                  ! 
                     END                                   ! 
LOC:ClientNo_New     ULONG                                 ! Client No.
LOC:User_Access_Group GROUP,PRE(LOC)                       ! 
User_Access_Rates    LONG                                  ! 
User_Access_Invoices LONG                                  ! 
                     END                                   ! 
LOC:Invoice_Hist_Info GROUP,PRE(L_IHI)                     ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
LOC:PrePayment       DECIMAL(10,2)                         ! 
LOC:DefaultEmailAddress STRING(255)                        ! 
LOC:Previous_FuelSurcharge BYTE                            ! 
LOC:Payment_Group    GROUP,PRE()                           ! 
L_P:Type_Q           STRING(20)                            ! 
L_P:Status_Q         STRING(20)                            ! 
L_P:Status_Filter    LONG                                  ! 
                     END                                   ! 
LOC:Rates_FuelCost_Group GROUP,PRE()                       ! 
L_RFC:CID            ULONG                                 ! Client ID
L_RFC:FuelSurcharge  DECIMAL(7,3)                          ! 
L_RFC:FuelSurchargeNow DECIMAL(7,3)                        ! 
L_RFC:ClientEffectiveBaseRate DECIMAL(10,3)                ! 
L_RFC:Clients_Surcharge DECIMAL(12,4)                      ! 
                     END                                   ! 
Rates_Unique_Q       QUEUE,PRE(RUQ)                        ! 
JID                  ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
LTID                 ULONG                                 ! Load Type ID
CTID                 ULONG(0)                              ! Container Type ID
CRTID                ULONG                                 ! Client Rate Type ID
ToMass               DECIMAL(9)                            ! Up to this mass in Kgs
Effective_Date       DATE                                  ! Effective from date
Effective_TIME       TIME                                  ! Not used!
                     END                                   ! 
Additional_Unique_Q  QUEUE,PRE(AUQ)                        ! 
ACCID                ULONG                                 ! Additional Charge Category ID
                     END                                   ! 
Container_Unique_Q   QUEUE,PRE(CUQ)                        ! 
FID                  ULONG                                 ! Floor ID
                     END                                   ! 
ContainerPRA_Unique_Q QUEUE,PRE(CPRUQ)                     ! 
JID                  ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
ToMass               DECIMAL(9)                            ! Up to this mass in Kgs
                     END                                   ! 
FuelCosts_Unique     BYTE                                  ! 
LOC:SearchDel        STRING(20)                            ! 
BRW23::View:Browse   VIEW(_Statements)
                       PROJECT(STA:STID)
                       PROJECT(STA:StatementDate)
                       PROJECT(STA:StatementTime)
                       PROJECT(STA:Current)
                       PROJECT(STA:Days30)
                       PROJECT(STA:Days60)
                       PROJECT(STA:Days90)
                       PROJECT(STA:Total)
                       PROJECT(STA:CID)
                     END
Queue:Browse:Statements QUEUE                         !Queue declaration for browse/combo box using ?List:Statements
STA:STID               LIKE(STA:STID)                 !List box control field - type derived from field
STA:StatementDate      LIKE(STA:StatementDate)        !List box control field - type derived from field
STA:StatementTime      LIKE(STA:StatementTime)        !List box control field - type derived from field
STA:Current            LIKE(STA:Current)              !List box control field - type derived from field
STA:Days30             LIKE(STA:Days30)               !List box control field - type derived from field
STA:Days60             LIKE(STA:Days60)               !List box control field - type derived from field
STA:Days90             LIKE(STA:Days90)               !List box control field - type derived from field
STA:Total              LIKE(STA:Total)                !List box control field - type derived from field
STA:CID                LIKE(STA:CID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW26::View:Browse   VIEW(_Invoice)
                       PROJECT(INV:IID)
                       PROJECT(INV:DINo)
                       PROJECT(INV:InvoiceDate)
                       PROJECT(INV:Total)
                       PROJECT(INV:Weight)
                       PROJECT(INV:ClientReference)
                       PROJECT(INV:ShipperName)
                       PROJECT(INV:ConsigneeName)
                       PROJECT(INV:CR_IID)
                       PROJECT(INV:MIDs)
                       PROJECT(INV:CID)
                       PROJECT(INV:BID)
                       PROJECT(INV:Status)
                     END
Queue:Browse:Invoices QUEUE                           !Queue declaration for browse/combo box using ?List:Invoices
INV:IID                LIKE(INV:IID)                  !List box control field - type derived from field
INV:IID_Style          LONG                           !Field style
INV:DINo               LIKE(INV:DINo)                 !List box control field - type derived from field
INV:DINo_Style         LONG                           !Field style
INV:InvoiceDate        LIKE(INV:InvoiceDate)          !List box control field - type derived from field
L_BI:Outstanding       LIKE(L_BI:Outstanding)         !List box control field - type derived from local data
INV:Total              LIKE(INV:Total)                !List box control field - type derived from field
L_BI:Payments          LIKE(L_BI:Payments)            !List box control field - type derived from local data
L_BI:Credited          LIKE(L_BI:Credited)            !List box control field - type derived from local data
INV:Weight             LIKE(INV:Weight)               !List box control field - type derived from field
INV:ClientReference    LIKE(INV:ClientReference)      !List box control field - type derived from field
INV:ShipperName        LIKE(INV:ShipperName)          !List box control field - type derived from field
INV:ConsigneeName      LIKE(INV:ConsigneeName)        !List box control field - type derived from field
LI_BG:DI_ContainerNos  LIKE(LI_BG:DI_ContainerNos)    !List box control field - type derived from local data
INV:CR_IID             LIKE(INV:CR_IID)               !List box control field - type derived from field
INV:MIDs               LIKE(INV:MIDs)                 !List box control field - type derived from field
INV:CID                LIKE(INV:CID)                  !List box control field - type derived from field
INV:BID                LIKE(INV:BID)                  !List box control field - type derived from field
INV:Status             LIKE(INV:Status)               !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(Clients_ContainerParkDiscounts)
                       PROJECT(CLI_CP:Effective_Date)
                       PROJECT(CLI_CP:ContainerParkRateDiscount)
                       PROJECT(CLI_CP:ContainerParkMinimium)
                       PROJECT(CLI_CP:CPDID)
                       PROJECT(CLI_CP:CID)
                       PROJECT(CLI_CP:FID)
                       JOIN(FLO:PKey_FID,CLI_CP:FID)
                         PROJECT(FLO:Floor)
                         PROJECT(FLO:FID)
                       END
                     END
Queue:Browse:ContainerPark QUEUE                      !Queue declaration for browse/combo box using ?List:ContainerPark
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
CLI_CP:Effective_Date  LIKE(CLI_CP:Effective_Date)    !List box control field - type derived from field
CLI_CP:ContainerParkRateDiscount LIKE(CLI_CP:ContainerParkRateDiscount) !List box control field - type derived from field
CLI_CP:ContainerParkMinimium LIKE(CLI_CP:ContainerParkMinimium) !List box control field - type derived from field
CLI_CP:CPDID           LIKE(CLI_CP:CPDID)             !Primary key field - type derived from field
CLI_CP:CID             LIKE(CLI_CP:CID)               !Browse key field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW29::View:Browse   VIEW(__RatesAdditionalCharges)
                       PROJECT(CARA:Charge)
                       PROJECT(CARA:Effective_Date)
                       PROJECT(CARA:ACID)
                       PROJECT(CARA:CID)
                       PROJECT(CARA:ACCID)
                       JOIN(ACCA:PKey_ACCID,CARA:ACCID)
                         PROJECT(ACCA:Description)
                         PROJECT(ACCA:ACCID)
                       END
                     END
Queue:Browse:AddCharge QUEUE                          !Queue declaration for browse/combo box using ?List:AddCharge
ACCA:Description       LIKE(ACCA:Description)         !List box control field - type derived from field
CARA:Charge            LIKE(CARA:Charge)              !List box control field - type derived from field
CARA:Effective_Date    LIKE(CARA:Effective_Date)      !List box control field - type derived from field
CARA:ACID              LIKE(CARA:ACID)                !Primary key field - type derived from field
CARA:CID               LIKE(CARA:CID)                 !Browse key field - type derived from field
ACCA:ACCID             LIKE(ACCA:ACCID)               !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW33::View:Browse   VIEW(__Rates)
                       PROJECT(RAT:ToMass)
                       PROJECT(RAT:RatePerKg)
                       PROJECT(RAT:MinimiumCharge)
                       PROJECT(RAT:Effective_Date)
                       PROJECT(RAT:AdHoc)
                       PROJECT(RAT:Added_Date)
                       PROJECT(RAT:Added_Time)
                       PROJECT(RAT:RID)
                       PROJECT(RAT:JID)
                       PROJECT(RAT:LTID)
                       PROJECT(RAT:CRTID)
                       PROJECT(RAT:CTID)
                       PROJECT(RAT:RUBID)
                       PROJECT(RAT:CID)
                       JOIN(CTYP:PKey_CTID,RAT:CTID)
                         PROJECT(CTYP:ContainerType)
                         PROJECT(CTYP:CTID)
                       END
                       JOIN(CRT:PKey_CRTID,RAT:CRTID)
                         PROJECT(CRT:ClientRateType)
                         PROJECT(CRT:CRTID)
                         PROJECT(CRT:LTID)
                         JOIN(LOAD2:PKey_LTID,CRT:LTID)
                           PROJECT(LOAD2:LoadType)
                           PROJECT(LOAD2:LTID)
                         END
                       END
                       JOIN(JOU:PKey_JID,RAT:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                     END
Queue:Browse:Rates   QUEUE                            !Queue declaration for browse/combo box using ?List:Rates
CRT:ClientRateType     LIKE(CRT:ClientRateType)       !List box control field - type derived from field
LOAD2:LoadType         LIKE(LOAD2:LoadType)           !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
RAT:ToMass             LIKE(RAT:ToMass)               !List box control field - type derived from field
RAT:RatePerKg          LIKE(RAT:RatePerKg)            !List box control field - type derived from field
RAT:MinimiumCharge     LIKE(RAT:MinimiumCharge)       !List box control field - type derived from field
RAT:Effective_Date     LIKE(RAT:Effective_Date)       !List box control field - type derived from field
CTYP:ContainerType     LIKE(CTYP:ContainerType)       !List box control field - type derived from field
RAT:AdHoc              LIKE(RAT:AdHoc)                !List box control field - type derived from field
RAT:AdHoc_Icon         LONG                           !Entry's icon ID
RAT:Added_Date         LIKE(RAT:Added_Date)           !List box control field - type derived from field
RAT:Added_Time         LIKE(RAT:Added_Time)           !List box control field - type derived from field
RAT:RID                LIKE(RAT:RID)                  !List box control field - type derived from field
RAT:JID                LIKE(RAT:JID)                  !List box control field - type derived from field
RAT:LTID               LIKE(RAT:LTID)                 !List box control field - type derived from field
RAT:CRTID              LIKE(RAT:CRTID)                !List box control field - type derived from field
RAT:CTID               LIKE(RAT:CTID)                 !List box control field - type derived from field
RAT:RUBID              LIKE(RAT:RUBID)                !List box control field - type derived from field
RAT:CID                LIKE(RAT:CID)                  !Browse key field - type derived from field
CTYP:CTID              LIKE(CTYP:CTID)                !Related join file key field - type derived from field
CRT:CRTID              LIKE(CRT:CRTID)                !Related join file key field - type derived from field
LOAD2:LTID             LIKE(LOAD2:LTID)               !Related join file key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(__RatesContainerPark)
                       PROJECT(CPRA:ToMass)
                       PROJECT(CPRA:RatePerKg)
                       PROJECT(CPRA:Effective_Date)
                       PROJECT(CPRA:CPID)
                       PROJECT(CPRA:FID)
                       PROJECT(CPRA:JID)
                       JOIN(JOU:PKey_JID,CPRA:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:ContainerParkDiscount
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
CPRA:ToMass            LIKE(CPRA:ToMass)              !List box control field - type derived from field
CPRA:RatePerKg         LIKE(CPRA:RatePerKg)           !List box control field - type derived from field
L_SF:ClientRatePerKg   LIKE(L_SF:ClientRatePerKg)     !List box control field - type derived from local data
CPRA:Effective_Date    LIKE(CPRA:Effective_Date)      !List box control field - type derived from field
CPRA:CPID              LIKE(CPRA:CPID)                !Primary key field - type derived from field
CPRA:FID               LIKE(CPRA:FID)                 !Browse key field - type derived from field
CPRA:JID               LIKE(CPRA:JID)                 !Browse key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(ClientsRateTypes)
                       PROJECT(CRT:ClientRateType)
                       PROJECT(CRT:CRTID)
                       PROJECT(CRT:CID)
                       PROJECT(CRT:LTID)
                       JOIN(LOAD2:PKey_LTID,CRT:LTID)
                         PROJECT(LOAD2:LoadType)
                         PROJECT(LOAD2:LTID)
                       END
                     END
Queue:Browse:ClientRateTypes QUEUE                    !Queue declaration for browse/combo box using ?List:ClientRateTypes
CRT:ClientRateType     LIKE(CRT:ClientRateType)       !List box control field - type derived from field
LOAD2:LoadType         LIKE(LOAD2:LoadType)           !List box control field - type derived from field
LOC:ClientRateType_Used LIKE(LOC:ClientRateType_Used) !List box control field - type derived from local data
CRT:CRTID              LIKE(CRT:CRTID)                !List box control field - type derived from field
CRT:CID                LIKE(CRT:CID)                  !Browse key field - type derived from field
LOAD2:LTID             LIKE(LOAD2:LTID)               !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW32::View:Browse   VIEW(ClientsPayments)
                       PROJECT(CLIP:DateMade)
                       PROJECT(CLIP:Amount)
                       PROJECT(CLIP:DateCaptured)
                       PROJECT(CLIP:Notes)
                       PROJECT(CLIP:CPID)
                       PROJECT(CLIP:StatusUpToDate)
                       PROJECT(CLIP:CPID_Reversal)
                       PROJECT(CLIP:Type)
                       PROJECT(CLIP:Status)
                       PROJECT(CLIP:CID)
                     END
Queue:Browse:ClientPayment QUEUE                      !Queue declaration for browse/combo box using ?List:ClientPayment
CLIP:DateMade          LIKE(CLIP:DateMade)            !List box control field - type derived from field
CLIP:Amount            LIKE(CLIP:Amount)              !List box control field - type derived from field
L_P:Type_Q             LIKE(L_P:Type_Q)               !List box control field - type derived from local data
L_P:Status_Q           LIKE(L_P:Status_Q)             !List box control field - type derived from local data
CLIP:DateCaptured      LIKE(CLIP:DateCaptured)        !List box control field - type derived from field
CLIP:Notes             LIKE(CLIP:Notes)               !List box control field - type derived from field
CLIP:CPID              LIKE(CLIP:CPID)                !List box control field - type derived from field
CLIP:StatusUpToDate    LIKE(CLIP:StatusUpToDate)      !List box control field - type derived from field
CLIP:StatusUpToDate_Icon LONG                         !Entry's icon ID
CLIP:CPID_Reversal     LIKE(CLIP:CPID_Reversal)       !List box control field - type derived from field
CLIP:Type              LIKE(CLIP:Type)                !Browse hot field - type derived from field
CLIP:Status            LIKE(CLIP:Status)              !Browse hot field - type derived from field
CLIP:CID               LIKE(CLIP:CID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW40::View:Browse   VIEW(Reminders)
                       PROJECT(REM:RID)
                       PROJECT(REM:ReminderDate)
                       PROJECT(REM:ReminderTime)
                       PROJECT(REM:Active)
                       PROJECT(REM:Popup)
                       PROJECT(REM:Notes)
                       PROJECT(REM:ID)
                       PROJECT(REM:UGID)
                       PROJECT(REM:UID)
                       JOIN(USEG:PKey_UGID,REM:UGID)
                         PROJECT(USEG:GroupName)
                         PROJECT(USEG:UGID)
                       END
                       JOIN(USE:PKey_UID,REM:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:UID)
                       END
                     END
Queue:Browse:9       QUEUE                            !Queue declaration for browse/combo box using ?List:9
REM:RID                LIKE(REM:RID)                  !List box control field - type derived from field
REM:ReminderDate       LIKE(REM:ReminderDate)         !List box control field - type derived from field
REM:ReminderTime       LIKE(REM:ReminderTime)         !List box control field - type derived from field
REM:Active             LIKE(REM:Active)               !List box control field - type derived from field
REM:Active_Icon        LONG                           !Entry's icon ID
REM:Popup              LIKE(REM:Popup)                !List box control field - type derived from field
REM:Popup_Icon         LONG                           !Entry's icon ID
REM:Notes              LIKE(REM:Notes)                !List box control field - type derived from field
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
REM:ID                 LIKE(REM:ID)                   !Browse key field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Related join file key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW45::View:Browse   VIEW(EmailAddresses)
                       PROJECT(EMAI:EmailName)
                       PROJECT(EMAI:EmailAddress)
                       PROJECT(EMAI:RateLetter)
                       PROJECT(EMAI:DefaultAddress)
                       PROJECT(EMAI:Operations)
                       PROJECT(EMAI:DefaultOnDI)
                       PROJECT(EMAI:OperationsReference)
                       PROJECT(EMAI:EAID)
                       PROJECT(EMAI:CID)
                     END
Queue:Browse_EmailAddresses QUEUE                     !Queue declaration for browse/combo box using ?List_Email
EMAI:EmailName         LIKE(EMAI:EmailName)           !List box control field - type derived from field
EMAI:EmailAddress      LIKE(EMAI:EmailAddress)        !List box control field - type derived from field
EMAI:RateLetter        LIKE(EMAI:RateLetter)          !List box control field - type derived from field
EMAI:RateLetter_Icon   LONG                           !Entry's icon ID
EMAI:DefaultAddress    LIKE(EMAI:DefaultAddress)      !List box control field - type derived from field
EMAI:DefaultAddress_Icon LONG                         !Entry's icon ID
EMAI:Operations        LIKE(EMAI:Operations)          !List box control field - type derived from field
EMAI:Operations_Icon   LONG                           !Entry's icon ID
EMAI:DefaultOnDI       LIKE(EMAI:DefaultOnDI)         !List box control field - type derived from field
EMAI:DefaultOnDI_Icon  LONG                           !Entry's icon ID
EMAI:OperationsReference LIKE(EMAI:OperationsReference) !List box control field - type derived from field
EMAI:EAID              LIKE(EMAI:EAID)                !Primary key field - type derived from field
EMAI:CID               LIKE(EMAI:CID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW49::View:Browse   VIEW(__RatesFuelCost)
                       PROJECT(FCRA:Effective_Date)
                       PROJECT(FCRA:FuelCost)
                       PROJECT(FCRA:FuelBaseRate)
                       PROJECT(FCRA:FuelSurcharge)
                       PROJECT(FCRA:RFCID)
                       PROJECT(FCRA:CID)
                       PROJECT(FCRA:FCID)
                       JOIN(FUE:PKey_FCID,FCRA:FCID)
                         PROJECT(FUE:FCID)
                       END
                     END
Queue:Browse_FuelCosts QUEUE                          !Queue declaration for browse/combo box using ?List_FuelCosts
FCRA:Effective_Date    LIKE(FCRA:Effective_Date)      !List box control field - type derived from field
FCRA:FuelCost          LIKE(FCRA:FuelCost)            !List box control field - type derived from field
FCRA:FuelBaseRate      LIKE(FCRA:FuelBaseRate)        !List box control field - type derived from field
FCRA:FuelSurcharge     LIKE(FCRA:FuelSurcharge)       !List box control field - type derived from field
FCRA:RFCID             LIKE(FCRA:RFCID)               !Primary key field - type derived from field
FCRA:CID               LIKE(FCRA:CID)                 !Browse key field - type derived from field
FUE:FCID               LIKE(FUE:FCID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW43::View:Browse   VIEW(__RatesToll)
                       PROJECT(TOL:CID)
                       PROJECT(TOL:Effective_Date)
                       PROJECT(TOL:TollRate)
                       PROJECT(TOL:TRID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
TOL:CID                LIKE(TOL:CID)                  !List box control field - type derived from field
TOL:Effective_Date     LIKE(TOL:Effective_Date)       !List box control field - type derived from field
TOL:TollRate           LIKE(TOL:TollRate)             !List box control field - type derived from field
TOL:TRID               LIKE(TOL:TRID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB16::View:FileDrop VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
FDB22::View:FileDrop VIEW(JourneysAlias)
                       PROJECT(A_JOU:Journey)
                       PROJECT(A_JOU:JID)
                     END
FDB4::View:FileDrop  VIEW(LoadTypes2)
                       PROJECT(LOAD2:LoadType)
                       PROJECT(LOAD2:LTID)
                     END
FDB5::View:FileDrop  VIEW(AddressContacts)
                       PROJECT(ADDC:ContactName)
                       PROJECT(ADDC:PrimaryContact)
                       PROJECT(ADDC:ACID)
                     END
Queue:FileDrop       QUEUE                            !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !
A_JOU:Journey          LIKE(A_JOU:Journey)            !List box control field - type derived from field
A_JOU:JID              LIKE(A_JOU:JID)                !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:3     QUEUE                            !
LOAD2:LoadType         LIKE(LOAD2:LoadType)           !List box control field - type derived from field
LOAD2:LTID             LIKE(LOAD2:LTID)               !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:2     QUEUE                            !
ADDC:ContactName       LIKE(ADDC:ContactName)         !List box control field - type derived from field
ADDC:PrimaryContact    LIKE(ADDC:PrimaryContact)      !List box control field - type derived from field
ADDC:PrimaryContact_Icon LONG                         !Entry's icon ID
ADDC:ACID              LIKE(ADDC:ACID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::CLI:Record  LIKE(CLI:RECORD),THREAD
BRW23::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW23::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW23::PopupChoice   SIGNED                       ! Popup current choice
BRW23::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW23::PopupChoiceExec BYTE(0)                    ! Popup executed
BRW11::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW11::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW11::PopupChoice   SIGNED                       ! Popup current choice
BRW11::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW11::PopupChoiceExec BYTE(0)                    ! Popup executed
BRW29::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW29::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW29::PopupChoice   SIGNED                       ! Popup current choice
BRW29::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW29::PopupChoiceExec BYTE(0)                    ! Popup executed
BRW33::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW33::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW33::PopupChoice   SIGNED                       ! Popup current choice
BRW33::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW33::PopupChoiceExec BYTE(0)                    ! Popup executed
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW6::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW6::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW6::PopupChoice    SIGNED                       ! Popup current choice
BRW6::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW6::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW32::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW32::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW32::PopupChoice   SIGNED                       ! Popup current choice
BRW32::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW32::PopupChoiceExec BYTE(0)                    ! Popup executed
BRW40::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW40::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW40::PopupChoice   SIGNED                       ! Popup current choice
BRW40::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW40::PopupChoiceExec BYTE(0)                    ! Popup executed
BRW45::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW45::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW45::PopupChoice   SIGNED                       ! Popup current choice
BRW45::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW45::PopupChoiceExec BYTE(0)                    ! Popup executed
BRW49::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW49::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW49::PopupChoice   SIGNED                       ! Popup current choice
BRW49::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW49::PopupChoiceExec BYTE(0)                    ! Popup executed
QuickWindow          WINDOW('Update the Clients File'),AT(,,476,348),FONT('Tahoma',8),RESIZE,GRAY,IMM,MAX,MDI,HLP('Update_Clients'), |
  SYSTEM
                       PANEL,AT(2,3,471,52),USE(?Panel_Header),BEVEL(-1,1)
                       PROMPT('Client Name:'),AT(8,8),USE(?CLI:ClientName:Prompt)
                       ENTRY(@s100),AT(86,8,252,10),USE(CLI:ClientName),REQ,TIP('Client name')
                       BUTTON('Unlock'),AT(342,8,27,10),USE(?Unlock),TIP('Unlock fields for edit')
                       PROMPT('Ops. Manager:'),AT(8,24),USE(?CLI:OpsManager:Prompt)
                       ENTRY(@s35),AT(86,24,144,10),USE(CLI:OpsManager),MSG('Ops Manager'),TIP('Ops Manager')
                       PROMPT('Contacts:'),AT(8,40),USE(?LOC:AddressName:Prompt:2)
                       BUTTON('...'),AT(70,40,12,10),USE(?Button_Add_Contacts)
                       LIST,AT(86,40,95,10),USE(ADDC:ContactName),HVSCROLL,DROP(15,100),FORMAT('70L(2)|M~Conta' & |
  'ct Name~@s35@12L(2)|MI~Primary~@p p@'),FROM(Queue:FileDrop:2),MSG('Contacts Name')
                       ENTRY(@s150),AT(190,40,277,10),USE(LOC:Client_Contacts),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                       PROMPT('Status:'),AT(388,8),USE(?CLI:Status:Prompt)
                       LIST,AT(414,8,52,10),USE(CLI:Status),DROP(15),FROM('Normal|#0|On Hold|#1|Closed|#2|Dormant|#3'), |
  TIP('Normal, On Hold, Closed, Dormant'),MSG('Normal, On Hold, Closed, Dormant')
                       PROMPT('Client No.:'),AT(376,24),USE(?CLI:ClientNo:Prompt)
                       ENTRY(@n_10b),AT(414,24,52,10),USE(CLI:ClientNo),RIGHT(1),MSG('Client No.'),TIP('Client No.')
                       PROMPT('CID:'),AT(280,24),USE(?CLI:CID:Prompt)
                       STRING(@n_10),AT(286,24,52,10),USE(CLI:CID),RIGHT(1),TRN
                       SHEET,AT(2,58,471,269),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab_General)
                           PROMPT('Address:'),AT(8,76),USE(?LOC:AddressName:Prompt),TRN
                           BUTTON('&&'),AT(54,76,12,10),USE(?Button_Edit),TIP('Edit Address')
                           BUTTON('...'),AT(70,76,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(86,76,144,10),USE(LOC:AddressName),MSG('Name of this address'),TIP('Name of th' & |
  'is address')
                           PROMPT('Address'),AT(8,90,235,10),USE(?Prompt_AddCont),TRN
                           BUTTON('->'),AT(71,106,12,10),USE(?Button_Email_Generaltab),TIP('Send an email')
                           PROMPT('Default Email:'),AT(7,106),USE(?LOC:DefaultEmailAddress:Prompt),TRN
                           ENTRY(@s255),AT(86,106,144,10),USE(LOC:DefaultEmailAddress),ALRT(MouseLeft),COLOR(00E9E9E9h), |
  FLAT,READONLY,SKIP,TIP('Click to email')
                           PROMPT('VAT No.:'),AT(8,122),USE(?CLI:VATNo:Prompt),TRN
                           ENTRY(@s20),AT(86,122,70,10),USE(CLI:VATNo),MSG('VAT No.'),TIP('VAT No.')
                           PROMPT('Date Opened:'),AT(8,152),USE(?CLI:DateOpened:Prompt),TRN
                           BUTTON('...'),AT(70,152,12,10),USE(?Calendar:DateOpen)
                           SPIN(@d5),AT(86,152,70,10),USE(CLI:DateOpened),RIGHT(1)
                           GROUP,AT(86,158,70,167),USE(?Group_DropDowns)
                             LIST,AT(86,168,70,10),USE(CLI:Terms),DROP(5),FROM('Pre Paid|#0|COD|#1|Account|#2|On Statement|#3'), |
  MSG('Terms - Pre Paid, COD, Account'),TIP('Terms - Pre Paid, COD, Account')
                             LIST,AT(124,186,70,10),USE(CLI:InsuranceRequired),DROP(5),FROM('None|#0|Included in Rat' & |
  'e|#1|Per DI|#2'),HIDE,MSG('Insurance Required'),TIP('Insurance Required')
                             LIST,AT(86,200,70,10),USE(CLI:InsuranceType),DROP(5),FROM('By Weight|#0|By Value|#1'),MSG('Insurance Type'), |
  TIP('Insurance Type')
                             ENTRY(@n-11.6),AT(86,214,43,10),USE(CLI:InsurancePercent),RIGHT(1),MSG('Insruance Percentage'), |
  TIP('Insruance Percentage')
                             ENTRY(@n-11.2),AT(86,232,43,10),USE(CLI:DocumentCharge),DECIMAL(12),MSG('Document Charge'), |
  TIP('Document Charge')
                             ENTRY(@n-11.2),AT(86,246,43,10),USE(CLI:MinimiumCharge),RIGHT(1)
                             CHECK(' &Fuel Surcharge Active'),AT(86,263),USE(CLI:FuelSurchargeActive),TRN
                             CHECK(' &Toll Charge Active'),AT(86,278),USE(CLI:TollChargeActive)
                             ENTRY(@n-11.2),AT(86,296,43,10),USE(LOC:Ton_Volume),DECIMAL(12),MSG('1 Ton is equivalen' & |
  't to this volume'),TIP('1 Ton is equivalent to this volume')
                             ENTRY(@n-11.2),AT(86,311,43,10),USE(CLI:VolumetricRatio),DECIMAL(12),MSG('A square cube' & |
  ' is equivalent to this weight'),TIP('A square cube is equivalent to this weight')
                             STRING('Kgs per cubic metre'),AT(135,311,75,10),USE(?String_Ratio),TRN
                           END
                           PROMPT('Terms:'),AT(8,168),USE(?CLI:Terms:Prompt),TRN
                           PROMPT('Insurance Required:'),AT(46,186),USE(?CLI:InsuranceRequired:Prompt),HIDE,TRN
                           PROMPT('Insurance Type:'),AT(8,200),USE(?CLI:InsuranceType:Prompt),TRN
                           PROMPT('Insurance Percent:'),AT(8,214),USE(?CLI:InsurancePercent:Prompt),TRN
                           PROMPT('Document Charge:'),AT(8,232),USE(?CLI:DocumentCharge:Prompt),TRN
                           PROMPT('Minimium Charge:'),AT(8,246),USE(?CLI:MinimiumCharge:Prompt),TRN
                           PROMPT('1 Ton = Volume:'),AT(8,296),USE(?LOC:Ton_Volume:Prompt),TRN
                           PROMPT('Volumetric Ratio:'),AT(8,310),USE(?CLI:VolumetricRatio:Prompt),TRN
                           GROUP,AT(248,70,223,236),USE(?Group_Right)
                             PROMPT('Branch:'),AT(370,76),USE(?CLI:ClientName:Prompt:2),RIGHT,TRN
                             LIST,AT(398,76,68,10),USE(BRA:BranchName),DROP(5),FORMAT('140L(2)|M~Branch Name~@s35@'),FROM(Queue:FileDrop),MSG('Branch Name')
                             PROMPT('BID:'),AT(307,76),USE(?CLI:BID:Prompt),TRN
                             STRING(@n_10),AT(328,76,24,10),USE(CLI:BID),RIGHT(1),TRN
                             PROMPT('Payment Period:'),AT(344,152),USE(?CLI:PaymentPeriod:Prompt),RIGHT,DISABLE,TRN
                             SPIN(@n6),AT(398,152,68,10),USE(CLI:PaymentPeriod),RIGHT(1),DISABLE,MSG('Payment Period'), |
  TIP('Payment required in period (in days, 0 is COD/COP)')
                             PROMPT('Account Limit:'),AT(344,168,51,10),USE(?CLI:Limit:Prompt),RIGHT,TRN
                             ENTRY(@n-17.2),AT(398,168,68,9),USE(CLI:AccountLimit),RIGHT(1),MSG('Account limit'),TIP('Account limit')
                             LINE,AT(250,286,216,0),USE(?Line4),COLOR(COLOR:Black)
                             PANEL,AT(248,182,221,122),USE(?Panel1),BEVEL(-1,-1),FILL(00E0E1E7h)
                             BUTTON('&Fetch'),AT(254,186,209,10),USE(?Button_Calc_Age_Analysis),TIP('Calculate the A' & |
  'ges Analysis with respect to today')
                             LINE,AT(350,200,0,85),USE(?Line2),COLOR(COLOR:Black)
                             PROMPT('Last 30 Days:'),AT(354,214,51,10),USE(?L_SI:Current:Prompt:2),RIGHT,COLOR(00E0E1E7h)
                             ENTRY(@n-14.2),AT(410,214,52,10),USE(L_IHI:Current),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Current'), |
  READONLY,SKIP,TIP('Current')
                             PROMPT('30 - 60 Days:'),AT(354,230,51,10),USE(?L_SI:Current:Prompt:3),RIGHT,COLOR(00E0E1E7h)
                             ENTRY(@n-14.2),AT(410,230,52,10),USE(L_IHI:Days30),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('30 Das'), |
  READONLY,SKIP,TIP('30 Das')
                             PROMPT('Owing'),AT(254,202,90,10),USE(?Prompt49),FONT(,,,FONT:bold,CHARSET:ANSI),CENTER,TRN
                             PROMPT('Total Invoiced'),AT(352,202,110,10),USE(?Prompt49:2),FONT(,,,FONT:bold,CHARSET:ANSI), |
  CENTER,COLOR(00E0E1E7h)
                             PROMPT('Current:'),AT(254,214,30,10),USE(?L_SI:Current:Prompt),RIGHT,COLOR(00E0E1E7h)
                             ENTRY(@n-14.2),AT(290,214,52,10),USE(L_SI:Current),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Current'), |
  READONLY,SKIP,TIP('Current')
                             PROMPT('60 - 90 Days:'),AT(354,242,51,10),USE(?L_SI:Current:Prompt:4),RIGHT,COLOR(00E0E1E7h)
                             ENTRY(@n-14.2),AT(410,242,52,10),USE(L_IHI:Days60),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('60 Days'), |
  READONLY,SKIP,TIP('60 Days')
                             PROMPT('90 - 120 Days:'),AT(354,256,51,10),USE(?L_SI:Current:Prompt:5),RIGHT,COLOR(00E0E1E7h)
                             ENTRY(@n-14.2),AT(410,256,52,10),USE(L_IHI:Days90),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('90 Days'), |
  READONLY,SKIP,TIP('90 Days')
                             PROMPT('Total:'),AT(354,272,51,10),USE(?L_SI:Current:Prompt:6),RIGHT,COLOR(00E0E1E7h)
                             PROMPT('30 Days:'),AT(254,230,30,10),USE(?L_SI:Days30:Prompt),RIGHT,COLOR(00E0E1E7h)
                             ENTRY(@n-14.2),AT(290,230,52,10),USE(L_SI:Days30),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('30 Das'), |
  READONLY,SKIP,TIP('30 Das')
                             PROMPT('60 Days:'),AT(254,242,30,10),USE(?L_SI:Days60:Prompt),RIGHT,COLOR(00E0E1E7h)
                             ENTRY(@n-14.2),AT(290,242,52,10),USE(L_SI:Days60),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('60 Days'), |
  READONLY,SKIP,TIP('60 Days')
                             PROMPT('90 Days:'),AT(254,256,30,10),USE(?L_SI:Days90:Prompt),RIGHT,COLOR(00E0E1E7h)
                             ENTRY(@n-14.2),AT(290,256,52,10),USE(L_SI:Days90),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('90 Days'), |
  READONLY,SKIP,TIP('90 Days')
                             PROMPT('Total:'),AT(254,272,30,10),USE(?L_SI:Total:Prompt),RIGHT,COLOR(00E0E1E7h)
                             ENTRY(@n-14.2),AT(290,272,52,10),USE(L_SI:Total),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total'), |
  READONLY,SKIP,TIP('Total')
                             ENTRY(@n-14.2),AT(410,272,52,10),USE(L_IHI:Total),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total'), |
  READONLY,SKIP,TIP('Total')
                             LINE,AT(254,286,209,0),USE(?Line3),COLOR(COLOR:Black)
                             PROMPT('Pre-Payment:'),AT(302,290),USE(?LOC:PrePayment:Prompt),TRN
                             ENTRY(@n-14.2),AT(352,290,56,10),USE(LOC:PrePayment),DECIMAL(12),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP
                             OPTION('Liability Cover:'),AT(255,91,113,57),USE(CLI:LiabilityCover,,?CLI:LiabilityCover:2), |
  BOXED
                               RADIO('Own Insurance'),AT(259,102),USE(?CLI:LiabilityCover:Radio1:2),VALUE('1')
                               RADIO('Liability - loss attributed'),AT(259,113),USE(?CLI:LiabilityCover:Radio2:2),VALUE('2')
                               RADIO('Liability - however caused'),AT(259,123),USE(?CLI:LiabilityCover:Radio3:2),VALUE('3')
                               RADIO('(none)'),AT(259,134),USE(?CLI:LiabilityCover:Radio4:2),VALUE('0')
                             END
                             OPTION('Liability Cross Border:'),AT(376,91,91,57),USE(CLI:LiabilityCrossBorder),BOXED
                               RADIO('Within RSA Borders'),AT(381,103),USE(?CLI:LiabilityCrossBorder:Radio1),VALUE('1')
                               RADIO('Door to Door'),AT(381,116),USE(?CLI:LiabilityCrossBorder:Radio2),VALUE('2')
                               RADIO('(none)'),AT(381,130),USE(?CLI:LiabilityCrossBorder:Radio3),VALUE('0')
                             END
                           END
                         END
                         TAB('&2) Additional && Notes etc.'),USE(?Tab_AdditionalNotes)
                           GROUP,AT(8,82,222,144),USE(?Group_Add_Left)
                             PROMPT('Accountant:'),AT(8,76),USE(?LOC:AccountantName:Prompt),TRN
                             BUTTON('...'),AT(68,76,12,10),USE(?CallLookupAccountant)
                             ENTRY(@s35),AT(86,76,144,10),USE(LOC:AccountantName),MSG('Accountants Name'),TIP('Accountants Name')
                             PROMPT('Sales Rep:'),AT(8,102),USE(?LOC:SalesRep:Prompt),TRN
                             BUTTON('...'),AT(68,102,12,10),USE(?CallLookupSalesRep)
                             ENTRY(@s35),AT(86,102,144,10),USE(LOC:SalesRep),MSG('Sales Rep. Name'),TIP('Sales Rep. Name')
                             PROMPT('Sales Rep. Earns Until:'),AT(8,118),USE(?CLI:SalesRepEarnsUntil:Prompt),TRN
                             SPIN(@d17),AT(86,118,70,10),USE(CLI:SalesRepEarnsUntil),RIGHT(1),TIP('Date up till when' & |
  ' the sales rep. earns commision on this client.<0DH,0AH>Set here to extend normal pe' & |
  'riod, otherwise system will set this on 1st invoice.'),MSG('Date up till when the sales rep. earns commision on this client')
                             BUTTON('...'),AT(161,118,12,10),USE(?Calendar)
                             PROMPT('Delivery Notes:'),AT(8,140),USE(?CLI:DeliveryNotes:Prompt),TRN
                             LIST,AT(86,140,70,10),USE(CLI:DeliveryNotes),DROP(5),FROM('Individual|#0|Summary|#1'),MSG('Delivery notes'), |
  TIP('Delivery notes - Individual or Summary')
                             CHECK(' Advice of Dispatch'),AT(86,156,84,8),USE(CLI:AdviceOfDispatch),MSG('Advise on dispatch'), |
  TIP('Advise on dispatch'),TRN
                             CHECK(' Status Confirmation'),AT(86,170,,8),USE(CLI:FaxConfirmation),MSG('Confirmation ' & |
  'of parcel status'),TIP('Confirmation of parcel status required'),TRN
                             CHECK(' Generate Invoice on DI creation'),AT(86,183),USE(CLI:GenerateInvoice),MSG('Generate a' & |
  'n invoice when DI created'),TIP('Generate an invoice when DI created'),TRN
                             LINE,AT(7,202,463,0),USE(?Line5),COLOR(COLOR:Black)
                           END
                           PROMPT('ACID:'),AT(274,76),USE(?CLI:ACID:Prompt),TRN
                           STRING(@n_10),AT(298,76,52,10),USE(CLI:ACID),RIGHT(1),TRN
                           GROUP('Discounts'),AT(336,122,132,68),USE(?Group_Discounts),BOXED,DISABLE,HIDE,TRN
                             PROMPT('On Invoice:'),AT(342,133),USE(?CLI:OnInvoice:Prompt),TRN
                             ENTRY(@n-7.2),AT(418,133,43,10),USE(CLI:OnInvoice),DECIMAL(12),MSG('On Invoice'),TIP('On Invoice')
                             PROMPT('Days 30:'),AT(342,149),USE(?CLI:Days30:Prompt),TRN
                             ENTRY(@n-7.2),AT(418,149,43,10),USE(CLI:Days30),DECIMAL(12),MSG('30 Days'),TIP('30 Days')
                             PROMPT('Days 60:'),AT(342,162),USE(?CLI:Days60:Prompt),TRN
                             ENTRY(@n-7.2),AT(418,162,43,10),USE(CLI:Days60),DECIMAL(12),MSG('60 Days'),TIP('60 Days')
                             PROMPT('Days 90:'),AT(342,175),USE(?CLI:Days90:Prompt),TRN
                             ENTRY(@n-7.2),AT(418,175,43,10),USE(CLI:Days90),DECIMAL(12),MSG('90 Days'),TIP('90 Days')
                           END
                           PROMPT('General Notes:'),AT(8,212),USE(?CLI:Notes:Prompt),TRN
                           PROMPT('Invoice Message:'),AT(8,257,57,10),USE(?CLI:InvoiceMessage:Prompt:2),TRN
                           GROUP,AT(86,212,263,85),USE(?Group_Notes)
                             TEXT,AT(86,212,383,40),USE(CLI:Notes),VSCROLL,BOXED
                             TEXT,AT(86,257,383,20),USE(CLI:InvoiceMessage),VSCROLL,BOXED
                             TEXT,AT(86,281,383,20),USE(CLI:PODMessage)
                           END
                           PROMPT('POD Message:'),AT(8,281,57,10),USE(?CLI:POD_Message:Prompt),TRN
                         END
                         TAB('&3) Email Addresses && Reminders'),USE(?Tab_EmailAddReminders)
                           PROMPT('Email Addresses'),AT(6,74),USE(?Prompt53),TRN
                           LIST,AT(6,86,463,104),USE(?List_Email),VSCROLL,FORMAT('60L(2)|M~Email Name~@s35@180L(2)' & |
  '|M~Email Address~@s255@42L(2)|MI~Rate Letter~C(0)@p p@42L(2)|MI~Default Email~C(0)@p' & |
  ' p@42L(2)|MI~Operations~C(0)@p p@48L(2)|MI~Default On DI~C(0)@p p@60L(2)|M~Operation' & |
  's Reference~L(1)@s35@'),FROM(Queue:Browse_EmailAddresses),IMM,MSG('Browsing Records')
                           BUTTON('  &Email'),AT(6,194,,12),USE(?Button_Email),LEFT,ICON('Email2.ico'),FLAT,TIP('Email sele' & |
  'cted person')
                           BUTTON('&Insert'),AT(342,194,42,12),USE(?Insert)
                           BUTTON('&Change'),AT(384,194,42,12),USE(?Change)
                           BUTTON('&Delete'),AT(426,194,42,12),USE(?Delete)
                           GROUP,AT(5,211,464,110),USE(?Group_3_bot)
                             LINE,AT(5,211,463,0),USE(?Line1),COLOR(COLOR:Black)
                             PROMPT('Client Reminders'),AT(5,215),USE(?Prompt52)
                             LIST,AT(5,227,463,78),USE(?List:9),HVSCROLL,FORMAT('36R(1)|M~Reminder ID~L(2)@n_10@36R(' & |
  '1)|M~Date~L(2)@d5@22R(1)|M~Time~L(2)@T1@26R(1)|MI~Active~L(2)@p p@26R(1)|MI~Popup~L(' & |
  '2)@p p@150L(1)|M~Notes~L(2)@s255@40L(1)|M~User~L(2)@s20@50L(1)|M~Group Name~L(2)@s35@'), |
  FROM(Queue:Browse:9),IMM,MSG('Browsing Records')
                             GROUP,AT(8,309,119,10),USE(?Group_ShowActive)
                               PROMPT('Show:'),AT(5,309),USE(?Show_Active:Prompt),TRN
                               LIST,AT(29,309,70,10),USE(L_RG:Show_Active),DROP(5),FROM('All|#0|Active|#1|In-Active|#2'), |
  MSG('Show active'),TIP('Show active')
                             END
                             BUTTON('&View'),AT(295,309,42,12),USE(?View:4)
                             BUTTON('&Insert'),AT(343,309,42,12),USE(?Insert:7)
                             BUTTON('&Change'),AT(385,309,42,12),USE(?Change:7)
                             BUTTON('&Delete'),AT(427,309,42,12),USE(?Delete:7)
                           END
                         END
                         TAB('&4) Rates'),USE(?Tab_Ratess)
                           CHECK(' Include Past'),AT(406,78),USE(IncludePast,,?IncludePast:2),MSG('Include past ra' & |
  'tes no longer effective'),TIP('Include past rates no longer effective')
                           SHEET,AT(6,76,463,228),USE(?Sheet2)
                             TAB('Rates'),USE(?Tab_Rates_Rates)
                               PROMPT('Journey:'),AT(69,94),USE(?Prompt30:3),TRN
                               LIST,AT(109,94,129,10),USE(L_JG:Journey),VSCROLL,DROP(15),FORMAT('280L(2)|M~Journey~@s7' & |
  '0@40R(2)|M~JID~L@n_10@'),FROM(Queue:FileDrop:1)
                               STRING(@n_10),AT(241,94),USE(L_JG:JID),RIGHT(1),TRN
                               BUTTON('Show All'),AT(292,94,45,14),USE(?Button_Show_All)
                               PROMPT('Load Type:'),AT(69,107),USE(?Prompt30:4),TRN
                               LIST,AT(109,107,129,10),USE(L_LG:LoadType),VSCROLL,DROP(15),FORMAT('400L(2)|M~Load Type~@s100@'), |
  FROM(Queue:FileDrop:3)
                               LIST,AT(10,122,453,160),USE(?List:Rates),HVSCROLL,FORMAT('60L(1)|M~Client Rate Type~L(2' & |
  ')@s100@60L(1)|M~Load Type~L(2)@s100@60L(1)|M~Journey~L(2)@s70@36R(1)|M~To Mass~L(2)@' & |
  'n-12.0@46R(1)|M~Rate Per Kg~L(2)@n-13.4@44R(1)|M~Min. Charge~L(2)@n-14.2@38R(1)|M~Ef' & |
  'fective Date~L(2)@d5@56L(1)|M~Container Type~L(2)@s35@30L(1)|MI~Ad Hoc~L(2)@p p@38R(' & |
  '1)|M~Added Date~L(2)@d5@34R(1)|M~Added Time~L(2)@t7@30R(1)|M~RID~L(2)@n_10@30R(1)|M~' & |
  'JID~L(2)@n_10@30R(1)|M~LTID~L(2)@n_10@30R(1)|M~CRTID~L(2)@n_10@30R(1)|M~CTID~L(2)@n_' & |
  '10@30R(1)|M~RUBID~L(2)@n_10@'),FROM(Queue:Browse:Rates),IMM,MSG('Browsing Records')
                               BUTTON('Duplicate Rate(s)'),AT(10,286,,14),USE(?Button_DupRates_Rate),HIDE
                               GROUP,AT(302,286,161,14),USE(?Group_Rates)
                                 BUTTON('&Insert'),AT(322,286,45,14),USE(?Insert:Rates)
                                 BUTTON('&Change'),AT(370,286,45,14),USE(?Change:Rates)
                                 BUTTON('&Delete'),AT(418,286,45,14),USE(?Delete:Rates)
                               END
                             END
                             TAB('Container Park && Discounts'),USE(?Tab_ContainerPark)
                               GROUP,AT(10,92,456,84),USE(?Group_CP_Disc)
                                 LIST,AT(10,95,455,70),USE(?List:ContainerPark),VSCROLL,FORMAT('80L(1)|M~Floor~C(0)@s35@' & |
  '54R(1)|M~Effective Date~C(0)@d5@50R(1)|M~Discount~C(0)@n7.2@52R(1)|M~Minimium~C(0)@n-13.2@'), |
  FROM(Queue:Browse:ContainerPark),IMM,MSG('Browsing Records')
                                 GROUP,AT(308,168,157,9),USE(?Group_ContainerPark)
                                   BUTTON('&Insert'),AT(324,167,45,10),USE(?Insert:CP)
                                   BUTTON('&Change'),AT(372,167,45,10),USE(?Change:CP)
                                   BUTTON('&Delete'),AT(420,167,45,10),USE(?Delete:CP)
                                 END
                               END
                               GROUP,AT(10,183,455,94),USE(?Group_FloorRatesDiscounted)
                                 PROMPT('Floor Rates Discounted'),AT(10,182),USE(?Prompt40),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                                 LIST,AT(10,193,455,103),USE(?List:ContainerParkDiscount),VSCROLL,COLOR(00E9E9E9h),FORMAT('100L(1)|M~' & |
  'Journey~C(0)@s70@52R(1)|M~To Mass~C(0)@n-12.0@52R(1)|M~Rate Per Kg~C(0)@n-13.4@52R(1' & |
  ')|M~Discount R/Kg~C(0)@n-13.4@32R(1)|M~Effective Date~C(0)@d5@'),FROM(Queue:Browse:2),IMM, |
  MSG('Browsing Records')
                               END
                             END
                             TAB('Additional Charges'),USE(?Tab_AddCharge)
                               LIST,AT(10,94,455,188),USE(?List:AddCharge),VSCROLL,FORMAT('140L(1)|M~Description~C(0)@' & |
  's35@60R(1)|M~Charge~C(0)@n-15.2@32R(1)|M~Effective Date~C(0)@d5@'),FROM(Queue:Browse:AddCharge), |
  IMM,MSG('Browsing Records')
                               GROUP,AT(312,286,153,14),USE(?Group_AddCharges)
                                 BUTTON('&Insert'),AT(324,286,45,14),USE(?Insert:AC)
                                 BUTTON('&Change'),AT(372,286,45,14),USE(?Change:AC)
                                 BUTTON('&Delete'),AT(420,286,45,14),USE(?Delete:AC)
                               END
                             END
                             TAB('Client Rate Types'),USE(?Tab_ClientRate)
                               LIST,AT(10,94,455,188),USE(?List:ClientRateTypes),HVSCROLL,FORMAT('150L(2)|M~Client Rat' & |
  'e Type~@s100@100L(2)|M~Load Type~@s100@54R(2)|M~Used on Rates~L@n13@40R(2)|M~CRTID~L@n_10@'), |
  FROM(Queue:Browse:ClientRateTypes),IMM,MSG('Browsing Records')
                               GROUP,AT(292,286,173,14),USE(?Group_ClientRateTypes)
                                 BUTTON('&Insert'),AT(324,286,45,14),USE(?Insert:CR)
                                 BUTTON('&Change'),AT(372,286,45,14),USE(?Change:CR)
                                 BUTTON('&Delete'),AT(420,286,45,14),USE(?Delete:CR)
                               END
                             END
                             TAB('Fuel Costs'),USE(?Tab_FuelCost)
                               LIST,AT(10,94,455,188),USE(?List_FuelCosts),VSCROLL,FORMAT('60R(1)|M~Effective Date~C(0' & |
  ')@d5@64R(1)|M~Fuel Cost (cents)~C(0)@n-10.3@60R(1)|M~Fuel Base Rate~C(0)@n-13.4@60R(' & |
  '1)|M~Fuel Surcharge~C(0)@n-13.2~%~@'),FROM(Queue:Browse_FuelCosts),IMM,MSG('Browsing Records')
                               GROUP,AT(304,286,161,14),USE(?Group_FuelCosts)
                                 BUTTON('&Insert'),AT(324,286,45,14),USE(?Insert:FC)
                                 BUTTON('&Change'),AT(372,286,45,14),USE(?Change:FC)
                                 BUTTON('&Delete'),AT(420,286,45,14),USE(?Delete:FC)
                               END
                               PROMPT('Rates shown are for the Default client.'),AT(10,284,312,20),USE(?Prompt_RatesShown), |
  FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI),HIDE,TRN
                             END
                             TAB('E-Toll'),USE(?Tab_EToll)
                               LIST,AT(291,98,171,180),USE(?List),RIGHT(1),FORMAT('40R(2)|M~CID~C(1)@n_10@52R(2)|M~Eff' & |
  'ective Date~C(0)@d5@60R(2)|M~Toll Rate~C(1)@N-7.2~%~@'),FROM(Queue:Browse),IMM,MSG('Client ID'), |
  TIP('Client ID')
                               BUTTON('&Insert'),AT(323,283,45,14),USE(?Insert:EToll)
                               BUTTON('&Change'),AT(370,283,45,14),USE(?Change:EToll)
                               BUTTON('&Delete'),AT(417,283,45,14),USE(?Delete:EToll)
                               PROMPT('Rates shown are for the Default client.'),AT(8,282,312,20),USE(?Prompt_EToll_RatesShown), |
  FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI),HIDE,TRN
                             END
                           END
                         END
                         TAB('&6) Invoices'),USE(?Tab_Invoices)
                           GROUP,AT(148,268,149,12),USE(?Group2),HIDE
                             BUTTON('&Insert'),AT(201,306,42,12),USE(?Insert:Invoices)
                             BUTTON('&Change'),AT(175,306,42,12),USE(?Change:Invoices)
                             BUTTON('&Delete'),AT(175,308,42,12),USE(?Delete:Invoices)
                           END
                           LIST,AT(6,76,463,207),USE(?List:Invoices),HVSCROLL,ALRT(CtrlU),FORMAT('44R(2)|MY~No.~C(' & |
  '0)@n_10@30R(2)|MY~DI No.~@n_10b@38R(2)|M~Date~C(0)@d5b@[38R(2)|M~Outstanding~@n-21.2' & |
  '@38R(2)|M~Total~C(0)@n-14.2@38R(2)|~Payments~@n-21.2@38R(2)|~Credited~@n-21.2@]|~Cha' & |
  'rges~36R(2)|M~Weight~C(0)@n-12.2@38L(2)|M~Client Ref.~C(0)@s60@60L(2)|M~Shipper~C(0)' & |
  '@s35@60L(2)|M~Consignee~C(0)@s35@90L(2)|M~Container Nos.~C(0)@s255@40R(2)|M~CR IID~C' & |
  '(0)@n_10b@48L(2)|M~MIDs~C(0)@s20@30R(2)|M~CID~C(0)@n_10@30R(2)|M~BID~C(0)@n_10@'),FROM(Queue:Browse:Invoices), |
  IMM,MSG('Browsing Records')
                           BUTTON('&Print'),AT(6,288,,14),USE(?Button_PrintInvoice)
                           BUTTON('Update Statuses'),AT(346,288,,14),USE(?Button_UpdateStatuses),TIP('Updates the ' & |
  'Statuses of all the Clients Invoices')
                           GROUP,AT(42,290,257,10),USE(?Group_Inv_Status)
                             PROMPT('Status:'),AT(42,292),USE(?Prompt44),TRN
                             LIST,AT(70,292,70,10),USE(?List_InvoiceStatus),DROP(15),FORMAT('80L(2)|M~Status~@s20@'),FROM(LOC:InvoiceStatus_Q)
                             CHECK(' Details'),AT(146,292),USE(L_BI:Deliveries_Commodities_Packaging),MSG('Lookup Del' & |
  'ivery Dates, Commodities & Packaging info.'),TIP('Click here to show lookup details ' & |
  'for Delivery Dates, Commodities & Packaging info.'),TRN
                             STRING(''),AT(192,292,95,10),USE(?String_No_Tagged)
                           END
                           BUTTON('&View'),AT(416,288,,14),USE(?View:Invoices),LEFT,ICON('WAVIEW.ICO'),FLAT
                         END
                         TAB('&7) Statements'),USE(?Tab_Statements)
                           LIST,AT(6,76,463,207),USE(?List:Statements),HVSCROLL,FORMAT('40R(2)|M~STID~L@n_10@40R(2' & |
  ')|M~Date~L@d5b@25R(1)|M~Time~L(2)@t7@54R(1)|M~Current~L(2)@n-14.2@54R(1)|M~30 Days~L' & |
  '(2)@n-14.2@54R(1)|M~60 Days~L(2)@n-14.2@54R(1)|M~90 Days~L(2)@n-14.2@54R(1)|M~Total~' & |
  'L(2)@n-14.2@'),FROM(Queue:Browse:Statements),IMM,MSG('Browsing Records')
                           BUTTON('&Print'),AT(6,288,45,14),USE(?Button_PrintStatement)
                           BUTTON('&View'),AT(416,288,,14),USE(?View:Statements),LEFT,ICON('WAVIEW.ICO'),FLAT
                           BUTTON('&Insert'),AT(266,288,45,14),USE(?Insert:Statements),HIDE
                           BUTTON('&Change'),AT(314,288,45,14),USE(?Change:Statements),HIDE
                           BUTTON('&Delete'),AT(362,288,45,14),USE(?Delete:Statements),HIDE
                         END
                         TAB('&8) Payments'),USE(?Tab_Payments)
                           GROUP,AT(73,294,115,10),USE(?Group_PayFilter_Group)
                             PROMPT('Status Filter:'),AT(73,294),USE(?L_P:Status_Filter:Prompt),FONT('Tahoma'),TRN
                             LIST,AT(118,294,70,10),USE(L_P:Status_Filter),RIGHT(1),DROP(5),FROM('Not Allocated|#0|P' & |
  'artial Allocation|#1|Fully Allocated|#2|All|#-1')
                           END
                           LIST,AT(6,78,462,207),USE(?List:ClientPayment),HVSCROLL,FORMAT('42R(1)|M~Date Made~L(2)' & |
  '@d5@50R(2)|M~Amount~L@n-14.2@40L(2)|M~Type~@s20@60L(2)|M~Status~@s20@40R(2)|M~Date C' & |
  'aptured~L@d5@100L(2)|M~Notes~@s255@34R(2)|M~CPID~L@n_10@45L(2)|MI~Status Up To Date~' & |
  '@p p@34R(2)|M~Reversal of CPID~L@n_10b@'),FROM(Queue:Browse:ClientPayment),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(328,290,45,14),USE(?Insert:5)
                           BUTTON('&Change'),AT(376,290,45,14),USE(?Change:5)
                           BUTTON('&Delete'),AT(424,290,45,14),USE(?Delete:5)
                           BUTTON('&View'),AT(270,290,,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT
                           BUTTON('Reverse'),AT(6,290,,14),USE(?Button_Reverse),LEFT,ICON('UNDO.ICO'),FLAT,TIP('Reverse se' & |
  'lected payment & allocations')
                         END
                         TAB('5) Deliveries'),USE(?TAB_Clients)
                           BUTTON('Deliveries'),AT(201,98),USE(?BUTTON_Deliveries)
                         END
                       END
                       BUTTON('&OK'),AT(362,330,,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT
                       BUTTON('Cancel'),AT(420,330,,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT
                       BUTTON('&Save'),AT(4,330,,14),USE(?Save),LEFT,KEY(CtrlAltEnter),ICON('SAVE.ICO'),FLAT,TIP('Save Recor' & |
  'd and Close')
                       BUTTON('&Print Rates'),AT(60,330,,14),USE(?Button_PrintRates),LEFT,ICON('�<02H,016H,07FH>'), |
  FLAT
                     END

BRW23::LastSortOrder       BYTE
BRW26::LastSortOrder       BYTE
BRW11::LastSortOrder       BYTE
BRW29::LastSortOrder       BYTE
BRW33::LastSortOrder       BYTE
BRW2::LastSortOrder       BYTE
BRW6::LastSortOrder       BYTE
BRW32::LastSortOrder       BYTE
BRW40::LastSortOrder       BYTE
BRW45::LastSortOrder       BYTE
BRW49::LastSortOrder       BYTE
BRW26::SortHeader  CLASS(SortHeaderClassType) !Declare SortHeader Class
QueueResorted          PROCEDURE(STRING pString),VIRTUAL
                  END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW_Statements       CLASS(BrowseClass)                    ! Browse using ?List:Statements
Q                      &Queue:Browse:Statements       !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW23::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW_Invoices         CLASS(BrowseClass)                    ! Browse using ?List:Invoices
Q                      &Queue:Browse:Invoices         !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
                     END

BRW26::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW_Cont_Park_Discounts CLASS(BrowseClass)                 ! Browse using ?List:ContainerPark
Q                      &Queue:Browse:ContainerPark    !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW11::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW_AddCharge        CLASS(BrowseClass)                    ! Browse using ?List:AddCharge
Q                      &Queue:Browse:AddCharge        !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW29::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW_Rates            CLASS(BrowseClass)                    ! Browse using ?List:Rates
Q                      &Queue:Browse:Rates            !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW33::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW_RatesContainerPark CLASS(BrowseClass)                  ! Browse using ?List:ContainerParkDiscount
Q                      &Queue:Browse:2                !Reference to browse queue
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW_ClientRateTypes  CLASS(BrowseClass)                    ! Browse using ?List:ClientRateTypes
Q                      &Queue:Browse:ClientRateTypes  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW_ClientPayment    CLASS(BrowseClass)                    ! Browse using ?List:ClientPayment
Q                      &Queue:Browse:ClientPayment    !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW32::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW40                CLASS(BrowseClass)                    ! Browse using ?List:9
Q                      &Queue:Browse:9                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW40::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW45                CLASS(BrowseClass)                    ! Browse using ?List_Email
Q                      &Queue:Browse_EmailAddresses   !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW45::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW_FuelCosts        CLASS(BrowseClass)                    ! Browse using ?List_FuelCosts
Q                      &Queue:Browse_FuelCosts        !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW49::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW_ETolls           CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW43::Sort0:Locator StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB16                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB22                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:3              !Reference to display queue
                     END

FDB5                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:2              !Reference to display queue
SetQueueRecord         PROCEDURE(),DERIVED
                     END

Calendar21           CalendarClass
Calendar39           CalendarClass
p_Client_Tags              shpTagClass

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
  !------------------------------------
  !Style for ?List:Invoices
  !------------------------------------
!---------------------------------------------------------------------------
Contact_Info                            ROUTINE
    CLEAR(LOC:Client_Contacts)
    ADD:AID                 = CLI:AID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       ! (p:Add, p:List, p:Delim)
       Add_to_List(CLIP(ADD:PhoneNo), LOC:Client_Contacts, ',',, 'P: ')
       Add_to_List(CLIP(ADD:PhoneNo2), LOC:Client_Contacts, ',',, '  P2: ')
       Add_to_List(CLIP(ADD:Fax), LOC:Client_Contacts, ',',, '  F: ')
    .

    DISPLAY(?LOC:Client_Contacts)
    EXIT
Get_New_Client_No                ROUTINE
    CLEAR(A_CLI:Record, -1)
    A_CLI:ClientNo  = 101
    SET(A_CLI:Key_ClientNo, A_CLI:Key_ClientNo)
    LOOP
       IF Access:ClientsAlias.TryNext() ~= LEVEL:Benign
          BREAK
       .

       IF LOC:ClientNo_New = 0
          LOC:ClientNo_New  = A_CLI:ClientNo - 1
       .

       IF LOC:ClientNo_New + 1 = A_CLI:ClientNo
          LOC:ClientNo_New  = A_CLI:ClientNo
          CYCLE
       .

       ! Ok, so no one has this no. then!
       ! Use it!
       CLI:ClientNo = LOC:ClientNo_New + 1
       DISPLAY(?CLI:ClientNo)
       BREAK
    .

    EXIT
User_Buttons        				ROUTINE
    ! Note:  any changes to here now also require changes to FormLocker.CanBeUnlocked
    IF LOC:User_Access_Rates > 0
       DISABLE(?Button_DupRates_Rate)
       DISABLE(?Insert:Rates)
       DISABLE(?Change:Rates)
       DISABLE(?Delete:Rates)

       DISABLE(?Insert:CP)
       DISABLE(?Change:CP)
       DISABLE(?Delete:CP)

       DISABLE(?Insert:AC)
       DISABLE(?Change:AC)
       DISABLE(?Delete:AC)

       DISABLE(?Insert:CR)
       DISABLE(?Change:CR)
       DISABLE(?Delete:CR)

       DISABLE(?Insert:EToll)
       DISABLE(?Change:EToll)
       DISABLE(?Delete:EToll)

       IF LOC:User_Access_Rates ~= 1       ! Disable / Allow View??
    .  .


    IF LOC:User_Access_Invoices > 0
       DISABLE(?Insert:Invoices)
       DISABLE(?Change:Invoices)
       DISABLE(?Delete:Invoices)

       DISABLE(?Insert:Statements)
       DISABLE(?Change:Statements)
       DISABLE(?Delete:Statements)

       IF LOC:User_Access_Invoices ~= 1       ! Disable / Allow View??
          DISABLE(?View:Invoices)
          DISABLE(?Tab_Invoices)

          DISABLE(?Tab_Statements)
    .  .
    EXIT
Load_Address                          ROUTINE
    ?Prompt_AddCont{PROP:Text}  = ''
    ADD:AID                     = CLI:AID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       LOC:AddressName          = ADD:AddressName

       SUBU:SUID                = ADD:SUID
       IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) ~= LEVEL:Benign
          CLEAR(SUBU:Record)
       .

       ?Prompt_AddCont{PROP:Text} = CLIP(ADD:AddressName) & ', ' & CLIP(ADD:Line1) & ', ' & CLIP(ADD:Line2) & ', ' & CLIP(SUBU:Suburb) & ', ' & CLIP(SUBU:PostalCode)

       !CLEAR(ADDC:Record,-1)
       !ADDC:AID                 = ADD:AID
       !SET(ADDC:FKey_AID, ADDC:FKey_AID)
       !IF Access:AddressContacts.TryNext() = LEVEL:Benign
       !   ?Prompt_AddCont{PROP:Text}   = CLIP(ADDC:ContactName) & ', Phone: ' & CLIP(ADD:PhoneNo) & ', Fax: ' & CLIP(ADD:Fax)
       !ELSE
       !   ?Prompt_AddCont{PROP:Text}   = '<no contacts>, Phone: ' & CLIP(ADD:PhoneNo) & ', Fax: ' & CLIP(ADD:Fax)
    .  !.

    EXIT
Print_Invoices                          ROUTINE
    DATA

R:Type          BYTE
R:Print         BYTE
R:Idx           LONG
R:Preview       BYTE

    CODE
    BRW_Invoices.UpdateViewRecord()

!    message('INV:IID: ' & INV:IID)

    IF INV:IID ~= 0
       ThisWindow.Update

       R:Type       = POPUP('Print Invoice Continuous|Print Invoice Laser')

       R:Print      = 2
       IF p_Client_Tags.NumberTagged() > 0
          R:Print   = MESSAGE('Print tagged Invoices or Selected Invoice?','Print Option',ICON:Question, 'Tagged|Selected', 1)
       .

       IF R:Print = 1
          ! (p:Type, p:Preview, p:From, p:To, p:Un_Printed, <shpTagClass p_Client_Tags>)
          !   1       2           3       4       5                   6
          !   p:Type
          !       0 - Continuous
          !       1 - Page printer
          !   p:Un_Printed
          !       0 - All
          !       1 - Un-Printed only

          CASE MESSAGE('Would you like to preview each invoice?', 'Print Option', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
          OF BUTTON:Yes
             R:Preview  = TRUE
          OF BUTTON:No
             R:Preview  = FALSE
          .

          Print_Invoices_Multiple(R:Type - 1, R:Preview,,,, p_Client_Tags)
       ELSE
          IF R:Type = 1
             ! (p:ID, p:PrintType, p:ID_Options, p:Preview)
             Print_Cont(INV:IID, 0)
          ELSE
             Print_Invoices(, INV:IID)            ! IKB - need an option for Credit Notes?
!             Print_Invoices(INV:IID, 0)
       .  .

       ThisWindow.Reset
    .
    EXIT
! -----------------  Invoice tagging  -----------
Un_Tag_All                      ROUTINE
    p_Client_Tags.ClearAllTags()
    BRW_Invoices.ResetFromBuffer()
!    ?String_No_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()
    EXIT



!Tag_All                             ROUTINE
!       ! Tag All in the current sort order...
!       !PUSHBIND()
!       !BIND('')
!
!       Man_View.Init(View_Man, Relate:Manifest)
!
!!       CASE CHOICE(?CurrentTab)
!!       OF 2                        ! Delivery
!!       OF 3                        ! Branch
!!       OF 4                        ! Client                - Tag on
!!       OF 5                        ! Client No.
!!       OF 6                        ! DID
!!       ELSE                        ! Invoice No.
!!       .
!
!       !Man_View.SetFilter('')
!
!       Man_View.Reset()
!       LOOP
!          IF Man_View.Next() ~= LEVEL:Benign
!             BREAK
!          .
!
!          p_Client_Tags.MakeTag(MAN:MID)
!       .
!
!       Man_View.Kill()
!   !    POPBIND()
!!       ?String_No_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()
!
!       BRW_Manifests.ResetFromBuffer()
!    EXIT
Tag_Toggle_this_rec       ROUTINE
    DATA
Choice_#            LONG

    CODE
       Choice_#        = CHOICE(?List:Invoices)

       !LOC:Last_Tagged_Rec_Id       = CLI:CID

       IF p_Client_Tags.IsTagged(INV:IID) = TRUE
          p_Client_Tags.ClearTag(INV:IID)
   !       db.debugout('[Select_RateMod_Clients]  UN Tagging - CLI:CID: ' & CLI:CID)
       ELSE
          p_Client_Tags.MakeTag(INV:IID)
   !       db.debugout('[Select_RateMod_Clients]  Tagging - CLI:CID: ' & CLI:CID)
       .

   !    BRW1.ResetFromBuffer()
   !    BRW1.Reset(1)
       GET(Queue:Browse:Invoices, Choice_#)
       IF ~ERRORCODE()
          DO Style_Entry
          PUT(Queue:Browse:Invoices)
       .
!       ?String_No_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()
    EXIT
Tag_Popup                       ROUTINE
    BRW_Invoices.Popup.AddItem('Clear Tags','PopupTagUntag')
    BRW_Invoices.Popup.AddItemEvent('PopupTagUntag',EVENT:User)
    BRW_Invoices.Popup.AddItem('-','Separator')
    EXIT
Style_Setup                     ROUTINE
    p_Client_Tags.Init(?String_No_Tagged)

!    IF ~OMITTED(1)
       ! Style:Normal
   !    ?list1{PROPSTYLE:FontStyle, 1}     = FONT:Regular
   !    ?list1{PROPSTYLE:TextColor, 1}     = COLOR:NONE
   !    ?list1{PROPSTYLE:BackColor, 1}     = COLOR:NONE
   !    ?list1{PROPSTYLE:TextSelected, 1}  = COLOR:NONE
   !    ?list1{PROPSTYLE:BackSelected, 1}  = COLOR:NONE

       ! Style:Header
       !?list1{PROPSTYLE:FontStyle, 1}     = FONT:Bold
       ?List:Invoices{PROPSTYLE:TextColor, 1}     = COLOR:HIGHLIGHTTEXT
       ?List:Invoices{PROPSTYLE:BackColor, 1}     = COLOR:HIGHLIGHT
       ?List:Invoices{PROPSTYLE:TextSelected, 1}  = COLOR:White
       ?List:Invoices{PROPSTYLE:BackSelected, 1}  = COLOR:Blue

!       SELF.Q.LOC:Day_NormalFG = -2147483634
!       SELF.Q.LOC:Day_NormalBG = -2147483635
!       SELF.Q.LOC:Day_SelectedFG = -1
!       SELF.Q.LOC:Day_SelectedBG = -1

        !?LOC:Selected{PROP:Text}    = 'Show Selected (' & RECORDS(p_Addresses) & ')'
!    ELSE
       !LOC:Omitted_1    = TRUE

       !HIDE(?LOC:Selected)
!    .
    EXIT
Style_Entry                       ROUTINE
    !IF LOC:Omitted_1 = FALSE
       IF p_Client_Tags.IsTagged(Queue:Browse:Invoices.INV:IID) = TRUE
          Queue:Browse:Invoices.INV:IID_Style                      = 1
          Queue:Browse:Invoices.INV:DINo_Style                     = 1
      ELSE
          Queue:Browse:Invoices.INV:IID_Style                      = 0
          Queue:Browse:Invoices.INV:DINo_Style                     = 0
    .!  .

    EXIT

!INV:DINo              
!LOC:Status           
!INV:InvoiceDate      
!INV:Weight           
!LOC:Terms            
!L_BI:Outstanding     
!INV:Total            
!L_BI:Payments        
!L_BI:Credited        
!INV:Insurance        
!INV:Documentation    
!INV:FuelSurcharge    
!INV:FreightCharge    
!INV:VAT              
!LOC:BranchName       
!INV:VolumetricWeight 
!INV:Printed          
!INV:Printed_Icon     
!INV:ShipperName      
!INV:ConsigneeName    
!INV:MIDs             
!INV:InvoiceTime      
!INV:BID              
!INV:DID              
!INV:ICID             
!INV:CID              
!INV:Terms            
!INV:Status           
Tag_From_To                  ROUTINE            ! -------------------  not used yet
    DATA
R:Forward_Backward              BYTE

    CODE
!    R:Current_Selected_Rec_ID       = REC:Rec_ID
!
!!    MESSAGE('Selected Rec ID: ' & R:Current_Selected_Rec_ID & '|REC:TA_Date: ' & FORMAT(REC:TA_Date,@d5) & '|REC:TA_Time: ' & FORMAT(REC:TA_Time,@t4) & |
!!            '||LOC:Last_Tagged_Rec_Id: ' & LOC:Last_Tagged_Rec_Id)
!
!    ! If there is no Last Tagged then this is the only record to tag
!    IF LOC:Last_Clicked_Rec_ID ~= 0
!       LOC:Last_Tagged_Rec_Id   = LOC:Last_Clicked_Rec_ID
!    .
!
!    IF LOC:Last_Tagged_Rec_Id = 0
!       InBox_Tags.MakeTag(R:Current_Selected_Rec_ID)
!
!       LOC:Last_Tagged_Rec_Id       = R:Current_Selected_Rec_ID
!       LOC:Last_Tagged_Date         = REC:TA_Date
!       LOC:Last_Tagged_Time         = REC:TA_Time
!    ELSE
!       ! The beginning position of the loop depends on the last tagged record being before or after the
!       ! currently selected one.
!       R:Forward_Backward           = 0
!       IF REC:TA_Date >= LOC:Last_Tagged_Date
!          IF REC:TA_Date = LOC:Last_Tagged_Date
!             IF REC:TA_Time >= LOC:Last_Tagged_Time
!                IF REC:TA_Time = LOC:Last_Tagged_Time
!                   IF R:Current_Selected_Rec_ID < LOC:Last_Tagged_Rec_Id
!                      R:Forward_Backward  = 1
!                .  .
!             ELSE
!                R:Forward_Backward  = 1
!          .  .
!       ELSE
!          R:Forward_Backward        = 1
!       .
!
!
!       IF R:Forward_Backward = 0
!          CLEAR(A_REC:Record,-1)
!
!          ! Forward meaning from earliest Last Tagged Date / Time - file is in descending date / time order
!          ! So we set these to the later date / time here
!          A_REC:User_ID_Private_To      = LOC:User_ID
!          A_REC:TA_Date                 = REC:TA_Date
!          A_REC:TA_Time                 = REC:TA_Time
!
!          R:User_ID_Private_To          = LOC:User_ID
!          R:TA_Date                     = LOC:Last_Tagged_Date
!          R:TA_Time                     = LOC:Last_Tagged_Time
!
!          R:To_Rec_ID                   = LOC:Last_Tagged_Rec_Id
!       ELSE
!          CLEAR(A_REC:Record,+1)
!
!          A_REC:User_ID_Private_To      = LOC:User_ID
!          A_REC:TA_Date                 = LOC:Last_Tagged_Date
!          A_REC:TA_Time                 = LOC:Last_Tagged_Time
!
!          R:User_ID_Private_To          = LOC:User_ID
!          R:TA_Date                     = REC:TA_Date
!          R:TA_Time                     = REC:TA_Time
!          R:To_Rec_ID                   = REC:Rec_ID
!       .
!
!       ! The Key used does not have a field below the Time, so if the date and time in the From and To are the
!       ! same then the Records will be in record no. order or received order!
!       R:From_Rec_ID    = 0
!       IF (LOC:Last_Tagged_Date = REC:TA_Date) AND (LOC:Last_Tagged_Time = REC:TA_Time)
!          IF R:Current_Selected_Rec_ID ~= LOC:Last_Tagged_Rec_Id
!             ! Then the greater Rec_ID must be the To Rec_ID
!             IF LOC:Last_Tagged_Rec_Id > R:Current_Selected_Rec_ID
!                R:To_Rec_ID     = LOC:Last_Tagged_Rec_Id
!                R:From_Rec_ID   = R:Current_Selected_Rec_ID
!             ELSE
!                R:To_Rec_ID     = R:Current_Selected_Rec_ID
!                R:From_Rec_ID   = LOC:Last_Tagged_Rec_Id
!       .  .  .
!
!       SET(A_REC:SKEY_Adjust_Date_Time, A_REC:SKEY_Adjust_Date_Time)
!       LOOP
!          IF Access:Received_Msgs_Alias.TryNext() ~= LEVEL:Benign
!             BREAK
!          .
!
!!       IF R:Forward_Backward = 0
!!          MESSAGE('Set Date / Time: ' & FORMAT(REC:TA_Date,@d5) & ' / ' & FORMAT(REC:TA_Time, @t4) & '||' |
!!                   & 'Found Date / Time: ' & FORMAT(A_REC:TA_Date,@d5) & ' / ' & FORMAT(A_REC:TA_Time, @t4) |
!!                   & '||A_REC:Rec_ID: ' & REC:Rec_ID & ' - to Rec_ID: ' & R:To_Rec_ID,'Forward')
!!       ELSE
!!          MESSAGE('Set Date / Time: ' & FORMAT(LOC:Last_Tagged_Date,@d5) & ' / ' & FORMAT(LOC:Last_Tagged_Time, @t4) & '||' |
!!                   & 'Found Date / Time: ' & FORMAT(A_REC:TA_Date,@d5) & ' / ' & FORMAT(A_REC:TA_Time, @t4) |
!!                   & '||A_REC:Rec_ID: ' & REC:Rec_ID & ' - to Rec_ID: ' & R:To_Rec_ID,'Backwards')
!!       .
!
!          IF A_REC:User_ID_Private_To ~= LOC:User_ID      |
!                OR A_REC:TA_Date < R:TA_Date OR (A_REC:TA_Date = R:TA_Date AND A_REC:TA_Time < R:TA_Time)
!             BREAK
!          .
!
!          IF R:From_Rec_ID ~= 0
!             !MESSAGE('cycles R:From_Rec_ID < A_REC:Rec_ID||' & R:From_Rec_ID & ' < ' & A_REC:Rec_ID,'To Rec_ID: ' & R:To_Rec_ID)
!             IF R:From_Rec_ID > A_REC:Rec_ID
!                CYCLE
!          .  .
!
!          InBox_Tags.MakeTag(A_REC:Rec_ID)
!
!          IF R:To_Rec_ID = A_REC:Rec_ID
!             BREAK
!    .  .  .
!
!    BRW1.ResetFromBuffer()
    EXIT




Check_Fuel_Rates            ROUTINE
    L_RFC:CID   = CLI:CID

    ! Check if this Client has any of their own fuel surcharge records
    IF CLI:FuelSurchargeActive = FALSE
       ?Prompt_RatesShown{PROP:Text}    = 'Fuel surcharges are not active on this client - see General tab option "Fuel Surcharge Active"'
       UNHIDE(?Prompt_RatesShown)
    ELSE
       ! Get_Client_FuelSurcharge
       ! (p:CID, p:Date, p:FuelBaseRate, p:CID_Used)
       ! (ULONG, LONG=0, <*DECIMAL>, <*ULONG>),STRING
       ! p:Date of -1 will get latest dated effective rate
       Get_Client_FuelSurcharge(CLI:CID, -1,, L_RFC:CID)
                                                            
   !    message('CLI:CID = L_RFC:CID|||' & CLI:CID & ' = ' & L_RFC:CID)

       ! L_RFC:CID     - CID should be the passed in CID or the base rate CID
       IF CLI:CID = L_RFC:CID
          HIDE(?Prompt_RatesShown)

          !ENABLE(?Insert:FC)
          ENABLE(?Change:FC)
          ENABLE(?Delete:FC)

          !BRW_FuelCosts.InsertControl  = ?Insert:FC
          BRW_FuelCosts.ChangeControl  = ?Change:FC
          BRW_FuelCosts.DeleteControl  = ?Delete:FC
       ELSE
          A_CLI:CID        = L_RFC:CID
          IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) ~= LEVEL:Benign
             CLEAR(A_CLI:Record)
          .
          ?Prompt_RatesShown{PROP:Text}    = 'This client does not have any special fuel surcharge rate specified. <13,10>Rates shown above are the ' &|
                       'default (specified under client ' & CLIP(A_CLI:ClientName) & ' - ' & A_CLI:ClientNo & ')'

          UNHIDE(?Prompt_RatesShown)

          !DISABLE(?Insert:FC)
          DISABLE(?Change:FC)
          DISABLE(?Delete:FC)

          !BRW_FuelCosts.InsertControl  = 0
          BRW_FuelCosts.ChangeControl  = 0
          BRW_FuelCosts.DeleteControl  = 0
    .  .
    EXIT
Check_EToll_Rates            ROUTINE
    EToll_CID   = CLI:CID

    ! Check if this Client has any of their own fuel surcharge records
    IF CLI:TollChargeActive = FALSE
       ?Prompt_EToll_RatesShown{PROP:Text}    = 'Toll charges are not active on this client - see General tab option "Toll Charge Active"'
       UNHIDE(?Prompt_EToll_RatesShown)
    ELSE
          ! Get_Client_FuelSurcharge
       ! (p:CID, p:Date, p:FuelBaseRate, p:CID_Used)
       ! (ULONG, LONG=0, <*DECIMAL>, <*ULONG>),STRING
       ! p:Date of -1 will get latest dated effective rate
       !Get_Client_FuelSurcharge(CLI:CID, -1,, L_RFC:CID)
       
       Get_Client_ETolls(CLI:CID, -1, EToll_CID)           ! Populates CID used for rate (in case of default)
                                                            
   !    message('CLI:CID = L_RFC:CID|||' & CLI:CID & ' = ' & L_RFC:CID)

       ! L_RFC:CID     - CID should be the passed in CID or the base rate CID
       IF CLI:CID = EToll_CID
          HIDE(?Prompt_EToll_RatesShown)

          !ENABLE(?Insert:FC)
          ENABLE(?Change:EToll)
          ENABLE(?Delete:EToll)

          !BRW_FuelCosts.InsertControl  = ?Insert:FC
          BRW_ETolls.ChangeControl  = ?Change:EToll
          BRW_ETolls.DeleteControl  = ?Delete:EToll
       ELSE
          A_CLI:CID        = EToll_CID                      ! BRW_ETolls - bug found via unusal conversion warning 12/03/14
          IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) ~= LEVEL:Benign
             CLEAR(A_CLI:Record)
          .
          ?Prompt_EToll_RatesShown{PROP:Text}    = 'This client does not have any special EToll surcharge rate specified. <13,10>Rates shown above are the ' &|
                       'default (specified under client ' & CLIP(A_CLI:ClientName) & ' - ' & A_CLI:ClientNo & ')'

          UNHIDE(?Prompt_EToll_RatesShown)

          !DISABLE(?Insert:FC)
          DISABLE(?Change:EToll)
          DISABLE(?Delete:EToll)

          !BRW_FuelCosts.InsertControl  = 0
          BRW_ETolls.ChangeControl  = 0
          BRW_ETolls.DeleteControl  = 0
    .  .
    EXIT
Access_Permissions         ROUTINE
     LOC:User_Access_Invoices   = Get_User_Access(GLO:UID, 'Update Invoices', 'Update_Clients')
     IF LOC:User_Access_Invoices > 0
        BRW_Invoices.InsertControl   = 0
        BRW_Invoices.ChangeControl   = 0
        BRW_Invoices.DeleteControl   = 0
  
        BRW_Statements.InsertControl   = 0
        BRW_Statements.ChangeControl   = 0
        BRW_Statements.DeleteControl   = 0
  
        IF LOC:User_Access_Invoices ~= 1       ! Disable / Allow View??
           BRW_Invoices.ViewControl              = 0
           BRW_Statements.ViewControl    = 0
     .  .
  
  
     db.debugout('[Update_Clients]  GLO:UID: ' & GLO:UID & ',   LOC:User_Access_Invoices: ' & LOC:User_Access_Invoices)
      
   ! SEE also other Get_User_Access calls elsewhere in procedure
      
   ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned)
   ! (ULONG, STRING      , STRING     , <STRING>       , BYTE=0             , <*BYTE>                 ),LONG
   ! Returns
   !   0   - Allow (default)
   !   1   - Disable
   !   2   - Hide
   !   100 - Allow                 - Default action
   !   101 - Disallow              - Default action
   Access_#   = Get_User_Access(GLO:UID, 'Print Rates Button', 'Update_Clients')
   IF Access_# > 0
      HIDE(?Button_PrintRates)
   .

   db.debugout('[Update_Clients - Gen Sec]  GLO:UID: ' & GLO:UID & ',   Print Rates - User_Access: ' & Access_#)
      
   EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Clients Record '
  OF InsertRecord
    ActionMessage = 'Clients Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Clients Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  CASE SELF.Request
  OF ChangeRecord OROF DeleteRecord
    QuickWindow{PROP:Text} = QuickWindow{PROP:Text} & '  (' & CLI:ClientName & ')' ! Append status message to window title text
  OF InsertRecord
    QuickWindow{PROP:Text} = QuickWindow{PROP:Text} & '  (New)'
  END
  QuickWindow{Prop:Text} = ActionMessage          ! Display status message in title bar
  CASE SELF.Request
  OF ViewRecord OROF ChangeRecord OROF DeleteRecord
     QuickWindow{Prop:Text} = QuickWindow{Prop:Text} & '  (' & CLIP(CLI:ClientName) & '  [' & CLI:ClientNo & '])' ! Append status message to window title text
  OF InsertRecord
    QuickWindow{Prop:Text} = QuickWindow{Prop:Text} & '  (New)'
  .
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Clients')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel_Header
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LQ:Value',LQ:Value)                       ! Added by: BrowseBox(ABC)
  BIND('L_LG:LTID',L_LG:LTID)                     ! Added by: BrowseBox(ABC)
  BIND('L_JG:JID',L_JG:JID)                       ! Added by: BrowseBox(ABC)
  BIND('L_P:Status_Filter',L_P:Status_Filter)     ! Added by: BrowseBox(ABC)
  BIND('L_RG:Show_Active',L_RG:Show_Active)       ! Added by: BrowseBox(ABC)
  BIND('LI_BG:Outstanding',LI_BG:Outstanding)     ! Added by: BrowseBox(ABC)
  BIND('L_BI:Outstanding',L_BI:Outstanding)       ! Added by: BrowseBox(ABC)
  BIND('L_BI:Payments',L_BI:Payments)             ! Added by: BrowseBox(ABC)
  BIND('L_BI:Credited',L_BI:Credited)             ! Added by: BrowseBox(ABC)
  BIND('LI_BG:DI_ContainerNos',LI_BG:DI_ContainerNos) ! Added by: BrowseBox(ABC)
  BIND('L_SF:ClientRatePerKg',L_SF:ClientRatePerKg) ! Added by: BrowseBox(ABC)
  BIND('LOC:ClientRateType_Used',LOC:ClientRateType_Used) ! Added by: BrowseBox(ABC)
  BIND('L_P:Type_Q',L_P:Type_Q)                   ! Added by: BrowseBox(ABC)
  BIND('L_P:Status_Q',L_P:Status_Q)               ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(CLI:Record,History::CLI:Record)
  SELF.AddHistoryField(?CLI:ClientName,3)
  SELF.AddHistoryField(?CLI:OpsManager,12)
  SELF.AddHistoryField(?CLI:Status,27)
  SELF.AddHistoryField(?CLI:ClientNo,2)
  SELF.AddHistoryField(?CLI:CID,1)
  SELF.AddHistoryField(?CLI:VATNo,34)
  SELF.AddHistoryField(?CLI:DateOpened,39)
  SELF.AddHistoryField(?CLI:Terms,35)
  SELF.AddHistoryField(?CLI:InsuranceRequired,13)
  SELF.AddHistoryField(?CLI:InsuranceType,14)
  SELF.AddHistoryField(?CLI:InsurancePercent,15)
  SELF.AddHistoryField(?CLI:DocumentCharge,10)
  SELF.AddHistoryField(?CLI:MinimiumCharge,8)
  SELF.AddHistoryField(?CLI:FuelSurchargeActive,50)
  SELF.AddHistoryField(?CLI:TollChargeActive,54)
  SELF.AddHistoryField(?CLI:VolumetricRatio,22)
  SELF.AddHistoryField(?CLI:BID,5)
  SELF.AddHistoryField(?CLI:PaymentPeriod,21)
  SELF.AddHistoryField(?CLI:AccountLimit,36)
  SELF.AddHistoryField(?CLI:LiabilityCover:2,52)
  SELF.AddHistoryField(?CLI:LiabilityCrossBorder,53)
  SELF.AddHistoryField(?CLI:SalesRepEarnsUntil,32)
  SELF.AddHistoryField(?CLI:DeliveryNotes,24)
  SELF.AddHistoryField(?CLI:AdviceOfDispatch,23)
  SELF.AddHistoryField(?CLI:FaxConfirmation,28)
  SELF.AddHistoryField(?CLI:GenerateInvoice,7)
  SELF.AddHistoryField(?CLI:ACID,11)
  SELF.AddHistoryField(?CLI:OnInvoice,17)
  SELF.AddHistoryField(?CLI:Days30,18)
  SELF.AddHistoryField(?CLI:Days60,19)
  SELF.AddHistoryField(?CLI:Days90,20)
  SELF.AddHistoryField(?CLI:Notes,25)
  SELF.AddHistoryField(?CLI:InvoiceMessage,26)
  SELF.AddHistoryField(?CLI:PODMessage,51)
  SELF.AddUpdateFile(Access:Clients)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Accountants.SetOpenRelated()
  Relate:Accountants.Open                         ! File Accountants used by this procedure, so make sure it's RelationManager is open
  Relate:ClientsAlias.Open                        ! File ClientsAlias used by this procedure, so make sure it's RelationManager is open
  Relate:JourneysAlias.Open                       ! File JourneysAlias used by this procedure, so make sure it's RelationManager is open
  Relate:__RatesToll.Open                         ! File __RatesToll used by this procedure, so make sure it's RelationManager is open
  Access:Clients.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SalesReps.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__Rates.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ClientsRateTypes.UseFile                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:AddressContacts.UseFile                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Add_Suburbs.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Clients
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.SaveControl = ?Save
  SELF.DisableCancelButton = 1
  BRW_Statements.Init(?List:Statements,Queue:Browse:Statements.ViewPosition,BRW23::View:Browse,Queue:Browse:Statements,Relate:_Statements,SELF) ! Initialize the browse manager
  BRW_Invoices.Init(?List:Invoices,Queue:Browse:Invoices.ViewPosition,BRW26::View:Browse,Queue:Browse:Invoices,Relate:_Invoice,SELF) ! Initialize the browse manager
  BRW_Cont_Park_Discounts.Init(?List:ContainerPark,Queue:Browse:ContainerPark.ViewPosition,BRW11::View:Browse,Queue:Browse:ContainerPark,Relate:Clients_ContainerParkDiscounts,SELF) ! Initialize the browse manager
  BRW_AddCharge.Init(?List:AddCharge,Queue:Browse:AddCharge.ViewPosition,BRW29::View:Browse,Queue:Browse:AddCharge,Relate:__RatesAdditionalCharges,SELF) ! Initialize the browse manager
  BRW_Rates.Init(?List:Rates,Queue:Browse:Rates.ViewPosition,BRW33::View:Browse,Queue:Browse:Rates,Relate:__Rates,SELF) ! Initialize the browse manager
  BRW_RatesContainerPark.Init(?List:ContainerParkDiscount,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:__RatesContainerPark,SELF) ! Initialize the browse manager
  BRW_ClientRateTypes.Init(?List:ClientRateTypes,Queue:Browse:ClientRateTypes.ViewPosition,BRW6::View:Browse,Queue:Browse:ClientRateTypes,Relate:ClientsRateTypes,SELF) ! Initialize the browse manager
  BRW_ClientPayment.Init(?List:ClientPayment,Queue:Browse:ClientPayment.ViewPosition,BRW32::View:Browse,Queue:Browse:ClientPayment,Relate:ClientsPayments,SELF) ! Initialize the browse manager
  BRW40.Init(?List:9,Queue:Browse:9.ViewPosition,BRW40::View:Browse,Queue:Browse:9,Relate:Reminders,SELF) ! Initialize the browse manager
  BRW45.Init(?List_Email,Queue:Browse_EmailAddresses.ViewPosition,BRW45::View:Browse,Queue:Browse_EmailAddresses,Relate:EmailAddresses,SELF) ! Initialize the browse manager
  BRW_FuelCosts.Init(?List_FuelCosts,Queue:Browse_FuelCosts.ViewPosition,BRW49::View:Browse,Queue:Browse_FuelCosts,Relate:__RatesFuelCost,SELF) ! Initialize the browse manager
  BRW_ETolls.Init(?List,Queue:Browse.ViewPosition,BRW43::View:Browse,Queue:Browse,Relate:__RatesToll,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW_Statements.Q &= Queue:Browse:Statements
  BRW_Statements.AddSortOrder(,STA:FKey_CID)      ! Add the sort order for STA:FKey_CID for sort order 1
  BRW_Statements.AddRange(STA:CID,CLI:CID)        ! Add single value range limit for sort order 1
  BRW_Statements.AddLocator(BRW23::Sort0:Locator) ! Browse has a locator for sort order 1
  BRW23::Sort0:Locator.Init(,STA:CID,1,BRW_Statements) ! Initialize the browse locator using  using key: STA:FKey_CID , STA:CID
  BRW_Statements.AppendOrder('-STA:StatementDate,-STA:STID') ! Append an additional sort order
  BRW_Statements.AddField(STA:STID,BRW_Statements.Q.STA:STID) ! Field STA:STID is a hot field or requires assignment from browse
  BRW_Statements.AddField(STA:StatementDate,BRW_Statements.Q.STA:StatementDate) ! Field STA:StatementDate is a hot field or requires assignment from browse
  BRW_Statements.AddField(STA:StatementTime,BRW_Statements.Q.STA:StatementTime) ! Field STA:StatementTime is a hot field or requires assignment from browse
  BRW_Statements.AddField(STA:Current,BRW_Statements.Q.STA:Current) ! Field STA:Current is a hot field or requires assignment from browse
  BRW_Statements.AddField(STA:Days30,BRW_Statements.Q.STA:Days30) ! Field STA:Days30 is a hot field or requires assignment from browse
  BRW_Statements.AddField(STA:Days60,BRW_Statements.Q.STA:Days60) ! Field STA:Days60 is a hot field or requires assignment from browse
  BRW_Statements.AddField(STA:Days90,BRW_Statements.Q.STA:Days90) ! Field STA:Days90 is a hot field or requires assignment from browse
  BRW_Statements.AddField(STA:Total,BRW_Statements.Q.STA:Total) ! Field STA:Total is a hot field or requires assignment from browse
  BRW_Statements.AddField(STA:CID,BRW_Statements.Q.STA:CID) ! Field STA:CID is a hot field or requires assignment from browse
  BRW_Invoices.Q &= Queue:Browse:Invoices
  BRW_Invoices.AddSortOrder(,INV:FKey_CID)        ! Add the sort order for INV:FKey_CID for sort order 1
  BRW_Invoices.AddRange(INV:CID,CLI:CID)          ! Add single value range limit for sort order 1
  BRW_Invoices.AddLocator(BRW26::Sort0:Locator)   ! Browse has a locator for sort order 1
  BRW26::Sort0:Locator.Init(,INV:CID,1,BRW_Invoices) ! Initialize the browse locator using  using key: INV:FKey_CID , INV:CID
  BRW_Invoices.AppendOrder('-INV:IID')            ! Append an additional sort order
  BRW_Invoices.SetFilter('(LQ:Value = INV:Status OR LQ:Value = 150)') ! Apply filter expression to browse
  BRW_Invoices.AddResetField(LI_BG:Change_Indicator) ! Apply the reset field
  BRW_Invoices.AddResetField(L_BI:Deliveries_Commodities_Packaging) ! Apply the reset field
  BRW_Invoices.AddField(INV:IID,BRW_Invoices.Q.INV:IID) ! Field INV:IID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:DINo,BRW_Invoices.Q.INV:DINo) ! Field INV:DINo is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:InvoiceDate,BRW_Invoices.Q.INV:InvoiceDate) ! Field INV:InvoiceDate is a hot field or requires assignment from browse
  BRW_Invoices.AddField(L_BI:Outstanding,BRW_Invoices.Q.L_BI:Outstanding) ! Field L_BI:Outstanding is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:Total,BRW_Invoices.Q.INV:Total) ! Field INV:Total is a hot field or requires assignment from browse
  BRW_Invoices.AddField(L_BI:Payments,BRW_Invoices.Q.L_BI:Payments) ! Field L_BI:Payments is a hot field or requires assignment from browse
  BRW_Invoices.AddField(L_BI:Credited,BRW_Invoices.Q.L_BI:Credited) ! Field L_BI:Credited is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:Weight,BRW_Invoices.Q.INV:Weight) ! Field INV:Weight is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:ClientReference,BRW_Invoices.Q.INV:ClientReference) ! Field INV:ClientReference is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:ShipperName,BRW_Invoices.Q.INV:ShipperName) ! Field INV:ShipperName is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:ConsigneeName,BRW_Invoices.Q.INV:ConsigneeName) ! Field INV:ConsigneeName is a hot field or requires assignment from browse
  BRW_Invoices.AddField(LI_BG:DI_ContainerNos,BRW_Invoices.Q.LI_BG:DI_ContainerNos) ! Field LI_BG:DI_ContainerNos is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:CR_IID,BRW_Invoices.Q.INV:CR_IID) ! Field INV:CR_IID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:MIDs,BRW_Invoices.Q.INV:MIDs) ! Field INV:MIDs is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:CID,BRW_Invoices.Q.INV:CID) ! Field INV:CID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:BID,BRW_Invoices.Q.INV:BID) ! Field INV:BID is a hot field or requires assignment from browse
  BRW_Invoices.AddField(INV:Status,BRW_Invoices.Q.INV:Status) ! Field INV:Status is a hot field or requires assignment from browse
  BRW_Cont_Park_Discounts.Q &= Queue:Browse:ContainerPark
  BRW_Cont_Park_Discounts.AddSortOrder(,CLI_CP:FKey_CID) ! Add the sort order for CLI_CP:FKey_CID for sort order 1
  BRW_Cont_Park_Discounts.AddRange(CLI_CP:CID,CLI:CID) ! Add single value range limit for sort order 1
  BRW_Cont_Park_Discounts.AddLocator(BRW11::Sort0:Locator) ! Browse has a locator for sort order 1
  BRW11::Sort0:Locator.Init(,CLI_CP:CID,1,BRW_Cont_Park_Discounts) ! Initialize the browse locator using  using key: CLI_CP:FKey_CID , CLI_CP:CID
  BRW_Cont_Park_Discounts.AppendOrder('-CLI_CP:Effective_Date,+FLO:Floor') ! Append an additional sort order
  BRW_Cont_Park_Discounts.AddField(FLO:Floor,BRW_Cont_Park_Discounts.Q.FLO:Floor) ! Field FLO:Floor is a hot field or requires assignment from browse
  BRW_Cont_Park_Discounts.AddField(CLI_CP:Effective_Date,BRW_Cont_Park_Discounts.Q.CLI_CP:Effective_Date) ! Field CLI_CP:Effective_Date is a hot field or requires assignment from browse
  BRW_Cont_Park_Discounts.AddField(CLI_CP:ContainerParkRateDiscount,BRW_Cont_Park_Discounts.Q.CLI_CP:ContainerParkRateDiscount) ! Field CLI_CP:ContainerParkRateDiscount is a hot field or requires assignment from browse
  BRW_Cont_Park_Discounts.AddField(CLI_CP:ContainerParkMinimium,BRW_Cont_Park_Discounts.Q.CLI_CP:ContainerParkMinimium) ! Field CLI_CP:ContainerParkMinimium is a hot field or requires assignment from browse
  BRW_Cont_Park_Discounts.AddField(CLI_CP:CPDID,BRW_Cont_Park_Discounts.Q.CLI_CP:CPDID) ! Field CLI_CP:CPDID is a hot field or requires assignment from browse
  BRW_Cont_Park_Discounts.AddField(CLI_CP:CID,BRW_Cont_Park_Discounts.Q.CLI_CP:CID) ! Field CLI_CP:CID is a hot field or requires assignment from browse
  BRW_Cont_Park_Discounts.AddField(FLO:FID,BRW_Cont_Park_Discounts.Q.FLO:FID) ! Field FLO:FID is a hot field or requires assignment from browse
  BRW_AddCharge.Q &= Queue:Browse:AddCharge
  BRW_AddCharge.AddSortOrder(,CARA:FKey_CID)      ! Add the sort order for CARA:FKey_CID for sort order 1
  BRW_AddCharge.AddRange(CARA:CID,CLI:CID)        ! Add single value range limit for sort order 1
  BRW_AddCharge.AddLocator(BRW29::Sort0:Locator)  ! Browse has a locator for sort order 1
  BRW29::Sort0:Locator.Init(,CARA:CID,1,BRW_AddCharge) ! Initialize the browse locator using  using key: CARA:FKey_CID , CARA:CID
  BRW_AddCharge.AppendOrder('+ACCA:Description,-CARA:Effective_Date') ! Append an additional sort order
  BRW_AddCharge.AddField(ACCA:Description,BRW_AddCharge.Q.ACCA:Description) ! Field ACCA:Description is a hot field or requires assignment from browse
  BRW_AddCharge.AddField(CARA:Charge,BRW_AddCharge.Q.CARA:Charge) ! Field CARA:Charge is a hot field or requires assignment from browse
  BRW_AddCharge.AddField(CARA:Effective_Date,BRW_AddCharge.Q.CARA:Effective_Date) ! Field CARA:Effective_Date is a hot field or requires assignment from browse
  BRW_AddCharge.AddField(CARA:ACID,BRW_AddCharge.Q.CARA:ACID) ! Field CARA:ACID is a hot field or requires assignment from browse
  BRW_AddCharge.AddField(CARA:CID,BRW_AddCharge.Q.CARA:CID) ! Field CARA:CID is a hot field or requires assignment from browse
  BRW_AddCharge.AddField(ACCA:ACCID,BRW_AddCharge.Q.ACCA:ACCID) ! Field ACCA:ACCID is a hot field or requires assignment from browse
  BRW_Rates.Q &= Queue:Browse:Rates
  BRW_Rates.FileLoaded = 1                        ! This is a 'file loaded' browse
  BRW_Rates.AddSortOrder(,RAT:FKey_CID)           ! Add the sort order for RAT:FKey_CID for sort order 1
  BRW_Rates.AddRange(RAT:CID,CLI:CID)             ! Add single value range limit for sort order 1
  BRW_Rates.AddLocator(BRW33::Sort0:Locator)      ! Browse has a locator for sort order 1
  BRW33::Sort0:Locator.Init(,RAT:CID,1,BRW_Rates) ! Initialize the browse locator using  using key: RAT:FKey_CID , RAT:CID
  BRW_Rates.AppendOrder('-RAT:Effective_Date,+LOAD2:LoadType,+JOU:Journey,+CRT:ClientRateType,+RAT:ToMass,+RAT:RID') ! Append an additional sort order
  BRW_Rates.SetFilter('((L_LG:LTID = 0 OR L_LG:LTID = RAT:LTID) AND (L_JG:JID = 0 OR L_JG:JID = RAT:JID))') ! Apply filter expression to browse
  BRW_Rates.AddResetField(LOC:Changed_Indicator)  ! Apply the reset field
  BRW_Rates.AddResetField(L_JG:JID)               ! Apply the reset field
  BRW_Rates.AddResetField(L_LG:LTID)              ! Apply the reset field
  ?List:Rates{PROP:IconList,1} = '~checkoffdim.ico'
  ?List:Rates{PROP:IconList,2} = '~checkon.ico'
  BRW_Rates.AddField(CRT:ClientRateType,BRW_Rates.Q.CRT:ClientRateType) ! Field CRT:ClientRateType is a hot field or requires assignment from browse
  BRW_Rates.AddField(LOAD2:LoadType,BRW_Rates.Q.LOAD2:LoadType) ! Field LOAD2:LoadType is a hot field or requires assignment from browse
  BRW_Rates.AddField(JOU:Journey,BRW_Rates.Q.JOU:Journey) ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:ToMass,BRW_Rates.Q.RAT:ToMass) ! Field RAT:ToMass is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:RatePerKg,BRW_Rates.Q.RAT:RatePerKg) ! Field RAT:RatePerKg is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:MinimiumCharge,BRW_Rates.Q.RAT:MinimiumCharge) ! Field RAT:MinimiumCharge is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:Effective_Date,BRW_Rates.Q.RAT:Effective_Date) ! Field RAT:Effective_Date is a hot field or requires assignment from browse
  BRW_Rates.AddField(CTYP:ContainerType,BRW_Rates.Q.CTYP:ContainerType) ! Field CTYP:ContainerType is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:AdHoc,BRW_Rates.Q.RAT:AdHoc) ! Field RAT:AdHoc is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:Added_Date,BRW_Rates.Q.RAT:Added_Date) ! Field RAT:Added_Date is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:Added_Time,BRW_Rates.Q.RAT:Added_Time) ! Field RAT:Added_Time is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:RID,BRW_Rates.Q.RAT:RID) ! Field RAT:RID is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:JID,BRW_Rates.Q.RAT:JID) ! Field RAT:JID is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:LTID,BRW_Rates.Q.RAT:LTID) ! Field RAT:LTID is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:CRTID,BRW_Rates.Q.RAT:CRTID) ! Field RAT:CRTID is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:CTID,BRW_Rates.Q.RAT:CTID) ! Field RAT:CTID is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:RUBID,BRW_Rates.Q.RAT:RUBID) ! Field RAT:RUBID is a hot field or requires assignment from browse
  BRW_Rates.AddField(RAT:CID,BRW_Rates.Q.RAT:CID) ! Field RAT:CID is a hot field or requires assignment from browse
  BRW_Rates.AddField(CTYP:CTID,BRW_Rates.Q.CTYP:CTID) ! Field CTYP:CTID is a hot field or requires assignment from browse
  BRW_Rates.AddField(CRT:CRTID,BRW_Rates.Q.CRT:CRTID) ! Field CRT:CRTID is a hot field or requires assignment from browse
  BRW_Rates.AddField(LOAD2:LTID,BRW_Rates.Q.LOAD2:LTID) ! Field LOAD2:LTID is a hot field or requires assignment from browse
  BRW_Rates.AddField(JOU:JID,BRW_Rates.Q.JOU:JID) ! Field JOU:JID is a hot field or requires assignment from browse
  BRW_RatesContainerPark.Q &= Queue:Browse:2
  BRW_RatesContainerPark.AddSortOrder(,CPRA:CKey_FID_JID_EffDate_ToMass) ! Add the sort order for CPRA:CKey_FID_JID_EffDate_ToMass for sort order 1
  BRW_RatesContainerPark.AddRange(CPRA:FID,CLI_CP:FID) ! Add single value range limit for sort order 1
  BRW_RatesContainerPark.AddLocator(BRW2::Sort0:Locator) ! Browse has a locator for sort order 1
  BRW2::Sort0:Locator.Init(,CPRA:JID,1,BRW_RatesContainerPark) ! Initialize the browse locator using  using key: CPRA:CKey_FID_JID_EffDate_ToMass , CPRA:JID
  BRW_RatesContainerPark.AddResetField(CLI_CP:FID) ! Apply the reset field
  BRW_RatesContainerPark.AddField(JOU:Journey,BRW_RatesContainerPark.Q.JOU:Journey) ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW_RatesContainerPark.AddField(CPRA:ToMass,BRW_RatesContainerPark.Q.CPRA:ToMass) ! Field CPRA:ToMass is a hot field or requires assignment from browse
  BRW_RatesContainerPark.AddField(CPRA:RatePerKg,BRW_RatesContainerPark.Q.CPRA:RatePerKg) ! Field CPRA:RatePerKg is a hot field or requires assignment from browse
  BRW_RatesContainerPark.AddField(L_SF:ClientRatePerKg,BRW_RatesContainerPark.Q.L_SF:ClientRatePerKg) ! Field L_SF:ClientRatePerKg is a hot field or requires assignment from browse
  BRW_RatesContainerPark.AddField(CPRA:Effective_Date,BRW_RatesContainerPark.Q.CPRA:Effective_Date) ! Field CPRA:Effective_Date is a hot field or requires assignment from browse
  BRW_RatesContainerPark.AddField(CPRA:CPID,BRW_RatesContainerPark.Q.CPRA:CPID) ! Field CPRA:CPID is a hot field or requires assignment from browse
  BRW_RatesContainerPark.AddField(CPRA:FID,BRW_RatesContainerPark.Q.CPRA:FID) ! Field CPRA:FID is a hot field or requires assignment from browse
  BRW_RatesContainerPark.AddField(CPRA:JID,BRW_RatesContainerPark.Q.CPRA:JID) ! Field CPRA:JID is a hot field or requires assignment from browse
  BRW_RatesContainerPark.AddField(JOU:JID,BRW_RatesContainerPark.Q.JOU:JID) ! Field JOU:JID is a hot field or requires assignment from browse
  BRW_ClientRateTypes.Q &= Queue:Browse:ClientRateTypes
  BRW_ClientRateTypes.AddSortOrder(,CRT:FKey_CID) ! Add the sort order for CRT:FKey_CID for sort order 1
  BRW_ClientRateTypes.AddRange(CRT:CID,CLI:CID)   ! Add single value range limit for sort order 1
  BRW_ClientRateTypes.AddLocator(BRW6::Sort0:Locator) ! Browse has a locator for sort order 1
  BRW6::Sort0:Locator.Init(,CRT:CID,1,BRW_ClientRateTypes) ! Initialize the browse locator using  using key: CRT:FKey_CID , CRT:CID
  BRW_ClientRateTypes.AppendOrder('+LOAD2:LoadType,+CRT:ClientRateType') ! Append an additional sort order
  BRW_ClientRateTypes.AddField(CRT:ClientRateType,BRW_ClientRateTypes.Q.CRT:ClientRateType) ! Field CRT:ClientRateType is a hot field or requires assignment from browse
  BRW_ClientRateTypes.AddField(LOAD2:LoadType,BRW_ClientRateTypes.Q.LOAD2:LoadType) ! Field LOAD2:LoadType is a hot field or requires assignment from browse
  BRW_ClientRateTypes.AddField(LOC:ClientRateType_Used,BRW_ClientRateTypes.Q.LOC:ClientRateType_Used) ! Field LOC:ClientRateType_Used is a hot field or requires assignment from browse
  BRW_ClientRateTypes.AddField(CRT:CRTID,BRW_ClientRateTypes.Q.CRT:CRTID) ! Field CRT:CRTID is a hot field or requires assignment from browse
  BRW_ClientRateTypes.AddField(CRT:CID,BRW_ClientRateTypes.Q.CRT:CID) ! Field CRT:CID is a hot field or requires assignment from browse
  BRW_ClientRateTypes.AddField(LOAD2:LTID,BRW_ClientRateTypes.Q.LOAD2:LTID) ! Field LOAD2:LTID is a hot field or requires assignment from browse
  BRW_ClientPayment.Q &= Queue:Browse:ClientPayment
  BRW_ClientPayment.AddSortOrder(,CLIP:FKey_CID)  ! Add the sort order for CLIP:FKey_CID for sort order 1
  BRW_ClientPayment.AddRange(CLIP:CID,CLI:CID)    ! Add single value range limit for sort order 1
  BRW_ClientPayment.AddLocator(BRW32::Sort0:Locator) ! Browse has a locator for sort order 1
  BRW32::Sort0:Locator.Init(,CLIP:CID,1,BRW_ClientPayment) ! Initialize the browse locator using  using key: CLIP:FKey_CID , CLIP:CID
  BRW_ClientPayment.AppendOrder('-CLIP:DateMade,+CLIP:CPID') ! Append an additional sort order
  BRW_ClientPayment.SetFilter('(L_P:Status_Filter = -1 OR CLIP:Status = L_P:Status_Filter)') ! Apply filter expression to browse
  BRW_ClientPayment.AddResetField(L_P:Status_Filter) ! Apply the reset field
  ?List:ClientPayment{PROP:IconList,1} = '~checkoffdim.ico'
  ?List:ClientPayment{PROP:IconList,2} = '~checkon.ico'
  BRW_ClientPayment.AddField(CLIP:DateMade,BRW_ClientPayment.Q.CLIP:DateMade) ! Field CLIP:DateMade is a hot field or requires assignment from browse
  BRW_ClientPayment.AddField(CLIP:Amount,BRW_ClientPayment.Q.CLIP:Amount) ! Field CLIP:Amount is a hot field or requires assignment from browse
  BRW_ClientPayment.AddField(L_P:Type_Q,BRW_ClientPayment.Q.L_P:Type_Q) ! Field L_P:Type_Q is a hot field or requires assignment from browse
  BRW_ClientPayment.AddField(L_P:Status_Q,BRW_ClientPayment.Q.L_P:Status_Q) ! Field L_P:Status_Q is a hot field or requires assignment from browse
  BRW_ClientPayment.AddField(CLIP:DateCaptured,BRW_ClientPayment.Q.CLIP:DateCaptured) ! Field CLIP:DateCaptured is a hot field or requires assignment from browse
  BRW_ClientPayment.AddField(CLIP:Notes,BRW_ClientPayment.Q.CLIP:Notes) ! Field CLIP:Notes is a hot field or requires assignment from browse
  BRW_ClientPayment.AddField(CLIP:CPID,BRW_ClientPayment.Q.CLIP:CPID) ! Field CLIP:CPID is a hot field or requires assignment from browse
  BRW_ClientPayment.AddField(CLIP:StatusUpToDate,BRW_ClientPayment.Q.CLIP:StatusUpToDate) ! Field CLIP:StatusUpToDate is a hot field or requires assignment from browse
  BRW_ClientPayment.AddField(CLIP:CPID_Reversal,BRW_ClientPayment.Q.CLIP:CPID_Reversal) ! Field CLIP:CPID_Reversal is a hot field or requires assignment from browse
  BRW_ClientPayment.AddField(CLIP:Type,BRW_ClientPayment.Q.CLIP:Type) ! Field CLIP:Type is a hot field or requires assignment from browse
  BRW_ClientPayment.AddField(CLIP:Status,BRW_ClientPayment.Q.CLIP:Status) ! Field CLIP:Status is a hot field or requires assignment from browse
  BRW_ClientPayment.AddField(CLIP:CID,BRW_ClientPayment.Q.CLIP:CID) ! Field CLIP:CID is a hot field or requires assignment from browse
  BRW40.Q &= Queue:Browse:9
  BRW40.AddSortOrder(,REM:Key_ID)                 ! Add the sort order for REM:Key_ID for sort order 1
  BRW40.AddRange(REM:ID,CLI:CID)                  ! Add single value range limit for sort order 1
  BRW40.AddLocator(BRW40::Sort0:Locator)          ! Browse has a locator for sort order 1
  BRW40::Sort0:Locator.Init(,REM:ID,1,BRW40)      ! Initialize the browse locator using  using key: REM:Key_ID , REM:ID
  BRW40.AppendOrder('-REM:ReminderDate,+REM:RID') ! Append an additional sort order
  BRW40.SetFilter('(L_RG:Show_Active = 0 OR (L_RG:Show_Active = 1 AND REM:Active = 1) OR (L_RG:Show_Active = 0 AND REM:Active = 0))') ! Apply filter expression to browse
  ?List:9{PROP:IconList,1} = '~checkoffdim.ico'
  ?List:9{PROP:IconList,2} = '~checkon.ico'
  BRW40.AddField(REM:RID,BRW40.Q.REM:RID)         ! Field REM:RID is a hot field or requires assignment from browse
  BRW40.AddField(REM:ReminderDate,BRW40.Q.REM:ReminderDate) ! Field REM:ReminderDate is a hot field or requires assignment from browse
  BRW40.AddField(REM:ReminderTime,BRW40.Q.REM:ReminderTime) ! Field REM:ReminderTime is a hot field or requires assignment from browse
  BRW40.AddField(REM:Active,BRW40.Q.REM:Active)   ! Field REM:Active is a hot field or requires assignment from browse
  BRW40.AddField(REM:Popup,BRW40.Q.REM:Popup)     ! Field REM:Popup is a hot field or requires assignment from browse
  BRW40.AddField(REM:Notes,BRW40.Q.REM:Notes)     ! Field REM:Notes is a hot field or requires assignment from browse
  BRW40.AddField(USE:Login,BRW40.Q.USE:Login)     ! Field USE:Login is a hot field or requires assignment from browse
  BRW40.AddField(USEG:GroupName,BRW40.Q.USEG:GroupName) ! Field USEG:GroupName is a hot field or requires assignment from browse
  BRW40.AddField(REM:ID,BRW40.Q.REM:ID)           ! Field REM:ID is a hot field or requires assignment from browse
  BRW40.AddField(USEG:UGID,BRW40.Q.USEG:UGID)     ! Field USEG:UGID is a hot field or requires assignment from browse
  BRW40.AddField(USE:UID,BRW40.Q.USE:UID)         ! Field USE:UID is a hot field or requires assignment from browse
  BRW45.Q &= Queue:Browse_EmailAddresses
  BRW45.FileLoaded = 1                            ! This is a 'file loaded' browse
  BRW45.AddSortOrder(,EMAI:FKey_CID)              ! Add the sort order for EMAI:FKey_CID for sort order 1
  BRW45.AddRange(EMAI:CID,CLI:CID)                ! Add single value range limit for sort order 1
  BRW45.AddLocator(BRW45::Sort0:Locator)          ! Browse has a locator for sort order 1
  BRW45::Sort0:Locator.Init(,EMAI:CID,1,BRW45)    ! Initialize the browse locator using  using key: EMAI:FKey_CID , EMAI:CID
  BRW45.AppendOrder('+EMAI:EmailName')            ! Append an additional sort order
  ?List_Email{PROP:IconList,1} = '~checkoffdim.ico'
  ?List_Email{PROP:IconList,2} = '~checkon.ico'
  BRW45.AddField(EMAI:EmailName,BRW45.Q.EMAI:EmailName) ! Field EMAI:EmailName is a hot field or requires assignment from browse
  BRW45.AddField(EMAI:EmailAddress,BRW45.Q.EMAI:EmailAddress) ! Field EMAI:EmailAddress is a hot field or requires assignment from browse
  BRW45.AddField(EMAI:RateLetter,BRW45.Q.EMAI:RateLetter) ! Field EMAI:RateLetter is a hot field or requires assignment from browse
  BRW45.AddField(EMAI:DefaultAddress,BRW45.Q.EMAI:DefaultAddress) ! Field EMAI:DefaultAddress is a hot field or requires assignment from browse
  BRW45.AddField(EMAI:Operations,BRW45.Q.EMAI:Operations) ! Field EMAI:Operations is a hot field or requires assignment from browse
  BRW45.AddField(EMAI:DefaultOnDI,BRW45.Q.EMAI:DefaultOnDI) ! Field EMAI:DefaultOnDI is a hot field or requires assignment from browse
  BRW45.AddField(EMAI:OperationsReference,BRW45.Q.EMAI:OperationsReference) ! Field EMAI:OperationsReference is a hot field or requires assignment from browse
  BRW45.AddField(EMAI:EAID,BRW45.Q.EMAI:EAID)     ! Field EMAI:EAID is a hot field or requires assignment from browse
  BRW45.AddField(EMAI:CID,BRW45.Q.EMAI:CID)       ! Field EMAI:CID is a hot field or requires assignment from browse
  BRW_FuelCosts.Q &= Queue:Browse_FuelCosts
  BRW_FuelCosts.AddSortOrder(,FCRA:FKey_CID)      ! Add the sort order for FCRA:FKey_CID for sort order 1
  BRW_FuelCosts.AddRange(FCRA:CID,L_RFC:CID)      ! Add single value range limit for sort order 1
  BRW_FuelCosts.AddLocator(BRW49::Sort0:Locator)  ! Browse has a locator for sort order 1
  BRW49::Sort0:Locator.Init(,FCRA:CID,1,BRW_FuelCosts) ! Initialize the browse locator using  using key: FCRA:FKey_CID , FCRA:CID
  BRW_FuelCosts.AppendOrder('-FCRA:Effective_Date,+FCRA:RFCID') ! Append an additional sort order
  BRW_FuelCosts.AddField(FCRA:Effective_Date,BRW_FuelCosts.Q.FCRA:Effective_Date) ! Field FCRA:Effective_Date is a hot field or requires assignment from browse
  BRW_FuelCosts.AddField(FCRA:FuelCost,BRW_FuelCosts.Q.FCRA:FuelCost) ! Field FCRA:FuelCost is a hot field or requires assignment from browse
  BRW_FuelCosts.AddField(FCRA:FuelBaseRate,BRW_FuelCosts.Q.FCRA:FuelBaseRate) ! Field FCRA:FuelBaseRate is a hot field or requires assignment from browse
  BRW_FuelCosts.AddField(FCRA:FuelSurcharge,BRW_FuelCosts.Q.FCRA:FuelSurcharge) ! Field FCRA:FuelSurcharge is a hot field or requires assignment from browse
  BRW_FuelCosts.AddField(FCRA:RFCID,BRW_FuelCosts.Q.FCRA:RFCID) ! Field FCRA:RFCID is a hot field or requires assignment from browse
  BRW_FuelCosts.AddField(FCRA:CID,BRW_FuelCosts.Q.FCRA:CID) ! Field FCRA:CID is a hot field or requires assignment from browse
  BRW_FuelCosts.AddField(FUE:FCID,BRW_FuelCosts.Q.FUE:FCID) ! Field FUE:FCID is a hot field or requires assignment from browse
  BRW_ETolls.Q &= Queue:Browse
  BRW_ETolls.AddSortOrder(,TOL:FKey_CID)          ! Add the sort order for TOL:FKey_CID for sort order 1
  BRW_ETolls.AddRange(TOL:CID,EToll_CID)          ! Add single value range limit for sort order 1
  BRW_ETolls.AddLocator(BRW43::Sort0:Locator)     ! Browse has a locator for sort order 1
  BRW43::Sort0:Locator.Init(,TOL:CID,1,BRW_ETolls) ! Initialize the browse locator using  using key: TOL:FKey_CID , TOL:CID
  BRW_ETolls.AppendOrder('-TOL:Effective_DateAndTime') ! Append an additional sort order
  BRW_ETolls.AddField(TOL:CID,BRW_ETolls.Q.TOL:CID) ! Field TOL:CID is a hot field or requires assignment from browse
  BRW_ETolls.AddField(TOL:Effective_Date,BRW_ETolls.Q.TOL:Effective_Date) ! Field TOL:Effective_Date is a hot field or requires assignment from browse
  BRW_ETolls.AddField(TOL:TollRate,BRW_ETolls.Q.TOL:TollRate) ! Field TOL:TollRate is a hot field or requires assignment from browse
  BRW_ETolls.AddField(TOL:TRID,BRW_ETolls.Q.TOL:TRID) ! Field TOL:TRID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_Clients',QuickWindow)      ! Restore window settings from non-volatile store
      ! p:Option
      !   0   = normal
      !   1   = rates
      !   2   = Emails
      IF p:Option <> 0
         DISABLE(?Save)
        
         HIDE(?Tab_General)
         !HIDE(?Tab_Deliveries)
         HIDE(?Tab_AdditionalNotes)
         HIDE(?Tab_Invoices)
         HIDE(?Tab_Statements)
         HIDE(?Tab_Payments)
         HIDE(?Tab_Ratess)
         HIDE(?Tab_EmailAddReminders)
  
         CASE p:Option
         OF 1
              UNHIDE(?Tab_Ratess)
              UNHIDE(?Tab_EmailAddReminders)
              SELECT(?Tab_Ratess)
         OF 2
            UNHIDE(?Tab_EmailAddReminders)
      .  .
     L_P:Status_Filter = GETINI('Update_Clients', 'L_P:Status_Filter', -1, GLO:Local_INI)
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      DO Load_Address
  
  
      SAL:SRID                = CLI:SRID
      IF Access:SalesReps.TryFetch(SAL:PKey_SRID) = LEVEL:Benign
         LOC:SalesRep         = SAL:SalesRep
      .
  
  
      ACCO:ACID               = CLI:ACID
      IF Access:Accountants.TryFetch(ACCO:PKey_ACID) = LEVEL:Benign
         LOC:AccountantName   = ACCO:AccountantName
      .
  BRW_Statements.AskProcedure = 4                 ! Will call: Update_Statements
  BRW_Invoices.AskProcedure = 5                   ! Will call: Update_Invoice
  BRW_Cont_Park_Discounts.AskProcedure = 6        ! Will call: Update_Clients_ContainerParkRates
  BRW_AddCharge.AskProcedure = 7                  ! Will call: Update_AdditionalCharges
  BRW_Rates.AskProcedure = 8                      ! Will call: Update_Rates(L_LG:LTID, 1, L_JG:JID)
  BRW_ClientRateTypes.AskProcedure = 9            ! Will call: Update_ClientsRateTypes
  BRW_ClientPayment.AskProcedure = 10             ! Will call: Update_ClientsPayments
  BRW40.AskProcedure = 11                         ! Will call: Update_Reminders(1, CLI:CID)
  BRW45.AskProcedure = 12                         ! Will call: Update_Email_Addresses(p:Option)
  BRW_FuelCosts.AskProcedure = 13                 ! Will call: Update_RatesFuelCost
  BRW_ETolls.AskProcedure = 14                    ! Will call: Update_EToll
  FDB16.Init(?BRA:BranchName,Queue:FileDrop.ViewPosition,FDB16::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB16.Q &= Queue:FileDrop
  FDB16.AddSortOrder(BRA:Key_BranchName)
  FDB16.AddField(BRA:BranchName,FDB16.Q.BRA:BranchName) !List box control field - type derived from field
  FDB16.AddField(BRA:BID,FDB16.Q.BRA:BID) !Primary key field - type derived from field
  FDB16.AddUpdateField(BRA:BID,CLI:BID)
  ThisWindow.AddItem(FDB16.WindowComponent)
  FDB22.Init(?L_JG:Journey,Queue:FileDrop:1.ViewPosition,FDB22::View:FileDrop,Queue:FileDrop:1,Relate:JourneysAlias,ThisWindow)
  FDB22.Q &= Queue:FileDrop:1
  FDB22.AddSortOrder(A_JOU:Key_Journey)
  FDB22.AddField(A_JOU:Journey,FDB22.Q.A_JOU:Journey) !List box control field - type derived from field
  FDB22.AddField(A_JOU:JID,FDB22.Q.A_JOU:JID) !List box control field - type derived from field
  FDB22.AddUpdateField(A_JOU:JID,L_JG:JID)
  ThisWindow.AddItem(FDB22.WindowComponent)
  FDB22.DefaultFill = 0
  FDB4.Init(?L_LG:LoadType,Queue:FileDrop:3.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop:3,Relate:LoadTypes2,ThisWindow)
  FDB4.Q &= Queue:FileDrop:3
  FDB4.AddSortOrder(LOAD2:Key_LoadType)
  FDB4.AddField(LOAD2:LoadType,FDB4.Q.LOAD2:LoadType) !List box control field - type derived from field
  FDB4.AddField(LOAD2:LTID,FDB4.Q.LOAD2:LTID) !Primary key field - type derived from field
  FDB4.AddUpdateField(LOAD2:LTID,L_LG:LTID)
  ThisWindow.AddItem(FDB4.WindowComponent)
  FDB4.DefaultFill = 0
  FDB5.Init(?ADDC:ContactName,Queue:FileDrop:2.ViewPosition,FDB5::View:FileDrop,Queue:FileDrop:2,Relate:AddressContacts,ThisWindow)
  FDB5.Q &= Queue:FileDrop:2
  FDB5.AddSortOrder(ADDC:FKey_AID)
  FDB5.AppendOrder('ADDC:ContactName')
  FDB5.AppendOrder('ADD:AddressName')
  FDB5.AppendOrder('')
  FDB5.AppendOrder('ADDC:ACID')
  FDB5.AddRange(ADDC:AID,CLI:AID)
  FDB5.AddField(ADDC:ContactName,FDB5.Q.ADDC:ContactName) !List box control field - type derived from field
  FDB5.AddField(ADDC:PrimaryContact,FDB5.Q.ADDC:PrimaryContact) !List box control field - type derived from field
  FDB5.AddField(ADDC:ACID,FDB5.Q.ADDC:ACID) !Primary key field - type derived from field
  ThisWindow.AddItem(FDB5.WindowComponent)
  ?ADDC:ContactName{PROP:IconList,1} = '~checkoffdim.ico'
  BRW_Statements.AddToolbarTarget(Toolbar)        ! Browse accepts toolbar control
  BRW_Invoices.AddToolbarTarget(Toolbar)          ! Browse accepts toolbar control
  BRW_Cont_Park_Discounts.AddToolbarTarget(Toolbar) ! Browse accepts toolbar control
  BRW_AddCharge.AddToolbarTarget(Toolbar)         ! Browse accepts toolbar control
  BRW_Rates.AddToolbarTarget(Toolbar)             ! Browse accepts toolbar control
  BRW_RatesContainerPark.AddToolbarTarget(Toolbar) ! Browse accepts toolbar control
  BRW_ClientRateTypes.AddToolbarTarget(Toolbar)   ! Browse accepts toolbar control
  BRW_ClientPayment.AddToolbarTarget(Toolbar)     ! Browse accepts toolbar control
  BRW40.AddToolbarTarget(Toolbar)                 ! Browse accepts toolbar control
  BRW45.AddToolbarTarget(Toolbar)                 ! Browse accepts toolbar control
  BRW_FuelCosts.AddToolbarTarget(Toolbar)         ! Browse accepts toolbar control
      CASE SELF.Request
      OF InsertRecord
         DISABLE(?Tab_Ratess)
         !DISABLE(?Tab_Deliveries)
         DISABLE(?Tab_Invoices)
         DISABLE(?Tab_Statements)
  
         DO Get_New_Client_No
      OF ViewRecord                       ! Rates mode is view only
         DISABLE(?Group_Rates)
         DISABLE(?Button_DupRates_Rate)
         DISABLE(?Group_ContainerPark)
         DISABLE(?Group_AddCharges)
         DISABLE(?Group_ClientRateTypes)
  
          DISABLE(?Group_FuelCosts)        
          
          HIDE(?Unlock)
      .
  !    ! p:Option           *** moved up to before display of window
  !    !   0   = normal
  !    !   1   = rates
  !    !   2   = Emails
  !    IF p:Option <> 0
  !       DISABLE(?Save)
  !      
  !       HIDE(?Tab_General)
  !       HIDE(?Tab_Deliveries)
  !       HIDE(?Tab_AdditionalNotes)
  !       HIDE(?Tab_Invoices)
  !       HIDE(?Tab_Statements)
  !       HIDE(?Tab_Payments)
  !       HIDE(?Tab_Ratess)
  !       HIDE(?Tab_EmailAddReminders)
  !
  !       CASE p:Option
  !       OF 1
  !          UNHIDE(?Tab_Ratess)
  !       OF 2
  !          UNHIDE(?Tab_EmailAddReminders)
  !    .  .
      DO Tag_Popup
      LOC:Previous_FuelSurcharge  = CLI:FuelSurchargeActive
  
      IF LOC:Previous_FuelSurcharge = FALSE
      !   DISABLE(?Group_FuelSur_Buttons)
      .
  BRW23::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW23::FormatManager.Init('MANTRNIS','Update_Clients',1,?List:Statements,23,BRW23::PopupTextExt,Queue:Browse:Statements,8,LFM_CFile,LFM_CFile.Record)
  BRW23::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW11::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW11::FormatManager.Init('MANTRNIS','Update_Clients',1,?List:ContainerPark,11,BRW11::PopupTextExt,Queue:Browse:ContainerPark,4,LFM_CFile,LFM_CFile.Record)
  BRW11::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW29::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW29::FormatManager.Init('MANTRNIS','Update_Clients',1,?List:AddCharge,29,BRW29::PopupTextExt,Queue:Browse:AddCharge,3,LFM_CFile,LFM_CFile.Record)
  BRW29::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW33::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW33::FormatManager.Init('MANTRNIS','Update_Clients',1,?List:Rates,33,BRW33::PopupTextExt,Queue:Browse:Rates,18,LFM_CFile,LFM_CFile.Record)
  BRW33::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_Clients',1,?List:ContainerParkDiscount,2,BRW2::PopupTextExt,Queue:Browse:2,5,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW6::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW6::FormatManager.Init('MANTRNIS','Update_Clients',1,?List:ClientRateTypes,6,BRW6::PopupTextExt,Queue:Browse:ClientRateTypes,4,LFM_CFile,LFM_CFile.Record)
  BRW6::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW32::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW32::FormatManager.Init('MANTRNIS','Update_Clients',1,?List:ClientPayment,32,BRW32::PopupTextExt,Queue:Browse:ClientPayment,10,LFM_CFile,LFM_CFile.Record)
  BRW32::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW40::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW40::FormatManager.Init('MANTRNIS','Update_Clients',1,?List:9,40,BRW40::PopupTextExt,Queue:Browse:9,10,LFM_CFile,LFM_CFile.Record)
  BRW40::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW45::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW45::FormatManager.Init('MANTRNIS','Update_Clients',1,?List_Email,45,BRW45::PopupTextExt,Queue:Browse_EmailAddresses,11,LFM_CFile,LFM_CFile.Record)
  BRW45::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW49::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW49::FormatManager.Init('MANTRNIS','Update_Clients',1,?List_FuelCosts,49,BRW49::PopupTextExt,Queue:Browse_FuelCosts,4,LFM_CFile,LFM_CFile.Record)
  BRW49::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
      DO Style_Setup
      LQ:Str      = 'All'
      LQ:Value    = 150
      ADD(LOC:InvoiceStatus_Q)
  
      LQ:Str      = 'No Payments'
      LQ:Value    = 0
      ADD(LOC:InvoiceStatus_Q)
  
      LQ:Str      = 'Partially Paid'
      LQ:Value    = 1
      ADD(LOC:InvoiceStatus_Q)
  
      LQ:Str      = 'Credit Note'
      LQ:Value    = 2
      ADD(LOC:InvoiceStatus_Q)
  
      LQ:Str      = 'Fully Paid - Credit'
      LQ:Value    = 3
      ADD(LOC:InvoiceStatus_Q)
  
      LQ:Str      = 'Fully Paid'
      LQ:Value    = 4
      ADD(LOC:InvoiceStatus_Q)
  
      LQ:Str      = 'Credit Note Shown'
      LQ:Value    = 5
      ADD(LOC:InvoiceStatus_Q)
  
      LQ:Str      = 'Bad Debt Shown'
      LQ:Value    = 6
      ADD(LOC:InvoiceStatus_Q)
  
      LQ:Str      = 'Over Paid'
      LQ:Value    = 7
      ADD(LOC:InvoiceStatus_Q)
  
  
  
      ! 'All|#255|No Payments|#0|Partially Paid|#1|Credit Notes|#2|Fully Paid - Credit|#3|
      !   Fully Paid|#4|Credit Note (shown)|#5|Bad Debt (shown)|#6'
      ! Load Default email address...
      CLEAR(EMAI:Record)
      EMAI:CID    = CLI:CID
      SET(EMAI:FKey_CID, EMAI:FKey_CID)
      LOOP
         NEXT(EMailAddresses)
         IF ERRORCODE()
            BREAK
         .
         IF EMAI:CID ~= CLI:CID
            BREAK
         .
         IF EMAI:DefaultAddress = TRUE
            LOC:DefaultEmailAddress   = EMAI:EmailAddress
            BREAK
      .  .
       FormLocker.Init(?Unlock,self.Request)
       FormLocker.AddField(?CLI:ClientName)
       FormLocker.AddField(?CLI:OpsManager)
       FormLocker.AddField(?CLI:ClientNo)
       FormLocker.AddField(?ADDC:ContactName)
       FormLocker.AddField(?BRA:BranchName)
       FormLocker.AddField(?LOC:AddressName)
       FormLocker.AddField(?CallLookup)
       FormLocker.AddField(?Button_Edit)
       FormLocker.AddField(?CLI:VATNo)
       FormLocker.AddField(?CLI:DateOpened)
       FormLocker.AddField(?Calendar:DateOpen)
       FormLocker.AddField(?CLI:Terms)
       FormLocker.AddField(?CLI:InsuranceRequired)
       FormLocker.AddField(?CLI:InsuranceType)
       FormLocker.AddField(?CLI:InsurancePercent)
       FormLocker.AddField(?CLI:DocumentCharge)
       FormLocker.AddField(?CLI:MinimiumCharge)
       FormLocker.AddField(?CLI:FuelSurchargeActive)
       FormLocker.AddField(?CLI:TollChargeActive)
       FormLocker.AddField(?LOC:Ton_Volume)
       FormLocker.AddField(?CLI:VolumetricRatio)
       FormLocker.AddField(?CLI:PaymentPeriod)
       FormLocker.AddField(?CLI:AccountLimit)
       FormLocker.AddField(?LOC:AccountantName)
       FormLocker.AddField(?CallLookupAccountant)
       FormLocker.AddField(?LOC:SalesRep)
       FormLocker.AddField(?CallLookupSalesRep)
       FormLocker.AddField(?CLI:SalesRepEarnsUntil)
       FormLocker.AddField(?Calendar)
       FormLocker.AddField(?CLI:DeliveryNotes)
       FormLocker.AddField(?CLI:AdviceOfDispatch)
       FormLocker.AddField(?CLI:FaxConfirmation)
       FormLocker.AddField(?CLI:GenerateInvoice)
       FormLocker.AddField(?CLI:Notes)
       FormLocker.AddField(?CLI:InvoiceMessage)
       FormLocker.AddField(?CLI:OnInvoice)
       FormLocker.AddField(?CLI:Days30)
       FormLocker.AddField(?CLI:Days60)
       FormLocker.AddField(?CLI:Days90)
  
       FormLocker.AddField(?CLI:PODMessage)
            DO Check_Fuel_Rates
            DO Check_EToll_Rates
     LOC:SearchDel = 'CLNO:' & CLI:ClientNo
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW26::SortHeader.Init(Queue:Browse:Invoices,?List:Invoices,'','',BRW26::View:Browse,INV:PKey_IID)
  BRW26::SortHeader.UseSortColors = False
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Accountants.Close
    Relate:ClientsAlias.Close
    Relate:JourneysAlias.Close
    Relate:__RatesToll.Close
  !Kill the Sort Header
  BRW26::SortHeader.Kill()
  END
  ! List Format Manager destructor
  BRW23::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW11::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW29::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW33::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW6::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW32::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW40::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW45::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW49::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_Clients',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  CLI:BID = GLO:BranchID
  CLI:ClientSearch = CLI:CID
  CLI:ClientNo = CLI:CID
  CLI:Terms = 2
  CLI:FuelSurchargeActive = 1
  CLI:TollChargeActive = 1
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Addresses(CLI:ClientName, 'CL', CLI:BID)
      Browse_Accountants
      Browse_SalesReps
      Update_Statements
      Update_Invoice
      Update_Clients_ContainerParkRates
      Update_AdditionalCharges
      Update_Rates(L_LG:LTID, 1, L_JG:JID)
      Update_ClientsRateTypes
      Update_ClientsPayments
      Update_Reminders(1, CLI:CID)
      Update_Email_Addresses(p:Option)
      Update_RatesFuelCost
      Update_EToll
    END
    ReturnValue = GlobalResponse
  END
        CASE Number         ! (need to change if you change no/order etc of Tabs!!!
        OF 13              ! Update_RatesFuelCost              
           DO Check_Fuel_Rates
        OF 14              ! Update_RatesFuelCost              
           DO Check_EToll_Rates
        .
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW26::SortHeader.SetAlerts()


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Add_Contacts
          ADD:AID     = CLI:AID
          IF Access:Addresses.Tryfetch(ADD:PKey_AID) ~= LEVEL:Benign
             CLEAR(ADD:Record)
          .
    OF ?Button_Edit
          ADD:AID     = CLI:AID
          IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
          ELSE
             MESSAGE('Address not found!', 'Address', ICON:Exclamation)
             CYCLE
          .
    OF ?Button_DupRates_Rate
      BRW_Rates.UpdateViewRecord()
    OF ?Button_PrintStatement
          IF STA:STID = 0
             CYCLE
          .
      BRW_Statements.UpdateViewRecord()
    OF ?Button_Reverse
      BRW_ClientPayment.UpdateViewRecord()
    OF ?Save
      !    MESSAGE('Self.Req: ' & SELF.Request & '||Change: ' & ChangeRecord & '||Insert: ' & InsertRecord)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Add_Contacts
      ThisWindow.Update()
      Browse_Address_Contacts(CLI:AID)
      ThisWindow.Reset
      FDB5.ResetQueue(1)
    OF ?Button_Edit
      ThisWindow.Update()
      GlobalRequest = ChangeRecord
      Update_Addresses()
      ThisWindow.Reset
          DO Load_Address
    OF ?CallLookup
      ThisWindow.Update()
      ADD:AddressName = LOC:AddressName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:AddressName = ADD:AddressName
        CLI:AID = ADD:AID
      END
      ThisWindow.Reset(1)
    OF ?LOC:AddressName
      IF LOC:AddressName OR ?LOC:AddressName{PROP:Req}
        ADD:AddressName = LOC:AddressName
        IF Access:Addresses.TryFetch(ADD:Key_Name)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:AddressName = ADD:AddressName
            CLI:AID = ADD:AID
          ELSE
            CLEAR(CLI:AID)
            SELECT(?LOC:AddressName)
            CYCLE
          END
        ELSE
          CLI:AID = ADD:AID
        END
      END
      ThisWindow.Reset()
          DO Contact_Info
    OF ?Button_Email_Generaltab
      ThisWindow.Update()
          ISExecute(QuickWindow{PROP:Handle}, 'mailto:' & CLIP(LOC:DefaultEmailAddress))
          ! (whandle, URL, p:Params, p:LauchDir)
          ! (unsigned, STRING, <STRING>, <STRING>),LONG,PROC
    OF ?Calendar:DateOpen
      ThisWindow.Update()
      Calendar39.SelectOnClose = True
      Calendar39.Ask('Select a Date',CLI:DateOpened)
      IF Calendar39.Response = RequestCompleted THEN
      CLI:DateOpened=Calendar39.SelectedDate
      DISPLAY(?CLI:DateOpened)
      END
      ThisWindow.Reset(True)
    OF ?CLI:FuelSurchargeActive
          IF CLI:FuelSurchargeActive = TRUE
             ! old code..
!             ! Check that there are some entries - if not warn user and stop non-stop select
!             IF QuickWindow{PROP:AcceptAll} = TRUE
!                FSRA:CID  = CLI:CID
!                IF Access:__RatesFuelSurcharge.TryFetch(FSRA:FKey_CID) ~= LEVEL:Benign
!                   CASE MESSAGE('Fuel Surcharge Active is set on this client but there are no Fuel Surcharge rates set.||Would you like to add a Fuel Surcharge rate now?', |
!                                  'Fuel Surcharge', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
!                   OF BUTTON:Yes
!                      QuickWindow{PROP:AcceptAll} = FALSE
!                      SELECT(?Tab_FuelSurcharge)
!                      CYCLE
!                   OF BUTTON:No
!                      CLI:FuelSurchargeActive     = FALSE
!             .  .  .
          ELSE
             ! If previous state was True, check if there are entries, if so then warn user - confirm
!             IF LOC:Previous_FuelSurcharge = TRUE
!                FSRA:CID  = CLI:CID
!                IF Access:__RatesFuelSurcharge.TryFetch(FSRA:FKey_CID) = LEVEL:Benign
!                   CASE MESSAGE('There are Fuel Surcharge records.||Are you sure you want to switch off Fuel Surcharges for this client?', |
!                                  'Fuel Surcharge', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
!                   OF BUTTON:Yes
!                      LOC:Previous_FuelSurcharge  = FALSE
!                   OF BUTTON:No
!                      CLI:FuelSurchargeActive     = TRUE
          . ! .  .  .
      
      
          IF CLI:FuelSurchargeActive = TRUE
!             ENABLE(?Group_FuelSur_Buttons)
          ELSE
!             DISABLE(?Group_FuelSur_Buttons)
            .
          
          DO Check_Fuel_Rates
    OF ?CLI:TollChargeActive
         DO Check_EToll_Rates
    OF ?LOC:Ton_Volume
          ! CLI:VolumetricRatio = 1 cube equivalent to this weight
          ! e.g. 1 ton = 2 cubic metres
          CLI:VolumetricRatio         = (1000 / LOC:Ton_Volume)
          DISPLAY(?CLI:VolumetricRatio)
    OF ?CLI:VolumetricRatio
          ! CLI:VolumetricRatio = 1 cube equivalent to this weight
          ! e.g. 1 ton = 2 cubic metres
          LOC:Ton_Volume              = (1000 / CLI:VolumetricRatio)
          DISPLAY(?LOC:Ton_Volume)
    OF ?Button_Calc_Age_Analysis
      ThisWindow.Update()
          SETCURSOR(CURSOR:WAIT)
      
                                   ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
          Junk_#      = Gen_Statement(0, CLI:CID, TODAY(), CLOCK(),, TRUE, LOC:Statement_Info)
      
      
          ! (ULONG, LONG=0, BYTE=0, *STRING),LONG,PROC
          ! (p:CID, p:Date, p:Option, p_Bal_Group)
          Junk_#      = Get_Client_Invoice_History(CLI:CID, , 0, LOC:Invoice_Hist_Info)
      
      
          ! (ULONG, LONG=-1, *DECIMAL),LONG
          ! (p:CID, p:Payment_Status, p:Total)
          Junk_#      = Get_Client_Advance_Payments(CLI:CID, , LOC:PrePayment)
      
      
          DISPLAY
          SETCURSOR()
    OF ?CallLookupAccountant
      ThisWindow.Update()
      ACCO:AccountantName = LOC:AccountantName
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LOC:AccountantName = ACCO:AccountantName
        CLI:ACID = ACCO:ACID
      END
      ThisWindow.Reset(1)
    OF ?LOC:AccountantName
      IF LOC:AccountantName OR ?LOC:AccountantName{PROP:Req}
        ACCO:AccountantName = LOC:AccountantName
        IF Access:Accountants.TryFetch(ACCO:Key_AccountantName)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            LOC:AccountantName = ACCO:AccountantName
            CLI:ACID = ACCO:ACID
          ELSE
            CLEAR(CLI:ACID)
            SELECT(?LOC:AccountantName)
            CYCLE
          END
        ELSE
          CLI:ACID = ACCO:ACID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookupSalesRep
      ThisWindow.Update()
      SAL:SalesRep = LOC:SalesRep
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        LOC:SalesRep = SAL:SalesRep
        CLI:SRID = SAL:SRID
      END
      ThisWindow.Reset(1)
          IF SELF.Request = InsertRecord AND CLI:SalesRepEarnsUntil = 0
             CLI:SalesRepEarnsUntil   = TODAY() + (365 * 2)
          .
    OF ?LOC:SalesRep
      IF LOC:SalesRep OR ?LOC:SalesRep{PROP:Req}
        SAL:SalesRep = LOC:SalesRep
        IF Access:SalesReps.TryFetch(SAL:Key_SalesRep)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            LOC:SalesRep = SAL:SalesRep
            CLI:SRID = SAL:SRID
          ELSE
            CLEAR(CLI:SRID)
            SELECT(?LOC:SalesRep)
            CYCLE
          END
        ELSE
          CLI:SRID = SAL:SRID
        END
      END
      ThisWindow.Reset()
          IF SELF.Request = InsertRecord AND CLI:SalesRepEarnsUntil = 0
             CLI:SalesRepEarnsUntil   = TODAY() + (365 * 2)
          .
    OF ?Calendar
      ThisWindow.Update()
      Calendar21.SelectOnClose = True
      Calendar21.Ask('Select Earn Until a Date',CLI:SalesRepEarnsUntil)
      IF Calendar21.Response = RequestCompleted THEN
      CLI:SalesRepEarnsUntil=Calendar21.SelectedDate
      DISPLAY(?CLI:SalesRepEarnsUntil)
      END
      ThisWindow.Reset(True)
    OF ?Button_Email
      ThisWindow.Update()
          GET(Queue:Browse_EmailAddresses, CHOICE(?List_Email))
          IF ERRORCODE()
             MESSAGE('Select an email address first.', 'Email', ICON:Asterisk)
          ELSE
             ISExecute(QuickWindow{PROP:Handle}, 'mailto:' & CLIP(Queue:Browse_EmailAddresses.EMAI:EmailAddress))
             ! (whandle, URL, p:Params, p:LauchDir)
             ! (unsigned, STRING, <STRING>, <STRING>),LONG,PROC
          .
    OF ?IncludePast:2
      BRW_Rates.ResetQueue(Reset:Queue)
      BRW_FuelCosts.ResetQueue(Reset:Queue)      
      BRW_AddCharge.ResetQueue(Reset:Queue)
      BRW_Cont_Park_Discounts.ResetQueue(Reset:Queue)
      BRW_RatesContainerPark.ResetQueue(Reset:Queue)
    OF ?Button_Show_All
      ThisWindow.Update()
          CLEAR(L_JG:Journey)
          CLEAR(L_JG:JID)
      
          CLEAR(L_LG:LoadType)
          CLEAR(L_LG:LTID)
      
          LOC:Changed_Indicator   += 1
      
          DISPLAY
      
    OF ?Button_DupRates_Rate
      ThisWindow.Update()
      Window_Duplicate_Client_Rates(CLI:CID, RAT:RID)
      ThisWindow.Reset
          LOC:Changed_Indicator   += 1
    OF ?Insert:FC
      ThisWindow.Update()
          DO Check_Fuel_Rates
    OF ?Button_PrintInvoice
      ThisWindow.Update()
      BRW_Invoices.UpdateViewRecord()
          DO Print_Invoices
    OF ?Button_UpdateStatuses
      ThisWindow.Update()
      Process_Invoices(CLI:CID)
      ThisWindow.Reset
    OF ?Button_PrintStatement
      ThisWindow.Update()
          IF STA:STID ~= 0
             ThisWindow.Update
      
             CASE POPUP('Print Statements Continuous|Print Statements Laser')
             OF 1
                ! (p:ID, p:PrintType, p:ID_Options, p:Preview)
                Print_Cont(STA:STID, 3)
             OF 2
                Print_Statement(CLI:CID, STA:STID)
             .
      
             ThisWindow.Reset
          .
    OF ?Button_Reverse
      ThisWindow.Update()
      GlobalRequest = InsertRecord
      Update_ClientsPayments(1, CLIP:CPID)
      ThisWindow.Reset
    OF ?BUTTON_Deliveries
      ThisWindow.Update()
      Browse_Deliveries('S',LOC:SearchDel)
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update()
          IF SELF.Request = InsertRecord
             IF CLI:ClientNo = 0
                MESSAGE('You have not specified a Client No.||Please specify one.', 'Save Client Record', ICON:Exclamation)
                SELECT(?CLI:ClientNo)
                QuickWindow{PROP:AcceptAll}    = FALSE
                CYCLE
             .
      
             Rep_ID_#     = CLI:CID / 1000000             ! Replication portion
             IF Rep_ID_# > 0
                CLI:ClientNo      = CLI:ClientNo          + (Rep_ID_# * 1000000)
                CLI:ClientName    = CLIP(CLI:ClientName)  & '  (' & CLI:CID & ')'
          .  .
      
            
            IF CLI:LiabilityCrossBorder > 0
              IF CLI:LiabilityCover <= 0 
                MESSAGE('If you have selected Liability Cross Border cover you must also select Liability Cover','Save Client Record', ICON:Exclamation)
                
                SELECT(?)
                QuickWindow{PROP:AcceptAll}    = FALSE
                CYCLE
              .
            .
            
              
          CLI:ClientSearch    = Get_Client_Search(CLI:ClientName)
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    OF ?Save
      ThisWindow.Update()
          IF SELF.Request ~= InsertRecord
             ENABLE(?Tab_Ratess)
             !ENABLE(?Tab_Deliveries)
             ENABLE(?Tab_Invoices)
             ENABLE(?Tab_Statements)
          .
    OF ?Button_PrintRates
      ThisWindow.Update()
      Print_Rates(CLI:CID)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Take Sort Headers Events
  IF BRW26::SortHeader.TakeEvents()
     RETURN Level:Notify
  END
  FormLocker.TakeEvent()
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?LOC:DefaultEmailAddress
    CASE EVENT()
    OF EVENT:AlertKey
          ISExecute(QuickWindow{PROP:Handle}, 'mailto:' & CLIP(LOC:DefaultEmailAddress))
          ! (whandle, URL, p:Params, p:LauchDir)
          ! (unsigned, STRING, <STRING>, <STRING>),LONG,PROC
    END
  OF ?List:Invoices
        ! how to use the tags -
        CASE EVENT()
        OF EVENT:Accepted
           !EVENT:MouseDown
    !       BRW1.UpdateViewRecord()
           CASE KeyCode()
           OF ShiftMouseLeft
              ! Tag all records between this one and last tagged record
    !          IF CHOICE(?CurrentTab) = 1
    !             DO Tag_From_To
    !          .
           OF CtrlMouseLeft
              DO Tag_Toggle_this_rec
           ELSE
    !          IF KeyCode() ~= MouseRight AND KeyCode() ~= CtrlMouseRight        !AND LOC:Last_Tagged_Rec_Id ~= 0
    !          InBox_Tags.ClearAllTags()                ! Clear all tags when no shift left mouse
    !          LOC:Last_Tagged_Rec_Id        = 0
    
    !          BRW1.ResetFromBuffer()
           .
        OF EVENT:User             ! Clear tags
           DO Un_Tag_All
        OF EVENT:User+1           ! Print?
        .
    
    CASE EVENT()
    OF EVENT:AlertKey
          IF KeyCode() = CtrlU
             DO Un_Tag_All
          .
      
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_JG:Journey
      !    get(Queue:FileDrop:1, choice(?L_JG:Journey))
      !    IF ERRORCODE()
      !    .
      !
      !    MESSAge('L_JG:JID: ' & L_JG:JID & '||A_JOU:JID: ' & Queue:FileDrop:1.A_JOU:JID)
      !    L_JG:JID    = Queue:FileDrop:1.A_JOU:JID
      !    display
      
          LOC:Changed_Indicator   += 1
    OF ?List:ContainerPark
      BRW_Cont_Park_Discounts.UpdateViewRecord()
    OF ?List_InvoiceStatus
          GET(LOC:InvoiceStatus_Q, CHOICE(?List_InvoiceStatus))
          IF ~ERRORCODE()
          .
      
          LI_BG:Change_Indicator  += 1
          DISPLAY
    OF ?L_P:Status_Filter
         PUTINI('Update_Clients', 'L_P:Status_Filter', L_P:Status_Filter, GLO:Local_INI)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          LQ:Str      = 'No Payments'
          LQ:Value    = 0
          GET(LOC:InvoiceStatus_Q,2)
          DO Contact_Info
          DO User_Buttons
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          ! CLI:VolumetricRatio = 1 cube equivalent to this weight
          ! e.g. 1 ton = 2 cubic metres
          LOC:Ton_Volume              = (1000 / CLI:VolumetricRatio)
    ELSE
      CASE EVENT()
      OF EVENT:User             ! Clear tags
         DO Un_Tag_All
      OF EVENT:User+1           ! Print?
      .
  
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW_Statements.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:Statements
    SELF.ChangeControl=?Change:Statements
    SELF.DeleteControl=?Delete:Statements
  END
  SELF.ViewControl = ?View:Statements                      ! Setup the control used to initiate view only mode


BRW_Statements.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW23::LastSortOrder <> NewOrder THEN
     BRW23::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW23::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_Statements.TakeNewSelection PROCEDURE

  CODE
  IF BRW23::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW23::PopupTextExt = ''
        BRW23::PopupChoiceExec = True
        BRW23::FormatManager.MakePopup(BRW23::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW23::PopupTextExt = '|-|' & CLIP(BRW23::PopupTextExt)
        END
        BRW23::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW23::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW23::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW23::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW23::PopupChoiceOn AND BRW23::PopupChoiceExec THEN
     BRW23::PopupChoiceExec = False
     BRW23::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW23::PopupTextExt)
     IF BRW23::FormatManager.DispatchChoice(BRW23::PopupChoice)
     ELSE
     END
  END


BRW_Invoices.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:Invoices
    SELF.ChangeControl=?Change:Invoices
    SELF.DeleteControl=?Delete:Invoices
  END
  SELF.ViewControl = ?View:Invoices                        ! Setup the control used to initiate view only mode
     DO Access_Permissions


BRW_Invoices.SetQueueRecord PROCEDURE

  CODE
      CLEAR(LOC:Invoice_Brws_Group)
      IF L_BI:Deliveries_Commodities_Packaging = TRUE
  ! these top three dont appear to be on the browse...?
  !       Get_Info_SQL(5, INV:DID, LI_BG:Delivery_Dates)       ! Now also Times - using 5 instead of 0
  !       Get_Info_SQL(1, INV:DID, LI_BG:Commodities)
  !       Get_Info_SQL(2, INV:DID, LI_BG:Packagings)
         Get_Info_SQL(6, INV:DID, LI_BG:DI_ContainerNos)
      .
  
      ! ??? For Del Dates we could instead use this function:
      !      IF Get_TripDelItems_Info(, 3, DEL:DID,,, L_SG:Tripsheets_Info) > 0
      !      .
      !      L_SG:Tripsheets_Info          = Get_1st_Element_From_Delim_Str(L_SG:Tripsheets_Info, ',')
      ! Returned value is last Delivery Date/Time....
  
  
  
  
      ! Invoices may be credited or have payments
      L_BI:Payments       = Get_ClientsPay_Alloc_Amt(INV:IID)
      L_BI:Credited       = -Get_Inv_Credited(INV:IID)                    ! Credited is negative
  
      ! Credit notes may have associated invoices or not??
      IF INV:CR_IID ~= 0                                                  ! Should only be for credit notes!
         IF INV:Status = 2 OR INV:Status = 5
         ELSE
            ! ??? problem, have Credit Note ID but not Credit Note!
         .
  
         ! The L_BI:Credited should be zero as Credit Notes cannot be credited.
  
         L_BI:Credited   += -Get_Inv_Credited(INV:CR_IID)                 ! This is total credited (and will include this Credit Note)
         L_BI:Payments   += Get_ClientsPay_Alloc_Amt(INV:CR_IID)
  
         A_INV:IID        = INV:CR_IID
         IF Access:InvoiceAlias.TryFetch(A_INV:PKey_IID) ~= LEVEL:Benign
            CLEAR(A_INV:Record)
         .
  
         L_BI:Outstanding = A_INV:Total - (L_BI:Payments + L_BI:Credited)
      ELSE
         L_BI:Outstanding = INV:Total - (L_BI:Payments + L_BI:Credited)
      .
  
  
  
      !     1               2               3             4               5               6               7
      ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown, Bad Debt
      EXECUTE INV:Status + 1
         LI_BG:Status = 'No Payments'
         LI_BG:Status = 'Partially Paid'
         LI_BG:Status = 'Credit Note'
         LI_BG:Status = 'Fully Paid - Credit'
         LI_BG:Status = 'Fully Paid'
         LI_BG:Status = 'Credit Note (shown)'
         LI_BG:Status = 'Bad Debt'
         LI_BG:Status = 'Over Paid'
      ELSE
         LI_BG:Status = '<unknown>'
      .
  
  !    ! Pre Paid|COD|Account
  !    CLEAR(LOC:Terms)
  !    EXECUTE INV:Terms
  !       LOC:Terms   = 'COD'
  !       LOC:Terms   = 'Account'
  !    ELSE
  !       LOC:Terms   = 'Pre Paid'
  !    .
  !
  PARENT.SetQueueRecord
  
  SELF.Q.INV:IID_Style = 0 ! 
  SELF.Q.INV:DINo_Style = 0 ! 
      DO Style_Entry


BRW_Invoices.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW26::LastSortOrder<>NewOrder THEN
     BRW26::SortHeader.ClearSort()
  END
  BRW26::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_Cont_Park_Discounts.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:CP
    SELF.ChangeControl=?Change:CP
    SELF.DeleteControl=?Delete:CP
  END


BRW_Cont_Park_Discounts.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  PARENT.ResetQueue(ResetMode)
    FREE(Container_Unique_Q)


BRW_Cont_Park_Discounts.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW11::LastSortOrder <> NewOrder THEN
     BRW11::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW11::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_Cont_Park_Discounts.TakeNewSelection PROCEDURE

  CODE
  IF BRW11::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW11::PopupTextExt = ''
        BRW11::PopupChoiceExec = True
        BRW11::FormatManager.MakePopup(BRW11::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW11::PopupTextExt = '|-|' & CLIP(BRW11::PopupTextExt)
        END
        BRW11::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW11::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW11::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW11::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW11::PopupChoiceOn AND BRW11::PopupChoiceExec THEN
     BRW11::PopupChoiceExec = False
     BRW11::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW11::PopupTextExt)
     IF BRW11::FormatManager.DispatchChoice(BRW11::PopupChoice)
     ELSE
     END
  END


BRW_Cont_Park_Discounts.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW11::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
    ! Container parks
    IF IncludePast = FALSE AND (CLI_CP:Effective_Date < TODAY() OR (CLI_CP:Effective_Date = TODAY() AND CLI_CP:Effective_TIME < CLOCK()))
      CUQ:FID   = CLI_CP:FID
      GET(Container_Unique_Q, +CUQ:FID)
      IF ERRORCODE()
        ADD(Container_Unique_Q)
      ELSE 
        ReturnValue = Record:Filtered
      .
    .
  BRW11::RecordStatus=ReturnValue
  RETURN ReturnValue


BRW_AddCharge.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:AC
    SELF.ChangeControl=?Change:AC
    SELF.DeleteControl=?Delete:AC
  END


BRW_AddCharge.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  PARENT.ResetQueue(ResetMode)
  FREE(Additional_Unique_Q)


BRW_AddCharge.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW29::LastSortOrder <> NewOrder THEN
     BRW29::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW29::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_AddCharge.TakeNewSelection PROCEDURE

  CODE
  IF BRW29::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW29::PopupTextExt = ''
        BRW29::PopupChoiceExec = True
        BRW29::FormatManager.MakePopup(BRW29::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW29::PopupTextExt = '|-|' & CLIP(BRW29::PopupTextExt)
        END
        BRW29::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW29::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW29::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW29::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW29::PopupChoiceOn AND BRW29::PopupChoiceExec THEN
     BRW29::PopupChoiceExec = False
     BRW29::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW29::PopupTextExt)
     IF BRW29::FormatManager.DispatchChoice(BRW29::PopupChoice)
     ELSE
     END
  END


BRW_AddCharge.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW29::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
    ! Additional_Unique_Q
    IF IncludePast = FALSE AND (CARA:Effective_Date < TODAY() OR (CARA:Effective_Date = TODAY() AND CARA:Effective_TIME < CLOCK()))
      AUQ:ACCID = CARA:ACCID
      GET(Container_Unique_Q, +AUQ:ACCID)
      IF ERRORCODE()
        ADD(Container_Unique_Q)
      ELSE
        ReturnValue = Record:Filtered
      .
    .
  BRW29::RecordStatus=ReturnValue
  RETURN ReturnValue


BRW_Rates.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:Rates
    SELF.ChangeControl=?Change:Rates
    SELF.DeleteControl=?Delete:Rates
  END
      LOC:User_Access_Rates   = Get_User_Access(GLO:UID, 'Update Rates', 'Update_Clients')
      IF LOC:User_Access_Rates > 0      
         SELF.InsertControl   = 0
         SELF.ChangeControl   = 0
         SELF.DeleteControl   = 0
  
         BRW_Cont_Park_Discounts.InsertControl    = 0
         BRW_Cont_Park_Discounts.ChangeControl    = 0
         BRW_Cont_Park_Discounts.DeleteControl    = 0
  
         BRW_AddCharge.InsertControl              = 0
         BRW_AddCharge.ChangeControl              = 0
         BRW_AddCharge.DeleteControl              = 0
  
         BRW_ClientRateTypes.InsertControl        = 0
         BRW_ClientRateTypes.ChangeControl        = 0
         BRW_ClientRateTypes.DeleteControl        = 0
  
         IF LOC:User_Access_Rates ~= 1       ! Disable / Allow View??
            SELF.ViewControl                      = 0
  
            BRW_Cont_Park_Discounts.ViewControl   = 0
            BRW_AddCharge.ViewControl             = 0            
            BRW_ClientRateTypes.ViewControl       = 0
      .  .
  
  
      db.debugout('[Update_Clients] BRW_Rates.init  GLO:UID: ' & GLO:UID & ',   LOC:User_Access_Rates: ' & LOC:User_Access_Rates)


BRW_Rates.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  PARENT.ResetQueue(ResetMode)
  FREE(Rates_Unique_Q)
      


BRW_Rates.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (RAT:AdHoc = 1)
    SELF.Q.RAT:AdHoc_Icon = 2                              ! Set icon from icon list
  ELSE
    SELF.Q.RAT:AdHoc_Icon = 1                              ! Set icon from icon list
  END


BRW_Rates.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW33::LastSortOrder <> NewOrder THEN
     BRW33::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW33::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_Rates.TakeNewSelection PROCEDURE

  CODE
  IF BRW33::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW33::PopupTextExt = ''
        BRW33::PopupChoiceExec = True
        BRW33::FormatManager.MakePopup(BRW33::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW33::PopupTextExt = '|-|' & CLIP(BRW33::PopupTextExt)
        END
        BRW33::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW33::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW33::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW33::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW33::PopupChoiceOn AND BRW33::PopupChoiceExec THEN
     BRW33::PopupChoiceExec = False
     BRW33::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW33::PopupTextExt)
     IF BRW33::FormatManager.DispatchChoice(BRW33::PopupChoice)
     ELSE
     END
  END


BRW_Rates.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW33::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
    IF IncludePast = FALSE AND (RAT:Effective_Date < TODAY() OR (RAT:Effective_Date = TODAY() AND RAT:Effective_TIME < CLOCK()))
  
      ! Allow only first instance of a record type
      ! RAT:JID, RAT:LTID, RAT:CTID, RAT:CRTID, RAT:ToMass, RAT:Effective_Date, RAT:Effective_TIME
      
      RUQ:JID     = RAT:JID
      RUQ:LTID    = RAT:LTID
      RUQ:CTID    = RAT:CTID
      RUQ:CRTID   = RAT:CRTID
      RUQ:ToMass  = RAT:ToMass
      RUQ:Effective_Date  = RAT:Effective_Date
      RUQ:Effective_TIME  = RAT:Effective_TIME
  
      GET(Rates_Unique_Q, +RUQ:JID,+RUQ:LTID,+RUQ:CTID,+RUQ:CRTID,+RUQ:ToMass)
      IF ERRORCODE()
        ADD(Rates_Unique_Q)
      ELSE
        ! We already have a later rate, discard, skip
        ReturnValue = Record:Filtered
      .
    .
  BRW33::RecordStatus=ReturnValue
  RETURN ReturnValue


BRW_RatesContainerPark.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  PARENT.ResetQueue(ResetMode)
    FREE(ContainerPRA_Unique_Q)


BRW_RatesContainerPark.SetQueueRecord PROCEDURE

  CODE
      L_SF:ClientRatePerKg    = CPRA:RatePerKg - (CPRA:RatePerKg * (CLI_CP:ContainerParkRateDiscount / 100))
  
      ! 10 - (10 * 0.10) = 9
                                                                             
  PARENT.SetQueueRecord
  


BRW_RatesContainerPark.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_RatesContainerPark.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END


BRW_RatesContainerPark.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW2::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
    ! Container parks
    IF IncludePast = FALSE AND (CPRA:Effective_Date < TODAY() OR (CPRA:Effective_Date = TODAY() AND CPRA:Effective_TIME < CLOCK()))
      CPRUQ:JID       = CPRA:JID
      CPRUQ:ToMass    = CPRA:ToMass
      GET(ContainerPRA_Unique_Q, +CPRUQ:JID,+CPRUQ:ToMass)
      IF ERRORCODE()
        ADD(ContainerPRA_Unique_Q)
      ELSE 
        ReturnValue = Record:Filtered
      .
    .
  BRW2::RecordStatus=ReturnValue
  RETURN ReturnValue


BRW_ClientRateTypes.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:CR
    SELF.ChangeControl=?Change:CR
    SELF.DeleteControl=?Delete:CR
  END


BRW_ClientRateTypes.SetQueueRecord PROCEDURE

  CODE
      LOC:ClientRateType_Used     = Check_Rates(CRT:CRTID)
  PARENT.SetQueueRecord
  


BRW_ClientRateTypes.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW6::LastSortOrder <> NewOrder THEN
     BRW6::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW6::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_ClientRateTypes.TakeNewSelection PROCEDURE

  CODE
  IF BRW6::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW6::PopupTextExt = ''
        BRW6::PopupChoiceExec = True
        BRW6::FormatManager.MakePopup(BRW6::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW6::PopupTextExt = '|-|' & CLIP(BRW6::PopupTextExt)
        END
        BRW6::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW6::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW6::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW6::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW6::PopupChoiceOn AND BRW6::PopupChoiceExec THEN
     BRW6::PopupChoiceExec = False
     BRW6::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW6::PopupTextExt)
     IF BRW6::FormatManager.DispatchChoice(BRW6::PopupChoice)
     ELSE
     END
  END


BRW_ClientPayment.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:5
    SELF.ChangeControl=?Change:5
    SELF.DeleteControl=?Delete:5
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW_ClientPayment.SetQueueRecord PROCEDURE

  CODE
      EXECUTE CLIP:Type + 1
         L_P:Type_Q       = 'Payment'
         L_P:Type_Q       = 'Reversal'
      ELSE
         L_P:Type_Q       = '?: ' & CLIP:Type
      .
  
  
      EXECUTE CLIP:Status + 1
         L_P:Status_Q     = 'Not Allocated'
         L_P:Status_Q     = 'Partial Allocation'
         L_P:Status_Q     = 'Fully Allocated'
      ELSE
         L_P:Status_Q     = '?: ' & CLIP:Status
      .
  PARENT.SetQueueRecord
  
  IF (CLIP:StatusUpToDate = 1)
    SELF.Q.CLIP:StatusUpToDate_Icon = 2                    ! Set icon from icon list
  ELSE
    SELF.Q.CLIP:StatusUpToDate_Icon = 1                    ! Set icon from icon list
  END


BRW_ClientPayment.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW32::LastSortOrder <> NewOrder THEN
     BRW32::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW32::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_ClientPayment.TakeNewSelection PROCEDURE

  CODE
  IF BRW32::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW32::PopupTextExt = ''
        BRW32::PopupChoiceExec = True
        BRW32::FormatManager.MakePopup(BRW32::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW32::PopupTextExt = '|-|' & CLIP(BRW32::PopupTextExt)
        END
        BRW32::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW32::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW32::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW32::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW32::PopupChoiceOn AND BRW32::PopupChoiceExec THEN
     BRW32::PopupChoiceExec = False
     BRW32::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW32::PopupTextExt)
     IF BRW32::FormatManager.DispatchChoice(BRW32::PopupChoice)
     ELSE
     END
  END


BRW40.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:7
    SELF.ChangeControl=?Change:7
    SELF.DeleteControl=?Delete:7
  END
  SELF.ViewControl = ?View:4                               ! Setup the control used to initiate view only mode


BRW40.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (REM:Active = 1)
    SELF.Q.REM:Active_Icon = 2                             ! Set icon from icon list
  ELSE
    SELF.Q.REM:Active_Icon = 1                             ! Set icon from icon list
  END
  IF (REM:Popup = 1)
    SELF.Q.REM:Popup_Icon = 2                              ! Set icon from icon list
  ELSE
    SELF.Q.REM:Popup_Icon = 1                              ! Set icon from icon list
  END


BRW40.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW40::LastSortOrder <> NewOrder THEN
     BRW40::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW40::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW40.TakeNewSelection PROCEDURE

  CODE
  IF BRW40::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW40::PopupTextExt = ''
        BRW40::PopupChoiceExec = True
        BRW40::FormatManager.MakePopup(BRW40::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW40::PopupTextExt = '|-|' & CLIP(BRW40::PopupTextExt)
        END
        BRW40::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW40::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW40::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW40::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW40::PopupChoiceOn AND BRW40::PopupChoiceExec THEN
     BRW40::PopupChoiceExec = False
     BRW40::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW40::PopupTextExt)
     IF BRW40::FormatManager.DispatchChoice(BRW40::PopupChoice)
     ELSE
     END
  END


BRW45.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW45.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (EMAI:RateLetter = 1)
    SELF.Q.EMAI:RateLetter_Icon = 2                        ! Set icon from icon list
  ELSE
    SELF.Q.EMAI:RateLetter_Icon = 1                        ! Set icon from icon list
  END
  IF (EMAI:DefaultAddress = 1)
    SELF.Q.EMAI:DefaultAddress_Icon = 2                    ! Set icon from icon list
  ELSE
    SELF.Q.EMAI:DefaultAddress_Icon = 1                    ! Set icon from icon list
  END
  IF (EMAI:Operations = 1)
    SELF.Q.EMAI:Operations_Icon = 2                        ! Set icon from icon list
  ELSE
    SELF.Q.EMAI:Operations_Icon = 1                        ! Set icon from icon list
  END
  IF (EMAI:DefaultOnDI = 1)
    SELF.Q.EMAI:DefaultOnDI_Icon = 2                       ! Set icon from icon list
  ELSE
    SELF.Q.EMAI:DefaultOnDI_Icon = 1                       ! Set icon from icon list
  END


BRW45.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW45::LastSortOrder <> NewOrder THEN
     BRW45::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW45::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW45.TakeNewSelection PROCEDURE

  CODE
  IF BRW45::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW45::PopupTextExt = ''
        BRW45::PopupChoiceExec = True
        BRW45::FormatManager.MakePopup(BRW45::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW45::PopupTextExt = '|-|' & CLIP(BRW45::PopupTextExt)
        END
        BRW45::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW45::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW45::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW45::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW45::PopupChoiceOn AND BRW45::PopupChoiceExec THEN
     BRW45::PopupChoiceExec = False
     BRW45::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW45::PopupTextExt)
     IF BRW45::FormatManager.DispatchChoice(BRW45::PopupChoice)
     ELSE
     END
  END


BRW_FuelCosts.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:FC
    SELF.ChangeControl=?Change:FC
    SELF.DeleteControl=?Delete:FC
  END


BRW_FuelCosts.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  PARENT.ResetQueue(ResetMode)
    FuelCosts_Unique = FALSE


BRW_FuelCosts.SetQueueRecord PROCEDURE

  CODE
      IF FCRA:FuelSurcharge < 0
         FCRA:FuelSurcharge   = 0.0
      .
  PARENT.SetQueueRecord
  


BRW_FuelCosts.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW49::LastSortOrder <> NewOrder THEN
     BRW49::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW49::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_FuelCosts.TakeNewSelection PROCEDURE

  CODE
  IF BRW49::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW49::PopupTextExt = ''
        BRW49::PopupChoiceExec = True
        BRW49::FormatManager.MakePopup(BRW49::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW49::PopupTextExt = '|-|' & CLIP(BRW49::PopupTextExt)
        END
        BRW49::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW49::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW49::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW49::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW49::PopupChoiceOn AND BRW49::PopupChoiceExec THEN
     BRW49::PopupChoiceExec = False
     BRW49::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW49::PopupTextExt)
     IF BRW49::FormatManager.DispatchChoice(BRW49::PopupChoice)
     ELSE
     END
  END


BRW_FuelCosts.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW49::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
    ! Fuel
    IF IncludePast = FALSE AND (FUE:EffectiveDate < TODAY() OR (FUE:EffectiveDate = TODAY() AND FUE:EffectiveTime < CLOCK()))
      IF FuelCosts_Unique = FALSE  
        FuelCosts_Unique = TRUE
      ELSE
        ReturnValue = Record:Filtered
      .
    .
  BRW49::RecordStatus=ReturnValue
  RETURN ReturnValue


BRW_ETolls.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:EToll
    SELF.ChangeControl=?Change:EToll
    SELF.DeleteControl=?Delete:EToll
  END


BRW_ETolls.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  PARENT.ResetQueue(ResetMode)
    EToll_Unique = FALSE


BRW_ETolls.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW43::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
    IF IncludePast = FALSE AND (TOL:Effective_Date < TODAY() OR (TOL:Effective_Date = TODAY() AND TOL:Effective_TIME < CLOCK()))
      IF EToll_Unique = FALSE
        EToll_Unique = TRUE
      ELSE
        ReturnValue = Record:Filtered
      .
    .
  BRW43::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Right, Resize:FixRight+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Right
  SELF.SetStrategy(?Group_Add_Left, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Add_Left
  SELF.SetStrategy(?Group_Discounts, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Discounts
  SELF.SetStrategy(?Group_CP_Disc, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:LockHeight) ! Override strategy for ?Group_CP_Disc
  SELF.SetStrategy(?Group_FloorRatesDiscounted, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom) ! Override strategy for ?Group_FloorRatesDiscounted
  SELF.SetStrategy(?Group_ShowActive, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_ShowActive
  SELF.SetStrategy(?Group_Inv_Status, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Inv_Status
  SELF.SetStrategy(?Group_DropDowns, Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_DropDowns
  SELF.SetStrategy(?Group_Notes, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Notes
  SELF.SetStrategy(?Panel_Header, Resize:Reposition, Resize:LockHeight) ! Override strategy for ?Panel_Header
  SELF.SetStrategy(?Group_3_bot, Resize:FixLeft+Resize:FixBottom, Resize:LockHeight) ! Override strategy for ?Group_3_bot
  SELF.SetStrategy(?Group_PayFilter_Group, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_PayFilter_Group
  SELF.SetStrategy(?Unlock, Resize:LockYPos, Resize:LockSize) ! Override strategy for ?Unlock
  SELF.SetStrategy(?Group_FuelCosts, Resize:FixRight+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_FuelCosts
  SELF.SetStrategy(?Group_AddCharges, Resize:FixRight+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_AddCharges
  SELF.SetStrategy(?Group_ClientRateTypes, Resize:FixRight+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_ClientRateTypes
  SELF.SetStrategy(?Group_ContainerPark, Resize:FixRight+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_ContainerPark
  SELF.SetStrategy(?Group_Rates, Resize:FixRight+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Rates
  SELF.SetStrategy(?Prompt_RatesShown, Resize:FixLeft+Resize:FixBottom, Resize:ConstantRight+Resize:LockHeight) ! Override strategy for ?Prompt_RatesShown
  SELF.SetStrategy(?List, Resize:FixRight+Resize:FixTop, Resize:LockWidth+Resize:ConstantBottom) ! Override strategy for ?List
  SELF.SetStrategy(?Prompt_EToll_RatesShown, Resize:FixLeft+Resize:FixBottom, Resize:ConstantRight+Resize:LockHeight) ! Override strategy for ?Prompt_EToll_RatesShown


FDB5.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  SELF.Q.ADDC:PrimaryContact_Icon = 1

BRW26::SortHeader.QueueResorted       PROCEDURE(STRING pString)
  CODE
    IF pString = ''
       BRW_Invoices.RestoreSort()
       BRW_Invoices.ResetSort(True)
    ELSE
       BRW_Invoices.ReplaceSort(pString,BRW26::Sort0:Locator)
       BRW_Invoices.SetLocatorFromSort()
    END
FormLocker.CanBeUnlocked    Procedure(LONG pCtrl)
p_Ret   BYTE(1)
    CODE
    IF LOC:User_Access_Rates > 0
           DISABLE(?IncludePast:2)
      
           CASE pCtrl
            OF ?Button_DupRates_Rate |
                OROF ?Insert:Rates OROF ?Change:Rates; OROF ?Delete:Rates |
                OROF ?Insert:CP OROF ?Change:CP OROF ?Delete:CP |
                OROF ?Insert:AC OROF ?Change:AC OROF ?Delete:AC |
                OROF ?Insert:CR OROF ?Change:CR OROF ?Delete:CR |
                OROF ?Insert:EToll OROF ?Change:EToll OROF ?Delete:EToll
            p_Ret   = FALSE
        .

        IF LOC:User_Access_Rates ~= 1       ! Disable / Allow View??
    .   .
        
    IF LOC:User_Access_Invoices > 0
        CASE pCtrl 
        OF ?Insert:Invoices OROF ?Change:Invoices OROF ?Delete:Invoices |
                OROF ?Insert:Statements OROF ?Change:Statements OROF ?Delete:Statements 
            p_Ret   = FALSE
        .
        
        IF LOC:User_Access_Invoices ~= 1       ! Disable / Allow View??
            CASE pCtrl
            OF ?View:Invoices OROF ?Tab_Invoices OROF ?Tab_Statements
               p_Ret    = FALSE                    
    .   .   .    
    RETURN(p_Ret)

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Invoice PROCEDURE (p:str_IID)

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Screen_Group     GROUP,PRE()                           ! 
L_SG:Paid            DECIMAL(12,2)                         ! Paid amount
L_SG:Credits         DECIMAL(12,2)                         ! 
L_SG:ItemType        STRING(20)                            ! Type of Item - Container or Loose
                     END                                   ! 
Sattement_Type       STRING(20)                            ! Client, Adhoc
LOC:Status           BYTE                                  ! used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown, Bad Debt
LOC:Login            STRING(20)                            ! User Login
LOC:Passing_Group    GROUP,PRE(L_PG)                       ! 
IID                  STRING(20)                            ! Invoice Number
CPID                 STRING(20)                            ! Cliets Payment ID
                     END                                   ! 
BRW2::View:Browse    VIEW(_InvoiceItems)
                       PROJECT(INI:Description)
                       PROJECT(INI:Commodity)
                       PROJECT(INI:ContainerDescription)
                       PROJECT(INI:Units)
                       PROJECT(INI:ItemNo)
                       PROJECT(INI:ITID)
                       PROJECT(INI:DIID)
                       PROJECT(INI:Type)
                       PROJECT(INI:IID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
INI:Description        LIKE(INI:Description)          !List box control field - type derived from field
INI:Commodity          LIKE(INI:Commodity)            !List box control field - type derived from field
INI:ContainerDescription LIKE(INI:ContainerDescription) !List box control field - type derived from field
INI:Units              LIKE(INI:Units)                !List box control field - type derived from field
L_SG:ItemType          LIKE(L_SG:ItemType)            !List box control field - type derived from local data
INI:ItemNo             LIKE(INI:ItemNo)               !List box control field - type derived from field
INI:ITID               LIKE(INI:ITID)                 !List box control field - type derived from field
INI:DIID               LIKE(INI:DIID)                 !List box control field - type derived from field
INI:Type               LIKE(INI:Type)                 !Browse hot field - type derived from field
INI:IID                LIKE(INI:IID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW3::View:Browse    VIEW(_StatementItems)
                       PROJECT(STAI:DINo)
                       PROJECT(STAI:Debit)
                       PROJECT(STAI:Credit)
                       PROJECT(STAI:Amount)
                       PROJECT(STAI:STIID)
                       PROJECT(STAI:IID)
                       PROJECT(STAI:STID)
                       JOIN(STA:PKey_STID,STAI:STID)
                         PROJECT(STA:STID)
                         PROJECT(STA:StatementDate)
                         PROJECT(STA:StatementTime)
                         PROJECT(STA:STRID)
                         JOIN(STAR:PKey_STRID,STA:STRID)
                           PROJECT(STAR:RunDescription)
                           PROJECT(STAR:RunDate)
                           PROJECT(STAR:RunTime)
                           PROJECT(STAR:Type)
                           PROJECT(STAR:STRID)
                         END
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
STAI:DINo              LIKE(STAI:DINo)                !List box control field - type derived from field
STAI:Debit             LIKE(STAI:Debit)               !List box control field - type derived from field
STAI:Credit            LIKE(STAI:Credit)              !List box control field - type derived from field
STAI:Amount            LIKE(STAI:Amount)              !List box control field - type derived from field
STA:STID               LIKE(STA:STID)                 !List box control field - type derived from field
STA:StatementDate      LIKE(STA:StatementDate)        !List box control field - type derived from field
STA:StatementTime      LIKE(STA:StatementTime)        !List box control field - type derived from field
STAR:RunDescription    LIKE(STAR:RunDescription)      !List box control field - type derived from field
STAR:RunDate           LIKE(STAR:RunDate)             !List box control field - type derived from field
STAR:RunTime           LIKE(STAR:RunTime)             !List box control field - type derived from field
Sattement_Type         LIKE(Sattement_Type)           !List box control field - type derived from local data
STAI:STIID             LIKE(STAI:STIID)               !List box control field - type derived from field
STAR:Type              LIKE(STAR:Type)                !Browse hot field - type derived from field
STAI:IID               LIKE(STAI:IID)                 !Browse key field - type derived from field
STAR:STRID             LIKE(STAR:STRID)               !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(ClientsPaymentsAllocation)
                       PROJECT(CLIPA:AllocationNo)
                       PROJECT(CLIPA:Amount)
                       PROJECT(CLIPA:AllocationDate)
                       PROJECT(CLIPA:Comment)
                       PROJECT(CLIPA:CPAID)
                       PROJECT(CLIPA:IID)
                       PROJECT(CLIPA:CPID)
                       JOIN(CLIP:PKey_CPID,CLIPA:CPID)
                         PROJECT(CLIP:DateMade)
                         PROJECT(CLIP:CPID)
                       END
                     END
Queue:Browse:Allocations QUEUE                        !Queue declaration for browse/combo box using ?List:Allocations
CLIP:DateMade          LIKE(CLIP:DateMade)            !List box control field - type derived from field
CLIP:CPID              LIKE(CLIP:CPID)                !List box control field - type derived from field
CLIPA:AllocationNo     LIKE(CLIPA:AllocationNo)       !List box control field - type derived from field
CLIPA:Amount           LIKE(CLIPA:Amount)             !List box control field - type derived from field
CLIPA:AllocationDate   LIKE(CLIPA:AllocationDate)     !List box control field - type derived from field
CLIPA:Comment          LIKE(CLIPA:Comment)            !List box control field - type derived from field
CLIPA:CPAID            LIKE(CLIPA:CPAID)              !Primary key field - type derived from field
CLIPA:IID              LIKE(CLIPA:IID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(InvoiceAlias)
                       PROJECT(A_INV:InvoiceDate)
                       PROJECT(A_INV:IID)
                       PROJECT(A_INV:InvoiceMessage)
                       PROJECT(A_INV:Total)
                       PROJECT(A_INV:Created_Date)
                       PROJECT(A_INV:CR_IID)
                     END
Queue:Browse:Credit_Note QUEUE                        !Queue declaration for browse/combo box using ?List:Credit_Note
A_INV:InvoiceDate      LIKE(A_INV:InvoiceDate)        !List box control field - type derived from field
A_INV:IID              LIKE(A_INV:IID)                !List box control field - type derived from field
A_INV:InvoiceMessage   LIKE(A_INV:InvoiceMessage)     !List box control field - type derived from field
A_INV:Total            LIKE(A_INV:Total)              !List box control field - type derived from field
A_INV:Created_Date     LIKE(A_INV:Created_Date)       !List box control field - type derived from field
A_INV:CR_IID           LIKE(A_INV:CR_IID)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::INV:Record  LIKE(INV:RECORD),THREAD
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW3::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW3::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW3::PopupChoice    SIGNED                       ! Popup current choice
BRW3::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW3::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW7::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW7::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW7::PopupChoice    SIGNED                       ! Popup current choice
BRW7::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW7::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW11::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW11::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW11::PopupChoice   SIGNED                       ! Popup current choice
BRW11::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW11::PopupChoiceExec BYTE(0)                    ! Popup executed
QuickWindow          WINDOW('Form Invoice'),AT(,,460,386),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('Update_Invoice'),SYSTEM
                       PROMPT('Invoice No. (IID):'),AT(10,8),USE(?INV:IID:Prompt:3),TRN
                       ENTRY(@n_10),AT(72,8,56,10),USE(INV:IID),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY,SKIP,MSG('Invoice Number')
                       PROMPT('Invoice Date:'),AT(234,8),USE(?INV:InvoiceDate:Prompt),TRN
                       SPIN(@d6),AT(282,8,56,10),USE(INV:InvoiceDate),RIGHT(1),MSG('Invoice Date'),TIP('Invoice Date')
                       BUTTON('...'),AT(342,8,12,10),USE(?Calendar_Inv_Date)
                       CHECK(' Printed'),AT(406,10,44,8),USE(INV:Printed),LEFT,DISABLE,MSG('Printed'),TIP('Printed')
                       PROMPT('Client:'),AT(10,22),USE(?CLI:ClientName:Prompt)
                       BUTTON('...'),AT(50,22,12,10),USE(?CallLookup_Client:2),TIP('Select Client / COD Client')
                       ENTRY(@s100),AT(72,22,152,10),USE(INV:ClientName),COLOR(00E9E9E9h),READONLY,SKIP,TIP('Client name')
                       PROMPT('Client No.:'),AT(234,22),USE(?INV:ClientNo:Prompt)
                       ENTRY(@n_10b),AT(282,22,56,10),USE(INV:ClientNo),RIGHT(1),COLOR(00E9E9E9h),MSG('Client No.'), |
  READONLY,SKIP,TIP('Client No.')
                       PROMPT('Total:'),AT(366,22),USE(?INV:Total:Prompt:2)
                       ENTRY(@n-14.2),AT(390,22,60,10),USE(INV:Total,,?INV:Total:2),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  READONLY,SKIP
                       PANEL,AT(4,4,452,34),USE(?Panel1),BEVEL(-1,1)
                       SHEET,AT(4,40,452,326),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('DID:'),AT(133,58),USE(?INV:DID:Prompt),TRN
                           STRING(@n_10b),AT(149,58),USE(INV:DID),RIGHT(1),TRN
                           PROMPT('DI No.:'),AT(9,58),USE(?INV:DINo:Prompt),TRN
                           ENTRY(@n_10b),AT(55,58,64,10),USE(INV:DINo),RIGHT(1),COLOR(00E9E9E9h),MSG('Delivery Ins' & |
  'truction Number'),READONLY,SKIP,TIP('Delivery Instruction Number')
                           PROMPT('Client Ref.:'),AT(9,71),USE(?INV:ClientReference:Prompt),TRN
                           ENTRY(@s60),AT(55,71,64,10),USE(INV:ClientReference),MSG('Client Reference'),TIP('Client Reference')
                           STRING(@n_10b),AT(149,71),USE(INV:CID),RIGHT(1),TRN
                           PROMPT('CID:'),AT(133,71),USE(?INV:CID:2),TRN
                           PROMPT('From Journal'),AT(212,58,78,10),USE(?Prompt_Journal),FONT(,,,FONT:bold,CHARSET:ANSI), |
  RIGHT,HIDE,TRN
                           GROUP,AT(326,58,118,10),USE(?Group_Credit)
                             PROMPT('Credit of Invoice (IID):'),AT(305,58),USE(?INV:CR_IID:Prompt:2),RIGHT,TRN
                             BUTTON(':'),AT(381,58,9,10),USE(?Button_JumpTo_Credit_of_Inv),TIP('Jump to Invoice')
                             ENTRY(@n_10),AT(394,58,56,10),USE(INV:CR_IID),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY,SKIP, |
  TRN,MSG('Credit of Invoice, Invoice IID of credited Invoice')
                           END
                           PROMPT('Creating MID:'),AT(309,71,69,10),USE(?INV:MID:Prompt:2),RIGHT,TRN
                           ENTRY(@n_10b),AT(394,71,56,10),USE(INV:MID),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY,SKIP,TRN,MSG('Generating MID')
                           PROMPT('MIDs:'),AT(309,84,69,10),USE(?INV:MIDs:Prompt),RIGHT,TRN
                           ENTRY(@s20),AT(394,84,56,10),USE(INV:MIDs),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('List of Ma' & |
  'nifest IDs that the delivery is currently manifested on'),READONLY,SKIP,TIP('List of Ma' & |
  'nifest IDs that the delivery is currently manifested on'),TRN
                           BUTTON('Update Status'),AT(274,98,64,12),USE(?Button_Upd_Status)
                           PROMPT('Status:'),AT(342,100,37,10),USE(?INV:Status:Prompt),RIGHT,TRN
                           LIST,AT(386,100,64,10),USE(INV:Status),DROP(15),FROM('No Payments|#0|Partially Paid|#1|' & |
  'Credit Note|#2|Fully Paid - Credit|#3|Fully Paid|#4|Credit Note Shown|#5|Bad Debt|#6' & |
  '|Over Paid|#7'),MSG('used for filtering - No Payments, Partially Paid, Credit Note, ' & |
  'Fully Paid - Credit, Fully Paid, Credit Note Shown'),TIP('No Payments, Partially Pai' & |
  'd, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note - Shown, Bad Debt, Over' & |
  ' Paid<0DH,0AH,0DH,0AH>Fully Paid, Credit Note Shown on a client statement')
                           PROMPT('Terms:'),AT(9,100),USE(?INV:Terms:Prompt),TRN
                           LIST,AT(55,100,65,10),USE(INV:Terms),COLOR(00E9E9E9h),DISABLE,DROP(5),FLAT,FROM('Pre Paid|#' & |
  '0|COD|#1|Account|#2'),MSG('Terms - Pre Paid, COD, Account'),SKIP,TIP('Terms - Pre Pa' & |
  'id, COD, Account')
                           LINE,AT(9,265,443,0),USE(?Line1:2),COLOR(COLOR:Black),LINEWIDTH(2)
                           ENTRY(@n_10),AT(55,87,65,10),USE(INV:POD_IID),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY,SKIP,MSG('POD No. - same as invoice except for Journal debit/credit of invoice')
                           PROMPT('POD IID:'),AT(9,87),USE(?INV:POD_IID:Prompt:2),RIGHT,TRN
                           PANEL,AT(10,122,438,143),USE(?Panel2),BEVEL(-1,-1)
                           PROMPT('Insurance:'),AT(22,127),USE(?INV:Insurance:Prompt),TRN
                           ENTRY(@n-14.2),AT(94,127,64,10),USE(INV:Insurance),RIGHT(1)
                           PROMPT('Documentation:'),AT(22,143),USE(?INV:Documentation:Prompt),TRN
                           ENTRY(@n-14.2),AT(94,143,64,10),USE(INV:Documentation),RIGHT(1)
                           PROMPT('Fuel Surcharge:'),AT(22,157),USE(?INV:FuelSurcharge:Prompt),TRN
                           ENTRY(@n-14.2),AT(94,157,64,10),USE(INV:FuelSurcharge),RIGHT(1)
                           PROMPT('Freight Charge:'),AT(22,170),USE(?INV:FreightCharge:Prompt),TRN
                           ENTRY(@n-14.2),AT(94,170,64,10),USE(INV:FreightCharge),RIGHT(1)
                           PROMPT('Additional Charge:'),AT(22,183),USE(?INV:AdditionalCharge:Prompt),TRN
                           ENTRY(@n-14.2),AT(94,183,64,10),USE(INV:AdditionalCharge),RIGHT(1),MSG('Addi'),TIP('Addi')
                           ENTRY(@n-14.2),AT(94,197,64,10),USE(INV:TollCharge),RIGHT(1)
                           PROMPT('Toll Charge:'),AT(22,196),USE(?INV:TollCharge:Prompt)
                           PROMPT('VAT:'),AT(22,231),USE(?INV:VAT:Prompt),TRN
                           ENTRY(@n-14.2),AT(94,231,64,10),USE(INV:VAT),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('VAT'),READONLY, |
  SKIP,TIP('VAT')
                           PROMPT('VAT Rate:'),AT(173,231),USE(?INV:VATRate:Prompt),TRN
                           ENTRY(@n-7.2),AT(213,231,37,10),USE(INV:VATRate),RIGHT(1),MSG('VAT rate'),TIP('VAT rate')
                           PROMPT('Total:'),AT(22,244),USE(?INV:Total:Prompt),TRN
                           ENTRY(@n-14.2),AT(94,244,64,10),USE(INV:Total),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                           PROMPT('Weight:'),AT(286,127),USE(?INV:Weight:Prompt),TRN
                           ENTRY(@n-14.2),AT(374,127,64,10),USE(INV:Weight),RIGHT(1),MSG('In kg''s'),TIP('In kg''s')
                           PROMPT('Volumetric Weight:'),AT(286,143),USE(?INV:VolumetricWeight:Prompt),TRN
                           ENTRY(@n-14.2),AT(374,143,64,10),USE(INV:VolumetricWeight),RIGHT(1),MSG('Weight based o' & |
  'n Volumetric calculation'),TIP('Weight based on Volumetric calculation (in kgs)')
                           PROMPT('Volume:'),AT(286,157),USE(?INV:Volume:Prompt),TRN
                           ENTRY(@n-14.2),AT(374,157,64,10),USE(INV:Volume),RIGHT(1),MSG('Volume for manual entry'),TIP('Volume for' & |
  ' manual entry (metres cubed)')
                           PROMPT('Total Allocated Payments:'),AT(286,231,,10),USE(?L_SG:Paid:Prompt),RIGHT,TRN
                           ENTRY(@n-17.2),AT(374,231,64,10),USE(L_SG:Paid),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Paid amount'), |
  READONLY,SKIP,TIP('Paid amount')
                           PROMPT('Total Credits:'),AT(286,244),USE(?L_SG:Credits:Prompt),TRN
                           ENTRY(@n-17.2),AT(374,244,64,10),USE(L_SG:Credits),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP
                           PROMPT('Invoice Items:'),AT(9,268),USE(?Prompt51)
                           LIST,AT(9,279,443,81),USE(?Browse:2),HVSCROLL,FORMAT('80L(2)|M~Description~C(0)@s150@80' & |
  'L(2)|M~Item Commodity~C(0)@s35@80L(2)|M~Container Description~C(0)@s150@28R(2)|M~Uni' & |
  'ts~C(0)@n6b@36L(2)|M~Item Type~C(0)@s20@32R(2)|M~Item No.~C(0)@n6@40R(2)|M~ITID~C(0)' & |
  '@n_10@40R(2)|M~DIID~C(0)@n_10@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the _Invoice' & |
  'Items file')
                           BUTTON('&Insert'),AT(213,330,42,12),USE(?Insert),HIDE
                           BUTTON('&Change'),AT(254,330,42,12),USE(?Change),HIDE
                           BUTTON('&Delete'),AT(297,330,42,12),USE(?Delete),HIDE
                           CHECK(' Bad Debt'),AT(403,43),USE(INV:BadDebt),MSG('This is a Bad Debt Credit Note'),TIP('This is a ' & |
  'Bad Debt Credit Note')
                         END
                         TAB('&2) Notes && Address'),USE(?Tab5)
                           PROMPT('Invoice Message:'),AT(10,58),USE(?INV:InvoiceMessage:Prompt:2),TRN
                           TEXT,AT(101,58,233,42),USE(INV:InvoiceMessage),VSCROLL,BOXED
                           LINE,AT(10,109,441,0),USE(?Line2:2),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Client Line 1:'),AT(10,114),USE(?INV:ClientLine1:Prompt),TRN
                           ENTRY(@s35),AT(101,114,144,10),USE(INV:ClientLine1),MSG('Address line 1'),TIP('Address line 1')
                           PROMPT('Client Line 2:'),AT(10,127),USE(?INV:ClientLine2:Prompt),TRN
                           ENTRY(@s35),AT(101,127,144,10),USE(INV:ClientLine2),MSG('Address line 2'),TIP('Address line 2')
                           PROMPT('Client Suburb:'),AT(10,143),USE(?INV:ClientSuburb:Prompt),TRN
                           ENTRY(@s50),AT(101,143,144,10),USE(INV:ClientSuburb),MSG('Suburb'),TIP('Suburb')
                           PROMPT('Client Postal Code:'),AT(10,157),USE(?INV:ClientPostalCode:Prompt),TRN
                           ENTRY(@s10),AT(101,157,64,10),USE(INV:ClientPostalCode)
                           PROMPT('VAT No.:'),AT(10,175),USE(?INV:VATNo:Prompt),TRN
                           ENTRY(@s20),AT(101,175,64,10),USE(INV:VATNo),MSG('VAT No.'),TIP('VAT No.')
                           BUTTON('...'),AT(82,218,12,10),USE(?CallLookup)
                           LINE,AT(9,214,441,0),USE(?Line6),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Branch:'),AT(9,218),USE(?BRA:BranchName:Prompt),TRN
                           ENTRY(@s35),AT(101,218,144,10),USE(INV:BranchName),COLOR(00E9E9E9h),MSG('Branch Name'),READONLY, |
  SKIP,TIP('Branch Name')
                           PROMPT('Created Date/Time:'),AT(9,266),USE(?INV:Created_Date:Prompt),TRN
                           STRING(@d6b),AT(101,266),USE(INV:Created_Date),TRN
                           STRING(@t4),AT(153,266),USE(INV:Created_Time),TRN
                           PROMPT('Invoice Time:'),AT(9,234),USE(?INV:InvoiceTime:Prompt),TRN
                           ENTRY(@t7),AT(101,234,64,10),USE(INV:InvoiceTime),RIGHT(1),MSG('Invoice Time'),TIP('Invoice Time')
                           PROMPT('User Login:'),AT(9,250),USE(?LOC:Login:Prompt),TRN
                           ENTRY(@s20),AT(101,250,64,10),USE(LOC:Login),COLOR(00E9E9E9h),MSG('User Login'),READONLY,SKIP, |
  TIP('User Login')
                           PROMPT('Journal ID:'),AT(246,250),USE(?INV:IJID:Prompt),TRN
                           STRING(@n_10b),AT(289,250),USE(INV:IJID),RIGHT(1),TRN
                           PROMPT('COD Add. ID:'),AT(246,266),USE(?INV:DC_ID:Prompt),TRN
                           STRING(@n_10b),AT(289,266),USE(INV:DC_ID),RIGHT(1),TRN
                         END
                         TAB('&3) Shipper && Consignee'),USE(?Tab6)
                           GROUP,AT(9,59,324,144),USE(?Group1)
                             PROMPT('Collected From:'),AT(9,59),USE(?INV:ShipperName:Prompt),TRN
                             ENTRY(@s35),AT(101,59,144,10),USE(INV:ShipperName),MSG('Name of this address'),TIP('Name of th' & |
  'is address')
                             PROMPT('Shipper Line 1:'),AT(9,72),USE(?INV:ShipperLine1:Prompt),TRN
                             ENTRY(@s35),AT(101,72,144,10),USE(INV:ShipperLine1),MSG('Address line 1'),TIP('Address line 1')
                             PROMPT('Shipper Line 2:'),AT(9,88),USE(?INV:ShipperLine2:Prompt),TRN
                             ENTRY(@s35),AT(101,88,144,10),USE(INV:ShipperLine2),MSG('Address line 2'),TIP('Address line 2')
                             PROMPT('Shipper Suburb:'),AT(9,101),USE(?INV:ShipperSuburb:Prompt),TRN
                             ENTRY(@s50),AT(101,101,144,10),USE(INV:ShipperSuburb),MSG('Suburb'),TIP('Suburb')
                             PROMPT('Shipper Postal Code:'),AT(9,115),USE(?INV:ShipperPostalCode:Prompt),TRN
                             ENTRY(@s10),AT(101,115,44,10),USE(INV:ShipperPostalCode)
                             LINE,AT(11,130,440,0),USE(?Line2),COLOR(COLOR:Black),LINEWIDTH(2)
                             PROMPT('Consignee Name:'),AT(9,136),USE(?INV:ConsigneeName:Prompt),TRN
                             ENTRY(@s35),AT(101,136,144,10),USE(INV:ConsigneeName),MSG('Name of this address'),TIP('Name of th' & |
  'is address')
                             PROMPT('Consignee Line 1:'),AT(9,152),USE(?INV:ConsigneeLine1:Prompt),TRN
                             ENTRY(@s35),AT(101,152,144,10),USE(INV:ConsigneeLine1),MSG('Address line 1'),TIP('Address line 1')
                             PROMPT('Consignee Line 2:'),AT(9,165),USE(?INV:ConsigneeLine2:Prompt),TRN
                             ENTRY(@s35),AT(101,165,144,10),USE(INV:ConsigneeLine2),MSG('Address line 2'),TIP('Address line 2')
                             PROMPT('Consignee Suburb:'),AT(9,179),USE(?INV:ConsigneeSuburb:Prompt),TRN
                             ENTRY(@s50),AT(101,179,144,10),USE(INV:ConsigneeSuburb),MSG('Suburb'),TIP('Suburb')
                             PROMPT('Consignee Postal Code:'),AT(9,192),USE(?INV:ConsigneePostalCode:Prompt),TRN
                             ENTRY(@s10),AT(101,192,44,10),USE(INV:ConsigneePostalCode)
                           END
                         END
                         TAB('&4) Statements && Payments'),USE(?Tab4)
                           PROMPT('Statements this Invoice / Credit Note appears on:'),AT(8,58),USE(?Prompt52),TRN
                           LIST,AT(8,71,443,82),USE(?List),HVSCROLL,FORMAT('[30R(1)|M~DI No.~L(2)@n_10@38R(1)|M~De' & |
  'bit~L(2)@n-14.2@38R(1)|M~Credit~L(2)@n-14.2@38R(1)|M~Amount~L(2)@n-14.2@]|M~Statemen' & |
  't Item~[48R(1)|M~Statement ID~L(2)@n_10@37R(1)|M~Date~L(2)@d5b@36R(1)|M~Time~L(2)@t7' & |
  '@70L(1)|M~Run Description~L(2)@s35@40R(1)|M~Run Date~L(2)@d5@36R(1)|M~Run Time~L(2)@' & |
  't7@44L(1)|M~Type~L(2)@s10@]|M~Statement~30R(1)|M~STIID~L(2)@n_10@'),FROM(Queue:Browse), |
  IMM,MSG('Browsing Records')
                           LINE,AT(7,159,443,0),USE(?Line5),COLOR(COLOR:Black),LINEWIDTH(2)
                           BUTTON('Jump to Payment'),AT(381,162,,10),USE(?Button_Jump_To_Payment),TIP('Open the Pa' & |
  'yment for the selected Allocation<0DH,0AH>(while leaving the current Invoice open)')
                           PROMPT('Payment Allocations made to this Invoice:'),AT(8,165),USE(?Prompt52:2),TRN
                           LIST,AT(8,178,443,70),USE(?List:Allocations),HVSCROLL,FORMAT('[38R(2)|M~Date~C(0)@d5@40' & |
  'R(2)|M~CPID~C(0)@n_10@]|M~Payment~[24R(2)|M~No.~L@n_5@56R(2)|M~Amount~L@n-14.2@40R(2' & |
  ')|M~Date~L@d5@100L(2)|M~Comment~@s255@]|M~Allocation~'),FROM(Queue:Browse:Allocations), |
  IMM,MSG('Browsing Records')
                           PROMPT('Credit Notes against this Invoice:'),AT(8,253),USE(?Prompt52:3),TRN
                           BUTTON('Jump to Credit'),AT(381,250,71,10),USE(?Button_Jump_To_Credit),TIP('Open the se' & |
  'lected Credit Note<0DH,0AH>(while leaving the current Invoice open)')
                           LIST,AT(8,266,443,70),USE(?List:Credit_Note),VSCROLL,FORMAT('38R(1)|M~Invoice Date~L(2)' & |
  '@d5b@40R(1)|M~Credit No.~L(2)@n_10@147L(1)|M~Message~L(2)@s255@46R(1)|M~Total~L(2)@n' & |
  '-15.2@40R(1)|M~Created Date~L(2)@d5b@'),FROM(Queue:Browse:Credit_Note),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('&Print'),AT(4,370,,14),USE(?Button_Print),LEFT,ICON(ICON:Print1),FLAT
                       BUTTON('&OK'),AT(354,370,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(406,370,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(302,370,47,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW2::LastSortOrder       BYTE
BRW3::LastSortOrder       BYTE
BRW7::LastSortOrder       BYTE
BRW11::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW3                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW_Allocations      CLASS(BrowseClass)                    ! Browse using ?List:Allocations
Q                      &Queue:Browse:Allocations      !Reference to browse queue
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW_Credit_Note      CLASS(BrowseClass)                    ! Browse using ?List:Credit_Note
Q                      &Queue:Browse:Credit_Note      !Reference to browse queue
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW11::Sort0:Locator StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar13           CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Calc_Inv                                            ROUTINE
   INV:VAT     = (INV:Insurance + INV:Documentation + INV:FuelSurcharge + INV:TollCharge + INV:AdditionalCharge + INV:FreightCharge) * (INV:VATRate / 100)

   INV:Total   = INV:Insurance + INV:Documentation + INV:FuelSurcharge + INV:TollCharge + INV:AdditionalCharge + INV:FreightCharge + INV:VAT
   
   IF INV:Total < 0.0
      ENABLE(?INV:BadDebt)
   ELSE
      IF INV:BadDebt <> 0
         MESSAGE('Bad Debt is selected but this Invoice has a positive amount, Bad Debt will be de-selected','Invoice Validation',ICON:Exclamation)
         INV:BadDebt = FALSE
      .
      DISABLE(?INV:BadDebt)
   .
   
   DISPLAY
   EXIT
Client_Address_From_General           ROUTINE
    CLI:CID                 = INV:CID
    IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
       INV:ClientName       = CLI:ClientName
       INV:VATNo            = CLI:VATNo

       A_ADD:AID            = CLI:AID

       IF Access:AddressAlias.TryFetch(A_ADD:PKey_AID) = LEVEL:Benign
          INV:ClientLine1   = A_ADD:Line1
          INV:ClientLine2   = A_ADD:Line2

          SUBU:SUID         = A_ADD:SUID
          IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) = LEVEL:Benign
             INV:ClientSuburb     = SUBU:Suburb
             INV:ClientPostalCode = SUBU:PostalCode
    .  .  .
    EXIT
Client_Address_From_COD                ROUTINE
    DCADD:DC_ID             = INV:DC_ID
    IF Access:Delivery_CODAddresses.TryFetch(DCADD:PKey_DC_ID) = LEVEL:Benign
       INV:ClientName       = DCADD:AddressName
       INV:VATNo            = DCADD:VATNo

       INV:ClientLine1      = DCADD:Line1
       INV:ClientLine2      = DCADD:Line2
       INV:ClientSuburb     = DCADD:Line3
       INV:ClientPostalCode = DCADD:Line4
    ELSE
       MESSAGE('Delivery COD Address is missing - DC_ID: ' & A_DEL:DC_ID, 'Update Invoice', ICON:Hand)
    .
    EXIT
! ---------------------------------------------------
Check_Omitted                                   ROUTINE
    IF ~OMITTED(1)
       ! Then load this IID no.
       INV:IID  = p:str_IID
       IF Access:_Invoice.TryFetch(INV:PKey_IID) = LEVEL:Benign

    .  .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Invoice Record'
  OF InsertRecord
    ActionMessage = 'Invoice Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Invoice Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Invoice')
      DO Check_Omitted
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?INV:IID:Prompt:3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_SG:ItemType',L_SG:ItemType)             ! Added by: BrowseBox(ABC)
  BIND('Sattement_Type',Sattement_Type)           ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(INV:Record,History::INV:Record)
  SELF.AddHistoryField(?INV:IID,1)
  SELF.AddHistoryField(?INV:InvoiceDate,36)
  SELF.AddHistoryField(?INV:Printed,38)
  SELF.AddHistoryField(?INV:ClientName,9)
  SELF.AddHistoryField(?INV:ClientNo,8)
  SELF.AddHistoryField(?INV:Total:2,46)
  SELF.AddHistoryField(?INV:DID,16)
  SELF.AddHistoryField(?INV:DINo,17)
  SELF.AddHistoryField(?INV:ClientReference,18)
  SELF.AddHistoryField(?INV:CID,6)
  SELF.AddHistoryField(?INV:CR_IID,3)
  SELF.AddHistoryField(?INV:MID,55)
  SELF.AddHistoryField(?INV:MIDs,19)
  SELF.AddHistoryField(?INV:Status,53)
  SELF.AddHistoryField(?INV:Terms,20)
  SELF.AddHistoryField(?INV:POD_IID,2)
  SELF.AddHistoryField(?INV:Insurance,40)
  SELF.AddHistoryField(?INV:Documentation,41)
  SELF.AddHistoryField(?INV:FuelSurcharge,42)
  SELF.AddHistoryField(?INV:FreightCharge,44)
  SELF.AddHistoryField(?INV:AdditionalCharge,43)
  SELF.AddHistoryField(?INV:TollCharge,47)
  SELF.AddHistoryField(?INV:VAT,45)
  SELF.AddHistoryField(?INV:VATRate,48)
  SELF.AddHistoryField(?INV:Total,46)
  SELF.AddHistoryField(?INV:Weight,49)
  SELF.AddHistoryField(?INV:VolumetricWeight,50)
  SELF.AddHistoryField(?INV:Volume,51)
  SELF.AddHistoryField(?INV:BadDebt,52)
  SELF.AddHistoryField(?INV:InvoiceMessage,15)
  SELF.AddHistoryField(?INV:ClientLine1,10)
  SELF.AddHistoryField(?INV:ClientLine2,11)
  SELF.AddHistoryField(?INV:ClientSuburb,12)
  SELF.AddHistoryField(?INV:ClientPostalCode,13)
  SELF.AddHistoryField(?INV:VATNo,14)
  SELF.AddHistoryField(?INV:BranchName,5)
  SELF.AddHistoryField(?INV:Created_Date,61)
  SELF.AddHistoryField(?INV:Created_Time,62)
  SELF.AddHistoryField(?INV:InvoiceTime,37)
  SELF.AddHistoryField(?INV:IJID,58)
  SELF.AddHistoryField(?INV:DC_ID,57)
  SELF.AddHistoryField(?INV:ShipperName,23)
  SELF.AddHistoryField(?INV:ShipperLine1,24)
  SELF.AddHistoryField(?INV:ShipperLine2,25)
  SELF.AddHistoryField(?INV:ShipperSuburb,26)
  SELF.AddHistoryField(?INV:ShipperPostalCode,27)
  SELF.AddHistoryField(?INV:ConsigneeName,29)
  SELF.AddHistoryField(?INV:ConsigneeLine1,30)
  SELF.AddHistoryField(?INV:ConsigneeLine2,31)
  SELF.AddHistoryField(?INV:ConsigneeSuburb,32)
  SELF.AddHistoryField(?INV:ConsigneePostalCode,33)
  SELF.AddUpdateFile(Access:_Invoice)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Add_Suburbs.SetOpenRelated()
  Relate:Add_Suburbs.Open                         ! File Add_Suburbs used by this procedure, so make sure it's RelationManager is open
  Relate:InvoiceAlias.Open                        ! File InvoiceAlias used by this procedure, so make sure it's RelationManager is open
  Access:Branches.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:AddressAlias.UseFile                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Delivery_CODAddresses.UseFile            ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_Invoice
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:_InvoiceItems,SELF) ! Initialize the browse manager
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:_StatementItems,SELF) ! Initialize the browse manager
  BRW_Allocations.Init(?List:Allocations,Queue:Browse:Allocations.ViewPosition,BRW7::View:Browse,Queue:Browse:Allocations,Relate:ClientsPaymentsAllocation,SELF) ! Initialize the browse manager
  BRW_Credit_Note.Init(?List:Credit_Note,Queue:Browse:Credit_Note.ViewPosition,BRW11::View:Browse,Queue:Browse:Credit_Note,Relate:InvoiceAlias,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  ?Browse:2{Prop:LineHeight} = 10
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    DISABLE(?INV:InvoiceDate)
    DISABLE(?Calendar_Inv_Date)
    DISABLE(?CallLookup_Client:2)
    ?INV:ClientName{PROP:ReadOnly} = True
    ?INV:ClientNo{PROP:ReadOnly} = True
    ?INV:Total:2{PROP:ReadOnly} = True
    ?INV:DINo{PROP:ReadOnly} = True
    ?INV:ClientReference{PROP:ReadOnly} = True
    DISABLE(?Button_JumpTo_Credit_of_Inv)
    ?INV:MIDs{PROP:ReadOnly} = True
    DISABLE(?Button_Upd_Status)
    DISABLE(?INV:Status)
    DISABLE(?INV:Terms)
    ?INV:Insurance{PROP:ReadOnly} = True
    ?INV:Documentation{PROP:ReadOnly} = True
    ?INV:FuelSurcharge{PROP:ReadOnly} = True
    ?INV:FreightCharge{PROP:ReadOnly} = True
    ?INV:AdditionalCharge{PROP:ReadOnly} = True
    ?INV:TollCharge{PROP:ReadOnly} = True
    ?INV:VAT{PROP:ReadOnly} = True
    ?INV:VATRate{PROP:ReadOnly} = True
    ?INV:Total{PROP:ReadOnly} = True
    ?INV:Weight{PROP:ReadOnly} = True
    ?INV:VolumetricWeight{PROP:ReadOnly} = True
    ?INV:Volume{PROP:ReadOnly} = True
    ?L_SG:Paid{PROP:ReadOnly} = True
    ?L_SG:Credits{PROP:ReadOnly} = True
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
    ?INV:InvoiceMessage{PROP:ReadOnly} = True
    ?INV:ClientLine1{PROP:ReadOnly} = True
    ?INV:ClientLine2{PROP:ReadOnly} = True
    ?INV:ClientSuburb{PROP:ReadOnly} = True
    ?INV:ClientPostalCode{PROP:ReadOnly} = True
    ?INV:VATNo{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?INV:BranchName{PROP:ReadOnly} = True
    ?INV:InvoiceTime{PROP:ReadOnly} = True
    ?LOC:Login{PROP:ReadOnly} = True
    ?INV:ShipperName{PROP:ReadOnly} = True
    ?INV:ShipperLine1{PROP:ReadOnly} = True
    ?INV:ShipperLine2{PROP:ReadOnly} = True
    ?INV:ShipperSuburb{PROP:ReadOnly} = True
    ?INV:ShipperPostalCode{PROP:ReadOnly} = True
    ?INV:ConsigneeName{PROP:ReadOnly} = True
    ?INV:ConsigneeLine1{PROP:ReadOnly} = True
    ?INV:ConsigneeLine2{PROP:ReadOnly} = True
    ?INV:ConsigneeSuburb{PROP:ReadOnly} = True
    ?INV:ConsigneePostalCode{PROP:ReadOnly} = True
    DISABLE(?Button_Jump_To_Payment)
    DISABLE(?Button_Jump_To_Credit)
    DISABLE(?Button_Print)
  END
  CASE SELF.Request                ! Set state of INV:BadDebt field
  OF ViewRecord   
     DISABLE(?INV:BadDebt)
  OF ChangeRecord   
     IF INV:Total > 0.0
        DISABLE(?INV:BadDebt)
  .  .
  
      
        ENABLE(?Button_Upd_Status)
        ENABLE(?Button_Jump_To_Payment)
        ENABLE(?Button_Jump_To_Credit)
        ENABLE(?Button_Print)
        ENABLE(?Button_JumpTo_Credit_of_Inv)
  
  BRW2.Q &= Queue:Browse:2
  BRW2.FileLoaded = 1                             ! This is a 'file loaded' browse
  BRW2.AddSortOrder(,INI:FKey_IID)                ! Add the sort order for INI:FKey_IID for sort order 1
  BRW2.AddRange(INI:IID,Relate:_InvoiceItems,Relate:_Invoice) ! Add file relationship range limit for sort order 1
  BRW2.AppendOrder('+INI:ItemNo')                 ! Append an additional sort order
  BRW2.AddField(INI:Description,BRW2.Q.INI:Description) ! Field INI:Description is a hot field or requires assignment from browse
  BRW2.AddField(INI:Commodity,BRW2.Q.INI:Commodity) ! Field INI:Commodity is a hot field or requires assignment from browse
  BRW2.AddField(INI:ContainerDescription,BRW2.Q.INI:ContainerDescription) ! Field INI:ContainerDescription is a hot field or requires assignment from browse
  BRW2.AddField(INI:Units,BRW2.Q.INI:Units)       ! Field INI:Units is a hot field or requires assignment from browse
  BRW2.AddField(L_SG:ItemType,BRW2.Q.L_SG:ItemType) ! Field L_SG:ItemType is a hot field or requires assignment from browse
  BRW2.AddField(INI:ItemNo,BRW2.Q.INI:ItemNo)     ! Field INI:ItemNo is a hot field or requires assignment from browse
  BRW2.AddField(INI:ITID,BRW2.Q.INI:ITID)         ! Field INI:ITID is a hot field or requires assignment from browse
  BRW2.AddField(INI:DIID,BRW2.Q.INI:DIID)         ! Field INI:DIID is a hot field or requires assignment from browse
  BRW2.AddField(INI:Type,BRW2.Q.INI:Type)         ! Field INI:Type is a hot field or requires assignment from browse
  BRW2.AddField(INI:IID,BRW2.Q.INI:IID)           ! Field INI:IID is a hot field or requires assignment from browse
  BRW3.Q &= Queue:Browse
  BRW3.AddSortOrder(,STAI:FKey_IID)               ! Add the sort order for STAI:FKey_IID for sort order 1
  BRW3.AddRange(STAI:IID,INV:IID)                 ! Add single value range limit for sort order 1
  BRW3.AddLocator(BRW3::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW3::Sort0:Locator.Init(,STAI:IID,1,BRW3)      ! Initialize the browse locator using  using key: STAI:FKey_IID , STAI:IID
  BRW3.AppendOrder('-STAR:RunDateTime,+STAI:STIID') ! Append an additional sort order
  BRW3.AddField(STAI:DINo,BRW3.Q.STAI:DINo)       ! Field STAI:DINo is a hot field or requires assignment from browse
  BRW3.AddField(STAI:Debit,BRW3.Q.STAI:Debit)     ! Field STAI:Debit is a hot field or requires assignment from browse
  BRW3.AddField(STAI:Credit,BRW3.Q.STAI:Credit)   ! Field STAI:Credit is a hot field or requires assignment from browse
  BRW3.AddField(STAI:Amount,BRW3.Q.STAI:Amount)   ! Field STAI:Amount is a hot field or requires assignment from browse
  BRW3.AddField(STA:STID,BRW3.Q.STA:STID)         ! Field STA:STID is a hot field or requires assignment from browse
  BRW3.AddField(STA:StatementDate,BRW3.Q.STA:StatementDate) ! Field STA:StatementDate is a hot field or requires assignment from browse
  BRW3.AddField(STA:StatementTime,BRW3.Q.STA:StatementTime) ! Field STA:StatementTime is a hot field or requires assignment from browse
  BRW3.AddField(STAR:RunDescription,BRW3.Q.STAR:RunDescription) ! Field STAR:RunDescription is a hot field or requires assignment from browse
  BRW3.AddField(STAR:RunDate,BRW3.Q.STAR:RunDate) ! Field STAR:RunDate is a hot field or requires assignment from browse
  BRW3.AddField(STAR:RunTime,BRW3.Q.STAR:RunTime) ! Field STAR:RunTime is a hot field or requires assignment from browse
  BRW3.AddField(Sattement_Type,BRW3.Q.Sattement_Type) ! Field Sattement_Type is a hot field or requires assignment from browse
  BRW3.AddField(STAI:STIID,BRW3.Q.STAI:STIID)     ! Field STAI:STIID is a hot field or requires assignment from browse
  BRW3.AddField(STAR:Type,BRW3.Q.STAR:Type)       ! Field STAR:Type is a hot field or requires assignment from browse
  BRW3.AddField(STAI:IID,BRW3.Q.STAI:IID)         ! Field STAI:IID is a hot field or requires assignment from browse
  BRW3.AddField(STAR:STRID,BRW3.Q.STAR:STRID)     ! Field STAR:STRID is a hot field or requires assignment from browse
  BRW_Allocations.Q &= Queue:Browse:Allocations
  BRW_Allocations.AddSortOrder(,CLIPA:Fkey_IID)   ! Add the sort order for CLIPA:Fkey_IID for sort order 1
  BRW_Allocations.AddRange(CLIPA:IID,INV:IID)     ! Add single value range limit for sort order 1
  BRW_Allocations.AddLocator(BRW7::Sort0:Locator) ! Browse has a locator for sort order 1
  BRW7::Sort0:Locator.Init(,CLIPA:IID,1,BRW_Allocations) ! Initialize the browse locator using  using key: CLIPA:Fkey_IID , CLIPA:IID
  BRW_Allocations.AppendOrder('+CLIPA:AllocationDate,+CLIPA:AllocationNo,+CLIPA:CPAID') ! Append an additional sort order
  BRW_Allocations.AddField(CLIP:DateMade,BRW_Allocations.Q.CLIP:DateMade) ! Field CLIP:DateMade is a hot field or requires assignment from browse
  BRW_Allocations.AddField(CLIP:CPID,BRW_Allocations.Q.CLIP:CPID) ! Field CLIP:CPID is a hot field or requires assignment from browse
  BRW_Allocations.AddField(CLIPA:AllocationNo,BRW_Allocations.Q.CLIPA:AllocationNo) ! Field CLIPA:AllocationNo is a hot field or requires assignment from browse
  BRW_Allocations.AddField(CLIPA:Amount,BRW_Allocations.Q.CLIPA:Amount) ! Field CLIPA:Amount is a hot field or requires assignment from browse
  BRW_Allocations.AddField(CLIPA:AllocationDate,BRW_Allocations.Q.CLIPA:AllocationDate) ! Field CLIPA:AllocationDate is a hot field or requires assignment from browse
  BRW_Allocations.AddField(CLIPA:Comment,BRW_Allocations.Q.CLIPA:Comment) ! Field CLIPA:Comment is a hot field or requires assignment from browse
  BRW_Allocations.AddField(CLIPA:CPAID,BRW_Allocations.Q.CLIPA:CPAID) ! Field CLIPA:CPAID is a hot field or requires assignment from browse
  BRW_Allocations.AddField(CLIPA:IID,BRW_Allocations.Q.CLIPA:IID) ! Field CLIPA:IID is a hot field or requires assignment from browse
  BRW_Credit_Note.Q &= Queue:Browse:Credit_Note
  BRW_Credit_Note.FileLoaded = 1                  ! This is a 'file loaded' browse
  BRW_Credit_Note.AddSortOrder(,A_INV:Key_CR_IID) ! Add the sort order for A_INV:Key_CR_IID for sort order 1
  BRW_Credit_Note.AddRange(A_INV:CR_IID,INV:IID)  ! Add single value range limit for sort order 1
  BRW_Credit_Note.AddLocator(BRW11::Sort0:Locator) ! Browse has a locator for sort order 1
  BRW11::Sort0:Locator.Init(,A_INV:CR_IID,1,BRW_Credit_Note) ! Initialize the browse locator using  using key: A_INV:Key_CR_IID , A_INV:CR_IID
  BRW_Credit_Note.AppendOrder('+A_INV:InvoiceDate,+A_INV:IID') ! Append an additional sort order
  BRW_Credit_Note.AddField(A_INV:InvoiceDate,BRW_Credit_Note.Q.A_INV:InvoiceDate) ! Field A_INV:InvoiceDate is a hot field or requires assignment from browse
  BRW_Credit_Note.AddField(A_INV:IID,BRW_Credit_Note.Q.A_INV:IID) ! Field A_INV:IID is a hot field or requires assignment from browse
  BRW_Credit_Note.AddField(A_INV:InvoiceMessage,BRW_Credit_Note.Q.A_INV:InvoiceMessage) ! Field A_INV:InvoiceMessage is a hot field or requires assignment from browse
  BRW_Credit_Note.AddField(A_INV:Total,BRW_Credit_Note.Q.A_INV:Total) ! Field A_INV:Total is a hot field or requires assignment from browse
  BRW_Credit_Note.AddField(A_INV:Created_Date,BRW_Credit_Note.Q.A_INV:Created_Date) ! Field A_INV:Created_Date is a hot field or requires assignment from browse
  BRW_Credit_Note.AddField(A_INV:CR_IID,BRW_Credit_Note.Q.A_INV:CR_IID) ! Field A_INV:CR_IID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_Invoice',QuickWindow)      ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      BRA:BID     = INV:BID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
      .
      L_SG:Paid       = Get_ClientsPay_Alloc_Amt(INV:IID, 0)     ! Decimal in string
      L_SG:Credits    = -Get_Inv_Credited(INV:IID)               ! Credited is negative
  
  BRW2.AskProcedure = 2                           ! Will call: Update_Invoice_Items
  BRW3.AddToolbarTarget(Toolbar)                  ! Browse accepts toolbar control
  BRW3.ToolbarItem.HelpButton = ?Help
  BRW_Allocations.AddToolbarTarget(Toolbar)       ! Browse accepts toolbar control
  BRW_Allocations.ToolbarItem.HelpButton = ?Help
  BRW_Credit_Note.AddToolbarTarget(Toolbar)       ! Browse accepts toolbar control
  BRW_Credit_Note.ToolbarItem.HelpButton = ?Help
      IF SELF.Request = InsertRecord
         INV:VATRate      = Get_Setup_Info(4)
         INV:InvoiceDate  = TODAY()
         INV:InvoiceTime  = CLOCK()
  
         INV:UID          = GLO:UID
      .
      IF INV:Status = 2 OR INV:Status = 5 OR INV:Status = 6    !  OR INV:Status = 3 NOT, see below
         UNHIDE(?Group_Credit)
  
         CASE INV:Status
         OF 2 OROF 5                        ! NOT OROF 3  this is an Invoice status actually, meaning paid by Credit Note I think... 25.03.14
            ?INV:IID:Prompt:3{PROP:Text}    = 'Credit No. (IID):'
  !          QuickWindow{PROP:Text}          = 'Credit Record Will Be'
         OF 6
            ?INV:IID:Prompt:3{PROP:Text}    = 'Bad Debt No. (IID):'
  !          QuickWindow{PROP:Text}          = 'Credit Record Will Be'
         .
  
  !       IF SELF.Request = InsertRecord
  !          QuickWindow{PROP:Text}    = CLIP(QuickWindow{PROP:Text}) & ' Added'
  !       ELSIF SELF.Request = ChangeRecord
  !          QuickWindow{PROP:Text}    = CLIP(QuickWindow{PROP:Text}) & ' Changed'
  !       .
      ELSE
         HIDE(?Group_Credit)
      .
  
  
      ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown, Bad Debt, Fully Paid
      !       0           1              2               3                    4           5               6         7 
      IF INV:IID ~= INV:POD_IID AND INV:CR_IID ~= 0 AND INV:Total > 0.0     ! really we need a record flag for this!
         UNHIDE(?Prompt_Journal)
      .
      IF INV:DINo = 0                 ! This is an Extra invoice most likely - allow the changing of this then
         ?INV:DINo{PROP:ReadOnly}     = FALSE
         ?INV:DINo{PROP:Background}   = -1
      .
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_Invoice',1,?Browse:2,2,BRW2::PopupTextExt,Queue:Browse:2,8,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW3::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW3::FormatManager.Init('MANTRNIS','Update_Invoice',1,?List,3,BRW3::PopupTextExt,Queue:Browse,12,LFM_CFile,LFM_CFile.Record)
  BRW3::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW7::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW7::FormatManager.Init('MANTRNIS','Update_Invoice',1,?List:Allocations,7,BRW7::PopupTextExt,Queue:Browse:Allocations,6,LFM_CFile,LFM_CFile.Record)
  BRW7::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW11::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW11::FormatManager.Init('MANTRNIS','Update_Invoice',1,?List:Credit_Note,11,BRW11::PopupTextExt,Queue:Browse:Credit_Note,5,LFM_CFile,LFM_CFile.Record)
  BRW11::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
      Get_User_Info(1, LOC:Login, INV:UID)
      ! (p:Info, p:Value, p:UID, p:Login)
      ! (BYTE, *STRING, <ULONG>, <STRING>),LONG,PROC
      !   p:Info  1   - Login
      !           2   - Password
      !           3   - User Name + Surname
      !           4   - Name
      !           5   - Surname
      !           6   - Access Level
      !           7   - UID
  
      
     IF SELF.Request = ViewRecord              ! Move window a bit and change colour
        ! Then move window a bit and colour
           
        QuickWindow{PROP:Xpos}  = QuickWindow{PROP:Xpos} + 15
        QuickWindow{PROP:Ypos}  = QuickWindow{PROP:Ypos} + 15
  
        QuickWindow{PROP:Background} = 00E6F0FAh
     .
     
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Suburbs.Close
    Relate:InvoiceAlias.Close
  END
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW3::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW7::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW11::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_Invoice',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  INV:BID = GLO:BranchID
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Branches
      Update_Invoice_Items
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Jump_To_Payment
      BRW_Allocations.UpdateViewRecord()
          L_PG:CPID   = CLIPA:CPID
    OF ?Button_Jump_To_Credit
      BRW_Credit_Note.UpdateViewRecord()
          L_PG:IID        = A_INV:IID
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar_Inv_Date
      ThisWindow.Update()
      Calendar13.SelectOnClose = True
      Calendar13.Ask('Select a Date',INV:InvoiceDate)
      IF Calendar13.Response = RequestCompleted THEN
      INV:InvoiceDate=Calendar13.SelectedDate
      DISPLAY(?INV:InvoiceDate)
      END
      ThisWindow.Reset(True)
    OF ?CallLookup_Client:2
      ThisWindow.Update()
          COD_#   = 0
          IF INV:Terms < 2 AND INV:CID ~= 0       ! And we have some CID previously
             CASE MESSAGE('This is a COD or Pre Paid Invoice.||Would you like to select a COD Name & Address for this Invoice?','COD / Pre Paid',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
             OF BUTTON:Yes
                COD_# = 1
          .  .
      
          IF COD_# = 0
             ! Get the client record
             CLI:CID            = INV:CID
             IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
                ! Ok
             .
             GlobalRequest      = SelectRecord
             Browse_Clients
      
             IF GlobalResponse = RequestCompleted
                INV:CID          = CLI:CID
                INV:ClientNo     = CLI:ClientNo
                INV:ClientName   = CLI:ClientName
                INV:Terms        = CLI:Terms
      
                ! Pre Paid|COD|Account|On Statement
                IF INV:Terms < 2
                   ! Offer the user to lookup a COD client record
                   CASE MESSAGE('This is a COD or Pre Paid account.||Would you like to select a COD Name & Address for this Invoice?','COD / Pre Paid',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                   OF BUTTON:Yes
                      COD_# = 1
                   ELSE
                      CLEAR(DCADD:DC_ID)
          .  .  .  .
      
      
          IF COD_# = 1
             DCADD:DC_ID        = INV:DC_ID
             IF Access:Delivery_CODAddresses.TryFetch(DCADD:PKey_DC_ID) = LEVEL:Benign
             .
      
             GlobalRequest      = SelectRecord
             Browse_CODAddresses()
             IF Globalresponse = RequestCompleted
                INV:DC_ID       = DCADD:DC_ID
          .  .
      
          ThisWindow.Reset()
      
          IF QuickWindow{PROP:AcceptAll} = FALSE AND (SELF.Request = InsertRecord OR SELF.Request = ChangeRecord)
             IF DCADD:DC_ID = 0
                DO Client_Address_From_General
             ELSE
                DO Client_Address_From_COD
          .  .
      
          DISPLAY
    OF ?INV:ClientName
          IF QuickWindow{PROP:AcceptAll} = FALSE AND (SELF.Request = InsertRecord OR SELF.Request = ChangeRecord)
             DO Client_Address_From_General
          .
    OF ?INV:DINo
          IF QuickWindow{PROP:AcceptAll} = FALSE
             ! try to lookup a corresponding DID for the entered DINo - not required though
             IF INV:DINo ~= 0
                DEL:DINo      = INV:DINo
                IF Access:Deliveries.TryFetch(DEL:Key_DINo) = LEVEL:Benign
                   INV:DID    = DEL:DID
                   DISPLAY
          .  .  .
    OF ?Button_JumpTo_Credit_of_Inv
      ThisWindow.Update()
          START(Update_Invoice_h,, INV:CR_IID, ViewRecord)
    OF ?Button_Upd_Status
      ThisWindow.Update()
      LOC:Status = Upd_Invoice_Paid_Status(INV:IID)
      ThisWindow.Reset
          IF LOC:Status ~= INV:Status
             LOC:Status   = INV:Status
             DISPLAY
             MESSAGE('The Invoice Status was updated.', 'Update Invoice - Status', ICON:Asterisk)
          .
    OF ?INV:Insurance
          IF SELF.Request = InsertRecord OR SELF.Request = ChangeRecord
             DO Calc_Inv
          .
    OF ?INV:Documentation
          IF SELF.Request = InsertRecord OR SELF.Request = ChangeRecord
             DO Calc_Inv
          .
    OF ?INV:FuelSurcharge
          IF SELF.Request = InsertRecord OR SELF.Request = ChangeRecord
             DO Calc_Inv
          .
    OF ?INV:FreightCharge
          IF SELF.Request = InsertRecord OR SELF.Request = ChangeRecord
             DO Calc_Inv
          .
    OF ?INV:AdditionalCharge
          IF SELF.Request = InsertRecord OR SELF.Request = ChangeRecord
             DO Calc_Inv
          .
    OF ?INV:VATRate
          IF SELF.Request = InsertRecord OR SELF.Request = ChangeRecord
             DO Calc_Inv
          .
    OF ?CallLookup
      ThisWindow.Update()
      BRA:BranchName = INV:BranchName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        INV:BranchName = BRA:BranchName
        INV:BID = BRA:BID
      END
      ThisWindow.Reset(1)
    OF ?INV:BranchName
      IF INV:BranchName OR ?INV:BranchName{PROP:Req}
        BRA:BranchName = INV:BranchName
        IF Access:Branches.TryFetch(BRA:Key_BranchName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            INV:BranchName = BRA:BranchName
            INV:BID = BRA:BID
          ELSE
            CLEAR(INV:BID)
            SELECT(?INV:BranchName)
            CYCLE
          END
        ELSE
          INV:BID = BRA:BID
        END
      END
      ThisWindow.Reset()
    OF ?Button_Jump_To_Payment
      ThisWindow.Update()
      START(Update_ClientsPayments_h, 25000, L_PG:CPID)
      ThisWindow.Reset
    OF ?Button_Jump_To_Credit
      ThisWindow.Update()
      START(Update_Invoice_h, 25000, L_PG:IID, ViewRecord)
      ThisWindow.Reset
    OF ?Button_Print
      ThisWindow.Update()
      Print_Invoices(,INV:IID)
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
          INV:StatusUpToDate  = FALSE
         IF INV:InvoiceDate = 0
            QuickWindow{PROP:AcceptAll}  = FALSE
            MESSAGE('The Invoice Date is zero.||Please provide an Invoice Date.', 'Update Invoice', ICON:Exclamation)
            SELECT(?INV:InvoiceDate)
            CYCLE
         .
      
      
         IF INV:Total > 0.0 AND INV:BadDebt <> 0
            MESSAGE('Bad Debt is selected but this Invoice has a positive amount, Bad Debt will be de-selected','Invoice Validation',ICON:Exclamation)
            INV:BadDebt = FALSE
            DISPLAY()
            CYCLE
         .
      
         IF INV:Total < 0.0 AND INV:BadDebt = TRUE AND SELF.Request = InsertRecord       
            CASE MESSAGE('This Credit is marked as a Bad Debt.||Are you sure that this is correct?', 'Credit Note - Bad Debt', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
            OF BUTTON:No
               QuickWindow{PROP:AcceptAll}   = FALSE
               SELECT(?INV:FreightCharge)
               CYCLE
         .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW2.SetQueueRecord PROCEDURE

  CODE
      EXECUTE INI:Type + 1
          L_SG:ItemType   = 'Container'
          L_SG:ItemType   = 'Loose'
          L_SG:ItemType   = 'Other'
      ELSE
          L_SG:ItemType   = '<Unknown ' & INI:Type & '>'
      .
  PARENT.SetQueueRecord
  


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END


BRW3.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
      ! Client|Ad Hoc
      EXECUTE STAR:Type + 1
         Sattement_Type   = 'Client'
         Sattement_Type   = 'Ad Hoc'
      .
  


BRW3.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW3::LastSortOrder <> NewOrder THEN
     BRW3::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW3::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW3.TakeNewSelection PROCEDURE

  CODE
  IF BRW3::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW3::PopupTextExt = ''
        BRW3::PopupChoiceExec = True
        BRW3::FormatManager.MakePopup(BRW3::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW3::PopupTextExt = '|-|' & CLIP(BRW3::PopupTextExt)
        END
        BRW3::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW3::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW3::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW3::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW3::PopupChoiceOn AND BRW3::PopupChoiceExec THEN
     BRW3::PopupChoiceExec = False
     BRW3::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW3::PopupTextExt)
     IF BRW3::FormatManager.DispatchChoice(BRW3::PopupChoice)
     ELSE
     END
  END


BRW_Allocations.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW7::LastSortOrder <> NewOrder THEN
     BRW7::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW7::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_Allocations.TakeNewSelection PROCEDURE

  CODE
  IF BRW7::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW7::PopupTextExt = ''
        BRW7::PopupChoiceExec = True
        BRW7::FormatManager.MakePopup(BRW7::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW7::PopupTextExt = '|-|' & CLIP(BRW7::PopupTextExt)
        END
        BRW7::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW7::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW7::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW7::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW7::PopupChoiceOn AND BRW7::PopupChoiceExec THEN
     BRW7::PopupChoiceExec = False
     BRW7::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW7::PopupTextExt)
     IF BRW7::FormatManager.DispatchChoice(BRW7::PopupChoice)
     ELSE
     END
  END


BRW_Credit_Note.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW11::LastSortOrder <> NewOrder THEN
     BRW11::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW11::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_Credit_Note.TakeNewSelection PROCEDURE

  CODE
  IF BRW11::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW11::PopupTextExt = ''
        BRW11::PopupChoiceExec = True
        BRW11::FormatManager.MakePopup(BRW11::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW11::PopupTextExt = '|-|' & CLIP(BRW11::PopupTextExt)
        END
        BRW11::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW11::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW11::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW11::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW11::PopupChoiceOn AND BRW11::PopupChoiceExec THEN
     BRW11::PopupChoiceExec = False
     BRW11::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW11::PopupTextExt)
     IF BRW11::FormatManager.DispatchChoice(BRW11::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Deliveries PROCEDURE (STRING p:Start_Insert,STRING p:SearchInfo)

CurrentTab           STRING(80)                            ! 
LOC:Terms            STRING(20)                            ! Terms for this DI - Pre Paid, COD Delivery, COD Pickup, Account
LOC:CID              ULONG                                 ! Client ID
LOC:Un_Manifested    BYTE(1)                               ! Only show un-manifested deliveries
LOC:Q_Group          GROUP,PRE()                           ! 
L_QG:Total_Units     USHORT                                ! Number of units
L_QG:Remaining_Units USHORT                                ! Number of units
L_QG:Manifested      STRING(50)                            ! Manifested status - this has maintenance function
L_QG:Weight          DECIMAL(10,2)                         ! Total Weight of DI
                     END                                   ! 
LOC:Screen_Group     GROUP,PRE(L_SG)                       ! 
BID                  ULONG                                 ! Branch ID
BranchName           STRING(35)                            ! Branch Name
Bad_Records_Type     BYTE(1)                               ! 
DI_Info              BYTE                                  ! Show additional DI info
Manifested           BYTE                                  ! Only show Manifested DI's on the DI release browse
Release_Button_Txt   CSTRING(30)                           ! Original text
                     END                                   ! 
LOC:Capture_Mode     BYTE                                  ! None, Continuous
LOC:Locator          STRING(35)                            ! 
LOC:Group            GROUP,PRE()                           ! 
L_G:Total            DECIMAL(12,2)                         ! Charge for the DI- Excludes VAT, Docs, Fuel, Insurance and Additional Charges charges
L_G:InsuanceAmount   DECIMAL(11,2)                         ! Charge for the DI- Excludes VAT, Docs, Fuel, Insurance and Additional Charges charges
L_G:TotalCharge      DECIMAL(11,2)                         ! Charge for the DI- Excludes VAT, Docs, Fuel, Insurance and Additional Charges charges
                     END                                   ! 
LOC:Insert_Post      LONG                                  ! 
LOC:Sortorder_Search LONG                                  ! for search / release browse
LOC:Upd_Deliv_Mode   BYTE                                  ! 
LOC:Upd_Deliv_Sec    BYTE                                  ! 
LOC:ReleaseReason    STRING(30)                            ! 
BRW1::View:Browse    VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:Rate)
                       PROJECT(DEL:Charge)
                       PROJECT(DEL:DocumentCharge)
                       PROJECT(DEL:FuelSurcharge)
                       PROJECT(DEL:TollCharge)
                       PROJECT(DEL:AdditionalCharge)
                       PROJECT(DEL:CreatedDate)
                       PROJECT(DEL:CreatedTime)
                       PROJECT(DEL:MultipleManifestsAllowed)
                       PROJECT(DEL:Manifested)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:BID)
                       PROJECT(DEL:CID)
                       PROJECT(DEL:DC_ID)
                       PROJECT(DEL:Terms)
                       PROJECT(DEL:Insure)
                       PROJECT(DEL:TotalConsignmentValue)
                       PROJECT(DEL:InsuranceRate)
                       PROJECT(DEL:VATRate)
                       PROJECT(DEL:JID)
                       PROJECT(DEL:FID)
                       PROJECT(DEL:UID)
                       PROJECT(DEL:DeliveryAID)
                       PROJECT(DEL:CollectionAID)
                       PROJECT(DEL:SID)
                       JOIN(INV:FKey_DID,DEL:DID)
                         PROJECT(INV:IID)
                       END
                       JOIN(BRA:PKey_BID,DEL:BID)
                         PROJECT(BRA:BranchName)
                         PROJECT(BRA:BID)
                       END
                       JOIN(USE:PKey_UID,DEL:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:UID)
                       END
                       JOIN(FLO:PKey_FID,DEL:FID)
                         PROJECT(FLO:Floor)
                         PROJECT(FLO:FID)
                       END
                       JOIN(A_ADD:PKey_AID,DEL:DeliveryAID)
                         PROJECT(A_ADD:AddressName)
                         PROJECT(A_ADD:AID)
                       END
                       JOIN(ADD:PKey_AID,DEL:CollectionAID)
                         PROJECT(ADD:AddressName)
                         PROJECT(ADD:AID)
                       END
                       JOIN(SERI:PKey_SID,DEL:SID)
                         PROJECT(SERI:ServiceRequirement)
                         PROJECT(SERI:SID)
                       END
                       JOIN(JOU:PKey_JID,DEL:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:DINo_NormalFG      LONG                           !Normal forground color
DEL:DINo_NormalBG      LONG                           !Normal background color
DEL:DINo_SelectedFG    LONG                           !Selected forground color
DEL:DINo_SelectedBG    LONG                           !Selected background color
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:ClientName_NormalFG LONG                          !Normal forground color
CLI:ClientName_NormalBG LONG                          !Normal background color
CLI:ClientName_SelectedFG LONG                        !Selected forground color
CLI:ClientName_SelectedBG LONG                        !Selected background color
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
CLI:ClientNo_NormalFG  LONG                           !Normal forground color
CLI:ClientNo_NormalBG  LONG                           !Normal background color
CLI:ClientNo_SelectedFG LONG                          !Selected forground color
CLI:ClientNo_SelectedBG LONG                          !Selected background color
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
DEL:ClientReference_NormalFG LONG                     !Normal forground color
DEL:ClientReference_NormalBG LONG                     !Normal background color
DEL:ClientReference_SelectedFG LONG                   !Selected forground color
DEL:ClientReference_SelectedBG LONG                   !Selected background color
DEL:DIDate             LIKE(DEL:DIDate)               !List box control field - type derived from field
DEL:DIDate_NormalFG    LONG                           !Normal forground color
DEL:DIDate_NormalBG    LONG                           !Normal background color
DEL:DIDate_SelectedFG  LONG                           !Selected forground color
DEL:DIDate_SelectedBG  LONG                           !Selected background color
L_QG:Weight            LIKE(L_QG:Weight)              !List box control field - type derived from local data
L_QG:Weight_NormalFG   LONG                           !Normal forground color
L_QG:Weight_NormalBG   LONG                           !Normal background color
L_QG:Weight_SelectedFG LONG                           !Selected forground color
L_QG:Weight_SelectedBG LONG                           !Selected background color
L_QG:Weight_Style      LONG                           !Field style
L_QG:Total_Units       LIKE(L_QG:Total_Units)         !List box control field - type derived from local data
L_QG:Total_Units_NormalFG LONG                        !Normal forground color
L_QG:Total_Units_NormalBG LONG                        !Normal background color
L_QG:Total_Units_SelectedFG LONG                      !Selected forground color
L_QG:Total_Units_SelectedBG LONG                      !Selected background color
L_QG:Total_Units_Style LONG                           !Field style
L_QG:Remaining_Units   LIKE(L_QG:Remaining_Units)     !List box control field - type derived from local data
L_QG:Remaining_Units_NormalFG LONG                    !Normal forground color
L_QG:Remaining_Units_NormalBG LONG                    !Normal background color
L_QG:Remaining_Units_SelectedFG LONG                  !Selected forground color
L_QG:Remaining_Units_SelectedBG LONG                  !Selected background color
L_QG:Remaining_Units_Style LONG                       !Field style
DEL:Rate               LIKE(DEL:Rate)                 !List box control field - type derived from field
DEL:Rate_NormalFG      LONG                           !Normal forground color
DEL:Rate_NormalBG      LONG                           !Normal background color
DEL:Rate_SelectedFG    LONG                           !Selected forground color
DEL:Rate_SelectedBG    LONG                           !Selected background color
DEL:Charge             LIKE(DEL:Charge)               !List box control field - type derived from field
DEL:Charge_NormalFG    LONG                           !Normal forground color
DEL:Charge_NormalBG    LONG                           !Normal background color
DEL:Charge_SelectedFG  LONG                           !Selected forground color
DEL:Charge_SelectedBG  LONG                           !Selected background color
DEL:DocumentCharge     LIKE(DEL:DocumentCharge)       !List box control field - type derived from field
DEL:DocumentCharge_NormalFG LONG                      !Normal forground color
DEL:DocumentCharge_NormalBG LONG                      !Normal background color
DEL:DocumentCharge_SelectedFG LONG                    !Selected forground color
DEL:DocumentCharge_SelectedBG LONG                    !Selected background color
DEL:FuelSurcharge      LIKE(DEL:FuelSurcharge)        !List box control field - type derived from field
DEL:FuelSurcharge_NormalFG LONG                       !Normal forground color
DEL:FuelSurcharge_NormalBG LONG                       !Normal background color
DEL:FuelSurcharge_SelectedFG LONG                     !Selected forground color
DEL:FuelSurcharge_SelectedBG LONG                     !Selected background color
DEL:TollCharge         LIKE(DEL:TollCharge)           !List box control field - type derived from field
DEL:TollCharge_NormalFG LONG                          !Normal forground color
DEL:TollCharge_NormalBG LONG                          !Normal background color
DEL:TollCharge_SelectedFG LONG                        !Selected forground color
DEL:TollCharge_SelectedBG LONG                        !Selected background color
DEL:AdditionalCharge   LIKE(DEL:AdditionalCharge)     !List box control field - type derived from field
DEL:AdditionalCharge_NormalFG LONG                    !Normal forground color
DEL:AdditionalCharge_NormalBG LONG                    !Normal background color
DEL:AdditionalCharge_SelectedFG LONG                  !Selected forground color
DEL:AdditionalCharge_SelectedBG LONG                  !Selected background color
L_G:TotalCharge        LIKE(L_G:TotalCharge)          !List box control field - type derived from local data
L_G:TotalCharge_NormalFG LONG                         !Normal forground color
L_G:TotalCharge_NormalBG LONG                         !Normal background color
L_G:TotalCharge_SelectedFG LONG                       !Selected forground color
L_G:TotalCharge_SelectedBG LONG                       !Selected background color
L_G:Total              LIKE(L_G:Total)                !List box control field - type derived from local data
L_G:Total_NormalFG     LONG                           !Normal forground color
L_G:Total_NormalBG     LONG                           !Normal background color
L_G:Total_SelectedFG   LONG                           !Selected forground color
L_G:Total_SelectedBG   LONG                           !Selected background color
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
JOU:Journey_NormalFG   LONG                           !Normal forground color
JOU:Journey_NormalBG   LONG                           !Normal background color
JOU:Journey_SelectedFG LONG                           !Selected forground color
JOU:Journey_SelectedBG LONG                           !Selected background color
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
ADD:AddressName_NormalFG LONG                         !Normal forground color
ADD:AddressName_NormalBG LONG                         !Normal background color
ADD:AddressName_SelectedFG LONG                       !Selected forground color
ADD:AddressName_SelectedBG LONG                       !Selected background color
A_ADD:AddressName      LIKE(A_ADD:AddressName)        !List box control field - type derived from field
A_ADD:AddressName_NormalFG LONG                       !Normal forground color
A_ADD:AddressName_NormalBG LONG                       !Normal background color
A_ADD:AddressName_SelectedFG LONG                     !Selected forground color
A_ADD:AddressName_SelectedBG LONG                     !Selected background color
DEL:CreatedDate        LIKE(DEL:CreatedDate)          !List box control field - type derived from field
DEL:CreatedDate_NormalFG LONG                         !Normal forground color
DEL:CreatedDate_NormalBG LONG                         !Normal background color
DEL:CreatedDate_SelectedFG LONG                       !Selected forground color
DEL:CreatedDate_SelectedBG LONG                       !Selected background color
DEL:CreatedTime        LIKE(DEL:CreatedTime)          !List box control field - type derived from field
DEL:CreatedTime_NormalFG LONG                         !Normal forground color
DEL:CreatedTime_NormalBG LONG                         !Normal background color
DEL:CreatedTime_SelectedFG LONG                       !Selected forground color
DEL:CreatedTime_SelectedBG LONG                       !Selected background color
L_QG:Manifested        LIKE(L_QG:Manifested)          !List box control field - type derived from local data
L_QG:Manifested_NormalFG LONG                         !Normal forground color
L_QG:Manifested_NormalBG LONG                         !Normal background color
L_QG:Manifested_SelectedFG LONG                       !Selected forground color
L_QG:Manifested_SelectedBG LONG                       !Selected background color
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:Floor_NormalFG     LONG                           !Normal forground color
FLO:Floor_NormalBG     LONG                           !Normal background color
FLO:Floor_SelectedFG   LONG                           !Selected forground color
FLO:Floor_SelectedBG   LONG                           !Selected background color
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BranchName_NormalFG LONG                          !Normal forground color
BRA:BranchName_NormalBG LONG                          !Normal background color
BRA:BranchName_SelectedFG LONG                        !Selected forground color
BRA:BranchName_SelectedBG LONG                        !Selected background color
INV:IID                LIKE(INV:IID)                  !List box control field - type derived from field
INV:IID_NormalFG       LONG                           !Normal forground color
INV:IID_NormalBG       LONG                           !Normal background color
INV:IID_SelectedFG     LONG                           !Selected forground color
INV:IID_SelectedBG     LONG                           !Selected background color
DEL:MultipleManifestsAllowed LIKE(DEL:MultipleManifestsAllowed) !List box control field - type derived from field
DEL:MultipleManifestsAllowed_NormalFG LONG            !Normal forground color
DEL:MultipleManifestsAllowed_NormalBG LONG            !Normal background color
DEL:MultipleManifestsAllowed_SelectedFG LONG          !Selected forground color
DEL:MultipleManifestsAllowed_SelectedBG LONG          !Selected background color
DEL:MultipleManifestsAllowed_Icon LONG                !Entry's icon ID
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
USE:Login_NormalFG     LONG                           !Normal forground color
USE:Login_NormalBG     LONG                           !Normal background color
USE:Login_SelectedFG   LONG                           !Selected forground color
USE:Login_SelectedBG   LONG                           !Selected background color
SERI:ServiceRequirement LIKE(SERI:ServiceRequirement) !List box control field - type derived from field
SERI:ServiceRequirement_NormalFG LONG                 !Normal forground color
SERI:ServiceRequirement_NormalBG LONG                 !Normal background color
SERI:ServiceRequirement_SelectedFG LONG               !Selected forground color
SERI:ServiceRequirement_SelectedBG LONG               !Selected background color
LOC:Terms              LIKE(LOC:Terms)                !List box control field - type derived from local data
LOC:Terms_NormalFG     LONG                           !Normal forground color
LOC:Terms_NormalBG     LONG                           !Normal background color
LOC:Terms_SelectedFG   LONG                           !Selected forground color
LOC:Terms_SelectedBG   LONG                           !Selected background color
DEL:Manifested         LIKE(DEL:Manifested)           !List box control field - type derived from field
DEL:Manifested_NormalFG LONG                          !Normal forground color
DEL:Manifested_NormalBG LONG                          !Normal background color
DEL:Manifested_SelectedFG LONG                        !Selected forground color
DEL:Manifested_SelectedBG LONG                        !Selected background color
DEL:DID                LIKE(DEL:DID)                  !List box control field - type derived from field
DEL:BID                LIKE(DEL:BID)                  !List box control field - type derived from field
DEL:CID                LIKE(DEL:CID)                  !List box control field - type derived from field
DEL:DC_ID              LIKE(DEL:DC_ID)                !List box control field - type derived from field
DEL:Terms              LIKE(DEL:Terms)                !Browse hot field - type derived from field
DEL:Insure             LIKE(DEL:Insure)               !Browse hot field - type derived from field
DEL:TotalConsignmentValue LIKE(DEL:TotalConsignmentValue) !Browse hot field - type derived from field
DEL:InsuranceRate      LIKE(DEL:InsuranceRate)        !Browse hot field - type derived from field
DEL:VATRate            LIKE(DEL:VATRate)              !Browse hot field - type derived from field
DEL:JID                LIKE(DEL:JID)                  !Browse key field - type derived from field
DEL:FID                LIKE(DEL:FID)                  !Browse key field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Related join file key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Related join file key field - type derived from field
A_ADD:AID              LIKE(A_ADD:AID)                !Related join file key field - type derived from field
ADD:AID                LIKE(ADD:AID)                  !Related join file key field - type derived from field
SERI:SID               LIKE(SERI:SID)                 !Related join file key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB8::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Deliveries File'),AT(,,510,299),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('BrowseDeliveries'),SYSTEM,TIMER(50)
                       GROUP,AT(8,18,163,10),USE(?Group_locator)
                         PROMPT('Locator:'),AT(8,22,,9),USE(?Prompt4),TRN
                         ENTRY(@s35),AT(36,22,89,9),USE(LOC:Locator)
                       END
                       LIST,AT(8,35,491,220),USE(?Browse:1),HVSCROLL,FORMAT('30R(2)|FM*~DI No.~C(0)@n_10@69L(2' & |
  ')|M*~Client~C(0)@s35@34R(2)|M*~Client No.~C(0)@n_10b@50L(2)|M*~Client Ref.~C(0)@s60@' & |
  '38R(1)|M*~DI Date~C(0)@d5b@44R(2)|M*Y~Weight~C(0)@n-14.2@[28R(2)|M*Y~Total~C(0)@n6@2' & |
  '8R(2)|M*Y~Remain~C(0)@n6@]|M~Units~[32R(1)|M*~Rate~C(0)@n-11.2@40R(1)|M*~Freight~C(0' & |
  ')@n-13.2@40R(1)|M*~Docs.~C(0)@n-11.2@40R(1)|M*~Fuel Surcharge~L(1)@n-11.2@40R(1)|M*~' & |
  'Toll Charge~C(0)@n-11.2@40R(1)|M*~Additional Charge~L(1)@n-15.2@40R(1)|M*~Total~C(0)' & |
  '@n-15.2@40R(1)|M*~Total incl.~C(0)@n-17.2@]|M~Financial~70L(2)|M*~Journey~C(0)@s70@8' & |
  '0L(2)|M*~From Address~C(0)@s35@80L(2)|M*~To Address~C(0)@s35@[38R(1)|M*~Date~C(0)@d5' & |
  'b@36R(1)|M*~Time~C(0)@t7b@]|M~Created~50L(2)|M*~Manifested~C(0)@s50b@36L(2)|M*~Floor' & |
  '~C(0)@s35@50L(2)|M*~Branch~C(0)@s35@42R(2)|M*~Invoice No.~C(0)@n_10b@30L(2)|M*I~Mult' & |
  'iple Manifests Allowed~@p p@38L(2)|M*~User~C(0)@s20@50L(2)|M*~Service Req.~C(0)@s35@' & |
  '36L(2)|M*~Terms~C(0)@s20@20L(2)|M*~Manifested~@n3@30R(2)|M~DID~L(2)@n_10@30R(2)|M~BI' & |
  'D~C(0)@n_10@30R(2)|M~CID~C(0)@n_10@30R(2)|M~DC ID~L(2)@n_10@'),FROM(Queue:Browse:1),IMM, |
  MSG('Browsing the Deliveries file')
                       BUTTON('&Select'),AT(117,259,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(293,259,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(345,259,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(399,259,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(451,259,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       CHECK(' DI Info.'),AT(453,22,,9),USE(L_SG:DI_Info),MSG('Show additional DI info'),TIP('Show addit' & |
  'ional DI info<0DH,0AH>Total Units, Remaining Units & Total Weight'),TRN
                       SHEET,AT(4,4,501,274),USE(?CurrentTab),FONT('Tahoma'),JOIN
                         TAB('&1) By DI No.'),USE(?Tab:1)
                           BUTTON('&Filter'),AT(8,259,49,14),USE(?Button_Filter),DISABLE
                           BUTTON('&Query'),AT(61,259,49,14),USE(?Query)
                         END
                         TAB('&2) By Client Ref.'),USE(?Tab:2)
                           BUTTON('Select Client'),AT(9,259,,14),USE(?SelectClients),LEFT,ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                         END
                         TAB('&3) By Journey'),USE(?Tab:3)
                           BUTTON('Select Journey'),AT(9,259,,14),USE(?SelectJourneys),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&4) By Floor'),USE(?Tab:4)
                           BUTTON('Select Floor'),AT(9,259,,14),USE(?SelectFloors),LEFT,ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                         END
                         TAB('&5) By DID'),USE(?Tab_DID)
                           GROUP,AT(9,262,116,10),USE(?Group_BadType)
                             PROMPT('Bad Type:'),AT(10,263),USE(?L_SG:Bad_Records_Type:Prompt),TRN
                             LIST,AT(50,262,78,10),USE(L_SG:Bad_Records_Type),DROP(5),FROM('Show All|#0|Missing Branch|#1')
                           END
                         END
                         TAB('&6) By DI Date'),USE(?Tab:5)
                         END
                         TAB('Search Results'),USE(?Tab_SearchResults),HIDE
                           BUTTON('&Search'),AT(9,259,,14),USE(?Button_Search),LEFT,ICON('Web.ico'),FLAT
                           BUTTON('&Release DI'),AT(66,259,,14),USE(?Button_ReleaseDI),LEFT,ICON('dotred.ico'),FLAT,HIDE, |
  SKIP
                           GROUP,AT(345,22,105,14),USE(?GROUP_searchtopright)
                             LIST,AT(367,22,78,10),USE(L_SG:Manifested),DROP(3,78),FROM('All|#0|Manifested|#1|Not-Ma' & |
  'nifested|#2'),MSG('Only show Manifested DI''s on the DI release browse'),TIP('Only show ' & |
  'Manifested DI''s on the DI release browse')
                             PROMPT('Manifested:'),AT(325,22),USE(?L_SG:Manifested:Prompt),TRN
                           END
                         END
                         TAB('&7) By MID'),USE(?Tab_MID),HIDE
                         END
                       END
                       GROUP,AT(4,284,357,10),USE(?Group_Bottom_Left)
                         CHECK(' &Un-Manifested'),AT(4,284),USE(LOC:Un_Manifested),MSG('Only show un-manifested ' & |
  'deliveries'),TIP('Only show un-manifested deliveries<0DH,0AH>Partially manifested de' & |
  'liveries will be included')
                         PROMPT('Branch:'),AT(88,284),USE(?Prompt1)
                         LIST,AT(118,284,87,10),USE(L_SG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                         PROMPT('Capture Mode:'),AT(224,284),USE(?LOC:Capture_Mode:Prompt)
                         LIST,AT(274,284,78,10),USE(LOC:Capture_Mode),DROP(5),FROM('None|#0|Continuous|#1'),MSG('None, Continuoes'), |
  TIP('None, Continuoes')
                       END
                       BUTTON('&Close'),AT(456,281,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON,AT(488,4,20,13),USE(?Button_Refresh),ICON('Cog 2.ico'),FLAT,TIP('Refresh the browse')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
QBE9                 QueryFormClass                        ! QBE List Class. 
QBV9                 QueryFormVisual                       ! QBE Visual Class
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Fetch                  PROCEDURE(BYTE Direction),DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateViewRecord       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort13:Locator IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 5
BRW1::Sort16:Locator IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 8
BRW1::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW1::Sort1:StepClass StepStringClass                      ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW1::Sort4:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 3
BRW1::Sort12:StepClass StepRealClass                       ! Conditional Step Manager - CHOICE(?CurrentTab) = 4
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB8                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

DID_Q_Type      QUEUE,TYPE
DID         LIKE(DEL:DID)
Weight      DECIMAL(20,1)
                .

SearchClass         CLASS,TYPE

DID_Q           &DID_Q_Type

Construct       PROCEDURE()
Destruct        PROCEDURE()

Set_Filter      PROCEDURE()

Add_Q           PROCEDURE(ULONG p:DID),LONG
Get_Q           PROCEDURE(ULONG p:DID),BYTE

Process_Q       PROCEDURE(BYTE p_Weights, LONG p_From_Weight, LONG p_To_Weight),STRING  ! Final process after search complete

Mod_Filter      PROCEDURE()
                     .



ReleaseClass        CLASS,TYPE
Set_Filter      PROCEDURE()
Mod_Filter      PROCEDURE()
    .



Release_        ReleaseClass

SearchMe_       SearchClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
  !------------------------------------
  !Style for ?Browse:1
  !------------------------------------
!---------------------------------------------------------------------------
Style_Entry                       ROUTINE
    IF L_SG:DI_Info = FALSE
       Queue:Browse:1.L_QG:Total_Units_Style                    = 2
       Queue:Browse:1.L_QG:Remaining_Units_Style                = 2
       Queue:Browse:1.L_QG:Weight_Style                         = 2
    ELSE
       Queue:Browse:1.L_QG:Total_Units_Style                    = 0
       Queue:Browse:1.L_QG:Remaining_Units_Style                = 0
       Queue:Browse:1.L_QG:Weight_Style                         = 0
    .
    EXIT
Style_Setup                     ROUTINE
    ?Browse:1{PROPSTYLE:TextColor, 2}     = -1
    ?Browse:1{PROPSTYLE:BackColor, 2}     = 0E9E9E9H
    ?Browse:1{PROPSTYLE:TextSelected, 2}  = -1
    ?Browse:1{PROPSTYLE:BackSelected, 2}  = -1
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Deliveries')
      Thread_Add_Del_Check_Global('Browse_Deliveries', THREAD(), '0')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt4
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Un_Manifested',LOC:Un_Manifested)              ! Added by: BrowseBox(ABC)
  BIND('L_SG:BID',L_SG:BID)                                ! Added by: BrowseBox(ABC)
  BIND('L_SG:Bad_Records_Type',L_SG:Bad_Records_Type)      ! Added by: BrowseBox(ABC)
  BIND('LOC:CID',LOC:CID)                                  ! Added by: BrowseBox(ABC)
  BIND('L_QG:Weight',L_QG:Weight)                          ! Added by: BrowseBox(ABC)
  BIND('L_QG:Total_Units',L_QG:Total_Units)                ! Added by: BrowseBox(ABC)
  BIND('L_QG:Remaining_Units',L_QG:Remaining_Units)        ! Added by: BrowseBox(ABC)
  BIND('L_G:TotalCharge',L_G:TotalCharge)                  ! Added by: BrowseBox(ABC)
  BIND('L_G:Total',L_G:Total)                              ! Added by: BrowseBox(ABC)
  BIND('L_QG:Manifested',L_QG:Manifested)                  ! Added by: BrowseBox(ABC)
  BIND('LOC:Terms',LOC:Terms)                              ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:AddressAlias.SetOpenRelated()
  Relate:AddressAlias.Open                                 ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Branches.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Floors.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ClientsRateTypes.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ServiceRequirements.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoadDeliveries.UseFile                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoad.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  ?Browse:1{Prop:LineHeight} = 10
  Do DefineListboxStyle
  QBE9.Init(QBV9, INIMgr,'Browse_Deliveries', GlobalErrors)
  QBE9.QkSupport = True
  QBE9.QkMenuIcon = 'QkQBE.ico'
  QBE9.QkIcon = 'QkLoad.ico'
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon DEL:ClientReference for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,DEL:SKey_ClientReference) ! Add the sort order for DEL:SKey_ClientReference for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(?LOC:Locator,DEL:ClientReference,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: DEL:SKey_ClientReference , DEL:ClientReference
  BRW1.AppendOrder('+DEL:DINo')                            ! Append an additional sort order
  BRW1.SetFilter('((LOC:Un_Manifested = 0 OR (DEL:Manifested >= 0 AND DEL:Manifested <<= 2) ) AND (L_SG:BID = DEL:BID OR L_SG:BID = 0))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:CID)                              ! Apply the reset field
  BRW1.AddResetField(L_SG:DI_Info)                         ! Apply the reset field
  BRW1::Sort4:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon DEL:JID for sort order 2
  BRW1.AddSortOrder(BRW1::Sort4:StepClass,DEL:FKey_JID)    ! Add the sort order for DEL:FKey_JID for sort order 2
  BRW1.AddRange(DEL:JID,Relate:Deliveries,Relate:Journeys) ! Add file relationship range limit for sort order 2
  BRW1.AppendOrder('+DEL:DINo')                            ! Append an additional sort order
  BRW1.SetFilter('((LOC:Un_Manifested = 0 OR (DEL:Manifested >= 0 AND DEL:Manifested <<= 2)) AND (L_SG:BID = DEL:BID OR L_SG:BID = 0))') ! Apply filter expression to browse
  BRW1.AddResetField(L_SG:DI_Info)                         ! Apply the reset field
  BRW1::Sort12:StepClass.Init(+ScrollSort:AllowAlpha)      ! Moveable thumb based upon DEL:FID for sort order 3
  BRW1.AddSortOrder(BRW1::Sort12:StepClass,DEL:FKey_FID)   ! Add the sort order for DEL:FKey_FID for sort order 3
  BRW1.AddRange(DEL:FID,Relate:Deliveries,Relate:Floors)   ! Add file relationship range limit for sort order 3
  BRW1.AppendOrder('+DEL:DINo')                            ! Append an additional sort order
  BRW1.SetFilter('((LOC:Un_Manifested = 0 OR (DEL:Manifested >= 0 AND DEL:Manifested <<= 2)) AND (L_SG:BID = DEL:BID OR L_SG:BID = 0))') ! Apply filter expression to browse
  BRW1.AddResetField(L_SG:DI_Info)                         ! Apply the reset field
  BRW1.AddSortOrder(,DEL:PKey_DID)                         ! Add the sort order for DEL:PKey_DID for sort order 4
  BRW1.AddLocator(BRW1::Sort13:Locator)                    ! Browse has a locator for sort order 4
  BRW1::Sort13:Locator.Init(?LOC:Locator,DEL:DID,1,BRW1)   ! Initialize the browse locator using ?LOC:Locator using key: DEL:PKey_DID , DEL:DID
  BRW1.AppendOrder('+DEL:DINo')                            ! Append an additional sort order
  BRW1.SetFilter('(L_SG:Bad_Records_Type = 0 OR (L_SG:Bad_Records_Type = 1 AND DEL:BID = 0))') ! Apply filter expression to browse
  BRW1.AddResetField(L_SG:Bad_Records_Type)                ! Apply the reset field
  BRW1.AddResetField(L_SG:DI_Info)                         ! Apply the reset field
  BRW1.AddSortOrder(,)                                     ! Add the sort order for  for sort order 5
  BRW1.AppendOrder('+DEL:DIDate,+CLI:ClientName,+DEL:DINo,+DEL:DID') ! Append an additional sort order
  BRW1.AddResetField(L_SG:DI_Info)                         ! Apply the reset field
  BRW1.AddSortOrder(,)                                     ! Add the sort order for  for sort order 6
  BRW1.AppendOrder('-DEL:DIDate,-DEL:DINo')                ! Append an additional sort order
  BRW1.SetFilter('(DEL:DID=0)')                            ! Apply filter expression to browse
  BRW1.AddResetField(L_SG:DI_Info)                         ! Apply the reset field
  BRW1.AddSortOrder(,DEL:Key_DINo)                         ! Add the sort order for DEL:Key_DINo for sort order 7
  BRW1.AddLocator(BRW1::Sort16:Locator)                    ! Browse has a locator for sort order 7
  BRW1::Sort16:Locator.Init(?LOC:Locator,DEL:DINo,1,BRW1)  ! Initialize the browse locator using ?LOC:Locator using key: DEL:Key_DINo , DEL:DINo
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon DEL:DINo for sort order 8
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,DEL:Key_DINo)    ! Add the sort order for DEL:Key_DINo for sort order 8
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 8
  BRW1::Sort0:Locator.Init(?LOC:Locator,DEL:DINo,1,BRW1)   ! Initialize the browse locator using ?LOC:Locator using key: DEL:Key_DINo , DEL:DINo
  BRW1.SetFilter('((LOC:Un_Manifested = 0 OR (DEL:Manifested >= 0 AND DEL:Manifested <<= 2)) AND (L_SG:BID = DEL:BID OR L_SG:BID = 0))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Un_Manifested)                    ! Apply the reset field
  BRW1.AddResetField(L_SG:BID)                             ! Apply the reset field
  BRW1.AddResetField(L_SG:DI_Info)                         ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(DEL:DINo,BRW1.Q.DEL:DINo)                  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientName,BRW1.Q.CLI:ClientName)      ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientNo,BRW1.Q.CLI:ClientNo)          ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW1.AddField(DEL:ClientReference,BRW1.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DIDate,BRW1.Q.DEL:DIDate)              ! Field DEL:DIDate is a hot field or requires assignment from browse
  BRW1.AddField(L_QG:Weight,BRW1.Q.L_QG:Weight)            ! Field L_QG:Weight is a hot field or requires assignment from browse
  BRW1.AddField(L_QG:Total_Units,BRW1.Q.L_QG:Total_Units)  ! Field L_QG:Total_Units is a hot field or requires assignment from browse
  BRW1.AddField(L_QG:Remaining_Units,BRW1.Q.L_QG:Remaining_Units) ! Field L_QG:Remaining_Units is a hot field or requires assignment from browse
  BRW1.AddField(DEL:Rate,BRW1.Q.DEL:Rate)                  ! Field DEL:Rate is a hot field or requires assignment from browse
  BRW1.AddField(DEL:Charge,BRW1.Q.DEL:Charge)              ! Field DEL:Charge is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DocumentCharge,BRW1.Q.DEL:DocumentCharge) ! Field DEL:DocumentCharge is a hot field or requires assignment from browse
  BRW1.AddField(DEL:FuelSurcharge,BRW1.Q.DEL:FuelSurcharge) ! Field DEL:FuelSurcharge is a hot field or requires assignment from browse
  BRW1.AddField(DEL:TollCharge,BRW1.Q.DEL:TollCharge)      ! Field DEL:TollCharge is a hot field or requires assignment from browse
  BRW1.AddField(DEL:AdditionalCharge,BRW1.Q.DEL:AdditionalCharge) ! Field DEL:AdditionalCharge is a hot field or requires assignment from browse
  BRW1.AddField(L_G:TotalCharge,BRW1.Q.L_G:TotalCharge)    ! Field L_G:TotalCharge is a hot field or requires assignment from browse
  BRW1.AddField(L_G:Total,BRW1.Q.L_G:Total)                ! Field L_G:Total is a hot field or requires assignment from browse
  BRW1.AddField(JOU:Journey,BRW1.Q.JOU:Journey)            ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AddressName,BRW1.Q.ADD:AddressName)    ! Field ADD:AddressName is a hot field or requires assignment from browse
  BRW1.AddField(A_ADD:AddressName,BRW1.Q.A_ADD:AddressName) ! Field A_ADD:AddressName is a hot field or requires assignment from browse
  BRW1.AddField(DEL:CreatedDate,BRW1.Q.DEL:CreatedDate)    ! Field DEL:CreatedDate is a hot field or requires assignment from browse
  BRW1.AddField(DEL:CreatedTime,BRW1.Q.DEL:CreatedTime)    ! Field DEL:CreatedTime is a hot field or requires assignment from browse
  BRW1.AddField(L_QG:Manifested,BRW1.Q.L_QG:Manifested)    ! Field L_QG:Manifested is a hot field or requires assignment from browse
  BRW1.AddField(FLO:Floor,BRW1.Q.FLO:Floor)                ! Field FLO:Floor is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BranchName,BRW1.Q.BRA:BranchName)      ! Field BRA:BranchName is a hot field or requires assignment from browse
  BRW1.AddField(INV:IID,BRW1.Q.INV:IID)                    ! Field INV:IID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:MultipleManifestsAllowed,BRW1.Q.DEL:MultipleManifestsAllowed) ! Field DEL:MultipleManifestsAllowed is a hot field or requires assignment from browse
  BRW1.AddField(USE:Login,BRW1.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW1.AddField(SERI:ServiceRequirement,BRW1.Q.SERI:ServiceRequirement) ! Field SERI:ServiceRequirement is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Terms,BRW1.Q.LOC:Terms)                ! Field LOC:Terms is a hot field or requires assignment from browse
  BRW1.AddField(DEL:Manifested,BRW1.Q.DEL:Manifested)      ! Field DEL:Manifested is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DID,BRW1.Q.DEL:DID)                    ! Field DEL:DID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:BID,BRW1.Q.DEL:BID)                    ! Field DEL:BID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:CID,BRW1.Q.DEL:CID)                    ! Field DEL:CID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DC_ID,BRW1.Q.DEL:DC_ID)                ! Field DEL:DC_ID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:Terms,BRW1.Q.DEL:Terms)                ! Field DEL:Terms is a hot field or requires assignment from browse
  BRW1.AddField(DEL:Insure,BRW1.Q.DEL:Insure)              ! Field DEL:Insure is a hot field or requires assignment from browse
  BRW1.AddField(DEL:TotalConsignmentValue,BRW1.Q.DEL:TotalConsignmentValue) ! Field DEL:TotalConsignmentValue is a hot field or requires assignment from browse
  BRW1.AddField(DEL:InsuranceRate,BRW1.Q.DEL:InsuranceRate) ! Field DEL:InsuranceRate is a hot field or requires assignment from browse
  BRW1.AddField(DEL:VATRate,BRW1.Q.DEL:VATRate)            ! Field DEL:VATRate is a hot field or requires assignment from browse
  BRW1.AddField(DEL:JID,BRW1.Q.DEL:JID)                    ! Field DEL:JID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:FID,BRW1.Q.DEL:FID)                    ! Field DEL:FID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BID,BRW1.Q.BRA:BID)                    ! Field BRA:BID is a hot field or requires assignment from browse
  BRW1.AddField(USE:UID,BRW1.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  BRW1.AddField(FLO:FID,BRW1.Q.FLO:FID)                    ! Field FLO:FID is a hot field or requires assignment from browse
  BRW1.AddField(A_ADD:AID,BRW1.Q.A_ADD:AID)                ! Field A_ADD:AID is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AID,BRW1.Q.ADD:AID)                    ! Field ADD:AID is a hot field or requires assignment from browse
  BRW1.AddField(SERI:SID,BRW1.Q.SERI:SID)                  ! Field SERI:SID is a hot field or requires assignment from browse
  BRW1.AddField(JOU:JID,BRW1.Q.JOU:JID)                    ! Field JOU:JID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:CID,BRW1.Q.CLI:CID)                    ! Field CLI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Deliveries',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      ! Load the Branch record
      BRA:BID             = GLO:BranchID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         L_SG:BID         = GLO:BranchID
         L_SG:BranchName  = BRA:BranchName
      .
      IF p:Start_Insert = 'S' OR p:Start_Insert = 'R'
         HIDE(?Tab_DID)
         HIDE(?Tab:1)
         HIDE(?Tab:2)
         HIDE(?Tab:3)
         HIDE(?Tab:4)
         HIDE(?Tab:5)
  
         UNHIDE(?Tab_SearchResults)
         HIDE(?Group_Bottom_Left)
  
         IF p:Start_Insert = 'R'
            UNHIDE(?Button_ReleaseDI)
         .
         UNHIDE(?L_SG:Manifested)
      ELSE
         ! User Level access code
         HIDE(?Tab_DID)
         IF GLO:AccessLevel >= 10
            UNHIDE(?Tab_DID)
            HIDE(?Select:2)
      .  .
  BRW1.QueryControl = ?Query
  BRW1.UpdateQuery(QBE9,1)
  BRW1.AskProcedure = 1                                    ! Will call: Update_Deliveries(LOC:Upd_Deliv_Mode)
  FDB8.Init(?L_SG:BranchName,Queue:FileDrop.ViewPosition,FDB8::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB8.Q &= Queue:FileDrop
  FDB8.AddSortOrder(BRA:Key_BranchName)
  FDB8.AddField(BRA:BranchName,FDB8.Q.BRA:BranchName) !List box control field - type derived from field
  FDB8.AddField(BRA:BID,FDB8.Q.BRA:BID) !Primary key field - type derived from field
  FDB8.AddUpdateField(BRA:BID,L_SG:BID)
  ThisWindow.AddItem(FDB8.WindowComponent)
  FDB8.DefaultFill = 0
      L_SG:Release_Button_Txt     = QuickWindow{PROP:Text}
      BRW1.Popup.AddItem('Print Delivery Note','PopupPrintDel')
      BRW1.Popup.AddItemEvent('PopupPrintDel',EVENT:User+1)
      BRW1.Popup.AddItem('Print Delivery Invoice','PopupPrintInv')
      BRW1.Popup.AddItemEvent('PopupPrintInv',EVENT:User+2)
      BRW1.Popup.AddItem('Print Delivery Invoice (Lazer)','PopupPrintInvL')
      BRW1.Popup.AddItemEvent('PopupPrintInvL',EVENT:User+3)
      BRW1.Popup.AddItem('-','Separator')
  SELF.SetAlerts()
      DO Style_Setup
      ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:Default_Action_Return)
      !DI_Edit_Post_#  =
      IF INLIST(Get_User_Access(GLO:UID, 'Deliveries', 'Update_Delivery', 'DI-Edit-Post', 1), '0', '100') > 0
      !IF DI_Edit_Post_#
         LOC:Upd_Deliv_Sec        = TRUE
         ?Change:4{PROP:Tip}      = 'Change the Record (CTRL-Click to Change Invoiced/Manifested)'
      .
  
  
      ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned)
      ! (ULONG, STRING      , STRING     , <STRING>       , BYTE=0             , <*BYTE>                 ),LONG
      !   1       2               3               4               5                   6
      ! Returns
      !   0   - Allow (default)
      !   1   - Disable
      !   2   - Hide
      !   100 - Allow                 - Default action
      !   101 - Disallow              - Default action
      !
      !   Application Sections are added if not found
      !   Application Section Usage is logged         - not, only if not found user level????
      !
      ! p:Default_Action
      !   - applies to 1st Add only
  
      ! No Group or User actions present then default action taken
      !   Group action present then applied
      !      User action present then applied, overriding Group
      ! Check order, User specific, Group specific, default
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Deliveries',QuickWindow)         ! Save window data to non-volatile store
  END
      Thread_Add_Del_Check_Global('Browse_Deliveries', THREAD(), '1')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Deliveries(LOC:Upd_Deliv_Mode)
    ReturnValue = GlobalResponse
  END
    IF Request = InsertRecord AND ReturnValue = RequestCompleted AND LOC:Capture_Mode > 0
       POST(EVENT:Accepted, ?Insert:4)
    .
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Change:4
      BRW1.UpdateBuffer()
          IF LOC:Upd_Deliv_Sec = TRUE AND KEYCODE() = CtrlMouseLeft
             LOC:Upd_Deliv_Mode   = 1
          ELSE
             LOC:Upd_Deliv_Mode   = 0
          .
      
      !    ikb
          ! Check CTRL-Click, then allow update if invoiced and permission
          ! Set tool tip to show this option if user has it
          !
          !       User_Access_#            = Get_User_Access(GLO:UID, 'Manifest', 'Update_Manifest', 'DI-Edit')
          !
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_SG:DI_Info
          CLEAR(LOC:Q_Group)
    OF ?SelectClients
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      LOC:CID = Select_RateMod_Clients()
      ThisWindow.Reset
    OF ?SelectJourneys
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Journey()
      ThisWindow.Reset
    OF ?SelectFloors
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Floors()
      ThisWindow.Reset
    OF ?Button_Search
      ThisWindow.Update()
         IF p:Start_Insert = 'R'
            Release_.Set_Filter()
         ELSE
            SearchMe_.Set_Filter()
         .
      
    OF ?Button_ReleaseDI
      ThisWindow.Update()
      BRW1.UpdateViewRecord()
          IF DEL:ReleasedUID = 0 AND DEL:DID <> 0       ! there is some DI to release
             ! (<*STRING>, ULONG=0, <*STRING>),LONG
             ! (p_Statement_Info, p:DID, p_Release_Status_Str)
             Check_Delivery_Release(, DEL:DID, LOC:ReleaseReason)
      
             CASE MESSAGE('Would you like to release this DI?||DI No. ' & DEL:DINo & '||Reason: ' & CLIP(LOC:ReleaseReason), 'Release DI', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
             OF BUTTON:Yes
                DEL:ReleasedUID   = GLO:UID
      
                IF Access:Deliveries.TryUpdate() = LEVEL:Benign
          .  .  .
      BRW1.ResetFromBuffer()
    OF ?L_SG:Manifested
         CASE p:Start_Insert 
         OF 'S'
            SearchMe_.Mod_Filter()
         OF 'R'
            Release_.Mod_Filter()
         .
    OF ?Button_Refresh
      ThisWindow.Update()
          SELECT(?Browse:1)
      BRW1.ResetQueue(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_SG:Manifested
         CASE p:Start_Insert 
         OF 'S'
            SearchMe_.Mod_Filter()
         OF 'R'
            Release_.Mod_Filter()
         .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          QuickWindow{PROP:Timer}  = 0
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF p:Start_Insert = 'S'
             ! Search called, call search window
             SearchMe_.Set_Filter()
          ELSIF p:Start_Insert = 'R'
             ! DI's requiring release
      !       BIND('DEL:DIDate',DEL:DIDate)
      !       BIND('DEL:ReleasedUID',DEL:ReleasedUID)
      !       BIND('DEL:Manifested',DEL:Manifested)
      
             Release_.Set_Filter()
          ELSIF DEFORMAT(p:Start_Insert) > 0
             LOC:Capture_Mode         = 1
      
             QuickWindow{PROP:Timer}  = 100
          .
    OF EVENT:Timer
          LOC:Insert_Post += 1
          IF LOC:Insert_Post > 1
             POST(EVENT:Accepted, ?Insert:4)
             QuickWindow{PROP:Timer}  = 0
      
             LOC:Insert_Post          = 0
          .
    ELSE
      CASE EVENT()
      OF EVENT:User
         ! Refresh browse
         BRW1.ResetFromFile()
      OF EVENT:User+1
         BRW1.UpdateViewRecord()
         IF DEL:DID ~= 0
            Print_Cont(DEL:DID, 2, 1)
         ELSE
            MESSAGE('No DID available to print.', 'Browse_Deliveries', ICON:Hand)
         .
      OF EVENT:User+2 OROF EVENT:User+3
         BRW1.UpdateViewRecord()
         IF DEL:DID ~= 0
            INV:DID  = DEL:DID
            IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
               IF EVENT() = EVENT:User+2
                  Print_Cont(INV:IID, 0)
               ELSE
                  ! DID or IID passed to restrict to a Delivery or Invoice - (p:DID, p:IID, p:From, p:To, p:Un_Printed)
                  Print_Invoices(, INV:IID)
               .
            ELSE
               MESSAGE('There is no invoice for DI No.: ' & DEL:DINo,'Print Invoice', ICON:Exclamation)
            .
         ELSE
            MESSAGE('No DID available to print.', 'Browse_Deliveries', ICON:Hand)
      .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

SearchClass.Set_Filter               PROCEDURE()

Criteria    GROUP,PRE(SFC)
DINo            LIKE(DEL:DINo)
InvoiceNo       LIKE(INV:IID)

ClientNo        LIKE(CLI:ClientNo)
Date            DATE

Sender          LIKE(ADD:AID)
Receiver        LIKE(ADD:AID)

Manifest        LIKE(MAN:MID)

Vessel          LIKE(DELI:ContainerVessel)
Container       LIKE(DELI:ContainerNo)
ClientRef       LIKE(DEL:ClientReference)

Weights         BYTE
FromWeight      LIKE(DELI:Weight)
ToWeight        LIKE(DELI:Weight)

SpecialInstructions LIKE(DEL:SpecialDeliveryInstructions)
            .


SearchView      VIEW(Deliveries)
       JOIN(DELI:FKey_DID_ItemNo, DEL:DID)    !DeliveryItems
          PROJECT(DELI:ContainerVessel,DELI:ContainerNo,DELI:Weight)
         JOIN(MALD:FKey_DIID, DELI:DIID)
           JOIN(MAL:PKey_MLID, MALD:MLID)
             JOIN(MAN:PKey_MID, MAL:MID)
                PROJECT(MAN:MID)
       . . . .
       JOIN(INV:FKey_DID, DEL:DID)
          PROJECT(INV:IID)
       .
       JOIN(CLI:PKey_CID, DEL:CID)
          PROJECT(CLI:ClientNo, CLI:ClientName, CLI:ClientSearch)
    .  .

View_Search     ViewManager


F           STRING(2048)
FS          STRING(4500)

Search_Result   BYTE(0)
FieldLabel      STRING(20)
               
    CODE
    ! Initial idea was to create a Q of items and then validate each record in the view against this Q
    ! Decided to make a filter instead.... just as well because the other method doesn't work??
    LOOP
       IF p:SearchInfo <> ''
            
            !MESSAGE(p:SearchInfo )
            
          CLEAR(Criteria)  
          FieldLabel       = Get_1st_Element_From_Delim_Str(p:SearchInfo,':',1)
          IF FieldLabel = 'CLNO'       ! client no.
             SFC:ClientNo  = Get_1st_Element_From_Delim_Str(p:SearchInfo,':',1)
             Search_Result = TRUE
          .     
          CLEAR(p:SearchInfo)
       .  
       IF Search_Result = FALSE  
          Search_Result = Deliveries_Search(Criteria)
       .         
       IF Search_Result = TRUE
          Search_Result = FALSE
          SETCURSOR(CURSOR:Wait)
          CLEAR(FS)
          CLEAR(F)

          PUSHBIND()
          BIND('DEL:DINo',DEL:DINo)
          BIND('INV:IID',INV:IID)
          BIND('CLI:ClientNo',CLI:ClientNo)
          BIND('DEL:DIDate',DEL:DIDate)
          BIND('DEL:CollectionAID',DEL:CollectionAID)
          BIND('DEL:DeliveryAID',DEL:DeliveryAID)
          BIND('MAN:MID',MAN:MID)
          BIND('DELI:ContainerVessel',DELI:ContainerVessel)
          BIND('DELI:ContainerNo',DELI:ContainerNo)
          BIND('DEL:ClientReference',DEL:ClientReference)
          BIND('DEL:SpecialDeliveryInstructions', DEL:SpecialDeliveryInstructions)

          IF SFC:DINo ~= 0
             F    = 'DEL:DINo = ' & SFC:DINo
          .
          IF SFC:InvoiceNo ~= 0
             ! (p:Add, p:List, p:Delim, p:Option, p:Prefix)
             Add_To_List(' INV:IID = ' & SFC:InvoiceNo, F, ' AND')
          .
          IF SFC:ClientNo ~= 0
             Add_To_List(' CLI:ClientNo = ' & SFC:ClientNo, F, ' AND')
          .
          IF SFC:Date ~= 0
             Add_To_List(' DEL:DIDate = ' & SFC:Date, F, ' AND')
          .

          IF SFC:Sender ~= 0
             Add_To_List(' DEL:CollectionAID = ' & SFC:Sender, F, ' AND')
          .
          IF SFC:Receiver ~= 0
             Add_To_List(' DEL:DeliveryAID = ' & SFC:Receiver, F, ' AND')
          .

          IF SFC:Manifest ~= 0
             Add_To_List(' MAN:MID = ' & SFC:Manifest, F, ' AND')
          .

          IF CLIP(SFC:Vessel) ~= ''
             Add_To_List(' DELI:ContainerVessel = <39>' & CLIP(LEFT(SFC:Vessel)) & '<39>', F, ' AND')
          .
          IF CLIP(SFC:Container) ~= ''
             Add_To_List(' SQL(b.ContainerNo LIKE <39>%' & CLIP(LEFT(SFC:Container)) & '%<39>)', F, ' AND')
          .

          IF CLIP(SFC:ClientRef) ~= ''
             Add_To_List(' SQL(a.ClientReference LIKE <39>%' & CLIP(LEFT(SFC:ClientRef)) & '%<39>)', F, ' AND')
          .
          
          IF CLIP(SFC:SpecialInstructions) ~= ''
             Add_To_List(' SQL(a.SpecialDeliveryInstructions LIKE <39>%' & CLIP(LEFT(SFC:SpecialInstructions)) & '%<39>)', F, ' AND')
          .             

          View_Search.Init(SearchView, Relate:Deliveries)
          View_Search.AddSortOrder()
          !View_Search.AppendOrder()
          !View_Search.AddRange()
          View_Search.SetFilter(CLIP(F))
          View_Search.Reset()

          ! Note: We will have as many sub records as there are items on the DI, so multiple entries per DI

          FREE(SELF.DID_Q)
          LOOP
             IF View_Search.Next() ~= LEVEL:Benign
                BREAK
             .

             ! This record complies!
             IF SELF.Add_Q(DEL:DID) < 0
                ! But already in Q
          .  .
          View_Search.Kill()
          POPBIND()

          SETCURSOR()

          FS    = SELF.Process_Q(SFC:Weights, SFC:FromWeight, SFC:ToWeight)

       db.debugout('[Browse_Deliveries - Search]  F: ' & CLIP(F) & '||Recs: ' & RECORDS(SELF.DID_Q) & '||FS: ' & CLIP(FS))
!       MESSAGE('[Browse_Deliveries - Search]  F: ' & CLIP(F) & '||Recs: ' & RECORDS(SELF.DID_Q) & '||FS: ' & CLIP(FS))

    display

          IF RECORDS(SELF.DID_Q) = 1        ! Load straight to DI form
             GET(SELF.DID_Q,1)
             DEL:DID            = SELF.DID_Q.DID
             IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
                GlobalRequest   = ViewRecord
                Update_Deliveries()
          .  .

          IF CLIP(FS) ~= ''
             BRW1.SetFilter(CLIP(FS))
             BRW1.ResetSort(1)

             IF RECORDS(SELF.DID_Q) > 1     ! Otherwise we have shown this DI already
                BREAK
             .
          ELSE
             MESSAGE('Nothing found for that selection.', 'Search DI''s', ICON:Exclamation)
          .
       ELSE
          BREAK
    .  .
    RETURN



SearchClass.Add_Q           PROCEDURE(ULONG p:DID)
r_Ret       LONG(0)
    CODE
    SELF.DID_Q.DID          = p:DID
    GET(SELF.DID_Q, SELF.DID_Q.DID)
    IF ERRORCODE()
       SELF.DID_Q.Weight    = DELI:Weight
       ADD(SELF.DID_Q)
    ELSE
       SELF.DID_Q.Weight   += DELI:Weight
       PUT(SELF.DID_Q)
       r_Ret        = -1
    .
    RETURN(r_Ret)


SearchClass.Get_Q             PROCEDURE(ULONG p:DID)
Result  LONG
    CODE
    SELF.DID_Q.DID  = p:DID
    GET(SELF.DID_Q, SELF.DID_Q.DID)
    IF ~ERRORCODE()
       Result   = TRUE
    ELSE
       Result   = FALSE
    .

    db.debugout('[Browse_Deliveries - Search]  T / F: ' & Result & ',  DID: ' & p:DID)

    RETURN(Result)


SearchClass.Process_Q       PROCEDURE(BYTE p_Weights, LONG p_From_Weight, LONG p_To_Weight)
r_Idx       LONG
FS          STRING(4500)
Cond_Err    BYTE
Conditions  LONG

    CODE
    CLEAR(Conditions)
    CLEAR(Cond_Err)

    r_Idx        = RECORDS(SELF.DID_Q) + 1
    LOOP
       r_Idx    -= 1
       GET(SELF.DID_Q, r_Idx)
       IF ERRORCODE()
          BREAK
       .

       ! This is used to check total weight at the moment
       IF p_Weights = TRUE
          IF SELF.DID_Q.Weight >= p_From_Weight AND SELF.DID_Q.Weight <= p_To_Weight
          ELSE
             DELETE(SELF.DID_Q)
             CYCLE
       .  .


       IF CLIP(FS) = ''
          FS        = 'DEL:DID=' & SELF.DID_Q.DID
       ELSE
          ! Check that we have space in the string, otherwise we may have a BIND error on invalid field..
          IF Cond_Err = TRUE OR LEN(CLIP(FS)) + LEN(' OR DEL:DID=' & SELF.DID_Q.DID) > LEN(FS)
             Cond_Err    = TRUE
          ELSE
             Conditions += 1
             FS          = CLIP(FS) & ' OR DEL:DID=' & SELF.DID_Q.DID
    .  .  .

    IF Cond_Err = TRUE
       MESSAGE('There were too many results for the search to show them all.||Showing ' & Conditions & ' of ' & |
              RECORDS(SELF.DID_Q) & '.','Search Results',ICON:Exclamation)
    .
    RETURN(FS)
SearchClass.Mod_Filter            PROCEDURE()
    CODE
    BRW1.SetFilter('','ikbrel3')
    IF L_SG:Manifested = 1
       BRW1.SetFilter('DEL:Manifested > 0','ikbrel3')
    ELSIF L_SG:Manifested = 2
       BRW1.SetFilter('DEL:Manifested <<= 0','ikbrel3')
    .
    BRW1.ResetSort(1)
    RETURN
SearchClass.Construct               PROCEDURE()
    CODE
    SELF.DID_Q           &= NEW(DID_Q_Type)
    RETURN

SearchClass.Destruct        PROCEDURE()
    CODE
    DISPOSE(SELF.DID_Q)
    RETURN

ReleaseClass.Set_Filter             PROCEDURE()

Dates       GROUP,PRE(L)
From           DATE
ToD            DATE
            .

    CODE
    ! (p:From, p:To, p:Option, p:Heading)
    L:From   = TODAY()
    L:ToD    = TODAY()

    Dates    = Ask_Date_Range(L:From, L:ToD,, 'Date Range for DI Release Codes')

    BRW1.SetFilter('DEL:DIDate >= ' & L:From & ' AND DEL:DIDate < ' & L:ToD + 1)
    BRW1.SetFilter('DEL:ReleasedUID = 0','ikbrel2')

    IF L_SG:Manifested = TRUE
       BRW1.SetFilter('DEL:Manifested = 1','ikbrel3')
    ELSE
       BRW1.SetFilter('','ikbrel3')
    .

    BRW1.ResetSort(1)
    RETURN
ReleaseClass.Mod_Filter            PROCEDURE()
    CODE
    BRW1.SetFilter('','ikbrel3')
    IF L_SG:Manifested = 1
       BRW1.SetFilter('DEL:Manifested > 0','ikbrel3')
    ELSIF L_SG:Manifested = 2
       BRW1.SetFilter('DEL:Manifested <<= 0','ikbrel3')
    .
    BRW1.ResetSort(1)
    RETURN

BRW1.Fetch PROCEDURE(BYTE Direction)

GreenBarIndex   LONG,AUTO
  CODE
  PARENT.Fetch(Direction)
  !----------------------------------------------------------------------
    LOOP GreenBarIndex=1 TO RECORDS(SELF.Q)
      GET(SELF.Q,GreenBarIndex)
      SELF.Q.DEL:DINo_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:DINo
      SELF.Q.DEL:DINo_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:DINo_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:DINo_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.CLI:ClientName_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for CLI:ClientName
      SELF.Q.CLI:ClientName_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.CLI:ClientName_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.CLI:ClientName_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.CLI:ClientNo_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for CLI:ClientNo
      SELF.Q.CLI:ClientNo_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.CLI:ClientNo_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.CLI:ClientNo_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:ClientReference_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:ClientReference
      SELF.Q.DEL:ClientReference_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:ClientReference_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:ClientReference_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:DIDate_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:DIDate
      SELF.Q.DEL:DIDate_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:DIDate_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:DIDate_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.L_QG:Weight_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for L_QG:Weight
      SELF.Q.L_QG:Weight_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.L_QG:Weight_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.L_QG:Weight_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.L_QG:Total_Units_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for L_QG:Total_Units
      SELF.Q.L_QG:Total_Units_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.L_QG:Total_Units_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.L_QG:Total_Units_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.L_QG:Remaining_Units_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for L_QG:Remaining_Units
      SELF.Q.L_QG:Remaining_Units_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.L_QG:Remaining_Units_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.L_QG:Remaining_Units_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:Rate_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:Rate
      SELF.Q.DEL:Rate_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:Rate_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:Rate_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:Charge_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:Charge
      SELF.Q.DEL:Charge_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:Charge_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:Charge_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:DocumentCharge_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:DocumentCharge
      SELF.Q.DEL:DocumentCharge_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:DocumentCharge_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:DocumentCharge_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:FuelSurcharge_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:FuelSurcharge
      SELF.Q.DEL:FuelSurcharge_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:FuelSurcharge_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:FuelSurcharge_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:TollCharge_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:TollCharge
      SELF.Q.DEL:TollCharge_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:TollCharge_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:TollCharge_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:AdditionalCharge_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:AdditionalCharge
      SELF.Q.DEL:AdditionalCharge_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:AdditionalCharge_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:AdditionalCharge_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.L_G:TotalCharge_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for L_G:TotalCharge
      SELF.Q.L_G:TotalCharge_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.L_G:TotalCharge_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.L_G:TotalCharge_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.L_G:Total_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for L_G:Total
      SELF.Q.L_G:Total_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.L_G:Total_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.L_G:Total_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.JOU:Journey_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for JOU:Journey
      SELF.Q.JOU:Journey_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.JOU:Journey_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.JOU:Journey_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.ADD:AddressName_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for ADD:AddressName
      SELF.Q.ADD:AddressName_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.ADD:AddressName_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.ADD:AddressName_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.A_ADD:AddressName_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for A_ADD:AddressName
      SELF.Q.A_ADD:AddressName_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.A_ADD:AddressName_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.A_ADD:AddressName_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:CreatedDate_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:CreatedDate
      SELF.Q.DEL:CreatedDate_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:CreatedDate_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:CreatedDate_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:CreatedTime_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:CreatedTime
      SELF.Q.DEL:CreatedTime_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:CreatedTime_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:CreatedTime_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.L_QG:Manifested_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for L_QG:Manifested
      SELF.Q.L_QG:Manifested_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.L_QG:Manifested_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.L_QG:Manifested_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.FLO:Floor_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for FLO:Floor
      SELF.Q.FLO:Floor_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.FLO:Floor_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.FLO:Floor_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.BRA:BranchName_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for BRA:BranchName
      SELF.Q.BRA:BranchName_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.BRA:BranchName_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.BRA:BranchName_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.INV:IID_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for INV:IID
      SELF.Q.INV:IID_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.INV:IID_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.INV:IID_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:MultipleManifestsAllowed_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:MultipleManifestsAllowed
      SELF.Q.DEL:MultipleManifestsAllowed_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:MultipleManifestsAllowed_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:MultipleManifestsAllowed_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.USE:Login_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for USE:Login
      SELF.Q.USE:Login_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.USE:Login_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.USE:Login_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.SERI:ServiceRequirement_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for SERI:ServiceRequirement
      SELF.Q.SERI:ServiceRequirement_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.SERI:ServiceRequirement_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.SERI:ServiceRequirement_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.LOC:Terms_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for LOC:Terms
      SELF.Q.LOC:Terms_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.LOC:Terms_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.LOC:Terms_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:Manifested_NormalFG   = CHOOSE(GreenBarIndex % 2,-1,-1) ! Set color values for DEL:Manifested
      SELF.Q.DEL:Manifested_NormalBG   = CHOOSE(GreenBarIndex % 2,-1,11599814)
      SELF.Q.DEL:Manifested_SelectedFG = CHOOSE(GreenBarIndex % 2,-1,-1)
      SELF.Q.DEL:Manifested_SelectedBG = CHOOSE(GreenBarIndex % 2,-1,-1)
      PUT(SELF.Q)
    END
  !----------------------------------------------------------------------


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?CurrentTab) = 6
    RETURN SELF.SetSort(5,Force)
  ELSIF CHOICE(?CurrentTab) = 7
    RETURN SELF.SetSort(6,Force)
  ELSIF CHOICE(?CurrentTab) = 8
    RETURN SELF.SetSort(7,Force)
  ELSE
    RETURN SELF.SetSort(8,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! 'Pre Paid|#0|COD|#1|Account|#2'
      EXECUTE DEL:Terms
         LOC:Terms    = 'COD'
         LOC:Terms    = 'Account'
      ELSE
         LOC:Terms    = 'Pre Paid'
      .
  

      IF L_SG:DI_Info = TRUE
      db.Debugout('           **********  L_SG:DI_Info: ' & L_SG:DI_Info)
   
         L_QG:Total_Units          = Get_DelItem_s_Totals(DEL:DID, 3)
         L_QG:Remaining_Units      = Get_DelItem_s_Totals(DEL:DID, 1)
         L_QG:Weight               = Get_DelItem_s_Totals(DEL:DID, 6) / 100
      .
  
      ! Not Manifested|Partially Manifested|Partially Manifested Multiple|Fully Manifested|Fully Manifested Multiple
      EXECUTE DEL:Manifested
         L_QG:Manifested  = 'Partially Manifested'
         L_QG:Manifested  = 'Partially Manifested Multiple'
         L_QG:Manifested  = 'Fully Manifested'
         L_QG:Manifested  = 'Fully Manifested Multiple'
      ELSE
         L_QG:Manifested  = 'Not Manifested'
      .


    IF DEL:Insure = TRUE
       L_G:InsuanceAmount   = DEL:TotalConsignmentValue * (DEL:InsuranceRate / 100)
    .
    L_G:TotalCharge         = DEL:Charge + DEL:AdditionalCharge + DEL:DocumentCharge + DEL:FuelSurcharge + DEL:TollCharge + L_G:InsuanceAmount
    L_G:Total               = L_G:TotalCharge + L_G:TotalCharge * (DEL:VATRate / 100)

  PARENT.SetQueueRecord
  
  SELF.Q.L_QG:Weight_Style = 0 ! 
  SELF.Q.L_QG:Total_Units_Style = 0 ! 
  SELF.Q.L_QG:Remaining_Units_Style = 0 ! 
  IF (DEL:MultipleManifestsAllowed = 1)
    SELF.Q.DEL:MultipleManifestsAllowed_Icon = 2           ! Set icon from icon list
  ELSE
    SELF.Q.DEL:MultipleManifestsAllowed_Icon = 1           ! Set icon from icon list
  END
  !----------------------------------------------------------------------
      SELF.Q.DEL:DINo_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:DINo
      SELF.Q.DEL:DINo_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:DINo_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:DINo_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.CLI:ClientName_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for CLI:ClientName
      SELF.Q.CLI:ClientName_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.CLI:ClientName_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.CLI:ClientName_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.CLI:ClientNo_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for CLI:ClientNo
      SELF.Q.CLI:ClientNo_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.CLI:ClientNo_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.CLI:ClientNo_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:ClientReference_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:ClientReference
      SELF.Q.DEL:ClientReference_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:ClientReference_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:ClientReference_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:DIDate_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:DIDate
      SELF.Q.DEL:DIDate_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:DIDate_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:DIDate_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.L_QG:Weight_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for L_QG:Weight
      SELF.Q.L_QG:Weight_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.L_QG:Weight_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.L_QG:Weight_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.L_QG:Total_Units_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for L_QG:Total_Units
      SELF.Q.L_QG:Total_Units_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.L_QG:Total_Units_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.L_QG:Total_Units_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.L_QG:Remaining_Units_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for L_QG:Remaining_Units
      SELF.Q.L_QG:Remaining_Units_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.L_QG:Remaining_Units_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.L_QG:Remaining_Units_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:Rate_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:Rate
      SELF.Q.DEL:Rate_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:Rate_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:Rate_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:Charge_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:Charge
      SELF.Q.DEL:Charge_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:Charge_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:Charge_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:DocumentCharge_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:DocumentCharge
      SELF.Q.DEL:DocumentCharge_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:DocumentCharge_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:DocumentCharge_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:FuelSurcharge_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:FuelSurcharge
      SELF.Q.DEL:FuelSurcharge_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:FuelSurcharge_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:FuelSurcharge_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:TollCharge_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:TollCharge
      SELF.Q.DEL:TollCharge_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:TollCharge_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:TollCharge_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:AdditionalCharge_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:AdditionalCharge
      SELF.Q.DEL:AdditionalCharge_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:AdditionalCharge_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:AdditionalCharge_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.L_G:TotalCharge_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for L_G:TotalCharge
      SELF.Q.L_G:TotalCharge_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.L_G:TotalCharge_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.L_G:TotalCharge_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.L_G:Total_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for L_G:Total
      SELF.Q.L_G:Total_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.L_G:Total_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.L_G:Total_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.JOU:Journey_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for JOU:Journey
      SELF.Q.JOU:Journey_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.JOU:Journey_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.JOU:Journey_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.ADD:AddressName_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for ADD:AddressName
      SELF.Q.ADD:AddressName_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.ADD:AddressName_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.ADD:AddressName_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.A_ADD:AddressName_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for A_ADD:AddressName
      SELF.Q.A_ADD:AddressName_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.A_ADD:AddressName_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.A_ADD:AddressName_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:CreatedDate_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:CreatedDate
      SELF.Q.DEL:CreatedDate_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:CreatedDate_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:CreatedDate_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:CreatedTime_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:CreatedTime
      SELF.Q.DEL:CreatedTime_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:CreatedTime_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:CreatedTime_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.L_QG:Manifested_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for L_QG:Manifested
      SELF.Q.L_QG:Manifested_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.L_QG:Manifested_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.L_QG:Manifested_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.FLO:Floor_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for FLO:Floor
      SELF.Q.FLO:Floor_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.FLO:Floor_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.FLO:Floor_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.BRA:BranchName_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for BRA:BranchName
      SELF.Q.BRA:BranchName_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.BRA:BranchName_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.BRA:BranchName_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.INV:IID_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for INV:IID
      SELF.Q.INV:IID_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.INV:IID_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.INV:IID_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:MultipleManifestsAllowed_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:MultipleManifestsAllowed
      SELF.Q.DEL:MultipleManifestsAllowed_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:MultipleManifestsAllowed_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:MultipleManifestsAllowed_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.USE:Login_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for USE:Login
      SELF.Q.USE:Login_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.USE:Login_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.USE:Login_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.SERI:ServiceRequirement_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for SERI:ServiceRequirement
      SELF.Q.SERI:ServiceRequirement_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.SERI:ServiceRequirement_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.SERI:ServiceRequirement_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.LOC:Terms_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for LOC:Terms
      SELF.Q.LOC:Terms_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.LOC:Terms_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.LOC:Terms_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:Manifested_NormalFG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1) ! Set color values for DEL:Manifested
      SELF.Q.DEL:Manifested_NormalBG   = CHOOSE(CHOICE(?Browse:1) % 2,-1,11599814)
      SELF.Q.DEL:Manifested_SelectedFG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
      SELF.Q.DEL:Manifested_SelectedBG = CHOOSE(CHOICE(?Browse:1) % 2,-1,-1)
  !----------------------------------------------------------------------
      DO Style_Entry


BRW1.TakeNewSelection PROCEDURE

  CODE
  PARENT.TakeNewSelection
      IF p:Start_Insert = 'R'                 ! Release mode
         BRW1.UpdateViewRecord()
  
         ! (p_Statement_Info, p:DID)
         ! (<*STRING>, ULONG=0),LONG
         ! p:DID
         !   0   - Delivery and Client records loaded
         !           Client fields   - CLI:Terms, CLI:UpdatedDate, CLI:BalanceCurrent, CLI:Balance30Days, CLI:Balance60Days
         !                             CLI:Balance90Days, CLI:CID, CLI:Status, CLI:Limit
         !           Delivery fields - DEL:ReleasedUID
         !
         !   Returns
         !   0   - None of the Below
         !   1   - Released
         !   2   - Acc. Pre-Paid
         !   3   - 60 Day+ Balance
         !   4   - Account Limit
         !   5   - Acc. On Hold
         !   6   - Acc. Closed
         !   7   - Acc. Dormant
         !   8   -
  
         !EXECUTE Check_Delivery_Release(,DEL:DID)
         Check_Delivery_Release(, DEL:DID, LOC:ReleaseReason)
         QuickWindow{PROP:Text}    = L_SG:Release_Button_Txt & ' - ' & DEL:DINo & ' ' & CLIP(LOC:ReleaseReason)

!            QuickWindow{PROP:Text}    = L_SG:Release_Button_Txt & ' - ' & DEL:DINo & ' Acc. Pre-Paid'
!            QuickWindow{PROP:Text}    = L_SG:Release_Button_Txt & ' - ' & DEL:DINo & ' 60 Day+ Balance'
!            QuickWindow{PROP:Text}    = L_SG:Release_Button_Txt & ' - ' & DEL:DINo & ' Account Limit'
!            QuickWindow{PROP:Text}    = L_SG:Release_Button_Txt & ' - ' & DEL:DINo & ' Acc. On Hold'
!            QuickWindow{PROP:Text}    = L_SG:Release_Button_Txt & ' - ' & DEL:DINo & ' Acc. Closed'
!            QuickWindow{PROP:Text}    = L_SG:Release_Button_Txt & ' - ' & DEL:DINo & ' Acc. Dormant'
!            QuickWindow{PROP:Text}    = L_SG:Release_Button_Txt & ' - ' & DEL:DINo & ' '
!         ELSE
!            QuickWindow{PROP:Text}    = L_SG:Release_Button_Txt & ' - ' & DEL:DINo & ' <unknown release>'
      .  !.
  


BRW1.UpdateViewRecord PROCEDURE

  CODE
      ! Update the buffer contents to prevent error msg
      GET(Queue:Browse:1,Choice(?Browse:1))
      IF ~ERRORCODE() 
         REGET(self.View, Queue:Browse:1.Viewposition)
         IF ERRORCODE() 
           ! MESSAGE('Errorcode on reget.||Error: ' & ERROR() & '||Code: ' & ERRORCODE() & '|||F Err: ' & FILEERROR() & '|||Request: ' & GlobalRequest & ' (chg: ' & ChangeRecord & ')')
            IF ERRORCODE() = 35
               ! See if the record still is present in the table before allowing no error
               DEL:DID    = Queue:Browse:1.DEL:DID
               IF Access:Deliveries.Tryfetch(DEL:PKey_DID) = LEVEL:Benign
                  ! Ok - no error and continue
                  RETURN
               ELSE
                  ! Problem - report error
            .  .
         ELSE
            ! Do nothing - it will be successed in PARENT call
      .  .
  
  
      !db.debugout('[Browse_Deliveries]  temp - update view rec called...')
  PARENT.UpdateViewRecord


BRW1.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
      IF SELF.Records() <> 0
         ENABLE(?Button_ReleaseDI)
      ELSE
         DISABLE(?Button_ReleaseDI)
      .


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF p:Start_Insert = 'R'
         CASE Check_Delivery_Release(,DEL:DID)
         OF 1                                      ! Released
            ReturnValue    = Record:Filtered
         OF 0                                      ! No release needed
            ReturnValue    = Record:Filtered
         ELSE
      .  .
  
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Bottom_Left, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Bottom_Left
  SELF.SetStrategy(?Group_BadType, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_BadType
  SELF.SetStrategy(?Group_locator, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_locator
  SELF.SetStrategy(?GROUP_searchtopright, Resize:FixRight+Resize:FixTop, Resize:LockSize) ! Override strategy for ?GROUP_searchtopright


FDB8.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      ! Add an All
      Queue:FileDrop.BRA:BranchName      = 'All'
      GET(Queue:FileDrop, Queue:FileDrop.BRA:BranchName)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop)
         Queue:FileDrop.BRA:BranchName      = 'All'
         Queue:FileDrop.BRA:BID             = 0
         ADD(Queue:FileDrop)
      .
  RETURN ReturnValue


FDB8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
  !   what was this doing here?   17 April 06
  !    IF p:Start_Insert = 'S'
  !       IF SearchMe_.Get_Q(DEL:DID) > 0
  !       ELSE
  !          ReturnValue    = Record:Filtered
  !    .  .
  !
  !
  !    db.debugout('ReturnValue: ' & ReturnValue)
  RETURN ReturnValue

