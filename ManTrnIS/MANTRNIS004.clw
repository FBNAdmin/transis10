

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS004.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_TransporterPayments PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LO_G:Screen          GROUP,PRE(LO_G)                       !
TransporterName      STRING(35)                            !Transporters Name
UnAllocated_Amt      DECIMAL(10,2)                         !
Allocated_Amount     DECIMAL(10,2)                         !
                     END                                   !
LO_G:Cost_Total      DECIMAL(12,2)                         !
L_BG:Payments        DECIMAL(12,2)                         !
L_BG:Credits         DECIMAL(12,2)                         !
L_BG:Outstanding     DECIMAL(12,2)                         !
LOC:Tag_Group        GROUP,PRE(L_TG)                       !
Loading_Problems     LONG                                  !
Problems             STRING(1000)                          !
To_Allocate          DECIMAL(10,2)                         !Amount of payment
Change_Allocation_Amount DECIMAL(10,2)                     !Amount of payment allocated to this Invoice
TRPAID_Selected      ULONG                                 !Trasnporter Payment Allocation ID
ID                   ULONG                                 !
                     END                                   !
BRW2::View:Browse    VIEW(TransporterPaymentsAllocations)
                       PROJECT(TRAPA:AllocationNo)
                       PROJECT(TRAPA:AllocationDate)
                       PROJECT(TRAPA:Amount)
                       PROJECT(TRAPA:Comment)
                       PROJECT(TRAPA:TIN)
                       PROJECT(TRAPA:TPID)
                       PROJECT(TRAPA:TRPAID)
                       JOIN(INT:PKey_TIN,TRAPA:TIN)
                         PROJECT(INT:Cost)
                         PROJECT(INT:VAT)
                         PROJECT(INT:CreatedDate)
                         PROJECT(INT:TIN)
                         PROJECT(INT:MID)
                         PROJECT(INT:BID)
                         JOIN(MAN:PKey_MID,INT:MID)
                           PROJECT(MAN:MID)
                           PROJECT(MAN:JID)
                           JOIN(JOU:PKey_JID,MAN:JID)
                             PROJECT(JOU:Journey)
                             PROJECT(JOU:JID)
                           END
                         END
                         JOIN(BRA:PKey_BID,INT:BID)
                           PROJECT(BRA:BranchName)
                           PROJECT(BRA:BID)
                         END
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
TRAPA:AllocationNo     LIKE(TRAPA:AllocationNo)       !List box control field - type derived from field
TRAPA:AllocationDate   LIKE(TRAPA:AllocationDate)     !List box control field - type derived from field
TRAPA:Amount           LIKE(TRAPA:Amount)             !List box control field - type derived from field
TRAPA:Comment          LIKE(TRAPA:Comment)            !List box control field - type derived from field
TRAPA:TIN              LIKE(TRAPA:TIN)                !List box control field - type derived from field
L_BG:Outstanding       LIKE(L_BG:Outstanding)         !List box control field - type derived from local data
LO_G:Cost_Total        LIKE(LO_G:Cost_Total)          !List box control field - type derived from local data
L_BG:Payments          LIKE(L_BG:Payments)            !List box control field - type derived from local data
L_BG:Credits           LIKE(L_BG:Credits)             !List box control field - type derived from local data
INT:Cost               LIKE(INT:Cost)                 !List box control field - type derived from field
INT:VAT                LIKE(INT:VAT)                  !List box control field - type derived from field
MAN:MID                LIKE(MAN:MID)                  !List box control field - type derived from field
INT:CreatedDate        LIKE(INT:CreatedDate)          !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
TRAPA:TPID             LIKE(TRAPA:TPID)               !List box control field - type derived from field
TRAPA:TRPAID           LIKE(TRAPA:TRPAID)             !List box control field - type derived from field
INT:TIN                LIKE(INT:TIN)                  !Related join file key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::TRAP:Record LIKE(TRAP:RECORD),THREAD
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Transporter Payments'),AT(,,388,290),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateTransporterPayments'),SYSTEM
                       SHEET,AT(4,4,381,267),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           GROUP,AT(53,22,202,10),USE(?Group_Transporter)
                             BUTTON('...'),AT(53,22,12,10),USE(?CallLookup)
                             ENTRY(@s35),AT(69,22,186,10),USE(LO_G:TransporterName),MSG('Transporters Name'),REQ,TIP('Transporters Name')
                           END
                           PROMPT('TPID:'),AT(261,6),USE(?TRAP:TPID:Prompt),TRN
                           STRING(@n_10),AT(337,6,,10),USE(TRAP:TPID),RIGHT(1),TRN
                           PROMPT('Transporter:'),AT(9,22),USE(?LO_G:TransporterName:Prompt),TRN
                           PROMPT('TID:'),AT(261,22),USE(?TRAP:TID:Prompt),TRN
                           STRING(@n_10),AT(337,22,,10),USE(TRAP:TID),RIGHT(1),TRN
                           PROMPT('Date Captured:'),AT(261,40),USE(?TRAP:DateCaptured:Prompt),TRN
                           STRING(@d6b),AT(333,40,,10),USE(TRAP:DateCaptured),RIGHT(1),TRN
                           PROMPT('Date Made:'),AT(9,40),USE(?TRAP:DateMade:Prompt),TRN
                           SPIN(@d17),AT(69,40,64,10),USE(TRAP:DateMade),RIGHT(1),MSG('Date payment was made'),TIP('Date payme' & |
  'nt was made')
                           BUTTON('...'),AT(53,40,12,10),USE(?Calendar)
                           PROMPT('Payment Amount:'),AT(9,54),USE(?TRAP:Amount:Prompt),TRN
                           ENTRY(@n-14.2),AT(69,54,64,10),USE(TRAP:Amount),RIGHT(1),MSG('Amount of payment'),TIP('Amount of payment')
                           PROMPT('Un-Allocated Amt:'),AT(261,68),USE(?L_SV:UnAllocated_Amt:Prompt),TRN
                           ENTRY(@n-14.2),AT(322,68,57,10),USE(LO_G:UnAllocated_Amt),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           PROMPT('Type:'),AT(9,68),USE(?TRAP:Type:Prompt),TRN
                           LIST,AT(69,68,64,10),USE(TRAP:Type),DISABLE,DROP(5),FROM('Payment|#0|Reversal|#1'),MSG('Payment, Reversal'), |
  TIP('Payment, Reversal')
                           PROMPT('Allocated Amount:'),AT(261,54),USE(?L_SV:Allocated_Amount:Prompt),TRN
                           ENTRY(@n-14.2),AT(322,54,57,10),USE(LO_G:Allocated_Amount),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           LINE,AT(10,81,370,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           LIST,AT(10,86,369,107),USE(?Browse:2),HVSCROLL,FORMAT('[22R(2)|M~No.~C(0)@n_5@38R(2)|M~' & |
  'Date~C(0)@d5b@52R(1)|M~Amount~C(0)@n-14.2@80L(2)|M~Comment~C(0)@s255@]|M~Allocation~' & |
  '[40R(2)|M~TIN~C(0)@n_10@44R(1)|M~Outstanding~C(0)@n-17.2@44R(1)|M~Total~C(0)@n-17.2@' & |
  '44R(1)|M~Payments~C(0)@n-17.2@44R(1)|M~Credits~C(0)@n-17.2@44R(2)|M~Cost~C(0)@n-14.2' & |
  '@38R(2)|M~VAT~C(0)@n-14.2@36R(2)|M~MID~C(0)@n_10@38R(2)|M~Date~C(0)@d5b@50L(2)|M~Jou' & |
  'rney~C(0)@s70@50L(2)|M~Branch~C(0)@s35@]|M~Transporter Invocie~40R(2)|M~TPID~C(0)@n_' & |
  '10@40R(2)|M~TRPAID~C(0)@n_10@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the Transport' & |
  'erPaymentsAllocations file')
                           PROMPT('Change Allocation Amount:'),AT(93,198),USE(?Change_Allocation_Amount:Prompt),TRN
                           ENTRY(@n-14.2),AT(182,198,57,10),USE(L_TG:Change_Allocation_Amount),RIGHT(1),MSG('Amount of payment'), |
  TIP('Amount of payment allocated to this Invoice')
                           BUTTON('Update'),AT(245,198,49,10),USE(?Button_Update)
                           LINE,AT(78,212,230,0),USE(?Line3),COLOR(COLOR:Black)
                           BUTTON('&Save'),AT(10,214,53,14),USE(?Save),LEFT,KEY(CtrlAltEnter),ICON('SAVE.ICO'),FLAT,TIP('Save Recor' & |
  'd and Close')
                           BUTTON('&Tag Invoices'),AT(146,214,,14),USE(?Button_TagInvoices),LEFT,ICON('Doctag.ico'),FLAT
                           GROUP,AT(225,214,155,14),USE(?Group_Browse)
                             BUTTON('&Insert'),AT(225,214,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                             BUTTON('&Change'),AT(278,214,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                             BUTTON('&Delete'),AT(330,214,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                           END
                           LINE,AT(10,231,370,0),USE(?Line1:2),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Notes:'),AT(9,236),USE(?TRAP:Notes:Prompt),TRN
                           TEXT,AT(69,236,313,30),USE(TRAP:Notes),VSCROLL,MSG('Notes for this payment'),TIP('Notes for ' & |
  'this payment')
                           PROMPT('TPID Reversal:'),AT(5,276),USE(?TRAP:TPID_Reversal:Prompt),HIDE,TRN
                           STRING(@n_10),AT(57,276,44,10),USE(TRAP:TPID_Reversal),RIGHT(1),HIDE,TRN
                         END
                         TAB('&2) Bulk Allocation'),USE(?Tab2)
                           PROMPT('Problems:'),AT(10,24),USE(?Problems:Prompt),TRN
                           TEXT,AT(61,24,321,102),USE(L_TG:Problems),VSCROLL,BOXED
                         END
                       END
                       BUTTON('&OK'),AT(284,274,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(336,274,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(194,274,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW2::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar10           CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
Inv_Tags              shpTagClass

IS_Q                  Q_Class

!Find_Row            PROCEDURE(STRING p_Element, *LONG p_ColNo),LONG,PROC
!
!Get_Row_Item        PROCEDURE(LONG p_RowNo=0, LONG p_ColNo, *STRING p_Element),LONG,PROC
!
!Set_Row_Item        PROCEDURE(LONG p_ColNo, STRING p_Element, LONG p_RowNo=0),LONG,PROC
!Add_Row_Item        PROCEDURE(LONG p_ColNo, STRING p_Element),LONG,PROC
!
!Delete_Row          PROCEDURE(LONG p_RowNo),LONG,PROC

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Add_From_Tags                   ROUTINE
    DATA
R:Idx           ULONG

    CODE
    L_TG:Loading_Problems        = FALSE

    IF Inv_Tags.NumberTagged() > 0
       L_TG:To_Allocate          = LO_G:UnAllocated_Amt

       LOOP
          R:Idx    += 1
          GET(Inv_Tags.TagQueue, R:Idx)
          IF ERRORCODE()
             BREAK
          .

          INT:TIN               = Inv_Tags.TagQueue.Ptr

          IF Access:_InvoiceTransporter.TryFetch(INT:PKey_TIN) = LEVEL:Benign
             L_TG:ID            = INT:TIN              ! Used to error notes

             DO Get_Inv_Allocate
          ELSE
             IF CLIP(L_TG:Problems) = ''
                L_TG:Problems   = 'TIN: ' & Inv_Tags.TagQueue.Ptr
             ELSE
                L_TG:Problems   = CLIP(L_TG:Problems) & ', TIN: ' & Inv_Tags.TagQueue.Ptr
             .
             L_TG:Problems      = CLIP(L_TG:Problems) & ' (Tagged Invoice not found!)'
       .  .

       ThisWindow.Reset(1)

       IF L_TG:Loading_Problems = TRUE
          MESSAGE('Some problems were encountered.||' & |
                  'Please check the Problems information box under the Bulk Allocation tab.', 'Invoice Allocation', ICON:Exclamation)
    .  .

    ENABLE(?Group_Browse)           ! Not sure why this is being disabled when Insert Mode???  16 May 06
    EXIT
Get_Inv_Allocate                  ROUTINE
    DATA
R:Allocate          LIKE(LO_G:UnAllocated_Amt)
R:Outstanding       LIKE(LO_G:UnAllocated_Amt)
R:Found             BYTE

    CODE
    ! Check this invoice is not already allocated here...
    ! October 2 - not sure how this check was intended to work.  Need to iterate through all instances of this
    ! invoice ID in the ClientsPaymentsAllocation table to be check...  or iterate through all invoices allocated
    ! to this payment to look for this IID.  One of the 2.
    R:Found                 = FALSE

    CLEAR(TRAPA:Record,-1)
    TRAPA:TIN               = INT:TIN
    SET(TRAPA:FKey_TIN, TRAPA:FKey_TIN)
    LOOP
       IF Access:TransporterPaymentsAllocations.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF TRAPA:TIN ~= INT:TIN
          BREAK
       .
       IF TRAPA:TPID = TRAP:TPID
          R:Found           = TRUE
          BREAK
    .  .


    !TRAPA:CPAID             = INV:IID
    IF R:Found = TRUE
       IF CLIP(L_TG:Problems) = ''
          L_TG:Problems     = 'TINs: ' & L_TG:ID
       ELSE
          L_TG:Problems     = CLIP(L_TG:Problems) & ', ' & L_TG:ID
       .
       L_TG:Problems        = CLIP(L_TG:Problems) & ' (Invoice already allocated here.)'
       L_TG:Loading_Problems = TRUE
    ELSE
!    db.debugout('[Update_ClientsPayments - Add_From_Tags]  Get...Outstand..')

       ! Checks Invoice for amts and Creates allocation entry
       R:Outstanding           = DEFORMAT(Get_InvTransporter_Outstanding(INT:TIN))

       IF R:Outstanding <= 0.0                                         ! Nothing to allocate
          IF CLIP(L_TG:Problems) = ''
             L_TG:Problems     = 'TINs: ' & L_TG:ID
          ELSE
             L_TG:Problems     = CLIP(L_TG:Problems) & ', ' & L_TG:ID
          .
          L_TG:Problems        = CLIP(L_TG:Problems) & ' (No outstanding on Invoice)'
          L_TG:Loading_Problems = TRUE
       ELSE
          IF R:Outstanding < L_TG:To_Allocate                                 ! Can allocate full amount
             R:Allocate        = R:Outstanding
          ELSE
             R:Allocate        = L_TG:To_Allocate
          .

          ! New check - is R:Allocate <= allocation specified on tag screen
          Col_#         = 1
          Row_#         = IS_Q.Find_Row(INT:TIN, Col_#)
          Allocate_"    = ''
          IF Row_# > 0
             IS_Q.Get_Row_Item(Row_#, 2, Allocate_")
             R:Allocate        = Allocate_"
          .

          ! Create allocation record
          CLEAR(TRAPA:Record)
          TRAPA:TPID               = TRAP:TPID
          IF Access:TransporterPaymentsAllocations.TryPrimeAutoInc() ~= LEVEL:Benign
             IF CLIP(L_TG:Problems) = ''
                L_TG:Problems  = 'TINs: ' & L_TG:ID
             ELSE
                L_TG:Problems  = CLIP(L_TG:Problems) & ', ' & L_TG:ID
             .
             L_TG:Problems     = CLIP(L_TG:Problems) & ' (Transporter Payment could not be Primed)'
             L_TG:Loading_Problems = TRUE
          ELSE
             !TRAPA:CPAID
             !TRAPA:CPID            =
             !TRAPA:AllocationNo    =
             TRAPA:TIN             = INT:TIN
             TRAPA:Amount          = R:Allocate
             TRAPA:AllocationDate  = TODAY()
             TRAPA:AllocationTime  = CLOCK()
             TRAPA:Comment         = ''

             IF Access:TransporterPaymentsAllocations.TryInsert() = LEVEL:Benign             ! Added
                L_TG:To_Allocate      -= R:Allocate                                         ! Reduce available

                Upd_InvoiceTransporter_Paid_Status(INT:TIN)
             ELSE
                IF CLIP(L_TG:Problems) = ''
                   L_TG:Problems   = 'TINs: ' & L_TG:ID
                ELSE
                   L_TG:Problems   = CLIP(L_TG:Problems) & ', ' & L_TG:ID
                .
                L_TG:Problems      = CLIP(L_TG:Problems) & ' (Transporter Payment could not be Inserted)'
                L_TG:Loading_Problems = TRUE
                Access:TransporterPaymentsAllocations.CancelAutoInc()
    .  .  .  .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Transporter Payment Record'
  OF InsertRecord
    ActionMessage = 'Transporter Payment Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Transporter Payment Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_TransporterPayments')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CallLookup
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_BG:Outstanding',L_BG:Outstanding)       ! Added by: BrowseBox(ABC)
  BIND('LO_G:Cost_Total',LO_G:Cost_Total)         ! Added by: BrowseBox(ABC)
  BIND('L_BG:Payments',L_BG:Payments)             ! Added by: BrowseBox(ABC)
  BIND('L_BG:Credits',L_BG:Credits)               ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:TransporterPayments)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(TRAP:Record,History::TRAP:Record)
  SELF.AddHistoryField(?TRAP:TPID,1)
  SELF.AddHistoryField(?TRAP:TID,2)
  SELF.AddHistoryField(?TRAP:DateCaptured,5)
  SELF.AddHistoryField(?TRAP:DateMade,9)
  SELF.AddHistoryField(?TRAP:Amount,11)
  SELF.AddHistoryField(?TRAP:Type,13)
  SELF.AddHistoryField(?TRAP:Notes,12)
  SELF.AddHistoryField(?TRAP:TPID_Reversal,14)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                         ! File Transporter used by this procedure, so make sure it's RelationManager is open
  Access:TransporterPayments.UseFile              ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TransporterPayments
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.SaveControl = ?Save
  SELF.DisableCancelButton = 1
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:TransporterPaymentsAllocations,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?LO_G:TransporterName{PROP:ReadOnly} = True
    DISABLE(?Calendar)
    ?TRAP:Amount{PROP:ReadOnly} = True
    ?LO_G:UnAllocated_Amt{PROP:ReadOnly} = True
    DISABLE(?TRAP:Type)
    ?LO_G:Allocated_Amount{PROP:ReadOnly} = True
    ?L_TG:Change_Allocation_Amount{PROP:ReadOnly} = True
    DISABLE(?Button_Update)
    DISABLE(?Button_TagInvoices)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.FileLoaded = 1                             ! This is a 'file loaded' browse
  BRW2.AddSortOrder(,TRAPA:FKey_TPID_AllocationNo) ! Add the sort order for TRAPA:FKey_TPID_AllocationNo for sort order 1
  BRW2.AddRange(TRAPA:TPID,Relate:TransporterPaymentsAllocations,Relate:TransporterPayments) ! Add file relationship range limit for sort order 1
  BRW2.AddLocator(BRW2::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW2::Sort0:Locator.Init(,TRAPA:AllocationNo,1,BRW2) ! Initialize the browse locator using  using key: TRAPA:FKey_TPID_AllocationNo , TRAPA:AllocationNo
  BRW2.AddField(TRAPA:AllocationNo,BRW2.Q.TRAPA:AllocationNo) ! Field TRAPA:AllocationNo is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:AllocationDate,BRW2.Q.TRAPA:AllocationDate) ! Field TRAPA:AllocationDate is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:Amount,BRW2.Q.TRAPA:Amount) ! Field TRAPA:Amount is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:Comment,BRW2.Q.TRAPA:Comment) ! Field TRAPA:Comment is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:TIN,BRW2.Q.TRAPA:TIN)       ! Field TRAPA:TIN is a hot field or requires assignment from browse
  BRW2.AddField(L_BG:Outstanding,BRW2.Q.L_BG:Outstanding) ! Field L_BG:Outstanding is a hot field or requires assignment from browse
  BRW2.AddField(LO_G:Cost_Total,BRW2.Q.LO_G:Cost_Total) ! Field LO_G:Cost_Total is a hot field or requires assignment from browse
  BRW2.AddField(L_BG:Payments,BRW2.Q.L_BG:Payments) ! Field L_BG:Payments is a hot field or requires assignment from browse
  BRW2.AddField(L_BG:Credits,BRW2.Q.L_BG:Credits) ! Field L_BG:Credits is a hot field or requires assignment from browse
  BRW2.AddField(INT:Cost,BRW2.Q.INT:Cost)         ! Field INT:Cost is a hot field or requires assignment from browse
  BRW2.AddField(INT:VAT,BRW2.Q.INT:VAT)           ! Field INT:VAT is a hot field or requires assignment from browse
  BRW2.AddField(MAN:MID,BRW2.Q.MAN:MID)           ! Field MAN:MID is a hot field or requires assignment from browse
  BRW2.AddField(INT:CreatedDate,BRW2.Q.INT:CreatedDate) ! Field INT:CreatedDate is a hot field or requires assignment from browse
  BRW2.AddField(JOU:Journey,BRW2.Q.JOU:Journey)   ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW2.AddField(BRA:BranchName,BRW2.Q.BRA:BranchName) ! Field BRA:BranchName is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:TPID,BRW2.Q.TRAPA:TPID)     ! Field TRAPA:TPID is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:TRPAID,BRW2.Q.TRAPA:TRPAID) ! Field TRAPA:TRPAID is a hot field or requires assignment from browse
  BRW2.AddField(INT:TIN,BRW2.Q.INT:TIN)           ! Field INT:TIN is a hot field or requires assignment from browse
  BRW2.AddField(JOU:JID,BRW2.Q.JOU:JID)           ! Field JOU:JID is a hot field or requires assignment from browse
  BRW2.AddField(BRA:BID,BRW2.Q.BRA:BID)           ! Field BRA:BID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_TransporterPayments',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 2
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_TransporterPayments',1,?Browse:2,2,BRW2::PopupTextExt,Queue:Browse:2,17,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Transporter.Close
  END
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_TransporterPayments',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
      IF SELF.Request = InsertRecord
         DISABLE(?Group_Browse)
      .
  


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Transporter
      Update_TransporterPaymentsAllocations(TRAP:TPID)
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_TagInvoices
          Inv_Tags.ClearAllTags()
          IS_Q.Clear_Q()
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      TRA:TransporterName = LO_G:TransporterName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LO_G:TransporterName = TRA:TransporterName
        TRAP:TID = TRA:TID
      END
      ThisWindow.Reset(1)
    OF ?LO_G:TransporterName
      IF LO_G:TransporterName OR ?LO_G:TransporterName{PROP:Req}
        TRA:TransporterName = LO_G:TransporterName
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LO_G:TransporterName = TRA:TransporterName
            TRAP:TID = TRA:TID
          ELSE
            CLEAR(TRAP:TID)
            SELECT(?LO_G:TransporterName)
            CYCLE
          END
        ELSE
          TRAP:TID = TRA:TID
        END
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
      Calendar10.SelectOnClose = True
      Calendar10.Ask('Select a Date',TRAP:DateMade)
      IF Calendar10.Response = RequestCompleted THEN
      TRAP:DateMade=Calendar10.SelectedDate
      DISPLAY(?TRAP:DateMade)
      END
      ThisWindow.Reset(True)
    OF ?TRAP:Amount
          LO_G:UnAllocated_Amt    = TRAP:Amount - LO_G:Allocated_Amount
          DISPLAY(?LO_G:UnAllocated_Amt)
    OF ?L_TG:Change_Allocation_Amount
          IF L_TG:Change_Allocation_Amount < 0.0
             L_TG:Change_Allocation_Amount     = 0.0
             DISPLAY(?L_TG:Change_Allocation_Amount)
          .
    OF ?Button_Update
      ThisWindow.Update()
          IF L_TG:TRPAID_Selected = 0
             MESSAGE('Please select an Invoice Payment to allocate.', 'Update Transporters Payments', ICON:Exclamation)
          ELSE
             TRAPA:TRPAID          = L_TG:TRPAID_Selected
             IF Access:TransporterPaymentsAllocations.TryFetch(TRAPA:PKey_TRPAID) = LEVEL:Benign
                ! Check that payment doesn't exceed the invoice total
                IF L_TG:Change_Allocation_Amount > Get_InvTransporter_Outstanding(TRAPA:TIN) + TRAPA:Amount
                   MESSAGE('The Payment Allocation of ' & FORMAT(L_TG:Change_Allocation_Amount,@n12.2) & ' would be greater than the outstanding amount owed.', 'Update Transporters Payments', ICON:Exclamation)
                   L_TG:Change_Allocation_Amount  = Get_InvTransporter_Outstanding(TRAPA:TIN) + TRAPA:Amount
                   DISPLAY(?L_TG:Change_Allocation_Amount)
                ELSE
                   ! Then we can allocate the new amount, otherwise we must deny this update
                   TRAPA:Amount      = L_TG:Change_Allocation_Amount
                   IF Access:TransporterPaymentsAllocations.TryUpdate() = LEVEL:Benign
                      Upd_InvoiceTransporter_Paid_Status(TRAPA:TIN)
      
                      ThisWindow.Reset(1)
                   ELSE
                      MESSAGE('Invoice Payment could not be updated.', 'Update Transporters Payments', ICON:Exclamation)
                .  .
             ELSE
                MESSAGE('Invoice Payment could not be fetched.', 'Update Transporters Payments', ICON:Exclamation)
          .  .
    OF ?Save
      ThisWindow.Update()
          ENABLE(?Group_Browse)
          !DISABLE(?Save)
    OF ?Button_TagInvoices
      ThisWindow.Update()
      Browse_Transporter_Invoices(TRAP:TID, Inv_Tags, IS_Q)
      ThisWindow.Reset
          DO Add_From_Tags
      
          BRW2.ResetFromFile()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
          IF TRAP:TID = 0
             MESSAGE('Please select a Transporter.', 'Transporter Needed', ICON:Exclamation)
             QuickWindow{PROP:AcceptAll}  = FALSE
             CYCLE
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:2
          BRW2.UpdateViewRecord()
      
          L_TG:TRPAID_Selected            = TRAPA:TRPAID
          L_TG:Change_Allocation_Amount   = TRAPA:Amount
          DISPLAY(?L_TG:Change_Allocation_Amount)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          TRA:TID = TRAP:TID
          IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
             LO_G:TransporterName = TRA:TransporterName
          .
          IF SELF.Request = InsertRecord
             DISABLE(?Group_Browse)
          ELSE
             DISABLE(?Group_Transporter)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.ResetFromView PROCEDURE

LO_G:Allocated_Amount:Sum REAL                             ! Sum variable for browse totals
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:TransporterPaymentsAllocations.SetQuickScan(1)
  SELF.Reset
  IF SELF.UseMRP
     IF SELF.View{PROP:IPRequestCount} = 0
          SELF.View{PROP:IPRequestCount} = 60
     END
  END
  LOOP
    IF SELF.UseMRP
       IF SELF.View{PROP:IPRequestCount} = 0
            SELF.View{PROP:IPRequestCount} = 60
       END
    END
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      SETCURSOR()
      RETURN
    END
    SELF.SetQueueRecord
    LO_G:Allocated_Amount:Sum += TRAPA:Amount
  END
  SELF.View{PROP:IPRequestCount} = 0
  LO_G:Allocated_Amount = LO_G:Allocated_Amount:Sum
      LO_G:UnAllocated_Amt    = TRAP:Amount - LO_G:Allocated_Amount
  PARENT.ResetFromView
  Relate:TransporterPaymentsAllocations.SetQuickScan(0)
  SETCURSOR()


BRW2.SetQueueRecord PROCEDURE

  CODE
      ! (INT:TIN, INT:CR_TIN, LO_G:Cost_Total, L_BG:Payments, L_BG:Credits, L_BG:Outstanding)
      !ikb
  
      LO_G:Cost_Total  = INT:Cost + INT:VAT
  
      ! (p:TIN, p:CR_TIN, p:Status, p_b:Cost_Total, pb:Payments, pb:Credits, pb:Outstanding)
      Get_Transporter_Totals(INT:TIN, INT:CR_TIN, INT:Status, LO_G:Cost_Total, L_BG:Payments, L_BG:Credits, L_BG:Outstanding)
  
  !    ! Invoices may be credited or have payments
  !    L_BG:Payments       = Get_TransportersPay_Alloc_Amt(INT:TIN)
  !
  !    ! p:Options
  !    !   0.  All
  !    !   1.  Credits
  !    !   2.  Debits
  !    L_BG:Credits        = -Get_InvTransporter_Credited(INT:TIN, 1)      ! Credited is negative
  !
  !    ! Credit notes may have associated invoices or not??
  !    IF INT:CR_TIN ~= 0                                                  ! Should only be for credit notes!
  !       ! No Payments|Partially Paid|Credit Note|Fully Paid
  !       IF INT:Status = 2
  !       ELSE
  !          ! We have an extra / additional / adjustment invoice
  !       .
  !
  !       ! The L_BG:Credited should be zero as Credit Notes cannot be credited.
  !
  !       L_BG:Credits    += -Get_InvTransporter_Credited(INT:CR_TIN, 1)   ! This is total credited (and will include this Credit Note)
  !       L_BG:Payments   += Get_TransportersPay_Alloc_Amt(INT:CR_TIN)
  !
  !       A_INT:TIN        = INT:CR_TIN
  !       IF Access:InvoiceTransporterAlias.TryFetch(A_INT:PKey_TIN) ~= LEVEL:Benign
  !          CLEAR(A_INT:Record)
  !       .
  !
  !       L_BG:Outstanding = (A_INT:Cost + A_INT:VAT) - (L_BG:Payments + L_BG:Credits)
  !    ELSE
  !       L_BG:Outstanding = LO_G:Cost_Total  - (L_BG:Payments + L_BG:Credits)
  !    .
  PARENT.SetQueueRecord
  


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_TransporterPaymentsAllocations PROCEDURE (p:TPID)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:TransporterInvoice_Group GROUP,PRE(L_TIG)              !
Total                DECIMAL(12,2)                         !Total of the Invoice
Owing                DECIMAL(12,2)                         !Amount still owing
Allocated_Paid       DECIMAL(12,2)                         !
Payment_Unallocated  DECIMAL(12,2)                         !
OrigAmt              DECIMAL(12,2)                         !
OrigPaid             DECIMAL(12,2)                         !
OrigRemaining        DECIMAL(12,2)                         !
TID                  ULONG                                 !Transporter ID
                     END                                   !
History::TRAPA:Record LIKE(TRAPA:RECORD),THREAD
QuickWindow          WINDOW('Form Transporter Payment Allocations'),AT(,,399,205),FONT('Tahoma',8,,FONT:regular), |
  DOUBLE,CENTER,GRAY,IMM,MDI,HLP('UpdateTransporterPaymentsAllocations'),SYSTEM
                       SHEET,AT(4,4,392,183),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           GROUP,AT(205,40,195,125),USE(?Group2)
                             PANEL,AT(205,40,186,125),USE(?Panel1),BEVEL(-1,-1)
                             PROMPT('MID:'),AT(210,45),USE(?INT:MID:Prompt),TRN
                             STRING(@n_10),AT(341,45),USE(INT:MID),RIGHT(1),TRN
                             PROMPT('Date:'),AT(210,58),USE(?INT:CreatedDate:Prompt),TRN
                             STRING(@d5b),AT(346,58),USE(INT:CreatedDate),RIGHT(1),TRN
                             PROMPT('Cost:'),AT(210,69),USE(?INT:Cost:Prompt),TRN
                             ENTRY(@n-14.2),AT(321,69,64,10),USE(INT:Cost),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP
                             PROMPT('VAT:'),AT(210,85),USE(?INT:VAT:Prompt),TRN
                             ENTRY(@n-14.2),AT(321,85,64,10),USE(INT:VAT),DECIMAL(12),COLOR(00E9E9E9h),MSG('VAT amount'), |
  READONLY,SKIP,TIP('VAT amount')
                             PROMPT('Total:'),AT(210,98),USE(?Total:Prompt),TRN
                             ENTRY(@n-17.2),AT(321,98,64,10),USE(L_TIG:Total),DECIMAL(12),COLOR(00E9E9E9h),MSG('Total of t' & |
  'he Invoice'),READONLY,SKIP,TIP('Total of the Invoice')
                             PROMPT('Paid this Invoice (allocated):'),AT(210,114),USE(?Allocated_Paid:Prompt),TRN
                             ENTRY(@n-17.2),AT(321,114,64,10),USE(L_TIG:Allocated_Paid),DECIMAL(12),COLOR(00E9E9E9h),READONLY, |
  SKIP
                             LINE,AT(210,128,174,0),USE(?Line1),COLOR(COLOR:Black)
                             PROMPT('Invoice Remaining:'),AT(210,133),USE(?Owing:Prompt),TRN
                             ENTRY(@n-17.2),AT(321,133,64,10),USE(L_TIG:Owing),DECIMAL(12),COLOR(00E9E9E9h),MSG('Amount still owing'), |
  READONLY,SKIP,TIP('Amount still owing')
                             LINE,AT(210,146,174,0),USE(?Line1:2),COLOR(COLOR:Black)
                             PROMPT('Payment Unallocated:'),AT(210,149),USE(?Payment_Unallocated:Prompt),TRN
                             ENTRY(@n-17.2),AT(321,149,64,10),USE(L_TIG:Payment_Unallocated),FONT(,,,FONT:bold,CHARSET:ANSI), |
  DECIMAL(12),COLOR(00E9E9E9h),READONLY,SKIP
                           END
                           GROUP,AT(69,4,82,26),USE(?Group1),HIDE
                             PROMPT('TRPAID:'),AT(69,4),USE(?TRAPA:TRPAID:Prompt),TRN
                             STRING(@n_10),AT(106,4,,10),USE(TRAPA:TRPAID),RIGHT(1),TRN
                             PROMPT('TPID:'),AT(69,20),USE(?TRAPA:TPID:Prompt),TRN
                             STRING(@n_10),AT(106,20,44,10),USE(TRAPA:TPID),RIGHT(1),TRN
                           END
                           PROMPT('Transporter Invoice:'),AT(9,38),USE(?TRAPA:TIN:Prompt:2),TRN
                           BUTTON('...'),AT(106,38,12,10),USE(?CallLookup)
                           ENTRY(@n_10),AT(125,38,64,10),USE(TRAPA:TIN),RIGHT(1),MSG('Transporter Invoice No.')
                           PROMPT('Transporter:'),AT(9,51),USE(?TRAPA:TIN:Prompt:3),TRN
                           ENTRY(@s35),AT(50,52,137,10),USE(A_TRA:TransporterName),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP,MSG('Transporters Name')
                           LINE,AT(9,67,189,0),USE(?Line3:2),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT(''),AT(9,162,180,20),USE(?String_Msg)
                           PROMPT('Allocation No.:'),AT(9,22),USE(?TRAPA:AllocationNo:Prompt),TRN
                           STRING(@n6),AT(145,22,44,10),USE(TRAPA:AllocationNo),RIGHT(1),TRN
                           LINE,AT(197,20,0,164),USE(?Line3),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Allocation Date:'),AT(9,90),USE(?TRAPA:AllocationDate:Prompt),TRN
                           BUTTON('...'),AT(106,90,12,10),USE(?Calendar)
                           SPIN(@d17),AT(125,90,64,10),USE(TRAPA:AllocationDate),RIGHT(1),MSG('Date allocation made'), |
  TIP('Date allocation made')
                           PROMPT('Amount:'),AT(9,76),USE(?TRAPA:Amount:Prompt),TRN
                           BUTTON('Allocate Max'),AT(65,76,,10),USE(?Button_Max)
                           ENTRY(@n-14.2),AT(125,76,64,10),USE(TRAPA:Amount),RIGHT(1),MSG('Amount of payment'),TIP('Amount of ' & |
  'payment allocated to this Invoice')
                           PROMPT('Comment:'),AT(9,106),USE(?TRAPA:Comment:Prompt:2),TRN
                           TEXT,AT(9,116,180,40),USE(TRAPA:Comment,,?TRAPA:Comment:2),VSCROLL,BOXED,MSG('Comment')
                         END
                       END
                       BUTTON('&OK'),AT(294,188,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(347,188,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,188,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar8            CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
New_Inv                     ROUTINE
    db.debugout('[Upd._TrnPayAlloc.]  - New_Inv - in')

    ThisWindow.Update

    L_TIG:Total                     = INT:Cost + INT:VAT

    ! Remaining will exclude any original amt on this allocation

    db.debugout('[Upd._TrnPayAlloc.]  - New_Inv - Get_TransportersPay_Alloc_Amt')


    L_TIG:OrigRemaining             = A_TRAP:Amount - Get_TransportersPay_Alloc_Amt( A_TRAP:TPID, 1 )


    db.debugout('[Upd._TrnPayAlloc.]  - New_Inv - Get_TransportersPay_Alloc_Amt -  done')


    ! If payment specified is greater than the amt we have left to allocate then reduce to this
    IF TRAPA:Amount > (L_TIG:OrigRemaining + L_TIG:OrigAmt)
       TRAPA:Amount                 = L_TIG:OrigRemaining + L_TIG:OrigAmt
    .

    L_TIG:Payment_Unallocated       = L_TIG:OrigRemaining + L_TIG:OrigAmt - TRAPA:Amount

    IF TRAPA:TIN ~= 0
    db.debugout('[Upd._TrnPayAlloc.]  - New_Inv - Get_TransportersPay_Alloc_Amt 2')

       L_TIG:OrigPaid               = Get_TransportersPay_Alloc_Amt( TRAPA:TIN, 0 )

    db.debugout('[Upd._TrnPayAlloc.]  - New_Inv - Get_TransportersPay_Alloc_Amt 2 - done')

       ! If the amt paid now is greater than the amt to pay then set to this amount
       IF TRAPA:Amount > L_TIG:Total - (L_TIG:OrigPaid - L_TIG:OrigAmt)
          TRAPA:Amount              = L_TIG:Total - (L_TIG:OrigPaid - L_TIG:OrigAmt)
       .

       L_TIG:Allocated_Paid         = L_TIG:OrigPaid - L_TIG:OrigAmt + TRAPA:Amount

       L_TIG:Owing                  = L_TIG:Total - L_TIG:Allocated_Paid

       IF TRAPA:Amount = 0.0
          ! Allocate the maximium available or max owing if smaller than available
          IF L_TIG:Owing > L_TIG:Payment_Unallocated
             TRAPA:Amount           = L_TIG:Payment_Unallocated
          ELSE
             TRAPA:Amount           = L_TIG:Owing
       .  .

       L_TIG:Payment_Unallocated    = L_TIG:OrigRemaining + L_TIG:OrigAmt - TRAPA:Amount
       L_TIG:Allocated_Paid         = L_TIG:OrigPaid - L_TIG:OrigAmt + TRAPA:Amount

       L_TIG:Owing                  = L_TIG:Total - L_TIG:Allocated_Paid
    .

    ThisWindow.Reset(1)

    A_TRA:TID                       = INT:TID
    IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) ~= LEVEL:Benign
       CLEAR(A_TRA:Record)
    .

    
    IF L_TIG:Payment_Unallocated <= 0
       ?String_Msg{PROP:Text}           = 'Zero available to allocate.'
       ?String_Msg{PROP:FontColor}      = -1
    ELSE
       IF TRAPA:TIN ~= 0 AND INT:TID ~= A_TRAP:TID
          TRA:TID                       = A_TRAP:TID
          IF Access:Transporter.TryFetch(TRA:PKey_TID) ~= LEVEL:Benign
             CLEAR(TRA:Record)
          .
          ?String_Msg{PROP:Text}        = 'Transporter on Invoice does not match Transporter on Payment (' & CLIP(TRA:TransporterName) & ')'
          ?String_Msg{PROP:FontColor}   = COLOR:Red
       ELSE
          ?String_Msg{PROP:Text}        = ''
          ?String_Msg{PROP:FontColor}   = -1
    .  .

    DISPLAY

    db.debugout('[Upd._TrnPayAlloc.]  - New_Inv - out')

    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Transporter Payment Alloc. Record'
  OF InsertRecord
    ActionMessage = 'Transporter Payment Alloc. Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Trans. Payment Alloc. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_TransporterPaymentsAllocations')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:TransporterPaymentsAllocations)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(TRAPA:Record,History::TRAPA:Record)
  SELF.AddHistoryField(?TRAPA:TRPAID,1)
  SELF.AddHistoryField(?TRAPA:TPID,2)
  SELF.AddHistoryField(?TRAPA:TIN,11)
  SELF.AddHistoryField(?TRAPA:AllocationNo,3)
  SELF.AddHistoryField(?TRAPA:AllocationDate,6)
  SELF.AddHistoryField(?TRAPA:Amount,8)
  SELF.AddHistoryField(?TRAPA:Comment:2,9)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                                  ! File Transporter used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterAlias.Open                             ! File TransporterAlias used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterPaymentsAlias.Open                     ! File TransporterPaymentsAlias used by this procedure, so make sure it's RelationManager is open
  Access:_InvoiceTransporter.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TransporterPaymentsAllocations
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?INT:Cost{PROP:ReadOnly} = True
    ?INT:VAT{PROP:ReadOnly} = True
    ?L_TIG:Total{PROP:ReadOnly} = True
    ?L_TIG:Allocated_Paid{PROP:ReadOnly} = True
    ?L_TIG:Owing{PROP:ReadOnly} = True
    ?L_TIG:Payment_Unallocated{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?TRAPA:TIN{PROP:ReadOnly} = True
    DISABLE(?Calendar)
    DISABLE(?Button_Max)
    ?TRAPA:Amount{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_TransporterPaymentsAllocations',QuickWindow) ! Restore window settings from non-volatile store
      IF SELF.Request = InsertRecord
         TRAPA:TPID       = p:TPID
      ELSE
         p:TPID           = TRAPA:TPID
      .
  
  
      IF A_TRAP:TPID ~= TRAPA:TPID
         A_TRAP:TPID        = TRAPA:TPID
         IF Access:TransporterPaymentsAlias.TryFetch(A_TRAP:PKey_TPID) ~= LEVEL:Benign
            CLEAR(A_TRAP:Record)
            MESSAGE('Could not find Transporter Payment record!','Update Transporter Payment Allocations',ICON:Hand)
      .  .
  
      L_TIG:TID           = A_TRAP:TID
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Transporter.Close
    Relate:TransporterAlias.Close
    Relate:TransporterPaymentsAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_TransporterPaymentsAllocations',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Transporter_Invoices(L_TIG:TID)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      INT:TIN = TRAPA:TIN
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        TRAPA:TIN = INT:TIN
      END
      ThisWindow.Reset(1)
          DO New_Inv
    OF ?TRAPA:TIN
      IF FALSE
      
      IF TRAPA:TIN OR ?TRAPA:TIN{PROP:Req}
        INT:TIN = TRAPA:TIN
        IF Access:_InvoiceTransporter.TryFetch(INT:PKey_TIN)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            TRAPA:TIN = INT:TIN
          ELSE
            SELECT(?TRAPA:TIN)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
        .
          DO New_Inv
    OF ?Calendar
      ThisWindow.Update()
      Calendar8.SelectOnClose = True
      Calendar8.Ask('Select a Date',TRAPA:AllocationDate)
      IF Calendar8.Response = RequestCompleted THEN
      TRAPA:AllocationDate=Calendar8.SelectedDate
      DISPLAY(?TRAPA:AllocationDate)
      END
      ThisWindow.Reset(True)
    OF ?Button_Max
      ThisWindow.Update()
          IF L_TIG:Owing > (L_TIG:Payment_Unallocated + TRAPA:Amount)
             TRAPA:Amount      += L_TIG:Payment_Unallocated
          ELSE
             TRAPA:Amount      += L_TIG:Owing
          .
      
          DISPLAY
          DO New_Inv
    OF ?TRAPA:Amount
          DO New_Inv
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
          INT:TIN     = TRAPA:TIN
          IF Access:_InvoiceTransporter.TryFetch(INT:PKey_TIN) = LEVEL:Benign
             IF INT:TID ~= A_TRAP:TID
                CASE MESSAGE('The Transporter specified on the selected Invoice (TIN: ' & TRAPA:TIN & ') does not match the transporter specified on the Payment.||Is this correct?', 'Transporter Check', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
                OF BUTTON:No
                   QuickWindow{PROP:AcceptAll}    = FALSE
                   SELECT(?TRAPA:TIN)
                   CYCLE
          .  .  .
          Upd_InvoiceTransporter_Paid_Status(TRAPA:TIN)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          INT:TIN                     = TRAPA:TIN
          IF Access:_InvoiceTransporter.TryFetch(INT:PKey_TIN) = LEVEL:Benign
             L_TIG:OrigPaid           = Get_TransportersPay_Alloc_Amt( TRAPA:TIN, 0 )
          ELSE
             CLEAR(INT:Record)
          .                                       
      
          L_TIG:OrigAmt               = TRAPA:Amount
      
          L_TIG:Payment_Unallocated   = A_TRAP:Amount - Get_TransportersPay_Alloc_Amt( A_TRAP:TPID, 1 )
      
          db.debugout('[Upd._TrnPayAlloc.]  A_TRAP:TPID: ' & A_TRAP:TPID & ',  Total Amt: ' & A_TRAP:Amount)
      
      
          DO New_Inv
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Transporter_Invoices PROCEDURE (p:TID, <shpTagClass p_Client_Tags>, <Q_Class p_Q>)

CurrentTab           STRING(80)                            !
LOC:DateGroup        GROUP,PRE(L_DG)                       !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
LOC:Locator          STRING(35)                            !
LOC:Reset            LONG                                  !
LOC:Tag_Group        GROUP,PRE(LOC)                        !
Tagging              BYTE                                  !
Selected_Clients     BYTE                                  !
Tagged_Value         DECIMAL(10,2)                         !
Tagging_Values_Present BYTE                                !is the tagging values Q also passed
Payment              DECIMAL(11,2)                         !
                     END                                   !
LOC:Browse_Group     GROUP,PRE()                           !
L_BG:Status          STRING(15)                            !
L_BG:Total           DECIMAL(12,2)                         !
L_BG:Outstanding     DECIMAL(11,2)                         !
L_BG:Payments        DECIMAL(11,2)                         !
L_BG:Credits         DECIMAL(10,2)                         !
                     END                                   !
LOC:Filter_Group     GROUP,PRE(L_FG)                       !
Status               BYTE                                  !used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid
                     END                                   !
LOC:Style_Group      GROUP,PRE(L_ST)                       !
Tagged               BYTE                                  !
                     END                                   !
LOC:Selected_TID     ULONG                                 !Transporter ID
BRW1::View:Browse    VIEW(_InvoiceTransporter)
                       PROJECT(INT:TIN)
                       PROJECT(INT:CIN)
                       PROJECT(INT:InvoiceDate)
                       PROJECT(INT:MID)
                       PROJECT(INT:Cost)
                       PROJECT(INT:VAT)
                       PROJECT(INT:Manifest)
                       PROJECT(INT:Broking)
                       PROJECT(INT:ExtraInv)
                       PROJECT(INT:VATRate)
                       PROJECT(INT:CreatedDate)
                       PROJECT(INT:DINo)
                       PROJECT(INT:Leg)
                       PROJECT(INT:CR_TIN)
                       PROJECT(INT:Rate)
                       PROJECT(INT:TID)
                       PROJECT(INT:BID)
                       PROJECT(INT:Status)
                       PROJECT(INT:DID)
                       PROJECT(INT:UID)
                       JOIN(MAN:PKey_MID,INT:MID)
                         PROJECT(MAN:MID)
                         PROJECT(MAN:VCID)
                         JOIN(VCO:PKey_VCID,MAN:VCID)
                           PROJECT(VCO:CompositionName)
                           PROJECT(VCO:VCID)
                         END
                       END
                       JOIN(BRA:PKey_BID,INT:BID)
                         PROJECT(BRA:BranchName)
                         PROJECT(BRA:BID)
                       END
                       JOIN(TRA:PKey_TID,INT:TID)
                         PROJECT(TRA:TransporterName)
                         PROJECT(TRA:TID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
INT:TIN                LIKE(INT:TIN)                  !List box control field - type derived from field
INT:TIN_Style          LONG                           !Field style
INT:CIN                LIKE(INT:CIN)                  !List box control field - type derived from field
INT:CIN_Style          LONG                           !Field style
INT:InvoiceDate        LIKE(INT:InvoiceDate)          !List box control field - type derived from field
INT:InvoiceDate_Style  LONG                           !Field style
INT:MID                LIKE(INT:MID)                  !List box control field - type derived from field
VCO:CompositionName    LIKE(VCO:CompositionName)      !List box control field - type derived from field
L_BG:Status            LIKE(L_BG:Status)              !List box control field - type derived from local data
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
L_BG:Outstanding       LIKE(L_BG:Outstanding)         !List box control field - type derived from local data
L_BG:Total             LIKE(L_BG:Total)               !List box control field - type derived from local data
L_BG:Payments          LIKE(L_BG:Payments)            !List box control field - type derived from local data
L_BG:Credits           LIKE(L_BG:Credits)             !List box control field - type derived from local data
INT:Cost               LIKE(INT:Cost)                 !List box control field - type derived from field
INT:VAT                LIKE(INT:VAT)                  !List box control field - type derived from field
INT:Manifest           LIKE(INT:Manifest)             !List box control field - type derived from field
INT:Manifest_Icon      LONG                           !Entry's icon ID
INT:Broking            LIKE(INT:Broking)              !List box control field - type derived from field
INT:Broking_Icon       LONG                           !Entry's icon ID
INT:ExtraInv           LIKE(INT:ExtraInv)             !List box control field - type derived from field
INT:ExtraInv_Icon      LONG                           !Entry's icon ID
INT:VATRate            LIKE(INT:VATRate)              !List box control field - type derived from field
INT:CreatedDate        LIKE(INT:CreatedDate)          !List box control field - type derived from field
INT:DINo               LIKE(INT:DINo)                 !List box control field - type derived from field
INT:Leg                LIKE(INT:Leg)                  !List box control field - type derived from field
INT:CR_TIN             LIKE(INT:CR_TIN)               !List box control field - type derived from field
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
INT:Rate               LIKE(INT:Rate)                 !List box control field - type derived from field
INT:TID                LIKE(INT:TID)                  !List box control field - type derived from field
INT:BID                LIKE(INT:BID)                  !List box control field - type derived from field
INT:Status             LIKE(INT:Status)               !Browse hot field - type derived from field
INT:DID                LIKE(INT:DID)                  !Browse hot field - type derived from field
INT:UID                LIKE(INT:UID)                  !Browse key field - type derived from field
MAN:MID                LIKE(MAN:MID)                  !Related join file key field - type derived from field
VCO:VCID               LIKE(VCO:VCID)                 !Related join file key field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Related join file key field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Invoice Transporter File'),AT(,,453,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_Transporter_Invoices'),SYSTEM
                       GROUP,AT(112,184,118,10),USE(?Group_ShowStatus)
                         PROMPT('Show Status:'),AT(112,184),USE(?L_FG:Status:Prompt)
                         LIST,AT(158,184,72,10),USE(L_FG:Status),DROP(15),FROM('All|#255|No Payments|#0|Partiall' & |
  'y Paid|#1|Credit Note|#2|Fully Paid|#3'),TIP('No Payments, Partially Paid, Credit No' & |
  'te, Fully Paid')
                       END
                       LIST,AT(8,34,436,120),USE(?Browse:1),HVSCROLL,ALRT(CtrlU),ALRT(CtrlA),FORMAT('30R(2)|FMY' & |
  '~TIN~C(0)@n_10@42L(1)|FMY~Creditor Inv.~C(0)@s30@[38R(2)|MY~Date~C(0)@d5b@](37)|M~In' & |
  'voice~[30R(2)|M~MID~C(0)@n_10b@44L(2)|M~Composition~C(0)@s35@]|M~Manifest~40L(2)|M~S' & |
  'tatus~C(0)@s15@70L(2)|M~Transporter~C(0)@s35@[44R(1)|M~Outstanding~C(0)@n-14.2@44R(1' & |
  ')|M~Total~C(0)@n-17.2@44R(1)|M~Payments~C(0)@n-14.2@44R(1)|M~Credits~C(0)@n-14.2@44R' & |
  '(1)|M~Cost~C(0)@n-14.2@42R(1)|M~VAT~C(0)@n-14.2@]|M~Charges~12R(2)|MI~Manifest Invoi' & |
  'ce~L(1)@p p@12R(2)|MI~Broking~L(1)@p p@12R(2)|MI~Extra Inv.~L(1)@p p@38R(1)|M~VAT Ra' & |
  'te~C(0)@n-7.2@38R(2)|M~Created Date~L(1)@d5b@[40R(2)|M~DI No.~L(1)@n_10b@20R(2)|M~Le' & |
  'g~L(1)@n5b@]|M~Delivery~[40R(1)|M~TIN~C(0)@n_10b@]|M~Credit / Adj.~50L(2)|M~Branch~C' & |
  '(0)@s35@52R(1)|M~Rate~C(0)@n-13.4@30R(2)|M~TID~C(0)@n_10b@40R(2)|M~BID~C(0)@n_10b@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the Transporter Invoice file')
                       BUTTON('&Select'),AT(186,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(240,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(292,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(344,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(396,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       GROUP,AT(7,20,134,10),USE(?Group_Locator)
                         PROMPT('Locator:'),AT(7,20),USE(?LOC:Locator:Prompt:2),TRN
                         ENTRY(@s35),AT(39,20,102,10),USE(LOC:Locator)
                       END
                       SHEET,AT(4,4,445,172),USE(?CurrentTab)
                         TAB('&1) By Transporter Invoice No.'),USE(?Tab:2)
                         END
                         TAB('&2) By Transporter'),USE(?Tab:3)
                           STRING(''),AT(315,22,130,10),USE(?String_Tagged),RIGHT(1)
                           BUTTON('Select Transporter'),AT(9,158,,14),USE(?SelectTransporter),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                           BUTTON('Select Tagged'),AT(103,158,,14),USE(?Select_Tagged),LEFT,ICON('CHECK1.ICO'),FLAT
                         END
                         TAB('&3) By Branch'),USE(?Tab:4)
                           BUTTON('Select Branch'),AT(9,158,,14),USE(?SelectBranches),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&4) By Manifest'),USE(?Tab:5)
                           BUTTON('Select Manifest'),AT(9,158,,14),USE(?SelectManifest),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&5) By User'),USE(?Tab:6)
                           BUTTON('Select User'),AT(9,158,,14),USE(?SelectUsers),LEFT,ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                         END
                         TAB('&6 By DI No.'),USE(?Tab6)
                           BUTTON('Select Delivery'),AT(9,158,,14),USE(?Button_Delivery),LEFT,ICON('WAPARENT.ICO'),FLAT
                           BUTTON('Clear Delivery'),AT(93,158,,14),USE(?Button_Clear_Delivery),LEFT,ICON('BCOLOR.ICO'), |
  FLAT,TIP('Clear selected delivery')
                         END
                         TAB('&7) By Date'),USE(?Tab7)
                         END
                       END
                       BUTTON('&Close'),AT(400,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('Limit to Date Range'),AT(4,180,95,14),USE(?Button_DateRange)
                       BUTTON('Create Credit / Invoice'),AT(292,180,,14),USE(?Button_Credit_Debit),LEFT,ICON('CLEAR.ICO'), |
  FLAT
                       BUTTON('&Help'),AT(398,2,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
BRW1::SortHeader  CLASS(SortHeaderClassType) !Declare SortHeader Class
QueueResorted          PROCEDURE(STRING pString),VIRTUAL
                  END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

View_Tag     VIEW(_InvoiceTransporter)
    PROJECT(INT:TIN) 
    .

Tag_View     ViewManager
Pay_Window WINDOW('Payment Value'),AT(,,162,69),FONT('MS Sans Serif',8,,FONT:regular),CENTER,GRAY,DOUBLE, |
         MDI
       PROMPT('Payment:'),AT(22,14),USE(?Prompt1)
       ENTRY(@n-20.2),AT(58,14,81,10),USE(LOC:Payment),RIGHT(1)
       PROMPT(''),AT(22,28,116,10),USE(?Prompt_Info)
       BUTTON('OK'),AT(33,50,45,14),USE(?Button_OK)
       BUTTON('Cancel'),AT(83,50,45,14),USE(?Button_Cancel)
     END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
  !------------------------------------
  !Style for ?Browse:1
  !------------------------------------
!---------------------------------------------------------------------------
Un_Tag_All                      ROUTINE
    IF LOC:Tagging > 0
       p_Client_Tags.ClearAllTags()
       BRW1.ResetFromBuffer()
       ?String_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()
    .
    EXIT
Tag_All                             ROUTINE
    IF LOC:Tagging > 0
       ! Tag All in the current sort order...
       !PUSHBIND()
       !BIND('')

       Tag_View.Init(View_Tag, Relate:_InvoiceTransporter)

       CASE CHOICE(?CurrentTab)
       OF 2                                         ! Branch
!          Tag_View.AddSortOrder(CLI:FKey_BID)
!          Tag_View.AppendOrder('CLI:CID')
!          Tag_View.AddRange(CLI:BID, BRA:BID)
       ELSE                                         ! All others
          Tag_View.AddSortOrder(INT:PKey_TIN)
       .

       !Tag_View.SetFilter('')

       Tag_View.Reset()
       LOOP
          IF Tag_View.Next() ~= LEVEL:Benign
             BREAK
          .

          p_Client_Tags.MakeTag(INT:TIN)
       .

       Tag_View.Kill()

       !POPBIND()
       ?String_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()

       BRW1.ResetFromBuffer()
    .
    EXIT
Tag_Toggle_this_rec       ROUTINE
    DATA
R:Choice            LONG
R:Col               LONG

    CODE
    IF LOC:Tagging > 0
       R:Col            = 1
       R:Choice         = CHOICE(?Browse:1)

       GET(Queue:Browse:1, R:Choice)
       IF ~ERRORCODE()
          IF p_Client_Tags.IsTagged(INT:TIN) = TRUE
             p_Client_Tags.ClearTag(INT:TIN)

             IF LOC:Tagging_Values_Present = TRUE
                Row_#  = p_Q.Find_Row(INT:TIN, R:Col)
                IF Row_# > 0
                   Elem_"  = ''
                   p_Q.Get_Row_Item(Row_#, 2, Elem_")
                   Queue:Browse:1.L_BG:Outstanding     = Elem_"
             .  .

             LOC:Tagged_Value  -= Queue:Browse:1.L_BG:Outstanding
          ELSE
             IF LOC:Tagging_Values_Present = FALSE
                p_Client_Tags.MakeTag(INT:TIN)
             ELSE
                DO Payment_Window
                IF Queue:Browse:1.L_BG:Outstanding > 0.0
                ! - taking out check - allow negatives to be tagged, they want to allocate credit notes too... 24 April 06
                ! might do this, asking them first...
                   p_Client_Tags.MakeTag(INT:TIN)

                   Row_#        = p_Q.Update_Row_Item(INT:TIN, 1)       ! ID
                   IF Row_# > 0
                      Row_#     = p_Q.Set_Row_Item(2, Queue:Browse:1.L_BG:Outstanding, Row_#)   ! Data
             .  .  .

             LOC:Tagged_Value  += Queue:Browse:1.L_BG:Outstanding
          .

      !    BRW1.ResetFromBuffer()
      !    BRW1.Reset(1)
          ?String_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged() & ' @ ' & FORMAT(LOC:Tagged_Value,@n-10.2)

          DO Style_Entry
          PUT(Queue:Browse:1)

    .  .
    EXIT



Payment_Window          ROUTINE
    DATA
R:Cancel    BYTE

    CODE
    OPEN(Pay_Window)
    ?Prompt_Info{PROP:Text} = 'Outstanding: ' & FORMAT(Queue:Browse:1.L_BG:Outstanding,@n-11.2)
    LOC:Payment = Queue:Browse:1.L_BG:Outstanding

    DISPLAY
    ACCEPT
       CASE FIELD()
       OF ?Button_OK
          CASE EVENT()
          OF EVENT:Accepted
             IF LOC:Payment > Queue:Browse:1.L_BG:Outstanding
                MESSAGE('The payment cannot be greater than the total Outstanding on this item.', 'Payment Amount', ICON:Exclamation)
                SELECT(?LOC:Payment)
                CYCLE
             ELSE
                BREAK
          .  .
       OF ?Button_Cancel
          CASE EVENT()
          OF EVENT:Accepted
             R:Cancel   = TRUE
             BREAK
          .
       OF ?LOC:Payment
          CASE EVENT()
          OF EVENT:Accepted
             IF LOC:Payment > Queue:Browse:1.L_BG:Outstanding
                MESSAGE('The payment cannot be greater than the total Outstanding on this item.', '', ICON:Exclamation)
                SELECT(?LOC:Payment)
                CYCLE
          .  .
    .  .
    CLOSE(Pay_Window)

    IF R:Cancel = TRUE
       Queue:Browse:1.L_BG:Outstanding  = 0.0
    ELSE
       Queue:Browse:1.L_BG:Outstanding  = LOC:Payment
    .
    EXIT
Style_Setup                     ROUTINE
!    IF ~OMITTED(1)
       ! Style:Normal
   !    ?list1{PROPSTYLE:FontStyle, 1}     = FONT:Regular
   !    ?list1{PROPSTYLE:TextColor, 1}     = COLOR:NONE
   !    ?list1{PROPSTYLE:BackColor, 1}     = COLOR:NONE
   !    ?list1{PROPSTYLE:TextSelected, 1}  = COLOR:NONE
   !    ?list1{PROPSTYLE:BackSelected, 1}  = COLOR:NONE

       ! Style:Header
       !?list1{PROPSTYLE:FontStyle, 1}     = FONT:Bold
       ?Browse:1{PROPSTYLE:TextColor, 1}     = COLOR:HIGHLIGHTTEXT
       ?Browse:1{PROPSTYLE:BackColor, 1}     = COLOR:HIGHLIGHT
       ?Browse:1{PROPSTYLE:TextSelected, 1}  = COLOR:White
       ?Browse:1{PROPSTYLE:BackSelected, 1}  = COLOR:Blue

       ?Browse:1{PROPSTYLE:TextColor, 2}     = 0B7H
       ?Browse:1{PROPSTYLE:BackColor, 2}     = -1
       ?Browse:1{PROPSTYLE:TextSelected, 2}  = 0D5D5FFH
       ?Browse:1{PROPSTYLE:BackSelected, 2}  = -1

       ?Browse:1{PROPSTYLE:TextColor, 3}     = COLOR:Blue
       ?Browse:1{PROPSTYLE:BackColor, 3}     = -1
       ?Browse:1{PROPSTYLE:TextSelected, 3}  = 0FFD0D0H
       ?Browse:1{PROPSTYLE:BackSelected, 3}  = -1

!       SELF.Q.LOC:Day_NormalFG = -2147483634
!       SELF.Q.LOC:Day_NormalBG = -2147483635
!       SELF.Q.LOC:Day_SelectedFG = -1
!       SELF.Q.LOC:Day_SelectedBG = -1
    EXIT
Style_Entry                       ROUTINE
    Queue:Browse:1.INT:TIN_Style                                = 0
    Queue:Browse:1.INT:InvoiceDate_Style                        = 0

    IF LOC:Tagging > 0
       L_ST:Tagged      = p_Client_Tags.IsTagged(Queue:Browse:1.INT:TIN)

       IF L_ST:Tagged = TRUE
          Queue:Browse:1.INT:TIN_Style                          = 1
          Queue:Browse:1.INT:InvoiceDate_Style                  = 1
       ELSE
          Queue:Browse:1.INT:TIN_Style                          = 0
          Queue:Browse:1.INT:InvoiceDate_Style                  = 0
    .  .

    ! Style for Extra Invoice or has Extra Invoice, let this be overridden by Tag style, when tagged
    IF LOC:Tagging > 0 AND L_ST:Tagged = TRUE
    ELSE
       IF INT:CR_TIN ~= 0 AND INT:Status ~= 2
          Queue:Browse:1.INT:TIN_Style                          = 2
          Queue:Browse:1.INT:InvoiceDate_Style                  = 2
       ELSIF Get_InvTransporter_Credited(INT:TIN, 2,,1) > 0.0       ! If have Adj.
          Queue:Browse:1.INT:TIN_Style                          = 3
          Queue:Browse:1.INT:InvoiceDate_Style                  = 3
    .  .
    EXIT



Check_Omitted                ROUTINE
    IF OMITTED(2) = FALSE
       LOC:Tagging                  = TRUE              ! Switch on tagging
    .
    IF OMITTED(3) = FALSE
       LOC:Tagging_Values_Present   = TRUE              ! Values class also passed - for amts
    .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Transporter_Invoices')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?L_FG:Status:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_FG:Status',L_FG:Status)                 ! Added by: BrowseBox(ABC)
  BIND('L_DG:From_Date',L_DG:From_Date)           ! Added by: BrowseBox(ABC)
  BIND('L_DG:To_Date',L_DG:To_Date)               ! Added by: BrowseBox(ABC)
  BIND('L_BG:Status',L_BG:Status)                 ! Added by: BrowseBox(ABC)
  BIND('L_BG:Outstanding',L_BG:Outstanding)       ! Added by: BrowseBox(ABC)
  BIND('L_BG:Total',L_BG:Total)                   ! Added by: BrowseBox(ABC)
  BIND('L_BG:Payments',L_BG:Payments)             ! Added by: BrowseBox(ABC)
  BIND('L_BG:Credits',L_BG:Credits)               ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                            ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:InvoiceTransporterAlias.Open             ! File InvoiceTransporterAlias used by this procedure, so make sure it's RelationManager is open
  Access:Manifest.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Users.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_InvoiceTransporter,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
      DO Style_Setup
      DO Check_Omitted
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,INT:FKey_TID)                ! Add the sort order for INT:FKey_TID for sort order 1
  BRW1.AddRange(INT:TID,LOC:Selected_TID)         ! Add single value range limit for sort order 1
  BRW1.AppendOrder('+INT:TIN')                    ! Append an additional sort order
  BRW1.SetFilter('((L_FG:Status = 255 OR L_FG:Status = INT:Status) AND (0 = L_DG:From_Date OR INT:InvoiceDate >= L_DG:From_Date) AND (0 = L_DG:To_Date OR INT:InvoiceDate << L_DG:To_Date))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Reset)                   ! Apply the reset field
  BRW1.AddSortOrder(,INT:FKey_BID)                ! Add the sort order for INT:FKey_BID for sort order 2
  BRW1.AddRange(INT:BID,Relate:_InvoiceTransporter,Relate:Branches) ! Add file relationship range limit for sort order 2
  BRW1.AppendOrder('+INT:TIN')                    ! Append an additional sort order
  BRW1.SetFilter('((L_FG:Status = 255 OR L_FG:Status = INT:Status) AND (0 = L_DG:From_Date OR INT:InvoiceDate >= L_DG:From_Date) AND (0 = L_DG:To_Date OR INT:InvoiceDate << L_DG:To_Date))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Reset)                   ! Apply the reset field
  BRW1.AddSortOrder(,INT:Fkey_MID)                ! Add the sort order for INT:Fkey_MID for sort order 3
  BRW1.AddRange(INT:MID,Relate:_InvoiceTransporter,Relate:Manifest) ! Add file relationship range limit for sort order 3
  BRW1.AppendOrder('+INT:TIN')                    ! Append an additional sort order
  BRW1.SetFilter('((L_FG:Status = 255 OR L_FG:Status = INT:Status) AND (0 = L_DG:From_Date OR INT:InvoiceDate >= L_DG:From_Date) AND (0 = L_DG:To_Date OR INT:InvoiceDate << L_DG:To_Date))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Reset)                   ! Apply the reset field
  BRW1.AddSortOrder(,INT:FKey_UID)                ! Add the sort order for INT:FKey_UID for sort order 4
  BRW1.AddRange(INT:UID,Relate:_InvoiceTransporter,Relate:Users) ! Add file relationship range limit for sort order 4
  BRW1.AppendOrder('+INT:TIN')                    ! Append an additional sort order
  BRW1.SetFilter('((L_FG:Status = 255 OR L_FG:Status = INT:Status) AND (0 = L_DG:From_Date OR INT:InvoiceDate >= L_DG:From_Date) AND (0 = L_DG:To_Date OR INT:InvoiceDate << L_DG:To_Date))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Reset)                   ! Apply the reset field
  BRW1.AddSortOrder(,)                            ! Add the sort order for  for sort order 5
  BRW1.AppendOrder('+INT:DINo,+INT:Leg,+INT:TIN') ! Append an additional sort order
  BRW1.SetFilter('((DEL:DID = 0 OR DEL:DID = INT:DID)  AND (L_FG:Status = 255 OR L_FG:Status = INT:Status) AND (0 = L_DG:From_Date OR INT:InvoiceDate >= L_DG:From_Date) AND (0 = L_DG:To_Date OR INT:InvoiceDate << L_DG:To_Date))') ! Apply filter expression to browse
  BRW1.AddResetField(DEL:DID)                     ! Apply the reset field
  BRW1.AddSortOrder(,)                            ! Add the sort order for  for sort order 6
  BRW1.AppendOrder('+INT:InvoiceDate,+INT:TIN')   ! Append an additional sort order
  BRW1.AddSortOrder(,INT:PKey_TIN)                ! Add the sort order for INT:PKey_TIN for sort order 7
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 7
  BRW1::Sort0:Locator.Init(?LOC:Locator,INT:TIN,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: INT:PKey_TIN , INT:TIN
  BRW1.SetFilter('((L_FG:Status = 255 OR INT:Status = L_FG:Status) AND (0 = L_DG:From_Date OR INT:InvoiceDate >= L_DG:From_Date) AND (0 = L_DG:To_Date OR INT:InvoiceDate << L_DG:To_Date))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Reset)                   ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(INT:TIN,BRW1.Q.INT:TIN)           ! Field INT:TIN is a hot field or requires assignment from browse
  BRW1.AddField(INT:CIN,BRW1.Q.INT:CIN)           ! Field INT:CIN is a hot field or requires assignment from browse
  BRW1.AddField(INT:InvoiceDate,BRW1.Q.INT:InvoiceDate) ! Field INT:InvoiceDate is a hot field or requires assignment from browse
  BRW1.AddField(INT:MID,BRW1.Q.INT:MID)           ! Field INT:MID is a hot field or requires assignment from browse
  BRW1.AddField(VCO:CompositionName,BRW1.Q.VCO:CompositionName) ! Field VCO:CompositionName is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Status,BRW1.Q.L_BG:Status)   ! Field L_BG:Status is a hot field or requires assignment from browse
  BRW1.AddField(TRA:TransporterName,BRW1.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Outstanding,BRW1.Q.L_BG:Outstanding) ! Field L_BG:Outstanding is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Total,BRW1.Q.L_BG:Total)     ! Field L_BG:Total is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Payments,BRW1.Q.L_BG:Payments) ! Field L_BG:Payments is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Credits,BRW1.Q.L_BG:Credits) ! Field L_BG:Credits is a hot field or requires assignment from browse
  BRW1.AddField(INT:Cost,BRW1.Q.INT:Cost)         ! Field INT:Cost is a hot field or requires assignment from browse
  BRW1.AddField(INT:VAT,BRW1.Q.INT:VAT)           ! Field INT:VAT is a hot field or requires assignment from browse
  BRW1.AddField(INT:Manifest,BRW1.Q.INT:Manifest) ! Field INT:Manifest is a hot field or requires assignment from browse
  BRW1.AddField(INT:Broking,BRW1.Q.INT:Broking)   ! Field INT:Broking is a hot field or requires assignment from browse
  BRW1.AddField(INT:ExtraInv,BRW1.Q.INT:ExtraInv) ! Field INT:ExtraInv is a hot field or requires assignment from browse
  BRW1.AddField(INT:VATRate,BRW1.Q.INT:VATRate)   ! Field INT:VATRate is a hot field or requires assignment from browse
  BRW1.AddField(INT:CreatedDate,BRW1.Q.INT:CreatedDate) ! Field INT:CreatedDate is a hot field or requires assignment from browse
  BRW1.AddField(INT:DINo,BRW1.Q.INT:DINo)         ! Field INT:DINo is a hot field or requires assignment from browse
  BRW1.AddField(INT:Leg,BRW1.Q.INT:Leg)           ! Field INT:Leg is a hot field or requires assignment from browse
  BRW1.AddField(INT:CR_TIN,BRW1.Q.INT:CR_TIN)     ! Field INT:CR_TIN is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BranchName,BRW1.Q.BRA:BranchName) ! Field BRA:BranchName is a hot field or requires assignment from browse
  BRW1.AddField(INT:Rate,BRW1.Q.INT:Rate)         ! Field INT:Rate is a hot field or requires assignment from browse
  BRW1.AddField(INT:TID,BRW1.Q.INT:TID)           ! Field INT:TID is a hot field or requires assignment from browse
  BRW1.AddField(INT:BID,BRW1.Q.INT:BID)           ! Field INT:BID is a hot field or requires assignment from browse
  BRW1.AddField(INT:Status,BRW1.Q.INT:Status)     ! Field INT:Status is a hot field or requires assignment from browse
  BRW1.AddField(INT:DID,BRW1.Q.INT:DID)           ! Field INT:DID is a hot field or requires assignment from browse
  BRW1.AddField(INT:UID,BRW1.Q.INT:UID)           ! Field INT:UID is a hot field or requires assignment from browse
  BRW1.AddField(MAN:MID,BRW1.Q.MAN:MID)           ! Field MAN:MID is a hot field or requires assignment from browse
  BRW1.AddField(VCO:VCID,BRW1.Q.VCO:VCID)         ! Field VCO:VCID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BID,BRW1.Q.BRA:BID)           ! Field BRA:BID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:TID,BRW1.Q.TRA:TID)           ! Field TRA:TID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_Transporter_Invoices',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      IF p:TID ~= 0
         ! Only show for this Transporter
         HIDE(?Tab:2)
         HIDE(?Tab:4)
         HIDE(?Tab:5)
         HIDE(?Tab:6)
         HIDE(?Tab6)
         HIDE(?Tab7)
  
         HIDE(?SelectTransporter)
  
         TRA:TID          = p:TID
         IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
         .
         LOC:Selected_TID = p:TID
      .
  BRW1.AskProcedure = 1
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_Transporter_Invoices',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,31,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW1::SortHeader.Init(Queue:Browse:1,?Browse:1,'','',BRW1::View:Browse,INT:PKey_TIN)
  BRW1::SortHeader.UseSortColors = False
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:InvoiceTransporterAlias.Close
  !Kill the Sort Header
  BRW1::SortHeader.Kill()
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_Transporter_Invoices',QuickWindow) ! Save window data to non-volatile store
  END
      IF LOC:Selected_Clients = FALSE AND LOC:Tagging = TRUE
         p_Client_Tags.ClearAllTags()
      .
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_TransporterInvoice
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW1::SortHeader.SetAlerts()


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_DateRange
            L_DG:From_Date  = GETINI('Browse_Transporter_Invoices', 'From_Date', , GLO:Local_INI)
            L_DG:To_Date    = GETINI('Browse_Transporter_Invoices', 'To_Date', , GLO:Local_INI)
    OF ?Button_Credit_Debit
      BRW1.UpdateViewRecord()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_FG:Status
          LOC:Reset   += 1
    OF ?SelectTransporter
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Transporter()
      ThisWindow.Reset
          LOC:Selected_TID = TRA:TID
    OF ?Select_Tagged
      ThisWindow.Update()
          LOC:Selected_Clients  = TRUE
      
          POST(EVENT:CloseWindow)
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Branches()
      ThisWindow.Reset
    OF ?SelectManifest
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_Manifests('')
      ThisWindow.Reset
    OF ?SelectUsers
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Users()
      ThisWindow.Reset
    OF ?Button_Delivery
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Deliveries()
      ThisWindow.Reset
    OF ?Button_Clear_Delivery
      ThisWindow.Update()
          CLEAR(DEL:Record)
    OF ?Button_DateRange
      ThisWindow.Update()
      LOC:DateGroup = Ask_Date_Range(L_DG:From_Date, L_DG:To_Date)
      ThisWindow.Reset
          IF L_DG:To_Date ~= 0
             L_DG:To_Date    += 1
          .
      
          IF L_DG:From_Date > 0 AND L_DG:To_Date > 0
             ?Button_DateRange{PROP:Text}    = FORMAT(L_DG:From_Date,@d5) & ' - ' & FORMAT(L_DG:To_Date,@d5)
          ELSIF L_DG:From_Date > 0
             ?Button_DateRange{PROP:Text}    = 'From ' & FORMAT(L_DG:From_Date,@d5)
          ELSIF L_DG:To_Date > 0
             ?Button_DateRange{PROP:Text}    = 'To ' & FORMAT(L_DG:To_Date-1,@d5)
          ELSE
             ?Button_DateRange{PROP:Text}    = 'Limit to Date Range'
          .
      
      
          PUTINI('Browse_Transporter_Invoices', 'From_Date', L_DG:From_Date, GLO:Local_INI)
          PUTINI('Browse_Transporter_Invoices', 'To_Date', L_DG:To_Date, GLO:Local_INI)
      
      
          LOC:Reset       += 1
    OF ?Button_Credit_Debit
      ThisWindow.Update()
      GlobalRequest = InsertRecord
      Update_TransporterInvoice(INT:TIN, 1)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Take Sort Headers Events
  IF BRW1::SortHeader.TakeEvents()
     RETURN Level:Notify
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
        CASE EVENT()
        OF EVENT:Accepted
           !EVENT:MouseDown
    !       BRW1.UpdateViewRecord()
    
           IF KeyCode() = ShiftMouseLeft
              ! Tag all records between this one and last tagged record
              IF CHOICE(?CurrentTab) = 1
                 !DO Tag_From_To
              .
           ELSIF KeyCode() = CtrlMouseLeft
              DO Tag_Toggle_this_rec
           ELSIF KeyCode() ~= MouseRight AND KeyCode() ~= CtrlMouseRight        !AND LOC:Last_Tagged_Rec_Id ~= 0
    !          InBox_Tags.ClearAllTags()                ! Clear all tags when no shift left mouse
    !          LOC:Last_Tagged_Rec_Id        = 0
    
    !          BRW1.ResetFromBuffer()
           .
    
    !       LOC:Last_Clicked_Rec_ID      = REC:Rec_ID
    !       LOC:Last_Tagged_Date         = REC:TA_Date
    !       LOC:Last_Tagged_Time         = REC:TA_Time
        .
    CASE EVENT()
    OF EVENT:AlertKey
          CASE Keycode()
          OF CtrlA
             DO Tag_All
          OF CtrlU
             DO Un_Tag_All
          .
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_FG:Status
          LOC:Reset   += 1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          ?String_Tagged{PROP:Text}   = ''
          IF LOC:Tagging ~= TRUE
             HIDE(?Select_Tagged)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?CurrentTab) = 6
    RETURN SELF.SetSort(5,Force)
  ELSIF CHOICE(?CurrentTab) = 7
    RETURN SELF.SetSort(6,Force)
  ELSE
    RETURN SELF.SetSort(7,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
       ! No Payments, Partially Paid, Credit Note, Fully Paid
       EXECUTE INT:Status + 1
          L_BG:Status     = 'No Payments'
          L_BG:Status     = 'Partially Paid'
          L_BG:Status     = 'Credit Note'
          L_BG:Status     = 'Fully Paid'
       .
  
  
      L_BG:Total  = INT:Cost + INT:VAT
  
      ! (p:TIN, p:CR_TIN, p:Status, p_b:Cost_Total, pb:Payments, pb:Credits, pb:Outstanding)
      Get_Transporter_Totals(INT:TIN, INT:CR_TIN, INT:Status, L_BG:Total, L_BG:Payments, L_BG:Credits, L_BG:Outstanding)
  
  
  !    ! Invoices may be credited or have payments
  !    L_BG:Payments       = Get_TransportersPay_Alloc_Amt(INT:TIN)
  !
  !    ! p:Options
  !    !   0.  All
  !    !   1.  Credits
  !    !   2.  Debits
  !    L_BG:Credits        = -Get_InvTransporter_Credited(INT:TIN,1)       ! Credited is negative
  !
  !    ! Credit notes may have associated invoices or not??
  !    IF INT:CR_TIN ~= 0                                                  ! Should only be for credit notes!
  !       ! No Payments|Partially Paid|Credit Note|Fully Paid
  !       IF INT:Status = 2
  !       ELSE
  !          ! ??? problem, have Credit Note ID but not Credit Note!
  !       .
  !
  !       ! The L_BG:Credited should be zero as Credit Notes cannot be credited.
  !
  !       L_BG:Credits    += -Get_InvTransporter_Credited(INT:CR_TIN, 1)   ! This is total credited (and will include this Credit Note)
  !       L_BG:Payments   += Get_TransportersPay_Alloc_Amt(INT:CR_TIN)
  !
  !       A_INT:TIN        = INT:CR_TIN
  !       IF Access:InvoiceTransporterAlias.TryFetch(A_INT:PKey_TIN) ~= LEVEL:Benign
  !          CLEAR(A_INT:Record)
  !       .
  !
  !       L_BG:Outstanding = (A_INT:Cost + A_INT:VAT) - (L_BG:Payments + L_BG:Credits)
  !    ELSE
  !       L_BG:Outstanding = L_BG:Total  - (L_BG:Payments + L_BG:Credits)
  !    .
  PARENT.SetQueueRecord
  
  SELF.Q.INT:TIN_Style = 0 ! 
  SELF.Q.INT:CIN_Style = 0 ! 
  SELF.Q.INT:InvoiceDate_Style = 0 ! 
  IF (INT:Manifest = 1)
    SELF.Q.INT:Manifest_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.INT:Manifest_Icon = 1                           ! Set icon from icon list
  END
  IF (INT:Broking = 1)
    SELF.Q.INT:Broking_Icon = 2                            ! Set icon from icon list
  ELSE
    SELF.Q.INT:Broking_Icon = 1                            ! Set icon from icon list
  END
  IF (INT:ExtraInv = 1)
    SELF.Q.INT:ExtraInv_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.INT:ExtraInv_Icon = 1                           ! Set icon from icon list
  END
      DO Style_Entry
  


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder<>NewOrder THEN
     BRW1::SortHeader.ClearSort()
  END
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>6,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>6,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     BRW1::SortHeader.RestoreHeaderText()
     BRW1.RestoreSort()
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
        BRW1::SortHeader.ResetSort()
     ELSE
        BRW1::SortHeader.SortQueue()
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Button_DateRange, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Button_DateRange
  SELF.SetStrategy(?Group_ShowStatus, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_ShowStatus

BRW1::SortHeader.QueueResorted       PROCEDURE(STRING pString)
  CODE
    IF pString = ''
       BRW1.RestoreSort()
       BRW1.ResetSort(True)
    ELSE
       BRW1.ReplaceSort(pString,BRW1::Sort0:Locator)
       BRW1.SetLocatorFromSort()
    END
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_TransporterInvoice PROCEDURE (p:TIN, p:Adjustment, p:Pos)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Status           BYTE                                  !used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid
LOC:Screen_Group     GROUP,PRE(L_SG)                       !
TransporterName      STRING(35)                            !Transporters Name
Total                DECIMAL(12,2)                         !
ChargesVAT           BYTE                                  !This Transporter charges / pays VAT
Total_Payments       DECIMAL(12,2)                         !
Total_Credits        DECIMAL(12,2)                         !Credit notes passed total
Leg_AddressName1     STRING('''={9} Address Name ={10}''S') !Name of this address
Leg_AddressName2     STRING('''={9} Address Name ={10}'' ') !Name of this address
Login                STRING(20)                            !User Login
Extra_Leg            BYTE                                  !This is an extra leg invoice
                     END                                   !
LOC:Request          LONG                                  !
LOC:Original_Details GROUP,PRE(L_OD)                       !
TID                  ULONG                                 !Transporter ID
CR_TIN               ULONG                                 !Transporter Invoice No.
MID                  ULONG                                 !Manifest ID
Comment              STRING(255)                           !
Manifest             BYTE                                  !This is the Manifest invoice
                     END                                   !
L_SQ:Alias_Invoice_Status STRING(20)                       !used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid
LOC:Security         GROUP,PRE(L_SECG)                     !
Change_BID           BYTE                                  !When not in insert mode
                     END                                   !
BRW2::View:Browse    VIEW(TransporterPaymentsAllocations)
                       PROJECT(TRAPA:TPID)
                       PROJECT(TRAPA:AllocationNo)
                       PROJECT(TRAPA:AllocationDate)
                       PROJECT(TRAPA:AllocationTime)
                       PROJECT(TRAPA:Amount)
                       PROJECT(TRAPA:Comment)
                       PROJECT(TRAPA:MID)
                       PROJECT(TRAPA:TRPAID)
                       PROJECT(TRAPA:TIN)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
TRAPA:TPID             LIKE(TRAPA:TPID)               !List box control field - type derived from field
TRAPA:AllocationNo     LIKE(TRAPA:AllocationNo)       !List box control field - type derived from field
TRAPA:AllocationDate   LIKE(TRAPA:AllocationDate)     !List box control field - type derived from field
TRAPA:AllocationTime   LIKE(TRAPA:AllocationTime)     !List box control field - type derived from field
TRAPA:Amount           LIKE(TRAPA:Amount)             !List box control field - type derived from field
TRAPA:Comment          LIKE(TRAPA:Comment)            !List box control field - type derived from field
TRAPA:MID              LIKE(TRAPA:MID)                !List box control field - type derived from field
TRAPA:TRPAID           LIKE(TRAPA:TRPAID)             !List box control field - type derived from field
TRAPA:TIN              LIKE(TRAPA:TIN)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW13::View:Browse   VIEW(InvoiceTransporterAlias)
                       PROJECT(A_INT:TIN)
                       PROJECT(A_INT:CR_TIN)
                       PROJECT(A_INT:Cost)
                       PROJECT(A_INT:VAT)
                       PROJECT(A_INT:ExtraInv)
                       PROJECT(A_INT:InvoiceDate)
                       PROJECT(A_INT:TID)
                       PROJECT(A_INT:Status)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
A_INT:TIN              LIKE(A_INT:TIN)                !List box control field - type derived from field
A_INT:CR_TIN           LIKE(A_INT:CR_TIN)             !List box control field - type derived from field
A_INT:Cost             LIKE(A_INT:Cost)               !List box control field - type derived from field
A_INT:VAT              LIKE(A_INT:VAT)                !List box control field - type derived from field
A_INT:ExtraInv         LIKE(A_INT:ExtraInv)           !List box control field - type derived from field
A_INT:ExtraInv_Icon    LONG                           !Entry's icon ID
A_INT:InvoiceDate      LIKE(A_INT:InvoiceDate)        !List box control field - type derived from field
L_SQ:Alias_Invoice_Status LIKE(L_SQ:Alias_Invoice_Status) !List box control field - type derived from local data
A_INT:TID              LIKE(A_INT:TID)                !List box control field - type derived from field
A_INT:Status           LIKE(A_INT:Status)             !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::INT:Record  LIKE(INT:RECORD),THREAD
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW13::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW13::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW13::PopupChoice   SIGNED                       ! Popup current choice
BRW13::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW13::PopupChoiceExec BYTE(0)                    ! Popup executed
QuickWindow          WINDOW('Form Invoice Transporter'),AT(,,362,278),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,IMM,MDI,HLP('Update_InvoiceTransporter'),SYSTEM
                       PROMPT('TIN:'),AT(306,4),USE(?INT:TIN:Prompt)
                       STRING(@n_10),AT(315,4),USE(INT:TIN),RIGHT(1),TRN
                       SHEET,AT(2,2,357,252),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           GROUP,AT(73,20,161,10),USE(?Group_Transporter)
                             BUTTON('...'),AT(52,20,12,10),USE(?CallLookup)
                             ENTRY(@s35),AT(68,20,129,10),USE(L_SG:TransporterName),MSG('Transporters Name'),TIP('Transporters Name')
                           END
                           GROUP,AT(47,20,209,46),USE(?Group_TID_MID)
                             GROUP,AT(127,36,110,10),USE(?Group_Cr_TIN)
                               BUTTON('...'),AT(116,36,12,10),USE(?CallLookup_TransInv)
                               ENTRY(@n_10b),AT(134,36,64,10),USE(INT:CR_TIN),RIGHT(1),MSG('Transporter Invoice No.')
                             END
                             GROUP,AT(125,52,110,10),USE(?Group_MID)
                               BUTTON('...'),AT(116,52,12,10),USE(?CallLookup_Manifest)
                               ENTRY(@n_10b),AT(134,52,64,10),USE(INT:MID),RIGHT(1),MSG('Manifest ID')
                             END
                           END
                           BUTTON('Change'),AT(204,20,39,10),USE(?Button_Change_Transp)
                           BUTTON('&Jump To'),AT(203,37,,10),USE(?Button_Jump_to_Adj_Invoice),SKIP
                           BUTTON('&Jump To'),AT(203,52,39,10),USE(?Button_Jump_to_Manifest),SKIP
                           BUTTON('...'),AT(274,52,12,10),USE(?CallLookup_Branch)
                           PROMPT('Transporter:'),AT(9,20),USE(?TransporterName:Prompt),TRN
                           PROMPT('Adjustment to Invoice:'),AT(9,36),USE(?INT:CR_TIN:Prompt:2),TRN
                           PROMPT('TID:'),AT(290,20),USE(?INT:TID:Prompt),TRN
                           STRING(@n_10),AT(310,20),USE(INT:TID),RIGHT(1),TRN
                           PROMPT('BID:'),AT(290,31),USE(?INT:BID:Prompt),TRN
                           STRING(@n_10),AT(310,31,44,10),USE(INT:BID),RIGHT(1),TRN
                           PROMPT('Branch Name:'),AT(290,42),USE(?BRA:BranchName:Prompt),TRN
                           ENTRY(@s35),AT(290,52,64,10),USE(BRA:BranchName),COLOR(00E9E9E9h),MSG('Branch Name'),READONLY, |
  SKIP,TIP('Branch Name')
                           PROMPT('Manifest MID:'),AT(9,52),USE(?INT:MID:Prompt:2),TRN
                           STRING('* Adjustment Info *'),AT(134,71,201,10),USE(?String_AdjInfo),FONT(,,,FONT:bold,CHARSET:ANSI), |
  HIDE,TRN
                           PROMPT('Cost:'),AT(9,84),USE(?INT:Cost:Prompt),TRN
                           ENTRY(@n-14.2),AT(134,84,64,10),USE(INT:Cost),RIGHT(1)
                           PROMPT('VAT:'),AT(9,100),USE(?INT:VAT:Prompt),TRN
                           ENTRY(@n-14.2),AT(134,100,64,10),USE(INT:VAT),DECIMAL(12),COLOR(00E9E9E9h),MSG('VAT amount'), |
  READONLY,SKIP,TIP('VAT amount')
                           PROMPT('Rate:'),AT(288,100),USE(?INT:Rate:Prompt),TRN
                           ENTRY(@n-13.4),AT(310,100,44,10),USE(INT:Rate),RIGHT(1),COLOR(00E9E9E9h),MSG('Rate per Kg'), |
  READONLY,SKIP,TIP('Rate per Kg')
                           LINE,AT(7,196,348,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('VAT Rate:'),AT(272,84),USE(?INT:VATRate:Prompt),TRN
                           ENTRY(@n-7.2),AT(310,84,44,10),USE(INT:VATRate),RIGHT(1),MSG('VAT rate'),TIP('VAT rate')
                           BUTTON('Change'),AT(63,100,43,10),USE(?Button_Change_VAT)
                           PROMPT('Total:'),AT(9,116),USE(?Total:Prompt),TRN
                           BUTTON('Change'),AT(63,116,43,10),USE(?Button_ChangeTotal)
                           CHECK('Manifest'),AT(208,116),USE(INT:Manifest),LEFT,MSG('This is the Manifest invoice'),TIP('This is th' & |
  'e Manifest invoice'),TRN
                           CHECK(' Extra Inv.'),AT(258,116),USE(INT:ExtraInv),LEFT,MSG('Is this Invoice an Extra Invoice'), |
  TIP('Is this Invoice an Extra Invoice'),TRN
                           ENTRY(@n-17.2),AT(134,116,64,10),USE(L_SG:Total),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP
                           LINE,AT(7,68,348,0),USE(?Line1:2),COLOR(COLOR:Black),LINEWIDTH(2)
                           CHECK(' Broking'),AT(312,116),USE(INT:Broking),LEFT,MSG('Broking Manifest'),TIP('Broking Manifest'), |
  TRN
                           LINE,AT(7,134,348,0),USE(?Line1:3),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Comment:'),AT(9,138),USE(?INT:Comment:Prompt:2),TRN
                           TEXT,AT(63,138,291,30),USE(INT:Comment),VSCROLL,BOXED
                           PROMPT('To create a Credit Note enter a negative cost.  To create an additional Invoice' & |
  ' enter a positive cost.'),AT(63,172,291,20),USE(?Prompt_Action),FONT(,,,FONT:bold,CHARSET:ANSI), |
  HIDE,TRN
                           PROMPT('Created:'),AT(261,202),USE(?INT:CreatedDate:Prompt),TRN
                           GROUP,AT(123,202,93,10),USE(?Group3),TRN
                             ENTRY(@d5b),AT(134,202,64,10),USE(INT:InvoiceDate),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP
                             BUTTON('...'),AT(116,202,12,10),USE(?Calendar),DISABLE
                           END
                           STRING(@d5b),AT(291,202,,10),USE(INT:CreatedDate),LEFT(1),TRN
                           PROMPT('Creditor Inv.:'),AT(9,218),USE(?INT:CIN:Prompt),TRN
                           ENTRY(@s30),AT(134,218,64,10),USE(INT:CIN),MSG('Creditors Invoice No. - 3rd party reference'), |
  TIP('Creditors Invoice No. - 3rd party reference')
                           PROMPT('Date Received:'),AT(222,218),USE(?INT:CIN_DateReceived:Prompt),TRN
                           BUTTON('...'),AT(275,218,12,10),USE(?Calendar:CINDateReceived)
                           ENTRY(@d5b),AT(290,218,64,10),USE(INT:CIN_DateReceived),TIP('Received from Creditor')
                           STRING(@t7),AT(329,202,,10),USE(INT:CreatedTime),RIGHT(1),TRN
                           LINE,AT(7,233,348,0),USE(?Line1:4),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Invoice Date:'),AT(9,202),USE(?INT:InvoiceDate:Prompt),TRN
                           BUTTON('Change'),AT(63,202,43,10),USE(?Button_ChangeInvDate)
                           PROMPT('Status:'),AT(9,238),USE(?INT:Status:Prompt),TRN
                           BUTTON('Update'),AT(63,238,43,10),USE(?Button_Update)
                           LIST,AT(134,238,64,10),USE(INT:Status),COLOR(00E9E9E9h),DISABLE,DROP(15),FROM('No Payment' & |
  's|#0|Partially Paid|#1|Credit Note|#2|Fully Paid|#3'),TIP('No Payments, Partially Pa' & |
  'id, Credit Note, Fully Paid'),MSG('used for filtering - No Payments, Partially Paid,' & |
  ' Credit Note, Fully Paid')
                           CHECK('Extra Leg'),AT(308,238),USE(L_SG:Extra_Leg),LEFT,DISABLE,MSG('This is an extra l' & |
  'eg invoice'),TIP('This is an extra leg invoice'),TRN
                         END
                         TAB('&2) Payment Allocations / Credit Notes && Additionals'),USE(?Tab:2)
                           LIST,AT(9,18,343,94),USE(?Browse:2),HVSCROLL,FORMAT('30R(2)|M~TPID~C(0)@n_10@[24R(2)|M~' & |
  'No.~C(0)@n_5@38R(2)|M~Date~C(0)@d5b@38R(2)|M~Time~C(0)@t7@]|M~Allocation~52R(1)|M~Am' & |
  'ount~C(0)@n-14.2@80L(2)|M~Comment~@s255@40R(2)|M~MID~C(0)@n_10@40R(2)|M~TRPAID~C(0)@n_10@'), |
  FROM(Queue:Browse:2),IMM,MSG('Browsing the TransporterPaymentsAllocations file')
                           BUTTON('&Insert'),AT(198,114,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(252,114,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(304,114,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                           STRING('Credit Notes / Additional Invoices'),AT(9,132),USE(?String7)
                           LIST,AT(9,143,343,90),USE(?List),VSCROLL,FORMAT('36R(2)|M~TIN~C(0)@n_10@44R(2)|M~Relate' & |
  'd TIN~C(0)@n_10@48R(2)|M~Cost~C(0)@n-14.2@44R(2)|M~VAT~C(0)@n-14.2@34R(2)|MI~Extra I' & |
  'nv.~C(0)@p p@38R(2)|M~Date Invoice~L(1)@d5b@80L(2)|M~Status~C(0)@s20@36R(2)|M~TID~C(0)@n_10@'), |
  FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           PROMPT('User:'),AT(9,238),USE(?Login:Prompt),TRN
                           ENTRY(@s20),AT(44,238,64,10),USE(L_SG:Login),COLOR(00E9E9E9h),MSG('User Login'),READONLY,SKIP, |
  TIP('User Login')
                         END
                         TAB('&3) Extra Leg'),USE(?Tab_ExtraLeg),DISABLE
                           PROMPT('DI No.:'),AT(11,25),USE(?INT:DINo:Prompt),TRN
                           ENTRY(@n_10b),AT(77,25,60,10),USE(INT:DINo),RIGHT(1),COLOR(00E9E9E9h),MSG('Delivery Ins' & |
  'truction Number'),READONLY,SKIP,TIP('Delivery Instruction Number')
                           PROMPT('DID:'),AT(288,25),USE(?INT:DID:Prompt),TRN
                           STRING(@n_10b),AT(308,25),USE(INT:DID),RIGHT(1),TRN
                           BUTTON('...'),AT(142,25,12,10),USE(?Button_ChooseDI),TIP('Select Delivery Instruction')
                           PROMPT('DLID:'),AT(288,39),USE(?INT:DLID:Prompt),TRN
                           STRING(@n_10b),AT(308,39),USE(INT:DLID),RIGHT(1),TRN
                           PROMPT('Collection Address:'),AT(11,66),USE(?L_SG:Leg_AddressName1:Prompt),TRN
                           ENTRY(@s35),AT(77,66,145,10),USE(L_SG:Leg_AddressName1),COLOR(00E9E9E9h),MSG('Name of th' & |
  'is address'),READONLY,SKIP,TIP('Name of this address')
                           PROMPT('AID:'),AT(288,66),USE(?INT:CollectionAID:Prompt),TRN
                           STRING(@n_10b),AT(308,66),USE(INT:CollectionAID),RIGHT(1),TRN
                           PROMPT('AID:'),AT(288,84),USE(?INT:DeliveryAID:Prompt),TRN
                           STRING(@n_10b),AT(308,84),USE(INT:DeliveryAID),RIGHT(1),TRN
                           PROMPT('Delivery Address:'),AT(11,84),USE(?Leg_AddressName2:Prompt),TRN
                           ENTRY(@s35),AT(77,84,145,10),USE(L_SG:Leg_AddressName2),COLOR(00E9E9E9h),MSG('Name of th' & |
  'is address'),READONLY,SKIP,TIP('Name of this address')
                           PROMPT('Leg No.:'),AT(11,39),USE(?INT:Leg:Prompt),TRN
                           ENTRY(@n5b),AT(77,39,60,10),USE(INT:Leg),RIGHT(1),COLOR(00E9E9E9h),MSG('Leg Number'),READONLY, |
  SKIP,TIP('Leg Number')
                         END
                         TAB('* Dev Notes *'),USE(?Tab3),HIDE
                           PROMPT('If this invoice has a INT:CR_TIN then it is an adjustment to an existing Invoic' & |
  'e - either a credit or an additional debit.  If it is either then the MID must be go' & |
  't from this adjusting Invoice and not set by the user.'),AT(10,22,341,30),USE(?Prompt16), |
  TRN
                           PROMPT('However if this invoice is not an adjustment to a previous invoice but an addit' & |
  'ional invocie or credit note, then the user may select the MID (or none) to associat' & |
  'e the invoice with.'),AT(10,54,341,30),USE(?Prompt16:2),TRN
                           BUTTON('&Help'),AT(8,234,26,15),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                         END
                       END
                       BUTTON('&OK'),AT(258,260,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(310,260,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       PROMPT(''),AT(4,258,229,20),USE(?Prompt_Invoice)
                     END

BRW2::LastSortOrder       BYTE
BRW13::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW13                CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW13::Sort0:Locator StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar12           CalendarClass
Calendar15           CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
Local_Class     CLASS
Check_Manifest      PROCEDURE(),LONG
    .

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Calcs                              ROUTINE
    IF LOC:Request ~= ViewRecord
       IF INT:VAT_Specified = FALSE
          INT:VAT       = INT:Cost * (INT:VATRate / 100)
    .  .

    !INT:Rate    = Weight / INT:Cost + INT:VAT

    L_SG:Total          = INT:Cost + INT:VAT


    ! INT:Status    - No Payments|Partially Paid|Credit Note|Fully Paid
    IF LOC:Request = InsertRecord
       IF INT:Cost < 0.0
          INT:Status    = 2                             ! Credit
       ELSE
          INT:Status    = 0
       .
    ELSIF LOC:Request = ChangeRecord
       IF INT:Cost < 0.0
          IF INT:Status ~= 2
             INT:Status = 2                             ! Credit
             MESSAGE('Status of this record was not correct.||This record is a credit note record, the Status has been changed.','Status',ICON:Exclamation)
          .
       ELSE
          IF INT:Status = 2                             ! This is not a credit
             INT:Status    = 0
             MESSAGE('Status of this record was not correct.||This record is NOT a credit note record, the Status has been changed.','Status',ICON:Exclamation)
    .  .  .

    DISPLAY
    EXIT
Adjust_Info                     ROUTINE
    IF TRA:TID ~= INT:TID OR CLIP(L_SG:TransporterName) = ''
    !db.debugout('[Upd._Trans.Inv.]  TRA:TID: ' & TRA:TID & ',   INT:TID: ' & INT:TID & ',   L_SG:TransporterName: ' & CLIP(L_SG:TransporterName))

       CLEAR(TRA:Record)

       TRA:TID                     = INT:TID
       IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
          L_SG:TransporterName     = TRA:TransporterName

          IF ThisWindow.Request = InsertRecord
             IF TRA:ChargesVAT = TRUE
                INT:VATRate           = Get_Setup_Info(4)
             ELSE
                INT:VATRate           = 0.0
          .  .
       ELSE
          CLEAR(TRA:Record)
    .  .



    IF INT:CR_TIN ~= 0
       ! p:Option
       !   0.  Invoice Paid
       !   1.  Clients Payments Allocation total
       L_SG:Total_Payments  = Get_TransportersPay_Alloc_Amt(INT:CR_TIN, 0)

       ! p:Options
       !   0.  All
       !   1.  Credits
       !   2.  Debits
       ! p:TIN_to_Ignore
       L_SG:Total_Credits   = -Get_InvTransporter_Credited(INT:CR_TIN, 1, INT:TIN)      ! Change to positive value

       A_INT:TIN      = INT:CR_TIN
       IF Access:InvoiceTransporterAlias.TryFetch(A_INT:PKey_TIN) ~= LEVEL:Benign
       ELSE
          ?Prompt_Invoice{PROP:Text}    = 'Owing on Invoice: ' & LEFT(CLIP(FORMAT(A_INT:Cost + A_INT:VAT - (L_SG:Total_Payments + L_SG:Total_Credits) + L_SG:Total,@n12.2))) & |
                                          '<13,10>Total Invoice Cost: ' & LEFT(CLIP(FORMAT(A_INT:Cost + A_INT:VAT, @n12.2)))
       .

       IF INT:Cost = 0.0
          ?String_AdjInfo{PROP:Text}    = 'Adjustment to Invoice'
       ELSIF INT:Cost > 0.0
          ?String_AdjInfo{PROP:Text}    = 'Debit - Adjustment to Invoice'
       ELSE
          ?String_AdjInfo{PROP:Text}    = 'Credit - Adjustment to Invoice'
       .
       UNHIDE(?String_AdjInfo)
    ELSE
       ?String_AdjInfo{PROP:Text}       = ''
       ?Prompt_Invoice{PROP:Text}       = ''
       HIDE(?String_AdjInfo)
    .

    DISPLAY
    EXIT
MID_Chosen                  ROUTINE
    ! Make sure we are not in non-stop select / accept all mode - where the user did not select anything...
    IF QuickWindow{PROP:AcceptAll} = FALSE
       IF INT:MID ~= 0
          ! Check the state of the Manifest  -  Loading, Loaded, On Route, Transferred
          IF MAN:State < 3
             MESSAGE('The selected Manifest is not completed (Transferred).', 'Update Transporter Invoices', ICON:Hand)

             CLEAR(INT:MID)
             SELECT(?INT:MID)
          ELSE
             ! We have an Invoice for this Manifest, get this number too?
             CLEAR(A_INT:Record,-1)
             A_INT:MID   = INT:MID
             SET(A_INT:Fkey_MID, A_INT:Fkey_MID)
             LOOP
                IF Access:InvoiceTransporterAlias.TryNext() ~= LEVEL:Benign
                   CLEAR(A_INT:Record)
                   BREAK
                .
                IF A_INT:MID ~= INT:MID
                   CLEAR(A_INT:Record)
                   BREAK
                .
                IF A_INT:CR_TIN = 0         ! This is the original Manifest Invoice
                   BREAK
             .  .

             ! If same Invoice and not credit / additional invoice then use this TIN for this CR_TIN
             IF A_INT:CR_TIN = 0 AND A_INT:MID = INT:MID
                IF INT:CR_TIN ~= 0 AND INT:CR_TIN ~= A_INT:TIN
                   CASE MESSAGE('You have specified a Credit Invoice No. already but the Manifest No. you have selected does not match this Invoice no.||Would you like to replace the Credit Invoice no. with the Manifest Invoice no.?', 'Manifest Invoice', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                   OF BUTTON:Yes
                      INT:CR_TIN    = A_INT:TIN
                   OF BUTTON:No
                      CLEAR(INT:CR_TIN)
                   .
                ELSE
                   INT:CR_TIN   = A_INT:TIN
  !                 db.debugout('[Update_Trans.Inv.]  Assign A_INT:TIN: ' & A_INT:TIN & ',  INT:MID: ' & INT:MID & ',  A_INT:MID: ' & A_INT:MID)
             .  .

             IF INT:TID = 0
                INT:TID = MAN:TID
             .

             ! Look up the transporter
             DO Adjust_Info
       .  .
    ELSE
!       QuickWindow{PROP:AcceptAll}  = FALSE
!       MESSAGE('The selected Manifest is not completed.', 'Update Transporter Invoices', ICON:Hand)
!       CLEAR(INT:MID)
!       SELECT(?INT:MID)
    .


    IF INT:MID ~= 0
       ENABLE(?L_SG:Extra_Leg)
    ELSE
       DISABLE(?L_SG:Extra_Leg)
    .
    EXIT
Check_Flags                  ROUTINE
    IF QuickWindow{PROP:AcceptAll} = FALSE
       IF L_SG:Extra_Leg = TRUE AND (INT:ExtraInv = TRUE OR INT:Manifest = TRUE)
          INT:Manifest  = FALSE
          INT:ExtraInv  = FALSE
          MESSAGE('You cannot have an Extra Leg and an Extra Invoice or Manifest invoice.||Un-check Extra Leg if you are sure.', 'Invoice Flag Validation', ICON:Hand)
       ELSE
          IF INT:ExtraInv = TRUE AND INT:Manifest = TRUE
             CASE MESSAGE('Both Manifest invoice and Extra Invoice cannot be set at the same time.||Is this invoice an Extra Invoice?', 'Invoice Flag Validation', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
             OF BUTTON:Yes
                INT:Manifest  = FALSE
             OF BUTTON:No
                INT:ExtraInv  = FALSE
       .  .  .

       DISPLAY
    ELSE                    ! OK button pressed
       ! Check the other flags, either a Manifest Invoice, an Adjustment or an Extra Invoice
       IF INT:CR_TIN > 0 AND (INT:Manifest = TRUE OR INT:ExtraInv = TRUE)
          INT:Manifest  = FALSE
          INT:ExtraInv  = FALSE
          QuickWindow{PROP:AcceptAll} = FALSE
          MESSAGE('You cannot have an Adjustment invoice which is also a Extra Leg or Manifest invoice.||Manifest and Extra Invoice flags cleared.', 'Invoice Flag Validation', ICON:Hand)
       ELSIF INT:CR_TIN = 0
          IF INT:Manifest = FALSE AND INT:ExtraInv = FALSE AND L_SG:Extra_Leg = FALSE
             INT:ExtraInv   = TRUE
             QuickWindow{PROP:AcceptAll} = FALSE
             MESSAGE('You cannot have an invoice which is not an Extra Leg, Manifest, Adjustment or Extra Invoice.||Extra Invoice flag set.', 'Invoice Flag Validation', ICON:Hand)
       .  .
       DISPLAY
    .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Transporter Invoice Record'
  OF InsertRecord
    ActionMessage = 'Transporter Inv. Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Transporter Inv. Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_TransporterInvoice')
      IF p:TIN ~= 0
         CLEAR(INT:Record)
         IF Access:_InvoiceTransporter.PrimeRecord() = LEVEL:Benign
  
      .  .
  SELF.Request = GlobalRequest                    ! Store the incoming request
      LOC:Request     = SELF.Request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?INT:TIN:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_SQ:Alias_Invoice_Status',L_SQ:Alias_Invoice_Status) ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:_InvoiceTransporter)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(INT:Record,History::INT:Record)
  SELF.AddHistoryField(?INT:TIN,1)
  SELF.AddHistoryField(?INT:CR_TIN,2)
  SELF.AddHistoryField(?INT:MID,5)
  SELF.AddHistoryField(?INT:TID,3)
  SELF.AddHistoryField(?INT:BID,4)
  SELF.AddHistoryField(?INT:Cost,15)
  SELF.AddHistoryField(?INT:VAT,16)
  SELF.AddHistoryField(?INT:Rate,17)
  SELF.AddHistoryField(?INT:VATRate,18)
  SELF.AddHistoryField(?INT:Manifest,29)
  SELF.AddHistoryField(?INT:ExtraInv,34)
  SELF.AddHistoryField(?INT:Broking,28)
  SELF.AddHistoryField(?INT:Comment,32)
  SELF.AddHistoryField(?INT:InvoiceDate,21)
  SELF.AddHistoryField(?INT:CreatedDate,25)
  SELF.AddHistoryField(?INT:CIN,36)
  SELF.AddHistoryField(?INT:CIN_DateReceived,39)
  SELF.AddHistoryField(?INT:CreatedTime,26)
  SELF.AddHistoryField(?INT:Status,30)
  SELF.AddHistoryField(?INT:DINo,9)
  SELF.AddHistoryField(?INT:DID,8)
  SELF.AddHistoryField(?INT:DLID,10)
  SELF.AddHistoryField(?INT:CollectionAID,12)
  SELF.AddHistoryField(?INT:DeliveryAID,13)
  SELF.AddHistoryField(?INT:Leg,11)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                           ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Relate:InvoiceTransporterAlias.Open             ! File InvoiceTransporterAlias used by this procedure, so make sure it's RelationManager is open
  Access:_InvoiceTransporter.UseFile              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Branches.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Invoice.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_InvoiceTransporter
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:TransporterPaymentsAllocations,SELF) ! Initialize the browse manager
  BRW13.Init(?List,Queue:Browse.ViewPosition,BRW13::View:Browse,Queue:Browse,Relate:InvoiceTransporterAlias,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?L_SG:TransporterName{PROP:ReadOnly} = True
    DISABLE(?CallLookup_TransInv)
    ?INT:CR_TIN{PROP:ReadOnly} = True
    DISABLE(?CallLookup_Manifest)
    DISABLE(?Button_Change_Transp)
    DISABLE(?Button_Jump_to_Adj_Invoice)
    DISABLE(?Button_Jump_to_Manifest)
    DISABLE(?CallLookup_Branch)
    ?BRA:BranchName{PROP:ReadOnly} = True
    ?INT:Cost{PROP:ReadOnly} = True
    ?INT:VAT{PROP:ReadOnly} = True
    ?INT:Rate{PROP:ReadOnly} = True
    ?INT:VATRate{PROP:ReadOnly} = True
    DISABLE(?Button_Change_VAT)
    DISABLE(?Button_ChangeTotal)
    ?L_SG:Total{PROP:ReadOnly} = True
    ?INT:InvoiceDate{PROP:ReadOnly} = True
    DISABLE(?Calendar)
    ?INT:CIN{PROP:ReadOnly} = True
    DISABLE(?Calendar:CINDateReceived)
    ?INT:CIN_DateReceived{PROP:ReadOnly} = True
    DISABLE(?Button_ChangeInvDate)
    DISABLE(?Button_Update)
    DISABLE(?INT:Status)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    ?L_SG:Login{PROP:ReadOnly} = True
    ?INT:DINo{PROP:ReadOnly} = True
    DISABLE(?Button_ChooseDI)
    ?L_SG:Leg_AddressName1{PROP:ReadOnly} = True
    ?L_SG:Leg_AddressName2{PROP:ReadOnly} = True
    ?INT:Leg{PROP:ReadOnly} = True
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,TRAPA:FKey_TIN)              ! Add the sort order for TRAPA:FKey_TIN for sort order 1
  BRW2.AddRange(TRAPA:TIN,Relate:TransporterPaymentsAllocations,Relate:_InvoiceTransporter) ! Add file relationship range limit for sort order 1
  BRW2.AddField(TRAPA:TPID,BRW2.Q.TRAPA:TPID)     ! Field TRAPA:TPID is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:AllocationNo,BRW2.Q.TRAPA:AllocationNo) ! Field TRAPA:AllocationNo is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:AllocationDate,BRW2.Q.TRAPA:AllocationDate) ! Field TRAPA:AllocationDate is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:AllocationTime,BRW2.Q.TRAPA:AllocationTime) ! Field TRAPA:AllocationTime is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:Amount,BRW2.Q.TRAPA:Amount) ! Field TRAPA:Amount is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:Comment,BRW2.Q.TRAPA:Comment) ! Field TRAPA:Comment is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:MID,BRW2.Q.TRAPA:MID)       ! Field TRAPA:MID is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:TRPAID,BRW2.Q.TRAPA:TRPAID) ! Field TRAPA:TRPAID is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:TIN,BRW2.Q.TRAPA:TIN)       ! Field TRAPA:TIN is a hot field or requires assignment from browse
  BRW13.Q &= Queue:Browse
  BRW13.AddSortOrder(,A_INT:Key_CR_TIN)           ! Add the sort order for A_INT:Key_CR_TIN for sort order 1
  BRW13.AddRange(A_INT:CR_TIN,INT:TIN)            ! Add single value range limit for sort order 1
  BRW13.AddLocator(BRW13::Sort0:Locator)          ! Browse has a locator for sort order 1
  BRW13::Sort0:Locator.Init(,A_INT:CR_TIN,1,BRW13) ! Initialize the browse locator using  using key: A_INT:Key_CR_TIN , A_INT:CR_TIN
  ?List{PROP:IconList,1} = '~checkoffdim.ico'
  ?List{PROP:IconList,2} = '~checkon.ico'
  BRW13.AddField(A_INT:TIN,BRW13.Q.A_INT:TIN)     ! Field A_INT:TIN is a hot field or requires assignment from browse
  BRW13.AddField(A_INT:CR_TIN,BRW13.Q.A_INT:CR_TIN) ! Field A_INT:CR_TIN is a hot field or requires assignment from browse
  BRW13.AddField(A_INT:Cost,BRW13.Q.A_INT:Cost)   ! Field A_INT:Cost is a hot field or requires assignment from browse
  BRW13.AddField(A_INT:VAT,BRW13.Q.A_INT:VAT)     ! Field A_INT:VAT is a hot field or requires assignment from browse
  BRW13.AddField(A_INT:ExtraInv,BRW13.Q.A_INT:ExtraInv) ! Field A_INT:ExtraInv is a hot field or requires assignment from browse
  BRW13.AddField(A_INT:InvoiceDate,BRW13.Q.A_INT:InvoiceDate) ! Field A_INT:InvoiceDate is a hot field or requires assignment from browse
  BRW13.AddField(L_SQ:Alias_Invoice_Status,BRW13.Q.L_SQ:Alias_Invoice_Status) ! Field L_SQ:Alias_Invoice_Status is a hot field or requires assignment from browse
  BRW13.AddField(A_INT:TID,BRW13.Q.A_INT:TID)     ! Field A_INT:TID is a hot field or requires assignment from browse
  BRW13.AddField(A_INT:Status,BRW13.Q.A_INT:Status) ! Field A_INT:Status is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
      ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned)
      ! (ULONG, STRING      , STRING     , <STRING>       , BYTE=0             , <*BYTE>),LONG
      !   1       2               3               4               5                   6
      ! Returns
      !   0   - Allow (default)
      !   1   - Disable
      !   2   - Hide
      !   100 - Allow                 - Default action
      !   101 - Disallow              - Default action
      !
      !   Application Sections are added if not found
      !   Application Section Usage is logged         - not, only if not found user level????
      !
      ! p:Default_Action
      !   - applies to 1st Add only
  
      L_SECG:Change_BID   = Get_User_Access(GLO:UID, 'Transporters', 'Update_TransporterInvoice', 'Change_Branch', 2)
  INIMgr.Fetch('Update_TransporterInvoice',QuickWindow) ! Restore window settings from non-volatile store
      IF SELF.Request = InsertRecord
         ?INT:InvoiceDate{PROP:ReadOnly}     = FALSE
         ?INT:InvoiceDate{PROP:Skip}         = FALSE
         ?INT:InvoiceDate{PROP:BackGround}   = -1
         ENABLE(?Calendar)
  
         IF p:TIN ~= 0
            ! We are creating an Invoice / Credit Note from an Invoice
  
            A_INT:TIN         = p:TIN
            IF Access:InvoiceTransporterAlias.TryFetch(A_INT:PKey_TIN) = LEVEL:Benign
               INT:BID        = A_INT:BID               !GLO:BranchID
  
               INT:MID        = A_INT:MID
               INT:TID        = A_INT:TID
               INT:JID        = A_INT:JID
  
               INT:CR_TIN     = A_INT:TIN
               INT:Broking    = A_INT:Broking               ! added 29 5 13 
         .  .
  
         DISABLE(?Button_Update)
      ELSE
         LOC:Original_Details :=: INT:Record
  
  
         DISABLE(?Group_TID_MID)
         DISABLE(?Group_Transporter)
  
         IF L_SECG:Change_BID ~= 0
            DISABLE(?CallLookup_Branch)
         .
  
         !   Get_User_Access
         !   (ULONG, STRING, STRING, <STRING>, BYTE=0),LONG
         !   (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:Default_Action)
         !   0   - Allow (default)
         !   1   - Disable
         !   2   - Hide
         !   100 - Allow
         !   101 - Disallow
  
         IF Get_User_Access(GLO:UID, 'Edit Trans. Invoices', 'Update_TransporterInvoice', 'Invoice Date', 2) = 0
            ENABLE(?CallLookup_Branch)
      .  .
      IF p:Adjustment = 1
         DISABLE(?Group_Transporter)
      .
      IF INT:DID ~= 0 OR INT:DINo ~= 0
         ENABLE(?Tab_ExtraLeg)
      .
  
      IF INT:CollectionAID ~= 0
         ADD:AID  = INT:CollectionAID
         IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
            L_SG:Leg_AddressName1 = ADD:AddressName
      .  .
      IF INT:DeliveryAID ~= 0
         ADD:AID  = INT:DeliveryAID
         IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
            L_SG:Leg_AddressName2 = ADD:AddressName
      .  .
      IF INT:MID ~= 0
         ENABLE(?L_SG:Extra_Leg)
  
         IF INT:DID ~= 0 OR INT:DINo ~= 0
            L_SG:Extra_Leg    = TRUE
      .  .
      IF SELF.Request = ViewRecord                    ! Jump to buttons
         ENABLE(?Button_Jump_to_Adj_Invoice)
         ENABLE(?Button_Jump_to_Manifest)
      .
  
  
      IF INT:CR_TIN = 0
         DISABLE(?Button_Jump_to_Adj_Invoice)
      .
      IF INT:MID = 0
         DISABLE(?Button_Jump_to_Manifest)
      .
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 5
  BRW13.AddToolbarTarget(Toolbar)                 ! Browse accepts toolbar control
  BRW13.ToolbarItem.HelpButton = ?Help
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_TransporterInvoice',1,?Browse:2,2,BRW2::PopupTextExt,Queue:Browse:2,8,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW13::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW13::FormatManager.Init('MANTRNIS','Update_TransporterInvoice',1,?List,13,BRW13::PopupTextExt,Queue:Browse,9,LFM_CFile,LFM_CFile.Record)
  BRW13::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
    Relate:InvoiceTransporterAlias.Close
  END
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW13::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_TransporterInvoice',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  INT:BID = GLO:BranchID
  INT:UID = GLO:UID
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Transporter
      Select_TransporterInvoice(1)
      Browse_Manifests(INT:TID)
      Browse_Branches
      Update_TransporterPaymentsAllocations
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      TRA:TransporterName = L_SG:TransporterName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SG:TransporterName = TRA:TransporterName
        INT:TID = TRA:TID
        L_SG:ChargesVAT = TRA:ChargesVAT
      END
      ThisWindow.Reset(1)
          IF SELF.Request = InsertRecord AND QuickWindow{Prop:AcceptAll} = False
             IF L_SG:ChargesVAT = TRUE
                INT:VATRate   = Get_Setup_Info(4)
             ELSE
                INT:VATRate   = 0.0
             .
             DO Calcs
          ELSIF SELF.Request = ChangeRecord AND ?Group_Transporter{PROP:Enabled} = TRUE AND QuickWindow{Prop:AcceptAll} = False
             IF L_SG:ChargesVAT = TRUE AND INT:VATRate = 0.0
                CASE MESSAGE('This Transporter is paying VAT, but the Invoice has no VAT Rate against it.||Would you like to set the VAT Rate now?','Transporter',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                OF BUTTON:Yes
                   INT:VATRate   = Get_Setup_Info(4)
                   DO Calcs
                .
             ELSIF L_SG:ChargesVAT = FALSE AND INT:VATRate ~= 0.0
                CASE MESSAGE('This Transporter is not paying VAT, but the Invoice has a VAT Rate against it.||Would you like to set the VAT Rate to zero now?','Transporter',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                OF BUTTON:Yes
                   INT:VATRate   = 0.0
                   DO Calcs
          .  .  .
      
    OF ?L_SG:TransporterName
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_SG:TransporterName OR ?L_SG:TransporterName{PROP:Req}
          TRA:TransporterName = L_SG:TransporterName
          IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              L_SG:TransporterName = TRA:TransporterName
              INT:TID = TRA:TID
              L_SG:ChargesVAT = TRA:ChargesVAT
            ELSE
              CLEAR(INT:TID)
              CLEAR(L_SG:ChargesVAT)
              SELECT(?L_SG:TransporterName)
              CYCLE
            END
          ELSE
            INT:TID = TRA:TID
            L_SG:ChargesVAT = TRA:ChargesVAT
          END
        END
      END
      ThisWindow.Reset()
          IF SELF.Request = InsertRecord AND QuickWindow{Prop:AcceptAll} = False
             IF L_SG:ChargesVAT = TRUE
                INT:VATRate   = Get_Setup_Info(4)
             ELSE
                INT:VATRate   = 0.0
             .
             DO Calcs
          ELSIF SELF.Request = ChangeRecord AND ?Group_Transporter{PROP:Enabled} = TRUE AND QuickWindow{Prop:AcceptAll} = False
             IF L_SG:ChargesVAT = TRUE AND INT:VATRate = 0.0
                CASE MESSAGE('This Transporter is paying VAT, but the Invoice has no VAT Rate against it.||Would you like to set the VAT Rate now?','Transporter',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                OF BUTTON:Yes
                   INT:VATRate   = Get_Setup_Info(4)
                   DO Calcs
                .
             ELSIF L_SG:ChargesVAT = FALSE AND INT:VATRate ~= 0.0
                CASE MESSAGE('This Transporter is not paying VAT, but the Invoice has a VAT Rate against it.||Would you like to set the VAT Rate to zero now?','Transporter',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                OF BUTTON:Yes
                   INT:VATRate   = 0.0
                   DO Calcs
          .  .  .
    OF ?CallLookup_TransInv
      ThisWindow.Update()
      A_INT:TIN = INT:CR_TIN
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        INT:CR_TIN = A_INT:TIN
        INT:MID = A_INT:MID
        INT:TID = A_INT:TID
      END
      ThisWindow.Reset(1)
          DO Adjust_Info
          ! Set MID from this Invoice then
          IF INT:CR_TIN ~= 0
             DISABLE(?Group_MID)
          ELSE
             ENABLE(?Group_MID)
          .
      
    OF ?INT:CR_TIN
      IF NOT QuickWindow{PROP:AcceptAll}
        IF INT:CR_TIN OR ?INT:CR_TIN{PROP:Req}
          A_INT:TIN = INT:CR_TIN
          IF Access:InvoiceTransporterAlias.TryFetch(A_INT:PKey_TIN)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              INT:CR_TIN = A_INT:TIN
              INT:MID = A_INT:MID
              INT:TID = A_INT:TID
            ELSE
              CLEAR(INT:MID)
              CLEAR(INT:TID)
              SELECT(?INT:CR_TIN)
              CYCLE
            END
          ELSE
            INT:MID = A_INT:MID
            INT:TID = A_INT:TID
          END
        END
      END
      ThisWindow.Reset()
          ! Set MID from this Invoice then
          IF INT:CR_TIN ~= 0
             DISABLE(?Group_MID)
          ELSE
             ENABLE(?Group_MID)
          .
      
          DO Adjust_Info
    OF ?CallLookup_Manifest
      ThisWindow.Update()
      MAN:MID = INT:MID
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        INT:MID = MAN:MID
      END
      ThisWindow.Reset(1)
          DO MID_Chosen
    OF ?INT:MID
      IF NOT QuickWindow{PROP:AcceptAll}
        IF INT:MID OR ?INT:MID{PROP:Req}
          MAN:MID = INT:MID
          IF Access:Manifest.TryFetch(MAN:PKey_MID)
            IF SELF.Run(3,SelectRecord) = RequestCompleted
              INT:MID = MAN:MID
            ELSE
              SELECT(?INT:MID)
              CYCLE
            END
          END
        END
      END
      ThisWindow.Reset()
          DO MID_Chosen
    OF ?Button_Change_Transp
      ThisWindow.Update()
          IF SELF.Request = ChangeRecord
             ENABLE(?Group_Transporter)
          .
    OF ?Button_Jump_to_Adj_Invoice
      ThisWindow.Update()
      START(Start_Trans_Invoice, 25000, CLIP(INT:CR_TIN))
      ThisWindow.Reset
    OF ?Button_Jump_to_Manifest
      ThisWindow.Update()
      START(Start_Manifest, 25000, CLIP(INT:MID))
      ThisWindow.Reset
    OF ?CallLookup_Branch
      ThisWindow.Update()
      BRA:BranchName = BRA:BranchName
      IF SELF.Run(4,SelectRecord) = RequestCompleted
        BRA:BranchName = BRA:BranchName
        INT:BID = BRA:BID
      END
      ThisWindow.Reset(1)
    OF ?BRA:BranchName
      IF BRA:BranchName OR ?BRA:BranchName{PROP:Req}
        BRA:BranchName = BRA:BranchName
        IF Access:Branches.TryFetch(BRA:Key_BranchName)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            BRA:BranchName = BRA:BranchName
            INT:BID = BRA:BID
          ELSE
            CLEAR(INT:BID)
            SELECT(?BRA:BranchName)
            CYCLE
          END
        ELSE
          INT:BID = BRA:BID
        END
      END
      ThisWindow.Reset()
    OF ?INT:Cost
          DO Calcs
          DO Adjust_Info
    OF ?INT:VAT
          IF INT:VAT_Specified = TRUE
             INT:VATRate     = ((INT:Cost + INT:VAT) / INT:Cost - 1) * 100           ! Calc new VAT rate
      
             DO Calcs
          .
    OF ?INT:VATRate
          DO Calcs
    OF ?Button_Change_VAT
      ThisWindow.Update()
          IF ?Button_Change_VAT{PROP:Text} = 'Change'
             INT:VAT_Specified                = TRUE
      
             ?Button_Change_VAT{PROP:Text}    = 'Auto'
      
             ?INT:VAT{PROP:Background}        = -1
             ?INT:VAT{PROP:ReadOnly}          = FALSE
             ?INT:VAT{PROP:Skip}              = FALSE
      
             DISABLE(?INT:VATRate)
          ELSE
             INT:VAT_Specified                = FALSE
      
             ?Button_Change_VAT{PROP:Text}    = 'Change'
      
             ?INT:VAT{PROP:Background}        = 0E9E9E9H
             ?INT:VAT{PROP:ReadOnly}          = TRUE
             ?INT:VAT{PROP:Skip}              = TRUE
      
             ENABLE(?INT:VATRate)
      
             POST(EVENT:Accepted, ?INT:Cost)              ! Re-calc
          .
    OF ?Button_ChangeTotal
      ThisWindow.Update()
          IF ?Button_ChangeTotal{PROP:Text} = 'Change'
             ?L_SG:Total{PROP:Background}         = -1
             ?L_SG:Total{PROP:ReadOnly}           = FALSE
             ?L_SG:Total{PROP:Skip}               = FALSE
          .
    OF ?INT:Manifest
          DO Check_Flags
    OF ?INT:ExtraInv
          DO Check_Flags
    OF ?L_SG:Total
          IF ?L_SG:Total{PROP:ReadOnly} = FALSE
             ! Calculate the VAT & Total
             INT:Cost                             = L_SG:Total / (1 + (INT:VATRate / 100))
      
             DISPLAY(?INT:Cost)
      
             ?L_SG:Total{PROP:Background}         = 0E9E9E9H
             ?L_SG:Total{PROP:ReadOnly}           = TRUE
             ?L_SG:Total{PROP:Skip}               = TRUE
      
             POST(EVENT:Accepted, ?INT:Cost)              ! Re-calc
          .
    OF ?Calendar
      ThisWindow.Update()
      Calendar12.SelectOnClose = True
      Calendar12.Ask('Select a Date',INT:InvoiceDate)
      IF Calendar12.Response = RequestCompleted THEN
      INT:InvoiceDate=Calendar12.SelectedDate
      DISPLAY(?INT:InvoiceDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:CINDateReceived
      ThisWindow.Update()
      Calendar15.SelectOnClose = True
      Calendar15.Ask('Select a Date',INT:CIN_DateReceived)
      IF Calendar15.Response = RequestCompleted THEN
      INT:CIN_DateReceived=Calendar15.SelectedDate
      DISPLAY(?INT:CIN_DateReceived)
      END
      ThisWindow.Reset(True)
    OF ?Button_ChangeInvDate
      ThisWindow.Update()
          ?INT:InvoiceDate{PROP:ReadOnly}     = FALSE
          ?INT:InvoiceDate{PROP:Skip}         = FALSE
          ?INT:InvoiceDate{PROP:BackGround}   = -1
      
          ENABLE(?Calendar)
          DISPLAY
    OF ?Button_Update
      ThisWindow.Update()
      LOC:Status = Upd_InvoiceTransporter_Paid_Status(INT:TIN)
      ThisWindow.Reset
          IF LOC:Status ~= INT:Status
             INT:Status   = LOC:Status
      
             MESSAGE('The status has been updated.', 'Invoice Status', ICON:Asterisk)
             DISPLAY
          .
    OF ?L_SG:Extra_Leg
          IF L_SG:Extra_Leg = TRUE
             IF INT:ExtraInv = TRUE OR INT:Manifest = TRUE
                L_SG:Extra_Leg    = FALSE
                MESSAGE('You cannot have an Extra Leg when this Invoice is:||1. an Extra Invoice (additional to an existing Invoice)|| or ||2. the Manifest invoice.', 'Extra Leg Validation', ICON:Hand)
             ELSE
                !
             .
          ELSE
             IF INT:DID ~= 0 OR INT:DINo ~= 0
                CASE MESSAGE('Are you sure you want to cancel Extra Leg details on this Invoice?||This will clear all Extra Leg info such as the DI No.', 'Extra Leg Info', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                OF BUTTON:Yes
                   CLEAR(INT:DeliveryGroup)
                OF BUTTON:No
                   L_SG:Extra_Leg = TRUE
                .
             ELSE
                CLEAR(INT:DeliveryGroup)
          .  .
      
      
          IF L_SG:Extra_Leg = TRUE
             ENABLE(?Tab_ExtraLeg)
          ELSE
             DISABLE(?Tab_ExtraLeg)
          .
      
      
          DISPLAY
    OF ?Button_ChooseDI
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_Invoice(,,, INT:MID)
      ThisWindow.Reset
          ! Set values
          IF INV:IID ~= 0
             INT:DID              = INV:DID
             INT:DINo             = INV:DINo
             !INT:DLID             =
             !INT:Leg              =
             !INT:CollectionAID    =
             !INT:DeliveryAID      =
          .
      
          DISPLAY
    OF ?OK
      ThisWindow.Update()
          IF L_SG:Extra_Leg = FALSE
             IF INT:DID ~= 0 OR INT:DINo ~= 0
                CASE MESSAGE('Are you sure you want to cancel Extra Leg details on this Invoice?||This will clear all Extra Leg info such as the DI No.', 'Extra Leg Info', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                OF BUTTON:Yes
                   CLEAR(INT:DeliveryGroup)
                OF BUTTON:No
                   L_SG:Extra_Leg = TRUE
                   ENABLE(?Tab_ExtraLeg)
      
                   QuickWindow{PROP:AcceptAll}  = FALSE
                   SELECT(?INT:InvoiceDate)
                   CYCLE
                .
             ELSE
                CLEAR(INT:DeliveryGroup)
             .
          ELSE
             IF INT:DID = 0 OR INT:DINo = 0
                CASE MESSAGE('The Extra Leg option is checked but there is no DI specified for the Extra Leg.||Without a DI specified this invoice will not be clasified as an extra leg invoice.||Would you like to choose a DI or cancel the Extra Leg option?', 'Extra Leg Info', ICON:Question, 'Choose DI|Cancel', 1)
                OF 1
                   QuickWindow{PROP:AcceptAll}  = FALSE
                   SELECT(?INT:DINo)
                   CYCLE
          .  .  .
      
      
          IF L_SG:Extra_Leg = TRUE
             !
          ELSE
             DISABLE(?Tab_ExtraLeg)
          .
          IF INT:CR_TIN ~= 0
             IF INT:Cost > 0.0
                IF INT:ExtraInv ~= TRUE
                   CASE MESSAGE('This is an Invoice Adjustment, would you like the Extra Invoice option checked on?  (recommended)', 'Adjustment', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                   OF BUTTON:Yes
                      INT:ExtraInv    = TRUE
          .  .  .  .
          IF SELF.Request = InsertRecord
             IF INT:InvoiceDate = 0
                CASE MESSAGE('There is no Date specified for this Invoice.||Would you like to set the Invoice date to today?', 'Invoice Date Required', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                OF BUTTON:No
                   QuickWindow{PROP:AcceptAll}  = FALSE
                   SELECT(?INT:InvoiceDate)
                   CYCLE
                ELSE
                   INT:InvoiceDate    = TODAY()
             .  .
      
             IF INT:TID = 0
                MESSAGE('Please select a Transporter for this Invoice.', 'Transporter Required', ICON:Hand)
                QuickWindow{PROP:AcceptAll}  = FALSE
                SELECT(?L_SG:TransporterName)
                CYCLE
             .
      
      
         !    IF INT:BID = 0
         !    .
      
      
             IF INT:MID = 0
                CASE MESSAGE('No Manifest has been associated with this Invoice, would you like to associate one now?', 'Manifest', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                OF BUTTON:Yes
                   QuickWindow{PROP:AcceptAll}  = FALSE
                   SELECT(?INT:MID)
                   CYCLE
             .  .
          ELSE
             IF INT:InvoiceDate = 0
                MESSAGE('Warning!||No Date is specified for this Invoice.', 'Invoice Date Required', ICON:Hand)
          .  .
      
          ! Check that the Credit is not greater than the Invoice selected
          IF INT:Cost < 0.0 AND INT:CR_TIN ~= 0
             ! p:Option
             !   0.  Invoice Paid
             !   1.  Clients Payments Allocation total
             L_SG:Total_Payments  = Get_TransportersPay_Alloc_Amt(INT:CR_TIN, 0)
      
             ! p:Options
             !   0.  All
             !   1.  Credits
             !   2.  Debits
             ! p:TIN_to_Ignore
             L_SG:Total_Credits   = -Get_InvTransporter_Credited(INT:CR_TIN, 1, INT:TIN)      ! Change to positive value
      
             A_INT:TIN      = INT:CR_TIN
             IF Access:InvoiceTransporterAlias.TryFetch(A_INT:PKey_TIN) ~= LEVEL:Benign
             ELSE
                IF -INT:Cost > (A_INT:Cost + A_INT:VAT - (L_SG:Total_Payments + L_SG:Total_Credits))
                   IF (L_SG:Total_Payments + L_SG:Total_Credits) > 0.0
                      UNHIDE(?Prompt_Action)
                      ?Prompt_Action{PROP:Text}      = 'Total Payments: ' & LEFT(CLIP(FORMAT(L_SG:Total_Payments,@n12.2))) & '<13,10>Total Credits: ' & LEFT(CLIP(FORMAT(L_SG:Total_Credits,@n12.2)))
                      ?Prompt_Action{PROP:FontColor} = COLOR:Red
                   .
      
                   MESSAGE('The specified Credit amount of ' & INT:Cost & ' is greater than the cost outstanding owing on the Invoice being credited which is ' & LEFT(CLIP(FORMAT(A_INT:Cost + A_INT:VAT - (L_SG:Total_Payments + L_SG:Total_Credits),@n12.2))) & '.||Please reduce the Credit amount.', 'Invoice Credit', ICON:Hand)
                   QuickWindow{PROP:AcceptAll}    = FALSE
                   SELECT(?INT:Cost)
                   CYCLE
          .  .  .
      
          IF INT:Cost < 0.0
             IF INT:CR_TIN = 0
                CASE MESSAGE('The cost specified is less than zero but no Credited Invoice no. has been selected.||Would you like to select one now?', 'Credit Invoice No.', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                OF BUTTON:Yes
                   QuickWindow{PROP:AcceptAll}  = FALSE
                   SELECT(?INT:CR_TIN)
                   CYCLE
          .  .  .
      
      
          INT:InvoiceTime     = CLOCK()
          IF Local_Class.Check_Manifest() < 0
             QuickWindow{PROP:AcceptAll}  = FALSE
             SELECT(?INT:MID)
             CYCLE
          .
          IF INT:CIN ~= 0 AND INT:CIN_DateReceived = 0
             MESSAGE('You have Creditor Invoice details specified, please specify a Date that these were received.', 'Creditor Invoice', ICON:Exclamation)
      
             QuickWindow{PROP:AcceptAll}  = FALSE
             SELECT(?INT:CIN_DateReceived)
             CYCLE
          .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
          INT:StatusUpToDate  = FALSE
          DO Check_Flags
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeCompleted()
      ! Check completed
      IF ReturnValue = LEVEL:Benign AND SELF.Request = ChangeRecord
         IF INT:TID ~= L_OD:TID
            ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
            Add_Log('Transporter changed - Previous TID: ' & L_OD:TID & ',  new TID: ' & INT:TID, 'Update_Transporter', 'Transporter Inv', FALSE)
      .  .
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF p:Pos > 0
             ! Change window position a little from default
             QuickWindow{PROP:XPos}   = QuickWindow{PROP:XPos} + 10
             QuickWindow{PROP:YPos}   = QuickWindow{PROP:YPos} + 10
          .
          IF INT:CR_TIN ~= 0
             IF L_SG:Extra_Leg = TRUE
                MESSAGE('This Invoice is an adjustment to another invoice, but it is also marked as an Extra Leg Invoice.||Please check that this is correct.', 'Adjustment & Extra Leg', ICON:Hand)
             ELSE
                DISABLE(?L_SG:Extra_Leg)
          .  .
          IF INT:VAT_Specified = FALSE
             ?Button_Change_VAT{PROP:Text}    = 'Change'
      
             ?INT:VAT{PROP:Background}        = 0E9E9E9H
             ?INT:VAT{PROP:ReadOnly}          = TRUE
             ?INT:VAT{PROP:Skip}              = TRUE
      
             ENABLE(?INT:VATRate)
          ELSE
             ?Button_Change_VAT{PROP:Text}    = 'Auto'
      
             ?INT:VAT{PROP:Background}        = -1
             ?INT:VAT{PROP:ReadOnly}          = FALSE
             ?INT:VAT{PROP:Skip}              = FALSE
      
             DISABLE(?INT:VATRate)
          .
          ! Set MID from this Invoice then
          IF INT:CR_TIN ~= 0
             DISABLE(?Group_MID)
          ELSE
             ENABLE(?Group_MID)
          .
      
          DO Calcs
          DO Adjust_Info
          IF p:Adjustment = 1
             UNHIDE(?Prompt_Action)
          .
          BRA:BID     = INT:BID
          IF Access:Branches.TryFetch(BRA:PKey_BID) ~= LEVEL:Benign
             CLEAR(BRA:Record)
          .
          ! (p:Info, p:Value, p:UID)
          ! (BYTE, *STRING, <ULONG>),LONG
          Get_User_Info(1, L_SG:Login, INT:UID)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local_Class.Check_Manifest          PROCEDURE()
L_Found     BYTE
L_Return    LONG
    CODE
    IF INT:MID = 0
       IF L_OD:MID > 0 AND INT:MID = 0
          ! We had a MID and now dont... check?
       .
    ELSE
       IF INT:Manifest ~= L_OD:Manifest            ! Status has changed... run checks
          ! Apply rule, there cannot be 2 MID records with this flag, the collery (sp?) there must be one?

          ! Check how many there are other than the current record
          CLEAR(A_INT:Record,-1)
          A_INT:MID        = INT:MID
          A_INT:Manifest   = TRUE
          SET(A_INT:SKey_MID_Manifest,A_INT:SKey_MID_Manifest)
          LOOP
             IF Access:InvoiceTransporterAlias.TryNext() ~= LEVEL:Benign
                BREAK
             .
             IF A_INT:MID ~= INT:MID OR A_INT:Manifest ~= TRUE
                BREAK
             .
             IF L_Found = TRUE
                ! We have another
                L_Found = 2
                BREAK
             .

             IF INT:TIN ~= A_INT:TIN            ! Exclude the current record
                L_Found    = TRUE
          .  .

          ! Act on information
          IF INT:Manifest = TRUE
             IF L_Found > 0
                L_Return   = -1
                MESSAGE('There is another Transporter Invoice which is marked as the Manifest invoice.||You can only have 1 Manifest invoice, un-mark the other Invoice then re-mark this Invoice if this is the correct Manifest invoice.||Manifest Invoice TIN: ' & A_INT:TIN, 'Validation', ICON:Hand)
             .
          ELSE
             IF L_Found <= 0
                MESSAGE('There is NO Transporter Invoice which is marked as the Manifest invoice.||You should have 1 Manifest invoice for each Manifest.', 'Validation', ICON:Exclamation)
    .  .  .  .

    RETURN(L_Return)

BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END


BRW13.SetQueueRecord PROCEDURE

  CODE
       ! No Payments, Partially Paid, Credit Note, Fully Paid
       EXECUTE A_INT:Status + 1
          L_SQ:Alias_Invoice_Status     = 'No Payments'
          L_SQ:Alias_Invoice_Status     = 'Partially Paid'
          L_SQ:Alias_Invoice_Status     = 'Credit Note'
          L_SQ:Alias_Invoice_Status     = 'Fully Paid'
       .
  PARENT.SetQueueRecord
  
  IF (A_INT:ExtraInv = 1)
    SELF.Q.A_INT:ExtraInv_Icon = 2                         ! Set icon from icon list
  ELSE
    SELF.Q.A_INT:ExtraInv_Icon = 1                         ! Set icon from icon list
  END


BRW13.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW13::LastSortOrder <> NewOrder THEN
     BRW13::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW13::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW13.TakeNewSelection PROCEDURE

  CODE
  IF BRW13::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW13::PopupTextExt = ''
        BRW13::PopupChoiceExec = True
        BRW13::FormatManager.MakePopup(BRW13::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW13::PopupTextExt = '|-|' & CLIP(BRW13::PopupTextExt)
        END
        BRW13::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW13::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW13::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW13::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW13::PopupChoiceOn AND BRW13::PopupChoiceExec THEN
     BRW13::PopupChoiceExec = False
     BRW13::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW13::PopupTextExt)
     IF BRW13::FormatManager.DispatchChoice(BRW13::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Start_Manifest       PROCEDURE  (p:MID)                    ! Declare Procedure
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Manifest.Open()
     .
     Access:Manifest.UseFile()
    ! (p:MID)
    MAN:MID             = p:MID
    IF Access:Manifest.Fetch(MAN:PKey_MID) = LEVEL:Benign
       GlobalRequest    = ChangeRecord
       Update_Manifest()
    ELSE

    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:Manifest.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Start_Trans_Invoice  PROCEDURE  (p:TIN)                    ! Declare Procedure
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_InvoiceTransporter.Open()
     .
     Access:_InvoiceTransporter.UseFile()
    ! (p:TIN)
    INT:TIN             = p:TIN
    IF Access:_InvoiceTransporter.Fetch(INT:PKey_TIN) = LEVEL:Benign
       GlobalRequest    = ChangeRecord
       Update_TransporterInvoice(,,1)
    ELSE

    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:_InvoiceTransporter.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_VehicleComposition PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:Capacity         ULONG                                 !In Kgs
LOC:TID              ULONG                                 !Transporter ID
LOC:TID_Tra          ULONG                                 !Transporter ID
LOC:Show_Archived    BYTE                                  !
LOC:Archive_Warning_Show BYTE                              !
BRW1::View:Browse    VIEW(VehicleComposition)
                       PROJECT(VCO:CompositionName)
                       PROJECT(VCO:Archived)
                       PROJECT(VCO:TID)
                       PROJECT(VCO:VCID)
                       PROJECT(VCO:TTID0)
                       JOIN(TRU:PKey_TTID,VCO:TTID0)
                         PROJECT(TRU:Registration)
                         PROJECT(TRU:TTID)
                       END
                       JOIN(TRA:PKey_TID,VCO:TID)
                         PROJECT(TRA:TransporterName)
                         PROJECT(TRA:TID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
VCO:CompositionName    LIKE(VCO:CompositionName)      !List box control field - type derived from field
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
TRU:Registration       LIKE(TRU:Registration)         !List box control field - type derived from field
LOC:Capacity           LIKE(LOC:Capacity)             !List box control field - type derived from local data
VCO:Archived           LIKE(VCO:Archived)             !List box control field - type derived from field
VCO:Archived_Icon      LONG                           !Entry's icon ID
VCO:TID                LIKE(VCO:TID)                  !List box control field - type derived from field
VCO:VCID               LIKE(VCO:VCID)                 !List box control field - type derived from field
VCO:TTID0              LIKE(VCO:TTID0)                !Browse key field - type derived from field
TRU:TTID               LIKE(TRU:TTID)                 !Related join file key field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Vehicle Composition File'),AT(,,340,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MAX,MDI,HLP('BrowseVehicleComposition'),SYSTEM
                       LIST,AT(8,30,323,124),USE(?Browse:1),HVSCROLL,ALRT(F2Key),FORMAT('80L(2)|M~Combination ' & |
  'Name~@s35@66L(2)|M~Transporter~C(0)@s35@66L(2)|M~Horse Registration~C(0)@s20@36R(2)|' & |
  'M~Capacity~C(0)@n13@34R(2)|MI~Archived~C(0)@p p@30R(2)|M~TID~C(0)@n_10@30R(2)|M~VCID' & |
  '~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the VehicleComposition file')
                       BUTTON('&Select'),AT(74,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(126,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(178,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(230,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(282,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,332,172),USE(?CurrentTab)
                         TAB('&1) By Name'),USE(?Tab:3)
                         END
                         TAB('&2) By Transporter'),USE(?Tab:2)
                           BUTTON('Select Transporter'),AT(9,158,62,16),USE(?SelectTransporter),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&3) By Truck'),USE(?Tab:4)
                         END
                         TAB('4) By VCID'),USE(?TAB1)
                         END
                       END
                       BUTTON('&Close'),AT(287,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       CHECK(' Show Archived'),AT(178,184),USE(LOC:Show_Archived),TIP('Check to show Archived.' & |
  '<0DH,0AH,0DH,0AH>F2 to toggle Archive on selected.')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 4
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW1::Sort2:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

Archive_Toggle                  ROUTINE
DATA
R:Yes   BYTE

CODE  
  BRW1.UpdateViewRecord()
  BRW1.UpdateBuffer()
  
  ! Toggle archived on highlighted...
  IF LOC:Archive_Warning_Show = 0
     IF VCO:Archived = FALSE
        CASE MESSAGE('Archive this Vehicle?','Archive Vehicles',ICON:Question, 'Yes|Yes (don''t warn again)|No', 3)
        OF 1
          R:Yes = TRUE
        OF 2    
          R:Yes = TRUE          
          LOC:Archive_Warning_Show  = 1
        .
      ELSE
        CASE MESSAGE('Un-Archive this Vehicle?','Archive Vehicles',ICON:Question, 'Yes|Yes (don''t warn again)|No', 3)
        OF 1
          R:Yes = TRUE
        OF 2    
          R:Yes = TRUE          
          LOC:Archive_Warning_Show  = 1
        .        
      .
  ELSE
    R:Yes = TRUE
  .
  
  IF R:Yes = TRUE    
    BRW1.UpdateViewRecord()
    
    BRW1.UpdateBuffer()
    IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = Level:Benign
      IF VCO:Archived  = 1
        VCO:Archived = 0
      ELSE
        VCO:Archived = 1
        VCO:Archived_Date = TODAY()
        VCO:Archived_Time = CLOCK()
      .          
      IF Access:VehicleComposition.TryUpdate() = Level:Benign
        BRW1.ResetFromBuffer()  
  . . .    

  EXIT
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_VehicleComposition')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Show_Archived',LOC:Show_Archived)     ! Added by: BrowseBox(ABC)
  BIND('LOC:Capacity',LOC:Capacity)               ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                         ! File Transporter used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:VehicleComposition,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon VCO:TID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,VCO:FKey_TID) ! Add the sort order for VCO:FKey_TID for sort order 1
  BRW1.AddRange(VCO:TID,Relate:VehicleComposition,Relate:Transporter) ! Add file relationship range limit for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)            ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,VCO:TID,1,BRW1)       ! Initialize the browse locator using  using key: VCO:FKey_TID , VCO:TID
  BRW1.AppendOrder('+VCO:CompositionName')        ! Append an additional sort order
  BRW1.SetFilter('(LOC:Show_Archived = 1 OR VCO:Archived = 0)') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Show_Archived)           ! Apply the reset field
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon VCO:TTID0 for sort order 2
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,VCO:FKey_TID0) ! Add the sort order for VCO:FKey_TID0 for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,VCO:TTID0,1,BRW1)     ! Initialize the browse locator using  using key: VCO:FKey_TID0 , VCO:TTID0
  BRW1.SetFilter('(LOC:Show_Archived = 1 OR VCO:Archived = 0)') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Show_Archived)           ! Apply the reset field
  BRW1.AddSortOrder(,VCO:PKey_VCID)               ! Add the sort order for VCO:PKey_VCID for sort order 3
  BRW1.AddLocator(BRW1::Sort3:Locator)            ! Browse has a locator for sort order 3
  BRW1::Sort3:Locator.Init(,VCO:VCID,1,BRW1)      ! Initialize the browse locator using  using key: VCO:PKey_VCID , VCO:VCID
  BRW1.SetFilter('(LOC:Show_Archived = 1 OR VCO:Archived = 0)') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Show_Archived)           ! Apply the reset field
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon VCO:CompositionName for sort order 4
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,VCO:Key_Name) ! Add the sort order for VCO:Key_Name for sort order 4
  BRW1.SetFilter('(LOC:Show_Archived = 1 OR VCO:Archived = 0)') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:Show_Archived)           ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(VCO:CompositionName,BRW1.Q.VCO:CompositionName) ! Field VCO:CompositionName is a hot field or requires assignment from browse
  BRW1.AddField(TRA:TransporterName,BRW1.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW1.AddField(TRU:Registration,BRW1.Q.TRU:Registration) ! Field TRU:Registration is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Capacity,BRW1.Q.LOC:Capacity) ! Field LOC:Capacity is a hot field or requires assignment from browse
  BRW1.AddField(VCO:Archived,BRW1.Q.VCO:Archived) ! Field VCO:Archived is a hot field or requires assignment from browse
  BRW1.AddField(VCO:TID,BRW1.Q.VCO:TID)           ! Field VCO:TID is a hot field or requires assignment from browse
  BRW1.AddField(VCO:VCID,BRW1.Q.VCO:VCID)         ! Field VCO:VCID is a hot field or requires assignment from browse
  BRW1.AddField(VCO:TTID0,BRW1.Q.VCO:TTID0)       ! Field VCO:TTID0 is a hot field or requires assignment from browse
  BRW1.AddField(TRU:TTID,BRW1.Q.TRU:TTID)         ! Field TRU:TTID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:TID,BRW1.Q.TRA:TID)           ! Field TRA:TID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_VehicleComposition',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_VehicleComposition',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,8,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Transporter.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_VehicleComposition',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
      IF LOC:TID ~= 0
         TRA:TID      = LOC:TID
         IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
      .  .
  
      LOC:TID_Tra  = TRA:TID


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_VehicleComposition(LOC:TID_Tra)
    ReturnValue = GlobalResponse
  END
     IF CHOICE(?CurrentTab) = 1
        IF ReturnValue = RequestCompleted
           ! Get the transporter for this added entry
           TRA:TID  = VCO:TID
           LOC:TID  = VCO:TID
     .  .
  
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectTransporter
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_Transporter()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      IF KEYCODE() = F2Key
        DO Archive_Toggle
      .
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      LOC:Capacity    = Get_VehComp_Info( VCO:VCID, 1 )
  PARENT.SetQueueRecord
  
  IF (VCO:Archived = 1)
    SELF.Q.VCO:Archived_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.VCO:Archived_Icon = 1                           ! Set icon from icon list
  END


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>3,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>3,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

