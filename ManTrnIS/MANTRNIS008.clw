

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS008.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_TripSheets PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:TripSheetState   BYTE                                  !State of this Trip Sheet
LOC:List_State       STRING(20)                            !State of this Trip Sheet
LOC:BranchID         ULONG                                 !Branch ID
LOC:BranchName       STRING(35)                            !Branch Name
LOC:Sel_TRID         ULONG                                 !Tripsheet ID
LOC:Locator          ULONG                                 !
BRW1::View:Browse    VIEW(TripSheets)
                       PROJECT(TRI:TRID)
                       PROJECT(TRI:DepartDate)
                       PROJECT(TRI:DepartTime)
                       PROJECT(TRI:ReturnedDate)
                       PROJECT(TRI:ReturnedTime)
                       PROJECT(TRI:Notes)
                       PROJECT(TRI:Collections)
                       PROJECT(TRI:VCID)
                       PROJECT(TRI:BID)
                       PROJECT(TRI:State)
                       JOIN(VCO:PKey_VCID,TRI:VCID)
                         PROJECT(VCO:CompositionName)
                         PROJECT(VCO:VCID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
TRI:TRID               LIKE(TRI:TRID)                 !List box control field - type derived from field
VCO:CompositionName    LIKE(VCO:CompositionName)      !List box control field - type derived from field
TRI:DepartDate         LIKE(TRI:DepartDate)           !List box control field - type derived from field
TRI:DepartTime         LIKE(TRI:DepartTime)           !List box control field - type derived from field
TRI:ReturnedDate       LIKE(TRI:ReturnedDate)         !List box control field - type derived from field
TRI:ReturnedTime       LIKE(TRI:ReturnedTime)         !List box control field - type derived from field
TRI:Notes              LIKE(TRI:Notes)                !List box control field - type derived from field
LOC:List_State         LIKE(LOC:List_State)           !List box control field - type derived from local data
TRI:Collections        LIKE(TRI:Collections)          !List box control field - type derived from field
TRI:Collections_Icon   LONG                           !Entry's icon ID
TRI:VCID               LIKE(TRI:VCID)                 !List box control field - type derived from field
TRI:BID                LIKE(TRI:BID)                  !List box control field - type derived from field
TRI:State              LIKE(TRI:State)                !Browse hot field - type derived from field
VCO:VCID               LIKE(VCO:VCID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Trip Sheets File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MAX,MDI,HLP('BrowseTripSheets'),SYSTEM
                       GROUP,AT(8,22,91,10),USE(?Group_Locate)
                         PROMPT('Locator:'),AT(8,22),USE(?LOC:Locator:Prompt),TRN
                         ENTRY(@n_10),AT(41,22,51,10),USE(LOC:Locator),RIGHT(1),FLAT
                       END
                       LIST,AT(8,35,341,119),USE(?Browse:1),HVSCROLL,FORMAT('42R(2)|M~Tripsheet ID~C(0)@N_10@8' & |
  '0L(2)|M~Vehicle Composition~@s35@[46R(2)|M~Date~C(0)@d6b@80R(2)|M~Time~C(0)@t7b@](81' & |
  ')|M~Depart~[46R(2)|M~Date~C(0)@d6b@36R(2)|M~Time~C(0)@t7b@](81)|M~Returned~100L(2)|M' & |
  '~Notes~@s255@60L(2)|M~Trip Sheet State~@s20@40L(2)|MI~Collections~@p p@40R(2)|M~VCID' & |
  '~L@n_10@30R(2)|M~BID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the TripSheets file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Branch && Tripsheet ID'),USE(?Tab:2)
                           STRING('Branch Name selected'),AT(257,22,95,10),USE(?String_Branch),RIGHT(1),TRN
                           BUTTON('Select Branch'),AT(8,158,,14),USE(?SelectBranches),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&2) By Branch && Departure'),USE(?Tab:3)
                           BUTTON('Select Branch'),AT(9,158,,14),USE(?SelectBranches:2),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&3) By Vehicle Compositions'),USE(?Tab3)
                           BUTTON('Vehicle Composition'),AT(9,158,,14),USE(?SelectVehicleComposition),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       GROUP,AT(4,184,131,10),USE(?Group_TripSheet)
                         PROMPT('Trip Sheet State:'),AT(4,184),USE(?LOC:TripSheetState:Prompt)
                         LIST,AT(62,184,69,10),USE(LOC:TripSheetState),VSCROLL,DROP(15),FROM('Loading|#0|Loaded|' & |
  '#1|On Route|#2|Transferred|#3|Finalised|#4|All|#255'),MSG('State of this Trip Sheet'),TIP('State of t' & |
  'his Trip Sheet')
                       END
                       BUTTON('Capture POD''s'),AT(164,180,80,14),USE(?Button_CapturePods),LEFT,ICON('WIZEDIT.ICO'), |
  FLAT,HIDE,TIP('Bulk capture POD''s')
                       BUTTON('&Print'),AT(248,180,,14),USE(?Button_Print),LEFT,ICON(ICON:Print1),FLAT,SKIP
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                     END

BRW1::LastSortOrder       BYTE
MDISyncro MDISynchronization
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_TripSheets')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Locator:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:TripSheetState',LOC:TripSheetState)   ! Added by: BrowseBox(ABC)
  BIND('LOC:BranchID',LOC:BranchID)               ! Added by: BrowseBox(ABC)
  BIND('LOC:List_State',LOC:List_State)           ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                            ! File Branches used by this procedure, so make sure it's RelationManager is open
  Access:VehicleComposition.UseFile               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TripSheets,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
      BRA:BID             = GLO:BranchID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         LOC:BranchID    = BRA:BID
         LOC:BranchName  = BRA:BranchName
  
         ?String_Branch{PROP:Text}   = 'Branch - ' & BRA:BranchName
      .
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon TRI:BID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,TRI:FKey_BID) ! Add the sort order for TRI:FKey_BID for sort order 1
  BRW1.AddRange(TRI:BID,Relate:TripSheets,Relate:Branches) ! Add file relationship range limit for sort order 1
  BRW1.AppendOrder('-TRI:DepartDate,+TRI:TRID')   ! Append an additional sort order
  BRW1.SetFilter('(LOC:TripSheetState = 255 OR LOC:TripSheetState = TRI:State)') ! Apply filter expression to browse
  BRW1.AddSortOrder(,TRI:FKey_VCID)               ! Add the sort order for TRI:FKey_VCID for sort order 2
  BRW1.AddRange(TRI:VCID,Relate:TripSheets,Relate:VehicleComposition) ! Add file relationship range limit for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,TRI:VCID,1,BRW1)      ! Initialize the browse locator using  using key: TRI:FKey_VCID , TRI:VCID
  BRW1.AppendOrder('+TRI:State,-TRI:DepartDate,+TRI:TRID') ! Append an additional sort order
  BRW1.SetFilter('(LOC:TripSheetState = 255 OR LOC:TripSheetState = TRI:State)') ! Apply filter expression to browse
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon TRI:TRID for sort order 3
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,TRI:PKey_TID) ! Add the sort order for TRI:PKey_TID for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(?LOC:Locator,TRI:TRID,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: TRI:PKey_TID , TRI:TRID
  BRW1.SetFilter('((LOC:TripSheetState = 255 OR LOC:TripSheetState = TRI:State) AND (LOC:BranchID = 0 OR TRI:BID = LOC:BranchID))') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:BranchID)                ! Apply the reset field
  BRW1.AddResetField(LOC:TripSheetState)          ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(TRI:TRID,BRW1.Q.TRI:TRID)         ! Field TRI:TRID is a hot field or requires assignment from browse
  BRW1.AddField(VCO:CompositionName,BRW1.Q.VCO:CompositionName) ! Field VCO:CompositionName is a hot field or requires assignment from browse
  BRW1.AddField(TRI:DepartDate,BRW1.Q.TRI:DepartDate) ! Field TRI:DepartDate is a hot field or requires assignment from browse
  BRW1.AddField(TRI:DepartTime,BRW1.Q.TRI:DepartTime) ! Field TRI:DepartTime is a hot field or requires assignment from browse
  BRW1.AddField(TRI:ReturnedDate,BRW1.Q.TRI:ReturnedDate) ! Field TRI:ReturnedDate is a hot field or requires assignment from browse
  BRW1.AddField(TRI:ReturnedTime,BRW1.Q.TRI:ReturnedTime) ! Field TRI:ReturnedTime is a hot field or requires assignment from browse
  BRW1.AddField(TRI:Notes,BRW1.Q.TRI:Notes)       ! Field TRI:Notes is a hot field or requires assignment from browse
  BRW1.AddField(LOC:List_State,BRW1.Q.LOC:List_State) ! Field LOC:List_State is a hot field or requires assignment from browse
  BRW1.AddField(TRI:Collections,BRW1.Q.TRI:Collections) ! Field TRI:Collections is a hot field or requires assignment from browse
  BRW1.AddField(TRI:VCID,BRW1.Q.TRI:VCID)         ! Field TRI:VCID is a hot field or requires assignment from browse
  BRW1.AddField(TRI:BID,BRW1.Q.TRI:BID)           ! Field TRI:BID is a hot field or requires assignment from browse
  BRW1.AddField(TRI:State,BRW1.Q.TRI:State)       ! Field TRI:State is a hot field or requires assignment from browse
  BRW1.AddField(VCO:VCID,BRW1.Q.VCO:VCID)         ! Field VCO:VCID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_TripSheets',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_TripSheets',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,12,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_TripSheets',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_TripSheet
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_CapturePods
      BRW1.UpdateViewRecord()
          CLEAR(LOC:Sel_TRID)
          IF TRI:TRID ~= 0
             CASE MESSAGE('Would you like to capture the selected Trip Sheets POD<39>s (TRID ' & TRI:TRID & ') or all outstanding POD<39>s?', 'Capture POD<39>s', ICON:Question, 'Selected|All', 1)
             OF 1
                LOC:Sel_TRID  = TRI:TRID
          .  .
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Branches()
      ThisWindow.Reset
          IF GlobalResponse = RequestCancelled
             LOC:BranchID    = 0
             LOC:BranchName  = '<None>'
          ELSE
             LOC:BranchID    = BRA:BID
             LOC:BranchName  = BRA:BranchName
          .
          ?String_Branch{PROP:Text}   = 'Branch - ' & LOC:BranchName
      
    OF ?SelectBranches:2
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Branches()
      ThisWindow.Reset
    OF ?SelectVehicleComposition
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_VehicleComposition()
      ThisWindow.Reset
    OF ?LOC:TripSheetState
          ! Loading|Loaded|On Route|Transferred|Finalised
          IF LOC:TripSheetState >= 2
             UNHIDE(?Button_CapturePods)
          ELSIF ?Button_CapturePods{PROP:Hide} = FALSE
             HIDE(?Button_CapturePods)
          .
    OF ?Button_CapturePods
      ThisWindow.Update()
      TripSheet_BulkPOD(LOC:Sel_TRID)
      ThisWindow.Reset
      BRW1.ResetFromFile()
    OF ?Button_Print
      ThisWindow.Update()
      BRW1.UpdateViewRecord()
            IF TRI:Collections = TRUE
               Print_TripSheet_Collections(TRI:TRID)
            ELSE
               EXECUTE POPUP('Print|Print With Preview')
                  Print_TripSheet_no_Preview(TRI:TRID)
                  Print_TripSheet(TRI:TRID)
            .  .
            
            ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF MDISyncro.TakeEvent() THEN CYCLE.
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LOC:TripSheetState
          ! Loading|Loaded|On Route|Transferred
          IF LOC:TripSheetState >= 2
             UNHIDE(?Button_CapturePods)
          ELSIF ?Button_CapturePods{PROP:Hide} = FALSE
             HIDE(?Button_CapturePods)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! Loading|Loaded|On Route|Transferred|Finalised
      EXECUTE TRI:State + 1
         LOC:List_State   = 'Loading'
         LOC:List_State   = 'Loaded'
         LOC:List_State   = 'On Route'
         LOC:List_State   = 'Transferred'
         LOC:List_State   = 'Finalised'
      ELSE
         LOC:List_State   = '<unknown>'
      .
  PARENT.SetQueueRecord
  
  IF (TRI:Collections = 1)
    SELF.Q.TRI:Collections_Icon = 2                        ! Set icon from icon list
  ELSE
    SELF.Q.TRI:Collections_Icon = 1                        ! Set icon from icon list
  END


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>2,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>2,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_TripSheet, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_TripSheet

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
TripSheet_Load_DI    PROCEDURE  (p:TRID, p:VCID, p:DINo, p:DIID, p:Quiet_Items) ! Declare Procedure
R:CanLoad            DECIMAL(6)                            !
R:Remain             DECIMAL(6)                            !
R:Needed             DECIMAL(6)                            !
R:Load               ULONG                                 !
L_DG:Release_State   LONG                                  !
L_SV:Loaded_Capacity DECIMAL(6)                            !
L_SV:Remaining_Capacity DECIMAL(6)                         !
L_SV:Capacity        DECIMAL(6)                            !
LOC:No_Loaded        LONG                                  !
LOC:DID              ULONG                                 !Delivery ID
LOC:Info_Group       GROUP,PRE(L_IG)                       !
DI_No_Items          LONG                                  !
Errors               LONG                                  !
Errors_None          LONG                                  !
                     END                                   !
Tek_Failed_File     STRING(100)

DelI_View       VIEW(DeliveryItems)
    .


DelI_Mgr        ViewManager
                    MAP
Load_DI_Items           PROCEDURE(BYTE pp_:Quiet_Items=1)
                    END

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetDeliveries.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Deliveries.Open()
     .
     Access:DeliveryItems.UseFile()
     Access:TripSheetDeliveries.UseFile()
     Access:TripSheetDeliveriesAlias.UseFile()
     Access:Deliveries.UseFile()
    ! (p:TRID, p:VCIqD, p:DINo, p:DIID, p:Quiet_Items)
    ! (ULONG, ULONG, ULONG, ULONG=0, BYTE=0)
    ! p:Quiet_Items - quiet on each item

    CLEAR(LOC:No_Loaded)

    DEL:DINo    = p:DINo
    IF Access:Deliveries.TryFetch(DEL:Key_DINo) ~= LEVEL:Benign
       MESSAGE('DI No. not found.||DI No.: ' & p:DINo, 'Load DI Items', ICON:Exclamation)
    ELSE
       LOC:DID  = DEL:DID

       ! Check Delivered
       IF DEL:Delivered > 1
          Upd_Delivery_Del_Status(LOC:DID)
          IF DEL:Delivered > 1
             IF Access:Deliveries.TryFetch(DEL:Key_DINo) ~= LEVEL:Benign
                !
             ELSE
                LOC:No_Loaded   = -2            ! Delivered already
                MESSAGE('The DI is already delivered.', 'Load DI Items', ICON:Exclamation)
       .  .  .

       ! Check Released
       IF LOC:No_Loaded >= 0
          L_DG:Release_State    = Check_Delivery_Release(, LOC:DID, Reason_")

          ! (p_Statement_Info, p:DID, p_Release_Status_Str)
          ! (<*STRING>, ULONG=0, <*STRING>),LONG
          !   Returns
          !   0   - None of the Below
          !   1   - Released
          !   2   - Acc. Pre-Paid
          !   3   - 60 Day+ Balance
          !   4   - Account Limit
          !   5   - Acc. On Hold
          !   6   - Acc. Closed
          !   7   - Acc. Dormant
          !
          !   8   - (Payment) Period exceeded

          IF L_DG:Release_State > 1          ! 0 ok or 1 released, > is needs releasing
             !MESSAGE('The DI of the selected item needs to be released please see the Release Status for the reason.', 'Load This Item', ICON:Exclamation)
             LOC:No_Loaded      = -3         ! Not released
!             MESSAGE('The DI needs to be released please see the Release Status for the reason.', 'Load DI Items', ICON:Exclamation)
             MESSAGE('The DI needs to be released please see below for the reason.||DI: ' & DEL:DINo & |
                    '|Release Status: ' & CLIP(Reason_") & ' ' & L_DG:Release_State, 'Load DI Items', ICON:Exclamation)
       .  .


       ! Load items
       IF LOC:No_Loaded >= 0
          Load_DI_Items()
            
          ! Some not loaded as over limit
          IF L_IG:Errors < 0
             IF LOC:No_Loaded > 0
                ! Some items loaded but not all
                IF p:Quiet_Items = TRUE
                    CASE MESSAGE('Not all Items loaded due to weight restrictions.|||Would you like to override loading per item?', |
                        'Load DI Items', ICON:Question, BUTTON:Yes+BUTTON:NO, BUTTON:YES)
                    OF BUTTON:YES    
                       Load_DI_Items(0) 
                .   .
             ELSE
                LOC:No_Loaded   = -1
                IF p:Quiet_Items = TRUE
                   CASE MESSAGE('No items on this DI (' & p:DINo & ') were loaded due to weight restrictions..|||Would you like to override loading per item?', |
                        'Load DI Items', ICON:Question, BUTTON:Yes+BUTTON:NO, BUTTON:YES)
                   OF BUTTON:YES
                      Load_DI_Items(0)
             .  .  .
          ELSIF L_IG:Errors = 0 AND LOC:No_Loaded = 0 AND L_IG:Errors_None > 0
             ! None loaded, all already loaded
             IF p:Quiet_Items = TRUE
                MESSAGE('All items on this DI (' & p:DINo & ')  are already loaded on Trip Sheet Deliveries.', 'Load DI Items', ICON:Exclamation)
       .  .  .

       Upd_Delivery_Del_Status(LOC:DID)
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:No_Loaded)

    Exit

CloseFiles     Routine
    Relate:DeliveryItems.Close()
    Relate:TripSheetDeliveries.Close()
    Relate:TripSheetDeliveriesAlias.Close()
    Relate:Deliveries.Close()
    Exit
Load_Item                       ROUTINE
    ! If previously loaded on this TRDI:TDID we need to add to that entry
    A_TRDI:TRID             = p:TRID
    A_TRDI:DIID             = DELI:DIID
    IF Access:TripSheetDeliveriesAlias.TryFetch(A_TRDI:CKey_TRID_DIID) = LEVEL:Benign
       MESSAGE('This Item, DIID ' & DELI:DIID & ' from DI No. ' & p:DINo & ', is already loaded on this Trip Sheet.||Please modify the loaded entry.', 'Trip Sheet Deliveries', ICON:Exclamation)
    ELSE
       ! Insert
       CLEAR(TRDI:Record)

       IF Access:TripSheetDeliveries.PrimeRecord() = LEVEL:Benign
          !TRDI:TDID
          TRDI:TRID         = p:TRID

          TRDI:UnitsLoaded  = R:Load
          TRDI:DIID         = DELI:DIID

          IF Access:TripSheetDeliveries.TryInsert() ~= LEVEL:Benign
             Access:TripSheetDeliveries.CancelAutoInc()
             CLEAR(TRDI:Record)
       .  .

       LOC:No_Loaded       += R:Load

       !MESSAGE('Units have been loaded.', 'Trip Sheet Deliveries', ICON:Asterisk)
    .
    EXIT
Check_Capacity              ROUTINE             ! Sets R:Needed
    ! Check we have capacity
    R:Needed         = 0.0
    
    IF (DELI:Weight / DELI:Units) = 0
       R:CanLoad     = DELI:Units                                             
    ELSE
       R:CanLoad     = L_SV:Remaining_Capacity / (DELI:Weight / DELI:Units)         ! Weight per unit divided into remaining capacity = units canload
    .

    R:Remain         = Get_TripDelItemUnDel_Units(DELI:DIID, 1)                     ! Only undelivered and not on incomplete trip, how many to be delivered still of this unit
    
    IF R:CanLoad > R:Remain
       R:Load        = R:Remain                                                     ! Set R:Load to total remaining or if lesss then how much will fit
    ELSE
       R:Load        = R:CanLoad
    .

    ! Due to rounding R:Load may be slightly higher than possible load..
    LOOP
       IF R:Load <= 0
          BREAK
       .
       R:Needed      = DELI:Weight * (R:Load / DELI:Units)                          ! Weight of all units / (to load units / all units),  1000 / (10/15) = 1000 * 66% = 666 kgs needed
                                                                                    ! Weight needed
       IF R:Needed > L_SV:Remaining_Capacity
          R:Load    -= 1                                                            ! Reduce if higher that actual capacity remaining
       ELSE
          BREAK
    .  .

    R:Needed         = DELI:Weight * (R:Load / DELI:Units)                          ! Needed weight is total weight for units * by % of units to load
    EXIT
!                                               old
!    p:DIID
!    L_CV:DID
!    TRDI:TRID           - need to load a record
!
!
!    L_SV:Loaded_Capacity       = Get_TripDelItems_Info(TRDI:TRID, 0) / 100
!    L_SV:Remaining_Capacity    = L_SV:Capacity - L_SV:Loaded_Capacity
!
!
!
!    L_DG:Release_State      = Check_Delivery_Release(, L_CV:DID)
!
!    IF L_DG:Release_State > 1          ! 0 ok or 1 released
!       !MESSAGE('The DI of the selected item needs to be released please see the Release Status for the reason.', 'Load This Item', ICON:Exclamation)
!       MESSAGE('The DI needs to be released please see the Release Status for the reason.', 'Load This Item', ICON:Exclamation)
!    ELSE
!       ! Check we have capacity
!       DELI:DIID           = p:DIID
!       IF Access:DeliveryItems.TryFetch(DELI:PKey_DIID) ~= LEVEL:Benign
!          MESSAGE('Failed to Fetch Delivery Item.||DIID: ' & p:DIID, 'Load Max Items', ICON:Asterisk)
!       ELSE
!          IF (DELI:Weight / DELI:Units) = 0
!             R:CanLoad     = DELI:Units
!          ELSE
!             R:CanLoad     = L_SV:Remaining_Capacity / (DELI:Weight / DELI:Units)
!          .
!
!          R:Remain         = Get_TripDelItemUnDel_Units(p:DIID, 1)     ! Only undelivered and not on incomplete trip
!          
!          IF R:CanLoad > R:Remain
!             R:Load        = R:Remain
!          ELSE
!             R:Load        = R:CanLoad
!          .
!
!          ! Due to rounding R:Load may be slightly higher than possible load..
!          LOOP
!             IF R:Load <= 0
!                BREAK
!             .
!             R:Needed      = DELI:Weight * (R:Load / DELI:Units)
!
!             IF R:Needed > L_SV:Remaining_Capacity
!                R:Load    -= 1
!             ELSE
!                BREAK
!          .  .
!
!          R:Needed         = DELI:Weight * (R:Load / DELI:Units)
!
!       db.debugout('[Update_TripSheetDeliveries_Load]  Needed: ' & R:Needed & ',  R:Load: ' & R:Load)
!
!          IF R:Needed <= L_SV:Remaining_Capacity AND R:Load > 0
!             ! Loop through the Delivery Items
!
!             ! If previously loaded on this TRDI:TDID we need to add to that entry
!             A_TRDI:TRID           = TRDI:TRID
!             A_TRDI:DIID           = DELI:DIID
!             IF Access:TripSheetDeliveriesAlias.TryFetch(A_TRDI:CKey_TRID_DIID) = LEVEL:Benign
!                IF TRDI:TDID ~= A_TRDI:TDID
!                   MESSAGE('This Item is already loaded on this Trip Sheet.||Please modify the loaded entry.', 'Trip Sheet Deliveries', ICON:Exclamation)
!                ELSE
!                   R:Loaded           = TRUE
!                   TRDI:UnitsLoaded  += R:Load
!                   TRDI:DIID          = DELI:DIID
!                   IF Access:TripSheetDeliveries.Update() = LEVEL:Benign
!                .  .
!             ELSE
!                R:Loaded              = TRUE
!                TRDI:UnitsLoaded     += R:Load
!                TRDI:DIID             = DELI:DIID
!                IF Access:TripSheetDeliveries.Update() = LEVEL:Benign
!             .  .
!
!             DO Remaining_Capacity
!
!             !L_SV:Reload_Deliveries += 1
!             !L_SV:Last_DIID_Set      = 0
!
!             Upd_Delivery_Del_Status(DELI:DID)
!
!!             IF R:Loaded = TRUE
!!                ! We prime the record always - because a cancel will delete it always
!!                IF Access:TripSheetDeliveries.PrimeRecord() ~= LEVEL:Benign .
!!                ThisWindow.PrimeFields()
!!
!!                CASE MESSAGE('Units of the selected item have been loaded.||Load some more?', 'Trip Sheet Deliveries', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
!!                OF BUTTON:Yes
!!                   BRW_Deliveries.ResetFromBuffer()
!!                   !BRW_Deliveries.PostNewSelection
!!!                   SELECT(?List_Deliveries, CHOICE(?List_Deliveries))
!!                   FDB11.ResetQueue(1)
!!                   SELECT(?L_IG:Item_Desc,1)
!!                OF BUTTON:No
!!                   POST(EVENT:Accepted, ?Cancel)
!!             .  .
!          ELSE
!             IF R:Load <= 0
!                MESSAGE('No Items loaded, 1 unit of this Delivery Item would be overweight.', 'Load Max Items', ICON:Exclamation)
!             ELSE
!                MESSAGE('No Items loaded - needed: ' & R:Needed & '||for ' & R:Load & ' units.', 'Load Max Items', ICON:Exclamation)
!    .  .  .  .
Load_DI_Items       PROCEDURE(BYTE pp_:Quiet_Items=1)
    CODE
    ! Get_VehComp_Info        
    ! (p:VCID, p:Option, p:Truck, p:Additional)
    ! (ULONG, BYTE, BYTE=0, BYTE=0),STRING
    ! p:Option  1.  Capacity                            of all
    !           2.  Registrations                       of all
    !           3.  Makes & Models                      of all
    !           4.  Description of Truck / Trailer      requires p:Truck  0 to 3
    !           5.  Truck / Trailer Capacity            requires p:Truck  0 to 3
    !           6.  Truck / Trailer ID (TTID)           requires p:Truck  0 to 3
    !           7.  Count of vehicles in composition
    !           8.  Compositions Capacity
    !           9.  Driver of Horse
    !           10. Transporters ID for this VCID
    !           11. Licensing dates                     of all
    !
    ! p:Truck
    !           - Truck no. in combination, 1 to 4
    ! p:Additional
    !           - Specifies next option to be called, for cache

    L_SV:Capacity            = Get_VehComp_Info(p:VCID, 1)
    L_SV:Loaded_Capacity     = Get_TripDelItems_Info(p:TRID, 0) / 100
    L_SV:Remaining_Capacity  = L_SV:Capacity - L_SV:Loaded_Capacity

    ! Loop through items on DI and load them
    DelI_Mgr.Init(DelI_View, Relate:DeliveryItems)
    DelI_Mgr.AddSortOrder(DELI:FKey_DID_ItemNo)
    DelI_Mgr.AddRange(DELI:DID, LOC:DID)
    !DelI_Mgr.SetFilter()

    DelI_Mgr.Reset()
    LOOP
       IF DelI_Mgr.Next() ~= LEVEL:Benign
          BREAK
       .
       IF p:DIID ~= 0 AND p:DIID ~= DELI:DIID
          CYCLE
       .
       L_IG:DI_No_Items   += 1

       DO Check_Capacity
       db.debugout('[TripSheet_Load_DI]  Needed: ' & R:Needed & ',  R:Load: ' & R:Load & ',  L_SV:Capacity: ' & L_SV:Capacity & |
                         ',  L_SV:Loaded_Capacity: ' & L_SV:Loaded_Capacity & ',  L_SV:Remaining_Capacity: ' & L_SV:Remaining_Capacity & |
                         ',  R:Remain (to load): ' & R:Remain)

       IF R:Needed <= L_SV:Remaining_Capacity AND R:Load > 0
          DO Load_Item
       ELSE
          IF R:Remain <= 0
             L_IG:Errors_None += 1
             IF p:Quiet_Items = FALSE
                MESSAGE('There are no units left to load for this item on DI No. ' & p:DINo & ', Item No. ' & DELI:ItemNo & ' (DIID ' & DELI:DIID & ').', 'Load DI Items', ICON:Exclamation)
             .
          ELSE
             L_IG:Errors      -= 1

             IF p:Quiet_Items = FALSE
                IF R:Load <= 0
                   CASE MESSAGE('Not all Items loaded, 1 unit of this Delivery Item No. ' & DELI:ItemNo & ' (DIID ' & DELI:DIID & ') would be overweight (loaded wieght is ' & |
                      FORMAT(L_SV:Loaded_Capacity,'@n10.1') & ', item weight is ' & FORMAT(DELI:Weight / DELI:Units,'@n10.1') & ').' & |
                      '|||Would you like to load the remaining ' & R:Remain & ' units anyway?', 'Load DI Items', ICON:Question, BUTTON:Yes+BUTTON:NO,BUTTON:NO)
                   OF BUTTON:YES
                      R:Load    = R:Remain  
                      DO Load_Item
                   .       
                ELSE
                   CASE MESSAGE('Not all Items loaded - needed capacity (kgs): ' & FORMAT(R:Needed,'@n10.1') & '||for ' & R:Load & ' units on Item No. ' & DELI:ItemNo & |
                      ' (DIID ' & DELI:DIID & ').', 'Load DI Items' & |
                      '|||Would you like to load the remaining ' & R:Remain & ' units anyway?', 'Load DI Items', ICON:Question, BUTTON:Yes+BUTTON:NO,BUTTON:NO)
                   OF BUTTON:YES
                      R:Load    = R:Remain  
                      DO Load_Item
          .  .  .  .
          ! BREAK   
    .  .

    DelI_Mgr.Kill()

    RETURN
!!! <summary>
!!! Generated from procedure template - Window
!!! **** changes such as additional calls to procedures using templates, needs checking of RUN method
!!! </summary>
Update_TripSheet PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Screen_Group     GROUP,PRE(L_SG)                       !
TID                  ULONG                                 !Transporter ID
TransporterName      STRING(35)                            !Transporters Name
CompositionName      STRING(35)                            !
MakeModels           STRING(150)                           !Make & Model
Driver               STRING(35)                            !Driver - Note these only apply to Horse & Combined types
Assistant            STRING(35)                            !
Registration         STRING(120)                           !
Capacity             DECIMAL(6)                            !In Kgs
No_Vehicles          LONG                                  !Number of vehicles in combination
Tot_Weight           DECIMAL(8,2)                          !In kg's
No_Items             ULONG                                 !
Suburb               STRING(50)                            !Suburb
Notifications        BYTE                                  !
                     END                                   !
LOC:Item_Description STRING(200)                           !
LOC:State_Group      GROUP,PRE(L_STG)                      !
Allow_Capture        BYTE                                  !
Orig_State           BYTE                                  !State of this manifest
No_Incomplete        ULONG                                 !
Incomplete_DIID_List STRING(200)                           !
Item_List            STRING(500)                           !
MinimiumLoad         DECIMAL(8)                            !In Kgs
TRID                 ULONG                                 !Transporter Rate ID
Setup_Rate           BYTE                                  !
                     END                                   !
LOC:DI_Express_Load_Group GROUP,PRE(L_DL)                  !
DINo                 ULONG                                 !Delivery Instruction Number
DID                  ULONG                                 !Delivery ID
Last_State           BYTE                                  !
State_Progression    BYTE                                  !
                     END                                   !
BRW2::View:Browse    VIEW(TripSheetDeliveries)
                       PROJECT(TRDI:DeliveredDate)
                       PROJECT(TRDI:DIID)
                       PROJECT(TRDI:TDID)
                       PROJECT(TRDI:UnitsLoaded)
                       PROJECT(TRDI:TRID)
                       JOIN(DELI:PKey_DIID,TRDI:DIID)
                         PROJECT(DELI:ItemNo)
                         PROJECT(DELI:Type)
                         PROJECT(DELI:Units)
                         PROJECT(DELI:ContainerNo)
                         PROJECT(DELI:DIID)
                         PROJECT(DELI:CMID)
                         PROJECT(DELI:PTID)
                         PROJECT(DELI:DID)
                         JOIN(COM:PKey_CMID,DELI:CMID)
                           PROJECT(COM:Commodity)
                           PROJECT(COM:CMID)
                         END
                         JOIN(PACK:PKey_PTID,DELI:PTID)
                           PROJECT(PACK:Packaging)
                           PROJECT(PACK:PTID)
                         END
                         JOIN(DEL:PKey_DID,DELI:DID)
                           PROJECT(DEL:DINo)
                           PROJECT(DEL:DID)
                           PROJECT(DEL:CID)
                           JOIN(CLI:PKey_CID,DEL:CID)
                             PROJECT(CLI:ClientName)
                             PROJECT(CLI:CID)
                           END
                         END
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
INV:IID                LIKE(INV:IID)                  !List box control field - type derived from field
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
LOC:Item_Description   LIKE(LOC:Item_Description)     !List box control field - type derived from local data
TRDI:DeliveredDate     LIKE(TRDI:DeliveredDate)       !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !List box control field - type derived from field
TRDI:DIID              LIKE(TRDI:DIID)                !List box control field - type derived from field
TRDI:TDID              LIKE(TRDI:TDID)                !List box control field - type derived from field
TRDI:UnitsLoaded       LIKE(TRDI:UnitsLoaded)         !Browse hot field - type derived from field
DELI:Type              LIKE(DELI:Type)                !Browse hot field - type derived from field
DELI:Units             LIKE(DELI:Units)               !Browse hot field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !Browse hot field - type derived from field
PACK:Packaging         LIKE(PACK:Packaging)           !Browse hot field - type derived from field
TRDI:TRID              LIKE(TRDI:TRID)                !Browse key field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Related join file key field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Related join file key field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Related join file key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::TRI:Record  LIKE(TRI:RECORD),THREAD
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Trip Sheet'),AT(,,361,283),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdateTripSheet'),SYSTEM
                       PROMPT('BID:'),AT(216,6),USE(?TRI:BID:Prompt)
                       STRING(@n_10),AT(233,6,31),USE(TRI:BID),RIGHT(1)
                       SHEET,AT(4,4,354,259),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('TRID:'),AT(277,6),USE(?TRI:TRID:Prompt)
                           STRING(@N_10),AT(297,6),USE(TRI:TRID),RIGHT(1)
                           GROUP,AT(9,22,344,69),USE(?Group_Top)
                             GROUP,AT(9,22,339,11),USE(?Group_Trans)
                               PROMPT('Transporter:'),AT(9,22),USE(?TransporterName:Prompt),TRN
                               BUTTON('...'),AT(50,22,12,10),USE(?CallLookup)
                               ENTRY(@s35),AT(66,22,105,10),USE(L_SG:TransporterName),MSG('Transporters Name'),REQ,TIP('Transporters Name')
                               PROMPT('Composition:'),AT(182,22),USE(?CompositionName:Prompt),TRN
                               BUTTON('...'),AT(226,22,12,10),USE(?CallLookup:2)
                               ENTRY(@s35),AT(242,22,105,10),USE(L_SG:CompositionName)
                             END
                             PROMPT('Notes:'),AT(9,42),USE(?TRI:Notes:Prompt),TRN
                             TEXT,AT(66,42,282,30),USE(TRI:Notes),VSCROLL,BOXED,MSG('Notes'),TIP('Notes')
                             CHECK(' &Collections'),AT(9,62),USE(TRI:Collections),MSG('Collections'),TIP('Collections'), |
  TRN
                             PROMPT('Suburb:'),AT(9,79),USE(?Prompt17),TRN
                             BUTTON('...'),AT(50,79,12,10),USE(?CallLookup:Suburb)
                             ENTRY(@s50),AT(66,79,105,10),USE(L_SG:Suburb)
                             PROMPT('Express Load - DI No.:'),AT(181,79),USE(?DINo:Prompt),TRN
                             BUTTON('&Load'),AT(257,79,32,10),USE(?Button_Load),DEFAULT
                             ENTRY(@n_10b),AT(291,79,57,10),USE(L_DL:DINo),RIGHT(1),COLOR(00B0FFC6h),MSG('Delivery I' & |
  'nstruction Number'),TIP('Delivery Instruction Number')
                           END
                           GROUP,AT(234,100,113,10),USE(?Group_LoadedWeight)
                             PROMPT('Loaded Weight:'),AT(237,100),USE(?Tot_Weight:Prompt)
                             ENTRY(@n-11.2),AT(293,100,57,10),USE(L_SG:Tot_Weight),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('In kg''s'), |
  READONLY,SKIP,TIP('In kg''s')
                           END
                           SHEET,AT(9,98,344,162),USE(?Sheet2)
                             TAB('Trip Sheet Items'),USE(?Tab_Items)
                               LIST,AT(13,118,334,123),USE(?List),HVSCROLL,FORMAT('40R(2)|M~DI No.~L(1)@n_10@40R(2)|M~' & |
  'Invoice No.~L(1)@n_10@20R(2)|M~Item (DI)~L(1)@n6@60L(2)|M~Client~L(1)@s35@50L(2)|M~C' & |
  'ommodity~L(1)@s35@80L(2)|M~Container / Packaging~L(1)@s200@40R(2)|M~Delivered Date~L' & |
  '(1)@d5@30R(2)|M~DID~L(1)@n_10@30R(2)|M~DIID~L(1)@n_10@30R(2)|M~TDID~L(1)@n_10@'),FROM(Queue:Browse), |
  IMM,MSG('Browsing the Trip Sheet Items file')
                               BUTTON('C&apture'),AT(13,244,,14),USE(?Button_Save),LEFT,ICON('SAVE.ICO'),DISABLE,FLAT,TIP('Press to s' & |
  'tart capturing Delivery Items')
                               BUTTON('&View'),AT(125,244,,12),USE(?View_TripSheet),LEFT,ICON('waview.ico'),FLAT
                               BUTTON('&Insert'),AT(179,244,,12),USE(?Insert),LEFT,ICON('WAinsert.ICO'),FLAT
                               BUTTON('&Change'),AT(233,244,,12),USE(?Change),LEFT,ICON('WACHANGE.ICO'),FLAT
                               BUTTON('&Delete'),AT(291,244,,12),USE(?Delete),LEFT,ICON('WAdelete.ICO'),FLAT
                             END
                             TAB('Prints && Additionals'),USE(?Tab6)
                               GROUP,AT(13,117,317,139),USE(?Group_Extra)
                                 GROUP,AT(135,212,38,42),USE(?Group_Time),HIDE
                                   REGION,AT(135,218,38,36),USE(?Region1),IMM
                                   IMAGE('clockface.jpg'),AT(135,218,38,36),USE(?Image1)
                                   LINE,AT(135,236,18,0),USE(?Line1),COLOR(COLOR:Maroon),LINEWIDTH(2)
                                 END
                                 BUTTON('Print Trip Sheet'),AT(69,117,75,12),USE(?Button_Print_TripSheet)
                                 PROMPT('Driver:'),AT(13,136),USE(?Driver:Prompt)
                                 BUTTON('...'),AT(51,136,12,10),USE(?CallLookup:3)
                                 ENTRY(@s35),AT(69,136,193,10),USE(L_SG:Driver),MSG('Driver'),READONLY,SKIP,TIP('Driver')
                                 STRING(@n_10),AT(263,136),USE(TRI:DRID),RIGHT(1),TRN
                                 PROMPT('Assistant:'),AT(13,152),USE(?TRI:Assistant_DRID:Prompt)
                                 BUTTON('...'),AT(51,152,12,10),USE(?CallLookup:Driver_Assistant)
                                 ENTRY(@s35),AT(69,152,193,10),USE(L_SG:Assistant),READONLY,SKIP
                                 STRING(@n_10),AT(263,152),USE(TRI:Assistant_DRID),RIGHT(1),TRN
                                 BUTTON('View these Vehicles'),AT(69,170,,12),USE(?Button_View_Composition)
                                 PROMPT('Make && Model:'),AT(13,186),USE(?MakeModel:Prompt)
                                 ENTRY(@s150),AT(69,186,237,10),USE(L_SG:MakeModels),COLOR(00E9E9E9h),FLAT,MSG('Make & Model'), |
  READONLY,SKIP,TIP('Make & Model')
                                 PROMPT('Registrations:'),AT(13,199),USE(?Registration:Prompt)
                                 ENTRY(@s120),AT(69,199,237,10),USE(L_SG:Registration),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                                 PROMPT('Total Capacity:'),AT(13,212),USE(?Capacity:Prompt)
                                 ENTRY(@n-8.0),AT(69,212,57,10),USE(L_SG:Capacity),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                                 PROMPT('No. Vehicles:'),AT(193,212),USE(?No_Vehicles:Prompt)
                                 ENTRY(@n-14),AT(248,212,57,10),USE(L_SG:No_Vehicles),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Number of ' & |
  'vehicles in combination'),READONLY,SKIP,TIP('Number of vehicles in combination')
                                 PROMPT('Depart Date:'),AT(13,231),USE(?TRI:DepartDate:Prompt)
                                 ENTRY(@d6),AT(69,231,57,10),USE(TRI:DepartDate),RIGHT(1)
                                 PROMPT('Depart Time:'),AT(13,245),USE(?TRI:DepartTime:Prompt)
                                 ENTRY(@t7),AT(69,245,57,10),USE(TRI:DepartTime),RIGHT(1)
                                 PROMPT('Returned Date:'),AT(193,231),USE(?TRI:ReturnedDate:Prompt)
                                 ENTRY(@d6),AT(248,231,57,10),USE(TRI:ReturnedDate),RIGHT(1)
                                 PROMPT('Returned Time:'),AT(193,245),USE(?TRI:ReturnedTime:Prompt)
                                 ENTRY(@t7),AT(248,245,57,10),USE(TRI:ReturnedTime),RIGHT(1)
                               END
                             END
                           END
                         END
                       END
                       BUTTON,AT(4,266,,14),USE(?Button_Prev),LEFT,ICON('PRIORPG.ICO'),FLAT,SKIP
                       BUTTON('Transferred'),AT(28,266,,14),USE(?Button_Next),LEFT,ICON('NEXTPG.ICO'),FLAT,SKIP
                       PROMPT('Load Complete'),AT(100,270,,10),USE(?Prompt_Change),FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI)
                       BUTTON('&OK'),AT(248,266,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept data an' & |
  'd close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(307,266,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                     END

BRW2::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Clock_Class         CLASS

Active      BYTE
XPos        LONG
YPos        LONG

CXPos       LONG
CYPos       LONG
LWidth      LONG

Init             PROCEDURE(LONG RegCtl)

MouseClick       PROCEDURE()

MouseOut         PROCEDURE()

MouseMove        PROCEDURE()

DrawLine         PROCEDURE()

    .
                     MAP
Next                 PROCEDURE(),BYTE     !- cycle or not
                     .
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Veh_Composition                 ROUTINE
    VCO:VCID                = TRI:VCID
    IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
       ! If we have selected a Vehicle with a different Transporter then change the Transporter now
       ! if the vehicle is not shown for all transporters...
       IF L_SG:TID ~= VCO:TID AND VCO:ShowForAllTransporters ~= TRUE
          L_SG:TID          = VCO:TID
          TRI:TID           = VCO:TID

          TRA:TID           = L_SG:TID
          IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
             L_SG:TransporterName = TRA:TransporterName
       .  .

       TRU:TTID             = VCO:TTID0
       IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
          !L_SG:Driver       = TRU:Driver
          !L_SG:Assistant    = TRU:Assistant


          L_SG:MakeModels   = Get_VehComp_Info(TRI:VCID, 3)

          L_SG:Registration = Get_VehComp_Info(TRI:VCID, 2)

          L_SG:Capacity     = Get_VehComp_Info(TRI:VCID, 1)

          L_SG:No_Vehicles  = Get_VehComp_Info(TRI:VCID, 7)
       .

       ENABLE(?Button_Save)
       DISPLAY
    .

    IF QuickWindow{PROP:AcceptAll} = FALSE AND TRU:DRID ~= TRI:DRID AND TRU:DRID ~= 0 AND TRI:State < 2 AND TRI:VCID ~= 0
       ! Loading, Loaded, On Route, Transferred, Finalised
       IF TRI:DRID = 0
          TRI:DRID              = TRU:DRID
       ELSE
          DRI:DRID              = TRU:DRID                      ! Taken from the Trip
          IF Access:Drivers.TryFetch(DRI:PKey_DRID) ~= LEVEL:Benign
             !L_SG:Driver       = CLIP(DRI:FirstName) & ' ' & DRI:Surname
             CLEAR(DRI:Record)
          .

          CASE MESSAGE('The driver on the 1st Vehicle (horse or combined) for this composition is - ' & CLIP(CLIP(DRI:FirstName) & ' ' & DRI:Surname) & |
                       '(' & TRU:DRID & ')|which is not the same as the specified driver on this Trip Sheet - ' & CLIP(L_SG:Driver) & |
                       '(' & TRI:DRID & ')||Would you like to change the Trip Sheet driver to the Vehicle driver?', 'Driver', ICON:Question,|
                       BUTTON:Yes+BUTTON:No,BUTTON:Yes)
          OF BUTTON:Yes
             TRI:DRID          = TRU:DRID
       .  .

       DRI:DRID             = TRI:DRID                      ! Taken from the Trip
       IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
          L_SG:Driver       = CLIP(DRI:FirstName) & ' ' & DRI:Surname
       .
       DISPLAY(?L_SG:Driver)
    ELSIF CLIP(L_SG:Driver) = ''
       DRI:DRID             = TRI:DRID                      ! Taken from the Trip
       IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
          L_SG:Driver       = CLIP(DRI:FirstName) & ' ' & DRI:Surname
       .
       DISPLAY(?L_SG:Driver)
    .
    EXIT
Load_State                          ROUTINE
    ENABLE(?Button_Prev)
    ENABLE(?Button_Next)

    ! Loading|#0|Loaded|#1|On Route|#2|Transferred|#3
    CASE TRI:State
    OF 0                ! Loading|#0|
       ?Prompt_Change{PROP:Text}        = ''

       ?Button_Next{PROP:Text}          = '&Loading'
       ?Button_Next{PROP:Tip}           = 'Status is Loading - set Status to Loaded'

       DISABLE(?Button_Prev)
    OF 1                ! Loaded|#1|
       ?Prompt_Change{PROP:Text}        = 'Load Complete'

       ?Button_Next{PROP:Text}          = '&Loaded'
       ?Button_Next{PROP:Tip}           = 'Status is Loaded - set Status to On Route'
    OF 2                ! On Route|#2|
       ! Nothing more can be done - set view mode
       ?Prompt_Change{PROP:Text}        = 'On Route - No Changes'

       ?Button_Next{PROP:Text}          = 'On &Route'
       ?Button_Next{PROP:Tip}           = 'Status is On Route - set Status to Transferred'
    OF 3                ! Transferred|#3
       ?Prompt_Change{PROP:Text}        = 'Update Delivered'

       ?Button_Next{PROP:Text}          = '&Transferred'
       ?Button_Next{PROP:Tip}           = 'Status is Transferred - complete the Tripsheet'
    OF 4                ! Finalised|#4
       ?Prompt_Change{PROP:Text}        = 'Finalised - No Changes'

       ?Button_Next{PROP:Text}          = '&Finalised'
       ?Button_Next{PROP:Tip}           = 'Status is Finalised - this stage is Final'

       DISABLE(?Button_Next)
    .
    DO Load_State_Buttons

    IF L_STG:Orig_State >= 2
       ! When in these states update form is in view mode only - so manual update here
       IF Access:Tripsheets.Update() = LEVEL:Benign
       .

       ! If new state would allow edits then tell user how to do it
       IF TRI:State < 2
          MESSAGE('This state allows the TripSheet to be edited.||To edit the TripSheet please click OK and then re-load the TripSheet.', 'Trip Sheet State Change', ICON:Exclamation)
          POST(EVENT:CloseWindow)
    .  .

    L_DL:Last_State     = TRI:State
    EXIT



Load_State_Buttons                  ROUTINE
    ! Loading,  Loaded,  On Route,  Transferred, Finalised
    IF TRI:State <= 0
       IF ?Button_Save{PROP:Enabled} = FALSE AND L_STG:Orig_State < 2 AND L_STG:Allow_Capture = TRUE
          IF TRI:State > 0
             DISABLE(?Insert)
             DISABLE(?Change)
             DISABLE(?Delete)
          ELSE
             ENABLE(?Insert)
             ENABLE(?Change)
             ENABLE(?Delete)
       .  .
    ELSIF TRI:State > 3
       L_STG:Allow_Capture = FALSE

       ENABLE(?View_TripSheet)

       DISABLE(?Button_Save)

       DISABLE(?Insert)
       DISABLE(?Change)
       DISABLE(?Delete)
    ELSE
       L_STG:Allow_Capture = FALSE

       DISABLE(?Button_Save)

       DISABLE(?Insert)
       DISABLE(?Delete)

       IF TRI:State = 3
          ENABLE(?Delete)
    .  .

    EXIT
Calc_Totals                          ROUTINE
    ! (p:TRID, p:Option, p:DID, p:TDID, p:DIID, p:DelDate_List)
    ! (<ULONG>, BYTE=0, <ULONG>, <ULONG>, <ULONG>, <*STRING>),ULONG
    ! p:Option
    ! TRID passed
    !   0. Weight (real weight - not volumetric)
    !   1. Units
    !       if p:DID is provided then the result is limited to the DID passed
    !       if p:TDID is provided then exclude this from weight
    ! DID passed only
    !   2. List of delivery dates, no of entries returned - for DIID!
    !   3. List of Delivered Date & Times

    L_SG:Tot_Weight     = Get_TripDelItems_Info(TRI:TRID, 0) / 100
    L_SG:No_Items       = Get_TripDelItems_Info(TRI:TRID, 1)

    DISPLAY
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Trip Sheet Record'
  OF InsertRecord
    ActionMessage = 'Trip Sheet Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Trip Sheet Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_TripSheet')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRI:BID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Item_Description',LOC:Item_Description) ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:TripSheets)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(TRI:Record,History::TRI:Record)
  SELF.AddHistoryField(?TRI:BID,2)
  SELF.AddHistoryField(?TRI:TRID,1)
  SELF.AddHistoryField(?TRI:Notes,12)
  SELF.AddHistoryField(?TRI:Collections,17)
  SELF.AddHistoryField(?TRI:DRID,14)
  SELF.AddHistoryField(?TRI:Assistant_DRID,15)
  SELF.AddHistoryField(?TRI:DepartDate,6)
  SELF.AddHistoryField(?TRI:DepartTime,7)
  SELF.AddHistoryField(?TRI:ReturnedDate,10)
  SELF.AddHistoryField(?TRI:ReturnedTime,11)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Add_Suburbs.SetOpenRelated()
  Relate:Add_Suburbs.Open                         ! File Add_Suburbs used by this procedure, so make sure it's RelationManager is open
  Access:Transporter.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleComposition.UseFile               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailer.UseFile                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleMakeModel.UseFile                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DriversAlias.UseFile                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Invoice.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TripSheets
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:TripSheetDeliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
      DO Load_State
      IF TRI:State >= 2               ! On Route
         ThisWindow.Request   = ViewRecord
      .
  
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?L_SG:TransporterName{PROP:ReadOnly} = True
    DISABLE(?CallLookup:2)
    ?L_SG:CompositionName{PROP:ReadOnly} = True
    DISABLE(?CallLookup:Suburb)
    ?L_SG:Suburb{PROP:ReadOnly} = True
    DISABLE(?Button_Load)
    ?L_DL:DINo{PROP:ReadOnly} = True
    ?L_SG:Tot_Weight{PROP:ReadOnly} = True
    DISABLE(?Button_Save)
    DISABLE(?View_TripSheet)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
    DISABLE(?Button_Print_TripSheet)
    DISABLE(?CallLookup:3)
    ?L_SG:Driver{PROP:ReadOnly} = True
    DISABLE(?CallLookup:Driver_Assistant)
    ?L_SG:Assistant{PROP:ReadOnly} = True
    DISABLE(?Button_View_Composition)
    ?L_SG:MakeModels{PROP:ReadOnly} = True
    ?L_SG:Registration{PROP:ReadOnly} = True
    ?L_SG:No_Vehicles{PROP:ReadOnly} = True
    ?TRI:DepartDate{PROP:ReadOnly} = True
    ?TRI:DepartTime{PROP:ReadOnly} = True
    ?TRI:ReturnedDate{PROP:ReadOnly} = True
    ?TRI:ReturnedTime{PROP:ReadOnly} = True
    DISABLE(?Button_Prev)
    DISABLE(?Button_Next)
  END
      IF TRI:State >= 2               ! On Route
         ENABLE(?Button_Next)
         ENABLE(?Button_Prev)
  
         ENABLE(?View_TripSheet)
      .
  
      ENABLE(?Button_Print_TripSheet)
      ENABLE(?Button_View_Composition)
  
      ?Prompt_Change{PROP:Text}    = ''
  
      L_STG:Orig_State  = TRI:State
  BRW2.Q &= Queue:Browse
  BRW2.FileLoaded = 1                             ! This is a 'file loaded' browse
  BRW2.AddSortOrder(,TRDI:FKey_TRID)              ! Add the sort order for TRDI:FKey_TRID for sort order 1
  BRW2.AddRange(TRDI:TRID,TRI:TRID)               ! Add single value range limit for sort order 1
  BRW2.AddLocator(BRW2::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW2::Sort0:Locator.Init(,TRDI:TRID,1,BRW2)     ! Initialize the browse locator using  using key: TRDI:FKey_TRID , TRDI:TRID
  BRW2.AppendOrder('+DEL:DINo,+DELI:ItemNo,+TRDI:TDID') ! Append an additional sort order
  BRW2.AddField(DEL:DINo,BRW2.Q.DEL:DINo)         ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW2.AddField(INV:IID,BRW2.Q.INV:IID)           ! Field INV:IID is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ItemNo,BRW2.Q.DELI:ItemNo)   ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW2.AddField(CLI:ClientName,BRW2.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW2.AddField(COM:Commodity,BRW2.Q.COM:Commodity) ! Field COM:Commodity is a hot field or requires assignment from browse
  BRW2.AddField(LOC:Item_Description,BRW2.Q.LOC:Item_Description) ! Field LOC:Item_Description is a hot field or requires assignment from browse
  BRW2.AddField(TRDI:DeliveredDate,BRW2.Q.TRDI:DeliveredDate) ! Field TRDI:DeliveredDate is a hot field or requires assignment from browse
  BRW2.AddField(DEL:DID,BRW2.Q.DEL:DID)           ! Field DEL:DID is a hot field or requires assignment from browse
  BRW2.AddField(TRDI:DIID,BRW2.Q.TRDI:DIID)       ! Field TRDI:DIID is a hot field or requires assignment from browse
  BRW2.AddField(TRDI:TDID,BRW2.Q.TRDI:TDID)       ! Field TRDI:TDID is a hot field or requires assignment from browse
  BRW2.AddField(TRDI:UnitsLoaded,BRW2.Q.TRDI:UnitsLoaded) ! Field TRDI:UnitsLoaded is a hot field or requires assignment from browse
  BRW2.AddField(DELI:Type,BRW2.Q.DELI:Type)       ! Field DELI:Type is a hot field or requires assignment from browse
  BRW2.AddField(DELI:Units,BRW2.Q.DELI:Units)     ! Field DELI:Units is a hot field or requires assignment from browse
  BRW2.AddField(DELI:ContainerNo,BRW2.Q.DELI:ContainerNo) ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW2.AddField(PACK:Packaging,BRW2.Q.PACK:Packaging) ! Field PACK:Packaging is a hot field or requires assignment from browse
  BRW2.AddField(TRDI:TRID,BRW2.Q.TRDI:TRID)       ! Field TRDI:TRID is a hot field or requires assignment from browse
  BRW2.AddField(DELI:DIID,BRW2.Q.DELI:DIID)       ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW2.AddField(COM:CMID,BRW2.Q.COM:CMID)         ! Field COM:CMID is a hot field or requires assignment from browse
  BRW2.AddField(PACK:PTID,BRW2.Q.PACK:PTID)       ! Field PACK:PTID is a hot field or requires assignment from browse
  BRW2.AddField(CLI:CID,BRW2.Q.CLI:CID)           ! Field CLI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_TripSheet',QuickWindow)    ! Restore window settings from non-volatile store
  L_SG:Notifications   = GETINI('Delivery', 'NotificationsOn', '1', GLO:Global_INI)
      L_DL:State_Progression  = GETINI('TripSheet_Prompts', 'State_Progression', 2, GLO:Local_INI)
  
      
  
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      IF SELF.Request = InsertRecord
         DISABLE(?Insert)
      ELSIF SELF.Request = ChangeRecord
         IF TRI:BID = 0
            TRI:BID   = GLO:BranchID
            MESSAGE('The Branch ID was zero on this Trip Sheet.  This has now been set to your Branch.||You should save this record.', 'Update Trip Sheet', ICON:Hand)
         .
      ELSE
         DISABLE(?Button_Save)
         L_STG:Allow_Capture  = TRUE
  
  !       IF Get_ManLoad_Info(MAN:MID, 1) > 0
  !          DISABLE(?Group_Trans)
      .!  .
      VCO:VCID                = TRI:VCID
      IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
         L_SG:CompositionName = VCO:CompositionName
      .
  
      DO Veh_Composition
  
  
      L_SG:TID                = TRI:TID
      TRA:TID                 = TRI:TID
      IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
         L_SG:TransporterName = TRA:TransporterName
      .
  
  
      DRI:DRID            = TRI:Assistant_DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_SG:Assistant   = DRI:FirstNameSurname
      .
  
  
      SUBU:SUID           = TRI:SUID
      IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) = LEVEL:Benign
         L_SG:Suburb      = SUBU:Suburb
      ELSE
         CLEAR(L_SG:Suburb)
      .
      DO Calc_Totals
  
      DO Load_State               ! ??? 2nd load state?
  BRW2.AskProcedure = 6
  BRW2.AddToolbarTarget(Toolbar)                  ! Browse accepts toolbar control
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_TripSheet',1,?List,2,BRW2::PopupTextExt,Queue:Browse,10,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Suburbs.Close
  END
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_TripSheet',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  TRI:BID = GLO:BranchID
    ! Save the record with the branch?
    IF Access:TripSheets.Update() = LEVEL:Benign
    .
  PARENT.PrimeFields


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
      DO Load_State_Buttons


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Transporter
      Select_VehicleComposition
      Browse_Suburbs
      Browse_Drivers('','')
      Browse_Drivers_Alias_h('','1')
      Update_TripSheetDeliveries_h(L_DL:DINo)
    END
    ReturnValue = GlobalResponse
  END
        IF Number = 6 AND SELF.Request = ViewRecord         ! 6 = Update_TripSheetDeliveries_h
           GlobalRequest = Request
           Update_TripSheetDeliveries_h
        .
  
        IF Number = 6
           DO Calc_Totals
        .
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?CallLookup:2
          ! Load Transporter record
          TRA:TID     = L_SG:TID
          IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
          .
    OF ?Insert
          CLEAR(L_DL:DINo)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      TRA:TransporterName = L_SG:TransporterName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SG:TransporterName = TRA:TransporterName
        L_SG:TID = TRA:TID
        TRI:TID = TRA:TID
      END
      ThisWindow.Reset(1)
    OF ?L_SG:TransporterName
      IF L_SG:TransporterName OR ?L_SG:TransporterName{PROP:Req}
        TRA:TransporterName = L_SG:TransporterName
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SG:TransporterName = TRA:TransporterName
            L_SG:TID = TRA:TID
            TRI:TID = TRA:TID
          ELSE
            CLEAR(L_SG:TID)
            CLEAR(TRI:TID)
            SELECT(?L_SG:TransporterName)
            CYCLE
          END
        ELSE
          L_SG:TID = TRA:TID
          TRI:TID = TRA:TID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update()
      VCO:CompositionName = L_SG:CompositionName
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_SG:CompositionName = VCO:CompositionName
        TRI:VCID = VCO:VCID
      END
      ThisWindow.Reset(1)
          DO Veh_Composition
    OF ?L_SG:CompositionName
      IF L_SG:CompositionName OR ?L_SG:CompositionName{PROP:Req}
        VCO:CompositionName = L_SG:CompositionName
        IF Access:VehicleComposition.TryFetch(VCO:Key_Name)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            L_SG:CompositionName = VCO:CompositionName
            TRI:VCID = VCO:VCID
          ELSE
            CLEAR(TRI:VCID)
            SELECT(?L_SG:CompositionName)
            CYCLE
          END
        ELSE
          TRI:VCID = VCO:VCID
        END
      END
      ThisWindow.Reset()
          IF CLIP(L_SG:CompositionName) = ''
             CLEAR(TRI:VCID)
          .                                                                                                                               
          DO Veh_Composition
    OF ?TRI:Collections
          IF TRI:Collections = TRUE
             IF L_SG:No_Items > 0
                TRI:Collections   = FALSE
                MESSAGE('Cannot set Collections only TripSheet when there are Delivery Items attached.||To set this please remove all Delivery Items.', 'Collections', ICON:Exclamation)
             ELSE
                DISABLE(?Tab_Items)
             .
          ELSE
             ! Ok
          .
      
          DISPLAY
    OF ?CallLookup:Suburb
      ThisWindow.Update()
      SUBU:Suburb = L_SG:Suburb
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        L_SG:Suburb = SUBU:Suburb
        TRI:SUID = SUBU:SUID
      END
      ThisWindow.Reset(1)
    OF ?L_SG:Suburb
      IF L_SG:Suburb OR ?L_SG:Suburb{PROP:Req}
        SUBU:Suburb = L_SG:Suburb
        IF Access:Add_Suburbs.TryFetch(SUBU:Key_Suburb)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            L_SG:Suburb = SUBU:Suburb
            TRI:SUID = SUBU:SUID
          ELSE
            CLEAR(TRI:SUID)
            SELECT(?L_SG:Suburb)
            CYCLE
          END
        ELSE
          TRI:SUID = SUBU:SUID
        END
      END
      ThisWindow.Reset()
    OF ?Button_Load
      ThisWindow.Update()
          ! (ULONG, ULONG, ULONG, ULONG=0), LONG
          ! (p:TRID, p:VCID, p:DINo, p:DIID)
          Loaded_#    = TripSheet_Load_DI(TRI:TRID, TRI:VCID, L_DL:DINo,, 1)
          ! -1    = full
          ! -2    = Delivered
          ! -3    = Needs releasing
      
          IF Loaded_# = -1
             BRW2.Ask(InsertRecord)
          .
      
          DO Calc_Totals
      BRW2.ResetFromFile()
          SELECT(?L_DL:DINo)
    OF ?Button_Save
      ThisWindow.Update()
          IF Access:TripSheets.Update() = LEVEL:Benign
             ENABLE(?Insert)
             DISABLE(?Button_Save)
      
             L_STG:Allow_Capture      = TRUE
      
             POST(EVENT:Accepted, ?Insert)
          .
    OF ?Button_Print_TripSheet
      ThisWindow.Update()
          ThisWindow.Update
      
          IF TRI:Collections = TRUE
             Print_TripSheet_Collections(TRI:TRID)
          ELSE
             EXECUTE POPUP('Print|Print With Preview')
                Print_TripSheet_no_Preview(TRI:TRID)
                Print_TripSheet(TRI:TRID)
          .  .
      
          ThisWindow.Reset
      
    OF ?CallLookup:3
      ThisWindow.Update()
      DRI:FirstNameSurname = L_SG:Driver
      IF SELF.Run(4,SelectRecord) = RequestCompleted
        L_SG:Driver = DRI:FirstNameSurname
        TRI:DRID = DRI:DRID
      END
      ThisWindow.Reset(1)
          CLEAR(L_SG:Driver)
      
          DRI:DRID          = TRI:DRID
          IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
             L_SG:Driver    = CLIP(DRI:FirstName) & ' ' & DRI:Surname
          .
          DISPLAY(?L_SG:Driver)
      
    OF ?L_SG:Driver
      IF L_SG:Driver OR ?L_SG:Driver{PROP:Req}
        DRI:FirstNameSurname = L_SG:Driver
        IF Access:Drivers.TryFetch(DRI:SKey_FirstNameSurname)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            L_SG:Driver = DRI:FirstNameSurname
            TRI:DRID = DRI:DRID
          ELSE
            CLEAR(TRI:DRID)
            SELECT(?L_SG:Driver)
            CYCLE
          END
        ELSE
          TRI:DRID = DRI:DRID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:Driver_Assistant
      ThisWindow.Update()
      A_DRI:FirstNameSurname = L_SG:Assistant
      IF SELF.Run(5,SelectRecord) = RequestCompleted
        L_SG:Assistant = A_DRI:FirstNameSurname
        TRI:Assistant_DRID = A_DRI:DRID
      END
      ThisWindow.Reset(1)
    OF ?L_SG:Assistant
      IF L_SG:Assistant OR ?L_SG:Assistant{PROP:Req}
        A_DRI:FirstNameSurname = L_SG:Assistant
        IF Access:DriversAlias.TryFetch(A_DRI:SKey_FirstNameSurname)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            L_SG:Assistant = A_DRI:FirstNameSurname
            TRI:Assistant_DRID = A_DRI:DRID
          ELSE
            CLEAR(TRI:Assistant_DRID)
            SELECT(?L_SG:Assistant)
            CYCLE
          END
        ELSE
          TRI:Assistant_DRID = A_DRI:DRID
        END
      END
      ThisWindow.Reset()
    OF ?Button_View_Composition
      ThisWindow.Update()
      GlobalRequest = ViewRecord
      Update_VehicleComposition()
      ThisWindow.Reset
    OF ?Button_Prev
      ThisWindow.Update()
          ! Loading|#0|Loaded|#1|On Route|#2|Transferred|
          TRI:State   -= 1
      
          CASE TRI:State
          OF 0                ! Loading|#0|
             ?Button_Prev{PROP:Tip}           = 'Status is Loading - this stage is Earliest'
          OF 1                ! Loaded|#1|
             ?Button_Prev{PROP:Tip}           = 'Status is Loaded - set Status to Loading'
          OF 2                ! On Route|#2|
             ?Button_Prev{PROP:Tip}           = 'Status is On Route - set Status to Loaded'
          OF 3                ! Transferred|#3
             ?Button_Prev{PROP:Tip}           = 'Status is Transferred - set status to On Route'
          OF 4                ! Finalised|#4
             ?Button_Prev{PROP:Tip}           = 'Status is Finalised - set status to Transferred'
          ELSE
             TRI:State                        = 0
          .
      
      
          DO Load_State
    OF ?Button_Next
      ThisWindow.Update()
      IF Next() > 0
         CYCLE
      .
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Region1
    CASE EVENT()
    OF EVENT:MouseUp
          Clock_Class.MouseClick()
    OF EVENT:MouseOut
          Clock_Class.MouseOut()
    OF EVENT:MouseMove
          Clock_Class.MouseMove()
      
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF TRI:Collections = TRUE
             IF L_SG:No_Items > 0
                TRI:Collections   = FALSE
             ELSE
                DISABLE(?Tab_Items)
             .
          ELSE
             ! Ok
          .
          Clock_Class.Init(?Region1)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Next                                            PROCEDURE()!,BYTE - cycle or not
L:Cycle        BYTE(0)

CODE   
   ! Loading       - Can add items
   ! Loaded        - All items added - no adding or updating
   ! On Route      - No adding - update of Return Date / Time and Items delivered
   ! Transferred   - set Return Date / Time - Must capture all Item delivery status / how many delivered
   ! Finalised     - TripSheet returned and all info captured

   TRI:State   += 1
   IF TRI:State > 4
      TRI:State = 4
   .

   IF TRI:State = 1
      IF Check_COD_Invoices(TRI:TRID, 0) > 0
         CASE MESSAGE('Would you like to print COD invoices now?', 'Print COD Invoices', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
         OF BUTTON:Yes
            Check_COD_Invoices(TRI:TRID, 1)
      .  .

      ! Check that we have some items - check for a minimium load requirement?
      DO Calc_Totals
                                        ! (p:TID, p:JID, p:VCID, p:Setup, p:Setup_VCID, p:MinimiumLoad, p:BaseCharge, p:TRID, p:Local_Rate)
                                        ! (ULONG, ULONG, ULONG=0, *BYTE, <*ULONG>, <*DECIMAL>, <*DECIMAL>, <*ULONG>, BYTE=0), ULONG
      Throw_Rate_#  = Get_Transporter_Rate(L_SG:TID, 0, TRI:VCID, L_STG:Setup_Rate,, L_STG:MinimiumLoad,, L_STG:TRID, 1) / 100
      IF L_STG:MinimiumLoad > L_SG:Tot_Weight
         CASE MESSAGE('The Minimium Load (' & CLIP(LEFT(FORMAT(L_STG:MinimiumLoad,@n-12.2))) & ') for this Transporter and Vehicle Composition has not been met.||Would you still like to change the status to Loaded?', 'State Change', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
         OF BUTTON:No
            TRI:State          = 0
   .  .  .
                                                                                                                                    

   IF TRI:State = 2
      IF L_DL:State_Progression = 1 AND TRI:DepartDate = 0
         TRI:DepartDate     = TODAY()
         TRI:DepartTime     = CLOCK()
      .  

      IF TRI:DepartDate = 0
         CASE MESSAGE('To set state On Route please supply a Depart Date & Time.||Would you like to set the Depart Date & Time to today and now?', 'State Change', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
         OF BUTTON:Yes
            TRI:DepartDate     = TODAY()
            TRI:DepartTime     = CLOCK()
         OF BUTTON:No
            TRI:State          = 1
            SELECT(?TRI:DepartDate)
            L:Cycle = TRUE
      .  .

      IF TRI:State = 2
         IF L_SG:Notifications = TRUE
            Manifest_Emails_Setup(TRI:TRID,13)     ! Send emails
         .

         CASE MESSAGE('Would you like to print the Trip Sheet now?', 'Print Trip Sheet', ICON:Question, '1 Copy|2 Copies|No', 3)
         OF 1
            IF TRI:Collections = TRUE
               Print_TripSheet_Collections(TRI:TRID)
            ELSE
               Print_TripSheet(TRI:TRID)
            .
         OF 2
            LOOP 2 TIMES
               IF TRI:Collections = TRUE
                  Print_TripSheet_Collections(TRI:TRID)
               ELSE
                  Print_TripSheet(TRI:TRID)
   .  .  .  .  .

               
   IF TRI:State = 3
!       IF TRI:ReturnedDate = 0 AND L_DL:State_Progression ~= 1
!          TRI:State             = 2
!          MESSAGE('Please specify a Returned Date & Time.', 'State Change', ICON:Exclamation)
!
!          ENABLE(?TRI:ReturnedDate)
!          ENABLE(?TRI:ReturnedTime)
!          ?TRI:ReturnedDate{PROP:ReadOnly}  = FALSE
!          ?TRI:ReturnedTime{PROP:ReadOnly}  = FALSE
!          SELECT(?TRI:ReturnedDate)
!       ELSE
         IF Access:TripSheets.Update() = LEVEL:Benign
            MESSAGE('State changed to Transferred.', 'Update Trip Sheet', ICON:Asterisk)
            POST(EVENT:Accepted,?OK)            ! Close the window
   .  .  !.


   IF TRI:State = 4                                            ! Finalised
      ! Check through all Items and make sure they all have Delivered Dates and Times
      ! Check that all Items have been completed
      L_STG:Incomplete_DIID_List    = ''
      L_STG:No_Incomplete           = Get_TripDelItems_Incomplete(TRI:TRID, L_STG:Incomplete_DIID_List)
      IF L_STG:No_Incomplete > 0
         ! Collect details to show the user....
         L_STG:Item_List            = ''
         LOOP
            IF CLIP(L_STG:Incomplete_DIID_List) = ''
               BREAK
            .

            DELI:DIID      = Get_1st_Element_From_Delim_Str(L_STG:Incomplete_DIID_List, ',', TRUE)
            IF Access:DeliveryItems.TryFetch(DELI:PKey_DIID) = LEVEL:Benign
               DEL:DID     = DELI:DID
               IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
                  IF CLIP(L_STG:Item_List) = ''
                     L_STG:Item_List  = 'DI No.: ' & DEL:DINo & ',  Item No.: ' & DELI:ItemNo & ', DI Date: ' & FORMAT(DEL:DIDate,@d6)
                  ELSE
                     L_STG:Item_List  = CLIP(L_STG:Item_List) & '|' & 'DI No.: ' & DEL:DINo & ',  Item No.: ' & DELI:ItemNo & ', DI Date: ' & FORMAT(DEL:DIDate,@d6)
               .  .
            ELSE
               MESSAGE('Problem getting DIID: ' & DELI:DIID)
      .  .  .

      IF L_STG:No_Incomplete > 0
         TRI:State    = 3
         MESSAGE('There are Trip Sheet Items which are misssing Delivery Dates and/or Delivered Units.|' & |
                 'Please click on the Trip Sheet Items and the change button to edit.|Or go to the Tripsheet Browse and click the button at the bottom labeled Capture POD<39>s.||' & |
                    CLIP(L_STG:Item_List) , 'Trip Sheet Items Incomplete', ICON:Exclamation)
      ELSE
         CASE MESSAGE('Are you sure?||No editing of the Trip Sheet will be allowed once finalised.', 'Finalise Trip Sheet', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
         OF BUTTON:YES
           IF L_SG:Notifications = TRUE
              Manifest_Emails_Setup(TRI:TRID,14)     ! Send emails
           .   
         OF BUTTON:No
            TRI:State = 3
   .  .  .

   IF L:Cycle = FALSE AND TRI:State < 2 AND L_DL:Last_State ~= TRI:State                          ! If indeed we did change states, then automate
      IF L_DL:State_Progression = 1
         POST(EVENT:Accepted, ?Button_Next)
      ELSIF L_DL:State_Progression = 2
         CASE MESSAGE('State has been advanced.||Would you like to advance the state of this Trip Sheet again?', 'Prompt for State Progression', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            POST(EVENT:Accepted, ?Button_Next)
   .  .  .

   IF L:Cycle = FALSE
      IF Access:TripSheets.Update() <> Level:Benign        ! always save it
         MESSAGE('Could not update the Tripsheet.||Error: ' & ERROR())
      .
                 
      DO Load_State
   .
         
   DISPLAY
   RETURN(L:Cycle)
! ----------------------------------------------------------------------------------------
Clock_Class.MouseClick       PROCEDURE()
    CODE
    SELF.Active     = TRUE
    RETURN

Clock_Class.MouseOut         PROCEDURE()
    CODE
    IF SELF.Active = TRUE
       SELF.XPos    = MOUSEX()
       SELF.YPos    = MOUSEY()

       SELF.DrawLine()
    .
    SELF.Active     = FALSE
    RETURN

Clock_Class.MouseMove        PROCEDURE()
    CODE
    IF SELF.Active = TRUE
       SELF.XPos    = MOUSEX()
       SELF.YPos    = MOUSEY()

       SELF.DrawLine()
    .
    RETURN

Clock_Class.DrawLine         PROCEDURE()

H2      DECIMAL(10.4)
W2      DECIMAL(10.4)
L2      DECIMAL(10.4)
H       DECIMAL(10.4)
W       DECIMAL(10.4)

    CODE
    H2                  = ABS(SELF.YPos - SELF.CYPos)
    W2                  = ABS(SELF.XPos - SELF.CXPos)
    L2                  = SQRT((H2 * H2) + (W2 * W2))

    H                   = (H2 * SELF.LWidth) / L2

    W                   = SQRT((SELF.LWidth * SELF.LWidth) - (H * H))

    IF SELF.YPos - SELF.CYPos < 0
       H                = -H
    .

    IF (SELF.XPos - SELF.CXPos) < 0
       W                = -W
    .

    ?Line1{PROP:Width}  = W
    ?Line1{PROP:Height} = H
    RETURN


Clock_Class.Init             PROCEDURE(LONG RegCtl)
    CODE
    SELF.CXPos          = RegCtl{PROP:XPos} + (RegCtl{PROP:Width} / 2)
    SELF.CYPos          = RegCtl{PROP:YPos} + (RegCtl{PROP:Height} / 2)

    ?Line1{PROP:XPos}   = SELF.CXPos
    ?Line1{PROP:YPos}   = SELF.CYPos

    SELF.LWidth         = ?Line1{PROP:Width}
    RETURN

BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END
  SELF.ViewControl = ?View_TripSheet                       ! Setup the control used to initiate view only mode


BRW2.SetQueueRecord PROCEDURE

  CODE
      ! DELI:Units
      ! DELI:Type
      ! DELI:ContainerNo
      ! PACK:Packaging
  
      CASE DELI:Type
      OF 0                                ! Container
         LOC:Item_Description     = 'Container: ' & DELI:ContainerNo
      OF 1                                ! Loose
         IF LEN(CLIP(TRDI:UnitsLoaded)) > 3
            LOC:Item_Description  = TRDI:UnitsLoaded & ' (of ' & DELI:Units & ') X ' & CLIP(PACK:Packaging)
         ELSE
            LOC:Item_Description  = RIGHT(TRDI:UnitsLoaded,3) & ' (of ' & DELI:Units & ') X ' & CLIP(PACK:Packaging)
      .  .
  
  
  
      ! TRU:Registration
      ! VMM:MakeModel
      ! VMM:Capacity
      !L_BG:Vehicle_Desc           = CLIP(VMM:MakeModel) & ' - ' & CLIP(TRU:Registration)  & ', Capacity: ' &  VMM:Capacity
  
  
  
  
      ! Get Invoice
      str_"       = Get_Invoices(0, 4, 1,, DELI:DID,,, 2)
      INV:IID     = Get_1st_Element_From_Delim_Str(str_", ',', TRUE)
      IF Access:_Invoice.TryFetch(INV:PKey_IID) ~= LEVEL:Benign
         CLEAR(INV:Record)
      .
      
  
      ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info  , p:LogName)
      !   1           2       3           4          5       6          7        8            9                 10        11          12
      ! (LONG  , BYTE    , BYTE=0, BYTE=0        , ULONG=0, <LONG>  , BYTE=0  , BYTE=0    , BYTE=0           , BYTE=0  , <STRING>, <STRING>),STRING
      ! p:Option
      !   0.  - Total Charges inc.    (default)
      !   1.  - Excl VAT
      !   2.  - VAT
      !   3.  - Kgs
      !   4.  - IIDs
      ! p:Type
      !   0.  - All                   (default)
      !   1.  - Invoices                                  Total >= 0.0
      !   2.  - Credit Notes                              Total < 0.0    Instead of <, should be invoices with A_INV:CR_IID
      !   3.  - Credit Notes excl Bad Debts               Total < 0.0
      !   4.  - Credit Notes excl Journals                Total < 0.0
      !   5.  - Credit Notes excl Bad Debts & Journals    Total < 0.0
      ! p:Limit_On    &    p:ID
      !   0.  - Date [& Branch]       (default)
      !   1.  - MID
      !   2.  - DID
      ! p:Manifest_Option
      !   0.  - None                  (default)
      !   1.  - Overnight
      !   2.  - Broking
      ! p:Not_Manifest
      !   0.  - All                   (default)
      !   1.  - Not Manifest
      !   2.  - Manifest
  PARENT.SetQueueRecord
  


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Top, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Top
  SELF.SetStrategy(?Group_Trans, Resize:FixRight+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Trans
  SELF.SetStrategy(?Group_Extra, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Extra
  SELF.SetStrategy(?Group_LoadedWeight, Resize:FixRight+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_LoadedWeight
  SELF.SetStrategy(?Prompt_Change, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Prompt_Change

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_TripSheetDeliveries_Load PROCEDURE (p:DI_No)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Screen_Vars      GROUP,PRE(L_SV)                       !
Reg_Make_Model       STRING(100)                           !Registration & Make / Model
Capacity             DECIMAL(6)                            !In Kgs
Loaded_Capacity      DECIMAL(6)                            !In Kgs
Remaining_Capacity   DECIMAL(6)                            !In Kgs
No_Items             USHORT                                !Number of units
Reload_Deliveries    ULONG                                 !
Last_DIID_Set        ULONG                                 !
Actual_Weight_Total  DECIMAL(8,2)                          !In kg's
Actual_Units         ULONG                                 !
Selected_Weight_All  DECIMAL(8,2)                          !The weigtht of the selected items
Selected_Item_Units_Weight DECIMAL(8,2)                    !In kg's
Floor                STRING(35)                            !Floor Name
DI_No_Locator        ULONG                                 !Delivery Instruction Number
Release_Status       BYTE(1)                               !Show release status on browse
Only_Released        BYTE                                  !Only show released DI's
From_Date            DATE                                  !
To_Date              DATE                                  !
Search_Mode          BYTE                                  !Search Mode only
Search_DIs           STRING(20)                            !
                     END                                   !
LOC:Item_Loaded_Group GROUP,PRE(L_ILG)                     !
DINo                 ULONG                                 !Delivery Instruction Number
DIDate               DATE                                  !DI Date
Journey              STRING(70)                            !Description
ItemNo               USHORT                                !Item Number
Commodity            STRING(35)                            !Commodity
                     END                                   !
LOC:Item_Desc        STRING(150)                           !
LOC:Config_Vars      GROUP,PRE(L_CV)                       !
FID                  ULONG                                 !Floor ID
DID                  ULONG                                 !Delivery ID
DI_No                ULONG                                 !Delivery Instruction Number
DIID                 ULONG                                 !Delivery Item ID
DID_Loaded_Del       ULONG                                 !DID of selected loaded Delivery Item
                     END                                   !
L_DG:Terms           STRING(20)                            !Terms
L_DG:Delivered       STRING(20)                            !Delivered Status - this has maintenance function - Not Delivered, Partially Delivered, Delivered
L_DG:ReleaseStatus   STRING(20)                            !Is this cargo ok to be loaded
L_DG:Release_State   LONG                                  !
L_IG:Item_Desc       STRING(150)                           !
L_IG:Item_Weight     DECIMAL(8,2)                          !In kg's
L_IG:Item_Units      USHORT                                !Number of units
LOC:Manifest_High_State_Group GROUP,PRE(L_MHS)             !
MIDs                 STRING(200)                           !
DIIDs                STRING(500)                           !
                     END                                   !
LOC:Manifest_State_Q QUEUE,PRE(L_MSQ)                      !
MID                  ULONG                                 !Manifest ID
DIID                 ULONG                                 !Delivery Item ID
                     END                                   !
Search_Group         GROUP,PRE(L_SG)                       !
Tripsheets_Info      STRING(200)                           !
                     END                                   !
BRW12::View:Browse   VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:Insure)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:Manifested)
                       PROJECT(DEL:Delivered)
                       PROJECT(DEL:FID)
                       PROJECT(DEL:DIDateAndTime)
                       PROJECT(DEL:SID)
                       PROJECT(DEL:CRTID)
                       PROJECT(DEL:JID)
                       PROJECT(DEL:CID)
                       JOIN(FloorsAlias,'DEL:FID = A_FLO:FID')
                       END
                       JOIN(INV:FKey_DID,DEL:DID)
                         PROJECT(INV:IID)
                       END
                       JOIN(SERI:PKey_SID,DEL:SID)
                         PROJECT(SERI:ServiceRequirement)
                         PROJECT(SERI:SID)
                       END
                       JOIN(CRT:PKey_CRTID,DEL:CRTID)
                         PROJECT(CRT:ClientRateType)
                         PROJECT(CRT:CRTID)
                       END
                       JOIN(JOU:PKey_JID,DEL:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List_Deliveries
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:DINo_Style         LONG                           !Field style
INV:IID                LIKE(INV:IID)                  !List box control field - type derived from field
INV:IID_Style          LONG                           !Field style
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
L_DG:ReleaseStatus     LIKE(L_DG:ReleaseStatus)       !List box control field - type derived from local data
L_DG:ReleaseStatus_Style LONG                         !Field style
SERI:ServiceRequirement LIKE(SERI:ServiceRequirement) !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
L_DG:Terms             LIKE(L_DG:Terms)               !List box control field - type derived from local data
CRT:ClientRateType     LIKE(CRT:ClientRateType)       !List box control field - type derived from field
L_DG:Delivered         LIKE(L_DG:Delivered)           !List box control field - type derived from local data
DEL:Insure             LIKE(DEL:Insure)               !List box control field - type derived from field
DEL:Insure_Icon        LONG                           !Entry's icon ID
DEL:DIDate             LIKE(DEL:DIDate)               !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !List box control field - type derived from field
DEL:Manifested         LIKE(DEL:Manifested)           !Browse hot field - type derived from field
DEL:Delivered          LIKE(DEL:Delivered)            !Browse hot field - type derived from field
DEL:FID                LIKE(DEL:FID)                  !Browse hot field - type derived from field
DEL:DIDateAndTime      LIKE(DEL:DIDateAndTime)        !Browse hot field - type derived from field
SERI:SID               LIKE(SERI:SID)                 !Related join file key field - type derived from field
CRT:CRTID              LIKE(CRT:CRTID)                !Related join file key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB10::View:FileDrop VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FBNFloor)
                       PROJECT(FLO:FID)
                     END
FDB11::View:FileDrop VIEW(DeliveryItems)
                       PROJECT(DELI:Units)
                       PROJECT(DELI:ItemNo)
                       PROJECT(DELI:ContainerNo)
                       PROJECT(DELI:VolumetricWeight)
                       PROJECT(DELI:Weight)
                       PROJECT(DELI:DIID)
                       PROJECT(DELI:PTID)
                       PROJECT(DELI:CMID)
                       JOIN(PACK:PKey_PTID,DELI:PTID)
                         PROJECT(PACK:Packaging)
                         PROJECT(PACK:PTID)
                       END
                       JOIN(COM:PKey_CMID,DELI:CMID)
                         PROJECT(COM:Commodity)
                         PROJECT(COM:CMID)
                       END
                     END
Queue:FileDrop:1     QUEUE                            !Queue declaration for browse/combo box using ?L_SV:Floor
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:FBNFloor           LIKE(FLO:FBNFloor)             !List box control field - type derived from field
FLO:FBNFloor_Icon      LONG                           !Entry's icon ID
FLO:FID                LIKE(FLO:FID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:2     QUEUE                            !Queue declaration for browse/combo box using ?L_IG:Item_Desc
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
L_IG:Item_Desc         LIKE(L_IG:Item_Desc)           !List box control field - type derived from local data
L_IG:Item_Units        LIKE(L_IG:Item_Units)          !List box control field - type derived from local data
L_IG:Item_Weight       LIKE(L_IG:Item_Weight)         !List box control field - type derived from local data
DELI:Units             LIKE(DELI:Units)               !List box control field - type derived from field
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !Browse hot field - type derived from field
DELI:VolumetricWeight  LIKE(DELI:VolumetricWeight)    !Browse hot field - type derived from field
DELI:Weight            LIKE(DELI:Weight)              !Browse hot field - type derived from field
PACK:Packaging         LIKE(PACK:Packaging)           !Browse hot field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Browse hot field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Browse hot field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::TRDI:Record LIKE(TRDI:RECORD),THREAD
BRW12::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW12::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW12::PopupChoice   SIGNED                       ! Popup current choice
BRW12::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW12::PopupChoiceExec BYTE(0)                    ! Popup executed
QuickWindow          WINDOW('Form Trip Sheet Deliveries'),AT(,,368,321),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('UpdateManifestLoad'),SYSTEM
                       GROUP,AT(4,308,155,10),USE(?Group_Bottom2)
                         PROMPT('TRID:'),AT(5,308),USE(?TRDI:TRID:Prompt)
                         STRING(@N_10),AT(27,308),USE(TRDI:TRID),RIGHT(1)
                         PROMPT('TDID:'),AT(75,308),USE(?TRDI:TDID:Prompt)
                         STRING(@n_10),AT(97,308),USE(TRDI:TDID),RIGHT(1)
                       END
                       SHEET,AT(4,4,360,298),USE(?CurrentTab),WIZARD
                         TAB,USE(?Tab2)
                           GROUP,AT(9,10,352,64),USE(?Group_Top)
                             PROMPT('Vehicle Composition:'),AT(13,10),USE(?L_SV:Reg_Make_Model:Prompt),TRN
                             ENTRY(@s100),AT(89,10,121,10),USE(L_SV:Reg_Make_Model),COLOR(00E9E9E9h),MSG('Registrati' & |
  'on & Make / Model'),READONLY,SKIP,TIP('Registration & Make / Model')
                             PROMPT('Capacity:'),AT(238,13),USE(?Capacity:Prompt),TRN
                             ENTRY(@n-8.0),AT(309,10,54,10),USE(L_SV:Capacity),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                             LINE,AT(14,26,349,0),USE(?Line1:2),COLOR(COLOR:Black),LINEWIDTH(2)
                             PROMPT('Deliveries on Floor:'),AT(13,31),USE(?Prompt3),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                             LIST,AT(89,31,121,10),USE(L_SV:Floor),VSCROLL,DROP(15),FORMAT('80L(2)|M~Floor~@s35@12L(' & |
  '2)|MI~FBN Floor~@p p@'),FROM(Queue:FileDrop:1)
                             PROMPT('Remaining Capacity:'),AT(238,31),USE(?Remaining_Capacity:Prompt),TRN
                             ENTRY(@n-8.0),AT(309,31,54,10),USE(L_SV:Remaining_Capacity),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                             PROMPT('Search:'),AT(13,47,57,10),USE(?DI_No_Locator:Prompt),COLOR(00D5FFFFh)
                             BUTTON,AT(73,47,12,10),USE(?Button_Clear_SearchDI),ICON('cancel2.ico'),TIP('Clear searc' & |
  'hed DI No.')
                             ENTRY(@S20),AT(89,47,52,10),USE(L_SV:Search_DIs),LEFT(1),COLOR(00D5FFFFh),TIP('Searches:<0DH>' & |
  '<0AH> DI No., Client Name, Client Ref., Client No.<0DH,0AH,0DH,0AH>Items must have b' & |
  'een Manifested and transferred.')
                             BUTTON,AT(142,47,12,10),USE(?Button_Search),ICON('RefreshI.ico'),TIP('Search now.<0DH,0AH>' & |
  'If the Search is on a DI No., then pressing this button<0DH,0AH>will give a reason f' & |
  'or the DI not being available to a Tripsheet.')
                             CHECK(' Search Mode'),AT(157,47),USE(L_SV:Search_Mode),TIP('Search Mode only'),TRN
                             CHECK('Only Released'),AT(225,47),USE(L_SV:Only_Released),LEFT,MSG('Only show released DI''s'), |
  TIP('Only show released DI''s'),TRN
                             CHECK(' Release Status'),AT(297,47),USE(L_SV:Release_Status),LEFT,MSG('Show release sta' & |
  'tus on browse'),TIP('Show release status on browse'),TRN
                             PROMPT('From Date:'),AT(11,63),USE(?From_Date:Prompt),TRN
                             BUTTON('...'),AT(73,63,12,10),USE(?Calendar)
                             SPIN(@d5b),AT(89,63,64,10),USE(L_SV:From_Date),TIP('Filter DI''s from this date (clear ' & |
  'for no from date)')
                             BUTTON,AT(162,63,12,10),USE(?Button_DayB),ICON('�<02H>�<07FH>'),TIP('Back a day')
                             BUTTON,AT(177,63,12,10),USE(?Button_DayF),ICON('�<02H>�<07FH>'),TIP('Forward a day')
                             BUTTON,AT(194,63,12,10),USE(?Button_LastWeek),ICON('Calendar.ico'),TIP('Last week')
                             PROMPT('To Date:'),AT(238,63),USE(?To_Date:Prompt),TRN
                             BUTTON('...'),AT(281,63,12,10),USE(?Calendar:2)
                             SPIN(@d5b),AT(298,63,64,10),USE(L_SV:To_Date),TIP('Filter DI''s to this date (clear for' & |
  ' no to date)')
                           END
                           GROUP,AT(9,231,352,70),USE(?Group_Bottom)
                             PROMPT('Delivery Items:'),AT(13,236),USE(?Prompt3:3),TRN
                             LIST,AT(89,236,131,10),USE(L_IG:Item_Desc),HVSCROLL,DROP(15,300),FORMAT('60L(2)|M~Commo' & |
  'dity~@s35@76L(2)|M~Pack. / Container No.~@s150@36R(2)|M~Units Left~L@n6@44R(2)|M~Wei' & |
  'ght Left~L@n-11.2@40R(2)|M~Total Units~L@n6@24R(2)|M~Item No.~L@n6@'),FROM(Queue:FileDrop:2)
                             PROMPT('Units to Load:'),AT(238,236),USE(?L_SV:No_Items:Prompt),TRN
                             SPIN(@n6),AT(301,236,64,10),USE(L_SV:No_Items),RIGHT(1),HVSCROLL,MSG('Number of units'),STEP(1), |
  TIP('Number of units')
                             BUTTON('Load Max'),AT(89,266,59,12),USE(?Button_AllItems),TIP('On the selected delivery')
                             BUTTON('Load Units'),AT(301,266,59,12),USE(?Button_ThisItem),TIP('Load the item selecte' & |
  'd in Delivery Items with the Units to Load shown')
                             PROMPT('Weight:'),AT(13,250),USE(?Selected_Weight_All:Prompt),TRN
                             ENTRY(@n-11.2),AT(89,250,52,10),USE(L_SV:Selected_Weight_All),RIGHT(1),COLOR(00E9E9E9h),MSG('In kg''s'), |
  READONLY,SKIP,TIP('The weigtht of the selected items<0DH,0AH>In kg''s')
                             PROMPT('Weight:'),AT(238,250),USE(?Selected_Item_Units_Weight:Prompt),TRN
                             ENTRY(@n-11.2),AT(301,250,52,10),USE(L_SV:Selected_Item_Units_Weight),RIGHT(1),COLOR(00E9E9E9h), |
  MSG('In kg''s'),READONLY,SKIP,TIP('Weight of the selected item units<0DH,0AH>In kg''s')
                             LINE,AT(13,282,349,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                             PROMPT('Units Loaded:'),AT(13,287),USE(?TRDI:UnitsLoaded:Prompt:2)
                             ENTRY(@n6),AT(89,287,50,10),USE(TRDI:UnitsLoaded),RIGHT(1),COLOR(00E9E9E9h),MSG('Number of units'), |
  READONLY,SKIP,TIP('Number of units')
                           END
                           LIST,AT(9,79,351,151),USE(?List_Deliveries),HVSCROLL,FORMAT('34R(2)|FMY~DI No.~L@n_10@3' & |
  '4R(2)|MY~Invoice No.~L@n_10@60L(2)|M~Client Name~@s35@32R(2)|M~Client No.~L@n_10b@50' & |
  'L(2)|M~Client Ref.~@s30@56L(2)|MY~Release Status~@s20@48L(2)|M~Service Req.~@s35@70L' & |
  '(2)|M~Journey~@s70@50L(2)|M~Terms~@s20@70L(2)|M~Load Type~@s35@60L(2)|M~Delivered~@s' & |
  '20@26L(2)|MI~Insure~@p p@46R(2)|M~DI Date~L@d6@30R(2)|M~DID~L@n_10@'),FROM(Queue:Browse), |
  IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('&OK'),AT(264,304,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,HIDE,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&OK'),AT(316,304,49,14),USE(?Cancel),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(154,304,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW12::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW_Deliveries       CLASS(BrowseClass)                    ! Browse using ?List_Deliveries
Q                      &Queue:Browse                  !Reference to browse queue
ApplyFilter            PROCEDURE(),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator IncrementalLocatorClass               ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB10                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

FDB11                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:2              !Reference to display queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(SIGNED Field),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Calendar2            CalendarClass
Calendar3            CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
  !------------------------------------
  !Style for ?List_Deliveries
  !------------------------------------
!---------------------------------------------------------------------------
Remaining_Capacity                  ROUTINE
    Time_#  = CLOCK()
    L_SV:Loaded_Capacity       = Get_TripDelItems_Info(TRDI:TRID, 0) / 100
    IF CLOCK() - Time_# > 30
       db.debugout('[Upd._TripSheetDel._Load]  Get_TripDelItems_Info time: ' & CLOCK() - Time_#)
    .

    L_SV:Remaining_Capacity    = L_SV:Capacity - L_SV:Loaded_Capacity

    DISPLAY(?L_SV:Remaining_Capacity)
    EXIT
Load_This_Item                         ROUTINE
    DATA

R:Needed        LIKE(L_SV:Remaining_Capacity)
R:LessAlreadyOn LIKE(L_SV:Remaining_Capacity)
R:Remain        ULONG
R:Loaded        BYTE            ! Something was loaded
R:Exceeds       BYTE

    CODE
    IF L_CV:DID = 0
       MESSAGE('Please select a DI item from the List to load.', 'Load This Item', ICON:Exclamation)
    ELSE
       L_DG:Release_State      = Check_Delivery_Release(, L_CV:DID)
       IF L_DG:Release_State > 1          ! 0 ok or 1 released
          L_DG:Release_State      = Check_Delivery_Release(, L_CV:DID, L_DG:ReleaseStatus)
          MESSAGE('The DI of the selected item needs to be released please see below for the reason.||DI: ' & L_CV:DI_No & |
               '|Release Status: ' & L_DG:ReleaseStatus & ' ' & L_DG:Release_State, 'Load This Item', ICON:Exclamation)
       ELSE
          IF L_SV:No_Items <= 0
             MESSAGE('You have 0 specified in the Units to Load field.', 'Load This Item', ICON:Asterisk)
          ELSE
             ! Check we have capacity
             DELI:DIID            = L_CV:DIID
             IF Access:DeliveryItems.TryFetch(DELI:PKey_DIID) ~= LEVEL:Benign
                MESSAGE('Failed to Fetch Delivery Item.||DIID: ' & L_CV:DIID, 'Load This Item', ICON:Hand)
             ELSE
                R:Needed          = DELI:Weight * (L_SV:No_Items / DELI:Units)

                ! Weight for Del Item
                !R:Needed          = Get_DelItem_s_Totals(L_CV:DID, 0, L_CV:DIID) / 100

                ! Less weight already loaded for this DIID
                ! Weight for this DIID delivered on any Trip Sheet Delivery
                !R:LessAlreadyOn   = Get_DelTripDel_Weight(L_CV:DID, L_CV:DIID) / 100
                !R:Needed         -= R:LessAlreadyOn
                CLEAR(R:Exceeds)

                IF R:Needed > L_SV:Remaining_Capacity
                   R:Exceeds       = TRUE

                   CASE MESSAGE('The capacity of this Truck / Trailer will be exceeded if you load: ' & CLIP(LEFT(FORMAT(R:Needed,@n12.2))) |
                           & ' which is the total weight of the specified no. of items (' & L_SV:No_Items & ') on the selected delivery.' |
                           & '||Do you want to allow this to be loaded now?' |
                            , 'Truck / Trailer', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
                   OF BUTTON:Yes
                      R:Exceeds    = FALSE
                .  .

                IF R:Exceeds = FALSE                   !R:Needed <= L_SV:Remaining_Capacity
                   ! Loop through the Delivery Items

                   ! If previously loaded on this TRDI:TDID we need to add to that entry
                   A_TRDI:TRID           = TRDI:TRID
                   A_TRDI:DIID           = DELI:DIID
                   IF Access:TripSheetDeliveriesAlias.TryFetch(A_TRDI:CKey_TRID_DIID) = LEVEL:Benign
                      IF TRDI:TDID ~= A_TRDI:TDID
                         MESSAGE('This Item is already loaded on this Trip Sheet.||Please modify the other entry.', 'Trip Sheet Deliveries', ICON:Exclamation)
                      ELSE
                         R:Loaded           = TRUE
                         TRDI:UnitsLoaded  += L_SV:No_Items
                         TRDI:DIID          = DELI:DIID
                         IF Access:TripSheetDeliveries.Update() = LEVEL:Benign
                      .  .
                   ELSE
                      R:Loaded              = TRUE
                      TRDI:UnitsLoaded     += L_SV:No_Items
                      TRDI:DIID             = DELI:DIID
                      IF Access:TripSheetDeliveries.Update() = LEVEL:Benign
                   .  .

                   DO Remaining_Capacity

                   L_SV:Reload_Deliveries += 1
                   L_SV:Last_DIID_Set      = 0

                   Upd_Delivery_Del_Status(DELI:DID)

                   IF R:Loaded = TRUE
                      ! We prime the record always - because a cancel will delete it always
                      IF Access:TripSheetDeliveries.PrimeRecord() ~= LEVEL:Benign .
                      ThisWindow.PrimeFields()
   !                   BRW_Deliveries.ResetQueue(Reset:Queue)
   !                   BRW_Deliveries.PostNewSelection
                      !SELECT(?List_Deliveries, R:Q_Pos + 1)

                      CASE MESSAGE('Units of the selected item have been loaded.||Load some more?', 'Trip Sheet Deliveries', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                      OF BUTTON:Yes
                         ! Need to add an additional record then, as we have already updated the current one...
                         BRW_Deliveries.ResetFromBuffer()
                         FDB11.ResetQueue(1)
                         SELECT(?L_IG:Item_Desc,1)
                      OF BUTTON:No
                         POST(EVENT:Accepted, ?Cancel)
                   .  .
                ELSE
   !                MESSAGE('The capacity of this Truck / Trailer would be exceeded if you loaded: ' & CLIP(LEFT(FORMAT(R:Needed,@n12.2))) |
   !                        & ' which is the total weight of the specified no. of items (' & L_SV:No_Items & ') on the selected delivery.' |
   !                         , 'Truck / Trailer', ICON:Hand)
    .  .  .  .  .
    EXIT
Load_Max_Items                        ROUTINE
    DATA

R:CanLoad       LIKE(L_SV:Remaining_Capacity)
R:Remain        LIKE(L_SV:Remaining_Capacity)
R:Needed        LIKE(L_SV:Remaining_Capacity)
R:Load          ULONG
R:Loaded        BYTE

    CODE
    IF L_CV:DID = 0
       MESSAGE('Please select a DI item from the List to load.', 'Load This Item', ICON:Exclamation)
    ELSE
       L_DG:Release_State      = Check_Delivery_Release(, L_CV:DID)
       IF L_DG:Release_State > 1          ! 0 ok or 1 released
          L_DG:Release_State      = Check_Delivery_Release(, L_CV:DID, L_DG:ReleaseStatus)
          MESSAGE('The DI of the selected item needs to be released please see below for the reason.||DI: ' & L_CV:DI_No & |
               '|Release Status: ' & L_DG:ReleaseStatus & ' ' & L_DG:Release_State, 'Load This Item', ICON:Exclamation)
       ELSE
          ! Check we have capacity
          DELI:DIID    = L_CV:DIID
          IF Access:DeliveryItems.TryFetch(DELI:PKey_DIID) ~= LEVEL:Benign
             MESSAGE('Failed to Fetch Delivery Item.||DIID: ' & L_CV:DIID, 'Load Max Items', ICON:Asterisk)
          ELSE
             IF (DELI:Weight / DELI:Units) = 0
                R:CanLoad     = DELI:Units
             ELSE
                R:CanLoad     = L_SV:Remaining_Capacity / (DELI:Weight / DELI:Units)
             .

             R:Remain         = Get_TripDelItemUnDel_Units(L_CV:DIID, 1)     ! Only undelivered and not on incomplete trip
             
             IF R:CanLoad > R:Remain
                R:Load        = R:Remain
             ELSE
                R:Load        = R:CanLoad
             .

             ! Due to rounding R:Load may be slightly higher than possible load..
             LOOP
                IF R:Load <= 0
                   BREAK
                .
                R:Needed      = DELI:Weight * (R:Load / DELI:Units)

                IF R:Needed > L_SV:Remaining_Capacity
                   R:Load    -= 1
                ELSE
                   BREAK
             .  .

             R:Needed         = DELI:Weight * (R:Load / DELI:Units)

          db.debugout('[Update_TripSheetDeliveries_Load]  Needed: ' & R:Needed & ',  R:Load: ' & R:Load)

             IF R:Needed <= L_SV:Remaining_Capacity AND R:Load > 0
                ! Loop through the Delivery Items

                ! If previously loaded on this TRDI:TDID we need to add to that entry
                A_TRDI:TRID           = TRDI:TRID
                A_TRDI:DIID           = DELI:DIID
                IF Access:TripSheetDeliveriesAlias.TryFetch(A_TRDI:CKey_TRID_DIID) = LEVEL:Benign
                   IF TRDI:TDID ~= A_TRDI:TDID
                      MESSAGE('This Item is already loaded on this Trip Sheet.||Please modify the loaded entry.', 'Trip Sheet Deliveries', ICON:Exclamation)
                   ELSE
                      R:Loaded           = TRUE
                      TRDI:UnitsLoaded  += R:Load
                      TRDI:DIID          = DELI:DIID
                      IF Access:TripSheetDeliveries.Update() = LEVEL:Benign
                   .  .
                ELSE
                   R:Loaded              = TRUE
                   TRDI:UnitsLoaded     += R:Load
                   TRDI:DIID             = DELI:DIID
                   IF Access:TripSheetDeliveries.Update() = LEVEL:Benign
                .  .

                DO Remaining_Capacity

                L_SV:Reload_Deliveries += 1
                L_SV:Last_DIID_Set      = 0

                Upd_Delivery_Del_Status(DELI:DID)

                IF R:Loaded = TRUE
                   ! We prime the record always - because a cancel will delete it always
                   IF Access:TripSheetDeliveries.PrimeRecord() ~= LEVEL:Benign .
                   ThisWindow.PrimeFields()

                   CASE MESSAGE('Units of the selected item have been loaded.||Load some more?', 'Trip Sheet Deliveries', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                   OF BUTTON:Yes
                      BRW_Deliveries.ResetFromBuffer()
                      !BRW_Deliveries.PostNewSelection
   !                   SELECT(?List_Deliveries, CHOICE(?List_Deliveries))
                      FDB11.ResetQueue(1)
                      SELECT(?L_IG:Item_Desc,1)
                   OF BUTTON:No
                      POST(EVENT:Accepted, ?Cancel)
                .  .
             ELSE
                IF R:Load <= 0
                   MESSAGE('No Items loaded, 1 unit of this Delivery Item would be overweight.', 'Load Max Items', ICON:Exclamation)
                ELSE
                   MESSAGE('No Items loaded - needed: ' & R:Needed & '||for ' & R:Load & ' units.', 'Load Max Items', ICON:Exclamation)
    .  .  .  .  .
    EXIT
! -------------------------------------------------------------------
Style_Entry                                 ROUTINE
    IF L_SV:Release_Status = TRUE
       IF L_DG:Release_State = 3
          Queue:Browse.DEL:DINo_Style              = 1
          Queue:Browse.INV:IID_Style               = 1
          Queue:Browse.L_DG:ReleaseStatus_Style    = 1
       ELSIF L_DG:Release_State = 4
          Queue:Browse.DEL:DINo_Style              = 2
          Queue:Browse.INV:IID_Style               = 2
          Queue:Browse.L_DG:ReleaseStatus_Style    = 2
       ELSIF INLIST(L_DG:Release_State, '2','5','6','7') > 0
          Queue:Browse.DEL:DINo_Style              = 3
          Queue:Browse.INV:IID_Style               = 3
          Queue:Browse.L_DG:ReleaseStatus_Style    = 3
    .  .
    EXIT
Style_Setup                               ROUTINE
    ! Style:Normal
!    ?list1{PROPSTYLE:FontStyle, 1}     = FONT:Regular
!    ?list1{PROPSTYLE:TextColor, 1}     = COLOR:NONE
!    ?list1{PROPSTYLE:BackColor, 1}     = COLOR:NONE
!    ?list1{PROPSTYLE:TextSelected, 1}  = COLOR:NONE
!    ?list1{PROPSTYLE:BackSelected, 1}  = COLOR:NONE

    ! Style:Header
    !?list1{PROPSTYLE:FontStyle, 1}     = FONT:Bold
    ?List_Deliveries{PROPSTYLE:TextColor, 1}     = 004080FFH
    ?List_Deliveries{PROPSTYLE:BackColor, 1}     = -1
    ?List_Deliveries{PROPSTYLE:TextSelected, 1}  = 009FBEFFH
    ?List_Deliveries{PROPSTYLE:BackSelected, 1}  = -1

    ?List_Deliveries{PROPSTYLE:TextColor, 2}     = 0012BA3FH
    ?List_Deliveries{PROPSTYLE:BackColor, 2}     = -1
    ?List_Deliveries{PROPSTYLE:TextSelected, 2}  = 00FF99CCH
    ?List_Deliveries{PROPSTYLE:BackSelected, 2}  = -1

    ?List_Deliveries{PROPSTYLE:TextColor, 3}     = 00808000H
    ?List_Deliveries{PROPSTYLE:BackColor, 3}     = -1
    ?List_Deliveries{PROPSTYLE:TextSelected, 3}  = 00FFFF80H
    ?List_Deliveries{PROPSTYLE:BackSelected, 3}  = -1
    EXIT
! -------------------------------------------------------------------
Set_Filter                                  ROUTINE
    BRW_Deliveries.SetFilter('DEL:Manifested >= 3 AND DEL:Delivered << 2 AND (DEL:FID = L_CV:FID OR L_CV:FID = 0) AND (A_FLO:TripSheetLoading = 1)')

    IF L_SV:From_Date > 0
       BRW_Deliveries.SetFilter('(0 = L_SV:From_Date OR DEL:DIDate >= L_SV:From_Date)','2')
    ELSE
       BRW_Deliveries.SetFilter('','2')
    .
    IF L_SV:To_Date > 0
       BRW_Deliveries.SetFilter('(0 = L_SV:To_Date OR DEL:DIDate << L_SV:To_Date + 1)','1')
    ELSE
       BRW_Deliveries.SetFilter('','1')
    .

    IF CLIP(L_SV:Search_DIs) ~='' OR L_SV:Search_Mode = TRUE
!       BRW_Deliveries.SetFilter('(DEL:DINo = L_SV:DI_No_Locator)','3')
       IF CLIP(L_SV:Search_DIs) =''
          BRW_Deliveries.SetFilter('0 = 1', '3')
       ELSE
          IF NUMERIC(L_SV:Search_DIs) = TRUE
             BRW_Deliveries.SetFilter('((DEL:DINo = ' & DEFORMAT(L_SV:Search_DIs) & ') OR ' & |
                                   '(SQL(a.ClientReference LIKE <39>%' & CLIP(L_SV:Search_DIs) & '%<39>)) OR ' & |
                                   '(SQL(g.ClientName LIKE <39>%' & CLIP(L_SV:Search_DIs) & '%<39>)) OR ' & |
                                   '(CLI:ClientNo = ' & DEFORMAT(L_SV:Search_DIs) & '))','3')
          ELSE
             BRW_Deliveries.SetFilter('((SQL(a.ClientReference LIKE <39>%' & CLIP(L_SV:Search_DIs) & '%<39>)) OR ' & |
                                   '(SQL(g.ClientName LIKE <39>%' & CLIP(L_SV:Search_DIs) & '%<39>)))','3')
       .  .
    ELSE
       BRW_Deliveries.SetFilter('','3')
    .
    EXIT
! -------------------------------------------------------------------
Search                                      ROUTINE
    DATA
R:Msg                       STRING(500)

    CODE
    IF RECORDS(Queue:Browse) <= 0
       ! Then check if the search term is a valid DI no., if it is give the user a reason for not listed
       DEL:DINo     = DEFORMAT(L_SV:Search_DIs)
       IF Access:Deliveries.TryFetch(DEL:Key_DINo) = LEVEL:Benign
          ! We have a DI for this search term, assume user was looking for DI
          IF DEL:Manifested <= 0
             Upd_Delivery_Man_Status(DEL:DID,,1)                       ! Update the status - DEL:Manifested
          .

          IF DEL:Manifested <= 0
             MESSAGE('The DI ' & DEL:DINo & ' is not on a Manifest.', 'Tripsheet Search', ICON:Hand)
          ELSE
             CLEAR(L_SG:Tripsheets_Info)
                                   ! (<ULONG>, BYTE=0, <ULONG>, <ULONG>, <ULONG>, <*STRING>),ULONG
             IF Get_TripDelItems_Info(, 4, DEL:DID,,, L_SG:Tripsheets_Info) > 0
                R:Msg   = '(TRID''s: ' & CLIP(L_SG:Tripsheets_Info)

                CLEAR(L_SG:Tripsheets_Info)
                IF Get_TripDelItems_Info(, 3, DEL:DID,,, L_SG:Tripsheets_Info) > 0
                .
                L_SG:Tripsheets_Info          = Get_1st_Element_From_Delim_Str(L_SG:Tripsheets_Info, ',')

                R:Msg   = CLIP(R:Msg) & ', Last Del.: ' & CLIP(L_SG:Tripsheets_Info) & ')'
                MESSAGE('The DI ' & DEL:DINo & ' has already been delivered.  ' & CLIP(R:Msg), 'Tripsheet Search', ICON:Hand)
             ELSE
                ! ??? Di should be in list?
    .  .  .  .

    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Trip Sheet Del. Record'
  OF InsertRecord
    ActionMessage = 'Trip Sheet Del. Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Trip Sheet Del. Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_TripSheetDeliveries_Load')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRDI:TRID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_CV:FID',L_CV:FID)                       ! Added by: BrowseBox(ABC)
  BIND('L_SV:To_Date',L_SV:To_Date)               ! Added by: BrowseBox(ABC)
  BIND('L_SV:From_Date',L_SV:From_Date)           ! Added by: BrowseBox(ABC)
  BIND('L_SV:Search_DIs',L_SV:Search_DIs)         ! Added by: BrowseBox(ABC)
  BIND('L_DG:ReleaseStatus',L_DG:ReleaseStatus)   ! Added by: BrowseBox(ABC)
  BIND('L_DG:Terms',L_DG:Terms)                   ! Added by: BrowseBox(ABC)
  BIND('L_DG:Delivered',L_DG:Delivered)           ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:TripSheetDeliveries)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(TRDI:Record,History::TRDI:Record)
  SELF.AddHistoryField(?TRDI:TRID,2)
  SELF.AddHistoryField(?TRDI:TDID,1)
  SELF.AddHistoryField(?TRDI:UnitsLoaded,4)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Deliveries.Open                          ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Relate:FloorsAlias.Open                         ! File FloorsAlias used by this procedure, so make sure it's RelationManager is open
  Relate:TripSheetDeliveriesAlias.Open            ! File TripSheetDeliveriesAlias used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                            ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Access:TripSheets.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleComposition.UseFile               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TripSheetDeliveries
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
    SELF.CancelAction = Cancel:Cancel           ! Modify not to ask user
  BRW_Deliveries.Init(?List_Deliveries,Queue:Browse.ViewPosition,BRW12::View:Browse,Queue:Browse,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    ?L_SV:Reg_Make_Model{PROP:ReadOnly} = True
    DISABLE(?L_SV:Floor)
    DISABLE(?Button_Clear_SearchDI)
    ?L_SV:Search_DIs{PROP:ReadOnly} = True
    DISABLE(?Button_Search)
    DISABLE(?Calendar)
    DISABLE(?Button_DayB)
    DISABLE(?Button_DayF)
    DISABLE(?Button_LastWeek)
    DISABLE(?Calendar:2)
    DISABLE(?L_IG:Item_Desc)
    DISABLE(?Button_AllItems)
    DISABLE(?Button_ThisItem)
    ?L_SV:Selected_Weight_All{PROP:ReadOnly} = True
    ?L_SV:Selected_Item_Units_Weight{PROP:ReadOnly} = True
  END
  BRW_Deliveries.Q &= Queue:Browse
  BRW_Deliveries.AddSortOrder(,DEL:Key_DINo)      ! Add the sort order for DEL:Key_DINo for sort order 1
  BRW_Deliveries.AddLocator(BRW12::Sort0:Locator) ! Browse has a locator for sort order 1
  BRW12::Sort0:Locator.Init(,DEL:DINo,1,BRW_Deliveries) ! Initialize the browse locator using  using key: DEL:Key_DINo , DEL:DINo
  BRW_Deliveries.SetFilter('(DEL:Manifested >= 3 AND DEL:Delivered << 2 AND (DEL:FID = L_CV:FID OR L_CV:FID = 0) AND (A_FLO:TripSheetLoading = 1) AND (0 = L_SV:To_Date OR DEL:DIDate << L_SV:To_Date + 1) AND (0 = L_SV:From_Date OR DEL:DIDate >= L_SV:From_Date))') ! Apply filter expression to browse
  BRW_Deliveries.AddResetField(L_SV:From_Date)    ! Apply the reset field
  BRW_Deliveries.AddResetField(L_SV:Only_Released) ! Apply the reset field
  BRW_Deliveries.AddResetField(L_SV:Reload_Deliveries) ! Apply the reset field
  BRW_Deliveries.AddResetField(L_SV:Search_DIs)   ! Apply the reset field
  BRW_Deliveries.AddResetField(L_SV:Search_Mode)  ! Apply the reset field
  BRW_Deliveries.AddResetField(L_SV:To_Date)      ! Apply the reset field
  ?List_Deliveries{PROP:IconList,1} = '~checkoffdim.ico'
  ?List_Deliveries{PROP:IconList,2} = '~checkon.ico'
  BRW_Deliveries.AddField(DEL:DINo,BRW_Deliveries.Q.DEL:DINo) ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(INV:IID,BRW_Deliveries.Q.INV:IID) ! Field INV:IID is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(CLI:ClientName,BRW_Deliveries.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(CLI:ClientNo,BRW_Deliveries.Q.CLI:ClientNo) ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:ClientReference,BRW_Deliveries.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(L_DG:ReleaseStatus,BRW_Deliveries.Q.L_DG:ReleaseStatus) ! Field L_DG:ReleaseStatus is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(SERI:ServiceRequirement,BRW_Deliveries.Q.SERI:ServiceRequirement) ! Field SERI:ServiceRequirement is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(JOU:Journey,BRW_Deliveries.Q.JOU:Journey) ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(L_DG:Terms,BRW_Deliveries.Q.L_DG:Terms) ! Field L_DG:Terms is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(CRT:ClientRateType,BRW_Deliveries.Q.CRT:ClientRateType) ! Field CRT:ClientRateType is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(L_DG:Delivered,BRW_Deliveries.Q.L_DG:Delivered) ! Field L_DG:Delivered is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:Insure,BRW_Deliveries.Q.DEL:Insure) ! Field DEL:Insure is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:DIDate,BRW_Deliveries.Q.DEL:DIDate) ! Field DEL:DIDate is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:DID,BRW_Deliveries.Q.DEL:DID) ! Field DEL:DID is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:Manifested,BRW_Deliveries.Q.DEL:Manifested) ! Field DEL:Manifested is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:Delivered,BRW_Deliveries.Q.DEL:Delivered) ! Field DEL:Delivered is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:FID,BRW_Deliveries.Q.DEL:FID) ! Field DEL:FID is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:DIDateAndTime,BRW_Deliveries.Q.DEL:DIDateAndTime) ! Field DEL:DIDateAndTime is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(SERI:SID,BRW_Deliveries.Q.SERI:SID) ! Field SERI:SID is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(CRT:CRTID,BRW_Deliveries.Q.CRT:CRTID) ! Field CRT:CRTID is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(JOU:JID,BRW_Deliveries.Q.JOU:JID) ! Field JOU:JID is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(CLI:CID,BRW_Deliveries.Q.CLI:CID) ! Field CLI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_TripSheetDeliveries_Load',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      VCO:VCID                 = TRI:VCID
      IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
         L_SV:Reg_Make_Model   = VCO:CompositionName
                                               ! (p:VCID, p:Option, p:Truck, p:Additional)
         L_SV:Capacity         = Get_VehComp_Info(TRI:VCID, 1)
      .
  
  
      ! Set the floor
      FLO:FID             = Get_Branch_Info(GLO:BranchID, 3)
      IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
         L_SV:Floor       = FLO:Floor
         L_CV:FID         = FLO:FID
      .
  
      L_SV:Release_Status = GETINI('Update_TripSheetDeliveries_Load', 'Release_Status', 1, GLO:Local_INI)
  !    ! test -----------
  !    Time_#  = CLOCK()
  !    _SQLTemp{PROP:SQL}  = 'SELECT  A.DID, A.DINO, A.DIDATEANDTIME, A.BID, A.CID, A.CLIENTREFERENCE, A.JID, A.COLLECTIONAID, ' & |
  !                        'A.DELIVERYAID, A.CRTID, A.LTID, A.SID, A.TRIPSHEETINTID, A.TRIPSHEETOUTTID, A.INSURE, A.FID, A.MANIFESTED, ' & |
  !                        'A.DELIVERED, A.DC_ID, A.DELCID, A.COLLECTEDBYDRID, A.UID, B.FID, B.FLOOR, B.FBNFLOOR, B.AID, B.PRINT_RATES, ' & |
  !                        'B.TRIPSHEETLOADING, C.IID, C.POD_IID, C.CR_IID, C.BID, C.CID, C.DID, C.DINO, C.ICID, C.MID, C.IJID, D.SID, ' & |
  !                        'D.SERVICEREQUIREMENT, E.CRTID, E.CID, E.CLIENTRATETYPE, E.LTID, F.JID, F.JOURNEY, F.BID, F.FID, G.CID, ' & |
  !                        'G.CLIENTNO, G.CLIENTNAME, G.CLIENTSEARCH, G.BID, G.AID, G.ACID ' & |
  !                        'FROM  {{oj ' & |
  !                        'dbo.Deliveries A ' & |
  !                        'LEFT OUTER JOIN dbo.Floors B ON  A.FID =  B.FID ' & |
  !                        'LEFT OUTER JOIN dbo.ServiceRequirements D ON  A.SID= D.SID ' & |
  !                        'LEFT OUTER JOIN dbo._Invoice C ON  A.DID= C.DID ' & |
  !                        'LEFT OUTER JOIN dbo.ClientsRateTypes E ON  A.CRTID= E.CRTID ' & |
  !                        'LEFT OUTER JOIN dbo.Journeys F ON  A.JID= F.JID ' & |
  !                        'LEFT OUTER JOIN dbo.Clients G ON  A.CID= G.CID ' & |
  !                        '} ' & |
  !                        'WHERE (  A.MANIFESTED >= 3 AND  A.DELIVERED < 2 AND ( A.FID = 1 OR 1 = 0) AND  B.TRIPSHEETLOADING = 1 ) ' & |
  !                        'ORDER BY  A.DINO'
  !
  !    MESSAGE('Prop complete - time: ' & CLOCK() - Time_#)
  FDB10.Init(?L_SV:Floor,Queue:FileDrop:1.ViewPosition,FDB10::View:FileDrop,Queue:FileDrop:1,Relate:Floors,ThisWindow)
  FDB10.Q &= Queue:FileDrop:1
  FDB10.AddSortOrder(FLO:Key_Floor)
  FDB10.SetFilter('FLO:TripSheetLoading = 1')
  FDB10.AddField(FLO:Floor,FDB10.Q.FLO:Floor) !List box control field - type derived from field
  FDB10.AddField(FLO:FBNFloor,FDB10.Q.FLO:FBNFloor) !List box control field - type derived from field
  FDB10.AddField(FLO:FID,FDB10.Q.FLO:FID) !Primary key field - type derived from field
  FDB10.AddUpdateField(FLO:FID,L_CV:FID)
  ThisWindow.AddItem(FDB10.WindowComponent)
  ?L_SV:Floor{PROP:IconList,1} = '~checkoffdim.ico'
  ?L_SV:Floor{PROP:IconList,2} = '~checkon.ico'
  FDB11.Init(?L_IG:Item_Desc,Queue:FileDrop:2.ViewPosition,FDB11::View:FileDrop,Queue:FileDrop:2,Relate:DeliveryItems,ThisWindow)
  FDB11.Q &= Queue:FileDrop:2
  FDB11.AddSortOrder(DELI:FKey_DID_ItemNo)
  FDB11.AddRange(DELI:DID,L_CV:DID)
  FDB11.AddField(COM:Commodity,FDB11.Q.COM:Commodity) !List box control field - type derived from field
  FDB11.AddField(L_IG:Item_Desc,FDB11.Q.L_IG:Item_Desc) !List box control field - type derived from local data
  FDB11.AddField(L_IG:Item_Units,FDB11.Q.L_IG:Item_Units) !List box control field - type derived from local data
  FDB11.AddField(L_IG:Item_Weight,FDB11.Q.L_IG:Item_Weight) !List box control field - type derived from local data
  FDB11.AddField(DELI:Units,FDB11.Q.DELI:Units) !List box control field - type derived from field
  FDB11.AddField(DELI:ItemNo,FDB11.Q.DELI:ItemNo) !List box control field - type derived from field
  FDB11.AddField(DELI:ContainerNo,FDB11.Q.DELI:ContainerNo) !Browse hot field - type derived from field
  FDB11.AddField(DELI:VolumetricWeight,FDB11.Q.DELI:VolumetricWeight) !Browse hot field - type derived from field
  FDB11.AddField(DELI:Weight,FDB11.Q.DELI:Weight) !Browse hot field - type derived from field
  FDB11.AddField(PACK:Packaging,FDB11.Q.PACK:Packaging) !Browse hot field - type derived from field
  FDB11.AddField(PACK:PTID,FDB11.Q.PACK:PTID) !Browse hot field - type derived from field
  FDB11.AddField(COM:CMID,FDB11.Q.COM:CMID) !Browse hot field - type derived from field
  FDB11.AddField(DELI:DIID,FDB11.Q.DELI:DIID) !Primary key field - type derived from field
  FDB11.AddUpdateField(DELI:DIID,L_CV:DIID)
  FDB11.AddUpdateField(DELI:DID,L_CV:DID)
  ThisWindow.AddItem(FDB11.WindowComponent)
  BRW_Deliveries.AddToolbarTarget(Toolbar)        ! Browse accepts toolbar control
  BRW_Deliveries.ToolbarItem.HelpButton = ?Help
  BRW12::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW12::FormatManager.Init('MANTRNIS','Update_TripSheetDeliveries_Load',1,?List_Deliveries,12,BRW12::PopupTextExt,Queue:Browse,18,LFM_CFile,LFM_CFile.Record)
  BRW12::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
      DO Style_Setup
      IF p:DI_No ~= 0             ! Then dont load ini date - use just this DINo in filter!
         L_SV:Search_Mode    = 1
         L_SV:Search_DIs     = p:DI_No
      ELSE
         L_SV:From_Date      = GETINI('Update_TripSheetDeliveries_Load', 'From_Date', TODAY()-2, GLO:Local_INI)
         L_SV:To_Date        = GETINI('Update_TripSheetDeliveries_Load', 'To_Date', TODAY(), GLO:Local_INI)
  
         IF GETINI('Update_TripSheetDeliveries_Load', 'Using_Dates', 'W', GLO:Local_INI) = 'W'
            L_SV:From_Date   = TODAY() - 8
            L_SV:To_Date     = TODAY() - 1
         .
  
         L_SV:Search_Mode    = GETINI('Update_TripSheetDeliveries_Load', 'Search_Mode', '', GLO:Local_INI)
      .
      DO Set_Filter
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
    Relate:FloorsAlias.Close
    Relate:TripSheetDeliveriesAlias.Close
    Relate:_SQLTemp.Close
  END
  ! List Format Manager destructor
  BRW12::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_TripSheetDeliveries_Load',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  TRDI:TRID = TRI:TRID
  PARENT.PrimeFields


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  DELI:DIID = TRDI:DIID                                    ! Assign linking field value
  Access:DeliveryItems.Fetch(DELI:PKey_DIID)
  DEL:DID = DELI:DID                                       ! Assign linking field value
  Access:Deliveries.Fetch(DEL:PKey_DID)
  CLI:CID = DEL:CID                                        ! Assign linking field value
  Access:Clients.Fetch(CLI:PKey_CID)
  JOU:JID = DEL:JID                                        ! Assign linking field value
  Access:Journeys.Fetch(JOU:PKey_JID)
  PACK:PTID = DELI:PTID                                    ! Assign linking field value
  Access:PackagingTypes.Fetch(PACK:PKey_PTID)
  COM:CMID = DELI:CMID                                     ! Assign linking field value
  Access:Commodities.Fetch(COM:PKey_CMID)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
          ! Prevent the warning on Form cancel by saving the buffer again now...      - see the init
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Clear_SearchDI
      ThisWindow.Update()
          CLEAR(L_SV:Search_DIs)
          DISPLAY()
    OF ?Button_Search
      ThisWindow.Update()
      BRW_Deliveries.ResetQueue(1)
          DO Search
    OF ?L_SV:Search_Mode
          PUTINI('Update_TripSheetDeliveries_Load', 'Search_Mode', L_SV:Search_Mode, GLO:Local_INI)
          IF L_SV:Search_Mode = TRUE
             L_SV:Floor       = 'All'
             L_CV:FID         = 0
      
             FLO:Floor        = 'All'
             FLO:FID          = 0
             DISPLAY
          .
    OF ?L_SV:Release_Status
          PUTINI('Update_TripSheetDeliveries_Load', 'Release_Status', L_SV:Release_Status, GLO:Local_INI)
    OF ?Calendar
      ThisWindow.Update()
      Calendar2.SelectOnClose = True
      Calendar2.Ask('Select a Date',L_SV:From_Date)
      IF Calendar2.Response = RequestCompleted THEN
      L_SV:From_Date=Calendar2.SelectedDate
      DISPLAY(?L_SV:From_Date)
      END
      ThisWindow.Reset(True)
          PUTINI('Update_TripSheetDeliveries_Load', 'From_Date', L_SV:From_Date, GLO:Local_INI)
      
          PUTINI('Update_TripSheetDeliveries_Load', 'Using_Dates', 'U', GLO:Local_INI)
      
      
    OF ?L_SV:From_Date
          PUTINI('Update_TripSheetDeliveries_Load', 'From_Date', L_SV:From_Date, GLO:Local_INI)
      
          PUTINI('Update_TripSheetDeliveries_Load', 'Using_Dates', 'U', GLO:Local_INI)
      
      
    OF ?Button_DayB
      ThisWindow.Update()
          L_SV:From_Date  -= 1
          L_SV:To_Date    -= 1
      
      
          PUTINI('Update_TripSheetDeliveries_Load', 'To_Date', L_SV:To_Date, GLO:Local_INI)
          PUTINI('Update_TripSheetDeliveries_Load', 'From_Date', L_SV:From_Date, GLO:Local_INI)
      
          PUTINI('Update_TripSheetDeliveries_Load', 'Using_Dates', 'U', GLO:Local_INI)
      
          DISPLAY
    OF ?Button_DayF
      ThisWindow.Update()
          L_SV:From_Date  += 1
          L_SV:To_Date    += 1
      
      
          PUTINI('Update_TripSheetDeliveries_Load', 'To_Date', L_SV:To_Date, GLO:Local_INI)
          PUTINI('Update_TripSheetDeliveries_Load', 'From_Date', L_SV:From_Date, GLO:Local_INI)
      
          PUTINI('Update_TripSheetDeliveries_Load', 'Using_Dates', 'U', GLO:Local_INI)
      
          DISPLAY
    OF ?Button_LastWeek
      ThisWindow.Update()
          L_SV:From_Date  = TODAY() - 8
          L_SV:To_Date    = TODAY() - 1
      
          PUTINI('Update_TripSheetDeliveries_Load', 'Using_Dates', 'W', GLO:Local_INI)
      
          DISPLAY
      
      
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar3.SelectOnClose = True
      Calendar3.Ask('Select a Date',L_SV:To_Date)
      IF Calendar3.Response = RequestCompleted THEN
      L_SV:To_Date=Calendar3.SelectedDate
      DISPLAY(?L_SV:To_Date)
      END
      ThisWindow.Reset(True)
          PUTINI('Update_TripSheetDeliveries_Load', 'To_Date', L_SV:To_Date, GLO:Local_INI)
      
          PUTINI('Update_TripSheetDeliveries_Load', 'Using_Dates', 'U', GLO:Local_INI)
    OF ?L_SV:To_Date
          PUTINI('Update_TripSheetDeliveries_Load', 'To_Date', L_SV:To_Date, GLO:Local_INI)
      
          PUTINI('Update_TripSheetDeliveries_Load', 'Using_Dates', 'U', GLO:Local_INI)
    OF ?L_SV:No_Items
          IF L_SV:Actual_Units ~= 0
             L_SV:Selected_Item_Units_Weight      = L_SV:Actual_Weight_Total  *  (L_SV:No_Items / L_SV:Actual_Units)
          ELSE
             L_SV:Selected_Item_Units_Weight      = 0
          .
          DISPLAY(?L_SV:Selected_Item_Units_Weight)
    OF ?Button_AllItems
      ThisWindow.Update()
          DO Load_Max_Items
    OF ?Button_ThisItem
      ThisWindow.Update()
          DO Load_This_Item
    OF ?OK
      ThisWindow.Update()
          IF TRDI:UnitsLoaded <= 0
             ! Items are specified to load AND no loaded item specified on the current record
             IF L_SV:No_Items ~= 0 AND TRDI:DIID = 0
                QuickWindow{PROP:AcceptAll}  = FALSE
                DO Load_This_Item
      
             ! No item specified on the current record AND nothing loaded (based on remaining capacity)
             ELSIF TRDI:DIID = 0 AND (L_SV:Remaining_Capacity = L_SV:Capacity)
                MESSAGE('Please load some units or click OK to exit.', 'Trip Sheet Delivery - Accept', ICON:Hand)
                QuickWindow{PROP:AcceptAll}  = FALSE
             .
          ELSE
             POST(EVENT:Accepted, ?Cancel)
          .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_SV:Floor
          L_SV:Reload_Deliveries  += 1
    OF ?L_IG:Item_Desc
          GET(Queue:FileDrop:2, CHOICE(?L_IG:Item_Desc))
          IF ~ERRORCODE()
             L_SV:No_Items                     = Queue:FileDrop:2.L_IG:Item_Units
             ?L_SV:No_Items{PROP:RangeHigh}    = Queue:FileDrop:2.L_IG:Item_Units
      
             L_SV:Selected_Weight_All          = Queue:FileDrop:2.L_IG:Item_Weight
             L_SV:Selected_Item_Units_Weight   = Queue:FileDrop:2.L_IG:Item_Weight
             L_SV:Actual_Weight_Total          = Queue:FileDrop:2.DELI:Weight
             L_SV:Actual_Units                 = Queue:FileDrop:2.DELI:Units
          ELSE
             CLEAR(L_SV:Selected_Weight_All)
             CLEAR(L_SV:Selected_Item_Units_Weight)
             CLEAR(L_SV:No_Items)
             CLEAR(L_SV:Actual_Weight_Total)
             CLEAR(L_SV:Actual_Units)
          .
      
          DISPLAY(?L_SV:Selected_Weight_All)
          DISPLAY(?L_SV:Selected_Item_Units_Weight)
          DISPLAY(?L_SV:No_Items)
      
    OF ?L_SV:No_Items
          IF L_SV:Actual_Units ~= 0
             L_SV:Selected_Item_Units_Weight      = L_SV:Actual_Weight_Total  *  (L_SV:No_Items / L_SV:Actual_Units)
          ELSE
             L_SV:Selected_Item_Units_Weight      = 0
          .
          DISPLAY(?L_SV:Selected_Item_Units_Weight)
    OF ?List_Deliveries
      BRW_Deliveries.UpdateViewRecord()
          L_CV:DID    = DEL:DID
          L_CV:DI_No  = DEL:DINo
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          DO Remaining_Capacity
          SELECT(?L_IG:Item_Desc)
      
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          SELECT(?L_SV:Search_DIs)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW_Deliveries.ApplyFilter PROCEDURE

  CODE
      DO Set_Filter
  PARENT.ApplyFilter


BRW_Deliveries.SetQueueRecord PROCEDURE

  CODE
      ! Pre Paid|COD Delivery|COD Pickup|Account
!      EXECUTE DEL:Terms
!         L_DG:Terms       = 'COD Delivery'
!         L_DG:Terms       = 'COD Pickup'
!         L_DG:Terms       = 'Account'
!      ELSE
!         L_DG:Terms       = 'Pre Paid'
!      .
  
    ! Not Delivered|Partially Delivered|Delivered
    EXECUTE DEL:Delivered + 1
       L_DG:Delivered   = 'Not Delivered'
       L_DG:Delivered   = 'Partially Delivered'
       L_DG:Delivered   = 'Delivered'
    ELSE
       L_DG:Delivered   = ''
    .


    L_DG:ReleaseStatus      = ''

    IF L_SV:Release_Status = TRUE
       !   0   - Delivery and Client records loaded  - if called without DID passed
    Time_#  = CLOCK()

       ! (<*STRING>, ULONG=0, <*STRING>),LONG
       ! (p_Statement_Info, p:DID, p_Release_Status_Str)
       L_DG:Release_State      = Check_Delivery_Release(, DEL:DID, L_DG:ReleaseStatus)

    IF CLOCK() - Time_# > 30
       db.debugout('[Upd._TripSheetDel._Load]  Check_Delivery_Release 2  time: ' & CLOCK() - Time_#)
    .

!       EXECUTE L_DG:Release_State
!          L_DG:ReleaseStatus            = 'Released'
!          L_DG:ReleaseStatus            = 'Acc. Pre-Paid'
!          L_DG:ReleaseStatus            = '60 Day+ Balance'
!          L_DG:ReleaseStatus            = 'Account Limit'
!          L_DG:ReleaseStatus            = 'Acc. On Hold'
!          L_DG:ReleaseStatus            = 'Acc. Closed'
!          L_DG:ReleaseStatus            = 'Acc. Dormant'
    .  !.
  PARENT.SetQueueRecord
  
  SELF.Q.DEL:DINo_Style = 0 ! 
  SELF.Q.INV:IID_Style = 0 ! 
  SELF.Q.L_DG:ReleaseStatus_Style = 0 ! 
  IF (DEL:Insure = 1)
    SELF.Q.DEL:Insure_Icon = 2                             ! Set icon from icon list
  ELSE
    SELF.Q.DEL:Insure_Icon = 1                             ! Set icon from icon list
  END
      IF DEL:ReleasedUID ~= 0
      ELSE
         DO Style_Entry
      .
  


BRW_Deliveries.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW12::LastSortOrder <> NewOrder THEN
     BRW12::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW12::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_Deliveries.TakeNewSelection PROCEDURE

  CODE
  IF BRW12::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW12::PopupTextExt = ''
        BRW12::PopupChoiceExec = True
        BRW12::FormatManager.MakePopup(BRW12::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW12::PopupTextExt = '|-|' & CLIP(BRW12::PopupTextExt)
        END
        BRW12::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW12::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW12::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW12::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW12::PopupChoiceOn AND BRW12::PopupChoiceExec THEN
     BRW12::PopupChoiceExec = False
     BRW12::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW12::PopupTextExt)
     IF BRW12::FormatManager.DispatchChoice(BRW12::PopupChoice)
     ELSE
     END
  END


BRW_Deliveries.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
LOC:Filtered        BYTE
LOC:DID             LIKE(DEL:DID)
  CODE
      ! Check if there are any Items on this DID that have been Manifested (in filter) AND Transferred.
      ! Transferred will required checking the Manifest(s) that the Items are on.
      ! Then check that the item is not on another TripSheet pending delivery or already delivered.
  
      ! Filter:       DEL:Manifested >= 3 AND DEL:Delivered < 2 AND (DEL:FID = L_CV:FID OR L_CV:FID = 0)
  
      LOC:DID                 = DEL:DID
  
      Time_#  = CLOCK()
      ! Loading, Loaded, On Route, Transferred
      IF Get_DelMan_State(LOC:DID, L_MHS:MIDs, L_MHS:DIIDs) < 3       ! Tranferred
         LOC:Filtered         = TRUE
      .
      IF CLOCK() - Time_# > 30
         db.debugout('[Upd._TripSheetDel._Load]  Get_DelMan_State time: ' & CLOCK() - Time_#)
      .
  
      IF L_SV:Only_Released = TRUE AND LOC:Filtered = FALSE
         Time_#  = CLOCK()
         L_DG:Release_State   = Check_Delivery_Release(, LOC:DID)
      IF CLOCK() - Time_# > 30
         db.debugout('[Upd._TripSheetDel._Load]  Check_Delivery_Release time: ' & CLOCK() - Time_#)
      .
  
         !IF Check_Delivery_Release(, LOC:DID) ~= 1
         IF L_DG:Release_State ~= 1
            LOC:Filtered      = TRUE
      .  .
  
  
      IF LOC:Filtered = FALSE
         ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache)
         ! (ULONG, BYTE, <ULONG>, <*STRING>, BYTE=0),ULONG        - note: Ulong returned
         !
         ! p:Option
         !   0. Weight of all DID items - Volumetric considered
         !   1. Total Items not loaded on the DID    - Manifest
         !   2. Total Items loaded on the DID        - Manifest
         !   3. Total Items on the DID
         !   4. Total Items not loaded on the DID    - Tripsheet
         !   5. Total Items loaded on the DID        - Tripsheet
         !   6. Weight of all DID items - real weight
         !   7. Total Items not delivered on the DID - Tripsheet     (similar to 4)
  
         ! Check we have some Un-Delivered Items
  !       db.debugout('DEL:DINo: ' & DEL:DINo)
  
         Time_#  = CLOCK()
         IF Get_DelItem_s_Totals(LOC:DID, 4) = 0
            LOC:Filtered  = TRUE
  
            db.debugout('[Upd._TripSheetDel._Load]   LOC:DID: ' & LOC:DID & ',  Filtered because all items already loaded.')
         .
        IF CLOCK() - Time_# > 30
         db.debugout('[Upd._TripSheetDel._Load]  Get_DelItem_s_Totals time: ' & CLOCK() - Time_#)
        .
      .
  
  
      IF LOC:Filtered = TRUE
         ReturnValue      = Record:Filtered
      ELSE
         ! Add list of MIDs and DIIDs to Q for later use in Items allowed in Drop
         ! L_MHS:MIDs ignored for now
         LOOP
            IF CLIP(L_MHS:DIIDs) = ''
               BREAK
            .
            L_MSQ:DIID    = Get_1st_Element_From_Delim_Str(L_MHS:DIIDs, ',', TRUE)
            ADD(LOC:Manifest_State_Q, L_MSQ:DIID)
         .
  ReturnValue = PARENT.ValidateRecord()
      .
  BRW12::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Top, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Top
  SELF.SetStrategy(?Group_Bottom, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Bottom
  SELF.SetStrategy(?Group_Bottom2, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Bottom2


FDB10.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      ! Add an All
      Queue:FileDrop:1.FLO:Floor      = 'All'
      GET(Queue:FileDrop:1, Queue:FileDrop:1.FLO:Floor)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop:1)
         Queue:FileDrop:1.FLO:Floor           = 'All'
         Queue:FileDrop:1.FLO:FBNFloor        = 'All'
         !Queue:FileDrop:1.FLO:FBNFloor_Icon
         Queue:FileDrop:1.FLO:FID             = 0
         ADD(Queue:FileDrop:1)
  
  
      IF L_SV:Search_Mode = TRUE
         L_SV:Floor       = 'All'
         L_CV:FID         = 0
  
         FLO:Floor        = 'All'
         FLO:FID          = 0
         DISPLAY
      .
      .
  RETURN ReturnValue


FDB10.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (FLO:FBNFloor = 1)
    SELF.Q.FLO:FBNFloor_Icon = 2
  ELSE
    SELF.Q.FLO:FBNFloor_Icon = 1
  END


FDB11.SetQueueRecord PROCEDURE

  CODE
      ! Hot fields:
      !   DELI:Type
      !   DELI:ContainerNo
      !   PACK:Packaging
      !   DELI:VolumetricWeight
      !   DELI:Weight
      !   DELI:Units
      !   DEL:DINo
  
      !       0. Units not = Delivered Units
      !       1. Units not on trip sheets or rejected = Delivered & Un Delivered on incomplete Trips
      !       2. Units not = Delivered & Rejected
      !       3. Units not on tripsheet or rejected = Delivered & Rejected & Un Delivered on incomplete Trips
  
      L_IG:Item_Units             = Get_TripDelItemUnDel_Units(DELI:DIID, 3)
  
      CASE DELI:Type
      OF 0                                ! Container
         L_IG:Item_Desc           = DELI:ContainerNo
      OF 1                                ! Loose
         L_IG:Item_Desc           = L_IG:Item_Units & ' * ' & PACK:Packaging
      .
  
      L_IG:Item_Weight            = DELI:Weight           * (L_IG:Item_Units / DELI:Units)
  
  
  PARENT.SetQueueRecord
  


FDB11.TakeNewSelection PROCEDURE(SIGNED Field)

  CODE
  PARENT.TakeNewSelection(Field)
      CLEAR(L_SV:Selected_Weight_All)
      CLEAR(L_SV:Selected_Item_Units_Weight)
      CLEAR(L_SV:No_Items)
      CLEAR(L_SV:Actual_Weight_Total)
      CLEAR(L_SV:Actual_Units)
  
      IF L_SV:Last_DIID_Set ~= L_CV:DIID
         L_SV:Last_DIID_Set                   = L_CV:DIID
         GET(Queue:FileDrop:2, CHOICE(?L_IG:Item_Desc))
         IF ~ERRORCODE()
            L_SV:No_Items                     = Queue:FileDrop:2.L_IG:Item_Units
            ?L_SV:No_Items{PROP:RangeHigh}    = Queue:FileDrop:2.L_IG:Item_Units
  
            L_SV:Selected_Weight_All          = Queue:FileDrop:2.L_IG:Item_Weight
            L_SV:Selected_Item_Units_Weight   = Queue:FileDrop:2.L_IG:Item_Weight
  
  
            L_SV:Actual_Weight_Total          = Queue:FileDrop:2.DELI:Weight
            L_SV:Actual_Units                 = Queue:FileDrop:2.DELI:Units
  
            DISPLAY(?L_SV:No_Items)
            DISPLAY(?L_SV:Selected_Weight_All)
            DISPLAY(?L_SV:Selected_Item_Units_Weight)
      .  .


FDB11.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      !       0. Delivered Units
      !       1. Delivered & Un Delivered on incomplete Trips
      !       2. Delivered & Rejected
      !       3. Delivered & Rejected & Un Delivered on incomplete Trips
  
      L_MSQ:DIID          = DELI:DIID
      GET(LOC:Manifest_State_Q, L_MSQ:DIID)
      IF ERRORCODE()
         ReturnValue      = Record:Filtered
      ELSE
         IF Get_TripDelItemUnDel_Units(DELI:DIID, 3) = 0
            ReturnValue   = Record:Filtered
         ELSE
  ReturnValue = PARENT.ValidateRecord()
      .  .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Update_TripSheetDeliveries_h PROCEDURE  (p:DI_No)          ! Declare Procedure
LOC:Request          BYTE                                  !
LOC:DID              ULONG                                 !Delivery ID

  CODE
    LOC:Request     = GlobalRequest

    IF LOC:Request = InsertRecord
       Update_TripSheetDeliveries_Load(p:DI_No)
       IF GlobalResponse = RequestCompleted
       ELSE
       .
    ELSE
       LOC:DID      = DELI:DID
       Update_TripSheetDeliveries()
    .
!       IF GlobalResponse = RequestCompleted
!          IF LOC:Request = DeleteRecord
!             Upd_Delivery_Del_Status(LOC:DID)
!    .  .  .

    RETURN
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Shortages_Damages PROCEDURE (p:Option)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Lookup_Vars      GROUP,PRE(L_LG)                       !
BranchName           STRING(35)                            !Branch Name
Delivery_Driver      STRING(70)                            !Firstname & Surname
Tripsheet_Driver     STRING(70)                            !Firstname & Surname
Client_Consignee     STRING(100)                           !
ClientNo             ULONG                                 !Client No.
Delivery_Items       STRING(256)                           !
Delivery_Address     STRING(100)                           !
Consignee            STRING('''={9} Address Name ={10}'' {115}') !Name of this address
                     END                                   !
LOC:Cur_Group        GROUP,PRE(L_CG)                       !
DID                  ULONG                                 !Delivery ID
                     END                                   !
History::SHO:Record  LIKE(SHO:RECORD),THREAD
QuickWindow          WINDOW('Form Shortages & Damages'),AT(,,409,357),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_Shortages_Damages'),SYSTEM
                       SHEET,AT(4,4,402,334),USE(?CurrentTab),JOIN
                         TAB('&1) General'),USE(?Tab_General)
                           PROMPT('Shortages && Damages ID:'),AT(9,20),USE(?SHO:SDID:Prompt),TRN
                           STRING(@n_10),AT(134,20,66,10),USE(SHO:SDID),RIGHT(1),TRN
                           PROMPT('DI No.:'),AT(9,34),USE(?SHO:DINo:Prompt),TRN
                           BUTTON('...'),AT(117,34,12,10),USE(?CallLookup_Delivery)
                           ENTRY(@n_10),AT(134,34,66,10),USE(SHO:DINo),RIGHT(1),MSG('Delivery Instruction Number'),REQ, |
  TIP('Delivery Instruction Number')
                           PROMPT('Items:'),AT(217,34),USE(?SHO:Items:Prompt:2),TRN
                           BUTTON('...'),AT(241,34,12,10),USE(?CallLookup_DeliveryItems)
                           ENTRY(@s35),AT(257,34,144,10),USE(SHO:Items,,?SHO:Items:2),COLOR(00E9E9E9h),MSG('Delivery i' & |
  'tems that were a problem (item nos.)'),READONLY,SKIP,TIP('Delivery items that were a' & |
  ' problem (item nos.)')
                           PROMPT('POD IID:'),AT(9,47),USE(?SHO:POD_IID:Prompt),TRN
                           ENTRY(@n_10),AT(134,47,66,10),USE(SHO:POD_IID),RIGHT(1),MSG('POD No.'),TIP('POD No.')
                           PROMPT('Packaging:'),AT(217,50),USE(?Delivery_Items:Prompt),TRN
                           TEXT,AT(257,47,144,26),USE(L_LG:Delivery_Items),VSCROLL,BOXED,COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Invoice No.:'),AT(9,63),USE(?SHO:IID:Prompt),TRN
                           ENTRY(@n_10),AT(134,63,66,10),USE(SHO:IID),RIGHT(1),MSG('Invoice ID'),TIP('Invoice ID')
                           PROMPT('Delivery Depot:'),AT(9,79),USE(?SHO:DeliveryDepot:Prompt),TRN
                           ENTRY(@s35),AT(134,79,144,10),USE(SHO:DeliveryDepot),MSG('Delivery Depot'),TIP('Delivery Depot')
                           PROMPT('Collection Branch:'),AT(9,95),USE(?BranchName:Prompt),TRN
                           BUTTON('...'),AT(117,95,12,10),USE(?CallLookup_Branch)
                           ENTRY(@s35),AT(134,95,144,10),USE(L_LG:BranchName),MSG('Branch Name'),REQ,TIP('Branch Name')
                           PROMPT('Collection Driver:'),AT(9,111),USE(?L_LG:Tripsheet_Driver:Prompt),TRN
                           BUTTON('...'),AT(117,111,12,10),USE(?CallLookup_TripSheet_Driver)
                           ENTRY(@s70),AT(134,111,144,10),USE(L_LG:Tripsheet_Driver),COLOR(00E9E9E9h),MSG('Firstname & Surname'), |
  READONLY,SKIP,TIP('Firstname & Surname')
                           PROMPT('Tripsheet DRID:'),AT(306,111),USE(?SHO:TripSheetDriverID:Prompt),TRN
                           STRING(@n_10),AT(358,111),USE(SHO:TripSheetDriverID),RIGHT(1),TRN
                           PROMPT('Delivery Driver:'),AT(9,125),USE(?L_LG:Delivery_Driver:Prompt),TRN
                           BUTTON('...'),AT(117,125,12,10),USE(?CallLookup_Delivery_Driver)
                           ENTRY(@s70),AT(134,125,144,10),USE(L_LG:Delivery_Driver),COLOR(00E9E9E9h),MSG('Firstname & Surname'), |
  READONLY,SKIP,TIP('Firstname & Surname')
                           PROMPT('Delivery DRID:'),AT(306,125),USE(?SHO:DeliveryDriverID:Prompt),TRN
                           STRING(@n_10),AT(358,125),USE(SHO:DeliveryDriverID),RIGHT(1),TRN
                           PROMPT('DI Date:'),AT(9,143),USE(?SHO:DI_Date:Prompt),TRN
                           BUTTON('...'),AT(117,143,12,10),USE(?Calendar_DI_Date)
                           SPIN(@d5b),AT(134,143,66,10),USE(SHO:DI_Date)
                           PROMPT('Fork Lift Driver 1:'),AT(9,159),USE(?SHO:FLDriver1:Prompt),TRN
                           ENTRY(@s35),AT(134,159,144,10),USE(SHO:FLDriver1),MSG('Drivers ID'),TIP('Drivers ID')
                           PROMPT('Fork Lift Driver 2:'),AT(9,173),USE(?SHO:FLDriver2:Prompt),TRN
                           ENTRY(@s35),AT(134,173,144,10),USE(SHO:FLDriver2),MSG('Drivers ID'),TIP('Drivers ID')
                           PROMPT('Create Date:'),AT(286,20),USE(?SHO:Create_Date:Prompt),TRN
                           ENTRY(@d5b),AT(334,20,66,10),USE(SHO:Create_Date),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                           PROMPT('Supervisor 1:'),AT(9,189),USE(?SHO:Supervisor1:Prompt),TRN
                           ENTRY(@s35),AT(134,189,144,10),USE(SHO:Supervisor1)
                           PROMPT('Supervisor 2:'),AT(9,202),USE(?SHO:Supervisor2:Prompt),TRN
                           ENTRY(@s35),AT(134,202,144,10),USE(SHO:Supervisor2)
                           PROMPT('Delivery Address (from DI):'),AT(9,234),USE(?L_LG:Delivery_Address:Prompt),TRN
                           ENTRY(@s100),AT(134,234,267,10),USE(L_LG:Delivery_Address),COLOR(00E9E9E9h),READONLY,SKIP
                           PROMPT('Consignee:'),AT(9,253),USE(?Consignee:Prompt),TRN
                           BUTTON('...'),AT(117,253,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(134,253,144,10),USE(L_LG:Consignee),MSG('Name of this address'),TIP('Name of th' & |
  'is address')
                           PROMPT('Consignee AID:'),AT(306,253),USE(?SHO:ConsigneeAID:Prompt),TRN
                           STRING(@n_10),AT(358,253),USE(SHO:ConsigneeAID),RIGHT(1),TRN
                           PROMPT('Client:'),AT(9,218),USE(?SHO:RemarksOnPOD:Prompt:2),TRN
                           BUTTON('...'),AT(117,218,12,10),USE(?CallLookup_Client)
                           ENTRY(@s100),AT(134,218,144,10),USE(L_LG:Client_Consignee),REQ,TIP('Client name')
                           PROMPT('Client No.:'),AT(286,218),USE(?ClientNo:Prompt),TRN
                           ENTRY(@n_10b),AT(325,218,76,10),USE(L_LG:ClientNo),RIGHT(1),COLOR(00E9E9E9h),MSG('Client No.'), |
  READONLY,SKIP,TIP('Client No.')
                           PROMPT('Remarks On POD:'),AT(9,271),USE(?SHO:RemarksOnPOD:Prompt),TRN
                           TEXT,AT(134,271,267,20),USE(SHO:RemarksOnPOD),VSCROLL
                           PROMPT('Report No. (CRO or DUPR):'),AT(9,298),USE(?SHO:ReportNo:Prompt),TRN
                           ENTRY(@s35),AT(134,298,144,10),USE(SHO:ReportNo),MSG('CRO or DUPR No. (attach copy)'),TIP('CRO or DUP' & |
  'R No. (attach copy)')
                           PROMPT('Report Comments:'),AT(9,311),USE(?SHO:ReportComments:Prompt),TRN
                           TEXT,AT(134,314,267,19),USE(SHO:ReportComments),VSCROLL,MSG('Shortages or Damages'),TIP('Shortage o' & |
  'r Damage Comments on CRO or DUPR')
                         END
                         TAB('&2) Source'),USE(?Tab_Source)
                           CHECK('Damaged'),AT(135,22,56,8),USE(SHO:Damaged),TRN
                           CHECK('Unpacked'),AT(195,22,56,8),USE(SHO:Unpacked),TRN
                           CHECK('Short'),AT(255,22,56,8),USE(SHO:Short),TRN
                           CHECK('Wet'),AT(315,22,56,8),USE(SHO:Wet),TRN
                           CHECK('Taped'),AT(135,33,56,8),USE(SHO:Taped),TRN
                           CHECK('Palletised'),AT(195,33,56,8),USE(SHO:Palletised),TRN
                           CHECK('Loose'),AT(255,33,56,8),USE(SHO:Loose),TRN
                           CHECK('Other'),AT(315,33,56,8),USE(SHO:Other),TRN
                           PROMPT('Condition Comment:'),AT(9,46),USE(?SHO:ConditionComment:Prompt),TRN
                           TEXT,AT(135,46,210,19),USE(SHO:ConditionComment),VSCROLL,MSG('Condition of Cargo when uplifted'), |
  TIP('Condition of Cargo when uplifted<0DH,0AH>eg. taped with yellow tape')
                           PROMPT('Drivers Notation on DI:'),AT(9,70),USE(?SHO:DriversNotationDI:Prompt),TRN
                           TEXT,AT(135,70,210,19),USE(SHO:DriversNotationDI),VSCROLL
                           CHECK('Driver Signed'),AT(135,95,70,8),USE(SHO:DriverSigned),MSG('Did the driver sign t' & |
  'he clients documents'),TIP('Did the driver sign the clients documents'),TRN
                           CHECK('Driver Comments Entered'),AT(249,95,,8),USE(SHO:DriverCommentsEntered),MSG('Did the dr' & |
  'iver insert comments on the clients document'),TIP('Did the driver insert comments o' & |
  'n the clients document'),TRN
                           PROMPT('Driver Comments:'),AT(9,110),USE(?SHO:DriverComments:Prompt),TRN
                           TEXT,AT(135,110,210,19),USE(SHO:DriverComments),VSCROLL,MSG('Driver Comments on Clients' & |
  ' Documents'),TIP('Driver Comments on Clients Documents')
                           PROMPT('Depot Condition Received Dispatch:'),AT(9,135),USE(?SHO:DepotConditionReceivedDispatch:Prompt), |
  TRN
                           TEXT,AT(135,135,210,19),USE(SHO:DepotConditionReceivedDispatch),VSCROLL,MSG('Condition ' & |
  'of goods when received by dispatch'),TIP('Condition of goods when received by dispatch')
                           CHECK('Depot Condition Received'),AT(135,161,,8),USE(SHO:DepotConditionReceived),TIP('Did the co' & |
  'nsignment arrive at the FBN depot in the same condition as the Driver received it'),TRN,MSG('Did the consignment arrive at the FBN depot in the same condition as the Driver received it')
                           CHECK('Depot Supervisor Notation'),AT(246,161,,8),USE(SHO:DepotSupervisorNotation),MSG('If not rec' & |
  'eived in uplift condition by depot'),TIP('If not received in uplift condition by dep' & |
  'ot, did the supervisor make a notation on the drivers DI'),TRN
                           PROMPT('Depot Remarks:'),AT(9,175),USE(?SHO:DepotRemarks:Prompt),TRN
                           TEXT,AT(135,175,210,19),USE(SHO:DepotRemarks),VSCROLL,MSG('Remarks'),TIP('Remarks')
                           CHECK('Depot Warning Issued Driver'),AT(135,199,120,8),USE(SHO:DepotWarningIssuedDriver),MSG('If goods w' & |
  'ere not received by the depot in uplift condition was a warning issued to the driver'), |
  TIP('If goods were not received by the depot in uplift condition was a warning issued' & |
  ' to the driver'),TRN
                         END
                         TAB('&3) Destination'),USE(?Tab_Destination)
                           CHECK('Dest Condition Received - is it the same as Dispatch Depot Condition?'),AT(139,22), |
  USE(SHO:DestConditionReceived),MSG('Is the condition the same as received by dispatch depot'), |
  TIP('Is the condition the same as received by dispatch depot'),TRN
                           CHECK('Dest Short'),AT(139,39,70,8),USE(SHO:DestShort),TRN
                           CHECK('Dest Damaged'),AT(213,39,70,8),USE(SHO:DestDamaged),TRN
                           CHECK('Dest Un Packed'),AT(287,39,70,8),USE(SHO:DestUnPacked),TRN
                           CHECK('Dest Loose'),AT(139,51,70,8),USE(SHO:DestLoose),TRN
                           CHECK('Dest Taped'),AT(213,51,70,8),USE(SHO:DestTaped),TRN
                           CHECK('Dest Palletised'),AT(287,51,70,8),USE(SHO:DestPalletised),TRN
                           CHECK('Dest Further Damages'),AT(139,73,92,8),USE(SHO:DestFurtherDamages),MSG('Were there' & |
  ' further damages'),TIP('Were there further damages'),TRN
                           CHECK('Repacked'),AT(243,73,70,8),USE(SHO:DestFurtherDamages_Repacked),MSG('If further ' & |
  'damages, was cargo Repacked'),TIP('If further damages, was cargo Repacked'),TRN
                           CHECK('Retaped'),AT(319,73,70,8),USE(SHO:DestFurtherDamages_Retaped),MSG('If further da' & |
  'mages, was cargo Retaped'),TIP('If further damages, was cargo Retaped'),TRN
                           PROMPT('Dest Further Damages Remarks:'),AT(9,86),USE(?SHO:DestFurtherDamages_Remarks:Prompt), |
  TRN
                           TEXT,AT(139,86,210,20),USE(SHO:DestFurtherDamages_Remarks),VSCROLL
                           GROUP('Materials Used to Re-Pack'),AT(139,110,235,57),USE(?Group_Repack),BOXED,TRN
                             CHECK(' Straps'),AT(149,126,70,8),USE(SHO:DestFurtherDamages_Materials_Straps),TRN
                             CHECK(' Clear Tape'),AT(225,126,70,8),USE(SHO:DestFurtherDamages_Materials_ClearTape),TRN
                             CHECK(' B/Tape'),AT(299,126,70,8),USE(SHO:DestFurtherDamages_Materials_B_Tape),TRN
                             PROMPT('Other:'),AT(149,150),USE(?SHO:DestFurtherDamages_Materials_Other:Prompt)
                             CHECK(' Boxes'),AT(149,137,70,8),USE(SHO:DestFurtherDamages_Materials_Boxes),TRN
                             ENTRY(@s35),AT(223,150,144,10),USE(SHO:DestFurtherDamages_Materials_Other)
                           END
                           PROMPT('Final Destination Condition:'),AT(9,172),USE(?SHO:FinalDestination_Condition:Prompt), |
  TRN
                           TEXT,AT(139,172,210,20),USE(SHO:FinalDestination_Condition),VSCROLL,MSG('Condition of C' & |
  'argo at final destination'),TIP('Condition of Cargo at final destination')
                           PROMPT('Final Destination Reported:'),AT(9,199),USE(?SHO:FinalDestination_Reported:Prompt), |
  TRN
                           TEXT,AT(139,199,210,20),USE(SHO:FinalDestination_Reported),VSCROLL,MSG('Any damages rep' & |
  'orted and noted by client'),TIP('Any damages reported and noted by client')
                           PROMPT('Final Destination Drivers Comments Off Loading:'),AT(9,225,122,20),USE(?SHO:FinalDestination_DriversCommentsOffLoading:Prompt), |
  TRN
                           TEXT,AT(139,225,210,20),USE(SHO:FinalDestination_DriversCommentsOffLoading),VSCROLL,MSG('Drivers co' & |
  'mments regarding the off loading at client'),TIP('Drivers comments regarding the off' & |
  ' loading at client')
                           CHECK(' Client reported && noted damages?'),AT(139,278),USE(SHO:DestClientReportedDamages), |
  TRN
                           PROMPT('Client Reported Damages Comments:'),AT(9,291),USE(?SHO:DestClientReportedDamagesComments:Prompt), |
  TRN
                           TEXT,AT(139,291,210,20),USE(SHO:DestClientReportedDamagesComments),VSCROLL,BOXED
                         END
                         TAB('&4) Office'),USE(?Tab_Office)
                           CHECK(' Driver Advised POD Endorsed'),AT(135,19,120,8),USE(SHO:DriverAdvised_POD_Endorsed), |
  MSG('Did the driver advise the office that the POD was endorsed'),TIP('Did the driver' & |
  ' advise the office that the POD was endorsed'),TRN
                           PROMPT('Driver Advised Person:'),AT(9,30),USE(?SHO:DriverAdvised_Person:Prompt),TRN
                           ENTRY(@s35),AT(135,30,144,10),USE(SHO:DriverAdvised_Person),MSG('Who the driver advised' & |
  ' of endorsed POD'),TIP('Who the driver advised of endorsed POD')
                           CHECK('Client Phoned'),AT(135,44,70,8),USE(SHO:ClientPhoned),MSG('Did the client phone ' & |
  'any FBN staff member'),TIP('Did the client phone any FBN staff member'),TRN
                           PROMPT('Client Phoned Information:'),AT(9,57),USE(?SHO:ClientPhoned_Information:Prompt),TRN
                           TEXT,AT(135,57,210,20),USE(SHO:ClientPhoned_Information),VSCROLL,MSG('Information from client'), |
  TIP('Information from client')
                           PROMPT('What action should have been taken:'),AT(9,83),USE(?SHO:WhatActionShouldHaveBeenTaken:Prompt), |
  TRN
                           TEXT,AT(135,83,210,20),USE(SHO:WhatActionShouldHaveBeenTaken),VSCROLL,TIP('In your own ' & |
  'words what action should have been taken to prevent this incident'),MSG('In your own words what action should have been taken to prevent this incident')
                           CHECK('Have Warnings Been Issued'),AT(135,111,112,8),USE(SHO:HaveWarningsBeenIssued),MSG('Have any w' & |
  'arnings been issued to FBN staff'),TIP('Have any warnings been issued to FBN staff'),TRN
                           PROMPT('Warning Issued To:'),AT(9,126),USE(?SHO:WarningIssuedTo:Prompt),TRN
                           ENTRY(@s35),AT(135,126,144,10),USE(SHO:WarningIssuedTo)
                           CHECK('Stolen Goods Reported To Police'),AT(135,142,136,8),USE(SHO:StolenGoods_ReportedToPolice), |
  MSG('If goods were stolen has this been reported to the police'),TIP('If goods were s' & |
  'tolen has this been reported to the police'),TRN
                           PROMPT('Police Reference:'),AT(9,151),USE(?SHO:PoliceReference:Prompt),TRN
                           ENTRY(@s35),AT(135,151,144,10),USE(SHO:PoliceReference)
                           PROMPT('Who Thought Reponsible:'),AT(9,174),USE(?SHO:WhoThoughtReponsible:Prompt),TRN
                           ENTRY(@s35),AT(135,174,144,10),USE(SHO:WhoThoughtReponsible),MSG('Who do you think is r' & |
  'esponsible'),TIP('Who do you think is responsible')
                           PROMPT('Any Other Relevant Information:'),AT(9,188),USE(?SHO:AnyOtherRelevantInformation:Prompt), |
  TRN
                           TEXT,AT(135,188,210,20),USE(SHO:AnyOtherRelevantInformation),VSCROLL
                         END
                         TAB('&5) Summary'),USE(?Tab_Summary)
                           PROMPT('Date Investigation Completed:'),AT(9,19),USE(?SHO:DateInvestigationCompleted:Prompt), |
  TRN
                           BUTTON('...'),AT(117,19,12,10),USE(?Calendar_DateCompleted)
                           ENTRY(@d5b),AT(135,19,104,10),USE(SHO:DateInvestigationCompleted)
                           PROMPT('Consignment Collected By:'),AT(9,39),USE(?SHO:ConsignmentCollectedBy:Prompt),TRN
                           ENTRY(@s35),AT(135,39,144,10),USE(SHO:ConsignmentCollectedBy),MSG('(Driver)'),TIP('(Driver)')
                           PROMPT('Supervisor Signed For Cargo:'),AT(9,54),USE(?SHO:SupervisorSignedForCargo:Prompt), |
  TRN
                           ENTRY(@s35),AT(135,54,144,10),USE(SHO:SupervisorSignedForCargo),MSG('Supervisor who sig' & |
  'ned for cargo'),TIP('Supervisor who signed for cargo')
                           PROMPT('Consignment Delivered By:'),AT(9,70),USE(?SHO:ConsignmentDeliveredBy:Prompt),TRN
                           ENTRY(@s35),AT(135,70,144,10),USE(SHO:ConsignmentDeliveredBy),MSG('(Driver)'),TIP('(Driver)')
                           PROMPT('Supervisor Signed For Cargo Delivered:'),AT(9,84),USE(?SHO:SupervisorSignedForCargoDelivered:Prompt), |
  TRN
                           ENTRY(@s35),AT(135,84,144,10),USE(SHO:SupervisorSignedForCargoDelivered),MSG('Supervisor' & |
  ' who signed for cargo to be delivered'),TIP('Supervisor who signed for cargo to be delivered')
                           PROMPT('Cause Of Damage:'),AT(9,99),USE(?SHO:CauseOfDamage:Prompt),TRN
                           ENTRY(@s50),AT(135,99,144,10),USE(SHO:CauseOfDamage)
                           PROMPT('Branch Responsible:'),AT(9,114),USE(?SHO:BranchResponsible:Prompt),TRN
                           ENTRY(@s35),AT(135,114,144,10),USE(SHO:BranchResponsible),MSG('for damage'),TIP('for damage')
                           PROMPT('Local Driver Responsible:'),AT(9,129),USE(?SHO:LocalDriverResponsible:Prompt),TRN
                           ENTRY(@s35),AT(135,129,144,10),USE(SHO:LocalDriverResponsible),MSG('for damage'),TIP('for damage')
                           PROMPT('Long Distance Driver Responsible:'),AT(9,145),USE(?SHO:LongDistanceDriverResponsible:Prompt), |
  TRN
                           ENTRY(@s35),AT(135,145,144,10),USE(SHO:LongDistanceDriverResponsible),MSG('for damage'),TIP('for damage')
                           PROMPT('Client Responsible:'),AT(9,159),USE(?SHO:ClientResponsible:Prompt),TRN
                           ENTRY(@s35),AT(135,159,144,10),USE(SHO:ClientResponsible),MSG('for damage'),TIP('for damage')
                           PROMPT('Supervisor Accepted Damaged Cargo:'),AT(9,174),USE(?SHO:SupervisorAcceptedDamagedCargo:Prompt), |
  TRN
                           ENTRY(@s35),AT(135,174,144,10),USE(SHO:SupervisorAcceptedDamagedCargo)
                           PROMPT('Supervisor Investigating Damages:'),AT(9,190),USE(?SHO:SupervisorInvestigatingDamages:Prompt), |
  TRN
                           ENTRY(@s35),AT(135,190,144,10),USE(SHO:SupervisorInvestigatingDamages)
                           PROMPT('Corrective Action Taken:'),AT(9,206),USE(?SHO:CorrectiveActionTaken:Prompt),TRN
                           TEXT,AT(135,206,265,81),USE(SHO:CorrectiveActionTaken),VSCROLL,BOXED
                           PROMPT('Warnings Issued 1:'),AT(9,291),USE(?SHO:WarningsIssued1:Prompt),TRN
                           ENTRY(@s50),AT(135,291,144,10),USE(SHO:WarningsIssued1)
                           PROMPT('Warnings Issued 2:'),AT(9,307),USE(?SHO:WarningsIssued2:Prompt),TRN
                           ENTRY(@s50),AT(135,307,144,10),USE(SHO:WarningsIssued2)
                           PROMPT('Warnings Issued 3:'),AT(9,321),USE(?SHO:WarningsIssued3:Prompt),TRN
                           ENTRY(@s50),AT(135,321,144,10),USE(SHO:WarningsIssued3)
                         END
                         TAB('Other (hidden)'),USE(?Tab:3),HIDE
                           PROMPT('Items:'),AT(9,167),USE(?SHO:Items:Prompt),TRN
                           ENTRY(@s35),AT(141,167,144,10),USE(SHO:Items),MSG('Delivery items that were a problem (' & |
  'item nos.)'),TIP('Delivery items that were a problem (item nos.)')
                           PROMPT('Tripsheet ID (TRID):'),AT(9,182),USE(?SHO:TRID:Prompt),TRN
                           ENTRY(@N_10),AT(141,182,66,10),USE(SHO:TRID),RIGHT(1),MSG('Tripsheet ID'),TIP('Tripsheet ID')
                           PROMPT('Tripsheet Items:'),AT(9,195),USE(?SHO:TripsheetItems:Prompt),TRN
                           ENTRY(@s35),AT(141,195,144,11),USE(SHO:TripsheetItems),MSG('Tripsheet Items'),TIP('Tripsheet Items')
                         END
                       END
                       BUTTON('&OK'),AT(302,340,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(356,340,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,340,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar11           CalendarClass
Calendar13           CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_DI_Info                          ROUTINE           ! for Inserting...
    ! Load anything needed
    IF QuickWindow{PROP:AcceptAll} = FALSE
       BRA:BID                  = SHO:CollectionBID
       IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
          L_LG:BranchName       = BRA:BranchName
    .  .

    DEL:DINo    = SHO:DINo
    IF Access:Deliveries.TryFetch(DEL:Key_DINo) ~= LEVEL:Benign
       MESSAGE('Delivery not found.||DI No.: ' & SHO:DINo & '||p_Option: ' & p:Option, 'Update_Shortages_Damages - Load_DI_Info', ICON:Hand)
    .

    IF L_CG:DID ~= DEL:DID
       L_CG:DID = DEL:DID

       CLEAR(SHO:Items)
       CLEAR(L_LG:Delivery_Items)


       INV:DID     = DEL:DID
       IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
       .


       !SHO:Items

       ! Cant establish without knowing item
       !SHO:TRID                    =
       !SHO:TripSheetDriverID       =

       !SHO:TripsheetItems
       SHO:IID                     = INV:IID
       SHO:POD_IID                 = INV:POD_IID
       !SHO:CollectionBID

       SHO:DeliveryDriverID        = DEL:CollectedByDRID

       SHO:DI_Date                 = DEL:DIDate
       SHO:DI_Time                 = DEL:DITime

       SHO:ConsigneeCID            = DEL:CID

       CLI:CID                     = DEL:CID
       IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
          L_LG:Client_Consignee    = CLI:ClientName
       .

       DO Load_Screen
    .

    DISPLAY
    EXIT
Lookup_Delivery_Items           ROUTINE
    ! Lookup Delivery Items
    DEL:DINo    = SHO:DINo
    IF Access:Deliveries.TryFetch(DEL:Key_DINo)
    .

    GlobalRequest   = SelectRecord
    Select_DeliveryItems()
    IF GlobalResponse = RequestCompleted
       DO Load_Delivery_Items
    .

    DISPLAY
    EXIT
Load_Delivery_Items               ROUTINE
    ! (p:Add, p:List, p:Delim, p:Option, p:Prefix)
    ! (STRING, *STRING, STRING, BYTE=0, <STRING>)
    Add_to_List(DELI:ItemNo & ' (' & DELI:DIID & ')', SHO:Items, ', ')

    ! Get_DelItems_Info
    ! (p:DIID, p:Option)
    Add_to_List(Get_DelItems_Info(DELI:DIID, 2) & ' (' & Get_DelItems_Info(DELI:DIID, 1) & ')', L_LG:Delivery_Items, ', ')
    EXIT
! --------------------------------------------------
Load_Screen                           ROUTINE
    ! Load screen variables
    BRA:BID                     = SHO:CollectionBID
    IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
       L_LG:BranchName          = BRA:BranchName
    .

    A_DRI:DRID                  = SHO:DeliveryDriverID
    IF Access:DriversAlias.TryFetch(A_DRI:PKey_DRID) = LEVEL:Benign
       L_LG:Delivery_Driver     = A_DRI:FirstNameSurname
    .

    DRI:DRID                    = SHO:TripSheetDriverID
    IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
       L_LG:Tripsheet_Driver    = DRI:FirstNameSurname
    .
    

    CLI:CID                     = SHO:ConsigneeCID
    IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
       L_LG:Client_Consignee    = CLI:ClientName
       L_LG:ClientNo            = CLI:ClientNo
    .

    !L_LG:Delivery_Items  more complicated


    DEL:DID                     = SHO:DID
    IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
       L_LG:Delivery_Address    = Get_Address(DEL:DeliveryAID)
    .

    L_LG:Consignee              = Get_Address(SHO:ConsigneeAID)
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Shortages & Damages Record'
  OF InsertRecord
    ActionMessage = 'Shortages & Damages Rec. Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Shortages & Damages Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Shortages_Damages')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SHO:SDID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Shortages_Damages)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(SHO:Record,History::SHO:Record)
  SELF.AddHistoryField(?SHO:SDID,1)
  SELF.AddHistoryField(?SHO:DINo,4)
  SELF.AddHistoryField(?SHO:Items:2,5)
  SELF.AddHistoryField(?SHO:POD_IID,9)
  SELF.AddHistoryField(?SHO:IID,8)
  SELF.AddHistoryField(?SHO:DeliveryDepot,10)
  SELF.AddHistoryField(?SHO:TripSheetDriverID,12)
  SELF.AddHistoryField(?SHO:DeliveryDriverID,15)
  SELF.AddHistoryField(?SHO:DI_Date,20)
  SELF.AddHistoryField(?SHO:FLDriver1,13)
  SELF.AddHistoryField(?SHO:FLDriver2,16)
  SELF.AddHistoryField(?SHO:Create_Date,24)
  SELF.AddHistoryField(?SHO:Supervisor1,14)
  SELF.AddHistoryField(?SHO:Supervisor2,17)
  SELF.AddHistoryField(?SHO:ConsigneeAID,27)
  SELF.AddHistoryField(?SHO:RemarksOnPOD,28)
  SELF.AddHistoryField(?SHO:ReportNo,29)
  SELF.AddHistoryField(?SHO:ReportComments,30)
  SELF.AddHistoryField(?SHO:Damaged,32)
  SELF.AddHistoryField(?SHO:Unpacked,33)
  SELF.AddHistoryField(?SHO:Short,34)
  SELF.AddHistoryField(?SHO:Wet,35)
  SELF.AddHistoryField(?SHO:Taped,36)
  SELF.AddHistoryField(?SHO:Palletised,37)
  SELF.AddHistoryField(?SHO:Loose,38)
  SELF.AddHistoryField(?SHO:Other,39)
  SELF.AddHistoryField(?SHO:ConditionComment,40)
  SELF.AddHistoryField(?SHO:DriversNotationDI,42)
  SELF.AddHistoryField(?SHO:DriverSigned,43)
  SELF.AddHistoryField(?SHO:DriverCommentsEntered,44)
  SELF.AddHistoryField(?SHO:DriverComments,45)
  SELF.AddHistoryField(?SHO:DepotConditionReceivedDispatch,47)
  SELF.AddHistoryField(?SHO:DepotConditionReceived,48)
  SELF.AddHistoryField(?SHO:DepotSupervisorNotation,49)
  SELF.AddHistoryField(?SHO:DepotRemarks,50)
  SELF.AddHistoryField(?SHO:DepotWarningIssuedDriver,51)
  SELF.AddHistoryField(?SHO:DestConditionReceived,53)
  SELF.AddHistoryField(?SHO:DestShort,55)
  SELF.AddHistoryField(?SHO:DestDamaged,56)
  SELF.AddHistoryField(?SHO:DestUnPacked,57)
  SELF.AddHistoryField(?SHO:DestLoose,58)
  SELF.AddHistoryField(?SHO:DestTaped,59)
  SELF.AddHistoryField(?SHO:DestPalletised,60)
  SELF.AddHistoryField(?SHO:DestFurtherDamages,61)
  SELF.AddHistoryField(?SHO:DestFurtherDamages_Repacked,62)
  SELF.AddHistoryField(?SHO:DestFurtherDamages_Retaped,63)
  SELF.AddHistoryField(?SHO:DestFurtherDamages_Remarks,64)
  SELF.AddHistoryField(?SHO:DestFurtherDamages_Materials_Straps,65)
  SELF.AddHistoryField(?SHO:DestFurtherDamages_Materials_ClearTape,66)
  SELF.AddHistoryField(?SHO:DestFurtherDamages_Materials_B_Tape,67)
  SELF.AddHistoryField(?SHO:DestFurtherDamages_Materials_Boxes,68)
  SELF.AddHistoryField(?SHO:DestFurtherDamages_Materials_Other,69)
  SELF.AddHistoryField(?SHO:FinalDestination_Condition,73)
  SELF.AddHistoryField(?SHO:FinalDestination_Reported,74)
  SELF.AddHistoryField(?SHO:FinalDestination_DriversCommentsOffLoading,75)
  SELF.AddHistoryField(?SHO:DestClientReportedDamages,70)
  SELF.AddHistoryField(?SHO:DestClientReportedDamagesComments,71)
  SELF.AddHistoryField(?SHO:DriverAdvised_POD_Endorsed,77)
  SELF.AddHistoryField(?SHO:DriverAdvised_Person,78)
  SELF.AddHistoryField(?SHO:ClientPhoned,79)
  SELF.AddHistoryField(?SHO:ClientPhoned_Information,80)
  SELF.AddHistoryField(?SHO:WhatActionShouldHaveBeenTaken,81)
  SELF.AddHistoryField(?SHO:HaveWarningsBeenIssued,82)
  SELF.AddHistoryField(?SHO:WarningIssuedTo,83)
  SELF.AddHistoryField(?SHO:StolenGoods_ReportedToPolice,84)
  SELF.AddHistoryField(?SHO:PoliceReference,85)
  SELF.AddHistoryField(?SHO:WhoThoughtReponsible,86)
  SELF.AddHistoryField(?SHO:AnyOtherRelevantInformation,87)
  SELF.AddHistoryField(?SHO:DateInvestigationCompleted,91)
  SELF.AddHistoryField(?SHO:ConsignmentCollectedBy,93)
  SELF.AddHistoryField(?SHO:SupervisorSignedForCargo,94)
  SELF.AddHistoryField(?SHO:ConsignmentDeliveredBy,95)
  SELF.AddHistoryField(?SHO:SupervisorSignedForCargoDelivered,96)
  SELF.AddHistoryField(?SHO:CauseOfDamage,97)
  SELF.AddHistoryField(?SHO:BranchResponsible,98)
  SELF.AddHistoryField(?SHO:LocalDriverResponsible,99)
  SELF.AddHistoryField(?SHO:LongDistanceDriverResponsible,100)
  SELF.AddHistoryField(?SHO:ClientResponsible,101)
  SELF.AddHistoryField(?SHO:SupervisorAcceptedDamagedCargo,102)
  SELF.AddHistoryField(?SHO:SupervisorInvestigatingDamages,103)
  SELF.AddHistoryField(?SHO:CorrectiveActionTaken,104)
  SELF.AddHistoryField(?SHO:WarningsIssued1,105)
  SELF.AddHistoryField(?SHO:WarningsIssued2,106)
  SELF.AddHistoryField(?SHO:WarningsIssued3,107)
  SELF.AddHistoryField(?SHO:Items,5)
  SELF.AddHistoryField(?SHO:TRID,6)
  SELF.AddHistoryField(?SHO:TripsheetItems,7)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:AddressAlias.Open                                 ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  Relate:Shortages_Damages.Open                            ! File Shortages_Damages used by this procedure, so make sure it's RelationManager is open
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Branches.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DriversAlias.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TripSheets.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TripSheetDeliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Shortages_Damages
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup_Delivery)
    ?SHO:DINo{PROP:ReadOnly} = True
    DISABLE(?CallLookup_DeliveryItems)
    ?SHO:Items:2{PROP:ReadOnly} = True
    ?SHO:POD_IID{PROP:ReadOnly} = True
    ?SHO:IID{PROP:ReadOnly} = True
    ?SHO:DeliveryDepot{PROP:ReadOnly} = True
    DISABLE(?CallLookup_Branch)
    ?L_LG:BranchName{PROP:ReadOnly} = True
    DISABLE(?CallLookup_TripSheet_Driver)
    ?L_LG:Tripsheet_Driver{PROP:ReadOnly} = True
    DISABLE(?CallLookup_Delivery_Driver)
    ?L_LG:Delivery_Driver{PROP:ReadOnly} = True
    DISABLE(?Calendar_DI_Date)
    ?SHO:FLDriver1{PROP:ReadOnly} = True
    ?SHO:FLDriver2{PROP:ReadOnly} = True
    ?SHO:Supervisor1{PROP:ReadOnly} = True
    ?SHO:Supervisor2{PROP:ReadOnly} = True
    ?L_LG:Delivery_Address{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?L_LG:Consignee{PROP:ReadOnly} = True
    DISABLE(?CallLookup_Client)
    ?L_LG:Client_Consignee{PROP:ReadOnly} = True
    ?L_LG:ClientNo{PROP:ReadOnly} = True
    ?SHO:ReportNo{PROP:ReadOnly} = True
    ?SHO:DestFurtherDamages_Materials_Other{PROP:ReadOnly} = True
    ?SHO:DriverAdvised_Person{PROP:ReadOnly} = True
    ?SHO:WarningIssuedTo{PROP:ReadOnly} = True
    ?SHO:PoliceReference{PROP:ReadOnly} = True
    ?SHO:WhoThoughtReponsible{PROP:ReadOnly} = True
    DISABLE(?Calendar_DateCompleted)
    ?SHO:DateInvestigationCompleted{PROP:ReadOnly} = True
    ?SHO:ConsignmentCollectedBy{PROP:ReadOnly} = True
    ?SHO:SupervisorSignedForCargo{PROP:ReadOnly} = True
    ?SHO:ConsignmentDeliveredBy{PROP:ReadOnly} = True
    ?SHO:SupervisorSignedForCargoDelivered{PROP:ReadOnly} = True
    ?SHO:CauseOfDamage{PROP:ReadOnly} = True
    ?SHO:BranchResponsible{PROP:ReadOnly} = True
    ?SHO:LocalDriverResponsible{PROP:ReadOnly} = True
    ?SHO:LongDistanceDriverResponsible{PROP:ReadOnly} = True
    ?SHO:ClientResponsible{PROP:ReadOnly} = True
    ?SHO:SupervisorAcceptedDamagedCargo{PROP:ReadOnly} = True
    ?SHO:SupervisorInvestigatingDamages{PROP:ReadOnly} = True
    ?SHO:CorrectiveActionTaken{PROP:ReadOnly} = True
    ?SHO:WarningsIssued1{PROP:ReadOnly} = True
    ?SHO:WarningsIssued2{PROP:ReadOnly} = True
    ?SHO:WarningsIssued3{PROP:ReadOnly} = True
    ?SHO:Items{PROP:ReadOnly} = True
    ?SHO:TRID{PROP:ReadOnly} = True
    ?SHO:TripsheetItems{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Shortages_Damages',QuickWindow)     ! Restore window settings from non-volatile store
      ! (p:Option)
  
      L_CG:DID = SHO:DID
  
      IF p:Option = 1             ! Calling from TripSheet_DeliveryItems
         DEL:DID  = SHO:DID
         IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
            SHO:DINo            = DEL:DINo
            SHO:DID             = DEL:DID
            SHO:CollectionBID   = DEL:BID
  
            IF Access:Shortages_Damages.TryUpdate() = LEVEL:Benign
            .
  
            DO Load_DI_Info
      .  .
  
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  IF ?SHO:DestClientReportedDamages{Prop:Checked}
    ENABLE(?SHO:DestClientReportedDamagesComments)
  END
  IF NOT ?SHO:DestClientReportedDamages{PROP:Checked}
    DISABLE(?SHO:DestClientReportedDamagesComments)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressAlias.Close
    Relate:Shortages_Damages.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Shortages_Damages',QuickWindow)  ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_Deliveries
      Browse_Branches
      Browse_Drivers('','')
      Browse_Drivers_Alias_h('','')
      Browse_Addresses_Alias_h
      Select_Clients
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup_Delivery
      ThisWindow.Update()
      DEL:DINo = SHO:DINo
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        SHO:DINo = DEL:DINo
        SHO:DID = DEL:DID
        SHO:CollectionBID = DEL:BID
      END
      ThisWindow.Reset(1)
          DO Load_DI_Info
    OF ?SHO:DINo
      IF SHO:DINo OR ?SHO:DINo{PROP:Req}
        DEL:DINo = SHO:DINo
        IF Access:Deliveries.TryFetch(DEL:Key_DINo)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            SHO:DINo = DEL:DINo
            SHO:DID = DEL:DID
            SHO:CollectionBID = DEL:BID
          ELSE
            CLEAR(SHO:DID)
            CLEAR(SHO:CollectionBID)
            SELECT(?SHO:DINo)
            CYCLE
          END
        ELSE
          SHO:DID = DEL:DID
          SHO:CollectionBID = DEL:BID
        END
      END
      ThisWindow.Reset()
          DO Load_DI_Info
    OF ?CallLookup_DeliveryItems
      ThisWindow.Update()
          DO Lookup_Delivery_Items
    OF ?CallLookup_Branch
      ThisWindow.Update()
      BRA:BranchName = L_LG:BranchName
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_LG:BranchName = BRA:BranchName
        SHO:CollectionBID = BRA:BID
      END
      ThisWindow.Reset(1)
    OF ?L_LG:BranchName
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_LG:BranchName OR ?L_LG:BranchName{PROP:Req}
          BRA:BranchName = L_LG:BranchName
          IF Access:Branches.TryFetch(BRA:Key_BranchName)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              L_LG:BranchName = BRA:BranchName
              SHO:CollectionBID = BRA:BID
            ELSE
              CLEAR(SHO:CollectionBID)
              SELECT(?L_LG:BranchName)
              CYCLE
            END
          ELSE
            SHO:CollectionBID = BRA:BID
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_TripSheet_Driver
      ThisWindow.Update()
      DRI:FirstNameSurname = L_LG:Tripsheet_Driver
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        L_LG:Tripsheet_Driver = DRI:FirstNameSurname
        SHO:TripSheetDriverID = DRI:DRID
      END
      ThisWindow.Reset(1)
    OF ?L_LG:Tripsheet_Driver
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_LG:Tripsheet_Driver OR ?L_LG:Tripsheet_Driver{PROP:Req}
          DRI:FirstNameSurname = L_LG:Tripsheet_Driver
          IF Access:Drivers.TryFetch(DRI:SKey_FirstNameSurname)
            IF SELF.Run(3,SelectRecord) = RequestCompleted
              L_LG:Tripsheet_Driver = DRI:FirstNameSurname
              SHO:TripSheetDriverID = DRI:DRID
            ELSE
              CLEAR(SHO:TripSheetDriverID)
              SELECT(?L_LG:Tripsheet_Driver)
              CYCLE
            END
          ELSE
            SHO:TripSheetDriverID = DRI:DRID
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_Delivery_Driver
      ThisWindow.Update()
      A_DRI:FirstNameSurname = L_LG:Delivery_Driver
      IF SELF.Run(4,SelectRecord) = RequestCompleted
        L_LG:Delivery_Driver = A_DRI:FirstNameSurname
        SHO:DeliveryDriverID = A_DRI:DRID
      END
      ThisWindow.Reset(1)
    OF ?L_LG:Delivery_Driver
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_LG:Delivery_Driver OR ?L_LG:Delivery_Driver{PROP:Req}
          A_DRI:FirstNameSurname = L_LG:Delivery_Driver
          IF Access:DriversAlias.TryFetch(A_DRI:SKey_FirstNameSurname)
            IF SELF.Run(4,SelectRecord) = RequestCompleted
              L_LG:Delivery_Driver = A_DRI:FirstNameSurname
              SHO:DeliveryDriverID = A_DRI:DRID
            ELSE
              CLEAR(SHO:DeliveryDriverID)
              SELECT(?L_LG:Delivery_Driver)
              CYCLE
            END
          ELSE
            SHO:DeliveryDriverID = A_DRI:DRID
          END
        END
      END
      ThisWindow.Reset()
    OF ?Calendar_DI_Date
      ThisWindow.Update()
      Calendar11.SelectOnClose = True
      Calendar11.Ask('Select a Date',SHO:DI_Date)
      IF Calendar11.Response = RequestCompleted THEN
      SHO:DI_Date=Calendar11.SelectedDate
      DISPLAY(?SHO:DI_Date)
      END
      ThisWindow.Reset(True)
    OF ?CallLookup
      ThisWindow.Update()
      A_ADD:AddressName = L_LG:Consignee
      IF SELF.Run(5,SelectRecord) = RequestCompleted
        L_LG:Consignee = A_ADD:AddressName
        SHO:ConsigneeAID = A_ADD:AID
      END
      ThisWindow.Reset(1)
    OF ?L_LG:Consignee
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_LG:Consignee OR ?L_LG:Consignee{PROP:Req}
          A_ADD:AddressName = L_LG:Consignee
          IF Access:AddressAlias.TryFetch(A_ADD:Key_Name)
            IF SELF.Run(5,SelectRecord) = RequestCompleted
              L_LG:Consignee = A_ADD:AddressName
              SHO:ConsigneeAID = A_ADD:AID
            ELSE
              CLEAR(SHO:ConsigneeAID)
              SELECT(?L_LG:Consignee)
              CYCLE
            END
          ELSE
            SHO:ConsigneeAID = A_ADD:AID
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_Client
      ThisWindow.Update()
      CLI:ClientName = L_LG:Client_Consignee
      IF SELF.Run(6,SelectRecord) = RequestCompleted
        L_LG:Client_Consignee = CLI:ClientName
        SHO:ConsigneeCID = CLI:CID
        L_LG:ClientNo = CLI:ClientNo
      END
      ThisWindow.Reset(1)
    OF ?L_LG:Client_Consignee
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_LG:Client_Consignee OR ?L_LG:Client_Consignee{PROP:Req}
          CLI:ClientName = L_LG:Client_Consignee
          IF Access:Clients.TryFetch(CLI:Key_ClientName)
            IF SELF.Run(6,SelectRecord) = RequestCompleted
              L_LG:Client_Consignee = CLI:ClientName
              SHO:ConsigneeCID = CLI:CID
              L_LG:ClientNo = CLI:ClientNo
            ELSE
              CLEAR(SHO:ConsigneeCID)
              CLEAR(L_LG:ClientNo)
              SELECT(?L_LG:Client_Consignee)
              CYCLE
            END
          ELSE
            SHO:ConsigneeCID = CLI:CID
            L_LG:ClientNo = CLI:ClientNo
          END
        END
      END
      ThisWindow.Reset()
    OF ?SHO:DestClientReportedDamages
      IF ?SHO:DestClientReportedDamages{PROP:Checked}
        ENABLE(?SHO:DestClientReportedDamagesComments)
      END
      IF NOT ?SHO:DestClientReportedDamages{PROP:Checked}
        DISABLE(?SHO:DestClientReportedDamagesComments)
      END
      ThisWindow.Reset()
    OF ?Calendar_DateCompleted
      ThisWindow.Update()
      Calendar13.SelectOnClose = True
      Calendar13.Ask('Select a Date',SHO:DateInvestigationCompleted)
      IF Calendar13.Response = RequestCompleted THEN
      SHO:DateInvestigationCompleted=Calendar13.SelectedDate
      DISPLAY(?SHO:DateInvestigationCompleted)
      END
      ThisWindow.Reset(True)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Browse_Drivers_Alias_h PROCEDURE  (p, p:Type)              ! Declare Procedure

  CODE
    Browse_Drivers(p, p:Type)
    A_DRI:FirstNameSurname  = DRI:FirstNameSurname
    A_DRI:DRID              = DRI:DRID
    
