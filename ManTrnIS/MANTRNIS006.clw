

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS006.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_DeliveryItems PROCEDURE (p:RealMode, p:DID)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Enter_Volume     BYTE                                  !Enter the volume (not calculated)
LOC:Local_Vars       GROUP,PRE(L_LV)                       !
Commodity            STRING(35)                            !Commodity
ContainerOperator    STRING(35)                            !Container Operator
ContainerType        STRING(35)                            !Type
ContainerReturnAddressName STRING(35)                      !Name of this address
Packaging            STRING(35)                            !
CID                  ULONG                                 !Client ID
Container_Info_Not_Available BYTE                          !For containerised this is an option
Volume_Per_Unit      BYTE                                  !Volume is per unit
VolumetricRatio      DECIMAL(8,2)                          !x square cubes weigh this amount
Updated_Child        BYTE                                  !Has the user updated the children at all
Volume_Rounding      BYTE                                  !Round volume value to kgs
VolumetricRatio_FBN  DECIMAL(4,2)                          !x square cubes weigh this amount
                     END                                   !
LOC:Load_Type_Group  GROUP,PRE(L_LT)                       !
TurnIn               BYTE                                  !Container Turn In required for this Load Type
LoadOption           BYTE                                  !Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
ContainerParkStandard BYTE                                 !Container Park Standard Rates
                     END                                   !
LOC:Browse_Totals    GROUP,PRE(L_BT)                       !
TripSheet_Loaded     ULONG                                 !
                     END                                   !
LOC:Item_Comp        GROUP,PRE(L_ICG)                      !
DICID                ULONG                                 !
DIID                 ULONG                                 !Delivery Item ID
Length               DECIMAL(6)                            !Length in metres
Breadth              DECIMAL(6)                            !Breadth in metres
Height               DECIMAL(6)                            !Height in metres
Volume               DECIMAL(8,3)                          !Volume for manual entry (metres cubed)
Volume_Unit          DECIMAL(8,3)                          !Volume of 1 unit
Units                USHORT                                !Number of units
Weight               DECIMAL(8,2)                          !In kg's
VolumetricWeight     DECIMAL(8,2)                          !Weight based on Volumetric calculation (in kgs)
                     END                                   !
LOC:Tot_Comp_Q_Group GROUP,PRE(L_TCG)                      !
Units                USHORT                                !Number of units
Weight               DECIMAL(8,2)                          !In kg's
VolumetricWeight     DECIMAL(8,2)                          !Weight based on Volumetric calculation (in kgs)
Volume               DECIMAL(8,3)                          !Volume for manual entry (metres cubed)
                     END                                   !
LOC:Vol_Ratio_Q      QUEUE,PRE(L_VRQ)                      !
VolumetricRatio_Vol  DECIMAL(8,2)                          !x square cubes weigh this amount
VolumetricRatio      DECIMAL(8,2)                          !x square cubes weigh this amount
Client               STRING(10)                            !
                     END                                   !
LOC:cm_Volume        GROUP,PRE(L_cV)                       !in cm ....
Length               DECIMAL(5)                            !Length in metres
Breadth              DECIMAL(5)                            !Breadth in metres
Height               DECIMAL(5)                            !Height in metres
                     END                                   !
BRW4::View:Browse    VIEW(TripSheetDeliveries)
                       PROJECT(TRDI:TRID)
                       PROJECT(TRDI:UnitsLoaded)
                       PROJECT(TRDI:UnitsDelivered)
                       PROJECT(TRDI:UnitsNotAccepted)
                       PROJECT(TRDI:DeliveredDate)
                       PROJECT(TRDI:DeliveredTime)
                       PROJECT(TRDI:TDID)
                       PROJECT(TRDI:DIID)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
TRDI:TRID              LIKE(TRDI:TRID)                !List box control field - type derived from field
TRDI:UnitsLoaded       LIKE(TRDI:UnitsLoaded)         !List box control field - type derived from field
TRDI:UnitsDelivered    LIKE(TRDI:UnitsDelivered)      !List box control field - type derived from field
TRDI:UnitsNotAccepted  LIKE(TRDI:UnitsNotAccepted)    !List box control field - type derived from field
TRDI:DeliveredDate     LIKE(TRDI:DeliveredDate)       !List box control field - type derived from field
TRDI:DeliveredTime     LIKE(TRDI:DeliveredTime)       !List box control field - type derived from field
TRDI:TDID              LIKE(TRDI:TDID)                !Primary key field - type derived from field
TRDI:DIID              LIKE(TRDI:DIID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(DeliveryItems_Components)
                       PROJECT(DELIC:Length)
                       PROJECT(DELIC:Breadth)
                       PROJECT(DELIC:Height)
                       PROJECT(DELIC:Volume)
                       PROJECT(DELIC:VolumetricWeight)
                       PROJECT(DELIC:Units)
                       PROJECT(DELIC:DICID)
                       PROJECT(DELIC:DIID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
DELIC:Length           LIKE(DELIC:Length)             !List box control field - type derived from field
DELIC:Breadth          LIKE(DELIC:Breadth)            !List box control field - type derived from field
DELIC:Height           LIKE(DELIC:Height)             !List box control field - type derived from field
DELIC:Volume           LIKE(DELIC:Volume)             !List box control field - type derived from field
DELIC:VolumetricWeight LIKE(DELIC:VolumetricWeight)   !List box control field - type derived from field
DELIC:Units            LIKE(DELIC:Units)              !List box control field - type derived from field
DELIC:DICID            LIKE(DELIC:DICID)              !List box control field - type derived from field
DELIC:DIID             LIKE(DELIC:DIID)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(ManifestLoadDeliveries)
                       PROJECT(MALD:MLDID)
                       PROJECT(MALD:MLID)
                       PROJECT(MALD:UnitsLoaded)
                       PROJECT(MALD:DIID)
                       JOIN(MAL:PKey_MLID,MALD:MLID)
                         PROJECT(MAL:MID)
                         PROJECT(MAL:MLID)
                         PROJECT(MAL:TTID)
                         JOIN(TRU:PKey_TTID,MAL:TTID)
                           PROJECT(TRU:Registration)
                           PROJECT(TRU:TTID)
                         END
                         JOIN(MAN:PKey_MID,MAL:MID)
                           PROJECT(MAN:MID)
                           PROJECT(MAN:CreatedDate)
                         END
                       END
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?Browse:6
MAN:MID                LIKE(MAN:MID)                  !List box control field - type derived from field
MALD:MLDID             LIKE(MALD:MLDID)               !List box control field - type derived from field
MALD:MLID              LIKE(MALD:MLID)                !List box control field - type derived from field
MALD:UnitsLoaded       LIKE(MALD:UnitsLoaded)         !List box control field - type derived from field
MAN:CreatedDate        LIKE(MAN:CreatedDate)          !List box control field - type derived from field
TRU:Registration       LIKE(TRU:Registration)         !List box control field - type derived from field
MAL:MID                LIKE(MAL:MID)                  !List box control field - type derived from field
MALD:DIID              LIKE(MALD:DIID)                !Browse key field - type derived from field
MAL:MLID               LIKE(MAL:MLID)                 !Related join file key field - type derived from field
TRU:TTID               LIKE(TRU:TTID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB19::View:FileDropCombo VIEW(Vessels)
                       PROJECT(VES:Vessel)
                       PROJECT(VES:VEID)
                     END
FDCB18::View:FileDropCombo VIEW(PackagingTypes)
                       PROJECT(PACK:Packaging)
                       PROJECT(PACK:PTID)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?DELI:ContainerVessel
VES:Vessel             LIKE(VES:Vessel)               !List box control field - type derived from field
VES:VEID               LIKE(VES:VEID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?L_LV:Packaging
PACK:Packaging         LIKE(PACK:Packaging)           !List box control field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DELI:Record LIKE(DELI:RECORD),THREAD
BRW4::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW4::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW4::PopupChoice    SIGNED                       ! Popup current choice
BRW4::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW4::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW6::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW6::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW6::PopupChoice    SIGNED                       ! Popup current choice
BRW6::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW6::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Delivery Items'),AT(,,373,284),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  IMM,MDI,HLP('Update_DeliveryItems'),SYSTEM
                       SHEET,AT(4,2,366,266),USE(?CurrentTab),JOIN
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('DIID:'),AT(307,4),USE(?DELI:DIID:Prompt),TRN
                           STRING(@n_10),AT(317,4),USE(DELI:DIID),FONT(,7),RIGHT(1),TRN
                           PROMPT('Commodity:'),AT(9,20),USE(?Commodity:Prompt:3),TRN
                           ENTRY(@s35),AT(73,19,137,10),USE(L_LV:Commodity),MSG('Commosity'),REQ,TIP('Commosity')
                           BUTTON('...'),AT(215,18,12,11),USE(?CallLookup:4)
                           PROMPT('Item No.:'),AT(277,20),USE(?DELI:ItemNo:Prompt),TRN
                           SPIN(@n6),AT(317,20,48,10),USE(DELI:ItemNo),RIGHT(1),COLOR(00E9E9E9h),MSG('Item Number'),REQ, |
  SKIP,TIP('Item Number')
                           PROMPT('Packaging:'),AT(9,36),USE(?Commodity:Prompt:2),TRN
                           COMBO(@s35),AT(73,36,137,10),USE(L_LV:Packaging),VSCROLL,DROP(15),FORMAT('140L(2)|M~Pac' & |
  'kaging~@s35@'),FROM(Queue:FileDropCombo:1),IMM
                           PROMPT('Units:'),AT(9,52),USE(?DELI:Units:Prompt),TRN
                           SPIN(@n6),AT(73,52,50,10),USE(DELI:Units),RIGHT(1),MSG('Number of units'),TIP('Number of units')
                           PROMPT('Delivered:'),AT(277,52),USE(?DELI:DeliveredUnits:Prompt),TRN
                           SPIN(@n6),AT(317,52,48,10),USE(DELI:DeliveredUnits),RIGHT(1),COLOR(00E6E6E6h),MSG('Units delivered'), |
  SKIP,TIP('Units delivered')
                           LINE,AT(10,70,354,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('&Weight (kgs):'),AT(9,77),USE(?DELI:Weight:Prompt),TRN
                           ENTRY(@n-11.1b),AT(73,77,50,10),USE(DELI:Weight),RIGHT(1),MSG('In kg''s')
                           CHECK(' Volume Rounding'),AT(206,77),USE(L_LV:Volume_Rounding),MSG('Round volume value to kgs'), |
  TIP('Round volume value to kgs'),TRN
                           CHECK(' Volume Per Unit'),AT(294,77),USE(L_LV:Volume_Per_Unit),MSG('Volume is per unit'),TIP('Volume is per unit'), |
  TRN
                           GROUP,AT(9,93,358,34),USE(?Volume_Group)
                             GROUP,AT(73,93,163,20),USE(?Group_Measure),TRN
                               PROMPT('Length (cm):'),AT(73,93),USE(?DELI:Length:Prompt),TRN
                               ENTRY(@n-7.0b),AT(73,103,38,10),USE(L_cV:Length),RIGHT(1),MSG('Length in metres'),TIP('Length in metres')
                               STRING('X'),AT(117,103),USE(?String1),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                               PROMPT('Breadth (cm):'),AT(129,93),USE(?DELI:Breadth:Prompt),TRN
                               ENTRY(@n-7.0b),AT(129,103,38,10),USE(L_cV:Breadth),RIGHT(1),MSG('Breadth in metres'),TIP('Breadth in metres')
                               STRING('X'),AT(173,103),USE(?String1:2),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                               PROMPT('Height (cm):'),AT(185,93),USE(?DELI:Height:Prompt),TRN
                               ENTRY(@n-7.0b),AT(185,103,38,10),USE(L_cV:Height),RIGHT(1),MSG('Height in metres'),TIP('Height in metres')
                               STRING('='),AT(230,103),USE(?String3),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                             END
                             GROUP,AT(242,93,52,20),USE(?Group_VolTot)
                               PROMPT('Volume (m):'),AT(242,93),USE(?DELI:Volume:Prompt),TRN
                               ENTRY(@n-11.3),AT(242,103,52,10),USE(DELI:Volume_Unit),RIGHT(1),MSG('Volume for manual entry'), |
  TIP('Volume for manual entry (metres cubed)')
                             END
                             PROMPT('Total Volume (m):'),AT(313,93),USE(?DELI:Volume:Prompt:2),TRN
                             ENTRY(@n-11.3),AT(313,103,52,10),USE(DELI:Volume),RIGHT(1),COLOR(00E9E9E9h),READONLY,SKIP, |
  TIP('Volume X Unis if Volume Per Unit specified'),MSG('Volume for manual entry (metres cubed)')
                             PROMPT('Volume Option:'),AT(9,93),USE(?LOC:Enter_Volume:Prompt),TRN
                             LIST,AT(9,103,60,10),USE(LOC:Enter_Volume),DROP(5),FROM('Calculate Vol.|#0|Enter Vol.|#' & |
  '1|Split Vol.|#2'),MSG('Enter the volume (not calculated)'),TIP('Enter the volume (no' & |
  't calculated)')
                             PROMPT('Volumetric Weight:'),AT(9,116),USE(?DELI:VolumetricWeight:Prompt),TRN
                             ENTRY(@n-11.1),AT(73,116,50,10),USE(DELI:VolumetricWeight),RIGHT(1),COLOR(00E9E9E9h),MSG('Weight bas' & |
  'ed on Volumetric calculation'),READONLY,SKIP,TIP('Weight based on Volumetric calculation')
                             BUTTON('S'),AT(138,116,18,10),USE(?Button_Set_Vol_Ratio),TIP('Set volumetric ratio from' & |
  ' Client details')
                             BUTTON('E'),AT(161,116,18,10),USE(?Button_Set_Vol_Ratio:2),TIP('Allow to enter a volume' & |
  'tric ratio')
                             PROMPT('Volumetric Ratio:'),AT(185,116),USE(?DELI:VolumetricRatio:Prompt)
                             ENTRY(@n6.2),AT(242,116,52,10),USE(L_LV:VolumetricRatio_FBN),RIGHT(1),COLOR(00E9E9E9h),MSG('A square c' & |
  'ube is equivalent to this weight'),READONLY,SKIP,TIP('A square cube is equivalent to' & |
  ' this weight')
                             LIST,AT(313,116,52,10),USE(L_LV:VolumetricRatio),RIGHT(1),VSCROLL,DROP(10,150),FORMAT('50R(1)|M~V' & |
  'olumetric Ratio~L(2)@n-11.2@50R(1)|M~Kg''s per cubic metre~L(2)@n-11.2@40L(1)|M~Clie' & |
  'nt~L(2)@s10@'),FROM(LOC:Vol_Ratio_Q),TIP('Metre''s cubed that = 1 ton')
                           END
                           LINE,AT(10,130,354,0),USE(?Line1:2),COLOR(COLOR:Black),LINEWIDTH(2)
                           SHEET,AT(9,135,358,128),USE(?Sheet2)
                             TAB('Container'),USE(?Tab_Container)
                               CHECK(' Container Info Not Available'),AT(257,151),USE(L_LV:Container_Info_Not_Available), |
  MSG('For containerised this is an option'),TIP('For containerised this is an option'),TRN
                               GROUP('Container Information'),AT(13,159,350,100),USE(?Group_Container),BOXED
                                 GROUP,AT(19,169,202,10),USE(?Group_ContainerType)
                                   PROMPT('Type:'),AT(19,169),USE(?L_LV:ContainerType:Prompt),TRN
                                   BUTTON('...'),AT(61,169,12,10),USE(?CallLookup:2)
                                   ENTRY(@s35),AT(77,169,130,10),USE(L_LV:ContainerType),MSG('Type'),TIP('Type')
                                 END
                                 GROUP,AT(19,183,290,70),USE(?Group_Container_Sub)
                                   PROMPT('Operator:'),AT(19,183),USE(?ContainerOperator:Prompt),TRN
                                   BUTTON('...'),AT(61,183,12,10),USE(?CallLookup)
                                   ENTRY(@s35),AT(77,183,130,10),USE(L_LV:ContainerOperator),MSG('Container Operator'),TIP('Container Operator')
                                   PROMPT('Container No.:'),AT(19,197),USE(?DELI:ContainerNo:Prompt),TRN
                                   ENTRY(@s35),AT(77,197,130,10),USE(DELI:ContainerNo)
                                   PROMPT('Seal No.:'),AT(19,210),USE(?DELI:SealNo:Prompt),TRN
                                   ENTRY(@s35),AT(77,210,130,10),USE(DELI:SealNo),MSG('Container Seal no.'),TIP('Container Seal no.')
                                   PROMPT('Vessel:'),AT(19,226),USE(?DELI:ContainerVessel:Prompt:2),TRN
                                   COMBO(@s35),AT(77,226,130,10),USE(DELI:ContainerVessel),VSCROLL,DROP(15),FORMAT('200L(2)|M~' & |
  'Vessel~@s50@'),FROM(Queue:FileDropCombo),IMM,MSG('Vessel this container arrived on')
                                   PROMPT('ETA:'),AT(227,226),USE(?DELI:ETA:Prompt),TRN
                                   SPIN(@d6),AT(249,226,60,10),USE(DELI:ETA),RIGHT(1),MSG('Estimated time of arrival for t' & |
  'his Vessel'),TIP('Estimated time of arrival for this Vessel')
                                   GROUP,AT(19,242,202,10),USE(?Group_TurnIn)
                                     PROMPT('Turn In:'),AT(19,242),USE(?ContainerReturnAddressName:Prompt),TRN
                                     BUTTON('...'),AT(61,242,12,10),USE(?CallLookup:3)
                                     ENTRY(@s35),AT(77,242,130,10),USE(L_LV:ContainerReturnAddressName),MSG('Name of this address'), |
  TIP('Name of this address')
                                   END
                                 END
                                 PROMPT(''),AT(213,171,96,36),USE(?Prompt_ContainerInfo),CENTER,TRN
                                 CHECK(' &Show on Invoice'),AT(173,151),USE(DELI:ShowOnInvoice),MSG('Show these containe' & |
  'r details on the Invoice generated from this DI'),TIP('Show these container details ' & |
  'on the Invoice generated from this DI'),TRN
                               END
                             END
                             TAB('Item Components'),USE(?Tab_Split_Vol)
                               LIST,AT(13,154,300,78),USE(?List),VSCROLL,FORMAT('32R(1)|M~Length~C(0)@n8@32R(1)|M~Brea' & |
  'dth~C(0)@n8@32R(1)|M~Height~C(0)@n8@44R(1)|M~Volume~C(0)@n-11.3@64R(1)|M~Volumetric ' & |
  'Weight~C(0)@n-11.1@38R(1)|M~Units~C(0)@n6@40R(1)|M~DICID~C(0)@n_10@'),FROM(Queue:Browse), |
  IMM,MSG('Browsing Records')
                               BUTTON('Add'),AT(319,153,42,12),USE(?Button_Add)
                               BUTTON('Update'),AT(319,167,42,12),USE(?Button_Update)
                               BUTTON('Delete'),AT(319,182,42,12),USE(?Button_Delete)
                               PROMPT('Total Units:'),AT(319,209,39,10),USE(?Units:Prompt:2)
                               ENTRY(@n6),AT(319,222,39,10),USE(L_TCG:Units),RIGHT(1),COLOR(00E9E9E9h),MSG('Number of units'), |
  READONLY,SKIP,TIP('Number of units')
                               PROMPT('Length (cm):'),AT(13,236),USE(?Length:Prompt),TRN
                               PROMPT('Breadth (cm):'),AT(65,236),USE(?Breadth:Prompt),TRN
                               PROMPT('Height (cm):'),AT(117,236),USE(?Height:Prompt),TRN
                               PROMPT('Volume (m):'),AT(213,236),USE(?Volume:Prompt),TRN
                               ENTRY(@n8b),AT(13,247,38,10),USE(L_ICG:Length),RIGHT(1),MSG('Length in cm'),TIP('Length in cm')
                               ENTRY(@n8b),AT(65,247,38,10),USE(L_ICG:Breadth),RIGHT(1),MSG('Breadth in cm'),TIP('Breadth in cm')
                               ENTRY(@n8b),AT(117,247,38,10),USE(L_ICG:Height),RIGHT(1),MSG('Height in cm'),TIP('Height in cm')
                               SPIN(@n6),AT(177,247,30,10),USE(L_ICG:Units),RIGHT(1),MSG('Number of units'),TIP('Number of units')
                               ENTRY(@n-11.3),AT(213,247,33,10),USE(L_ICG:Volume),RIGHT(1),COLOR(00E9E9E9h),MSG('Volume for' & |
  ' manual entry'),READONLY,SKIP,TIP('Volume for manual entry (metres cubed)')
                               PROMPT('Vol. Weight:'),AT(257,236),USE(?VolumetricWeight:Prompt),TRN
                               ENTRY(@n-11.1),AT(257,247,39,10),USE(L_ICG:VolumetricWeight),RIGHT(1),COLOR(00E9E9E9h),MSG('Weight bas' & |
  'ed on Volumetric calculation'),READONLY,SKIP,TIP('Weight based on Volumetric calcula' & |
  'tion (in kgs)')
                               BUTTON('&Insert'),AT(187,135,42,12),USE(?Insert),HIDE,TIP('Insert Delivery Component')
                               BUTTON('&Change'),AT(231,135,42,12),USE(?Change),HIDE
                               BUTTON('&Delete'),AT(275,135,42,12),USE(?Delete),HIDE
                               PROMPT('Units:'),AT(177,236),USE(?Units:Prompt),TRN
                             END
                           END
                         END
                         TAB('&2) Trip Sheet Deliveries'),USE(?Tab:4)
                           LIST,AT(7,22,358,226),USE(?Browse:4),HVSCROLL,FORMAT('40R(2)|M~Trip Sheet ID (TRID)~L@N' & |
  '_10@[36R(2)|M~Loaded~C(0)@n6@36R(2)|M~Delivered~C(0)@n6@52R(2)|M~Not Accepted~L@n6@]' & |
  '|M~Units~[48R(2)|M~Date~C(0)@d6b@36R(2)|M~Time~C(0)@t7b@]|M~Deliverd~'),FROM(Queue:Browse:4), |
  IMM,MSG('Browsing the MainfestLoadDeliveries file')
                           PROMPT('Trip Sheet Loaded:'),AT(9,253),USE(?TripSheet_Loaded:Prompt),TRN
                           ENTRY(@n13),AT(77,253,55,10),USE(L_BT:TripSheet_Loaded),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                         END
                         TAB('&3) Mainfest Load Deliveries'),USE(?Tab:5)
                           LIST,AT(7,22,357,242),USE(?Browse:6),HVSCROLL,FORMAT('40R(2)|M~MID~C(0)@n_10@40R(2)|M~M' & |
  'LDID~C(0)@n_10@40R(2)|M~MLID~C(0)@n_10@48R(2)|M~Units Loaded~C(0)@n6@48R(2)|M~Create' & |
  'd Date~C(0)@d6@80L(2)|M~Registration~C(0)@s20@40R(2)|M~MID (Load)~C(0)@n_10@'),FROM(Queue:Browse:6), |
  IMM,MSG('Browsing the MainfestLoadDeliveries file')
                         END
                       END
                       BUTTON('&OK'),AT(264,268,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(317,268,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,268,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW4::LastSortOrder       BYTE
BRW6::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
ResetFromView          PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW2                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
PrimeRecord            PROCEDURE(BYTE SuppressClear = 0),BYTE,PROC,DERIVED
ResetFromView          PROCEDURE(),DERIVED
SetAlerts              PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW6                 CLASS(BrowseClass)                    ! Browse using ?Browse:6
Q                      &Queue:Browse:6                !Reference to browse queue
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB19               CLASS(FileDropComboClass)             ! File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB18               CLASS(FileDropComboClass)             ! File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

LOC:Delivery_Composition_Group GROUP,PRE(L_DCG)
CMID                       LIKE(DELI:CMID)
Type                       LIKE(DELI:Type)
ShowOnInvoice              LIKE(DELI:ShowOnInvoice)
COID                       LIKE(DELI:COID)
CTID                       LIKE(DELI:CTID)
ContainerVessel            LIKE(DELI:ContainerVessel)
ETA                        LIKE(DELI:ETA)
ETA_TIME                   LIKE(DELI:ETA_TIME)
ByContainer                LIKE(DELI:ByContainer)
Length                     LIKE(DELI:Length)
Breadth                    LIKE(DELI:Breadth)
Height                     LIKE(DELI:Height)
Volume                     LIKE(DELI:Volume)
Volume_Unit                LIKE(DELI:Volume_Unit)
PTID                       LIKE(DELI:PTID)
VolumetricRatio            LIKE(DELI:VolumetricRatio)
                         END
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Compute_Vol                       ROUTINE
    DELI:Length         = L_cV:Length  / 100
    DELI:Breadth        = L_cV:Breadth / 100
    DELI:Height         = L_cV:Height  / 100

    CASE LOC:Enter_Volume
    OF 0                            ! Calc. Volume
       DELI:Volume_Unit         = DELI:Length * DELI:Breadth * DELI:Height
    OF 1                            ! Enter Volume
    OF 2                            ! Split vol.
    .

    IF LOC:Enter_Volume ~= 2
       IF L_LV:Volume_Per_Unit = TRUE
          DELI:Volume           = DELI:Volume_Unit * DELI:Units
       ELSE
          DELI:Volume           = DELI:Volume_Unit
       .

       DELI:VolumetricWeight    = DELI:Volume * DELI:VolumetricRatio

       IF L_LV:Volume_Rounding = TRUE
          DELI:Volume           = ROUND(DELI:Volume,1)
    .  .

    !L_cV:Volume         = DELI:Volume
    !L_cV:Volume_Unit    = DELI:Volume_Unit

    DISPLAY
    EXIT
Delivery_Comp_WVol          ROUTINE
    L_ICG:Volume            = (L_ICG:Length / 100) * (L_ICG:Breadth / 100) * (L_ICG:Height / 100)
    L_ICG:Volume_Unit       = L_ICG:Volume


    !DELIC:Volume              = DELIC:Length * DELIC:Breadth * DELIC:Height

    IF L_LV:Volume_Per_Unit = TRUE
       L_ICG:Volume         = L_ICG:Volume * L_ICG:Units
       !DELIC:Volume           = DELIC:Volume * DELIC:Units
    .

    L_ICG:VolumetricWeight  = L_ICG:Volume * DELI:VolumetricRatio
    !DELIC:VolumetricWeight    = DELIC:Volume * DELI:VolumetricRatio


    DISPLAY
    EXIT
Vol_Q                                   ROUTINE
    FREE(LOC:Vol_Ratio_Q)

    L_VRQ:VolumetricRatio_Vol   = 2.5
    L_VRQ:VolumetricRatio       = 1000 / L_VRQ:VolumetricRatio_Vol
    ADD(LOC:Vol_Ratio_Q)

    L_VRQ:VolumetricRatio_Vol   = 3
    L_VRQ:VolumetricRatio       = 1000 / L_VRQ:VolumetricRatio_Vol
    ADD(LOC:Vol_Ratio_Q)

    L_VRQ:VolumetricRatio_Vol   = 4
    L_VRQ:VolumetricRatio       = 1000 / L_VRQ:VolumetricRatio_Vol
    ADD(LOC:Vol_Ratio_Q)

    L_VRQ:VolumetricRatio_Vol   = 5
    L_VRQ:VolumetricRatio       = 1000 / L_VRQ:VolumetricRatio_Vol
    ADD(LOC:Vol_Ratio_Q)
    EXIT
DelComp_SetSplit                ROUTINE
    ! When the split button is pressed update the records
    CLEAR(DELIC:Record,-1)
    DELIC:DIID  = DELI:DIID
    SET(DELIC:FKey_DIID, DELIC:FKey_DIID)
    LOOP
       IF Access:DeliveryItems_Components.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF DELIC:DIID ~= DELI:DIID
          BREAK
       .

       DELIC:Volume            = (DELIC:Length / 100) * (DELIC:Breadth / 100) * (DELIC:Height / 100)
       DELIC:Volume_Unit       = DELIC:Volume


       IF L_LV:Volume_Per_Unit = TRUE
          DELIC:Volume         = DELIC:Volume * DELIC:Units
       .

       DELIC:VolumetricWeight  = DELIC:Volume * DELI:VolumetricRatio
       IF Access:DeliveryItems_Components.TryUpdate() ~= LEVEL:Benign
    .  .

    BRW2.ResetFromFile()
    BRW2.ResetFromView()
    EXIT
Enter_Vol_Check                 ROUTINE
    CASE LOC:Enter_Volume
    OF 0                        ! Calc. Volume
    OF 1                        ! Enter Volume
    OF 2                        ! Split vol.
       ! Set Del Item Vol.
       DO DelComp_SetSplit

       DELI:Volume_Unit      = L_TCG:Volume                             ! Rounded if required

       DELI:Volume           = DELI:Volume_Unit

       DELI:VolumetricWeight = DELI:Volume * DELI:VolumetricRatio       !L_TCG:VolumetricWeight
    .
    EXIT
Container_Options                ROUTINE
    IF QuickWindow{PROP:AcceptAll} = FALSE
       IF L_LV:Container_Info_Not_Available = TRUE
          CLEAR(L_LV:ContainerOperator)
          !CLEAR(L_LV:ContainerType)
          CLEAR(L_LV:ContainerReturnAddressName)

          CLEAR(DELI:ContainerNo)
          CLEAR(DELI:ContainerVessel)
          CLEAR(DELI:ContainerReturnAID)
          CLEAR(DELI:COID)
          !CLEAR(DELI:CTID)

          ?L_LV:ContainerOperator{PROP:Req}                = FALSE
          !?L_LV:ContainerType{PROP:Req}                    = FALSE
          ?L_LV:ContainerReturnAddressName{PROP:Req}       = FALSE
          ?DELI:ContainerNo{PROP:Req}                      = FALSE
          ?DELI:ContainerVessel{PROP:Req}                  = FALSE

          DISABLE(?Group_Container_Sub)
       ELSE
          ENABLE(?Group_Container_Sub)

          CLEAR(L_LV:ContainerOperator)
          !CLEAR(L_LV:ContainerType)
          CLEAR(L_LV:ContainerReturnAddressName)

          CLEAR(DELI:ContainerNo)
          CLEAR(DELI:ContainerVessel)
          CLEAR(DELI:ContainerReturnAID)
          CLEAR(DELI:COID)
          !CLEAR(DELI:CTID)

          IF L_LT:LoadOption = 1 OR L_LT:LoadOption = 2             ! Container Park or Container
             ?L_LV:ContainerOperator{PROP:Req}             = TRUE
             !?L_LV:ContainerType{PROP:Req}                 = TRUE
             ?DELI:ContainerNo{PROP:Req}                   = TRUE
             ?DELI:ContainerVessel{PROP:Req}               = TRUE
          .
          IF L_LT:TurnIn = TRUE
             ?L_LV:ContainerReturnAddressName{PROP:Req}    = TRUE
       .  .

       IF DELI:Type = 1                        ! Loose
          ?L_LV:ContainerType{PROP:Req}                    = FALSE
       .

       DISPLAY
    .
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Delivery Items Record'
  OF InsertRecord
    ActionMessage = 'Delivery Items Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Delivery Items Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_DeliveryItems')
  SELF.Request = GlobalRequest                    ! Store the incoming request
      IF p:RealMode = 0                   ! If not passed set to current, this wasnt done until  27 Nov 06
         p:RealMode     = SELF.Request
      .
  
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DELI:DIID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DELI:Record,History::DELI:Record)
  SELF.AddHistoryField(?DELI:DIID,1)
  SELF.AddHistoryField(?DELI:ItemNo,3)
  SELF.AddHistoryField(?DELI:Units,25)
  SELF.AddHistoryField(?DELI:DeliveredUnits,31)
  SELF.AddHistoryField(?DELI:Weight,27)
  SELF.AddHistoryField(?DELI:Volume_Unit,24)
  SELF.AddHistoryField(?DELI:Volume,23)
  SELF.AddHistoryField(?DELI:VolumetricWeight,29)
  SELF.AddHistoryField(?DELI:ContainerNo,10)
  SELF.AddHistoryField(?DELI:SealNo,13)
  SELF.AddHistoryField(?DELI:ContainerVessel,12)
  SELF.AddHistoryField(?DELI:ETA,16)
  SELF.AddHistoryField(?DELI:ShowOnInvoice,7)
  SELF.AddUpdateFile(Access:DeliveryItems)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                           ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Relate:Vessels.Open                             ! File Vessels used by this procedure, so make sure it's RelationManager is open
  Access:DeliveryItems.UseFile                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Commodities.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ContainerOperators.UseFile               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ContainerTypes.UseFile                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PackagingTypes.UseFile                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ClientsRateTypes.UseFile                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Floors.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItemAlias2.UseFile               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveryItems
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:TripSheetDeliveries,SELF) ! Initialize the browse manager
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:DeliveryItems_Components,SELF) ! Initialize the browse manager
  BRW6.Init(?Browse:6,Queue:Browse:6.ViewPosition,BRW6::View:Browse,Queue:Browse:6,Relate:ManifestLoadDeliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    ?L_LV:Commodity{PROP:ReadOnly} = True
    DISABLE(?CallLookup:4)
    ?L_LV:Packaging{PROP:ReadOnly} = True
    ?DELI:Weight{PROP:ReadOnly} = True
    ?L_cV:Length{PROP:ReadOnly} = True
    ?L_cV:Breadth{PROP:ReadOnly} = True
    ?L_cV:Height{PROP:ReadOnly} = True
    ?DELI:Volume_Unit{PROP:ReadOnly} = True
    ?DELI:Volume{PROP:ReadOnly} = True
    ?DELI:VolumetricWeight{PROP:ReadOnly} = True
    DISABLE(?Button_Set_Vol_Ratio)
    DISABLE(?Button_Set_Vol_Ratio:2)
    ?L_LV:VolumetricRatio_FBN{PROP:ReadOnly} = True
    DISABLE(?L_LV:VolumetricRatio)
    DISABLE(?CallLookup:2)
    ?L_LV:ContainerType{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?L_LV:ContainerOperator{PROP:ReadOnly} = True
    ?DELI:ContainerNo{PROP:ReadOnly} = True
    ?DELI:SealNo{PROP:ReadOnly} = True
    DISABLE(?DELI:ContainerVessel)
    DISABLE(?CallLookup:3)
    ?L_LV:ContainerReturnAddressName{PROP:ReadOnly} = True
    DISABLE(?Button_Add)
    DISABLE(?Button_Update)
    DISABLE(?Button_Delete)
    ?L_TCG:Units{PROP:ReadOnly} = True
    ?L_ICG:Length{PROP:ReadOnly} = True
    ?L_ICG:Breadth{PROP:ReadOnly} = True
    ?L_ICG:Height{PROP:ReadOnly} = True
    ?L_ICG:Volume{PROP:ReadOnly} = True
    ?L_ICG:VolumetricWeight{PROP:ReadOnly} = True
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
    ?L_BT:TripSheet_Loaded{PROP:ReadOnly} = True
  END
  BRW4.Q &= Queue:Browse:4
  BRW4::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon TRDI:DIID for sort order 1
  BRW4.AddSortOrder(BRW4::Sort0:StepClass,TRDI:FKey_DIID) ! Add the sort order for TRDI:FKey_DIID for sort order 1
  BRW4.AddRange(TRDI:DIID,Relate:TripSheetDeliveries,Relate:DeliveryItems) ! Add file relationship range limit for sort order 1
  BRW4.AddField(TRDI:TRID,BRW4.Q.TRDI:TRID)       ! Field TRDI:TRID is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:UnitsLoaded,BRW4.Q.TRDI:UnitsLoaded) ! Field TRDI:UnitsLoaded is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:UnitsDelivered,BRW4.Q.TRDI:UnitsDelivered) ! Field TRDI:UnitsDelivered is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:UnitsNotAccepted,BRW4.Q.TRDI:UnitsNotAccepted) ! Field TRDI:UnitsNotAccepted is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:DeliveredDate,BRW4.Q.TRDI:DeliveredDate) ! Field TRDI:DeliveredDate is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:DeliveredTime,BRW4.Q.TRDI:DeliveredTime) ! Field TRDI:DeliveredTime is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:TDID,BRW4.Q.TRDI:TDID)       ! Field TRDI:TDID is a hot field or requires assignment from browse
  BRW4.AddField(TRDI:DIID,BRW4.Q.TRDI:DIID)       ! Field TRDI:DIID is a hot field or requires assignment from browse
  BRW2.Q &= Queue:Browse
  BRW2.FileLoaded = 1                             ! This is a 'file loaded' browse
  BRW2.AddSortOrder(,DELIC:FKey_DIID)             ! Add the sort order for DELIC:FKey_DIID for sort order 1
  BRW2.AddRange(DELIC:DIID,DELI:DIID)             ! Add single value range limit for sort order 1
  BRW2.AddLocator(BRW2::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW2::Sort0:Locator.Init(,DELIC:DIID,1,BRW2)    ! Initialize the browse locator using  using key: DELIC:FKey_DIID , DELIC:DIID
  BRW2.AppendOrder('+DELIC:DICID')                ! Append an additional sort order
  BRW2.AddField(DELIC:Length,BRW2.Q.DELIC:Length) ! Field DELIC:Length is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Breadth,BRW2.Q.DELIC:Breadth) ! Field DELIC:Breadth is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Height,BRW2.Q.DELIC:Height) ! Field DELIC:Height is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Volume,BRW2.Q.DELIC:Volume) ! Field DELIC:Volume is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:VolumetricWeight,BRW2.Q.DELIC:VolumetricWeight) ! Field DELIC:VolumetricWeight is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Units,BRW2.Q.DELIC:Units)   ! Field DELIC:Units is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:DICID,BRW2.Q.DELIC:DICID)   ! Field DELIC:DICID is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:DIID,BRW2.Q.DELIC:DIID)     ! Field DELIC:DIID is a hot field or requires assignment from browse
  BRW6.Q &= Queue:Browse:6
  BRW6::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon MALD:DIID for sort order 1
  BRW6.AddSortOrder(BRW6::Sort0:StepClass,MALD:FKey_DIID) ! Add the sort order for MALD:FKey_DIID for sort order 1
  BRW6.AddRange(MALD:DIID,Relate:ManifestLoadDeliveries,Relate:DeliveryItems) ! Add file relationship range limit for sort order 1
  BRW6.AppendOrder('+MAN:MID,+TRU:Registration')  ! Append an additional sort order
  BRW6.AddField(MAN:MID,BRW6.Q.MAN:MID)           ! Field MAN:MID is a hot field or requires assignment from browse
  BRW6.AddField(MALD:MLDID,BRW6.Q.MALD:MLDID)     ! Field MALD:MLDID is a hot field or requires assignment from browse
  BRW6.AddField(MALD:MLID,BRW6.Q.MALD:MLID)       ! Field MALD:MLID is a hot field or requires assignment from browse
  BRW6.AddField(MALD:UnitsLoaded,BRW6.Q.MALD:UnitsLoaded) ! Field MALD:UnitsLoaded is a hot field or requires assignment from browse
  BRW6.AddField(MAN:CreatedDate,BRW6.Q.MAN:CreatedDate) ! Field MAN:CreatedDate is a hot field or requires assignment from browse
  BRW6.AddField(TRU:Registration,BRW6.Q.TRU:Registration) ! Field TRU:Registration is a hot field or requires assignment from browse
  BRW6.AddField(MAL:MID,BRW6.Q.MAL:MID)           ! Field MAL:MID is a hot field or requires assignment from browse
  BRW6.AddField(MALD:DIID,BRW6.Q.MALD:DIID)       ! Field MALD:DIID is a hot field or requires assignment from browse
  BRW6.AddField(MAL:MLID,BRW6.Q.MAL:MLID)         ! Field MAL:MLID is a hot field or requires assignment from browse
  BRW6.AddField(TRU:TTID,BRW6.Q.TRU:TTID)         ! Field TRU:TTID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_DeliveryItems',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      L_LV:CID                        = DEL:CID
      CLI:CID                         = DEL:CID
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         IF p:RealMode = InsertRecord          !SELF.Request =
            DELI:VolumetricRatio      = CLI:VolumetricRatio
      .  .
  
      L_LV:VolumetricRatio_FBN        = 1000 / DELI:VolumetricRatio
      ! Load Type
      !DISABLE(?Group_Container)
      DISABLE(?Group_TurnIn)
      DISABLE(?L_LV:Container_Info_Not_Available)
  
      ?L_LV:ContainerOperator{PROP:Req}                   = FALSE
      ?L_LV:ContainerType{PROP:Req}                       = FALSE
      ?L_LV:ContainerReturnAddressName{PROP:Req}          = FALSE
      ?DELI:ContainerNo{PROP:Req}                         = FALSE
      ?DELI:ContainerVessel{PROP:Req}                     = FALSE

      LOAD2:LTID              = DEL:LTID
      IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
         LOC:Load_Type_Group :=: LOAD2:Record
  
         ! If we have a Container load that is not a Full load we can only allow 1 container type
         ! because the Container Type determines the Rate which is at Delivery level so we cannot
         ! have different rated Items on same Delivery.

         ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
         !              0               1             2          3              4               5
         IF L_LT:LoadOption = 2 OR L_LT:LoadOption = 4
            DELI:Type         = 0                         ! Container
            ?Prompt_ContainerInfo{PROP:Text}    = 'Container type required.'

            IF p:RealMode = InsertRecord        ! SELF.Request =
               DELI:Units  = 1
            .
            DISABLE(?DELI:Units)
  
            DISABLE(?Volume_Group)

            Item_No_#         = 0
            DELI:CTID         = Get_DelItems_ContainerType(DEL:DID, Item_No_#)
            IF Get_ContainerType_Info(DELI:CTID,0) = 1
               ENABLE(?Volume_Group)
            .

            ! Not sure what this was about...
            !IF LOAD:FullLoad_ContainerPark ~= 1
               ! Check for other items, if found set Container type ID and disable
               IF DELI:ItemNo ~= Item_No_#                ! If we aren't on the 1st Delivery Item of the DI
                  IF DELI:CTID ~= 0
                     DISABLE(?Group_ContainerType)
                     ?Prompt_ContainerInfo{PROP:Text}        = 'Container type set to 1st Items type.'
               .  .
         ELSE
            DELI:Type         = 1                         ! Loose
         .

         IF L_LT:LoadOption = 2 OR L_LT:LoadOption = 4 OR L_LT:LoadOption = 1
         !IF L_LT:ContainerOption = 1 OR L_LT:ContainerOption = 2      ! None|Container|Containerised
       !     ENABLE(?Group_Container)
            ?L_LV:ContainerOperator{PROP:Req}             = TRUE
            ?L_LV:ContainerType{PROP:Req}                 = TRUE
            ?DELI:ContainerNo{PROP:Req}                   = TRUE
            ?DELI:ContainerVessel{PROP:Req}               = TRUE

            !IF L_LT:ContainerOption = 2
               ENABLE(?L_LV:Container_Info_Not_Available)
         .  !.

         IF L_LT:TurnIn = TRUE
            ENABLE(?Group_TurnIn)
            ?L_LV:ContainerReturnAddressName{PROP:Req}    = TRUE
      .  .
      ! (p:RealMode, p:DID)
      ! (BYTE=0, ULONG=0)
  
      CASE p:RealMode
      OF InsertRecord
         ! p:DID
         ! ULONG=0
         ! p:DID is a different DID to the one calling this insert / update.  It is used in Delivery Compositions to
         !       get the Item from this DID so that some fields can be defaulted for this new DID Item.
  
         ! Set item defaults from previous item in passed DID
         IF p:DID ~= 0
            ! Using the Delivery Items record, load the Delivery Items record
            A_DELI2:DID              = p:DID
            A_DELI2:ItemNo           = 1
            IF Access:DeliveryItemAlias2.TryFetch(A_DELI2:FKey_DID_ItemNo) = LEVEL:Benign
               ! Set defaults     - using a group means we dont need to do individual assignments!
               LOC:Delivery_Composition_Group :=: A_DELI2:Record
               DELI:Record                    :=: LOC:Delivery_Composition_Group
         .  .
      OF ViewRecord
         DISABLE(?L_LV:Container_Info_Not_Available)
      OF ChangeRecord            ! SELF.Request
         ! Check if we specified a volume or not
         IF DELI:Volume ~= 0.0
            IF DELI:Length ~= 0.0 AND DELI:Breadth ~= 0.0 AND DELI:Height ~= 0.0
               ! We calculated the volume
               L_cV:Length        = DELI:Length  * 100
               L_cV:Breadth       = DELI:Breadth * 100
               L_cV:Height        = DELI:Height  * 100
            ELSE
               DELIC:DIID         = DELI:DIID
               IF Access:DeliveryItems_Components.TryFetch(DELIC:FKey_DIID) = LEVEL:Benign
                  LOC:Enter_Volume   = 2
               ELSE
                  LOC:Enter_Volume   = 1
      .  .  .  .
  
  
      CASE p:RealMode
      OF InsertRecord
         IF L_LT:LoadOption = 2 OR L_LT:LoadOption = 4 OR L_LT:LoadOption = 1 OR L_LT:TurnIn = TRUE
            DELI:ShowOnInvoice    = TRUE                ! This may be
         .
  
         IF L_LT:LoadOption = 1 OR L_LT:LoadOption = 2             ! Container Park or Container
            ! If FBN Floor then dont ask for container info.
            FLO:FID = DEL:FID
            IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
               IF FLO:FBNFloor = TRUE
                  L_LV:Container_Info_Not_Available  = TRUE
                  DELI:ShowOnInvoice                 = FALSE
                  DO Container_Options
         .  .  .
      ELSE
         IF L_LT:LoadOption = 1 OR L_LT:LoadOption = 2             ! Container Park or Container
            IF DELI:COID ~= 0 OR CLIP(DELI:ContainerNo) ~= '' OR CLIP(DELI:ContainerVessel) ~= 0 OR CLIP(DELI:SealNo) ~= '' |
                  OR DELI:ContainerReturnAID ~= 0
            ELSE
               L_LV:Container_Info_Not_Available      = TRUE
               DO Container_Options
      .  .  .
  FDCB19.Init(DELI:ContainerVessel,?DELI:ContainerVessel,Queue:FileDropCombo.ViewPosition,FDCB19::View:FileDropCombo,Queue:FileDropCombo,Relate:Vessels,ThisWindow,GlobalErrors,1,1,0)
  FDCB19.AskProcedure = 6
  FDCB19.Q &= Queue:FileDropCombo
  FDCB19.AddSortOrder(VES:Key_Vessel)
  FDCB19.AddField(VES:Vessel,FDCB19.Q.VES:Vessel) !List box control field - type derived from field
  FDCB19.AddField(VES:VEID,FDCB19.Q.VES:VEID) !Primary key field - type derived from field
  FDCB19.AddUpdateField(VES:Vessel,DELI:ContainerVessel)
  ThisWindow.AddItem(FDCB19.WindowComponent)
  FDCB19.DefaultFill = 0
  FDCB18.Init(L_LV:Packaging,?L_LV:Packaging,Queue:FileDropCombo:1.ViewPosition,FDCB18::View:FileDropCombo,Queue:FileDropCombo:1,Relate:PackagingTypes,ThisWindow,GlobalErrors,1,1,0)
  FDCB18.AskProcedure = 7
  FDCB18.Q &= Queue:FileDropCombo:1
  FDCB18.AddSortOrder(PACK:Key_Packaging)
  FDCB18.AddField(PACK:Packaging,FDCB18.Q.PACK:Packaging) !List box control field - type derived from field
  FDCB18.AddField(PACK:PTID,FDCB18.Q.PACK:PTID) !Primary key field - type derived from field
  FDCB18.AddUpdateField(PACK:PTID,DELI:PTID)
  ThisWindow.AddItem(FDCB18.WindowComponent)
  FDCB18.DefaultFill = 0
  BRW2.AddToolbarTarget(Toolbar)                  ! Browse accepts toolbar control
  BRW2.ToolbarItem.HelpButton = ?Help
  BRW4::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW4::FormatManager.Init('MANTRNIS','Update_DeliveryItems',1,?Browse:4,4,BRW4::PopupTextExt,Queue:Browse:4,6,LFM_CFile,LFM_CFile.Record)
  BRW4::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW6::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW6::FormatManager.Init('MANTRNIS','Update_DeliveryItems',1,?Browse:6,6,BRW6::PopupTextExt,Queue:Browse:6,7,LFM_CFile,LFM_CFile.Record)
  BRW6::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
    Relate:Vessels.Close
  END
  ! List Format Manager destructor
  BRW4::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW6::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_DeliveryItems',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  DELI:Units = 1
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Commodities((L_LV:Commodity))
      Browse_PackagingTypes
      Browse_ContainerTypes
      Browse_ContainerOperators
      Browse_Addresses
      Update_Vessels
      Update_PackagingTypes
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_LV:Commodity
      IF L_LV:Commodity OR ?L_LV:Commodity{PROP:Req}
        COM:Commodity = L_LV:Commodity
        IF Access:Commodities.TryFetch(COM:Key_Commodity)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_LV:Commodity = COM:Commodity
            DELI:CMID = COM:CMID
          ELSE
            CLEAR(DELI:CMID)
            SELECT(?L_LV:Commodity)
            CYCLE
          END
        ELSE
          DELI:CMID = COM:CMID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:4
      ThisWindow.Update()
      COM:Commodity = L_LV:Commodity
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_LV:Commodity = COM:Commodity
        DELI:CMID = COM:CMID
      END
      ThisWindow.Reset(1)
    OF ?L_LV:Packaging
      FDCB18.TakeAccepted()
    OF ?L_LV:Volume_Rounding
          DO Compute_Vol
    OF ?L_LV:Volume_Per_Unit
          DO Compute_Vol
      
          DO Enter_Vol_Check
      
          DISPLAY
    OF ?L_cV:Length
          DO Compute_Vol
    OF ?L_cV:Breadth
          DO Compute_Vol
    OF ?L_cV:Height
          DO Compute_Vol
    OF ?DELI:Volume_Unit
          DO Compute_Vol
    OF ?LOC:Enter_Volume
          IF QuickWindow{PROP:AcceptAll} = FALSE
             CASE LOC:Enter_Volume
             OF 0                        ! Calc. Volume
                ENABLE(?Group_Measure)
                DISABLE(?Group_VolTot)
                DISABLE(?Tab_Split_Vol)
             OF 1                        ! Enter Volume
                DISABLE(?Group_Measure)
                ENABLE(?Group_VolTot)
                DISABLE(?Tab_Split_Vol)
      
                L_LV:Volume_Per_Unit = FALSE
                DISPLAY(?L_LV:Volume_Per_Unit)
             OF 2                        ! Split vol.
                DISABLE(?Group_Measure)
                DISABLE(?Group_VolTot)
                ENABLE(?Tab_Split_Vol)
      
                SELECT(?Tab_Split_Vol)
          .  .
          
    OF ?Button_Set_Vol_Ratio
      ThisWindow.Update()
          CLI:CID                       = L_LV:CID
          IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
             DELI:VolumetricRatio    = CLI:VolumetricRatio
             DISPLAY(?L_LV:VolumetricRatio_FBN)
      
             DO Vol_Q
      
             CLEAR(LOC:Vol_Ratio_Q)
             L_VRQ:VolumetricRatio           = DELI:VolumetricRatio
             GET(LOC:Vol_Ratio_Q, L_VRQ:VolumetricRatio)
             IF ERRORCODE()
                L_VRQ:VolumetricRatio        = DELI:VolumetricRatio
                L_VRQ:VolumetricRatio_Vol    = 1000 / L_VRQ:VolumetricRatio
                L_VRQ:Client                 = 'Client'
                ADD(LOC:Vol_Ratio_Q, +L_VRQ:VolumetricRatio)
             ELSE
                L_VRQ:Client                 = 'Client'
                PUT(LOC:Vol_Ratio_Q)
             .
      
      
             L_LV:VolumetricRatio    = L_VRQ:VolumetricRatio_Vol
      
      
             DO Compute_Vol
      
             DO Enter_Vol_Check
          .
    OF ?Button_Set_Vol_Ratio:2
      ThisWindow.Update()
          ?L_LV:VolumetricRatio_FBN{PROP:Background}  = -1
          ?L_LV:VolumetricRatio_FBN{PROP:ReadOnly}    = FALSE
    OF ?L_LV:VolumetricRatio_FBN
          DELI:VolumetricRatio      = 1000 / L_LV:VolumetricRatio_FBN
          
          DO Compute_Vol
    OF ?L_LV:Container_Info_Not_Available
          DO Container_Options
    OF ?CallLookup:2
      ThisWindow.Update()
      CTYP:ContainerType = L_LV:ContainerType
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        L_LV:ContainerType = CTYP:ContainerType
        DELI:CTID = CTYP:CTID
      END
      ThisWindow.Reset(1)
          ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
          IF L_LT:LoadOption ~= 2 AND L_LT:LoadOption ~= 4 OR Get_ContainerType_Info(DELI:CTID,0) = 1
             ENABLE(?Volume_Group)
          ELSE
             DISABLE(?Volume_Group)
          .
    OF ?L_LV:ContainerType
      IF L_LV:ContainerType OR ?L_LV:ContainerType{PROP:Req}
        CTYP:ContainerType = L_LV:ContainerType
        IF Access:ContainerTypes.TryFetch(CTYP:Key_Type)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            L_LV:ContainerType = CTYP:ContainerType
            DELI:CTID = CTYP:CTID
          ELSE
            CLEAR(DELI:CTID)
            SELECT(?L_LV:ContainerType)
            CYCLE
          END
        ELSE
          DELI:CTID = CTYP:CTID
        END
      END
      ThisWindow.Reset()
          IF L_LT:LoadOption ~= 2 AND L_LT:LoadOption ~= 4 OR Get_ContainerType_Info(DELI:CTID,0) = 1
             ENABLE(?Volume_Group)
          ELSE
             DISABLE(?Volume_Group)
          .
    OF ?CallLookup
      ThisWindow.Update()
      CONO:ContainerOperator = L_LV:ContainerOperator
      IF SELF.Run(4,SelectRecord) = RequestCompleted
        L_LV:ContainerOperator = CONO:ContainerOperator
        DELI:COID = CONO:COID
      END
      ThisWindow.Reset(1)
    OF ?L_LV:ContainerOperator
      IF L_LV:ContainerOperator OR ?L_LV:ContainerOperator{PROP:Req}
        CONO:ContainerOperator = L_LV:ContainerOperator
        IF Access:ContainerOperators.TryFetch(CONO:Key_Operator)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            L_LV:ContainerOperator = CONO:ContainerOperator
            DELI:COID = CONO:COID
          ELSE
            CLEAR(DELI:COID)
            SELECT(?L_LV:ContainerOperator)
            CYCLE
          END
        ELSE
          DELI:COID = CONO:COID
        END
      END
      ThisWindow.Reset()
    OF ?DELI:ContainerVessel
      FDCB19.TakeAccepted()
    OF ?CallLookup:3
      ThisWindow.Update()
      ADD:AddressName = L_LV:ContainerReturnAddressName
      IF SELF.Run(5,SelectRecord) = RequestCompleted
        L_LV:ContainerReturnAddressName = ADD:AddressName
        DELI:ContainerReturnAID = ADD:AID
      END
      ThisWindow.Reset(1)
    OF ?L_LV:ContainerReturnAddressName
      IF L_LV:ContainerReturnAddressName OR ?L_LV:ContainerReturnAddressName{PROP:Req}
        ADD:AddressName = L_LV:ContainerReturnAddressName
        IF Access:Addresses.TryFetch(ADD:Key_Name)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            L_LV:ContainerReturnAddressName = ADD:AddressName
            DELI:ContainerReturnAID = ADD:AID
          ELSE
            CLEAR(DELI:ContainerReturnAID)
            SELECT(?L_LV:ContainerReturnAddressName)
            CYCLE
          END
        ELSE
          DELI:ContainerReturnAID = ADD:AID
        END
      END
      ThisWindow.Reset()
    OF ?Button_Add
      ThisWindow.Update()
          IF L_ICG:Units <= 0
             MESSAGE('Please enter a value for units.', 'Delivery Item Components', ICON:Exclamation)
             SELECT(?L_ICG:Units)
             CYCLE
          .
          IF L_ICG:Length = 0 OR L_ICG:Breadth = 0 OR L_ICG:Height = 0
             MESSAGE('Please enter a value for all the dimensions.', 'Delivery Item Components', ICON:Exclamation)
      !       MESSAGE('Please enter a value for all the dimensions.||L_ICG:Length: ' & L_ICG:Length &',  L_ICG:Breadth: ' & L_ICG:Breadth & ',  L_ICG:Height: ' & L_ICG:Height, 'Delivery Item Components', ICON:Exclamation)
             SELECT(?L_ICG:Length)
             CYCLE
          .
      
      
          IF L_TCG:Units + L_ICG:Units > DELI:Units
             MESSAGE('Too many units would be loaded.', 'Delivery Item Components', ICON:Exclamation)
             L_ICG:Units                  = DELI:Units - L_TCG:Units
          ELSE
             IF Access:DeliveryItems_Components.TryPrimeAutoInc() ~= LEVEL:Benign
                MESSAGE('File problem on add prime.', 'Delivery Item Components', ICON:Exclamation)
             ELSE
                DO Delivery_Comp_WVol
      
                !DELIC:DICID
                DELIC:DIID                = DELI:DIID
      
                DELIC:Length              = L_ICG:Length
                DELIC:Breadth             = L_ICG:Breadth
                DELIC:Height              = L_ICG:Height
      
                DELIC:Units               = L_ICG:Units
                DELIC:Weight              = L_ICG:Weight
      
                DELIC:Volume              = L_ICG:Volume
                DELIC:Volume_Unit         = L_ICG:Volume_Unit
      
                DELIC:VolumetricWeight    = L_ICG:VolumetricWeight
      
                !DELIC:Volume              = DELIC:Length * DELIC:Breadth * DELIC:Height
                !DELIC:VolumetricWeight    = DELIC:Volume * DELI:VolumetricRatio
      
                IF Access:DeliveryItems_Components.Insert() ~= LEVEL:Benign
                   !MESSAGE('File problem on add.', 'Delivery Item Components', ICON:Exclamation)
                ELSE
                   !BRW2.ResetFromFile()
                   BRW2.ResetFromView()
      
                   CLEAR(LOC:Item_Comp)
      
                   DO Enter_Vol_Check
      
                   L_LV:Updated_Child     = TRUE
          .  .  .
      
          DISPLAY
    OF ?Button_Update
      ThisWindow.Update()
          IF L_ICG:Units <= 0
             MESSAGE('Please enter a value for units or delete this entry.', 'Delivery Item Components', ICON:Exclamation)
             SELECT(?L_ICG:Units)
             CYCLE
          .
          IF L_ICG:Length = 0.0 OR L_ICG:Breadth = 0.0 OR L_ICG:Height = 0.0
             MESSAGE('Please enter a value for all the dimensions.', 'Delivery Item Components', ICON:Exclamation)
             SELECT(?L_ICG:Length)
             CYCLE
          .
      
          IF (L_TCG:Units + L_ICG:Units - DELIC:Units) > DELI:Units
             MESSAGE('Too many units would be loaded.', 'Delivery Item Components', ICON:Exclamation)
          ELSE
             DO Delivery_Comp_WVol
      
             DELIC:Length              = L_ICG:Length
             DELIC:Breadth             = L_ICG:Breadth
             DELIC:Height              = L_ICG:Height
      
             DELIC:Units               = L_ICG:Units
             DELIC:Weight              = L_ICG:Weight
      
             DELIC:Volume              = L_ICG:Volume
             DELIC:VolumetricWeight    = L_ICG:VolumetricWeight
      
             IF Access:DeliveryItems_Components.TryUpdate() ~= LEVEL:Benign
                MESSAGE('File problem on update.', 'Delivery Item Components', ICON:Exclamation)
             ELSE
                BRW2.ResetFromFile()
                BRW2.ResetFromView()
      
                DO Enter_Vol_Check
      
                L_LV:Updated_Child     = TRUE
          .  .
      
          DISPLAY
    OF ?Button_Delete
      ThisWindow.Update()
      BRW2.UpdateViewRecord()
      !    CASE MESSAGE('Are you sure?', 'Delete Component', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
      !    OF BUTTON:Yes
          IF Relate:DeliveryItems_Components.Delete() = LEVEL:Benign
             CLEAR(LOC:Item_Comp)
             BRW2.ResetFromFile()
             BRW2.ResetFromView()
      
             DO Enter_Vol_Check
      
             L_LV:Updated_Child     = TRUE
          .
    OF ?L_ICG:Length
          DO Delivery_Comp_WVol
    OF ?L_ICG:Breadth
          DO Delivery_Comp_WVol
    OF ?L_ICG:Height
          DO Delivery_Comp_WVol
    OF ?L_ICG:Units
          DO Delivery_Comp_WVol
    OF ?OK
      ThisWindow.Update()
          IF DELI:Units <= 0
             QuickWindow{PROP:AcceptAll}       = FALSE
             SELECT(?DELI:Units)
             CYCLE
          .
      
      
          IF DELI:Weight = 0.0 AND DELI:VolumetricWeight = 0.0
             CASE MESSAGE('You have no Weight or Volumetric Weight.||Would you like to change this?', 'Missing Information', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
             OF BUTTON:Yes
                QuickWindow{PROP:AcceptAll}    = FALSE
                SELECT(?DELI:Weight)
                CYCLE
          .  .
      
          IF LOC:Enter_Volume = 2         ! Splitting - then we must have these agree
             IF DELI:Volume_Unit ~= L_TCG:Volume OR DELI:VolumetricWeight ~= L_TCG:VolumetricWeight OR L_TCG:Units ~= DELI:Units
                BRW2.ResetFromView()
                IF DELI:Volume_Unit ~= L_TCG:Volume OR DELI:VolumetricWeight ~= L_TCG:VolumetricWeight OR L_TCG:Units ~= DELI:Units
                   MESSAGE('When Volume Option is Split, the split totals must by kept the same as the item values.||The Volumes or Units are not totaling to the Delivery Item specified amounts.', 'Missing Information', ICON:Hand)
                   QuickWindow{PROP:AcceptAll}    = FALSE
                   SELECT(?)
                   CYCLE
          .  .  .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
    IF p:RealMode ~= InsertRecord AND L_LV:Updated_Child = TRUE
       IF LOC:Enter_Volume = 2         ! Splitting - then we must have these agree
          IF DELI:Volume_Unit ~= L_TCG:Volume OR DELI:VolumetricWeight ~= L_TCG:VolumetricWeight OR L_TCG:Units ~= DELI:Units
             BRW2.ResetFromView()
             IF DELI:Volume_Unit ~= L_TCG:Volume OR DELI:VolumetricWeight ~= L_TCG:VolumetricWeight OR L_TCG:Units ~= DELI:Units
                MESSAGE('When Volume Option is Split, the split totals must by kept the same as the item values.||The Volumes or Units are not totaling to the Delivery Item specified amounts.', 'Missing Information', ICON:Hand)
                QuickWindow{PROP:AcceptAll}    = FALSE
                SELECT(?DELI:Units)
                RETURN( LEVEL:Notify )
    .  .  .  .
  
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LOC:Enter_Volume
          CASE LOC:Enter_Volume
          OF 0                        ! Calc. Volume
             ENABLE(?Group_Measure)
             DISABLE(?Group_VolTot)
             DISABLE(?Tab_Split_Vol)
          OF 1                        ! Enter Volume
             DISABLE(?Group_Measure)
             ENABLE(?Group_VolTot)
             DISABLE(?Tab_Split_Vol)
      
             L_LV:Volume_Per_Unit = FALSE
             DISPLAY(?L_LV:Volume_Per_Unit)
          OF 2                        ! Split vol.
             DISABLE(?Group_Measure)
             DISABLE(?Group_VolTot)
             SELECT(?Tab_Split_Vol)
          .
          
    OF ?L_LV:VolumetricRatio
          IF QuickWindow{PROP:AcceptAll} = FALSE
             GET(LOC:Vol_Ratio_Q, CHOICE(?L_LV:VolumetricRatio))
             IF ~ERRORCODE()
                DELI:VolumetricRatio      = L_VRQ:VolumetricRatio
                L_LV:VolumetricRatio_FBN  = L_VRQ:VolumetricRatio_Vol         ! FBN style...
                DISPLAY
                DO Compute_Vol
          .  .
    OF ?List
      BRW2.UpdateViewRecord()
          LOC:Item_Comp   :=: DELIC:RECORD
          DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          COM:CMID                    = DELI:CMID
          IF Access:Commodities.TryFetch(COM:PKey_CMID) = LEVEL:Benign
             L_LV:Commodity           = COM:Commodity
          .
      
          CONO:COID                   = DELI:COID
          IF Access:ContainerOperators.TryFetch(CONO:PKey_COID) = LEVEL:Benign
             L_LV:ContainerOperator   = CONO:ContainerOperator
          .
      
          CTYP:CTID                   = DELI:CTID
          IF Access:ContainerTypes.TryFetch(CTYP:PKey_CTID) = LEVEL:Benign
             L_LV:ContainerType       = CTYP:ContainerType
          .
      
          ADD:AID                     = DELI:ContainerReturnAID
          IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
             L_LV:ContainerReturnAddressName  = ADD:AddressName
          .
      
          PACK:PTID                   = DELI:PTID
          IF Access:PackagingTypes.TryFetch(PACK:PKey_PTID) = LEVEL:Benign
             L_LV:Packaging           = PACK:Packaging
          .
      
          L_LV:Volume_Per_Unit        = TRUE
          IF p:RealMode = InsertRecord
          ELSE
             IF DELI:Volume ~= DELI:Volume_Unit
                L_LV:Volume_Per_Unit  = TRUE
             ELSE
                L_LV:Volume_Per_Unit  = FALSE
      
                DELIC:DIID   = DELI:DIID
                IF Access:DeliveryItems_Components.TryFetch(DELIC:FKey_DIID) = LEVEL:Benign
                   IF DELIC:Volume ~= DELIC:Volume_Unit
                      L_LV:Volume_Per_Unit  = TRUE
          .  .  .  .
      
      
          CASE LOC:Enter_Volume
          OF 0                        ! Calc. Volume
             ENABLE(?Group_Measure)
             DISABLE(?Group_VolTot)
             DISABLE(?Tab_Split_Vol)
          OF 1                        ! Enter Volume
             DISABLE(?Group_Measure)
             ENABLE(?Group_VolTot)
             DISABLE(?Tab_Split_Vol)
          OF 2                        ! Split vol.
             DISABLE(?Group_Measure)
             DISABLE(?Group_VolTot)
             ENABLE(?Tab_Split_Vol)
      
             SELECT(?Tab_Split_Vol)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          DO Vol_Q
      
          CLEAR(LOC:Vol_Ratio_Q)
          L_VRQ:VolumetricRatio           = DELI:VolumetricRatio
          GET(LOC:Vol_Ratio_Q, L_VRQ:VolumetricRatio)
          IF ERRORCODE()
             L_VRQ:VolumetricRatio        = DELI:VolumetricRatio
             L_VRQ:VolumetricRatio_Vol    = 1000 / L_VRQ:VolumetricRatio
             L_VRQ:Client                 = 'Client'
             ADD(LOC:Vol_Ratio_Q, +L_VRQ:VolumetricRatio)
          ELSE
             L_VRQ:Client                 = 'Client'
             PUT(LOC:Vol_Ratio_Q)
          .
      
      
          L_LV:VolumetricRatio    = L_VRQ:VolumetricRatio_Vol
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW4.ResetFromView PROCEDURE

L_BT:TripSheet_Loaded:Sum REAL                             ! Sum variable for browse totals
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:TripSheetDeliveries.SetQuickScan(1)
  SELF.Reset
  IF SELF.UseMRP
     IF SELF.View{PROP:IPRequestCount} = 0
          SELF.View{PROP:IPRequestCount} = 60
     END
  END
  LOOP
    IF SELF.UseMRP
       IF SELF.View{PROP:IPRequestCount} = 0
            SELF.View{PROP:IPRequestCount} = 60
       END
    END
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      SETCURSOR()
      RETURN
    END
    SELF.SetQueueRecord
    L_BT:TripSheet_Loaded:Sum += TRDI:UnitsLoaded
  END
  SELF.View{PROP:IPRequestCount} = 0
  L_BT:TripSheet_Loaded = L_BT:TripSheet_Loaded:Sum
  PARENT.ResetFromView
  Relate:TripSheetDeliveries.SetQuickScan(0)
  SETCURSOR()


BRW4.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW4::LastSortOrder <> NewOrder THEN
     BRW4::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW4::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW4.TakeNewSelection PROCEDURE

  CODE
  IF BRW4::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW4::PopupTextExt = ''
        BRW4::PopupChoiceExec = True
        BRW4::FormatManager.MakePopup(BRW4::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW4::PopupTextExt = '|-|' & CLIP(BRW4::PopupTextExt)
        END
        BRW4::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW4::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW4::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW4::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW4::PopupChoiceOn AND BRW4::PopupChoiceExec THEN
     BRW4::PopupChoiceExec = False
     BRW4::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW4::PopupTextExt)
     IF BRW4::FormatManager.DispatchChoice(BRW4::PopupChoice)
     ELSE
     END
  END


BRW2.PrimeRecord PROCEDURE(BYTE SuppressClear = 0)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.PrimeRecord(SuppressClear)
  DELIC:Units = 1
  RETURN ReturnValue


BRW2.ResetFromView PROCEDURE

L_TCG:Units:Sum      REAL                                  ! Sum variable for browse totals
L_TCG:Weight:Sum     REAL                                  ! Sum variable for browse totals
L_TCG:VolumetricWeight:Sum REAL                            ! Sum variable for browse totals
L_TCG:Volume:Sum     REAL                                  ! Sum variable for browse totals
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:DeliveryItems_Components.SetQuickScan(1)
  SELF.Reset
  IF SELF.UseMRP
     IF SELF.View{PROP:IPRequestCount} = 0
          SELF.View{PROP:IPRequestCount} = 60
     END
  END
  LOOP
    IF SELF.UseMRP
       IF SELF.View{PROP:IPRequestCount} = 0
            SELF.View{PROP:IPRequestCount} = 60
       END
    END
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      SETCURSOR()
      RETURN
    END
    SELF.SetQueueRecord
    L_TCG:Units:Sum += DELIC:Units
    L_TCG:Weight:Sum += DELIC:Weight
    L_TCG:VolumetricWeight:Sum += DELIC:VolumetricWeight
    L_TCG:Volume:Sum += DELIC:Volume
  END
  SELF.View{PROP:IPRequestCount} = 0
  L_TCG:Units = L_TCG:Units:Sum
  L_TCG:Weight = L_TCG:Weight:Sum
  L_TCG:VolumetricWeight = L_TCG:VolumetricWeight:Sum
  L_TCG:Volume = L_TCG:Volume:Sum
       IF L_LV:Volume_Rounding = TRUE
          L_TCG:Volume         = ROUND(L_TCG:Volume,1)
       .
  PARENT.ResetFromView
  Relate:DeliveryItems_Components.SetQuickScan(0)
  SETCURSOR()


BRW2.SetAlerts PROCEDURE

  CODE
  SELF.EditViaPopup = False
  PARENT.SetAlerts


BRW6.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW6::LastSortOrder <> NewOrder THEN
     BRW6::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW6::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW6.TakeNewSelection PROCEDURE

  CODE
  IF BRW6::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW6::PopupTextExt = ''
        BRW6::PopupChoiceExec = True
        BRW6::FormatManager.MakePopup(BRW6::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW6::PopupTextExt = '|-|' & CLIP(BRW6::PopupTextExt)
        END
        BRW6::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW6::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW6::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW6::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW6::PopupChoiceOn AND BRW6::PopupChoiceExec THEN
     BRW6::PopupChoiceExec = False
     BRW6::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW6::PopupTextExt)
     IF BRW6::FormatManager.DispatchChoice(BRW6::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Update_DeliveryItems_h PROCEDURE  (p:DID)                  ! Declare Procedure
LOC:Request          LONG                                  !
LOC:DIID             ULONG                                 !Delivery Item ID
LOC:Response         LIKE(GlobalResponse)                  !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias2.Open()
     .
     Access:DeliveryItems.UseFile()
     Access:DeliveryItemAlias2.UseFile()
    ! Has a primed buffer of the Alias
    ! (p:DID)

    db.debugout('[Update_DeliveryItems_h] - start')

    IF GlobalRequest = DeleteRecord
       IF Relate:DeliveryItemAlias2.Delete() = LEVEL:Benign
          GlobalResponse        = RequestCancelled
       .
    ELSE
       LOC:Request              = GlobalRequest

       LOC:DIID                 = A_DELI2:DIID

       ! Load Delivery Items to same buffer as Delivery Items Alias
       DELI:DIID                = A_DELI2:DIID
       IF Access:DeliveryItems.TryFetch(DELI:PKey_DIID) = LEVEL:Benign
          !IF LOC:Request = InsertRecord
          !   IF Access:DeliveryItemAlias2.DeleteRecord(0) = LEVEL:Benign
          !.  .

          IF LOC:Request = InsertRecord
             DELI:Units         = 1                 ! Default units to 1
          .

          ! Set ChangeRecord mode for update
          IF LOC:Request = ViewRecord
             GlobalRequest      = ViewRecord
          ELSE
             GlobalRequest      = ChangeRecord
          .
          Update_DeliveryItems(LOC:Request, p:DID)
          LOC:Response          = GlobalResponse
          GlobalRequest         = LOC:Request

          A_DELI2:DIID          = DELI:DIID                     ! Was loaded only down below,  27 Nov 06
          IF Access:DeliveryItemAlias2.TryFetch(A_DELI2:PKey_DIID) = LEVEL:Benign
          .

          IF LOC:Response = RequestCompleted
             ! User accepted - werent deleting - update buffer
             IF LOC:Request ~= DeleteRecord
                ! ????????? previously we reloaded alias here   27 Nov 06
                ! Ok then
             .
          ELSE
             ! User cancelled - were inserting - remove record
             IF LOC:Request = InsertRecord
                A_DELI2:DIID    = LOC:DIID
                IF Access:DeliveryItemAlias2.TryFetch(A_DELI2:PKey_DIID) = LEVEL:Benign
                   IF Access:DeliveryItemAlias2.DeleteRecord(0) = LEVEL:Benign
    .  .  .  .  .  .

    db.debugout('[Update_DeliveryItems_h] - end')
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:DeliveryItems.Close()
    Relate:DeliveryItemAlias2.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Floors PROCEDURE (p:Option)

CurrentTab           STRING(80)                            !
LOC:Option           BYTE                                  !Set to one when called from frame, passed to Update for special handling
LOC:DIs              ULONG                                 !DI's on this floor
BRW1::View:Browse    VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FBNFloor)
                       PROJECT(FLO:FID)
                       PROJECT(FLO:AID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
LOC:DIs                LIKE(LOC:DIs)                  !List box control field - type derived from local data
FLO:FBNFloor           LIKE(FLO:FBNFloor)             !List box control field - type derived from field
FLO:FBNFloor_Icon      LONG                           !Entry's icon ID
FLO:FID                LIKE(FLO:FID)                  !List box control field - type derived from field
FLO:AID                LIKE(FLO:AID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Floors File'),AT(,,257,188),FONT('Tahoma',8),RESIZE,GRAY,IMM,MDI,HLP('BrowseFloors'), |
  SYSTEM
                       LIST,AT(8,22,240,122),USE(?Browse:1),HVSCROLL,FORMAT('144L(2)|M~Floor~@s35@30R(2)|M~DI''' & |
  's~L@n13@40R(2)|MI~FBN Floor~C(0)@p p@40R(2)|M~FID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM, |
  MSG('Browsing Records')
                       BUTTON('&Select'),AT(8,148,,14),USE(?Select:2),LEFT,ICON('WAselect.ico'),FLAT
                       SHEET,AT(4,4,249,162),USE(?CurrentTab)
                         TAB('By Floor'),USE(?Tab:2)
                         END
                       END
                       GROUP,AT(78,148,171,14),USE(?Group_Updates)
                         BUTTON('&Insert'),AT(78,148,,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT
                         BUTTON('&Change'),AT(134,148,,14),USE(?Change:3),LEFT,ICON('WAchange.ico'),DEFAULT,FLAT
                         BUTTON('&Delete'),AT(195,148,,14),USE(?Delete:3),LEFT,ICON('WAdelete.ico'),FLAT
                       END
                       BUTTON('Cl&ose'),AT(201,170,,14),USE(?Close),LEFT,ICON('waclose.ico'),FLAT
                       BUTTON('Help'),AT(4,170,45,14),USE(?Help),HIDE,STD(STD:Help)
                       BUTTON('&View'),AT(144,170,53,14),USE(?View),LEFT,ICON('waview.ico'),FLAT
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Floors')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:DIs',LOC:DIs)                         ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                           ! File Addresses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Floors,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
      IF DEFORMAT(p:Option) = 1
         DISABLE(?Group_Updates)
         LOC:Option   = 1
      .
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon FLO:AID for sort order 1
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,FLO:FKey_AID) ! Add the sort order for FLO:FKey_AID for sort order 1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) ! Moveable thumb based upon FLO:Floor for sort order 2
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,FLO:Key_Floor) ! Add the sort order for FLO:Key_Floor for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,FLO:Floor,1,BRW1)     ! Initialize the browse locator using  using key: FLO:Key_Floor , FLO:Floor
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(FLO:Floor,BRW1.Q.FLO:Floor)       ! Field FLO:Floor is a hot field or requires assignment from browse
  BRW1.AddField(LOC:DIs,BRW1.Q.LOC:DIs)           ! Field LOC:DIs is a hot field or requires assignment from browse
  BRW1.AddField(FLO:FBNFloor,BRW1.Q.FLO:FBNFloor) ! Field FLO:FBNFloor is a hot field or requires assignment from browse
  BRW1.AddField(FLO:FID,BRW1.Q.FLO:FID)           ! Field FLO:FID is a hot field or requires assignment from browse
  BRW1.AddField(FLO:AID,BRW1.Q.FLO:AID)           ! Field FLO:AID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_Floors',QuickWindow)       ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_Floors',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,5,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_Floors',QuickWindow)             ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Floors(LOC:Option)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END
  SELF.ViewControl = ?View                                 ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      LOC:DIs     = Get_Floor_DIs(FLO:FID)
  PARENT.SetQueueRecord
  
  IF (FLO:FBNFloor = 1)
    SELF.Q.FLO:FBNFloor_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.FLO:FBNFloor_Icon = 1                           ! Set icon from icon list
  END


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>1,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>1,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Floors PROCEDURE (p:Option)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
AddressName          STRING(35)                            !Name of this address
LOC:Group            GROUP,PRE(L_G)                        !
Total                DECIMAL(12,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel, Insurance and Additional Charges charges
InsuanceAmount       DECIMAL(11,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel, Insurance and Additional Charges charges
TotalCharge          DECIMAL(11,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel, Insurance and Additional Charges charges
ManifestedDelivered  BYTE(1)                               !Not Manifested, Manifested / Not Delivered, Partially Delivered, Delivered
TRIDs                STRING(100)                           !
                     END                                   !
LOC:Report_Vars      GROUP,PRE()                           !
DI_Items_Details     STRING(200)                           !Details of items on the DI
Delivery_Details     STRING(100)                           !
                     END                                   !
LOC:Delivery_Progress_Group GROUP,PRE(L_DP)                !
DINo                 ULONG                                 !Delivery Instruction Number
                     END                                   !
BRW2::View:Browse    VIEW(__RatesContainerPark)
                       PROJECT(CPRA:ToMass)
                       PROJECT(CPRA:RatePerKg)
                       PROJECT(CPRA:Effective_Date)
                       PROJECT(CPRA:CPID)
                       PROJECT(CPRA:FID)
                       PROJECT(CPRA:JID)
                       JOIN(JOU:PKey_JID,CPRA:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
CPRA:ToMass            LIKE(CPRA:ToMass)              !List box control field - type derived from field
CPRA:RatePerKg         LIKE(CPRA:RatePerKg)           !List box control field - type derived from field
CPRA:Effective_Date    LIKE(CPRA:Effective_Date)      !List box control field - type derived from field
CPRA:CPID              LIKE(CPRA:CPID)                !Primary key field - type derived from field
CPRA:FID               LIKE(CPRA:FID)                 !Browse key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:DocumentCharge)
                       PROJECT(DEL:FuelSurcharge)
                       PROJECT(DEL:Charge)
                       PROJECT(DEL:AdditionalCharge)
                       PROJECT(DEL:VATRate)
                       PROJECT(DEL:Insure)
                       PROJECT(DEL:TotalConsignmentValue)
                       PROJECT(DEL:InsuranceRate)
                       PROJECT(DEL:Manifested)
                       PROJECT(DEL:Delivered)
                       PROJECT(DEL:FID)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:DeliveryAID)
                       PROJECT(DEL:CollectionAID)
                       PROJECT(DEL:CID)
                       JOIN(A_ADD:PKey_AID,DEL:DeliveryAID)
                         PROJECT(A_ADD:AddressName)
                         PROJECT(A_ADD:AID)
                       END
                       JOIN(ADD:PKey_AID,DEL:CollectionAID)
                         PROJECT(ADD:AddressName)
                         PROJECT(ADD:AID)
                       END
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:DIDate             LIKE(DEL:DIDate)               !List box control field - type derived from field
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
A_ADD:AddressName      LIKE(A_ADD:AddressName)        !List box control field - type derived from field
DI_Items_Details       LIKE(DI_Items_Details)         !List box control field - type derived from local data
Delivery_Details       LIKE(Delivery_Details)         !List box control field - type derived from local data
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
DEL:DocumentCharge     LIKE(DEL:DocumentCharge)       !Browse hot field - type derived from field
DEL:FuelSurcharge      LIKE(DEL:FuelSurcharge)        !Browse hot field - type derived from field
DEL:Charge             LIKE(DEL:Charge)               !Browse hot field - type derived from field
DEL:AdditionalCharge   LIKE(DEL:AdditionalCharge)     !Browse hot field - type derived from field
DEL:VATRate            LIKE(DEL:VATRate)              !Browse hot field - type derived from field
DEL:Insure             LIKE(DEL:Insure)               !Browse hot field - type derived from field
DEL:TotalConsignmentValue LIKE(DEL:TotalConsignmentValue) !Browse hot field - type derived from field
DEL:InsuranceRate      LIKE(DEL:InsuranceRate)        !Browse hot field - type derived from field
DEL:Manifested         LIKE(DEL:Manifested)           !Browse hot field - type derived from field
DEL:Delivered          LIKE(DEL:Delivered)            !Browse hot field - type derived from field
DEL:FID                LIKE(DEL:FID)                  !Browse hot field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Primary key field - type derived from field
A_ADD:AID              LIKE(A_ADD:AID)                !Related join file key field - type derived from field
ADD:AID                LIKE(ADD:AID)                  !Related join file key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::FLO:Record  LIKE(FLO:RECORD),THREAD
BRW2::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW2::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW2::PopupChoice    SIGNED                       ! Popup current choice
BRW2::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW2::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW8::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW8::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW8::PopupChoice    SIGNED                       ! Popup current choice
BRW8::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW8::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Update the Floors File'),AT(,,249,241),FONT('Tahoma',8),RESIZE,CENTER,GRAY,IMM,MAX, |
  MDI,HLP('UpdateFloors'),SYSTEM
                       SHEET,AT(4,4,242,217),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           GROUP,AT(9,22,235,127),USE(?Group_Left)
                             PROMPT('Floor:'),AT(9,22),USE(?FLO:Floor:Prompt),TRN
                             ENTRY(@s35),AT(62,22,144,10),USE(FLO:Floor),MSG('Floor Name'),REQ,TIP('Floor Name')
                             CHECK(' &FBN Floor'),AT(62,36),USE(FLO:FBNFloor),MSG('Is this a FBN Floor'),TIP('Is this a FBN Floor'), |
  TRN
                             PROMPT('Address:'),AT(9,58),USE(?AddressName:Prompt),TRN
                             BUTTON('...'),AT(45,58,12,10),USE(?CallLookup)
                             ENTRY(@s35),AT(62,58,144,10),USE(AddressName),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                             CHECK(' Print Rates'),AT(62,86),USE(FLO:Print_Rates),MSG('Print this Floors rates on ra' & |
  'te letters'),TIP('Print this Floors rates on rate letters'),TRN
                             CHECK(' Trip Sheet Loading'),AT(62,116),USE(FLO:TripSheetLoading),MSG('Show this Floor ' & |
  'on the Trip Sheet Loading'),TIP('Show this Floor on the Trip Sheet Loading'),TRN
                           END
                         END
                         TAB('&2) Deliveries'),USE(?Tab:3)
                           PROMPT('Locate DI No.:'),AT(9,22),USE(?DINo:Prompt),TRN
                           ENTRY(@n_10b),AT(61,22,68,10),USE(L_DP:DINo),RIGHT(1),MSG('Delivery Instruction Number'),REQ, |
  TIP('Delivery Instruction Number')
                           GROUP,AT(159,196,84,22),USE(?Group_Del_Bot_Right)
                             PROMPT('To Update Progress:'),AT(159,196,67,10),USE(?Prompt4),TRN
                             BUTTON('&Insert'),AT(159,206,42,12),USE(?Button_InsertProg)
                             BUTTON('&Change'),AT(201,206,42,12),USE(?Button_InsertProg:2)
                           END
                           GROUP,AT(9,196,94,22),USE(?Group_Del_Bot_Left)
                             PROMPT('Show Manifested / Delivered:'),AT(9,196,126,10),USE(?L_G:ManifestedDelivered:Prompt), |
  TRN
                             LIST,AT(9,208,126,10),USE(L_G:ManifestedDelivered),VSCROLL,DROP(15,100),FROM('Not Manife' & |
  'sted|#0|Manifested / Not Delivered|#1|Partially Delivered|#2|Delivered|#3'),MSG('Manifested status'), |
  TIP('Manifested status')
                           END
                           LIST,AT(9,36,234,156),USE(?List),HVSCROLL,FORMAT('30R(1)|M~DI No.~L(2)@n_10@36R(1)|M~DI' & |
  ' Date~L(2)@d5b@80L(1)|M~Consignor Address~@s35@80L(1)|M~Consignee Address~@s35@100L(' & |
  '1)|M~DI Item Details~L(2)@s200@100L(1)|M~Delivery Details~L(2)@s100@70L(1)|M~Client~L(2)@s35@'), |
  FROM(Queue:Browse),IMM,MSG('Browsing Records')
                         END
                         TAB('&3) Container Park Rates'),USE(?Tab:2)
                           LIST,AT(8,21,234,182),USE(?Browse:2),HVSCROLL,FORMAT('50L(2)|M~Journey~C(0)@s70@44R(2)|' & |
  'M~To Mass~C(0)@n-12.0@46D(10)|M~Rate Per Kg~C(0)@n-11.2@5R(1)|M~Effective Date~C(0)@d5@'), |
  FROM(Queue:Browse:2),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(117,206,42,12),USE(?Insert:3)
                           BUTTON('&Change'),AT(159,206,42,12),USE(?Change:3)
                           BUTTON('&Delete'),AT(201,206,42,12),USE(?Delete:3)
                         END
                       END
                       BUTTON('&OK'),AT(136,226,,14),USE(?OK),LEFT,ICON('waok.ico'),DEFAULT,FLAT
                       BUTTON('Cancel'),AT(191,226,,14),USE(?Cancel),LEFT,ICON('wacancel.ico'),FLAT
                       BUTTON('Help'),AT(202,2,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

BRW2::LastSortOrder       BYTE
BRW8::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW8                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Floors Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Floors Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Floors')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?FLO:Floor:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_G:ManifestedDelivered',L_G:ManifestedDelivered) ! Added by: BrowseBox(ABC)
  BIND('DI_Items_Details',DI_Items_Details)       ! Added by: BrowseBox(ABC)
  BIND('Delivery_Details',Delivery_Details)       ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(FLO:Record,History::FLO:Record)
  SELF.AddHistoryField(?FLO:Floor,2)
  SELF.AddHistoryField(?FLO:FBNFloor,3)
  SELF.AddHistoryField(?FLO:Print_Rates,5)
  SELF.AddHistoryField(?FLO:TripSheetLoading,6)
  SELF.AddUpdateFile(Access:Floors)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                           ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Access:TripSheets.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Floors
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:__RatesContainerPark,SELF) ! Initialize the browse manager
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon CPRA:FID for sort order 1
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,CPRA:FKey_FID) ! Add the sort order for CPRA:FKey_FID for sort order 1
  BRW2.AddRange(CPRA:FID,FLO:FID)                 ! Add single value range limit for sort order 1
  BRW2.AppendOrder('-CPRA:Effective_Date,+JOU:Journey,+CPRA:ToMass') ! Append an additional sort order
  BRW2.AddField(JOU:Journey,BRW2.Q.JOU:Journey)   ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW2.AddField(CPRA:ToMass,BRW2.Q.CPRA:ToMass)   ! Field CPRA:ToMass is a hot field or requires assignment from browse
  BRW2.AddField(CPRA:RatePerKg,BRW2.Q.CPRA:RatePerKg) ! Field CPRA:RatePerKg is a hot field or requires assignment from browse
  BRW2.AddField(CPRA:Effective_Date,BRW2.Q.CPRA:Effective_Date) ! Field CPRA:Effective_Date is a hot field or requires assignment from browse
  BRW2.AddField(CPRA:CPID,BRW2.Q.CPRA:CPID)       ! Field CPRA:CPID is a hot field or requires assignment from browse
  BRW2.AddField(CPRA:FID,BRW2.Q.CPRA:FID)         ! Field CPRA:FID is a hot field or requires assignment from browse
  BRW2.AddField(JOU:JID,BRW2.Q.JOU:JID)           ! Field JOU:JID is a hot field or requires assignment from browse
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,DEL:Key_DINo)                ! Add the sort order for DEL:Key_DINo for sort order 1
  BRW8.AddLocator(BRW8::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW8::Sort0:Locator.Init(?L_DP:DINo,DEL:DINo,1,BRW8) ! Initialize the browse locator using ?L_DP:DINo using key: DEL:Key_DINo , DEL:DINo
  BRW8.SetFilter('(DEL:FID = FLO:FID AND ( (L_G:ManifestedDelivered = 0 AND DEL:Manifested <<= 0) OR (L_G:ManifestedDelivered = 1 AND (DEL:Manifested >= 1 AND DEL:Delivered <<= 0)) OR (L_G:ManifestedDelivered = 2 AND DEL:Delivered = 1) OR (L_G:ManifestedDelivered = 3 AND DEL:Delivered = 2) ))') ! Apply filter expression to browse
  BRW8.AddResetField(L_G:ManifestedDelivered)     ! Apply the reset field
  BRW8.AddField(DEL:DINo,BRW8.Q.DEL:DINo)         ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW8.AddField(DEL:DIDate,BRW8.Q.DEL:DIDate)     ! Field DEL:DIDate is a hot field or requires assignment from browse
  BRW8.AddField(ADD:AddressName,BRW8.Q.ADD:AddressName) ! Field ADD:AddressName is a hot field or requires assignment from browse
  BRW8.AddField(A_ADD:AddressName,BRW8.Q.A_ADD:AddressName) ! Field A_ADD:AddressName is a hot field or requires assignment from browse
  BRW8.AddField(DI_Items_Details,BRW8.Q.DI_Items_Details) ! Field DI_Items_Details is a hot field or requires assignment from browse
  BRW8.AddField(Delivery_Details,BRW8.Q.Delivery_Details) ! Field Delivery_Details is a hot field or requires assignment from browse
  BRW8.AddField(CLI:ClientName,BRW8.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW8.AddField(DEL:DocumentCharge,BRW8.Q.DEL:DocumentCharge) ! Field DEL:DocumentCharge is a hot field or requires assignment from browse
  BRW8.AddField(DEL:FuelSurcharge,BRW8.Q.DEL:FuelSurcharge) ! Field DEL:FuelSurcharge is a hot field or requires assignment from browse
  BRW8.AddField(DEL:Charge,BRW8.Q.DEL:Charge)     ! Field DEL:Charge is a hot field or requires assignment from browse
  BRW8.AddField(DEL:AdditionalCharge,BRW8.Q.DEL:AdditionalCharge) ! Field DEL:AdditionalCharge is a hot field or requires assignment from browse
  BRW8.AddField(DEL:VATRate,BRW8.Q.DEL:VATRate)   ! Field DEL:VATRate is a hot field or requires assignment from browse
  BRW8.AddField(DEL:Insure,BRW8.Q.DEL:Insure)     ! Field DEL:Insure is a hot field or requires assignment from browse
  BRW8.AddField(DEL:TotalConsignmentValue,BRW8.Q.DEL:TotalConsignmentValue) ! Field DEL:TotalConsignmentValue is a hot field or requires assignment from browse
  BRW8.AddField(DEL:InsuranceRate,BRW8.Q.DEL:InsuranceRate) ! Field DEL:InsuranceRate is a hot field or requires assignment from browse
  BRW8.AddField(DEL:Manifested,BRW8.Q.DEL:Manifested) ! Field DEL:Manifested is a hot field or requires assignment from browse
  BRW8.AddField(DEL:Delivered,BRW8.Q.DEL:Delivered) ! Field DEL:Delivered is a hot field or requires assignment from browse
  BRW8.AddField(DEL:FID,BRW8.Q.DEL:FID)           ! Field DEL:FID is a hot field or requires assignment from browse
  BRW8.AddField(DEL:DID,BRW8.Q.DEL:DID)           ! Field DEL:DID is a hot field or requires assignment from browse
  BRW8.AddField(A_ADD:AID,BRW8.Q.A_ADD:AID)       ! Field A_ADD:AID is a hot field or requires assignment from browse
  BRW8.AddField(ADD:AID,BRW8.Q.ADD:AID)           ! Field ADD:AID is a hot field or requires assignment from browse
  BRW8.AddField(CLI:CID,BRW8.Q.CLI:CID)           ! Field CLI:CID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_Floors',QuickWindow)       ! Restore window settings from non-volatile store
      L_G:ManifestedDelivered = GETINI('Update_Floors', 'ManifestedDelivered', , GLO:Local_INI)
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 2
  BRW8.AddToolbarTarget(Toolbar)                  ! Browse accepts toolbar control
  BRW8.ToolbarItem.HelpButton = ?Help
      IF p:Option = 1
         HIDE(?Tab:1)
         HIDE(?Tab:2)
      .
  BRW2::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW2::FormatManager.Init('MANTRNIS','Update_Floors',1,?Browse:2,2,BRW2::PopupTextExt,Queue:Browse:2,4,LFM_CFile,LFM_CFile.Record)
  BRW2::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW8::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW8::FormatManager.Init('MANTRNIS','Update_Floors',1,?List,8,BRW8::PopupTextExt,Queue:Browse,7,LFM_CFile,LFM_CFile.Record)
  BRW8::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  ! List Format Manager destructor
  BRW2::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW8::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_Floors',QuickWindow)             ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Addresses(FLO:Floor)
      Update_ContainerParkRates
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      ADD:AddressName = AddressName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        AddressName = ADD:AddressName
        FLO:AID = ADD:AID
      END
      ThisWindow.Reset(1)
    OF ?AddressName
      IF AddressName OR ?AddressName{PROP:Req}
        ADD:AddressName = AddressName
        IF Access:Addresses.TryFetch(ADD:Key_Name)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            AddressName = ADD:AddressName
            FLO:AID = ADD:AID
          ELSE
            CLEAR(FLO:AID)
            SELECT(?AddressName)
            CYCLE
          END
        ELSE
          FLO:AID = ADD:AID
        END
      END
      ThisWindow.Reset()
    OF ?Button_InsertProg
      ThisWindow.Update()
      GlobalRequest = InsertRecord
      Browse_DI_Progress()
      ThisWindow.Reset
    OF ?Button_InsertProg:2
      ThisWindow.Update()
      GlobalRequest = ChangeRecord
      Browse_DI_Progress()
      ThisWindow.Reset
    OF ?L_G:ManifestedDelivered
          PUTINI('Update_Floors', 'ManifestedDelivered', L_G:ManifestedDelivered, GLO:Local_INI)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_G:ManifestedDelivered
          PUTINI('Update_Floors', 'ManifestedDelivered', L_G:ManifestedDelivered, GLO:Local_INI)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          ADD:AID         = FLO:AID
          IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
             AddressName  = ADD:AddressName
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF p:Option = 1
             IF FLO:FBNFloor = TRUE
                QuickWindow{PROP:Text}   = 'Floor - ' & CLIP(FLO:Floor) & '  (FBN Floor)'
             ELSE
                QuickWindow{PROP:Text}   = 'Floor - ' & CLIP(FLO:Floor)
          .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW2::LastSortOrder <> NewOrder THEN
     BRW2::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW2::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  IF BRW2::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW2::PopupTextExt = ''
        BRW2::PopupChoiceExec = True
        BRW2::FormatManager.MakePopup(BRW2::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW2::PopupTextExt = '|-|' & CLIP(BRW2::PopupTextExt)
        END
        BRW2::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW2::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW2::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW2::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW2::PopupChoiceOn AND BRW2::PopupChoiceExec THEN
     BRW2::PopupChoiceExec = False
     BRW2::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW2::PopupTextExt)
     IF BRW2::FormatManager.DispatchChoice(BRW2::PopupChoice)
     ELSE
     END
  END


BRW8.SetQueueRecord PROCEDURE

  CODE
      CLEAR(LOC:Report_Vars)
  
      ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache)
      ! (ULONG, BYTE, <ULONG>, <*STRING>, BYTE=0),ULONG
      !
      ! p:Option
      !   0. Weight of all DID items - Volumetric considered
      !   1. Total Items not loaded on the DID    - Manifest
      !   2. Total Items loaded on the DID        - Manifest
      !   3. Total Items on the DID
      !   4. Total Items not loaded on the DID    - Tripsheet
      !   5. Total Items loaded on the DID        - Tripsheet
      !   6. Weight of all DID items - real weight
      !   7. Total Items not delivered on the DID - Tripsheet     (similar to 4)
      !
      ! p:DIID is passed to check a single DIID
      ! p:Items_DIID_List is passed to return a list of DIIDs for the given Option
  
      DI_Items_Details        = 'Items: ' & Get_DelItem_s_Totals(DEL:DID, 3) & ',  Weight: ' & Get_DelItem_s_Totals(DEL:DID, 6) / 100
  
      ! (p:TRID, p:Option, p:DID, p:TDID, p:DIID, p:Del_List)
      ! (<ULONG>, BYTE=0, <ULONG>, <ULONG>, <ULONG>, <*STRING>),ULONG
      !   1         2        3        4       5           6
      ! p:Del_List
      !   Either list of Dates & Times or TRID's
      ! p:Option
      ! TRID passed
      !   0. Weight (real weight - not volumetric)
      !   1. Units
      !       if p:DID is provided then the result is limited to the DID passed
      !       if p:TDID is provided then exclude this from weight
      !   2. List of TS-Delivery delivered Dates, no of entries returned - for DIID or TRID!
      ! DID passed only       (no of entries returned )
      !   3. List of TS delivered Dates & Times   (d5 - t1, d5 - t1)
      !   4. List of TS TRIDs
      !
      !   Note - if option 3 or 4 used but param 3 ommitted then this will cause a GPF in current form!
      CLEAR(L_G:TRIDs)
      Count_#                 = Get_TripDelItems_Info(, 4, DEL:DID, ,, L_G:TRIDs)
  
      LOOP
         IF CLIP(L_G:TRIDs) = ''
            BREAK
         .
  
         ! (p:Delim_List, p:Delim, p:Chop_Out, p:Case_Sensitive)
         ! (*STRING, STRING, BYTE = 0, BYTE = 0),STRING
         TRI:TRID                = Get_1st_Element_From_Delim_Str(L_G:TRIDs, ',', TRUE)
         IF Access:TripSheets.TryFetch(TRI:PKey_TID) = LEVEL:Benign
            ! (p:VCID, p:Option, p:Truck, p:Additional)
            ! (ULONG, BYTE, BYTE=0, BYTE=0),STRING
            ! p:Option  1.  Capacity                            of all
            !           2.  Registrations                       of all
            !           3.  Makes & Models                      of all
            !           4.  Description of Truck / Trailer      requires p:Truck  0 to 3
            !           5.  Truck / Trailer Capacity            requires p:Truck  0 to 3
            !           6.  Truck / Trailer ID (TTID)           requires p:Truck  0 to 3
            !           7.  Count of vehicles in composition
            !           8.  Compositions Capacity
            !           9.  Driver of Horse
            !           10. Transporters ID for this VCID
            !           11. Licensing dates                     of all
            !
            ! p:Truck
            !           - Truck no. in combination, 1 to 4
            !
            ! p:Additional
            !           - Specifies next option to be called, for cache
            !
            ! Cache works by caching 1 item that the caller specifies will be called next, if it expires or is not
            ! correct then not used.
  
            Delivery_Details  = ', ' & Get_VehComp_Info(TRI:VCID, 2)
         .
         Delivery_Details     = SUB(Delivery_Details, 3, LEN(Delivery_Details))
      .
  PARENT.SetQueueRecord
      IF DEL:Insure = TRUE
         L_G:InsuanceAmount   = DEL:TotalConsignmentValue * (DEL:InsuranceRate / 100)
      .
  
      L_G:TotalCharge         = DEL:Charge + DEL:AdditionalCharge + DEL:DocumentCharge + DEL:FuelSurcharge + L_G:InsuanceAmount
                                  
      L_G:Total               = L_G:TotalCharge + L_G:TotalCharge * (DEL:VATRate / 100)
  
  
  


BRW8.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW8::LastSortOrder <> NewOrder THEN
     BRW8::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW8::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  IF BRW8::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW8::PopupTextExt = ''
        BRW8::PopupChoiceExec = True
        BRW8::FormatManager.MakePopup(BRW8::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW8::PopupTextExt = '|-|' & CLIP(BRW8::PopupTextExt)
        END
        BRW8::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW8::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW8::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW8::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW8::PopupChoiceOn AND BRW8::PopupChoiceExec THEN
     BRW8::PopupChoiceExec = False
     BRW8::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW8::PopupTextExt)
     IF BRW8::FormatManager.DispatchChoice(BRW8::PopupChoice)
     ELSE
     END
  END


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  !    ! Not Manifested, Manifested / Not Delivered, Partially Delivered, Delivered
  !    CASE L_G:ManifestedDelivered
  !    OF 0
  !       IF DEL:Manifested > 0                        ! Not Manifested|Partially Manifested|Partially Manifested Multiple|Fully Manifested|Fully Manifested Multiple
  !          ReturnValue = Record:Filtered
  !       .
  !    OF 1
  !       IF DEL:Manifested < 1
  !          ReturnValue = Record:Filtered
  !       .
  !    OF 2
  !       IF DEL:Delivered ~= 1                        ! Not Delivered|Partially Delivered|Delivered
  !          ReturnValue = Record:Filtered
  !       .
  !    OF 3
  !       IF DEL:Delivered ~= 2                        ! Not Delivered|Partially Delivered|Delivered
  !          ReturnValue = Record:Filtered
  !    .  .
  !
  !    
  !    
  !!    db.debugout('ManifestedDelivered: ' & L_G:ManifestedDelivered & ',   DEL:Manifested: ' & DEL:Manifested & ',   DEL:Delivered: ' & DEL:Delivered)
  !
  !
  !    
  BRW8::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Left, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Left
  SELF.SetStrategy(?Group_Del_Bot_Right, Resize:FixRight+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Del_Bot_Right
  SELF.SetStrategy(?Group_Del_Bot_Left, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Del_Bot_Left

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_ContainerParkRates PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
Floor                STRING(35)                            !Floor Name
Journey              CSTRING(701)                          !Description
FDB8::View:FileDrop  VIEW(Journeys)
                       PROJECT(JOU:Journey)
                       PROJECT(JOU:JID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?Journey
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::CPRA:Record LIKE(CPRA:RECORD),THREAD
QuickWindow          WINDOW('Form Container Park Rates'),AT(,,163,136),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update__RatesContainerPark'),SYSTEM
                       SHEET,AT(4,4,155,113),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Floor:'),AT(9,24),USE(?Floor:Prompt),TRN
                           BUTTON('...'),AT(42,24,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(61,24,93,10),USE(Floor),MSG('Floor Name'),REQ,TIP('Floor Name')
                           PROMPT('Journey:'),AT(9,40),USE(?Floor:Prompt:2),TRN
                           BUTTON('...'),AT(42,40,12,10),USE(?Button_Journey)
                           LIST,AT(61,40,93,10),USE(Journey),HVSCROLL,DROP(15,150),FORMAT('280L(2)|M~Journey~@s255@'), |
  FROM(Queue:FileDrop)
                           PROMPT('To Mass:'),AT(9,62),USE(?CPRA:ToMass:Prompt),TRN
                           SPIN(@n-12.0),AT(82,62,71,10),USE(CPRA:ToMass),RIGHT(1),MSG('Up to this mass in Kgs'),TIP('Up to this' & |
  ' mass in Kgs')
                           PROMPT('Rate Per Kg:'),AT(9,76),USE(?CPRA:RatePerKg:Prompt),TRN
                           ENTRY(@n-13.4),AT(82,76,71,10),USE(CPRA:RatePerKg),RIGHT(1),MSG('Rate per Kg'),TIP('Rate per Kg')
                           PROMPT('Effective Date:'),AT(9,102),USE(?CPRA:Effective_Date:Prompt),TRN
                           SPIN(@d5),AT(82,102,71,10),USE(CPRA:Effective_Date),RIGHT(1),MSG('Effective from this date'), |
  REQ,TIP('Effective from this date')
                         END
                       END
                       BUTTON('&OK'),AT(58,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(110,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(110,4,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB8                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Container Park Rates Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Container Park Rates Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ContainerParkRates')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Floor:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CPRA:Record,History::CPRA:Record)
  SELF.AddHistoryField(?CPRA:ToMass,4)
  SELF.AddHistoryField(?CPRA:RatePerKg,5)
  SELF.AddHistoryField(?CPRA:Effective_Date,8)
  SELF.AddUpdateFile(Access:__RatesContainerPark)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Floors.Open                                       ! File Floors used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:__RatesContainerPark
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?Floor{PROP:ReadOnly} = True
    DISABLE(?Button_Journey)
    DISABLE(?Journey)
    ?CPRA:RatePerKg{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_ContainerParkRates',QuickWindow)    ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  FDB8.Init(?Journey,Queue:FileDrop.ViewPosition,FDB8::View:FileDrop,Queue:FileDrop,Relate:Journeys,ThisWindow)
  FDB8.Q &= Queue:FileDrop
  FDB8.AddSortOrder(JOU:Key_Journey)
  FDB8.AddField(JOU:Journey,FDB8.Q.JOU:Journey) !List box control field - type derived from field
  FDB8.AddField(JOU:JID,FDB8.Q.JOU:JID) !Primary key field - type derived from field
  FDB8.AddUpdateField(JOU:JID,CPRA:JID)
  ThisWindow.AddItem(FDB8.WindowComponent)
  FDB8.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Floors.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_ContainerParkRates',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Floors('')
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      FLO:Floor = Floor
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        Floor = FLO:Floor
        CPRA:FID = FLO:FID
      END
      ThisWindow.Reset(1)
    OF ?Floor
      IF Floor OR ?Floor{PROP:Req}
        FLO:Floor = Floor
        IF Access:Floors.TryFetch(FLO:Key_Floor)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Floor = FLO:Floor
            CPRA:FID = FLO:FID
          ELSE
            CLEAR(CPRA:FID)
            SELECT(?Floor)
            CYCLE
          END
        ELSE
          CPRA:FID = FLO:FID
        END
      END
      ThisWindow.Reset()
    OF ?Button_Journey
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Journey()
      ThisWindow.Reset
          Journey       = JOU:Journey
          CPRA:JID      = JOU:JID
          DISPLAY
      FDB8.ResetQueue(1)
          JOU:Journey   = Journey
          JOU:JID       = CPRA:JID
          DISPLAY
    OF ?OK
      ThisWindow.Update()
          IF CPRA:JID = 0
             MESSAGE('Please provide a Journey.', 'Update Container Park Rates', ICON:Exclamation)
             QuickWindow{PROP:AcceptAll}   = FALSE
             SELECT(?Journey)
             CYCLE
          .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF SELF.Request = InsertRecord
             CPRA:FID     = FLO:FID
             CPRA:JID     = JOU:JID
          .
      
          FLO:FID         = CPRA:FID
          IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
             Floor        = FLO:Floor
          .
      
          JOU:JID         = CPRA:JID
          IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
             Journey      = JOU:Journey
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_DI_Progress PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(DeliveryProgress)
                       PROJECT(DELP:StatusDate)
                       PROJECT(DELP:StatusTime)
                       PROJECT(DELP:DPID)
                       PROJECT(DELP:CalledAID)
                       PROJECT(DELP:ACID)
                       PROJECT(DELP:DSID)
                       PROJECT(DELP:DID)
                       JOIN(ADD:PKey_AID,DELP:CalledAID)
                         PROJECT(ADD:AddressName)
                         PROJECT(ADD:AID)
                       END
                       JOIN(ADDC:PKey_ACID,DELP:ACID)
                         PROJECT(ADDC:ContactName)
                         PROJECT(ADDC:ACID)
                       END
                       JOIN(DELS:PKey_DSID,DELP:DSID)
                         PROJECT(DELS:DeliveryStatus)
                         PROJECT(DELS:DSID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DELP:StatusDate        LIKE(DELP:StatusDate)          !List box control field - type derived from field
DELP:StatusTime        LIKE(DELP:StatusTime)          !List box control field - type derived from field
ADDC:ContactName       LIKE(ADDC:ContactName)         !List box control field - type derived from field
DELS:DeliveryStatus    LIKE(DELS:DeliveryStatus)      !List box control field - type derived from field
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
DELP:DPID              LIKE(DELP:DPID)                !Primary key field - type derived from field
DELP:CalledAID         LIKE(DELP:CalledAID)           !Browse key field - type derived from field
DELP:ACID              LIKE(DELP:ACID)                !Browse key field - type derived from field
DELP:DSID              LIKE(DELP:DSID)                !Browse key field - type derived from field
DELP:DID               LIKE(DELP:DID)                 !Browse key field - type derived from field
ADD:AID                LIKE(ADD:AID)                  !Related join file key field - type derived from field
ADDC:ACID              LIKE(ADDC:ACID)                !Related join file key field - type derived from field
DELS:DSID              LIKE(DELS:DSID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Delivery Progress File'),AT(,,330,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Update_DI_Progress'),SYSTEM
                       LIST,AT(8,30,314,124),USE(?Browse:1),HVSCROLL,FORMAT('42R(2)|M~Status Date~C(0)@d5@42R(' & |
  '2)|M~Status Time~C(0)@t7@80L(2)|M~Contact Name~C(0)@s35@100L(2)|M~Delivery Status~C(' & |
  '0)@s35@80L(2)|M~Address~C(0)@s35@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Deliv' & |
  'eryProgress file')
                       BUTTON('&Select'),AT(61,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(114,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(167,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(220,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(273,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,322,172),USE(?CurrentTab)
                         TAB('&1) By Delivery'),USE(?Tab:2)
                           BUTTON('Delivery'),AT(9,158,49,14),USE(?SelectDeliveries),LEFT,ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                           GROUP,AT(5,184,88,10),USE(?Group_DI)
                             PROMPT('DI No.:'),AT(5,184),USE(?DEL:DINo:Prompt)
                             ENTRY(@n_10b),AT(33,184,60,10),USE(DEL:DINo),RIGHT(1),COLOR(00E9E9E9h),MSG('Delivery In' & |
  'struction Number'),READONLY,SKIP,TIP('Delivery Instruction Number')
                           END
                         END
                         TAB('&2) By Called Address'),USE(?Tab:3)
                         END
                         TAB('&3) By Address Contact'),USE(?Tab:4)
                         END
                         TAB('&4) By Delivery Status'),USE(?Tab:5)
                           BUTTON('Delivery Status...'),AT(9,158,50,14),USE(?SelectDeliveryStatuses),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(277,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(220,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_DI_Progress')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Deliveries.Open                          ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Access:DeliveryStatuses.UseFile                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:DeliveryProgress,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,DELP:FKey_CalledAID)         ! Add the sort order for DELP:FKey_CalledAID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)            ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,DELP:CalledAID,1,BRW1) ! Initialize the browse locator using  using key: DELP:FKey_CalledAID , DELP:CalledAID
  BRW1.AppendOrder('+DELP:DID,+DELP:DPID')        ! Append an additional sort order
  BRW1.AddSortOrder(,DELP:FKey_ACID)              ! Add the sort order for DELP:FKey_ACID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,DELP:ACID,1,BRW1)     ! Initialize the browse locator using  using key: DELP:FKey_ACID , DELP:ACID
  BRW1.AppendOrder('+DELP:DID,+DELP:DPID')        ! Append an additional sort order
  BRW1.AddSortOrder(,DELP:FKey_DSID)              ! Add the sort order for DELP:FKey_DSID for sort order 3
  BRW1.AddRange(DELP:DSID,Relate:DeliveryProgress,Relate:DeliveryStatuses) ! Add file relationship range limit for sort order 3
  BRW1.AppendOrder('+DELP:DID,+DELP:DPID')        ! Append an additional sort order
  BRW1.AddSortOrder(,DELP:FKey_DID)               ! Add the sort order for DELP:FKey_DID for sort order 4
  BRW1.AddRange(DELP:DID,Relate:DeliveryProgress,Relate:Deliveries) ! Add file relationship range limit for sort order 4
  BRW1.AppendOrder('+DELP:StatusDateAndTime,+DELP:DPID') ! Append an additional sort order
  BRW1.AddField(DELP:StatusDate,BRW1.Q.DELP:StatusDate) ! Field DELP:StatusDate is a hot field or requires assignment from browse
  BRW1.AddField(DELP:StatusTime,BRW1.Q.DELP:StatusTime) ! Field DELP:StatusTime is a hot field or requires assignment from browse
  BRW1.AddField(ADDC:ContactName,BRW1.Q.ADDC:ContactName) ! Field ADDC:ContactName is a hot field or requires assignment from browse
  BRW1.AddField(DELS:DeliveryStatus,BRW1.Q.DELS:DeliveryStatus) ! Field DELS:DeliveryStatus is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AddressName,BRW1.Q.ADD:AddressName) ! Field ADD:AddressName is a hot field or requires assignment from browse
  BRW1.AddField(DELP:DPID,BRW1.Q.DELP:DPID)       ! Field DELP:DPID is a hot field or requires assignment from browse
  BRW1.AddField(DELP:CalledAID,BRW1.Q.DELP:CalledAID) ! Field DELP:CalledAID is a hot field or requires assignment from browse
  BRW1.AddField(DELP:ACID,BRW1.Q.DELP:ACID)       ! Field DELP:ACID is a hot field or requires assignment from browse
  BRW1.AddField(DELP:DSID,BRW1.Q.DELP:DSID)       ! Field DELP:DSID is a hot field or requires assignment from browse
  BRW1.AddField(DELP:DID,BRW1.Q.DELP:DID)         ! Field DELP:DID is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AID,BRW1.Q.ADD:AID)           ! Field ADD:AID is a hot field or requires assignment from browse
  BRW1.AddField(ADDC:ACID,BRW1.Q.ADDC:ACID)       ! Field ADDC:ACID is a hot field or requires assignment from browse
  BRW1.AddField(DELS:DSID,BRW1.Q.DELS:DSID)       ! Field DELS:DSID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_DI_Progress',QuickWindow)  ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_DI_Progress',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,5,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
      IF SELF.Request = InsertRecord
         POST(EVENT:Accepted, ?Insert:4)
      .
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_DI_Progress',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_DeliveryProgress
    ReturnValue = GlobalResponse
  END
    IF Request = InsertRecord AND ReturnValue = RequestCompleted
       POST(EVENT:CloseWindow)
    .
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectDeliveries
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Deliveries()
      ThisWindow.Reset
    OF ?SelectDeliveryStatuses
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_DeliveryStatuses()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetAlerts PROCEDURE

  CODE
  SELF.EditViaPopup = False
  PARENT.SetAlerts


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>3,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>3,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_DI, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_DI

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_ClientsRateTypes PROCEDURE (p:CID, p:LTID)

CurrentTab           STRING(80)                            !
LOC:ContainerOptions STRING(20)                            !Container option
LOC:FullLoad_ContainerPark STRING(20)                      !None, Full Load, Container Park
LOC:LTID             ULONG                                 !Load Type ID
LOC:LoadType         STRING(100)                           !Load Type
LoadOption           STRING(30)                            !Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
BRW1::View:Browse    VIEW(ClientsRateTypes)
                       PROJECT(CRT:ClientRateType)
                       PROJECT(CRT:CID)
                       PROJECT(CRT:CRTID)
                       PROJECT(CRT:LTID)
                       JOIN(LOAD2:PKey_LTID,CRT:LTID)
                         PROJECT(LOAD2:LoadType)
                         PROJECT(LOAD2:LTID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
CRT:ClientRateType     LIKE(CRT:ClientRateType)       !List box control field - type derived from field
LOAD2:LoadType         LIKE(LOAD2:LoadType)           !List box control field - type derived from field
CRT:CID                LIKE(CRT:CID)                  !List box control field - type derived from field
CRT:CRTID              LIKE(CRT:CRTID)                !Browse hot field - type derived from field
CRT:LTID               LIKE(CRT:LTID)                 !Browse hot field - type derived from field
LOAD2:LTID             LIKE(LOAD2:LTID)               !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB8::View:FileDrop  VIEW(LoadTypes2Alias)
                       PROJECT(A_LOAD2:LoadType)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?A_LOAD2:LoadType
A_LOAD2:LoadType       LIKE(A_LOAD2:LoadType)         !List box control field - type derived from field
LoadOption             LIKE(LoadOption)               !List box control field - type derived from local data
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Client Rate Types File'),AT(,,300,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MAX,MDI,HLP('BrowseLoadTypes'),SYSTEM
                       LIST,AT(8,22,283,132),USE(?Browse:1),HVSCROLL,FORMAT('200L(2)|M~Client Rate Type~@s80@1' & |
  '00L(2)|M~Load Type~@s100@40R(2)|M~CID~L@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he LoadTypes file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(82,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(136,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(188,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(242,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,292,172),USE(?CurrentTab)
                         TAB('&1) By Client Rate Type'),USE(?Tab:2)
                         END
                         TAB('2) All Clients'),USE(?TAB_AllClients),HIDE
                         END
                       END
                       GROUP,AT(4,182,156,10),USE(?Group_LoadType)
                         PROMPT('Load Type:'),AT(4,182),USE(?Prompt1)
                         LIST,AT(44,182,116,10),USE(A_LOAD2:LoadType),HVSCROLL,DROP(15),FORMAT('150L(2)|M~Load T' & |
  'ype~@s100@120L(2)|M~Load Option~@s30@'),FROM(Queue:FileDrop),MSG('Load Type')
                       END
                       BUTTON('Default Rates'),AT(176,180,55,14),USE(?Button_Default_Rates)
                       BUTTON('&Close'),AT(247,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(246,2,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB8                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_ClientsRateTypes')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:LTID',LOC:LTID)                       ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Clients.Open                             ! File Clients used by this procedure, so make sure it's RelationManager is open
  Relate:LoadTypes2Alias.Open                     ! File LoadTypes2Alias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
      IF p:CID ~= 0
         CLI:CID  = p:CID
         IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
  
      .  .
  
  
      LOC:LTID        = p:LTID
  
      A_LOAD2:LTID    = p:LTID
      IF Access:LoadTypes2Alias.TryFetch(A_LOAD2:PKey_LTID) = LEVEL:Benign
         LOC:LoadType = A_LOAD2:LoadType
      .
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ClientsRateTypes,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  IF p:CID = 0 AND p:LTID = 0 AND CLI:CID = 0
     UNHIDE(?TAB_AllClients)
     HIDE(?Tab:2)
  .
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.FileLoaded = 1                             ! This is a 'file loaded' browse
  BRW1.AddSortOrder(,CRT:Key_ClientRateType)      ! Add the sort order for CRT:Key_ClientRateType for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)            ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,CRT:ClientRateType,1,BRW1) ! Initialize the browse locator using  using key: CRT:Key_ClientRateType , CRT:ClientRateType
  BRW1.AddSortOrder(,CRT:FKey_CID)                ! Add the sort order for CRT:FKey_CID for sort order 2
  BRW1.AddRange(CRT:CID,Relate:ClientsRateTypes,Relate:Clients) ! Add file relationship range limit for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,CRT:CID,1,BRW1)       ! Initialize the browse locator using  using key: CRT:FKey_CID , CRT:CID
  BRW1.AppendOrder('+CRT:ClientRateType')         ! Append an additional sort order
  BRW1.SetFilter('(LOC:LTID = 0 OR LOC:LTID = CRT:LTID)') ! Apply filter expression to browse
  BRW1.AddResetField(LOC:LTID)                    ! Apply the reset field
  BRW1.AddField(CRT:ClientRateType,BRW1.Q.CRT:ClientRateType) ! Field CRT:ClientRateType is a hot field or requires assignment from browse
  BRW1.AddField(LOAD2:LoadType,BRW1.Q.LOAD2:LoadType) ! Field LOAD2:LoadType is a hot field or requires assignment from browse
  BRW1.AddField(CRT:CID,BRW1.Q.CRT:CID)           ! Field CRT:CID is a hot field or requires assignment from browse
  BRW1.AddField(CRT:CRTID,BRW1.Q.CRT:CRTID)       ! Field CRT:CRTID is a hot field or requires assignment from browse
  BRW1.AddField(CRT:LTID,BRW1.Q.CRT:LTID)         ! Field CRT:LTID is a hot field or requires assignment from browse
  BRW1.AddField(LOAD2:LTID,BRW1.Q.LOAD2:LTID)     ! Field LOAD2:LTID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_ClientsRateTypes',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  FDB8.Init(?A_LOAD2:LoadType,Queue:FileDrop.ViewPosition,FDB8::View:FileDrop,Queue:FileDrop,Relate:LoadTypes2Alias,ThisWindow)
  FDB8.Q &= Queue:FileDrop
  FDB8.AddSortOrder(A_LOAD2:Key_LoadType)
  FDB8.AddField(A_LOAD2:LoadType,FDB8.Q.A_LOAD2:LoadType) !List box control field - type derived from field
  FDB8.AddField(LoadOption,FDB8.Q.LoadOption) !List box control field - type derived from local data
  FDB8.AddUpdateField(A_LOAD2:LTID,LOC:LTID)
  ThisWindow.AddItem(FDB8.WindowComponent)
  FDB8.DefaultFill = 0
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_ClientsRateTypes',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,3,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
    Relate:LoadTypes2Alias.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_ClientsRateTypes',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_ClientsRateTypes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Default_Rates
      ThisWindow.Update()
          CLI:CID     = Get_Branch_Info(,4)
          IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
          .
          
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF p:CID ~= 0
             IF CLI:CID = p:CID
                QuickWindow{PROP:Text}    = CLIP(QuickWindow{PROP:Text}) & ' - ' & CLIP(CLI:ClientName)
          .  .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>1,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>1,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_LoadType, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_LoadType


FDB8.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      Queue:FileDrop.A_LOAD2:LoadType       = 'All'
      GET(Queue:FileDrop, Queue:FileDrop.A_LOAD2:LoadType)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop)
         Queue:FileDrop.A_LOAD2:LoadType       = 'All'
         ADD(Queue:FileDrop)
      .
  
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_ClientsRateTypes PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LoadType             STRING(100)                           !Load Type
FDB5::View:FileDrop  VIEW(LoadTypes2)
                       PROJECT(LOAD2:LoadType)
                       PROJECT(LOAD2:LTID)
                     END
BRW8::View:Browse    VIEW(__Rates)
                       PROJECT(RAT:ToMass)
                       PROJECT(RAT:RatePerKg)
                       PROJECT(RAT:MinimiumCharge)
                       PROJECT(RAT:Effective_Date)
                       PROJECT(RAT:RID)
                       PROJECT(RAT:CRTID)
                       PROJECT(RAT:JID)
                       PROJECT(RAT:LTID)
                       JOIN(JOU:PKey_JID,RAT:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                       JOIN(LOAD2:PKey_LTID,RAT:LTID)
                         PROJECT(LOAD2:LoadType)
                         PROJECT(LOAD2:LTID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LOAD2:LoadType         LIKE(LOAD2:LoadType)           !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
RAT:ToMass             LIKE(RAT:ToMass)               !List box control field - type derived from field
RAT:RatePerKg          LIKE(RAT:RatePerKg)            !List box control field - type derived from field
RAT:MinimiumCharge     LIKE(RAT:MinimiumCharge)       !List box control field - type derived from field
RAT:Effective_Date     LIKE(RAT:Effective_Date)       !List box control field - type derived from field
RAT:RID                LIKE(RAT:RID)                  !Primary key field - type derived from field
RAT:CRTID              LIKE(RAT:CRTID)                !Browse key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
LOAD2:LTID             LIKE(LOAD2:LTID)               !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?LoadType
LOAD2:LoadType         LIKE(LOAD2:LoadType)           !List box control field - type derived from field
LOAD2:LTID             LIKE(LOAD2:LTID)               !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::CRT:Record  LIKE(CRT:RECORD),THREAD
BRW8::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW8::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW8::PopupChoice    SIGNED                       ! Popup current choice
BRW8::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW8::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Clients Rate Types'),AT(,,358,108),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_ClientsRateTypes'),SYSTEM
                       SHEET,AT(4,4,350,87),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('CRTID:'),AT(283,6),USE(?CRT:CRTID:Prompt)
                           STRING(@n_10),AT(311,6),USE(CRT:CRTID),RIGHT(1)
                           PROMPT('Client Name:'),AT(9,22),USE(?CLI:ClientName:Prompt),TRN
                           ENTRY(@s100),AT(79,22,206,10),USE(CLI:ClientName),COLOR(00E9E9E9h),FLAT,READONLY,SKIP,TIP('Client name')
                           PROMPT('CID:'),AT(287,22),USE(?CRT:CID:Prompt)
                           STRING(@n_10),AT(305,22),USE(CRT:CID),RIGHT(1)
                           PROMPT('Load Type:'),AT(9,44),USE(?Prompt2),TRN
                           LIST,AT(79,44,143,10),USE(LoadType),VSCROLL,DROP(15),FORMAT('400L(2)|M~Load Type~@s100@'), |
  FROM(Queue:FileDrop)
                           PROMPT('Client Rate Type:'),AT(9,60),USE(?CRT:ClientRateType:Prompt),TRN
                           ENTRY(@s100),AT(79,60,270,10),USE(CRT:ClientRateType),MSG('Load Type'),REQ,TIP('Load Type')
                         END
                         TAB('&2) Rates'),USE(?Tab2)
                           LIST,AT(9,20,342,66),USE(?List),VSCROLL,FORMAT('80L(2)|M~Load Type~@s100@70L(2)|M~Journ' & |
  'ey~@s70@48R(2)|M~To Mass~L@n-12.0@52R(2)|M~Rate Per Kg~L@n-13.4@56R(2)|M~Minimium Ch' & |
  'arge~L@n-14.2@32R(2)|M~Effective Date~L@d5@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('&OK'),AT(254,92,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(305,92,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,92,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW8::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB5                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

BRW8                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Client Rate Types Record'
  OF InsertRecord
    ActionMessage = 'Client Rate Types Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Client Rate Types Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ClientsRateTypes')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CRT:CRTID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CRT:Record,History::CRT:Record)
  SELF.AddHistoryField(?CRT:CRTID,1)
  SELF.AddHistoryField(?CRT:CID,2)
  SELF.AddHistoryField(?CRT:ClientRateType,3)
  SELF.AddUpdateFile(Access:ClientsRateTypes)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Clients.Open                             ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ClientsRateTypes
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:__Rates,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    ?CLI:ClientName{PROP:ReadOnly} = True
    DISABLE(?LoadType)
    ?CRT:ClientRateType{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  BRW8.Q &= Queue:Browse
  BRW8.FileLoaded = 1                             ! This is a 'file loaded' browse
  BRW8.AddSortOrder(,RAT:FKey_CRTID)              ! Add the sort order for RAT:FKey_CRTID for sort order 1
  BRW8.AddRange(RAT:CRTID,CRT:CRTID)              ! Add single value range limit for sort order 1
  BRW8.AddLocator(BRW8::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW8::Sort0:Locator.Init(,RAT:CRTID,1,BRW8)     ! Initialize the browse locator using  using key: RAT:FKey_CRTID , RAT:CRTID
  BRW8.AppendOrder('+LOAD2:LoadType,+JOU:Journey,+RAT:ToMass,+RAT:RatePerKg,+RAT:RID') ! Append an additional sort order
  BRW8.AddField(LOAD2:LoadType,BRW8.Q.LOAD2:LoadType) ! Field LOAD2:LoadType is a hot field or requires assignment from browse
  BRW8.AddField(JOU:Journey,BRW8.Q.JOU:Journey)   ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW8.AddField(RAT:ToMass,BRW8.Q.RAT:ToMass)     ! Field RAT:ToMass is a hot field or requires assignment from browse
  BRW8.AddField(RAT:RatePerKg,BRW8.Q.RAT:RatePerKg) ! Field RAT:RatePerKg is a hot field or requires assignment from browse
  BRW8.AddField(RAT:MinimiumCharge,BRW8.Q.RAT:MinimiumCharge) ! Field RAT:MinimiumCharge is a hot field or requires assignment from browse
  BRW8.AddField(RAT:Effective_Date,BRW8.Q.RAT:Effective_Date) ! Field RAT:Effective_Date is a hot field or requires assignment from browse
  BRW8.AddField(RAT:RID,BRW8.Q.RAT:RID)           ! Field RAT:RID is a hot field or requires assignment from browse
  BRW8.AddField(RAT:CRTID,BRW8.Q.RAT:CRTID)       ! Field RAT:CRTID is a hot field or requires assignment from browse
  BRW8.AddField(JOU:JID,BRW8.Q.JOU:JID)           ! Field JOU:JID is a hot field or requires assignment from browse
  BRW8.AddField(LOAD2:LTID,BRW8.Q.LOAD2:LTID)     ! Field LOAD2:LTID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_ClientsRateTypes',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  FDB5.Init(?LoadType,Queue:FileDrop.ViewPosition,FDB5::View:FileDrop,Queue:FileDrop,Relate:LoadTypes2,ThisWindow)
  FDB5.Q &= Queue:FileDrop
  FDB5.AddSortOrder(LOAD2:Key_LoadType)
  FDB5.AddField(LOAD2:LoadType,FDB5.Q.LOAD2:LoadType) !List box control field - type derived from field
  FDB5.AddField(LOAD2:LTID,FDB5.Q.LOAD2:LTID) !Primary key field - type derived from field
  FDB5.AddUpdateField(LOAD2:LTID,CRT:LTID)
  ThisWindow.AddItem(FDB5.WindowComponent)
  FDB5.DefaultFill = 0
  BRW8.AddToolbarTarget(Toolbar)                  ! Browse accepts toolbar control
  BRW8.ToolbarItem.HelpButton = ?Help
  BRW8::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW8::FormatManager.Init('MANTRNIS','Update_ClientsRateTypes',1,?List,8,BRW8::PopupTextExt,Queue:Browse,6,LFM_CFile,LFM_CFile.Record)
  BRW8::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  ! List Format Manager destructor
  BRW8::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_ClientsRateTypes',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          LOAD2:LTID  = CRT:LTID
          IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
             LoadType = LOAD2:LoadType
          .
      
      
          IF SELF.Request = InsertRecord
             IF CRT:CID = 0
                CRT:CID  = CLI:CID
          .  .
      
          CLI:CID     = CRT:CID
          IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW8.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW8::LastSortOrder <> NewOrder THEN
     BRW8::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW8::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  IF BRW8::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW8::PopupTextExt = ''
        BRW8::PopupChoiceExec = True
        BRW8::FormatManager.MakePopup(BRW8::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW8::PopupTextExt = '|-|' & CLIP(BRW8::PopupTextExt)
        END
        BRW8::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW8::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW8::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW8::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW8::PopupChoiceOn AND BRW8::PopupChoiceExec THEN
     BRW8::PopupChoiceExec = False
     BRW8::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW8::PopupTextExt)
     IF BRW8::FormatManager.DispatchChoice(BRW8::PopupChoice)
     ELSE
     END
  END

