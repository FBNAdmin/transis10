

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('MANTRNIS036.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Clients PROCEDURE (<shpTagClass p_Client_Tags>,BYTE p_Option,<STRING p_ClientName>)

CurrentTab           STRING(80)                            !
LOC:Locator          STRING(50)                            !
LOC:User_Access      LONG                                  !
L_BG:Browse_Group    GROUP,PRE()                           !
L_BG:Ton_Volume      DECIMAL(5,2)                          !
L_BG:Client_Status   STRING(20)                            !
L_BG:InsuranceType   STRING(20)                            !Insurance Type
L_BG:DIs_Last_Period ULONG                                 !DI's in last period
L_BG:Weight_Last_Period DECIMAL(12,1)                      !
L_BG:Cost_Last_Period DECIMAL(15,2)                        !
L_BG:Cost_to_Weight_Last_Period DECIMAL(15,4)              !
L_BG:DIs_To_Date     ULONG                                 !
L_BG:Weight_To_Date  DECIMAL(12,1)                         !
L_BG:Cost_To_Date    DECIMAL(15,2)                         !
L_BG:Cost_to_Weight_To_Date DECIMAL(15,4)                  !
                     END                                   !
LOC:Client_Contacts  STRING(150)                           !
LOC:PhoneNo          STRING(20)                            !Phone no.
LOC:AID              ULONG                                 !Address ID - note once used certain information should not be changeable, such as the suburb
LOC:Tagging_Group    GROUP,PRE(LOC)                        !
Tagging              BYTE                                  !Is Tagging on?
Selected_Clients     BYTE                                  !
                     END                                   !
LOC:Options          GROUP,PRE(L_OG)                       !
Show_Activity        BYTE                                  !
No_DIs_Str           STRING(150)                           !
SQL                  STRING(255)                           !
Account_Status       LONG(-1)                              !Normal, On Hold, Closed, Dormant
                     END                                   !
LOC:Period_Dates     GROUP,PRE(LOD)                        !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
LOC:Filters_Group    GROUP,PRE(L_FG)                       !
Fuel_Surcharge_Clients BYTE                                !Filter on Clients that have specific Fuel Surcharge records
                     END                                   !
LOC:Clients_FuelSur_Q QUEUE,PRE(L_CFS_Q)                   !
CID                  ULONG                                 !Client ID
                     END                                   !
BRW1::View:Browse    VIEW(Clients)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:MinimiumCharge)
                       PROJECT(CLI:DocumentCharge)
                       PROJECT(CLI:AccountLimit)
                       PROJECT(CLI:OpsManager)
                       PROJECT(CLI:VATNo)
                       PROJECT(CLI:InsuranceRequired)
                       PROJECT(CLI:InsurancePercent)
                       PROJECT(CLI:FuelSurchargeActive)
                       PROJECT(CLI:TollChargeActive)
                       PROJECT(CLI:CID)
                       PROJECT(CLI:Terms)
                       PROJECT(CLI:VolumetricRatio)
                       PROJECT(CLI:Status)
                       PROJECT(CLI:AID)
                       PROJECT(CLI:BID)
                       PROJECT(CLI:ClientSearch)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:ClientName_Style   LONG                           !Field style
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
CLI:ClientNo_Style     LONG                           !Field style
CLI:MinimiumCharge     LIKE(CLI:MinimiumCharge)       !List box control field - type derived from field
CLI:DocumentCharge     LIKE(CLI:DocumentCharge)       !List box control field - type derived from field
CLI:AccountLimit       LIKE(CLI:AccountLimit)         !List box control field - type derived from field
CLI:OpsManager         LIKE(CLI:OpsManager)           !List box control field - type derived from field
L_BG:Client_Status     LIKE(L_BG:Client_Status)       !List box control field - type derived from local data
L_BG:DIs_Last_Period   LIKE(L_BG:DIs_Last_Period)     !List box control field - type derived from local data
L_BG:DIs_Last_Period_Style LONG                       !Field style
L_BG:Weight_Last_Period LIKE(L_BG:Weight_Last_Period) !List box control field - type derived from local data
L_BG:Weight_Last_Period_Style LONG                    !Field style
L_BG:Cost_Last_Period  LIKE(L_BG:Cost_Last_Period)    !List box control field - type derived from local data
L_BG:Cost_Last_Period_Style LONG                      !Field style
L_BG:Cost_to_Weight_Last_Period LIKE(L_BG:Cost_to_Weight_Last_Period) !List box control field - type derived from local data
L_BG:Cost_to_Weight_Last_Period_Style LONG            !Field style
L_BG:DIs_To_Date       LIKE(L_BG:DIs_To_Date)         !List box control field - type derived from local data
L_BG:DIs_To_Date_Style LONG                           !Field style
L_BG:Weight_To_Date    LIKE(L_BG:Weight_To_Date)      !List box control field - type derived from local data
L_BG:Weight_To_Date_Style LONG                        !Field style
L_BG:Cost_To_Date      LIKE(L_BG:Cost_To_Date)        !List box control field - type derived from local data
L_BG:Cost_To_Date_Style LONG                          !Field style
L_BG:Cost_to_Weight_To_Date LIKE(L_BG:Cost_to_Weight_To_Date) !List box control field - type derived from local data
L_BG:Cost_to_Weight_To_Date_Style LONG                !Field style
CLI:VATNo              LIKE(CLI:VATNo)                !List box control field - type derived from field
CLI:InsuranceRequired  LIKE(CLI:InsuranceRequired)    !List box control field - type derived from field
CLI:InsuranceRequired_Icon LONG                       !Entry's icon ID
L_BG:InsuranceType     LIKE(L_BG:InsuranceType)       !List box control field - type derived from local data
CLI:InsurancePercent   LIKE(CLI:InsurancePercent)     !List box control field - type derived from field
CLI:FuelSurchargeActive LIKE(CLI:FuelSurchargeActive) !List box control field - type derived from field
CLI:FuelSurchargeActive_Icon LONG                     !Entry's icon ID
CLI:TollChargeActive   LIKE(CLI:TollChargeActive)     !List box control field - type derived from field
CLI:TollChargeActive_Icon LONG                        !Entry's icon ID
CLI:CID                LIKE(CLI:CID)                  !List box control field - type derived from field
CLI:Terms              LIKE(CLI:Terms)                !Browse hot field - type derived from field
CLI:VolumetricRatio    LIKE(CLI:VolumetricRatio)      !Browse hot field - type derived from field
CLI:Status             LIKE(CLI:Status)               !Browse hot field - type derived from field
CLI:AID                LIKE(CLI:AID)                  !Browse hot field - type derived from field
CLI:BID                LIKE(CLI:BID)                  !Browse key field - type derived from field
CLI:ClientSearch       LIKE(CLI:ClientSearch)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Clients File'),AT(,,441,240),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MAX,MDI,HLP('Browse_Clients'),SYSTEM
                       GROUP,AT(240,6,197,10),USE(?Group_Contact)
                         PROMPT('Contact:'),AT(240,6),USE(?Prompt2)
                         ENTRY(@s150),AT(272,6,165,10),USE(LOC:Client_Contacts),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                       END
                       LIST,AT(8,32,424,167),USE(?Browse:1),HVSCROLL,ALRT(CtrlA),ALRT(CtrlU),ALRT(MouseLeft2),FORMAT('80L(2)|MY~' & |
  'Client Name~@s100@38R(2)|MY~Client No.~L(2)@n_10b@[44D(12)|M~Min. Charge~C(0)@n-11.2' & |
  '@42R(1)|M~Docs. Charge~L(1)@n-11.2@50R(1)|M~Account Limit~C(0)@n-17.2@]|M~Charges~50' & |
  'L(1)|M~Ops. Manager~C(0)@s35@40L(1)|M~Status~C(0)@s20@[28R(1)|MY~DI''s (last period)' & |
  '~L(1)@n6@38R(1)|MY~Weight (tons last period)~L(1)@n-17.1@38R(1)|MY~Value (last perio' & |
  'd)~L(1)@n-21@38R(1)|MY~Value to Weight (last period)~L(1)@n-20@28R(1)|MY~DI''s (to d' & |
  'ate)~L(1)@n8@38R(1)|MY~Weight (tons to date)~L(1)@n-17.1@38R(1)|MY~Value (to date)~L' & |
  '(1)@n-21@38R(1)|MY~Value to Weight (to date)~L(1)@n-20@](281)|M~Activity~40L(1)|M~VA' & |
  'T No.~C(0)@s20@[32L(1)|MI~Required~C(0)@p p@50L(1)|M~Type~C(0)@s20@40R(1)|M~Percent~' & |
  'C(0)@n-11.4@]|M~Insurance~25R(1)|MI~Fuel Surcharge Active~L(1)@p p@25R(1)|MI~Toll Ch' & |
  'arge Active~L(1)@p p@40R(1)|M~CID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he Clients file')
                       BUTTON('&Select'),AT(174,202,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('Select Tagged'),AT(92,202,,14),USE(?Select_Tagged),LEFT,ICON('CHECK1.ICO'),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                       STRING('Tagged'),AT(253,20,179,10),USE(?String_Tagged),RIGHT(1),TRN
                       BUTTON('&View'),AT(228,202,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(280,202,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(332,202,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(384,202,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       BUTTON('&Print Rates'),AT(4,222,,14),USE(?Button_PrintRates),LEFT,ICON(ICON:Print1),FLAT
                       BUTTON('&Rates'),AT(75,222,,14),USE(?Button_ShowRates),LEFT,ICON('List.ico'),FLAT,TIP('Show Rates')
                       BUTTON('Activity &Graphs'),AT(308,222,,14),USE(?Button_Graph),LEFT,ICON('Atime.ico'),FLAT
                       CHECK(' Show &Activity'),AT(138,226),USE(L_OG:Show_Activity),TIP('Note: this will cause' & |
  ' a slow down in browse loading when checked')
                       SHEET,AT(4,4,433,215),USE(?CurrentTab)
                         TAB('&1) By Client Name'),USE(?Tab:2)
                           BUTTON('&Filters'),AT(9,202,49,14),USE(?Button_Filters)
                         END
                         TAB('&2) By Branch && Name'),USE(?Tab:4)
                           BUTTON('Select Branch'),AT(9,202,,14),USE(?SelectBranches),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&3) By Client No.'),USE(?Tab4)
                         END
                       END
                       BUTTON('&Close'),AT(389,222,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(292,224,22,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       GROUP,AT(208,226,84,10),USE(?Group_Filters)
                         PROMPT('Status:'),AT(208,226),USE(?Account_Status:Prompt)
                         LIST,AT(232,226,60,10),USE(L_OG:Account_Status),LEFT(1),DROP(15),FROM('Normal|#0|On Hol' & |
  'd|#1|Closed|#2|Dormant|#3|All|#-1'),MSG('Normal, On Hold, Closed, Dormant'),TIP('Status fil' & |
  'ter<0DH,0AH>Normal, On Hold, Closed, Dormant, All')
                       END
                       PROMPT('Locator:'),AT(8,20),USE(?LOC:Locator:Prompt),TRN
                       STRING(@s50),AT(36,20,208,10),USE(LOC:Locator),TRN
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort2:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort3:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort0:StepClass StepStringClass                      ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

View_Tag     VIEW(Clients)
    PROJECT(CLI:CID)
    .

Tag_View     ViewManager
Clients_With_Fuel_Surcharges        CLASS

Inited      BYTE
Active      BYTE

Get         PROCEDURE(ULONG p_CID),LONG
Init        PROCEDURE()
    .

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
  !------------------------------------
  !Style for ?Browse:1
  !------------------------------------
!---------------------------------------------------------------------------
Un_Tag_All                      ROUTINE
    IF LOC:Tagging > 0
       p_Client_Tags.ClearAllTags()
       BRW1.ResetFromBuffer()
       ?String_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()
    .
    EXIT
Tag_All                             ROUTINE
    IF LOC:Tagging > 0
       ! Tag All in the current sort order...
       !PUSHBIND()
       !BIND('')

       Tag_View.Init(View_Tag, Relate:Clients)

       CASE CHOICE(?CurrentTab)
       OF 2                                         ! Branch
          Tag_View.AddSortOrder(CLI:FKey_BID)
          Tag_View.AppendOrder('CLI:CID')
          Tag_View.AddRange(CLI:BID, BRA:BID)
       ELSE                                         ! All others
          Tag_View.AddSortOrder(CLI:PKey_CID)
       .

       !Tag_View.SetFilter('')

       Tag_View.Reset()
       LOOP
          IF Tag_View.Next() ~= LEVEL:Benign
             BREAK
          .

          p_Client_Tags.MakeTag(CLI:CID)
       .

       Tag_View.Kill()

       !POPBIND()
       ?String_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()

       BRW1.ResetFromBuffer()
    .
    EXIT
Tag_Toggle_this_rec       ROUTINE
    DATA
R:Choice            LONG

    CODE
    IF LOC:Tagging > 0
       R:Choice        = CHOICE(?Browse:1)

       IF p_Client_Tags.IsTagged(CLI:CID) = TRUE
          p_Client_Tags.ClearTag(CLI:CID)
       ELSE
          p_Client_Tags.MakeTag(CLI:CID)
       .

   !    BRW1.ResetFromBuffer()
   !    BRW1.Reset(1)
       GET(Queue:Browse:1, R:Choice)
       IF ~ERRORCODE()
          DO Style_Entry_Tag
          PUT(Queue:Browse:1)
       .
       ?String_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()
    .
    EXIT

Style_Setup_Tag              ROUTINE
    IF OMITTED(1) = FALSE
       LOC:Tagging  = TRUE                          ! Switch on tagging

!    IF ~OMITTED(1)
       ! Style:Normal
   !    ?list1{PROPSTYLE:FontStyle, 1}     = FONT:Regular
   !    ?list1{PROPSTYLE:TextColor, 1}     = COLOR:NONE
   !    ?list1{PROPSTYLE:BackColor, 1}     = COLOR:NONE
   !    ?list1{PROPSTYLE:TextSelected, 1}  = COLOR:NONE
   !    ?list1{PROPSTYLE:BackSelected, 1}  = COLOR:NONE

       ! Style:Header
       !?list1{PROPSTYLE:FontStyle, 1}     = FONT:Bold
       ?Browse:1{PROPSTYLE:TextColor, 1}     = COLOR:HIGHLIGHTTEXT
       ?Browse:1{PROPSTYLE:BackColor, 1}     = COLOR:HIGHLIGHT
       ?Browse:1{PROPSTYLE:TextSelected, 1}  = COLOR:White
       ?Browse:1{PROPSTYLE:BackSelected, 1}  = COLOR:Blue

!       SELF.Q.LOC:Day_NormalFG = -2147483634
!       SELF.Q.LOC:Day_NormalBG = -2147483635
!       SELF.Q.LOC:Day_SelectedFG = -1
!       SELF.Q.LOC:Day_SelectedBG = -1
    .


    ?Browse:1{PROPSTYLE:TextColor, 2}     = -1
    ?Browse:1{PROPSTYLE:BackColor, 2}     = 004080FFh
    ?Browse:1{PROPSTYLE:TextSelected, 2}  = -1
    ?Browse:1{PROPSTYLE:BackSelected, 2}  = 009FBEFFh

    ?Browse:1{PROPSTYLE:TextColor, 3}     = -1
    ?Browse:1{PROPSTYLE:BackColor, 3}     = 0012BA3Fh
    ?Browse:1{PROPSTYLE:TextSelected, 3}  = -1
    ?Browse:1{PROPSTYLE:BackSelected, 3}  = 00FF99CCh

    ?Browse:1{PROPSTYLE:TextColor, 4}     = -1
    ?Browse:1{PROPSTYLE:BackColor, 4}     = 00808000h
    ?Browse:1{PROPSTYLE:TextSelected, 4}  = -1
    ?Browse:1{PROPSTYLE:BackSelected, 4}  = 00FFFF80h
    EXIT


Style_Entry_Tag               ROUTINE
    IF LOC:Tagging > 0
       IF p_Client_Tags.IsTagged(Queue:Browse:1.CLI:CID) = TRUE
          Queue:Browse:1.CLI:ClientName_Style                    = 1
          Queue:Browse:1.CLI:ClientNo_Style                      = 1
       ELSE
          Queue:Browse:1.CLI:ClientName_Style                    = 0
          Queue:Browse:1.CLI:ClientNo_Style                      = 0

          DO Colour
       .
    ELSE
       DO Colour
    .
    EXIT


Colour          ROUTINE
    CASE CLI:Status
    OF 1
       Queue:Browse:1.CLI:ClientName_Style                    = 2
       Queue:Browse:1.CLI:ClientNo_Style                      = 2
    OF 2
       Queue:Browse:1.CLI:ClientName_Style                    = 3
       Queue:Browse:1.CLI:ClientNo_Style                      = 3
    OF 3
       Queue:Browse:1.CLI:ClientName_Style                    = 4
       Queue:Browse:1.CLI:ClientNo_Style                      = 4
    ELSE
       Queue:Browse:1.CLI:ClientName_Style                    = 0
       Queue:Browse:1.CLI:ClientNo_Style                      = 0
    .
    EXIT
Security_General               ROUTINE
    LOC:User_Access   = Get_User_Access(GLO:UID, 'Print Rates Button', 'Browse_Clients')
    IF LOC:User_Access > 0
       IF LOC:User_Access ~= 1       ! Disable / Allow View??
            HIDE(?Button_PrintRates)
            HIDE(?Button_Graph)
            HIDE(?L_OG:Show_Activity)
            L_OG:Show_Activity = 0
    .  .

    db.debugout('[Browse_Clients - Gen Sec]  GLO:UID: ' & GLO:UID & ',   LOC:User_AccessL: ' & LOC:User_Access)

    EXIT
Filters_Window                  ROUTINE
    DATA
L:Fuel_Surcharge_Clients    BYTE

FSC_Window WINDOW('Filters'),AT(,,183,109),FONT('MS Sans Serif',8,,FONT:regular),CENTER,GRAY
       CHECK(' Only show Clients with &Fuel Surcharges'),AT(16,22),USE(L:Fuel_Surcharge_Clients)
       BUTTON('&OK'),AT(146,90),USE(?OkButton),DEFAULT
     END

    CODE
    OPEN(FSC_Window)
    L:Fuel_Surcharge_Clients    = L_FG:Fuel_Surcharge_Clients
    DISPLAY

    ACCEPT
       CASE FIELD()
       OF L:Fuel_Surcharge_Clients
          CASE EVENT()
          OF EVENT:Accepted
             UPDATE()
          .
       OF ?OkButton
          CASE EVENT()
          OF EVENT:Accepted
             IF L:Fuel_Surcharge_Clients = TRUE
                L_FG:Fuel_Surcharge_Clients         = TRUE
                Clients_With_Fuel_Surcharges.Init()
                Clients_With_Fuel_Surcharges.Active = TRUE
             ELSE
                L_FG:Fuel_Surcharge_Clients         = FALSE
                Clients_With_Fuel_Surcharges.Active = FALSE
             .
             BREAK
    .  .  .
    CLOSE(FSC_Window)

    IF L_FG:Fuel_Surcharge_Clients = TRUE
       ?Button_Filters{PROP:Text}   = '&Filters (*)'
       ?Button_Filters{PROP:FontColor}  = COLOR:Red
    ELSE
       ?Button_Filters{PROP:Text}   = '&Filters'
       ?Button_Filters{PROP:FontColor}  = COLOR:None
    .

    ThisWindow.Reset()
    EXIT
Check_ClientName_Passed    ROUTINE
   IF ~OMITTED(3)
      CLI:ClientName = p_ClientName
      CLI:ClientSearch = p_ClientName
      LOC:Locator = p_ClientName
   .
   
   EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Clients')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:Account_Status',L_OG:Account_Status) ! Added by: BrowseBox(ABC)
  BIND('L_BG:Client_Status',L_BG:Client_Status)   ! Added by: BrowseBox(ABC)
  BIND('L_BG:DIs_Last_Period',L_BG:DIs_Last_Period) ! Added by: BrowseBox(ABC)
  BIND('L_BG:Weight_Last_Period',L_BG:Weight_Last_Period) ! Added by: BrowseBox(ABC)
  BIND('L_BG:Cost_Last_Period',L_BG:Cost_Last_Period) ! Added by: BrowseBox(ABC)
  BIND('L_BG:Cost_to_Weight_Last_Period',L_BG:Cost_to_Weight_Last_Period) ! Added by: BrowseBox(ABC)
  BIND('L_BG:DIs_To_Date',L_BG:DIs_To_Date)       ! Added by: BrowseBox(ABC)
  BIND('L_BG:Weight_To_Date',L_BG:Weight_To_Date) ! Added by: BrowseBox(ABC)
  BIND('L_BG:Cost_To_Date',L_BG:Cost_To_Date)     ! Added by: BrowseBox(ABC)
  BIND('L_BG:Cost_to_Weight_To_Date',L_BG:Cost_to_Weight_To_Date) ! Added by: BrowseBox(ABC)
  BIND('L_BG:InsuranceType',L_BG:InsuranceType)   ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                           ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                            ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Access:Branches.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Clients,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  ?Browse:1{Prop:LineHeight} = 10
    ?Browse:1{Prop:LineHeight} = 10
  Do DefineListboxStyle
         DO Style_Setup_Tag
  DO Check_ClientName_Passed
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,CLI:FKey_BID)                ! Add the sort order for CLI:FKey_BID for sort order 1
  BRW1.AddRange(CLI:BID,BRA:BID)                  ! Add single value range limit for sort order 1
  BRW1.AddLocator(BRW1::Sort2:Locator)            ! Browse has a locator for sort order 1
  BRW1::Sort2:Locator.Init(?LOC:Locator,CLI:BID,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: CLI:FKey_BID , CLI:BID
  BRW1.AppendOrder('+CLI:ClientName')             ! Append an additional sort order
  BRW1.AddSortOrder(,CLI:Key_ClientNo)            ! Add the sort order for CLI:Key_ClientNo for sort order 2
  BRW1.AddLocator(BRW1::Sort3:Locator)            ! Browse has a locator for sort order 2
  BRW1::Sort3:Locator.Init(?LOC:Locator,CLI:ClientNo,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: CLI:Key_ClientNo , CLI:ClientNo
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:CaseSensitive,ScrollBy:Runtime) ! Moveable thumb based upon CLI:ClientSearch for sort order 3
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,CLI:Key_ClientSearch) ! Add the sort order for CLI:Key_ClientSearch for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(?LOC:Locator,CLI:ClientSearch,,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: CLI:Key_ClientSearch , CLI:ClientSearch
  BRW1.SetFilter('(L_OG:Account_Status = -1 OR L_OG:Account_Status = CLI:Status)') ! Apply filter expression to browse
  BRW1.AddResetField(L_FG:Fuel_Surcharge_Clients) ! Apply the reset field
  BRW1.AddResetField(L_OG:Account_Status)         ! Apply the reset field
  BRW1.AddResetField(L_OG:Show_Activity)          ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW1.AddField(CLI:ClientName,BRW1.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientNo,BRW1.Q.CLI:ClientNo) ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW1.AddField(CLI:MinimiumCharge,BRW1.Q.CLI:MinimiumCharge) ! Field CLI:MinimiumCharge is a hot field or requires assignment from browse
  BRW1.AddField(CLI:DocumentCharge,BRW1.Q.CLI:DocumentCharge) ! Field CLI:DocumentCharge is a hot field or requires assignment from browse
  BRW1.AddField(CLI:AccountLimit,BRW1.Q.CLI:AccountLimit) ! Field CLI:AccountLimit is a hot field or requires assignment from browse
  BRW1.AddField(CLI:OpsManager,BRW1.Q.CLI:OpsManager) ! Field CLI:OpsManager is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Client_Status,BRW1.Q.L_BG:Client_Status) ! Field L_BG:Client_Status is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:DIs_Last_Period,BRW1.Q.L_BG:DIs_Last_Period) ! Field L_BG:DIs_Last_Period is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Weight_Last_Period,BRW1.Q.L_BG:Weight_Last_Period) ! Field L_BG:Weight_Last_Period is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Cost_Last_Period,BRW1.Q.L_BG:Cost_Last_Period) ! Field L_BG:Cost_Last_Period is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Cost_to_Weight_Last_Period,BRW1.Q.L_BG:Cost_to_Weight_Last_Period) ! Field L_BG:Cost_to_Weight_Last_Period is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:DIs_To_Date,BRW1.Q.L_BG:DIs_To_Date) ! Field L_BG:DIs_To_Date is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Weight_To_Date,BRW1.Q.L_BG:Weight_To_Date) ! Field L_BG:Weight_To_Date is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Cost_To_Date,BRW1.Q.L_BG:Cost_To_Date) ! Field L_BG:Cost_To_Date is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:Cost_to_Weight_To_Date,BRW1.Q.L_BG:Cost_to_Weight_To_Date) ! Field L_BG:Cost_to_Weight_To_Date is a hot field or requires assignment from browse
  BRW1.AddField(CLI:VATNo,BRW1.Q.CLI:VATNo)       ! Field CLI:VATNo is a hot field or requires assignment from browse
  BRW1.AddField(CLI:InsuranceRequired,BRW1.Q.CLI:InsuranceRequired) ! Field CLI:InsuranceRequired is a hot field or requires assignment from browse
  BRW1.AddField(L_BG:InsuranceType,BRW1.Q.L_BG:InsuranceType) ! Field L_BG:InsuranceType is a hot field or requires assignment from browse
  BRW1.AddField(CLI:InsurancePercent,BRW1.Q.CLI:InsurancePercent) ! Field CLI:InsurancePercent is a hot field or requires assignment from browse
  BRW1.AddField(CLI:FuelSurchargeActive,BRW1.Q.CLI:FuelSurchargeActive) ! Field CLI:FuelSurchargeActive is a hot field or requires assignment from browse
  BRW1.AddField(CLI:TollChargeActive,BRW1.Q.CLI:TollChargeActive) ! Field CLI:TollChargeActive is a hot field or requires assignment from browse
  BRW1.AddField(CLI:CID,BRW1.Q.CLI:CID)           ! Field CLI:CID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:Terms,BRW1.Q.CLI:Terms)       ! Field CLI:Terms is a hot field or requires assignment from browse
  BRW1.AddField(CLI:VolumetricRatio,BRW1.Q.CLI:VolumetricRatio) ! Field CLI:VolumetricRatio is a hot field or requires assignment from browse
  BRW1.AddField(CLI:Status,BRW1.Q.CLI:Status)     ! Field CLI:Status is a hot field or requires assignment from browse
  BRW1.AddField(CLI:AID,BRW1.Q.CLI:AID)           ! Field CLI:AID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:BID,BRW1.Q.CLI:BID)           ! Field CLI:BID is a hot field or requires assignment from browse
  BRW1.AddField(CLI:ClientSearch,BRW1.Q.CLI:ClientSearch) ! Field CLI:ClientSearch is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Browse_Clients',QuickWindow)      ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      BRA:BID     = GLO:BranchID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
      .
      L_OG:Account_Status = GETINI('Browse_Clients', 'Account_Status', -1, GLO:Local_INI)
  BRW1.AskProcedure = 1
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('MANTRNIS','Browse_Clients',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,35,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
      ! p_Option  code
      ! Note security section is also relevant to this being ok... because the buttons are affected there
  
      IF p_Option = 1         ! Rates button - disable view / update / etc
         ! Move the Rate button to the same position as these buttons
         ?Button_ShowRates{PROP:XPos} = ?Delete:4{PROP:XPos}
         ?Button_ShowRates{PROP:YPos} = ?Delete:4{PROP:YPos}
  
         ?View:3{PROP:XPos}           = ?Change:4{PROP:XPos}
         ?View:3{PROP:YPos}           = ?Change:4{PROP:YPos}
  
         HIDE(?Delete:4)
         HIDE(?Insert:4)
         HIDE(?Change:4)
  
         ?Change:4{PROP:Default}          = FALSE
         ?Button_ShowRates{PROP:Default}  = TRUE
      .
      DO Security_General
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
    Relate:_SQLTemp.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_Clients',QuickWindow)            ! Save window data to non-volatile store
  END
      IF LOC:Selected_Clients ~= TRUE AND LOC:Tagging = TRUE
         p_Client_Tags.ClearAllTags()
      .
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Clients
    ReturnValue = GlobalResponse
  END
    IF SELF.Request = SelectRecord
       IF Request = InsertRecord
          IF ReturnValue = RequestCompleted
             ! Then assume the user has added a Client they want to Select
             POST(EVENT:Accepted, ?Select:2)
    .  .  .
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_PrintRates
      BRW1.UpdateViewRecord()
          IF CLI:CID = 0
             MESSAGE('Please select a Client.', 'Print Clients Rates', ICON:Exclamation)
             CYCLE
          .
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Select:2
      ThisWindow.Update()
          DO Un_Tag_All
    OF ?Select_Tagged
      ThisWindow.Update()
          LOC:Selected_Clients  = TRUE
      
          POST(EVENT:CloseWindow)
    OF ?Button_PrintRates
      ThisWindow.Update()
      Print_Rates(CLI:CID)
      ThisWindow.Reset
    OF ?Button_ShowRates
      ThisWindow.Update()
      GlobalRequest = ViewRecord
      Update_Clients(1)
      ThisWindow.Reset
    OF ?Button_Graph
      ThisWindow.Update()
          ThisWindow.Update
          EXECUTE POPUP('All Clients General|Clients and Journeys')
             Window_Client_Activity()
             Window_Client_ActivityJ()
          .
      
          ThisWindow.Reset
      
      
      !       START(Window_Client_Activity, 25000)
    OF ?L_OG:Show_Activity
          IF L_OG:Show_Activity = TRUE
             ! (p:From, p:To, p:Option, p:Heading)
             LOC:Period_Dates     = Ask_Date_Range(TODAY() - 7,, 1, 'Select Current Period From Date')
          .
    OF ?Button_Filters
      ThisWindow.Update()
          DO Filters_Window
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Branches()
      ThisWindow.Reset
    OF ?Close
      ThisWindow.Update()
          DO Un_Tag_All
    OF ?L_OG:Account_Status
          PUTINI('Browse_Clients', 'Account_Status', L_OG:Account_Status, GLO:Local_INI)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
        CASE EVENT()
        OF EVENT:Accepted
           !EVENT:MouseDown
           BRW1.UpdateViewRecord()
    
           IF KeyCode() = ShiftMouseLeft
              ! Tag all records between this one and last tagged record
              IF CHOICE(?CurrentTab) = 1
                 !DO Tag_From_To
              .
           ELSIF KeyCode() = CtrlMouseLeft
              DO Tag_Toggle_this_rec
           ELSIF KeyCode() ~= MouseRight AND KeyCode() ~= CtrlMouseRight        !AND LOC:Last_Tagged_Rec_Id ~= 0
    !          InBox_Tags.ClearAllTags()                ! Clear all tags when no shift left mouse
    !          LOC:Last_Tagged_Rec_Id        = 0
    
    !          BRW1.ResetFromBuffer()
           .
    
    !       LOC:Last_Clicked_Rec_ID      = REC:Rec_ID
    !       LOC:Last_Tagged_Date         = REC:TA_Date
    !       LOC:Last_Tagged_Time         = REC:TA_Time
        .
    CASE EVENT()
    OF EVENT:AlertKey
          CASE Keycode()
          OF CtrlA
             DO Tag_All
          OF CtrlU
             DO Un_Tag_All
          OF MouseLeft2
             IF p_Option = 1              ! Rates mode - otherwise double click will be view/change
      !          POST(EVENT:Accepted, ?Button_ShowRates)
          .  .
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      BRW1.UpdateViewRecord()
          CLEAR(LOC:Client_Contacts)
          ADD:AID       = CLI:AID
          IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
             ! (p:Add, p:List, p:Delim)
             ! Check for a Primary contact...
             CLEAR(ADDC:Record,-1)
             ADDC:AID   = CLI:AID
             SET(ADDC:FKey_AID, ADDC:FKey_AID)
             LOOP
                IF Access:AddressContacts.TryNext() ~= LEVEL:Benign
                   BREAK
                .
                IF ADDC:AID ~= CLI:AID
                   BREAK
                .
      
                IF ADDC:PrimaryContact = TRUE
                   Add_to_List(CLIP(ADDC:ContactName), LOC:Client_Contacts, ',',, '')
                   BREAK
             .  .
      
             Add_to_List(CLIP(ADD:PhoneNo), LOC:Client_Contacts, ',',, ' P: ')
             Add_to_List(CLIP(ADD:PhoneNo2), LOC:Client_Contacts, ',',, '  P2: ')
             Add_to_List(CLIP(ADD:Fax), LOC:Client_Contacts, ',',, '  F: ')
          .
      
          DISPLAY(?LOC:Client_Contacts)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF LOC:User_Access > 0
             DISABLE(?Insert:4)
             DISABLE(?Change:4)
             DISABLE(?Delete:4)
      
             IF LOC:User_Access ~= 1       ! Disable / Allow View??
                DISABLE(?View:3)
          .  .
          ?String_Tagged{PROP:Text}   = ''
          IF LOC:Tagging ~= TRUE
             HIDE(?Select_Tagged)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Clients_With_Fuel_Surcharges.Init        PROCEDURE()
    CODE
    FREE(LOC:Clients_FuelSur_Q)

    SET(_SQLTemp)
    IF ERRORCODE()
       MESSAGE('SQL Error: ' & FILEERROR() & '||Error: ' & ERROR())
    .
    _SQLTemp{PROP:SQL}      = 'SELECT c.CID FROM Clients AS c INNER JOIN __RatesFuelSurcharge AS r ON c.CID = r.CID'
    IF ERRORCODE()
       MESSAGE('SQL Error: ' & FILEERROR() & '||Error: ' & ERROR())
    .
    LOOP
       NEXT(_SQLTemp)
       IF ERRORCODE()
          BREAK
       .

       L_CFS_Q:CID  = _SQLTemp.S1
       GET(LOC:Clients_FuelSur_Q, L_CFS_Q:CID)
       IF ERRORCODE()
          L_CFS_Q:CID  = _SQLTemp.S1
          ADD(LOC:Clients_FuelSur_Q)
    .  .

    SELF.Inited     = TRUE
    RETURN
Clients_With_Fuel_Surcharges.Get        PROCEDURE(ULONG p_CID)  !,LONG
L:Found     LONG
    CODE

    IF SELF.Active = TRUE
       L_CFS_Q:CID  = p_CID
       GET(LOC:Clients_FuelSur_Q, L_CFS_Q:CID)
       IF ERRORCODE()
          !
       ELSE
          L:Found   = TRUE
       .
    ELSE
       L:Found      = TRUE
    .

    RETURN(L:Found)

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode
      LOC:User_Access   = Get_User_Access(GLO:UID, 'Update Clients', 'Browse_CLients')
      IF LOC:User_Access > 0
         SELF.InsertControl   = 0
         SELF.ChangeControl   = 0
         SELF.DeleteControl   = 0
  
         IF LOC:User_Access ~= 1       ! Disable / Allow View??
            SELF.ViewControl  = 0
  
            HIDE(?Button_Graph)
            HIDE(?L_OG:Show_Activity)
      .  .
  
  
      db.debugout('[Browse_Clients]  GLO:UID: ' & GLO:UID & ',   LOC:User_AccessL: ' & LOC:User_Access)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      EXECUTE CLI:InsuranceType
         L_BG:InsuranceType    = 'By Value'
      ELSE
         L_BG:InsuranceType    = 'By Weight'
      .
  
  
      ! CLI:VolumetricRatio = 1 cube equivalent to this weight
      ! e.g. 1 ton = 2 cubic metres
      L_BG:Ton_Volume          = (1000 / CLI:VolumetricRatio)
  
  
      ! Normal|On Hold|Closed|Dormant
      EXECUTE CLI:Status + 1
         L_BG:Client_Status   = 'Normal'
         L_BG:Client_Status   = 'On Hold'
         L_BG:Client_Status   = 'Closed'
         L_BG:Client_Status   = 'Dormant'
      ELSE
         L_BG:Client_Status   = '<unknown: ' & CLI:Status & '>'
      .
  
  
      IF L_OG:Show_Activity = TRUE
         ! (p:ID, p:Option, p:DateFrom, p:DateTo)
         ! (ULONG, BYTE, LONG=0, LONG=0)                 , STRING
         ! p:Option
         !   0. Weight of all CID items - Volumetric considered
         !   1. Total Items for CID
         !   2. Weight of all CID items - real weight
  
         L_OG:SQL                 = 'SELECT COUNT(*), SUM(DocumentCharge) + SUM(FuelSurcharge) + SUM(Charge) + SUM(AdditionalCharge) FROM Deliveries WHERE CID = ' & CLI:CID & ' AND DIDateAndTime >= <39>' & FORMAT(LOD:From_Date,@d8) & '<39>'
  
         Get_Info_SQL(4, CLI:CID, L_OG:No_DIs_Str, L_OG:SQL, 2)
         L_BG:DIs_Last_Period     = Get_1st_Element_From_Delim_Str(L_OG:No_DIs_Str, ',', TRUE)
         L_BG:Cost_Last_Period    = Get_1st_Element_From_Delim_Str(L_OG:No_DIs_Str, ',', TRUE)
  
         L_OG:SQL                 = 'SELECT COUNT(*), SUM(DocumentCharge) + SUM(FuelSurcharge) + SUM(Charge) + SUM(AdditionalCharge) FROM Deliveries WHERE CID = ' & CLI:CID
  
         Get_Info_SQL(4, CLI:CID, L_OG:No_DIs_Str, L_OG:SQL, 2)
         L_BG:DIs_To_Date         = Get_1st_Element_From_Delim_Str(L_OG:No_DIs_Str, ',', TRUE)
         L_BG:Cost_To_Date        = Get_1st_Element_From_Delim_Str(L_OG:No_DIs_Str, ',', TRUE)
  
         L_BG:Weight_Last_Period  = Get_DelItem_Client_Info(CLI:CID, 2, LOD:From_Date) / 1000     ! Tons
         L_BG:Weight_To_Date      = Get_DelItem_Client_Info(CLI:CID, 2)                / 1000     ! Tons
  
         L_BG:Cost_to_Weight_Last_Period  = L_BG:Cost_Last_Period / L_BG:Weight_Last_Period
         L_BG:Cost_to_Weight_To_Date      = L_BG:Cost_To_Date     / L_BG:Weight_To_Date
      .
  PARENT.SetQueueRecord
  
  SELF.Q.CLI:ClientName_Style = 0 ! 
  SELF.Q.CLI:ClientNo_Style = 0 ! 
  SELF.Q.L_BG:DIs_Last_Period_Style = 0 ! 
  SELF.Q.L_BG:Weight_Last_Period_Style = 0 ! 
  SELF.Q.L_BG:Cost_Last_Period_Style = 0 ! 
  SELF.Q.L_BG:Cost_to_Weight_Last_Period_Style = 0 ! 
  SELF.Q.L_BG:DIs_To_Date_Style = 0 ! 
  SELF.Q.L_BG:Weight_To_Date_Style = 0 ! 
  SELF.Q.L_BG:Cost_To_Date_Style = 0 ! 
  SELF.Q.L_BG:Cost_to_Weight_To_Date_Style = 0 ! 
  IF (CLI:InsuranceRequired = 1)
    SELF.Q.CLI:InsuranceRequired_Icon = 2                  ! Set icon from icon list
  ELSE
    SELF.Q.CLI:InsuranceRequired_Icon = 1                  ! Set icon from icon list
  END
  IF (CLI:FuelSurchargeActive = 1)
    SELF.Q.CLI:FuelSurchargeActive_Icon = 2                ! Set icon from icon list
  ELSE
    SELF.Q.CLI:FuelSurchargeActive_Icon = 1                ! Set icon from icon list
  END
  IF (CLI:TollChargeActive = 1)
    SELF.Q.CLI:TollChargeActive_Icon = 2                   ! Set icon from icon list
  ELSE
    SELF.Q.CLI:TollChargeActive_Icon = 1                   ! Set icon from icon list
  END
      DO Style_Entry_Tag
  


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>2,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>2,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF Clients_With_Fuel_Surcharges.Get(CLI:CID) ~= TRUE
         ReturnValue    = Record:FILTERED
      .
  
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Contact, Resize:FixRight+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Contact
  SELF.SetStrategy(?String_Tagged, Resize:FixRight+Resize:FixTop, Resize:LockSize) ! Override strategy for ?String_Tagged
  SELF.SetStrategy(?Group_Filters, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Filters

