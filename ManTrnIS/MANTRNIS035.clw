

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetEmail.inc'),ONCE

                     MAP
                       INCLUDE('MANTRNIS035.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! came from Manifest_Emails
!!! </summary>
Delivery_Tracking_Emails_Setup_old_before_tabs PROCEDURE 

LOC:Locals_Group     GROUP,PRE(L_LG)                       !
EmailTypeNotes       STRING(1024)                          !User notes
LastControl          LONG                                  !
                     END                                   !
DisplayString        STRING(255)                           !
LOC_MLID             ULONG                                 !
CurrentTab           LONG                                  !
LOC_Email_Group      GROUP,PRE(LO_EG)                      !
Email_Address        STRING(255)                           !
Content              STRING(5000)                          !
Substitution         STRING(30)                            !
Email_Type           STRING('''n Route'' {26}')            !
Prev_Email_Type      STRING(35)                            !
                     END                                   !
LOC_Loaded_Email_Group GROUP,PRE(L_EG)                     !
Subject              STRING(255)                           !
Content              STRING(5000)                          !
                     END                                   !
LOC_EmailSend_Group  GROUP,PRE(L_ESG)                      !
From                 STRING(255)                           !
Subject              STRING(255)                           !
CC                   STRING(255)                           !
BCC                  STRING(255)                           !
Server               STRING(255)                           !
Message              STRING(5000)                          !
HTML                 STRING(5000)                          !
SMTP_Username        STRING(255)                           !
SMTP_Password        STRING(255)                           !
                     END                                   !
QuickWindow          WINDOW('Delivery Tracking Emails'),AT(,,523,349),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,IMM,MDI,HLP('Manifest_Emails')
                       BUTTON('&Close'),AT(469,6,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       STRING('String2'),AT(5,6,377,14),USE(?STRING2),FONT('Microsoft Sans Serif',10,,FONT:bold)
                       SHEET,AT(5,24,513,323),USE(?SHEET1)
                         TAB('Content'),USE(?TAB_Content)
                           PROMPT('Email Type:'),AT(13,48),USE(?LO_EG:Email_Type:Prompt)
                           LIST,AT(61,48,93,10),USE(LO_EG:Email_Type,,?LO_EG:Email_Type:2),DROP(5),FROM('On Route|#' & |
  'On Route|Transferred|#Transferred|Out On Delivery|#Out On Delivery|Delivered|#Delive' & |
  'red|Manifest General|#Manifest General')
                           PROMPT('Subject (you can edit this):'),AT(17,85),USE(?L_ESG:Subject:Prompt)
                           ENTRY(@s255),AT(18,99,488,10),USE(L_ESG:Subject)
                           PROMPT('The content of the email (you can edit this):'),AT(17,114),USE(?LO_EG:Content:Prompt)
                           TEXT,AT(18,128,488,192),USE(LO_EG:Content),VSCROLL
                           BUTTON('Save Content'),AT(13,329,81,14),USE(?BUTTON_SaveContent),TIP('Save your email setup')
                           BUTTON('Load Saved Content'),AT(99,329),USE(?BUTTON_LoadContent),TIP('Load the last sav' & |
  'ed version of your email (changes in current version above will be lost)')
                           BUTTON('Copy to Clip.'),AT(184,329,81,14),USE(?BUTTON_CopyClip),TIP('Copy content to cl' & |
  'ipboard (can also use CTRL-C, CTRL-V')
                           PROMPT('Substitution:'),AT(323,332),USE(?LO_EG:Substitution:Prompt:2)
                           LIST,AT(366,332,108,10),USE(LO_EG:Substitution,,?LO_EG:Substitution:2),DROP(15,120),FORMAT('20L(2)'), |
  FROM('Client Name|#Client Name|Collection Add|#Collection Add|Delivery Add|#Delivery ' & |
  'Add|Client Ref|#Client Ref|Commodity|#Commodity|Items|#Items|Weight|#Weight|Special ' & |
  'Remarks|#Special Remarks|ETA|#ETA|Branch|#Branch|Dispatch Vehicle|#Dispatch Vehicle|' & |
  'Delivery Date/Time|#Delivery Date/Time|Branch Tel.|#Branch Tel.|DI No.|#DI No.|Packa' & |
  'ging|#Packaging')
                           BUTTON('&Add'),AT(479,329),USE(?BUTTON_Add),FONT('Microsoft Sans Serif',,,FONT:regular)
                           TEXT,AT(170,43,340,31),USE(L_LG:EmailTypeNotes),FLAT,MSG('User notes'),READONLY,SKIP
                           SHEET,AT(11,64,503,263),USE(?SHEET_ManType)
                             TAB('Overnight'),USE(?TAB_Overnight)
                             END
                             TAB('Broking'),USE(?TAB_Broking)
                             END
                             TAB('Local'),USE(?TAB_Local)
                             END
                           END
                         END
                         TAB('Email Server && Account'),USE(?TAB_Summary)
                           PROMPT('Email ready to send.  It will be sent to X addresses.'),AT(133,2,373,18),USE(?PROMPT_Summary), |
  FONT('Microsoft Sans Serif',,,FONT:regular),HIDE
                           BUTTON('&Save'),AT(437,288,69,25),USE(?Ok:2),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation'),TRN
                           ENTRY(@s255),AT(210,128,183,10),USE(L_ESG:From)
                           PROMPT('From:'),AT(149,127,18),USE(?L_ESG:From:Prompt)
                           ENTRY(@s255),AT(210,152,183,10),USE(L_ESG:CC)
                           PROMPT('CC:'),AT(149,151,12),USE(?L_ESG:CC:Prompt)
                           ENTRY(@s255),AT(210,170,183,10),USE(L_ESG:BCC)
                           PROMPT('BCC:'),AT(149,169,17),USE(?L_ESG:BCC:Prompt)
                           ENTRY(@s255),AT(210,201,183,10),USE(L_ESG:Server)
                           PROMPT('Server:'),AT(149,200,23),USE(?L_ESG:Server:Prompt)
                           ENTRY(@s255),AT(210,222,183,10),USE(L_ESG:SMTP_Username)
                           PROMPT('SMTP Username:'),AT(149,222,57),USE(?L_ESG:SMTP_Username:Prompt)
                           ENTRY(@s255),AT(210,239,183,10),USE(L_ESG:SMTP_Password),PASSWORD
                           PROMPT('SMTP Password:'),AT(149,239,55),USE(?L_ESG:SMTP_Password:Prompt)
                           PANEL,AT(131,78,280,188),USE(?PANEL2),BEVEL(1,-1)
                           PROMPT('Email Server and other details'),AT(149,89,245,26),USE(?PROMPT2),FONT('Microsoft ' & |
  'Sans Serif',,,FONT:bold)
                           BUTTON('&Sent Test Email'),AT(323,288,95,25),USE(?SendTestEmail),LEFT,ICON(ICON:Connect),FLAT, |
  MSG('Accept operation'),TIP('Accept Operation'),TRN
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Local Data Classes
ThisNetEmail         CLASS(NetEmailSend)                   ! Generated by NetTalk Extension (Class Definition)

                     END

                    MAP
Load_Content_File     PROCEDURE(STRING),STRING
Save_Content_File     PROCEDURE(STRING)

Merge_Content         PROCEDURE(ULONG p:DID, STRING p:Content),STRING
                    .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

Save_Stuff                       ROUTINE
  DATA
r:name_   STRING(6)

  CODE

  ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip)
  ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>),STRING
  !   1       2       3           4           5

  ! ThisEmail.Server         = Get_Section_from_Settings(p_EmailAccount, 'Server', 'BC Domain Register SMTP Server')
  r:name_ = 'Email-'

  stuff_"         = Get_Setting(r:name_ & 'Server', 2, L_ESG:Server,, 'FBN Mailserver')

  stuff_" = Get_Setting(r:name_ & 'Account', 2, L_ESG:SMTP_Username, 'Email Account')
  stuff_" = Get_Setting(r:name_ & 'Password', 2, L_ESG:SMTP_Password, 'Email Account Password')
  
  stuff_"           = Get_Setting(r:name_ & 'From', 2, L_ESG:From, 'From Email address')
 !       ThisEmail.ToList         = Get_Setting(r:name_ & 'To', 'BC Domain Register Co ZA address')
  stuff_"         = Get_Setting(r:name_ & 'CC', 2, L_ESG:CC, 'CC Email address')
  stuff_" = Get_Setting(r:name_ & 'BCC', 2, L_ESG:BCC, 'BCC Email address')

  EXIT

  
Load_Stuff                    ROUTINE
  DATA
r:name_   STRING(6)

  CODE

  ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip)
  ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>),STRING
  !   1       2       3           4           5

  ! ThisEmail.Server         = Get_Section_from_Settings(p_EmailAccount, 'Server', 'BC Domain Register SMTP Server')
  r:name_ = 'Email-'


  L_ESG:Server        = Get_Setting(r:name_ & 'Server', 1, 'mail.fbn-transport.co.za',, 'FBN Mailserver')

  L_ESG:SMTP_Username = Get_Setting(r:name_ & 'Account', 1, 'operations@fbn-transport.co.za', 'Email Account')
  L_ESG:SMTP_Password = Get_Setting(r:name_ & 'Password', 1, '', 'Email Account Password')
  
  L_ESG:From          = Get_Setting(r:name_ & 'From', 1, 'operations@fbn-transport.co.za', 'From Email address')
 !       ThisEmail.ToList         = Get_Setting(r:name_ & 'To', 'BC Domain Register Co ZA address')
  L_ESG:CC            = Get_Setting(r:name_ & 'CC', 1,'', 'CC Email address')
  L_ESG:BCC           = Get_Setting(r:name_ & 'BCC', 1,'', 'BCC Email address')
  EXIT
!--------------------------------------------
Content_Add_Subs     ROUTINE             ! add place holder string to content
   CASE L_LG:LastControl
   OF ?LO_EG:Content
!      message('content')
      in# = ?LO_EG:Content{PROP:Selected}   ! position to insert at
      
      LO_EG:Content   = SUB(LO_EG:Content,1,in#) & |
                          '[' & CLIP(LO_EG:Substitution) & ']'  & |
                          SUB(LO_EG:Content, in#, LEN(CLIP(LO_EG:Content)))
      
      DISPLAY(?LO_EG:Content)
      
      !?LO_EG:Content{PROP:SelStart} = in#  
      !SELECT(?LO_EG:Content, in#, in# + LEN(CLIP(LO_EG:Substitution)) + 2)
      SELECT(?LO_EG:Content, in# + LEN(CLIP(LO_EG:Substitution)) + 3)
   OF ?L_ESG:Subject
!      message('subject')
      in# = ?L_ESG:Subject{PROP:Selected}   ! position to insert at
      
      L_ESG:Subject   = SUB(L_ESG:Subject,1,in#) & |
                          '[' & CLIP(LO_EG:Substitution) & ']'  & |
                          SUB(L_ESG:Subject, in#, LEN(CLIP(L_ESG:Subject)))
      
      DISPLAY(?L_ESG:Subject)
      
      !?L_ESG:Subject{PROP:SelStart} = in#  
      !SELECT(?L_ESG:Subject, in#, in# + LEN(CLIP(LO_EG:Substitution)) + 2)
      SELECT(?L_ESG:Subject, in# + LEN(CLIP(LO_EG:Substitution)) + 3)
   .
   
   EXIT
   
   
  
!--------------------------------------------
Send_Test_Email                ROUTINE
  DATA
r:send_fails  LONG
r:content     LIKE(LO_EG:Content)

  CODE
  ! Save all the stuff you want to save here
  
  CASE MESSAGE('The test email will be sent to the From address "' & CLIP(L_ESG:From) & '"',,ICON:Question,BUTTON:OK+BUTTON:CANCEL)
  OF BUTTON:OK
     r:content = LO_EG:Content

     ! (p_Subject, p_Text, p_EmailAccount, p_HTML, p_Server, p_AuthUser, p_AuthPassword, p_From, p_To, p_CC, p_BCC, 
     ! p_Helo, p_Attach)
     IF ( Send_Email(L_ESG:Subject, r:content, 0,, L_ESG:Server, L_ESG:SMTP_Username, L_ESG:SMTP_Password |
                     , L_ESG:From, CLIP(L_ESG:From), L_ESG:CC, L_ESG:BCC) ) < 0
        r:send_fails += 1
     . 
     
     IF r:send_fails > 0
       MESSAGE('Emails failed to send.|||You can review the options and try again','Email Sending',ICON:Exclamation)
     ELSE
       MESSAGE('Email sending complete.')       
  .  .
    
  EXIT
!--------------------------------------------
Email_Type_NewSelection    ROUTINE
DATA
R_Merge_Str    CSTRING(500)

CODE
   ! Check if content has changed, if so then offer to save it
   IF CLIP(LO_EG:Content) ~= CLIP(Load_Content_File(LO_EG:Prev_Email_Type)) OR |
         CLIP(L_ESG:Subject) ~= CLIP(L_EG:Subject)
     
     CASE MESSAGE('Email subject/content has changed, would you like to save it?','Content',ICON:Question,BUTTON:Yes+BUTTON:NO)
     OF BUTTON:YES
       Save_Content_File(LO_EG:Prev_Email_Type)
     .
   .    

   LO_EG:Content           = Load_Content_File(LO_EG:Email_Type)
   L_ESG:Subject           = L_EG:Subject
   LO_EG:Prev_Email_Type   = LO_EG:Email_Type
   
   R_Merge_Str = 'This Content will be merged with the data and emailed to the listed email addresses on the DIs.' & | 
      '<13,10>You can use the Substitution options (see bottom right) to populate merge fields (for Subject & Body).'

   L_LG:EmailTypeNotes     = ''
   CASE LO_EG:Email_Type
   OF 'On Route'
      L_LG:EmailTypeNotes  = 'Content to use when the Manifest state is changed to On-Route from Loading.' |
         & ' ' & R_Merge_Str
   OF 'Transferred'
      L_LG:EmailTypeNotes  = 'Content to use when the Manifest state is changed to Transferred.' |
         & ' ' & R_Merge_Str
   OF 'Out On Delivery'
      L_LG:EmailTypeNotes  = 'Content to use when the Tripsheet state is changed to On Route.' |
         & ' ' & R_Merge_Str
   OF 'Delivered'
      L_LG:EmailTypeNotes  = 'Content to use when the Tripsheet state is changed to Transferred.' |
         & ' ' & R_Merge_Str
   OF 'Manifest General'
      L_LG:EmailTypeNotes  = 'Content to use as default when user right-clicks on Manifest Items list.' |
         & ' This Content will be merged with the data and emailed.  Email addresses are chosen from Client records' |
         &' at the time you do the right click.  You can also add addresses then and you can edit the email content then too.'
   .
   
   DISPLAY     
   EXIT
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Delivery_Tracking_Emails_Setup_old_before_tabs')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Cancel
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:NotificationsSetup.Open                           ! File NotificationsSetup used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
    ?STRING2{PROP:Text} = 'Delivery Tracking Email Setup'
                                               ! Generated by NetTalk Extension (Start)
  ThisNetEmail.SuppressErrorMsg = 1         ! No Object Generated Error Messages ! Generated by NetTalk Extension
  ThisNetEmail.init()
  if ThisNetEmail.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  Do DefineListboxStyle
  !ProcedureTemplate = Window
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  LO_EG:Email_Type     = 'On Route'
  DO Email_Type_NewSelection             
  DO Load_Stuff
  SELF.SetAlerts()
  ALERT(EscKey)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ThisNetEmail.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:NotificationsSetup.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
        IF CLIP(LO_EG:Content) ~= CLIP(Load_Content_File(LO_EG:Prev_Email_Type)) OR |
              CLIP(L_ESG:Subject) ~= CLIP(L_EG:Subject)
          
          CASE MESSAGE('Email subject/content has changed, would you like to save it?','Content',ICON:Question,BUTTON:Yes+BUTTON:NO)
          OF BUTTON:YES
            Save_Content_File(LO_EG:Prev_Email_Type)
          .
        .    
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?BUTTON_SaveContent
      ThisWindow.Update()
      Save_Content_File(LO_EG:Email_Type)
      
      !PUTINI('Manifest_Emails','EmailContent', LO_EG:Content, GLO:Local_INI)
    OF ?BUTTON_LoadContent
      ThisWindow.Update()
      CASE MESSAGE('Any changes to the above will be lost.||Are you sure?','Content',ICON:Question,BUTTON:YES+BUTTON:NO,BUTTON:NO)
      OF BUTTON:YES
      !  LO_EG:Content = GETINI('Manifest_Emails','EmailContent','', GLO:Local_INI)
        LO_EG:Content = Load_Content_File(LO_EG:Email_Type)
        DISPLAY
      .
    OF ?BUTTON_CopyClip
      ThisWindow.Update()
        SETCLIPBOARD(LO_EG:Content)
    OF ?BUTTON_Add
      ThisWindow.Update()
        DO Content_Add_Subs
    OF ?Ok:2
      ThisWindow.Update()
      DO Save_Stuff              ! save stuff 
      
      !  Save_Content_File(LO_EG:Email_Type)
      
      !  DO Send_Email
    OF ?SendTestEmail
      ThisWindow.Update()
        DO Send_Test_Email
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisNetEmail.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LO_EG:Email_Type:2
      DO Email_Type_NewSelection
        
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all Selected events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?L_ESG:Subject
      L_LG:LastControl  = ?L_ESG:Subject
    OF ?LO_EG:Content
      L_LG:LastControl  = ?LO_EG:Content
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      IF KEYCODE() = EscKey
         !MESSAGE('esc')         ! just ignore... dont exit
         
      .
      
         
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Load_Content_File   PROCEDURE(STRING p_Type)

mf FILE,DRIVER('ASCII','/FILEBUFFERS=8'), NAME('.\EmailContent.txt'),| 
         PRE(mf_),CREATE,THREAD
Record     RECORD,PRE()
Line        STRING(1024)
          END
    END

i_        LONG
l_txt     LIKE(LO_EG:Content)

  CODE
    ! Load subject
    L_EG:Subject  = Get_Setting('Email-' & CLIP(p_Type) & '-Subject', 1,'', 'Email subject')
    

    mf{PROP:Name} = CLIP(p_Type) & '.txt'
      
    OPEN(mf)
    IF ERRORCODE()
      ! no file yet
!      MESSAGE('No content file found?   ' & ERROR())
    ELSE
      CLEAR(l_txt)
      i_ = 0
      
      SET(mf)
      LOOP
        NEXT(mf)
        IF ERRORCODE()
!          MESSAGE('end of file?:  ' & ERROR() & '|||file: ' & mf{PROP:Name})
          BREAK
        .
        i_ += 1
        
!        MESSAGE('next: ' & ERROR() & '||line ' & i_ & ': ' & mf_:Line)

        IF i_ <= 1
          l_txt   = mf_:line 
        ELSE
          l_txt   = CLIP(l_txt) & '<13,10>' & CLIP(mf_:line)
        .
      .
      
      CLOSE(mf)    
    .
    
    L_EG:Content  = l_txt
  
  RETURN l_txt
  
Save_Content_File   PROCEDURE(STRING p_Type)

mf FILE,DRIVER('ASCII','/FILEBUFFERS=8'), NAME('.\EmailContent.txt'),| 
         PRE(mf_),CREATE,THREAD
Record     RECORD,PRE()
Line        STRING(1024)
          END
         END
i_                    LONG


  CODE
    stuff_" = Get_Setting('Email-' & CLIP(p_Type) & '-Subject', 2, L_ESG:Subject, 'Email subject')


    COPY(CLIP(p_Type) & '.txt', CLIP(p_Type) & FORMAT(TODAY(),@d6_)  & FORMAT(CLOCK(),@t1_) & '.txt')   ! a backup

    mf{PROP:Name} = CLIP(p_Type) & '.txt'
    
    CREATE(mf)
    OPEN(mf)
    IF ERRORCODE()
      MESSAGE('On Save - Open file error: ' & Error())
    ELSE
      mf_:Line  = CLIP(LO_EG:Content)
      
      ADD(mf)
      IF ERRORCODE()
        MESSAGE('On Save - Add error: ' & Error())
      .
      
      CLOSE(mf)
    .
    DISPLAY
  RETURN
  
!--------------------------------------------
Merge_Content       PROCEDURE(ULONG p:DID, STRING p:Content)
L:Content             LIKE(LO_EG:Content)

L:Count               LONG(0)
L:Commodities         STRING(255)
L:Units               LONG
L:Weight              DECIMAL(12)
                      
Del_View              VIEW(Deliveries)
                        PROJECT(DEL:DID,DEL:DINo,DEL:ClientReference,DEL:CollectionAID,DEL:DeliveryAID,DEL:Notes,DEL:ReceivedDate,DEL:ReceivedTime)
                        JOIN(DELI:FKey_DID_ItemNo,DEL:DID)
                          PROJECT(DELI:Units,DELI:Weight,DELI:CMID)
                          JOIN(COM:PKey_CMID, DELI:CMID)
                            PROJECT(COM:Commodity)
                        . .
                        JOIN(CLI:PKey_CID, DEL:CID)
                          PROJECT(CLI:ClientName)
                        .
                        JOIN(FLO:PKey_FID, DEL:FID)
                          PROJECT(FLO:Floor)
                      . .

View_Del              ViewManager

L:TripSheets          STRING(150)
L:TS_Veh              STRING(100)
L:BranchTel           STRING(100)
L:Del_Date            STRING(100)

  CODE
  L:Content = p:Content
    
  View_Del.Init(Del_View, Relate:Deliveries)
  View_Del.AddSortOrder(DEL:PKey_DID)
  View_Del.AppendOrder('DELI:ItemNo')
  View_Del.AddRange(DEL:DID, p:DID)
  !View_Del.SetFilter()
 
  View_Del.Reset()    
  LOOP
    IF View_Del.Next() ~= LEVEL:Benign        ! Failed to fetch the DI.. 
      BREAK      
    ELSE    
      L:Count += 1
      
      IF L:Count = 1                          ! some fields we only have one of
        ! Client Name|Collection Add|Delivery Add|Client Ref|Commodity|Items|Weight|Special Remarks|ETA|Branch|Dispatch Vehicle|Delivery Date/Time
        ! (p:String, p:Find, p:Replace, p:Occurances, p:No_Case)
        ! (*STRING, STRING, STRING, LONG=0, BYTE=0), LONG
        Replace_Strings(L:Content, '[Client Name]', CLIP(CLI:ClientName))
        Replace_Strings(L:Content, '[Collection Add]', CLIP(Get_Address(DEL:CollectionAID,,1)))
        Replace_Strings(L:Content, '[Delivery Add]', CLIP(Get_Address(DEL:DeliveryAID,,1)))
        Replace_Strings(L:Content, '[Client Ref]', CLIP(DEL:ClientReference))
        
        ! Check for a TripSheet delivery
        TRDI:DIID = DELI:DIID        
        IF Access:TripSheetDeliveries.TryFetch(TRDI:FKey_DIID) = Level:Benign
           ! We have at least one, use this one for now!     
           L:Del_Date  = FORMAT(TRDI:DeliveredDate,@d6) & ' ' & FORMAT(TRDI:DeliveredTime, @t1)
      . .
      
      Add_to_List(CLIP(COM:Commodity), L:Commodities, ', ')
      L:Units   += DELI:Units      
      L:Weight  += DELI:Weight        ! do they want volumetric?
    .
  .
  View_Del.Kill()    

  Replace_Strings(L:Content, '[Commodity]', CLIP(L:Commodities))
  Replace_Strings(L:Content, '[Items]', L:Units)
  Replace_Strings(L:Content, '[Weight]', L:Weight)
  Replace_Strings(L:Content, '[Special Remarks]', CLIP(DEL:Notes))
    
  !Branch|Dispatch Vehicle|Delivery Date/Time
  ! Branch is the DI FID name
  Replace_Strings(L:Content, '[Branch]', FLO:Floor)

  ! Get delivery vehicle info
  ! Returns comma del list of tripsheets in first to last order
  L:TripSheets  = Get_Delivery_TripSheets(DEL:DID)
  TRI:TRID      = Get_1st_Element_From_Delim_Str(L:TripSheets,',',1)
  IF Access:TripSheets.TryFetch(TRI:PKey_TID) = LEVEL:Benign
      ! p:Option  1.  Capacity                            of all
      !           2.  Registrations                       of all OR p:Truck = 100 gives horse reg only
      !           3.  Makes & Models                      of all
      !           4.  Description of Truck / Trailer      requires p:Truck  0 to 3
      !           5.  Truck / Trailer Capacity            requires p:Truck  0 to 3
      !           6.  Truck / Trailer ID (TTID)           requires p:Truck  0 to 3
      !           7.  Count of vehicles in composition
      !           8.  Compositions Capacity
      !           9.  Driver of Horse
      !           10. Transporters ID for this VCID
      !           11. Licensing dates                     of all
      L:TS_Veh    = Get_VehComp_Info(TRI:VCID, 2)
      L:BranchTel = Get_Address(Get_Branch_Info(TRI:BID, 2), 3)   ! could return comma del list      
  . 
    
  Replace_Strings(L:Content, '[Dispatch Vehicle]', CLIP(L:TS_Veh))     ! Trip sheet  
  Replace_Strings(L:Content, '[Branch Tel.]', CLIP(L:BranchTel))  

  ! see above in loop
  Replace_Strings(L:Content, '[Delivery Date/Time]', CLIP(L:Del_Date))

    
  t#  = DEFORMAT('13:00', @t1)     ! hh:mm      
  Date_Time_Advance(MAN:DepartDate, MAN:DepartTime, 1, t#)
  Replace_Strings(L:Content, '[ETA]', FORMAT(MAN:DepartDate,@d6) & ' @ ' & FORMAT(MAN:DepartTime, @t1))    

  RETURN(L:Content)
    
    ! Date_Time_Advance
    ! (*DATE, <*TIME>, BYTE, LONG)
    ! (p_Date, p_Time, p_Option, p_Val)
    !
    ! p_Option
    !       1 - Time
    !       2 - Days
    !       3 - Weeks
    !       4 - Months
    !       5 - Quarters
    !       6 - Years
    
    ! Get_Address
    ! (p:AID, p:Type, p:Details)
    ! (ULONG, BYTE=0, BYTE=0),STRING
    ! p:Type
    !   0 - Comma delimited string
    !   1 - Block
    !   2 - Comma delimited string - including empty Line 1 & 2 where they are empty
    !   3 - Phone Nos. (comma delim)
    ! p:Details
    !   0 - all
    !   1 - without City & Province
    !
    ! Returns
    !   Address Name, Line1 (if), Line2 (if), Post Code, City, Province
    !   or
    !   Address Name, Line1 (if), Line2 (if), Post Code
    !   or
    !   Address Name, Line1 (if), Line2 (if), <missing info string>
    !   or
    !   Phone1, Phone2 OR Phone1 OR Phone 2

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

