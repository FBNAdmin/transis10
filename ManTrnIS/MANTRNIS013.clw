

   MEMBER('MANTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE
   INCLUDE('NetEmail.inc'),ONCE
   INCLUDE('abbreak.inc'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('MANTRNIS013.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Window_Journey_Activity PROCEDURE 

LOC:Options          GROUP,PRE(L_OG)                       !
CID                  ULONG                                 !Client ID
ClientName           STRING(100)                           !
ClientNo             ULONG                                 !Client No.
FromDate             DATE                                  !
ToDate               DATE                                  !
                     END                                   !
LOC:Journey_Q        QUEUE,PRE(L_JQ)                       !
Journey              STRING(70)                            !Description
Kgs                  DECIMAL(11)                           !Kgs
DIs                  ULONG                                 !No. of DIs
JID                  ULONG                                 !Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
                     END                                   !
LOC:Work_Group       GROUP,PRE(L_WG)                       !
Idx                  LONG                                  !
                     END                                   !
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q1
ThisGraph4    Class(GlobalInsight)
ValidateRecord  PROCEDURE(long graphID),Long ,VIRTUAL
Reset           PROCEDURE (Byte graphForce=0),VIRTUAL
              End
ThisGraph4:ClickedOnPointName   String(255)
ThisGraph4:ClickedOnPointNumber Real
ThisGraph4:ClickedOnSetNumber   Long
ThisGraph4:Color                Group(iColorGroupType), PRE(ThisGraph4:Color)
                                End
!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
Window               WINDOW('Client Activity'),AT(,,397,223),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  MAX,MDI,IMM
                       SHEET,AT(4,2,390,198),USE(?Sheet1)
                         TAB('Options'),USE(?Tab1)
                           GROUP,AT(13,28,375,142),USE(?Group1),TRN
                             PROMPT('Client Name:'),AT(13,28),USE(?ClientName:Prompt),TRN
                             BUTTON('...'),AT(65,28,12,10),USE(?CallLookup)
                             ENTRY(@s100),AT(82,28,215,10),USE(L_OG:ClientName),REQ,TIP('Client name')
                             PROMPT('Client No.:'),AT(13,44),USE(?ClientNo:Prompt),TRN
                             ENTRY(@n_10b),AT(82,44,60,10),USE(L_OG:ClientNo),RIGHT(1),MSG('Client No.'),TIP('Client No.')
                             PROMPT('From Date:'),AT(13,64),USE(?FromDate:Prompt),TRN
                             BUTTON('...'),AT(65,64,12,10),USE(?Calendar)
                             ENTRY(@d6b),AT(82,64,60,10),USE(L_OG:FromDate)
                             PROMPT('Leave dates blank for all records.'),AT(153,64),USE(?Prompt5),TRN
                             PROMPT('To Date:'),AT(13,80),USE(?ToDate:Prompt),TRN
                             BUTTON('...'),AT(65,80,12,10),USE(?Calendar:2)
                             ENTRY(@d6b),AT(82,80,60,10),USE(L_OG:ToDate)
                           END
                           BUTTON('&OK'),AT(333,166,55,28),USE(?OkButton:2),DEFAULT
                         END
                         TAB('Activity'),USE(?Tab_Activity)
                           REGION,AT(9,18,382,179),USE(?Insight),BEVEL(1,-1),IMM
                         END
                         TAB('Data'),USE(?Tab3)
                           LIST,AT(9,18,343,177),USE(?List_J),VSCROLL,FORMAT('216L(2)|M~Journey~@s70@60D(2)|M~Kgs~' & |
  'L@n-15.0@52D(2)|M~DI s~L@n13@'),FROM(LOC:Journey_Q)
                         END
                       END
                       BUTTON('&Cancel'),AT(358,206,36,14),USE(?CancelButton)
                     END
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q2

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Calendar2            CalendarClass
Calendar3            CalendarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

DI_View         VIEW(Deliveries)
    PROJECT(DEL:DID, DEL:JID)
!       JOIN(JOU:PKey_JID, DEL:JID)
!       PROJECT(JOU:Journey)
    .  !.



DI_VM       ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_Data               ROUTINE
    SETCURSOR(CURSOR:Wait)
!DI_View         VIEW(Deliveries)
!    PROJECT(DEL:DID)
!       JOIN(JOU:PKey_JID, DEL:JID)
!       PROJECT(JOU:Journey)
!    .  .

    BIND('DEL:DIDate', DEL:DIDate)
    BIND('L_OG:FromDate', L_OG:FromDate)
    BIND('L_OG:ToDate', L_OG:ToDate)

    DI_VM.Init(DI_View, Relate:Deliveries)
    DI_VM.AddSortOrder(DEL:FKey_CID)
    DI_VM.AddRange(DEL:CID, L_OG:CID)
    !DI_VM.AppendOrder()
    IF L_OG:FromDate > 0
       DI_VM.SetFilter('DEL:DIDate >= L_OG:FromDate','1')
    .
    IF L_OG:ToDate > 0
       DI_VM.SetFilter('DEL:DIDate < L_OG:ToDate + 1','2')
    .

    DI_VM.Reset()
    LOOP
       IF DI_VM.Next() ~= LEVEL:Benign
          BREAK
       .

       CLEAR(LOC:Journey_Q)
       L_JQ:JID     = DEL:JID
       GET(LOC:Journey_Q, L_JQ:JID)
       IF ERRORCODE()
          CLEAR(LOC:Journey_Q)
          L_JQ:JID  = DEL:JID
          ADD(LOC:Journey_Q)
       .

       ! Get_DelItem_s_Totals
       ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache)
       ! (ULONG, BYTE, <ULONG>, <*STRING>, BYTE=0),ULONG
       !
       ! p:Option
       !   0. Weight of all DID items - Volumetric considered
       !   1. Total Items not loaded on the DID    - Manifest
       !   2. Total Items loaded on the DID        - Manifest
       !   3. Total Items on the DID
       !   4. Total Items not loaded on the DID    - Tripsheet
       !   5. Total Items loaded on the DID        - Tripsheet
       !   6. Weight of all DID items - real weight
       !   7. Total Items not delivered on the DID - Tripsheet     (similar to 4)
       !
       ! p:DIID is passed to check a single DIID
       ! p:Items_DIID_List is passed to return a list of DIIDs for the given Option
       !
       ! Cached entries are renewed every 1.5 seconds

       L_JQ:Kgs    += Get_DelItem_s_Totals(DEL:DID,0,,,TRUE) / 100  ! Returns a Ulong
       L_JQ:DIs    += 1

       PUT(LOC:Journey_Q)
    .
    DI_VM.Kill()

    UNBIND('DEL:DIDate')
    UNBIND('L_OG:FromDate')
    UNBIND('L_OG:ToDate')

    DO Load_Journeys
    SETCURSOR()
    EXIT
Load_Journeys         ROUTINE
    L_WG:Idx    = 0
    LOOP
       L_WG:Idx += 1
       GET(LOC:Journey_Q, L_WG:Idx)
       IF ERRORCODE()
          BREAK
       .

       JOU:JID          = L_JQ:JID
       IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
          L_JQ:Journey  = JOU:Journey
          PUT(LOC:Journey_Q)
    .  .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Window_Journey_Activity')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ClientName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Window_Journey_Activity',Window)           ! Restore window settings from non-volatile store
      L_OG:ClientName     = CLI:ClientName            ! Current buffer on this thread
      L_OG:CID            = CLI:CID
      L_OG:ClientNo       = CLI:ClientNo
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
    ! Start Of Insight InitStuff Group
  Bind('ThisGraph4:ClickedOnPointNumber',ThisGraph4:ClickedOnPointNumber) ! Insight ClickedOnVariable
  Bind('ThisGraph4:ClickedOnPointName',ThisGraph4:ClickedOnPointName)       ! Insight ClickedOnVariable
  Bind('ThisGraph4:ClickedOnSetNumber',ThisGraph4:ClickedOnSetNumber)     ! Insight ClickedOnVariable
  if ThisGraph4.Init(?Insight,Insight:Bar).
  if not ThisGraph4:Color:Fetched then ThisGraph4.GetWindowsColors(ThisGraph4:Color).
  Window{prop:buffer} = 1
  ThisGraph4.Stacked=0
  ThisGraph4.BlackAndWhite=0
  ThisGraph4.BackgroundPicture=''
  ThisGraph4.BackgroundColor=Color:Silver
  ThisGraph4.BackgroundShadeColor=Color:None
  ThisGraph4.BorderColor=Color:Black
  ThisGraph4.MinPointWidth=0
  ThisGraph4.PaperZoom=4
  ThisGraph4.ActiveInvisible=0
  ThisGraph4.PrintPortrait=0
  ThisGraph4.RightBorder=10
  ThisGraph4.MaxXGridTicks=16
  ThisGraph4.MaxYGridTicks=16
  ThisGraph4.WorkSpaceWidth=0
  ThisGraph4.WorkSpaceHeight=0
  ThisGraph4.AutoShade=1
  ThisGraph4.TopShade=0
  ThisGraph4.RightShade=0
  ThisGraph4.LongShade=0
  ThisGraph4.Pattern=Insight:None
  ThisGraph4.Float=0
  ThisGraph4.ZCluster=0
  ThisGraph4.Fill=0
  ThisGraph4.FillToZero=0
  ThisGraph4.SquareWave=0
  ThisGraph4.StackLines=1
  ThisGraph4.InnerRadius=0
  ThisGraph4.MaxPieRadius=0
  ThisGraph4.PieAngle=0
  ThisGraph4.PieLabelLineColor=Color:None
  ThisGraph4.AspectRatio=1
  ThisGraph4.PieLabelLines=0
  ThisGraph4.Shape=Insight:Auto
  ThisGraph4.LineFromZero=0
  ThisGraph4.LineWidth = 1
      ! Fonts
  ThisGraph4.LegendAngle   = 0
  ThisGraph4.ShowXLabelsEvery = 1
  ThisGraph4.DisplayOnX = 2   ! 0= don't display, 1= display number, 2= display name.
  ThisGraph4.SeparateYAxis = 1
  ThisGraph4.AutoXLabels = 0
  ThisGraph4.SpreadXLabels       = 0
  ThisGraph4.AutoXGridTicks = 1
  ThisGraph4.AutoScale = Scale:Low + Scale:High
  ThisGraph4.AutoYGridTicks = 0
  ThisGraph4.ShowDataLabels = 1
  ThisGraph4.DataLabelFormat = '@N13'
  ThisGraph4.ShowDataLabelsEvery = 1
  ThisGraph4.Depth = 200 / 10
          
  ThisGraph4.AddItem(1,1,LOC:Journey_Q,Insight:GraphField,L_JQ:Kgs,,,,,,L_JQ:Journey)
  ThisGraph4.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  
  ThisGraph4.SetSetType(1,Insight:None)
  If ThisGraph4.GetSet(1) = 0 then ThisGraph4.AddSetQ(1).
  ThisGraph4.SetQ.SquareWave = -1
  ThisGraph4.SetQ.Fill       = -1
  ThisGraph4.SetQ.FillToZero = -1
  ThisGraph4.SetQ.PointWidth = 66
  ThisGraph4.SetQ.Points     = 0
  Put(ThisGraph4.SetQ)
  !Mouse
  ThisGraph4.setMouseMoveParameters(1, '@N13')
  if ThisGraph4.SavedGraphType<>0 then ThisGraph4.Type = ThisGraph4.SavedGraphType.
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Window_Journey_Activity',Window)        ! Save window data to non-volatile store
  END
   ThisGraph4.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     ThisGraph4.Reset()
     Post(Event:accepted,?Insight)


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Select_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = L_OG:ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_OG:ClientName = CLI:ClientName
        L_OG:ClientNo = CLI:ClientNo
        L_OG:CID = CLI:CID
      END
      ThisWindow.Reset(1)
    OF ?L_OG:ClientName
      IF L_OG:ClientName OR ?L_OG:ClientName{PROP:Req}
        CLI:ClientName = L_OG:ClientName
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_OG:ClientName = CLI:ClientName
            L_OG:ClientNo = CLI:ClientNo
            L_OG:CID = CLI:CID
          ELSE
            CLEAR(L_OG:ClientNo)
            CLEAR(L_OG:CID)
            SELECT(?L_OG:ClientName)
            CYCLE
          END
        ELSE
          L_OG:ClientNo = CLI:ClientNo
          L_OG:CID = CLI:CID
        END
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
      Calendar2.SelectOnClose = True
      Calendar2.Ask('Select a Date',L_OG:FromDate)
      IF Calendar2.Response = RequestCompleted THEN
      L_OG:FromDate=Calendar2.SelectedDate
      DISPLAY(?L_OG:FromDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar3.SelectOnClose = True
      Calendar3.Ask('Select a Date',L_OG:ToDate)
      IF Calendar3.Response = RequestCompleted THEN
      L_OG:ToDate=Calendar3.SelectedDate
      DISPLAY(?L_OG:ToDate)
      END
      ThisWindow.Reset(True)
    OF ?OkButton:2
      ThisWindow.Update()
          DO Load_Data
          SELECT(?Tab_Activity)
    OF ?Insight
      If ((?Insight{prop:visible} or ThisGraph4.ActiveInvisible) and not 0{prop:acceptall})
        ThisGraph4.Draw()
      End
    OF ?CancelButton
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Insight
    CASE EVENT()
    Of INSIGHT:ResetAndDraw
      ThisGraph4.Reset()
      Post(event:accepted,?Insight)
    OF EVENT:MouseUp
        If ThisGraph4.GetPoint(ThisGraph4:ClickedOnSetNumber,ThisGraph4:ClickedOnPointNumber) = true
          ThisGraph4:ClickedOnPointName = ThisGraph4.GetPointName(ThisGraph4:ClickedOnSetNumber,ThisGraph4:ClickedOnPointNumber)
        End
    OF EVENT:MouseMove
              IF ThisGraph4.GetPointName() <> ''
      ?Insight{prop:tip} = ThisGraph4.GetPointSummary(1,'<10>')
              ELSE
      ?Insight{prop:tip} = ''
              END
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph4.ValidateRecord  PROCEDURE (long graphID)
ReturnValue  Long  ! set to > 0 to reject record
  Code
  ReturnValue = Parent.ValidateRecord(GraphID)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph4.Reset     PROCEDURE (Byte graphForce=0)
  Code
  self.HeaderName = 'Client Activity'
  self.XAxisName = 'Journeys'
  self.YAxisName = 'Kgs'
  Sort(LOC:Journey_Q,L_JQ:Journey)
      
  Self.SetSetPoints(1,0)
  Self.SetSetDescription(1,'Kgs')
  Self.SetSetDataLabels(1,0,'', 1,0)
  ThisGraph4.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  
  Parent.Reset(graphForce)
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group1

!!! <summary>
!!! Generated from procedure template - Report
!!! Report the _Invoice File
!!! </summary>
Print_SalesRep PROCEDURE (Long pSRID,Byte pSummary, Byte pSort, Ulong pCID, Long pDateFrom=0, Long pDateTo=0, Byte inclVAT, Byte FullyPaidOnly)

RepTotal             GROUP,PRE(REP)                        !
InvTotal             LIKE(INV:Total)                       !
ManifestTotal        LIKE(MAN:Cost)                        !
GPTotal              DECIMAL(14,2)                         !
GPPerCentTotal       DECIMAL(7,2)                          !
InvoiceTotalExVat    DECIMAL(11,2)                         !
                     END                                   !
ClientTotal          GROUP,PRE(CLT)                        !
InvTotal             LIKE(INV:Total)                       !
ManifestTotal        LIKE(MAN:Cost)                        !
GPTotal              DECIMAL(14,2)                         !
GPPerCentTotal       DECIMAL(7,2)                          !
InvoiceTotalExVat    DECIMAL(11,2)                         !
                     END                                   !
GrandTotal           GROUP,PRE(SUM)                        !
InvTotal             LIKE(INV:Total)                       !
ManifestTotal        LIKE(MAN:Cost)                        !
GPTotal              DECIMAL(14,2)                         !
GPPerCentTotal       DECIMAL(7,2)                          !
InvoiceTotalExVat    DECIMAL(11,2)                         !
                     END                                   !
MonthName            CSTRING(10)                           !
MonthTotal           GROUP,PRE(MNT)                        !
InvTotal             LIKE(INV:Total)                       !
ManifestTotal        LIKE(MAN:Cost)                        !
GPTotal              DECIMAL(14,2)                         !
GPPerCentTotal       DECIMAL(7,2)                          !
InvoiceTotalExVat    DECIMAL(11,2)                         !
                     END                                   !
RecordFilter         CSTRING(256)                          !
ListOrder            CSTRING(256)                          !
SalesRepName         STRING(35)                            !Sales Rep. Name
ManifestTotal        LIKE(MAN:Cost)                        !
GP                   DECIMAL(7,2)                          !
GPPerCent            DECIMAL(7,2)                          !
InvoiceTotalExVat    DECIMAL(11,2)                         !
Progress:Thermometer BYTE                                  !
DateFrom             LONG                                  !
DateTo               LONG                                  !
InvoiceTotal         DECIMAL(7,2)                          !
Process:View         VIEW(_Invoice)
                       PROJECT(INV:CID)
                       PROJECT(INV:DID)
                       PROJECT(INV:IID)
                       PROJECT(INV:InvoiceDate)
                       PROJECT(INV:MID)
                       PROJECT(INV:Status)
                       PROJECT(INV:Total)
                       PROJECT(INV:VAT)
                       PROJECT(INV:Weight)
                       JOIN(CLI:PKey_CID,INV:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:SRID)
                         JOIN(SAL:PKey_SRID,CLI:SRID)
                           PROJECT(SAL:SRID)
                           PROJECT(SAL:SalesRep)
                         END
                       END
                       JOIN(MAN:PKey_MID,INV:MID)
                         PROJECT(MAN:Cost)
                         PROJECT(MAN:MID)
                         PROJECT(MAN:VATRate)
                       END
                     END
ProgressWindow       WINDOW('Sales Rep Report'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,17,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                       PROMPT('...'),AT(15,2,111,14),USE(?Action),CENTER
                     END

Report               REPORT('Sales Rep Report 1'),AT(250,1100,7750,10083),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular,CHARSET:DEFAULT),THOUS
                       HEADER,AT(250,250,7750,885),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT)
                         STRING('Sales Rep Report'),AT(0,20,7760),USE(?ReportTitle),FONT('Microsoft Sans Serif',18, |
  ,FONT:bold,CHARSET:DEFAULT),CENTER
                         BOX,AT(0,604,7760,250),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(1115,604,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2208,604,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(3323,604,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(4427,604,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(5531,604,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(6635,604,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         STRING('Invoice Date'),AT(52,656,1042,170),USE(?HeaderTitle:1),TRN
                         STRING('Invoice No'),AT(1310,656,812,170),USE(?HeaderTitle:2),RIGHT(50),TRN
                         STRING('Invoice Total'),AT(2373,656,844,170),USE(?HeaderTitle:3),RIGHT(50),TRN
                         STRING('Manifest No'),AT(3508,656,823,170),USE(?HeaderTitle:4),RIGHT(50),TRN
                         STRING('Manifest Total'),AT(4581,656,854,170),USE(?HeaderTitle:5),RIGHT(50),TRN
                         STRING('GP'),AT(5727,656,812,170),USE(?HeaderTitle:6),RIGHT(50),TRN
                         STRING('%GP'),AT(6800,656,885,170),USE(?HeaderTitle:7),RIGHT(50),TRN
                         STRING(''),AT(3967,365,3771),USE(?DateRangeString),RIGHT(50)
                         STRING(@n_10),AT(31,31,427),USE(MAN:MID),RIGHT(1),HIDE
                         STRING(@n-14.2),AT(396,21),USE(MAN:Cost,,?MAN:Cost:3),RIGHT(1),HIDE
                         STRING(@n-7.2),AT(1344,31),USE(MAN:VATRate),RIGHT(1),HIDE
                         STRING(@n-7.2),AT(1844,31),USE(INV:VAT),RIGHT(1),HIDE
                         STRING(@n-7.2),AT(2323,21,479,167),USE(INV:Total),RIGHT(1),HIDE
                         STRING(@n-7.2),AT(5062,21,479,167),USE(INV:DID),RIGHT(1),HIDE
                         STRING(@n-7.2),AT(5604,31,479,167),USE(INV:Weight),RIGHT(1),HIDE
                         STRING(''),AT(52,365,3771,167),USE(?StatusString),LEFT(50)
                       END
Break_SalesRep         BREAK(SAL:SRID),USE(?BREAK1)
                         HEADER,AT(0,0,7750,396),USE(?Hdr_SalesRep)
                           STRING('Sales Rep:'),AT(52,177),USE(?SalesRepNameTitle)
                           STRING(@s35),AT(635,167,4906),USE(SAL:SalesRep),FONT('Microsoft Sans Serif',10,,FONT:bold)
                           LINE,AT(0,396,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         END
Break_Month              BREAK(MonthName),USE(?BREAK3)
Break_Client               BREAK(INV:CID),USE(?BREAK2)
                             HEADER,AT(0,0,7750,0),USE(?Hdr_Client)
                               STRING('Client:'),AT(52,177),USE(?ClientTitle),HIDE
                               STRING(@s100),AT(635,177,4906,167),USE(CLI:ClientName),FONT('Microsoft Sans Serif',,,FONT:bold), |
  HIDE
                               LINE,AT(0,396,7750,0),USE(?DetailEndLine:4),COLOR(COLOR:Black),HIDE
                             END
Detail_ByDate                DETAIL,AT(0,0,7750,250),USE(?Detail_ByDate)
                               LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                               LINE,AT(1115,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                               LINE,AT(2208,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                               LINE,AT(3323,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                               LINE,AT(4427,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                               LINE,AT(5531,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                               LINE,AT(6635,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                               STRING(@d6),AT(52,42,1042,170),USE(INV:InvoiceDate),LEFT
                               STRING(@n_10),AT(1240,42,833,170),USE(INV:IID),RIGHT
                               STRING(@n-15.2),AT(2323,42,844,170),USE(InvoiceTotal),RIGHT
                               STRING(@n_10b),AT(3458,42,823,170),USE(INV:MID),RIGHT
                               STRING(@n-14.2),AT(4542,42,844,170),USE(ManifestTotal),RIGHT
                               STRING(@n-15.2),AT(5677,42,812,170),USE(GP),RIGHT
                               STRING(@n-7.2),AT(6875,42,760,170),USE(GPPerCent),RIGHT
                               LINE,AT(0,250,7750,0),USE(?DetailEndLine:2),COLOR(COLOR:Black)
                             END
Detail_ByClient              DETAIL,AT(0,0,7750,250),USE(?Detail_ByClient)
                               LINE,AT(0,0,0,250),USE(?DetailLine:0:2),COLOR(COLOR:Black)
                               LINE,AT(1115,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                               LINE,AT(2208,0,0,250),USE(?DetailLine:9),COLOR(COLOR:Black)
                               LINE,AT(3323,0,0,250),USE(?DetailLine:10),COLOR(COLOR:Black)
                               LINE,AT(4427,0,0,250),USE(?DetailLine:11),COLOR(COLOR:Black)
                               LINE,AT(5531,0,0,250),USE(?DetailLine:12),COLOR(COLOR:Black)
                               LINE,AT(6635,0,0,250),USE(?DetailLine:13),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?DetailLine:14),COLOR(COLOR:Black)
                               STRING(@d6),AT(52,42,1042,170),USE(INV:InvoiceDate,,?INV:InvoiceDate:2),LEFT
                               STRING(@n_10),AT(1240,42,833,170),USE(INV:IID,,?INV:IID:2),RIGHT
                               STRING(@n-15.2),AT(2323,42,844,170),USE(InvoiceTotal,,?INV:Total:2),RIGHT
                               STRING(@n_10b),AT(3458,42,823,170),USE(INV:MID,,?INV:MID:2),RIGHT
                               STRING(@n-14.2),AT(4542,42,844,170),USE(ManifestTotal,,?MAN:Cost:2),RIGHT
                               STRING(@n-15.2),AT(5677,42,812,170),USE(GP,,?GP:2),RIGHT
                               STRING(@n-7.2),AT(6875,42,760,170),USE(GPPerCent,,?GPPerCent:2),RIGHT
                               LINE,AT(0,250,7750,0),USE(?DetailEndLine:3),COLOR(COLOR:Black)
                             END
NoDetail                     DETAIL,AT(0,0,7750,0),USE(?NoDetail)
                             END
                             FOOTER,AT(0,0,7750,0),USE(?Ftr_Client)
                               LINE,AT(0,0,0,250),USE(?DetailLine:0:3),COLOR(COLOR:Black),HIDE
                               LINE,AT(2208,0,0,250),USE(?DetailLine:16),COLOR(COLOR:Black),HIDE
                               LINE,AT(3323,0,0,250),USE(?DetailLine:17),COLOR(COLOR:Black),HIDE
                               LINE,AT(4427,0,0,250),USE(?DetailLine:18),COLOR(COLOR:Black),HIDE
                               LINE,AT(5531,0,0,250),USE(?DetailLine:19),COLOR(COLOR:Black),HIDE
                               LINE,AT(6635,0,0,250),USE(?DetailLine:20),COLOR(COLOR:Black),HIDE
                               LINE,AT(7750,0,0,250),USE(?DetailLine:21),COLOR(COLOR:Black),HIDE
                               STRING(@s100),AT(52,42,2094,146),USE(CLI:ClientName,,?CLI:ClientName:2)
                               STRING(@n-15.2),AT(2323,42,844,167),USE(CLT:InvTotal),RIGHT,HIDE
                               STRING(@n-14.2),AT(4542,42,844,167),USE(CLT:ManifestTotal),RIGHT,HIDE
                               STRING(@n-15.2),AT(5625,42,865,167),USE(CLT:GPTotal),RIGHT,HIDE
                               STRING(@n-7.2),AT(6896,42,740,167),USE(CLT:GPPerCentTotal),RIGHT,HIDE
                               LINE,AT(0,250,7750,0),USE(?Line19:5),COLOR(COLOR:Black)
                               LINE,AT(0,500,7750,0),USE(?ClientSeparator),COLOR(COLOR:White)
                             END
                           END
                           FOOTER,AT(0,0,7750,0),USE(?Ftr_Month)
                             LINE,AT(0,0,0,250),USE(?DetailLine:0:4),COLOR(COLOR:Black),HIDE
                             LINE,AT(2208,0,0,250),USE(?DetailLine:22),COLOR(COLOR:Black),HIDE
                             LINE,AT(3323,0,0,250),USE(?DetailLine:23),COLOR(COLOR:Black),HIDE
                             LINE,AT(4427,0,0,250),USE(?DetailLine:24),COLOR(COLOR:Black),HIDE
                             LINE,AT(5531,0,0,250),USE(?DetailLine:25),COLOR(COLOR:Black),HIDE
                             LINE,AT(6635,0,0,250),USE(?DetailLine:26),COLOR(COLOR:Black),HIDE
                             LINE,AT(7750,0,0,250),USE(?DetailLine:27),COLOR(COLOR:Black),HIDE
                             STRING(@s10),AT(52,42,2094,146),USE(MonthName),HIDE
                             STRING(@n-15.2),AT(2323,42,844,167),USE(MNT:InvTotal),RIGHT,HIDE
                             STRING(@n-14.2),AT(4542,42,844,167),USE(MNT:ManifestTotal),RIGHT,HIDE
                             STRING(@n-15.2),AT(5625,42,865,167),USE(MNT:GPTotal),RIGHT,HIDE
                             STRING(@n-7.2),AT(6896,42,740,167),USE(MNT:GPPerCentTotal),RIGHT,HIDE
                             LINE,AT(0,250,7750,0),USE(?Line19:6),COLOR(COLOR:Black)
                           END
                         END
                         FOOTER,AT(0,0,,396),USE(?Ftr_SalesRep)
                           LINE,AT(52,52,7676,0),USE(?Line19:3),COLOR(COLOR:Black)
                           STRING('Rep Total'),AT(313,104),USE(?String27:1),TRN
                           LINE,AT(52,312,7676,0),USE(?Line19:4),COLOR(COLOR:Black)
                           STRING(@n-15.2),AT(2198,104,969,167),USE(REP:InvTotal),RIGHT
                           STRING(@n-14.2),AT(4427,104,958,167),USE(REP:ManifestTotal),RIGHT
                           STRING(@n-15.2),AT(5552,104,937,167),USE(REP:GPTotal),RIGHT
                           STRING(@n-7.2),AT(6615,104,1021,167),USE(REP:GPPerCentTotal),RIGHT
                         END
                       END
grandtotals            DETAIL,AT(0,0,,396),USE(?grandtotals)
                         LINE,AT(52,52,7676,0),USE(?Line19),COLOR(COLOR:Black)
                         STRING('Grand Total'),AT(313,104),USE(?String27:2),TRN
                         LINE,AT(52,312,7676,0),USE(?Line19:2),COLOR(COLOR:Black)
                         STRING(@n-15.2),AT(2198,104,969,167),USE(SUM:InvTotal),RIGHT
                         STRING(@n-14.2),AT(4427,104,958,167),USE(SUM:ManifestTotal),RIGHT
                         STRING(@n-15.2),AT(5552,104,937,167),USE(SUM:GPTotal),RIGHT
                         STRING(@n-7.2),AT(6615,104,1021,167),USE(SUM:GPPerCentTotal),RIGHT
                       END
                       FOOTER,AT(250,11188,7771,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt:2),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp:2),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt:2),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp:2),FONT('Arial',8,, |
  FONT:regular),TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount:2),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
Reset                  PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
BreakMgr             BreakManagerClass                     ! Break Manager
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.AskPreview PROCEDURE

  CODE
  PRINT(RPT:grandtotals)  
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_SalesRep')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:_Invoice.SetOpenRelated()
  Relate:_Invoice.Open                                     ! File _Invoice used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BreakMgr.Init()
  BreakMgr.AddBreak()
  BreakMgr.AddLevel() !SaleRep
  BreakMgr.AddResetField(SAL:SRID)
  BreakMgr.AddTotal(REP:InvTotal,InvoiceTotal,eBreakTotalSum,1)
  BreakMgr.AddTotal(REP:ManifestTotal,ManifestTotal,eBreakTotalSum,1)
  BreakMgr.AddTotal(REP:GPTotal,GP,eBreakTotalSum,1)
  BreakMgr.AddTotal(REP:InvoiceTotalExVat,InvoiceTotalExVat,eBreakTotalSum,1)
  BreakMgr.AddLevel() !MonthName
  BreakMgr.AddResetField(MonthName)
  BreakMgr.AddTotal(MNT:InvTotal,InvoiceTotal,eBreakTotalSum,1)
  BreakMgr.AddTotal(MNT:ManifestTotal,ManifestTotal,eBreakTotalSum,1)
  BreakMgr.AddTotal(MNT:GPTotal,GP,eBreakTotalSum,1)
  BreakMgr.AddTotal(MNT:InvoiceTotalExVat,InvoiceTotalExVat,eBreakTotalSum,1)
  BreakMgr.AddLevel() !Client
  BreakMgr.AddResetField(INV:CID)
  BreakMgr.AddTotal(CLT:InvTotal,InvoiceTotal,eBreakTotalSum,1)
  BreakMgr.AddTotal(CLT:ManifestTotal,ManifestTotal,eBreakTotalSum,1)
  BreakMgr.AddTotal(CLT:GPTotal,GP,eBreakTotalSum,1)
  BreakMgr.AddTotal(CLT:InvoiceTotalExVat,InvoiceTotalExVat,eBreakTotalSum,1)
  SELF.AddItem(BreakMgr)
  SELF.Open(ProgressWindow)                                ! Open window
  ?Action{PROP:Text} = 'Initializing report...'
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF NOT TargetSelector.ASK(1) THEN
     SELF.Kill()
     RETURN Level:Fatal
  END
  IF NOT TargetSelector.GetPrintSelected() THEN
     SELF.SetReportTarget(TargetSelector.GetReportSelected())
  END
  SELF.AddItem(TargetSelector)
  ListOrder = '+SAL:SalesRep,+INV:InvoiceDateAndTime'
  Case pSort
  Of SalesRep_ByClient  
     ListOrder = '+SAL:SalesRep,+CLI:ClientName,+INV:InvoiceDateAndTime'
  End
  !Message(ListOrder,'Debug message',,,,2)
  !Sales Rep
  If pSRID ~= 0 Then
     RecordFilter = 'SAL:SRID='&pSRID
     SalesRepName = GetSQLValue('Select SalesRep From SalesReps Where SRID = '& pSRID)
  Else
     RecordFilter = 'SAL:SRID~=0'
     SalesRepName = 'All'
  End  
  !Client
  If pCID ~= 0 THEN
     RecordFilter = RecordFilter & ' AND INV:CID = '& pCID
  End
  !Dates
  DateFrom = pDateFrom; DateTo = pDateTo
  If pDateFrom And pDateTo And pDateFrom > pDateTo Then
     DateFrom = pDateTo; DateTo = pDateFrom
  End
  If DateFrom Then
     RecordFilter = RecordFilter & ' AND INV:InvoiceDate >= ' & DateFrom
  End
  If DateTo Then
     RecordFilter = RecordFilter & ' AND INV:InvoiceDate < ' & DateTo + 1
  End
  !Statuses  
  If FullyPaidOnly Then
     RecordFilter = RecordFilter & ' AND (INV:Status = 3 OR INV:Status = 4 OR INV:Status = 7)'
  Else
     RecordFilter = RecordFilter & ' AND (INV:Status = 3 OR INV:Status = 4 OR INV:Status = 7'&|
                                     ' OR INV:Status = 0 OR INV:Status = 1)'
  End  
  ThisReport.Init(Process:View, Relate:_Invoice, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder(ListOrder)
  ThisReport.SetFilter(RecordFilter)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_Invoice.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Invoice.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  If ReturnValue = Level:Benign Then
     SetTarget(Self.Report)
     If pSRID ~= 0 Then ! Do not show grand totals if only one Sales Rep
        Hide(?Line19,?SUM:GPPerCentTotal)
     Else
        UnHide(?Line19,?SUM:GPPerCentTotal)
     End
     If pSort = SalesRep_ByClient Then ! Show Client totals if sort by Client
        If Not pSummary Then
           Hide(?DetailEndLine) !top detail line line in the Sales Rep footer (will be shown in Client header)
           UnHide(?ClientTitle,?DetailEndLine:4) !show Client header
           UnHide(?DetailLine:0:3,?ClientSeparator) !show Client footer with separator
        Else !do not show header - one line per Client (footer)
           UnHide(?DetailEndLine) !top detail line line in the Sales Rep footer (will be shown in Client header)
           Hide(?ClientTitle,?DetailEndLine:4) !hide Client header
           UnHide(?DetailLine:0:3,?Line19:5) !show Client footer
           Hide(?ClientSeparator)
        End        
     Else
        Hide(?ClientTitle,?DetailEndLine:4) !hide Client header
        Hide(?DetailLine:0:3,?ClientSeparator) !hide Client footer
     End
     If pSort = SalesRep_ByMonth Then
        UnHide(?DetailLine:0:4,?Line19:6) !show Client footer
        ! hide %GP
        !Hide(?REP:GPPerCentTotal)
        !Hide(?SUM:GPPerCentTotal)
     Else
        Hide(?DetailLine:0:4,?Line19:6) !hide Client footer
        ! UnHide %GP
        !UnHide(?REP:GPPerCentTotal)
        !If pSRID = 0 
        !   UnHide(?SUM:GPPerCentTotal)
        !End
     End
     If DateFrom Or DateTo Then
        ?DateRangeString{PROP:Text} = Choose(DateFrom=0,'...',Format(DateFrom,@D17B)) & ' - ' & Choose(DateTo=0,'...',Format(DateTo,@D17B))
        UnHide(?DateRangeString)
     Else
        Hide(?DateRangeString)
     End
     !Including/Excluding VAT and Fully Paid Only
     ?StatusString{PROP:Text} = Choose(inclVAT~=False,'Including VAT','Excluding VAT') & ' - ' &|
                                Choose(FullyPaidOnly=False,'All Invoices Paid & Unpaid','Fully Paid Invoices')
     SetTarget(ProgressWindow)
  End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp:2{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp:2{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

MT          Like(TManifestTotals)
DIWeight                Decimal(14,2)
TotalWeightOfManifest   Decimal(14,2)
!ManifestWeightPercentage Decimal(5,2)
DIWeightCost            Decimal(14,2)
ExtraLegs               Decimal(14,2)
  CODE
  !message('ThisWindow.TakeRecord<13,10>------------------<13,10>InvoiceDate:'&Format(INV:InvoiceDate,@D17B)&'<13,10>'&|
  !        'Month: '&Upper(GetMonthName(Month(INV:InvoiceDate))))
  If pSort = SalesRep_ByMonth THEN
     !Message(Format(INV:InvoiceDate,@D17),'Debug message')
     MonthName = Upper(GetMonthName(Month(INV:InvoiceDate)))
     !Message(Month(INV:InvoiceDate) & ' Month: '& MonthName,'Debug message')
  End
  InvoiceTotalExVat = (INV:Total-INV:VAT)
  InvoiceTotal = Choose(inclVAT=1,INV:Total,InvoiceTotalExVat)
  Clear(GP)
  Clear(GPPerCent)
  Clear(ManifestTotal)
  !Clear(Delivery_Charges_Ex)
  !Clear(InvoiceTotalExVat)
  If MAN:MID Then
     DIWeight = INV:Weight
     !DIWeight = Get_DelItem_s_Totals(INV:DID,6)/100.00
     !TotalWeightOfManifest = Get_ManLoad_Info(MAN:MID,0)
     TotalWeightOfManifest = Get_Manifest_Info(MAN:MID, 2,,, 0)
     !ManifestWeightPercentage = DIWeight / TotalWeightOfManifest
     DIWeightCost = DIWeight / TotalWeightOfManifest * MAN:Cost
     !ExtraLegs = Get_Manifest_Info(MAN:MID, 6) ! VAT is included
     ExtraLegs = Get_DelLegs_info(INV:DID,1)
     GP = InvoiceTotalExVat - DIWeightCost - ExtraLegs
     GPPerCent = GP / InvoiceTotalExVat * 100.00
     !Stop('IID:'&INV:IID&'; MAN:MID = '&MAN:MID&'<13,10>InvoiceTotalExVat = '&InvoiceTotalExVat & |
     !'<13,10>ManifestWeightPercentage = '&DIWeight&' / '&TotalWeightOfManifest&' ('&ManifestWeightPercentage&'%)'& |
     !'<13,10>ExtraLegs = '& ExtraLegs&'<13,10>DIWeightCost = ManifestWeightPercentage * MAN:Cost = ' & ManifestWeightPercentage&' * '&MAN:Cost&' = '&DIWeightCost &|
     !'<13,10>GP = InvoiceTotalExVat - DIWeightCost - ExtraLegs = '& InvoiceTotalExVat &' - '&DIWeightCost&' - '&ExtraLegs&' = '& GP & |
     !'<13,10>GPPerCent = GP / InvoiceTotalExVat = '& GP &' / '&InvoiceTotalExVat & ' = ' &GPPerCent)
  
     ! Older calculations
     !GetManifestTotals(MAN:MID,MAN:Cost,MAN:VATRate,MT,0)
     !GP                  = MT.Gross_Profit
     !GPPerCent           = MT.Gross_Profit_Percent
     !Delivery_Charges_Ex = MT.Delivery_Charges_Ex
     !ManifestTotal       = Choose(inclVAT=1,MT.Total_Cost,MT.Cost)
     ManifestTotal       = Choose(inclVAT=1,MAN:Cost + MAN:Cost * (MAN:VATRate / 100.00),MAN:Cost)
  End
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


ThisReport.Reset PROCEDURE

  CODE
  PARENT.Reset
  Self.RecordsToProcess = GetSqlValue('Select Count(*) From _Invoice A '&|
                                      '  Left Outer Join Clients B On B.CID = A.CID ' &|
                                      '  Inner Join SalesReps C On C.SRID = B.SRID ' &|
                                      'Where C.SRID '& Choose(pSRID=0, '<<> ','= ') & pSRID &|
                                      Choose(pCID=0,'',' And A.CID = ' & pCID) &|
                                      Choose(DateFrom=0,'',' And InvoiceDateAndTime >= ''' & Format(DateFrom,@D10-) & '''') &|
                                      Choose(DateTo=0,'',' And InvoiceDateAndTime < ''' & Format(DateTo+1,@D10-) & '''') & |
                                      ' And (A.Status = 3 Or A.Status = 4 Or A.Status = 7' &|
                                      Choose(FullyPaidOnly=False,' Or A.Status = 0 Or A.Status = 1)',')'))
                                      
  !Message('Records to process: '&Self.RecordsToProcess &|
  !        '<13,10>SQL request: '& 'Select Count(*) From _Invoice A '&|
  !                                    '  Left Outer Join Clients B On B.CID = A.CID ' &|
  !                                    '  Inner Join SalesReps C On C.SRID = B.SRID ' &|
  !                                    'Where C.SRID '& Choose(pSRID=0, '<<> ','= ') & pSRID &|
  !                                    Choose(pCID=0,'',' And A.CID = ' & pCID) &|
  !                                    Choose(DateFrom=0,'',' And InvoiceDateAndTime >= ''' & Format(DateFrom,@D10-) & '''') &|
  !                                    Choose(DateTo=0,'',' And InvoiceDateAndTime < ''' & Format(DateTo+1,@D10-) & '''') |
  !        ,'Debug message',,,,2)


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ?Action{PROP:Text} = 'Fetching records...'
  SkipDetails = pSummary  
  ReturnValue = PARENT.TakeRecord()
  SUM:InvTotal            += InvoiceTotal
  SUM:ManifestTotal       += ManifestTotal
  !SUM:Delivery_Charges_Ex += Delivery_Charges_Ex
  SUM:InvoiceTotalExVat   += InvoiceTotalExVat
  SUM:GPTotal             += GP 
  
  !Old calculation
  !SUM:GPPerCentTotal      = Choose(SUM:Delivery_Charges_Ex = 0,0,(SUM:GPTotal / SUM:Delivery_Charges_Ex) * 100)
  !REP:GPPerCentTotal      = Choose(REP:Delivery_Charges_Ex = 0,0,(REP:GPTotal / REP:Delivery_Charges_Ex) * 100)
  !CLT:GPPerCentTotal      = Choose(CLT:Delivery_Charges_Ex = 0,0,(CLT:GPTotal / CLT:Delivery_Charges_Ex) * 100)
  !MNT:GPPerCentTotal      = Choose(MNT:Delivery_Charges_Ex = 0,0,(MNT:GPTotal / MNT:Delivery_Charges_Ex) * 100)
  
  SUM:GPPerCentTotal      = Choose(SUM:InvoiceTotalExVat = 0,0,(SUM:GPTotal / SUM:InvoiceTotalExVat) * 100.00)
  REP:GPPerCentTotal      = Choose(REP:InvoiceTotalExVat = 0,0,(REP:GPTotal / REP:InvoiceTotalExVat) * 100.00)
  CLT:GPPerCentTotal      = Choose(CLT:InvoiceTotalExVat = 0,0,(CLT:GPTotal / CLT:InvoiceTotalExVat) * 100.00)
  MNT:GPPerCentTotal      = Choose(MNT:InvoiceTotalExVat = 0,0,(MNT:GPTotal / MNT:InvoiceTotalExVat) * 100.00)
  
  !Stop('SUM:GPTotal: '&SUM:GPTotal&'<13,10>SUM:GPPerCentTotal: '&SUM:GPPerCentTotal)
  IF 0
    PRINT(RPT:Detail_ByDate)
  END
  IF 0
    PRINT(RPT:Detail_ByClient)
  END
  IF 0
    PRINT(RPT:NoDetail)
  END
  IF 0
    PRINT(RPT:grandtotals)
  END
  IF SkipDetails
    PRINT(RPT:NoDetail)
  Else
    Case pSort
    Of 0 ! by Date
       Print(RPT:Detail_ByDate)
    Of 1 ! by Client
       Print(RPT:Detail_ByClient)
    End   
  END  
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','MANTRNIS','New_t_SalesRep_ByDate','New_t_SalesRep_ByDate','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Window
!!! Sales Rep Report settings
!!! </summary>
Start_Print_SalesRep PROCEDURE 

SalesRepName         STRING(35)                            !Sales Rep. Name
Years                QUEUE,PRE(Y)                          !
Year                 USHORT                                !
                     END                                   !
EachSalesRep         BYTE                                  !
DateFrom             DATE                                  !
DateTo               DATE                                  !
AllSalesReps         BYTE                                  !
Details              BYTE                                  !
ListOption           BYTE                                  !
SRID                 ULONG                                 !Sales Rep ID
CID                  ULONG                                 !Client ID
AllClients           BYTE                                  !
ClientName           STRING(100)                           !
ClientNo             ULONG                                 !Client No.
InclVAT              BYTE                                  !
FullyPaidOnly        BYTE                                  !
AllPeriods           ULONG

Period:ThisYear      EQUATE(Period:Current9)
Period:LastYear      EQUATE(Period:Past9)

DatePeriodFilter     CLASS(DatePeriodClass)
FillQPeriod            PROCEDURE(ULONG pPeriodEquates),VIRTUAL
GetPeriodName          PROCEDURE(ULONG pPeriodEquate),STRING,VIRTUAL
MovePeriod             PROCEDURE(BYTE Direction),VIRTUAL
GetPeriodType          PROCEDURE,SHORT,VIRTUAL
                     END
QuickWindow          WINDOW('Sales Rep Reports'),AT(,,260,197),FONT('Tahoma',8,,FONT:regular,CHARSET:DEFAULT),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Start_Print_SalesRep')
                       CHECK(' All Sales Reps'),AT(9,3),USE(AllSalesReps),TRN
                       BUTTON('...'),AT(48,15,12,12),USE(?CallLookup),DISABLE
                       ENTRY(@s20),AT(64,15,189,11),USE(SalesRepName),DISABLE,READONLY,REQ,SKIP
                       PROMPT('Sales Rep:'),AT(9,18),USE(?PROMPT1),DISABLE,TRN
                       CHECK(' Each Sales Rep'),AT(87,3,65,10),USE(EachSalesRep),HIDE,TRN
                       CHECK(' Including VAT'),AT(9,31),USE(InclVAT)
                       CHECK(' Fully Paid Only'),AT(137,31,81),USE(FullyPaidOnly)
                       PROMPT('Period:'),AT(145,79,33),USE(?PeriodPrompt),HIDE
                       LIST,AT(170,79,82,11),USE(?PeriodList),DROP(10),FORMAT('16L(2)|M@s255@'),HIDE
                       PROMPT('Date from:'),AT(9,47),USE(?DateFrom:Prompt),RIGHT,TRN
                       ENTRY(@D17B),AT(49,46,60,11),USE(DateFrom),RIGHT(1)
                       BUTTON('...'),AT(113,45,12,12),USE(?CalendarFrom)
                       PROMPT('Date to:'),AT(137,48),USE(?DateTo:Prompt),RIGHT,TRN
                       ENTRY(@D17B),AT(167,46,60,11),USE(DateTo),RIGHT(1)
                       BUTTON('...'),AT(233,45,12,12),USE(?CalendarTo)
                       BUTTON('Previous'),AT(49,62,73,13),USE(?PrevPeriod),LEFT,ICON('movedown.ico'),FLAT,SKIP
                       BUTTON('Next'),AT(167,62,73,13),USE(?NextPeriod),LEFT,ICON('moveup.ico'),FLAT,SKIP
                       PROMPT('Report Mode:'),AT(9,79),USE(?ReportModePrompt)
                       LIST,AT(58,79,,11),USE(?ReportMode),DROP(2),FROM(' Normal| Summary')
                       PANEL,AT(5,94,251,80),USE(?Panel1),BEVEL(-1,1)
                       OPTION('List'),AT(10,97,110,42),USE(ListOption),BOXED,TRN
                         RADIO(' By Date'),AT(21,107,59),USE(?LISTOPTION:RADIO1),TRN,VALUE('0')
                         RADIO(' By Client'),AT(21,121,59,10),USE(?LISTOPTION:RADIO1:2),TRN,VALUE('1')
                       END
                       OPTION('Detalization'),AT(137,97,110,42),USE(Details),BOXED,TRN
                         RADIO(' Detailed'),AT(147,107,80),USE(?DETAILIZATIONOPTION:RADIO1),TRN,VALUE('0')
                         RADIO(' Summary'),AT(147,120,80,10),USE(?DETAILIZATIONOPTION:RADIO1:2),TRN,VALUE('1')
                       END
                       CHECK(' All Clients'),AT(10,142),USE(AllClients),HIDE,TRN
                       PROMPT('Client:'),AT(9,157),USE(?ClientPrompt),DISABLE,HIDE,TRN
                       ENTRY(@s100),AT(65,157,181,11),USE(ClientName),DISABLE,HIDE,READONLY,REQ,SKIP,TIP('Client name')
                       BUTTON('...'),AT(49,154,12,12),USE(?CallClientLookup),DISABLE,HIDE
                       PROMPT('Client No.:'),AT(169,143),USE(?ClientNo:Prompt),RIGHT,HIDE,TRN
                       STRING(@n_10b),AT(209,143,38,10),USE(ClientNo),RIGHT(1),HIDE,TRN
                       BUTTON('G&o'),AT(153,178,49,14),USE(?Ok),LEFT,ICON('WAOk.ICO'),DEFAULT,FLAT,MSG('Perform Operation'), |
  TIP('Perform Operation')
                       BUTTON('&Close'),AT(205,178,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(5,178,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar6            CalendarClass
Calendar7            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
RefreshOnCheckAll Routine
  If ?AllSalesReps{Prop:Checked} Or ?EachSalesRep{Prop:Checked} Then
    Disable(?PROMPT1)
    Disable(?CallLookup)
    Disable(?SalesRepName)
    If ?AllSalesReps{Prop:Checked} Then
       SalesRepName = 'All'
       EachSalesRep = False
    ElsIf ?EachSalesRep{Prop:Checked} Then
       SalesRepName = 'Each'
       AllSalesReps = False
    End
  Else
    Enable(?PROMPT1)
    Enable(?CallLookup)
    Enable(?SalesRepName)
    SalesRepName = ''
  End
  Display()
  SRID = 0

RefreshOnAllClients Routine
  If ?AllClients{Prop:Checked}
    Disable(?ClientPrompt)
    Disable(?ClientName)
    Disable(?CallClientLookup)
    ClientName = 'All'       
    Hide(?ClientNo:Prompt)
    Hide(?ClientNo)
  Else
    Enable(?ClientPrompt)
    Enable(?ClientName)
    Enable(?CallClientLookup)
    ClientName = ''
    ClientNo = ''
    UnHide(?ClientNo:Prompt)
    UnHide(?ClientNo)
  End  
  CID = 0
  Display()  

RefreshOnListOption Routine
  If ListOption = SalesRep_ByClient Then
     UNHIDE(?AllClients)
     UNHIDE(?ClientPrompt)
     UNHIDE(?ClientName)
     UNHIDE(?CallClientLookup)
     If Not AllClients Then
        UnHide(?ClientNo:Prompt)
        UnHide(?ClientNo)
     End
  Else   
     HIDE(?AllClients)
     HIDE(?ClientPrompt)
     HIDE(?ClientName)
     HIDE(?CallClientLookup)
     Hide(?ClientNo:Prompt)
     Hide(?ClientNo)
  End

SetDateRange Routine
  !DateTo   = Date(12,31,Y:Year)
  !DateFrom = Date(1,1,Y:Year)  
  DateTo = DATE(MONTH(TODAY()),1,YEAR(TODAY()))-1
  DateFrom=DATE(MONTH(DateTo),1,YEAR(DateTo))  
  
RefreshOnReportMode ROUTINE
  Case Choice(?ReportMode)
  Of 2
    Disable(?ListOption,?ClientName)
  Else  
    Enable(?ListOption,?ClientName)
  End

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

DatPar     GROUP(tgDatePeriodsParams).
  CODE
  GlobalErrors.SetProcedureName('Start_Print_SalesRep')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?AllSalesReps
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:SalesReps.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  InclVAT = False ! NOT Including VAT by Default (changed to "Off" 2 August,  2011)
  FullyPaidOnly = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  QuickWindow{PROP:MinWidth} = 260                         ! Restrict the minimum window width
  QuickWindow{PROP:MinHeight} = 160                        ! Restrict the minimum window height
  QuickWindow{PROP:MaxWidth} = 260                         ! Restrict the maximum window width
  QuickWindow{PROP:MaxHeight} = 160                        ! Restrict the maximum window height
  Resizer.Init(AppStrategy:Surface)                        ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  !Fill in Year drop list
  Free(Years)
  Loop Y# = GetSQLValue('Select Min(Year(InvoiceDateAndTime)) From _Invoice Where Year(InvoiceDateAndTime) > 1900') To Year(Today())
       Y:Year = Y#
       Add(Years)
  End
  Y:Year = Year(Today()) - 1
  Get(Years,Y:Year)
  If ErrorCode() THEN
     Get(Years,1)
  End
  !?Year{PROP:Selected} = Pointer(Years)
  Do SetDateRange
  AllSalesReps = True  !Do RefreshOnCheckAll
  SalesRepName = 'All'
  AllClients   = True
  ClientName = 'All'
  Do RefreshOnListOption
  ?ReportMode{PROP:Selected} = 1
  Do RefreshOnReportMode
  SELF.SetAlerts()
  AllPeriods=Period:All+Period:Today+Period:Yesterday+Period:ThisWeek+Period:LastWeek+Period:ThisMonth+Period:LastMonth+Period:DateRange + Period:ThisYear + Period:LastYear
  
  DatPar.PeriodEquates        = AllPeriods
  DatPar.DefaultPeriod        = Period:LastMonth
  DatPar.ListControl          = ?PeriodList
  DatPar.DateFromControl      = ?DateFrom
  DatPar.DateToControl        = ?DateTo
  DatPar.CurDateFrom          &= DateFrom
  DatPar.CurDateTo            &= DateTo
  DatPar.PrevControl          = ?PrevPeriod
  DatPar.NextControl          = ?NextPeriod
  
  DatePeriodFilter.Init(DatPar)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  DatePeriodFilter.Kill
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_SalesReps
      Select_Clients
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?CalendarFrom
      POST(Event:Accepted, ?DateFrom)
    OF ?CalendarTo
      POST(Event:Accepted, ?DateTo)
    OF ?ReportMode
      Do RefreshOnReportMode
    OF ?Cancel
      Post(EVENT:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?AllSalesReps
      Do RefreshOnCheckAll
    OF ?CallLookup
      ThisWindow.Update()
      SAL:SalesRep = SalesRepName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        SalesRepName = SAL:SalesRep
        SRID = SAL:SRID
      END
      ThisWindow.Reset(1)
    OF ?SalesRepName
      IF NOT QuickWindow{PROP:AcceptAll}
        IF SalesRepName OR ?SalesRepName{PROP:Req}
          SAL:SalesRep = SalesRepName
          IF Access:SalesReps.TryFetch(SAL:Key_SalesRep)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              SalesRepName = SAL:SalesRep
              SRID = SAL:SRID
            ELSE
              CLEAR(SRID)
              SELECT(?SalesRepName)
              CYCLE
            END
          ELSE
            SRID = SAL:SRID
          END
        END
      END
      ThisWindow.Reset()
    OF ?CalendarFrom
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',DateFrom)
      IF Calendar6.Response = RequestCompleted THEN
      DateFrom=Calendar6.SelectedDate
      DISPLAY(?DateFrom)
      END
      ThisWindow.Reset(True)
    OF ?CalendarTo
      ThisWindow.Update()
      Calendar7.SelectOnClose = True
      Calendar7.Ask('Select a Date',DateTo)
      IF Calendar7.Response = RequestCompleted THEN
      DateTo=Calendar7.SelectedDate
      DISPLAY(?DateTo)
      END
      ThisWindow.Reset(True)
    OF ?ListOption
      AllClients = True
      !?AllClients{PROP:Checked} = true
      Do RefreshOnAllClients
      Do RefreshOnListOption
      ThisWindow.Reset
    OF ?AllClients
      Do RefreshOnAllClients
    OF ?ClientName
      IF NOT QuickWindow{PROP:AcceptAll}
        IF ClientName OR ?ClientName{PROP:Req}
          CLI:ClientName = ClientName
          IF Access:Clients.TryFetch(CLI:Key_ClientName)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              ClientName = CLI:ClientName
              CID = CLI:CID
              ClientNo = CLI:ClientNo
            ELSE
              CLEAR(CID)
              CLEAR(ClientNo)
              SELECT(?ClientName)
              CYCLE
            END
          ELSE
            CID = CLI:CID
            ClientNo = CLI:ClientNo
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallClientLookup
      ThisWindow.Update()
      CLI:ClientName = ClientName
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        ClientName = CLI:ClientName
        CID = CLI:CID
        ClientNo = CLI:ClientNo
      END
      ThisWindow.Reset(1)
    OF ?Ok
      ThisWindow.Update()
      If Not ?AllSalesReps{PROP:Checked} THEN
         If SRID = 0 Then
            Message('Please choose Sales Rep.','TransIS - Required Fields',ICON:Asterisk)
            Cycle
         End
      Else
         SRID = 0
      End
      !Message('SRID='&SRID,'Debug message',,,,2)
      Case Choice(?ReportMode)
      Of 1
         If ListOption  = SalesRep_ByClient Then
            If Not ?AllClients{PROP:Checked} THEN
               If CID = 0 Then
                  Message('Please choose Client.','TransIS - Required Fields',ICON:Asterisk)
                  Cycle
               End
            Else
               CID = 0
            End
         Else
            CID = 0
            AllClients = True
            ?AllClients{PROP:Checked} = true
            Do RefreshOnAllClients
         End
         Print_SalesRep(SRID,Details,ListOption,CID,DateFrom,DateTo,InclVAT,FullyPaidOnly)
         !Print_SalesRep(SRID,Details,ListOption,CID,0,0)
      Of 2
         Print_SalesRep(SRID,SalesRep_Summary,SalesRep_ByMonth,0,DateFrom,DateTo,InclVAT,FullyPaidOnly)
      End
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  DatePeriodFilter.TakeEvent
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


DatePeriodFilter.FillQPeriod      PROCEDURE(ULONG pPeriodEquates)
 CODE
  Parent.FillQPeriod(pPeriodEquates)

  SELF.QPeriod.PeriodID=Period:ThisYear
  GET(SELF.QPeriod,SELF.QPeriod.PeriodID)
  IF ~ErrorCode()
     SELF.QPeriod.DateFrom=DATE(1,1,YEAR(TODAY()))
     SELF.QPeriod.DateTo=DATE(12,31,YEAR(TODAY()))
     PUT(SELF.QPeriod)
  END

  SELF.QPeriod.PeriodID=Period:LastYear
  GET(SELF.QPeriod,SELF.QPeriod.PeriodID)
  IF ~ErrorCode()
     SELF.QPeriod.DateFrom=DATE(1,1,YEAR(TODAY())-1)
     SELF.QPeriod.DateTo=DATE(12,31,YEAR(TODAY())-1)
     PUT(SELF.QPeriod)
  END


DatePeriodFilter.GetPeriodName    PROCEDURE(ULONG pPeriodEquate)
retStr    String(50)
 CODE
  CASE pPeriodEquate
  OF Period:ThisYear
    retStr='This Year'
  OF Period:LastYear
    retStr='Last Year'
  ELSE
    retStr=PARENT.GetPeriodName(pPeriodEquate)
  END
  RETURN retStr
  

DatePeriodFilter.MovePeriod               PROCEDURE(BYTE Direction) ! 1=NextPeriod
  CODE
  IF ~(SELF.PrevControl AND SELF.NextControl) THEN RETURN.
  !delta=SELF.GetPeriodType()
  IF ~Direction
    !SELF.DateFrom=SELF.DateTo+1
    SELF.DateFrom=DATE(MONTH(SELF.DateFrom)+1,1,YEAR(SELF.DateFrom))
    SELF.DateTo=DATE(MONTH(SELF.DateFrom)+1,1,YEAR(SELF.DateFrom))-1
  ELSE
    !SELF.DateTo=SELF.DateFrom-1    
    SELF.DateTo = DATE(MONTH(SELF.DateFrom),1,YEAR(SELF.DateFrom))-1
    SELF.DateFrom=DATE(MONTH(SELF.DateTo),1,YEAR(SELF.DateTo))    
  END
  Display(SELF.DateFromControl)
  Display(SELF.DateToControl)
  POST(Event:Accepted,SELF.DateFromControl)

DatePeriodFilter.GetPeriodType       PROCEDURE
  CODE
  Return -2 
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Clients_OverTime PROCEDURE 

LOC:Criteria_Group   GROUP,PRE(L_CG)                       !
Periods              BYTE                                  !
Trend_Periods        BYTE                                  !
FromDate             DATE                                  !
ToDate               DATE                                  !
FromDatePeriod2      DATE                                  !
ToDatePeriod2        DATE                                  !
Period2AutoSet       BYTE(1)                               !
Data_Q_Order         BYTE                                  !
                     END                                   !
LOC:Client_Criteria_Group GROUP,PRE(LCCG)                  !
FromDate             DATE                                  !
ToDate               DATE                                  !
FromDatePeriod2      DATE                                  !
ToDatePeriod2        DATE                                  !
Period2AutoSet       BYTE                                  !
Branch               STRING(35)                            !Branch Name
BID                  ULONG                                 !Branch ID
AllBranches          BYTE(1)                               !
SRID                 ULONG                                 !Sales Rep ID
SalesRep             STRING(35)                            !Sales Rep. Name
AllSalesReps         BYTE(1)                               !
IncludeOvernight     BYTE(1)                               !
IncludeBroking       BYTE(1)                               !
                     END                                   !
LOC:Totals_Group     GROUP,PRE(L_TG)                       !
Total_Period1        DECIMAL(20,2)                         !
Total_Period2        DECIMAL(20,2)                         !
Total_Difference     DECIMAL(20,2)                         !
Difference_Percent   DECIMAL(6,1)                          !
Clients_HigherPeriod1 ULONG                                !
Clients_HigherPeriod2 ULONG                                !
                     END                                   !
Data_Q_Graph         QUEUE,PRE(GQ)                         !
ClientNo             ULONG                                 !
ClientName           STRING(100)                           !
Period1Value         DECIMAL(14,2)                         !
Period2Value         DECIMAL(14,2)                         !
Difference           DECIMAL(14,2)                         !
CID                  ULONG                                 !
                     END                                   !
Data_Q               QUEUE,PRE(L_DQ)                       !
ClientNo             ULONG                                 !
ClientName           STRING(100)                           !
Period1Value         DECIMAL(14,2)                         !
Period2Value         DECIMAL(14,2)                         !
Difference           DECIMAL(14,2)                         !
DiffPercent          STRING(20)                            !
CID                  ULONG                                 !
                     END                                   !
LOC:Graph_Top        ULONG(10)                             !
LOC:Client_Q         QUEUE,PRE(L_CQ)                       !
Year                 ULONG                                 !
Month                ULONG                                 !
Value                DECIMAL(14,2)                         !
Value2               DECIMAL(14,2)                         !
Label1               STRING(20)                            !
Label2               STRING(20)                            !
                     END                                   !
LOC:Client_Graph_Group QUEUE,PRE(L_CGG)                    !
ClientName           STRING(120)                           !
ClientNo             ULONG                                 !Client No.
CID                  ULONG                                 !Client ID
                     END                                   !
LOC:Client_Data_Q    QUEUE,PRE(LCDG)                       !
ClientName           STRING(100)                           !
CID                  ULONG                                 !Client ID
TotalValue           DECIMAL(14,2)                         !Total since account opened
AccountOpened        DATE                                  !
AvgPerInvoice        DECIMAL(14,2)                         !
AvgPerInvoiceWeight  DECIMAL(14,2)                         !
TotalWeight          DECIMAL(14,2)                         !
AveragePerMonth      DECIMAL(14,2)                         !From opened
Last12MonthAverage   DECIMAL(14,2)                         !
Last12MonthAverageWeight DECIMAL(14)                       !
Last3MonthAvg        DECIMAL(14,2)                         !
Last3MonthAvgWeight  DECIMAL(14)                           !
                     END                                   !
FDCB12::View:FileDropCombo VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
FDCB13::View:FileDropCombo VIEW(SalesReps)
                       PROJECT(SAL:SalesRep)
                       PROJECT(SAL:SRID)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?LCCG:Branch
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?LCCG:SalesRep
SAL:SalesRep           LIKE(SAL:SalesRep)             !List box control field - type derived from field
SAL:SRID               LIKE(SAL:SRID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q1
ThisGraph1    Class(GlobalInsight)
ValidateRecord  PROCEDURE(long graphID),Long ,VIRTUAL
Reset           PROCEDURE (Byte graphForce=0),VIRTUAL
              End
ThisGraph1:Popup  Class(PopupClass)
               End
ThisGraph1:ClickedOnPointName   String(255)
ThisGraph1:ClickedOnPointNumber Real
ThisGraph1:ClickedOnSetNumber   Long
ThisGraph1:Color                Group(iColorGroupType), PRE(ThisGraph1:Color)
                                End
ThisGraph1:FileName              String(255)
!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q1
ThisGraph2    Class(GlobalInsight)
ValidateRecord  PROCEDURE(long graphID),Long ,VIRTUAL
SetPointName    PROCEDURE (Long graphSet),String ,VIRTUAL
Reset           PROCEDURE (Byte graphForce=0),VIRTUAL
              End
ThisGraph2:Popup  Class(PopupClass)
               End
ThisGraph2:ClickedOnPointName   String(255)
ThisGraph2:ClickedOnPointNumber Real
ThisGraph2:ClickedOnSetNumber   Long
ThisGraph2:Color                Group(iColorGroupType), PRE(ThisGraph2:Color)
                                End
ThisGraph2:FileName              String(255)
!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
Window               WINDOW('Clients Over Time'),AT(,,663,423),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,GRAY, |
  MDI
                       BUTTON('&Close'),AT(595,396,63,24),USE(?CancelButton),LEFT,ICON('WACancel.ico'),STD(STD:Close)
                       SHEET,AT(6,5,652,387),USE(?SHEET1)
                         TAB('Criteria'),USE(?TAB1)
                           PROMPT('From Date:'),AT(39,51),USE(?L_CG:FromDate:Prompt)
                           SPIN(@d6b),AT(84,50,60,10),USE(L_CG:FromDate),RIGHT(1)
                           BUTTON('...'),AT(149,48,12,12),USE(?Calendar)
                           PROMPT('To Date:'),AT(39,69),USE(?L_CG:ToDate:Prompt)
                           SPIN(@d6b),AT(84,70,60,10),USE(L_CG:ToDate),RIGHT(1)
                           BUTTON('...'),AT(149,68,12,12),USE(?Calendar:2)
                           PROMPT('Period 1'),AT(15,38),USE(?L_CG:FromDate:Prompt:2)
                           PROMPT('Period 2'),AT(15,120),USE(?L_CG:FromDatePeriod2:Prompt:2)
                           PROMPT('From Date:'),AT(39,134),USE(?L_CG:FromDatePeriod2:Prompt:3)
                           PROMPT('To Date:'),AT(38,155),USE(?L_CG:ToDatePeriod2:Prompt:2)
                           CHECK(' Period 2 Auto Set'),AT(15,100),USE(L_CG:Period2AutoSet),TIP('When checked this ' & |
  'will cause Period 2 to be set to the same number of days as Period 1 leading up to Period 1')
                           GROUP('group_period2'),AT(77,120,100,57),USE(?GROUP1),TRN
                             SPIN(@d17b),AT(84,134,60,10),USE(L_CG:FromDatePeriod2),RIGHT(1)
                             BUTTON('...'),AT(149,134,12,12),USE(?Calendar:3)
                             SPIN(@d17b),AT(84,155,60,10),USE(L_CG:ToDatePeriod2),RIGHT(1)
                             BUTTON('...'),AT(149,154,12,12),USE(?Calendar:4)
                           END
                           BUTTON('&Run'),AT(83,313,63,24),USE(?BUTTON_Run),LEFT,ICON('WAOk.ico')
                           GROUP('Quickset'),AT(248,38,112,108),USE(?GROUP2),BOXED
                             BUTTON('Last 30 Days'),AT(274,62,61),USE(?BUTTON_LastMonth)
                             BUTTON('Last 90 Days'),AT(274,79,61,14),USE(?BUTTON_Last3Months)
                             BUTTON('Last 180 Days'),AT(274,97,61,14),USE(?BUTTON_Last6Months)
                             BUTTON('Last 365 Days'),AT(274,115,61,14),USE(?BUTTON_Last12Months)
                           END
                           CHECK(' All Branches'),AT(83,194),USE(LCCG:AllBranches)
                           PROMPT('Branch:'),AT(15,207),USE(?PROMPT1)
                           COMBO(@s35),AT(84,208,105,10),USE(LCCG:Branch),DROP(5),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDropCombo),IMM
                           CHECK(' All Sales Reps'),AT(83,228),USE(LCCG:AllSalesReps)
                           COMBO(@s35),AT(84,242,105,10),USE(LCCG:SalesRep),DROP(5),FORMAT('140L(2)|M~Sales Rep~L(0)@s35@'), |
  FROM(Queue:FileDropCombo:1),IMM,MSG('Sales Rep. Name'),TIP('Sales Rep. Name')
                           PROMPT('Sales Rep.:'),AT(15,242),USE(?PROMPT1:2)
                           CHECK(' Include Overnight'),AT(83,263),USE(LCCG:IncludeOvernight)
                           CHECK(' Include Broking'),AT(83,277),USE(LCCG:IncludeBroking)
                         END
                         TAB('Data'),USE(?TAB2),LAYOUT(0)
                           LIST,AT(12,25,441,359),USE(?LIST_Diff),VSCROLL,FORMAT('35R(2)|M~Client No.~C(0)@s255@15' & |
  '0L(2)|M~Client Name~C(0)@s255@55R(2)|M~Period 1 Value~C(0)@n-14.2@55R(2)|M~Period 2 ' & |
  'Value~C(0)@n-14.2@55R(2)|M~Difference~C(0)@n-16.2@100L(2)|M~Diff %''s (p1 / p2)~C(2)@s255@'), |
  FROM(Data_Q)
                           LINE,AT(470,44,173,0),USE(?LINE1:2),COLOR(COLOR:Black)
                           BUTTON('Toggle Order'),AT(475,24),USE(?BUTTON_ToggleOrder),TIP('Reverse the order of the data')
                           PROMPT('Clients Higher Period 1:'),AT(484,119),USE(?L_TG:Clients_HigherPeriod1:Prompt)
                           ENTRY(@n13),AT(567,119,60,10),USE(L_TG:Clients_HigherPeriod1),RIGHT(1),READONLY,SKIP
                           PROMPT('Clients Higher Period 2:'),AT(484,134),USE(?L_TG:Clients_HigherPeriod2:Prompt)
                           ENTRY(@n13),AT(567,133,60,10),USE(L_TG:Clients_HigherPeriod2),RIGHT(1),READONLY,SKIP
                           PROMPT('Total Period 1:'),AT(484,49),USE(?L_TG:Total_Period1:Prompt)
                           ENTRY(@n-27.2),AT(567,49,60,10),USE(L_TG:Total_Period1),RIGHT(1),READONLY,SKIP
                           PROMPT('Total Period 2:'),AT(484,64),USE(?L_TG:Total_Period2:Prompt)
                           ENTRY(@n-27.2),AT(567,64,60,10),USE(L_TG:Total_Period2),RIGHT(1),READONLY,SKIP
                           PROMPT('Total Difference:'),AT(484,84),USE(?L_TG:Total_Difference:Prompt)
                           ENTRY(@n-27.2),AT(567,84,60,10),USE(L_TG:Total_Difference),RIGHT(1),READONLY,SKIP
                           PROMPT('Difference Percent:'),AT(484,99),USE(?L_TG:Difference_Percent:Prompt)
                           ENTRY(@n-9.1~ %~),AT(567,99,60,10),USE(L_TG:Difference_Percent),RIGHT(1),READONLY,SKIP,TIP('Diff. % of' & |
  ' Period 2 total')
                           LINE,AT(470,154,173,0),USE(?LINE1:3),COLOR(COLOR:Black)
                           PROMPT('Account Opened:'),AT(479,180),USE(?LCDG:AccountOpened:Prompt)
                           ENTRY(@d17),AT(575,180,60,10),USE(LCDG:AccountOpened),RIGHT(1),READONLY,SKIP
                           PROMPT('Average Per Month:'),AT(479,274),USE(?LCDG:AveragePerMonth:Prompt)
                           ENTRY(@n-19.2),AT(575,274,60,10),USE(LCDG:AveragePerMonth),RIGHT(1),MSG('From opened'),READONLY, |
  SKIP,TIP('From opened')
                           PROMPT('Last 12 Month Avg:'),AT(479,298),USE(?LCDG:Last12MonthAverage:Prompt)
                           ENTRY(@n-19.2),AT(575,298,60,10),USE(LCDG:Last12MonthAverage),RIGHT(1),READONLY,SKIP
                           PROMPT('Last 12 Month Avg Weight:'),AT(479,313),USE(?LCDG:Last12MonthAverageWeight:Prompt)
                           ENTRY(@n-19.0),AT(575,313,60,10),USE(LCDG:Last12MonthAverageWeight),RIGHT(1),READONLY,SKIP
                           PROMPT('Last 3 Month Avg:'),AT(479,328),USE(?LCDG:Last3MonthAvg:Prompt)
                           ENTRY(@n-19.2),AT(575,327,60,10),USE(LCDG:Last3MonthAvg),RIGHT(1),READONLY,SKIP
                           PROMPT('Last 3 Month Avg Weight:'),AT(479,343),USE(?LCDG:Last3MonthAvgWeight:Prompt)
                           ENTRY(@n-19.0),AT(575,342,60,10),USE(LCDG:Last3MonthAvgWeight),RIGHT(1),READONLY,SKIP
                           PROMPT('Total Value:'),AT(479,201),USE(?LCDG:TotalValue:Prompt:2)
                           ENTRY(@n-19.2),AT(575,200,60,10),USE(LCDG:TotalValue,,?LCDG:TotalValue:2),RIGHT(1),MSG('Total sinc' & |
  'e account opened'),READONLY,SKIP,TIP('Total since account opened')
                           ENTRY(@s100),AT(479,165,153,10),USE(LCDG:ClientName),READONLY,SKIP,TIP('Client name')
                           PROMPT('Avg Per Invoice:'),AT(479,216),USE(?LCDG:AvgPerInvoice:Prompt)
                           ENTRY(@n-19.2),AT(575,215,60,10),USE(LCDG:AvgPerInvoice),RIGHT(1)
                           PROMPT('Avg Per Invoice Weight:'),AT(479,252),USE(?LCDG:AvgPerInvoiceWeight:Prompt)
                           ENTRY(@n-19),AT(575,236,60,10),USE(LCDG:TotalWeight),RIGHT(1)
                           PROMPT('Total Weight:'),AT(479,237),USE(?LCDG:TotalWeight:Prompt)
                           ENTRY(@n-19.2),AT(575,251,60,10),USE(LCDG:AvgPerInvoiceWeight),RIGHT(1)
                           LINE,AT(470,367,173,0),USE(?LINE1),COLOR(COLOR:Black)
                           BUTTON('Export'),AT(475,371),USE(?BUTTON_Export)
                           BUTTON('Go to Client'),AT(595,371),USE(?BUTTON1),TIP('Go to the Client record')
                           BUTTON('Client Graph'),AT(535,371),USE(?BUTTON_ClientGraph),TIP('Show graph for selected client')
                         END
                         TAB('Graph'),USE(?TAB3)
                           REGION,AT(10,23,643,332),USE(?Insight),BEVEL(1,-1),IMM
                           PROMPT('Graph Top:'),AT(13,361),USE(?LOC:Graph_Top:Prompt)
                           SPIN(@n13),AT(56,361,60,10),USE(LOC:Graph_Top),RIGHT(1)
                           BUTTON('Refresh'),AT(123,358),USE(?BUTTON_Refresh)
                           BUTTON('Toggle Order'),AT(172,358,57,14),USE(?BUTTON_ToggleOrder:2),TIP('Reverse the or' & |
  'der of the data')
                           BUTTON('Graph All'),AT(123,374),USE(?BUTTON_Graph_All)
                           BUTTON('Graph Top High Differences'),AT(172,374,,14),USE(?BUTTON_Graph_HighLows),TIP('Top positi' & |
  've difference and top negative difference')
                           BUTTON('Graph Top Value Period 1'),AT(281,374,105,14),USE(?BUTTON_Graph_TopValuePeriod1),TIP('Top client' & |
  's by value in Period 1')
                           BUTTON('Graph Top Value Period 2'),AT(391,374,105,14),USE(?BUTTON_Graph_TopValuePeriod2),TIP('Top client' & |
  's by value in Period 2')
                           STRING('--'),AT(383,359,269),USE(?STRING_graphval),RIGHT(1)
                         END
                         TAB('Client Graph'),USE(?TAB4)
                           PROMPT('Client Name:'),AT(13,24),USE(?L_CGG:ClientName:Prompt)
                           ENTRY(@s120),AT(59,24,338,10),USE(L_CGG:ClientName),FLAT,READONLY,REQ,SKIP,TIP('Client name')
                           REGION,AT(10,37,643,316),USE(?Insight:2),BEVEL(1,-1),IMM
                           PROMPT('From Date:'),AT(10,360),USE(?LCCG:FromDate:Prompt)
                           SPIN(@d17),AT(49,359,60,10),USE(LCCG:FromDate),RIGHT(2)
                           BUTTON('...'),AT(114,357,12,12),USE(?Calendar:5)
                           PROMPT('To Date:'),AT(10,375),USE(?LCCG:ToDate:Prompt)
                           SPIN(@d17),AT(49,375,60,10),USE(LCCG:ToDate),RIGHT(2)
                           BUTTON('...'),AT(114,373,12,12),USE(?Calendar:6)
                           CHECK(' Period 2 Auto Set'),AT(143,360),USE(LCCG:Period2AutoSet)
                           GROUP('group_client_period2'),AT(224,359,164,28),USE(?GROUP_clientperiod2)
                             PROMPT('From Date Period 2:'),AT(231,360),USE(?LCCG:FromDatePeriod2:Prompt)
                             SPIN(@d17),AT(298,359,60,10),USE(LCCG:FromDatePeriod2),RIGHT(2)
                             BUTTON('...'),AT(363,358,12,12),USE(?Calendar:7)
                             PROMPT('To Date Period 2:'),AT(231,375),USE(?LCCG:ToDatePeriod2:Prompt)
                             SPIN(@d17),AT(298,375,60,10),USE(LCCG:ToDatePeriod2),RIGHT(2)
                             BUTTON('...'),AT(363,373,12,12),USE(?Calendar:8)
                           END
                           BUTTON('Refresh'),AT(398,357),USE(?BUTTON_RefreshClient)
                           BUTTON('Export Data'),AT(599,373,,14),USE(?BUTTON_ExportClient)
                           BUTTON('Default'),AT(398,373,40,14),USE(?BUTTON_Default),TIP('Default Dates to main cri' & |
  'teria dates')
                           STRING('--'),AT(478,357,175),USE(?STRING_clientgraphval),RIGHT(1)
                         END
                       END
                       STRING(''),AT(6,409,237,11),USE(?STRING3),FONT('Microsoft Sans Serif',,,FONT:regular),COLOR(00FFF8F0h)
                       STRING(''),AT(6,396,237,11),USE(?STRING2),FONT('Microsoft Sans Serif',,,FONT:regular),COLOR(00FFF8F0h)
                       STRING(''),AT(247,396,237,11),USE(?STRING1),FONT('Microsoft Sans Serif',,,FONT:regular),COLOR(00FFF8F0h)
                     END
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q2
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q2

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Calendar5            CalendarClass
Calendar6            CalendarClass
Calendar4            CalendarClass
Calendar7            CalendarClass
Calendar8            CalendarClass
Calendar9            CalendarClass
Calendar10           CalendarClass
Calendar11           CalendarClass
FDCB12               CLASS(FileDropComboClass)             ! File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB13               CLASS(FileDropComboClass)             ! File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

                    MAP
Get_Data              PROCEDURE()
Get_Data_Client       PROCEDURE(ULONG)
Get_Data_Client2      PROCEDURE(ULONG p:CID, LONG pFrom=0, LONG pTo=0),STRING

Export_Diff_Data      PROCEDURE()
Export_Client_Data    PROCEDURE(ULONG p_ClientNo)
                    .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
SetNote             ROUTINE
  DATA
r:mid   LONG
CODE
  DO Calc_Period2
  
  ! old code
  !r:mid   = L_CG:ToDate - ABS((L_CG:ToDate - L_CG:FromDate) /2)    
  !?String1{PROP:Text} = 'Mid Date: ' & FORMAT(r:mid, @d6)  
  !?String2{PROP:Text} = 'Period 1 is from: ' & FORMAT(L_CG:FromDate, @d6) & '  to  ' & FORMAT(r:mid, @d6) & '  (Days: ' & r:mid - L_CG:FromDate + 1  & ')'
  !?String3{PROP:Text} = 'Period 2 is from: ' & FORMAT(r:mid + 1, @d6) & '  to  ' & FORMAT(L_CG:ToDate, @d6) & '  (Days: ' & L_CG:ToDate - r:mid & ')'

  ?String1{PROP:Text} = ''
  
  ?String2{PROP:Text} = 'Period 1 is from: ' & FORMAT(L_CG:FromDate, @d6) & '  to  ' & FORMAT(L_CG:ToDate, @d6) & '  (Days: ' & L_CG:ToDate - L_CG:FromDate + 1  & ')'
  ?String3{PROP:Text} = 'Period 2 is from: ' & FORMAT(L_CG:FromDatePeriod2, @d6) & '  to  ' & FORMAT(L_CG:ToDatePeriod2, @d6) & '  (Days: ' & L_CG:ToDatePeriod2 - L_CG:FromDatePeriod2 + 1 & ')'
  EXIT 
  
Calc_Period2        ROUTINE
  IF L_CG:Period2AutoSet = TRUE
    L_CG:ToDatePeriod2    = L_CG:FromDate - 1
    L_CG:FromDatePeriod2  = L_CG:ToDatePeriod2 - (L_CG:ToDate - L_CG:FromDate)
    
    DISPLAY()
  .  
  EXIT
  
Calc_Group          ROUTINE
  CLEAR(LOC:Totals_Group)
  
  Idx_# = 0  
  LOOP
    Idx_# += 1
    GET(Data_Q, Idx_#)
    IF ERRORCODE()
      BREAK
    .
    
    L_TG:Total_Period1  += L_DQ:Period1Value
    L_TG:Total_Period2  += L_DQ:Period2Value
    
    IF L_DQ:Period1Value > L_DQ:Period2Value
      L_TG:Clients_HigherPeriod1 += 1
    ELSE
      L_TG:Clients_HigherPeriod2 += 1
    .
  .
  
  L_TG:Total_Difference   = L_TG:Total_Period1 - L_TG:Total_Period2
  
  L_TG:Difference_Percent = ROUND((L_TG:Total_Difference / L_TG:Total_Period2) * 100, 0.1)
  
  EXIT
  
Data_Q_Order        ROUTINE       ! toggles it
  DATA
rec1  LIKE(Data_Q.Difference)

  CODE
  GET(Data_Q,1)
  IF ~ERRORCODE()
    Rec1 = Data_Q.Difference 
    
    GET(Data_Q, RECORDS(Data_Q))
    IF ~ERRORCODE()
      IF Rec1 < Data_Q.Difference 
        SORT(Data_Q, -Data_Q.Difference)
        L_CG:Data_Q_Order   = 2     ! high to low
      ELSE
        SORT(Data_Q, +Data_Q.Difference)
        L_CG:Data_Q_Order   = 1     ! low to high
      .
    .
  .
          
  EXIT
  
! -----------------------------------
Set_G_Data          ROUTINE 
  FREE(Data_Q_Graph)
  i#  = 0
  c#  = 0
  LOOP
    i#  += 1
    GET(Data_Q,i#)
    IF ERRORCODE()
      BREAK
    .
    Data_Q_Graph  :=: Data_Q    
    ADD(Data_Q_Graph)
    c#  +=1
    IF c# >= LOC:Graph_Top
      BREAK
      
    .
  .  
  
  ThisGraph1.Reset(1)
  ThisGraph1.Draw()
  EXIT
Graph_High_Lows     ROUTINE
  ! requires ordered list, if we dont have one then order it first  
  IF L_CG:Data_Q_Order > 2 OR L_CG:Data_Q_Order = 0
    SORT(Data_Q, +Data_Q.Difference)
    L_CG:Data_Q_Order = 1
  .
  
  FREE(Data_Q_Graph)
  
  ! High records
  i#  = 0
  c#  = 0
  LOOP
    i#  += 1
    GET(Data_Q,i#)
    IF ERRORCODE()
      BREAK
    .
    Data_Q_Graph  :=: Data_Q    
    ADD(Data_Q_Graph)
    c#  +=1
    IF c# >= LOC:Graph_Top
      BREAK
    .
  .  
  
  ! Low records
  i#  = RECORDS(Data_Q) + 1
  c#  = 0
  LOOP
    i#  -= 1
    GET(Data_Q,i#)
    IF ERRORCODE()
      BREAK
    .
    Data_Q_Graph  :=: Data_Q    
    ADD(Data_Q_Graph)
    c#  +=1
    IF c# >= LOC:Graph_Top
      BREAK
    .
  .  
  
  SORT(Data_Q_Graph, Data_Q_Graph.Difference)
  
  ThisGraph1.Reset(1)
  ThisGraph1.Draw()
  EXIT
Graph_TopVal1       ROUTINE
  ! We dont want to change the order here of the Data_Q - bnecause then the toggle button wont make any sense
  ! and other operations wont make sense either as they rely on the order being a certain way... or do they
  ! But thats rubbish, lets change the order...
  SORT(Data_Q, -Data_Q.Period1Value)
  
  L_CG:Data_Q_Order = 3
  
  DO Set_G_Data
  EXIT
Graph_TopVal2       ROUTINE
  ! We dont want to change the order here of the Data_Q - bnecause then the toggle button wont make any sense
  ! and other operations wont make sense either as they rely on the order being a certain way... or do they
  ! But thats rubbish, lets change the order...
  SORT(Data_Q, -Data_Q.Period2Value)
  
  L_CG:Data_Q_Order = 4
  
  DO Set_G_Data
  EXIT
Client_Selected     ROUTINE
  DATA
str_    STRING(500)
dec_    DECIMAL(17,2)

  CODE
  CLEAR(LOC:Client_Data_Q)
  
  GET(Data_Q, CHOICE(?LIST_Diff))
  IF ERRORCODE()
    !
  ELSE
    LCDG:CID  = L_DQ:CID    
    GET(LOC:Client_Data_Q, LCDG:CID)
    IF ~ERRORCODE()      
      ! no need to load data then      
    ELSE
      CLEAR(CLI:Record)
      CLI:CID = LCDG:CID
      IF Access:Clients.TryFetch(CLI:PKey_CID) ~= Level:Benign
        ! No such client?
        MESSAGE('No such client found.  Weird?')
      ELSE
        LCDG:ClientName             = CLIP(CLI:ClientName) & ' (' & CLI:ClientNo & ')'
        LCDG:AccountOpened          = CLI:DateOpened
        
        str_                        = Get_Data_Client2(LCDG:CID)
        ! returns, total val, avg per inv, total weight, avg weight per invoice
!        MESSAGE('ret: ' & CLIP(str_))
        
        LCDG:TotalValue             = DEFORMAT(Get_1st_Element_From_Delim_Str(str_, ',', 1))
        LCDG:AvgPerInvoice          = DEFORMAT(Get_1st_Element_From_Delim_Str(str_, ',', 1))
        LCDG:TotalWeight            = DEFORMAT(Get_1st_Element_From_Delim_Str(str_, ',', 1))
        LCDG:AvgPerInvoiceWeight    = DEFORMAT(Get_1st_Element_From_Delim_Str(str_, ',', 1))
        
        LCDG:AveragePerMonth        = LCDG:TotalValue / Months_Between(LCDG:AccountOpened)
        
        str_                        = Get_Data_Client2(LCDG:CID, TODAY()-365)
!        MESSAGE('ret: ' & CLIP(str_))

        dec_                        = DEFORMAT(Get_1st_Element_From_Delim_Str(str_, ',', 1))  ! total from 12
        LCDG:Last12MonthAverage     = dec_ / 12
        dec_                        = DEFORMAT(Get_1st_Element_From_Delim_Str(str_, ',', 1))  ! avg per inv
        dec_                        = DEFORMAT(Get_1st_Element_From_Delim_Str(str_, ',', 1))  ! total weight
        LCDG:Last12MonthAverageWeight = dec_ / 12

        str_                        = Get_Data_Client2(LCDG:CID, TODAY()-90)
        ! returns, total val, avg per inv, total weight, avg weight per invoice
        dec_                        = DEFORMAT(Get_1st_Element_From_Delim_Str(str_, ',', 1))  ! total from 12
        LCDG:Last3MonthAvg          = dec_ / 3
        dec_                        = DEFORMAT(Get_1st_Element_From_Delim_Str(str_, ',', 1))  ! total from 12
        dec_                        = DEFORMAT(Get_1st_Element_From_Delim_Str(str_, ',', 1))  ! total from 12
        LCDG:Last3MonthAvgWeight    = dec_ / 3
      .  
    .
  . 
  DISPLAY()    
  EXIT
  
! -----------------------------------
Set_Client_G        ROUTINE
  GET(Data_Q, CHOICE(?LIST_Diff))
  IF ~ERRORCODE()
    CLEAR(CLI:Record)
    CLI:CID = Data_Q.CID
    IF Access:Clients.TryFetch(CLI:PKey_CID) ~= Level:Benign
      ! No such client?
      MESSAGE('No such client found.  Weird?')
    ELSE
      L_CGG:ClientName  = CLIP(CLI:ClientName) & ' (' & CLI:ClientNo & ')'
      L_CGG:ClientNo    = CLI:ClientNo
      L_CGG:CID         = CLI:CID

       IF LCCG:FromDate = 0
         LCCG:FromDate         = L_CG:FromDate
         LCCG:ToDate           = L_CG:ToDate
         LCCG:FromDatePeriod2  = L_CG:FromDatePeriod2
         LCCG:ToDatePeriod2    = L_CG:ToDatePeriod2
         LCCG:Period2AutoSet   = L_CG:Period2AutoSet
       .
       
       Get_Data_Client(Data_Q.CID)
      
       ! Draw graph
       ThisGraph2.Reset(1)
       ThisGraph2.Draw()  
       SELECT(?TAB4)
       POST(EVENT:Accepted, ?LCCG:Period2AutoSet)   ! enable / disable stuff
  . .

  EXIT
  
Export_Client_Data  ROUTINE
  Export_Client_Data(L_CGG:ClientNo)
  
  EXIT
  
Refresh_Client_Graph    ROUTINE
  Get_Data_Client(L_CGG:CID)

  ! Draw graph
  ThisGraph2.Reset(1)
  ThisGraph2.Draw()  
  EXIT
  
Calc_Period2_Client        ROUTINE
  IF LCCG:Period2AutoSet = TRUE
    LCCG:ToDatePeriod2    = LCCG:FromDate - 1
    LCCG:FromDatePeriod2  = LCCG:ToDatePeriod2 - (LCCG:ToDate - LCCG:FromDate)
    
    DISPLAY()
  .  
  EXIT
  

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Clients_OverTime')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CancelButton
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
    L_CG:FromDate         = GETINI('Clients_OverTime', 'FromDate', TODAY()-365, GLO:Global_INI)
    L_CG:ToDate           = GETINI('Clients_OverTime', 'ToDate', TODAY(), GLO:Global_INI)
  
    L_CG:FromDatePeriod2  = GETINI('Clients_OverTime', 'FromDate2', , GLO:Global_INI)
    L_CG:ToDatePeriod2    = GETINI('Clients_OverTime', 'ToDate2', , GLO:Global_INI)
    L_CG:Period2AutoSet   = GETINI('Clients_OverTime', 'Period2AutoSet', 1, GLO:Global_INI)
  
    
    ! Start Of Insight InitStuff Group
  Bind('ThisGraph1:ClickedOnPointNumber',ThisGraph1:ClickedOnPointNumber) ! Insight ClickedOnVariable
  Bind('ThisGraph1:ClickedOnPointName',ThisGraph1:ClickedOnPointName)       ! Insight ClickedOnVariable
  Bind('ThisGraph1:ClickedOnSetNumber',ThisGraph1:ClickedOnSetNumber)     ! Insight ClickedOnVariable
      ! LOC:Graph_Top - ULONG(10) -
  if ThisGraph1.Init(?Insight,Insight:Line).
  if not ThisGraph1:Color:Fetched then ThisGraph1.GetWindowsColors(ThisGraph1:Color).
  Window{prop:buffer} = 1
  ThisGraph1.LegendPosition = 2
  ThisGraph1.LegendText = 0
  ThisGraph1.LegendValue = 0
  ThisGraph1.LegendPercent = 0
  ThisGraph1.BackgroundPicture=''
  ThisGraph1.BackgroundColor=Color:Silver
  ThisGraph1.BackgroundShadeColor=Color:None
  ThisGraph1.BorderColor=Color:Black
  ThisGraph1.PaperZoom=4
  ThisGraph1.RightBorder=10
  ThisGraph1.MaxXGridTicks=16
  ThisGraph1.MaxYGridTicks=16
  ThisGraph1.AutoShade=1
  ThisGraph1.Pattern=Insight:None
  ThisGraph1.StackLines=1
  ThisGraph1.PieLabelLineColor=Color:None
  ThisGraph1.AspectRatio=1
  ThisGraph1.Shape=Insight:Auto
  ThisGraph1.LineWidth = 1
      ! Fonts
  ThisGraph1.LegendAngle   = 0
  ThisGraph1.XFont         = 'Verdana'
  ThisGraph1.XFontAngle    = 90
  ThisGraph1.ShowXLabelsEvery = 1
  ThisGraph1.DisplayOnX = 2   ! 0= don't display, 1= display number, 2= display name.
  ThisGraph1.SeparateYAxis = 0
  ThisGraph1.AutoXLabels = 0
  ThisGraph1.SpreadXLabels       = 0
  ThisGraph1.AutoXGridTicks = 1
  ThisGraph1.AutoScale = Scale:Low + Scale:High
  ThisGraph1.AutoYGridTicks = 0
  ThisGraph1.Depth = 0
          
  ThisGraph1.AddItem(1,1,Data_Q_Graph,Insight:GraphField,GQ:Difference,,,,,,GQ:ClientNo)
  ThisGraph1.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  
  ThisGraph1.SetSetType(1,Insight:None)
  If ThisGraph1.GetSet(1) = 0 then ThisGraph1.AddSetQ(1).
  ThisGraph1.SetQ.SquareWave = -1
  ThisGraph1.SetQ.Fill       = -1
  ThisGraph1.SetQ.FillToZero = -1
  ThisGraph1.SetQ.PointWidth = 66
  Put(ThisGraph1.SetQ)
          
  ThisGraph1.AddItem(2,2,Data_Q_Graph,Insight:GraphField,GQ:Period1Value,,,,,,GQ:ClientNo)
  ThisGraph1.SetSetYAxis(2,Scale:Low + Scale:High,,,)
  
  ThisGraph1.SetSetType(2,Insight:None)
  If ThisGraph1.GetSet(2) = 0 then ThisGraph1.AddSetQ(2).
  ThisGraph1.SetQ.SquareWave = -1
  ThisGraph1.SetQ.Fill       = -1
  ThisGraph1.SetQ.FillToZero = -1
  ThisGraph1.SetQ.PointWidth = 66
  Put(ThisGraph1.SetQ)
          
  ThisGraph1.AddItem(3,3,Data_Q_Graph,Insight:GraphField,GQ:Period2Value,,,,,,GQ:ClientNo)
  ThisGraph1.SetSetYAxis(3,Scale:Low + Scale:High,,,)
  
  ThisGraph1.SetSetType(3,Insight:None)
  If ThisGraph1.GetSet(3) = 0 then ThisGraph1.AddSetQ(3).
  ThisGraph1.SetQ.SquareWave = -1
  ThisGraph1.SetQ.Fill       = -1
  ThisGraph1.SetQ.FillToZero = -1
  ThisGraph1.SetQ.PointWidth = 66
  Put(ThisGraph1.SetQ)
  !Mouse
  ThisGraph1.setMouseMoveParameters(1, '')
  INIMgr.Fetch('Main','ThisGraph1.SavedGraphType',ThisGraph1.SavedGraphType)
  if ThisGraph1.SavedGraphType<>0 then ThisGraph1.Type = ThisGraph1.SavedGraphType.
  ThisGraph1:Popup.Init()
  ThisGraph1:Popup.AddItem('Zoom Out','ZoomOut','',1)
  ThisGraph1:Popup.AddItemEvent('ZoomOut',INSIGHT:ZoomOut,?Insight)
  ThisGraph1:Popup.AddItem('Zoom In','ZoomIn','',1)
  ThisGraph1:Popup.AddItemEvent('ZoomIn',INSIGHT:ZoomIn,?Insight)
  ThisGraph1:Popup.AddItem('Copy','Copy','',1)
  ThisGraph1:Popup.AddItemEvent('Copy',INSIGHT:Copy,?Insight)
  ThisGraph1:Popup.AddItem('Save As...','SaveAs','',1)
  ThisGraph1:Popup.AddItemEvent('SaveAs',INSIGHT:SaveAs,?Insight)
  ThisGraph1:Popup.AddItem('Print Graph','Print','',1)
  ThisGraph1:Popup.AddItemEvent('Print',INSIGHT:PrintGraph,?Insight)
  ThisGraph1:Popup.AddItem('Graph Type','SetGraphType','',1)
  ThisGraph1:Popup.AddItem('Line','LineType','SetGraphType',2)
  ThisGraph1:Popup.AddItem('Bar','BarType','SetGraphType',2)
  ThisGraph1:Popup.AddItem('Pareto','ParetoType','SetGraphType',2)
  ThisGraph1:Popup.AddItemEvent('LineType',INSIGHT:SelectGraphType+Insight:Line,?Insight)
  ThisGraph1:Popup.AddItemEvent('BarType',INSIGHT:SelectGraphType+Insight:Bar,?Insight)
  ThisGraph1:Popup.AddItemEvent('ParetoType',INSIGHT:SelectGraphType+Insight:Pareto,?Insight)
  ?STRING_graphval{prop:text} =  ''
    ! Start Of Insight InitStuff Group
  Bind('ThisGraph2:ClickedOnPointNumber',ThisGraph2:ClickedOnPointNumber) ! Insight ClickedOnVariable
  Bind('ThisGraph2:ClickedOnPointName',ThisGraph2:ClickedOnPointName)       ! Insight ClickedOnVariable
  Bind('ThisGraph2:ClickedOnSetNumber',ThisGraph2:ClickedOnSetNumber)     ! Insight ClickedOnVariable
      ! LOC:Graph_Top - ULONG(10) -
  if ThisGraph2.Init(?Insight:2,Insight:Line).
  if not ThisGraph2:Color:Fetched then ThisGraph2.GetWindowsColors(ThisGraph2:Color).
  Window{prop:buffer} = 1
  ThisGraph2.LegendPosition = 2
  ThisGraph2.LegendText = 0
  ThisGraph2.LegendValue = 0
  ThisGraph2.LegendPercent = 0
  ThisGraph2.BackgroundPicture=''
  ThisGraph2.BackgroundColor=Color:Silver
  ThisGraph2.BackgroundShadeColor=Color:None
  ThisGraph2.BorderColor=Color:Black
  ThisGraph2.PaperZoom=4
  ThisGraph2.RightBorder=10
  ThisGraph2.MaxXGridTicks=16
  ThisGraph2.MaxYGridTicks=16
  ThisGraph2.AutoShade=1
  ThisGraph2.Pattern=Insight:None
  ThisGraph2.StackLines=1
  ThisGraph2.PieLabelLineColor=Color:None
  ThisGraph2.AspectRatio=1
  ThisGraph2.Shape=Insight:Auto
  ThisGraph2.LineWidth = 1
      ! Fonts
  ThisGraph2.LegendAngle   = 0
  ThisGraph2.ShowXLabelsEvery = 1
  ThisGraph2.DisplayOnX = 2   ! 0= don't display, 1= display number, 2= display name.
  ThisGraph2.SeparateYAxis = 0
  ThisGraph2.AutoXLabels = 0
  ThisGraph2.SpreadXLabels       = 0
  ThisGraph2.AutoXGridTicks = 1
  ThisGraph2.AutoScale = Scale:Low + Scale:High
  ThisGraph2.AutoYGridTicks = 0
  ThisGraph2.Depth = 0
          
  ThisGraph2.AddItem(1,1,LOC:Client_Q,Insight:GraphField,L_CQ:Value,,,,,,)
  ThisGraph2.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  
  ThisGraph2.SetSetType(1,Insight:None)
  If ThisGraph2.GetSet(1) = 0 then ThisGraph2.AddSetQ(1).
  ThisGraph2.SetQ.SquareWave = -1
  ThisGraph2.SetQ.Fill       = -1
  ThisGraph2.SetQ.FillToZero = -1
  ThisGraph2.SetQ.PointWidth = 66
  Put(ThisGraph2.SetQ)
          
  ThisGraph2.AddItem(2,2,LOC:Client_Q,Insight:GraphField,L_CQ:Value2,,,,,,)
  ThisGraph2.SetSetYAxis(2,Scale:Low + Scale:High,,,)
  
  ThisGraph2.SetSetType(2,Insight:None)
  If ThisGraph2.GetSet(2) = 0 then ThisGraph2.AddSetQ(2).
  ThisGraph2.SetQ.SquareWave = -1
  ThisGraph2.SetQ.Fill       = -1
  ThisGraph2.SetQ.FillToZero = -1
  ThisGraph2.SetQ.PointWidth = 66
  Put(ThisGraph2.SetQ)
  !Mouse
  ThisGraph2.setMouseMoveParameters(1, '')
  if ThisGraph2.SavedGraphType<>0 then ThisGraph2.Type = ThisGraph2.SavedGraphType.
  ThisGraph2:Popup.Init()
  ThisGraph2:Popup.AddItem('Zoom Out','ZoomOut','',1)
  ThisGraph2:Popup.AddItemEvent('ZoomOut',INSIGHT:ZoomOut,?Insight:2)
  ThisGraph2:Popup.AddItem('Zoom In','ZoomIn','',1)
  ThisGraph2:Popup.AddItemEvent('ZoomIn',INSIGHT:ZoomIn,?Insight:2)
  ThisGraph2:Popup.AddItem('Cycle Sets','CycleSets','',1)
  ThisGraph2:Popup.AddItemEvent('CycleSets',INSIGHT:CycleSets,?Insight:2)
  ThisGraph2:Popup.AddItem('Copy','Copy','',1)
  ThisGraph2:Popup.AddItemEvent('Copy',INSIGHT:Copy,?Insight:2)
  ThisGraph2:Popup.AddItem('Save As...','SaveAs','',1)
  ThisGraph2:Popup.AddItemEvent('SaveAs',INSIGHT:SaveAs,?Insight:2)
  ThisGraph2:Popup.AddItem('Print Graph','Print','',1)
  ThisGraph2:Popup.AddItemEvent('Print',INSIGHT:PrintGraph,?Insight:2)
  ThisGraph2:Popup.AddItem('Graph Type','SetGraphType','',1)
  ThisGraph2:Popup.AddItem('Line','LineType','SetGraphType',2)
  ThisGraph2:Popup.AddItem('Bar','BarType','SetGraphType',2)
  ThisGraph2:Popup.AddItem('Pareto','ParetoType','SetGraphType',2)
  ThisGraph2:Popup.AddItemEvent('LineType',INSIGHT:SelectGraphType+Insight:Line,?Insight:2)
  ThisGraph2:Popup.AddItemEvent('BarType',INSIGHT:SelectGraphType+Insight:Bar,?Insight:2)
  ThisGraph2:Popup.AddItemEvent('ParetoType',INSIGHT:SelectGraphType+Insight:Pareto,?Insight:2)
  ?STRING_clientgraphval{prop:text} =  ''
  IF ?L_CG:Period2AutoSet{Prop:Checked}
    DISABLE(?GROUP1)
  END
  IF NOT ?L_CG:Period2AutoSet{PROP:Checked}
    ENABLE(?GROUP1)
  END
  IF ?LCCG:AllBranches{Prop:Checked}
    DISABLE(?LCCG:Branch)
  END
  IF NOT ?LCCG:AllBranches{PROP:Checked}
    ENABLE(?LCCG:Branch)
  END
  IF ?LCCG:AllSalesReps{Prop:Checked}
    DISABLE(?LCCG:SalesRep)
  END
  IF NOT ?LCCG:AllSalesReps{PROP:Checked}
    ENABLE(?LCCG:SalesRep)
  END
  IF ?LCCG:Period2AutoSet{Prop:Checked}
    DISABLE(?GROUP_clientperiod2)
  END
  IF NOT ?LCCG:Period2AutoSet{PROP:Checked}
    ENABLE(?GROUP_clientperiod2)
  END
  FDCB12.Init(LCCG:Branch,?LCCG:Branch,Queue:FileDropCombo.ViewPosition,FDCB12::View:FileDropCombo,Queue:FileDropCombo,Relate:Branches,ThisWindow,GlobalErrors,0,1,0)
  FDCB12.Q &= Queue:FileDropCombo
  FDCB12.AddSortOrder(BRA:Key_BranchName)
  FDCB12.AddField(BRA:BranchName,FDCB12.Q.BRA:BranchName) !List box control field - type derived from field
  FDCB12.AddField(BRA:BID,FDCB12.Q.BRA:BID) !Primary key field - type derived from field
  FDCB12.AddUpdateField(BRA:BID,LCCG:BID)
  ThisWindow.AddItem(FDCB12.WindowComponent)
  FDCB13.Init(LCCG:SalesRep,?LCCG:SalesRep,Queue:FileDropCombo:1.ViewPosition,FDCB13::View:FileDropCombo,Queue:FileDropCombo:1,Relate:SalesReps,ThisWindow,GlobalErrors,0,1,0)
  FDCB13.Q &= Queue:FileDropCombo:1
  FDCB13.AddSortOrder(SAL:Key_SalesRep)
  FDCB13.AddField(SAL:SalesRep,FDCB13.Q.SAL:SalesRep) !List box control field - type derived from field
  FDCB13.AddField(SAL:SRID,FDCB13.Q.SAL:SRID) !Primary key field - type derived from field
  FDCB13.AddUpdateField(SAL:SalesRep,LCCG:SalesRep)
  ThisWindow.AddItem(FDCB13.WindowComponent)
  FDCB13.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:_SQLTemp.Close
  END
    INIMgr.Update('Main','ThisGraph1.SavedGraphType',ThisGraph1.Type)
   ThisGraph1.Kill()
   ThisGraph2.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     ThisGraph1.Reset()
     Post(Event:accepted,?Insight)
     ThisGraph2.Reset()
     Post(Event:accepted,?Insight:2)


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?BUTTON_Run
      IF L_CG:FromDate > L_CG:ToDate
        MESSAGE('From date must be earlier than To date') 
        
        SELECT(?L_CG:FromDate)
        CYCLE
      .
    OF ?BUTTON_Refresh
      DO Set_G_Data          
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_CG:FromDate
      DO SetNote
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',L_CG:FromDate)
      IF Calendar5.Response = RequestCompleted THEN
      L_CG:FromDate=Calendar5.SelectedDate
      DISPLAY(?L_CG:FromDate)
      END
      ThisWindow.Reset(True)
    OF ?L_CG:ToDate
      DO SetNote
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',L_CG:ToDate)
      IF Calendar6.Response = RequestCompleted THEN
      L_CG:ToDate=Calendar6.SelectedDate
      DISPLAY(?L_CG:ToDate)
      END
      ThisWindow.Reset(True)
    OF ?L_CG:Period2AutoSet
      IF ?L_CG:Period2AutoSet{PROP:Checked}
        DISABLE(?GROUP1)
      END
      IF NOT ?L_CG:Period2AutoSet{PROP:Checked}
        ENABLE(?GROUP1)
      END
      ThisWindow.Reset()
    OF ?Calendar:3
      ThisWindow.Update()
      Calendar4.SelectOnClose = True
      Calendar4.Ask('Select a Date',L_CG:FromDatePeriod2)
      IF Calendar4.Response = RequestCompleted THEN
      L_CG:FromDatePeriod2=Calendar4.SelectedDate
      DISPLAY(?L_CG:FromDatePeriod2)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:4
      ThisWindow.Update()
      Calendar7.SelectOnClose = True
      Calendar7.Ask('Select a Date',L_CG:ToDatePeriod2)
      IF Calendar7.Response = RequestCompleted THEN
      L_CG:ToDatePeriod2=Calendar7.SelectedDate
      DISPLAY(?L_CG:ToDatePeriod2)
      END
      ThisWindow.Reset(True)
    OF ?BUTTON_Run
      ThisWindow.Update()
        PUTINI('Clients_OverTime', 'FromDate', L_CG:FromDate, GLO:Global_INI)
        PUTINI('Clients_OverTime', 'ToDate', L_CG:ToDate, GLO:Global_INI)
        PUTINI('Clients_OverTime', 'FromDate2', L_CG:FromDatePeriod2, GLO:Global_INI)
        PUTINI('Clients_OverTime', 'ToDate2', L_CG:ToDatePeriod2, GLO:Global_INI)
        PUTINI('Clients_OverTime', 'Period2AutoSet', L_CG:Period2AutoSet, GLO:Global_INI)
      
        SETCURSOR(CURSOR:Wait)
      
        Get_Data()
        DO Set_G_Data
        DO Calc_Group
      
        SETCURSOR()
        SELECT(?TAB2)
      
    OF ?BUTTON_LastMonth
      ThisWindow.Update()
      L_CG:ToDate   = TODAY() - 1
      L_CG:FromDate = L_CG:ToDate - 29
      
      DISPLAY
      
      POST(EVENT:Accepted, ?L_CG:FromDate)
      POST(EVENT:Accepted, ?BUTTON_Run)
    OF ?BUTTON_Last3Months
      ThisWindow.Update()
      L_CG:ToDate   = TODAY() - 1
      L_CG:FromDate = L_CG:ToDate - 89
      
      DISPLAY
      
      POST(EVENT:Accepted, ?L_CG:FromDate)
      POST(EVENT:Accepted, ?BUTTON_Run)
    OF ?BUTTON_Last6Months
      ThisWindow.Update()
      L_CG:ToDate   = TODAY() - 1
      L_CG:FromDate = L_CG:ToDate - 179
      
      DISPLAY
      
      POST(EVENT:Accepted, ?L_CG:FromDate)
      POST(EVENT:Accepted, ?BUTTON_Run)
    OF ?BUTTON_Last12Months
      ThisWindow.Update()
      L_CG:ToDate   = TODAY() - 1
      L_CG:FromDate = L_CG:ToDate - 364
      
      DISPLAY
      
      POST(EVENT:Accepted, ?L_CG:FromDate)
      POST(EVENT:Accepted, ?BUTTON_Run)
    OF ?LCCG:AllBranches
      IF ?LCCG:AllBranches{PROP:Checked}
        DISABLE(?LCCG:Branch)
      END
      IF NOT ?LCCG:AllBranches{PROP:Checked}
        ENABLE(?LCCG:Branch)
      END
      ThisWindow.Reset()
    OF ?LCCG:AllSalesReps
      IF ?LCCG:AllSalesReps{PROP:Checked}
        DISABLE(?LCCG:SalesRep)
      END
      IF NOT ?LCCG:AllSalesReps{PROP:Checked}
        ENABLE(?LCCG:SalesRep)
      END
      ThisWindow.Reset()
    OF ?BUTTON_ToggleOrder
      ThisWindow.Update()
        DO Data_Q_Order
    OF ?BUTTON_Export
      ThisWindow.Update()
        Export_Diff_Data()
    OF ?BUTTON1
      ThisWindow.Update()
      GET(Data_Q, CHOICE(?LIST_Diff))
      IF ~ERRORCODE()
        ! drill down to client
        CLEAR(CLI:Record)
        CLI:CID = Data_Q.CID
        IF Access:Clients.TryFetch(CLI:PKey_CID) = Level:Benign
          ! cant call update client from here as is in parent
          GlobalRequest = ChangeRecord    
          Update_Clients()
        .
      .
    OF ?BUTTON_ClientGraph
      ThisWindow.Update()
      DO Set_Client_G
    OF ?Insight
      If ((?Insight{prop:visible} or ThisGraph1.ActiveInvisible) and not 0{prop:acceptall})
        ThisGraph1.Draw()
      End
    OF ?LOC:Graph_Top
      DO Set_G_Data          
    OF ?BUTTON_ToggleOrder:2
      ThisWindow.Update()
      POST(EVENT:Accepted, ?BUTTON_ToggleOrder)
      POST(EVENT:Accepted, ?BUTTON_Refresh)
    OF ?BUTTON_Graph_All
      ThisWindow.Update()
      LOC:Graph_Top = RECORDS(Data_Q)
      DISPLAY(?LOC:Graph_Top)
      POST(EVENT:Accepted, ?BUTTON_Refresh)
    OF ?BUTTON_Graph_HighLows
      ThisWindow.Update()
      DO Graph_High_Lows
    OF ?BUTTON_Graph_TopValuePeriod1
      ThisWindow.Update()
      DO Graph_TopVal1
    OF ?BUTTON_Graph_TopValuePeriod2
      ThisWindow.Update()
      DO Graph_TopVal2
    OF ?Insight:2
      If ((?Insight:2{prop:visible} or ThisGraph2.ActiveInvisible) and not 0{prop:acceptall})
        ThisGraph2.Draw()
      End
    OF ?LCCG:FromDate
      DO Calc_Period2_Client
    OF ?Calendar:5
      ThisWindow.Update()
      Calendar8.SelectOnClose = True
      Calendar8.Ask('Select a Date',LCCG:FromDate)
      IF Calendar8.Response = RequestCompleted THEN
      LCCG:FromDate=Calendar8.SelectedDate
      DISPLAY(?LCCG:FromDate)
      END
      ThisWindow.Reset(True)
    OF ?LCCG:ToDate
      DO Calc_Period2_Client
    OF ?Calendar:6
      ThisWindow.Update()
      Calendar9.SelectOnClose = True
      Calendar9.Ask('Select a Date',LCCG:ToDate)
      IF Calendar9.Response = RequestCompleted THEN
      LCCG:ToDate=Calendar9.SelectedDate
      DISPLAY(?LCCG:ToDate)
      END
      ThisWindow.Reset(True)
    OF ?LCCG:Period2AutoSet
      IF ?LCCG:Period2AutoSet{PROP:Checked}
        DISABLE(?GROUP_clientperiod2)
      END
      IF NOT ?LCCG:Period2AutoSet{PROP:Checked}
        ENABLE(?GROUP_clientperiod2)
      END
      ThisWindow.Reset()
    OF ?Calendar:7
      ThisWindow.Update()
      Calendar10.SelectOnClose = True
      Calendar10.Ask('Select a Date',LCCG:FromDatePeriod2)
      IF Calendar10.Response = RequestCompleted THEN
      LCCG:FromDatePeriod2=Calendar10.SelectedDate
      DISPLAY(?LCCG:FromDatePeriod2)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:8
      ThisWindow.Update()
      Calendar11.SelectOnClose = True
      Calendar11.Ask('Select a Date',LCCG:ToDatePeriod2)
      IF Calendar11.Response = RequestCompleted THEN
      LCCG:ToDatePeriod2=Calendar11.SelectedDate
      DISPLAY(?LCCG:ToDatePeriod2)
      END
      ThisWindow.Reset(True)
    OF ?BUTTON_RefreshClient
      ThisWindow.Update()
      DO Refresh_Client_Graph
    OF ?BUTTON_ExportClient
      ThisWindow.Update()
      DO Export_Client_Data
    OF ?BUTTON_Default
      ThisWindow.Update()
      LCCG:FromDate         = L_CG:FromDate
      LCCG:ToDate           = L_CG:ToDate
      LCCG:FromDatePeriod2  = L_CG:FromDatePeriod2
      LCCG:ToDatePeriod2    = L_CG:ToDatePeriod2
      LCCG:Period2AutoSet   = L_CG:Period2AutoSet
      
      DISPLAY()
      
      POST(EVENT:Accepted, ?BUTTON_RefreshClient)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Insight
    CASE EVENT()
    Of INSIGHT:ResetAndDraw
      ThisGraph1.Reset()
      Post(event:accepted,?Insight)
    Of INSIGHT:PrintGraph
      ThisGraph1.PrintGraph('',255)
    Of INSIGHT:Copy
         ThisGraph1.ToClipBoard()
    Of INSIGHT:SaveAs
      if FileDialog('Save As...',ThisGraph1:FileName,'Windows Bitmap|*.bmp|Portable Network Graphics|*.png',10010011b) = 1
        ThisGraph1.SaveAs(ThisGraph1:FileName)
      End
    Of INSIGHT:ZoomIn
      ThisGraph1.Zoom(,-25,ThisGraph1:ClickedOnPointNumber)
      if ThisGraph1.zoom < 5 then ThisGraph1:Popup.SetItemEnable('ZoomIn',0).
      if ThisGraph1.zoom < 100 then ThisGraph1:Popup.SetItemEnable('ZoomOut',1).
    Of INSIGHT:ZoomOut
      ThisGraph1.Zoom(,+25,ThisGraph1:ClickedOnPointNumber)
      if ThisGraph1.zoom = 100 then ThisGraph1:Popup.SetItemEnable('ZoomOut',0).
      if ThisGraph1.zoom > 4 then ThisGraph1:Popup.SetItemEnable('ZoomIn',1).
    Of INSIGHT:SelectGraphType + Insight:Bar to INSIGHT:SelectGraphType + Insight:Gantt
      ThisGraph1.Type = event() - INSIGHT:SelectGraphType
      ThisGraph1.Draw()
    OF EVENT:MouseUp
        If ThisGraph1.GetPoint(ThisGraph1:ClickedOnSetNumber,ThisGraph1:ClickedOnPointNumber) = true
          ThisGraph1:ClickedOnPointName = ThisGraph1.GetPointName(ThisGraph1:ClickedOnSetNumber,ThisGraph1:ClickedOnPointNumber)
        End
      Case Keycode()
      Of MouseRight !Up
        ThisGraph1:Popup.Ask()
      End
    OF EVENT:MouseMove
      ?STRING_graphval{prop:text} =  ThisGraph1.GetPointSummary(0,' - ')
    END
  OF ?Insight:2
    CASE EVENT()
    Of INSIGHT:ResetAndDraw
      ThisGraph2.Reset()
      Post(event:accepted,?Insight:2)
    Of INSIGHT:PrintGraph
      ThisGraph2.PrintGraph('',255)
    Of INSIGHT:Copy
         ThisGraph2.ToClipBoard()
    Of INSIGHT:SaveAs
      if FileDialog('Save As...',ThisGraph2:FileName,'Windows Bitmap|*.bmp|Portable Network Graphics|*.png',10010011b) = 1
        ThisGraph2.SaveAs(ThisGraph2:FileName)
      End
    Of INSIGHT:ZoomIn
      ThisGraph2.Zoom(,-25,ThisGraph2:ClickedOnPointNumber)
      if ThisGraph2.zoom < 5 then ThisGraph2:Popup.SetItemEnable('ZoomIn',0).
      if ThisGraph2.zoom < 100 then ThisGraph2:Popup.SetItemEnable('ZoomOut',1).
    Of INSIGHT:ZoomOut
      ThisGraph2.Zoom(,+25,ThisGraph2:ClickedOnPointNumber)
      if ThisGraph2.zoom = 100 then ThisGraph2:Popup.SetItemEnable('ZoomOut',0).
      if ThisGraph2.zoom > 4 then ThisGraph2:Popup.SetItemEnable('ZoomIn',1).
    Of INSIGHT:CycleSets
      ThisGraph2.SwapSets(1)
      ThisGraph2.Draw()
    Of INSIGHT:SelectGraphType + Insight:Bar to INSIGHT:SelectGraphType + Insight:Gantt
      ThisGraph2.Type = event() - INSIGHT:SelectGraphType
      ThisGraph2.Draw()
    OF EVENT:MouseUp
        If ThisGraph2.GetPoint(ThisGraph2:ClickedOnSetNumber,ThisGraph2:ClickedOnPointNumber) = true
          ThisGraph2:ClickedOnPointName = ThisGraph2.GetPointName(ThisGraph2:ClickedOnSetNumber,ThisGraph2:ClickedOnPointNumber)
        End
      Case Keycode()
      Of MouseRight !Up
        ThisGraph2:Popup.Ask()
      End
    OF EVENT:MouseMove
      ?STRING_clientgraphval{prop:text} =  ThisGraph2.GetPointSummary(0,' - ')
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_CG:FromDate
      DO SetNote
    OF ?L_CG:ToDate
      DO SetNote
    OF ?LIST_Diff
      DO Client_Selected
    OF ?LOC:Graph_Top
      DO Set_G_Data          
    OF ?LCCG:FromDate
      DO Calc_Period2_Client
    OF ?LCCG:ToDate
      DO Calc_Period2_Client
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      DO SetNote
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Get_Data            PROCEDURE

_sql                  SQLQueryClass
_qry                  CSTRING(5000)
_man                  CSTRING(5000)
_manwhere             CSTRING(500)

CODE
  FREE(Data_Q)
  
  !Periods         BYTE !Years, Quarters, Months, Weeks, Days
  _sql.Init(GLO:DBOwner, '__SQLTemp2')
  !_sql.Log = 2
  
  ! Set Manifest info if needed
  _man = ''
  _manwhere = ''  
  IF LCCG:IncludeOvernight = 0 XOR LCCG:IncludeBroking = 0   ! only one
    _man = ' left outer join Manifest as m ON m.MID = _Invoice.MID'
    IF LCCG:IncludeBroking = 1
      _manwhere = ' and m.Broking = 1'
    ELSE !    IF LCCG:IncludeOvernight = 1    there is only one or the other
      _manwhere = ' and m.Broking = 0'
  . .
 
  _qry = ('SELECT c.ClientNo, c.ClientName, a.CID1, a.Tot1, b.Tot2 FROM ' & |
    ' (SELECT CID as CID1, SUM(Total) as Tot1 FROM _Invoice ' & |
      _man & |
    ' WHERE InvoiceDateAndTime >= ' & SQL_Get_DateT_G(L_CG:FromDate) & |         
      _manwhere & | 
  ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(L_CG:ToDate +1) & |    
  ' GROUP BY CID) as a' & |
  ' JOIN ' & | 
    ' (SELECT CID as CID2, SUM(Total) as Tot2 FROM _Invoice ' & |
      _man & |
    ' WHERE InvoiceDateAndTime >= ' & SQL_Get_DateT_G(L_CG:FromDatePeriod2) & | 
      _manwhere & | 
  ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(L_CG:ToDatePeriod2 +1) & |    
  ' GROUP BY CID) as b ' & |
    ' ON a.CID1 = b.CID2 ' & |
    ' inner join Clients as c on a.CID1 = c.CID' & |
    ' WHERE (1 = ' & LCCG:AllBranches & ' OR c.BID = ' & LCCG:BID & ')')
  
  IF LCCG:AllSalesReps = 0
    _qry = _qry & ' AND c.SRID = ' & LCCG:SRID
  .
  
  SETCLIPBOARD(_qry)

  _sql.PropSQL( CLIP(_qry) )    ! normal string passed
  
  IF ERRORCODE()
    !SETCLIPBOARD(CLIP(__SQLTemp{PROP:SQL}))
    MESSAGE('SQL error?  ' & ERROR() )
  ELSE 
    LOOP
      IF _sql.Next_Q() <= 0      
        BREAK 
      . 
        
      L_DQ:ClientNo       = DEFORMAT(_sql.Data_G.F1)
      L_DQ:ClientName     = _sql.Data_G.F2
      L_DQ:Period1Value   = DEFORMAT(_sql.Data_G.F4)
      L_DQ:Period2Value   = DEFORMAT(_sql.Data_G.F5)
      L_DQ:CID            = DEFORMAT(_sql.Data_G.F3)      
      L_DQ:Difference     = L_DQ:Period1Value - L_DQ:Period2Value
      IF L_DQ:Difference < 0.0
        ! higher in earlier period
      .
      L_DQ:DiffPercent      = '' & ROUND((ABS(L_DQ:Difference) / L_DQ:Period1Value) * 100, 0.1) & '  /  ' & | 
                            ROUND((ABS(L_DQ:Difference) / L_DQ:Period2Value) * 100, 0.1)      
      ADD(Data_Q)  
  .  .
    
  SORT(Data_Q, +L_DQ:Difference)    ! Descending order, lowest (highest) difference ascending
  L_CG:Data_Q_Order = 1
  RETURN
  
 
!                 Get_Data            PROCEDURE
!
!__SQLTemp        FILE,DRIVER('MSSQL'),OWNER(GLO:DBOwner),NAME('dbo.__SQLTemp'),PRE(__SQ),CREATE
!record            RECORD
!S1                  STRING(255)
!S2                  STRING(255)
!S3                  STRING(255)
!S4                  STRING(255)
!S5                  STRING(255)
!                  END
!                END
!
!!L:DayMid        LONG
!
!_sql    SQLQueryClass
!
!CODE
!  FREE(Data_Q)
!  
!  !Periods         BYTE !Years, Quarters, Months, Weeks, Days
!  ! old code
!  !L:DayMid      = L_CG:ToDate - ABS((L_CG:ToDate - L_CG:FromDate) /2)
!  _sql.Init(GLO:DBOwner, '__SQLTemp2')
!    
!  CREATE(__SQLTemp)
!  OPEN(__SQLTemp)  
!  
!  CLEAR(__SQ:Record)
!  __SQLTemp{PROP:SQL}
!  _sql.PropSQL = 'SELECT c.ClientNo, c.ClientName, a.CID1, a.Tot1, b.Tot2 FROM ' & |
!    '(SELECT CID as CID1, SUM(Total) as Tot1 FROM _Invoice WHERE InvoiceDateAndTime >= ' & | 
!    SQL_Get_DateT_G(L_CG:FromDate) & |         
!  ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(L_CG:ToDate +1) & |    
!  ' GROUP BY CID) as a' & |
!  ' JOIN ' & | 
!   '(SELECT CID as CID2, SUM(Total) as Tot2 FROM _Invoice WHERE InvoiceDateAndTime >= ' & SQL_Get_DateT_G(L_CG:FromDatePeriod2) & | 
!  ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(L_CG:ToDatePeriod2 +1) & |    
!  ' GROUP BY CID) as b ' & |
!    'ON a.CID1 = b.CID2 ' & |
!    'join Clients as c on a.CID1 = c.CID'
!  
!  !  SQL_Get_DateT_G(L_CG:FromDate) & |         
!  !' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(L:DayMid+1) & |    
!  ! '(SELECT CID as CID2, SUM(Total) as Tot2 FROM _Invoice WHERE InvoiceDateAndTime >= ' & SQL_Get_DateT_G(L:DayMid+1) & | 
!  !' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(L_CG:ToDate+1) & |    
!
!  ! SETCLIPBOARD(CLIP(__SQLTemp{PROP:SQL}))
!  
!  IF ERRORCODE()
!    !SETCLIPBOARD(CLIP(__SQLTemp{PROP:SQL}))
!    MESSAGE('SQL error?  ' & ERROR() & '||SQL: ' & CLIP(__SQLTemp{PROP:SQL}))
!  ELSE 
!    LOOP
!      NEXT(__SQLTemp)
!      IF ERRORCODE()
!        BREAK 
!      . 
!        
!      L_DQ:ClientNo       = DEFORMAT(__SQ:S1)
!      L_DQ:ClientName     = __SQ:S2
!      L_DQ:Period1Value   = DEFORMAT(__SQ:S4)
!      L_DQ:Period2Value   = DEFORMAT(__SQ:S5)
!      L_DQ:CID            = DEFORMAT(__SQ:S3)      
!      L_DQ:Difference     = L_DQ:Period1Value - L_DQ:Period2Value
!      IF L_DQ:Difference < 0.0
!        ! higher in earlier period
!      .
!      L_DQ:DiffPercent      = '' & ROUND((ABS(L_DQ:Difference) / L_DQ:Period1Value) * 100, 0.1) & '  /  ' & | 
!                            ROUND((ABS(L_DQ:Difference) / L_DQ:Period2Value) * 100, 0.1)      
!      ADD(Data_Q)  
!  .  .
!    
!  CLOSE(__SQLTemp)
!    
!  SORT(Data_Q, +L_DQ:Difference)    ! Descending order, lowest (highest) difference ascending
!  RETURN
!  
! 
Get_Data_Client            PROCEDURE(ULONG p:CID)

_sql                          SQLQueryClass

mth                           LONG
yer                           LONG
idx                           LONG

CODE
  FREE(LOC:Client_Q)

  _sql.Init(GLO:DBOwner, '__SQLTemp2')
  _sql.PropSQL('SELECT DatePart(Year,i.INVOICEDATEANDTIME), DatePart(Month,i.INVOICEDATEANDTIME), SUM(i.total) as Value' & |
    ' from _Invoice as i' & |
   ' where i.INVOICEDATEANDTIME >= ' & SQL_Get_DateT_G(LCCG:FromDate) &  |
    '	and i.INVOICEDATEANDTIME < ' &  SQL_Get_DateT_G(LCCG:ToDate + 1) & |
    ' and i.CID = ' & p:CID & |
    ' group by DatePart(Year,i.INVOICEDATEANDTIME), DatePart(Month,i.INVOICEDATEANDTIME)' & |
    ' order by DatePart(Year,i.INVOICEDATEANDTIME) asc, DatePart(Month,i.INVOICEDATEANDTIME) asc')
  
  IF ERRORCODE()
    MESSAGE('SQL error?  ' & ERROR())
  ELSE 
    LOOP
      IF _sql.Next_Q() <= 0
        BREAK 
      . 
      CLEAR(LOC:Client_Q)
        
      L_CQ:Year       = DEFORMAT(_sql.Data_G.F1)
      L_CQ:Month      = DEFORMAT(_sql.Data_G.F2)
      L_CQ:Value      = DEFORMAT(_sql.Data_G.F3)      
      
      L_CQ:Label1     = L_CQ:Month & '-' & SUB(CLIP(LEFT(L_CQ:Year)),3,2)   ! only 2 digits for year
      ADD(LOC:Client_Q)  
    .
  .  
  
      
  ! Check that all the months have an entry... otherwise it wont look good
  L_CQ:Year   = YEAR(LCCG:FromDate)
  L_CQ:Month  = MONTH(LCCG:FromDate)  
  LOOP
    GET(LOC:Client_Q, L_CQ:Year, L_CQ:Month)
    IF ERRORCODE()
      L_CQ:Label1     = L_CQ:Month & '-' & SUB(CLIP(LEFT(L_CQ:Year)),3,2)   ! only 2 digits for year
      L_CQ:Value      = 0.0      
      ADD(LOC:Client_Q)        
    .
    L_CQ:Month += 1
    IF L_CQ:Month > 12
      L_CQ:Month  = 1
      L_CQ:Year  += 1      
    .
    
    IF DATE(L_CQ:Month, 1, L_CQ:Year) > LCCG:ToDate
      BREAK
  . .    
  
  
  ! Now set 2...
  _sql.PropSQL('SELECT DatePart(Year,i.INVOICEDATEANDTIME), DatePart(Month,i.INVOICEDATEANDTIME), SUM(i.total) as Value' & |
    ' from _Invoice as i' & |
   ' where i.INVOICEDATEANDTIME >= ' & SQL_Get_DateT_G(LCCG:FromDatePeriod2) &  |
    '	and i.INVOICEDATEANDTIME < ' &  SQL_Get_DateT_G(LCCG:ToDatePeriod2 + 1) & |
    ' and i.CID = ' & p:CID & |
    ' group by DatePart(Year,i.INVOICEDATEANDTIME), DatePart(Month,i.INVOICEDATEANDTIME)' & |
    ' order by DatePart(Year,i.INVOICEDATEANDTIME) asc, DatePart(Month,i.INVOICEDATEANDTIME) asc')
  
  mth = MONTH(LCCG:FromDatePeriod2)
  yer = YEAR(LCCG:FromDatePeriod2)
  idx = 0
  
  IF ERRORCODE()    
    MESSAGE('SQL error?  ' & ERROR())
  ELSE 
    LOOP
      IF _sql.Next_Q() <= 0
        BREAK 
      . 
      
      idx += 1
      L_CQ:Year       = DEFORMAT(_sql.Data_G.F1)      ! Using just as temp vars!!! see Q get later
      L_CQ:Month      = DEFORMAT(_sql.Data_G.F2)
      
      ! then we need to full with zeros.. or actually just increment idx
      LOOP         
        IF mth = L_CQ:Month AND yer = L_CQ:Year
          BREAK
        .
        idx += 1
        
        mth += 1
        IF mth > 12
          mth = 1
          yer += 1
        .
      .
      
      ! here we should have the matched month... if periods are equal anyway!
      GET(LOC:Client_Q, idx)              
      L_CQ:Value2     = DEFORMAT(_sql.Data_G.F3)      
      L_CQ:Label2     = DEFORMAT(_sql.Data_G.F2) & '-' & SUB(CLIP(LEFT(DEFORMAT(_sql.Data_G.F1))),3,2)
      PUT(LOC:Client_Q)  
      
      mth += 1
      IF mth > 12
        mth = 1
        yer += 1
      .
    .
  .  
  RETURN
  
Get_Data_Client2           PROCEDURE(ULONG p:CID, LONG pFrom=0, LONG pTo=0)   !,STRING

_sql                          SQLQueryClass

Tot_                          DECIMAL(14,2)
Avg_                          DECIMAL(14,2)
Wei_                          DECIMAL(14,2)
Wei_a                         DECIMAL(14,2)

qry_                          STRING(1000)

CODE
  _sql.Init(GLO:DBOwner, '__SQLTemp2')
  
  qry_ = 'SELECT SUM(i.total) as Value, AVG(i.total) as Avg_, SUM(i.weight) as Wei_, AVG(i.weight) as Wei_a' & |
          ' from _Invoice as i' & |
          ' where i.CID = ' & p:CID
  IF pFrom <> 0
    qry_ = CLIP(qry_) & ' and i.INVOICEDATEANDTIME >= ' & SQL_Get_DateT_G(pFrom)
  .
  IF pTo <> 0
    qry_ = CLIP(qry_) & '	and i.INVOICEDATEANDTIME < ' &  SQL_Get_DateT_G(pTo + 1)
  .
    
  _sql.PropSQL( CLIP(qry_) )
  IF ERRORCODE()
    MESSAGE('SQL error?  ' & ERROR())
  ELSE 
    LOOP
      IF _sql.Next_Q() <= 0
        BREAK 
      . 
      Tot_      = DEFORMAT(_sql.Data_G.F1)
      Avg_      = DEFORMAT(_sql.Data_G.F2) 
      Wei_      = DEFORMAT(_sql.Data_G.F3) 
      Wei_a     = DEFORMAT(_sql.Data_G.F4) 
        
      BREAK           
    .
  .  
  
!  mESSAGE('tot_: ' & tot_ & '||_sql.Data_G.F1: ' & CLIP(_sql.Data_G.F1))
    
  
  RETURN(Tot_ & ',' & Avg_ & ',' & Wei_ & ',' & Wei_a)
  
!                     Get_Data_Client            PROCEDURE(ULONG p:CID)
!
!__SQLTemp        FILE,DRIVER('MSSQL'),OWNER(GLO:DBOwner),NAME('dbo.__SQLTemp'),PRE(__SQ),CREATE
!record            RECORD
!S1                  STRING(255)
!S2                  STRING(255)
!S3                  STRING(255)
!S4                  STRING(255)
!S5                  STRING(255)
!                  END
!                END
!
!
!L:DayMid        LONG
!CODE
!  FREE(LOC:Client_Q)
!  
!  CREATE(__SQLTemp)
!  OPEN(__SQLTemp)  
!  
!  CLEAR(__SQ:Record)
!  __SQLTemp{PROP:SQL}  = 'SELECT DatePart(Year,i.INVOICEDATEANDTIME), DatePart(Month,i.INVOICEDATEANDTIME), SUM(i.total) as Value' & |
!    ' from _Invoice as i' & |
!   ' where i.INVOICEDATEANDTIME >= ' & SQL_Get_DateT_G(L_CG:FromDate) &  |
!    '	and i.INVOICEDATEANDTIME < ' &  SQL_Get_DateT_G(L_CG:ToDate + 1) & |
!    ' and i.CID = ' & p:CID & |
!    ' group by DatePart(Year,i.INVOICEDATEANDTIME), DatePart(Month,i.INVOICEDATEANDTIME)' & |
!    ' order by DatePart(Year,i.INVOICEDATEANDTIME) asc, DatePart(Month,i.INVOICEDATEANDTIME) asc'
!  
!  ! SETCLIPBOARD(CLIP(__SQLTemp{PROP:SQL}))
!  
!  IF ERRORCODE()
!    SETCLIPBOARD(CLIP(__SQLTemp{PROP:SQL}))
!    MESSAGE('SQL error?  ' & ERROR() & '||SQL: ' & CLIP(__SQLTemp{PROP:SQL}))
!  ELSE 
!    LOOP
!      NEXT(__SQLTemp)
!      IF ERRORCODE()
!        BREAK 
!      . 
!        
!      L_CQ:Year       = DEFORMAT(__SQ:S1)    
!      L_CQ:Month      = DEFORMAT(__SQ:S2)
!      L_CQ:Value      = DEFORMAT(__SQ:S3)
!      
!      ADD(LOC:Client_Q)  
!    .
!  .
!    
!  CLOSE(__SQLTemp)      
!
!    
!  ! Check that all the months have an entry... otherwise it wont look good
!  L_CQ:Year   = YEAR(L_CG:FromDate)
!  L_CQ:Month  = MONTH(L_CG:FromDate)  
!  LOOP
!    GET(LOC:Client_Q, L_CQ:Year, L_CQ:Month)
!    IF ERRORCODE()
!      L_CQ:Value      = 0.0      
!      ADD(LOC:Client_Q)        
!    .
!    L_CQ:Month += 1
!    IF L_CQ:Month > 12
!      L_CQ:Month  = 1
!      L_CQ:Year  += 1      
!    .
!    
!    IF DATE(L_CQ:Month, 1, L_CQ:Year) > L_CG:ToDate
!      BREAK
!  . .    
!  RETURN
!  
Export_Diff_Data    PROCEDURE()

p_fname               STRING(255)

pfi        FILE,DRIVER('ASCII'),OWNER(GLO:DBOwner),NAME(''),PRE(pf),CREATE
record            RECORD
S1                  STRING(2550)
                  END
                END

  CODE
  p_fname = 'ClientsOverTime.csv'
  FILEDIALOG('Export file to save', p_fname, '*.csv', 3)
  IF CLIP(p_fname) = ''
    !
  ELSE
    pfi{PROP:Name} = CLIP(p_fname)
    CREATE(pfi)
    OPEN(pfi)
    IF ERRORCODE()
      MESSAGE('Error on open file: ' & CLIP(p_fname))
    ELSE
      pf:s1 = ',' & ?STRING2{PROP:Text}
      ADD(pfi)
      pf:s1 = ',' & ?STRING3{PROP:Text}
      ADD(pfi)
      pf:s1 = 'Client No., Client Name, Period 1 Value, Period 2 Value, Difference, Diff % (p1 / p2)'
      ADD(pfi)
      
      ! now add data
      i_# = 0
      LOOP
        i_# += 1
        GET(Data_Q, i_#)
        IF ERRORCODE()
          BREAK
        .
        CLEAR(pf:Record)
        
        Add_to_List('"' & CLIP(Data_Q.ClientNo) & '"', pf:s1, ',')
        Add_to_List('"' & CLIP(Data_Q.ClientName) & '"', pf:s1, ',')
        Add_to_List('"' & FORMAT(Data_Q.Period1Value, @n-15.2) & '"', pf:s1, ',')
        Add_to_List('"' & FORMAT(Data_Q.Period2Value, @n-15.2) & '"', pf:s1, ',')
        Add_to_List('"' & FORMAT(Data_Q.Difference, @n-15.2) & '"', pf:s1, ',')
        Add_to_List('"<39>' & CLIP(Data_Q.DiffPercent) & '"', pf:s1, ',')
        ADD(pfi)      
      .
      
      CLOSE(pfi)
    .
  .
  RETURN
  
Export_Client_Data    PROCEDURE(ULONG p_ClientNo)

p_fname               STRING(255)

pfi        FILE,DRIVER('ASCII'),OWNER(GLO:DBOwner),NAME(''),PRE(pf),CREATE
record            RECORD
S1                  STRING(2550)
                  END
                END

  CODE
  p_fname = 'ClientsOverTime-Client-' & p_ClientNo & '.csv'
  FILEDIALOG('Export file to save', p_fname, '*.csv', 3)
  IF CLIP(p_fname) = ''
    !
  ELSE
    pfi{PROP:Name} = CLIP(p_fname)
    CREATE(pfi)
    OPEN(pfi)
    IF ERRORCODE()
      MESSAGE('Error on open file: ' & CLIP(p_fname))
    ELSE
      pf:s1 = 'Client: ' & CLIP(L_CGG:ClientName)
      ADD(pfi)
      pf:s1 = 'Data Period 1: ' & FORMAT(LCCG:FromDate,@d6b) & ' - ' & FORMAT(LCCG:ToDate,@d6b)
      ADD(pfi)
      pf:s1 = 'Data Period 2: ' & FORMAT(LCCG:FromDatePeriod2,@d6b) & ' - ' & FORMAT(LCCG:ToDatePeriod2,@d6b)
      ADD(pfi)
      pf:s1 = 'Period 1, Period 1 Value, Period 2, Period 2 Value'
      ADD(pfi)
      
      ! now add data
      i_# = 0
      LOOP
        i_# += 1
        GET(LOC:Client_Q, i_#)
        IF ERRORCODE()
          BREAK
        .
        CLEAR(pf:Record)
        
        Add_to_List('"' & CLIP(L_CQ:Label1) & '"', pf:s1, ',')
        Add_to_List('"' & FORMAT(L_CQ:Value, @n-15.2) & '"', pf:s1, ',')
        Add_to_List('"' & CLIP(L_CQ:Label2) & '"', pf:s1, ',')
        Add_to_List('"' & FORMAT(L_CQ:Value2, @n-15.2) & '"', pf:s1, ',')
!        Add_to_List('"' & FORMAT(Data_Q.Difference, @n15.2) & '"', pf:s1, ',')
!        Add_to_List('"<39>' & CLIP(Data_Q.DiffPercent) & '"', pf:s1, ',')
        ADD(pfi)      
      .
      
      CLOSE(pfi)
    .
  .
  RETURN
  
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph1.ValidateRecord  PROCEDURE (long graphID)
ReturnValue  Long  ! set to > 0 to reject record
  Code
  ReturnValue = Parent.ValidateRecord(GraphID)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph1.Reset     PROCEDURE (Byte graphForce=0)
  Code
  self.HeaderName = 'Comparison'
  self.XAxisName = 'Client No.'
      
  Self.SetSetDescription(1,'Difference')
  Self.SetSetDataLabels(1,0,'', 1,0)
  ThisGraph1.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  
      
  Self.SetSetDescription(2,'Period 1')
  Self.SetSetDataLabels(2,0,'', 1,0)
  ThisGraph1.SetSetYAxis(2,Scale:Low + Scale:High,,,)
  
      
  Self.SetSetDescription(3,'Period 2')
  Self.SetSetDataLabels(3,0,'', 1,0)
  ThisGraph1.SetSetYAxis(3,Scale:Low + Scale:High,,,)
  
  Parent.Reset(graphForce)
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph2.ValidateRecord  PROCEDURE (long graphID)
ReturnValue  Long  ! set to > 0 to reject record
  Code
  ReturnValue = Parent.ValidateRecord(GraphID)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph2.SetPointName     PROCEDURE(Long graphID)
ReturnValue  String(255)
  Code
  ReturnValue = CLIP(L_CQ:Label1) & '/' & CLIP(L_CQ:Label2)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph2.Reset     PROCEDURE (Byte graphForce=0)
  Code
  self.HeaderName = 'Client over Time'
  self.XAxisName = 'Months'
      
  Self.SetSetDescription(1,'Client Period 1')
  Self.SetSetDataLabels(1,0,'', 1,0)
  ThisGraph2.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  
      
  Self.SetSetDescription(2,'Client Period 2')
  Self.SetSetDataLabels(2,0,'', 1,0)
  ThisGraph2.SetSetYAxis(2,Scale:Low + Scale:High,,,)
  
  Parent.Reset(graphForce)
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
!!! <summary>
!!! Generated from procedure template - Window
!!! came from Manifest_Emails
!!! </summary>
Delivery_Tracking_Emails_Setup PROCEDURE 

LOC:Locals_Group     GROUP,PRE(L_LG)                       !
EmailTypeNotes       STRING(1024)                          !User notes
LastControl          LONG                                  !
                     END                                   !
DisplayString        STRING(255)                           !
LOC_MLID             ULONG                                 !
CurrentTab           LONG                                  !
LOC_Email_Group      GROUP,PRE(LO_EG)                      !
Email_Address        STRING(255)                           !
Content              STRING(5000)                          !
Substitution         STRING(30)                            !
Email_Type           STRING('''n Route'' {26}')            !
Prev_Email_Type      STRING(35)                            !
                     END                                   !
LOC_Loaded_Email_Group GROUP,PRE(L_EG)                     !
Subject              STRING(255)                           !
Content              STRING(5000)                          !
                     END                                   !
LOC_EmailSend_Group  GROUP,PRE(L_ESG)                      !
From                 STRING(255)                           !
Subject              STRING(255)                           !
CC                   STRING(255)                           !
BCC                  STRING(255)                           !
Server               STRING(255)                           !
Message              STRING(5000)                          !
HTML                 STRING(5000)                          !
SMTP_Username        STRING(255)                           !
SMTP_Password        STRING(255)                           !
Port                 ULONG(25)                             !
                     END                                   !
LO_SG                GROUP,PRE(LO_SG)                      !
DoNotGenerate        BYTE                                  !
                     END                                   !
QuickWindow          WINDOW('Delivery Tracking Emails'),AT(,,523,349),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,IMM,MDI,HLP('Manifest_Emails')
                       BUTTON('&Close'),AT(469,6,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       STRING('String2'),AT(5,6,377,14),USE(?STRING2),FONT('Microsoft Sans Serif',10,,FONT:bold)
                       SHEET,AT(5,24,513,323),USE(?SHEET1)
                         TAB('Content'),USE(?TAB_Content)
                           PROMPT('Email Type:'),AT(13,48),USE(?LO_EG:Email_Type:Prompt)
                           LIST,AT(61,48,93,10),USE(LO_EG:Email_Type,,?LO_EG:Email_Type:2),DROP(5),FROM('On Route|#' & |
  'On Route|Transferred|#Transferred|Out On Delivery|#Out On Delivery|Delivered|#Delive' & |
  'red|Manifest General|#Manifest General')
                           PROMPT('Subject (you can edit this):'),AT(17,85),USE(?L_ESG:Subject:Prompt)
                           ENTRY(@s255),AT(18,99,488,10),USE(L_ESG:Subject)
                           PROMPT('The content of the email (you can edit this):'),AT(17,114),USE(?LO_EG:Content:Prompt)
                           TEXT,AT(18,128,488,192),USE(LO_EG:Content),VSCROLL
                           CHECK(' Do Not Generate'),AT(432,84),USE(LO_SG:DoNotGenerate),TIP('Do not generate Noti' & |
  'fications for this Type / Stage')
                           BUTTON('Save Content'),AT(13,329,81,14),USE(?BUTTON_SaveContent),TIP('Save your email setup')
                           BUTTON('Load Saved Content'),AT(99,329),USE(?BUTTON_LoadContent),TIP('Load the last sav' & |
  'ed version of your email (changes in current version above will be lost)')
                           BUTTON('Copy to Clip.'),AT(184,329,81,14),USE(?BUTTON_CopyClip),TIP('Copy content to cl' & |
  'ipboard (can also use CTRL-C, CTRL-V')
                           PROMPT('Substitution:'),AT(323,332),USE(?LO_EG:Substitution:Prompt:2)
                           LIST,AT(366,332,108,10),USE(LO_EG:Substitution,,?LO_EG:Substitution:2),VSCROLL,DROP(25,120), |
  FORMAT('20L(2)'),FROM('Client Name|#Client Name|Collection Add|#Collection Add|Delive' & |
  'ry Add|#Delivery Add|Client Ref|#Client Ref|Commodity|#Commodity|Items|#Items|Weight' & |
  '|#Weight|Special Remarks|#Special Remarks|ETA|#ETA|Branch|#Branch|Dispatch Vehicle|#' & |
  'Dispatch Vehicle|Delivery Date/Time|#Delivery Date/Time|Branch Tel.|#Branch Tel.|DI ' & |
  'No.|#DI No.|Packaging|#Packaging')
                           BUTTON('&Add'),AT(479,329),USE(?BUTTON_Add),FONT('Microsoft Sans Serif',,,FONT:regular)
                           TEXT,AT(170,43,340,31),USE(L_LG:EmailTypeNotes),FLAT,MSG('User notes'),READONLY,SKIP
                           SHEET,AT(11,64,503,263),USE(?SHEET_ManType)
                             TAB('Overnight'),USE(?TAB_Overnight)
                             END
                             TAB('Broking'),USE(?TAB_Broking)
                             END
                             TAB('Local'),USE(?TAB_Local)
                             END
                           END
                         END
                         TAB('Email Server && Account'),USE(?TAB_Summary)
                           PROMPT('Email ready to send.  It will be sent to X addresses.'),AT(133,2,373,18),USE(?PROMPT_Summary), |
  FONT('Microsoft Sans Serif',,,FONT:regular),HIDE
                           BUTTON('&Save'),AT(437,288,69,25),USE(?Ok:2),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation'),TRN
                           ENTRY(@s255),AT(210,106,183,10),USE(L_ESG:From)
                           PROMPT('From:'),AT(149,105,18),USE(?L_ESG:From:Prompt)
                           ENTRY(@s255),AT(210,130,183,10),USE(L_ESG:CC)
                           PROMPT('CC:'),AT(149,129,12),USE(?L_ESG:CC:Prompt)
                           ENTRY(@s255),AT(210,148,183,10),USE(L_ESG:BCC)
                           PROMPT('BCC:'),AT(149,147,17),USE(?L_ESG:BCC:Prompt)
                           ENTRY(@s255),AT(210,175,183,10),USE(L_ESG:Server)
                           PROMPT('Server:'),AT(149,174,23),USE(?L_ESG:Server:Prompt)
                           PROMPT('Port:'),AT(149,195),USE(?L_ESG:Port:Prompt)
                           ENTRY(@n_10),AT(210,196,60,10),USE(L_ESG:Port),RIGHT(1)
                           ENTRY(@s255),AT(210,222,183,10),USE(L_ESG:SMTP_Username)
                           PROMPT('SMTP Username:'),AT(149,222,57),USE(?L_ESG:SMTP_Username:Prompt)
                           ENTRY(@s255),AT(210,239,183,10),USE(L_ESG:SMTP_Password),PASSWORD
                           PROMPT('SMTP Password:'),AT(149,239,55),USE(?L_ESG:SMTP_Password:Prompt)
                           PANEL,AT(131,55,280,212),USE(?PANEL2),BEVEL(1,-1)
                           PROMPT('Email Server and other details'),AT(149,60,245,26),USE(?PROMPT2),FONT('Microsoft ' & |
  'Sans Serif',,,FONT:bold)
                           BUTTON('&Sent Test Email'),AT(323,288,95,25),USE(?SendTestEmail),LEFT,ICON(ICON:Connect),FLAT, |
  MSG('Accept operation'),TIP('Accept Operation'),TRN
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Local Data Classes
ThisNetEmail         CLASS(NetEmailSend)                   ! Generated by NetTalk Extension (Class Definition)

                     END

                    MAP
!Load_Content_File     PROCEDURE(STRING),STRING
!Save_Content_File     PROCEDURE(STRING)

Merge_Content         PROCEDURE(ULONG p:DID, STRING p:Content),STRING
                    .
ManClass             CLASS
! TODO
! Implement a class to manage the changing of the drop down (the manifest stage)
! and the changing of the tab (the manifest type)

Stage                   BYTE
Type                    BYTE

Subject                 LIKE(NOT:Subject)
Template                LIKE(NOT:Template)
DoNotGenerate           LIKE(NOT:DoNotGenerate)

StageChange             PROCEDURE(STRING newStage),BYTE

TypeChange              PROCEDURE(BYTE newType),BYTE

Change                  PROCEDURE(BYTE newStage, BYTE newType, BYTE LoadOnly=0),BYTE

CheckChanged            PROCEDURE(STRING Subject, STRING Template, BYTE DoNotGenerate),BYTE

Load                    PROCEDURE(BYTE Stage=0, BYTE p_Type=0),BYTE
Save                    PROCEDURE(<STRING Subject>, <STRING Template>, <BYTE DoNotGenerate>),BYTE
                     .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

Save_Stuff                       ROUTINE
  DATA
r:name_   STRING(6)

  CODE

  ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip)
  ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>),STRING
  !   1       2       3           4           5

  ! ThisEmail.Server         = Get_Section_from_Settings(p_EmailAccount, 'Server', 'BC Domain Register SMTP Server')
  r:name_ = 'Email-'

  stuff_"         = Get_Setting(r:name_ & 'Server', 2, L_ESG:Server,, 'FBN Mailserver')
  stuff_"         = Get_Setting(r:name_ & 'Port', 2, L_ESG:Port,, 'FBN Mailserver Port')

  stuff_" = Get_Setting(r:name_ & 'Account', 2, L_ESG:SMTP_Username, 'Email Account')
  stuff_" = Get_Setting(r:name_ & 'Password', 2, L_ESG:SMTP_Password, 'Email Account Password')
  
  stuff_"           = Get_Setting(r:name_ & 'From', 2, L_ESG:From, 'From Email address')
 !       ThisEmail.ToList         = Get_Setting(r:name_ & 'To', 'BC Domain Register Co ZA address')
  stuff_"         = Get_Setting(r:name_ & 'CC', 2, L_ESG:CC, 'CC Email address')
  stuff_" = Get_Setting(r:name_ & 'BCC', 2, L_ESG:BCC, 'BCC Email address')

  EXIT

  
Load_Stuff                    ROUTINE
  DATA
r:name_   STRING(6)

  CODE

  ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip)
  ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>),STRING
  !   1       2       3           4           5

  ! ThisEmail.Server         = Get_Section_from_Settings(p_EmailAccount, 'Server', 'BC Domain Register SMTP Server')
  r:name_ = 'Email-'


  L_ESG:Server        = Get_Setting(r:name_ & 'Server', 1, 'mail.fbn-transport.co.za',, 'FBN Mailserver')
  L_ESG:Port        = Get_Setting(r:name_ & 'Port', 1, '25',, 'FBN Mailserver Port')

  L_ESG:SMTP_Username = Get_Setting(r:name_ & 'Account', 1, 'operations@fbn-transport.co.za', 'Email Account')
  L_ESG:SMTP_Password = Get_Setting(r:name_ & 'Password', 1, '', 'Email Account Password')
  
  L_ESG:From          = Get_Setting(r:name_ & 'From', 1, 'operations@fbn-transport.co.za', 'From Email address')
 !       ThisEmail.ToList         = Get_Setting(r:name_ & 'To', 'BC Domain Register Co ZA address')
  L_ESG:CC            = Get_Setting(r:name_ & 'CC', 1,'', 'CC Email address')
  L_ESG:BCC           = Get_Setting(r:name_ & 'BCC', 1,'', 'BCC Email address')
  EXIT
!--------------------------------------------
Content_Add_Subs     ROUTINE             ! add place holder string to content
   CASE L_LG:LastControl
   OF ?LO_EG:Content
!      message('content')
      in# = ?LO_EG:Content{PROP:Selected}   ! position to insert at
      
!      db.Debugout('LO_EG:Content: ' & LO_EG:Content)
      
      LO_EG:Content   = SUB(LO_EG:Content,1,in#-1) & |
                          '[' & CLIP(LO_EG:Substitution) & ']'  & |
                          SUB(LO_EG:Content, in#, LEN(CLIP(LO_EG:Content)))
!      db.Debugout('LO_EG:Content 2: ' & LO_EG:Content)
      DISPLAY(?LO_EG:Content)
      
      !?LO_EG:Content{PROP:SelStart} = in#  
      !SELECT(?LO_EG:Content, in#, in# + LEN(CLIP(LO_EG:Substitution)) + 2)
      SELECT(?LO_EG:Content, in# + LEN(CLIP(LO_EG:Substitution)) + 3)
   OF ?L_ESG:Subject
!      message('subject')
      in# = ?L_ESG:Subject{PROP:Selected}   ! position to insert at
      
      L_ESG:Subject   = SUB(L_ESG:Subject,1,in#) & |
                          '[' & CLIP(LO_EG:Substitution) & ']'  & |
                          SUB(L_ESG:Subject, in#, LEN(CLIP(L_ESG:Subject)))
      
      DISPLAY(?L_ESG:Subject)
      
      !?L_ESG:Subject{PROP:SelStart} = in#  
      !SELECT(?L_ESG:Subject, in#, in# + LEN(CLIP(LO_EG:Substitution)) + 2)
      SELECT(?L_ESG:Subject, in# + LEN(CLIP(LO_EG:Substitution)) + 3)
   .
   
   EXIT
   
   
  
!--------------------------------------------
Send_Test_Email                ROUTINE
  DATA
r:send_fails  LONG
r:content     LIKE(LO_EG:Content)

  CODE
  ! Save all the stuff you want to save here
  
  CASE MESSAGE('The test email will be sent to the From address "' & CLIP(L_ESG:From) & '"',,ICON:Question,BUTTON:OK+BUTTON:CANCEL)
  OF BUTTON:OK
     r:content = LO_EG:Content

     ! (p_Subject, p_Text, p_EmailAccount, p_HTML, p_Server, p_AuthUser, p_AuthPassword, p_From, p_To, p_CC, p_BCC, 
     ! p_Helo, p_Attach, p_Port)
     IF ( Send_Email(L_ESG:Subject, r:content, 0,, L_ESG:Server, L_ESG:SMTP_Username, L_ESG:SMTP_Password |
                     , L_ESG:From, CLIP(L_ESG:From), L_ESG:CC, L_ESG:BCC, , , L_ESG:Port) ) < 0
        r:send_fails += 1
     . 
     
     IF r:send_fails > 0
       MESSAGE('Emails failed to send.|||You can review the options and try again','Email Sending',ICON:Exclamation)
     ELSE
       MESSAGE('Email sending complete.')       
  .  .
    
  EXIT
!--------------------------------------------
Setup_Substitutions                 ROUTINE
DATA
CODE
   ?LO_EG:Substitution:2{PROP:From} = 'Client Name|#Client Name|Collection Add.|#Collection Add|Delivery Add.|#Delivery ' & |
      'Add|Client Ref.|#Client Ref|Commodity|#Commodity|Items|#Items|Weight|#Weight|Special ' & |
      'Remarks|#Special Remarks|ETA (M)|#ETA (M)|Branch (DI)|#Branch (DI)|Dispatch Vehicle (TS)|#Dispatch Vehicle (TS)|' & |
      'Delivery Date/Time (TS)|#Delivery Date/Time (TS)|Branch Tel. (TS)|#Branch Tel. (TS)|DI No.|#DI No.|Packa' & |
      'ging|#Packaging|Branch Journey-TO (M)|#Branch Journey-TO (M)'
   
   EXIT
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Delivery_Tracking_Emails_Setup')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Cancel
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:NotificationsSetup.Open                           ! File NotificationsSetup used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
    ?STRING2{PROP:Text} = 'Delivery Tracking Email Setup'
                                               ! Generated by NetTalk Extension (Start)
  ThisNetEmail.SuppressErrorMsg = 1         ! No Object Generated Error Messages ! Generated by NetTalk Extension
  ThisNetEmail.init()
  if ThisNetEmail.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  Do DefineListboxStyle
  !ProcedureTemplate = Window
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  LO_EG:Email_Type     = 'On Route'
  !DO Email_Type_NewSelection             
  
  ManClass.Change(1,1)
  DO Load_Stuff
  SELF.SetAlerts()
  ALERT(EscKey)
  DO Setup_Substitutions
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ThisNetEmail.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:NotificationsSetup.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
        IF ManClass.CheckChanged(L_ESG:Subject, LO_EG:Content, LO_SG:DoNotGenerate)
          CASE MESSAGE('Email subject/content/generate has changed, would you like to save it?','Content',ICON:Question,BUTTON:Yes+BUTTON:NO)
          OF BUTTON:YES
      !            Save_Content_File(LO_EG:Prev_Email_Type)
             ManClass.Save()
          .
        .    
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?BUTTON_SaveContent
      ThisWindow.Update()
      IF ManClass.Save() = FALSE
         MESSAGE('Save failed')
      .
      
      !Save_Content_File(LO_EG:Email_Type)
      
      !PUTINI('Manifest_Emails','EmailContent', LO_EG:Content, GLO:Local_INI)
    OF ?BUTTON_LoadContent
      ThisWindow.Update()
      CASE MESSAGE('Any changes to the above will be lost.||Are you sure?','Content',ICON:Question,BUTTON:YES+BUTTON:NO,BUTTON:NO)
      OF BUTTON:YES
      !  LO_EG:Content = GETINI('Manifest_Emails','EmailContent','', GLO:Local_INI)
         IF ManClass.Load() = FALSE
            MESSAGE('Failed to Load content')
         .
         DISPLAY
      .
    OF ?BUTTON_CopyClip
      ThisWindow.Update()
        SETCLIPBOARD(LO_EG:Content)
    OF ?BUTTON_Add
      ThisWindow.Update()
        DO Content_Add_Subs
    OF ?Ok:2
      ThisWindow.Update()
      DO Save_Stuff              ! save stuff 
      
      !  Save_Content_File(LO_EG:Email_Type)
      
      !  DO Send_Email
    OF ?SendTestEmail
      ThisWindow.Update()
        DO Send_Test_Email
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisNetEmail.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LO_EG:Email_Type:2
      ManClass.StageChange(LO_EG:Email_Type)
      
      !DO Email_Type_NewSelection
    OF ?SHEET_ManType
      ! Selected Tab
      ManClass.TypeChange(CHOICE(?SHEET_ManType))   
         
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all Selected events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?L_ESG:Subject
      L_LG:LastControl  = ?L_ESG:Subject
    OF ?LO_EG:Content
      L_LG:LastControl  = ?LO_EG:Content
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      IF KEYCODE() = EscKey
         !MESSAGE('esc')         ! just ignore... dont exit
         
      .
      
         
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!Load_Content_File   PROCEDURE(STRING p_Type)
!
!mf FILE,DRIVER('ASCII','/FILEBUFFERS=8'), NAME('.\EmailContent.txt'),| 
!         PRE(mf_),CREATE,THREAD
!Record     RECORD,PRE()
!Line        STRING(1024)
!          END
!    END
!
!i_        LONG
!l_txt     LIKE(LO_EG:Content)
!
!  CODE
!    ! Load subject
!    L_EG:Subject  = Get_Setting('Email-' & CLIP(p_Type) & '-Subject', 1,'', 'Email subject')
!    
!
!    mf{PROP:Name} = CLIP(p_Type) & '.txt'
!      
!    OPEN(mf)
!    IF ERRORCODE()
!      ! no file yet
!!      MESSAGE('No content file found?   ' & ERROR())
!    ELSE
!      CLEAR(l_txt)
!      i_ = 0
!      
!      SET(mf)
!      LOOP
!        NEXT(mf)
!        IF ERRORCODE()
!!          MESSAGE('end of file?:  ' & ERROR() & '|||file: ' & mf{PROP:Name})
!          BREAK
!        .
!        i_ += 1
!        
!!        MESSAGE('next: ' & ERROR() & '||line ' & i_ & ': ' & mf_:Line)
!
!        IF i_ <= 1
!          l_txt   = mf_:line 
!        ELSE
!          l_txt   = CLIP(l_txt) & '<13,10>' & CLIP(mf_:line)
!        .
!      .
!      
!      CLOSE(mf)    
!    .
!    
!    L_EG:Content  = l_txt
!  
!  RETURN l_txt
!  
!Save_Content_File   PROCEDURE(STRING p_Type)
!
!mf FILE,DRIVER('ASCII','/FILEBUFFERS=8'), NAME('.\EmailContent.txt'),| 
!         PRE(mf_),CREATE,THREAD
!Record     RECORD,PRE()
!Line        STRING(1024)
!          END
!         END
!i_                    LONG
!
!
!  CODE
!    stuff_" = Get_Setting('Email-' & CLIP(p_Type) & '-Subject', 2, L_ESG:Subject, 'Email subject')
!
!
!    COPY(CLIP(p_Type) & '.txt', CLIP(p_Type) & FORMAT(TODAY(),@d6_)  & FORMAT(CLOCK(),@t1_) & '.txt')   ! a backup
!
!    mf{PROP:Name} = CLIP(p_Type) & '.txt'
!    
!    CREATE(mf)
!    OPEN(mf)
!    IF ERRORCODE()
!      MESSAGE('On Save - Open file error: ' & Error())
!    ELSE
!      mf_:Line  = CLIP(LO_EG:Content)
!      
!      ADD(mf)
!      IF ERRORCODE()
!        MESSAGE('On Save - Add error: ' & Error())
!      .
!      
!      CLOSE(mf)
!    .
!    DISPLAY
!  RETURN
!  
!--------------------------------------------
Merge_Content       PROCEDURE(ULONG p:DID, STRING p:Content)
L:Content             LIKE(LO_EG:Content)

L:Count               LONG(0)
L:Commodities         STRING(255)
L:Units               LONG
L:Weight              DECIMAL(12)
                      
Del_View              VIEW(Deliveries)
                        PROJECT(DEL:DID,DEL:DINo,DEL:ClientReference,DEL:CollectionAID,DEL:DeliveryAID,DEL:Notes,DEL:ReceivedDate,DEL:ReceivedTime)
                        JOIN(DELI:FKey_DID_ItemNo,DEL:DID)
                          PROJECT(DELI:Units,DELI:Weight,DELI:CMID)
                          JOIN(COM:PKey_CMID, DELI:CMID)
                            PROJECT(COM:Commodity)
                        . .
                        JOIN(CLI:PKey_CID, DEL:CID)
                          PROJECT(CLI:ClientName)
                        .
                        JOIN(FLO:PKey_FID, DEL:FID)
                          PROJECT(FLO:Floor)
                      . .

View_Del              ViewManager

L:TripSheets          STRING(150)
L:TS_Veh              STRING(100)
L:BranchTel           STRING(100)
L:Del_Date            STRING(100)

  CODE
  L:Content = p:Content
    
  View_Del.Init(Del_View, Relate:Deliveries)
  View_Del.AddSortOrder(DEL:PKey_DID)
  View_Del.AppendOrder('DELI:ItemNo')
  View_Del.AddRange(DEL:DID, p:DID)
  !View_Del.SetFilter()
 
  View_Del.Reset()    
  LOOP
    IF View_Del.Next() ~= LEVEL:Benign        ! Failed to fetch the DI.. 
      BREAK      
    ELSE    
      L:Count += 1
      
      IF L:Count = 1                          ! some fields we only have one of
        ! Client Name|Collection Add|Delivery Add|Client Ref|Commodity|Items|Weight|Special Remarks|ETA|Branch|Dispatch Vehicle|Delivery Date/Time
        ! (p:String, p:Find, p:Replace, p:Occurances, p:No_Case)
        ! (*STRING, STRING, STRING, LONG=0, BYTE=0), LONG
        Replace_Strings(L:Content, '[Client Name]', CLIP(CLI:ClientName))
        Replace_Strings(L:Content, '[Collection Add]', CLIP(Get_Address(DEL:CollectionAID,,1)))
        Replace_Strings(L:Content, '[Delivery Add]', CLIP(Get_Address(DEL:DeliveryAID,,1)))
        Replace_Strings(L:Content, '[Client Ref]', CLIP(DEL:ClientReference))
        
        ! Check for a TripSheet delivery
        TRDI:DIID = DELI:DIID        
        IF Access:TripSheetDeliveries.TryFetch(TRDI:FKey_DIID) = Level:Benign
           ! We have at least one, use this one for now!     
           L:Del_Date  = FORMAT(TRDI:DeliveredDate,@d6) & ' ' & FORMAT(TRDI:DeliveredTime, @t1)
      . .
      
      Add_to_List(CLIP(COM:Commodity), L:Commodities, ', ')
      L:Units   += DELI:Units      
      L:Weight  += DELI:Weight        ! do they want volumetric?
    .
  .
  View_Del.Kill()    

  Replace_Strings(L:Content, '[Commodity]', CLIP(L:Commodities))
  Replace_Strings(L:Content, '[Items]', L:Units)
  Replace_Strings(L:Content, '[Weight]', L:Weight)
  Replace_Strings(L:Content, '[Special Remarks]', CLIP(DEL:Notes))
    
  !Branch|Dispatch Vehicle|Delivery Date/Time
  ! Branch is the DI FID name
  Replace_Strings(L:Content, '[Branch]', FLO:Floor)

  ! Get delivery vehicle info
  ! Returns comma del list of tripsheets in first to last order
  L:TripSheets  = Get_Delivery_TripSheets(DEL:DID)
  TRI:TRID      = Get_1st_Element_From_Delim_Str(L:TripSheets,',',1)
  IF Access:TripSheets.TryFetch(TRI:PKey_TID) = LEVEL:Benign
      ! p:Option  1.  Capacity                            of all
      !           2.  Registrations                       of all OR p:Truck = 100 gives horse reg only
      !           3.  Makes & Models                      of all
      !           4.  Description of Truck / Trailer      requires p:Truck  0 to 3
      !           5.  Truck / Trailer Capacity            requires p:Truck  0 to 3
      !           6.  Truck / Trailer ID (TTID)           requires p:Truck  0 to 3
      !           7.  Count of vehicles in composition
      !           8.  Compositions Capacity
      !           9.  Driver of Horse
      !           10. Transporters ID for this VCID
      !           11. Licensing dates                     of all
      L:TS_Veh    = Get_VehComp_Info(TRI:VCID, 2)
      L:BranchTel = Get_Address(Get_Branch_Info(TRI:BID, 2), 3)   ! could return comma del list      
  . 
    
  Replace_Strings(L:Content, '[Dispatch Vehicle]', CLIP(L:TS_Veh))     ! Trip sheet  
  Replace_Strings(L:Content, '[Branch Tel.]', CLIP(L:BranchTel))  

  ! see above in loop
  Replace_Strings(L:Content, '[Delivery Date/Time]', CLIP(L:Del_Date))

    
  t#  = DEFORMAT('13:00', @t1)     ! hh:mm      
  Date_Time_Advance(MAN:DepartDate, MAN:DepartTime, 1, t#)
  Replace_Strings(L:Content, '[ETA]', FORMAT(MAN:DepartDate,@d6) & ' @ ' & FORMAT(MAN:DepartTime, @t1))    

  RETURN(L:Content)
    
    ! Date_Time_Advance
    ! (*DATE, <*TIME>, BYTE, LONG)
    ! (p_Date, p_Time, p_Option, p_Val)
    !
    ! p_Option
    !       1 - Time
    !       2 - Days
    !       3 - Weeks
    !       4 - Months
    !       5 - Quarters
    !       6 - Years
    
    ! Get_Address
    ! (p:AID, p:Type, p:Details)
    ! (ULONG, BYTE=0, BYTE=0),STRING
    ! p:Type
    !   0 - Comma delimited string
    !   1 - Block
    !   2 - Comma delimited string - including empty Line 1 & 2 where they are empty
    !   3 - Phone Nos. (comma delim)
    ! p:Details
    !   0 - all
    !   1 - without City & Province
    !
    ! Returns
    !   Address Name, Line1 (if), Line2 (if), Post Code, City, Province
    !   or
    !   Address Name, Line1 (if), Line2 (if), Post Code
    !   or
    !   Address Name, Line1 (if), Line2 (if), <missing info string>
    !   or
    !   Phone1, Phone2 OR Phone1 OR Phone 2
! Class ================================================================
! Implement a class to manage the changing of the drop down (the manifest stage)
! and the changing of the tab (the manifest type)
ManClass.StageChange       PROCEDURE(STRING newStage)
R_Merge_Str                   CSTRING(500)
Stage                         BYTE
CODE
   R_Merge_Str = 'This Content will be merged with the data and emailed to the listed email addresses on the DIs.' & | 
      '<13,10>You can use the Substitution options (see bottom right) to populate merge fields (for Subject & Body).'

   L_LG:EmailTypeNotes     = ''
   CASE LO_EG:Email_Type
   OF 'On Route'
      Stage    = 1
      L_LG:EmailTypeNotes  = 'Content to use when the Manifest state is changed to On-Route from Loading.' |
         & ' ' & R_Merge_Str
   OF 'Transferred'
      Stage    = 2
      L_LG:EmailTypeNotes  = 'Content to use when the Manifest state is changed to Transferred.' |
         & ' ' & R_Merge_Str
   OF 'Out On Delivery'
      Stage    = 3
      L_LG:EmailTypeNotes  = 'Content to use when the Tripsheet state is changed to On Route.' |
         & ' ' & R_Merge_Str
   OF 'Delivered'
      Stage    = 4
      L_LG:EmailTypeNotes  = 'Content to use when the Tripsheet state is changed to Transferred.' |
         & ' ' & R_Merge_Str
   OF 'Manifest General'
      Stage    = 5
      L_LG:EmailTypeNotes  = 'Content to use as default when user right-clicks on Manifest Items list.' |
         & ' This Content will be merged with the data and emailed.  Email addresses are chosen from Client records' |
         &' at the time you do the right click.  You can also add addresses then and you can edit the email content then too.'
   .

   RETURN SELF.Change(Stage, SELF.Type)   
   
ManClass.TypeChange              PROCEDURE(BYTE newType)
CODE
   RETURN SELF.Change(SELF.Stage, newType)   


ManClass.Change      PROCEDURE(BYTE newStage, BYTE newType, BYTE LoadOnly=0)
Cont                    BYTE(1)
CODE
   ! Check if we need to ask to save
   UPDATE(?LO_EG:Content)
   IF LoadOnly = FALSE AND SELF.CheckChanged(L_ESG:Subject, LO_EG:Content, LO_SG:DoNotGenerate)
     CASE MESSAGE('Email subject/content/generate has changed, would you like to save it?','Content',ICON:Question,BUTTON:Yes+BUTTON:NO)
     OF BUTTON:YES
         Cont = SELF.Save()
     .
   .    
   
   IF Cont
      ! Change the loaded record
      Cont = SELF.Load(newStage, newType)
      IF Cont 
         L_ESG:Subject           = SELF.Subject
         LO_EG:Content           = SELF.Template
         LO_SG:DoNotGenerate     = SELF.DoNotGenerate
      ELSE
         MESSAGE('Failed to Load the new Stage/Type')
   .  .
   
   DISPLAY     
   RETURN Cont

   
ManClass.CheckChanged             PROCEDURE(STRING Subject, STRING Template, BYTE DoNotGenerate)
CODE
   ! If we have no content then also report unchanged
   IF CLIP(SELF.Subject) = CLIP(Subject) AND CLIP(SELF.Template) = CLIP(Template) AND DoNotGenerate = SELF.DoNotGenerate
      RETURN FALSE
   .
   RETURN TRUE
   
ManClass.Load        PROCEDURE(BYTE Stage=0, BYTE p_Type=0)
Loaded                  BYTE(0)
CODE
   IF Stage = 0 AND p_Type = 0
      Stage    = SELF.Stage
      p_Type   = SELF.Type
   .
   
   NOT:NoticeStage   = Stage
   NOT:LoadType      = p_Type
   IF Access:NotificationsSetup.TryFetch(NOT:Key_Stage_Type) = Level:Benign
      SELF.Subject   = NOT:Subject
      SELF.Template  = NOT:Template
      
      SELF.Stage     = NOT:NoticeStage
      SELF.Type      = NOT:LoadType
      SELF.DoNotGenerate = NOT:DoNotGenerate
      Loaded         = TRUE
   ELSE
      ! Try to create it then!
      SELF.Stage     = Stage
      SELF.Type      = p_Type
      IF SELF.Save('New subject', 'New Template', FALSE)
         Loaded      = TRUE
      ELSE   
         MESSAGE('Error: ' & ERROR() )
   .  .    
   
   RETURN Loaded
   
ManClass.Save        PROCEDURE(<STRING Subject>, <STRING Template>, <BYTE DoNotGenerate>)
Saved                   BYTE(0)
Sub                     LIKE(NOT:Subject)
Temp                    LIKE(NOT:Template)
CODE
   IF OMITTED(2)      ! Assume all are omitted then
      Sub            = L_ESG:Subject
      Temp           = LO_EG:Content
      DoNotGenerate  = LO_SG:DoNotGenerate
   ELSE
      Sub            = Subject
      Temp           = Template
      DoNotGenerate  = DoNotGenerate
   .
   
   NOT:NoticeStage   = SELF.Stage
   NOT:LoadType      = SELF.Type
   IF Access:NotificationsSetup.TryFetch(NOT:Key_Stage_Type) = Level:Benign
      NOT:Subject    = CLIP(Sub)
      NOT:Template   = CLIP(Temp)
      NOT:DoNotGenerate = DoNotGenerate
      
      IF Access:NotificationsSetup.Update() = Level:Benign     
         SELF.Subject   = Sub
         SELF.Template  = Temp
         SELF.DoNotGenerate = DoNotGenerate
         Saved          = TRUE
      .
   ELSE
      CLEAR(NOT:Record)
      IF Access:NotificationsSetup.PrimeAutoInc() = Level:Benign
         NOT:NoticeStage   = SELF.Stage
         NOT:LoadType      = SELF.Type
         
         NOT:Subject       = CLIP(Sub)
         NOT:Template      = CLIP(Temp)
         NOT:DoNotGenerate = DoNotGenerate
         IF Access:NotificationsSetup.Insert() = Level:Benign
            SELF.Subject   = Sub
            SELF.Template  = Temp
            SELF.DoNotGenerate = DoNotGenerate
            Saved          = TRUE
         .
      .
   .   
   RETURN(Saved)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Deliveries_with_email_etc_old_perhaps_____ PROCEDURE (p:Option)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Screen_Group     GROUP,PRE(L_SG)                       !
Manifested_Units     ULONG                                 !
Un_Manifested_Units  ULONG                                 !
Value                DECIMAL(11,2)                         !
Value_Type           BYTE                                  !
VATRate_Previous     DECIMAL(5,2)                          !Before any possible user change
Tripsheets_Info      STRING(255)                           !Used as to get TRID's and Delivered Date / Times
                     END                                   !
LOC:Lookup_Vars      GROUP,PRE(L_LG)                       !
COD_Address          STRING(35)                            !Name of this address
Driver_Name          STRING(70)                            !Driver Name
Journey              STRING(70)                            !Description
ClientName           STRING(100)                           !
ClientNo             ULONG                                 !Client No.
From_Address         STRING(35)                            !Name of this address
To_Address           STRING(35)                            !Name of this address
LoadType             STRING(100)                           !Load Type
ServiceRequirement   STRING(35)                            !Service Requirement
Floor                STRING(35)                            !Floor Name
FloorRate            STRING(35)                            !Floor Name
ClientRateType       STRING(100)                           !Load Type
ClientRateType_CID   ULONG                                 !Client ID
                     END                                   !
LOC:Load_Type_Group  GROUP,PRE(L_LT)                       !
TurnIn               BYTE                                  !Container Turn In required for this Load Type
LoadOption           BYTE                                  !Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
Hazchem              BYTE                                  !
ContainerParkStandard BYTE                                 !Container Park Standard Rates
FID                  ULONG                                 !Floor ID
                     END                                   !
LOC:Last_Selected    LONG                                  !
LOC:Request          LONG                                  !
LOC:Manifested       BYTE                                  !
LOC:Original_Request LONG                                  !
LOC:Client_Group     GROUP,PRE(L_CG)                       !
MinChargeChecked     BYTE                                  !
MinimiumCharge       DECIMAL(8,2)                          !
UseMinCharge         BYTE                                  !
GenerateInvoice      BYTE                                  !Generate an invoice when DI created
ClientTerms          BYTE                                  !Terms - Pre Paid, COD, Account, On Statement
ClientStatus         BYTE                                  !Normal, On Hold, Closed
                     END                                   !
LOC:Charge_Group     GROUP,PRE(L_CC)                       !
TotalWeight          DECIMAL(10,2)                         !
TotalUnits           ULONG                                 !Number of units
Charge               DECIMAL(9,2)                          !Charge for the DI (additional legs have own charges specified)
InsuanceAmount       DECIMAL(10,2)                         !
FuelSurchargePer     DECIMAL(12,4)                         !
MinChargeRateChecked BYTE                                  !
MinimiumChargeRate   DECIMAL(10,4)                         !Rate per Kg
UseMinRateCharge     BYTE                                  !
Leg_Costs            DECIMAL(9,2)                          !
TotalCharge          DECIMAL(9,2)                          !Charge for the DI (additional legs have own charges specified)
Rate_Checked         BYTE                                  !Once rate checked tell user - only once
VAT                  DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
TotalCharge_VAT      DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
                     END                                   !
LOC:Last_Group       GROUP,PRE(L_LG)                       !
Charge_Specified     BYTE                                  !User has specified a charge - no longer calculated (still check min)
Rate_Specified       BYTE                                  !User typed a rate
Last_Rate            DECIMAL(10,4)                         !Rate for DI (debtors journey rate possibly adjusted) - Charge will be Rate multiplied by the total weight in kgs.
Next_Rate            DECIMAL(10,4)                         !Rate for DI (debtors journey rate possibly adjusted) - Charge will be Rate multiplied by the total weight in kgs.
No_Items             ULONG                                 !
Last_No_Items        ULONG                                 !
CID                  ULONG                                 !Client ID
JID                  ULONG                                 !Journey ID
LTID                 ULONG                                 !Load Type ID
CRTID                ULONG                                 !Client Rate Type ID
PODMessage           STRING(255)                           !
                     END                                   !
LOC:Rate_Group       GROUP,PRE(L_RG)                       !
Setup_Rate           BYTE                                  !
To_Weight            DECIMAL(9)                            !Up to this mass in Kgs
Effective_Date       LONG                                  !Effective from this date
Selected_Rate        DECIMAL(10,4)                         !Rate per Kg
ID                   ULONG                                 !Rate / Container Park ID
Returned_Rate        DECIMAL(10,4)                         !Rate per Kg
                     END                                   !
LOC:Items_Group      GROUP,PRE()                           !
L_IG:MID             STRING(50)                            !Manifest ID
L_IG:Last_MID        ULONG                                 !Manifest ID
                     END                                   !
LOC:DeliveryComposition_Ret_Group GROUP,PRE(L_DC)          !Totals so far
Items                ULONG                                 !Total no. of items
Weight               DECIMAL(15,2)                         !Total weight
Volume               DECIMAL(12,3)                         !Total volume
Deliveries           ULONG                                 !Total deliveries
First_DID            ULONG                                 !Delivery ID
                     END                                   !
LOC:DeliveryComposition_Group_2 GROUP,PRE(L_DC2)           !
Start_of_Insert      BYTE                                  !set if new record and user not set anything yet - does default populate
                     END                                   !
LOC:Rate_Group_Ret   GROUP,PRE(L_RG)                       !for retrieving rates
RatePerKg            DECIMAL(10,4)                         !Rate per Kg
                     END                                   !
LOC:GeneralRatesClientID ULONG                             !Client ID
LOC:Delivery_Leg_Vars GROUP,PRE()                          !
L_DLG:VAT            DECIMAL(12,2)                         !
L_DLG:Total          DECIMAL(12,2)                         !
                     END                                   !
LOC:Form_Control_Group GROUP,PRE(L_FCG)                    !
DI_Date_Checked      BYTE                                  !set to 1 on 1st fail, 2nd asks use if correct
                     END                                   !
LOC:ReleaseReason    STRING(35)                            !
LOC:Str              STRING(600)                           !
BRW4::View:Browse    VIEW(DeliveryItems)
                       PROJECT(DELI:ItemNo)
                       PROJECT(DELI:Weight)
                       PROJECT(DELI:ContainerNo)
                       PROJECT(DELI:ContainerVessel)
                       PROJECT(DELI:ETA)
                       PROJECT(DELI:Length)
                       PROJECT(DELI:Breadth)
                       PROJECT(DELI:Height)
                       PROJECT(DELI:DIID)
                       PROJECT(DELI:DID)
                       PROJECT(DELI:ContainerReturnAID)
                       PROJECT(DELI:PTID)
                       PROJECT(DELI:CMID)
                       PROJECT(DELI:CTID)
                       PROJECT(DELI:COID)
                       JOIN(ADD:PKey_AID,DELI:ContainerReturnAID)
                         PROJECT(ADD:AddressName)
                         PROJECT(ADD:AID)
                       END
                       JOIN(PACK:PKey_PTID,DELI:PTID)
                         PROJECT(PACK:PTID)
                       END
                       JOIN(COM:PKey_CMID,DELI:CMID)
                         PROJECT(COM:Commodity)
                         PROJECT(COM:CMID)
                       END
                       JOIN(CTYP:PKey_CTID,DELI:CTID)
                         PROJECT(CTYP:ContainerType)
                         PROJECT(CTYP:CTID)
                       END
                       JOIN(CONO:PKey_COID,DELI:COID)
                         PROJECT(CONO:ContainerOperator)
                         PROJECT(CONO:COID)
                       END
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:ItemsContainers
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
DELI:Weight            LIKE(DELI:Weight)              !List box control field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !List box control field - type derived from field
DELI:ContainerVessel   LIKE(DELI:ContainerVessel)     !List box control field - type derived from field
DELI:ETA               LIKE(DELI:ETA)                 !List box control field - type derived from field
CTYP:ContainerType     LIKE(CTYP:ContainerType)       !List box control field - type derived from field
CONO:ContainerOperator LIKE(CONO:ContainerOperator)   !List box control field - type derived from field
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
DELI:Length            LIKE(DELI:Length)              !List box control field - type derived from field
DELI:Breadth           LIKE(DELI:Breadth)             !List box control field - type derived from field
DELI:Height            LIKE(DELI:Height)              !List box control field - type derived from field
L_IG:MID               LIKE(L_IG:MID)                 !List box control field - type derived from local data
DELI:DIID              LIKE(DELI:DIID)                !Primary key field - type derived from field
DELI:DID               LIKE(DELI:DID)                 !Browse key field - type derived from field
ADD:AID                LIKE(ADD:AID)                  !Related join file key field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Related join file key field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Related join file key field - type derived from field
CTYP:CTID              LIKE(CTYP:CTID)                !Related join file key field - type derived from field
CONO:COID              LIKE(CONO:COID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW20::View:Browse   VIEW(DeliveryItemAlias2)
                       PROJECT(A_DELI2:ItemNo)
                       PROJECT(A_DELI2:Units)
                       PROJECT(A_DELI2:Weight)
                       PROJECT(A_DELI2:Volume)
                       PROJECT(A_DELI2:VolumetricWeight)
                       PROJECT(A_DELI2:Length)
                       PROJECT(A_DELI2:Breadth)
                       PROJECT(A_DELI2:Height)
                       PROJECT(A_DELI2:DIID)
                       PROJECT(A_DELI2:DID)
                       PROJECT(A_DELI2:PTID)
                       PROJECT(A_DELI2:CMID)
                       JOIN(PACK:PKey_PTID,A_DELI2:PTID)
                         PROJECT(PACK:Packaging)
                         PROJECT(PACK:PTID)
                       END
                       JOIN(COM:PKey_CMID,A_DELI2:CMID)
                         PROJECT(COM:Commodity)
                         PROJECT(COM:CMID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?Browse:ItemsLoose
A_DELI2:ItemNo         LIKE(A_DELI2:ItemNo)           !List box control field - type derived from field
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
PACK:Packaging         LIKE(PACK:Packaging)           !List box control field - type derived from field
A_DELI2:Units          LIKE(A_DELI2:Units)            !List box control field - type derived from field
A_DELI2:Weight         LIKE(A_DELI2:Weight)           !List box control field - type derived from field
A_DELI2:Volume         LIKE(A_DELI2:Volume)           !List box control field - type derived from field
A_DELI2:VolumetricWeight LIKE(A_DELI2:VolumetricWeight) !List box control field - type derived from field
A_DELI2:Length         LIKE(A_DELI2:Length)           !List box control field - type derived from field
A_DELI2:Breadth        LIKE(A_DELI2:Breadth)          !List box control field - type derived from field
A_DELI2:Height         LIKE(A_DELI2:Height)           !List box control field - type derived from field
L_IG:MID               LIKE(L_IG:MID)                 !List box control field - type derived from local data
A_DELI2:DIID           LIKE(A_DELI2:DIID)             !List box control field - type derived from field
A_DELI2:DID            LIKE(A_DELI2:DID)              !Browse key field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Related join file key field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(DeliveryLegs)
                       PROJECT(DELL:Leg)
                       PROJECT(DELL:Cost)
                       PROJECT(DELL:VATRate)
                       PROJECT(DELL:DLID)
                       PROJECT(DELL:DID)
                       PROJECT(DELL:JID)
                       PROJECT(DELL:TID)
                       JOIN(JOU:PKey_JID,DELL:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                       JOIN(TRA:PKey_TID,DELL:TID)
                         PROJECT(TRA:TransporterName)
                         PROJECT(TRA:TID)
                       END
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?Browse:6
DELL:Leg               LIKE(DELL:Leg)                 !List box control field - type derived from field
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
L_DLG:Total            LIKE(L_DLG:Total)              !List box control field - type derived from local data
DELL:Cost              LIKE(DELL:Cost)                !List box control field - type derived from field
L_DLG:VAT              LIKE(L_DLG:VAT)                !List box control field - type derived from local data
DELL:VATRate           LIKE(DELL:VATRate)             !List box control field - type derived from field
DELL:DLID              LIKE(DELL:DLID)                !Primary key field - type derived from field
DELL:DID               LIKE(DELL:DID)                 !Browse key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(DeliveryProgress)
                       PROJECT(DELP:StatusDate)
                       PROJECT(DELP:ActionDate)
                       PROJECT(DELP:StatusTime)
                       PROJECT(DELP:ActionTime)
                       PROJECT(DELP:DPID)
                       PROJECT(DELP:DID)
                       PROJECT(DELP:ACID)
                       PROJECT(DELP:DSID)
                       JOIN(ADDC:PKey_ACID,DELP:ACID)
                         PROJECT(ADDC:ContactName)
                         PROJECT(ADDC:ACID)
                       END
                       JOIN(DELS:PKey_DSID,DELP:DSID)
                         PROJECT(DELS:DeliveryStatus)
                         PROJECT(DELS:DSID)
                       END
                     END
Queue:Browse:8       QUEUE                            !Queue declaration for browse/combo box using ?Browse:8
DELS:DeliveryStatus    LIKE(DELS:DeliveryStatus)      !List box control field - type derived from field
ADDC:ContactName       LIKE(ADDC:ContactName)         !List box control field - type derived from field
DELP:StatusDate        LIKE(DELP:StatusDate)          !List box control field - type derived from field
DELP:ActionDate        LIKE(DELP:ActionDate)          !List box control field - type derived from field
DELP:StatusTime        LIKE(DELP:StatusTime)          !List box control field - type derived from field
DELP:ActionTime        LIKE(DELP:ActionTime)          !List box control field - type derived from field
DELP:DPID              LIKE(DELP:DPID)                !Primary key field - type derived from field
DELP:DID               LIKE(DELP:DID)                 !Browse key field - type derived from field
ADDC:ACID              LIKE(ADDC:ACID)                !Related join file key field - type derived from field
DELS:DSID              LIKE(DELS:DSID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB27::View:FileDrop VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?L_LG:FloorRate
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DEL:Record  LIKE(DEL:RECORD),THREAD
BRW4::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW4::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW4::PopupChoice    SIGNED                       ! Popup current choice
BRW4::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW4::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW20::FormatManager ListFormatManagerClass,THREAD ! LFM object
BRW20::PopupTextExt  STRING(1024)                 ! Extended popup text
BRW20::PopupChoice   SIGNED                       ! Popup current choice
BRW20::PopupChoiceOn BYTE(1)                      ! Popup on/off choice
BRW20::PopupChoiceExec BYTE(0)                    ! Popup executed
BRW6::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW6::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW6::PopupChoice    SIGNED                       ! Popup current choice
BRW6::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW6::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW8::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW8::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW8::PopupChoice    SIGNED                       ! Popup current choice
BRW8::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW8::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Form Deliveries'),AT(,,629,404),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdateDeliveries'),SYSTEM
                       SHEET,AT(4,2,621,375),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('BID:'),AT(60,337),USE(?DEL:BID:Prompt),HIDE,TRN
                           STRING(@n_10),AT(365,22,43),USE(DEL:DID),RIGHT(1),TRN
                           STRING(@n_10),AT(76,337,,10),USE(DEL:BID),RIGHT(1),HIDE,TRN
                           PROMPT('DID:'),AT(349,22),USE(?DEL:BID:Prompt:2),TRN
                           PROMPT('DI No.:'),AT(9,22),USE(?DEL:DINo:Prompt),TRN
                           ENTRY(@n_10b),AT(65,22,70,10),USE(DEL:DINo),RIGHT(1),MSG('Delivery Instruction Number'),REQ, |
  TIP('Delivery Instruction Number')
                           STRING('Branch'),AT(142,22,59,10),USE(?String_Branch),TRN
                           PROMPT('DI Date:'),AT(217,22),USE(?DEL:DIDate:Prompt),TRN
                           SPIN(@d6),AT(270,22,70,10),USE(DEL:DIDate),RIGHT(1),MSG('DI Date'),REQ,TIP('DI Date')
                           BUTTON('...'),AT(254,22,12,10),USE(?Calendar)
                           PROMPT('Client:'),AT(9,36),USE(?LOC:ClientName:Prompt),TRN
                           BUTTON('...'),AT(49,36,12,10),USE(?CallLookup_Client),KEY(F2Key),TIP('Press F2 to access')
                           ENTRY(@s100),AT(65,36,93,10),USE(L_LG:ClientName),REQ,TIP('Client name')
                           ENTRY(@n_10b),AT(161,36,41,10),USE(L_LG:ClientNo),RIGHT(1),MSG('Client ID'),REQ,TIP('Client ID')
                           PROMPT('Client Ref.:'),AT(217,36),USE(?DEL:ClientReference:Prompt),TRN
                           ENTRY(@s60),AT(270,36,70,10),USE(DEL:ClientReference),LEFT(1),MSG('Client Reference'),TIP('Client Reference')
                           PROMPT('CID:'),AT(349,36),USE(?DEL:CID:Prompt),TRN
                           STRING(@n_10),AT(365,36,43),USE(DEL:CID),RIGHT(1),TRN
                           GROUP,AT(9,52,193,10),USE(?Group_LoadType)
                             PROMPT('Load Type:'),AT(9,52),USE(?LoadType:Prompt),TRN
                             BUTTON('...'),AT(49,52,12,10),USE(?CallLookup_LoadType),KEY(F3Key),TIP('Press F3 to access')
                             ENTRY(@s100),AT(65,52,137,10),USE(L_LG:LoadType),MSG('Load Type'),REQ,TIP('Load Type')
                           END
                           BUTTON(':'),AT(209,52,5,10),USE(?Button_ListRates),TIP('List the Clients Rates')
                           PROMPT('Rate Type:'),AT(217,52),USE(?ClientRateType:Prompt),TRN
                           BUTTON('...'),AT(254,52,12,10),USE(?CallLookup)
                           ENTRY(@s100),AT(270,52,137,10),USE(L_LG:ClientRateType),MSG('Client Rate Type'),REQ,TIP('Client Rate Type')
                           PROMPT('Journey:'),AT(9,68),USE(?Journey:Prompt),TRN
                           BUTTON('...'),AT(49,68,12,10),USE(?CallLookup_Journey),KEY(F4Key),TIP('Press F4 to access')
                           ENTRY(@s70),AT(65,68,137,10),USE(L_LG:Journey),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  MSG('Journey'),REQ,TIP('Journey')
                           PROMPT('Floor:'),AT(217,68),USE(?Floor:Prompt),TRN
                           BUTTON('...'),AT(254,68,12,10),USE(?CallLookup_Floor)
                           ENTRY(@s35),AT(270,68,137,10),USE(L_LG:Floor),MSG('Floor Name'),REQ,TIP('Floor Name')
                           BUTTON('...'),AT(49,82,12,10),USE(?CallLookup_CollectAdd),KEY(F5Key),TIP('Press F5 to access')
                           PROMPT('Collect:'),AT(9,82),USE(?From_Address:Prompt),TRN
                           ENTRY(@s35),AT(65,82,137,10),USE(L_LG:From_Address),MSG('Collection Address Name'),REQ,TIP('Collection' & |
  ' Address Name')
                           BUTTON('...'),AT(254,82,12,10),USE(?CallLookup_ToAdd),KEY(F6Key)
                           PROMPT('Deliver:'),AT(217,82),USE(?To_Address:Prompt),TRN
                           ENTRY(@s35),AT(270,82,137,10),USE(L_LG:To_Address),MSG('Delivery Address Name'),REQ,TIP('Delivery A' & |
  'ddress Name')
                           BUTTON('www'),AT(49,22,12,10),USE(?Button_Release),FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI), |
  LEFT,ICON('dotred.ico'),HIDE,TIP('Release this DI')
                           PROMPT('Driver:'),AT(9,98),USE(?Driver:Prompt),TRN
                           BUTTON('...'),AT(49,98,12,10),USE(?CallLookup_Driver_Name),KEY(F7Key),TIP('Press F7 to access')
                           ENTRY(@s70),AT(65,98,93,10),USE(L_LG:Driver_Name),MSG('Name of Driver'),TIP('Name of Dr' & |
  'iver collected by')
                           BUTTON('COD'),AT(169,98,,10),USE(?Button_COD_Address),FONT(,,,FONT:bold,CHARSET:ANSI),KEY(F8Key), |
  DISABLE,TIP('Specify a COD Address - F8 key<0DH,0AH>This will be used on the Invoice')
                           PROMPT('Service:'),AT(217,98),USE(?ServiceRequirement:Prompt),TRN
                           BUTTON('...'),AT(254,98,12,10),USE(?CallLookup_ServiceReq),KEY(F9Key),TIP('Press F9 to access')
                           ENTRY(@s255),AT(85,188,528,10),USE(DEL:NoticeEmailAddresses),MSG('Email addresses to se' & |
  'nd notices to of progress of DI'),TIP('Email addresses to send notices to of progress of DI')
                           PROMPT('Email Addresses:'),AT(9,188),USE(?DEL:NoticeEmailAddresses:Prompt)
                           BUTTON('...'),AT(68,188,12,10),USE(?Lookup_EmailAddresses),KEY(F7Key),TIP('Press F7 to access')
                           ENTRY(@s35),AT(270,98,137,10),USE(L_LG:ServiceRequirement),MSG('Service Requirement'),REQ, |
  TIP('Service Requirement')
                           LINE,AT(10,202,612,0),USE(?Line_diitem_div),COLOR(COLOR:Black),LINEWIDTH(2)
                           SHEET,AT(7,207,613,142),USE(?Sheet_Items),DISABLE,BELOW
                             TAB('General'),USE(?Tab_Loose)
                               LIST,AT(11,216,603,113),USE(?Browse:ItemsLoose),HVSCROLL,KEY(F10Key),FORMAT('32R(2)|M~I' & |
  'tem No.~C(0)@n6@60L(2)|M~Commodity~C(0)@s35@60L(2)|M~Packaging~C(0)@s35@26R(2)|M~Uni' & |
  'ts~C(0)@n6@44R(2)|M~Weight~C(0)@n-11.2@44R(2)|M~Volume~C(0)@n-11.3@62R(2)|M~Volumetr' & |
  'ic Weight~C(0)@n-11.2@28R(2)|M~Length~C(0)@n6@30R(2)|M~Breadth~C(0)@n6@30R(2)|M~Heig' & |
  'ht~C(0)@n6.2@60L(2)|M~MID List~C(0)@s50@40R(2)|M~DIID~C(0)@n_10@'),FROM(Queue:Browse),IMM, |
  MSG('Browsing the DeliveryProgress file')
                               BUTTON('&View'),AT(401,333,53,14),USE(?View:2),LEFT,ICON('WAview.ICO'),FLAT,TIP('View (General)')
                               BUTTON('&Insert'),AT(462,333,49,14),USE(?Insert),LEFT,ICON('WAINSERT.ICO'),FLAT
                               BUTTON('&Change'),AT(514,333,49,14),USE(?Change),LEFT,ICON('WACHANGE.ICO'),FLAT
                               BUTTON('&Delete'),AT(566,333,49,14),USE(?Delete),LEFT,ICON('WADELETE.ICO'),FLAT
                             END
                             TAB('Containers'),USE(?Tab_Containers)
                               LIST,AT(13,119,391,99),USE(?Browse:ItemsContainers),HVSCROLL,FORMAT('32R(2)|M~Item No.~' & |
  'C(0)@n6@50L(2)|M~Commodity~C(0)@s35@44R(2)|M~Weight~C(0)@n-11.2@50L(2)|M~Container N' & |
  'o.~C(0)@s35@50L(2)|M~Vessel~C(0)@s35@38R(2)|M~ETA~C(0)@d5@54L(2)|M~Container Type~C(' & |
  '0)@s35@66L(2)|M~Container Operator~C(0)@s35@56L(2)|M~Turn In Address~C(0)@s35@28R(2)' & |
  '|M~Length~C(0)@n6@30R(2)|M~Breadth~C(0)@n6@26R(2)|M~Height~C(0)@n6@40L(2)|M~L IG : M' & |
  'ID~C(0)@s50@'),FROM(Queue:Browse:4),IMM,MSG('Browsing the DeliveryProgress file')
                               BUTTON('&View'),AT(138,220,53,14),USE(?View),LEFT,ICON('WAview.ICO'),FLAT,TIP('View (Containers)')
                               BUTTON('&Insert'),AT(255,220,49,14),USE(?Insert:5),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record (c)')
                               BUTTON('&Change'),AT(309,220,49,14),USE(?Change:5),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record (c)')
                               BUTTON('&Delete'),AT(361,220,49,14),USE(?Delete:5),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record (c)')
                             END
                           END
                           BUTTON('&Tabs'),AT(163,335,48,14),USE(?Button_Wizard),LEFT,ICON('�<02H,011H,07FH>'),FLAT,SKIP
                           BUTTON('www'),AT(35,36,12,10),USE(?Button_ClientEmails),FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI), |
  LEFT,ICON('Icon0443.ico'),TIP('Client email addresses')
                         END
                         TAB('&2) Charges'),USE(?Tab_Charges)
                           CHECK(' &Insure'),AT(85,22,43,10),USE(DEL:Insure),MSG('Insure'),TIP('Insure the goods o' & |
  'n this DI.<0DH,0AH>Note this is in addition to any insurance that may be part of thi' & |
  's clients rate.'),TRN
                           PROMPT('Clients Terms:'),AT(282,22),USE(?L_CG:ClientTerms:Prompt),TRN
                           LIST,AT(337,22,71,10),USE(L_CG:ClientTerms),DISABLE,DROP(5),FROM('Pre Paid|#0|COD|#1|Ac' & |
  'count|#2|On Statement|#3'),MSG('Terms - Pre Paid, COD, Account'),TIP('Terms - Pre Pa' & |
  'id, COD, Account')
                           GROUP,AT(9,20,176,25),USE(?Group_Insured)
                             PROMPT('Consignment Value:'),AT(9,22),USE(?DEL:TotalConsignmentValue:Prompt),TRN
                             ENTRY(@n-15.2),AT(133,22,52,10),USE(DEL:TotalConsignmentValue),DECIMAL(12),MSG('Total valu' & |
  'e of the cargo on this DI - required if additional insurance required'),TIP('Total valu' & |
  'e of the cargo on this DI - required if additional insurance required')
                             PROMPT('Insurance Rate:'),AT(9,34),USE(?DEL:InsuranceRate:Prompt),TRN
                             ENTRY(@N-11.6~%~),AT(133,34,52,10),USE(DEL:InsuranceRate),RIGHT(1),MSG('Insurance rate per ton'), |
  TIP('Insurance rate per ton')
                           END
                           PROMPT('Insuance Amount:'),AT(9,50),USE(?InsuanceAmount:Prompt),TRN
                           ENTRY(@n-14.2),AT(133,50,52,10),USE(L_CC:InsuanceAmount),DECIMAL(12),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           CHECK(' Generate Invoice'),AT(337,36),USE(L_CG:GenerateInvoice),MSG('Generate an invoic' & |
  'e when DI created'),TIP('Generate an invoice when DI created'),TRN
                           PROMPT('+'),AT(190,50),USE(?Prompt25),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           BUTTON('Reverse Calculation'),AT(217,50,75,10),USE(?Button_ReverseCalc),TIP('You specif' & |
  'y the Insurance Amount - we''ll calculate the rate')
                           PROMPT('&Rate:'),AT(9,82),USE(?DEL:Rate:Prompt),TRN
                           ENTRY(@n-11.3),AT(133,82,52,10),USE(DEL:Rate),RIGHT(1),MSG('Rate used on this DI'),TIP('Rate used ' & |
  'on this DI<0DH,0AH>Charge will be this rate multiplied by the total weight in kgs.')
                           BUTTON('&Get Rate'),AT(74,82,53,14),USE(?Button_GetRate)
                           BUTTON('&List Rates'),AT(74,96,53,14),USE(?Button_ShowRates)
                           PROMPT('Total Weight:'),AT(282,82),USE(?L_CC:TotalWeight:Prompt),TRN
                           ENTRY(@n-14.1),AT(350,82,52,10),USE(L_CC:TotalWeight),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP
                           GROUP,AT(258,100,143,10),USE(?Group_RateFloor),DISABLE
                             PROMPT('Rate Floor:'),AT(278,100),USE(?FloorRate:Prompt:2),TRN
                             LIST,AT(330,100,71,10),USE(L_LG:FloorRate),VSCROLL,DROP(15,100),FORMAT('140L(2)|M~Floor~@s35@'), |
  FROM(Queue:FileDrop)
                           END
                           PANEL,AT(133,96,275,46),USE(?Panel1),BEVEL(-1,-1),FILL(00E9E9E9h)
                           STRING('Rate Information'),AT(137,100,138,10),USE(?String_Rate_Info),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           CHECK(' This is a Setup Rate'),AT(190,82),USE(L_RG:Setup_Rate),DISABLE,TRN
                           PROMPT('Rate Upper Weight:'),AT(278,114),USE(?To_Weight:Prompt),TRN
                           SPIN(@n-12.0),AT(350,114,52,10),USE(L_RG:To_Weight),RIGHT(1),COLOR(00E9E9E9h),DISABLE,MSG('Up to this' & |
  ' mass in Kgs'),TIP('Up to this mass in Kgs')
                           BUTTON('&View Invoice'),AT(74,128,,14),USE(?Button_View_Invoice),DISABLE
                           PROMPT('Rate Effective Date:'),AT(137,114),USE(?Effective_Date:Prompt),TRN
                           SPIN(@d5),AT(217,114,52,10),USE(L_RG:Effective_Date),RIGHT(1),COLOR(00E9E9E9h),DISABLE,MSG('Effective ' & |
  'from this date'),READONLY,SKIP,TIP('Effective from this date')
                           PROMPT('Min. Charge Client:'),AT(137,128),USE(?L_CG:MinimiumCharge:Prompt),TRN
                           ENTRY(@n-11.2),AT(217,128,52,10),USE(L_CG:MinimiumCharge),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           PROMPT('Min. Charge - Rate:'),AT(278,128),USE(?L_CC:MinimiumChargeRate:Prompt),TRN
                           ENTRY(@n-14.3),AT(350,128,52,10),USE(L_CC:MinimiumChargeRate),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           BUTTON('Additional Charges'),AT(217,152,75,10),USE(?Button_AdditionalCharges)
                           CHECK(' Calculate Additional Charge'),AT(302,152),USE(DEL:AdditionalCharge_Calculate),MSG('Calculate ' & |
  'the Additional Charge'),TIP('Calculate the Additional Charge'),TRN
                           PROMPT('Additional Charge Total:'),AT(9,152),USE(?DEL:AdditionalCharge:Prompt),TRN
                           ENTRY(@n-15.2),AT(133,152,52,10),USE(DEL:AdditionalCharge),DECIMAL(12),MSG('Addi'),TIP('Addi')
                           PROMPT('&Charge (based on rate):'),AT(9,170),USE(?DEL:Charge:Prompt),TRN
                           ENTRY(@n-13.2),AT(133,170,52,10),USE(DEL:Charge),RIGHT(1),MSG('Charge for the DI'),TIP('Charge for' & |
  ' the DI<0DH,0AH>Excludes VAT, Docs, Fuel and Insurance charges')
                           PROMPT('+'),AT(190,170),USE(?Prompt25:3),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           PROMPT('Leg costs'),AT(217,170,191,10),USE(?Prompt_LegCosts),FONT(,,COLOR:Black,FONT:bold, |
  CHARSET:ANSI),HIDE,TRN
                           BUTTON('<<>'),AT(334,190,12,10),USE(?Button_ChangeVAT),TIP('Change the VAT Rate')
                           PROMPT('Document Charge:'),AT(9,190),USE(?DEL:DocumentCharge:Prompt),TRN
                           ENTRY(@n-11.2),AT(133,190,52,10),USE(DEL:DocumentCharge),RIGHT(1),COLOR(00E9E9E9h),MSG('Document Charge'), |
  READONLY,SKIP,TIP('Document Charge applied to this DI<0DH,0AH>If you need to change t' & |
  'his please ask a supervisor')
                           PROMPT('+'),AT(190,190),USE(?Prompt25:2),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           BUTTON('Change'),AT(85,190,40,10),USE(?Button_Change_Doc),DISABLE
                           PROMPT('Fuel Surcharge:'),AT(9,204),USE(?DEL:FuelSurcharge:Prompt),TRN
                           PROMPT('VAT Rate:'),AT(285,190),USE(?DEL:VATRate:Prompt),TRN
                           ENTRY(@n7.2),AT(350,190,52,10),USE(DEL:VATRate),DECIMAL(12),COLOR(00E9E9E9h),MSG('VAT Rate'), |
  READONLY,SKIP,TIP('VAT Rate for this delivery')
                           ENTRY(@n-14.2~%~),AT(85,204,40,10),USE(L_CC:FuelSurchargePer),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           ENTRY(@N-11.2),AT(133,204,52,10),USE(DEL:FuelSurcharge),RIGHT(1),COLOR(00E9E9E9h),MSG('Fuel Surcharge'), |
  READONLY,SKIP,TIP('Fuel Surcharge applied to this DI')
                           PROMPT('+'),AT(190,204),USE(?Prompt25:4),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           PROMPT('Total Charge (excl):'),AT(9,222),USE(?TotalCharge:Prompt),FONT(,,COLOR:Black,FONT:bold, |
  CHARSET:ANSI),TRN
                           ENTRY(@n-13.2),AT(133,222,52,10),USE(L_CC:TotalCharge),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Charge for the DI (additional legs have own charg' & |
  'es specified)'),READONLY,SKIP,TIP('Charge for the DI (additional legs have own charg' & |
  'es specified)<0DH,0AH>Excludes VAT')
                           PROMPT('='),AT(190,222),USE(?Prompt25:5),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           PROMPT('VAT:'),AT(285,204),USE(?VAT:Prompt),TRN
                           ENTRY(@n-13.2),AT(350,204,52,10),USE(L_CC:VAT),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Charge for the DI'), |
  READONLY,SKIP,TIP('Charge for the DI<0DH,0AH>Excludes VAT, Docs, Fuel and Insurance charges')
                           PROMPT('Total Charge (incl):'),AT(285,222),USE(?TotalCharge:Prompt:2),TRN
                           ENTRY(@n-13.2),AT(350,222,52,10),USE(L_CC:TotalCharge_VAT),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Charge for the DI'),READONLY,SKIP,TIP('Charge for the DI<0DH,0AH>Excludes VAT, D' & |
  'ocs, Fuel and Insurance charges')
                         END
                         TAB('&3) Delivery Legs, Progress, Special Instr. && Notes'),USE(?Tab:6)
                           PROMPT('Delivery Legs'),AT(189,20),USE(?Prompt19:2),TRN
                           LINE,AT(181,20,0,148),USE(?Line2),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Delivery Progress'),AT(9,20),USE(?Prompt19),TRN
                           LIST,AT(9,32,167,70),USE(?Browse:8),HVSCROLL,FORMAT('60L(2)|M~Status~C(0)@s35@52L(2)|M~' & |
  'Contact Name~C(0)@s35@42R(2)|M~Status Date~C(0)@d5@42R(2)|M~Action Date~C(0)@d5b@42R' & |
  '(2)|M~Status Time~C(0)@t7@42R(2)|M~Action Time~C(0)@t7@'),FROM(Queue:Browse:8),IMM,MSG('Browsing t' & |
  'he DeliveryProgress file')
                           BUTTON('&Insert'),AT(21,104,49,14),USE(?Insert:9),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(73,104,49,14),USE(?Change:9),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(126,104,49,14),USE(?Delete:9),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                           PROMPT('Branch:'),AT(9,130),USE(?DEL:Manifested:Prompt:2),TRN
                           BUTTON('Change'),AT(41,142,50,10),USE(?Button_ChangeBranch),TIP('Change the Branch on t' & |
  'his DI & any associated Invoice')
                           PROMPT('Delivered:'),AT(189,152),USE(?DEL:Delivered:Prompt),TRN
                           LIST,AT(250,152,101,10),USE(DEL:Delivered),DISABLE,DROP(15),FROM('Not Delivered|#0|Part' & |
  'ially Delivered|#1|Delivered|#2|Delivered (manual)|#3'),MSG('Delivered Status'),TIP('Delivered Status')
                           STRING('User'),AT(41,154,101,10),USE(?String_User),TRN
                           PROMPT('User:'),AT(9,154),USE(?Prompt63),TRN
                           STRING('Branch'),AT(41,130,101,10),USE(?String_Branch:2),TRN
                           LIST,AT(189,32,219,70),USE(?Browse:6),HVSCROLL,FORMAT('18R(2)|M~Leg~C(0)@n6@60L(2)|M~Tr' & |
  'ansporter~C(0)@s35@60L(2)|M~Journey~C(0)@s70@38R(1)|M~Total~C(0)@n-17.2@50R(1)|M~Cos' & |
  't~C(0)@n-13.2@38R(1)|M~VAT~C(0)@n-17.2@38R(1)|M~VAT Rate~C(0)@n-7.2@'),FROM(Queue:Browse:6), |
  IMM,MSG('Browsing the DeliveryProgress file')
                           BUTTON('&Insert'),AT(253,104,49,14),USE(?Insert:7),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(305,104,49,14),USE(?Change:7),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(358,104,49,14),USE(?Delete:7),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                           LINE,AT(7,121,225,0),USE(?Line3:4),COLOR(COLOR:Black),LINEWIDTH(2)
                           LINE,AT(183,121,225,0),USE(?Line3:2),COLOR(COLOR:Black),LINEWIDTH(2)
                           CHECK(' &Multiple Manifests Allowed'),AT(250,126),USE(DEL:MultipleManifestsAllowed),MSG('Allow this' & |
  ' DI to be manifested on multiple manifests'),TIP('Allow this DI to be manifested on ' & |
  'multiple manifests'),TRN
                           PROMPT('Manifested:'),AT(189,138),USE(?DEL:Manifested:Prompt),TRN
                           LIST,AT(250,138,101,10),USE(DEL:Manifested),VSCROLL,DISABLE,DROP(15),FROM('Not Manifest' & |
  'ed|#0|Partially Manifested|#1|Partially Manifested Multiple|#2|Fully Manifested|#3|F' & |
  'ully Manifested Multiple|#4'),MSG('Manifested status'),TIP('Manifested status')
                           BUTTON('&Update'),AT(357,138,50,25),USE(?Button_UpdateManifested)
                           LINE,AT(10,166,174,0),USE(?Line3),COLOR(COLOR:Black),LINEWIDTH(2)
                           LINE,AT(183,166,225,0),USE(?Line3:3),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Special Delivery Instructions:'),AT(9,172),USE(?DEL:SpecialDeliveryInstructions:Prompt), |
  TRN
                           TEXT,AT(9,184,167,50),USE(DEL:SpecialDeliveryInstructions),VSCROLL,BOXED,MSG('This will ' & |
  'print on the DI and Delivery Note (& POD)'),TIP('This will print on the DI and Deliv' & |
  'ery Note (& POD)')
                           PROMPT('Notes:'),AT(189,172),USE(?DEL:Notes:Prompt),TRN
                           TEXT,AT(189,184,219,50),USE(DEL:Notes),VSCROLL,BOXED,MSG('General Notes'),TIP('General Notes')
                         END
                         TAB('&4) Multi Part'),USE(?Tab_Multi)
                           GROUP,AT(17,112,311,24),USE(?Group_Captured)
                             PROMPT('Items:'),AT(229,112),USE(?DELC:Items:Prompt:2),TRN
                             ENTRY(@n13),AT(290,112,65,10),USE(L_DC:Items),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total no. of items'), |
  SKIP,TIP('Total no. of items')
                             PROMPT('Weight:'),AT(17,112),USE(?DELC:Weight:Prompt:2),TRN
                             ENTRY(@n-21.2),AT(78,112,65,10),USE(L_DC:Weight),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total weight'), |
  SKIP,TIP('Total weight')
                             PROMPT('Deliveries:'),AT(229,126),USE(?DELC:Deliveries:Prompt:2),TRN
                             ENTRY(@n13),AT(290,126,65,10),USE(L_DC:Deliveries),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total no. ' & |
  'of deliveries'),SKIP,TIP('Total no. of deliveries')
                             PROMPT('Volume:'),AT(17,126),USE(?DELC:Volume:Prompt:2),TRN
                             ENTRY(@n-16.3),AT(78,126,65,10),USE(L_DC:Volume),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total volume'), |
  SKIP,TIP('Total volume'),TRN
                           END
                           GROUP,AT(17,54,311,24),USE(?Group_MultiTotals)
                             PROMPT('Weight:'),AT(17,54),USE(?DELC:Weight:Prompt),TRN
                             ENTRY(@n-21.2),AT(78,54,65,10),USE(DELC:Weight),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total weight'), |
  SKIP,TIP('Total weight')
                             PROMPT('Volume:'),AT(17,68),USE(?DELC:Volume:Prompt),TRN
                             ENTRY(@n-16.3),AT(78,68,65,10),USE(DELC:Volume),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total volume'), |
  SKIP,TIP('Total volume')
                           END
                           PANEL,AT(10,38,396,106),USE(?Panel2),BEVEL(-1,-1)
                           PROMPT('Multi-Part Delivery'),AT(78,42),USE(?Prompt50),FONT(,,,FONT:bold+FONT:underline,CHARSET:ANSI), |
  TRN
                           LINE,AT(18,87,382,0),USE(?Line5),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Deliveries Captured To Date'),AT(78,98),USE(?Prompt50:2),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           BUTTON('Specify Multi-Part'),AT(78,20,,12),USE(?Button_Specify)
                           PROMPT('This Delivery is part of a Multi Part delivery.'),AT(153,22,251,10),USE(?Prompt_Multi), |
  FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         END
                         TAB('DB'),USE(?Tab_DB),HIDE
                           BUTTON('Display'),AT(65,29,95,14),USE(?Button_DB_Display)
                           PROMPT('CRTID:'),AT(65,51),USE(?DEL:CRTID:Prompt),TRN
                           STRING(@n_10),AT(117,51),USE(DEL:CRTID),RIGHT(1),TRN
                           PROMPT('LTID:'),AT(65,63),USE(?DEL:LTID:Prompt),TRN
                           STRING(@n_10),AT(117,63),USE(DEL:LTID),RIGHT(1),TRN
                         END
                       END
                       BUTTON(':'),AT(528,4,9,10),USE(?Button_Value),FONT(,8),TIP('Set Charge option to show here')
                       ENTRY(@n-15.2),AT(569,4,50,10),USE(L_SG:Value),FONT(,,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Charge for the DI (additional legs have own charg' & |
  'es specified)'),READONLY,SKIP,TIP('Charge for the DI (additional legs have own charg' & |
  'es specified)<0DH,0AH>Excludes VAT, Docs and Fuel charges')
                       PROMPT(''),AT(8,353,615,20),USE(?Prompt_Manifest),FONT(,,COLOR:Red,,CHARSET:ANSI)
                       PROMPT('Charge:'),AT(541,4),USE(?Prompt_Value),TRN
                       BUTTON('&OK'),AT(513,381,49,20),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('Cancel'),AT(567,381,,20),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(2,387,49,9),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW4::LastSortOrder       BYTE
BRW20::LastSortOrder       BYTE
BRW6::LastSortOrder       BYTE
BRW8::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW_DeliveryItems    CLASS(BrowseClass)                    ! Browse using ?Browse:ItemsContainers
Q                      &Queue:Browse:4                !Reference to browse queue
AskRecord              PROCEDURE(BYTE Request),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW4::Sort0:StepClass StepLongClass                        ! Default Step Manager
BRW_DelItemsAlias    CLASS(BrowseClass)                    ! Browse using ?Browse:ItemsLoose
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW20::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW6                 CLASS(BrowseClass)                    ! Browse using ?Browse:6
Q                      &Queue:Browse:6                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW6::Sort0:StepClass StepLongClass                        ! Default Step Manager
BRW8                 CLASS(BrowseClass)                    ! Browse using ?Browse:8
Q                      &Queue:Browse:8                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:StepClass StepRealClass                        ! Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB27                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Calendar22           CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
!ManLD           VIEW(ManifestLoadDeliveries)
!    PROJECT(MALD:DIID, MALD:MLID)
!       JOIN(MAL:PKey_MLID, MALD:MLID)
!       PROJECT(MAL:MLID, MAL:MID)
!    .  .
View_Inv        VIEW(_Invoice)
    PROJECT(INV:IID)
    .

Inv_View        ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
New_Journey                 ROUTINE
    CLEAR(L_CC:Rate_Checked)

    ! Here we should check that the journey has actually changed.
    DO Charge_Calc
    DISPLAY
    EXIT
New_Load_Type           ROUTINE
    CLEAR(L_CC:Rate_Checked)

    LOAD2:LTID                  = DEL:LTID
    IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
       ENABLE(?Sheet_Items)

       LOC:Load_Type_Group     :=: LOAD2:Record
!       IF L_LT:ContainerOption = 1 OR L_LT:ContainerOption = 2
!          SELECT(?Tab_Containers)
!       ELSE
!          SELECT(?Tab_Loose)
!       .

       ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
       IF L_LT:ContainerParkStandard = TRUE
          DEL:FIDRate           = LOAD2:FID
          FLO:FID               = DEL:FIDRate
          IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
             L_LG:FloorRate     = FLO:Floor
             DISPLAY(?L_LG:FloorRate)
       .  .
          
       ! Check how many CRTIDs are available for this Load Type LTID for this Client - 3 Oct 12
       DEL:CRTID    = Check_ClientsRateTypes(DEL:CID, DEL:LTID)
       IF DEL:CRTID = 0 AND L_LG:LTID <> LOAD2:LTID       ! we have none and we did change load types, then set to zero
          ! MESSAGE('crtid is 0..')
          
          CLEAR(DEL:CRTID)
          CLEAR(L_LG:ClientRateType)
          CLEAR(L_LG:ClientRateType_CID)
       .
       IF DEL:CRTID ~= 0
          CRT:CRTID         = DEL:CRTID
          IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
             L_LG:ClientRateType        = CRT:ClientRateType
             L_LG:ClientRateType_CID    = CRT:CID
       .  .
          
    ELSE
       DISABLE(?Sheet_Items)
    .

    L_LG:LTID = LOAD2:LTID            ! set last load type seen here
          
    DO Floor_Changed
    DO Charge_Calc
    EXIT
New_Client                    ROUTINE
    ! Check if Client is On Hold
    IF L_CG:ClientStatus = 1
       MESSAGE('This Client is on Hold!', 'Update Deliveries - New Client', ICON:Exclamation)
    ELSIF L_CG:ClientStatus = 2
       MESSAGE('This Client account has been closed!', 'Update Deliveries - New Client', ICON:Hand)
       CLEAR(DEL:CID)
       CLEAR(L_LG:ClientName)
       CLEAR(L_LG:ClientNo)
       SELECT(?L_LG:ClientName)
       EXIT
	.
	
    ! Pre Paid, COD, Account
    IF DEL:Terms < 2
       !DISABLE(?L_LG:To_Address)
       !?L_LG:To_Address{PROP:Req}   = FALSE
       !ENABLE(?Group_COD_Add)
       ENABLE(?Button_COD_Address)
       ?Button_COD_Address{PROP:FontColor}  = COLOR:Red
    ELSE
       !ENABLE(?L_LG:To_Address)
       !?L_LG:To_Address{PROP:Req}   = TRUE
       !DISABLE(?Group_COD_Add)
       DISABLE(?Button_COD_Address)
       ?Button_COD_Address{PROP:FontColor}  = COLOR:None
       CLEAR(DEL:DC_ID)
    .


    IF L_LG:CID ~= DEL:CID                                  ! Client has changed.  This does not execute on Change, Delete unless client changed
       CLEAR(L_CG:MinChargeChecked)
       CLEAR(L_CG:UseMinCharge)
	   CLEAR(L_CC:Rate_Checked)
		
       IF QuickWindow{PROP:AcceptAll}  = FALSE
		  IF CLIP(L_LG:PODMessage) <> ''
			 MESSAGE(CLIP(CLI:PODMessage), 'POD Message', ICON:Exclamation)			 
		  .

          ! Check if 1 Journey - if so load these now as defaults
          DEL:JID              = Check_ClientRates_for_JID(DEL:CID, GETINI('Manifest', 'Default_Manifest_JID', 0, GLO:Local_INI))

          JOU:JID              = DEL:JID
          IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
             L_LG:Journey      = JOU:Journey
          ELSE
             CLEAR(L_LG:Journey)
          .

          ! Check if 1 Load Type if so load these now as defaults
          Count_#              = 0
          CLEAR(V_RATC_L:Record)
          V_RATC_L:CID         = DEL:CID
          SET(V_RATC_L:CKey_CID_LTID, V_RATC_L:CKey_CID_LTID)
          LOOP  
             IF Access:_View_Rates_Clients_LoadTypes.TryNext() ~= LEVEL:Benign
                BREAK
             .
             IF V_RATC_L:CID ~= DEL:CID
                BREAK
             .

             IF DEL:LTID ~= V_RATC_L:LTID   ! 1st time and then when differs
                Count_#   += 1
             .            
             
             DEL:LTID   = V_RATC_L:LTID
          .
          IF Count_# > 1
             CLEAR(DEL:LTID)
             CLEAR(L_LG:LoadType)          
          .

          IF DEL:LTID ~= 0             
             LOAD2:LTID        = DEL:LTID
             IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
                L_LG:LoadType  = LOAD2:LoadType
             .
             DO New_Load_Type
          .  

          ! Check Journeys available       
          Count_#              = 0
          CLEAR(V_RCJ:Record)
          V_RCJ:CID            = DEL:CID
          SET(V_RCJ:CKey_CID_JID, V_RCJ:CKey_CID_JID)
          LOOP  
             IF Access:_View_Rates_Client_Journeys.TryNext() ~= LEVEL:Benign
                BREAK
             .
             IF V_RCJ:CID ~= DEL:CID
                BREAK
             .

             IF DEL:JID ~= V_RCJ:JID   ! 1st time and then when differs
                Count_#   += 1
             .            
             
             DEL:JID   = V_RCJ:JID
          .
          IF Count_# > 1
             CLEAR(DEL:JID)
             CLEAR(L_LG:Journey)
          .

          IF DEL:JID ~= 0
             JOU:JID        = DEL:JID
             IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
                L_LG:Journey  = JOU:Journey
          .  .

          L_CC:FuelSurchargePer     = Get_Client_FuelSurcharge(DEL:CID, DEL:DIDate)
       .

       ! Check how many CRTIDs are available for this Load Type LTID for this Client
       DEL:CRTID    = Check_ClientsRateTypes(DEL:CID, DEL:LTID)
       IF DEL:CRTID = 0
          CLEAR(DEL:CRTID)
          CLEAR(L_LG:ClientRateType)
          CLEAR(L_LG:ClientRateType_CID)
       .
       IF DEL:CRTID ~= 0
          CRT:CRTID         = DEL:CRTID
          IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
             L_LG:ClientRateType        = CRT:ClientRateType
             L_LG:ClientRateType_CID    = CRT:CID
       .  .
       
       ! Override the clients percentage with the current rate
       IF DEL:FuelSurcharge ~= 0.0
          ! If we have data, set to this percentage rather
          L_CC:FuelSurchargePer = (DEL:FuelSurcharge / DEL:Charge) * 100
       .

       DO Charge_Calc
    .
    L_LG:CID                = DEL:CID
    EXIT
New_Client_Old                    ROUTINE
    ! Check if Client is On Hold
    IF L_CG:ClientStatus = 1
       MESSAGE('This Client is on Hold!', 'Update Deliveries - New Client', ICON:Exclamation)
    ELSIF L_CG:ClientStatus = 2
       MESSAGE('This Client account has been closed!', 'Update Deliveries - New Client', ICON:Hand)
       CLEAR(DEL:CID)
       CLEAR(L_LG:ClientName)
       CLEAR(L_LG:ClientNo)
       SELECT(?L_LG:ClientName)
       EXIT
	.
	
    ! Pre Paid, COD, Account
    IF DEL:Terms < 2
       !DISABLE(?L_LG:To_Address)
       !?L_LG:To_Address{PROP:Req}   = FALSE
       !ENABLE(?Group_COD_Add)
       ENABLE(?Button_COD_Address)
       ?Button_COD_Address{PROP:FontColor}  = COLOR:Red
    ELSE
       !ENABLE(?L_LG:To_Address)
       !?L_LG:To_Address{PROP:Req}   = TRUE
       !DISABLE(?Group_COD_Add)
       DISABLE(?Button_COD_Address)
       ?Button_COD_Address{PROP:FontColor}  = COLOR:None
       CLEAR(DEL:DC_ID)
    .


    IF L_LG:CID ~= DEL:CID                                  ! Client has changed.  This does not execute on Change, Delete unless client changed
       CLEAR(L_CG:MinChargeChecked)
       CLEAR(L_CG:UseMinCharge)
	   CLEAR(L_CC:Rate_Checked)
		
       IF QuickWindow{PROP:AcceptAll}  = FALSE
		  IF CLIP(L_LG:PODMessage) <> ''
			 MESSAGE(CLIP(CLI:PODMessage), 'POD Message', ICON:Exclamation)			 
		  .

          ! Check if 1 Journey - if so load these now as defaults
          DEL:JID              = Check_ClientRates_for_JID(DEL:CID, GETINI('Manifest', 'Default_Manifest_JID', 0, GLO:Local_INI))

          JOU:JID              = DEL:JID
          IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
             L_LG:Journey      = JOU:Journey
          ELSE
             CLEAR(L_LG:Journey)
          .


          ! Check if 1 Load Type if so load these now as defaults
          Count_#              = 0
          CLEAR(V_RCJ:Record)
          V_RCJ:CID            = DEL:CID
          SET(V_RCJ:CKey_CID_JID, V_RCJ:CKey_CID_JID)
          LOOP
             IF Access:_View_Rates_Client_Journeys.TryNext() ~= LEVEL:Benign
                BREAK
             .
             IF V_RCJ:CID ~= DEL:CID
                BREAK
             .

             IF DEL:LTID ~= V_RATC_L:LTID   ! 1st time and then when differs
                Count_#   += 1
             .            
             
             DEL:LTID   = V_RATC_L:LTID
          .
          IF Count_# > 1
             CLEAR(DEL:LTID)
             CLEAR(L_LG:LoadType)
          ELSIF Count_# = 1
             DEL:LTID          = V_RATC_L:LTID
          .

          IF DEL:LTID ~= 0
             LOAD2:LTID        = DEL:LTID
             IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
                L_LG:LoadType  = LOAD2:LoadType
          .  .

          L_CC:FuelSurchargePer     = Get_Client_FuelSurcharge(DEL:CID, DEL:DIDate)

          
       .

       ! Check how many CRTIDs are available for this Load Type LTID for this Client
       DEL:CRTID    = Check_ClientsRateTypes(DEL:CID, DEL:LTID)
       IF DEL:CRTID = 0
          CLEAR(DEL:CRTID)
          CLEAR(L_LG:ClientRateType)
          CLEAR(L_LG:ClientRateType_CID)
       .
       IF DEL:CRTID ~= 0
          CRT:CRTID         = DEL:CRTID
          IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
             L_LG:ClientRateType        = CRT:ClientRateType
             L_LG:ClientRateType_CID    = CRT:CID
       .  .

       ! Override the clients percentage with the current rate
       IF DEL:FuelSurcharge ~= 0.0
          ! If we have data, set to this percentage rather
          L_CC:FuelSurchargePer = (DEL:FuelSurcharge / DEL:Charge) * 100
       .

       DO Charge_Calc
    .
    L_LG:CID                = DEL:CID
    EXIT
Charge_Calc                  ROUTINE
    DATA
R:Order     BYTE
R:Charge    LIKE(L_CC:Charge)

    CODE
    IF ThisWindow.Request ~= ViewRecord

!                                      ! (p:DID, p:SelectedRate, p:Mass, p:MinimiumChargeRate, p:To_Weight, p:Effective_Date)
!    IF Delivery_Rates(DEL:DID, L_RG:Returned_Rate, L_CC:TotalWeight, L_CC:MinimiumChargeRate, L_RG:To_Weight, L_RG:Effective_Date) > 0
!       DEL:Rate            = L_RG:Returned_Rate
!
!       L_RG:Selected_Rate  = DEL:Rate
!
!       L_LG:Rate_Specified = TRUE
!    .
!

!     db.debugout('[Update_Deliveries]  Charge_Calc - Start')

       ! We only want to do this if there are some items captured.
       IF Get_DelItem_s_Totals(DEL:DID, 3,,, TRUE) > 0         ! No cache = TRUE
   !       IF ThisWindow{PROP:AcceptAll} ~= TRUE
   !  db.debugout('Update_Deliveries - Charge_Calc - after Get_DelItems.. 3 - DEL:DID: ' & DEL:DID)

   !          ThisWindow.Update

!     db.debugout('[Update_Deliveries]  Charge_Calc - after Get_DelItems.. 0 after update')

             L_CC:TotalWeight                  = Get_DelItem_s_Totals(DEL:DID,0,,,TRUE) / 100  ! Returns a Ulong
             L_CC:TotalUnits                   = Get_DelItem_s_Totals(DEL:DID,3,,,TRUE)        ! Get items (used in del comb)

!     db.debugout('[Update_Deliveries]  Charge_Calc - after Get_DelItems.. 0 before Reset')

   !          ThisWindow.Reset(1)

             IF L_LG:Charge_Specified = TRUE                    ! Make current rate match
                DEL:Rate                       = DEL:Charge / L_CC:TotalWeight
             .

             DO Rate_Calc                                                                ! Uses the L_CC:TotalWeight

!     db.debugout('[Update_Deliveries]  Charge_Calc - after Rate_Calc.. 0 - Charge_Specified: ' & L_LG:Charge_Specified & ', DEL:Rate: ' & DEL:Rate)

             ! Calc begins......
             IF L_LG:Charge_Specified = FALSE                   ! User has not set a charge
                L_CC:Charge                    = L_CC:TotalWeight * DEL:Rate
             ELSE
                L_CC:Charge                    = DEL:Charge
             .

             R:Charge      = L_CC:Charge

             LOOP 2 TIMES
                R:Order    += 1

                IF R:Order = 3         ! only 2nd loop - 1 not checked
                   R:Order = 1
                ELSE
                   IF R:Charge < L_CG:MinimiumCharge
                      IF R:Charge < L_CC:MinimiumChargeRate
                         ! The only condition that it matters
                         IF L_CG:MinimiumCharge < L_CC:MinimiumChargeRate
                            R:Order   = 2         ! Do bigger question 1st, if yes then will skip question 2!
                .  .  .  .

                IF R:Order = 1
                   ! If we are using a minimium weight then we cannot just set the Charge
                   ! Clients Minimium Charge
                   IF R:Charge < L_CG:MinimiumCharge
                      ! Checked is reset when Client is changed.
                      IF L_CG:MinChargeChecked = FALSE
                         CASE MESSAGE('The Delivery Charge is less then the Clients Minimum Charge.||Would you like to set the Charge to the Minimum Charge for this client of: ' & CLIP(LEFT(FORMAT(L_CG:MinimiumCharge,@n-12.2))), 'Charge Calc.', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                         OF BUTTON:Yes
                            DEL:Charge               = L_CG:MinimiumCharge
                            L_CG:UseMinCharge        = TRUE
                            R:Charge                 = DEL:Charge
                         .
                         L_CG:MinChargeChecked       = TRUE
                      .
                   ELSE
                      IF DEL:Charge = L_CG:MinimiumCharge
                      ELSE
                         L_CG:UseMinCharge           = FALSE
                         L_CG:MinChargeChecked       = FALSE
                .  .  .

                IF R:Order = 2
                   IF R:Charge < L_CC:MinimiumChargeRate
                      ! Checked is reset when Rate changes.
                      IF L_CC:MinChargeRateChecked = FALSE
                         CASE MESSAGE('The Delivery Charge is less then the Rates Minimum Charge.||Would you like to set the Charge to the Minimum Charge for this Rate of: ' & CLIP(LEFT(FORMAT(L_CC:MinimiumChargeRate,@n-12.2))), 'Charge Calc.', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                         OF BUTTON:Yes
                            DEL:Charge               = L_CC:MinimiumChargeRate
                            L_CC:UseMinRateCharge    = TRUE
                            R:Charge                 = DEL:Charge
                         .
                         L_CC:MinChargeRateChecked   = TRUE
                      .
                   ELSE
                      IF DEL:Charge = L_CC:MinimiumChargeRate
                      ELSE
                         L_CC:UseMinRateCharge       = FALSE
                         L_CC:MinChargeRateChecked   = FALSE
             .  .  .  .

             IF (L_CC:UseMinRateCharge = TRUE OR L_CG:UseMinCharge = TRUE)
                DEL:Rate                      = DEL:Charge / L_CC:TotalWeight
             ELSE
                IF L_LG:Charge_Specified = FALSE                   ! User has not set a charge
                   DEL:Charge                 = L_CC:Charge
                ELSE
                   DEL:Rate                   = DEL:Charge / L_CC:TotalWeight
       .  .  .  !.

       IF DEL:Insure = TRUE
          L_CC:InsuanceAmount              = DEL:TotalConsignmentValue * (DEL:InsuranceRate / 100)
       .

       IF L_CC:FuelSurchargePer > 0
          DEL:FuelSurcharge                = DEL:Charge * (L_CC:FuelSurchargePer / 100)
       .

       L_CC:TotalCharge                    = DEL:Charge + DEL:AdditionalCharge + DEL:DocumentCharge + DEL:FuelSurcharge + L_CC:InsuanceAmount

       L_CC:VAT                = L_CC:TotalCharge * (DEL:VATRate / 100)
       L_CC:TotalCharge_VAT    = L_CC:TotalCharge + L_CC:VAT

       DISPLAY

     db.debugout('[Update_Deliveries]  Charge_Calc - End.. 0 - DEL:DID: ' & DEL:DID)
    .

    DO Value_Type
    EXIT
Rate_Calc                      ROUTINE
    DATA
R:MinimiumChargeRate        LIKE(L_CC:MinimiumChargeRate)
R:To_Weight                 LIKE(L_RG:To_Weight)
R:Effective_Date            LIKE(L_RG:Effective_Date)
R:Setup_Rate                LIKE(L_RG:Setup_Rate)

    CODE
    ! Dont do calc when OK on window.............   sure ??
    IF QuickWindow{PROP:AcceptAll} = FALSE AND L_CC:Rate_Checked = FALSE AND ThisWindow.Request ~= ViewRecord
  db.debugout('[Update_Deliveries]  Rate_Calc - Start')
       ! Calculate the rate when?
       ! When inserting records or when something changes?
       ! Warn if something changes and the rate would change too.

       IF DEL:CID ~= 0 AND DEL:JID ~= 0 AND DEL:LTID ~= 0 AND L_CC:TotalWeight > 0.0
          CLEAR(L_CC:MinChargeRateChecked)
          CLEAR(L_CC:UseMinRateCharge)
          L_CC:Rate_Checked        = TRUE

                                                    ! (p:DID, p:CID, p:JID, p:CRTID, p:FID, p:Mass, p:Eff_Date, <*p:Mincharge>, |
                                                    ! <p:Setup>, <p:To_Weight>, <p:Effective_Date>, <p:RateID>)
           LOC:Rate_Group_Ret       = Get_Delivery_Rate(DEL:DID, DEL:CID, DEL:JID, DEL:CRTID, DEL:FIDRate, L_CC:TotalWeight,  |
                                                        DEL:DIDate, R:MinimiumChargeRate, R:Setup_Rate,               |
                                                        R:To_Weight, R:Effective_Date, L_RG:ID) ! was L_RG:Setup_Rate
          L_LG:Next_Rate           = L_RG:RatePerKg

          ! Note L_RG:ID can be the CP Rate, CP Client Discount Rate, or the Rate ID.
          IF L_RG:ID = 0 AND L_LG:Rate_Specified = FALSE
             MESSAGE('No rate was found.', 'Rate Calculation', ICON:Exclamation)
          .

    db.debugout('[Update_Deliveries]  Checking rate - found rate: ' & L_LG:Next_Rate & ',  current rate: ' & DEL:Rate & ',  Charge_Specified: ' & L_LG:Charge_Specified)

          IF L_LG:Next_Rate = 0.0
             SELECT(?DEL:Rate)
          .

          ! If we previously had a Rate and the New Rate will differ then warn the user
          !IF L_LG:Last_Rate ~= L_LG:Next_Rate AND (DEL:Rate ~= 0.0 AND (L_LG:Next_Rate ~= 0.0 OR LOC:Request = InsertRecord))
          IF DEL:Rate ~= L_LG:Next_Rate AND (DEL:Rate ~= 0.0 AND (L_LG:Next_Rate ~= 0.0 OR LOC:Request = InsertRecord))
             ! AND L_LG:Last_No_Items ~= L_LG:No_Items
             IF L_LG:Rate_Specified = FALSE
                ! Specified in the messages text means client rate specified, not user selected specified.
                CASE MESSAGE('The Rate used on this delivery (' & DEL:Rate & ') differs to the specified rate (' & L_LG:Next_Rate & ').' |
                             & '||Would you like to update the Delivery Rate with the new rate?', |
                             'Rate', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                OF BUTTON:Yes
                   L_RG:Selected_Rate       = 0.0
                   L_LG:Rate_Specified      = FALSE
                   L_LG:Charge_Specified    = FALSE

                   DEL:Rate                 = L_LG:Next_Rate

                   L_CC:MinimiumChargeRate  = R:MinimiumChargeRate
                   L_RG:To_Weight           = R:To_Weight     
                   L_RG:Effective_Date      = R:Effective_Date
                   L_RG:Setup_Rate          = R:Setup_Rate

                   DISPLAY
             .  .

             L_LG:Last_Rate        = L_LG:Next_Rate
             L_LG:Last_No_Items    = L_LG:No_Items

             !L_LG:CID              = DEL:CID
             !L_LG:JID              = DEL:JID
             !L_LG:LTID             = DEL:LTID
          ELSIF LOC:Request = InsertRecord AND DEL:Rate = 0.0 AND L_LG:Rate_Specified = FALSE
             L_CC:MinimiumChargeRate    = R:MinimiumChargeRate
             L_RG:To_Weight             = R:To_Weight
             L_RG:Effective_Date        = R:Effective_Date
             L_RG:Setup_Rate            = R:Setup_Rate

             DEL:Rate                   = L_LG:Next_Rate
             DISPLAY(?DEL:Rate)
          .

          IF L_RG:Selected_Rate = DEL:Rate AND L_RG:Selected_Rate ~= L_LG:Next_Rate
             ?String_Rate_Info{PROP:Text}   = 'Selected Rate used.'
          ELSIF L_RG:Setup_Rate = TRUE
             ?String_Rate_Info{PROP:Text}   = 'Default Rate used.'
          ELSIF L_RG:ID ~= 0
             ?String_Rate_Info{PROP:Text}   = 'Client Rate used.'
          ELSE
             ?String_Rate_Info{PROP:Text}   = 'No Rate found.'
       .  .

  db.debugout('[Update_Deliveries]  Rate_Calc - End')
    .
    EXIT
Validate_Col_Add            ROUTINE
    ADD:AID             = DEL:CollectionAID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       ?L_LG:From_Address{PROP:Tip}     = 'Collection Address'
       IF ADD:Client = TRUE AND ADD:Delivery = FALSE
          MESSAGE('The Collection address specified is a Client address and not a Delivery address.||Please select another address.', 'Collection Address', ICON:Exclamation)
          CLEAR(DEL:CollectionAID)
          CLEAR(L_LG:From_Address)
          SELECT(?L_LG:From_Address)
       ELSE
          DO Collection_Add_Tip
    .  .
    EXIT



Collection_Add_Tip            ROUTINE
    DATA
R_Return_Group           GROUP,PRE(R)
Suburb                     STRING(50) !Suburb
PostalCode                 STRING(10)
Country                    STRING(50) !Country
Province                   STRING(35) !Province
City                       STRING(35) !City
Found                      BYTE
                         END

R:Details               STRING(250)
    CODE
    R_Return_Group    = Get_Suburb_Details(ADD:SUID)
    IF R:Found = TRUE
       R:Details         = 'Collection Address (' & CLIP(R:City) & ')<10>'
       IF CLIP(ADD:Line1) ~= ''
          R:Details      = CLIP(R:Details) & CLIP(ADD:Line1) & '<10>'
       .
       IF CLIP(ADD:Line2) ~= ''
          R:Details      = CLIP(R:Details) & CLIP(ADD:Line2) & '<10>'
       .
       R:Details         = CLIP(R:Details) & CLIP(R:PostalCode) & '<10>' & |
                                             CLIP(R:Province)
    ELSE
       R:Details         = 'Collection Address<10>(Suburb info. missing - SUID: ' & ADD:SUID & ',  ColAID: ' & DEL:CollectionAID & ')'
    .
    ?L_LG:From_Address{PROP:Tip}     = CLIP(R:Details)
    EXIT
Validate_Del_Add            ROUTINE
    ADD:AID             = DEL:DeliveryAID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       ?L_LG:To_Address{PROP:Tip}           = 'Delivery Address'

       IF ADD:Client = TRUE AND ADD:Delivery = FALSE
          MESSAGE('The Delivery address specified is a Client address and not a Delivery address.||Please select another address.', 'Collection Address', ICON:Exclamation)
          CLEAR(DEL:DeliveryAID)
          CLEAR(L_LG:To_Address)
          SELECT(?L_LG:To_Address)
       ELSE
          DO Delivery_Add_Tip
    .  .
    EXIT


Delivery_Add_Tip            ROUTINE
    DATA
R_Return_Group           GROUP,PRE(R)
Suburb                     STRING(50) !Suburb
PostalCode                 STRING(10)
Country                    STRING(50) !Country
Province                   STRING(35) !Province
City                       STRING(35) !City
Found                      BYTE
                         END

R:Details               STRING(250)

    CODE
    R_Return_Group    = Get_Suburb_Details(ADD:SUID)

    IF R:Found = TRUE
       R:Details         = 'Delivery Address (' & CLIP(R:City) & ')<10>'
       IF CLIP(ADD:Line1) ~= ''
          R:Details      = CLIP(R:Details) & CLIP(ADD:Line1) & '<10>'
       .
       IF CLIP(ADD:Line2) ~= ''
          R:Details      = CLIP(R:Details) & CLIP(ADD:Line2) & '<10>'
       .
       R:Details         = CLIP(R:Details) & CLIP(R:PostalCode) & '<10>' & |
                                             CLIP(R:Province)
    ELSE
       R:Details         = 'Delivery Address<10>(Suburb info. missing - SUID: ' & ADD:SUID & ',  DelAID: ' & DEL:DeliveryAID & ')'
    .
    ?L_LG:To_Address{PROP:Tip}     = CLIP(R:Details)
    EXIT
Floor_Changed               ROUTINE
    ! Check load type is Container Park or not
    ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery

    IF L_LT:ContainerParkStandard = TRUE
       ! Load type is Container Park Special
    ELSIF L_LT:LoadOption = 1                   ! Container Park
       ENABLE(?Group_RateFloor)
       IF DEL:FIDRate = 0
          DEL:FIDRate       = DEL:FID
          CLEAR(L_LG:FloorRate)
       .

       IF CLIP(L_LG:FloorRate) = ''
          ! Load String
          FLO:FID           = DEL:FIDRate
          IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
             L_LG:FloorRate = FLO:Floor
       .  .
    ELSE
       CLEAR(DEL:FIDRate)
       DISABLE(?Group_RateFloor)
    .
    EXIT
Get_Rate_Button           ROUTINE
    DATA
R:MinimiumChargeRate        LIKE(L_CC:MinimiumChargeRate)
R:To_Weight                 LIKE(L_RG:To_Weight)
R:Effective_Date            LIKE(L_RG:Effective_Date)
R:Setup_Rate                LIKE(L_RG:Setup_Rate)

    CODE
    IF DEL:CID ~= 0 AND DEL:JID ~= 0 AND DEL:LTID ~= 0
       CLEAR(L_CC:MinChargeRateChecked)
       CLEAR(L_CC:UseMinRateCharge)
                                        ! (p:DID, p:CID, p:JID, p:CRTID, p:FID, p:Mass, p:Eff_Date, <*p:Mincharge>, |
                                        ! <p:Setup>, <p:To_Weight>, <p:Effective_Date>, <p:RateID>)
       LOC:Rate_Group_Ret = Get_Delivery_Rate(DEL:DID, DEL:CID, DEL:JID, DEL:CRTID, DEL:FIDRate, L_CC:TotalWeight, DEL:DIDate, |
                                              R:MinimiumChargeRate, R:Setup_Rate, R:To_Weight, |
                                              R:Effective_Date, L_RG:ID)
       L_LG:Next_Rate     = L_RG:RatePerKg

       IF L_RG:ID = 0
          MESSAGE('No rate was found.', 'Rate Calculation', ICON:Exclamation)
       ELSE
          CASE MESSAGE('The calculated rate would be: ' & L_LG:Next_Rate & '|With a minimium Charge of: ' & L_CC:MinimiumChargeRate    |
                       & '||Would you like to update the rate and charges now?', |
                       'Rate Calculation', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
          OF BUTTON:Yes
             CLEAR(L_CC:Rate_Checked)
             CLEAR(L_LG:Charge_Specified)
             CLEAR(L_LG:Rate_Specified)

             DEL:Rate               = L_LG:Next_Rate
             L_LG:Last_Rate         = L_LG:Next_Rate

             L_RG:Selected_Rate     = 0.0
             L_LG:Rate_Specified    = FALSE

             L_CC:MinimiumChargeRate  = R:MinimiumChargeRate
             L_RG:To_Weight           = R:To_Weight     
             L_RG:Effective_Date      = R:Effective_Date
             L_RG:Setup_Rate          = R:Setup_Rate

             DO Charge_Calc
    .  .  .
    EXIT
Date_Changed                ROUTINE                 ! Fuel Surcharge loaded here
    IF QuickWindow{PROP:AcceptAll}  = FALSE
       L_CC:FuelSurchargePer     = Get_Client_FuelSurcharge(DEL:CID, DEL:DIDate)
       DO Charge_Calc
       DISPLAY
    .
    EXIT
Apply_User_Permissions    ROUTINE               ! Special user permissions
    IF ThisWindow.Request = ViewRecord AND LOC:Original_Request = ChangeRecord
       ! (ULONG, STRING, STRING, <STRING>, BYTE=0, <*BYTE>),LONG
       ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned)

       ! Returns
       !   0   - Allow (default)
       !   1   - Disable
       !   2   - Hide
       !   100 - Allow                 - Default action
       !   101 - Disallow              - Default action

       CASE Get_User_Access(GLO:UID, 'Deliveries', 'Update_Deliveries', 'Change_Mode', 1)
       OF 0 OROF 100
          ! This user is allowed to change applications at this stage... only super user!
          ThisWindow.Request = ChangeRecord
       OF 2
       ELSE
    .  .
    EXIT
Value_Type                      ROUTINE
    EXECUTE L_SG:Value_Type
       L_SG:Value               = DEL:Charge
       L_SG:Value               = L_CC:TotalCharge
       L_SG:Value               = L_CC:TotalCharge_VAT
    .
    EXECUTE L_SG:Value_Type
       ?L_SG:Value{PROP:Tip}    = 'Delivery Charge (excl. VAT, Additional Charges, Docs., Fuel Surcharge & Insurance)'
       ?L_SG:Value{PROP:Tip}    = 'Total Charge (excl. VAT & incl. Additional Charges, Docs., Fuel Surcharge & Insurance)'
       ?L_SG:Value{PROP:Tip}    = 'Total Charge (all incl.)'
    .
    DISPLAY(?L_SG:Value)
    EXIT
Load_Lookups                ROUTINE
    ! Load lookup fields & additionals
    CLI:CID                     = DEL:CID
    IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
       L_LG:ClientName          = CLI:ClientName

       ! What if these change on the Client record???
       L_CG:MinimiumCharge      = CLI:MinimiumCharge          ! This is also specified on some rates???

       IF LOC:Request ~= ChangeRecord AND LOC:Request ~= ViewRecord
          L_CC:FuelSurchargePer = Get_Client_FuelSurcharge(CLI:CID, DEL:DIDate)         !CLI:FuelSurcharge
          DO Charge_Calc
       ELSIF DEL:FuelSurcharge ~= 0.0
          ! If we have data, set to this percentage rather
          L_CC:FuelSurchargePer = (DEL:FuelSurcharge / DEL:Charge) * 100
       .

       L_CG:GenerateInvoice     = CLI:GenerateInvoice

       L_LG:ClientNo            = CLI:ClientNo

       IF LOC:Request = InsertRecord
          DEL:DocumentCharge    = CLI:DocumentCharge
    .  .

    JOU:JID             = DEL:JID
    IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
!       db.debugout('[Update_Deliveries]  DEL:JID: ' & DEL:JID & ' - JOU:Journey: ' & JOU:Journey)
       L_LG:Journey     = JOU:Journey
    .

    ADD:AID             = DEL:CollectionAID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       L_LG:From_Address = ADD:AddressName
    ELSE
       CLEAR(ADD:Record)
    .
    DO Collection_Add_Tip

    ADD:AID             = DEL:DeliveryAID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
       L_LG:To_Address  = ADD:AddressName
    ELSE
       CLEAR(ADD:Record)
    .
    DO Delivery_Add_Tip

    LOAD2:LTID          = DEL:LTID
    IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
       L_LG:LoadType    = LOAD2:LoadType
    .

    CRT:CRTID           = DEL:CRTID
    IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
       L_LG:ClientRateType        = CRT:ClientRateType
       L_LG:ClientRateType_CID    = CRT:CID
    .

    SERI:SID            = DEL:SID
    IF Access:ServiceRequirements.TryFetch(SERI:PKey_SID) = LEVEL:Benign
       L_LG:ServiceRequirement  = SERI:ServiceRequirement
    .

    FLO:FID             = DEL:FID
    IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
       L_LG:Floor       = FLO:Floor
    .

    DCADD:DC_ID         = DEL:DC_ID
    IF Access:Delivery_CODAddresses.TryFetch(DCADD:PKey_DC_ID) = LEVEL:Benign
       L_LG:COD_Address = DCADD:AddressName
    .

    ! Pre Paid, COD, Account
    IF DEL:Terms < 2
       ENABLE(?Button_COD_Address)
    ELSE
       DISABLE(?Button_COD_Address)
    .

    ! Load Type
    LOAD2:LTID              = DEL:LTID
    IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
       LOC:Load_Type_Group :=: LOAD2:Record
    .

    EXIT
! ----------------------------------------------------------------      Delivery Compositions
Get_DelComp                 ROUTINE
    DATA
R:DELCID            LIKE(DEL:DELCID)
R:DELCID_Ext        LIKE(DEL:DELCID)

    CODE
    R:DELCID_Ext    = DEL:DELCID

    ! User will know from the button what action will be performed.
    ThisWindow.Update
    GlobalRequest   = SelectRecord
    R:DELCID        = Browse_DeliveryCompositions()

    IF DEL:DELCID ~= 0 AND DEL:DELCID ~= R:DELCID
       ! Previous DELCID not equal to chosen one - warn user
       CASE MESSAGE('You have selected a different Multi-Part Delivery to the previous one specified for this Delivery.||' & |
                    'Is this correct?', 'Multi-Part Delivery', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
       OF BUTTON:No
          CLEAR(R:DELCID)
       OF BUTTON:Yes
          IF R:DELCID = 0
             DEL:DELCID = 0                                 ! The other cases are delt with below
    .  .  .

    IF R:DELCID ~= 0
       DEL:DELCID           = R:DELCID
       DO Check_DelComp

       IF DEL:DELCID ~= 0
          IF L_DC2:Start_of_Insert = FALSE
             CASE MESSAGE('Would you like to use the previous Deliveries on this Multi-Part to create this delivery?', |
                        'Multi-Part Delivery', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
             OF BUTTON:Yes
                L_DC2:Start_of_Insert   = TRUE
          .  .

          IF L_DC2:Start_of_Insert = TRUE
             IF L_DC:First_DID = 0
                DELC:DELCID = R:DELCID
                IF Access:DeliveryComposition.TryFetch(DELC:PKey_DELCID) = LEVEL:Benign
                   DEL:CID                             = DELC:CID
                   DEL:ClientReference                 = DELC:Reference

                   CLI:CID                             = DEL:CID
                   IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
                      DEL:Terms                        = CLI:Terms
                      L_CG:ClientTerms                 = CLI:Terms
                      L_CG:ClientStatus                = CLI:Status
                   .

                   DO New_Client
                   IF DEL:CID = 0
                      ! Cancel???
                   ELSE
                      DO Load_Lookups

                      MESSAGE('This is the first DI in this Multi-Part Delivery.||Next, please specify the Delivery details.', 'Insert Delivery', ICON:Asterisk)
                      SELECT(?Tab:1)
                .  .
             ELSE
                ! Set Delivery fields from previous Delivery and create new item for user completion
                A_DEL:DID      = L_DC:First_DID
                IF Access:DeliveriesAlias.TryFetch(A_DEL:PKey_DID) = LEVEL:Benign
                   DEL:CID                             = A_DEL:CID
                   DEL:ClientReference                 = A_DEL:ClientReference
                   DEL:JID                             = A_DEL:JID
                   DEL:CollectionAID                   = A_DEL:CollectionAID
                   DEL:DeliveryAID                     = A_DEL:DeliveryAID
                   DEL:CRTID                           = A_DEL:CRTID
                   DEL:LTID                            = A_DEL:LTID
                   DEL:SID                             = A_DEL:SID

                   DEL:MultipleLoadDID                 = L_DC:First_DID                            ! ??

                   DEL:Rate                            = A_DEL:Rate
                   DEL:DocumentCharge                  = A_DEL:DocumentCharge
                   DEL:AdditionalCharge_Calculate      = A_DEL:AdditionalCharge_Calculate
                   DEL:AdditionalCharge                = A_DEL:AdditionalCharge
                   DEL:VATRate                         = A_DEL:VATRate
                   DEL:Insure                          = A_DEL:Insure
                   DEL:InsuranceRate                   = A_DEL:InsuranceRate
                   DEL:FID                             = A_DEL:FID
                   DEL:FIDRate                         = A_DEL:FIDRate
                   DEL:SpecialDeliveryInstructions     = A_DEL:SpecialDeliveryInstructions
                   DEL:Notes                           = A_DEL:Notes
                   DEL:MultipleManifestsAllowed        = A_DEL:MultipleManifestsAllowed
                   DEL:DC_ID                           = A_DEL:DC_ID
                   DEL:Terms                           = A_DEL:Terms

                   DEL:VATRate_OverriddenUserID        = A_DEL:VATRate_OverriddenUserID            ! ??

                   CLI:CID                             = DEL:CID
                   IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
                      DEL:Terms                        = CLI:Terms
                      L_CG:ClientTerms                 = CLI:Terms
                      L_CG:ClientStatus                = CLI:Status
                   .
                   !DO New_Client
                   !IF DEL:CID = 0

                   DO Load_Lookups

                   DO New_Load_Type

                   MESSAGE('Next, please specify the Delivery Item details.', 'Insert Delivery', ICON:Asterisk)

                   ! Call Item insert...       note L_DC:First_DID is used in parameter
                   POST(EVENT:Accepted, ?Insert)
             .  .
          ELSE
          .
       ELSE
          CLEAR(L_DC:First_DID)

!          IF R:DELCID_Ext ~= 0
!             CASE MESSAGE('This Delivery was already specified on this Multi-Part, you have selected not to add it ' & |
!                    'due to a condition being exceeded.||Would you like to remove this Delivery from the Multi-Part?', |
!                    'Multi-Part Delivery', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
!             OF BUTTON:Yes
!                DEL:DELCID  = 0
    .  .  !.  .

    DO DelComp_SetButton

    CLEAR(L_DC2:Start_of_Insert)
    DISPLAY
    EXIT
Check_DelComp             ROUTINE
    DATA
R:Deliveries    LIKE(L_DC:Deliveries)
R:Items         LIKE(L_DC:Items)
R:Weight        LIKE(L_DC:Weight)

R:Add           BYTE

R:Msg           STRING(255)

    CODE
    IF DEL:DELCID ~= 0
       R:Add            = TRUE

       ! Check that this Composition is not completely loaded already
       LOC:DeliveryComposition_Ret_Group = Get_Delivery_Info(DEL:DELCID)

       R:Deliveries     = L_DC:Deliveries
       R:Items          = L_DC:Items
       R:Weight         = L_DC:Weight

       IF LOC:Request ~= InsertRecord
          R:Deliveries -= 1
          R:Items      -= L_CC:TotalUnits
          R:Weight     -= L_CC:TotalWeight
       .

       ! All specified deliveries done
!       IF (R:Deliveries + 1) > DELC:Deliveries
!          CASE MESSAGE('This Delivery Composition already has the specified number of Deliveries.||Continue and add this one?', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!          OF BUTTON:No
!             R:Add      = FALSE
!          .
!       ELSIF (R:Items + L_CC:TotalUnits) > DELC:Items
!          CASE MESSAGE('This Delivery Composition already has the specified number of Items.||Continue and add these items? (Items: ' & L_CC:TotalUnits & ')', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!          OF BUTTON:No
!             R:Add      = FALSE
!          .

       IF LOC:Request = InsertRecord
          R:Msg     = 'Adding this DI to the selected Multi-Part Delivery would exceed the specified weight'
       ELSE
          R:Msg     = 'The selected Multi-Part Delivery exceeds the specified weight'
       .

       IF (R:Weight + L_CC:TotalWeight) > DELC:Weight
          CASE MESSAGE(CLIP(R:Msg) & ' by ' & |
                    FORMAT(DELC:Weight - R:Weight - L_CC:TotalWeight, @n12.2) & '.||Continue and add this Delivery weight? (Weight: ' & |
                    FORMAT(L_CC:TotalWeight,@n12.2) & ')', 'Multi-Part Delivery', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
          OF BUTTON:No                                                                                                                         
             R:Add          = FALSE
       .  .

       IF R:Add = TRUE
          ! Add in this deliveries info then
          L_DC:Deliveries  += 1                 !R:Deliveries          !1
          L_DC:Items       += L_CC:TotalUnits   !R:Items               !L_CC:TotalUnits
          L_DC:Weight      += L_CC:TotalWeight  !R:Weight              !L_CC:TotalWeight
       ELSE
          CLEAR(L_DC:First_DID)
          DEL:DELCID        = 0
    .  .

    DO DelComp_SetButton

    ThisWindow.Reset
    DISPLAY
    EXIT
DelComp_SetButton        ROUTINE
    IF DEL:DELCID = 0
       ?Button_Specify{PROP:Text} = 'Specify Multi-Part'
       HIDE(?Prompt_Multi)
       ?Tab_Multi{PROP:Font,4}    = FONT:Regular
    ELSE
       ?Button_Specify{PROP:Text} = 'Change Multi-Part'
       UNHIDE(?Prompt_Multi)
       ?Tab_Multi{PROP:Font,4}    = FONT:Bold
    .

    IF ?Button_Specify{PROP:Text} = 'Specify Multi-Part'
       CLEAR(LOC:DeliveryComposition_Ret_Group)
       CLEAR(LOC:DeliveryComposition_Group_2)
    .
    EXIT
!                           old
!Get_DelComp                 ROUTINE
!    DATA
!R:DELCID        LIKE(DEL:DELCID)
!
!R:Deliveries    LIKE(L_DC:Deliveries)
!R:Items         LIKE(L_DC:Items)
!R:Weight        LIKE(L_DC:Weight)
!
!R:Add           BYTE
!
!    CODE
!    ! User will know from the button what action will be performed.
!    ThisWindow.Update
!    GlobalRequest   = SelectRecord
!    R:DELCID        = Browse_DeliveryCompositions()
!
!    IF DEL:DELCID ~= 0 AND DEL:DELCID ~= R:DELCID
!       ! Previous DELCID not equal to chosen one - warn user
!       CASE MESSAGE('You have selected a different Delivery Composition to the previous one specified for this Delivery.||' & |
!                    'Is this correct?', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
!       OF BUTTON:No
!          CLEAR(R:DELCID)
!       OF BUTTON:Yes
!          IF R:DELCID = 0
!             DEL:DELCID = 0                                 ! The other cases are delt with below
!    .  .  .
!
!    IF R:DELCID ~= 0
!       R:Add            = TRUE
!
!       ! Check that this Composition is not completely loaded already
!       LOC:DeliveryComposition_Ret_Group = Get_Delivery_Info(R:DELCID)
!
!       R:Deliveries     = L_DC:Deliveries
!       R:Items          = L_DC:Items
!       R:Weight         = L_DC:Weight
!       IF DEL:DELCID = R:DELCID                             ! Previously part of this delivery
!          R:Deliveries -= 1
!          R:Items      -= L_CC:TotalUnits
!          R:Weight     -= L_CC:TotalWeight
!       .
!
!       ! All specified deliveries done
!!       IF (R:Deliveries + 1) > DELC:Deliveries
!!          CASE MESSAGE('This Delivery Composition already has the specified number of Deliveries.||Continue and add this one?', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!!          OF BUTTON:No
!!             R:Add      = FALSE
!!          .
!!       ELSIF (R:Items + L_CC:TotalUnits) > DELC:Items
!!          CASE MESSAGE('This Delivery Composition already has the specified number of Items.||Continue and add these items? (Items: ' & L_CC:TotalUnits & ')', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!!          OF BUTTON:No
!!             R:Add      = FALSE
!!          .
!       IF (R:Weight + L_CC:TotalWeight) > DELC:Weight
!          CASE MESSAGE('This Delivery Composition already has the specified weight.||Continue and add this Delivery weight? (Weight: ' & FORMAT(L_CC:TotalWeight,@n12.2) & ')', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!          OF BUTTON:No                                                                                                                         
!             R:Add      = FALSE
!          .
!       ELSE
!          !
!       .
!
!       IF R:Add = TRUE
!          DEL:DELCID        = R:DELCID
!
!          ! Add in this deliveries info then
!          L_DC:Deliveries  += 1                 !R:Deliveries          !1
!          L_DC:Items       += L_CC:TotalUnits   !R:Items               !L_CC:TotalUnits
!          L_DC:Weight      += L_CC:TotalWeight  !R:Weight              !L_CC:TotalWeight
!
!          IF L_DC2:Start_of_Insert = TRUE
!             IF L_DC:First_DID = 0
!                DELC:DELCID = R:DELCID
!                IF Access:DeliveryComposition.TryFetch(DELC:PKey_DELCID) = LEVEL:Benign
!                   DEL:CID                             = DELC:CID
!                   DEL:ClientReference                 = DELC:Reference
!
!                   CLI:CID                             = DEL:CID
!                   IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
!                      DEL:Terms                        = CLI:Terms
!                      L_CG:ClientTerms                 = CLI:Terms
!                      L_CG:ClientStatus                = CLI:Status
!                   .
!
!                   DO New_Client
!
!                   DO Load_Lookups
!
!                   MESSAGE('This is the first DI in this Multi Part Delivery.||Next, please specify the Delivery details.', 'Insert Delivery', ICON:Asterisk)
!                   SELECT(?Tab:1)
!                .
!             ELSE
!                ! Set Delivery fields from previous Delivery and create new item for user completion
!                A_DEL:DID      = L_DC:First_DID
!                IF Access:DeliveriesAlias.TryFetch(A_DEL:PKey_DID) = LEVEL:Benign
!                   DEL:CID                             = A_DEL:CID
!                   DEL:ClientReference                 = A_DEL:ClientReference
!                   DEL:JID                             = A_DEL:JID
!                   DEL:CollectionAID                   = A_DEL:CollectionAID
!                   DEL:DeliveryAID                     = A_DEL:DeliveryAID
!                   DEL:CRTID                           = A_DEL:CRTID
!                   DEL:LTID                            = A_DEL:LTID
!                   DEL:SID                             = A_DEL:SID
!
!                   DEL:MultipleLoadDID                 = L_DC:First_DID                            ! ??
!
!                   DEL:Rate                            = A_DEL:Rate
!                   DEL:DocumentCharge                  = A_DEL:DocumentCharge
!                   DEL:AdditionalCharge_Calculate      = A_DEL:AdditionalCharge_Calculate
!                   DEL:AdditionalCharge                = A_DEL:AdditionalCharge
!                   DEL:VATRate                         = A_DEL:VATRate
!                   DEL:Insure                          = A_DEL:Insure
!                   DEL:InsuranceRate                   = A_DEL:InsuranceRate
!                   DEL:FID                             = A_DEL:FID
!                   DEL:FIDRate                         = A_DEL:FIDRate
!                   DEL:SpecialDeliveryInstructions     = A_DEL:SpecialDeliveryInstructions
!                   DEL:Notes                           = A_DEL:Notes
!                   DEL:MultipleManifestsAllowed        = A_DEL:MultipleManifestsAllowed
!                   DEL:DC_ID                           = A_DEL:DC_ID
!                   DEL:Terms                           = A_DEL:Terms
!
!                   DEL:VATRate_OverriddenUserID        = A_DEL:VATRate_OverriddenUserID            ! ??
!
!                   DO Load_Lookups
!
!                   DO New_Load_Type
!
!                   MESSAGE('Next, please specify the Delivery Item details.', 'Insert Delivery', ICON:Asterisk)
!
!                   ! Call Item insert...       note L_DC:First_DID is used in parameter
!                   POST(EVENT:Accepted, ?Insert)
!          .  .  .
!       ELSE
!          CLEAR(L_DC:First_DID)
!
!          IF DEL:DELCID ~= 0
!             CASE MESSAGE('This Delivery was already specified on this Composition, you have selected not to add it due to a condition being exceeded.||Would you like to remove this Delivery from the Composition?', 'Delivery Composition', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!             OF BUTTON:Yes
!                DEL:DELCID  = 0
!                CLEAR(LOC:DeliveryComposition_Ret_Group)
!                CLEAR(LOC:DeliveryComposition_Group_2)
!    .  .  .  .
!
!    ThisWindow.Reset
!    IF DEL:DELCID = 0
!       ?Button_Specify{PROP:Text} = 'Specify Multi-Part'
!       HIDE(?Prompt_Multi)
!       ?Tab_Multi{PROP:Font,4}    = FONT:Regular
!    ELSE
!       ?Button_Specify{PROP:Text} = 'Change Multi-Part'
!       UNHIDE(?Prompt_Multi)
!       ?Tab_Multi{PROP:Font,4}    = FONT:Bold
!    .
!
!    IF ?Button_Specify{PROP:Text} = 'Specify Multi-Part'
!       CLEAR(LOC:DeliveryComposition_Ret_Group)
!       CLEAR(LOC:DeliveryComposition_Group_2)
!    .
!
!    CLEAR(L_DC2:Start_of_Insert)
!    DISPLAY
!    EXIT
! ----------------------------------------------------------------      
Check_Invoiced              ROUTINE
    ! Check that we dont already have an invoice for this DI

    Inv_View.Init(View_Inv, Relate:_Invoice)
    Inv_View.AddSortOrder(INV:FKey_DID)
    !Inv_View.AppendOrder()
    Inv_View.AddRange(INV:DID, DEL:DID)
    !Inv_View.SetFilter()

    Inv_View.Reset()
    LOOP
       IF Inv_View.Next() ~= LEVEL:Benign
          BREAK
       .

       IF INV:CR_IID = 0
          ?Prompt_Manifest{PROP:Text}  = 'Delivery Invoiced (Inv. no.: ' & INV:IID & ') - No editing.'
!       ELSE
!          CLEAR(INV:Record)
          BREAK
    .  .

    Inv_View.Kill()
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                               ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Deliveries Record '
  OF InsertRecord
    ActionMessage = 'Deliveries Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Deliveries Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage          ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Deliveries_with_email_etc_old_perhaps_____')
      LOC:Original_Request    = GlobalRequest
  SELF.Request = GlobalRequest                    ! Store the incoming request
      ! p:Option
      ! (BYTE=0)
      !           0   - Normal mode
      !           1   - Admin editing mode
      !           2   - Multi-part delivery (insert only)
  
      IF SELF.Request = InsertRecord
      ELSE                                                            ! p:Option used here
         IF p:Option = 2                                              ! Invalid, only valid for Insert
            p:Option  = 0
            MESSAGE('Option 2 is specified but this is only available for Inserts, form is currently not in insert mode.', 'Update_Deliveries', ICON:Hand)
         .
  
         ! Check for Change / Delete actions - see if we do not allow them to do these
  
         ! Get no of manifested units from this delivery
         L_SG:Manifested_Units    = Get_DelItem_s_Totals(DEL:DID, 2,,,TRUE)
         IF L_SG:Manifested_Units > 0
            ! No editing of this DI is allowed!
            ! unless...
            IF p:Option = 1
            ELSE
               SELF.Request       = ViewRecord
            .
            LOC:Manifested        = TRUE
         ELSIF DEL:Manifested > 0
            Upd_Delivery_Man_Status(DEL:DID,,1)                       ! Update the status - DEL:Manifested
            MESSAGE('The Manifested status of this Delivery was wrong, this has now beeen updated.', 'Update Deliveries', ICON:Asterisk)
      .  .
  
      CLEAR(INV:Record)
      IF SELF.Request = ChangeRecord OR SELF.Request = ViewRecord
         ! Check that we dont already have an invoice for this DI
         INV:DID          = DEL:DID
         IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
            ! We do have an invoice - set View mode
            IF p:Option = 1
            ELSE
               SELF.Request  = ViewRecord
            .
         ELSE
            CLEAR(INV:Record)
      .  .
  
      DO Apply_User_Permissions
      LOC:Request                 = SELF.Request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DEL:BID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_IG:MID',L_IG:MID)                       ! Added by: BrowseBox(ABC)
  BIND('L_DLG:Total',L_DLG:Total)                 ! Added by: BrowseBox(ABC)
  BIND('L_DLG:VAT',L_DLG:VAT)                     ! Added by: BrowseBox(ABC)
        BIND('MALD:DIID', MALD:DIID)
        BIND('A_DELI:DIID', A_DELI:DIID)
        BIND('DELI:DIID', DELI:DIID)
  
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DEL:Record,History::DEL:Record)
  SELF.AddHistoryField(?DEL:DID,1)
  SELF.AddHistoryField(?DEL:BID,7)
  SELF.AddHistoryField(?DEL:DINo,2)
  SELF.AddHistoryField(?DEL:DIDate,5)
  SELF.AddHistoryField(?DEL:ClientReference,9)
  SELF.AddHistoryField(?DEL:CID,8)
  SELF.AddHistoryField(?DEL:NoticeEmailAddresses,54)
  SELF.AddHistoryField(?DEL:Insure,29)
  SELF.AddHistoryField(?DEL:TotalConsignmentValue,30)
  SELF.AddHistoryField(?DEL:InsuranceRate,31)
  SELF.AddHistoryField(?DEL:Rate,20)
  SELF.AddHistoryField(?DEL:AdditionalCharge_Calculate,24)
  SELF.AddHistoryField(?DEL:AdditionalCharge,25)
  SELF.AddHistoryField(?DEL:Charge,23)
  SELF.AddHistoryField(?DEL:DocumentCharge,21)
  SELF.AddHistoryField(?DEL:VATRate,26)
  SELF.AddHistoryField(?DEL:FuelSurcharge,22)
  SELF.AddHistoryField(?DEL:Delivered,42)
  SELF.AddHistoryField(?DEL:MultipleManifestsAllowed,40)
  SELF.AddHistoryField(?DEL:Manifested,41)
  SELF.AddHistoryField(?DEL:SpecialDeliveryInstructions,34)
  SELF.AddHistoryField(?DEL:Notes,35)
  SELF.AddHistoryField(?DEL:CRTID,13)
  SELF.AddHistoryField(?DEL:LTID,14)
  SELF.AddUpdateFile(Access:Deliveries)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:AddressAlias.SetOpenRelated()
  Relate:AddressAlias.Open                        ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  Relate:DeliveriesAlias.Open                     ! File DeliveriesAlias used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                            ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Relate:_View_ContainerParkRates_Client.Open     ! File _View_ContainerParkRates_Client used by this procedure, so make sure it's RelationManager is open
  Relate:_View_Rates_Client.Open                  ! File _View_Rates_Client used by this procedure, so make sure it's RelationManager is open
  Relate:_View_Rates_Client_Journeys.Open         ! File _View_Rates_Client_Journeys used by this procedure, so make sure it's RelationManager is open
  Relate:_View_Rates_Clients_LoadTypes.Open       ! File _View_Rates_Clients_LoadTypes used by this procedure, so make sure it's RelationManager is open
  Access:EmailAddresses.UseFile                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ClientsRateTypes.UseFile                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ServiceRequirements.UseFile              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Floors.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoadDeliveries.UseFile           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoad.UseFile                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Delivery_CODAddresses.UseFile            ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Invoice.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__Rates.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients_ContainerParkDiscounts.UseFile   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesContainerPark.UseFile             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LoadTypes2.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Branches.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TripSheets.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Deliveries
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller             ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW_DeliveryItems.Init(?Browse:ItemsContainers,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:DeliveryItems,SELF) ! Initialize the browse manager
  BRW_DelItemsAlias.Init(?Browse:ItemsLoose,Queue:Browse.ViewPosition,BRW20::View:Browse,Queue:Browse,Relate:DeliveryItemAlias2,SELF) ! Initialize the browse manager
  BRW6.Init(?Browse:6,Queue:Browse:6.ViewPosition,BRW6::View:Browse,Queue:Browse:6,Relate:DeliveryLegs,SELF) ! Initialize the browse manager
  BRW8.Init(?Browse:8,Queue:Browse:8.ViewPosition,BRW8::View:Browse,Queue:Browse:8,Relate:DeliveryProgress,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
      IF SELF.Request = ChangeRecord OR SELF.Request = ViewRecord
         DO Check_Invoiced
      .
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                    ! Configure controls for View Only mode
    ?DEL:DINo{PROP:ReadOnly} = True
    DISABLE(?Calendar)
    DISABLE(?CallLookup_Client)
    ?L_LG:ClientName{PROP:ReadOnly} = True
    ?L_LG:ClientNo{PROP:ReadOnly} = True
    ?DEL:ClientReference{PROP:ReadOnly} = True
    DISABLE(?CallLookup_LoadType)
    ?L_LG:LoadType{PROP:ReadOnly} = True
    DISABLE(?Button_ListRates)
    DISABLE(?CallLookup)
    ?L_LG:ClientRateType{PROP:ReadOnly} = True
    DISABLE(?CallLookup_Journey)
    ?L_LG:Journey{PROP:ReadOnly} = True
    DISABLE(?CallLookup_Floor)
    ?L_LG:Floor{PROP:ReadOnly} = True
    DISABLE(?CallLookup_CollectAdd)
    ?L_LG:From_Address{PROP:ReadOnly} = True
    DISABLE(?CallLookup_ToAdd)
    ?L_LG:To_Address{PROP:ReadOnly} = True
    DISABLE(?Button_Release)
    DISABLE(?CallLookup_Driver_Name)
    ?L_LG:Driver_Name{PROP:ReadOnly} = True
    DISABLE(?Button_COD_Address)
    DISABLE(?CallLookup_ServiceReq)
    ?DEL:NoticeEmailAddresses{PROP:ReadOnly} = True
    DISABLE(?Lookup_EmailAddresses)
    ?L_LG:ServiceRequirement{PROP:ReadOnly} = True
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
    DISABLE(?Insert:5)
    DISABLE(?Change:5)
    DISABLE(?Delete:5)
    DISABLE(?Button_Wizard)
    DISABLE(?Button_ClientEmails)
    DISABLE(?L_CG:ClientTerms)
    ?DEL:TotalConsignmentValue{PROP:ReadOnly} = True
    ?DEL:InsuranceRate{PROP:ReadOnly} = True
    ?L_CC:InsuanceAmount{PROP:ReadOnly} = True
    DISABLE(?Button_ReverseCalc)
    ?DEL:Rate{PROP:ReadOnly} = True
    DISABLE(?Button_GetRate)
    DISABLE(?Button_ShowRates)
    ?L_CC:TotalWeight{PROP:ReadOnly} = True
    ?L_LG:FloorRate{PROP:ReadOnly} = True
    DISABLE(?Button_View_Invoice)
    ?L_CG:MinimiumCharge{PROP:ReadOnly} = True
    ?L_CC:MinimiumChargeRate{PROP:ReadOnly} = True
    DISABLE(?Button_AdditionalCharges)
    ?DEL:AdditionalCharge{PROP:ReadOnly} = True
    ?DEL:Charge{PROP:ReadOnly} = True
    DISABLE(?Button_ChangeVAT)
    ?DEL:DocumentCharge{PROP:ReadOnly} = True
    DISABLE(?Button_Change_Doc)
    ?DEL:VATRate{PROP:ReadOnly} = True
    ?L_CC:FuelSurchargePer{PROP:ReadOnly} = True
    ?DEL:FuelSurcharge{PROP:ReadOnly} = True
    ?L_CC:TotalCharge{PROP:ReadOnly} = True
    ?L_CC:VAT{PROP:ReadOnly} = True
    ?L_CC:TotalCharge_VAT{PROP:ReadOnly} = True
    DISABLE(?Insert:9)
    DISABLE(?Change:9)
    DISABLE(?Delete:9)
    DISABLE(?Button_ChangeBranch)
    DISABLE(?DEL:Delivered)
    DISABLE(?Insert:7)
    DISABLE(?Change:7)
    DISABLE(?Delete:7)
    DISABLE(?DEL:Manifested)
    DISABLE(?Button_UpdateManifested)
    ?L_DC:Weight{PROP:ReadOnly} = True
    ?L_DC:Volume{PROP:ReadOnly} = True
    ?DELC:Weight{PROP:ReadOnly} = True
    ?DELC:Volume{PROP:ReadOnly} = True
    DISABLE(?Button_Specify)
    DISABLE(?Button_DB_Display)
    DISABLE(?Button_Value)
    ?L_SG:Value{PROP:ReadOnly} = True
  END
      IF SELF.Request = ViewRecord
         ?DEL:SpecialDeliveryInstructions{PROP:ReadOnly}  = TRUE
         ?DEL:Notes{PROP:ReadOnly}                        = TRUE
  
         ENABLE(?Button_Value)
      .
      IF INV:DID = DEL:DID          ! Invoiced
         ENABLE(?Button_View_Invoice)
      .
  
      ENABLE(?View)
      ENABLE(?View:2)
  
      ENABLE(?Button_UpdateManifested)
      ENABLE(?Button_ChangeBranch)
  
  
  
  
      IF SELF.Request = ChangeRecord OR SELF.Request = ViewRecord
         ! Check, if not delivered then if Release required, show button
         ! Not Delivered|Partially Delivered|Delivered          
  
         CASE Get_User_Access(GLO:UID, 'Menus', 'Update_Deliveries', 'DI Release', 1)     ! Disable by default
         OF 0 OROF 100
            !ENABLE(?BrowseOperationsDeliveriestoRelease)
            IF DEL:ReleasedUID ~= 0
               User_" = ''
               Get_User_Info(1, User_", DEL:ReleasedUID)
               ?Button_Release{PROP:Tip}     = 'This DI was released by ' & CLIP(User_")
  
               ?Button_Release{PROP:Icon}    = '~dotgreen.ico'
               UNHIDE(?Button_Release)
            ELSE
               IF INV:DID = DEL:DID          ! Invoiced - we will only check the Release requirement when invoiced & not delivered
                  IF DEL:Delivered < 2
                     ! (<*STRING>, ULONG=0, <*STRING>),LONG
                     ! (p_Statement_Info, p:DID, p_Release_Status_Str)
                     CASE Check_Delivery_Release(, DEL:DID, LOC:ReleaseReason)
                     OF 1                                      ! Released - shouldnt be here, see DEL:ReleadedUID
                     OF 0
                        ! No release needed
                        ?Button_Release{PROP:Icon} = '~dotyellow.ico'
                        ?Button_Release{PROP:Tip}  = 'This DI does not require releasing'
                        UNHIDE(?Button_Release)
                     ELSE
                        ?Button_Release{PROP:Tip}  = 'To release this DI click here (' & CLIP(LOC:ReleaseReason) & ')'
                        UNHIDE(?Button_Release)
            .  .  .  .
         OF 2
            !HIDE(?BrowseOperationsDeliveriestoRelease)
         ELSE
            !DISABLE(?BrowseOperationsDeliveriestoRelease)
         .
  
         IF ?Button_Release{PROP:Hide} = FALSE
            ENABLE(?Button_Release)
      .  .
  
  BRW_DeliveryItems.Q &= Queue:Browse:4
  BRW4::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon DELI:ItemNo for sort order 1
  BRW_DeliveryItems.AddSortOrder(BRW4::Sort0:StepClass,DELI:FKey_DID_ItemNo) ! Add the sort order for DELI:FKey_DID_ItemNo for sort order 1
  BRW_DeliveryItems.AddRange(DELI:DID,Relate:DeliveryItems,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW_DeliveryItems.AddLocator(BRW4::Sort0:Locator) ! Browse has a locator for sort order 1
  BRW4::Sort0:Locator.Init(,DELI:ItemNo,1,BRW_DeliveryItems) ! Initialize the browse locator using  using key: DELI:FKey_DID_ItemNo , DELI:ItemNo
  BRW_DeliveryItems.AddField(DELI:ItemNo,BRW_DeliveryItems.Q.DELI:ItemNo) ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(COM:Commodity,BRW_DeliveryItems.Q.COM:Commodity) ! Field COM:Commodity is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:Weight,BRW_DeliveryItems.Q.DELI:Weight) ! Field DELI:Weight is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:ContainerNo,BRW_DeliveryItems.Q.DELI:ContainerNo) ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:ContainerVessel,BRW_DeliveryItems.Q.DELI:ContainerVessel) ! Field DELI:ContainerVessel is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:ETA,BRW_DeliveryItems.Q.DELI:ETA) ! Field DELI:ETA is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(CTYP:ContainerType,BRW_DeliveryItems.Q.CTYP:ContainerType) ! Field CTYP:ContainerType is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(CONO:ContainerOperator,BRW_DeliveryItems.Q.CONO:ContainerOperator) ! Field CONO:ContainerOperator is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(ADD:AddressName,BRW_DeliveryItems.Q.ADD:AddressName) ! Field ADD:AddressName is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:Length,BRW_DeliveryItems.Q.DELI:Length) ! Field DELI:Length is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:Breadth,BRW_DeliveryItems.Q.DELI:Breadth) ! Field DELI:Breadth is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:Height,BRW_DeliveryItems.Q.DELI:Height) ! Field DELI:Height is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(L_IG:MID,BRW_DeliveryItems.Q.L_IG:MID) ! Field L_IG:MID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:DIID,BRW_DeliveryItems.Q.DELI:DIID) ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(DELI:DID,BRW_DeliveryItems.Q.DELI:DID) ! Field DELI:DID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(ADD:AID,BRW_DeliveryItems.Q.ADD:AID) ! Field ADD:AID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(PACK:PTID,BRW_DeliveryItems.Q.PACK:PTID) ! Field PACK:PTID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(COM:CMID,BRW_DeliveryItems.Q.COM:CMID) ! Field COM:CMID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(CTYP:CTID,BRW_DeliveryItems.Q.CTYP:CTID) ! Field CTYP:CTID is a hot field or requires assignment from browse
  BRW_DeliveryItems.AddField(CONO:COID,BRW_DeliveryItems.Q.CONO:COID) ! Field CONO:COID is a hot field or requires assignment from browse
  BRW_DelItemsAlias.Q &= Queue:Browse
  BRW_DelItemsAlias.AddSortOrder(,A_DELI2:FKey_DID_ItemNo) ! Add the sort order for A_DELI2:FKey_DID_ItemNo for sort order 1
  BRW_DelItemsAlias.AddRange(A_DELI2:DID,DEL:DID) ! Add single value range limit for sort order 1
  BRW_DelItemsAlias.AddLocator(BRW20::Sort0:Locator) ! Browse has a locator for sort order 1
  BRW20::Sort0:Locator.Init(,A_DELI2:ItemNo,1,BRW_DelItemsAlias) ! Initialize the browse locator using  using key: A_DELI2:FKey_DID_ItemNo , A_DELI2:ItemNo
  BRW_DelItemsAlias.AddField(A_DELI2:ItemNo,BRW_DelItemsAlias.Q.A_DELI2:ItemNo) ! Field A_DELI2:ItemNo is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(COM:Commodity,BRW_DelItemsAlias.Q.COM:Commodity) ! Field COM:Commodity is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(PACK:Packaging,BRW_DelItemsAlias.Q.PACK:Packaging) ! Field PACK:Packaging is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:Units,BRW_DelItemsAlias.Q.A_DELI2:Units) ! Field A_DELI2:Units is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:Weight,BRW_DelItemsAlias.Q.A_DELI2:Weight) ! Field A_DELI2:Weight is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:Volume,BRW_DelItemsAlias.Q.A_DELI2:Volume) ! Field A_DELI2:Volume is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:VolumetricWeight,BRW_DelItemsAlias.Q.A_DELI2:VolumetricWeight) ! Field A_DELI2:VolumetricWeight is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:Length,BRW_DelItemsAlias.Q.A_DELI2:Length) ! Field A_DELI2:Length is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:Breadth,BRW_DelItemsAlias.Q.A_DELI2:Breadth) ! Field A_DELI2:Breadth is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:Height,BRW_DelItemsAlias.Q.A_DELI2:Height) ! Field A_DELI2:Height is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(L_IG:MID,BRW_DelItemsAlias.Q.L_IG:MID) ! Field L_IG:MID is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:DIID,BRW_DelItemsAlias.Q.A_DELI2:DIID) ! Field A_DELI2:DIID is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(A_DELI2:DID,BRW_DelItemsAlias.Q.A_DELI2:DID) ! Field A_DELI2:DID is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(PACK:PTID,BRW_DelItemsAlias.Q.PACK:PTID) ! Field PACK:PTID is a hot field or requires assignment from browse
  BRW_DelItemsAlias.AddField(COM:CMID,BRW_DelItemsAlias.Q.COM:CMID) ! Field COM:CMID is a hot field or requires assignment from browse
  BRW6.Q &= Queue:Browse:6
  BRW6::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon DELL:Leg for sort order 1
  BRW6.AddSortOrder(BRW6::Sort0:StepClass,DELL:FKey_DID_Leg) ! Add the sort order for DELL:FKey_DID_Leg for sort order 1
  BRW6.AddRange(DELL:DID,Relate:DeliveryLegs,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW6.AddLocator(BRW6::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW6::Sort0:Locator.Init(,DELL:Leg,1,BRW6)      ! Initialize the browse locator using  using key: DELL:FKey_DID_Leg , DELL:Leg
  BRW6.AddField(DELL:Leg,BRW6.Q.DELL:Leg)         ! Field DELL:Leg is a hot field or requires assignment from browse
  BRW6.AddField(TRA:TransporterName,BRW6.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW6.AddField(JOU:Journey,BRW6.Q.JOU:Journey)   ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW6.AddField(L_DLG:Total,BRW6.Q.L_DLG:Total)   ! Field L_DLG:Total is a hot field or requires assignment from browse
  BRW6.AddField(DELL:Cost,BRW6.Q.DELL:Cost)       ! Field DELL:Cost is a hot field or requires assignment from browse
  BRW6.AddField(L_DLG:VAT,BRW6.Q.L_DLG:VAT)       ! Field L_DLG:VAT is a hot field or requires assignment from browse
  BRW6.AddField(DELL:VATRate,BRW6.Q.DELL:VATRate) ! Field DELL:VATRate is a hot field or requires assignment from browse
  BRW6.AddField(DELL:DLID,BRW6.Q.DELL:DLID)       ! Field DELL:DLID is a hot field or requires assignment from browse
  BRW6.AddField(DELL:DID,BRW6.Q.DELL:DID)         ! Field DELL:DID is a hot field or requires assignment from browse
  BRW6.AddField(JOU:JID,BRW6.Q.JOU:JID)           ! Field JOU:JID is a hot field or requires assignment from browse
  BRW6.AddField(TRA:TID,BRW6.Q.TRA:TID)           ! Field TRA:TID is a hot field or requires assignment from browse
  BRW8.Q &= Queue:Browse:8
  BRW8::Sort0:StepClass.Init(+ScrollSort:AllowAlpha) ! Moveable thumb based upon DELP:DID for sort order 1
  BRW8.AddSortOrder(BRW8::Sort0:StepClass,DELP:FKey_DID) ! Add the sort order for DELP:FKey_DID for sort order 1
  BRW8.AddRange(DELP:DID,Relate:DeliveryProgress,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW8.AddField(DELS:DeliveryStatus,BRW8.Q.DELS:DeliveryStatus) ! Field DELS:DeliveryStatus is a hot field or requires assignment from browse
  BRW8.AddField(ADDC:ContactName,BRW8.Q.ADDC:ContactName) ! Field ADDC:ContactName is a hot field or requires assignment from browse
  BRW8.AddField(DELP:StatusDate,BRW8.Q.DELP:StatusDate) ! Field DELP:StatusDate is a hot field or requires assignment from browse
  BRW8.AddField(DELP:ActionDate,BRW8.Q.DELP:ActionDate) ! Field DELP:ActionDate is a hot field or requires assignment from browse
  BRW8.AddField(DELP:StatusTime,BRW8.Q.DELP:StatusTime) ! Field DELP:StatusTime is a hot field or requires assignment from browse
  BRW8.AddField(DELP:ActionTime,BRW8.Q.DELP:ActionTime) ! Field DELP:ActionTime is a hot field or requires assignment from browse
  BRW8.AddField(DELP:DPID,BRW8.Q.DELP:DPID)       ! Field DELP:DPID is a hot field or requires assignment from browse
  BRW8.AddField(DELP:DID,BRW8.Q.DELP:DID)         ! Field DELP:DID is a hot field or requires assignment from browse
  BRW8.AddField(ADDC:ACID,BRW8.Q.ADDC:ACID)       ! Field ADDC:ACID is a hot field or requires assignment from browse
  BRW8.AddField(DELS:DSID,BRW8.Q.DELS:DSID)       ! Field DELS:DSID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  INIMgr.Fetch('Update_Deliveries_with_email_etc_old_perhaps_____',QuickWindow) ! Restore window settings from non-volatile store
      L_SG:Value_Type     = GETINI('Delivery', 'Value_Type', 0, GLO:Local_INI)
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      IF SELF.Request = InsertRecord
         ! Set the floor
         DEL:FID                  = Get_Branch_Info(DEL:BID, 3)
  
         DEL:VATRate              = Get_Setup_Info(4)
  
         DEL:SID                  = GETINI('Delivery', 'Default_DI_SID', 0, GLO:Local_INI)
      ELSE
         L_LG:CID                 = DEL:CID                                       ! Set our existing CID
  
         L_LG:Charge_Specified    = TRUE
         ENABLE(?Sheet_Items)
      .
  
      L_SG:VATRate_Previous       = DEL:VATRate                                   ! Record the VAT rate on entry
  
      LOC:GeneralRatesClientID    = Get_Branch_Info(,4)
      DO Load_Lookups                 ! Load lookup fields & additionals
      IF SELF.Request ~= InsertRecord
         ! Set screen values
         L_CC:TotalWeight     = Get_DelItem_s_Totals(DEL:DID,0,,,TRUE) / 100         ! Returns a Ulong
         L_CC:TotalUnits      = Get_DelItem_s_Totals(DEL:DID,3,,,TRUE)
      .
  
      L_CC:TotalCharge        = DEL:Charge + DEL:AdditionalCharge + DEL:DocumentCharge + DEL:FuelSurcharge + L_CC:InsuanceAmount
      !L_CC:TotalCharge        = DEL:Charge + DEL:DocumentCharge + L_CC:InsuanceAmount + DEL:FuelSurcharge
      L_LG:Last_Rate          = DEL:Rate
  
      L_CC:VAT                = L_CC:TotalCharge * (DEL:VATRate / 100)
      L_CC:TotalCharge_VAT    = L_CC:TotalCharge + L_CC:VAT
  
      IF LOC:Manifested = TRUE                                          ! p:Option used here
         L_SG:Un_Manifested_Units   = Get_DelItem_s_Totals(DEL:DID, 1,,,TRUE)
  
         Get_Delivery_ManIDs(DEL:DID, 0, MIDs_", 1)
         IF L_SG:Un_Manifested_Units > 0
            ?Prompt_Manifest{PROP:Text}  = 'Delivery Manifested (Un: ' & L_SG:Un_Manifested_Units & ' / Man: ' & L_SG:Manifested_Units & ') - No editing. (MID''s: ' & CLIP(MIDs_") & ')'
         ELSE
            !?Prompt_Manifest{PROP:Text}  = 'Delivery has been fully Manifested (Man. no.: ' &  & ') - No editing.'
            IF INV:IID ~= 0
               IF p:Option = 1
                  ?Prompt_Manifest{PROP:Text}  = 'Delivery fully Manifested - ADMIN editing. (Inv.: ' & INV:IID & ', MID''s: ' & CLIP(MIDs_") & ')'
               ELSE
                  ?Prompt_Manifest{PROP:Text}  = 'Delivery fully Manifested - No editing. (Inv.: ' & INV:IID & ', MID''s: ' & CLIP(MIDs_") & ')'
               .
            ELSE
               IF p:Option = 1
                  ?Prompt_Manifest{PROP:Text}  = 'Delivery fully Manifested - ADMIN editing. (MID''s: ' & CLIP(MIDs_") & ')'
               ELSE
                  ?Prompt_Manifest{PROP:Text}  = 'Delivery fully Manifested - No editing. (MID''s: ' & CLIP(MIDs_") & ')'
         .  .  .
  
         ! Get_TripDelItems_Info
         ! Check that they have been delivered
         !
         ! (p:TRID, p:Option, p:DID, p:TDID, p:DIID, p:Del_List)
         ! (<ULONG>, BYTE=0, <ULONG>, <ULONG>, <ULONG>, <*STRING>),ULONG
         !   1         2        3        4       5           6
         ! p:Del_List
         !   Either list of Dates & Times or TRID's
         ! p:Option
         ! TRID passed
         !   0. Weight (real weight - not volumetric)
         !   1. Units
         !       if p:DID is provided then the result is limited to the DID passed
         !       if p:TDID is provided then exclude this from weight
         ! DID passed only
         !   2. List of Delivery Dates, no of entries returned - for DIID!
         !   3. List of Delivered Date & Times
         !   4. List of TRIDs
  
         CLEAR(L_SG:Tripsheets_Info)
         IF Get_TripDelItems_Info(, 4, DEL:DID,,, L_SG:Tripsheets_Info) > 0
            ?Prompt_Manifest{PROP:Text}   = CLIP(?Prompt_Manifest{PROP:Text}) & ', Delivered. (TRID''s: ' & CLIP(L_SG:Tripsheets_Info)
  
            CLEAR(L_SG:Tripsheets_Info)
            IF Get_TripDelItems_Info(, 3, DEL:DID,,, L_SG:Tripsheets_Info) > 0
            .
            L_SG:Tripsheets_Info          = Get_1st_Element_From_Delim_Str(L_SG:Tripsheets_Info, ',')
            ?Prompt_Manifest{PROP:Text}   = CLIP(?Prompt_Manifest{PROP:Text}) & ', Last Del.: ' & CLIP(L_SG:Tripsheets_Info) & ')'
         .
  
         DISABLE(?DEL:Insure)
      .
  IF ?DEL:Insure{Prop:Checked}
    ENABLE(?Group_Insured)
  END
  IF NOT ?DEL:Insure{PROP:Checked}
    DISABLE(?Group_Insured)
  END
  BRW_DeliveryItems.AskProcedure = 10
  BRW_DelItemsAlias.AskProcedure = 11
  BRW6.AskProcedure = 12
  BRW8.AskProcedure = 13
  FDB27.Init(?L_LG:FloorRate,Queue:FileDrop.ViewPosition,FDB27::View:FileDrop,Queue:FileDrop,Relate:Floors,ThisWindow)
  FDB27.Q &= Queue:FileDrop
  FDB27.AddSortOrder(FLO:Key_Floor)
  FDB27.AddField(FLO:Floor,FDB27.Q.FLO:Floor) !List box control field - type derived from field
  FDB27.AddField(FLO:FID,FDB27.Q.FLO:FID) !Primary key field - type derived from field
  FDB27.AddUpdateField(FLO:FID,DEL:FIDRate)
  ThisWindow.AddItem(FDB27.WindowComponent)
  BRW_DelItemsAlias.AddToolbarTarget(Toolbar)     ! Browse accepts toolbar control
  BRW_DelItemsAlias.ToolbarItem.HelpButton = ?Help
      ! User stuff
      IF GLO:AccessLevel >= 100
         UNHIDE(?Tab_DB)
      .
      IF GLO:AccessLevel >= 10
         ENABLE(?Button_Change_Doc)
      .
  
  
      !   (p:Info, p:Value, p:UID, p:Login)
      !   (BYTE, *STRING, <ULONG>, <STRING>),LONG,PROC
      !   p:Info  1   - Login
      !           2   - Password
      !           3   - User Name + Surname
      !           4   - Name
      !           5   - Surname
      !           6   - Access Level
      !           7   - UID
      User_"  = ''
      Get_User_Info(1, User_", DEL:UID)
      ?String_User{PROP:Text} = CLIP(User_")
      DO Value_Type
    ?Button_ClientEmails{PROP:Tip} = 'Clients Operations emails: ' & CLIP(Get_Client_Emails(DEL:CID,2,1))
  BRW4::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW4::FormatManager.Init('MANTRNIS','Update_Deliveries_with_email_etc_old_perhaps_____',1,?Browse:ItemsContainers,4,BRW4::PopupTextExt,Queue:Browse:4,13,LFM_CFile,LFM_CFile.Record)
  BRW4::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW20::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW20::FormatManager.Init('MANTRNIS','Update_Deliveries_with_email_etc_old_perhaps_____',1,?Browse:ItemsLoose,20,BRW20::PopupTextExt,Queue:Browse,12,LFM_CFile,LFM_CFile.Record)
  BRW20::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW6::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW6::FormatManager.Init('MANTRNIS','Update_Deliveries_with_email_etc_old_perhaps_____',1,?Browse:6,6,BRW6::PopupTextExt,Queue:Browse:6,7,LFM_CFile,LFM_CFile.Record)
  BRW6::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  BRW8::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW8::FormatManager.Init('MANTRNIS','Update_Deliveries_with_email_etc_old_perhaps_____',1,?Browse:8,8,BRW8::PopupTextExt,Queue:Browse:8,6,LFM_CFile,LFM_CFile.Record)
  BRW8::FormatManager.BindInterface(,,,'.\MANTRNIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
        UNBIND('MALD:DIID')
        UNBIND('A_DELI:DIID')
        UNBIND('DELI:DIID')
  
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressAlias.Close
    Relate:DeliveriesAlias.Close
    Relate:_SQLTemp.Close
    Relate:_View_ContainerParkRates_Client.Close
    Relate:_View_Rates_Client.Close
    Relate:_View_Rates_Client_Journeys.Close
    Relate:_View_Rates_Clients_LoadTypes.Close
  END
  ! List Format Manager destructor
  BRW4::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW20::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW6::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW8::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Update_Deliveries_with_email_etc_old_perhaps_____',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  DEL:BID = GLO:BranchID
  DEL:DIDate = TODAY()
  DEL:DITime = CLOCK()
  DEL:MultipleManifestsAllowed = 1
  PARENT.PrimeFields


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  DELC:DELCID = DEL:DELCID                                 ! Assign linking field value
  Access:DeliveryComposition.Fetch(DELC:PKey_DELCID)
  PARENT.Reset(Force)
      IF SELF.Request = ViewRecord
         ENABLE(?View:2)
         ENABLE(?View)
      .


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Clients
      Browse_LoadTypes('V', DEL:CID)
      Browse_ClientsRateTypes(DEL:CID, DEL:LTID)
      Select_Journey(DEL:CID, DEL:LTID, DEL:FIDRate)
      Browse_Floors('')
      Browse_Addresses(, 'DC')
      Browse_Addresses_Alias_h(, 'DC')
      Browse_Drivers('','')
      Browse_ServiceRequirements
      Update_DeliveryItems(, L_DC:First_DID)
      Update_DeliveryItems_h(L_DC:First_DID)
      Update_DeliveryLegs
      Update_DeliveryProgress
    END
    ReturnValue = GlobalResponse
  END
    IF (Number = 10 OR Number = 11)         ! Update_DeliveryItems AND Update_DeliveryItems_h
       IF SELF.Request = ViewRecord
          CASE Number
          OF 10
             IF DELI:DIID = 0
                MESSAGE('Please select a Delivery Item.', 'Select Delivery Item', ICON:Exclamation)
             ELSE
                GlobalRequest  = ViewRecord
                Update_DeliveryItems
             .
          OF 11
             IF A_DELI:DIID = 0
                MESSAGE('Please select a Delivery Item.', 'Select Delivery Item', ICON:Exclamation)
             ELSE
                GlobalRequest  = ViewRecord
                Update_DeliveryItems_h
          .  .
       ELSE
    db.debugout('[Update_Deliveries]  RUN() - After Run - Number: ' & Number)
  
          DO Charge_Calc
  
          ! This is needed - otherwise missing record errors from ABC class.. 8 Dec 04
          ! BRW_DelItemsAlias.ResetFromFile() done in event
          POST(EVENT:User)
  
    db.debugout('[Update_Deliveries]  RUN() - After Run - After Charge calc - Number: ' & Number)
    .  .
  
    CLEAR(L_DC:First_DID)           ! When it is set, it is to be used only once
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DEL:DINo
          ! Check this DI No.
          IF SELF.Request = InsertRecord
             CLEAR(A_DEL:Record)
             A_DEL:DINo  = DEL:DINo
             IF Access:DeliveriesAlias.TryFetch(A_DEL:Key_DINo) = LEVEL:Benign
                ! Maybe this is this record though, check the internal ID
                IF A_DEL:DID = DEL:DID
                   ! This is our current record!
                ELSE
                   MESSAGE('There already exists a DI with this DI No.', 'DI Exists', ICON:Exclamation)
                   SELECT(?DEL:DINo)
                   CYCLE
          .  .  .
    OF ?DEL:DIDate
          DO Date_Changed
    OF ?Calendar
      ThisWindow.Update()
      Calendar22.SelectOnClose = True
      Calendar22.Ask('Select a Date',DEL:DIDate)
      IF Calendar22.Response = RequestCompleted THEN
      DEL:DIDate=Calendar22.SelectedDate
      DISPLAY(?DEL:DIDate)
      END
      ThisWindow.Reset(True)
    OF ?CallLookup_Client
      ThisWindow.Update()
      CLI:ClientName = L_LG:ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_LG:ClientName = CLI:ClientName
        DEL:CID = CLI:CID
        DEL:DocumentCharge = CLI:DocumentCharge
        DEL:Terms = CLI:Terms
        L_CG:MinimiumCharge = CLI:MinimiumCharge
        L_CG:ClientTerms = CLI:Terms
        L_LG:ClientNo = CLI:ClientNo
        L_CG:ClientStatus = CLI:Status
        L_LG:PODMessage = CLI:PODMessage
      END
      ThisWindow.Reset(1)
          DO New_Client
    OF ?L_LG:ClientName
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_LG:ClientName OR ?L_LG:ClientName{PROP:Req}
          CLI:ClientName = L_LG:ClientName
          IF Access:Clients.TryFetch(CLI:Key_ClientName)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              L_LG:ClientName = CLI:ClientName
              DEL:CID = CLI:CID
              DEL:DocumentCharge = CLI:DocumentCharge
              DEL:Terms = CLI:Terms
              L_CG:MinimiumCharge = CLI:MinimiumCharge
              L_CG:ClientTerms = CLI:Terms
              L_LG:ClientNo = CLI:ClientNo
              L_CG:ClientStatus = CLI:Status
              L_LG:PODMessage = CLI:PODMessage
            ELSE
              CLEAR(DEL:CID)
              CLEAR(DEL:DocumentCharge)
              CLEAR(DEL:Terms)
              CLEAR(L_CG:MinimiumCharge)
              CLEAR(L_CG:ClientTerms)
              CLEAR(L_LG:ClientNo)
              CLEAR(L_CG:ClientStatus)
              CLEAR(L_LG:PODMessage)
              SELECT(?L_LG:ClientName)
              CYCLE
            END
          ELSE
            DEL:CID = CLI:CID
            DEL:DocumentCharge = CLI:DocumentCharge
            DEL:Terms = CLI:Terms
            L_CG:MinimiumCharge = CLI:MinimiumCharge
            L_CG:ClientTerms = CLI:Terms
            L_LG:ClientNo = CLI:ClientNo
            L_CG:ClientStatus = CLI:Status
            L_LG:PODMessage = CLI:PODMessage
          END
        END
      END
      ThisWindow.Reset()
          DO New_Client
    OF ?L_LG:ClientNo
          ! Lookup the client
          IF L_LG:ClientNo ~= 0
             CLI:ClientNo         = L_LG:ClientNo
             IF Access:Clients.TryFetch(CLI:Key_ClientNo) = LEVEL:Benign
                L_LG:ClientName   = CLI:ClientName
      
                POST(EVENT:Accepted, ?L_LG:ClientName)      ! Assign all other fields!
             ELSE
                MESSAGE('Cannot find a client with the Client No. specified: ' & L_LG:ClientNo, 'Update Deliveries', ICON:Exclamation)
      
                CLEAR(L_LG:ClientNo)
                SELECT(?L_LG:ClientNo)
                DISPLAY
                CYCLE
          .  .
    OF ?CallLookup_LoadType
      ThisWindow.Update()
      LOAD2:LoadType = L_LG:LoadType
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_LG:LoadType = LOAD2:LoadType
        DEL:LTID = LOAD2:LTID
      END
      ThisWindow.Reset(1)
          DO New_Load_Type
    OF ?L_LG:LoadType
      IF L_LG:LoadType OR ?L_LG:LoadType{PROP:Req}
        LOAD2:LoadType = L_LG:LoadType
        IF Access:LoadTypes2.TryFetch(LOAD2:Key_LoadType)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            L_LG:LoadType = LOAD2:LoadType
            DEL:LTID = LOAD2:LTID
          ELSE
            CLEAR(DEL:LTID)
            SELECT(?L_LG:LoadType)
            CYCLE
          END
        ELSE
          DEL:LTID = LOAD2:LTID
        END
      END
      ThisWindow.Reset()
          DO New_Load_Type
    OF ?Button_ListRates
      ThisWindow.Update()
                         ! (p:DID  , p:LTID  , p:CID  , p:JID  , p:DIDate  , p:FIDRate  , p:SelectedRate    , p:Mass          , p:MinimiumChargeRate   , p:To_Weight   , p:Effective_Date)
          IF Delivery_Rates(DEL:DID, DEL:LTID, DEL:CID, DEL:JID, DEL:DIDate, DEL:FIDRate, L_RG:Returned_Rate, L_CC:TotalWeight, L_CC:MinimiumChargeRate, L_RG:To_Weight, L_RG:Effective_Date) > 0
             db.debugout('[Update_Deliveries]   2 L_CC:MinimiumChargeRate: ' & L_CC:MinimiumChargeRate & ',  L_RG:To_Weight: ' & L_RG:To_Weight & ',  L_RG:Effective_Date: ' & FORMAT(L_RG:Effective_Date,@d5))
      
             DEL:Rate            = L_RG:Returned_Rate
      
             L_RG:Selected_Rate  = DEL:Rate
      
             L_LG:Rate_Specified = TRUE
      
      !       POST(EVENT:Accepted, ?DEL:Rate)
          .
      
      !    ThisWindow.Reset
    OF ?CallLookup
      ThisWindow.Update()
      CRT:ClientRateType = L_LG:ClientRateType
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        L_LG:ClientRateType = CRT:ClientRateType
        DEL:CRTID = CRT:CRTID
        L_LG:ClientRateType_CID = CRT:CID
      END
      ThisWindow.Reset(1)
    OF ?L_LG:ClientRateType
         CRT:CID = DEL:CID
      
         IF L_LG:ClientRateType OR ?L_LG:ClientRateType{Prop:Req}
            CRT:ClientRateType = L_LG:ClientRateType
            IF Access:ClientsRateTypes.TryFetch(CRT:SKey_CID_ClientRateType) ~= LEVEL:Benign
               CRT:CID   = LOC:GeneralRatesClientID         ! Try the setup client then
         .  .
      
        db.debugout('[Update_Deliveries]  1.  DEL:CID: ' & DEL:CID & ',  CRT:CID: '& CRT:CID & ',  L_LG:ClientRateType_CID: ' & L_LG:ClientRateType_CID & ',  LOC:GeneralRatesClientID: ' & LOC:GeneralRatesClientID)
      
      
      IF L_LG:ClientRateType OR ?L_LG:ClientRateType{PROP:Req}
        CRT:ClientRateType = L_LG:ClientRateType
        IF Access:ClientsRateTypes.TryFetch(CRT:SKey_CID_ClientRateType)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            L_LG:ClientRateType = CRT:ClientRateType
            DEL:CRTID = CRT:CRTID
            L_LG:ClientRateType_CID = CRT:CID
          ELSE
            CLEAR(DEL:CRTID)
            CLEAR(L_LG:ClientRateType_CID)
            SELECT(?L_LG:ClientRateType)
            CYCLE
          END
        ELSE
          DEL:CRTID = CRT:CRTID
          L_LG:ClientRateType_CID = CRT:CID
        END
      END
      ThisWindow.Reset()
          ! Check that the Client Rate is applicable to this Client - or Setup client Rate Type
          IF L_LG:ClientRateType_CID ~= DEL:CID AND L_LG:ClientRateType_CID ~= LOC:GeneralRatesClientID
                db.debugout('[Update_Deliveries]   DEL:CID: ' & DEL:CID & ',  L_LG:ClientRateType_CID: ' & L_LG:ClientRateType_CID & ', LOC:GeneralRatesClientID: ' & LOC:GeneralRatesClientID)
      
             CLEAR(CRT:Record)
             CLEAR(L_LG:ClientRateType)
             CLEAR(DEL:CRTID)
      
             SELECT(?L_LG:ClientRateType)
          .
    OF ?CallLookup_Journey
      ThisWindow.Update()
      JOU:Journey = L_LG:Journey
      IF SELF.Run(4,SelectRecord) = RequestCompleted
        L_LG:Journey = JOU:Journey
        DEL:JID = JOU:JID
      END
      ThisWindow.Reset(1)
          DO New_Journey
      
          IF DEL:CollectionAID ~= 0 AND DEL:DeliveryAID ~= 0
             SELECT(?CallLookup_CollectAdd)
          .
    OF ?L_LG:Journey
      IF L_LG:Journey OR ?L_LG:Journey{PROP:Req}
        JOU:Journey = L_LG:Journey
        IF Access:Journeys.TryFetch(JOU:Key_Journey)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            L_LG:Journey = JOU:Journey
            DEL:JID = JOU:JID
          ELSE
            CLEAR(DEL:JID)
            SELECT(?L_LG:Journey)
            CYCLE
          END
        ELSE
          DEL:JID = JOU:JID
        END
      END
      ThisWindow.Reset()
          DO New_Journey
    OF ?CallLookup_Floor
      ThisWindow.Update()
      FLO:Floor = L_LG:Floor
      IF SELF.Run(5,SelectRecord) = RequestCompleted
        L_LG:Floor = FLO:Floor
        DEL:FID = FLO:FID
      END
      ThisWindow.Reset(1)
          DO Floor_Changed
    OF ?L_LG:Floor
      IF L_LG:Floor OR ?L_LG:Floor{PROP:Req}
        FLO:Floor = L_LG:Floor
        IF Access:Floors.TryFetch(FLO:Key_Floor)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            L_LG:Floor = FLO:Floor
            DEL:FID = FLO:FID
          ELSE
            CLEAR(DEL:FID)
            SELECT(?L_LG:Floor)
            CYCLE
          END
        ELSE
          DEL:FID = FLO:FID
        END
      END
      ThisWindow.Reset()
          DO Floor_Changed
    OF ?CallLookup_CollectAdd
      ThisWindow.Update()
      ADD:AddressName = L_LG:From_Address
      IF SELF.Run(6,SelectRecord) = RequestCompleted
        L_LG:From_Address = ADD:AddressName
        DEL:CollectionAID = ADD:AID
      END
      ThisWindow.Reset(1)
          DO Validate_Col_Add
    OF ?L_LG:From_Address
      IF L_LG:From_Address OR ?L_LG:From_Address{PROP:Req}
        ADD:AddressName = L_LG:From_Address
        IF Access:Addresses.TryFetch(ADD:Key_Name)
          IF SELF.Run(6,SelectRecord) = RequestCompleted
            L_LG:From_Address = ADD:AddressName
            DEL:CollectionAID = ADD:AID
          ELSE
            CLEAR(DEL:CollectionAID)
            SELECT(?L_LG:From_Address)
            CYCLE
          END
        ELSE
          DEL:CollectionAID = ADD:AID
        END
      END
      ThisWindow.Reset()
          DO Validate_Col_Add
    OF ?CallLookup_ToAdd
      ThisWindow.Update()
      A_ADD:AddressName = L_LG:To_Address
      IF SELF.Run(7,SelectRecord) = RequestCompleted
        L_LG:To_Address = A_ADD:AddressName
        DEL:DeliveryAID = A_ADD:AID
      END
      ThisWindow.Reset(1)
          DO Validate_Del_Add
    OF ?L_LG:To_Address
      IF L_LG:To_Address OR ?L_LG:To_Address{PROP:Req}
        A_ADD:AddressName = L_LG:To_Address
        IF Access:AddressAlias.TryFetch(A_ADD:Key_Name)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            L_LG:To_Address = A_ADD:AddressName
            DEL:DeliveryAID = A_ADD:AID
          ELSE
            CLEAR(DEL:DeliveryAID)
            SELECT(?L_LG:To_Address)
            CYCLE
          END
        ELSE
          DEL:DeliveryAID = A_ADD:AID
        END
      END
      ThisWindow.Reset()
          DO Validate_Del_Add
    OF ?Button_Release
      ThisWindow.Update()
          IF DEL:ReleasedUID = 0
             CASE MESSAGE('Would you like to release this DI?||Note other changes you may have made will be saved.', 'Release DI', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:Yes
                ?Button_Release{PROP:Icon} = '~dotgreen.ico'
                DEL:ReleasedUID   = GLO:UID
      
      !          User_" = ''                  - we are exiting afterwards now - so no need for this
      !          Get_User_Info(1, User_", DEL:ReleasedUID)
      !          ?Button_Release{PROP:Tip}     = 'This DI was released by ' & CLIP(User_")
      
                IF Access:Deliveries.TryUpdate() = LEVEL:Benign
                   ThisWindow.CancelAction        = Cancel:Cancel
                   POST(EVENT:Accepted, ?Cancel)                      ! Saved, so exit
             .  .
          ELSE
             CASE MESSAGE('Would you like to cancel the DI release?||Note other changes you may have made will be saved.', 'Cancel DI Release', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:Yes
                ?Button_Release{PROP:Icon}    = '~dotred.ico'
                DEL:ReleasedUID               = 0
                ?Button_Release{PROP:Tip}     = 'To release this DI click here'
      
                IF Access:Deliveries.TryUpdate() = LEVEL:Benign
          .  .  .
    OF ?CallLookup_Driver_Name
      ThisWindow.Update()
      DRI:FirstNameSurname = L_LG:Driver_Name
      IF SELF.Run(8,SelectRecord) = RequestCompleted
        L_LG:Driver_Name = DRI:FirstNameSurname
        DEL:CollectedByDRID = DRI:DRID
      END
      ThisWindow.Reset(1)
    OF ?L_LG:Driver_Name
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_LG:Driver_Name OR ?L_LG:Driver_Name{PROP:Req}
          DRI:FirstNameSurname = L_LG:Driver_Name
          IF Access:Drivers.TryFetch(DRI:SKey_FirstNameSurname)
            IF SELF.Run(8,SelectRecord) = RequestCompleted
              L_LG:Driver_Name = DRI:FirstNameSurname
              DEL:CollectedByDRID = DRI:DRID
            ELSE
              CLEAR(DEL:CollectedByDRID)
              SELECT(?L_LG:Driver_Name)
              CYCLE
            END
          ELSE
            DEL:CollectedByDRID = DRI:DRID
          END
        END
      END
      ThisWindow.Reset()
    OF ?Button_COD_Address
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_CODAddresses()
      ThisWindow.Reset
          IF GlobalResponse = RequestCompleted
             DEL:DC_ID       = DCADD:DC_ID
      
             ! Set the Tip to the COD Address
             DCADD:DC_ID     = DEL:DC_ID
             IF Access:Delivery_CODAddresses.TryFetch(DCADD:PKey_DC_ID) = LEVEL:Benign
                ?Button_COD_Address{PROP:Tip}       = 'COD Address specified is:<13,10>' & CLIP(DCADD:AddressName) & '<13,10>' & CLIP(DCADD:Line1) & |
                                                 CLIP(DCADD:Line2) & '<13,10>' & CLIP(DCADD:Line3) & '<13,10>' & CLIP(DCADD:Line4) & |
                                                 '<13,10>' & CLIP(DCADD:Line5) & '<13,10>VAT No.: ' & CLIP(DCADD:VATNo)
                ?Button_COD_Address{PROP:FontColor}  = COLOR:None
             ELSE
                ?Button_COD_Address{PROP:Tip}       = 'Specify a COD Address<13,10>This will be used on the Invoice'
                ?Button_COD_Address{PROP:FontColor}  = COLOR:Red
          .  .
    OF ?CallLookup_ServiceReq
      ThisWindow.Update()
      SERI:ServiceRequirement = L_LG:ServiceRequirement
      IF SELF.Run(9,SelectRecord) = RequestCompleted
        L_LG:ServiceRequirement = SERI:ServiceRequirement
        DEL:SID = SERI:SID
      END
      ThisWindow.Reset(1)
    OF ?Lookup_EmailAddresses
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_EmailAddresses(2)
      ThisWindow.Reset
      IF GlobalResponse = RequestCompleted
        ! we should have an email address
        ! (p:Add, p:List, p:Delim, p:Option, p:Prefix, p:Beginning, p:IgnoreIfExists)
        Add_to_List(EMAI:EmailAddress, DEL:NoticeEmailAddresses, ', ',,,, 1)  
        
        DISPLAY(?DEL:NoticeEmailAddresses)
       
        ! message('added: ' & EMAI:EmailAddress )
      .
    OF ?L_LG:ServiceRequirement
      IF L_LG:ServiceRequirement OR ?L_LG:ServiceRequirement{PROP:Req}
        SERI:ServiceRequirement = L_LG:ServiceRequirement
        IF Access:ServiceRequirements.TryFetch(SERI:Key_ServiceRequirement)
          IF SELF.Run(9,SelectRecord) = RequestCompleted
            L_LG:ServiceRequirement = SERI:ServiceRequirement
            DEL:SID = SERI:SID
          ELSE
            CLEAR(DEL:SID)
            SELECT(?L_LG:ServiceRequirement)
            CYCLE
          END
        ELSE
          DEL:SID = SERI:SID
        END
      END
      ThisWindow.Reset()
    OF ?Delete
      ThisWindow.Update()
          IF RECORDS(Queue:Browse:4) OR RECORDS(Queue:Browse)
             ! We don't want Load Type changed when we already have Items with the current specified Load Type
             DISABLE(?Group_LoadType)
          ELSE
             ENABLE(?Group_LoadType)
          .
    OF ?Delete:5
      ThisWindow.Update()
          IF RECORDS(Queue:Browse:4) OR RECORDS(Queue:Browse)
             ! We don't want Load Type changed when we already have Items with the current specified Load Type
             DISABLE(?Group_LoadType)
          ELSE
             ENABLE(?Group_LoadType)
          .
    OF ?Button_Wizard
      ThisWindow.Update()
          IF ?Sheet_Items{PROP:Wizard} = TRUE
             ?Sheet_Items{PROP:Wizard}    = FALSE
             ?Sheet_Items{PROP:NoSheet}   = FALSE
          ELSE
             ?Sheet_Items{PROP:Wizard}    = TRUE
             ?Sheet_Items{PROP:NoSheet}   = TRUE
          .
    OF ?Button_ClientEmails
      ThisWindow.Update()
        ! Load the client form with Emails list showing only...        
            CLI:CID = DEL:CID
            IF Access:Clients.TryFetch(CLI:PKey_CID) = Level:Benign
              GlobalRequest = ChangeRecord
              
              Update_Clients(2)
              
              ?Button_ClientEmails{PROP:Tip} = 'Clients Operations emails: ' & CLIP(Get_Client_Emails(DEL:CID, 2, 1))
            .
            
    OF ?DEL:Insure
      IF ?DEL:Insure{PROP:Checked}
        ENABLE(?Group_Insured)
      END
      IF NOT ?DEL:Insure{PROP:Checked}
        DISABLE(?Group_Insured)
      END
      ThisWindow.Reset()
          DO Charge_Calc
    OF ?DEL:TotalConsignmentValue
          DO Charge_Calc
    OF ?DEL:InsuranceRate
          DO Charge_Calc
    OF ?L_CC:InsuanceAmount
          DEL:InsuranceRate   = (L_CC:InsuanceAmount / DEL:TotalConsignmentValue) * 100
          DISPLAY(?DEL:InsuranceRate)
      
          DO Charge_Calc
    OF ?Button_ReverseCalc
      ThisWindow.Update()
          ?L_CC:InsuanceAmount{PROP:ReadOnly}     = FALSE
          ?L_CC:InsuanceAmount{PROP:Background}   = -1
          ?L_CC:InsuanceAmount{PROP:Skip}         = FALSE
      
    OF ?DEL:Rate
          IF QuickWindow{PROP:AcceptAll} = FALSE
             CLEAR(L_CC:Rate_Checked)
      
             CLEAR(L_CC:MinChargeRateChecked)
             CLEAR(L_CC:UseMinRateCharge)
         !    CLEAR(L_CG:MinChargeChecked)
         !    CLEAR(L_CG:MinimiumCharge)
      
             L_LG:Charge_Specified   = FALSE
             L_LG:Rate_Specified     = TRUE
             DO Charge_Calc
          .
    OF ?Button_GetRate
      ThisWindow.Update()
          DO Get_Rate_Button
    OF ?Button_ShowRates
      ThisWindow.Update()
                         ! (p:DID  , p:LTID  , p:CID  , p:JID  , p:DIDate  , p:FIDRate  , p:SelectedRate    , p:Mass          , p:MinimiumChargeRate   , p:To_Weight   , p:Effective_Date)
          IF Delivery_Rates(DEL:DID, DEL:LTID, DEL:CID, DEL:JID, DEL:DIDate, DEL:FIDRate, L_RG:Returned_Rate, L_CC:TotalWeight, L_CC:MinimiumChargeRate, L_RG:To_Weight, L_RG:Effective_Date) > 0
             db.debugout('[Update_Deliveries]   L_CC:MinimiumChargeRate: ' & L_CC:MinimiumChargeRate & ',  L_RG:To_Weight: ' & L_RG:To_Weight & ',  L_RG:Effective_Date: ' & FORMAT(L_RG:Effective_Date,@d5))
      
             DEL:Rate            = L_RG:Returned_Rate
      
             L_RG:Selected_Rate  = DEL:Rate
      
      !       ThisWindow.Reset
      
             !IF DEL:Rate ~= 0.0    - we know it is specified - the IF!
      
             L_LG:Rate_Specified = TRUE
      
             !.
      
             POST(EVENT:Accepted, ?DEL:Rate)
          .
      
          ThisWindow.Reset
    OF ?Button_View_Invoice
      ThisWindow.Update()
          IF DEL:DID ~= 0
             INV:DID          = DEL:DID
             IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
                GlobalRequest    = ViewRecord
                Update_Invoice()
          .  .
    OF ?Button_AdditionalCharges
      ThisWindow.Update()
      Browse_DeliveryAdditionalCharges_Delivery(DEL:DID, DEL:CID)
      ThisWindow.Reset
          IF DEL:CID ~= 0 AND DEL:AdditionalCharge_Calculate = TRUE
             DEL:AdditionalCharge     = Get_DeliveryAdditionalCharges(DEL:DID) / 100
             DISPLAY(?DEL:AdditionalCharge)
          .
    OF ?DEL:AdditionalCharge_Calculate
          IF DEL:CID ~= 0 AND DEL:AdditionalCharge_Calculate = TRUE
             DEL:AdditionalCharge     = Get_DeliveryAdditionalCharges(DEL:DID) / 100
             DISPLAY(?DEL:AdditionalCharge)
          .
    OF ?DEL:AdditionalCharge
          DO Charge_Calc
    OF ?DEL:Charge
          IF QuickWindow{PROP:AcceptAll} = FALSE       ! Not on accept all then - user specified
             L_LG:Charge_Specified    = TRUE
          .
      
          DO Charge_Calc
    OF ?Button_ChangeVAT
      ThisWindow.Update()
          IF ?DEL:VATRate{PROP:Skip} = FALSE
             ?DEL:VATRate{PROP:Skip}         = TRUE
             ?DEL:VATRate{PROP:ReadOnly}     = TRUE
      
             ?DEL:VATRate{PROP:Background}   = 0E9E9E9H
          ELSE
             ?DEL:VATRate{PROP:Skip}         = FALSE
             ?DEL:VATRate{PROP:ReadOnly}     = FALSE
      
             ?DEL:VATRate{PROP:Background}   = -1
          .
    OF ?DEL:DocumentCharge
          DO Charge_Calc
    OF ?Button_Change_Doc
      ThisWindow.Update()
          ?DEL:DocumentCharge{PROP:ReadOnly}      = FALSE
          ?DEL:DocumentCharge{PROP:Skip}          = FALSE
          ?DEL:DocumentCharge{PROP:Background}    = -1
    OF ?DEL:VATRate
          DO Charge_Calc
    OF ?L_CC:FuelSurchargePer
          DEL:FuelSurcharge   = DEL:Charge * (L_CC:FuelSurchargePer / 100)
          DISPLAY(?DEL:FuelSurcharge)
          DO Charge_Calc
    OF ?DEL:FuelSurcharge
          ! If we have an amt entered by the user then re-calc the percent that this represents...
      
          L_CC:FuelSurchargePer   = (DEL:FuelSurcharge / DEL:Charge) * 100
      
          !IF L_CC:FuelSurchargePer > 0
          !   DEL:FuelSurcharge    = DEL:Charge * (L_CC:FuelSurchargePer / 100)
          !.
      
          DO Charge_Calc
    OF ?Button_ChangeBranch
      ThisWindow.Update()
          ! Check if this DI has an invoice
          Stop_#  = FALSE
      
          INV:DID = DEL:DID
          IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
             CASE MESSAGE('This DI already has an Invoice.||Do you want to change the Branch on both the DI and the Invoice?', 'DI Branch Change', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:No
                Stop_#    = TRUE
             .
          ELSE
             CLEAR(INV:Record)
          .
      
          IF Stop_# = FALSE
             GlobalRequest    = SelectRecord
             Select_Branches()
      
             IF BRA:BID ~= DEL:BID
                IF DEL:Manifested = TRUE OR INV:IID ~= 0
                   ! Check the Manifest branch is not the same?
                   MAN:MID        = INV:MID
                   IF Access:Manifest.TryFetch(MAN:PKey_MID) = LEVEL:Benign
                      IF MAN:BID ~= BRA:BID
                         CASE MESSAGE('The Manifest that this DI is on has a different Branch to the new selected branch.||Do you still want to change the DI & Invoice branch?||The Manifest Branch will not be changed.', 'DI Branch Change', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
                         OF BUTTON:No
                            Stop_#    = TRUE
                .  .  .  .
      
                IF Stop_# = FALSE
                   IF INV:IID ~= 0
                      ! Change the Invoice branch...
                      INV:BID         = BRA:BID
                      INV:BranchName  = BRA:BranchName
                      IF Access:_Invoice.Update() = LEVEL:Benign
                   .  .
      
                   IF SELF.Request = ViewRecord
                      DEL:BID = BRA:BID
                      IF Access:Deliveries.Update() = LEVEL:Benign
                      .
                   ELSE
                      DEL:BID = BRA:BID
                      MESSAGE('The Branch has been changed but the DI is in edit mode so this change will not be saved until you save the DI.', 'DI Branch Change', ICON:Exclamation)
                   .
      
                   ?String_Branch{PROP:Text}    = BRA:BranchName
                   ?String_Branch:2{PROP:Text}  = BRA:BranchName
      
                   DISPLAY
                .
             ELSE
                MESSAGE('You selected the same branch that is already specified on this DI.','DI Branch Change',ICON:Asterisk)
          .  .
    OF ?Button_UpdateManifested
      ThisWindow.Update()
         ThisWindow.Update
         DEL:Manifested = Upd_Delivery_Man_Status(DEL:DID,,1)
         DEL:Delivered  = Upd_Delivery_Del_Status(DEL:DID)
         ThisWindow.Reset
      
    OF ?Button_Specify
      ThisWindow.Update()
          DO Get_DelComp
      
    OF ?Button_DB_Display
      ThisWindow.Update()
          DISPLAY
    OF ?Button_Value
      ThisWindow.Update()
          L_SG:Value_Type  = POPUP('Delivery Charge|Total Charge excl.|Total Charge incl.', ?L_SG:Value{PROP:XPos}, ?L_SG:Value{PROP:YPos}, 1)
          PUTINI('Delivery', 'Value_Type', L_SG:Value_Type, GLO:Local_INI)
      
      
          DO Value_Type
    OF ?OK
      ThisWindow.Update()
          IF SELF.Request <> ViewRecord
             IF L_CG:ClientStatus = 1
                !MESSAGE('This Client is on Hold!', 'Update Deliveries - New Client', ICON:Exclamation)
             ELSIF L_CG:ClientStatus = 2
                QuickWindow{PROP:AcceptAll}    = FALSE
                MESSAGE('Client account, ' & CLIP(L_LG:ClientName) & ' (no. ' & L_LG:ClientNo & ') has been closed!||Please select a valid client.', 'Update Deliveries', ICON:Hand)
                CLEAR(DEL:CID)
                CLEAR(L_LG:ClientName)
                CLEAR(L_LG:ClientNo)
                SELECT(?L_LG:ClientName)
                CYCLE
          .  .
          IF SELF.Request <> ViewRecord
             IF RECORDS(Queue:Browse) <= 0 AND RECORDS(Queue:Browse:4) <= 0
                CASE MESSAGE('You have not added any items to this delivery.||Would you still like to exit?', 'Update Delivery', ICON:Exclamation, BUTTON:Yes+BUTTON:No,BUTTON:No)
                OF BUTTON:No
                   QuickWindow{PROP:AcceptAll}    = FALSE
                   SELECT(?Sheet_Items)
                   CYCLE
                .
             ELSE
                DO Charge_Calc
      
                IF L_CC:TotalCharge = 0.0 OR DEL:Charge = 0.0
                   CASE MESSAGE('The Total Charge is: ' & CLIP(LEFT(FORMAT(L_CC:TotalCharge, @n-12.2))) & '|The Charge is: ' & CLIP(LEFT(FORMAT(DEL:Charge, @n-12.2))) &'|||Do you want to continue?', 'Update Delivery', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
                   OF BUTTON:No
                      QuickWindow{PROP:AcceptAll} = FALSE
                      SELECT(?DEL:Rate)
                      CYCLE
             .  .  .
      
             IF DEL:Terms = 1
                IF DEL:DC_ID = 0
                   CASE MESSAGE('This is a COD delivery would you like to enter a COD Address?', 'Update Delivery', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                   OF BUTTON:Yes
                      QuickWindow{PROP:AcceptAll} = FALSE
                      SELECT(?Button_COD_Address)
                      CYCLE
             .  .  .
      
             IF DEL:CID = 0
                QuickWindow{PROP:AcceptAll} = FALSE
                SELECT(?L_LG:ClientName)
                CYCLE
             .
      
             IF DEL:DIDate > TODAY() OR DEL:DIDate < TODAY() - 7
                L_FCG:DI_Date_Checked += 1
                IF L_FCG:DI_Date_Checked = 1
                   MESSAGE('Are you sure that the DI Date is correct?||Please check it.||Todays date is: ' & FORMAT(TODAY(),@d6) & '  (' & Week_Day(TODAY()) & ')', 'Update Delivery', ICON:Exclamation)
                   QuickWindow{PROP:AcceptAll} = FALSE
                   SELECT(?DEL:DIDate)
                   CYCLE
                ELSE
                   CASE MESSAGE('Please confirm that the DI Date is correct.||Todays date is: ' & FORMAT(TODAY(),@d6) & '  (' & Week_Day(TODAY()) & ')', 'Update Delivery', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
                   OF BUTTON:No
                      QuickWindow{PROP:AcceptAll} = FALSE
                      SELECT(?DEL:DIDate)
                      CYCLE
             .  .  .
      
             ! Check Multi Part Delivery
             IF DEL:DELCID ~= 0
                DO Check_DelComp
                IF DEL:DELCID = 0
                   QuickWindow{PROP:AcceptAll} = FALSE
                   SELECT(?Tab_Multi)
                   CYCLE
             .  .         
             
              IF DEL:CollectionAID = DEL:DeliveryAID
                   MESSAGE('The Collection address cannot be the same as the Delivery address.||Please select a new Collection or Delivery address.','Update Delivery',ICON:Exclamation)
                   QuickWindow{PROP:AcceptAll} = FALSE
                   SELECT(?L_LG:From_Address)
                   CYCLE                        
          .  .  
          ! Set variables on successful validation
          IF SELF.Request <> ViewRecord
             DEL:UID     = GLO:UID
      
             IF L_SG:VATRate_Previous ~= DEL:VATRate
      			DEL:VATRate_OverriddenUserID  = TRUE
      		.
      		
             ! Check if there was / is a POD Message for the Client - if there is then add it to the 
             ! DEL:SpecialDeliveryInstructions field
                     
      		! (p:Add , p:List , p:Delim, p:Option, p:Prefix, p:Beginning)
          	! (STRING, *STRING, STRING , BYTE=0  , <STRING>, BYTE=0)
      		LOC:Str				= DEL:SpecialDeliveryInstructions
      		IF INSTRING(L_LG:PODMessage, LOC:Str, 1, 1) <= 0
      		   Add_to_List(L_LG:PODMessage, LOC:Str, ', ')				
      		   DEL:SpecialDeliveryInstructions		= LOC:Str
      		.
      				
      		!MESSAGE(DEL:SpecialDeliveryInstructions)
      	
          .
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeCompleted()
      IF ReturnValue = LEVEL:Benign
         ! We have completed processing and validation.. ??
         ! If we are inserting, check if we should invoice this now
         IF L_CG:GenerateInvoice = TRUE
            IF Check_Delivery_Invoiced(DEL:DID) = 0
               Gen_Invoice_s(DEL:DID)
            .
  
            ! Check to see if the Invoice has been printed before, if not then ask user if theyd like to now
            IF Check_Delivery_Invoiced(DEL:DID, 1) = 0    ! Not printed
               CASE MESSAGE('Invoice has been generated.||Print it now?', 'Update Delivery - Invoice Print', ICON:Question, |
                              BUTTON:Yes+BUTTON:No, BUTTON:Yes)
               OF BUTTON:Yes
                  Print_Cont(DEL:DID, 0, 1)                  ! Use a DID to print the Invoice    
         .  .  . 
      
      .
  
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
          IF CHOICE(?CurrentTab) = 2
        !  db.debugout('[Update_Deliveries]  CurrentTab - 2 - start')
             ! (p:DID, p:Option)
             ! p:Option          0. = Number
             !                   1. = Total Cost
             !                   2. = Total Cost inc.
             !                   3. = TIDs
      
             IF Get_DelLegs_Info(DEL:DID, 0) = 0
                HIDE(?Prompt_LegCosts)
             ELSE
                ?Prompt_LegCosts{PROP:Text}   = 'Total costs for Legs: R ' & CLIP(LEFT(FORMAT(Get_DelLegs_Info(DEL:DID, 2), @n12.2)))
                UNHIDE(?Prompt_LegCosts)
             .
        !  db.debugout('[Update_Deliveries]  CurrentTab - 2 - end')
          .  !.
    OF ?DEL:DIDate
          DO Date_Changed
    OF ?Browse:ItemsContainers
          IF RECORDS(Queue:Browse) <= 0 AND RECORDS(Queue:Browse:4) <= 0
             POST(EVENT:Accepted, ?Insert:5)
          .
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all Selected events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?Browse:ItemsLoose
          IF RECORDS(Queue:Browse) <= 0 AND RECORDS(Queue:Browse:4) <= 0
             POST(EVENT:Accepted, ?Insert)
          .
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseDown
        GLO:ClosingDown   = TRUE
    OF EVENT:OpenWindow
          LOAD2:LTID                  = DEL:LTID
          IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
             ENABLE(?Sheet_Items)
      
             LOC:Load_Type_Group    :=: LOAD2:Record

             ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
             IF L_LT:LoadOption = 1 OR L_LT:LoadOption = 2 OR L_LT:LoadOption = 4
                SELECT(?Tab_Containers)
             ELSE
                SELECT(?Tab_Loose)
             .
      
             DO Floor_Changed
          ELSE
             IF SELF.Request ~= InsertRecord
                DISABLE(?Sheet_Items)
          .  .
          ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
          IF L_LT:LoadOption = 0 OR L_LT:LoadOption = 1 OR L_LT:LoadOption = 2 OR L_LT:LoadOption = 4 OR L_LT:ContainerParkStandard = TRUE
          ELSE
             SELECT(?Tab_Loose)
      
             IF SELF.Request = InsertRecord
                SELECT(FIRSTFIELD())
             ELSE
                SELECT(?Sheet_Items)
          .  .
      
      
             ?Sheet_Items{PROP:Wizard}    = TRUE
             ?Sheet_Items{PROP:NoSheet}   = TRUE
          ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
          IF L_LT:LoadOption = 1 OR L_LT:LoadOption = 2 OR L_LT:LoadOption = 4 OR L_LT:ContainerParkStandard = TRUE
             SELECT(?Tab_Containers)
          ELSE
             SELECT(?Tab_Loose)
          .
          ?String_Rate_Info{PROP:Text}    = ''
          IF DEL:DC_ID ~= 0
             DCADD:DC_ID = DEL:DC_ID
             IF Access:Delivery_CODAddresses.TryFetch(DCADD:PKey_DC_ID) = LEVEL:Benign
                ?Button_COD_Address{PROP:Tip} = 'COD Address specified is:<13,10>' & CLIP(DCADD:AddressName) & '<13,10>' & CLIP(DCADD:Line1) & |
                                                 CLIP(DCADD:Line2) & '<13,10>' & CLIP(DCADD:Line3) & '<13,10>' & CLIP(DCADD:Line4) & |
                                                 '<13,10>' & CLIP(DCADD:Line5) & '<13,10>VAT No.: ' & CLIP(DCADD:VATNo)
          .  .
          BRA:BID     = DEL:BID
          IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
             ?String_Branch{PROP:Text}    = BRA:BranchName
             ?String_Branch:2{PROP:Text}  = BRA:BranchName
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF (RECORDS(Queue:Browse:4) OR RECORDS(Queue:Browse)) AND SELF.Request ~= InsertRecord
             ! We don't want Load Type changed when we already have Items with the current specified Load Type
             DISABLE(?Group_LoadType)
          .
          ! Check if this part of a multi part delivery or not
          IF DEL:DELCID = 0
             ?Button_Specify{PROP:Text}       = 'Specify Multi-Part'
             HIDE(?Prompt_Multi)
          ELSE
             ?Button_Specify{PROP:Text}       = 'Change Multi-Part'
      
             UNHIDE(?Prompt_Multi)
             LOC:DeliveryComposition_Ret_Group = Get_Delivery_Info(DEL:DELCID)
      
             ?Tab_Multi{PROP:Font,4}          = FONT:Bold
          .
      
      
          ! DEL:DINo
          IF SELF.Request = InsertRecord
             SELECT(?DEL:DINo, ?DEL:DINo{PROP:SelStart}, ?DEL:DINo{PROP:SelEnd})
          .
          IF SELF.Request = InsertRecord                          ! p:Option is used here
             IF p:Option = 2
                ! 1st thing we want to do is request the user to choose / specify the Delivery Composition
                MESSAGE('Next, please specify the Multi-Part Delivery Composition that this DI is to be part of.', 'Insert Delivery', ICON:Asterisk)
      
                L_DC2:Start_of_Insert  = TRUE
      
                SELECT(?Tab_Multi)
                POST(EVENT:Accepted, ?Button_Specify)
             .
          ELSE
             ?DEL:DIDate{PROP:Tip}    = 'Created on ' & FORMAT(DEL:CreatedDate,@d5b) & ' @ ' & FORMAT(DEL:CreatedTime,@t4b)
          .
    ELSE
      CASE EVENT()
      OF EVENT:User
         BRW_DelItemsAlias.ResetFromFile()
  
         IF L_CC:TotalUnits > 0          ! If we have items, then go to the charges Tab...
            IF DI_Items_Action() = 2
               SELECT(?Tab_Charges)
            ELSE
      .  .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW_DeliveryItems.AskRecord PROCEDURE(BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
      db.debugout('[Update_Deliveries]  BRW4 Ask Record')
  ReturnValue = PARENT.AskRecord(Request)
  RETURN ReturnValue


BRW_DeliveryItems.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:5
    SELF.ChangeControl=?Change:5
    SELF.DeleteControl=?Delete:5
  END
  SELF.ViewControl = ?View                                 ! Setup the control used to initiate view only mode


BRW_DeliveryItems.SetQueueRecord PROCEDURE

  CODE
      ! (p:DID, p:No, p:MAN_DIID_List, p:List_Option, p:DIID)
      ! (ULONG, SHORT, <*STRING>, BYTE=0, ULONG=0),ULONG, PROC
      ! Look through all the Items for the Delivery and enumerate the MAN:MIDs into a Q
      ! p:No
      !   0. Return number of different MIDs (for this DID)
      !   x. Return this number MID - return the nth MID (for this DID)
      !
      ! p:List_Option
      !   0. Return list with MID, DIID, MID, DIID
      !   x. Return list with MID, MID
      !
      ! If not omitted - p:MAN_DIID_List
      !       Load it with MID,DIID,MID,DIID - which is the MIDs and the 1st DIID on each
      !
      ! A delivery item can be loaded on different trailers - i.e. different ManLoads
      ! Item can appear on more than one ManifestLoad but a single ManifestLoad can only have 1 instance of the Item
      !
      ! If p:DIID provided then only get for this DIID
  
  
      db.debugout('[Update_Deliveries]  Del Items - start')
  
      CLEAR(L_IG:MID)
      Get_Delivery_ManIDs(DEL:DID, 0, L_IG:MID, , DELI:DIID)
  
      db.debugout('[Update_Deliveries]  Del Items - end')
  
  
  
  
  
  
  
  
  !    OPEN(ManLD)
  !    ManLD{PROP:Filter}  = 'MALD:DIID = DELI:DIID'
  !    ManLD{PROP:Order}   = 'MAL:MID,MAL:MLID'
  !    SET(ManLD)
  !
  !    LOOP
  !       NEXT(ManLD)
  !       IF ERRORCODE()
  !          BREAK
  !       .
  !
  !       IF L_IG:Last_MID = MAL:MID
  !          CYCLE
  !       .
  !       L_IG:Last_MID = MAL:MID
  !
  !       IF CLIP(L_IG:MID) = ''
  !          L_IG:MID  = MAL:MID
  !       ELSE
  !          L_IG:MID  = CLIP(L_IG:MID) & ', ' & MAL:MID
  !    .  .
  !
  !    CLOSE(ManLD)
  
  
  
  PARENT.SetQueueRecord
  


BRW_DeliveryItems.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW4::LastSortOrder <> NewOrder THEN
     BRW4::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW4::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_DeliveryItems.TakeNewSelection PROCEDURE

  CODE
  IF BRW4::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW4::PopupTextExt = ''
        BRW4::PopupChoiceExec = True
        BRW4::FormatManager.MakePopup(BRW4::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW4::PopupTextExt = '|-|' & CLIP(BRW4::PopupTextExt)
        END
        BRW4::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW4::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW4::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW4::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW4::PopupChoiceOn AND BRW4::PopupChoiceExec THEN
     BRW4::PopupChoiceExec = False
     BRW4::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW4::PopupTextExt)
     IF BRW4::FormatManager.DispatchChoice(BRW4::PopupChoice)
     ELSE
     END
  END


BRW_DelItemsAlias.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END
  SELF.ViewControl = ?View:2                               ! Setup the control used to initiate view only mode


BRW_DelItemsAlias.ResetFromView PROCEDURE

L_LG:No_Items:Cnt    LONG                                  ! Count variable for browse totals
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:DeliveryItemAlias2.SetQuickScan(1)
  SELF.Reset
  IF SELF.UseMRP
     IF SELF.View{PROP:IPRequestCount} = 0
          SELF.View{PROP:IPRequestCount} = 60
     END
  END
  LOOP
    IF SELF.UseMRP
       IF SELF.View{PROP:IPRequestCount} = 0
            SELF.View{PROP:IPRequestCount} = 60
       END
    END
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      SETCURSOR()
      RETURN
    END
    SELF.SetQueueRecord
    L_LG:No_Items:Cnt += 1
  END
  SELF.View{PROP:IPRequestCount} = 0
  L_LG:No_Items = L_LG:No_Items:Cnt
  PARENT.ResetFromView
  Relate:DeliveryItemAlias2.SetQuickScan(0)
  SETCURSOR()


BRW_DelItemsAlias.SetQueueRecord PROCEDURE

  CODE
      ! (p:DID, p:No, p:MAN_DIID_List, p:List_Option, p:DIID)
      db.debugout('[Update_Deliveries]  Del Items Alias - start')
  
      Get_Delivery_ManIDs(DEL:DID, 0, L_IG:MID, , A_DELI2:DIID)
  
      db.debugout('[Update_Deliveries]  Del Items Alias - end')
  
  !    OPEN(ManLD)
  !    ManLD{PROP:Filter}  = 'MALD:DIID = A_DELI:DIID'
  !    ManLD{PROP:Order}   = 'MAL:MID,MAL:MLID'
  !    SET(ManLD)
  !
  !    LOOP
  !       NEXT(ManLD)
  !       IF ERRORCODE()
  !          BREAK
  !       .
  !
  !       IF L_IG:Last_MID = MAL:MID
  !          CYCLE
  !       .
  !       L_IG:Last_MID = MAL:MID
  !
  !       IF CLIP(L_IG:MID) = ''
  !          L_IG:MID  = MAL:MID
  !       ELSE
  !          L_IG:MID  = CLIP(L_IG:MID) & ', ' & MAL:MID
  !    .  .
  !
  !    CLOSE(ManLD)
  PARENT.SetQueueRecord
  


BRW_DelItemsAlias.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW20::LastSortOrder <> NewOrder THEN
     BRW20::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW20::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_DelItemsAlias.TakeNewSelection PROCEDURE

  CODE
  IF BRW20::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW20::PopupTextExt = ''
        BRW20::PopupChoiceExec = True
        BRW20::FormatManager.MakePopup(BRW20::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW20::PopupTextExt = '|-|' & CLIP(BRW20::PopupTextExt)
        END
        BRW20::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW20::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW20::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW20::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW20::PopupChoiceOn AND BRW20::PopupChoiceExec THEN
     BRW20::PopupChoiceExec = False
     BRW20::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW20::PopupTextExt)
     IF BRW20::FormatManager.DispatchChoice(BRW20::PopupChoice)
     ELSE
     END
  END


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:7
    SELF.ChangeControl=?Change:7
    SELF.DeleteControl=?Delete:7
  END


BRW6.SetQueueRecord PROCEDURE

  CODE
      L_DLG:VAT   = DELL:Cost * (DELL:VATRate / 100)
      L_DLG:Total = DELL:Cost + L_DLG:VAT
  PARENT.SetQueueRecord
  


BRW6.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW6::LastSortOrder <> NewOrder THEN
     BRW6::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW6::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW6.TakeNewSelection PROCEDURE

  CODE
  IF BRW6::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW6::PopupTextExt = ''
        BRW6::PopupChoiceExec = True
        BRW6::FormatManager.MakePopup(BRW6::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW6::PopupTextExt = '|-|' & CLIP(BRW6::PopupTextExt)
        END
        BRW6::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW6::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW6::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW6::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW6::PopupChoiceOn AND BRW6::PopupChoiceExec THEN
     BRW6::PopupChoiceExec = False
     BRW6::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW6::PopupTextExt)
     IF BRW6::FormatManager.DispatchChoice(BRW6::PopupChoice)
     ELSE
     END
  END


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:9
    SELF.ChangeControl=?Change:9
    SELF.DeleteControl=?Delete:9
  END


BRW8.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW8::LastSortOrder <> NewOrder THEN
     BRW8::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW8::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  IF BRW8::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW8::PopupTextExt = ''
        BRW8::PopupChoiceExec = True
        BRW8::FormatManager.MakePopup(BRW8::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW8::PopupTextExt = '|-|' & CLIP(BRW8::PopupTextExt)
        END
        BRW8::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW8::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW8::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW8::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW8::PopupChoiceOn AND BRW8::PopupChoiceExec THEN
     BRW8::PopupChoiceExec = False
     BRW8::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW8::PopupTextExt)
     IF BRW8::FormatManager.DispatchChoice(BRW8::PopupChoice)
     ELSE
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.RemoveControl(?Group_LoadType)                      ! Remove ?Group_LoadType from the resizer, it will not be moved or sized


FDB27.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      ! Check that there are Rates for this Floor - only show Floors with Rates
      ! (p:FID, p:JID, p:Eff_Date, p:Mass, p:Rate)
  
      ! Only do this if we are using Container Park Rates
      ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
      IF L_LT:LoadOption = 1
         IF Get_Floor_Rate(FLO:FID, DEL:JID, DEL:DIDate) <= 0
            ReturnValue    = Record:Filtered
            RETURN(ReturnValue)
      .  .
  
  
  
  ReturnValue = PARENT.ValidateRecord()
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Source
!!! Using in silent mode and none silent - NOT USED YET
!!! </summary>
Manifest_Emails      PROCEDURE                             ! Declare Procedure
LOC_Add_Q            QUEUE,PRE(L_AQ)                       !
EmailAddress         STRING(255)                           !
EmailName            STRING(35)                            !
DID                  ULONG                                 !
                     END                                   !
!Load_Q_Class        CLASS
!
!VM_DD                 &ViewManager
!
!InitMan               PROCEDURE()  
!InitTrip              PROCEDURE()  
!
!Loop_                 PROCEDURE()
!
!Construct             PROCEDURE()
!Destruct              PROCEDURE()
!                    .
!View_               VIEW(ManifestLoad)
!                      PROJECT(MAL:MLID, MAL:TTID)
!                      JOIN(TRU:PKey_TTID, MAL:TTID)
!                        PROJECT(TRU:Registration,TRU:Capacity,TRU:TID)
!                      .
!
!                      JOIN(MALD:FSKey_MLID_DIID, MAL:MLID)
!                        PROJECT(MALD:DIID, MALD:UnitsLoaded)
!                        JOIN(DELI:PKey_DIID, MALD:DIID)
!                          PROJECT(DELI:DID, DELI:CMID)
!                          JOIN(COM:PKey_CMID, DELI:CMID)
!                            PROJECT(COM:Commodity)
!                          .  
!                                
!                          JOIN(DEL:PKey_DID, DELI:DID)
!                            PROJECT(DEL:CID, DEL:ClientReference, DEL:NoticeEmailAddresses)
!                            JOIN(CLI:PKey_CID, DEL:CID)
!                              PROJECT(CLI:ClientName,CLI:ClientNo)
!                            .
!                            
!                            JOIN(EMAI:FKey_CID, DEL:CID)
!                              PROJECT(EMAI:EAID,EMAI:EmailName,EMAI:EmailAddress,EMAI:DefaultAddress, |
!                                EMAI:RateLetter,EMAI:Operations,EMAI:OperationsReference)
!                    . . . . .
!                            
!View2_              VIEW(TripSheetDeliveries)
!                      PROJECT(TRDI:TDID, TRDI:TRID, TRDI:UnitsLoaded, TRDI:DIID)
!
!                      JOIN(DELI:PKey_DIID, TRDI:DIID)                        
!                          PROJECT(DELI:DID, DELI:CMID)
!                                
!                          JOIN(DEL:PKey_DID, DELI:DID)
!                            PROJECT(DEL:CID, DEL:ClientReference, DEL:NoticeEmailAddresses)
!!                            JOIN(CLI:PKey_CID, DEL:CID)
!!                              PROJECT(CLI:ClientName,CLI:ClientNo)
!!                            .
!!                            
!!                            JOIN(EMAI:FKey_CID, DEL:CID)
!!                              PROJECT(EMAI:EAID,EMAI:EmailName,EMAI:EmailAddress,EMAI:DefaultAddress, |
!!                                    EMAI:RateLetter,EMAI:Operations,EMAI:OperationsReference)
!                    . .   . !.
!                              
!VM_   ViewManager
!Send_Emails          PROCEDURE()
!Merge_Content         PROCEDURE(ULONG p:DID, STRING p:Content),STRING
!Load_Q_DIs            PROCEDURE()
!Check_Add_Q           PROCEDURE(ULONG pDID, STRING pEmailName, STRING pEmailAddress, BYTE pOption=0)
!Load_Content_File     PROCEDURE(STRING),STRING
!
!

  CODE
!Send_Email                PROCEDURE()
!  
!r:idx         LONG
!r:send_fails  LONG
!r:content     LIKE(LO_EG:Content)
!
!!r:stuff       STRING(1000)
!
!   CODE
!   ! Save all the stuff you want to save here
!   IF RECORDS(LOC_Add_Q) <= 0
!     MESSAGE('There aren''t any email addresses to send to.')
!   ELSE
!     r:idx = 0
!     LOOP
!       r:idx += 1
!       GET(LOC_Add_Q, r:idx)
!       IF ERRORCODE()
!         BREAK
!       .
!
!       r:content = Merge_Content(L_AQ:DID, LO_EG:Content)
!       
! !      Add_to_List(L_ESG:Subject, r:stuff, ',')
! !      Add_to_List(L_ESG:SMTP_Username, r:stuff, ',')
! !      Add_to_List(L_ESG:Server, r:stuff, ',')
! !      Add_to_List(L_ESG:From, r:stuff, ',')
! !      Add_to_List(L_AQ:EmailAddress, r:stuff, ',')
! !      CASE message('content:<13,10> >' & CLIP(r:content) & '<13,10> > <13,10> <13,10> email stuff: ' & CLIP(r:stuff) & '<13,10>__',,, 'Set clip?|No',1)
! !      OF 1
! !        SETCLIPBOARD(r:content)
! !      .      
!       
!       ! (p_Subject, p_Text, p_EmailAccount, p_HTML, p_Server, p_AuthUser, p_AuthPassword, p_From, p_To, p_CC, p_BCC, 
!       ! p_Helo, p_Attach)
!   !    MESSAGE('add: ' & CLIP(L_AQ:EmailAddress))
!       IF ( Send_Email(L_ESG:Subject, r:content, 0,, L_ESG:Server, L_ESG:SMTP_Username, L_ESG:SMTP_Password |
!                       , L_ESG:From, CLIP(L_AQ:EmailAddress), L_ESG:CC, L_ESG:BCC) ) < 0
!          r:send_fails += 1
!     . .
!     
!     IF r:send_fails > 0
!       MESSAGE(r:send_fails & ' out of the ' & RECORDS(LOC_Add_Q) & ' emails failed to send.|||You can review the options and try again','Email Sending',ICON:Exclamation)
!     ELSE
!       MESSAGE('Email sending complete.')
!       POST(EVENT:CloseWindow)
!   . .
!    
!   RETURN
!         
!Merge_Content       PROCEDURE(ULONG p:DID, STRING p:Content)
!L:Content             LIKE(LO_EG:Content)
!
!L:Count               LONG(0)
!L:Commodities         STRING(255)
!L:Units               LONG
!L:Weight              DECIMAL(12)
!                      
!Del_View              VIEW(Deliveries)
!                        PROJECT(DEL:DID,DEL:DINo,DEL:ClientReference,DEL:CollectionAID,DEL:DeliveryAID,DEL:Notes,DEL:ReceivedDate,DEL:ReceivedTime)
!                        JOIN(DELI:FKey_DID_ItemNo,DEL:DID)
!                          PROJECT(DELI:Units,DELI:Weight,DELI:CMID)
!                          JOIN(COM:PKey_CMID, DELI:CMID)
!                            PROJECT(COM:Commodity)
!                        . .
!                        JOIN(CLI:PKey_CID, DEL:CID)
!                          PROJECT(CLI:ClientName)
!                        .
!                        JOIN(FLO:PKey_FID, DEL:FID)
!                          PROJECT(FLO:Floor)
!                      . .
!
!View_Del              ViewManager
!
!L:TripSheets          STRING(150)
!L:TS_Veh              STRING(100)
!L:BranchTel           STRING(100)
!L:Del_Date            STRING(100)
!
!  CODE
!  L:Content = p:Content
!    
!  View_Del.Init(Del_View, Relate:Deliveries)
!  View_Del.AddSortOrder(DEL:PKey_DID)
!  View_Del.AppendOrder('DELI:ItemNo')
!  View_Del.AddRange(DEL:DID, p:DID)
!  !View_Del.SetFilter()
! 
!  View_Del.Reset()    
!  LOOP
!    IF View_Del.Next() ~= LEVEL:Benign        ! Failed to fetch the DI.. 
!      BREAK      
!    ELSE    
!      L:Count += 1
!      
!      IF L:Count = 1                          ! some fields we only have one of
!        ! Client Name|Collection Add|Delivery Add|Client Ref|Commodity|Items|Weight|Special Remarks|ETA|Branch|Dispatch Vehicle|Delivery Date/Time
!        ! (p:String, p:Find, p:Replace, p:Occurances, p:No_Case)
!        ! (*STRING, STRING, STRING, LONG=0, BYTE=0), LONG
!        Replace_Strings(L:Content, '[Client Name]', CLIP(CLI:ClientName))
!        Replace_Strings(L:Content, '[Collection Add]', CLIP(Get_Address(DEL:CollectionAID,,1)))
!        Replace_Strings(L:Content, '[Delivery Add]', CLIP(Get_Address(DEL:DeliveryAID,,1)))
!        Replace_Strings(L:Content, '[Client Ref]', CLIP(DEL:ClientReference))
!        
!        ! Check for a TripSheet delivery
!        TRDI:DIID = DELI:DIID        
!        IF Access:TripSheetDeliveries.TryFetch(TRDI:FKey_DIID) = Level:Benign
!           ! We have at least one, use this one for now!     
!           L:Del_Date  = FORMAT(TRDI:DeliveredDate,@d6) & ' ' & FORMAT(TRDI:DeliveredTime, @t1)
!      . .
!      
!      Add_to_List(CLIP(COM:Commodity), L:Commodities, ', ')
!      L:Units   += DELI:Units      
!      L:Weight  += DELI:Weight        ! do they want volumetric?
!    .
!  .
!  View_Del.Kill()    
!
!  Replace_Strings(L:Content, '[Commodity]', CLIP(L:Commodities))
!  Replace_Strings(L:Content, '[Items]', L:Units)
!  Replace_Strings(L:Content, '[Weight]', L:Weight)
!  Replace_Strings(L:Content, '[Special Remarks]', CLIP(DEL:Notes))
!    
!  !Branch|Dispatch Vehicle|Delivery Date/Time
!  ! Branch is the DI FID name
!  Replace_Strings(L:Content, '[Branch]', FLO:Floor)
!
!  ! Get delivery vehicle info
!  ! Returns comma del list of tripsheets in first to last order
!  L:TripSheets  = Get_Delivery_TripSheets(DEL:DID)
!  TRI:TRID      = Get_1st_Element_From_Delim_Str(L:TripSheets,',',1)
!  IF Access:TripSheets.TryFetch(TRI:PKey_TID) = LEVEL:Benign
!      ! p:Option  1.  Capacity                            of all
!      !           2.  Registrations                       of all OR p:Truck = 100 gives horse reg only
!      !           3.  Makes & Models                      of all
!      !           4.  Description of Truck / Trailer      requires p:Truck  0 to 3
!      !           5.  Truck / Trailer Capacity            requires p:Truck  0 to 3
!      !           6.  Truck / Trailer ID (TTID)           requires p:Truck  0 to 3
!      !           7.  Count of vehicles in composition
!      !           8.  Compositions Capacity
!      !           9.  Driver of Horse
!      !           10. Transporters ID for this VCID
!      !           11. Licensing dates                     of all
!      L:TS_Veh    = Get_VehComp_Info(TRI:VCID, 2)
!      L:BranchTel = Get_Address(Get_Branch_Info(TRI:BID, 2), 3)   ! could return comma del list      
!  . 
!    
!  Replace_Strings(L:Content, '[Dispatch Vehicle]', CLIP(L:TS_Veh))     ! Trip sheet  
!  Replace_Strings(L:Content, '[Branch Tel.]', CLIP(L:BranchTel))  
!
!  ! see above in loop
!  Replace_Strings(L:Content, '[Delivery Date/Time]', CLIP(L:Del_Date))
!
!    
!  t#  = DEFORMAT('13:00', @t1)     ! hh:mm      
!  Date_Time_Advance(MAN:DepartDate, MAN:DepartTime, 1, t#)
!  Replace_Strings(L:Content, '[ETA]', FORMAT(MAN:DepartDate,@d6) & ' @ ' & FORMAT(MAN:DepartTime, @t1))    
!
!  RETURN(L:Content)
!    
!    ! Date_Time_Advance
!    ! (*DATE, <*TIME>, BYTE, LONG)
!    ! (p_Date, p_Time, p_Option, p_Val)
!    !
!    ! p_Option
!    !       1 - Time
!    !       2 - Days
!    !       3 - Weeks
!    !       4 - Months
!    !       5 - Quarters
!    !       6 - Years
!    
!    ! Get_Address
!    ! (p:AID, p:Type, p:Details)
!    ! (ULONG, BYTE=0, BYTE=0),STRING
!    ! p:Type
!    !   0 - Comma delimited string
!    !   1 - Block
!    !   2 - Comma delimited string - including empty Line 1 & 2 where they are empty
!    !   3 - Phone Nos. (comma delim)
!    ! p:Details
!    !   0 - all
!    !   1 - without City & Province
!    !
!    ! Returns
!    !   Address Name, Line1 (if), Line2 (if), Post Code, City, Province
!    !   or
!    !   Address Name, Line1 (if), Line2 (if), Post Code
!    !   or
!    !   Address Name, Line1 (if), Line2 (if), <missing info string>
!    !   or
!    !   Phone1, Phone2 OR Phone1 OR Phone 2
!Load_Q_DIs          PROCEDURE()
!cl    Load_Q_Class
!  CODE
!  IF p:Option = 11 OR p:Option = 12
!    cl.InitMan()
!  ELSIF p:Option = 13 OR p:Option = 14
!    cl.InitTrip()
!  .    
!  cl.Loop_()        
!  RETURN
!  
!Load_Q_Class.InitTrip   PROCEDURE()
!  CODE
!  IF TRI:TRID ~= p:ID
!    CLEAR(TRI:Record)
!    TRI:TRID = p:ID
!    IF Access:TripSheets.TryFetch(TRI:PKey_TID) ~= Level:Benign
!      ! return??
!  . .
!  SELF.VM_DD.Init(View2_, Relate:TripSheetDeliveries)
!  SELF.VM_DD.AddSortOrder(TRDI:FKey_TRID)
!  SELF.VM_DD.AppendOrder('CLI:ClientName,DEL:DINo')
!  !SELF.VM_DD.SetFilter('EMAI:Operations = 1')
!  SELF.VM_DD.AddRange(TRDI:TRID,p:ID)
!  SELF.VM_DD.Reset(1)
!
!  IF ERRORCODE()
!    MESSAGE('Error on view trip sheet reset: ' & ERROR())
!  .
!  RETURN
!
!Load_Q_Class.InitMan    PROCEDURE()
!  CODE
!  IF MAN:MID ~= p:ID
!    CLEAR(MAN:Record)
!    MAN:MID = p:ID
!    IF Access:Manifest.TryFetch(MAN:PKey_MID) ~= Level:Benign
!      ! return??
!  . .
!
!  SELF.VM_DD.Init(View_, Relate:ManifestLoad)
!  SELF.VM_DD.AddSortOrder(MAL:FKey_MID)
!  SELF.VM_DD.AppendOrder('MAL:MLID,CLI:ClientName,DEL:DINo')
!  !SELF.VM_DD.SetFilter('EMAI:Operations = 1')
!  SELF.VM_DD.AddRange(MAL:MID,p:ID)
!  SELF.VM_DD.Reset(1)
!
!  IF ERRORCODE()
!    MESSAGE('Error on view man reset: ' & ERROR())
!  .
!  RETURN
!    
!
!Load_Q_Class.Loop_   PROCEDURE()
!  CODE  
!  LOOP
!    IF SELF.VM_DD.Next() ~= Level:Benign
!      BREAK
!    .
!
!    IF CLIP(DEL:NoticeEmailAddresses) <> ''
!      Check_Add_Q(DEL:DID, '', DEL:NoticeEmailAddresses)      ! Add unique address to address q LOC_Add_Q
!  . .
!
!  SELF.VM_DD.Kill()  
!  RETURN
!      
!    
!Load_Q_Class.Construct  PROCEDURE()
!  CODE
!  FREE(LOC_Add_Q)  
!  SELF.VM_DD   &= NEW(ViewManager)
!  RETURN
!    
!Load_Q_Class.Destruct  PROCEDURE()
!  CODE
!  DISPOSE(SELF.VM_DD)
!  RETURN
!Check_Add_Q         PROCEDURE(pDID, pEmailName, pEmailAddress, pOption)
!  CODE
!    
!  IF pOption = 0
!    ! Add unique address to address q
!    L_AQ:DID          = pDID
!    L_AQ:EmailName    = pEmailName
!    L_AQ:EmailAddress = pEmailAddress
!    
!    GET(LOC_Add_Q, L_AQ:EmailAddress)    ! Unique Addresses
!    IF ERRORCODE()      
!      ADD(LOC_Add_Q)      
!    .
!  ELSE
!    ! delete from Q
!    L_AQ:EmailAddress = pEmailAddress
!    
!    GET(LOC_Add_Q, L_AQ:EmailAddress)    ! Unique Addresses
!    IF ~ERRORCODE()      
!      DELETE(LOC_Add_Q)
!  . .
!  RETURN
!Load_Content_File   PROCEDURE(STRING p_Type)
!
!mf FILE,DRIVER('ASCII','/FILEBUFFERS=8'), NAME('.\EmailContent.txt'),| 
!         PRE(mf_),CREATE,THREAD
!Record     RECORD,PRE()
!Line        STRING(1024)
!          END
!    END
!
!i_        LONG
!l_txt     LIKE(LO_EG:Content)
!
!  CODE
!    ! Load subject
!    L_EG:Subject  = Get_Setting('Email-' & CLIP(p_Type) & '-Subject', 1,'', 'Email subject')
!    
!
!    mf{PROP:Name} = CLIP(p_Type) & '.txt'
!      
!    OPEN(mf)
!    IF ERRORCODE()
!      ! no file yet
!!      MESSAGE('No content file found?   ' & ERROR())
!    ELSE
!      CLEAR(l_txt)
!      i_ = 0
!      
!      SET(mf)
!      LOOP
!        NEXT(mf)
!        IF ERRORCODE()
!!          MESSAGE('end of file?:  ' & ERROR() & '|||file: ' & mf{PROP:Name})
!          BREAK
!        .
!        i_ += 1
!        
!!        MESSAGE('next: ' & ERROR() & '||line ' & i_ & ': ' & mf_:Line)
!
!        IF i_ <= 1
!          l_txt   = mf_:line 
!        ELSE
!          l_txt   = CLIP(l_txt) & '<13,10>' & CLIP(mf_:line)
!        .
!      .
!      
!      CLOSE(mf)    
!    .
!    
!    L_EG:Content  = l_txt
!  
!  RETURN l_txt
!  
