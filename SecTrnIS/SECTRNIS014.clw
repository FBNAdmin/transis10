

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS014.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_UsersGroups PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Login            STRING(20)                            !User Login
FDB7::View:FileDrop  VIEW(UserGroups)
                       PROJECT(USEG:GroupName)
                       PROJECT(USEG:UGID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?USEG:GroupName
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::USEBG:Record LIKE(USEBG:RECORD),THREAD
QuickWindow          WINDOW('Form Users Groups'),AT(,,173,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('Update_UsersGroups'),SYSTEM
                       SHEET,AT(4,4,165,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Login:'),AT(9,22),USE(?LOC:Login:Prompt),TRN
                           BUTTON('...'),AT(45,22,12,10),USE(?CallLookup)
                           ENTRY(@s20),AT(61,22,103,10),USE(LOC:Login),COLOR(00E9E9E9h),MSG('User Login'),READONLY,SKIP, |
  TIP('User Login')
                           PROMPT('User Group:'),AT(9,52),USE(?Prompt2),TRN
                           LIST,AT(61,52,103,10),USE(USEG:GroupName),DROP(5),FORMAT('140L(2)|M~Group Name~@s35@'),FROM(Queue:FileDrop),MSG('Name of this group of users')
                           PROMPT('UGID:'),AT(9,66),USE(?USEBG:UGID:Prompt),TRN
                           STRING(@n_10),AT(58,66,104,10),USE(USEBG:UGID),RIGHT(1),TRN
                         END
                       END
                       BUTTON('&OK'),AT(68,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(120,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(2,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Vessel Record'
  OF InsertRecord
    ActionMessage = 'Vessel Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Vessel Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_UsersGroups')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Login:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:UsersGroups)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(USEBG:Record,History::USEBG:Record)
  SELF.AddHistoryField(?USEBG:UGID,3)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:UserGroups.Open                                   ! File UserGroups used by this procedure, so make sure it's RelationManager is open
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UsersGroups
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
      IF USE:UID = 0
         !RETURN(LEVEL:Fatal)
         ?LOC:Login{PROP:Background}  = -1
         ?LOC:Login{PROP:Req}         = TRUE
         ?LOC:Login{PROP:Skip}        = FALSE
         ?LOC:Login{PROP:ReadOnly}    = FALSE
  
         ENABLE(?CallLookup)
      ELSE
         LOC:Login    = USE:Login
         USEBG:UID    = USE:UID
      .
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?LOC:Login{PROP:ReadOnly} = True
    DISABLE(?USEG:GroupName)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_UsersGroups',QuickWindow)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  FDB7.Init(?USEG:GroupName,Queue:FileDrop.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop,Relate:UserGroups,ThisWindow)
  FDB7.Q &= Queue:FileDrop
  FDB7.AddSortOrder(USEG:Key_GroupName)
  FDB7.AddField(USEG:GroupName,FDB7.Q.USEG:GroupName) !List box control field - type derived from field
  FDB7.AddField(USEG:UGID,FDB7.Q.USEG:UGID) !Primary key field - type derived from field
  FDB7.AddUpdateField(USEG:UGID,USEBG:UGID)
  ThisWindow.AddItem(FDB7.WindowComponent)
  FDB7.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:UserGroups.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_UsersGroups',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Users
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      USE:Login = LOC:Login
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:Login = USE:Login
        USEBG:UID = USE:UID
      END
      ThisWindow.Reset(1)
    OF ?LOC:Login
      IF NOT QuickWindow{PROP:AcceptAll}
        IF LOC:Login OR ?LOC:Login{PROP:Req}
          USE:Login = LOC:Login
          IF Access:Users.TryFetch(USE:Key_Login)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              LOC:Login = USE:Login
              USEBG:UID = USE:UID
            ELSE
              CLEAR(USEBG:UID)
              SELECT(?LOC:Login)
              CYCLE
            END
          ELSE
            USEBG:UID = USE:UID
          END
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

