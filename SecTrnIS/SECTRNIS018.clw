

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module

                     MAP
                       INCLUDE('SECTRNIS018.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
UserLogout           PROCEDURE                             ! Declare Procedure

  CODE
    CLEAR(GLO:User_Group)
    POST(EVENT:User,,1)
    RETURN
