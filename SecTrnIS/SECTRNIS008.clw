

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS008.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_UsersAccesses_Extra PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Locals           GROUP,PRE(LO)                         !
ApplicationSection   STRING(35)                            !Application Section Name
ExtraSection         STRING(100)                           !
DefaultAction        STRING(20)                            !Default action for security is - Allow, View Only, No Access
Login                STRING(20)                            !User Login
                     END                                   !
FDB9::View:FileDrop  VIEW(ApplicationSections_Extras)
                       PROJECT(APPSE:ExtraSection)
                       PROJECT(APPSE:ASEID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?LO:ExtraSection
APPSE:ExtraSection     LIKE(APPSE:ExtraSection)       !List box control field - type derived from field
LO:DefaultAction       STRING(1)                      !List box control field - unable to determine correct data type
APP:DefaultAction      LIKE(APP:DefaultAction)        !Browse hot field - type derived from field
APPSE:ASEID            LIKE(APPSE:ASEID)              !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::USACE:Record LIKE(USACE:RECORD),THREAD
QuickWindow          WINDOW('Form Users Accesses Extra'),AT(,,200,139),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateUsersAccessesExtra'),SYSTEM
                       SHEET,AT(4,4,192,116),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('User Login:'),AT(9,20),USE(?Login:Prompt),TRN
                           BUTTON('...'),AT(74,20,12,10),USE(?CallLookup:2)
                           ENTRY(@s20),AT(90,20,100,10),USE(LO:Login),MSG('User Login'),REQ,TIP('User Login')
                           PROMPT('Application Section:'),AT(9,34),USE(?ApplicationSection:Prompt),TRN
                           BUTTON('...'),AT(74,34,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(90,34,100,10),USE(LO:ApplicationSection),MSG('Application Section name'),TIP('Applicatio' & |
  'n Section name')
                           PROMPT('Extra Section:'),AT(9,48),USE(?Prompt3),TRN
                           LIST,AT(90,48,100,10),USE(LO:ExtraSection),VSCROLL,DROP(15),FORMAT('100L(2)|M~Extra Sec' & |
  'tion~@s100@80L(2)|M~Default Action~@s20@'),FROM(Queue:FileDrop)
                           PROMPT('User Access Option:'),AT(9,78),USE(?USACE:AccessOption:Prompt),TRN
                           LIST,AT(90,78,100,10),USE(USACE:AccessOption),DROP(5),FROM('Allow|#0|View Only|#1|No Access|#2'), |
  MSG('Option'),TIP('Option')
                         END
                       END
                       BUTTON('&OK'),AT(94,122,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(147,122,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,122,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB9                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
SetQueueRecord         PROCEDURE(),DERIVED
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a User Access Extra Record'
  OF InsertRecord
    ActionMessage = 'User Access Extra Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'User Access Extra Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_UsersAccesses_Extra')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Login:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:UsersAccessesExtra)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(USACE:Record,History::USACE:Record)
  SELF.AddHistoryField(?USACE:AccessOption,6)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ApplicationSections.SetOpenRelated()
  Relate:ApplicationSections.Open                          ! File ApplicationSections used by this procedure, so make sure it's RelationManager is open
  Access:UsersAccesses.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UsersAccessesExtra
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup:2)
    ?LO:Login{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?LO:ApplicationSection{PROP:ReadOnly} = True
    ?LO:ExtraSection{PROP:ReadOnly} = True
    DISABLE(?USACE:AccessOption)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_UsersAccesses_Extra',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  FDB9.Init(?LO:ExtraSection,Queue:FileDrop.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop,Relate:ApplicationSections_Extras,ThisWindow)
  FDB9.Q &= Queue:FileDrop
  FDB9.AddSortOrder(APPSE:SKey_ASID_ExtraSection)
  FDB9.AddRange(APPSE:ASID,USACE:ASID)
  FDB9.AddField(APPSE:ExtraSection,FDB9.Q.APPSE:ExtraSection) !List box control field - type derived from field
  FDB9.AddField(LO:DefaultAction,FDB9.Q.LO:DefaultAction) !List box control field - unable to determine correct data type
  FDB9.AddField(APP:DefaultAction,FDB9.Q.APP:DefaultAction) !Browse hot field - type derived from field
  FDB9.AddField(APPSE:ASEID,FDB9.Q.APPSE:ASEID) !Primary key field - type derived from field
  FDB9.AddUpdateField(APPSE:ASEID,USACE:ASEID)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
      IF SELF.Request = ChangeRecord
         USE:UID      = USACE:UID
         IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
            LO:Login  = USE:Login
         .
  
         APP:ASID     = USACE:ASID
         IF Access:ApplicationSections.TryFetch(APP:PKey_ASID) = LEVEL:Benign
            LO:ApplicationSection = APP:ApplicationSection
         .
  
         APPSE:ASEID  = USACE:ASEID
         IF Access:ApplicationSections_Extras.TryFetch(APPSE:PKey_ASEID) = LEVEL:Benign
            LO:ExtraSection       = APPSE:ExtraSection
      .  .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_UsersAccesses_Extra',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_Users
      Browse_Application_Sections
      Browse_Application_Section_Extras
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup:2
      ThisWindow.Update()
      USE:Login = LO:Login
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LO:Login = USE:Login
        USACE:UID = USE:UID
      END
      ThisWindow.Reset(1)
    OF ?LO:Login
      IF LO:Login OR ?LO:Login{PROP:Req}
        USE:Login = LO:Login
        IF Access:Users.TryFetch(USE:Key_Login)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LO:Login = USE:Login
            USACE:UID = USE:UID
          ELSE
            CLEAR(USACE:UID)
            SELECT(?LO:Login)
            CYCLE
          END
        ELSE
          USACE:UID = USE:UID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      APP:ApplicationSection = LO:ApplicationSection
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LO:ApplicationSection = APP:ApplicationSection
        USACE:ASID = APP:ASID
      END
      ThisWindow.Reset(1)
    OF ?LO:ApplicationSection
      IF LO:ApplicationSection OR ?LO:ApplicationSection{PROP:Req}
        APP:ApplicationSection = LO:ApplicationSection
        IF Access:ApplicationSections.TryFetch(APP:Key_ApplicationSection)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            LO:ApplicationSection = APP:ApplicationSection
            USACE:ASID = APP:ASID
          ELSE
            CLEAR(USACE:ASID)
            SELECT(?LO:ApplicationSection)
            CYCLE
          END
        ELSE
          USACE:ASID = APP:ASID
        END
      END
      ThisWindow.Reset(1)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


FDB9.SetQueueRecord PROCEDURE

  CODE
      ! Allow|View Only|No Access
      EXECUTE APPSE:DefaultAction + 1
         LO:DefaultAction = 'Allow'
         LO:DefaultAction = 'View Only'
         LO:DefaultAction = 'No Access'
      .
  PARENT.SetQueueRecord
  

