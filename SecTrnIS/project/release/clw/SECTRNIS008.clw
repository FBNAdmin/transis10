

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS008.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Application_Sections PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:AccessOption     STRING(10)                            ! Option - Allow, View Only, No Access
LOC:ExtraAccessOption STRING(20)                           ! Option - Allow, View Only, No Access
LOC:AccessOption_Groups STRING(20)                         ! Option - Allow, View Only, No Access
BRW2::View:Browse    VIEW(UsersAccesses)
                       PROJECT(USAC:UAID)
                       PROJECT(USAC:ASID)
                       PROJECT(USAC:UID)
                       JOIN(USE:PKey_UID,USAC:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:AccessLevel)
                         PROJECT(USE:UID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
USE:AccessLevel        LIKE(USE:AccessLevel)          !List box control field - type derived from field
LOC:AccessOption       LIKE(LOC:AccessOption)         !List box control field - type derived from local data
USAC:UAID              LIKE(USAC:UAID)                !Primary key field - type derived from field
USAC:ASID              LIKE(USAC:ASID)                !Browse key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(ApplicationSections_Extras)
                       PROJECT(APPSE:ExtraSection)
                       PROJECT(APPSE:DefaultAction)
                       PROJECT(APPSE:ASEID)
                       PROJECT(APPSE:ASID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
APPSE:ExtraSection     LIKE(APPSE:ExtraSection)       !List box control field - type derived from field
LOC:ExtraAccessOption  LIKE(LOC:ExtraAccessOption)    !List box control field - type derived from local data
APPSE:DefaultAction    LIKE(APPSE:DefaultAction)      !Browse hot field - type derived from field
APPSE:ASEID            LIKE(APPSE:ASEID)              !Primary key field - type derived from field
APPSE:ASID             LIKE(APPSE:ASID)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(Application_Section_Usage)
                       PROJECT(APPSU:ProcedureName)
                       PROJECT(APPSU:LastCalledDate)
                       PROJECT(APPSU:LastCalledTime)
                       PROJECT(APPSU:ASUID)
                       PROJECT(APPSU:ASID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
APPSU:ProcedureName    LIKE(APPSU:ProcedureName)      !List box control field - type derived from field
APPSU:LastCalledDate   LIKE(APPSU:LastCalledDate)     !List box control field - type derived from field
APPSU:LastCalledTime   LIKE(APPSU:LastCalledTime)     !List box control field - type derived from field
APPSU:ASUID            LIKE(APPSU:ASUID)              !Primary key field - type derived from field
APPSU:ASID             LIKE(APPSU:ASID)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(UserGroupAccesses)
                       PROJECT(UGAC:AccessOption)
                       PROJECT(UGAC:UGAID)
                       PROJECT(UGAC:ASID)
                       PROJECT(UGAC:UGID)
                       JOIN(USEG:PKey_UGID,UGAC:UGID)
                         PROJECT(USEG:GroupName)
                         PROJECT(USEG:UGID)
                       END
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:3
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
LOC:AccessOption_Groups LIKE(LOC:AccessOption_Groups) !List box control field - type derived from local data
UGAC:AccessOption      LIKE(UGAC:AccessOption)        !Browse hot field - type derived from field
UGAC:UGAID             LIKE(UGAC:UGAID)               !Primary key field - type derived from field
UGAC:ASID              LIKE(UGAC:ASID)                !Browse key field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::APP:Record  LIKE(APP:RECORD),THREAD
QuickWindow          WINDOW('Form Application Sections'),AT(,,283,219),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateApplicationSections'),SYSTEM
                       SHEET,AT(5,2,276,196),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Application Section:'),AT(9,25),USE(?APP:ApplicationSection:Prompt),TRN
                           ENTRY(@s35),AT(93,25,144,10),USE(APP:ApplicationSection),MSG('Application Section name'),TIP('Applicatio' & |
  'n Section name')
                           PROMPT('Default Action:'),AT(9,41),USE(?APP:DefaultAction:Prompt),TRN
                           LIST,AT(93,41,80,10),USE(APP:DefaultAction),DROP(5),FROM('Allow|#0|View Only|#1|No Access|#2'), |
  MSG('Default action for security is'),TIP('Default action for security is')
                           LINE,AT(7,56,267,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           LIST,AT(9,63,267,120),USE(?List),VSCROLL,FORMAT('116L(2)|M~Extra Section~@s100@80L(2)|M' & |
  '~Default Extra Access Option~@s20@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(121,185,49,12),USE(?Insert),LEFT,ICON('WAINSERT.ICO'),FLAT
                           BUTTON('&Change'),AT(174,185,49,12),USE(?Change),LEFT,ICON('WAchange.ICO'),FLAT
                           BUTTON('&Delete'),AT(226,185,49,12),USE(?Delete),LEFT,ICON('WAdelete.ICO'),FLAT
                         END
                         TAB('&2) Group Accesses'),USE(?Tab4)
                           LIST,AT(11,24,267,153),USE(?List:3),VSCROLL,FORMAT('140L(2)|M~Group Name~@s35@80L(2)|M~' & |
  'Access Option~@s20@'),FROM(Queue:Browse:3),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(123,181,49,14),USE(?Insert:2),LEFT,ICON('WAINSERT.ICO'),FLAT
                           BUTTON('&Change'),AT(175,181,49,14),USE(?Change:2),LEFT,ICON('WACHANGE.ICO'),FLAT
                           BUTTON('&Delete'),AT(228,181,49,14),USE(?Delete:2),LEFT,ICON('WADELETE.ICO'),FLAT
                         END
                         TAB('&3) Users Accesses'),USE(?Tab:2)
                           LIST,AT(11,24,267,153),USE(?Browse:2),HVSCROLL,FORMAT('80L(2)|M~Login~@s20@50R(2)|M~Acc' & |
  'ess Level~L@n3@40L(2)|M~Access Option~@s10@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he UsersAccesses file')
                           BUTTON('&Insert'),AT(123,181,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(176,181,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(228,181,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&4) Usage (Procedures)'),USE(?Tab3)
                           LIST,AT(8,20,268,175),USE(?List:2),VSCROLL,FORMAT('120L(2)|M~Procedure Name~@s100@50R(2' & |
  ')|M~Last Called Date~L@d6@50R(2)|M~Last Called Time~L@t7@'),FROM(Queue:Browse:1),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('&OK'),AT(178,202,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(232,202,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,202,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW7                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW11                CLASS(BrowseClass)                    ! Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
                     END

BRW11::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW12                CLASS(BrowseClass)                    ! Browse using ?List:3
Q                      &Queue:Browse:3                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Application Section Record'
  OF InsertRecord
    ActionMessage = 'Application Section Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'App. Section Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Application_Sections')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?APP:ApplicationSection:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:AccessOption',LOC:AccessOption)                ! Added by: BrowseBox(ABC)
  BIND('LOC:ExtraAccessOption',LOC:ExtraAccessOption)      ! Added by: BrowseBox(ABC)
  BIND('LOC:AccessOption_Groups',LOC:AccessOption_Groups)  ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:ApplicationSections)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(APP:Record,History::APP:Record)
  SELF.AddHistoryField(?APP:ApplicationSection,2)
  SELF.AddHistoryField(?APP:DefaultAction,3)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ApplicationSections.SetOpenRelated()
  Relate:ApplicationSections.Open                          ! File ApplicationSections used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ApplicationSections
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:UsersAccesses,SELF) ! Initialize the browse manager
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:ApplicationSections_Extras,SELF) ! Initialize the browse manager
  BRW11.Init(?List:2,Queue:Browse:1.ViewPosition,BRW11::View:Browse,Queue:Browse:1,Relate:Application_Section_Usage,SELF) ! Initialize the browse manager
  BRW12.Init(?List:3,Queue:Browse:3.ViewPosition,BRW12::View:Browse,Queue:Browse:3,Relate:UserGroupAccesses,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?APP:ApplicationSection{PROP:ReadOnly} = True
    DISABLE(?APP:DefaultAction)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
    DISABLE(?Insert:2)
    DISABLE(?Change:2)
    DISABLE(?Delete:2)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW2.AddSortOrder(,USAC:FKey_ASID)                       ! Add the sort order for USAC:FKey_ASID for sort order 1
  BRW2.AddRange(USAC:ASID,Relate:UsersAccesses,Relate:ApplicationSections) ! Add file relationship range limit for sort order 1
  BRW2.AppendOrder('+USAC:UAID')                           ! Append an additional sort order
  BRW2.AddField(USE:Login,BRW2.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW2.AddField(USE:AccessLevel,BRW2.Q.USE:AccessLevel)    ! Field USE:AccessLevel is a hot field or requires assignment from browse
  BRW2.AddField(LOC:AccessOption,BRW2.Q.LOC:AccessOption)  ! Field LOC:AccessOption is a hot field or requires assignment from browse
  BRW2.AddField(USAC:UAID,BRW2.Q.USAC:UAID)                ! Field USAC:UAID is a hot field or requires assignment from browse
  BRW2.AddField(USAC:ASID,BRW2.Q.USAC:ASID)                ! Field USAC:ASID is a hot field or requires assignment from browse
  BRW2.AddField(USE:UID,BRW2.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  BRW7.Q &= Queue:Browse
  BRW7.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW7.AddSortOrder(,APPSE:SKey_ASID_ExtraSection)         ! Add the sort order for APPSE:SKey_ASID_ExtraSection for sort order 1
  BRW7.AddRange(APPSE:ASID,APP:ASID)                       ! Add single value range limit for sort order 1
  BRW7.AddLocator(BRW7::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW7::Sort0:Locator.Init(,APPSE:ExtraSection,1,BRW7)     ! Initialize the browse locator using  using key: APPSE:SKey_ASID_ExtraSection , APPSE:ExtraSection
  BRW7.AddField(APPSE:ExtraSection,BRW7.Q.APPSE:ExtraSection) ! Field APPSE:ExtraSection is a hot field or requires assignment from browse
  BRW7.AddField(LOC:ExtraAccessOption,BRW7.Q.LOC:ExtraAccessOption) ! Field LOC:ExtraAccessOption is a hot field or requires assignment from browse
  BRW7.AddField(APPSE:DefaultAction,BRW7.Q.APPSE:DefaultAction) ! Field APPSE:DefaultAction is a hot field or requires assignment from browse
  BRW7.AddField(APPSE:ASEID,BRW7.Q.APPSE:ASEID)            ! Field APPSE:ASEID is a hot field or requires assignment from browse
  BRW7.AddField(APPSE:ASID,BRW7.Q.APPSE:ASID)              ! Field APPSE:ASID is a hot field or requires assignment from browse
  BRW11.Q &= Queue:Browse:1
  BRW11.AddSortOrder(,APPSU:FKey_ASID_ProcedureName)       ! Add the sort order for APPSU:FKey_ASID_ProcedureName for sort order 1
  BRW11.AddRange(APPSU:ASID,APP:ASID)                      ! Add single value range limit for sort order 1
  BRW11.AddLocator(BRW11::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW11::Sort0:Locator.Init(,APPSU:ProcedureName,1,BRW11)  ! Initialize the browse locator using  using key: APPSU:FKey_ASID_ProcedureName , APPSU:ProcedureName
  BRW11.AppendOrder('+APPSU:ASUID')                        ! Append an additional sort order
  BRW11.AddField(APPSU:ProcedureName,BRW11.Q.APPSU:ProcedureName) ! Field APPSU:ProcedureName is a hot field or requires assignment from browse
  BRW11.AddField(APPSU:LastCalledDate,BRW11.Q.APPSU:LastCalledDate) ! Field APPSU:LastCalledDate is a hot field or requires assignment from browse
  BRW11.AddField(APPSU:LastCalledTime,BRW11.Q.APPSU:LastCalledTime) ! Field APPSU:LastCalledTime is a hot field or requires assignment from browse
  BRW11.AddField(APPSU:ASUID,BRW11.Q.APPSU:ASUID)          ! Field APPSU:ASUID is a hot field or requires assignment from browse
  BRW11.AddField(APPSU:ASID,BRW11.Q.APPSU:ASID)            ! Field APPSU:ASID is a hot field or requires assignment from browse
  BRW12.Q &= Queue:Browse:3
  BRW12.FileLoaded = 1                                     ! This is a 'file loaded' browse
  BRW12.AddSortOrder(,UGAC:FKey_ASID)                      ! Add the sort order for UGAC:FKey_ASID for sort order 1
  BRW12.AddRange(UGAC:ASID,APP:ASID)                       ! Add single value range limit for sort order 1
  BRW12.AddLocator(BRW12::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW12::Sort0:Locator.Init(,UGAC:ASID,1,BRW12)            ! Initialize the browse locator using  using key: UGAC:FKey_ASID , UGAC:ASID
  BRW12.AppendOrder('+USEG:GroupName')                     ! Append an additional sort order
  BRW12.AddField(USEG:GroupName,BRW12.Q.USEG:GroupName)    ! Field USEG:GroupName is a hot field or requires assignment from browse
  BRW12.AddField(LOC:AccessOption_Groups,BRW12.Q.LOC:AccessOption_Groups) ! Field LOC:AccessOption_Groups is a hot field or requires assignment from browse
  BRW12.AddField(UGAC:AccessOption,BRW12.Q.UGAC:AccessOption) ! Field UGAC:AccessOption is a hot field or requires assignment from browse
  BRW12.AddField(UGAC:UGAID,BRW12.Q.UGAC:UGAID)            ! Field UGAC:UGAID is a hot field or requires assignment from browse
  BRW12.AddField(UGAC:ASID,BRW12.Q.UGAC:ASID)              ! Field UGAC:ASID is a hot field or requires assignment from browse
  BRW12.AddField(USEG:UGID,BRW12.Q.USEG:UGID)              ! Field USEG:UGID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Application_Sections',QuickWindow)  ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1                                    ! Will call: Update_Users_Accesses
  BRW7.AskProcedure = 2                                    ! Will call: Update_ApplicationSections_Extra
  BRW12.AskProcedure = 3                                   ! Will call: Update_GroupAccesses
  BRW7.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW7.ToolbarItem.HelpButton = ?Help
  BRW11.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW11.ToolbarItem.HelpButton = ?Help
  BRW12.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW12.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Application_Sections',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Update_Users_Accesses
      Update_ApplicationSections_Extra
      Update_GroupAccesses
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.SetQueueRecord PROCEDURE

  CODE
      ! Allow|View Only|No Access
      EXECUTE USAC:AccessOption + 1
         LOC:AccessOption = 'Allow'
         LOC:AccessOption = 'View Only'
         LOC:AccessOption = 'No Access'
      .
  PARENT.SetQueueRecord
  


BRW7.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW7.SetQueueRecord PROCEDURE

  CODE
      ! Allow|View Only|No Access
      EXECUTE APPSE:DefaultAction + 1
         LOC:ExtraAccessOption    = 'Allow'
         LOC:ExtraAccessOption    = 'View Only'
         LOC:ExtraAccessOption    = 'No Access'
      .
  
  PARENT.SetQueueRecord
  


BRW12.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW12.SetQueueRecord PROCEDURE

  CODE
      ! Option - Allow, View Only, No Access
      EXECUTE UGAC:AccessOption + 1
         LOC:AccessOption_Groups = 'Allow'
         LOC:AccessOption_Groups = 'View Only'
         LOC:AccessOption_Groups = 'No Access'
      .
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

