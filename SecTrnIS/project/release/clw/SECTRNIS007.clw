

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS007.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_GroupAccesses PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:GroupName        STRING(35)                            ! Name of this group of users
LOC:ApplicationSection STRING(35)                          ! Application Section Name
LOC:ExtraSec_AccessOption STRING(20)                       ! Option - Allow, View Only, No Access
LOC:ExtraSec_DefaultAction STRING(20)                      ! Default action for security is - Allow, View Only, No Access
FDB8::View:FileDrop  VIEW(ApplicationSections)
                       PROJECT(APP:ApplicationSection)
                       PROJECT(APP:ASID)
                     END
FDB7::View:FileDrop  VIEW(UserGroups)
                       PROJECT(USEG:GroupName)
                       PROJECT(USEG:UGID)
                     END
BRW9::View:Browse    VIEW(UserGroupAccessesExtra)
                       PROJECT(UGACE:AccessOption)
                       PROJECT(UGACE:UGAEID)
                       PROJECT(UGACE:UGID)
                       PROJECT(UGACE:ASEID)
                       JOIN(APPSE:PKey_ASEID,UGACE:ASEID)
                         PROJECT(APPSE:ExtraSection)
                         PROJECT(APPSE:DefaultAction)
                         PROJECT(APPSE:ASEID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
APPSE:ExtraSection     LIKE(APPSE:ExtraSection)       !List box control field - type derived from field
LOC:ExtraSec_DefaultAction LIKE(LOC:ExtraSec_DefaultAction) !List box control field - type derived from local data
LOC:ExtraSec_AccessOption LIKE(LOC:ExtraSec_AccessOption) !List box control field - type derived from local data
UGACE:AccessOption     LIKE(UGACE:AccessOption)       !Browse hot field - type derived from field
APPSE:DefaultAction    LIKE(APPSE:DefaultAction)      !Browse hot field - type derived from field
UGACE:UGAEID           LIKE(UGACE:UGAEID)             !Primary key field - type derived from field
UGACE:UGID             LIKE(UGACE:UGID)               !Browse key field - type derived from field
APPSE:ASEID            LIKE(APPSE:ASEID)              !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !
APP:ApplicationSection LIKE(APP:ApplicationSection)   !List box control field - type derived from field
APP:ASID               LIKE(APP:ASID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop       QUEUE                            !
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::UGAC:Record LIKE(UGAC:RECORD),THREAD
QuickWindow          WINDOW('Form User Group Accesses'),AT(,,241,217),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_GroupAccesses'),SYSTEM
                       SHEET,AT(4,4,235,194),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Group Name:'),AT(9,20),USE(?LOC:GroupName:Prompt:2),TRN
                           LIST,AT(94,22,100,10),USE(LOC:GroupName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Group Name~@s35@'), |
  FROM(Queue:FileDrop)
                           PROMPT('Application Section:'),AT(9,36),USE(?LOC:ApplicationSection:Prompt:2),TRN
                           LIST,AT(94,38,100,10),USE(LOC:ApplicationSection),VSCROLL,DROP(15),FORMAT('140L(2)|M~Ap' & |
  'plication Section~@s35@'),FROM(Queue:FileDrop:1)
                           PROMPT('Access Option:'),AT(9,54),USE(?UGAC:AccessOption:Prompt),TRN
                           LIST,AT(94,54,100,10),USE(UGAC:AccessOption),DROP(5),FROM('Allow|#0|View Only|#1|No Access|#2'), |
  MSG('Option'),TIP('Option')
                           LINE,AT(9,70,225,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           LIST,AT(9,76,227,102),USE(?List),VSCROLL,FORMAT('106L(2)|M~Extra Section~@s100@52L(2)|M' & |
  '~Default Action~@s20@52L(2)|M~Access Option~@s20@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(82,182,49,12),USE(?Insert),LEFT,ICON('WAINSERT.ICO'),FLAT
                           BUTTON('&Change'),AT(134,182,49,12),USE(?Change),LEFT,ICON('WAchange.ICO'),FLAT
                           BUTTON('&Delete'),AT(186,182,49,12),USE(?Delete),LEFT,ICON('WAdelete.ICO'),FLAT
                         END
                       END
                       BUTTON('&OK'),AT(134,200,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(188,200,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,200,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB8                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

BRW9                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a User Group Accesses Record'
  OF InsertRecord
    ActionMessage = 'User Group Accesses Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'User Group Accesses Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_GroupAccesses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:GroupName:Prompt:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:ExtraSec_DefaultAction',LOC:ExtraSec_DefaultAction) ! Added by: BrowseBox(ABC)
  BIND('LOC:ExtraSec_AccessOption',LOC:ExtraSec_AccessOption) ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:UserGroupAccesses)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(UGAC:Record,History::UGAC:Record)
  SELF.AddHistoryField(?UGAC:AccessOption,4)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ApplicationSections.SetOpenRelated()
  Relate:ApplicationSections.Open                          ! File ApplicationSections used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UserGroupAccesses
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:UserGroupAccessesExtra,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?LOC:GroupName)
    DISABLE(?LOC:ApplicationSection)
    DISABLE(?UGAC:AccessOption)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW9.Q &= Queue:Browse
  BRW9.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW9.AddSortOrder(,UGACE:FKey_UGID)                      ! Add the sort order for UGACE:FKey_UGID for sort order 1
  BRW9.AddRange(UGACE:UGID,UGAC:UGID)                      ! Add single value range limit for sort order 1
  BRW9.AddLocator(BRW9::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW9::Sort0:Locator.Init(,UGACE:UGID,1,BRW9)             ! Initialize the browse locator using  using key: UGACE:FKey_UGID , UGACE:UGID
  BRW9.AppendOrder('+APPSE:ExtraSection,+UGACE:UGAEID')    ! Append an additional sort order
  BRW9.SetFilter('(UGACE:ASID = UGAC:ASID)')               ! Apply filter expression to browse
  BRW9.AddResetField(UGAC:ASID)                            ! Apply the reset field
  BRW9.AddResetField(UGAC:UGID)                            ! Apply the reset field
  BRW9.AddField(APPSE:ExtraSection,BRW9.Q.APPSE:ExtraSection) ! Field APPSE:ExtraSection is a hot field or requires assignment from browse
  BRW9.AddField(LOC:ExtraSec_DefaultAction,BRW9.Q.LOC:ExtraSec_DefaultAction) ! Field LOC:ExtraSec_DefaultAction is a hot field or requires assignment from browse
  BRW9.AddField(LOC:ExtraSec_AccessOption,BRW9.Q.LOC:ExtraSec_AccessOption) ! Field LOC:ExtraSec_AccessOption is a hot field or requires assignment from browse
  BRW9.AddField(UGACE:AccessOption,BRW9.Q.UGACE:AccessOption) ! Field UGACE:AccessOption is a hot field or requires assignment from browse
  BRW9.AddField(APPSE:DefaultAction,BRW9.Q.APPSE:DefaultAction) ! Field APPSE:DefaultAction is a hot field or requires assignment from browse
  BRW9.AddField(UGACE:UGAEID,BRW9.Q.UGACE:UGAEID)          ! Field UGACE:UGAEID is a hot field or requires assignment from browse
  BRW9.AddField(UGACE:UGID,BRW9.Q.UGACE:UGID)              ! Field UGACE:UGID is a hot field or requires assignment from browse
  BRW9.AddField(APPSE:ASEID,BRW9.Q.APPSE:ASEID)            ! Field APPSE:ASEID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_GroupAccesses',QuickWindow)         ! Restore window settings from non-volatile store
      APP:ASID    = UGAC:ASID
      IF Access:ApplicationSections.TryFetch(APP:PKey_ASID) = LEVEL:Benign
         LOC:ApplicationSection   = APP:ApplicationSection
      .
  
  
      USEG:UGID   = UGAC:UGID
      IF Access:UserGroups.TryFetch(USEG:PKey_UGID) = LEVEL:Benign
         LOC:GroupName            = USEG:GroupName
      .
      
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  FDB8.Init(?LOC:ApplicationSection,Queue:FileDrop:1.ViewPosition,FDB8::View:FileDrop,Queue:FileDrop:1,Relate:ApplicationSections,ThisWindow)
  FDB8.Q &= Queue:FileDrop:1
  FDB8.AddSortOrder(APP:Key_ApplicationSection)
  FDB8.AddField(APP:ApplicationSection,FDB8.Q.APP:ApplicationSection) !List box control field - type derived from field
  FDB8.AddField(APP:ASID,FDB8.Q.APP:ASID) !Primary key field - type derived from field
  FDB8.AddUpdateField(APP:ASID,UGAC:ASID)
  ThisWindow.AddItem(FDB8.WindowComponent)
  FDB8.DefaultFill = 0
  FDB7.Init(?LOC:GroupName,Queue:FileDrop.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop,Relate:UserGroups,ThisWindow)
  FDB7.Q &= Queue:FileDrop
  FDB7.AddSortOrder(USEG:Key_GroupName)
  FDB7.AddField(USEG:GroupName,FDB7.Q.USEG:GroupName) !List box control field - type derived from field
  FDB7.AddField(USEG:UGID,FDB7.Q.USEG:UGID) !Primary key field - type derived from field
  FDB7.AddUpdateField(USEG:UGID,UGAC:UGID)
  ThisWindow.AddItem(FDB7.WindowComponent)
  FDB7.DefaultFill = 0
  BRW9.AskProcedure = 1                                    ! Will call: Update_UserGroupAccesses_ApplicationSectionExtra
  BRW9.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW9.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_GroupAccesses',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_UserGroupAccesses_ApplicationSectionExtra
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW9.SetQueueRecord PROCEDURE

  CODE
      EXECUTE UGACE:AccessOption + 1
         LOC:ExtraSec_AccessOption    = 'Allow'
         LOC:ExtraSec_AccessOption    = 'View Only'
         LOC:ExtraSec_AccessOption    = 'No Access'
      .
  
      EXECUTE APPSE:DefaultAction + 1
         LOC:ExtraSec_DefaultAction   = 'Allow'
         LOC:ExtraSec_DefaultAction   = 'View Only'
         LOC:ExtraSec_DefaultAction   = 'No Access'
      .
  PARENT.SetQueueRecord
  

