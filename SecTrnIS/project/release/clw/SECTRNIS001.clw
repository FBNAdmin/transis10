

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Procedure not yet defined
!!! </summary>
Main_sec PROCEDURE !Procedure not yet defined
  CODE
  GlobalErrors.ThrowMessage(Msg:ProcedureToDo,'Main_sec')  ! This procedure acts as a place holder for a procedure yet to be defined
  SETKEYCODE(0)
  GlobalResponse = RequestCancelled                        ! Request cancelled is the implied action
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_UsersAccesses_Extra PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Locals           GROUP,PRE(LO)                         ! 
ApplicationSection   STRING(35)                            ! Application Section Name
ExtraSection         STRING(100)                           ! 
DefaultAction        STRING(20)                            ! Default action for security is - Allow, View Only, No Access
Login                STRING(20)                            ! User Login
                     END                                   ! 
FDB9::View:FileDrop  VIEW(ApplicationSections_Extras)
                       PROJECT(APPSE:ExtraSection)
                       PROJECT(APPSE:ASEID)
                     END
Queue:FileDrop       QUEUE                            !
APPSE:ExtraSection     LIKE(APPSE:ExtraSection)       !List box control field - type derived from field
LO:DefaultAction       STRING(1)                      !List box control field - unable to determine correct data type
APP:DefaultAction      LIKE(APP:DefaultAction)        !Browse hot field - type derived from field
APPSE:ASEID            LIKE(APPSE:ASEID)              !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::USACE:Record LIKE(USACE:RECORD),THREAD
QuickWindow          WINDOW('Form Users Accesses Extra'),AT(,,200,139),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateUsersAccessesExtra'),SYSTEM
                       SHEET,AT(4,4,192,116),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('User Login:'),AT(9,20),USE(?Login:Prompt),TRN
                           BUTTON('...'),AT(74,20,12,10),USE(?CallLookup:2)
                           ENTRY(@s20),AT(90,20,100,10),USE(LO:Login),MSG('User Login'),REQ,TIP('User Login')
                           PROMPT('Application Section:'),AT(9,34),USE(?ApplicationSection:Prompt),TRN
                           BUTTON('...'),AT(74,34,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(90,34,100,10),USE(LO:ApplicationSection),MSG('Application Section name'),TIP('Applicatio' & |
  'n Section name')
                           PROMPT('Extra Section:'),AT(9,48),USE(?Prompt3),TRN
                           LIST,AT(90,48,100,10),USE(LO:ExtraSection),VSCROLL,DROP(15),FORMAT('100L(2)|M~Extra Sec' & |
  'tion~@s100@80L(2)|M~Default Action~@s20@'),FROM(Queue:FileDrop)
                           PROMPT('User Access Option:'),AT(9,78),USE(?USACE:AccessOption:Prompt),TRN
                           LIST,AT(90,78,100,10),USE(USACE:AccessOption),DROP(5),FROM('Allow|#0|View Only|#1|No Access|#2'), |
  MSG('Option'),TIP('Option')
                         END
                       END
                       BUTTON('&OK'),AT(94,122,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(147,122,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,122,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB9                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
SetQueueRecord         PROCEDURE(),DERIVED
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a User Access Extra Record'
  OF InsertRecord
    ActionMessage = 'User Access Extra Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'User Access Extra Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_UsersAccesses_Extra')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Login:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:UsersAccessesExtra)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(USACE:Record,History::USACE:Record)
  SELF.AddHistoryField(?USACE:AccessOption,6)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ApplicationSections.SetOpenRelated()
  Relate:ApplicationSections.Open                          ! File ApplicationSections used by this procedure, so make sure it's RelationManager is open
  Access:UsersAccesses.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UsersAccessesExtra
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup:2)
    ?LO:Login{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?LO:ApplicationSection{PROP:ReadOnly} = True
    ?LO:ExtraSection{PROP:ReadOnly} = True
    DISABLE(?USACE:AccessOption)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_UsersAccesses_Extra',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  FDB9.Init(?LO:ExtraSection,Queue:FileDrop.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop,Relate:ApplicationSections_Extras,ThisWindow)
  FDB9.Q &= Queue:FileDrop
  FDB9.AddSortOrder(APPSE:SKey_ASID_ExtraSection)
  FDB9.AddRange(APPSE:ASID,USACE:ASID)
  FDB9.AddField(APPSE:ExtraSection,FDB9.Q.APPSE:ExtraSection) !List box control field - type derived from field
  FDB9.AddField(LO:DefaultAction,FDB9.Q.LO:DefaultAction) !List box control field - unable to determine correct data type
  FDB9.AddField(APP:DefaultAction,FDB9.Q.APP:DefaultAction) !Browse hot field - type derived from field
  FDB9.AddField(APPSE:ASEID,FDB9.Q.APPSE:ASEID) !Primary key field - type derived from field
  FDB9.AddUpdateField(APPSE:ASEID,USACE:ASEID)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
      IF SELF.Request = ChangeRecord
         USE:UID      = USACE:UID
         IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
            LO:Login  = USE:Login
         .
  
         APP:ASID     = USACE:ASID
         IF Access:ApplicationSections.TryFetch(APP:PKey_ASID) = LEVEL:Benign
            LO:ApplicationSection = APP:ApplicationSection
         .
  
         APPSE:ASEID  = USACE:ASEID
         IF Access:ApplicationSections_Extras.TryFetch(APPSE:PKey_ASEID) = LEVEL:Benign
            LO:ExtraSection       = APPSE:ExtraSection
      .  .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_UsersAccesses_Extra',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_Users
      Browse_Application_Sections
      Browse_Application_Section_Extras
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup:2
      ThisWindow.Update()
      USE:Login = LO:Login
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LO:Login = USE:Login
        USACE:UID = USE:UID
      END
      ThisWindow.Reset(1)
    OF ?LO:Login
      IF LO:Login OR ?LO:Login{PROP:Req}
        USE:Login = LO:Login
        IF Access:Users.TryFetch(USE:Key_Login)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LO:Login = USE:Login
            USACE:UID = USE:UID
          ELSE
            CLEAR(USACE:UID)
            SELECT(?LO:Login)
            CYCLE
          END
        ELSE
          USACE:UID = USE:UID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      APP:ApplicationSection = LO:ApplicationSection
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LO:ApplicationSection = APP:ApplicationSection
        USACE:ASID = APP:ASID
      END
      ThisWindow.Reset(1)
    OF ?LO:ApplicationSection
      IF LO:ApplicationSection OR ?LO:ApplicationSection{PROP:Req}
        APP:ApplicationSection = LO:ApplicationSection
        IF Access:ApplicationSections.TryFetch(APP:Key_ApplicationSection)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            LO:ApplicationSection = APP:ApplicationSection
            USACE:ASID = APP:ASID
          ELSE
            CLEAR(USACE:ASID)
            SELECT(?LO:ApplicationSection)
            CYCLE
          END
        ELSE
          USACE:ASID = APP:ASID
        END
      END
      ThisWindow.Reset(1)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


FDB9.SetQueueRecord PROCEDURE

  CODE
      ! Allow|View Only|No Access
      EXECUTE APPSE:DefaultAction + 1
         LO:DefaultAction = 'Allow'
         LO:DefaultAction = 'View Only'
         LO:DefaultAction = 'No Access'
      .
  PARENT.SetQueueRecord
  

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_ApplicationSections_Extra PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:AccessOption     STRING(20)                            ! Option - Allow, View Only, No Access
BRW2::View:Browse    VIEW(UsersAccessesExtra)
                       PROJECT(USACE:AccessOption)
                       PROJECT(USACE:UAEID)
                       PROJECT(USACE:ASEID)
                       PROJECT(USACE:UID)
                       JOIN(USE:PKey_UID,USACE:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:UID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
LOC:AccessOption       LIKE(LOC:AccessOption)         !List box control field - type derived from local data
USACE:AccessOption     LIKE(USACE:AccessOption)       !Browse hot field - type derived from field
USACE:UAEID            LIKE(USACE:UAEID)              !Primary key field - type derived from field
USACE:ASEID            LIKE(USACE:ASEID)              !Browse key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::APPSE:Record LIKE(APPSE:RECORD),THREAD
QuickWindow          WINDOW('Form Application Sections Extras'),AT(,,279,138),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Update_ApplicationSections_Extra'),SYSTEM
                       SHEET,AT(4,4,271,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Extra Section:'),AT(9,26),USE(?APPSE:ExtraSection:Prompt),TRN
                           ENTRY(@s100),AT(73,26,197,10),USE(APPSE:ExtraSection)
                           PROMPT('Default Action:'),AT(9,40),USE(?APPSE:DefaultAction:Prompt),TRN
                           LIST,AT(73,40,100,10),USE(APPSE:DefaultAction),DROP(5),FROM('Allow|#0|View Only|#1|No Access|#2'), |
  MSG('Default action for security is'),TIP('Default action for security is')
                         END
                         TAB('&2) Users Accesses Extra'),USE(?Tab:2)
                           LIST,AT(9,20,263,74),USE(?Browse:2),HVSCROLL,FORMAT('80L(2)|M~Login~C(0)@s20@80L(2)|M~A' & |
  'ccess Option~C(0)@s20@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the UsersAccessesExtra file')
                           BUTTON('&Insert'),AT(117,98,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(169,98,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(223,98,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(174,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(226,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Application Sections Extra Record'
  OF InsertRecord
    ActionMessage = 'App. Sections Extra Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'App. Sections Extra Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ApplicationSections_Extra')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?APPSE:ExtraSection:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:AccessOption',LOC:AccessOption)                ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:ApplicationSections_Extras)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(APPSE:Record,History::APPSE:Record)
  SELF.AddHistoryField(?APPSE:ExtraSection,3)
  SELF.AddHistoryField(?APPSE:DefaultAction,4)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ApplicationSections_Extras.SetOpenRelated()
  Relate:ApplicationSections_Extras.Open                   ! File ApplicationSections_Extras used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ApplicationSections_Extras
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:UsersAccessesExtra,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?APPSE:ExtraSection{PROP:ReadOnly} = True
    DISABLE(?APPSE:DefaultAction)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,USACE:FKey_ASEID)                     ! Add the sort order for USACE:FKey_ASEID for sort order 1
  BRW2.AddRange(USACE:ASEID,Relate:UsersAccessesExtra,Relate:ApplicationSections_Extras) ! Add file relationship range limit for sort order 1
  BRW2.AddField(USE:Login,BRW2.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW2.AddField(LOC:AccessOption,BRW2.Q.LOC:AccessOption)  ! Field LOC:AccessOption is a hot field or requires assignment from browse
  BRW2.AddField(USACE:AccessOption,BRW2.Q.USACE:AccessOption) ! Field USACE:AccessOption is a hot field or requires assignment from browse
  BRW2.AddField(USACE:UAEID,BRW2.Q.USACE:UAEID)            ! Field USACE:UAEID is a hot field or requires assignment from browse
  BRW2.AddField(USACE:ASEID,BRW2.Q.USACE:ASEID)            ! Field USACE:ASEID is a hot field or requires assignment from browse
  BRW2.AddField(USE:UID,BRW2.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_ApplicationSections_Extra',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1                                    ! Will call: Update_UsersAccesses_Extra
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections_Extras.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_ApplicationSections_Extra',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_UsersAccesses_Extra
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.SetQueueRecord PROCEDURE

  CODE
      ! Allow|View Only|No Access
      EXECUTE USACE:AccessOption + 1
         LOC:AccessOption = 'Allow'
         LOC:AccessOption = 'View Only'
         LOC:AccessOption = 'No Access'
      .
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Application_Section_Extras PROCEDURE 

CurrentTab           STRING(80)                            ! 
LOC:Locals           GROUP,PRE(LO)                         ! 
DefaultAction        STRING(20)                            ! Default action for security is - Allow, View Only, No Access
                     END                                   ! 
BRW1::View:Browse    VIEW(ApplicationSections_Extras)
                       PROJECT(APPSE:ExtraSection)
                       PROJECT(APPSE:ASEID)
                       PROJECT(APPSE:ASID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
APPSE:ExtraSection     LIKE(APPSE:ExtraSection)       !List box control field - type derived from field
LO:DefaultAction       STRING(1)                      !List box control field - unable to determine correct data type
APPSE:ASEID            LIKE(APPSE:ASEID)              !Primary key field - type derived from field
APPSE:ASID             LIKE(APPSE:ASID)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Application Sections Extras File'),AT(,,330,198),FONT('Tahoma',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_Application_Section_Extras'),SYSTEM
                       LIST,AT(8,30,314,124),USE(?Browse:1),HVSCROLL,FORMAT('150L(2)|M~Extra Section~@s100@80L' & |
  '(2)|M~Default Action~@s20@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the ApplicationS' & |
  'ections_Extras file')
                       BUTTON('&Select'),AT(61,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(114,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(167,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(220,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(273,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,322,172),USE(?CurrentTab),FONT('Tahoma')
                         TAB('&1) By Application Section'),USE(?Tab:2)
                           BUTTON('Applicati...'),AT(9,158,16,14),USE(?SelectApplicationSections),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&2) By Extra Section'),USE(?Tab:3)
                         END
                       END
                       BUTTON('&Close'),AT(277,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Application_Section_Extras')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:DefaultAction',LO:DefaultAction)                ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:ApplicationSections.SetOpenRelated()
  Relate:ApplicationSections.Open                          ! File ApplicationSections used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ApplicationSections_Extras,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,APPSE:SKey_ASID_ExtraSection)         ! Add the sort order for APPSE:SKey_ASID_ExtraSection for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,APPSE:ASID,1,BRW1)             ! Initialize the browse locator using  using key: APPSE:SKey_ASID_ExtraSection , APPSE:ASID
  BRW1.AddSortOrder(,APPSE:FKey_ASID)                      ! Add the sort order for APPSE:FKey_ASID for sort order 2
  BRW1.AddRange(APPSE:ASID,Relate:ApplicationSections_Extras,Relate:ApplicationSections) ! Add file relationship range limit for sort order 2
  BRW1.AddField(APPSE:ExtraSection,BRW1.Q.APPSE:ExtraSection) ! Field APPSE:ExtraSection is a hot field or requires assignment from browse
  BRW1.AddField(LO:DefaultAction,BRW1.Q.LO:DefaultAction)  ! Field LO:DefaultAction is a hot field or requires assignment from browse
  BRW1.AddField(APPSE:ASEID,BRW1.Q.APPSE:ASEID)            ! Field APPSE:ASEID is a hot field or requires assignment from browse
  BRW1.AddField(APPSE:ASID,BRW1.Q.APPSE:ASID)              ! Field APPSE:ASID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Application_Section_Extras',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                                    ! Will call: Update_ApplicationSections_Extra
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Application_Section_Extras',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_ApplicationSections_Extra
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectApplicationSections
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_Application_Sections()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      ! Allow|View Only|No Access
      EXECUTE APPSE:DefaultAction + 1
         LO:DefaultAction     = 'Allow'
         LO:DefaultAction     = 'View Only'
         LO:DefaultAction     = 'No Access'
      .
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_UserGroupAccesses_ApplicationSectionExtra PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:GroupName        STRING(35)                            ! Name of this group of users
LOC:ApplicationSection STRING(35)                          ! Application Section Name
LOC:ExtraSection     STRING(100)                           ! 
FDB7::View:FileDrop  VIEW(UserGroups)
                       PROJECT(USEG:GroupName)
                       PROJECT(USEG:UGID)
                     END
FDB8::View:FileDrop  VIEW(ApplicationSections)
                       PROJECT(APP:ApplicationSection)
                       PROJECT(APP:ASID)
                     END
FDB9::View:FileDrop  VIEW(ApplicationSections_Extras)
                       PROJECT(APPSE:ExtraSection)
                       PROJECT(APPSE:ASEID)
                     END
Queue:FileDrop       QUEUE                            !
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !
APP:ApplicationSection LIKE(APP:ApplicationSection)   !List box control field - type derived from field
APP:ASID               LIKE(APP:ASID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:2     QUEUE                            !
APPSE:ExtraSection     LIKE(APPSE:ExtraSection)       !List box control field - type derived from field
APPSE:ASEID            LIKE(APPSE:ASEID)              !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::UGACE:Record LIKE(UGACE:RECORD),THREAD
QuickWindow          WINDOW('Form User Group Accesses Extra'),AT(,,208,147),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Update_UserGroupAccesses_ApplicationSectionExtra'),SYSTEM
                       SHEET,AT(4,4,201,124),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('User Group:'),AT(9,24),USE(?Prompt2),TRN
                           LIST,AT(98,42,100,10),USE(LOC:ApplicationSection),VSCROLL,DROP(15),FORMAT('140L(2)|M~Ap' & |
  'plication Section~@s35@'),FROM(Queue:FileDrop:1)
                           PROMPT('Application Section:'),AT(9,42),USE(?Prompt2:2),TRN
                           LIST,AT(98,60,100,10),USE(LOC:ExtraSection),VSCROLL,DROP(15),FORMAT('400L(2)|M~Extra Se' & |
  'ction~@s100@'),FROM(Queue:FileDrop:2)
                           PROMPT('Application Section Extras:'),AT(9,60),USE(?Prompt2:3),TRN
                           LIST,AT(98,24,100,10),USE(LOC:GroupName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Group Name~@s35@'), |
  FROM(Queue:FileDrop)
                           PROMPT('Access Option:'),AT(9,114),USE(?UGACE:AccessOption:Prompt),TRN
                           LIST,AT(101,114,100,10),USE(UGACE:AccessOption),DROP(5),FROM('Allow|#0|View Only|#1|No Access|#2'), |
  MSG('Option'),TIP('Option')
                         END
                       END
                       BUTTON('&OK'),AT(102,130,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(156,130,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,130,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB8                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

FDB9                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:2              !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a User Group Accesses Record'
  OF InsertRecord
    ActionMessage = 'User Group Accesses Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'User Group Accesses Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_UserGroupAccesses_ApplicationSectionExtra')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:UserGroupAccessesExtra)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(UGACE:Record,History::UGACE:Record)
  SELF.AddHistoryField(?UGACE:AccessOption,6)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ApplicationSections.SetOpenRelated()
  Relate:ApplicationSections.Open                          ! File ApplicationSections used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UserGroupAccessesExtra
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?LOC:ApplicationSection)
    DISABLE(?LOC:ExtraSection)
    DISABLE(?LOC:GroupName)
    DISABLE(?UGACE:AccessOption)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_UserGroupAccesses_ApplicationSectionExtra',QuickWindow) ! Restore window settings from non-volatile store
      USEG:UGID   = UGACE:UGID
      IF Access:UserGroups.TryFetch(USEG:PKey_UGID) = LEVEL:Benign
         LOC:GroupName            = USEG:GroupName
      .
  
  
      APP:ASID    = UGACE:ASID
      IF Access:ApplicationSections.TryFetch(APP:PKey_ASID) = LEVEL:Benign
         LOC:ApplicationSection   = APP:ApplicationSection
      .
  
      APPSE:ASEID = UGACE:ASEID
      IF Access:ApplicationSections_Extras.TryFetch(APPSE:PKey_ASEID) = LEVEL:Benign
         LOC:ExtraSection         = APPSE:ExtraSection
      .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  FDB7.Init(?LOC:GroupName,Queue:FileDrop.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop,Relate:UserGroups,ThisWindow)
  FDB7.Q &= Queue:FileDrop
  FDB7.AddSortOrder(USEG:Key_GroupName)
  FDB7.AddField(USEG:GroupName,FDB7.Q.USEG:GroupName) !List box control field - type derived from field
  FDB7.AddField(USEG:UGID,FDB7.Q.USEG:UGID) !Primary key field - type derived from field
  FDB7.AddUpdateField(USEG:UGID,UGACE:UGID)
  ThisWindow.AddItem(FDB7.WindowComponent)
  FDB7.DefaultFill = 0
  FDB8.Init(?LOC:ApplicationSection,Queue:FileDrop:1.ViewPosition,FDB8::View:FileDrop,Queue:FileDrop:1,Relate:ApplicationSections,ThisWindow)
  FDB8.Q &= Queue:FileDrop:1
  FDB8.AddSortOrder(APP:Key_ApplicationSection)
  FDB8.AddField(APP:ApplicationSection,FDB8.Q.APP:ApplicationSection) !List box control field - type derived from field
  FDB8.AddField(APP:ASID,FDB8.Q.APP:ASID) !Primary key field - type derived from field
  FDB8.AddUpdateField(APP:ASID,UGACE:ASID)
  ThisWindow.AddItem(FDB8.WindowComponent)
  FDB8.DefaultFill = 0
  FDB9.Init(?LOC:ExtraSection,Queue:FileDrop:2.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop:2,Relate:ApplicationSections_Extras,ThisWindow)
  FDB9.Q &= Queue:FileDrop:2
  FDB9.AddSortOrder(APPSE:SKey_ASID_ExtraSection)
  FDB9.AddRange(APPSE:ASID,UGACE:ASID)
  FDB9.AddField(APPSE:ExtraSection,FDB9.Q.APPSE:ExtraSection) !List box control field - type derived from field
  FDB9.AddField(APPSE:ASEID,FDB9.Q.APPSE:ASEID) !Primary key field - type derived from field
  FDB9.AddUpdateField(APPSE:ASEID,UGACE:ASEID)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_UserGroupAccesses_ApplicationSectionExtra',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_GroupAccesses PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:GroupName        STRING(35)                            ! Name of this group of users
LOC:ApplicationSection STRING(35)                          ! Application Section Name
LOC:ExtraSec_AccessOption STRING(20)                       ! Option - Allow, View Only, No Access
LOC:ExtraSec_DefaultAction STRING(20)                      ! Default action for security is - Allow, View Only, No Access
FDB8::View:FileDrop  VIEW(ApplicationSections)
                       PROJECT(APP:ApplicationSection)
                       PROJECT(APP:ASID)
                     END
FDB7::View:FileDrop  VIEW(UserGroups)
                       PROJECT(USEG:GroupName)
                       PROJECT(USEG:UGID)
                     END
BRW9::View:Browse    VIEW(UserGroupAccessesExtra)
                       PROJECT(UGACE:AccessOption)
                       PROJECT(UGACE:UGAEID)
                       PROJECT(UGACE:UGID)
                       PROJECT(UGACE:ASEID)
                       JOIN(APPSE:PKey_ASEID,UGACE:ASEID)
                         PROJECT(APPSE:ExtraSection)
                         PROJECT(APPSE:DefaultAction)
                         PROJECT(APPSE:ASEID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
APPSE:ExtraSection     LIKE(APPSE:ExtraSection)       !List box control field - type derived from field
LOC:ExtraSec_DefaultAction LIKE(LOC:ExtraSec_DefaultAction) !List box control field - type derived from local data
LOC:ExtraSec_AccessOption LIKE(LOC:ExtraSec_AccessOption) !List box control field - type derived from local data
UGACE:AccessOption     LIKE(UGACE:AccessOption)       !Browse hot field - type derived from field
APPSE:DefaultAction    LIKE(APPSE:DefaultAction)      !Browse hot field - type derived from field
UGACE:UGAEID           LIKE(UGACE:UGAEID)             !Primary key field - type derived from field
UGACE:UGID             LIKE(UGACE:UGID)               !Browse key field - type derived from field
APPSE:ASEID            LIKE(APPSE:ASEID)              !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !
APP:ApplicationSection LIKE(APP:ApplicationSection)   !List box control field - type derived from field
APP:ASID               LIKE(APP:ASID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop       QUEUE                            !
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::UGAC:Record LIKE(UGAC:RECORD),THREAD
QuickWindow          WINDOW('Form User Group Accesses'),AT(,,241,217),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_GroupAccesses'),SYSTEM
                       SHEET,AT(4,4,235,194),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Group Name:'),AT(9,20),USE(?LOC:GroupName:Prompt:2),TRN
                           LIST,AT(94,22,100,10),USE(LOC:GroupName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Group Name~@s35@'), |
  FROM(Queue:FileDrop)
                           PROMPT('Application Section:'),AT(9,36),USE(?LOC:ApplicationSection:Prompt:2),TRN
                           LIST,AT(94,38,100,10),USE(LOC:ApplicationSection),VSCROLL,DROP(15),FORMAT('140L(2)|M~Ap' & |
  'plication Section~@s35@'),FROM(Queue:FileDrop:1)
                           PROMPT('Access Option:'),AT(9,54),USE(?UGAC:AccessOption:Prompt),TRN
                           LIST,AT(94,54,100,10),USE(UGAC:AccessOption),DROP(5),FROM('Allow|#0|View Only|#1|No Access|#2'), |
  MSG('Option'),TIP('Option')
                           LINE,AT(9,70,225,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           LIST,AT(9,76,227,102),USE(?List),VSCROLL,FORMAT('106L(2)|M~Extra Section~@s100@52L(2)|M' & |
  '~Default Action~@s20@52L(2)|M~Access Option~@s20@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(82,182,49,12),USE(?Insert),LEFT,ICON('WAINSERT.ICO'),FLAT
                           BUTTON('&Change'),AT(134,182,49,12),USE(?Change),LEFT,ICON('WAchange.ICO'),FLAT
                           BUTTON('&Delete'),AT(186,182,49,12),USE(?Delete),LEFT,ICON('WAdelete.ICO'),FLAT
                         END
                       END
                       BUTTON('&OK'),AT(134,200,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(188,200,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,200,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB8                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

BRW9                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a User Group Accesses Record'
  OF InsertRecord
    ActionMessage = 'User Group Accesses Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'User Group Accesses Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_GroupAccesses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:GroupName:Prompt:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:ExtraSec_DefaultAction',LOC:ExtraSec_DefaultAction) ! Added by: BrowseBox(ABC)
  BIND('LOC:ExtraSec_AccessOption',LOC:ExtraSec_AccessOption) ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:UserGroupAccesses)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(UGAC:Record,History::UGAC:Record)
  SELF.AddHistoryField(?UGAC:AccessOption,4)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ApplicationSections.SetOpenRelated()
  Relate:ApplicationSections.Open                          ! File ApplicationSections used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UserGroupAccesses
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:UserGroupAccessesExtra,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?LOC:GroupName)
    DISABLE(?LOC:ApplicationSection)
    DISABLE(?UGAC:AccessOption)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW9.Q &= Queue:Browse
  BRW9.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW9.AddSortOrder(,UGACE:FKey_UGID)                      ! Add the sort order for UGACE:FKey_UGID for sort order 1
  BRW9.AddRange(UGACE:UGID,UGAC:UGID)                      ! Add single value range limit for sort order 1
  BRW9.AddLocator(BRW9::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW9::Sort0:Locator.Init(,UGACE:UGID,1,BRW9)             ! Initialize the browse locator using  using key: UGACE:FKey_UGID , UGACE:UGID
  BRW9.AppendOrder('+APPSE:ExtraSection,+UGACE:UGAEID')    ! Append an additional sort order
  BRW9.SetFilter('(UGACE:ASID = UGAC:ASID)')               ! Apply filter expression to browse
  BRW9.AddResetField(UGAC:ASID)                            ! Apply the reset field
  BRW9.AddResetField(UGAC:UGID)                            ! Apply the reset field
  BRW9.AddField(APPSE:ExtraSection,BRW9.Q.APPSE:ExtraSection) ! Field APPSE:ExtraSection is a hot field or requires assignment from browse
  BRW9.AddField(LOC:ExtraSec_DefaultAction,BRW9.Q.LOC:ExtraSec_DefaultAction) ! Field LOC:ExtraSec_DefaultAction is a hot field or requires assignment from browse
  BRW9.AddField(LOC:ExtraSec_AccessOption,BRW9.Q.LOC:ExtraSec_AccessOption) ! Field LOC:ExtraSec_AccessOption is a hot field or requires assignment from browse
  BRW9.AddField(UGACE:AccessOption,BRW9.Q.UGACE:AccessOption) ! Field UGACE:AccessOption is a hot field or requires assignment from browse
  BRW9.AddField(APPSE:DefaultAction,BRW9.Q.APPSE:DefaultAction) ! Field APPSE:DefaultAction is a hot field or requires assignment from browse
  BRW9.AddField(UGACE:UGAEID,BRW9.Q.UGACE:UGAEID)          ! Field UGACE:UGAEID is a hot field or requires assignment from browse
  BRW9.AddField(UGACE:UGID,BRW9.Q.UGACE:UGID)              ! Field UGACE:UGID is a hot field or requires assignment from browse
  BRW9.AddField(APPSE:ASEID,BRW9.Q.APPSE:ASEID)            ! Field APPSE:ASEID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_GroupAccesses',QuickWindow)         ! Restore window settings from non-volatile store
      APP:ASID    = UGAC:ASID
      IF Access:ApplicationSections.TryFetch(APP:PKey_ASID) = LEVEL:Benign
         LOC:ApplicationSection   = APP:ApplicationSection
      .
  
  
      USEG:UGID   = UGAC:UGID
      IF Access:UserGroups.TryFetch(USEG:PKey_UGID) = LEVEL:Benign
         LOC:GroupName            = USEG:GroupName
      .
      
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  FDB8.Init(?LOC:ApplicationSection,Queue:FileDrop:1.ViewPosition,FDB8::View:FileDrop,Queue:FileDrop:1,Relate:ApplicationSections,ThisWindow)
  FDB8.Q &= Queue:FileDrop:1
  FDB8.AddSortOrder(APP:Key_ApplicationSection)
  FDB8.AddField(APP:ApplicationSection,FDB8.Q.APP:ApplicationSection) !List box control field - type derived from field
  FDB8.AddField(APP:ASID,FDB8.Q.APP:ASID) !Primary key field - type derived from field
  FDB8.AddUpdateField(APP:ASID,UGAC:ASID)
  ThisWindow.AddItem(FDB8.WindowComponent)
  FDB8.DefaultFill = 0
  FDB7.Init(?LOC:GroupName,Queue:FileDrop.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop,Relate:UserGroups,ThisWindow)
  FDB7.Q &= Queue:FileDrop
  FDB7.AddSortOrder(USEG:Key_GroupName)
  FDB7.AddField(USEG:GroupName,FDB7.Q.USEG:GroupName) !List box control field - type derived from field
  FDB7.AddField(USEG:UGID,FDB7.Q.USEG:UGID) !Primary key field - type derived from field
  FDB7.AddUpdateField(USEG:UGID,UGAC:UGID)
  ThisWindow.AddItem(FDB7.WindowComponent)
  FDB7.DefaultFill = 0
  BRW9.AskProcedure = 1                                    ! Will call: Update_UserGroupAccesses_ApplicationSectionExtra
  BRW9.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW9.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_GroupAccesses',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_UserGroupAccesses_ApplicationSectionExtra
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW9.SetQueueRecord PROCEDURE

  CODE
      EXECUTE UGACE:AccessOption + 1
         LOC:ExtraSec_AccessOption    = 'Allow'
         LOC:ExtraSec_AccessOption    = 'View Only'
         LOC:ExtraSec_AccessOption    = 'No Access'
      .
  
      EXECUTE APPSE:DefaultAction + 1
         LOC:ExtraSec_DefaultAction   = 'Allow'
         LOC:ExtraSec_DefaultAction   = 'View Only'
         LOC:ExtraSec_DefaultAction   = 'No Access'
      .
  PARENT.SetQueueRecord
  

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Application_Sections PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:AccessOption     STRING(10)                            ! Option - Allow, View Only, No Access
LOC:ExtraAccessOption STRING(20)                           ! Option - Allow, View Only, No Access
LOC:AccessOption_Groups STRING(20)                         ! Option - Allow, View Only, No Access
BRW2::View:Browse    VIEW(UsersAccesses)
                       PROJECT(USAC:UAID)
                       PROJECT(USAC:ASID)
                       PROJECT(USAC:UID)
                       JOIN(USE:PKey_UID,USAC:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:AccessLevel)
                         PROJECT(USE:UID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
USE:AccessLevel        LIKE(USE:AccessLevel)          !List box control field - type derived from field
LOC:AccessOption       LIKE(LOC:AccessOption)         !List box control field - type derived from local data
USAC:UAID              LIKE(USAC:UAID)                !Primary key field - type derived from field
USAC:ASID              LIKE(USAC:ASID)                !Browse key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(ApplicationSections_Extras)
                       PROJECT(APPSE:ExtraSection)
                       PROJECT(APPSE:DefaultAction)
                       PROJECT(APPSE:ASEID)
                       PROJECT(APPSE:ASID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
APPSE:ExtraSection     LIKE(APPSE:ExtraSection)       !List box control field - type derived from field
LOC:ExtraAccessOption  LIKE(LOC:ExtraAccessOption)    !List box control field - type derived from local data
APPSE:DefaultAction    LIKE(APPSE:DefaultAction)      !Browse hot field - type derived from field
APPSE:ASEID            LIKE(APPSE:ASEID)              !Primary key field - type derived from field
APPSE:ASID             LIKE(APPSE:ASID)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(Application_Section_Usage)
                       PROJECT(APPSU:ProcedureName)
                       PROJECT(APPSU:LastCalledDate)
                       PROJECT(APPSU:LastCalledTime)
                       PROJECT(APPSU:ASUID)
                       PROJECT(APPSU:ASID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
APPSU:ProcedureName    LIKE(APPSU:ProcedureName)      !List box control field - type derived from field
APPSU:LastCalledDate   LIKE(APPSU:LastCalledDate)     !List box control field - type derived from field
APPSU:LastCalledTime   LIKE(APPSU:LastCalledTime)     !List box control field - type derived from field
APPSU:ASUID            LIKE(APPSU:ASUID)              !Primary key field - type derived from field
APPSU:ASID             LIKE(APPSU:ASID)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(UserGroupAccesses)
                       PROJECT(UGAC:AccessOption)
                       PROJECT(UGAC:UGAID)
                       PROJECT(UGAC:ASID)
                       PROJECT(UGAC:UGID)
                       JOIN(USEG:PKey_UGID,UGAC:UGID)
                         PROJECT(USEG:GroupName)
                         PROJECT(USEG:UGID)
                       END
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:3
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
LOC:AccessOption_Groups LIKE(LOC:AccessOption_Groups) !List box control field - type derived from local data
UGAC:AccessOption      LIKE(UGAC:AccessOption)        !Browse hot field - type derived from field
UGAC:UGAID             LIKE(UGAC:UGAID)               !Primary key field - type derived from field
UGAC:ASID              LIKE(UGAC:ASID)                !Browse key field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::APP:Record  LIKE(APP:RECORD),THREAD
QuickWindow          WINDOW('Form Application Sections'),AT(,,283,219),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateApplicationSections'),SYSTEM
                       SHEET,AT(5,2,276,196),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Application Section:'),AT(9,25),USE(?APP:ApplicationSection:Prompt),TRN
                           ENTRY(@s35),AT(93,25,144,10),USE(APP:ApplicationSection),MSG('Application Section name'),TIP('Applicatio' & |
  'n Section name')
                           PROMPT('Default Action:'),AT(9,41),USE(?APP:DefaultAction:Prompt),TRN
                           LIST,AT(93,41,80,10),USE(APP:DefaultAction),DROP(5),FROM('Allow|#0|View Only|#1|No Access|#2'), |
  MSG('Default action for security is'),TIP('Default action for security is')
                           LINE,AT(7,56,267,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           LIST,AT(9,63,267,120),USE(?List),VSCROLL,FORMAT('116L(2)|M~Extra Section~@s100@80L(2)|M' & |
  '~Default Extra Access Option~@s20@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(121,185,49,12),USE(?Insert),LEFT,ICON('WAINSERT.ICO'),FLAT
                           BUTTON('&Change'),AT(174,185,49,12),USE(?Change),LEFT,ICON('WAchange.ICO'),FLAT
                           BUTTON('&Delete'),AT(226,185,49,12),USE(?Delete),LEFT,ICON('WAdelete.ICO'),FLAT
                         END
                         TAB('&2) Group Accesses'),USE(?Tab4)
                           LIST,AT(11,24,267,153),USE(?List:3),VSCROLL,FORMAT('140L(2)|M~Group Name~@s35@80L(2)|M~' & |
  'Access Option~@s20@'),FROM(Queue:Browse:3),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(123,181,49,14),USE(?Insert:2),LEFT,ICON('WAINSERT.ICO'),FLAT
                           BUTTON('&Change'),AT(175,181,49,14),USE(?Change:2),LEFT,ICON('WACHANGE.ICO'),FLAT
                           BUTTON('&Delete'),AT(228,181,49,14),USE(?Delete:2),LEFT,ICON('WADELETE.ICO'),FLAT
                         END
                         TAB('&3) Users Accesses'),USE(?Tab:2)
                           LIST,AT(11,24,267,153),USE(?Browse:2),HVSCROLL,FORMAT('80L(2)|M~Login~@s20@50R(2)|M~Acc' & |
  'ess Level~L@n3@40L(2)|M~Access Option~@s10@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he UsersAccesses file')
                           BUTTON('&Insert'),AT(123,181,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(176,181,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(228,181,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&4) Usage (Procedures)'),USE(?Tab3)
                           LIST,AT(8,20,268,175),USE(?List:2),VSCROLL,FORMAT('120L(2)|M~Procedure Name~@s100@50R(2' & |
  ')|M~Last Called Date~L@d6@50R(2)|M~Last Called Time~L@t7@'),FROM(Queue:Browse:1),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('&OK'),AT(178,202,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(232,202,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,202,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW7                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW11                CLASS(BrowseClass)                    ! Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
                     END

BRW11::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW12                CLASS(BrowseClass)                    ! Browse using ?List:3
Q                      &Queue:Browse:3                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Application Section Record'
  OF InsertRecord
    ActionMessage = 'Application Section Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'App. Section Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Application_Sections')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?APP:ApplicationSection:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:AccessOption',LOC:AccessOption)                ! Added by: BrowseBox(ABC)
  BIND('LOC:ExtraAccessOption',LOC:ExtraAccessOption)      ! Added by: BrowseBox(ABC)
  BIND('LOC:AccessOption_Groups',LOC:AccessOption_Groups)  ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:ApplicationSections)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(APP:Record,History::APP:Record)
  SELF.AddHistoryField(?APP:ApplicationSection,2)
  SELF.AddHistoryField(?APP:DefaultAction,3)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ApplicationSections.SetOpenRelated()
  Relate:ApplicationSections.Open                          ! File ApplicationSections used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ApplicationSections
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:UsersAccesses,SELF) ! Initialize the browse manager
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:ApplicationSections_Extras,SELF) ! Initialize the browse manager
  BRW11.Init(?List:2,Queue:Browse:1.ViewPosition,BRW11::View:Browse,Queue:Browse:1,Relate:Application_Section_Usage,SELF) ! Initialize the browse manager
  BRW12.Init(?List:3,Queue:Browse:3.ViewPosition,BRW12::View:Browse,Queue:Browse:3,Relate:UserGroupAccesses,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?APP:ApplicationSection{PROP:ReadOnly} = True
    DISABLE(?APP:DefaultAction)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
    DISABLE(?Insert:2)
    DISABLE(?Change:2)
    DISABLE(?Delete:2)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW2.AddSortOrder(,USAC:FKey_ASID)                       ! Add the sort order for USAC:FKey_ASID for sort order 1
  BRW2.AddRange(USAC:ASID,Relate:UsersAccesses,Relate:ApplicationSections) ! Add file relationship range limit for sort order 1
  BRW2.AppendOrder('+USAC:UAID')                           ! Append an additional sort order
  BRW2.AddField(USE:Login,BRW2.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW2.AddField(USE:AccessLevel,BRW2.Q.USE:AccessLevel)    ! Field USE:AccessLevel is a hot field or requires assignment from browse
  BRW2.AddField(LOC:AccessOption,BRW2.Q.LOC:AccessOption)  ! Field LOC:AccessOption is a hot field or requires assignment from browse
  BRW2.AddField(USAC:UAID,BRW2.Q.USAC:UAID)                ! Field USAC:UAID is a hot field or requires assignment from browse
  BRW2.AddField(USAC:ASID,BRW2.Q.USAC:ASID)                ! Field USAC:ASID is a hot field or requires assignment from browse
  BRW2.AddField(USE:UID,BRW2.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  BRW7.Q &= Queue:Browse
  BRW7.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW7.AddSortOrder(,APPSE:SKey_ASID_ExtraSection)         ! Add the sort order for APPSE:SKey_ASID_ExtraSection for sort order 1
  BRW7.AddRange(APPSE:ASID,APP:ASID)                       ! Add single value range limit for sort order 1
  BRW7.AddLocator(BRW7::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW7::Sort0:Locator.Init(,APPSE:ExtraSection,1,BRW7)     ! Initialize the browse locator using  using key: APPSE:SKey_ASID_ExtraSection , APPSE:ExtraSection
  BRW7.AddField(APPSE:ExtraSection,BRW7.Q.APPSE:ExtraSection) ! Field APPSE:ExtraSection is a hot field or requires assignment from browse
  BRW7.AddField(LOC:ExtraAccessOption,BRW7.Q.LOC:ExtraAccessOption) ! Field LOC:ExtraAccessOption is a hot field or requires assignment from browse
  BRW7.AddField(APPSE:DefaultAction,BRW7.Q.APPSE:DefaultAction) ! Field APPSE:DefaultAction is a hot field or requires assignment from browse
  BRW7.AddField(APPSE:ASEID,BRW7.Q.APPSE:ASEID)            ! Field APPSE:ASEID is a hot field or requires assignment from browse
  BRW7.AddField(APPSE:ASID,BRW7.Q.APPSE:ASID)              ! Field APPSE:ASID is a hot field or requires assignment from browse
  BRW11.Q &= Queue:Browse:1
  BRW11.AddSortOrder(,APPSU:FKey_ASID_ProcedureName)       ! Add the sort order for APPSU:FKey_ASID_ProcedureName for sort order 1
  BRW11.AddRange(APPSU:ASID,APP:ASID)                      ! Add single value range limit for sort order 1
  BRW11.AddLocator(BRW11::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW11::Sort0:Locator.Init(,APPSU:ProcedureName,1,BRW11)  ! Initialize the browse locator using  using key: APPSU:FKey_ASID_ProcedureName , APPSU:ProcedureName
  BRW11.AppendOrder('+APPSU:ASUID')                        ! Append an additional sort order
  BRW11.AddField(APPSU:ProcedureName,BRW11.Q.APPSU:ProcedureName) ! Field APPSU:ProcedureName is a hot field or requires assignment from browse
  BRW11.AddField(APPSU:LastCalledDate,BRW11.Q.APPSU:LastCalledDate) ! Field APPSU:LastCalledDate is a hot field or requires assignment from browse
  BRW11.AddField(APPSU:LastCalledTime,BRW11.Q.APPSU:LastCalledTime) ! Field APPSU:LastCalledTime is a hot field or requires assignment from browse
  BRW11.AddField(APPSU:ASUID,BRW11.Q.APPSU:ASUID)          ! Field APPSU:ASUID is a hot field or requires assignment from browse
  BRW11.AddField(APPSU:ASID,BRW11.Q.APPSU:ASID)            ! Field APPSU:ASID is a hot field or requires assignment from browse
  BRW12.Q &= Queue:Browse:3
  BRW12.FileLoaded = 1                                     ! This is a 'file loaded' browse
  BRW12.AddSortOrder(,UGAC:FKey_ASID)                      ! Add the sort order for UGAC:FKey_ASID for sort order 1
  BRW12.AddRange(UGAC:ASID,APP:ASID)                       ! Add single value range limit for sort order 1
  BRW12.AddLocator(BRW12::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW12::Sort0:Locator.Init(,UGAC:ASID,1,BRW12)            ! Initialize the browse locator using  using key: UGAC:FKey_ASID , UGAC:ASID
  BRW12.AppendOrder('+USEG:GroupName')                     ! Append an additional sort order
  BRW12.AddField(USEG:GroupName,BRW12.Q.USEG:GroupName)    ! Field USEG:GroupName is a hot field or requires assignment from browse
  BRW12.AddField(LOC:AccessOption_Groups,BRW12.Q.LOC:AccessOption_Groups) ! Field LOC:AccessOption_Groups is a hot field or requires assignment from browse
  BRW12.AddField(UGAC:AccessOption,BRW12.Q.UGAC:AccessOption) ! Field UGAC:AccessOption is a hot field or requires assignment from browse
  BRW12.AddField(UGAC:UGAID,BRW12.Q.UGAC:UGAID)            ! Field UGAC:UGAID is a hot field or requires assignment from browse
  BRW12.AddField(UGAC:ASID,BRW12.Q.UGAC:ASID)              ! Field UGAC:ASID is a hot field or requires assignment from browse
  BRW12.AddField(USEG:UGID,BRW12.Q.USEG:UGID)              ! Field USEG:UGID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Application_Sections',QuickWindow)  ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1                                    ! Will call: Update_Users_Accesses
  BRW7.AskProcedure = 2                                    ! Will call: Update_ApplicationSections_Extra
  BRW12.AskProcedure = 3                                   ! Will call: Update_GroupAccesses
  BRW7.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW7.ToolbarItem.HelpButton = ?Help
  BRW11.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW11.ToolbarItem.HelpButton = ?Help
  BRW12.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW12.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Application_Sections',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Update_Users_Accesses
      Update_ApplicationSections_Extra
      Update_GroupAccesses
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.SetQueueRecord PROCEDURE

  CODE
      ! Allow|View Only|No Access
      EXECUTE USAC:AccessOption + 1
         LOC:AccessOption = 'Allow'
         LOC:AccessOption = 'View Only'
         LOC:AccessOption = 'No Access'
      .
  PARENT.SetQueueRecord
  


BRW7.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW7.SetQueueRecord PROCEDURE

  CODE
      ! Allow|View Only|No Access
      EXECUTE APPSE:DefaultAction + 1
         LOC:ExtraAccessOption    = 'Allow'
         LOC:ExtraAccessOption    = 'View Only'
         LOC:ExtraAccessOption    = 'No Access'
      .
  
  PARENT.SetQueueRecord
  


BRW12.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW12.SetQueueRecord PROCEDURE

  CODE
      ! Option - Allow, View Only, No Access
      EXECUTE UGAC:AccessOption + 1
         LOC:AccessOption_Groups = 'Allow'
         LOC:AccessOption_Groups = 'View Only'
         LOC:AccessOption_Groups = 'No Access'
      .
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Users_Accesses PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:ApplicationSection STRING(35)                          ! Application Section Name
LOC:DefaultAction    STRING(20)                            ! Default action for security is - Allow, View Only, No Access
LOC:AccessOption     STRING(20)                            ! Option - Allow, View Only, No Access
LOC:Login            STRING(20)                            ! User Login
BRW8::View:Browse    VIEW(UsersAccessesExtra)
                       PROJECT(USACE:AccessOption)
                       PROJECT(USACE:UAEID)
                       PROJECT(USACE:UID)
                       PROJECT(USACE:ASEID)
                       JOIN(APPSE:PKey_ASEID,USACE:ASEID)
                         PROJECT(APPSE:ExtraSection)
                         PROJECT(APPSE:DefaultAction)
                         PROJECT(APPSE:ASEID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
APPSE:ExtraSection     LIKE(APPSE:ExtraSection)       !List box control field - type derived from field
LOC:DefaultAction      LIKE(LOC:DefaultAction)        !List box control field - type derived from local data
LOC:AccessOption       LIKE(LOC:AccessOption)         !List box control field - type derived from local data
USACE:AccessOption     LIKE(USACE:AccessOption)       !Browse hot field - type derived from field
APPSE:DefaultAction    LIKE(APPSE:DefaultAction)      !Browse hot field - type derived from field
USACE:UAEID            LIKE(USACE:UAEID)              !Primary key field - type derived from field
USACE:UID              LIKE(USACE:UID)                !Browse key field - type derived from field
APPSE:ASEID            LIKE(APPSE:ASEID)              !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::USAC:Record LIKE(USAC:RECORD),THREAD
QuickWindow          WINDOW('Form Users Accesses'),AT(,,188,203),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('Users_Accesses'),SYSTEM
                       SHEET,AT(4,4,180,183),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('User Login:'),AT(9,22),USE(?LOC:Login:Prompt),FONT('Tahoma'),TRN
                           BUTTON('...'),AT(62,22,12,10),USE(?CallLookup_UserLogin)
                           ENTRY(@s20),AT(78,22,100,10),USE(LOC:Login),MSG('User Login'),REQ,TIP('User Login')
                           PROMPT('App. Section:'),AT(9,40),USE(?LOC:ApplicationSection:Prompt),FONT('Tahoma'),TRN
                           BUTTON('...'),AT(62,40,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(78,40,100,10),USE(LOC:ApplicationSection),MSG('Application Section name'),TIP('Applicatio' & |
  'n Section name')
                           PROMPT('Access Option:'),AT(9,56),USE(?USAC:Option:Prompt),FONT('Tahoma'),TRN
                           LIST,AT(78,56,100,10),USE(USAC:AccessOption),DROP(5),FROM('Allow|#0|View Only|#1|No Access|#2'), |
  MSG('Option'),TIP('Option')
                           LINE,AT(7,71,173,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           LIST,AT(9,76,171,92),USE(?List),VSCROLL,FORMAT('60L(2)|M~Extra Section~@s100@52L(2)|M~D' & |
  'efault Action~@s20@52L(2)|M~Access Option~@s20@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(30,171,49,12),USE(?Insert),LEFT,ICON('wainsert.ico'),FLAT,SKIP
                           BUTTON('&Change'),AT(81,171,49,12),USE(?Change),LEFT,ICON('wachange.ico'),FLAT
                           BUTTON('&Delete'),AT(130,171,49,12),USE(?Delete),LEFT,ICON('wadelete.ico'),FLAT
                         END
                       END
                       BUTTON('&OK'),AT(82,188,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(134,188,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,188,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW8                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a User Accesses Record'
  OF InsertRecord
    ActionMessage = 'User Accesses Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'User Accesses Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Users_Accesses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Login:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:DefaultAction',LOC:DefaultAction)              ! Added by: BrowseBox(ABC)
  BIND('LOC:AccessOption',LOC:AccessOption)                ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:UsersAccesses)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(USAC:Record,History::USAC:Record)
  SELF.AddHistoryField(?USAC:AccessOption,4)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ApplicationSections.SetOpenRelated()
  Relate:ApplicationSections.Open                          ! File ApplicationSections used by this procedure, so make sure it's RelationManager is open
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UsersAccesses
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:UsersAccessesExtra,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup_UserLogin)
    ?LOC:Login{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?LOC:ApplicationSection{PROP:ReadOnly} = True
    DISABLE(?USAC:AccessOption)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW8.Q &= Queue:Browse
  BRW8.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW8.AddSortOrder(,USACE:FKey_UID)                       ! Add the sort order for USACE:FKey_UID for sort order 1
  BRW8.AddRange(USACE:UID,USAC:UID)                        ! Add single value range limit for sort order 1
  BRW8.AddLocator(BRW8::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW8::Sort0:Locator.Init(,USACE:UID,1,BRW8)              ! Initialize the browse locator using  using key: USACE:FKey_UID , USACE:UID
  BRW8.AppendOrder('+USACE:UAEID,+APPSE:ASEID')            ! Append an additional sort order
  BRW8.SetFilter('(USACE:ASID = USAC:ASID)')               ! Apply filter expression to browse
  BRW8.AddField(APPSE:ExtraSection,BRW8.Q.APPSE:ExtraSection) ! Field APPSE:ExtraSection is a hot field or requires assignment from browse
  BRW8.AddField(LOC:DefaultAction,BRW8.Q.LOC:DefaultAction) ! Field LOC:DefaultAction is a hot field or requires assignment from browse
  BRW8.AddField(LOC:AccessOption,BRW8.Q.LOC:AccessOption)  ! Field LOC:AccessOption is a hot field or requires assignment from browse
  BRW8.AddField(USACE:AccessOption,BRW8.Q.USACE:AccessOption) ! Field USACE:AccessOption is a hot field or requires assignment from browse
  BRW8.AddField(APPSE:DefaultAction,BRW8.Q.APPSE:DefaultAction) ! Field APPSE:DefaultAction is a hot field or requires assignment from browse
  BRW8.AddField(USACE:UAEID,BRW8.Q.USACE:UAEID)            ! Field USACE:UAEID is a hot field or requires assignment from browse
  BRW8.AddField(USACE:UID,BRW8.Q.USACE:UID)                ! Field USACE:UID is a hot field or requires assignment from browse
  BRW8.AddField(APPSE:ASEID,BRW8.Q.APPSE:ASEID)            ! Field APPSE:ASEID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_Users_Accesses',QuickWindow)        ! Restore window settings from non-volatile store
      USE:UID = USAC:UID
      IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
         LOC:Login    = USE:Login
      .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW8.AskProcedure = 3                                    ! Will call: Update_UsersAccesses_Extra
  BRW8.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW8.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Users_Accesses',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_Users
      Browse_Application_Sections
      Update_UsersAccesses_Extra
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup_UserLogin
      ThisWindow.Update()
      USE:Login = LOC:Login
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:Login = USE:Login
        USAC:UID = USE:UID
      END
      ThisWindow.Reset(1)
    OF ?LOC:Login
      IF LOC:Login OR ?LOC:Login{PROP:Req}
        USE:Login = LOC:Login
        IF Access:Users.TryFetch(USE:Key_Login)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:Login = USE:Login
            USAC:UID = USE:UID
          ELSE
            CLEAR(USAC:UID)
            SELECT(?LOC:Login)
            CYCLE
          END
        ELSE
          USAC:UID = USE:UID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      APP:ApplicationSection = LOC:ApplicationSection
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LOC:ApplicationSection = APP:ApplicationSection
        USAC:ASID = APP:ASID
      END
      ThisWindow.Reset(1)
    OF ?LOC:ApplicationSection
      IF LOC:ApplicationSection OR ?LOC:ApplicationSection{PROP:Req}
        APP:ApplicationSection = LOC:ApplicationSection
        IF Access:ApplicationSections.TryFetch(APP:Key_ApplicationSection)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            LOC:ApplicationSection = APP:ApplicationSection
            USAC:ASID = APP:ASID
          ELSE
            CLEAR(USAC:ASID)
            SELECT(?LOC:ApplicationSection)
            CYCLE
          END
        ELSE
          USAC:ASID = APP:ASID
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF USAC:ASID ~= 0
             APP:ASID = USAC:ASID
             IF Access:ApplicationSections.TryFetch(APP:PKey_ASID) = LEVEL:Benign
                LOC:ApplicationSection    = APP:ApplicationSection
          .  .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.SetQueueRecord PROCEDURE

  CODE
      ! Allow|View Only|No Access
      EXECUTE USACE:AccessOption + 1
         LOC:AccessOption     = 'Allow'
         LOC:AccessOption     = 'View Only'
         LOC:AccessOption     = 'No Access'
      .
  
  
  
      ! Allow|View Only|No Access
      EXECUTE APPSE:DefaultAction + 1
         LOC:DefaultAction    = 'Allow'
         LOC:DefaultAction    = 'View Only'
         LOC:DefaultAction    = 'No Access'
      .
  PARENT.SetQueueRecord
  

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Application_Sections PROCEDURE 

CurrentTab           STRING(80)                            ! 
Locator              STRING(50)                            ! 
LOC:DefaultAction    STRING(20)                            ! Default action for security is
BRW1::View:Browse    VIEW(ApplicationSections)
                       PROJECT(APP:ApplicationSection)
                       PROJECT(APP:DefaultAction)
                       PROJECT(APP:ASID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
APP:ApplicationSection LIKE(APP:ApplicationSection)   !List box control field - type derived from field
LOC:DefaultAction      LIKE(LOC:DefaultAction)        !List box control field - type derived from local data
APP:DefaultAction      LIKE(APP:DefaultAction)        !Browse hot field - type derived from field
APP:ASID               LIKE(APP:ASID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Application Sections File'),AT(,,276,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_Application_Sections'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('150L(2)|M~Application Section~@s3' & |
  '5@12L(2)|M~Default Action~@s20@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Applica' & |
  'tionSections file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Application Section'),USE(?Tab:2)
                           GROUP,AT(8,19,163,10),USE(?Group1)
                             PROMPT('Locator:'),AT(8,19),USE(?Locator:Prompt),TRN
                             STRING(@s50),AT(38,19),USE(Locator),TRN
                           END
                         END
                       END
                       BUTTON('&Close'),AT(224,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(148,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Application_Sections')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:DefaultAction',LOC:DefaultAction)              ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:ApplicationSections.SetOpenRelated()
  Relate:ApplicationSections.Open                          ! File ApplicationSections used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ApplicationSections,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,APP:Key_ApplicationSection)           ! Add the sort order for APP:Key_ApplicationSection for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(?Locator,APP:ApplicationSection,1,BRW1) ! Initialize the browse locator using ?Locator using key: APP:Key_ApplicationSection , APP:ApplicationSection
  BRW1.AddField(APP:ApplicationSection,BRW1.Q.APP:ApplicationSection) ! Field APP:ApplicationSection is a hot field or requires assignment from browse
  BRW1.AddField(LOC:DefaultAction,BRW1.Q.LOC:DefaultAction) ! Field LOC:DefaultAction is a hot field or requires assignment from browse
  BRW1.AddField(APP:DefaultAction,BRW1.Q.APP:DefaultAction) ! Field APP:DefaultAction is a hot field or requires assignment from browse
  BRW1.AddField(APP:ASID,BRW1.Q.APP:ASID)                  ! Field APP:ASID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Application_Sections',QuickWindow)  ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                                    ! Will call: Update_Application_Sections
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Application_Sections',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Application_Sections
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.SetQueueRecord PROCEDURE

  CODE
      EXECUTE APP:DefaultAction + 1           ! 'Allow|#0|View Only|#1|No Access|#2'
         LOC:DefaultAction    = 'Allow'
         LOC:DefaultAction    = 'View Only'
         LOC:DefaultAction    = 'Disallow'
      ELSE
         LOC:DefaultAction    = '<unknown: ' & APP:DefaultAction & '>'
      .
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group1

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Users PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Option           STRING(20)                            ! Option
LOC:Q_Vars           GROUP,PRE()                           ! 
QV:ReminderType      STRING(20)                            ! General, Client
QV:RemindOption      STRING(20)                            ! All, Group & User, User, Group
                     END                                   ! 
LOC:Password         STRING(35)                            ! User Password
BRW5::View:Browse    VIEW(UsersAccesses)
                       PROJECT(USAC:AccessOption)
                       PROJECT(USAC:UAID)
                       PROJECT(USAC:UID)
                       PROJECT(USAC:ASID)
                       JOIN(APP:PKey_ASID,USAC:ASID)
                         PROJECT(APP:ApplicationSection)
                         PROJECT(APP:ASID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
APP:ApplicationSection LIKE(APP:ApplicationSection)   !List box control field - type derived from field
LOC:Option             LIKE(LOC:Option)               !List box control field - type derived from local data
USAC:AccessOption      LIKE(USAC:AccessOption)        !Browse hot field - type derived from field
USAC:UAID              LIKE(USAC:UAID)                !Primary key field - type derived from field
USAC:UID               LIKE(USAC:UID)                 !Browse key field - type derived from field
APP:ASID               LIKE(APP:ASID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(UsersGroups)
                       PROJECT(USEBG:UGID)
                       PROJECT(USEBG:UBGID)
                       PROJECT(USEBG:UID)
                       JOIN(USEG:PKey_UGID,USEBG:UGID)
                         PROJECT(USEG:GroupName)
                         PROJECT(USEG:UGID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
USEBG:UGID             LIKE(USEBG:UGID)               !List box control field - type derived from field
USEBG:UBGID            LIKE(USEBG:UBGID)              !Primary key field - type derived from field
USEBG:UID              LIKE(USEBG:UID)                !Browse key field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(Reminders)
                       PROJECT(REM:RID)
                       PROJECT(REM:Active)
                       PROJECT(REM:Popup)
                       PROJECT(REM:ReminderDate)
                       PROJECT(REM:ReminderTime)
                       PROJECT(REM:Notes)
                       PROJECT(REM:ReminderType)
                       PROJECT(REM:RemindOption)
                       PROJECT(REM:UID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
REM:RID                LIKE(REM:RID)                  !List box control field - type derived from field
QV:ReminderType        LIKE(QV:ReminderType)          !List box control field - type derived from local data
REM:Active             LIKE(REM:Active)               !List box control field - type derived from field
REM:Active_Icon        LONG                           !Entry's icon ID
REM:Popup              LIKE(REM:Popup)                !List box control field - type derived from field
REM:Popup_Icon         LONG                           !Entry's icon ID
REM:ReminderDate       LIKE(REM:ReminderDate)         !List box control field - type derived from field
REM:ReminderTime       LIKE(REM:ReminderTime)         !List box control field - type derived from field
QV:RemindOption        LIKE(QV:RemindOption)          !List box control field - type derived from local data
REM:Notes              LIKE(REM:Notes)                !List box control field - type derived from field
REM:ReminderType       LIKE(REM:ReminderType)         !Browse hot field - type derived from field
REM:RemindOption       LIKE(REM:RemindOption)         !Browse hot field - type derived from field
REM:UID                LIKE(REM:UID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::USE:Record  LIKE(USE:RECORD),THREAD
QuickWindow          WINDOW('Form Users'),AT(,,246,258),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM,MDI, |
  HLP('UpdateUsers'),SYSTEM
                       SHEET,AT(4,4,238,235),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Login:'),AT(9,22),USE(?USE:Login:Prompt),TRN
                           ENTRY(@s20),AT(74,22,84,10),USE(USE:Login),MSG('User Login'),REQ,TIP('User Login')
                           PROMPT('Name:'),AT(9,54),USE(?USE:Name:Prompt),TRN
                           ENTRY(@s35),AT(74,54,144,10),USE(USE:Name),MSG('User Name'),TIP('User Name')
                           PROMPT('Surname:'),AT(9,68),USE(?USE:Surname:Prompt),TRN
                           ENTRY(@s35),AT(74,68,144,10),USE(USE:Surname),MSG('User Surname'),TIP('User Surname')
                           GROUP,AT(9,88,106,10),USE(?Group_Level)
                             PROMPT('Access Level:'),AT(9,88),USE(?USE:AccessLevel:Prompt),TRN
                             SPIN(@n3),AT(74,88,40,10),USE(USE:AccessLevel),RIGHT(1),MSG('Users general access level'), |
  TIP('Users general access level')
                           END
                           PROMPT('Password:'),AT(9,124),USE(?USE:Password:Prompt),TRN
                           ENTRY(@s35),AT(74,124,144,10),USE(USE:Password),MSG('User Password'),PASSWORD,TIP('User Password')
                           PROMPT('Confirm Password:'),AT(9,141),USE(?Password:Prompt),TRN
                           ENTRY(@s35),AT(74,141,144,10),USE(LOC:Password),MSG('User Password'),PASSWORD,TIP('User Password')
                         END
                         TAB('&2) Users Groups'),USE(?Tab2)
                           GROUP,AT(113,224,126,12),USE(?Group_UserGroups)
                             BUTTON('&Insert'),AT(113,224,42,12),USE(?Insert:2)
                             BUTTON('&Change'),AT(154,224,42,12),USE(?Change:2)
                             BUTTON('&Delete'),AT(197,224,42,12),USE(?Delete:2)
                           END
                           LIST,AT(9,20,229,200),USE(?List:2),VSCROLL,FORMAT('140L(1)|M~Group Name~L(2)@s35@40R(1)' & |
  '|M~UGID~L(2)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing Records')
                         END
                         TAB('&3) App. Sections'),USE(?Tab3)
                           GROUP,AT(113,224,126,12),USE(?Group_AppSec)
                             BUTTON('&Insert'),AT(113,224,42,12),USE(?Insert)
                             BUTTON('&Change'),AT(154,224,42,12),USE(?Change)
                             BUTTON('&Delete'),AT(197,224,42,12),USE(?Delete)
                           END
                           LIST,AT(9,22,229,198),USE(?List),VSCROLL,FORMAT('100L(2)|M~Application Sections~@s35@80' & |
  'L(2)|M~Option~@s20@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                         END
                         TAB('&4) Reminders'),USE(?Tab4)
                           LIST,AT(6,21,230,214),USE(?List:3),VSCROLL,FORMAT('30R(2)|M~RID~L@n_10@55L(2)|M~Reminde' & |
  'r Type~@s20@26L(2)|MI~Active~@p p@26L(2)|MI~Popup~@p p@38L(2)|M~Date~@d5@34L(2)|M~Ti' & |
  'me~@t7@52L(2)|M~Remind Option~@s20@120L(2)|M~Notes~@s255@'),FROM(Queue:Browse:2),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('&OK'),AT(138,242,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(192,242,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,242,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW5                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW9                 CLASS(BrowseClass)                    ! Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW9::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW11                CLASS(BrowseClass)                    ! Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW11::Sort0:Locator StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Users')
      IF USE:UID = GLO:UID OR GLO:AccessLevel >= 10
      ELSE
         MESSAGE('You can only edit or view your own user details.', 'User Update', ICON:Hand)
         RETURN(LEVEL:Fatal)
      .
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?USE:Login:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Option',LOC:Option)                            ! Added by: BrowseBox(ABC)
  BIND('QV:ReminderType',QV:ReminderType)                  ! Added by: BrowseBox(ABC)
  BIND('QV:RemindOption',QV:RemindOption)                  ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Users)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(USE:Record,History::USE:Record)
  SELF.AddHistoryField(?USE:Login,2)
  SELF.AddHistoryField(?USE:Name,4)
  SELF.AddHistoryField(?USE:Surname,5)
  SELF.AddHistoryField(?USE:AccessLevel,6)
  SELF.AddHistoryField(?USE:Password,3)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Reminders.SetOpenRelated()
  Relate:Reminders.Open                                    ! File Reminders used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Users
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:UsersAccesses,SELF) ! Initialize the browse manager
  BRW9.Init(?List:2,Queue:Browse:1.ViewPosition,BRW9::View:Browse,Queue:Browse:1,Relate:UsersGroups,SELF) ! Initialize the browse manager
  BRW11.Init(?List:3,Queue:Browse:2.ViewPosition,BRW11::View:Browse,Queue:Browse:2,Relate:Reminders,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?USE:Login{PROP:ReadOnly} = True
    ?USE:Name{PROP:ReadOnly} = True
    ?USE:Surname{PROP:ReadOnly} = True
    ?USE:Password{PROP:ReadOnly} = True
    ?LOC:Password{PROP:ReadOnly} = True
    DISABLE(?Insert:2)
    DISABLE(?Change:2)
    DISABLE(?Delete:2)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
  END
      ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned)
      !   0   - Allow (default)
      !   1   - Disable
      !   2   - Hide
      !   100 - Allow                 - Default action
      !   101 - Disallow              - Default action
  
      CASE Get_User_Access(GLO:UID, 'Security', 'Update_Users', 'Upd Groups & App. Sec.', 0)
      OF 0 OROF 100
      ELSE
         DISABLE(?Insert:2)
         DISABLE(?Change:2)
         DISABLE(?Delete:2)
         DISABLE(?Insert)
         DISABLE(?Change)
         DISABLE(?Delete)
  
         BRW9.InsertControl = 0
         BRW9.ChangeControl = 0
         BRW9.DeleteControl = 0
         BRW5.InsertControl = 0
         BRW5.ChangeControl = 0
         BRW5.DeleteControl = 0
  
         DISABLE(?Group_AppSec)
         DISABLE(?Group_UserGroups)
      .
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,USAC:FKey_UID)                        ! Add the sort order for USAC:FKey_UID for sort order 1
  BRW5.AddRange(USAC:UID,USE:UID)                          ! Add single value range limit for sort order 1
  BRW5.AddLocator(BRW5::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW5::Sort0:Locator.Init(,USAC:UID,1,BRW5)               ! Initialize the browse locator using  using key: USAC:FKey_UID , USAC:UID
  BRW5.AppendOrder('+APP:ApplicationSection')              ! Append an additional sort order
  BRW5.AddField(APP:ApplicationSection,BRW5.Q.APP:ApplicationSection) ! Field APP:ApplicationSection is a hot field or requires assignment from browse
  BRW5.AddField(LOC:Option,BRW5.Q.LOC:Option)              ! Field LOC:Option is a hot field or requires assignment from browse
  BRW5.AddField(USAC:AccessOption,BRW5.Q.USAC:AccessOption) ! Field USAC:AccessOption is a hot field or requires assignment from browse
  BRW5.AddField(USAC:UAID,BRW5.Q.USAC:UAID)                ! Field USAC:UAID is a hot field or requires assignment from browse
  BRW5.AddField(USAC:UID,BRW5.Q.USAC:UID)                  ! Field USAC:UID is a hot field or requires assignment from browse
  BRW5.AddField(APP:ASID,BRW5.Q.APP:ASID)                  ! Field APP:ASID is a hot field or requires assignment from browse
  BRW9.Q &= Queue:Browse:1
  BRW9.AddSortOrder(,USEBG:Key_UID)                        ! Add the sort order for USEBG:Key_UID for sort order 1
  BRW9.AddRange(USEBG:UID,USE:UID)                         ! Add single value range limit for sort order 1
  BRW9.AddLocator(BRW9::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW9::Sort0:Locator.Init(,USEBG:UID,1,BRW9)              ! Initialize the browse locator using  using key: USEBG:Key_UID , USEBG:UID
  BRW9.AppendOrder('+USEG:GroupName,+USEBG:UBGID')         ! Append an additional sort order
  BRW9.AddField(USEG:GroupName,BRW9.Q.USEG:GroupName)      ! Field USEG:GroupName is a hot field or requires assignment from browse
  BRW9.AddField(USEBG:UGID,BRW9.Q.USEBG:UGID)              ! Field USEBG:UGID is a hot field or requires assignment from browse
  BRW9.AddField(USEBG:UBGID,BRW9.Q.USEBG:UBGID)            ! Field USEBG:UBGID is a hot field or requires assignment from browse
  BRW9.AddField(USEBG:UID,BRW9.Q.USEBG:UID)                ! Field USEBG:UID is a hot field or requires assignment from browse
  BRW9.AddField(USEG:UGID,BRW9.Q.USEG:UGID)                ! Field USEG:UGID is a hot field or requires assignment from browse
  BRW11.Q &= Queue:Browse:2
  BRW11.AddSortOrder(,REM:FKey_UID)                        ! Add the sort order for REM:FKey_UID for sort order 1
  BRW11.AddRange(REM:UID,USE:UID)                          ! Add single value range limit for sort order 1
  BRW11.AddLocator(BRW11::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW11::Sort0:Locator.Init(,REM:UID,1,BRW11)              ! Initialize the browse locator using  using key: REM:FKey_UID , REM:UID
  BRW11.AppendOrder('-REM:ReminderDate,+REM:RID')          ! Append an additional sort order
  ?List:3{PROP:IconList,1} = '~checkoffdim.ico'
  ?List:3{PROP:IconList,2} = '~checkon.ico'
  BRW11.AddField(REM:RID,BRW11.Q.REM:RID)                  ! Field REM:RID is a hot field or requires assignment from browse
  BRW11.AddField(QV:ReminderType,BRW11.Q.QV:ReminderType)  ! Field QV:ReminderType is a hot field or requires assignment from browse
  BRW11.AddField(REM:Active,BRW11.Q.REM:Active)            ! Field REM:Active is a hot field or requires assignment from browse
  BRW11.AddField(REM:Popup,BRW11.Q.REM:Popup)              ! Field REM:Popup is a hot field or requires assignment from browse
  BRW11.AddField(REM:ReminderDate,BRW11.Q.REM:ReminderDate) ! Field REM:ReminderDate is a hot field or requires assignment from browse
  BRW11.AddField(REM:ReminderTime,BRW11.Q.REM:ReminderTime) ! Field REM:ReminderTime is a hot field or requires assignment from browse
  BRW11.AddField(QV:RemindOption,BRW11.Q.QV:RemindOption)  ! Field QV:RemindOption is a hot field or requires assignment from browse
  BRW11.AddField(REM:Notes,BRW11.Q.REM:Notes)              ! Field REM:Notes is a hot field or requires assignment from browse
  BRW11.AddField(REM:ReminderType,BRW11.Q.REM:ReminderType) ! Field REM:ReminderType is a hot field or requires assignment from browse
  BRW11.AddField(REM:RemindOption,BRW11.Q.REM:RemindOption) ! Field REM:RemindOption is a hot field or requires assignment from browse
  BRW11.AddField(REM:UID,BRW11.Q.REM:UID)                  ! Field REM:UID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_Users',QuickWindow)                 ! Restore window settings from non-volatile store
      LOC:Password    = USE:Password
      IF GLO:AccessLevel > 10
         ?USE:Password{PROP:Password}  = FALSE
         ?LOC:Password{PROP:Password}  = FALSE
      .
  
  
      IF GLO:AccessLevel >= 10
      ELSE
         DISABLE(?Group_Level)
      .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW5.AskProcedure = 1                                    ! Will call: Update_Users_Accesses
  BRW9.AskProcedure = 2                                    ! Will call: Update_UsersGroups
  BRW5.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW5.ToolbarItem.HelpButton = ?Help
  BRW9.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW9.ToolbarItem.HelpButton = ?Help
  BRW11.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW11.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Reminders.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Users',QuickWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Update_Users_Accesses
      Update_UsersGroups
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
          IF LEN(CLIP(USE:Password)) < 3
             MESSAGE('Password is too short.||Must be at least 3 characters.', 'Update Users', ICON:Hand)
             QuickWindow{PROP:AcceptAll}  = FALSE
             SELECT(?USE:Password)
             CYCLE
          .
      
      
          IF USE:Password ~= LOC:Password
             MESSAGE('Passwords do not match, please re-enter.', 'Update Users', ICON:Hand)
             QuickWindow{PROP:AcceptAll}  = FALSE
             SELECT(?USE:Password)
             CYCLE
          .
      
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?USE:Login
          IF CLIP(USE:Name) = ''
             USE:Name     = USE:Login
             DISPLAY(?USE:Name)
          .
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW5.SetQueueRecord PROCEDURE

  CODE
      ! Active|Disable|Hide
      EXECUTE USAC:AccessOption + 1
         LOC:Option   = 'Active'
         LOC:Option   = 'Disable'
         LOC:Option   = 'Hide'
      ELSE
         LOC:Option   = '<Unknown: ' & USAC:AccessOption & '>'
      .
  PARENT.SetQueueRecord
  


BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW11.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
      ! General|Client
      EXECUTE REM:ReminderType + 1
         QV:ReminderType  = 'General'
         QV:ReminderType  = 'Client'
      .
  
  
      ! All|Group & User|User|Group
      EXECUTE REM:RemindOption + 1
         QV:RemindOption  = 'All'
         QV:RemindOption  = 'Group & User'
         QV:RemindOption  = 'User'
         QV:RemindOption  = 'Group'
      .
  
  IF (REM:Active = 1)
    SELF.Q.REM:Active_Icon = 2                             ! Set icon from icon list
  ELSE
    SELF.Q.REM:Active_Icon = 1                             ! Set icon from icon list
  END
  IF (REM:Popup = 1)
    SELF.Q.REM:Popup_Icon = 2                              ! Set icon from icon list
  ELSE
    SELF.Q.REM:Popup_Icon = 1                              ! Set icon from icon list
  END

