

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS003.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Users PROCEDURE 

CurrentTab           STRING(80)                            ! 
BRW1::View:Browse    VIEW(Users)
                       PROJECT(USE:Login)
                       PROJECT(USE:Name)
                       PROJECT(USE:Surname)
                       PROJECT(USE:AccessLevel)
                       PROJECT(USE:Password)
                       PROJECT(USE:UID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
USE:Name               LIKE(USE:Name)                 !List box control field - type derived from field
USE:Surname            LIKE(USE:Surname)              !List box control field - type derived from field
USE:AccessLevel        LIKE(USE:AccessLevel)          !List box control field - type derived from field
USE:Password           LIKE(USE:Password)             !List box control field - type derived from field
USE:UID                LIKE(USE:UID)                  !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Users File'),AT(,,358,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Browse_Users'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Login~@s20@80L(2)|M~Name' & |
  '~@s35@80L(2)|M~Surname~@s35@52R(2)|M~Access Level~C(0)@n3b@80L(2)|M~Password~@s35@30' & |
  'R(2)|M~UID~L@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Users file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Login'),USE(?Tab:2)
                         END
                         TAB('&2) By Name && Surname'),USE(?Tab:3)
                         END
                       END
                       BUTTON('&Close'),AT(305,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Users')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Users.SetOpenRelated()
  Relate:Users.Open                                        ! File Users used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Users,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,USE:SKey_Name_Surname)                ! Add the sort order for USE:SKey_Name_Surname for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,USE:Name,1,BRW1)               ! Initialize the browse locator using  using key: USE:SKey_Name_Surname , USE:Name
  BRW1.AddSortOrder(,USE:Key_Login)                        ! Add the sort order for USE:Key_Login for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,USE:Login,1,BRW1)              ! Initialize the browse locator using  using key: USE:Key_Login , USE:Login
  BRW1.AddField(USE:Login,BRW1.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW1.AddField(USE:Name,BRW1.Q.USE:Name)                  ! Field USE:Name is a hot field or requires assignment from browse
  BRW1.AddField(USE:Surname,BRW1.Q.USE:Surname)            ! Field USE:Surname is a hot field or requires assignment from browse
  BRW1.AddField(USE:AccessLevel,BRW1.Q.USE:AccessLevel)    ! Field USE:AccessLevel is a hot field or requires assignment from browse
  BRW1.AddField(USE:Password,BRW1.Q.USE:Password)          ! Field USE:Password is a hot field or requires assignment from browse
  BRW1.AddField(USE:UID,BRW1.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Users',QuickWindow)                 ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                                    ! Will call: Update_Users
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Users.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Users',QuickWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Users
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?Browse:1
      BRW1.UpdateViewRecord()
          IF USE:UID = GLO:UID OR GLO:AccessLevel >= 10
             BRW1.ChangeControl=?Change:4
             ENABLE(?Change:4)
          ELSE
             BRW1.ChangeControl=0
             DISABLE(?Change:4)
          .
    END
  ReturnValue = PARENT.TakeNewSelection()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF GLO:AccessLevel < 10
             BRW1.InsertControl=0
             BRW1.DeleteControl=0
             BRW1.ViewControl = 0
             DISABLE(?View:3)
             DISABLE(?Insert:4)
             DISABLE(?Delete:4)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      IF GLO:AccessLevel < 10
         USE:Password     = '**********'
      .
      IF GLO:AccessLevel < 5
         USE:AccessLevel  = ''
      .
  
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_UsersGroups PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Login            STRING(20)                            ! User Login
FDB7::View:FileDrop  VIEW(UserGroups)
                       PROJECT(USEG:GroupName)
                       PROJECT(USEG:UGID)
                     END
Queue:FileDrop       QUEUE                            !
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::USEBG:Record LIKE(USEBG:RECORD),THREAD
QuickWindow          WINDOW('Form Users Groups'),AT(,,173,138),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('Update_UsersGroups'),SYSTEM
                       SHEET,AT(4,4,165,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Login:'),AT(9,22),USE(?LOC:Login:Prompt),TRN
                           BUTTON('...'),AT(45,22,12,10),USE(?CallLookup)
                           ENTRY(@s20),AT(61,22,103,10),USE(LOC:Login),COLOR(00E9E9E9h),MSG('User Login'),READONLY,SKIP, |
  TIP('User Login')
                           PROMPT('User Group:'),AT(9,52),USE(?Prompt2),TRN
                           LIST,AT(61,52,103,10),USE(USEG:GroupName),DROP(5),FORMAT('140L(2)|M~Group Name~@s35@'),FROM(Queue:FileDrop),MSG('Name of this group of users')
                           PROMPT('UGID:'),AT(9,66),USE(?USEBG:UGID:Prompt),TRN
                           STRING(@n_10),AT(58,66,104,10),USE(USEBG:UGID),RIGHT(1),TRN
                         END
                       END
                       BUTTON('&OK'),AT(68,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(120,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(2,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Vessel Record'
  OF InsertRecord
    ActionMessage = 'Vessel Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Vessel Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_UsersGroups')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Login:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:UsersGroups)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(USEBG:Record,History::USEBG:Record)
  SELF.AddHistoryField(?USEBG:UGID,3)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:UserGroups.Open                                   ! File UserGroups used by this procedure, so make sure it's RelationManager is open
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UsersGroups
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
      IF USE:UID = 0
         !RETURN(LEVEL:Fatal)
         ?LOC:Login{PROP:Background}  = -1
         ?LOC:Login{PROP:Req}         = TRUE
         ?LOC:Login{PROP:Skip}        = FALSE
         ?LOC:Login{PROP:ReadOnly}    = FALSE
  
         ENABLE(?CallLookup)
      ELSE
         LOC:Login    = USE:Login
         USEBG:UID    = USE:UID
      .
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?LOC:Login{PROP:ReadOnly} = True
    DISABLE(?USEG:GroupName)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_UsersGroups',QuickWindow)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  FDB7.Init(?USEG:GroupName,Queue:FileDrop.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop,Relate:UserGroups,ThisWindow)
  FDB7.Q &= Queue:FileDrop
  FDB7.AddSortOrder(USEG:Key_GroupName)
  FDB7.AddField(USEG:GroupName,FDB7.Q.USEG:GroupName) !List box control field - type derived from field
  FDB7.AddField(USEG:UGID,FDB7.Q.USEG:UGID) !Primary key field - type derived from field
  FDB7.AddUpdateField(USEG:UGID,USEBG:UGID)
  ThisWindow.AddItem(FDB7.WindowComponent)
  FDB7.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:UserGroups.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_UsersGroups',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_Users
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      USE:Login = LOC:Login
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:Login = USE:Login
        USEBG:UID = USE:UID
      END
      ThisWindow.Reset(1)
    OF ?LOC:Login
      IF NOT QuickWindow{PROP:AcceptAll}
        IF LOC:Login OR ?LOC:Login{PROP:Req}
          USE:Login = LOC:Login
          IF Access:Users.TryFetch(USE:Key_Login)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              LOC:Login = USE:Login
              USEBG:UID = USE:UID
            ELSE
              CLEAR(USEBG:UID)
              SELECT(?LOC:Login)
              CYCLE
            END
          ELSE
            USEBG:UID = USE:UID
          END
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_UserGroups PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
BRW5::View:Browse    VIEW(UsersGroups)
                       PROJECT(USEBG:UID)
                       PROJECT(USEBG:UBGID)
                       PROJECT(USEBG:UGID)
                       JOIN(USE:PKey_UID,USEBG:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:UID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
USEBG:UID              LIKE(USEBG:UID)                !List box control field - type derived from field
USEBG:UBGID            LIKE(USEBG:UBGID)              !List box control field - type derived from field
USEBG:UGID             LIKE(USEBG:UGID)               !Browse key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::USEG:Record LIKE(USEG:RECORD),THREAD
QuickWindow          WINDOW('Form User Groups'),AT(,,213,166),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdateUserGroups'),SYSTEM
                       SHEET,AT(4,4,205,142),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('UGID:'),AT(9,36),USE(?USEG:UGID:Prompt),TRN
                           STRING(@n_10),AT(101,36,104,10),USE(USEG:UGID),RIGHT(1),TRN
                           PROMPT('Group Name:'),AT(9,22),USE(?USEG:GroupName:Prompt),TRN
                           ENTRY(@s35),AT(61,22,144,10),USE(USEG:GroupName),MSG('Name of this group of users'),REQ,TIP('Name of th' & |
  'is group of users')
                         END
                         TAB('&2) Users in Group'),USE(?Tab2)
                           LIST,AT(9,20,197,106),USE(?List),FORMAT('100L(2)|M~Login~@s20@40R(2)|M~UID~L@n_10@40R(2' & |
  ')|M~UBGID~L@n_10@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(77,130,42,12),USE(?Insert)
                           BUTTON('&Change'),AT(121,130,42,12),USE(?Change)
                           BUTTON('&Delete'),AT(165,130,42,12),USE(?Delete)
                         END
                       END
                       BUTTON('&OK'),AT(108,148,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(160,148,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,148,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW5                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW5::Sort0:Locator  StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a User Groups Record'
  OF InsertRecord
    ActionMessage = 'User Groups Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'User Groups Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_UserGroups')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?USEG:UGID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:UserGroups)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(USEG:Record,History::USEG:Record)
  SELF.AddHistoryField(?USEG:UGID,1)
  SELF.AddHistoryField(?USEG:GroupName,2)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:UserGroups.Open                                   ! File UserGroups used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UserGroups
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:UsersGroups,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?USEG:GroupName{PROP:ReadOnly} = True
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,USEBG:FKey_UGID)                      ! Add the sort order for USEBG:FKey_UGID for sort order 1
  BRW5.AddRange(USEBG:UGID,USEG:UGID)                      ! Add single value range limit for sort order 1
  BRW5.AddLocator(BRW5::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW5::Sort0:Locator.Init(,USEBG:UGID,1,BRW5)             ! Initialize the browse locator using  using key: USEBG:FKey_UGID , USEBG:UGID
  BRW5.AppendOrder('+USE:Login,+USEBG:UBGID')              ! Append an additional sort order
  BRW5.AddField(USE:Login,BRW5.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW5.AddField(USEBG:UID,BRW5.Q.USEBG:UID)                ! Field USEBG:UID is a hot field or requires assignment from browse
  BRW5.AddField(USEBG:UBGID,BRW5.Q.USEBG:UBGID)            ! Field USEBG:UBGID is a hot field or requires assignment from browse
  BRW5.AddField(USEBG:UGID,BRW5.Q.USEBG:UGID)              ! Field USEBG:UGID is a hot field or requires assignment from browse
  BRW5.AddField(USE:UID,BRW5.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_UserGroups',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW5.AskProcedure = 1                                    ! Will call: Update_UsersGroups
  BRW5.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW5.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:UserGroups.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_UserGroups',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_UsersGroups
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_UserGroups PROCEDURE 

CurrentTab           STRING(80)                            ! 
BRW1::View:Browse    VIEW(UserGroups)
                       PROJECT(USEG:GroupName)
                       PROJECT(USEG:UGID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the User Groups File'),AT(,,330,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Browse_UserGroups'),SYSTEM
                       LIST,AT(8,30,314,124),USE(?Browse:1),HVSCROLL,FORMAT('144L(2)|M~Group Name~@s35@42R(2)|' & |
  'M~UG ID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the UserGroups file')
                       BUTTON('&Select'),AT(61,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(114,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(167,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(220,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(273,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,322,172),USE(?CurrentTab)
                         TAB('&1) By Group Name'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(277,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_UserGroups')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:UserGroups.Open                                   ! File UserGroups used by this procedure, so make sure it's RelationManager is open
  Access:UsersGroups.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:UserGroups,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,USEG:PKey_UGID)                       ! Add the sort order for USEG:PKey_UGID for sort order 1
  BRW1.AddRange(USEG:UGID,Relate:UserGroups,Relate:UsersGroups) ! Add file relationship range limit for sort order 1
  BRW1.AddSortOrder(,USEG:Key_GroupName)                   ! Add the sort order for USEG:Key_GroupName for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,USEG:GroupName,1,BRW1)         ! Initialize the browse locator using  using key: USEG:Key_GroupName , USEG:GroupName
  BRW1.AddField(USEG:GroupName,BRW1.Q.USEG:GroupName)      ! Field USEG:GroupName is a hot field or requires assignment from browse
  BRW1.AddField(USEG:UGID,BRW1.Q.USEG:UGID)                ! Field USEG:UGID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_UserGroups',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1                                    ! Will call: Update_UserGroups
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:UserGroups.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_UserGroups',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_UserGroups
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
UserLogout           PROCEDURE                             ! Declare Procedure

  CODE
    CLEAR(GLO:User_Group)
    POST(EVENT:User,,1)
    RETURN
!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
UserLogin PROCEDURE 

LOC:Group            GROUP,PRE(L_G)                        ! 
Login                STRING(20)                            ! User Login
Password             STRING(35)                            ! User Password
Remember_Login       BYTE                                  ! Remember my login on this computer
                     END                                   ! 
QuickWindow          WINDOW('Login'),AT(,,260,120),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY,IMM,MODAL, |
  HLP('Login')
                       SHEET,AT(4,4,251,97),USE(?Sheet1)
                         TAB('User Login'),USE(?Tab1)
                           PROMPT('Login:'),AT(10,28),USE(?Login:Prompt),FONT(,10,,,CHARSET:ANSI),TRN
                           ENTRY(@s20),AT(70,28),USE(L_G:Login),FONT(,10,,,CHARSET:ANSI),MSG('User Login'),REQ,TIP('User Login')
                           BUTTON('...'),AT(53,28,12,14),USE(?CallLookup)
                           PROMPT('Password:'),AT(10,48,,12),USE(?Password:Prompt),FONT(,10,,,CHARSET:ANSI),TRN
                           ENTRY(@s35),AT(70,48),USE(L_G:Password),FONT(,10,,,CHARSET:ANSI),MSG('User Password'),PASSWORD, |
  TIP('User Password')
                           CHECK(' Remember Login'),AT(70,78),USE(L_G:Remember_Login),FONT(,10,,,CHARSET:ANSI),HIDE,MSG('Remember m' & |
  'y login on this computer'),TIP('Remember my login on this computer'),TRN
                         END
                       END
                       BUTTON('&Cancel'),AT(206,104,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(4,104,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('&OK'),AT(154,104,49,14),USE(?Ok:2),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UserLogin')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Login:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Control.Open                                      ! File Control used by this procedure, so make sure it's RelationManager is open
  Relate:Users.SetOpenRelated()
  Relate:Users.Open                                        ! File Users used by this procedure, so make sure it's RelationManager is open
  Access:UsersGroups.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
      L_G:Login           = GETINI('Login', 'LastLogin', '', 'C:\TransIS.INI')
      L_G:Remember_Login  = GETINI('Login', 'RememberPassword_New', 0, 'C:\TransIS.INI')
  
      IF L_G:Remember_Login > 0
         SET(Control)
         NEXT(Control)
         IF ~ERRORCODE()
            L_G:Password  = CTR:RememberPassword
      .  .
  
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Control.Close
    Relate:Users.Close
  END
      POST(EVENT:User,, 1)
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Select_Users_NonMDI
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_G:Login
      IF L_G:Login OR ?L_G:Login{PROP:Req}
        USE:Login = L_G:Login
        IF Access:Users.TryFetch(USE:Key_Login)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_G:Login = USE:Login
          ELSE
            SELECT(?L_G:Login)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      USE:Login = L_G:Login
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_G:Login = USE:Login
      END
      ThisWindow.Reset(1)
    OF ?Cancel
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    OF ?Ok:2
      ThisWindow.Update()
          ! Check password
          IF CLIP(L_G:Login) = ''
             MESSAGE('Please supply a login.', 'Login', ICON:Exclamation)
          ELSE
             CLEAR(USE:Record)
             USE:Login        = L_G:Login
             IF Access:Users.TryFetch(USE:Key_Login) ~= LEVEL:Benign
                MESSAGE('Unable to load user. (1)', 'Login', ICON:Exclamation)
             ELSE
                IF CLIP(USE:Password) = CLIP(L_G:Password)
                   ! Ok login the user
                   PUTINI('Login', 'LastLogin', L_G:Login, 'C:\TransIS.INI')
                   PUTINI('Login', 'RememberPassword_New', L_G:Remember_Login, 'C:\TransIS.INI')
      
                   SET(CTR:PKey_CID)
                   NEXT(Control)
                   IF ~ERRORCODE()
                      IF L_G:Remember_Login = TRUE
                         CTR:RememberPassword = L_G:Password
                      ELSE
                         CTR:RememberPassword = ''
                      .
                      PUT(Control)
                   ELSE
                      IF L_G:Remember_Login = TRUE
                         CTR:RememberPassword = L_G:Password
                         ADD(Control)
                   .  .
      
                   Add_Audit(10, GlobalErrors.GetProcedureName(), 'User logged in: ' & CLIP(L_G:Login), 'Password correct.', 10)
      
                   GLO:UID            = USE:UID
                   GLO:Login          = USE:Login
                   GLO:AccessLevel    = USE:AccessLevel
                   GLO:LoggedInDate   = TODAY()
                   GLO:LoggedInTime   = CLOCK()
      
                   USEBG:UID          = USE:UID
                   SET(USEBG:Key_UID, USEBG:Key_UID)
                   LOOP
                      IF Access:UsersGroups.TryNext() ~= LEVEL:Benign
                         BREAK
                      .
                      IF USEBG:UID ~= USE:UID
                         BREAK
                      .
      
                      IF CLIP(GLO:UGIDs) = ''
                         GLO:UGIDs    = USEBG:UGID
                      ELSE
                         GLO:UGIDs    = CLIP(GLO:UGIDs) & ',' & USEBG:UGID
                   .  .
      
                   POST(EVENT:CloseWindow)
      
                   ! Check password is sufficient
                   IF LEN(CLIP(L_G:Password)) < 3
                      MESSAGE('The new password requirements mean that you need to change your password.||Please do so on the User screen.', 'User Login', ICON:Exclamation)
                      START(Update_User_h,,CLIP(GLO:UID))
                   .
                ELSE
                   ! Store this in audit
                   Add_Audit(10, GlobalErrors.GetProcedureName(), 'Bad login for user: ' & CLIP(L_G:Login), 'Password failed.', 10)
      
                   MESSAGE('The Login and Password do not match.', 'Login', ICON:Hand)
                   SELECT(?L_G:Password)
          .  .  .
      
          CYCLE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF CLIP(L_G:Login) ~= ''
             SELECT(?L_G:Password)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Update_User_h        PROCEDURE  (p:UID)                    ! Declare Procedure
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Users.Open()
     .
     Access:Users.UseFile()
    USE:UID = p:UID
    IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
       GlobalRequest    = ChangeRecord
       Update_Users()
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:Users.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
MySettings PROCEDURE 

LOC:MySettings       GROUP,PRE(LO)                         ! 
StartReminders       BYTE                                  ! Start application with reminders On
ReminderPopups       BYTE                                  ! Popup reminders
                     END                                   ! 
CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Setup_Group      GROUP,PRE(L_SG)                       ! 
Branch               STRING(35)                            ! Branch Name
Address              STRING(35)                            ! Name of this address
DefaultRatesClient   STRING(35)                            ! 
DefaultRatesTransporter STRING(35)                         ! Transporters Name
                     END                                   ! 
LOC:Local_Machine_Settings GROUP,PRE(L_LM)                 ! 
Dot_Empty_Lines      USHORT                                ! Print a dot on empty lines below this line (0 is off)
BID                  ULONG                                 ! Branch ID
Printer_Ports        GROUP,PRE(L_LM)                       ! 
Invoice              STRING(4)                             ! 
Invoice2             STRING(255)                           ! 
Credit_Note          STRING(4)                             ! 
Credit_Note2         STRING(255)                           ! 
Delivery_Note        STRING(4)                             ! 
Delivery_Note2       STRING(255)                           ! 
Statement            STRING(4)                             ! 
Statement2           STRING(255)                           ! 
Invoice_Print_Option BYTE                                  ! 
                     END                                   ! 
Manifest_Group       GROUP,PRE(L_LM)                       ! 
Print_Manifest       BYTE                                  ! Prompt to print the manifest
Create_Invoices      BYTE                                  ! Prompt to create invoices
Print_Delivery_Notes BYTE                                  ! Prompt to print Delivery Notes
Print_Invoices       BYTE                                  ! Prompt to Print Invoices
State_Progression    BYTE                                  ! How the State of a Manifest is progressed
                     END                                   ! 
TripSheet_Group      GROUP,PRE(L_TS)                       ! 
TS_State_Progression BYTE                                  ! 
                     END                                   ! 
Debug_Group          GROUP,PRE(L_DG)                       ! 
Thread_Send_Proc_Events BYTE                               ! On or Off
                     END                                   ! 
Default_Manifest_Journey STRING(70)                        ! Description
Default_Manifest_JID ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
Default_Service      STRING(35)                            ! Service Requirement
Default_SID          ULONG                                 ! Service Requirement ID
Default_Manifest_Transporter STRING(35)                    ! Transporters Name
Default_Manifest_Transporter_ID ULONG                      ! Transporter ID
DI_Items_Action      BYTE                                  ! Action after capturing a new DI item
BusyHandling         BYTE                                  ! Database connection type setting
                     END                                   ! 
LOC:Global_Settings  GROUP,PRE(L_GS)                       ! This network - GLO INI
ReplicatedDatabaseID LONG                                  ! The ID of the Replicated Database - used to decide auto numbering
                     END                                   ! 
LOC:Ini_Group        GROUP,PRE(L_INIG)                     ! 
Global_INI           CSTRING('.\TransIS.INI<0>{241}')      ! All user ini file location (all users of this TransIS path)
Local_INI            CSTRING('TransISL.INI<0>{242}')       ! Local machine ini file location (no path will put it in Windows folder)
                     END                                   ! 
QuickWindow          WINDOW('My Settings'),AT(,,295,256),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY,MDI, |
  HLP('MySettings'),SYSTEM
                       SHEET,AT(4,16,287,219),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab2)
                           PROMPT('Branch:'),AT(14,34),USE(?L_SG:Branch:Prompt),TRN
                           BUTTON('...'),AT(85,34,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(101,34,121,10),USE(L_SG:Branch),MSG('Branch Name'),REQ,TIP('Branch Name')
                           PROMPT('Replicated Database ID:'),AT(14,55),USE(?ReplicatedDatabaseID:Prompt),TRN
                           ENTRY(@n_10),AT(101,55,87,10),USE(L_GS:ReplicatedDatabaseID),RIGHT(1),MSG('The ID of th' & |
  'e Replicated Database - used to decide auto numbering'),TIP('The ID of the Replicate' & |
  'd Database - used to decide auto numbering')
                           CHECK(' Start up with Reminders'),AT(101,71),USE(LO:StartReminders),MSG('Start applicat' & |
  'ion with reminders On'),TIP('Start application with reminders On'),TRN
                           CHECK(' Reminder Popups'),AT(101,87),USE(LO:ReminderPopups),MSG('Popup reminders'),TIP('Popup reminders'), |
  TRN
                           PROMPT('DI Items Action:'),AT(22,103),USE(?DI_Items_Action:Prompt),TRN
                           LIST,AT(101,103,87,10),USE(L_LM:DI_Items_Action),DROP(5),FROM('Ask Always|#0|General Ta' & |
  'b|#1|Charges Tab|#2'),MSG('Action after capturing a new DI item'),TIP('Action after ' & |
  'capturing a new DI item perform this action on returning to the DI screen')
                           GROUP('Manifest Prompts'),AT(17,127,266,58),USE(?Group_ManifestPrompts),BOXED,TRN
                             CHECK(' Print Manifest'),AT(22,140),USE(L_LM:Print_Manifest),MSG('Prompt to print the manifest'), |
  TIP('Prompt to print the manifest'),TRN
                             CHECK(' Create Invoices'),AT(22,151),USE(L_LM:Create_Invoices),MSG('Prompt to create invoices'), |
  TIP('Prompt to create invoices'),TRN
                             CHECK(' Print Delivery Notes'),AT(126,140),USE(L_LM:Print_Delivery_Notes),MSG('Prompt to ' & |
  'print Delivery Notes'),TIP('Prompt to print Delivery Notes'),TRN
                             CHECK(' Print Invoices'),AT(126,151),USE(L_LM:Print_Invoices),MSG('Print Invoices'),TIP('Prompt to ' & |
  'Print Invoices'),TRN
                             PROMPT('State Progression:'),AT(22,167),USE(?State_Progression:Prompt),TRN
                             LIST,AT(126,167,61,10),USE(L_LM:State_Progression),DROP(5),FROM('Manual|#0|Automatic|#1' & |
  '|Prompted|#2'),MSG('How the State of a Manifest is progressed'),TIP('How the State o' & |
  'f a Manifest is progressed')
                           END
                           GROUP('Trip Sheet Prompts'),AT(17,189,266,37),USE(?Group7),BOXED,TRN
                             PROMPT('State Progression:'),AT(22,205),USE(?L_LM:TS_State_Progression:Prompt),TRN
                             LIST,AT(126,205,61,10),USE(L_LM:TS_State_Progression),DROP(5),FROM('Manual|#0|Automatic' & |
  '|#1|Prompted|#2')
                           END
                         END
                         TAB('&2) Defaults'),USE(?Tab_Local_Defaults)
                           GROUP('Delivery && Manifest Defaults'),AT(14,55,253,52),USE(?Group4),BOXED,TRN
                             PROMPT('Journey:'),AT(21,68),USE(?Default_Manifest_Journey:Prompt),TRN
                             ENTRY(@s70),AT(122,68,139,10),USE(L_LM:Default_Manifest_Journey),MSG('Description'),TIP('Description')
                             BUTTON('...'),AT(105,68,12,10),USE(?CallLookup_Def_Man_Journey)
                             PROMPT('Transporter:'),AT(21,90),USE(?Default_Manifest_Transporter:Prompt),TRN
                             BUTTON('...'),AT(105,90,12,10),USE(?CallLookup_Def_Man_Transp)
                             ENTRY(@s35),AT(122,90,139,10),USE(L_LM:Default_Manifest_Transporter),MSG('Transporters Name'), |
  REQ,TIP('Transporters Name')
                           END
                           GROUP('Delivery Defaults'),AT(14,154,253,47),USE(?Group3),BOXED,TRN
                             PROMPT('Service Requirement:'),AT(21,167),USE(?L_LM:Default_Service:Prompt),TRN
                             ENTRY(@s35),AT(122,167,139,10),USE(L_LM:Default_Service),MSG('Service Requirement'),TIP('Service Requirement')
                             BUTTON('...'),AT(105,167,12,10),USE(?CallLookup_Def_Service)
                           END
                         END
                         TAB('&3) Printing'),USE(?Tab_Local_Printing)
                           OPTION('Invoice Print Option'),AT(70,52,47,150),USE(L_LM:Invoice_Print_Option),TRN
                             RADIO(' Default to Ports'),AT(206,52),USE(?Invoice_Print_Option:Radio1),TIP('Use the Po' & |
  'rt as the default'),TRN,VALUE('0')
                             RADIO(' Default to Devices'),AT(197,122),USE(?Invoice_Print_Option:Radio2),TIP('Use the De' & |
  'vice as the default'),TRN,VALUE('1')
                           END
                           GROUP('Printer Ports'),AT(14,61,266,49),USE(?Group_PrinterPort),BOXED,TRN
                             PROMPT('Invoice:'),AT(21,74),USE(?L_LM:Invoice_Printer_Port:Prompt),TRN
                             LIST,AT(70,74,40,10),USE(L_LM:Invoice),DROP(5),FROM('LPT1|#LPT1|LPT2|#LPT2|LPT3|#LPT3|LPT4|#LPT4')
                             PROMPT('Credit Note:'),AT(21,90),USE(?Credit_Note_Printer_Port:Prompt),TRN
                             LIST,AT(70,90,40,10),USE(L_LM:Credit_Note),DROP(5),FROM('LPT1|#LPT1|LPT2|#LPT2|LPT3|#LP' & |
  'T3|LPT4|#LPT4')
                             PROMPT('Delivery Note:'),AT(169,74),USE(?Delivery_Note_Printer_Port:Prompt),TRN
                             LIST,AT(226,74,40,10),USE(L_LM:Delivery_Note),DROP(5),FROM('LPT1|#LPT1|LPT2|#LPT2|LPT3|' & |
  '#LPT3|LPT4|#LPT4')
                             PROMPT('Statement:'),AT(169,90),USE(?Statement_Printer_Port:Prompt),TRN
                             LIST,AT(226,90,40,10),USE(L_LM:Statement),DROP(5),FROM('LPT1|#LPT1|LPT2|#LPT2|LPT3|#LPT' & |
  '3|LPT4|#LPT4')
                           END
                           GROUP('Printer Devices'),AT(14,130,266,74),USE(?Group_PrinterDevices),BOXED,TRN
                             PROMPT('Invoice:'),AT(21,143),USE(?Invoice2:Prompt),TRN
                             ENTRY(@s255),AT(133,143,121,10),USE(L_LM:Invoice2),COLOR(00E9E9E9h),READONLY
                             BUTTON('...'),AT(258,143,12,10),USE(?Button_Printer),TIP('Choose a printer')
                             PROMPT('Delivery Note:'),AT(21,159),USE(?Delivery_Note2:Prompt),TRN
                             ENTRY(@s255),AT(133,159,121,10),USE(L_LM:Delivery_Note2),COLOR(00E9E9E9h),READONLY,SKIP
                             BUTTON('...'),AT(258,159,12,10),USE(?Button_Printer:2),TIP('Choose a printer')
                             PROMPT('Credit Note:'),AT(21,173),USE(?Credit_Note2:Prompt),TRN
                             ENTRY(@s255),AT(133,173,121,10),USE(L_LM:Credit_Note2),COLOR(00E9E9E9h),READONLY,SKIP
                             BUTTON('...'),AT(258,173,12,10),USE(?Button_Printer:3),TIP('Choose a printer')
                             PROMPT('Statement:'),AT(21,186),USE(?Statement2:Prompt),TRN
                             ENTRY(@s255),AT(133,186,121,10),USE(L_LM:Statement2),COLOR(00E9E9E9h),READONLY,SKIP
                             BUTTON('...'),AT(258,186,12,10),USE(?Button_Printer:4),TIP('Choose a printer')
                           END
                           PROMPT('Dot Empty Lines:'),AT(14,215),USE(?Dot_Empty_Lines:Prompt),TRN
                           SPIN(@n6),AT(133,215,40,10),USE(L_LM:Dot_Empty_Lines),RIGHT(1),MSG('Print a dot on empt' & |
  'y lines below this line (0 is off)'),TIP('Print a dot on empty lines below this line' & |
  ' (0 is off)<0DH,0AH>Default is 45')
                         END
                         TAB('&4) Debug'),USE(?Tab3)
                           CHECK(' Thread Send Proc Events - Local'),AT(69,36),USE(L_LM:Thread_Send_Proc_Events),MSG('On or Of'), |
  TIP('On or Of'),TRN
                           PROMPT('Testing Mode:'),AT(10,98),USE(?GLO:Testing_Mode:Prompt),TRN
                           SPIN(@n3),AT(69,98,52,10),USE(GLO:Testing_Mode),RIGHT(1)
                           PROMPT('Busy Handling:'),AT(10,218),USE(?L_LM:BusyHandling:Prompt),TRN
                           LIST,AT(69,218,123,10),USE(L_LM:BusyHandling),DROP(5),FROM('Default|#0|Do Nothing|#1|Co' & |
  'nnection Per Thread|#2|Retry on Busy (default)|#3|Connection Locking|#4'),MSG('Database c' & |
  'onnection type setting'),TIP('Database connection type setting')
                         END
                         TAB('&5) INI Files'),USE(?Tab5)
                           GROUP('Locations (Folder / Directory)'),AT(9,36,276,68),USE(?Group6),BOXED,TRN
                           END
                           PROMPT('Global INI:'),AT(17,58),USE(?GLO:Global_INI:Prompt),TRN
                           BUTTON('...'),AT(58,58,12,10),USE(?LookupFile)
                           ENTRY(@s254),AT(77,58,201,10),USE(L_INIG:Global_INI),MSG('All user ini'),TIP('All user ini')
                           PROMPT('Local INI:'),AT(17,79),USE(?GLO:Local_INI:Prompt),TRN
                           BUTTON('...'),AT(58,79,12,10),USE(?LookupFile:2)
                           ENTRY(@s254),AT(77,79,201,10),USE(L_INIG:Local_INI),MSG('Local machine ini file location'), |
  TIP('Local machine ini file location')
                         END
                       END
                       BUTTON('Edit User'),AT(4,240,,14),USE(?Button_EditUser),LEFT,ICON(ICON:Child),FLAT
                       PROMPT('These are local settings to each machine.'),AT(11,4,273,10),USE(?Prompt10),FONT(,, |
  ,FONT:bold,CHARSET:ANSI),CENTER
                       BUTTON('&OK'),AT(188,240,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(242,240,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(122,240,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup7          SelectFileClass
FileLookup8          SelectFileClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MySettings')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?L_SG:Branch:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddItem(?OK,RequestCancelled)                       ! Add the cancel control to the window manager
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ServiceRequirements.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
      LO:StartReminders   = GETINI('MySettings', 'StartReminders-' & CLIP(GLO:Login), '1', GLO:Local_INI)
      LO:ReminderPopups   = GETINI('MySettings', 'ReminderPopups-' & CLIP(GLO:Login), '1', GLO:Local_INI)
  
  
      L_INIG:Local_INI    = GETINI('MySettings', 'Local_INI-' & CLIP(GLO:Login), GLO:Local_INI, GLO:Local_INI)
      L_INIG:Global_INI   = GETINI('MySettings', 'Global_INI-' & CLIP(GLO:Login), GLO:Global_INI, GLO:Local_INI)
      L_LM:Invoice                = GETINI('Printer_Ports', 'Invoice', 'LPT1', GLO:Local_INI)
      L_LM:Credit_Note            = GETINI('Printer_Ports', 'Credit_Note', 'LPT1', GLO:Local_INI)
      L_LM:Delivery_Note          = GETINI('Printer_Ports', 'Delivery_Note', 'LPT1', GLO:Local_INI)
      L_LM:Statement              = GETINI('Printer_Ports', 'Statement', 'LPT1', GLO:Local_INI)
  
      L_LM:Invoice2               = GETINI('Printer_Devices', 'Invoice', '', GLO:Local_INI)
      L_LM:Delivery_Note2         = GETINI('Printer_Devices', 'Delivery_Note', '', GLO:Local_INI)
      L_LM:Credit_Note2           = GETINI('Printer_Devices', 'Credit_Note', '', GLO:Local_INI)
      L_LM:Statement2             = GETINI('Printer_Devices', 'Statement', '', GLO:Local_INI)
  
      L_LM:Invoice_Print_Option   = GETINI('Printer_Devices', 'Invoice_Opt', '', GLO:Local_INI)
  
  
  
      L_LM:Print_Manifest         = GETINI('Manifest_Prompts', 'Print_Manifest', 0, GLO:Local_INI)
      L_LM:Create_Invoices        = GETINI('Manifest_Prompts', 'Create_Invoices', 0, GLO:Local_INI)
      L_LM:Print_Delivery_Notes   = GETINI('Manifest_Prompts', 'Print_Delivery_Notes', 0, GLO:Local_INI)
      L_LM:Print_Invoices         = GETINI('Manifest_Prompts', 'Print_Invoices', 0, GLO:Local_INI)
      L_LM:State_Progression      = GETINI('Manifest_Prompts', 'State_Progression', 2, GLO:Local_INI)
  
      L_LM:TS_State_Progression   = GETINI('TripSheet_Prompts', 'State_Progression', 2, GLO:Local_INI)
  
  
      L_LM:Thread_Send_Proc_Events    = GETINI('Setup', 'Thread_Send_Proc_Events', 1, GLO:Local_INI)
  
  
  
      L_LM:Default_Manifest_JID   = GETINI('Manifest', 'Default_Manifest_JID', 0, GLO:Local_INI)
      JOU:JID                             = L_LM:Default_Manifest_JID
      IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
         L_LM:Default_Manifest_Journey    = JOU:Journey
      .
  
  
      L_LM:Default_SID            = GETINI('Delivery', 'Default_DI_SID', 0, GLO:Local_INI)
      SERI:SID                    = L_LM:Default_SID
      IF Access:ServiceRequirements.TryFetch(SERI:PKey_SID) = LEVEL:Benign
         L_LM:Default_Service     = SERI:ServiceRequirement
      .
  
  
      L_LM:Default_Manifest_Transporter_ID    = GETINI('Manifest', 'Default_Manifest_TransID', 0, GLO:Local_INI)
      TRA:TID                     = L_LM:Default_Manifest_Transporter_ID
      IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
         L_LM:Default_Manifest_Transporter    = TRA:TransporterName
      .
  
  
      GLO:Testing_Mode            = GETINI('Setup', 'Testing_Mode', 0, GLO:Local_INI)
  
  
      L_LM:DI_Items_Action        = GETINI('Setup', 'DI_Items_Action', , GLO:Local_INI)
      L_LM:BID            = GETINI('Browse_Setup', 'Branch_ID', , GLO:Global_INI)
      BRA:BID             = L_LM:BID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         L_SG:Branch      = BRA:BranchName
      .
      L_GS:ReplicatedDatabaseID   = GETINI('Replication_Setup', 'ReplicatedDatabaseID', , GLO:Global_INI)
      L_LM:BusyHandling       = GETINI('Setup','BusyHandling','3','.\TransIS.INI')
  
      L_LM:Dot_Empty_Lines    = GETINI('Print_Cont', 'Dot_Empty_Lines', 45, GLO:Local_INI)
  FileLookup7.Init
  FileLookup7.ClearOnCancel = False
  FileLookup7.Flags=BOR(FileLookup7.Flags,FILE:LongName)   ! Allow long filenames
  FileLookup7.Flags=BOR(FileLookup7.Flags,FILE:Save)       ! Allow save Dialog
  FileLookup7.SetMask('All Files','*.*')                   ! Set the file mask
  FileLookup7.DefaultDirectory='TransIS.INI'
  FileLookup7.WindowTitle='Global INI'
  FileLookup8.Init
  FileLookup8.ClearOnCancel = False
  FileLookup8.Flags=BOR(FileLookup8.Flags,FILE:LongName)   ! Allow long filenames
  FileLookup8.Flags=BOR(FileLookup8.Flags,FILE:Save)       ! Allow save Dialog
  FileLookup8.SetMask('All Files','*.*')                   ! Set the file mask
  FileLookup8.DefaultDirectory='.\TransIS.INI'
  FileLookup8.WindowTitle='Local INI'
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Branches
      Select_Journey
      Select_Transporter
      Browse_ServiceRequirements
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
          PUTINI('MySettings', 'StartReminders-' & CLIP(GLO:Login), LO:StartReminders, GLO:Local_INI)
          PUTINI('MySettings', 'ReminderPopups-' & CLIP(GLO:Login), LO:ReminderPopups, GLO:Local_INI)
      
          PUTINI('MySettings', 'Local_INI-' & CLIP(GLO:Login), L_INIG:Local_INI, GLO:Local_INI)
          PUTINI('MySettings', 'Global_INI-' & CLIP(GLO:Login), L_INIG:Global_INI, GLO:Local_INI)
      
          PUTINI('MySettings', 'Local_INI-' & CLIP(GLO:Login), L_INIG:Local_INI, L_INIG:Local_INI)
          PUTINI('MySettings', 'Global_INI-' & CLIP(GLO:Login), L_INIG:Global_INI, L_INIG:Local_INI)
          PUTINI('Printer_Ports', 'Invoice', L_LM:Invoice, GLO:Local_INI)
          PUTINI('Printer_Ports', 'Credit_Note', L_LM:Credit_Note, GLO:Local_INI)
          PUTINI('Printer_Ports', 'Delivery_Note', L_LM:Delivery_Note, GLO:Local_INI)
          PUTINI('Printer_Ports', 'Statement', L_LM:Statement, GLO:Local_INI)
      
          PUTINI('Printer_Devices', 'Invoice', L_LM:Invoice2, GLO:Local_INI)
          PUTINI('Printer_Devices', 'Credit_Note', L_LM:Credit_Note2, GLO:Local_INI)
          PUTINI('Printer_Devices', 'Delivery_Note', L_LM:Delivery_Note2, GLO:Local_INI)
          PUTINI('Printer_Devices', 'Statement', L_LM:Statement2, GLO:Local_INI)
      
          PUTINI('Printer_Devices', 'Invoice_Opt', L_LM:Invoice_Print_Option, GLO:Local_INI)
      
          PUTINI('Manifest_Prompts', 'Print_Manifest', L_LM:Print_Manifest, GLO:Local_INI)
          PUTINI('Manifest_Prompts', 'Create_Invoices', L_LM:Create_Invoices, GLO:Local_INI)
          PUTINI('Manifest_Prompts', 'Print_Delivery_Notes', L_LM:Print_Delivery_Notes, GLO:Local_INI)
          PUTINI('Manifest_Prompts', 'Print_Invoices', L_LM:Print_Invoices, GLO:Local_INI)
          PUTINI('Manifest_Prompts', 'State_Progression', L_LM:State_Progression, GLO:Local_INI)
      
          PUTINI('TripSheet_Prompts', 'State_Progression', L_LM:TS_State_Progression, GLO:Local_INI)
      
          PUTINI('Setup', 'Thread_Send_Proc_Events', L_LM:Thread_Send_Proc_Events, GLO:Local_INI)
      
      
      
          PUTINI('Manifest', 'Default_Manifest_JID', L_LM:Default_Manifest_JID, GLO:Local_INI)
      
          PUTINI('Delivery', 'Default_DI_SID', L_LM:Default_SID, GLO:Local_INI)
      
      
      
          PUTINI('Manifest', 'Default_Manifest_TransID', L_LM:Default_Manifest_Transporter_ID, GLO:Local_INI)
      
          PUTINI('Setup', 'Testing_Mode', GLO:Testing_Mode, GLO:Local_INI)
      
          PUTINI('Setup', 'DI_Items_Action', L_LM:DI_Items_Action, GLO:Local_INI)
      
      
          PUTINI('Browse_Setup', 'Branch_ID', L_LM:BID, GLO:Global_INI)
      
      
      
          ! Global ini
          PUTINI('Replication_Setup', 'ReplicatedDatabaseID', L_GS:ReplicatedDatabaseID, GLO:Global_INI)
      
          GLO:ReplicatedDatabaseID    = L_GS:ReplicatedDatabaseID
          PUTINI('Setup','BusyHandling',L_LM:BusyHandling,'.\TransIS.INI')
      
          PUTINI('Print_Cont', 'Dot_Empty_Lines', L_LM:Dot_Empty_Lines, GLO:Local_INI)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      BRA:BranchName = L_SG:Branch
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SG:Branch = BRA:BranchName
        L_LM:BID = BRA:BID
      END
      ThisWindow.Reset(1)
    OF ?L_SG:Branch
      IF L_SG:Branch OR ?L_SG:Branch{PROP:Req}
        BRA:BranchName = L_SG:Branch
        IF Access:Branches.TryFetch(BRA:Key_BranchName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SG:Branch = BRA:BranchName
            L_LM:BID = BRA:BID
          ELSE
            CLEAR(L_LM:BID)
            SELECT(?L_SG:Branch)
            CYCLE
          END
        ELSE
          L_LM:BID = BRA:BID
        END
      END
      ThisWindow.Reset()
    OF ?L_LM:Default_Manifest_Journey
      IF L_LM:Default_Manifest_Journey OR ?L_LM:Default_Manifest_Journey{PROP:Req}
        JOU:Journey = L_LM:Default_Manifest_Journey
        IF Access:Journeys.TryFetch(JOU:Key_Journey)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            L_LM:Default_Manifest_Journey = JOU:Journey
            L_LM:Default_Manifest_JID = JOU:JID
          ELSE
            CLEAR(L_LM:Default_Manifest_JID)
            SELECT(?L_LM:Default_Manifest_Journey)
            CYCLE
          END
        ELSE
          L_LM:Default_Manifest_JID = JOU:JID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_Def_Man_Journey
      ThisWindow.Update()
      JOU:Journey = L_LM:Default_Manifest_Journey
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_LM:Default_Manifest_Journey = JOU:Journey
        L_LM:Default_Manifest_JID = JOU:JID
      END
      ThisWindow.Reset(1)
    OF ?CallLookup_Def_Man_Transp
      ThisWindow.Update()
      TRA:TransporterName = L_LM:Default_Manifest_Transporter
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        L_LM:Default_Manifest_Transporter = TRA:TransporterName
        L_LM:Default_Manifest_Transporter_ID = TRA:TID
      END
      ThisWindow.Reset(1)
    OF ?L_LM:Default_Manifest_Transporter
      IF L_LM:Default_Manifest_Transporter OR ?L_LM:Default_Manifest_Transporter{PROP:Req}
        TRA:TransporterName = L_LM:Default_Manifest_Transporter
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            L_LM:Default_Manifest_Transporter = TRA:TransporterName
            L_LM:Default_Manifest_Transporter_ID = TRA:TID
          ELSE
            CLEAR(L_LM:Default_Manifest_Transporter_ID)
            SELECT(?L_LM:Default_Manifest_Transporter)
            CYCLE
          END
        ELSE
          L_LM:Default_Manifest_Transporter_ID = TRA:TID
        END
      END
      ThisWindow.Reset()
    OF ?L_LM:Default_Service
      IF L_LM:Default_Service OR ?L_LM:Default_Service{PROP:Req}
        SERI:ServiceRequirement = L_LM:Default_Service
        IF Access:ServiceRequirements.TryFetch(SERI:Key_ServiceRequirement)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            L_LM:Default_Service = SERI:ServiceRequirement
            L_LM:Default_SID = SERI:SID
          ELSE
            CLEAR(L_LM:Default_SID)
            SELECT(?L_LM:Default_Service)
            CYCLE
          END
        ELSE
          L_LM:Default_SID = SERI:SID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_Def_Service
      ThisWindow.Update()
      SERI:ServiceRequirement = L_LM:Default_Service
      IF SELF.Run(4,SelectRecord) = RequestCompleted
        L_LM:Default_Service = SERI:ServiceRequirement
        L_LM:Default_SID = SERI:SID
      END
      ThisWindow.Reset(1)
    OF ?Button_Printer
      ThisWindow.Update()
          IF PRINTERDIALOG() = 1
             L_LM:Invoice2                = PRINTER{PROPPRINT:Device}
             L_LM:Invoice_Print_Option    = 1
          ELSE
          .
      
          DISPLAY
    OF ?Button_Printer:2
      ThisWindow.Update()
          IF PRINTERDIALOG() = 1
             L_LM:Delivery_Note2          = PRINTER{PROPPRINT:Device}
             L_LM:Invoice_Print_Option    = 1
          ELSE
          .
      
          DISPLAY
    OF ?Button_Printer:3
      ThisWindow.Update()
          IF PRINTERDIALOG() = 1
             L_LM:Credit_Note2            = PRINTER{PROPPRINT:Device}
             L_LM:Invoice_Print_Option    = 1
          ELSE
          .
      
          DISPLAY
    OF ?Button_Printer:4
      ThisWindow.Update()
          IF PRINTERDIALOG() = 1
             L_LM:Statement2              = PRINTER{PROPPRINT:Device}
             L_LM:Invoice_Print_Option    = 1
          ELSE
          .
      
          DISPLAY
    OF ?LookupFile
      ThisWindow.Update()
      L_INIG:Global_INI = FileLookup7.Ask(1)
      DISPLAY
    OF ?LookupFile:2
      ThisWindow.Update()
      L_INIG:Local_INI = FileLookup8.Ask(1)
      DISPLAY
    OF ?Button_EditUser
      ThisWindow.Update()
      Update_User_h(GLO:UID)
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    OF ?Cancel
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

