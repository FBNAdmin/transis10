

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS019.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Settings PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
History::SETI:Record LIKE(SETI:RECORD),THREAD
QuickWindow          WINDOW('Form Settings'),AT(,,358,122),FONT('MS Sans Serif',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateSettings'),SYSTEM
                       SHEET,AT(4,4,350,100),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Setting:'),AT(8,20),USE(?SETI:Setting:Prompt),TRN
                           ENTRY(@s100),AT(61,20,289,10),USE(SETI:Setting),COLOR(00E9E9E9h),READONLY,REQ,SKIP
                           PROMPT('Value:'),AT(8,34),USE(?SETI:Value:Prompt),TRN
                           ENTRY(@s100),AT(61,34,289,10),USE(SETI:Value)
                           PROMPT('Tip:'),AT(8,48),USE(?SETI:Tip:Prompt),TRN
                           TEXT,AT(61,48,289,24),USE(SETI:Tip),VSCROLL,BOXED,MSG('Tip of how to set this setting'),TIP('Tip of how' & |
  ' to set this setting')
                           PROMPT('Picture:'),AT(8,76),USE(?SETI:Picture:Prompt),TRN
                           ENTRY(@s20),AT(61,76,69,10),USE(SETI:Picture)
                           BUTTON('Apply'),AT(134,76,,10),USE(?Button_Apply)
                           BUTTON('Un-Apply'),AT(170,76,,10),USE(?Button_Apply:2)
                           PROMPT('SETI ID:'),AT(8,90),USE(?SETI:SETI_ID:Prompt),TRN
                           ENTRY(@n_10),AT(61,90,69,10),USE(SETI:SETI_ID),RIGHT(1),COLOR(00E9E9E9h),READONLY,REQ
                         END
                       END
                       BUTTON('&OK'),AT(250,106,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(304,106,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,106,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a User Group Accesses Record'
  OF InsertRecord
    ActionMessage = 'User Group Accesses Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'User Group Accesses Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Settings')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SETI:Setting:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Settings)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(SETI:Record,History::SETI:Record)
  SELF.AddHistoryField(?SETI:Setting,2)
  SELF.AddHistoryField(?SETI:Value,3)
  SELF.AddHistoryField(?SETI:Tip,5)
  SELF.AddHistoryField(?SETI:Picture,4)
  SELF.AddHistoryField(?SETI:SETI_ID,1)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Settings.Open                                     ! File Settings used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Settings
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?SETI:Setting{PROP:ReadOnly} = True
    ?SETI:Value{PROP:ReadOnly} = True
    ?SETI:Tip{PROP:ReadOnly} = True
    ?SETI:Picture{PROP:ReadOnly} = True
    DISABLE(?Button_Apply)
    DISABLE(?Button_Apply:2)
    ?SETI:SETI_ID{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Settings',QuickWindow)              ! Restore window settings from non-volatile store
      IF SELF.Request = InsertRecord
         ?SETI:Setting{PROP:ReadOnly}     = FALSE
         ?SETI:Setting{PROP:Skip}         = FALSE
         ?SETI:Setting{PROP:Background}   = -1
  
         MESSAGE('If you do not know exactly how and why to create Settings please cancel this insert and speak to support.', 'Inserting Settings', ICON:Asterisk)
      .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
      IF SELF.Request ~= InsertRecord AND CLIP(SETI:Picture) ~= ''
         ?SETI:Value{PROP:Text}   = CLIP(SETI:Picture)
      .
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Settings.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Settings',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Apply:2
      ThisWindow.Update()
          ?SETI:Value{PROP:Text}  = '@s150'
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

