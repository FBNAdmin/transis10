

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS018.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
MySettings PROCEDURE 

LOC:MySettings       GROUP,PRE(LO)                         ! 
StartReminders       BYTE                                  ! Start application with reminders On
ReminderPopups       BYTE                                  ! Popup reminders
                     END                                   ! 
CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Setup_Group      GROUP,PRE(L_SG)                       ! 
Branch               STRING(35)                            ! Branch Name
Address              STRING(35)                            ! Name of this address
DefaultRatesClient   STRING(35)                            ! 
DefaultRatesTransporter STRING(35)                         ! Transporters Name
                     END                                   ! 
LOC:Local_Machine_Settings GROUP,PRE(L_LM)                 ! 
Dot_Empty_Lines      USHORT                                ! Print a dot on empty lines below this line (0 is off)
BID                  ULONG                                 ! Branch ID
Printer_Ports        GROUP,PRE(L_LM)                       ! 
Invoice              STRING(4)                             ! 
Invoice2             STRING(255)                           ! 
Credit_Note          STRING(4)                             ! 
Credit_Note2         STRING(255)                           ! 
Delivery_Note        STRING(4)                             ! 
Delivery_Note2       STRING(255)                           ! 
Statement            STRING(4)                             ! 
Statement2           STRING(255)                           ! 
Invoice_Print_Option BYTE                                  ! 
                     END                                   ! 
Manifest_Group       GROUP,PRE(L_LM)                       ! 
Print_Manifest       BYTE                                  ! Prompt to print the manifest
Create_Invoices      BYTE                                  ! Prompt to create invoices
Print_Delivery_Notes BYTE                                  ! Prompt to print Delivery Notes
Print_Invoices       BYTE                                  ! Prompt to Print Invoices
State_Progression    BYTE                                  ! How the State of a Manifest is progressed
                     END                                   ! 
TripSheet_Group      GROUP,PRE(L_TS)                       ! 
TS_State_Progression BYTE                                  ! 
                     END                                   ! 
Debug_Group          GROUP,PRE(L_DG)                       ! 
Thread_Send_Proc_Events BYTE                               ! On or Off
                     END                                   ! 
Default_Manifest_Journey STRING(70)                        ! Description
Default_Manifest_JID ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
Default_Service      STRING(35)                            ! Service Requirement
Default_SID          ULONG                                 ! Service Requirement ID
Default_Manifest_Transporter STRING(35)                    ! Transporters Name
Default_Manifest_Transporter_ID ULONG                      ! Transporter ID
DI_Items_Action      BYTE                                  ! Action after capturing a new DI item
BusyHandling         BYTE                                  ! Database connection type setting
                     END                                   ! 
LOC:Global_Settings  GROUP,PRE(L_GS)                       ! This network - GLO INI
ReplicatedDatabaseID LONG                                  ! The ID of the Replicated Database - used to decide auto numbering
                     END                                   ! 
LOC:Ini_Group        GROUP,PRE(L_INIG)                     ! 
Global_INI           CSTRING('.\TransIS.INI<0>{241}')      ! All user ini file location (all users of this TransIS path)
Local_INI            CSTRING('TransISL.INI<0>{242}')       ! Local machine ini file location (no path will put it in Windows folder)
                     END                                   ! 
QuickWindow          WINDOW('My Settings'),AT(,,295,256),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY,MDI, |
  HLP('MySettings'),SYSTEM
                       SHEET,AT(4,16,287,219),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab2)
                           PROMPT('Branch:'),AT(14,34),USE(?L_SG:Branch:Prompt),TRN
                           BUTTON('...'),AT(85,34,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(101,34,121,10),USE(L_SG:Branch),MSG('Branch Name'),REQ,TIP('Branch Name')
                           PROMPT('Replicated Database ID:'),AT(14,55),USE(?ReplicatedDatabaseID:Prompt),TRN
                           ENTRY(@n_10),AT(101,55,87,10),USE(L_GS:ReplicatedDatabaseID),RIGHT(1),MSG('The ID of th' & |
  'e Replicated Database - used to decide auto numbering'),TIP('The ID of the Replicate' & |
  'd Database - used to decide auto numbering')
                           CHECK(' Start up with Reminders'),AT(101,71),USE(LO:StartReminders),MSG('Start applicat' & |
  'ion with reminders On'),TIP('Start application with reminders On'),TRN
                           CHECK(' Reminder Popups'),AT(101,87),USE(LO:ReminderPopups),MSG('Popup reminders'),TIP('Popup reminders'), |
  TRN
                           PROMPT('DI Items Action:'),AT(22,103),USE(?DI_Items_Action:Prompt),TRN
                           LIST,AT(101,103,87,10),USE(L_LM:DI_Items_Action),DROP(5),FROM('Ask Always|#0|General Ta' & |
  'b|#1|Charges Tab|#2'),MSG('Action after capturing a new DI item'),TIP('Action after ' & |
  'capturing a new DI item perform this action on returning to the DI screen')
                           GROUP('Manifest Prompts'),AT(17,127,266,58),USE(?Group_ManifestPrompts),BOXED,TRN
                             CHECK(' Print Manifest'),AT(22,140),USE(L_LM:Print_Manifest),MSG('Prompt to print the manifest'), |
  TIP('Prompt to print the manifest'),TRN
                             CHECK(' Create Invoices'),AT(22,151),USE(L_LM:Create_Invoices),MSG('Prompt to create invoices'), |
  TIP('Prompt to create invoices'),TRN
                             CHECK(' Print Delivery Notes'),AT(126,140),USE(L_LM:Print_Delivery_Notes),MSG('Prompt to ' & |
  'print Delivery Notes'),TIP('Prompt to print Delivery Notes'),TRN
                             CHECK(' Print Invoices'),AT(126,151),USE(L_LM:Print_Invoices),MSG('Print Invoices'),TIP('Prompt to ' & |
  'Print Invoices'),TRN
                             PROMPT('State Progression:'),AT(22,167),USE(?State_Progression:Prompt),TRN
                             LIST,AT(126,167,61,10),USE(L_LM:State_Progression),DROP(5),FROM('Manual|#0|Automatic|#1' & |
  '|Prompted|#2'),MSG('How the State of a Manifest is progressed'),TIP('How the State o' & |
  'f a Manifest is progressed')
                           END
                           GROUP('Trip Sheet Prompts'),AT(17,189,266,37),USE(?Group7),BOXED,TRN
                             PROMPT('State Progression:'),AT(22,205),USE(?L_LM:TS_State_Progression:Prompt),TRN
                             LIST,AT(126,205,61,10),USE(L_LM:TS_State_Progression),DROP(5),FROM('Manual|#0|Automatic' & |
  '|#1|Prompted|#2')
                           END
                         END
                         TAB('&2) Defaults'),USE(?Tab_Local_Defaults)
                           GROUP('Delivery && Manifest Defaults'),AT(14,55,253,52),USE(?Group4),BOXED,TRN
                             PROMPT('Journey:'),AT(21,68),USE(?Default_Manifest_Journey:Prompt),TRN
                             ENTRY(@s70),AT(122,68,139,10),USE(L_LM:Default_Manifest_Journey),MSG('Description'),TIP('Description')
                             BUTTON('...'),AT(105,68,12,10),USE(?CallLookup_Def_Man_Journey)
                             PROMPT('Transporter:'),AT(21,90),USE(?Default_Manifest_Transporter:Prompt),TRN
                             BUTTON('...'),AT(105,90,12,10),USE(?CallLookup_Def_Man_Transp)
                             ENTRY(@s35),AT(122,90,139,10),USE(L_LM:Default_Manifest_Transporter),MSG('Transporters Name'), |
  REQ,TIP('Transporters Name')
                           END
                           GROUP('Delivery Defaults'),AT(14,154,253,47),USE(?Group3),BOXED,TRN
                             PROMPT('Service Requirement:'),AT(21,167),USE(?L_LM:Default_Service:Prompt),TRN
                             ENTRY(@s35),AT(122,167,139,10),USE(L_LM:Default_Service),MSG('Service Requirement'),TIP('Service Requirement')
                             BUTTON('...'),AT(105,167,12,10),USE(?CallLookup_Def_Service)
                           END
                         END
                         TAB('&3) Printing'),USE(?Tab_Local_Printing)
                           OPTION('Invoice Print Option'),AT(70,52,47,150),USE(L_LM:Invoice_Print_Option),TRN
                             RADIO(' Default to Ports'),AT(206,52),USE(?Invoice_Print_Option:Radio1),TIP('Use the Po' & |
  'rt as the default'),TRN,VALUE('0')
                             RADIO(' Default to Devices'),AT(197,122),USE(?Invoice_Print_Option:Radio2),TIP('Use the De' & |
  'vice as the default'),TRN,VALUE('1')
                           END
                           GROUP('Printer Ports'),AT(14,61,266,49),USE(?Group_PrinterPort),BOXED,TRN
                             PROMPT('Invoice:'),AT(21,74),USE(?L_LM:Invoice_Printer_Port:Prompt),TRN
                             LIST,AT(70,74,40,10),USE(L_LM:Invoice),DROP(5),FROM('LPT1|#LPT1|LPT2|#LPT2|LPT3|#LPT3|LPT4|#LPT4')
                             PROMPT('Credit Note:'),AT(21,90),USE(?Credit_Note_Printer_Port:Prompt),TRN
                             LIST,AT(70,90,40,10),USE(L_LM:Credit_Note),DROP(5),FROM('LPT1|#LPT1|LPT2|#LPT2|LPT3|#LP' & |
  'T3|LPT4|#LPT4')
                             PROMPT('Delivery Note:'),AT(169,74),USE(?Delivery_Note_Printer_Port:Prompt),TRN
                             LIST,AT(226,74,40,10),USE(L_LM:Delivery_Note),DROP(5),FROM('LPT1|#LPT1|LPT2|#LPT2|LPT3|' & |
  '#LPT3|LPT4|#LPT4')
                             PROMPT('Statement:'),AT(169,90),USE(?Statement_Printer_Port:Prompt),TRN
                             LIST,AT(226,90,40,10),USE(L_LM:Statement),DROP(5),FROM('LPT1|#LPT1|LPT2|#LPT2|LPT3|#LPT' & |
  '3|LPT4|#LPT4')
                           END
                           GROUP('Printer Devices'),AT(14,130,266,74),USE(?Group_PrinterDevices),BOXED,TRN
                             PROMPT('Invoice:'),AT(21,143),USE(?Invoice2:Prompt),TRN
                             ENTRY(@s255),AT(133,143,121,10),USE(L_LM:Invoice2),COLOR(00E9E9E9h),READONLY
                             BUTTON('...'),AT(258,143,12,10),USE(?Button_Printer),TIP('Choose a printer')
                             PROMPT('Delivery Note:'),AT(21,159),USE(?Delivery_Note2:Prompt),TRN
                             ENTRY(@s255),AT(133,159,121,10),USE(L_LM:Delivery_Note2),COLOR(00E9E9E9h),READONLY,SKIP
                             BUTTON('...'),AT(258,159,12,10),USE(?Button_Printer:2),TIP('Choose a printer')
                             PROMPT('Credit Note:'),AT(21,173),USE(?Credit_Note2:Prompt),TRN
                             ENTRY(@s255),AT(133,173,121,10),USE(L_LM:Credit_Note2),COLOR(00E9E9E9h),READONLY,SKIP
                             BUTTON('...'),AT(258,173,12,10),USE(?Button_Printer:3),TIP('Choose a printer')
                             PROMPT('Statement:'),AT(21,186),USE(?Statement2:Prompt),TRN
                             ENTRY(@s255),AT(133,186,121,10),USE(L_LM:Statement2),COLOR(00E9E9E9h),READONLY,SKIP
                             BUTTON('...'),AT(258,186,12,10),USE(?Button_Printer:4),TIP('Choose a printer')
                           END
                           PROMPT('Dot Empty Lines:'),AT(14,215),USE(?Dot_Empty_Lines:Prompt),TRN
                           SPIN(@n6),AT(133,215,40,10),USE(L_LM:Dot_Empty_Lines),RIGHT(1),MSG('Print a dot on empt' & |
  'y lines below this line (0 is off)'),TIP('Print a dot on empty lines below this line' & |
  ' (0 is off)<0DH,0AH>Default is 45')
                         END
                         TAB('&4) Debug'),USE(?Tab3)
                           CHECK(' Thread Send Proc Events - Local'),AT(69,36),USE(L_LM:Thread_Send_Proc_Events),MSG('On or Of'), |
  TIP('On or Of'),TRN
                           PROMPT('Testing Mode:'),AT(10,98),USE(?GLO:Testing_Mode:Prompt),TRN
                           SPIN(@n3),AT(69,98,52,10),USE(GLO:Testing_Mode),RIGHT(1)
                           PROMPT('Busy Handling:'),AT(10,218),USE(?L_LM:BusyHandling:Prompt),TRN
                           LIST,AT(69,218,123,10),USE(L_LM:BusyHandling),DROP(5),FROM('Default|#0|Do Nothing|#1|Co' & |
  'nnection Per Thread|#2|Retry on Busy (default)|#3|Connection Locking|#4'),MSG('Database c' & |
  'onnection type setting'),TIP('Database connection type setting')
                         END
                         TAB('&5) INI Files'),USE(?Tab5)
                           GROUP('Locations (Folder / Directory)'),AT(9,36,276,68),USE(?Group6),BOXED,TRN
                           END
                           PROMPT('Global INI:'),AT(17,58),USE(?GLO:Global_INI:Prompt),TRN
                           BUTTON('...'),AT(58,58,12,10),USE(?LookupFile)
                           ENTRY(@s254),AT(77,58,201,10),USE(L_INIG:Global_INI),MSG('All user ini'),TIP('All user ini')
                           PROMPT('Local INI:'),AT(17,79),USE(?GLO:Local_INI:Prompt),TRN
                           BUTTON('...'),AT(58,79,12,10),USE(?LookupFile:2)
                           ENTRY(@s254),AT(77,79,201,10),USE(L_INIG:Local_INI),MSG('Local machine ini file location'), |
  TIP('Local machine ini file location')
                         END
                       END
                       BUTTON('Edit User'),AT(4,240,,14),USE(?Button_EditUser),LEFT,ICON(ICON:Child),FLAT
                       PROMPT('These are local settings to each machine.'),AT(11,4,273,10),USE(?Prompt10),FONT(,, |
  ,FONT:bold,CHARSET:ANSI),CENTER
                       BUTTON('&OK'),AT(188,240,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(242,240,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(122,240,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup7          SelectFileClass
FileLookup8          SelectFileClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MySettings')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?L_SG:Branch:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddItem(?OK,RequestCancelled)                       ! Add the cancel control to the window manager
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ServiceRequirements.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
      LO:StartReminders   = GETINI('MySettings', 'StartReminders-' & CLIP(GLO:Login), '1', GLO:Local_INI)
      LO:ReminderPopups   = GETINI('MySettings', 'ReminderPopups-' & CLIP(GLO:Login), '1', GLO:Local_INI)
  
  
      L_INIG:Local_INI    = GETINI('MySettings', 'Local_INI-' & CLIP(GLO:Login), GLO:Local_INI, GLO:Local_INI)
      L_INIG:Global_INI   = GETINI('MySettings', 'Global_INI-' & CLIP(GLO:Login), GLO:Global_INI, GLO:Local_INI)
      L_LM:Invoice                = GETINI('Printer_Ports', 'Invoice', 'LPT1', GLO:Local_INI)
      L_LM:Credit_Note            = GETINI('Printer_Ports', 'Credit_Note', 'LPT1', GLO:Local_INI)
      L_LM:Delivery_Note          = GETINI('Printer_Ports', 'Delivery_Note', 'LPT1', GLO:Local_INI)
      L_LM:Statement              = GETINI('Printer_Ports', 'Statement', 'LPT1', GLO:Local_INI)
  
      L_LM:Invoice2               = GETINI('Printer_Devices', 'Invoice', '', GLO:Local_INI)
      L_LM:Delivery_Note2         = GETINI('Printer_Devices', 'Delivery_Note', '', GLO:Local_INI)
      L_LM:Credit_Note2           = GETINI('Printer_Devices', 'Credit_Note', '', GLO:Local_INI)
      L_LM:Statement2             = GETINI('Printer_Devices', 'Statement', '', GLO:Local_INI)
  
      L_LM:Invoice_Print_Option   = GETINI('Printer_Devices', 'Invoice_Opt', '', GLO:Local_INI)
  
  
  
      L_LM:Print_Manifest         = GETINI('Manifest_Prompts', 'Print_Manifest', 0, GLO:Local_INI)
      L_LM:Create_Invoices        = GETINI('Manifest_Prompts', 'Create_Invoices', 0, GLO:Local_INI)
      L_LM:Print_Delivery_Notes   = GETINI('Manifest_Prompts', 'Print_Delivery_Notes', 0, GLO:Local_INI)
      L_LM:Print_Invoices         = GETINI('Manifest_Prompts', 'Print_Invoices', 0, GLO:Local_INI)
      L_LM:State_Progression      = GETINI('Manifest_Prompts', 'State_Progression', 2, GLO:Local_INI)
  
      L_LM:TS_State_Progression   = GETINI('TripSheet_Prompts', 'State_Progression', 2, GLO:Local_INI)
  
  
      L_LM:Thread_Send_Proc_Events    = GETINI('Setup', 'Thread_Send_Proc_Events', 1, GLO:Local_INI)
  
  
  
      L_LM:Default_Manifest_JID   = GETINI('Manifest', 'Default_Manifest_JID', 0, GLO:Local_INI)
      JOU:JID                             = L_LM:Default_Manifest_JID
      IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
         L_LM:Default_Manifest_Journey    = JOU:Journey
      .
  
  
      L_LM:Default_SID            = GETINI('Delivery', 'Default_DI_SID', 0, GLO:Local_INI)
      SERI:SID                    = L_LM:Default_SID
      IF Access:ServiceRequirements.TryFetch(SERI:PKey_SID) = LEVEL:Benign
         L_LM:Default_Service     = SERI:ServiceRequirement
      .
  
  
      L_LM:Default_Manifest_Transporter_ID    = GETINI('Manifest', 'Default_Manifest_TransID', 0, GLO:Local_INI)
      TRA:TID                     = L_LM:Default_Manifest_Transporter_ID
      IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
         L_LM:Default_Manifest_Transporter    = TRA:TransporterName
      .
  
  
      GLO:Testing_Mode            = GETINI('Setup', 'Testing_Mode', 0, GLO:Local_INI)
  
  
      L_LM:DI_Items_Action        = GETINI('Setup', 'DI_Items_Action', , GLO:Local_INI)
      L_LM:BID            = GETINI('Browse_Setup', 'Branch_ID', , GLO:Global_INI)
      BRA:BID             = L_LM:BID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         L_SG:Branch      = BRA:BranchName
      .
      L_GS:ReplicatedDatabaseID   = GETINI('Replication_Setup', 'ReplicatedDatabaseID', , GLO:Global_INI)
      L_LM:BusyHandling       = GETINI('Setup','BusyHandling','3','.\TransIS.INI')
  
      L_LM:Dot_Empty_Lines    = GETINI('Print_Cont', 'Dot_Empty_Lines', 45, GLO:Local_INI)
  FileLookup7.Init
  FileLookup7.ClearOnCancel = False
  FileLookup7.Flags=BOR(FileLookup7.Flags,FILE:LongName)   ! Allow long filenames
  FileLookup7.Flags=BOR(FileLookup7.Flags,FILE:Save)       ! Allow save Dialog
  FileLookup7.SetMask('All Files','*.*')                   ! Set the file mask
  FileLookup7.DefaultDirectory='TransIS.INI'
  FileLookup7.WindowTitle='Global INI'
  FileLookup8.Init
  FileLookup8.ClearOnCancel = False
  FileLookup8.Flags=BOR(FileLookup8.Flags,FILE:LongName)   ! Allow long filenames
  FileLookup8.Flags=BOR(FileLookup8.Flags,FILE:Save)       ! Allow save Dialog
  FileLookup8.SetMask('All Files','*.*')                   ! Set the file mask
  FileLookup8.DefaultDirectory='.\TransIS.INI'
  FileLookup8.WindowTitle='Local INI'
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Branches
      Select_Journey
      Select_Transporter
      Browse_ServiceRequirements
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
          PUTINI('MySettings', 'StartReminders-' & CLIP(GLO:Login), LO:StartReminders, GLO:Local_INI)
          PUTINI('MySettings', 'ReminderPopups-' & CLIP(GLO:Login), LO:ReminderPopups, GLO:Local_INI)
      
          PUTINI('MySettings', 'Local_INI-' & CLIP(GLO:Login), L_INIG:Local_INI, GLO:Local_INI)
          PUTINI('MySettings', 'Global_INI-' & CLIP(GLO:Login), L_INIG:Global_INI, GLO:Local_INI)
      
          PUTINI('MySettings', 'Local_INI-' & CLIP(GLO:Login), L_INIG:Local_INI, L_INIG:Local_INI)
          PUTINI('MySettings', 'Global_INI-' & CLIP(GLO:Login), L_INIG:Global_INI, L_INIG:Local_INI)
          PUTINI('Printer_Ports', 'Invoice', L_LM:Invoice, GLO:Local_INI)
          PUTINI('Printer_Ports', 'Credit_Note', L_LM:Credit_Note, GLO:Local_INI)
          PUTINI('Printer_Ports', 'Delivery_Note', L_LM:Delivery_Note, GLO:Local_INI)
          PUTINI('Printer_Ports', 'Statement', L_LM:Statement, GLO:Local_INI)
      
          PUTINI('Printer_Devices', 'Invoice', L_LM:Invoice2, GLO:Local_INI)
          PUTINI('Printer_Devices', 'Credit_Note', L_LM:Credit_Note2, GLO:Local_INI)
          PUTINI('Printer_Devices', 'Delivery_Note', L_LM:Delivery_Note2, GLO:Local_INI)
          PUTINI('Printer_Devices', 'Statement', L_LM:Statement2, GLO:Local_INI)
      
          PUTINI('Printer_Devices', 'Invoice_Opt', L_LM:Invoice_Print_Option, GLO:Local_INI)
      
          PUTINI('Manifest_Prompts', 'Print_Manifest', L_LM:Print_Manifest, GLO:Local_INI)
          PUTINI('Manifest_Prompts', 'Create_Invoices', L_LM:Create_Invoices, GLO:Local_INI)
          PUTINI('Manifest_Prompts', 'Print_Delivery_Notes', L_LM:Print_Delivery_Notes, GLO:Local_INI)
          PUTINI('Manifest_Prompts', 'Print_Invoices', L_LM:Print_Invoices, GLO:Local_INI)
          PUTINI('Manifest_Prompts', 'State_Progression', L_LM:State_Progression, GLO:Local_INI)
      
          PUTINI('TripSheet_Prompts', 'State_Progression', L_LM:TS_State_Progression, GLO:Local_INI)
      
          PUTINI('Setup', 'Thread_Send_Proc_Events', L_LM:Thread_Send_Proc_Events, GLO:Local_INI)
      
      
      
          PUTINI('Manifest', 'Default_Manifest_JID', L_LM:Default_Manifest_JID, GLO:Local_INI)
      
          PUTINI('Delivery', 'Default_DI_SID', L_LM:Default_SID, GLO:Local_INI)
      
      
      
          PUTINI('Manifest', 'Default_Manifest_TransID', L_LM:Default_Manifest_Transporter_ID, GLO:Local_INI)
      
          PUTINI('Setup', 'Testing_Mode', GLO:Testing_Mode, GLO:Local_INI)
      
          PUTINI('Setup', 'DI_Items_Action', L_LM:DI_Items_Action, GLO:Local_INI)
      
      
          PUTINI('Browse_Setup', 'Branch_ID', L_LM:BID, GLO:Global_INI)
      
      
      
          ! Global ini
          PUTINI('Replication_Setup', 'ReplicatedDatabaseID', L_GS:ReplicatedDatabaseID, GLO:Global_INI)
      
          GLO:ReplicatedDatabaseID    = L_GS:ReplicatedDatabaseID
          PUTINI('Setup','BusyHandling',L_LM:BusyHandling,'.\TransIS.INI')
      
          PUTINI('Print_Cont', 'Dot_Empty_Lines', L_LM:Dot_Empty_Lines, GLO:Local_INI)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      BRA:BranchName = L_SG:Branch
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SG:Branch = BRA:BranchName
        L_LM:BID = BRA:BID
      END
      ThisWindow.Reset(1)
    OF ?L_SG:Branch
      IF L_SG:Branch OR ?L_SG:Branch{PROP:Req}
        BRA:BranchName = L_SG:Branch
        IF Access:Branches.TryFetch(BRA:Key_BranchName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SG:Branch = BRA:BranchName
            L_LM:BID = BRA:BID
          ELSE
            CLEAR(L_LM:BID)
            SELECT(?L_SG:Branch)
            CYCLE
          END
        ELSE
          L_LM:BID = BRA:BID
        END
      END
      ThisWindow.Reset()
    OF ?L_LM:Default_Manifest_Journey
      IF L_LM:Default_Manifest_Journey OR ?L_LM:Default_Manifest_Journey{PROP:Req}
        JOU:Journey = L_LM:Default_Manifest_Journey
        IF Access:Journeys.TryFetch(JOU:Key_Journey)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            L_LM:Default_Manifest_Journey = JOU:Journey
            L_LM:Default_Manifest_JID = JOU:JID
          ELSE
            CLEAR(L_LM:Default_Manifest_JID)
            SELECT(?L_LM:Default_Manifest_Journey)
            CYCLE
          END
        ELSE
          L_LM:Default_Manifest_JID = JOU:JID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_Def_Man_Journey
      ThisWindow.Update()
      JOU:Journey = L_LM:Default_Manifest_Journey
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_LM:Default_Manifest_Journey = JOU:Journey
        L_LM:Default_Manifest_JID = JOU:JID
      END
      ThisWindow.Reset(1)
    OF ?CallLookup_Def_Man_Transp
      ThisWindow.Update()
      TRA:TransporterName = L_LM:Default_Manifest_Transporter
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        L_LM:Default_Manifest_Transporter = TRA:TransporterName
        L_LM:Default_Manifest_Transporter_ID = TRA:TID
      END
      ThisWindow.Reset(1)
    OF ?L_LM:Default_Manifest_Transporter
      IF L_LM:Default_Manifest_Transporter OR ?L_LM:Default_Manifest_Transporter{PROP:Req}
        TRA:TransporterName = L_LM:Default_Manifest_Transporter
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            L_LM:Default_Manifest_Transporter = TRA:TransporterName
            L_LM:Default_Manifest_Transporter_ID = TRA:TID
          ELSE
            CLEAR(L_LM:Default_Manifest_Transporter_ID)
            SELECT(?L_LM:Default_Manifest_Transporter)
            CYCLE
          END
        ELSE
          L_LM:Default_Manifest_Transporter_ID = TRA:TID
        END
      END
      ThisWindow.Reset()
    OF ?L_LM:Default_Service
      IF L_LM:Default_Service OR ?L_LM:Default_Service{PROP:Req}
        SERI:ServiceRequirement = L_LM:Default_Service
        IF Access:ServiceRequirements.TryFetch(SERI:Key_ServiceRequirement)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            L_LM:Default_Service = SERI:ServiceRequirement
            L_LM:Default_SID = SERI:SID
          ELSE
            CLEAR(L_LM:Default_SID)
            SELECT(?L_LM:Default_Service)
            CYCLE
          END
        ELSE
          L_LM:Default_SID = SERI:SID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_Def_Service
      ThisWindow.Update()
      SERI:ServiceRequirement = L_LM:Default_Service
      IF SELF.Run(4,SelectRecord) = RequestCompleted
        L_LM:Default_Service = SERI:ServiceRequirement
        L_LM:Default_SID = SERI:SID
      END
      ThisWindow.Reset(1)
    OF ?Button_Printer
      ThisWindow.Update()
          IF PRINTERDIALOG() = 1
             L_LM:Invoice2                = PRINTER{PROPPRINT:Device}
             L_LM:Invoice_Print_Option    = 1
          ELSE
          .
      
          DISPLAY
    OF ?Button_Printer:2
      ThisWindow.Update()
          IF PRINTERDIALOG() = 1
             L_LM:Delivery_Note2          = PRINTER{PROPPRINT:Device}
             L_LM:Invoice_Print_Option    = 1
          ELSE
          .
      
          DISPLAY
    OF ?Button_Printer:3
      ThisWindow.Update()
          IF PRINTERDIALOG() = 1
             L_LM:Credit_Note2            = PRINTER{PROPPRINT:Device}
             L_LM:Invoice_Print_Option    = 1
          ELSE
          .
      
          DISPLAY
    OF ?Button_Printer:4
      ThisWindow.Update()
          IF PRINTERDIALOG() = 1
             L_LM:Statement2              = PRINTER{PROPPRINT:Device}
             L_LM:Invoice_Print_Option    = 1
          ELSE
          .
      
          DISPLAY
    OF ?LookupFile
      ThisWindow.Update()
      L_INIG:Global_INI = FileLookup7.Ask(1)
      DISPLAY
    OF ?LookupFile:2
      ThisWindow.Update()
      L_INIG:Local_INI = FileLookup8.Ask(1)
      DISPLAY
    OF ?Button_EditUser
      ThisWindow.Update()
      Update_User_h(GLO:UID)
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    OF ?Cancel
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

