

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS006.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_UserGroupAccesses_ApplicationSectionExtra PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:GroupName        STRING(35)                            ! Name of this group of users
LOC:ApplicationSection STRING(35)                          ! Application Section Name
LOC:ExtraSection     STRING(100)                           ! 
FDB7::View:FileDrop  VIEW(UserGroups)
                       PROJECT(USEG:GroupName)
                       PROJECT(USEG:UGID)
                     END
FDB8::View:FileDrop  VIEW(ApplicationSections)
                       PROJECT(APP:ApplicationSection)
                       PROJECT(APP:ASID)
                     END
FDB9::View:FileDrop  VIEW(ApplicationSections_Extras)
                       PROJECT(APPSE:ExtraSection)
                       PROJECT(APPSE:ASEID)
                     END
Queue:FileDrop       QUEUE                            !
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !
APP:ApplicationSection LIKE(APP:ApplicationSection)   !List box control field - type derived from field
APP:ASID               LIKE(APP:ASID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:2     QUEUE                            !
APPSE:ExtraSection     LIKE(APPSE:ExtraSection)       !List box control field - type derived from field
APPSE:ASEID            LIKE(APPSE:ASEID)              !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::UGACE:Record LIKE(UGACE:RECORD),THREAD
QuickWindow          WINDOW('Form User Group Accesses Extra'),AT(,,208,147),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Update_UserGroupAccesses_ApplicationSectionExtra'),SYSTEM
                       SHEET,AT(4,4,201,124),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('User Group:'),AT(9,24),USE(?Prompt2),TRN
                           LIST,AT(98,42,100,10),USE(LOC:ApplicationSection),VSCROLL,DROP(15),FORMAT('140L(2)|M~Ap' & |
  'plication Section~@s35@'),FROM(Queue:FileDrop:1)
                           PROMPT('Application Section:'),AT(9,42),USE(?Prompt2:2),TRN
                           LIST,AT(98,60,100,10),USE(LOC:ExtraSection),VSCROLL,DROP(15),FORMAT('400L(2)|M~Extra Se' & |
  'ction~@s100@'),FROM(Queue:FileDrop:2)
                           PROMPT('Application Section Extras:'),AT(9,60),USE(?Prompt2:3),TRN
                           LIST,AT(98,24,100,10),USE(LOC:GroupName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Group Name~@s35@'), |
  FROM(Queue:FileDrop)
                           PROMPT('Access Option:'),AT(9,114),USE(?UGACE:AccessOption:Prompt),TRN
                           LIST,AT(101,114,100,10),USE(UGACE:AccessOption),DROP(5),FROM('Allow|#0|View Only|#1|No Access|#2'), |
  MSG('Option'),TIP('Option')
                         END
                       END
                       BUTTON('&OK'),AT(102,130,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(156,130,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,130,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB8                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

FDB9                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:2              !Reference to display queue
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a User Group Accesses Record'
  OF InsertRecord
    ActionMessage = 'User Group Accesses Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'User Group Accesses Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_UserGroupAccesses_ApplicationSectionExtra')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:UserGroupAccessesExtra)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(UGACE:Record,History::UGACE:Record)
  SELF.AddHistoryField(?UGACE:AccessOption,6)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ApplicationSections.SetOpenRelated()
  Relate:ApplicationSections.Open                          ! File ApplicationSections used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UserGroupAccessesExtra
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?LOC:ApplicationSection)
    DISABLE(?LOC:ExtraSection)
    DISABLE(?LOC:GroupName)
    DISABLE(?UGACE:AccessOption)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_UserGroupAccesses_ApplicationSectionExtra',QuickWindow) ! Restore window settings from non-volatile store
      USEG:UGID   = UGACE:UGID
      IF Access:UserGroups.TryFetch(USEG:PKey_UGID) = LEVEL:Benign
         LOC:GroupName            = USEG:GroupName
      .
  
  
      APP:ASID    = UGACE:ASID
      IF Access:ApplicationSections.TryFetch(APP:PKey_ASID) = LEVEL:Benign
         LOC:ApplicationSection   = APP:ApplicationSection
      .
  
      APPSE:ASEID = UGACE:ASEID
      IF Access:ApplicationSections_Extras.TryFetch(APPSE:PKey_ASEID) = LEVEL:Benign
         LOC:ExtraSection         = APPSE:ExtraSection
      .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  FDB7.Init(?LOC:GroupName,Queue:FileDrop.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop,Relate:UserGroups,ThisWindow)
  FDB7.Q &= Queue:FileDrop
  FDB7.AddSortOrder(USEG:Key_GroupName)
  FDB7.AddField(USEG:GroupName,FDB7.Q.USEG:GroupName) !List box control field - type derived from field
  FDB7.AddField(USEG:UGID,FDB7.Q.USEG:UGID) !Primary key field - type derived from field
  FDB7.AddUpdateField(USEG:UGID,UGACE:UGID)
  ThisWindow.AddItem(FDB7.WindowComponent)
  FDB7.DefaultFill = 0
  FDB8.Init(?LOC:ApplicationSection,Queue:FileDrop:1.ViewPosition,FDB8::View:FileDrop,Queue:FileDrop:1,Relate:ApplicationSections,ThisWindow)
  FDB8.Q &= Queue:FileDrop:1
  FDB8.AddSortOrder(APP:Key_ApplicationSection)
  FDB8.AddField(APP:ApplicationSection,FDB8.Q.APP:ApplicationSection) !List box control field - type derived from field
  FDB8.AddField(APP:ASID,FDB8.Q.APP:ASID) !Primary key field - type derived from field
  FDB8.AddUpdateField(APP:ASID,UGACE:ASID)
  ThisWindow.AddItem(FDB8.WindowComponent)
  FDB8.DefaultFill = 0
  FDB9.Init(?LOC:ExtraSection,Queue:FileDrop:2.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop:2,Relate:ApplicationSections_Extras,ThisWindow)
  FDB9.Q &= Queue:FileDrop:2
  FDB9.AddSortOrder(APPSE:SKey_ASID_ExtraSection)
  FDB9.AddRange(APPSE:ASID,UGACE:ASID)
  FDB9.AddField(APPSE:ExtraSection,FDB9.Q.APPSE:ExtraSection) !List box control field - type derived from field
  FDB9.AddField(APPSE:ASEID,FDB9.Q.APPSE:ASEID) !Primary key field - type derived from field
  FDB9.AddUpdateField(APPSE:ASEID,UGACE:ASEID)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_UserGroupAccesses_ApplicationSectionExtra',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

