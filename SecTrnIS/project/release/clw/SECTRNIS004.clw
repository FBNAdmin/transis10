

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS004.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_ApplicationSections_Extra PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:AccessOption     STRING(20)                            ! Option - Allow, View Only, No Access
BRW2::View:Browse    VIEW(UsersAccessesExtra)
                       PROJECT(USACE:AccessOption)
                       PROJECT(USACE:UAEID)
                       PROJECT(USACE:ASEID)
                       PROJECT(USACE:UID)
                       JOIN(USE:PKey_UID,USACE:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:UID)
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
LOC:AccessOption       LIKE(LOC:AccessOption)         !List box control field - type derived from local data
USACE:AccessOption     LIKE(USACE:AccessOption)       !Browse hot field - type derived from field
USACE:UAEID            LIKE(USACE:UAEID)              !Primary key field - type derived from field
USACE:ASEID            LIKE(USACE:ASEID)              !Browse key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::APPSE:Record LIKE(APPSE:RECORD),THREAD
QuickWindow          WINDOW('Form Application Sections Extras'),AT(,,279,138),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Update_ApplicationSections_Extra'),SYSTEM
                       SHEET,AT(4,4,271,112),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Extra Section:'),AT(9,26),USE(?APPSE:ExtraSection:Prompt),TRN
                           ENTRY(@s100),AT(73,26,197,10),USE(APPSE:ExtraSection)
                           PROMPT('Default Action:'),AT(9,40),USE(?APPSE:DefaultAction:Prompt),TRN
                           LIST,AT(73,40,100,10),USE(APPSE:DefaultAction),DROP(5),FROM('Allow|#0|View Only|#1|No Access|#2'), |
  MSG('Default action for security is'),TIP('Default action for security is')
                         END
                         TAB('&2) Users Accesses Extra'),USE(?Tab:2)
                           LIST,AT(9,20,263,74),USE(?Browse:2),HVSCROLL,FORMAT('80L(2)|M~Login~C(0)@s20@80L(2)|M~A' & |
  'ccess Option~C(0)@s20@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the UsersAccessesExtra file')
                           BUTTON('&Insert'),AT(117,98,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(169,98,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(223,98,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(174,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(226,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Application Sections Extra Record'
  OF InsertRecord
    ActionMessage = 'App. Sections Extra Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'App. Sections Extra Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ApplicationSections_Extra')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?APPSE:ExtraSection:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:AccessOption',LOC:AccessOption)                ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:ApplicationSections_Extras)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(APPSE:Record,History::APPSE:Record)
  SELF.AddHistoryField(?APPSE:ExtraSection,3)
  SELF.AddHistoryField(?APPSE:DefaultAction,4)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ApplicationSections_Extras.SetOpenRelated()
  Relate:ApplicationSections_Extras.Open                   ! File ApplicationSections_Extras used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ApplicationSections_Extras
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:UsersAccessesExtra,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?APPSE:ExtraSection{PROP:ReadOnly} = True
    DISABLE(?APPSE:DefaultAction)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,USACE:FKey_ASEID)                     ! Add the sort order for USACE:FKey_ASEID for sort order 1
  BRW2.AddRange(USACE:ASEID,Relate:UsersAccessesExtra,Relate:ApplicationSections_Extras) ! Add file relationship range limit for sort order 1
  BRW2.AddField(USE:Login,BRW2.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW2.AddField(LOC:AccessOption,BRW2.Q.LOC:AccessOption)  ! Field LOC:AccessOption is a hot field or requires assignment from browse
  BRW2.AddField(USACE:AccessOption,BRW2.Q.USACE:AccessOption) ! Field USACE:AccessOption is a hot field or requires assignment from browse
  BRW2.AddField(USACE:UAEID,BRW2.Q.USACE:UAEID)            ! Field USACE:UAEID is a hot field or requires assignment from browse
  BRW2.AddField(USACE:ASEID,BRW2.Q.USACE:ASEID)            ! Field USACE:ASEID is a hot field or requires assignment from browse
  BRW2.AddField(USE:UID,BRW2.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_ApplicationSections_Extra',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1                                    ! Will call: Update_UsersAccesses_Extra
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections_Extras.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_ApplicationSections_Extra',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_UsersAccesses_Extra
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.SetQueueRecord PROCEDURE

  CODE
      ! Allow|View Only|No Access
      EXECUTE USACE:AccessOption + 1
         LOC:AccessOption = 'Allow'
         LOC:AccessOption = 'View Only'
         LOC:AccessOption = 'No Access'
      .
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

