

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module

                     MAP
                       INCLUDE('SECTRNIS020.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Update_User_h        PROCEDURE  (p:UID)                    ! Declare Procedure
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Users.Open()
     .
     Access:Users.UseFile()
    USE:UID = p:UID
    IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
       GlobalRequest    = ChangeRecord
       Update_Users()
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:Users.Close()
    Exit
