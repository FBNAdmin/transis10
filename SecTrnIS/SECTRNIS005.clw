

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS005.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Users_Accesses PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:ApplicationSection STRING(35)                          !Application Section Name
LOC:DefaultAction    STRING(20)                            !Default action for security is - Allow, View Only, No Access
LOC:AccessOption     STRING(20)                            !Option - Allow, View Only, No Access
LOC:Login            STRING(20)                            !User Login
BRW8::View:Browse    VIEW(UsersAccessesExtra)
                       PROJECT(USACE:AccessOption)
                       PROJECT(USACE:UAEID)
                       PROJECT(USACE:UID)
                       PROJECT(USACE:ASEID)
                       JOIN(APPSE:PKey_ASEID,USACE:ASEID)
                         PROJECT(APPSE:ExtraSection)
                         PROJECT(APPSE:DefaultAction)
                         PROJECT(APPSE:ASEID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
APPSE:ExtraSection     LIKE(APPSE:ExtraSection)       !List box control field - type derived from field
LOC:DefaultAction      LIKE(LOC:DefaultAction)        !List box control field - type derived from local data
LOC:AccessOption       LIKE(LOC:AccessOption)         !List box control field - type derived from local data
USACE:AccessOption     LIKE(USACE:AccessOption)       !Browse hot field - type derived from field
APPSE:DefaultAction    LIKE(APPSE:DefaultAction)      !Browse hot field - type derived from field
USACE:UAEID            LIKE(USACE:UAEID)              !Primary key field - type derived from field
USACE:UID              LIKE(USACE:UID)                !Browse key field - type derived from field
APPSE:ASEID            LIKE(APPSE:ASEID)              !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::USAC:Record LIKE(USAC:RECORD),THREAD
QuickWindow          WINDOW('Form Users Accesses'),AT(,,188,203),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('Users_Accesses'),SYSTEM
                       SHEET,AT(4,4,180,183),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('User Login:'),AT(9,22),USE(?LOC:Login:Prompt),FONT('Tahoma'),TRN
                           BUTTON('...'),AT(62,22,12,10),USE(?CallLookup_UserLogin)
                           ENTRY(@s20),AT(78,22,100,10),USE(LOC:Login),MSG('User Login'),REQ,TIP('User Login')
                           PROMPT('App. Section:'),AT(9,40),USE(?LOC:ApplicationSection:Prompt),FONT('Tahoma'),TRN
                           BUTTON('...'),AT(62,40,12,10),USE(?CallLookup)
                           ENTRY(@s35),AT(78,40,100,10),USE(LOC:ApplicationSection),MSG('Application Section name'),TIP('Applicatio' & |
  'n Section name')
                           PROMPT('Access Option:'),AT(9,56),USE(?USAC:Option:Prompt),FONT('Tahoma'),TRN
                           LIST,AT(78,56,100,10),USE(USAC:AccessOption),DROP(5),FROM('Allow|#0|View Only|#1|No Access|#2'), |
  MSG('Option'),TIP('Option')
                           LINE,AT(7,71,173,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           LIST,AT(9,76,171,92),USE(?List),VSCROLL,FORMAT('60L(2)|M~Extra Section~@s100@52L(2)|M~D' & |
  'efault Action~@s20@52L(2)|M~Access Option~@s20@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(30,171,49,12),USE(?Insert),LEFT,ICON('wainsert.ico'),FLAT,SKIP
                           BUTTON('&Change'),AT(81,171,49,12),USE(?Change),LEFT,ICON('wachange.ico'),FLAT
                           BUTTON('&Delete'),AT(130,171,49,12),USE(?Delete),LEFT,ICON('wadelete.ico'),FLAT
                         END
                       END
                       BUTTON('&OK'),AT(82,188,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(134,188,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,188,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW8                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a User Accesses Record'
  OF InsertRecord
    ActionMessage = 'User Accesses Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'User Accesses Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Users_Accesses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Login:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:DefaultAction',LOC:DefaultAction)              ! Added by: BrowseBox(ABC)
  BIND('LOC:AccessOption',LOC:AccessOption)                ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:UsersAccesses)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(USAC:Record,History::USAC:Record)
  SELF.AddHistoryField(?USAC:AccessOption,4)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ApplicationSections.SetOpenRelated()
  Relate:ApplicationSections.Open                          ! File ApplicationSections used by this procedure, so make sure it's RelationManager is open
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UsersAccesses
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:UsersAccessesExtra,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup_UserLogin)
    ?LOC:Login{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?LOC:ApplicationSection{PROP:ReadOnly} = True
    DISABLE(?USAC:AccessOption)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW8.Q &= Queue:Browse
  BRW8.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW8.AddSortOrder(,USACE:FKey_UID)                       ! Add the sort order for USACE:FKey_UID for sort order 1
  BRW8.AddRange(USACE:UID,USAC:UID)                        ! Add single value range limit for sort order 1
  BRW8.AddLocator(BRW8::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW8::Sort0:Locator.Init(,USACE:UID,1,BRW8)              ! Initialize the browse locator using  using key: USACE:FKey_UID , USACE:UID
  BRW8.AppendOrder('+USACE:UAEID,+APPSE:ASEID')            ! Append an additional sort order
  BRW8.SetFilter('(USACE:ASID = USAC:ASID)')               ! Apply filter expression to browse
  BRW8.AddField(APPSE:ExtraSection,BRW8.Q.APPSE:ExtraSection) ! Field APPSE:ExtraSection is a hot field or requires assignment from browse
  BRW8.AddField(LOC:DefaultAction,BRW8.Q.LOC:DefaultAction) ! Field LOC:DefaultAction is a hot field or requires assignment from browse
  BRW8.AddField(LOC:AccessOption,BRW8.Q.LOC:AccessOption)  ! Field LOC:AccessOption is a hot field or requires assignment from browse
  BRW8.AddField(USACE:AccessOption,BRW8.Q.USACE:AccessOption) ! Field USACE:AccessOption is a hot field or requires assignment from browse
  BRW8.AddField(APPSE:DefaultAction,BRW8.Q.APPSE:DefaultAction) ! Field APPSE:DefaultAction is a hot field or requires assignment from browse
  BRW8.AddField(USACE:UAEID,BRW8.Q.USACE:UAEID)            ! Field USACE:UAEID is a hot field or requires assignment from browse
  BRW8.AddField(USACE:UID,BRW8.Q.USACE:UID)                ! Field USACE:UID is a hot field or requires assignment from browse
  BRW8.AddField(APPSE:ASEID,BRW8.Q.APPSE:ASEID)            ! Field APPSE:ASEID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_Users_Accesses',QuickWindow)        ! Restore window settings from non-volatile store
      USE:UID = USAC:UID
      IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
         LOC:Login    = USE:Login
      .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW8.AskProcedure = 3
  BRW8.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW8.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ApplicationSections.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Users_Accesses',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_Users
      Browse_Application_Sections
      Update_UsersAccesses_Extra
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup_UserLogin
      ThisWindow.Update()
      USE:Login = LOC:Login
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:Login = USE:Login
        USAC:UID = USE:UID
      END
      ThisWindow.Reset(1)
    OF ?LOC:Login
      IF LOC:Login OR ?LOC:Login{PROP:Req}
        USE:Login = LOC:Login
        IF Access:Users.TryFetch(USE:Key_Login)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:Login = USE:Login
            USAC:UID = USE:UID
          ELSE
            CLEAR(USAC:UID)
            SELECT(?LOC:Login)
            CYCLE
          END
        ELSE
          USAC:UID = USE:UID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      APP:ApplicationSection = LOC:ApplicationSection
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LOC:ApplicationSection = APP:ApplicationSection
        USAC:ASID = APP:ASID
      END
      ThisWindow.Reset(1)
    OF ?LOC:ApplicationSection
      IF LOC:ApplicationSection OR ?LOC:ApplicationSection{PROP:Req}
        APP:ApplicationSection = LOC:ApplicationSection
        IF Access:ApplicationSections.TryFetch(APP:Key_ApplicationSection)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            LOC:ApplicationSection = APP:ApplicationSection
            USAC:ASID = APP:ASID
          ELSE
            CLEAR(USAC:ASID)
            SELECT(?LOC:ApplicationSection)
            CYCLE
          END
        ELSE
          USAC:ASID = APP:ASID
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF USAC:ASID ~= 0
             APP:ASID = USAC:ASID
             IF Access:ApplicationSections.TryFetch(APP:PKey_ASID) = LEVEL:Benign
                LOC:ApplicationSection    = APP:ApplicationSection
          .  .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.SetQueueRecord PROCEDURE

  CODE
      ! Allow|View Only|No Access
      EXECUTE USACE:AccessOption + 1
         LOC:AccessOption     = 'Allow'
         LOC:AccessOption     = 'View Only'
         LOC:AccessOption     = 'No Access'
      .
  
  
  
      ! Allow|View Only|No Access
      EXECUTE APPSE:DefaultAction + 1
         LOC:DefaultAction    = 'Allow'
         LOC:DefaultAction    = 'View Only'
         LOC:DefaultAction    = 'No Access'
      .
  PARENT.SetQueueRecord
  

