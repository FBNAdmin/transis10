

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS016.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_UserGroups PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW5::View:Browse    VIEW(UsersGroups)
                       PROJECT(USEBG:UID)
                       PROJECT(USEBG:UBGID)
                       PROJECT(USEBG:UGID)
                       JOIN(USE:PKey_UID,USEBG:UID)
                         PROJECT(USE:Login)
                         PROJECT(USE:UID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
USEBG:UID              LIKE(USEBG:UID)                !List box control field - type derived from field
USEBG:UBGID            LIKE(USEBG:UBGID)              !List box control field - type derived from field
USEBG:UGID             LIKE(USEBG:UGID)               !Browse key field - type derived from field
USE:UID                LIKE(USE:UID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::USEG:Record LIKE(USEG:RECORD),THREAD
QuickWindow          WINDOW('Form User Groups'),AT(,,213,166),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('UpdateUserGroups'),SYSTEM
                       SHEET,AT(4,4,205,142),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('UGID:'),AT(9,36),USE(?USEG:UGID:Prompt),TRN
                           STRING(@n_10),AT(101,36,104,10),USE(USEG:UGID),RIGHT(1),TRN
                           PROMPT('Group Name:'),AT(9,22),USE(?USEG:GroupName:Prompt),TRN
                           ENTRY(@s35),AT(61,22,144,10),USE(USEG:GroupName),MSG('Name of this group of users'),REQ,TIP('Name of th' & |
  'is group of users')
                         END
                         TAB('&2) Users in Group'),USE(?Tab2)
                           LIST,AT(9,20,197,106),USE(?List),FORMAT('100L(2)|M~Login~@s20@40R(2)|M~UID~L@n_10@40R(2' & |
  ')|M~UBGID~L@n_10@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(77,130,42,12),USE(?Insert)
                           BUTTON('&Change'),AT(121,130,42,12),USE(?Change)
                           BUTTON('&Delete'),AT(165,130,42,12),USE(?Delete)
                         END
                       END
                       BUTTON('&OK'),AT(108,148,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(160,148,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,148,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW5                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW5::Sort0:Locator  StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a User Groups Record'
  OF InsertRecord
    ActionMessage = 'User Groups Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'User Groups Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_UserGroups')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?USEG:UGID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:UserGroups)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(USEG:Record,History::USEG:Record)
  SELF.AddHistoryField(?USEG:UGID,1)
  SELF.AddHistoryField(?USEG:GroupName,2)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:UserGroups.Open                                   ! File UserGroups used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UserGroups
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:UsersGroups,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?USEG:GroupName{PROP:ReadOnly} = True
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,USEBG:FKey_UGID)                      ! Add the sort order for USEBG:FKey_UGID for sort order 1
  BRW5.AddRange(USEBG:UGID,USEG:UGID)                      ! Add single value range limit for sort order 1
  BRW5.AddLocator(BRW5::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW5::Sort0:Locator.Init(,USEBG:UGID,1,BRW5)             ! Initialize the browse locator using  using key: USEBG:FKey_UGID , USEBG:UGID
  BRW5.AppendOrder('+USE:Login,+USEBG:UBGID')              ! Append an additional sort order
  BRW5.AddField(USE:Login,BRW5.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW5.AddField(USEBG:UID,BRW5.Q.USEBG:UID)                ! Field USEBG:UID is a hot field or requires assignment from browse
  BRW5.AddField(USEBG:UBGID,BRW5.Q.USEBG:UBGID)            ! Field USEBG:UBGID is a hot field or requires assignment from browse
  BRW5.AddField(USEBG:UGID,BRW5.Q.USEBG:UGID)              ! Field USEBG:UGID is a hot field or requires assignment from browse
  BRW5.AddField(USE:UID,BRW5.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_UserGroups',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW5.AskProcedure = 1
  BRW5.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW5.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:UserGroups.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_UserGroups',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_UsersGroups
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END

