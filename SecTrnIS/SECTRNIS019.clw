

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS019.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
UserLogin PROCEDURE 

LOC:Group            GROUP,PRE(L_G)                        !
Login                STRING(20)                            !User Login
Password             STRING(35)                            !User Password
Remember_Login       BYTE                                  !Remember my login on this computer
                     END                                   !
QuickWindow          WINDOW('Login'),AT(,,260,120),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY,IMM,MODAL, |
  HLP('Login')
                       SHEET,AT(4,4,251,97),USE(?Sheet1)
                         TAB('User Login'),USE(?Tab1)
                           PROMPT('Login:'),AT(10,28),USE(?Login:Prompt),FONT(,10,,,CHARSET:ANSI),TRN
                           ENTRY(@s20),AT(70,28),USE(L_G:Login),FONT(,10,,,CHARSET:ANSI),MSG('User Login'),REQ,TIP('User Login')
                           BUTTON('...'),AT(53,28,12,14),USE(?CallLookup)
                           PROMPT('Password:'),AT(10,48,,12),USE(?Password:Prompt),FONT(,10,,,CHARSET:ANSI),TRN
                           ENTRY(@s35),AT(70,48),USE(L_G:Password),FONT(,10,,,CHARSET:ANSI),MSG('User Password'),PASSWORD, |
  TIP('User Password')
                           CHECK(' Remember Login'),AT(70,78),USE(L_G:Remember_Login),FONT(,10,,,CHARSET:ANSI),HIDE,MSG('Remember m' & |
  'y login on this computer'),TIP('Remember my login on this computer'),TRN
                         END
                       END
                       BUTTON('&Cancel'),AT(206,104,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(4,104,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('&OK'),AT(154,104,49,14),USE(?Ok:2),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UserLogin')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Login:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Control.Open                                      ! File Control used by this procedure, so make sure it's RelationManager is open
  Relate:Users.SetOpenRelated()
  Relate:Users.Open                                        ! File Users used by this procedure, so make sure it's RelationManager is open
  Access:UsersGroups.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
      L_G:Login           = GETINI('Login', 'LastLogin', '', 'C:\TransIS.INI')
      L_G:Remember_Login  = GETINI('Login', 'RememberPassword_New', 0, 'C:\TransIS.INI')
  
      IF L_G:Remember_Login > 0
         SET(Control)
         NEXT(Control)
         IF ~ERRORCODE()
            L_G:Password  = CTR:RememberPassword
      .  .
  
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Control.Close
    Relate:Users.Close
  END
      POST(EVENT:User,, 1)
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Select_Users_NonMDI
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_G:Login
      IF L_G:Login OR ?L_G:Login{PROP:Req}
        USE:Login = L_G:Login
        IF Access:Users.TryFetch(USE:Key_Login)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_G:Login = USE:Login
          ELSE
            SELECT(?L_G:Login)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      USE:Login = L_G:Login
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_G:Login = USE:Login
      END
      ThisWindow.Reset(1)
    OF ?Cancel
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    OF ?Ok:2
      ThisWindow.Update()
          ! Check password
          IF CLIP(L_G:Login) = ''
             MESSAGE('Please supply a login.', 'Login', ICON:Exclamation)
          ELSE
             CLEAR(USE:Record)
             USE:Login        = L_G:Login
             IF Access:Users.TryFetch(USE:Key_Login) ~= LEVEL:Benign
                MESSAGE('Unable to load user. (1)', 'Login', ICON:Exclamation)
             ELSE
                IF CLIP(USE:Password) = CLIP(L_G:Password)
                   ! Ok login the user
                   PUTINI('Login', 'LastLogin', L_G:Login, 'C:\TransIS.INI')
                   PUTINI('Login', 'RememberPassword_New', L_G:Remember_Login, 'C:\TransIS.INI')
      
                   SET(CTR:PKey_CID)
                   NEXT(Control)
                   IF ~ERRORCODE()
                      IF L_G:Remember_Login = TRUE
                         CTR:RememberPassword = L_G:Password
                      ELSE
                         CTR:RememberPassword = ''
                      .
                      PUT(Control)
                   ELSE
                      IF L_G:Remember_Login = TRUE
                         CTR:RememberPassword = L_G:Password
                         ADD(Control)
                   .  .
      
                   Add_Audit(10, GlobalErrors.GetProcedureName(), 'User logged in: ' & CLIP(L_G:Login), 'Password correct.', 10)
      
                   GLO:UID            = USE:UID
                   GLO:Login          = USE:Login
                   GLO:AccessLevel    = USE:AccessLevel
                   GLO:LoggedInDate   = TODAY()
                   GLO:LoggedInTime   = CLOCK()
      
                   USEBG:UID          = USE:UID
                   SET(USEBG:Key_UID, USEBG:Key_UID)
                   LOOP
                      IF Access:UsersGroups.TryNext() ~= LEVEL:Benign
                         BREAK
                      .
                      IF USEBG:UID ~= USE:UID
                         BREAK
                      .
      
                      IF CLIP(GLO:UGIDs) = ''
                         GLO:UGIDs    = USEBG:UGID
                      ELSE
                         GLO:UGIDs    = CLIP(GLO:UGIDs) & ',' & USEBG:UGID
                   .  .
      
                   POST(EVENT:CloseWindow)
      
                   ! Check password is sufficient
                   IF LEN(CLIP(L_G:Password)) < 3
                      MESSAGE('The new password requirements mean that you need to change your password.||Please do so on the User screen.', 'User Login', ICON:Exclamation)
                      START(Update_User_h,,CLIP(GLO:UID))
                   .
                ELSE
                   ! Store this in audit
                   Add_Audit(10, GlobalErrors.GetProcedureName(), 'Bad login for user: ' & CLIP(L_G:Login), 'Password failed.', 10)
      
                   MESSAGE('The Login and Password do not match.', 'Login', ICON:Hand)
                   SELECT(?L_G:Password)
          .  .  .
      
          CYCLE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF CLIP(L_G:Login) ~= ''
             SELECT(?L_G:Password)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

