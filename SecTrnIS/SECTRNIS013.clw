

   MEMBER('SECTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SECTRNIS013.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Users PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Option           STRING(20)                            !Option
LOC:Q_Vars           GROUP,PRE()                           !
QV:ReminderType      STRING(20)                            !General, Client
QV:RemindOption      STRING(20)                            !All, Group & User, User, Group
                     END                                   !
LOC:Password         STRING(35)                            !User Password
BRW5::View:Browse    VIEW(UsersAccesses)
                       PROJECT(USAC:AccessOption)
                       PROJECT(USAC:UAID)
                       PROJECT(USAC:UID)
                       PROJECT(USAC:ASID)
                       JOIN(APP:PKey_ASID,USAC:ASID)
                         PROJECT(APP:ApplicationSection)
                         PROJECT(APP:ASID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
APP:ApplicationSection LIKE(APP:ApplicationSection)   !List box control field - type derived from field
LOC:Option             LIKE(LOC:Option)               !List box control field - type derived from local data
USAC:AccessOption      LIKE(USAC:AccessOption)        !Browse hot field - type derived from field
USAC:UAID              LIKE(USAC:UAID)                !Primary key field - type derived from field
USAC:UID               LIKE(USAC:UID)                 !Browse key field - type derived from field
APP:ASID               LIKE(APP:ASID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(UsersGroups)
                       PROJECT(USEBG:UGID)
                       PROJECT(USEBG:UBGID)
                       PROJECT(USEBG:UID)
                       JOIN(USEG:PKey_UGID,USEBG:UGID)
                         PROJECT(USEG:GroupName)
                         PROJECT(USEG:UGID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
USEG:GroupName         LIKE(USEG:GroupName)           !List box control field - type derived from field
USEBG:UGID             LIKE(USEBG:UGID)               !List box control field - type derived from field
USEBG:UBGID            LIKE(USEBG:UBGID)              !Primary key field - type derived from field
USEBG:UID              LIKE(USEBG:UID)                !Browse key field - type derived from field
USEG:UGID              LIKE(USEG:UGID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(Reminders)
                       PROJECT(REM:RID)
                       PROJECT(REM:Active)
                       PROJECT(REM:Popup)
                       PROJECT(REM:ReminderDate)
                       PROJECT(REM:ReminderTime)
                       PROJECT(REM:Notes)
                       PROJECT(REM:ReminderType)
                       PROJECT(REM:RemindOption)
                       PROJECT(REM:UID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
REM:RID                LIKE(REM:RID)                  !List box control field - type derived from field
QV:ReminderType        LIKE(QV:ReminderType)          !List box control field - type derived from local data
REM:Active             LIKE(REM:Active)               !List box control field - type derived from field
REM:Active_Icon        LONG                           !Entry's icon ID
REM:Popup              LIKE(REM:Popup)                !List box control field - type derived from field
REM:Popup_Icon         LONG                           !Entry's icon ID
REM:ReminderDate       LIKE(REM:ReminderDate)         !List box control field - type derived from field
REM:ReminderTime       LIKE(REM:ReminderTime)         !List box control field - type derived from field
QV:RemindOption        LIKE(QV:RemindOption)          !List box control field - type derived from local data
REM:Notes              LIKE(REM:Notes)                !List box control field - type derived from field
REM:ReminderType       LIKE(REM:ReminderType)         !Browse hot field - type derived from field
REM:RemindOption       LIKE(REM:RemindOption)         !Browse hot field - type derived from field
REM:UID                LIKE(REM:UID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::USE:Record  LIKE(USE:RECORD),THREAD
QuickWindow          WINDOW('Form Users'),AT(,,246,258),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM,MDI, |
  HLP('UpdateUsers'),SYSTEM
                       SHEET,AT(4,4,238,235),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Login:'),AT(9,22),USE(?USE:Login:Prompt),TRN
                           ENTRY(@s20),AT(74,22,84,10),USE(USE:Login),MSG('User Login'),REQ,TIP('User Login')
                           PROMPT('Name:'),AT(9,54),USE(?USE:Name:Prompt),TRN
                           ENTRY(@s35),AT(74,54,144,10),USE(USE:Name),MSG('User Name'),TIP('User Name')
                           PROMPT('Surname:'),AT(9,68),USE(?USE:Surname:Prompt),TRN
                           ENTRY(@s35),AT(74,68,144,10),USE(USE:Surname),MSG('User Surname'),TIP('User Surname')
                           GROUP,AT(9,88,106,10),USE(?Group_Level)
                             PROMPT('Access Level:'),AT(9,88),USE(?USE:AccessLevel:Prompt),TRN
                             SPIN(@n3),AT(74,88,40,10),USE(USE:AccessLevel),RIGHT(1),MSG('Users general access level'), |
  TIP('Users general access level')
                           END
                           PROMPT('Password:'),AT(9,124),USE(?USE:Password:Prompt),TRN
                           ENTRY(@s35),AT(74,124,144,10),USE(USE:Password),MSG('User Password'),PASSWORD,TIP('User Password')
                           PROMPT('Confirm Password:'),AT(9,141),USE(?Password:Prompt),TRN
                           ENTRY(@s35),AT(74,141,144,10),USE(LOC:Password),MSG('User Password'),PASSWORD,TIP('User Password')
                         END
                         TAB('&2) Users Groups'),USE(?Tab2)
                           GROUP,AT(113,224,126,12),USE(?Group_UserGroups)
                             BUTTON('&Insert'),AT(113,224,42,12),USE(?Insert:2)
                             BUTTON('&Change'),AT(154,224,42,12),USE(?Change:2)
                             BUTTON('&Delete'),AT(197,224,42,12),USE(?Delete:2)
                           END
                           LIST,AT(9,20,229,200),USE(?List:2),VSCROLL,FORMAT('140L(1)|M~Group Name~L(2)@s35@40R(1)' & |
  '|M~UGID~L(2)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing Records')
                         END
                         TAB('&3) App. Sections'),USE(?Tab3)
                           GROUP,AT(113,224,126,12),USE(?Group_AppSec)
                             BUTTON('&Insert'),AT(113,224,42,12),USE(?Insert)
                             BUTTON('&Change'),AT(154,224,42,12),USE(?Change)
                             BUTTON('&Delete'),AT(197,224,42,12),USE(?Delete)
                           END
                           LIST,AT(9,22,229,198),USE(?List),VSCROLL,FORMAT('100L(2)|M~Application Sections~@s35@80' & |
  'L(2)|M~Option~@s20@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                         END
                         TAB('&4) Reminders'),USE(?Tab4)
                           LIST,AT(6,21,230,214),USE(?List:3),VSCROLL,FORMAT('30R(2)|M~RID~L@n_10@55L(2)|M~Reminde' & |
  'r Type~@s20@26L(2)|MI~Active~@p p@26L(2)|MI~Popup~@p p@38L(2)|M~Date~@d5@34L(2)|M~Ti' & |
  'me~@t7@52L(2)|M~Remind Option~@s20@120L(2)|M~Notes~@s255@'),FROM(Queue:Browse:2),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('&OK'),AT(138,242,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(192,242,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,242,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW5                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW9                 CLASS(BrowseClass)                    ! Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW9::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW11                CLASS(BrowseClass)                    ! Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW11::Sort0:Locator StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Users')
      IF USE:UID = GLO:UID OR GLO:AccessLevel >= 10
      ELSE
         MESSAGE('You can only edit or view your own user details.', 'User Update', ICON:Hand)
         RETURN(LEVEL:Fatal)
      .
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?USE:Login:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Option',LOC:Option)                            ! Added by: BrowseBox(ABC)
  BIND('QV:ReminderType',QV:ReminderType)                  ! Added by: BrowseBox(ABC)
  BIND('QV:RemindOption',QV:RemindOption)                  ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Users)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(USE:Record,History::USE:Record)
  SELF.AddHistoryField(?USE:Login,2)
  SELF.AddHistoryField(?USE:Name,4)
  SELF.AddHistoryField(?USE:Surname,5)
  SELF.AddHistoryField(?USE:AccessLevel,6)
  SELF.AddHistoryField(?USE:Password,3)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Reminders.SetOpenRelated()
  Relate:Reminders.Open                                    ! File Reminders used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Users
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:UsersAccesses,SELF) ! Initialize the browse manager
  BRW9.Init(?List:2,Queue:Browse:1.ViewPosition,BRW9::View:Browse,Queue:Browse:1,Relate:UsersGroups,SELF) ! Initialize the browse manager
  BRW11.Init(?List:3,Queue:Browse:2.ViewPosition,BRW11::View:Browse,Queue:Browse:2,Relate:Reminders,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?USE:Login{PROP:ReadOnly} = True
    ?USE:Name{PROP:ReadOnly} = True
    ?USE:Surname{PROP:ReadOnly} = True
    ?USE:Password{PROP:ReadOnly} = True
    ?LOC:Password{PROP:ReadOnly} = True
    DISABLE(?Insert:2)
    DISABLE(?Change:2)
    DISABLE(?Delete:2)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
  END
      ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned)
      !   0   - Allow (default)
      !   1   - Disable
      !   2   - Hide
      !   100 - Allow                 - Default action
      !   101 - Disallow              - Default action
  
      CASE Get_User_Access(GLO:UID, 'Security', 'Update_Users', 'Upd Groups & App. Sec.', 0)
      OF 0 OROF 100
      ELSE
         DISABLE(?Insert:2)
         DISABLE(?Change:2)
         DISABLE(?Delete:2)
         DISABLE(?Insert)
         DISABLE(?Change)
         DISABLE(?Delete)
  
         BRW9.InsertControl = 0
         BRW9.ChangeControl = 0
         BRW9.DeleteControl = 0
         BRW5.InsertControl = 0
         BRW5.ChangeControl = 0
         BRW5.DeleteControl = 0
  
         DISABLE(?Group_AppSec)
         DISABLE(?Group_UserGroups)
      .
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,USAC:FKey_UID)                        ! Add the sort order for USAC:FKey_UID for sort order 1
  BRW5.AddRange(USAC:UID,USE:UID)                          ! Add single value range limit for sort order 1
  BRW5.AddLocator(BRW5::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW5::Sort0:Locator.Init(,USAC:UID,1,BRW5)               ! Initialize the browse locator using  using key: USAC:FKey_UID , USAC:UID
  BRW5.AppendOrder('+APP:ApplicationSection')              ! Append an additional sort order
  BRW5.AddField(APP:ApplicationSection,BRW5.Q.APP:ApplicationSection) ! Field APP:ApplicationSection is a hot field or requires assignment from browse
  BRW5.AddField(LOC:Option,BRW5.Q.LOC:Option)              ! Field LOC:Option is a hot field or requires assignment from browse
  BRW5.AddField(USAC:AccessOption,BRW5.Q.USAC:AccessOption) ! Field USAC:AccessOption is a hot field or requires assignment from browse
  BRW5.AddField(USAC:UAID,BRW5.Q.USAC:UAID)                ! Field USAC:UAID is a hot field or requires assignment from browse
  BRW5.AddField(USAC:UID,BRW5.Q.USAC:UID)                  ! Field USAC:UID is a hot field or requires assignment from browse
  BRW5.AddField(APP:ASID,BRW5.Q.APP:ASID)                  ! Field APP:ASID is a hot field or requires assignment from browse
  BRW9.Q &= Queue:Browse:1
  BRW9.AddSortOrder(,USEBG:Key_UID)                        ! Add the sort order for USEBG:Key_UID for sort order 1
  BRW9.AddRange(USEBG:UID,USE:UID)                         ! Add single value range limit for sort order 1
  BRW9.AddLocator(BRW9::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW9::Sort0:Locator.Init(,USEBG:UID,1,BRW9)              ! Initialize the browse locator using  using key: USEBG:Key_UID , USEBG:UID
  BRW9.AppendOrder('+USEG:GroupName,+USEBG:UBGID')         ! Append an additional sort order
  BRW9.AddField(USEG:GroupName,BRW9.Q.USEG:GroupName)      ! Field USEG:GroupName is a hot field or requires assignment from browse
  BRW9.AddField(USEBG:UGID,BRW9.Q.USEBG:UGID)              ! Field USEBG:UGID is a hot field or requires assignment from browse
  BRW9.AddField(USEBG:UBGID,BRW9.Q.USEBG:UBGID)            ! Field USEBG:UBGID is a hot field or requires assignment from browse
  BRW9.AddField(USEBG:UID,BRW9.Q.USEBG:UID)                ! Field USEBG:UID is a hot field or requires assignment from browse
  BRW9.AddField(USEG:UGID,BRW9.Q.USEG:UGID)                ! Field USEG:UGID is a hot field or requires assignment from browse
  BRW11.Q &= Queue:Browse:2
  BRW11.AddSortOrder(,REM:FKey_UID)                        ! Add the sort order for REM:FKey_UID for sort order 1
  BRW11.AddRange(REM:UID,USE:UID)                          ! Add single value range limit for sort order 1
  BRW11.AddLocator(BRW11::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW11::Sort0:Locator.Init(,REM:UID,1,BRW11)              ! Initialize the browse locator using  using key: REM:FKey_UID , REM:UID
  BRW11.AppendOrder('-REM:ReminderDate,+REM:RID')          ! Append an additional sort order
  ?List:3{PROP:IconList,1} = '~checkoffdim.ico'
  ?List:3{PROP:IconList,2} = '~checkon.ico'
  BRW11.AddField(REM:RID,BRW11.Q.REM:RID)                  ! Field REM:RID is a hot field or requires assignment from browse
  BRW11.AddField(QV:ReminderType,BRW11.Q.QV:ReminderType)  ! Field QV:ReminderType is a hot field or requires assignment from browse
  BRW11.AddField(REM:Active,BRW11.Q.REM:Active)            ! Field REM:Active is a hot field or requires assignment from browse
  BRW11.AddField(REM:Popup,BRW11.Q.REM:Popup)              ! Field REM:Popup is a hot field or requires assignment from browse
  BRW11.AddField(REM:ReminderDate,BRW11.Q.REM:ReminderDate) ! Field REM:ReminderDate is a hot field or requires assignment from browse
  BRW11.AddField(REM:ReminderTime,BRW11.Q.REM:ReminderTime) ! Field REM:ReminderTime is a hot field or requires assignment from browse
  BRW11.AddField(QV:RemindOption,BRW11.Q.QV:RemindOption)  ! Field QV:RemindOption is a hot field or requires assignment from browse
  BRW11.AddField(REM:Notes,BRW11.Q.REM:Notes)              ! Field REM:Notes is a hot field or requires assignment from browse
  BRW11.AddField(REM:ReminderType,BRW11.Q.REM:ReminderType) ! Field REM:ReminderType is a hot field or requires assignment from browse
  BRW11.AddField(REM:RemindOption,BRW11.Q.REM:RemindOption) ! Field REM:RemindOption is a hot field or requires assignment from browse
  BRW11.AddField(REM:UID,BRW11.Q.REM:UID)                  ! Field REM:UID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_Users',QuickWindow)                 ! Restore window settings from non-volatile store
      LOC:Password    = USE:Password
      IF GLO:AccessLevel > 10
         ?USE:Password{PROP:Password}  = FALSE
         ?LOC:Password{PROP:Password}  = FALSE
      .
  
  
      IF GLO:AccessLevel >= 10
      ELSE
         DISABLE(?Group_Level)
      .
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW5.AskProcedure = 1
  BRW9.AskProcedure = 2
  BRW5.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW5.ToolbarItem.HelpButton = ?Help
  BRW9.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW9.ToolbarItem.HelpButton = ?Help
  BRW11.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  BRW11.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Reminders.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Users',QuickWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Update_Users_Accesses
      Update_UsersGroups
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
          IF LEN(CLIP(USE:Password)) < 3
             MESSAGE('Password is too short.||Must be at least 3 characters.', 'Update Users', ICON:Hand)
             QuickWindow{PROP:AcceptAll}  = FALSE
             SELECT(?USE:Password)
             CYCLE
          .
      
      
          IF USE:Password ~= LOC:Password
             MESSAGE('Passwords do not match, please re-enter.', 'Update Users', ICON:Hand)
             QuickWindow{PROP:AcceptAll}  = FALSE
             SELECT(?USE:Password)
             CYCLE
          .
      
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?USE:Login
          IF CLIP(USE:Name) = ''
             USE:Name     = USE:Login
             DISPLAY(?USE:Name)
          .
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW5.SetQueueRecord PROCEDURE

  CODE
      ! Active|Disable|Hide
      EXECUTE USAC:AccessOption + 1
         LOC:Option   = 'Active'
         LOC:Option   = 'Disable'
         LOC:Option   = 'Hide'
      ELSE
         LOC:Option   = '<Unknown: ' & USAC:AccessOption & '>'
      .
  PARENT.SetQueueRecord
  


BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW11.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
      ! General|Client
      EXECUTE REM:ReminderType + 1
         QV:ReminderType  = 'General'
         QV:ReminderType  = 'Client'
      .
  
  
      ! All|Group & User|User|Group
      EXECUTE REM:RemindOption + 1
         QV:RemindOption  = 'All'
         QV:RemindOption  = 'Group & User'
         QV:RemindOption  = 'User'
         QV:RemindOption  = 'Group'
      .
  
  IF (REM:Active = 1)
    SELF.Q.REM:Active_Icon = 2                             ! Set icon from icon list
  ELSE
    SELF.Q.REM:Active_Icon = 1                             ! Set icon from icon list
  END
  IF (REM:Popup = 1)
    SELF.Q.REM:Popup_Icon = 2                              ! Set icon from icon list
  ELSE
    SELF.Q.REM:Popup_Icon = 1                              ! Set icon from icon list
  END

