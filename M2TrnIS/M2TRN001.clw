

   MEMBER('M2TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M2TRN001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! p_Type - Fuel cost type - Standard or Client
!!! �
!!! </summary>
Browse_FuelCost PROCEDURE (p_Type)

CurrentTab           STRING(80)                            !
FuelCostType         BYTE                                  !Type of this Fuel Cost
TypeStr              STRING(20)                            !Standard|Client
BRW1::View:Browse    VIEW(_FuelCost)
                       PROJECT(FUE:FuelCost)
                       PROJECT(FUE:FuelBaseRate)
                       PROJECT(FUE:EffectiveDate)
                       PROJECT(FUE:FCID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
FUE:FuelCost           LIKE(FUE:FuelCost)             !List box control field - type derived from field
FUE:FuelBaseRate       LIKE(FUE:FuelBaseRate)         !List box control field - type derived from field
FUE:EffectiveDate      LIKE(FUE:EffectiveDate)        !List box control field - type derived from field
TypeStr                LIKE(TypeStr)                  !List box control field - type derived from local data
A_FUE:FuelCostType     LIKE(A_FUE:FuelCostType)       !Browse hot field - type derived from field
FUE:FCID               LIKE(FUE:FCID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Fuel Cost file'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Browse_FuelCost'),SYSTEM
                       LIST,AT(8,36,261,118),USE(?Browse:1),HVSCROLL,FORMAT('70R(1)|M~Fuel Cost (cents)~C(0)@n' & |
  '-14.3@70R(1)|M~Fuel Base Rate %~C(0)@n-13.4~%~@70R(1)|M~Effective Date~C(0)@D6B@80L(' & |
  '2)|M~Type~C(0)@s20@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the _FuelCost file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Effective Date'),USE(?Tab:3)
                         END
                         TAB('&1) By Fuel Cost'),USE(?Tab:2),HIDE
                         END
                       END
                       OPTION,AT(10,19,174,18),USE(FuelCostType),MSG('Type of this Fuel Cost'),TIP('Type of th' & |
  'is Fuel Cost<0DH,0AH>Standard or Client specific entry'),TRN
                         RADIO(' Standard'),AT(12,22),USE(?FuelCostType:Radio1),TRN,VALUE('0')
                         RADIO(' Client'),AT(64,22),USE(?FuelCostType:Radio2),TRN,VALUE('1')
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_FuelCost')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('FuelCostType',FuelCostType)                        ! Added by: BrowseBox(ABC)
  BIND('TypeStr',TypeStr)                                  ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:_FuelCost.SetOpenRelated()
  Relate:_FuelCost.Open                                    ! File _FuelCost used by this procedure, so make sure it's RelationManager is open
  Relate:_FuelCostsAlias.Open                              ! File _FuelCostsAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_FuelCost,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW1.AddSortOrder(,FUE:SKey_EffectiveDate)               ! Add the sort order for FUE:SKey_EffectiveDate for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,FUE:EffectiveDate,1,BRW1)      ! Initialize the browse locator using  using key: FUE:SKey_EffectiveDate , FUE:EffectiveDate
  BRW1.SetFilter('(FuelCostType = FUE:FuelCostType)')      ! Apply filter expression to browse
  BRW1.AddSortOrder(,FUE:SKey_EffectiveDate)               ! Add the sort order for FUE:SKey_EffectiveDate for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,FUE:EffectiveDate,1,BRW1)      ! Initialize the browse locator using  using key: FUE:SKey_EffectiveDate , FUE:EffectiveDate
  BRW1.SetFilter('(FuelCostType = FUE:FuelCostType)')      ! Apply filter expression to browse
  BRW1.AddResetField(FuelCostType)                         ! Apply the reset field
  BRW1.AddField(FUE:FuelCost,BRW1.Q.FUE:FuelCost)          ! Field FUE:FuelCost is a hot field or requires assignment from browse
  BRW1.AddField(FUE:FuelBaseRate,BRW1.Q.FUE:FuelBaseRate)  ! Field FUE:FuelBaseRate is a hot field or requires assignment from browse
  BRW1.AddField(FUE:EffectiveDate,BRW1.Q.FUE:EffectiveDate) ! Field FUE:EffectiveDate is a hot field or requires assignment from browse
  BRW1.AddField(TypeStr,BRW1.Q.TypeStr)                    ! Field TypeStr is a hot field or requires assignment from browse
  BRW1.AddField(A_FUE:FuelCostType,BRW1.Q.A_FUE:FuelCostType) ! Field A_FUE:FuelCostType is a hot field or requires assignment from browse
  BRW1.AddField(FUE:FCID,BRW1.Q.FUE:FCID)                  ! Field FUE:FCID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_FuelCost',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_FuelCost.Close
    Relate:_FuelCostsAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_FuelCost',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_FuelCost((p_Type))
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Select:2
      ThisWindow.Update()
          A_FUE:Record    :=: FUE:Record
      
      !    message('A_FUE:FCID: ' & A_FUE:FCID)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
      EXECUTE FUE:FuelCostType + 1
         TypeStr     = 'Standard'
         TypeStr     = 'Client'
      .
  PARENT.SetQueueRecord
  


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_RatesFuelCost PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:ClientName       STRING(100)                           !
LOC:ClientNo         ULONG                                 !Client No.
FuelReBaseRate       DECIMAL(10,4)                         !% cost of total cost
EffectiveDate        DATE                                  !
Clients_Cost_Base_Date DATE                                !
Clients_Cost_Base_FuelCost DECIMAL(10,3)                   !Cost per litre
LOC:CPL_Group        GROUP,PRE(L_CLP)                      !
X_CostBase_Date      LONG                                  !
Y_CostChange_Date    LONG                                  !
X_Cost_As_At         DECIMAL(15,4)                         !Cost per litre
Y_CostChange         DECIMAL(15,4)                         !Cost per litre
Z_ChangeInCPL        DECIMAL(17,7)                         !Cost per litre
Q_ChangePercent      DECIMAL(17,7)                         !
                     END                                   !
LOC:Cost_Group       GROUP,PRE(L_CG)                       !
M_FuelBaseRate       DECIMAL(15,4)                         !% cost of total cost
OtherCosts           DECIMAL(15,4)                         !% cost of total cost
TotalCostPer         DECIMAL(15,4,100)                     !% cost of total cost
N_IncreaseOnBase     DECIMAL(15,4)                         !% cost of total cost
O_FuelCostChange     DECIMAL(15,4)                         !% cost of total cost
P_OtherCostChange    DECIMAL(15,4)                         !% cost of total cost
R_CostChangeTotal    DECIMAL(15,4)                         !% cost of total cost
S_RevisedBase        DECIMAL(15,4)                         !% cost of total cost
T_OtherRevised       DECIMAL(15,4)                         !% cost of total cost
ST_RevisedTotal      DECIMAL(15,4)                         !% cost of total cost
                     END                                   !
History::FCRA:Record LIKE(FCRA:RECORD),THREAD
QuickWindow          WINDOW('Form Rates Fuel Cost'),AT(,,379,241),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_RatesFuelCost'),SYSTEM
                       SHEET,AT(3,4,373,63),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Client Name:'),AT(7,23),USE(?LOC:ClientName:Prompt),TRN
                           BUTTON('...'),AT(79,23,12,10),USE(?CallLookup),SKIP
                           ENTRY(@s100),AT(95,23,168,10),USE(LOC:ClientName),COLOR(00E9E9E9h),FLAT,READONLY,SKIP,TIP('Client name')
                           PROMPT('Client No.:'),AT(7,36),USE(?LOC:ClientNo:Prompt),TRN
                           ENTRY(@n_10b),AT(95,36,64,10),USE(LOC:ClientNo),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Client No.'), |
  READONLY,SKIP,TIP('Client No.')
                           PROMPT('CID:'),AT(202,36),USE(?FCRA:CID:Prompt),TRN
                           STRING(@n_10),AT(219,36,,10),USE(FCRA:CID),RIGHT(1),TRN
                           PROMPT('Effective Date:'),AT(7,52),USE(?FCRA:Effective_Date:Prompt),TRN
                           BUTTON('...'),AT(79,52,12,10),USE(?Calendar)
                           SPIN(@d5),AT(95,52,64,10),USE(FCRA:Effective_Date),RIGHT(1),MSG('Effective from this date'), |
  REQ,TIP('Effective from this date')
                         END
                       END
                       SHEET,AT(3,68,373,129),USE(?Sheet1),WIZARD
                         TAB,USE(?Tab1)
                           PROMPT('Cost Base Date:'),AT(74,77),USE(?CostBase_Date:Prompt),TRN
                           PROMPT('Cost Change Date:'),AT(159,77),USE(?CostChange_Date:Prompt),TRN
                           PROMPT('Change in CPL'),AT(251,77),USE(?ChangeInCPL:Prompt),TRN
                           PROMPT('CPL Change %'),AT(323,77),USE(?ChangePercent:Prompt),TRN
                           BUTTON('...'),AT(59,90,10,10),USE(?Button_CostBaseDate)
                           ENTRY(@d6b),AT(74,90,60,10),USE(L_CLP:X_CostBase_Date),RIGHT(1)
                           PROMPT('Fuel Cost (cents):'),AT(11,106),USE(?CostBase:Prompt),TRN
                           ENTRY(@n-14.2),AT(74,106,60,10),USE(L_CLP:X_Cost_As_At),RIGHT(1),MSG('Cost per litre (cents)'), |
  TIP('Cost per litre (cents)')
                           BUTTON('...'),AT(146,90,10,10),USE(?Button_CostChangeDate)
                           ENTRY(@d6b),AT(159,90,60,10),USE(L_CLP:Y_CostChange_Date),RIGHT(1)
                           ENTRY(@n-14.2),AT(159,106,60,10),USE(L_CLP:Y_CostChange),RIGHT(1),MSG('Cost per litre (cents)'), |
  TIP('Cost per litre (cents)')
                           ENTRY(@n-14.2),AT(251,106,47,10),USE(L_CLP:Z_ChangeInCPL),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Cost per litre'), |
  READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@n-13.4~%~),AT(323,106,47,10),USE(L_CLP:Q_ChangePercent),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  READONLY,SKIP
                           PROMPT('Base Rate'),AT(50,127),USE(?Prompt16),TRN
                           PROMPT('CPL Change %'),AT(115,127),USE(?ChangePercent:Prompt:2),TRN
                           PROMPT('Increase'),AT(179,127),USE(?L_CG:IncreaseOnBase:Prompt),TRN
                           PROMPT('Change in Cost'),AT(251,127),USE(?L_CG:FuelCostChange:Prompt),TRN
                           PROMPT('Revised Base'),AT(326,127),USE(?L_CG:RevisedBase:Prompt),TRN
                           PROMPT('Fuel:'),AT(11,143),USE(?L_CG:FuelBaseRate:Prompt),TRN
                           ENTRY(@N-13.4~%~),AT(50,143,47,10),USE(L_CG:M_FuelBaseRate),RIGHT(1),MSG('Cost per litre'), |
  TIP('Cost per litre')
                           PROMPT('Other:'),AT(11,159),USE(?L_CG:OtherCosts:Prompt),TRN
                           ENTRY(@N-13.4~%~),AT(50,159,47,10),USE(L_CG:OtherCosts),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Cost per litre'), |
  READONLY,SKIP,TIP('Cost per litre')
                           PROMPT('Total:'),AT(11,180),USE(?L_CG:TotalCostPer:Prompt),TRN
                           ENTRY(@N-13.4~%~),AT(50,180,47,10),USE(L_CG:TotalCostPer),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Cost per litre'), |
  READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@n-13.4~%~),AT(115,143,47,10),USE(L_CLP:Q_ChangePercent,,?L_CLP:ChangePercent:2),RIGHT(1), |
  COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                           ENTRY(@N-13.4~%~),AT(179,143,47,10),USE(L_CG:N_IncreaseOnBase),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(251,143,47,10),USE(L_CG:O_FuelCostChange),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(251,159,47,10),USE(L_CG:P_OtherCostChange),RIGHT(1),COLOR(00E9E9E9h), |
  FLAT,MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(251,180,47,10),USE(L_CG:R_CostChangeTotal),RIGHT(1),COLOR(00E9E9E9h), |
  FLAT,MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(323,143,47,10),USE(L_CG:S_RevisedBase),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(323,159,47,10),USE(L_CG:T_OtherRevised),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(323,180,47,10),USE(L_CG:ST_RevisedTotal),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                         END
                       END
                       PANEL,AT(3,200,373,20),USE(?Panel1)
                       STRING(@N-13.2~%~),AT(276,206),USE(L_CG:N_IncreaseOnBase,,?L_CG:N_IncreaseOnBase:2),FONT(, |
  ,,FONT:bold,CHARSET:ANSI)
                       PROMPT(' = '),AT(246,206),USE(?Prompt15),FONT(,,,FONT:bold,CHARSET:ANSI)
                       PROMPT('Fuel Surcharge at:'),AT(40,206),USE(?Prompt14),FONT(,,,FONT:bold,CHARSET:ANSI)
                       STRING(@D18B),AT(130,206,96,10),USE(L_CLP:Y_CostChange_Date,,?L_CLP:Y_CostChange_Date:2),FONT(, |
  ,,FONT:bold,CHARSET:ANSI)
                       BUTTON('Create &Report'),AT(4,224,59,14),USE(?Report),SKIP
                       PROMPT('Note:  Negative Fuel Surcharges will not be used, they will appear as zero.'),AT(69, |
  223,153,17),USE(?Prompt_Neg),FONT(,,COLOR:Red,,CHARSET:ANSI),HIDE
                       BUTTON('&OK'),AT(274,224,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(328,224,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(224,224,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

gFuelSurchargeReport            GROUP,PRE(gFSR)
ReportHeader                                       CSTRING(256)
Client_Address                                     CSTRING(256)
Run_date                                           CSTRING(256)
Client_No                                          CSTRING(256)
Client_fax_No                                      CSTRING(256)
FuelBaseRate_1stRecord                             CSTRING(256)
FuelBaseRate_As_of_ClientsFuelCostEffectiveDate    CSTRING(256)
ClientsFuelSurcharge_At_RunForEffectiveDate        CSTRING(256)
ClientsFuelSurcharge_EffectiveDate                 CSTRING(256)
FuelCostBaseDate                                   CSTRING(256)
FuelCost_At_FuelCostBaseDate                       CSTRING(256)
FuelCost_At_Run_Date                               CSTRING(256)
CPLChange                                          CSTRING(256)
CPLChangePercent                                   CSTRING(256)
FuelCostBaseDate_Month_Year                        CSTRING(256)
BaseRate                                           CSTRING(256)
BaseIncrease                                       CSTRING(256)
BaseIncreasePercent                                CSTRING(256)
BaseChange                                         CSTRING(256)
OtherRate                                          CSTRING(256)
OtherChange                                        CSTRING(256)
TotalChange                                        CSTRING(256)
RevisedFuelBasePercent                             CSTRING(256)
RevisedOtherPercent                                CSTRING(256)
RevisedTotalPercent                                CSTRING(256)
FuelSurcharge                                      CSTRING(256)
                                 END

DocPath             CString(FILE:MaxFileName)
PDFPath             CString(FILE:MaxFilePath)
PDFFileName         CString(FILE:MaxFileName)
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar7            CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Client_Surcharge            ROUTINE
    IF QuickWindow{PROP:AcceptAll} = FALSE
       ! (p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Cur_Eff_Date, |
       !  p:X_Fuel_Cost, p:X_Eff_Date, p:M_Fuel_Base_Rate,
       !  p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base,
       !  p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per)
       ! (ULONG, ULONG, *DECIMAL, LONG, <*DECIMAL>, <*LONG>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING


       ! (p:X_Fuel_Cost, p:Y_Fuel_Cost, p:M_Fuel_Base_Rate,
       !  p:Z_CPL_Change, p:Q_CPL_Change_Per, p:O_Change_FuelC_Per,
       !  p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per)
       ! (*DECIMAL, *DECIMAL, *DECIMAL, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING

       FCRA:FuelSurcharge  = Calc_FuelBaseRate(Clients_Cost_Base_FuelCost, FCRA:FuelCost, FCRA:FuelBaseRate)

       !FCRA:FuelSurcharge  = Get_Client_FuelSurcharge_Calcs(FCRA:CID, FCRA:FCID, FCRA:FuelCost, FCRA:Effective_Date,,,,,,,, FuelReBaseRate)
    ELSE
       !Get_Client_FuelSurcharge_Calcs(FCRA:CID, FCRA:FCID, FCRA:FuelCost, FCRA:Effective_Date,,,,,,, FuelReBaseRate)
    .

    ! This allows manual override but will reset every time the user comes back into the form....  ???

    DISPLAY
    EXIT
Calc_Values                    ROUTINE
    L_CG:N_IncreaseOnBase    = Calc_FuelBaseRate(L_CLP:X_Cost_As_At, L_CLP:Y_CostChange, L_CG:M_FuelBaseRate,     |
                                      L_CLP:Z_ChangeInCPL, L_CLP:Q_ChangePercent,                         |
                                      L_CG:O_FuelCostChange, L_CG:S_RevisedBase, L_CG:T_OtherRevised)


    L_CG:OtherCosts          = 100 - L_CG:M_FuelBaseRate
    L_CG:TotalCostPer        = 100

    L_CG:P_OtherCostChange   = 100 - L_CG:M_FuelBaseRate
    L_CG:R_CostChangeTotal   = L_CG:P_OtherCostChange + L_CG:O_FuelCostChange

    L_CG:T_OtherRevised      = L_CG:P_OtherCostChange / L_CG:R_CostChangeTotal * 100
    L_CG:ST_RevisedTotal     = L_CG:S_RevisedBase + L_CG:T_OtherRevised


    
    IF L_CG:N_IncreaseOnBase < 0.0
       ?L_CG:N_IncreaseOnBase:2{PROP:FontColor}   = Color:Red
       UNHIDE(?Prompt_Neg)
    ELSE
       ?L_CG:N_IncreaseOnBase:2{PROP:FontColor}      = Color:None
       HIDE(?Prompt_Neg)
    .

    DISPLAY
    EXIT
Get_Previous                  ROUTINE
    DATA
R:Eff_Date      DATE

    CODE
    IF FCRA:Effective_Date <> 0
       R:Eff_Date   = FCRA:Effective_Date
    ELSE
       R:Eff_Date   = TODAY()
    .

    ! (p:CID, p:Date, p:FuelBaseRate)
    ! (ULONG, LONG=0, <*DECIMAL>),STRING
    !
    ! p:Date      - effective date
    !Get_Client_FuelSurcharge(FCRA:CID, TODAY() - 1, )



    ! (p:CID, p:Date, p:Return_Eff_Date)
    ! (ULONG, LONG=0, *LONG), *DECIMAL
    !
    ! p:Date                - as of date
    ! p:Return_Eff_Date     - effective date of the returned record

    Get_Client_FuelCosts(FCRA:CID, R:Eff_Date - 1)      ! Get uses Alias tables, so we will expect Alias record loaded after

!    db.debugout('FCRA:CID: ' & FCRA:CID & '     A_FCRA:CID: ' & A_FCRA:CID)

    IF A_FCRA:CID = FCRA:CID
       FCRA:FCID            = A_FCRA:FCID
       FCRA:FCID_CostBase   = A_FCRA:FCID_CostBase

       FCRA:FuelBaseRate    = A_FCRA:FuelBaseRate
       FCRA:CostBase_Date   = A_FCRA:CostBase_Date
    ELSE
       ! No prior fuelsurcharge found
    .

    DISPLAY
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Rates Fuel Cost Record'
  OF InsertRecord
    ActionMessage = 'Rates Fuel Cost Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Rates Fuel Cost Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_RatesFuelCost')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:ClientName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:__RatesFuelCost)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(FCRA:Record,History::FCRA:Record)
  SELF.AddHistoryField(?FCRA:CID,2)
  SELF.AddHistoryField(?FCRA:Effective_Date,10)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.SetOpenRelated()
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Relate:__RatesFuelCostAlias.Open                         ! File __RatesFuelCostAlias used by this procedure, so make sure it's RelationManager is open
  Access:_FuelCost.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:__RatesFuelCost
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?LOC:ClientName{PROP:ReadOnly} = True
    ?LOC:ClientNo{PROP:ReadOnly} = True
    DISABLE(?Calendar)
    DISABLE(?Button_CostBaseDate)
    ?L_CLP:X_CostBase_Date{PROP:ReadOnly} = True
    ?L_CLP:X_Cost_As_At{PROP:ReadOnly} = True
    DISABLE(?Button_CostChangeDate)
    ?L_CLP:Y_CostChange_Date{PROP:ReadOnly} = True
    ?L_CLP:Y_CostChange{PROP:ReadOnly} = True
    ?L_CLP:Z_ChangeInCPL{PROP:ReadOnly} = True
    ?L_CLP:Q_ChangePercent{PROP:ReadOnly} = True
    ?L_CG:M_FuelBaseRate{PROP:ReadOnly} = True
    ?L_CG:OtherCosts{PROP:ReadOnly} = True
    ?L_CG:TotalCostPer{PROP:ReadOnly} = True
    ?L_CLP:ChangePercent:2{PROP:ReadOnly} = True
    ?L_CG:N_IncreaseOnBase{PROP:ReadOnly} = True
    ?L_CG:O_FuelCostChange{PROP:ReadOnly} = True
    ?L_CG:P_OtherCostChange{PROP:ReadOnly} = True
    ?L_CG:R_CostChangeTotal{PROP:ReadOnly} = True
    ?L_CG:S_RevisedBase{PROP:ReadOnly} = True
    ?L_CG:T_OtherRevised{PROP:ReadOnly} = True
    ?L_CG:ST_RevisedTotal{PROP:ReadOnly} = True
    DISABLE(?Report)
  END
      IF SELF.Request = InsertRecord
         FCRA:CID     = CLI:CID
  
         DO Get_Previous
      .
  
  
      CLI:CID             = FCRA:CID
      ! Need to check is this affects the client record loaded on calling proc. - are changed fields kept??
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         LOC:ClientName   = CLI:ClientName
         LOC:ClientNo     = CLI:ClientNo
      .
      FUE:FCID                        = FCRA:FCID
      IF Access:_FuelCost.TryFetch(FUE:PKey_FCID) = LEVEL:Benign
         EffectiveDate                = FUE:EffectiveDate
      .
  
      FUE:FCID                        = FCRA:FCID_CostBase
      IF Access:_FuelCost.TryFetch(FUE:PKey_FCID) = LEVEL:Benign
         Clients_Cost_Base_Date       = FUE:EffectiveDate
         Clients_Cost_Base_FuelCost   = FUE:FuelCost
      .
  
  
  
      L_CLP:Y_CostChange      = FCRA:FuelCost
      ! FCRA:FCID               =???  should we bother setting this?
      !L_CLP:X_Cost_As_At      = FCRA:FCID_CostBase
      L_CLP:X_Cost_As_At      = FCRA:CostBase_X
  
      L_CG:M_FuelBaseRate     = FCRA:FuelBaseRate
      L_CLP:X_CostBase_Date   = FCRA:CostBase_Date
      L_CLP:Y_CostChange_Date = FCRA:CostChange_Date
  
  
      ! Fix for incorrect field above - when 0 costbase but not zero cost base date then look up - 4/02/10
      IF FCRA:CostBase_X = 0.0 AND FCRA:CostBase_Date <> 0
         L_CLP:X_Cost_As_At   = Get_FuelCost(L_CLP:X_CostBase_Date)
      .
  
  
  
      ! Used on DI's
      L_CG:N_IncreaseOnBase   = FCRA:FuelSurcharge
  !    DO Client_Surcharge
  
      DO Calc_Values
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_RatesFuelCost',QuickWindow)         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
    Relate:__RatesFuelCostAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_RatesFuelCost',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Select_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = LOC:ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:ClientName = CLI:ClientName
        FCRA:CID = CLI:CID
        LOC:ClientNo = CLI:ClientNo
      END
      ThisWindow.Reset(1)
    OF ?LOC:ClientName
      IF LOC:ClientName OR ?LOC:ClientName{PROP:Req}
        CLI:ClientName = LOC:ClientName
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:ClientName = CLI:ClientName
            FCRA:CID = CLI:CID
            LOC:ClientNo = CLI:ClientNo
          ELSE
            CLEAR(FCRA:CID)
            CLEAR(LOC:ClientNo)
            SELECT(?LOC:ClientName)
            CYCLE
          END
        ELSE
          FCRA:CID = CLI:CID
          LOC:ClientNo = CLI:ClientNo
        END
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
      Calendar7.SelectOnClose = True
      Calendar7.Ask('Select a Date',FCRA:Effective_Date)
      IF Calendar7.Response = RequestCompleted THEN
      FCRA:Effective_Date=Calendar7.SelectedDate
      DISPLAY(?FCRA:Effective_Date)
      END
      ThisWindow.Reset(True)
    OF ?Button_CostBaseDate
      ThisWindow.Update()
          IF L_CLP:X_CostBase_Date = 0
             FUE:EffectiveDate           = 0
          ELSE
             FUE:EffectiveDate           = L_CLP:X_CostBase_Date
          .
          IF Access:_FuelCost.TryFetch(FUE:SKey_EffectiveDate) = LEVEL:Benign
          .
      
          GlobalRequest               = SelectRecord
          Browse_FuelCost(0)
          IF GlobalResponse = RequestCompleted
             L_CLP:X_CostBase_Date    = FUE:EffectiveDate
             L_CG:M_FuelBaseRate      = FUE:FuelBaseRate
      
             L_CLP:X_Cost_As_At       = Get_FuelCost(L_CLP:X_CostBase_Date)
          .
      
          DO Calc_Values
    OF ?L_CLP:X_Cost_As_At
          DO Calc_Values
    OF ?Button_CostChangeDate
      ThisWindow.Update()
          ! FCRA:FuelCost   = Get_FuelCost(FCRA:Effective_Date)
          IF L_CLP:Y_CostChange_Date = 0
             FUE:EffectiveDate           = TODAY()
          ELSE
             FUE:EffectiveDate           = L_CLP:Y_CostChange_Date
          .
          IF Access:_FuelCost.TryFetch(FUE:SKey_EffectiveDate) = LEVEL:Benign
          .
      
          GlobalRequest               = SelectRecord
          Browse_FuelCost(0)
          IF GlobalResponse = RequestCompleted
             L_CLP:Y_CostChange_Date  = FUE:EffectiveDate
             L_CLP:Y_CostChange       = Get_FuelCost(L_CLP:Y_CostChange_Date)
      
          .
      
      
          DO Calc_Values
    OF ?L_CLP:Y_CostChange
          DO Calc_Values
    OF ?L_CG:M_FuelBaseRate
          DO Calc_Values
    OF ?Report
      ThisWindow.Update()
      !!!! Eugene !!!
                   gFSR:Client_Address                                     = Get_Address(CLI:AID,1,0)
                   gFSR:Run_date                                           = FORMAT(L_CLP:Y_CostChange_Date,@d17)
                   gFSR:Client_No                                          = CLI:ClientNo
                   ADD:AID = CLI:AID
                   IF Access:Addresses.Fetch(ADD:PKey_AID)=Level:Benign
                      gFSR:Client_fax_No                                   = clip(ADD:Fax)
                   ELSE
                      gFSR:Client_fax_No                                   = ''
                   END
                   gFSR:FuelBaseRate_1stRecord                             = 'FuelBaseRate_1stRecord - ??'
                   gFSR:FuelBaseRate_As_of_ClientsFuelCostEffectiveDate    = L_CLP:X_Cost_As_At !????
                   gFSR:ClientsFuelSurcharge_At_RunForEffectiveDate        = 'ClientsFuelSurcharge_At_RunForEffectiveDate - ??'
                   gFSR:ClientsFuelSurcharge_EffectiveDate                 = FORMAT(FCRA:Effective_Date,@d17)
                   gFSR:FuelCostBaseDate                                   = FORMAT(L_CLP:X_CostBase_Date,@d17)
                   gFSR:FuelCost_At_FuelCostBaseDate                       = FORMAT(L_CLP:X_Cost_As_At,@n14.2)
                   gFSR:FuelCost_At_Run_Date                               = FORMAT(L_CLP:Y_CostChange,@n14.2)
                   gFSR:CPLChange                                          = FORMAT(L_CLP:Z_ChangeInCPL,@n14.2)!gFSR:FuelCost_At_Run_Date-gFSR:FuelCost_At_FuelCostBaseDate
                   gFSR:CPLChangePercent                                   = FORMAT(L_CLP:Q_ChangePercent,@n14.2)!gFSR:CPLChange/gFSR:FuelCost_At_FuelCostBaseDate * 100
                   gFSR:FuelCostBaseDate_Month_Year                        = CHOOSE(MONTH(gFSR:FuelCostBaseDate),'JAN','FEB','MAR','APR','MAY','JUN',|
                                                                                                                 'JUL','AUG','SEP','OCT','NOV','DEC','')&|
                                                                            ' '&YEAR(gFSR:FuelCostBaseDate)
                   gFSR:BaseRate                                           = FORMAT(L_CG:M_FuelBaseRate,@n14.2)
                   gFSR:BaseIncrease                                       = FORMAT(L_CG:N_IncreaseOnBase,@n14.2)!gFSR:BaseRate*gFSR:CPLChangePercent/100
                   gFSR:BaseIncreasePercent                                = FORMAT(gFSR:BaseIncrease*gFSR:BaseRate/100,@n14.2)
                   gFSR:BaseChange                                         = FORMAT(L_CG:O_FuelCostChange,@n14.2)!gFSR:BaseRate+gFSR:BaseIncrease
                   gFSR:OtherRate                                          = FORMAT(L_CG:OtherCosts,@n14.2)!100-gFSR:BaseRate
                   gFSR:OtherChange                                        = FORMAT(L_CG:P_OtherCostChange,@n14.2)!gFSR:OtherRate
                   gFSR:TotalChange                                        = FORMAT(L_CG:R_CostChangeTotal,@n14.2)!gFSR:BaseChange+gFSR:OtherChange
                   gFSR:RevisedFuelBasePercent                             = FORMAT(L_CG:S_RevisedBase,@n14.2)!gFSR:BaseChange/gFSR:TotalChange*100
                   gFSR:RevisedOtherPercent                                = FORMAT(L_CG:T_OtherRevised,@n14.2)!gFSR:OtherRate/gFSR:TotalChange*100
                   gFSR:RevisedTotalPercent                                = FORMAT(L_CG:ST_RevisedTotal,@n14.2)!gFSR:RevisedFuelBasePercent+gFSR:RevisedOtherPercent
                   gFSR:FuelSurcharge                                      = FORMAT(gFSR:TotalChange-100,@n14.2)
                   DocPath=MergeWordReport(gFuelSurchargeReport,WORDREPORT_FuelSurcharge)
                   IF DocPath<>''
                      IF FileDialog('Select path for PDF file',PDFPath,,FILE:Directory+FILE:KeepDir+FILE:LongName)
                          PDFFileName = PDFPath&'\'&FixFileName('Fuel Surcharge '&FORMAT(TODAY(),@d17-)&' - '&clip(CLI:ClientNo)&' '&clip(CLI:ClientName))&'.pdf'
                          IF FileToPDF(DocPath,PDFFileName,'Fuel Surcharge')=Level:Benign
                              ShellExecute(PDFFileName)
                              !Message('Report was merged successfuly!',,ICON:Asterisk)
                          ELSE
                              Message('Can not create PDF!',,ICON:Exclamation)
                          END
                      END
                   ELSE
                      Message('Can not create DOC!',,ICON:Exclamation)
                   END
      
      !!!
    OF ?OK
      ThisWindow.Update()
          ! Calculation fields...
          FCRA:FuelCost           = L_CLP:Y_CostChange
          ! FCRA:FCID               =???  should we bother setting this?
          !FCRA:FCID_CostBase      = L_CLP:X_Cost_As_At
          FCRA:CostBase_X         = L_CLP:X_Cost_As_At
      
          FCRA:FuelBaseRate       = L_CG:M_FuelBaseRate
      
          FCRA:CostBase_Date      = L_CLP:X_CostBase_Date
          FCRA:CostChange_Date    = L_CLP:Y_CostChange_Date
      
      
          ! Used on DI's
          ! 06/06/2012 - change rounding to 2 decimal places, which is as it appears on screen
          FCRA:FuelSurcharge      = ROUND(L_CG:N_IncreaseOnBase, .01)
      
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Window_FuelSurcharge PROCEDURE (p:Type, p:Cur_Cost, p:Eff_Date, p:CID, p:Base_Rate_ID, p:Base_Cost_ID)

gFuelSurchargeReport            GROUP,PRE(gFSR)
ReportHeader                                       CSTRING(256)
Client_Address                                     CSTRING(256)
Run_date                                           CSTRING(256)
Client_No                                          CSTRING(256)
Client_fax_No                                      CSTRING(256)
FuelBaseRate_1stRecord                             CSTRING(256)
FuelBaseRate_As_of_ClientsFuelCostEffectiveDate    CSTRING(256)
ClientsFuelSurcharge_At_RunForEffectiveDate        CSTRING(256)
ClientsFuelSurcharge_EffectiveDate                 CSTRING(256)
FuelCostBaseDate                                   CSTRING(256)
FuelCost_At_FuelCostBaseDate                       CSTRING(256)
FuelCost_At_Run_Date                               CSTRING(256)
CPLChange                                          CSTRING(256)
CPLChangePercent                                   CSTRING(256)
FuelCostBaseDate_Month_Year                        CSTRING(256)
BaseRate                                           CSTRING(256)
BaseIncrease                                       CSTRING(256)
BaseIncreasePercent                                CSTRING(256)
BaseChange                                         CSTRING(256)
OtherRate                                          CSTRING(256)
OtherChange                                        CSTRING(256)
TotalChange                                        CSTRING(256)
RevisedFuelBasePercent                             CSTRING(256)
RevisedOtherPercent                                CSTRING(256)
RevisedTotalPercent                                CSTRING(256)
FuelSurcharge                                      CSTRING(256)
                                 END

DocPath             CString(FILE:MaxFileName)
PDFPath             CString(FILE:MaxFilePath)
PDFFileName         CString(FILE:MaxFileName)
LOC:CPL_Group        GROUP,PRE(L_CLP)                      !
X_CostBase_Date      LONG                                  !
Y_CostChange_Date    LONG                                  !
X_Cost_As_At         DECIMAL(15,4)                         !Cost per litre
Y_CostChange         DECIMAL(15,4)                         !Cost per litre
Z_ChangeInCPL        DECIMAL(17,7)                         !Cost per litre
Q_ChangePercent      DECIMAL(17,7)                         !
                     END                                   !
LOC:Cost_Group       GROUP,PRE(L_CG)                       !
M_FuelBaseRate       DECIMAL(15,4)                         !% cost of total cost
OtherCosts           DECIMAL(15,4)                         !% cost of total cost
TotalCostPer         DECIMAL(15,4,100)                     !% cost of total cost
N_IncreaseOnBase     DECIMAL(15,4)                         !% cost of total cost
O_FuelCostChange     DECIMAL(15,4)                         !% cost of total cost
P_OtherCostChange    DECIMAL(15,4)                         !% cost of total cost
R_CostChangeTotal    DECIMAL(15,4)                         !% cost of total cost
S_RevisedBase        DECIMAL(15,4)                         !% cost of total cost
T_OtherRevised       DECIMAL(15,4)                         !% cost of total cost
ST_RevisedTotal      DECIMAL(15,4)                         !% cost of total cost
                     END                                   !
QuickWindow          WINDOW('Fuel Surcharge'),AT(,,379,184),FONT('Tahoma',8,,FONT:regular),CENTER,GRAY,IMM,MDI, |
  HLP('Window_FuelSurcharge'),SYSTEM
                       SHEET,AT(3,4,373,136),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           PROMPT('Cost Base Date:'),AT(71,23),USE(?CostBase_Date:Prompt),TRN
                           PROMPT('Cost Change Date:'),AT(158,23),USE(?CostChange_Date:Prompt),TRN
                           PROMPT('Change in CPL'),AT(250,23),USE(?ChangeInCPL:Prompt),TRN
                           PROMPT('CPL Change %'),AT(322,23),USE(?ChangePercent:Prompt),TRN
                           BUTTON('...'),AT(58,36,10,10),USE(?Button_CostBaseDate)
                           ENTRY(@d6b),AT(71,36,60,10),USE(L_CLP:X_CostBase_Date),RIGHT(1)
                           BUTTON('...'),AT(143,36,10,10),USE(?Button_CostChangeDate)
                           ENTRY(@d6b),AT(158,36,60,10),USE(L_CLP:Y_CostChange_Date),RIGHT(1)
                           PROMPT('Fuel Cost:'),AT(10,48),USE(?CostBase:Prompt),TRN
                           ENTRY(@n-14.2),AT(71,48,60,10),USE(L_CLP:X_Cost_As_At),RIGHT(1),MSG('Cost per litre'),TIP('Cost per litre')
                           ENTRY(@n-14.2),AT(158,48,60,10),USE(L_CLP:Y_CostChange),RIGHT(1),MSG('Cost per litre'),TIP('Cost per litre')
                           ENTRY(@n-14.2),AT(250,48,47,10),USE(L_CLP:Z_ChangeInCPL),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Cost per litre'), |
  READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@n-13.4~%~),AT(322,48,47,10),USE(L_CLP:Q_ChangePercent),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  READONLY,SKIP
                           PROMPT('Base Rate'),AT(47,76),USE(?Prompt16),TRN
                           PROMPT('CPL Change %'),AT(114,76),USE(?ChangePercent:Prompt:2),TRN
                           PROMPT('Increase'),AT(178,76),USE(?L_CG:IncreaseOnBase:Prompt),TRN
                           PROMPT('Change in Cost'),AT(250,76),USE(?L_CG:FuelCostChange:Prompt),TRN
                           PROMPT('Revised Base'),AT(323,76),USE(?L_CG:RevisedBase:Prompt),TRN
                           PROMPT('Fuel:'),AT(10,88),USE(?L_CG:FuelBaseRate:Prompt),TRN
                           ENTRY(@N-13.4~%~),AT(47,88,47,10),USE(L_CG:M_FuelBaseRate),RIGHT(1),MSG('Cost per litre'), |
  TIP('Cost per litre')
                           PROMPT('Other:'),AT(10,104),USE(?L_CG:OtherCosts:Prompt),TRN
                           ENTRY(@N-13.4~%~),AT(47,104,47,10),USE(L_CG:OtherCosts),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Cost per litre'), |
  READONLY,SKIP,TIP('Cost per litre')
                           PROMPT('Total:'),AT(10,124),USE(?L_CG:TotalCostPer:Prompt),TRN
                           ENTRY(@N-13.4~%~),AT(47,124,47,10),USE(L_CG:TotalCostPer),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Cost per litre'), |
  READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@n-13.4~%~),AT(114,88,47,10),USE(L_CLP:Q_ChangePercent,,?L_CLP:ChangePercent:2),RIGHT(1), |
  COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                           ENTRY(@N-13.4~%~),AT(178,88,47,10),USE(L_CG:N_IncreaseOnBase),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(250,88,47,10),USE(L_CG:O_FuelCostChange),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(250,104,47,10),USE(L_CG:P_OtherCostChange),RIGHT(1),COLOR(00E9E9E9h), |
  FLAT,MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(250,124,47,10),USE(L_CG:R_CostChangeTotal),RIGHT(1),COLOR(00E9E9E9h), |
  FLAT,MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(322,88,47,10),USE(L_CG:S_RevisedBase),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(322,104,47,10),USE(L_CG:T_OtherRevised),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(322,124,47,10),USE(L_CG:ST_RevisedTotal),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                         END
                       END
                       PANEL,AT(3,142,373,20),USE(?Panel1)
                       STRING(@N-13.2~%~),AT(276,148),USE(L_CG:N_IncreaseOnBase,,?L_CG:N_IncreaseOnBase:2),FONT(, |
  ,,FONT:bold,CHARSET:ANSI)
                       PROMPT(' = '),AT(246,148),USE(?Prompt15),FONT(,,,FONT:bold,CHARSET:ANSI)
                       PROMPT('Fuel Surcharge at:'),AT(40,148),USE(?Prompt14),FONT(,,,FONT:bold,CHARSET:ANSI)
                       STRING(@D18B),AT(130,148,96,10),USE(L_CLP:Y_CostChange_Date,,?L_CLP:Y_CostChange_Date:2),FONT(, |
  ,,FONT:bold,CHARSET:ANSI)
                       BUTTON('Create Report'),AT(4,166,59,14),USE(?Report)
                       BUTTON('&OK'),AT(326,166,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Calc_Values         ROUTINE
    L_CG:N_IncreaseOnBase    = Calc_FuelBaseRate(L_CLP:X_Cost_As_At, L_CLP:Y_CostChange, L_CG:M_FuelBaseRate,     |
                                      L_CLP:Z_ChangeInCPL, L_CLP:Q_ChangePercent,                         |
                                      L_CG:O_FuelCostChange, L_CG:S_RevisedBase, L_CG:T_OtherRevised)


    L_CG:OtherCosts          = 100 - L_CG:M_FuelBaseRate
    L_CG:TotalCostPer        = 100

    L_CG:P_OtherCostChange   = 100 - L_CG:M_FuelBaseRate
    L_CG:R_CostChangeTotal   = L_CG:P_OtherCostChange + L_CG:O_FuelCostChange

    L_CG:T_OtherRevised      = L_CG:P_OtherCostChange / L_CG:R_CostChangeTotal * 100
    L_CG:ST_RevisedTotal     = L_CG:S_RevisedBase + L_CG:T_OtherRevised

    DISPLAY
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Window_FuelSurcharge')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CostBase_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  Relate:_FuelBaseRate.Open                                ! File _FuelBaseRate used by this procedure, so make sure it's RelationManager is open
  Relate:_FuelCost.SetOpenRelated()
  Relate:_FuelCost.Open                                    ! File _FuelCost used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Window_FuelSurcharge',QuickWindow)         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_FuelBaseRate.Close
    Relate:_FuelCost.Close
  END
  IF SELF.Opened
    INIMgr.Update('Window_FuelSurcharge',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_CostBaseDate
      ThisWindow.Update()
          GlobalRequest               = SelectRecord
      
          IF L_CLP:X_CostBase_Date = 0
             FUE:EffectiveDate           = 0
          ELSE
             FUE:EffectiveDate           = L_CLP:X_CostBase_Date
          .
          IF Access:_FuelCost.TryFetch(FUE:SKey_EffectiveDate) = LEVEL:Benign
          .
      
          Browse_FuelCost(0)
          IF GlobalResponse = RequestCompleted
             L_CLP:X_CostBase_Date    = FUE:EffectiveDate
             L_CG:M_FuelBaseRate      = FUE:FuelBaseRate
          .
      
          L_CLP:X_Cost_As_At          = Get_FuelCost(L_CLP:X_CostBase_Date)
      
          DO Calc_Values
    OF ?Button_CostChangeDate
      ThisWindow.Update()
          ! FCRA:FuelCost   = Get_FuelCost(FCRA:Effective_Date)
          GlobalRequest               = SelectRecord
      
          IF L_CLP:Y_CostChange_Date = 0
             FUE:EffectiveDate           = TODAY()
          ELSE
             FUE:EffectiveDate           = L_CLP:Y_CostChange_Date
          .
          IF Access:_FuelCost.TryFetch(FUE:SKey_EffectiveDate) = LEVEL:Benign
          .
      
          Browse_FuelCost(0)
          IF GlobalResponse = RequestCompleted
             L_CLP:Y_CostChange_Date  = FUE:EffectiveDate
          .
      
          L_CLP:Y_CostChange          = Get_FuelCost(L_CLP:Y_CostChange_Date)
      
          DO Calc_Values
    OF ?L_CLP:X_Cost_As_At
          DO Calc_Values
    OF ?L_CLP:Y_CostChange
          DO Calc_Values
    OF ?L_CG:M_FuelBaseRate
          DO Calc_Values
    OF ?Report
      ThisWindow.Update()
      !!!! Eugene !!!
                   gFSR:Client_Address                                     = Get_Address(CLI:AID,1,0)
                   gFSR:Run_date                                           = FORMAT(L_CLP:Y_CostChange_Date,@d17)
                   gFSR:Client_No                                          = CLI:ClientNo
                   ADD:AID = CLI:AID
                   IF Access:Addresses.Fetch(ADD:PKey_AID)=Level:Benign
                      gFSR:Client_fax_No                                   = clip(ADD:Fax)
                   ELSE
                      gFSR:Client_fax_No                                   = ''
                   END
                   gFSR:FuelBaseRate_1stRecord                             = 'FuelBaseRate_1stRecord - ??'
                   gFSR:FuelBaseRate_As_of_ClientsFuelCostEffectiveDate    = L_CLP:X_Cost_As_At !????
                   gFSR:ClientsFuelSurcharge_At_RunForEffectiveDate        = 'ClientsFuelSurcharge_At_RunForEffectiveDate - ??'
                   gFSR:ClientsFuelSurcharge_EffectiveDate                 = FORMAT(FCRA:Effective_Date,@d17)
                   gFSR:FuelCostBaseDate                                   = FORMAT(L_CLP:X_CostBase_Date,@d17)
                   gFSR:FuelCost_At_FuelCostBaseDate                       = FORMAT(L_CLP:X_Cost_As_At,@n14.2)
                   gFSR:FuelCost_At_Run_Date                               = FORMAT(L_CLP:Y_CostChange,@n14.2)
                   gFSR:CPLChange                                          = FORMAT(L_CLP:Z_ChangeInCPL,@n14.2)!gFSR:FuelCost_At_Run_Date-gFSR:FuelCost_At_FuelCostBaseDate
                   gFSR:CPLChangePercent                                   = FORMAT(L_CLP:Q_ChangePercent,@n14.2)!gFSR:CPLChange/gFSR:FuelCost_At_FuelCostBaseDate * 100
                   gFSR:FuelCostBaseDate_Month_Year                        = CHOOSE(MONTH(gFSR:FuelCostBaseDate),'JAN','FEB','MAR','APR','MAY','JUN',|
                                                                                                                 'JUL','AUG','SEP','OCT','NOV','DEC','')&|
                                                                            ' '&YEAR(gFSR:FuelCostBaseDate)
                   gFSR:BaseRate                                           = FORMAT(L_CG:M_FuelBaseRate,@n14.2)
                   gFSR:BaseIncrease                                       = FORMAT(L_CG:N_IncreaseOnBase,@n14.2)!gFSR:BaseRate*gFSR:CPLChangePercent/100
                   gFSR:BaseIncreasePercent                                = FORMAT(gFSR:BaseIncrease*gFSR:BaseRate/100,@n14.2)
                   gFSR:BaseChange                                         = FORMAT(L_CG:O_FuelCostChange,@n14.2)!gFSR:BaseRate+gFSR:BaseIncrease
                   gFSR:OtherRate                                          = FORMAT(L_CG:OtherCosts,@n14.2)!100-gFSR:BaseRate
                   gFSR:OtherChange                                        = FORMAT(L_CG:P_OtherCostChange,@n14.2)!gFSR:OtherRate
                   gFSR:TotalChange                                        = FORMAT(L_CG:R_CostChangeTotal,@n14.2)!gFSR:BaseChange+gFSR:OtherChange
                   gFSR:RevisedFuelBasePercent                             = FORMAT(L_CG:S_RevisedBase,@n14.2)!gFSR:BaseChange/gFSR:TotalChange*100
                   gFSR:RevisedOtherPercent                                = FORMAT(L_CG:T_OtherRevised,@n14.2)!gFSR:OtherRate/gFSR:TotalChange*100
                   gFSR:RevisedTotalPercent                                = FORMAT(L_CG:ST_RevisedTotal,@n14.2)!gFSR:RevisedFuelBasePercent+gFSR:RevisedOtherPercent
                   gFSR:FuelSurcharge                                      = FORMAT(gFSR:TotalChange-100,@n14.2)
                   DocPath=MergeWordReport(gFuelSurchargeReport,WORDREPORT_FuelSurcharge)
                   IF DocPath<>''
                      IF FileDialog('Select path for PDF file',PDFPath,,FILE:Directory+FILE:KeepDir+FILE:LongName)
                          PDFFileName = PDFPath&'\'&FixFileName('Fuel Surcharge '&FORMAT(TODAY(),@d17-)&' - '&clip(CLI:ClientNo)&' '&clip(CLI:ClientName))&'.pdf'
                          IF FileToPDF(DocPath,PDFFileName,'Fuel Surcharge')=Level:Benign
                              ShellExecute(PDFFileName)
                              !Message('Report was merged successfuly!',,ICON:Asterisk)
                          ELSE
                              Message('Can not create PDF!',,ICON:Exclamation)
                          END
                      END
                   ELSE
                      Message('Can not create DOC!',,ICON:Exclamation)
                   END
      
      !!!
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          ! (p:Type, p:Cur_Cost, p:Eff_Date, p:CID, p:Base_Rate_ID, p:Base_Cost_ID)
          ! (BYTE, *DECIMAL, LONG, <ULONG>, <ULONG>, <ULONG>)
      
          L_CLP:Y_CostChange       = p:Cur_Cost
          L_CLP:Y_CostChange_Date  = p:Eff_Date
          IF p:Type = 0                       ! Call from Update_RatesFuelCost
             ! Window_FuelSurcharge(0, FCRA:FuelCost, FCRA:Effective_Date, FCRA:CID, FCRA:FCID)
             ! (p:Type, p:Cur_Cost, p:Eff_Date, p:CID, p:Base_Rate_ID, p:Base_Cost_ID)
             ! (BYTE  , *DECIMAL  , LONG      , <ULONG>, <ULONG>, <ULONG>)
      
             FUE:FCID     = p:Base_Rate_ID
             IF Access:_FuelCost.TryFetch(FUE:PKey_FCID) = LEVEL:Benign
                L_CG:M_FuelBaseRate   = FUE:FuelBaseRate
             .
      
             FUE:FCID     = p:Base_Cost_ID
             IF Access:_FuelCost.TryFetch(FUE:PKey_FCID) = LEVEL:Benign
                L_CLP:X_Cost_As_At    = FUE:FuelCost
                L_CLP:X_CostBase_Date = FUE:EffectiveDate
             .
      
      
             ! (p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Cur_Eff_Date,
             !     p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base, p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per)
             ! (ULONG, ULONG, *DECIMAL, *LONG,
             !    <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING
      
      
             ! (p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Cur_Eff_Date, p:X_Fuel_Cost, p:X_Eff_Date, p:M_Fuel_Base_Rate, p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base, p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per)
             ! (ULONG, ULONG, *DECIMAL, LONG, <*DECIMAL>, <*LONG>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING
             !Get_Client_FuelSurcharge_Calcs(p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Eff_Date, L_CLP:X_Cost_As_At, L_CLP:X_CostBase_Date, L_CG:M_FuelBaseRate, L_CLP:Z_ChangeInCPL, L_CLP:Q_ChangePercent, L_CG:N_IncreaseOnBase, L_CG:O_FuelCostChange, L_CG:S_RevisedBase, L_CG:T_OtherRevised)
      
             L_CG:N_IncreaseOnBase    = Calc_FuelBaseRate(L_CLP:X_Cost_As_At, L_CLP:Y_CostChange, L_CG:M_FuelBaseRate,     |
                                               L_CLP:Z_ChangeInCPL, L_CLP:Q_ChangePercent,                         |
                                               L_CG:O_FuelCostChange, L_CG:S_RevisedBase, L_CG:T_OtherRevised)
          .
      
      
      
      
      
      !L_CG:M_FuelBaseRate
      !L_CG:O_FuelCostChange
      !L_CG:S_RevisedBase
          DO Calc_Values
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Window_FuelSurcharge_old2 PROCEDURE (p:Type, p:Cur_Cost, p:Eff_Date, p:CID, p:Base_Rate_ID, p:Base_Cost_ID)

gFuelSurchargeReport            GROUP,PRE(gFSR)
ReportHeader                                       CSTRING(256)
Client_Address                                     CSTRING(256)
Run_date                                           CSTRING(256)
Client_No                                          CSTRING(256)
Client_fax_No                                      CSTRING(256)
FuelBaseRate_1stRecord                             CSTRING(256)
FuelBaseRate_As_of_ClientsFuelCostEffectiveDate    CSTRING(256)
ClientsFuelSurcharge_At_RunForEffectiveDate        CSTRING(256)
ClientsFuelSurcharge_EffectiveDate                 CSTRING(256)
FuelCostBaseDate                                   CSTRING(256)
FuelCost_At_FuelCostBaseDate                       CSTRING(256)
FuelCost_At_Run_Date                               CSTRING(256)
CPLChange                                          CSTRING(256)
CPLChangePercent                                   CSTRING(256)
FuelCostBaseDate_Month_Year                        CSTRING(256)
BaseRate                                           CSTRING(256)
BaseIncrease                                       CSTRING(256)
BaseIncreasePercent                                CSTRING(256)
BaseChange                                         CSTRING(256)
OtherRate                                          CSTRING(256)
OtherChange                                        CSTRING(256)
TotalChange                                        CSTRING(256)
RevisedFuelBasePercent                             CSTRING(256)
RevisedOtherPercent                                CSTRING(256)
RevisedTotalPercent                                CSTRING(256)
FuelSurcharge                                      CSTRING(256)
                                 END

DocPath             CString(FILE:MaxFileName)
PDFPath             CString(FILE:MaxFilePath)
PDFFileName         CString(FILE:MaxFileName)
LOC:CPL_Group        GROUP,PRE(L_CLP)                      !
X_CostBase_Date      LONG                                  !
Y_CostChange_Date    LONG                                  !
X_Cost_As_At         DECIMAL(15,4)                         !Cost per litre
Y_CostChange         DECIMAL(15,4)                         !Cost per litre
Z_ChangeInCPL        DECIMAL(17,7)                         !Cost per litre
Q_ChangePercent      DECIMAL(17,7)                         !
                     END                                   !
LOC:Cost_Group       GROUP,PRE(L_CG)                       !
M_FuelBaseRate       DECIMAL(15,4)                         !% cost of total cost
OtherCosts           DECIMAL(15,4)                         !% cost of total cost
TotalCostPer         DECIMAL(15,4,100)                     !% cost of total cost
N_IncreaseOnBase     DECIMAL(15,4)                         !% cost of total cost
O_FuelCostChange     DECIMAL(15,4)                         !% cost of total cost
P_OtherCostChange    DECIMAL(15,4)                         !% cost of total cost
R_CostChangeTotal    DECIMAL(15,4)                         !% cost of total cost
S_RevisedBase        DECIMAL(15,4)                         !% cost of total cost
T_OtherRevised       DECIMAL(15,4)                         !% cost of total cost
ST_RevisedTotal      DECIMAL(15,4)                         !% cost of total cost
                     END                                   !
FCID_CostBase        ULONG                                 !Fuel Cost ID - used for Cost Base
QuickWindow          WINDOW('Fuel Surcharge'),AT(,,379,161),FONT('Tahoma',8,,FONT:regular),CENTER,GRAY,IMM,MDI, |
  HLP('Window_FuelSurcharge'),SYSTEM
                       SHEET,AT(3,4,373,136),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           PROMPT('Cost Base Date:'),AT(67,23),USE(?CostBase_Date:Prompt),TRN
                           PROMPT('Cost Change Date:'),AT(154,23),USE(?CostChange_Date:Prompt),TRN
                           PROMPT('Change in CPL'),AT(247,23),USE(?ChangeInCPL:Prompt),TRN
                           PROMPT('CPL Change %'),AT(318,23),USE(?ChangePercent:Prompt),TRN
                           BUTTON('...'),AT(54,36,10,10),USE(?Button_CostBaseDate)
                           ENTRY(@d6b),AT(67,36,60,10),USE(L_CLP:X_CostBase_Date),CENTER,COLOR(00E9E9E9h),FLAT,READONLY
                           ENTRY(@d6b),AT(154,36,60,10),USE(L_CLP:Y_CostChange_Date),CENTER,COLOR(00E9E9E9h),FLAT,READONLY
                           PROMPT('Fuel Cost:'),AT(10,48),USE(?CostBase:Prompt),TRN
                           ENTRY(@n-14.3),AT(67,48,60,10),USE(L_CLP:X_Cost_As_At),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Cost per litre'), |
  READONLY,TIP('Cost per litre')
                           ENTRY(@n-14.3),AT(154,48,60,10),USE(L_CLP:Y_CostChange),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Cost per litre'), |
  READONLY,TIP('Cost per litre')
                           ENTRY(@n-14.3),AT(246,48,47,10),USE(L_CLP:Z_ChangeInCPL),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Cost per litre'), |
  READONLY,TIP('Cost per litre')
                           ENTRY(@n-13.4~%~),AT(318,48,47,10),USE(L_CLP:Q_ChangePercent),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  READONLY
                           PROMPT('Base Rate'),AT(43,76),USE(?Prompt16),TRN
                           PROMPT('CPL Change %'),AT(110,76),USE(?ChangePercent:Prompt:2),TRN
                           PROMPT('Increase'),AT(174,76),USE(?L_CG:IncreaseOnBase:Prompt),TRN
                           PROMPT('Change in Cost'),AT(246,76),USE(?L_CG:FuelCostChange:Prompt),TRN
                           PROMPT('Revised Base'),AT(322,76),USE(?L_CG:RevisedBase:Prompt),TRN
                           PROMPT('Fuel:'),AT(10,88),USE(?L_CG:FuelBaseRate:Prompt),TRN
                           ENTRY(@N-13.4~%~),AT(43,88,47,10),USE(L_CG:M_FuelBaseRate),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,TIP('Cost per litre')
                           PROMPT('Other:'),AT(10,104),USE(?L_CG:OtherCosts:Prompt),TRN
                           ENTRY(@N-13.4~%~),AT(43,104,47,10),USE(L_CG:OtherCosts),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Cost per litre'), |
  READONLY,TIP('Cost per litre')
                           PROMPT('Total:'),AT(10,124),USE(?L_CG:TotalCostPer:Prompt),TRN
                           ENTRY(@N-13.4~%~),AT(43,124,47,10),USE(L_CG:TotalCostPer),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Cost per litre'), |
  READONLY,TIP('Cost per litre')
                           ENTRY(@n-13.4~%~),AT(110,88,47,10),USE(L_CLP:Q_ChangePercent,,?L_CLP:ChangePercent:2),RIGHT(1), |
  COLOR(00E9E9E9h),FLAT,READONLY
                           ENTRY(@N-13.4~%~),AT(174,88,47,10),USE(L_CG:N_IncreaseOnBase),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(246,88,47,10),USE(L_CG:O_FuelCostChange),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(246,104,47,10),USE(L_CG:P_OtherCostChange),RIGHT(1),COLOR(00E9E9E9h), |
  FLAT,MSG('Cost per litre'),READONLY,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(246,124,47,10),USE(L_CG:R_CostChangeTotal),RIGHT(1),COLOR(00E9E9E9h), |
  FLAT,MSG('Cost per litre'),READONLY,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(318,88,47,10),USE(L_CG:S_RevisedBase),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(318,104,47,10),USE(L_CG:T_OtherRevised),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,TIP('Cost per litre')
                           ENTRY(@N-13.4~%~),AT(318,124,47,10),USE(L_CG:ST_RevisedTotal),RIGHT(1),COLOR(00E9E9E9h),FLAT, |
  MSG('Cost per litre'),READONLY,TIP('Cost per litre')
                         END
                       END
                       BUTTON('Create Report'),AT(4,144,59,14),USE(?Report)
                       BUTTON('&OK'),AT(326,144,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Window_FuelSurcharge_old2')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CostBase_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  Relate:_FuelBaseRate.Open                                ! File _FuelBaseRate used by this procedure, so make sure it's RelationManager is open
  Relate:_FuelCost.SetOpenRelated()
  Relate:_FuelCost.Open                                    ! File _FuelCost used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_FuelBaseRate.Close
    Relate:_FuelCost.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Browse_FuelCost(('0'))
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_CLP:X_CostBase_Date
      IF L_CLP:X_CostBase_Date OR ?L_CLP:X_CostBase_Date{PROP:Req}
        FUE:EffectiveDate = L_CLP:X_CostBase_Date
        IF Access:_FuelCost.TryFetch(FUE:SKey_EffectiveDate)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_CLP:X_CostBase_Date = FUE:EffectiveDate
            FCID_CostBase = FUE:FCID
          ELSE
            CLEAR(FCID_CostBase)
            SELECT(?L_CLP:X_CostBase_Date)
            CYCLE
          END
        ELSE
          FCID_CostBase = FUE:FCID
        END
      END
      ThisWindow.Reset()
    OF ?Report
      ThisWindow.Update()
      !!!! Eugene !!!
                   gFSR:Client_Address                                     = Get_Address(CLI:AID,1,0)
                   gFSR:Run_date                                           = FORMAT(L_CLP:Y_CostChange_Date,@d17)
                   gFSR:Client_No                                          = CLI:ClientNo
                   ADD:AID = CLI:AID
                   IF Access:Addresses.Fetch(ADD:PKey_AID)=Level:Benign
                      gFSR:Client_fax_No                                   = clip(ADD:Fax)
                   ELSE
                      gFSR:Client_fax_No                                   = ''
                   END
                   gFSR:FuelBaseRate_1stRecord                             = 'FuelBaseRate_1stRecord - ??'
                   gFSR:FuelBaseRate_As_of_ClientsFuelCostEffectiveDate    = L_CLP:X_Cost_As_At !????
                   gFSR:ClientsFuelSurcharge_At_RunForEffectiveDate        = 'ClientsFuelSurcharge_At_RunForEffectiveDate - ??'
                   gFSR:ClientsFuelSurcharge_EffectiveDate                 = FORMAT(FCRA:Effective_Date,@d17)
                   gFSR:FuelCostBaseDate                                   = FORMAT(L_CLP:X_CostBase_Date,@d17)
                   gFSR:FuelCost_At_FuelCostBaseDate                       = FORMAT(L_CLP:X_Cost_As_At,@n14.2)
                   gFSR:FuelCost_At_Run_Date                               = FORMAT(L_CLP:Y_CostChange,@n14.2)
                   gFSR:CPLChange                                          = FORMAT(L_CLP:Z_ChangeInCPL,@n14.2)!gFSR:FuelCost_At_Run_Date-gFSR:FuelCost_At_FuelCostBaseDate
                   gFSR:CPLChangePercent                                   = FORMAT(L_CLP:Q_ChangePercent,@n14.2)!gFSR:CPLChange/gFSR:FuelCost_At_FuelCostBaseDate * 100
                   gFSR:FuelCostBaseDate_Month_Year                        = CHOOSE(MONTH(gFSR:FuelCostBaseDate),'JAN','FEB','MAR','APR','MAY','JUN',|
                                                                                                                 'JUL','AUG','SEP','OCT','NOV','DEC','')&|
                                                                            ' '&YEAR(gFSR:FuelCostBaseDate)
                   gFSR:BaseRate                                           = FORMAT(L_CG:M_FuelBaseRate,@n14.2)
                   gFSR:BaseIncrease                                       = FORMAT(L_CG:N_IncreaseOnBase,@n14.2)!gFSR:BaseRate*gFSR:CPLChangePercent/100
                   gFSR:BaseIncreasePercent                                = FORMAT(gFSR:BaseIncrease*gFSR:BaseRate/100,@n14.2)
                   gFSR:BaseChange                                         = FORMAT(L_CG:O_FuelCostChange,@n14.2)!gFSR:BaseRate+gFSR:BaseIncrease
                   gFSR:OtherRate                                          = FORMAT(L_CG:OtherCosts,@n14.2)!100-gFSR:BaseRate
                   gFSR:OtherChange                                        = FORMAT(L_CG:P_OtherCostChange,@n14.2)!gFSR:OtherRate
                   gFSR:TotalChange                                        = FORMAT(L_CG:R_CostChangeTotal,@n14.2)!gFSR:BaseChange+gFSR:OtherChange
                   gFSR:RevisedFuelBasePercent                             = FORMAT(L_CG:S_RevisedBase,@n14.2)!gFSR:BaseChange/gFSR:TotalChange*100
                   gFSR:RevisedOtherPercent                                = FORMAT(L_CG:T_OtherRevised,@n14.2)!gFSR:OtherRate/gFSR:TotalChange*100
                   gFSR:RevisedTotalPercent                                = FORMAT(L_CG:ST_RevisedTotal,@n14.2)!gFSR:RevisedFuelBasePercent+gFSR:RevisedOtherPercent
                   gFSR:FuelSurcharge                                      = FORMAT(gFSR:TotalChange-100,@n14.2)
                   DocPath=MergeWordReport(gFuelSurchargeReport,WORDREPORT_FuelSurcharge)
                   IF DocPath<>''
                      IF FileDialog('Select path for PDF file',PDFPath,,FILE:Directory+FILE:KeepDir+FILE:LongName)
                          PDFFileName = PDFPath&'\'&FixFileName('Fuel Surcharge '&FORMAT(TODAY(),@d17-)&' - '&clip(CLI:ClientNo)&' '&clip(CLI:ClientName))&'.pdf'
                          IF FileToPDF(DocPath,PDFFileName,'Fuel Surcharge')=Level:Benign
                              ShellExecute(PDFFileName)
                              !Message('Report was merged successfuly!',,ICON:Asterisk)
                          ELSE
                              Message('Can not create PDF!',,ICON:Exclamation)
                          END
                      END
                   ELSE
                      Message('Can not create DOC!',,ICON:Exclamation)
                   END
      
      !!!
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          ! (p:Type, p:Cur_Cost, p:Eff_Date, p:CID, p:Base_Rate_ID, p:Base_Cost_ID)
          ! (BYTE, *DECIMAL, LONG, <ULONG>, <ULONG>, <ULONG>)
      
          L_CLP:Y_CostChange       = p:Cur_Cost
          L_CLP:Y_CostChange_Date  = p:Eff_Date
          IF p:Type = 0                       ! Call from Update_RatesFuelCost
             ! Window_FuelSurcharge(0, FCRA:FuelCost, FCRA:Effective_Date, FCRA:CID, FCRA:FCID)
             ! (p:Type, p:Cur_Cost, p:Eff_Date, p:CID, p:Base_Rate_ID, p:Base_Cost_ID)
             ! (BYTE  , *DECIMAL  , LONG      , <ULONG>, <ULONG>, <ULONG>)
      
             FUE:FCID     = p:Base_Rate_ID
             IF Access:_FuelCost.TryFetch(FUE:PKey_FCID) = LEVEL:Benign
                L_CG:M_FuelBaseRate   = FUE:FuelBaseRate
             .
      
             FUE:FCID     = p:Base_Cost_ID
             IF Access:_FuelCost.TryFetch(FUE:PKey_FCID) = LEVEL:Benign
                L_CLP:X_Cost_As_At    = FUE:FuelCost
                L_CLP:X_CostBase_Date = FUE:EffectiveDate
             .
      
      
             ! (p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Cur_Eff_Date,
             !     p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base, p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per)
             ! (ULONG, ULONG, *DECIMAL, *LONG,
             !    <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING
      
      
             ! (p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Cur_Eff_Date, p:X_Fuel_Cost, p:X_Eff_Date, p:M_Fuel_Base_Rate, p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base, p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per)
             ! (ULONG, ULONG, *DECIMAL, LONG, <*DECIMAL>, <*LONG>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING
             !Get_Client_FuelSurcharge_Calcs(p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Eff_Date, L_CLP:X_Cost_As_At, L_CLP:X_CostBase_Date, L_CG:M_FuelBaseRate, L_CLP:Z_ChangeInCPL, L_CLP:Q_ChangePercent, L_CG:N_IncreaseOnBase, L_CG:O_FuelCostChange, L_CG:S_RevisedBase, L_CG:T_OtherRevised)
      
             L_CG:N_IncreaseOnBase    = Calc_FuelBaseRate(L_CLP:X_Cost_As_At, L_CLP:Y_CostChange, L_CG:M_FuelBaseRate,     |
                                               L_CLP:Z_ChangeInCPL, L_CLP:Q_ChangePercent,                         |
                                               L_CG:O_FuelCostChange, L_CG:S_RevisedBase, L_CG:T_OtherRevised)
          .
          IF p:Type = 1                       ! Call from Update_FuelCost
             ! (p:Date, p:Eff_Date, p:BaseRate)
             ! (LONG, <*LONG>, <*DECIMAL>)
             L_CLP:X_Cost_As_At       = Get_FuelCost(L_CLP:Y_CostChange_Date - 1, L_CLP:X_CostBase_Date, L_CG:M_FuelBaseRate)
      
             ! (p:X_Fuel_Cost, p:Y_Fuel_Cost, p:M_Fuel_Base_Rate, p:Z_CPL_Change, p:Q_CPL_Change_Per,
             !    p:N_Increase_On_Base, p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per)
             ! (*DECIMAL, *DECIMAL, *DECIMAL, <*DECIMAL>, <*DECIMAL>,
             !    <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING
      
             L_CG:N_IncreaseOnBase  = Calc_FuelBaseRate(L_CLP:X_Cost_As_At, L_CLP:Y_CostChange, L_CG:M_FuelBaseRate, |
                                  L_CLP:Z_ChangeInCPL, L_CLP:Q_ChangePercent, |
                                  L_CG:O_FuelCostChange, L_CG:S_RevisedBase, L_CG:T_OtherRevised)
          .
             L_CG:OtherCosts          = 100 - L_CG:M_FuelBaseRate
             L_CG:TotalCostPer        = 100
      
             L_CG:P_OtherCostChange   = 100 - L_CG:M_FuelBaseRate
             L_CG:R_CostChangeTotal   = L_CG:P_OtherCostChange + L_CG:O_FuelCostChange
      
             L_CG:T_OtherRevised      = L_CG:P_OtherCostChange / L_CG:R_CostChangeTotal * 100
             L_CG:ST_RevisedTotal     = L_CG:S_RevisedBase + L_CG:T_OtherRevised
      
          DISPLAY
      !       old
      
      
      
      
      !    ! (p_FuelCost, p_FuelCost_Effective_Date, p:Effective_Date)
      !    ! (*DECIMAL, LONG, LONG)
      !    !FCRA:FCID   = p:FCID
      !    !IF Access:__RatesFuelCost.TryFetch(FCRA:PKey_FCID) = LEVEL:Benign
      !
      !       ! Get_Client_FuelBaseRate
      !
      !    ! (p:FuelCost, p:FuelCost_Date, p:Increase_OnBase, p:As_At_Date, p:Clients_FuelBaseRate, p:Cost_As_At, p:FuelCost_Diff,
      !    !    p:CPL_Change_Per, p:FuelBaseRate, p:O)
      !    ! (*DECIMAL, LONG, *DECIMAL, LONG=0, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>)
      !
      !       Get_Client_FuelBaseRate(p_FuelCost, p_FuelCost_Effective_Date, L_CG:IncreaseOnBase, p:Effective_Date, |
      !                L_CG:RevisedBase, L_CLP:Cost_As_At, L_CLP:ChangeInCPL, L_CLP:ChangePercent, L_CG:FuelBaseRate, |
      !                L_CG:FuelCostChange)
      !
      !       L_CLP:ChangePercent      = L_CLP:ChangePercent * 100
      !
      !       L_CLP:CostChange_Date    = p_FuelCost_Effective_Date
      !       L_CLP:CostBase_Date      = p:Effective_Date
      !
      !       L_CLP:CostChange         = p_FuelCost                            ! Y
      !       ! L_CLP:ChangeInCPL        = FCRA:FuelCost - L_CLP:CostBase        ! Z
      !       ! L_CLP:ChangePercent                                              ! Q
      !
      !       L_CG:TotalCostPer        = 100
      !       L_CG:OtherCosts          = 100 - L_CG:FuelBaseRate               !
      !
      !       ! L_CG:IncreaseOnBase                                            ! N
      !       ! L_CG:FuelBaseRate                                              ! M
      !       L_CG:OtherCostChange     = 100 - L_CG:FuelBaseRate               ! P
      !       L_CG:CostChangeTotal     = L_CG:FuelCostChange + L_CG:OtherCostChange    ! R
      !
      !       L_CG:OtherRevised        = L_CG:OtherCostChange / L_CG:CostChangeTotal * 100
      !       L_CG:RevisedTotal        = L_CG:RevisedBase + L_CG:OtherRevised
      !    !.
      !
      !                                              
      !
      !    DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

