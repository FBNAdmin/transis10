

   MEMBER('M2TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M2TRN003.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_FuelBaseRate PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::FUB:Record  LIKE(FUB:RECORD),THREAD
QuickWindow          WINDOW('Form Fuel Base Rate'),AT(,,224,84),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('Update_FuelBaseRate'),SYSTEM
                       SHEET,AT(4,4,216,58),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Description:'),AT(9,20),USE(?FUB:Description:Prompt),TRN
                           ENTRY(@s35),AT(73,20,144,10),USE(FUB:Description)
                           PROMPT('Fuel Base Rate:'),AT(9,34),USE(?FUB:FuelBaseRate:Prompt),TRN
                           ENTRY(@N-13.4~%~),AT(73,34,52,10),USE(FUB:FuelBaseRate),RIGHT(1),MSG('Cost per litre'),TIP('Cost per litre')
                           PROMPT('Effective Date:'),AT(9,48),USE(?FUB:EffectiveDate:Prompt),TRN
                           SPIN(@D6B),AT(73,48,52,10),USE(FUB:EffectiveDate),RIGHT(1)
                           BUTTON('...'),AT(129,48,12,10),USE(?Calendar)
                         END
                       END
                       BUTTON('&OK'),AT(118,66,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(172,66,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,66,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar7            CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Fuel Base Rate a Record'
  OF InsertRecord
    ActionMessage = 'Fuel Base Rate Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Fuel Base Rate Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_FuelBaseRate')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?FUB:Description:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:_FuelBaseRate)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(FUB:Record,History::FUB:Record)
  SELF.AddHistoryField(?FUB:Description,2)
  SELF.AddHistoryField(?FUB:FuelBaseRate,3)
  SELF.AddHistoryField(?FUB:EffectiveDate,6)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:_FuelBaseRate.Open                                ! File _FuelBaseRate used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_FuelBaseRate
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?FUB:Description{PROP:ReadOnly} = True
    ?FUB:FuelBaseRate{PROP:ReadOnly} = True
    DISABLE(?Calendar)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_FuelBaseRate',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_FuelBaseRate.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_FuelBaseRate',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar7.SelectOnClose = True
      Calendar7.Ask('Select a Date',FUB:EffectiveDate)
      IF Calendar7.Response = RequestCompleted THEN
      FUB:EffectiveDate=Calendar7.SelectedDate
      DISPLAY(?FUB:EffectiveDate)
      END
      ThisWindow.Reset(True)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_FuelBaseRate PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(_FuelBaseRate)
                       PROJECT(FUB:Description)
                       PROJECT(FUB:FuelBaseRate)
                       PROJECT(FUB:EffectiveDate)
                       PROJECT(FUB:FBRID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
FUB:Description        LIKE(FUB:Description)          !List box control field - type derived from field
FUB:FuelBaseRate       LIKE(FUB:FuelBaseRate)         !List box control field - type derived from field
FUB:EffectiveDate      LIKE(FUB:EffectiveDate)        !List box control field - type derived from field
FUB:FBRID              LIKE(FUB:FBRID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Fuel Base Rate file'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Browse_FuelBaseRate'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('130L|M~Description~@s35@60R(1)|M~' & |
  'Fuel Base Rate~C(0)@N-13.4~%~@80R(1)|M~Effective Date~C(0)@D6B@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the _FuelBaseRate file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Fuel Base Rate'),USE(?Tab:2)
                         END
                         TAB('&2) By Effective Date'),USE(?Tab:3)
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_FuelBaseRate')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:_FuelBaseRate.Open                                ! File _FuelBaseRate used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_FuelBaseRate,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,FUB:SKey_EffectiveDate)               ! Add the sort order for FUB:SKey_EffectiveDate for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,FUB:EffectiveDate,1,BRW1)      ! Initialize the browse locator using  using key: FUB:SKey_EffectiveDate , FUB:EffectiveDate
  BRW1.AddSortOrder(,FUB:PKey_FBRID)                       ! Add the sort order for FUB:PKey_FBRID for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,FUB:FBRID,1,BRW1)              ! Initialize the browse locator using  using key: FUB:PKey_FBRID , FUB:FBRID
  BRW1.AddField(FUB:Description,BRW1.Q.FUB:Description)    ! Field FUB:Description is a hot field or requires assignment from browse
  BRW1.AddField(FUB:FuelBaseRate,BRW1.Q.FUB:FuelBaseRate)  ! Field FUB:FuelBaseRate is a hot field or requires assignment from browse
  BRW1.AddField(FUB:EffectiveDate,BRW1.Q.FUB:EffectiveDate) ! Field FUB:EffectiveDate is a hot field or requires assignment from browse
  BRW1.AddField(FUB:FBRID,BRW1.Q.FUB:FBRID)                ! Field FUB:FBRID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_FuelBaseRate',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_FuelBaseRate.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_FuelBaseRate',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_FuelBaseRate
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! ** empty **
!!! </summary>
Main_M2              PROCEDURE                             ! Declare Procedure

  CODE
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_FuelCost PROCEDURE (p_Type)

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
FuelCost             DECIMAL(10,3)                         !Cost per litre
FuelBaseRate         DECIMAL(10,4)                         !% cost of total cost
Use_FuelCost         BYTE                                  !
LOC:CalcBaseRate     BYTE                                  !
History::FUE:Record  LIKE(FUE:RECORD),THREAD
QuickWindow          WINDOW('Form Fuel Cost'),AT(,,237,212),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('Update_FuelCost'),SYSTEM
                       SHEET,AT(2,4,232,189),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Fuel Cost Type:'),AT(8,22),USE(?FUE:FuelCostType:Prompt),TRN
                           LIST,AT(68,22,60,10),USE(FUE:FuelCostType),DROP(5),FROM('Standard|#0|Client|#1'),MSG('Type of th' & |
  'is Fuel Cost'),TIP('Type of this Fuel Cost<0DH,0AH>Standard or Client specific entry')
                           PROMPT('Fuel Cost (cents):'),AT(8,42),USE(?FUE:FuelCost:Prompt),TRN
                           ENTRY(@n-14.3),AT(68,42,60,10),USE(FUE:FuelCost),RIGHT(1),MSG('Cost per litre'),TIP('Cost per litre')
                           PROMPT('Effective Date:'),AT(8,58),USE(?FUE:EffectiveDate:Prompt),TRN
                           SPIN(@D6B),AT(68,58,60,10),USE(FUE:EffectiveDate),RIGHT(1)
                           BUTTON('...'),AT(134,58,12,10),USE(?Calendar)
                           CHECK(' Use another Fuel Cost to Calc. Base Rate'),AT(68,78),USE(Use_FuelCost),TRN
                           GROUP('Calc. Base Values'),AT(8,94,223,46),USE(?Group_Calc),BOXED,TRN
                             BUTTON('...'),AT(156,107,12,10),USE(?Button_Select_FuelBase)
                             PROMPT('Fuel Cost (cents):'),AT(30,107),USE(?FuelCost:Prompt),TRN
                             ENTRY(@n-14.3),AT(90,107,60,10),USE(FuelCost),RIGHT(1),MSG('Cost per litre'),TIP('Cost per litre')
                             PROMPT('Fuel Base Rate:'),AT(30,123),USE(?FuelBaseRate:Prompt),TRN
                             ENTRY(@n-13.4~%~),AT(90,123,60,10),USE(FuelBaseRate),RIGHT(1),MSG('% cost of total cost'), |
  TIP('% cost of total cost')
                           END
                           CHECK(' Calc. Base Rate'),AT(68,154),USE(LOC:CalcBaseRate),TRN
                           PROMPT('Fuel Base Rate:'),AT(8,168),USE(?FUE:FuelBaseRate:Prompt),TRN
                           ENTRY(@n-13.4~%~),AT(68,168,60,10),USE(FUE:FuelBaseRate),RIGHT(1),MSG('% cost of total cost'), |
  TIP('% cost of total cost')
                           BUTTON('[]'),AT(134,168,12,10),USE(?Button_BaseRateCalc),TIP('Use formula screen')
                         END
                       END
                       BUTTON('&OK'),AT(134,196,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(186,196,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,196,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar7            CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Get_Revised_Base            ROUTINE
    DATA
R:M_FuelBaseRate    LIKE(FUE:FuelBaseRate)
R:X_Fuel_Cost       LIKE(FUE:FuelBaseRate)
R:FuelBaseRate      LIKE(FUE:FuelBaseRate)

    CODE
    ! (p:Date, p:Eff_Date, p:BaseRate)
    ! (LONG, <*LONG>, <*DECIMAL>)

!    R:X_Fuel_Cost   = Get_FuelCost(FUE:EffectiveDate - 1, , R:M_FuelBaseRate)

    ! (p:X_Fuel_Cost, p:Y_Fuel_Cost, p:M_Fuel_Base_Rate, p:Z_CPL_Change, p:Q_CPL_Change_Per,
    !    p:N_Increase_On_Base, p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per)
    ! (*DECIMAL, *DECIMAL, *DECIMAL, <*DECIMAL>, <*DECIMAL>,
    !    <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING



    ! We'll put the increase on base rate in here instead... not sure what FBN want

    IF LOC:CalcBaseRate = TRUE
       FUE:FuelBaseRate    = Calc_FuelBaseRate(FuelCost, FUE:FuelCost, FuelBaseRate,    |
                                                 , , ,                                  |
                                                 , R:FuelBaseRate,)
    .

!    IF R:FuelBaseRate <> 0
!       FUE:FuelBaseRate    = R:FuelBaseRate
!    .

    DISPLAY
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Fuel Cost a Record'
  OF InsertRecord
    ActionMessage = 'Fuel Cost Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Fuel Cost Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_FuelCost')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?FUE:FuelCostType:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:_FuelCost)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(FUE:Record,History::FUE:Record)
  SELF.AddHistoryField(?FUE:FuelCostType,9)
  SELF.AddHistoryField(?FUE:FuelCost,2)
  SELF.AddHistoryField(?FUE:EffectiveDate,7)
  SELF.AddHistoryField(?FUE:FuelBaseRate,3)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:_FuelCost.SetOpenRelated()
  Relate:_FuelCost.Open                                    ! File _FuelCost used by this procedure, so make sure it's RelationManager is open
  Relate:_FuelCostsAlias.Open                              ! File _FuelCostsAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_FuelCost
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?FUE:FuelCostType)
    ?FUE:FuelCost{PROP:ReadOnly} = True
    DISABLE(?Calendar)
    DISABLE(?Button_Select_FuelBase)
    ?FuelCost{PROP:ReadOnly} = True
    ?FuelBaseRate{PROP:ReadOnly} = True
    ?FUE:FuelBaseRate{PROP:ReadOnly} = True
    DISABLE(?Button_BaseRateCalc)
  END
      IF p_Type = 1
         FUE:FuelCostType = 1
      .
      IF FUE:Base_FCID = 0 AND SELF.Request <> InsertRecord
         LOC:CalcBaseRate     = FALSE
      .
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_FuelCost',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  IF ?Use_FuelCost{Prop:Checked}
    ENABLE(?Group_Calc)
  END
  IF NOT ?Use_FuelCost{PROP:Checked}
    DISABLE(?Group_Calc)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_FuelCost.Close
    Relate:_FuelCostsAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_FuelCost',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?FUE:FuelCost
          DO Get_Revised_Base
    OF ?FUE:EffectiveDate
          DO Get_Revised_Base
    OF ?Calendar
      ThisWindow.Update()
      Calendar7.SelectOnClose = True
      Calendar7.Ask('Select a Date',FUE:EffectiveDate)
      IF Calendar7.Response = RequestCompleted THEN
      FUE:EffectiveDate=Calendar7.SelectedDate
      DISPLAY(?FUE:EffectiveDate)
      END
      ThisWindow.Reset(True)
          DO Get_Revised_Base
    OF ?Use_FuelCost
      IF ?Use_FuelCost{PROP:Checked}
        ENABLE(?Group_Calc)
      END
      IF NOT ?Use_FuelCost{PROP:Checked}
        DISABLE(?Group_Calc)
      END
      ThisWindow.Reset()
    OF ?Button_Select_FuelBase
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_FuelCost_Alias()
      ThisWindow.Reset
          FUE:Base_FCID   = A_FUE:FCID
      
          FuelCost        = A_FUE:FuelCost
          FuelBaseRate    = A_FUE:FuelBaseRate
      
      
          
          DO Get_Revised_Base
          DISPLAY
    OF ?FUE:FuelBaseRate
          DO Get_Revised_Base
    OF ?Button_BaseRateCalc
      ThisWindow.Update()
        ! (BYTE, *DECIMAL, LONG, <ULONG>, <ULONG>, <ULONG>)
        ! (p:Type, p:Cur_Cost, p:Eff_Date, p:CID, p:Base_Rate_ID, p:Base_Cost_ID)
          Window_FuelSurcharge(0, FUE:FuelCost, FUE:EffectiveDate,, FUE:Base_FCID, FUE:Base_FCID)
      
          DO Get_Revised_Base
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_FuelCost_Alias PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(_FuelCostsAlias)
                       PROJECT(A_FUE:FuelCost)
                       PROJECT(A_FUE:FuelBaseRate)
                       PROJECT(A_FUE:EffectiveDate)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
A_FUE:FuelCost         LIKE(A_FUE:FuelCost)           !List box control field - type derived from field
A_FUE:FuelBaseRate     LIKE(A_FUE:FuelBaseRate)       !List box control field - type derived from field
A_FUE:EffectiveDate    LIKE(A_FUE:EffectiveDate)      !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Fuel Cost file'),AT(,,277,198),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Browse_FuelCost'),SYSTEM
                       LIST,AT(8,30,261,124),USE(?Browse:1),HVSCROLL,FORMAT('70R(1)|M~Fuel Cost (cents)~C(0)@n' & |
  '-14.3@70R(1)|M~Fuel Base Rate %~C(0)@n-13.4~%~@80R(1)|M~Effective Date~C(0)@D6B@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the _FuelCost file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(61,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(114,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(167,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(220,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,269,172),USE(?CurrentTab)
                         TAB('&1) By Effective Date'),USE(?Tab:3)
                         END
                         TAB('&1) By Fuel Cost'),USE(?Tab:2),HIDE
                         END
                       END
                       BUTTON('&Close'),AT(225,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_FuelCost_Alias')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:_FuelCostsAlias.Open                              ! File _FuelCostsAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_FuelCostsAlias,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,)                                     ! Add the sort order for  for sort order 1
  BRW1.AddSortOrder(,A_FUE:SKey_EffectiveDate)             ! Add the sort order for A_FUE:SKey_EffectiveDate for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,A_FUE:EffectiveDate,1,BRW1)    ! Initialize the browse locator using  using key: A_FUE:SKey_EffectiveDate , A_FUE:EffectiveDate
  BRW1.AppendOrder('+A_FUE:FCID')                          ! Append an additional sort order
  BRW1.SetFilter('(A_FUE:FuelCost <<> 0.0)')               ! Apply filter expression to browse
  BRW1.AddField(A_FUE:FuelCost,BRW1.Q.A_FUE:FuelCost)      ! Field A_FUE:FuelCost is a hot field or requires assignment from browse
  BRW1.AddField(A_FUE:FuelBaseRate,BRW1.Q.A_FUE:FuelBaseRate) ! Field A_FUE:FuelBaseRate is a hot field or requires assignment from browse
  BRW1.AddField(A_FUE:EffectiveDate,BRW1.Q.A_FUE:EffectiveDate) ! Field A_FUE:EffectiveDate is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_FuelCost_Alias',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_FuelCostsAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_FuelCost_Alias',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_FuelCost
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

