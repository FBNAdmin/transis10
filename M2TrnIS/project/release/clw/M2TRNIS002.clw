

   MEMBER('M2TRNIS.clw')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('M2TRNIS002.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_RatesFuelCost_old PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:ClientName       STRING(100)                           ! 
LOC:ClientNo         ULONG                                 ! Client No.
FuelReBaseRate       DECIMAL(10,4)                         ! % cost of total cost
EffectiveDate        DATE                                  ! 
Clients_Cost_Base_Date DATE                                ! 
Clients_Cost_Base_FuelCost DECIMAL(10,3)                   ! Cost per litre
LOC:CPL_Group        GROUP,PRE(L_CLP)                      ! 
X_CostBase_Date      LONG                                  ! 
Y_CostChange_Date    LONG                                  ! 
X_Cost_As_At         DECIMAL(15,4)                         ! Cost per litre
Y_CostChange         DECIMAL(15,4)                         ! Cost per litre
Z_ChangeInCPL        DECIMAL(17,7)                         ! Cost per litre
Q_ChangePercent      DECIMAL(17,7)                         ! 
                     END                                   ! 
LOC:Cost_Group       GROUP,PRE(L_CG)                       ! 
M_FuelBaseRate       DECIMAL(15,4)                         ! % cost of total cost
OtherCosts           DECIMAL(15,4)                         ! % cost of total cost
TotalCostPer         DECIMAL(15,4,100)                     ! % cost of total cost
N_IncreaseOnBase     DECIMAL(15,4)                         ! % cost of total cost
O_FuelCostChange     DECIMAL(15,4)                         ! % cost of total cost
P_OtherCostChange    DECIMAL(15,4)                         ! % cost of total cost
R_CostChangeTotal    DECIMAL(15,4)                         ! % cost of total cost
S_RevisedBase        DECIMAL(15,4)                         ! % cost of total cost
T_OtherRevised       DECIMAL(15,4)                         ! % cost of total cost
ST_RevisedTotal      DECIMAL(15,4)                         ! % cost of total cost
                     END                                   ! 
History::FCRA:Record LIKE(FCRA:RECORD),THREAD
QuickWindow          WINDOW('Form Rates Fuel Cost'),AT(,,311,191),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_RatesFuelCost'),SYSTEM
                       SHEET,AT(4,4,303,167),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Client Name:'),AT(6,22),USE(?LOC:ClientName:Prompt),TRN
                           BUTTON('...'),AT(81,22,12,10),USE(?CallLookup)
                           ENTRY(@s100),AT(97,22,168,10),USE(LOC:ClientName),COLOR(00E9E9E9h),FLAT,READONLY,SKIP,TIP('Client name')
                           PROMPT('Client No.:'),AT(6,36),USE(?LOC:ClientNo:Prompt),TRN
                           ENTRY(@n_10b),AT(97,36,64,10),USE(LOC:ClientNo),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Client No.'), |
  READONLY,SKIP,TIP('Client No.')
                           PROMPT('CID:'),AT(202,36),USE(?FCRA:CID:Prompt),TRN
                           STRING(@n_10),AT(221,36,,10),USE(FCRA:CID),RIGHT(1),TRN
                           PROMPT('Effective Date:'),AT(9,58),USE(?FCRA:Effective_Date:Prompt),TRN
                           BUTTON('...'),AT(81,58,12,10),USE(?Calendar)
                           SPIN(@d5),AT(97,58,64,10),USE(FCRA:Effective_Date),RIGHT(1),MSG('Effective from this date'), |
  REQ,TIP('Effective from this date')
                           PROMPT('Fuel Cost (in Cents):'),AT(9,76),USE(?FCRA:FuelCost:Prompt),TRN
                           ENTRY(@n-10.3),AT(97,76,64,10),USE(FCRA:FuelCost),RIGHT(1),MSG('Fuel Cost'),TIP('The cost o' & |
  'f fuel when the account was opened OR <0DH,0AH>when the last "leveling" rate adjustm' & |
  'ent was done')
                           PROMPT('Clients Base Rate:'),AT(9,96),USE(?LOC:Clients_Base_Rate:Prompt),TRN
                           BUTTON('...'),AT(81,96,12,10),USE(?CallLookup:2)
                           ENTRY(@n-13.4),AT(97,96,64,10),USE(FCRA:FuelBaseRate),RIGHT(1),COLOR(00E9E9E9h),FLAT,READONLY, |
  SKIP,MSG('% cost of total cost')
                           ENTRY(@n_10),AT(174,96,50,10),USE(FCRA:FCID),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Fuel Cost ID'), |
  READONLY,SKIP,TIP('Fuel Cost ID')
                           ENTRY(@D6B),AT(237,96,64,10),USE(EffectiveDate),COLOR(00E9E9E9h),FLAT,SKIP
                           ENTRY(@d6b),AT(237,115,64,10),USE(Clients_Cost_Base_Date),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                           PROMPT('Clients Base Cost:'),AT(9,115),USE(?LOC:Clients_Base_Rate:Prompt:2),TRN
                           BUTTON('...'),AT(81,115,12,10),USE(?CallLookup:Clients_cost_base_date)
                           ENTRY(@n-14.3),AT(97,115,64,10),USE(Clients_Cost_Base_FuelCost),RIGHT(1),COLOR(00E9E9E9h), |
  FLAT,MSG('Cost per litre'),READONLY,SKIP,TIP('Cost per litre')
                           ENTRY(@n_10),AT(174,115,50,10),USE(FCRA:FCID_CostBase),RIGHT(1),COLOR(00E9E9E9h),FLAT,SKIP,MSG('Fuel Cost ID - used for Cost Base')
                           PROMPT('Clients Surcharge:'),AT(9,136),USE(?LOC:Clients_Surcharge:Prompt),TRN
                           BUTTON,AT(81,136,12,10),USE(?Button_Refresh),ICON('RefreshI.ico'),FLAT
                           ENTRY(@n-14.3),AT(97,136,64,10),USE(FCRA:FuelSurcharge),RIGHT(1),COLOR(00E9E9E9h)
                           PROMPT('Rate Information @ Effective Date:'),AT(9,156),USE(?Prompt8),TRN
                           BUTTON('[]'),AT(122,156,16,10),USE(?Button_Fuel)
                         END
                       END
                       BUTTON('&OK'),AT(204,174,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(258,174,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(4,174,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar7            CalendarClass
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Client_Surcharge            ROUTINE
    IF QuickWindow{PROP:AcceptAll} = FALSE
       ! (p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Cur_Eff_Date, |
       !  p:X_Fuel_Cost, p:X_Eff_Date, p:M_Fuel_Base_Rate,
       !  p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base,
       !  p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per)
       ! (ULONG, ULONG, *DECIMAL, LONG, <*DECIMAL>, <*LONG>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING


       ! (p:X_Fuel_Cost, p:Y_Fuel_Cost, p:M_Fuel_Base_Rate,
       !  p:Z_CPL_Change, p:Q_CPL_Change_Per, p:O_Change_FuelC_Per,
       !  p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per)
       ! (*DECIMAL, *DECIMAL, *DECIMAL, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING

       FCRA:FuelSurcharge  = Calc_FuelBaseRate(Clients_Cost_Base_FuelCost, FCRA:FuelCost, FCRA:FuelBaseRate)

       !FCRA:FuelSurcharge  = Get_Client_FuelSurcharge_Calcs(FCRA:CID, FCRA:FCID, FCRA:FuelCost, FCRA:Effective_Date,,,,,,,, FuelReBaseRate)
    ELSE
       !Get_Client_FuelSurcharge_Calcs(FCRA:CID, FCRA:FCID, FCRA:FuelCost, FCRA:Effective_Date,,,,,,, FuelReBaseRate)
    .

    ! This allows manual override but will reset every time the user comes back into the form....  ???

    DISPLAY
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Rates Fuel Cost Record'
  OF InsertRecord
    ActionMessage = 'Rates Fuel Cost Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Rates Fuel Cost Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_RatesFuelCost_old')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:ClientName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:__RatesFuelCost)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(FCRA:Record,History::FCRA:Record)
  SELF.AddHistoryField(?FCRA:CID,2)
  SELF.AddHistoryField(?FCRA:Effective_Date,10)
  SELF.AddHistoryField(?FCRA:FuelCost,3)
  SELF.AddHistoryField(?FCRA:FuelBaseRate,6)
  SELF.AddHistoryField(?FCRA:FCID,4)
  SELF.AddHistoryField(?FCRA:FCID_CostBase,5)
  SELF.AddHistoryField(?FCRA:FuelSurcharge,7)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.SetOpenRelated()
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:_FuelCost.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:__RatesFuelCost
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup)
    ?LOC:ClientName{PROP:ReadOnly} = True
    ?LOC:ClientNo{PROP:ReadOnly} = True
    DISABLE(?Calendar)
    ?FCRA:FuelCost{PROP:ReadOnly} = True
    DISABLE(?CallLookup:2)
    ?FCRA:FuelBaseRate{PROP:ReadOnly} = True
    ?FCRA:FCID{PROP:ReadOnly} = True
    ?EffectiveDate{PROP:ReadOnly} = True
    ?Clients_Cost_Base_Date{PROP:ReadOnly} = True
    DISABLE(?CallLookup:Clients_cost_base_date)
    ?Clients_Cost_Base_FuelCost{PROP:ReadOnly} = True
    ?FCRA:FCID_CostBase{PROP:ReadOnly} = True
    DISABLE(?Button_Refresh)
    ?FCRA:FuelSurcharge{PROP:ReadOnly} = True
    DISABLE(?Button_Fuel)
  END
      IF SELF.Request = InsertRecord
         FCRA:CID     = CLI:CID
      .
  
  
      CLI:CID     = FCRA:CID
      ! Need to check is this affects the client record loaded on calling proc. - are changed fields kept??
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         LOC:ClientName   = CLI:ClientName
         LOC:ClientNo     = CLI:ClientNo
      .
      FUE:FCID     = FCRA:FCID
      IF Access:_FuelCost.TryFetch(FUE:PKey_FCID) = LEVEL:Benign
         EffectiveDate    = FUE:EffectiveDate
      .
  
      FUE:FCID     = FCRA:FCID_CostBase
      IF Access:_FuelCost.TryFetch(FUE:PKey_FCID) = LEVEL:Benign
         Clients_Cost_Base_Date       = FUE:EffectiveDate
         Clients_Cost_Base_FuelCost   = FUE:FuelCost
      .
      DO Client_Surcharge
  
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_RatesFuelCost_old',QuickWindow)     ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_RatesFuelCost_old',QuickWindow)  ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_Clients
      Browse_FuelCost((1))
      Browse_FuelCost((1))
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update()
      CLI:ClientName = LOC:ClientName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:ClientName = CLI:ClientName
        FCRA:CID = CLI:CID
        LOC:ClientNo = CLI:ClientNo
      END
      ThisWindow.Reset(1)
    OF ?LOC:ClientName
      IF LOC:ClientName OR ?LOC:ClientName{PROP:Req}
        CLI:ClientName = LOC:ClientName
        IF Access:Clients.TryFetch(CLI:Key_ClientName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:ClientName = CLI:ClientName
            FCRA:CID = CLI:CID
            LOC:ClientNo = CLI:ClientNo
          ELSE
            CLEAR(FCRA:CID)
            CLEAR(LOC:ClientNo)
            SELECT(?LOC:ClientName)
            CYCLE
          END
        ELSE
          FCRA:CID = CLI:CID
          LOC:ClientNo = CLI:ClientNo
        END
      END
      ThisWindow.Reset()
    OF ?Calendar
      ThisWindow.Update()
      Calendar7.SelectOnClose = True
      Calendar7.Ask('Select a Date',FCRA:Effective_Date)
      IF Calendar7.Response = RequestCompleted THEN
      FCRA:Effective_Date=Calendar7.SelectedDate
      DISPLAY(?FCRA:Effective_Date)
      END
      ThisWindow.Reset(True)
          IF FCRA:FuelCost = 0.0 AND QuickWindow{PROP:AcceptAll} ~= TRUE
             FCRA:FuelCost   = Get_FuelCost(FCRA:Effective_Date)
             DISPLAY(?FCRA:FuelCost)
          .
          DO Client_Surcharge
      
    OF ?FCRA:Effective_Date
          IF FCRA:FuelCost = 0.0 AND QuickWindow{PROP:AcceptAll} ~= TRUE
             FCRA:FuelCost   = Get_FuelCost(FCRA:Effective_Date)
             DISPLAY(?FCRA:FuelCost)
          .
          DO Client_Surcharge
      
    OF ?FCRA:FuelCost
          DO Client_Surcharge
      
    OF ?CallLookup:2
      ThisWindow.Update()
      FUE:FCID = FCRA:FCID
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        FCRA:FCID = FUE:FCID
        FCRA:FuelBaseRate = FUE:FuelBaseRate
        EffectiveDate = FUE:EffectiveDate
      END
      ThisWindow.Reset(1)
          DO Client_Surcharge
      
    OF ?FCRA:FCID
      IF NOT QuickWindow{PROP:AcceptAll}
        IF FCRA:FCID OR ?FCRA:FCID{PROP:Req}
          FUE:FCID = FCRA:FCID
          IF Access:_FuelCost.TryFetch(FUE:PKey_FCID)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              FCRA:FCID = FUE:FCID
              FCRA:FuelBaseRate = FUE:FuelBaseRate
              EffectiveDate = FUE:EffectiveDate
            ELSE
              CLEAR(FCRA:FuelBaseRate)
              CLEAR(EffectiveDate)
              SELECT(?FCRA:FCID)
              CYCLE
            END
          ELSE
            FCRA:FuelBaseRate = FUE:FuelBaseRate
            EffectiveDate = FUE:EffectiveDate
          END
        END
      END
      ThisWindow.Reset()
          DO Client_Surcharge
      
    OF ?CallLookup:Clients_cost_base_date
      ThisWindow.Update()
      FUE:FCID = FCRA:FCID_CostBase
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        FCRA:FCID_CostBase = FUE:FCID
        Clients_Cost_Base_FuelCost = FUE:FuelCost
        Clients_Cost_Base_Date = FUE:EffectiveDate
      END
      ThisWindow.Reset(1)
    OF ?FCRA:FCID_CostBase
      IF FCRA:FCID_CostBase OR ?FCRA:FCID_CostBase{PROP:Req}
        FUE:FCID = FCRA:FCID_CostBase
        IF Access:_FuelCost.TryFetch(FUE:PKey_FCID)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            FCRA:FCID_CostBase = FUE:FCID
            Clients_Cost_Base_FuelCost = FUE:FuelCost
            Clients_Cost_Base_Date = FUE:EffectiveDate
          ELSE
            CLEAR(Clients_Cost_Base_FuelCost)
            CLEAR(Clients_Cost_Base_Date)
            SELECT(?FCRA:FCID_CostBase)
            CYCLE
          END
        ELSE
          Clients_Cost_Base_FuelCost = FUE:FuelCost
          Clients_Cost_Base_Date = FUE:EffectiveDate
        END
      END
      ThisWindow.Reset()
    OF ?Button_Refresh
      ThisWindow.Update()
          DO Client_Surcharge            
    OF ?Button_Fuel
      ThisWindow.Update()
      Window_FuelSurcharge(0, FCRA:FuelCost, FCRA:Effective_Date, FCRA:CID, FCRA:FCID, FCRA:FCID_CostBase)
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_EToll PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
History::TOL:Record  LIKE(TOL:RECORD),THREAD
QuickWindow          WINDOW('Form Rates - Toll'),AT(,,199,114),FONT('Microsoft Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Update_EToll'),SYSTEM
                       SHEET,AT(4,4,191,89),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('CID:'),AT(121,6),USE(?TOL:CID:Prompt),TRN
                           STRING(@n_10),AT(140,6,53,10),USE(TOL:CID),RIGHT(1),TRN
                           PROMPT('Toll Rate:'),AT(8,29),USE(?TOL:TollRate:Prompt),TRN
                           ENTRY(@n-7.2),AT(72,29,59,10),USE(TOL:TollRate),RIGHT(1),MSG('Toll Rate'),TIP('Toll Rate')
                           PROMPT('Effective Date:'),AT(8,48),USE(?TOL:Effective_Date:Prompt),TRN
                           SPIN(@d5),AT(72,48,59,10),USE(TOL:Effective_Date),RIGHT(1),MSG('Effective from this date'), |
  REQ,TIP('Effective from this date')
                         END
                       END
                       BUTTON('&OK'),AT(89,97,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(143,97,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(7,97,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Toll Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Toll Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_EToll')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TOL:CID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:__RatesToll)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(TOL:Record,History::TOL:Record)
  SELF.AddHistoryField(?TOL:CID,2)
  SELF.AddHistoryField(?TOL:TollRate,3)
  SELF.AddHistoryField(?TOL:Effective_Date,6)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:__RatesToll.Open                                  ! File __RatesToll used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:__RatesToll
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?TOL:TollRate{PROP:ReadOnly} = True
  END
      IF SELF.Request = InsertRecord
         TOL:CID    = CLI:CID
      .
  
  
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_EToll',QuickWindow)                 ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:__RatesToll.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_EToll',QuickWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Process
!!! Process the EmailAddresses File
!!! </summary>
RunOnce_De_Duplicate_EmailAddresses PROCEDURE 

Progress:Thermometer BYTE                                  ! 
AddID_Remove         ULONG                                 ! 
RemoveCount          ULONG                                 ! 
addQ                 QUEUE,PRE(Q)                          ! 
EmailAddress         STRING(255)                           ! Email Address
IDs                  STRING(1024)                          ! 
                     END                                   ! 
Process:View         VIEW(EmailAddresses)
                     END
ProgressWindow       WINDOW('Process EmailAddresses'),AT(,,142,59),FONT('Microsoft Sans Serif',8,,FONT:regular, |
  CHARSET:DEFAULT),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RunOnce_De_Duplicate_EmailAddresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:EmailAddresses.Open                               ! File EmailAddresses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('RunOnce_De_Duplicate_EmailAddresses',ProgressWindow) ! Restore window settings from non-volatile store
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:EmailAddresses, ?Progress:PctText, Progress:Thermometer, ProgressMgr, EMAI:EAID)
  ThisProcess.AddSortOrder(EMAI:PKey_EAID)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(EmailAddresses,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EmailAddresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('RunOnce_De_Duplicate_EmailAddresses',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
      ?Progress:UserString{PROP:Text} = 'Deleting now...'
      Progress:Thermometer = 0
      DISPLAY()
      
      i# = 0
      LOOP
         i# += 1
         get(addQ, i#)
         IF ERRORCODE() 
            BREAK
         .
         db.Debugout(CLIP(Q:EmailAddress) & '      IDs: ' & Q:IDs)
         
         ! Keep first one and remove others
         Get_1st_Element_From_Delim_Str(Q:IDs, ',', true)
         
         LOOP 
            AddID_Remove = Get_1st_Element_From_Delim_Str(Q:IDs, ',', true)
            db.Debugout(CLIP(Q:EmailAddress) & '      Removing: ' & AddID_Remove)
            IF CLIP(AddID_Remove) = 0  ! because its an integer
               BREAK
            .
            
            ! Delete it
            EMAI:EAID = AddID_Remove
            IF Access:EmailAddresses.TryFetch(EMAI:PKey_EAID) = Level:Benign
               IF Access:EmailAddresses.DeleteRecord(0) <> Level:Benign
                  MESSAGE('Could not delete this duplicate:  ' & AddID_Remove)
               ELSE
                  RemoveCount += 1
               .
            .
            
            Progress:Thermometer = i# / RECORDS(addQ)      
            YIELD()
            DISPLAY()      
            YIELD()
            db.Debugout(CLIP(Q:EmailAddress) & '      moving on...')
         .   
      .
         
      MESSAGE('Removed ' & RemoveCount & ' duplicate Email Addresses.')
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  CLEAR(addQ)
  Q:EmailAddress = CLIP(EMAI:EmailAddress)
  GET(addQ, Q:EmailAddress)
  IF ERRORCODE()
     Q:IDs = EMAI:EAID
     ADD(addQ)
  ELSE
     Add_to_List(EMAI:EAID, Q:IDs, ',')
     PUT(addQ)
  .
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue

