rem TrnISDct
del TrnISDct\*.clw /q
del TrnISDct\*.inc /q
del TrnISDct\*.bpp /q
del TrnISDct\*.exp /q
del TrnISDct\*.shp /q
del TrnISDct\map\release\*.* /q
del TrnISDct\obj\release\*.* /q
del TrnISDct\map\debug\*.* /q
del TrnISDct\obj\debug\*.* /q
rem FunTrnIS
del FunTrnIS\*.clw /q
del FunTrnIS\*.inc /q
del FunTrnIS\*.bpp /q
del FunTrnIS\*.exp /q
del FunTrnIS\*.shp /q
del FunTrnIS\map\release\*.* /q
del FunTrnIS\obj\release\*.* /q
del FunTrnIS\map\debug\*.* /q
del FunTrnIS\obj\debug\*.* /q
rem M2TrnIS
del M2TrnIS\*.clw /q
del M2TrnIS\*.inc /q
del M2TrnIS\*.bpp /q
del M2TrnIS\*.exp /q
del M2TrnIS\*.shp /q
del M2TrnIS\map\release\*.* /q
del M2TrnIS\obj\release\*.* /q
del M2TrnIS\map\debug\*.* /q
del M2TrnIS\obj\debug\*.* /q
rem M3TrnIS
del M3TrnIS\*.clw /q
del M3TrnIS\*.inc /q
del M3TrnIS\*.bpp /q
del M3TrnIS\*.exp /q
del M3TrnIS\*.shp /q
del M3TrnIS\map\release\*.* /q
del M3TrnIS\obj\release\*.* /q
del M3TrnIS\map\debug\*.* /q
del M3TrnIS\obj\debug\*.* /q
rem ManTrnIS
del ManTrnIS\*.clw /q
del ManTrnIS\*.inc /q
del ManTrnIS\*.bpp /q
del ManTrnIS\*.exp /q
del ManTrnIS\*.shp /q
del ManTrnIS\map\release\*.* /q
del ManTrnIS\obj\release\*.* /q
del ManTrnIS\map\debug\*.* /q
del ManTrnIS\obj\debug\*.* /q
rem RptTrnIS
del RptTrnIS\*.clw /q
del RptTrnIS\*.inc /q
del RptTrnIS\*.bpp /q
del RptTrnIS\*.exp /q
del RptTrnIS\*.shp /q
del RptTrnIS\map\release\*.* /q
del RptTrnIS\obj\release\*.* /q
del RptTrnIS\map\debug\*.* /q
del RptTrnIS\obj\debug\*.* /q
rem SecTrnIS
del SecTrnIS\*.clw /q
del SecTrnIS\*.inc /q
del SecTrnIS\*.bpp /q
del SecTrnIS\*.exp /q
del SecTrnIS\*.shp /q
del SecTrnIS\map\release\*.* /q
del SecTrnIS\obj\release\*.* /q
del SecTrnIS\map\debug\*.* /q
del SecTrnIS\obj\debug\*.* /q
rem SelTrnIS
del SelTrnIS\*.clw /q
del SelTrnIS\*.inc /q
del SelTrnIS\*.bpp /q
del SelTrnIS\*.exp /q
del SelTrnIS\*.shp /q
del SelTrnIS\map\release\*.* /q
del SelTrnIS\obj\release\*.* /q
del SelTrnIS\map\debug\*.* /q
del SelTrnIS\obj\debug\*.* /q
rem TransISm
del TransISm\*.clw /q
del TransISm\*.inc /q
del TransISm\*.bpp /q
del TransISm\*.exp /q
del TransISm\*.shp /q
del TransISm\map\release\*.* /q
del TransISm\obj\release\*.* /q
del TransISm\map\debug\*.* /q
del TransISm\obj\debug\*.* /q
copy Lib\ikb_lib.lib Lib\ikb_lib.lib.old
del Lib\*.Lib /q
copy Lib\ikb_lib.lib.old Lib\ikb_lib.lib
rem ikb_lib
del ikb_lib\ikb_lib*.clw /q
del ikb_lib\ikb_l_*.clw /q
del ikb_lib\*.bpp /q
del ikb_lib\*.exp /q
del ikb_lib\*.shp /q
del IKB_Lib\obj\release\*.* /q
rem bin
del bin\TRANSIS.exe /q
del bin\M3TRNIS.dll /q
del bin\SECTRNIS.dll /q
del bin\MANTRNIS.dll /q
del bin\M2TRNIS.dll /q
del bin\SelTrnIS.dll /q
del bin\RPTTRNIS.dll /q
del bin\FunTrnIS.dll /q
del bin\TrnISDct.dll /q
