

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS003.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('DBUTRANS009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS014.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_DeliveryItems PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:Locator          STRING(20)                            !
BRW1::View:Browse    VIEW(DeliveryItems)
                       PROJECT(DELI:DIID)
                       PROJECT(DELI:DID)
                       PROJECT(DELI:ItemNo)
                       PROJECT(DELI:Units)
                       PROJECT(DELI:Weight)
                       PROJECT(DELI:Type)
                       PROJECT(DELI:ContainerNo)
                       PROJECT(DELI:ContainerVessel)
                       PROJECT(DELI:SealNo)
                       PROJECT(DELI:ETA)
                       PROJECT(DELI:CMID)
                       PROJECT(DELI:PTID)
                       JOIN(PACK:PKey_PTID,DELI:PTID)
                         PROJECT(PACK:Packaging)
                         PROJECT(PACK:PTID)
                       END
                       JOIN(COM:PKey_CMID,DELI:CMID)
                         PROJECT(COM:Commodity)
                         PROJECT(COM:CMID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DELI:DIID              LIKE(DELI:DIID)                !List box control field - type derived from field
DELI:DID               LIKE(DELI:DID)                 !List box control field - type derived from field
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Units             LIKE(DELI:Units)               !List box control field - type derived from field
DELI:Weight            LIKE(DELI:Weight)              !List box control field - type derived from field
DELI:Type              LIKE(DELI:Type)                !List box control field - type derived from field
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
PACK:Packaging         LIKE(PACK:Packaging)           !List box control field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !List box control field - type derived from field
DELI:ContainerVessel   LIKE(DELI:ContainerVessel)     !List box control field - type derived from field
DELI:SealNo            LIKE(DELI:SealNo)              !List box control field - type derived from field
DELI:ETA               LIKE(DELI:ETA)                 !List box control field - type derived from field
DELI:CMID              LIKE(DELI:CMID)                !Browse key field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Related join file key field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Delivery Items file'),AT(,,411,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MAX,MDI,HLP('Brwose_DeliveryItems'),SYSTEM
                       LIST,AT(8,32,394,122),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~DIID~C(0)@n_10@40R(2)|M~' & |
  'DID~C(0)@n_10@30R(2)|M~Item No.~C(0)@n_5@24R(2)|M~Units~C(0)@n6@44R(2)|M~Weight~C(0)' & |
  '@n-11.2@20R(2)|M~Type~C(0)@n3@80L(2)|M~Commodity~C(0)@s35@80L(2)|M~Packaging~C(0)@s3' & |
  '5@40L(2)|M~Container No.~@s35@40L(2)|M~Container Vessel~@s35@40L(2)|M~Seal No~@s35@4' & |
  '0R(2)|M~ETA~C(0)@d6@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the DeliveryItems file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       PROMPT(' Locator:'),AT(10,18),USE(?LOC:Locator:Prompt)
                       ENTRY(@s20),AT(44,20,76,9),USE(LOC:Locator)
                       SHEET,AT(4,4,403,172),USE(?CurrentTab)
                         TAB('by DIID'),USE(?Tab3)
                         END
                         TAB('&2) By Commodity'),USE(?Tab:3)
                           BUTTON('Select Commodities'),AT(8,158,118,14),USE(?SelectCommodities),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&1) By Delivery ID && Item No.'),USE(?Tab:2)
                           BUTTON('Select Deliveries'),AT(8,158,,14),USE(?SelectDeliveries),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(306,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(360,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort3:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_DeliveryItems')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Access:Commodities.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ContainerOperators.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ContainerTypes.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PackagingTypes.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:DeliveryItems,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,DELI:FKey_CMID)                       ! Add the sort order for DELI:FKey_CMID for sort order 1
  BRW1.AddRange(DELI:CMID,Relate:DeliveryItems,Relate:Commodities) ! Add file relationship range limit for sort order 1
  BRW1.AddSortOrder(,DELI:FKey_DID_ItemNo)                 ! Add the sort order for DELI:FKey_DID_ItemNo for sort order 2
  BRW1.AddRange(DELI:DID,Relate:DeliveryItems,Relate:Deliveries) ! Add file relationship range limit for sort order 2
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort3:Locator.Init(?LOC:Locator,DELI:ItemNo,1,BRW1) ! Initialize the browse locator using ?LOC:Locator using key: DELI:FKey_DID_ItemNo , DELI:ItemNo
  BRW1.AppendOrder('+DELI:DIID')                           ! Append an additional sort order
  BRW1.AddSortOrder(,DELI:PKey_DIID)                       ! Add the sort order for DELI:PKey_DIID for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(?LOC:Locator,DELI:DIID,1,BRW1)  ! Initialize the browse locator using ?LOC:Locator using key: DELI:PKey_DIID , DELI:DIID
  BRW1.AddField(DELI:DIID,BRW1.Q.DELI:DIID)                ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW1.AddField(DELI:DID,BRW1.Q.DELI:DID)                  ! Field DELI:DID is a hot field or requires assignment from browse
  BRW1.AddField(DELI:ItemNo,BRW1.Q.DELI:ItemNo)            ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW1.AddField(DELI:Units,BRW1.Q.DELI:Units)              ! Field DELI:Units is a hot field or requires assignment from browse
  BRW1.AddField(DELI:Weight,BRW1.Q.DELI:Weight)            ! Field DELI:Weight is a hot field or requires assignment from browse
  BRW1.AddField(DELI:Type,BRW1.Q.DELI:Type)                ! Field DELI:Type is a hot field or requires assignment from browse
  BRW1.AddField(COM:Commodity,BRW1.Q.COM:Commodity)        ! Field COM:Commodity is a hot field or requires assignment from browse
  BRW1.AddField(PACK:Packaging,BRW1.Q.PACK:Packaging)      ! Field PACK:Packaging is a hot field or requires assignment from browse
  BRW1.AddField(DELI:ContainerNo,BRW1.Q.DELI:ContainerNo)  ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW1.AddField(DELI:ContainerVessel,BRW1.Q.DELI:ContainerVessel) ! Field DELI:ContainerVessel is a hot field or requires assignment from browse
  BRW1.AddField(DELI:SealNo,BRW1.Q.DELI:SealNo)            ! Field DELI:SealNo is a hot field or requires assignment from browse
  BRW1.AddField(DELI:ETA,BRW1.Q.DELI:ETA)                  ! Field DELI:ETA is a hot field or requires assignment from browse
  BRW1.AddField(DELI:CMID,BRW1.Q.DELI:CMID)                ! Field DELI:CMID is a hot field or requires assignment from browse
  BRW1.AddField(PACK:PTID,BRW1.Q.PACK:PTID)                ! Field PACK:PTID is a hot field or requires assignment from browse
  BRW1.AddField(COM:CMID,BRW1.Q.COM:CMID)                  ! Field COM:CMID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_DeliveryItems',QuickWindow)         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_DeliveryItems',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Delivery_Items
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectCommodities
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectCommodities()
      ThisWindow.Reset
    OF ?SelectDeliveries
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_Deliveries()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Select a Journeys Record
!!! </summary>
SelectJourneys PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Journeys)
                       PROJECT(JOU:Journey)
                       PROJECT(JOU:Description)
                       PROJECT(JOU:BID)
                       PROJECT(JOU:JID)
                       PROJECT(JOU:FID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
JOU:Description        LIKE(JOU:Description)          !List box control field - type derived from field
JOU:BID                LIKE(JOU:BID)                  !List box control field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Primary key field - type derived from field
JOU:FID                LIKE(JOU:FID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Journeys Record'),AT(,,248,198),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('SelectJourneys'),SYSTEM
                       LIST,AT(8,30,232,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Journey~L(2)@s70@80L(2)|' & |
  'M~Description~L(2)@s255@80R(2)|M~BID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he Journeys file')
                       BUTTON('&Select'),AT(191,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,240,172),USE(?CurrentTab)
                         TAB('&1) By Journey'),USE(?Tab:2)
                         END
                         TAB('&2) By Branch'),USE(?Tab:3)
                         END
                         TAB('&3) By Floor'),USE(?Tab:4)
                         END
                       END
                       BUTTON('&Close'),AT(142,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(195,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectJourneys')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Journeys.Open                                     ! File Journeys used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Journeys,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,JOU:FKey_BID)                         ! Add the sort order for JOU:FKey_BID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,JOU:BID,1,BRW1)                ! Initialize the browse locator using  using key: JOU:FKey_BID , JOU:BID
  BRW1.AddSortOrder(,JOU:FKey_FID)                         ! Add the sort order for JOU:FKey_FID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,JOU:FID,1,BRW1)                ! Initialize the browse locator using  using key: JOU:FKey_FID , JOU:FID
  BRW1.AddSortOrder(,JOU:Key_Journey)                      ! Add the sort order for JOU:Key_Journey for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(,JOU:Journey,1,BRW1)            ! Initialize the browse locator using  using key: JOU:Key_Journey , JOU:Journey
  BRW1.AddField(JOU:Journey,BRW1.Q.JOU:Journey)            ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW1.AddField(JOU:Description,BRW1.Q.JOU:Description)    ! Field JOU:Description is a hot field or requires assignment from browse
  BRW1.AddField(JOU:BID,BRW1.Q.JOU:BID)                    ! Field JOU:BID is a hot field or requires assignment from browse
  BRW1.AddField(JOU:JID,BRW1.Q.JOU:JID)                    ! Field JOU:JID is a hot field or requires assignment from browse
  BRW1.AddField(JOU:FID,BRW1.Q.JOU:FID)                    ! Field JOU:FID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('SelectJourneys',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Journeys.Close
  END
  IF SELF.Opened
    INIMgr.Update('SelectJourneys',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Select a VehicleComposition Record
!!! </summary>
SelectVehicleComposition PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(VehicleComposition)
                       PROJECT(VCO:TID)
                       PROJECT(VCO:CompositionName)
                       PROJECT(VCO:Capacity)
                       PROJECT(VCO:ShowForAllTransporters)
                       PROJECT(VCO:VCID)
                       PROJECT(VCO:TTID0)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
VCO:TID                LIKE(VCO:TID)                  !List box control field - type derived from field
VCO:CompositionName    LIKE(VCO:CompositionName)      !List box control field - type derived from field
VCO:Capacity           LIKE(VCO:Capacity)             !List box control field - type derived from field
VCO:ShowForAllTransporters LIKE(VCO:ShowForAllTransporters) !List box control field - type derived from field
VCO:VCID               LIKE(VCO:VCID)                 !Primary key field - type derived from field
VCO:TTID0              LIKE(VCO:TTID0)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a VehicleComposition Record'),AT(,,292,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('SelectVehicleComposition'),SYSTEM
                       LIST,AT(8,30,276,124),USE(?Browse:1),HVSCROLL,FORMAT('80R(2)|M~TID~C(0)@n_10@80L(2)|M~C' & |
  'omposition Name~L(2)@s35@44R(2)|M~Capacity~C(0)@n10.0@80R(14)|M~Show For All Transpo' & |
  'rters~C(0)@n3@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the VehicleComposition file')
                       BUTTON('&Select'),AT(235,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,284,172),USE(?CurrentTab)
                         TAB('&1) By Transporter'),USE(?Tab:2)
                         END
                         TAB('&2) By Name'),USE(?Tab:3)
                         END
                         TAB('&3) By Truck'),USE(?Tab:4)
                         END
                       END
                       BUTTON('&Close'),AT(186,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(239,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectVehicleComposition')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:VehicleComposition.Open                           ! File VehicleComposition used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:VehicleComposition,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,VCO:Key_Name)                         ! Add the sort order for VCO:Key_Name for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,VCO:CompositionName,1,BRW1)    ! Initialize the browse locator using  using key: VCO:Key_Name , VCO:CompositionName
  BRW1.AddSortOrder(,VCO:FKey_TID0)                        ! Add the sort order for VCO:FKey_TID0 for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,VCO:TTID0,1,BRW1)              ! Initialize the browse locator using  using key: VCO:FKey_TID0 , VCO:TTID0
  BRW1.AddSortOrder(,VCO:FKey_TID)                         ! Add the sort order for VCO:FKey_TID for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(,VCO:TID,1,BRW1)                ! Initialize the browse locator using  using key: VCO:FKey_TID , VCO:TID
  BRW1.AddField(VCO:TID,BRW1.Q.VCO:TID)                    ! Field VCO:TID is a hot field or requires assignment from browse
  BRW1.AddField(VCO:CompositionName,BRW1.Q.VCO:CompositionName) ! Field VCO:CompositionName is a hot field or requires assignment from browse
  BRW1.AddField(VCO:Capacity,BRW1.Q.VCO:Capacity)          ! Field VCO:Capacity is a hot field or requires assignment from browse
  BRW1.AddField(VCO:ShowForAllTransporters,BRW1.Q.VCO:ShowForAllTransporters) ! Field VCO:ShowForAllTransporters is a hot field or requires assignment from browse
  BRW1.AddField(VCO:VCID,BRW1.Q.VCO:VCID)                  ! Field VCO:VCID is a hot field or requires assignment from browse
  BRW1.AddField(VCO:TTID0,BRW1.Q.VCO:TTID0)                ! Field VCO:TTID0 is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('SelectVehicleComposition',QuickWindow)     ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:VehicleComposition.Close
  END
  IF SELF.Opened
    INIMgr.Update('SelectVehicleComposition',QuickWindow)  ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Select a Transporter Record
!!! </summary>
SelectTransporter PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Transporter)
                       PROJECT(TRA:TID)
                       PROJECT(TRA:TransporterName)
                       PROJECT(TRA:BID)
                       PROJECT(TRA:ACID)
                       PROJECT(TRA:OpsManager)
                       PROJECT(TRA:VATNo)
                       PROJECT(TRA:Linked_CID)
                       PROJECT(TRA:ChargesVAT)
                       PROJECT(TRA:Broking)
                       PROJECT(TRA:AID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
TRA:TID                LIKE(TRA:TID)                  !List box control field - type derived from field
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
TRA:BID                LIKE(TRA:BID)                  !List box control field - type derived from field
TRA:ACID               LIKE(TRA:ACID)                 !List box control field - type derived from field
TRA:OpsManager         LIKE(TRA:OpsManager)           !List box control field - type derived from field
TRA:VATNo              LIKE(TRA:VATNo)                !List box control field - type derived from field
TRA:Linked_CID         LIKE(TRA:Linked_CID)           !List box control field - type derived from field
TRA:ChargesVAT         LIKE(TRA:ChargesVAT)           !List box control field - type derived from field
TRA:Broking            LIKE(TRA:Broking)              !List box control field - type derived from field
TRA:AID                LIKE(TRA:AID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Transporter Record'),AT(,,358,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('SelectTransporter'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80R(2)|M~TID~C(0)@n_10@80L(2)|M~T' & |
  'ransporter Name~L(2)@s35@80R(2)|M~BID~C(0)@n_10@80R(2)|M~ACID~C(0)@n_10@80L(2)|M~Ops' & |
  ' Manager~L(2)@s35@80L(2)|M~VAT No~L(2)@s20@80R(2)|M~Linked CID~C(0)@n_10@48R(2)|M~Ch' & |
  'arges VAT~C(0)@n3@32R(2)|M~Broking~C(0)@n3@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he Transporter file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Transporter Name'),USE(?Tab:2)
                         END
                         TAB('&2) By Branch ID'),USE(?Tab:3)
                         END
                         TAB('&3) By Address ID'),USE(?Tab:4)
                         END
                         TAB('&4) By Accountant ID'),USE(?Tab:5)
                         END
                       END
                       BUTTON('&Close'),AT(252,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 4
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectTransporter')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Transporter.SetOpenRelated()
  Relate:Transporter.Open                                  ! File Transporter used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Transporter,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,TRA:FKey_BID)                         ! Add the sort order for TRA:FKey_BID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,TRA:BID,1,BRW1)                ! Initialize the browse locator using  using key: TRA:FKey_BID , TRA:BID
  BRW1.AddSortOrder(,TRA:FKey_AID)                         ! Add the sort order for TRA:FKey_AID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,TRA:AID,1,BRW1)                ! Initialize the browse locator using  using key: TRA:FKey_AID , TRA:AID
  BRW1.AddSortOrder(,TRA:FKey_ACID)                        ! Add the sort order for TRA:FKey_ACID for sort order 3
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort3:Locator.Init(,TRA:ACID,1,BRW1)               ! Initialize the browse locator using  using key: TRA:FKey_ACID , TRA:ACID
  BRW1.AddSortOrder(,TRA:Key_TransporterName)              ! Add the sort order for TRA:Key_TransporterName for sort order 4
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 4
  BRW1::Sort0:Locator.Init(,TRA:TransporterName,1,BRW1)    ! Initialize the browse locator using  using key: TRA:Key_TransporterName , TRA:TransporterName
  BRW1.AddField(TRA:TID,BRW1.Q.TRA:TID)                    ! Field TRA:TID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:TransporterName,BRW1.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW1.AddField(TRA:BID,BRW1.Q.TRA:BID)                    ! Field TRA:BID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:ACID,BRW1.Q.TRA:ACID)                  ! Field TRA:ACID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:OpsManager,BRW1.Q.TRA:OpsManager)      ! Field TRA:OpsManager is a hot field or requires assignment from browse
  BRW1.AddField(TRA:VATNo,BRW1.Q.TRA:VATNo)                ! Field TRA:VATNo is a hot field or requires assignment from browse
  BRW1.AddField(TRA:Linked_CID,BRW1.Q.TRA:Linked_CID)      ! Field TRA:Linked_CID is a hot field or requires assignment from browse
  BRW1.AddField(TRA:ChargesVAT,BRW1.Q.TRA:ChargesVAT)      ! Field TRA:ChargesVAT is a hot field or requires assignment from browse
  BRW1.AddField(TRA:Broking,BRW1.Q.TRA:Broking)            ! Field TRA:Broking is a hot field or requires assignment from browse
  BRW1.AddField(TRA:AID,BRW1.Q.TRA:AID)                    ! Field TRA:AID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('SelectTransporter',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Transporter.Close
  END
  IF SELF.Opened
    INIMgr.Update('SelectTransporter',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

