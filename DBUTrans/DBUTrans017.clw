

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS017.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('DBUTRANS004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS014.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS016.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Invoice PROCEDURE 

CurrentTab           STRING(80)                            !
Locate               ULONG                                 !
BRW1::View:Browse    VIEW(_Invoice)
                       PROJECT(INV:IID)
                       PROJECT(INV:Weight)
                       PROJECT(INV:DID)
                       PROJECT(INV:DINo)
                       PROJECT(INV:MID)
                       PROJECT(INV:MIDs)
                       PROJECT(INV:Total)
                       PROJECT(INV:FreightCharge)
                       PROJECT(INV:POD_IID)
                       PROJECT(INV:CR_IID)
                       PROJECT(INV:BID)
                       PROJECT(INV:BranchName)
                       PROJECT(INV:CID)
                       PROJECT(INV:ClientNo)
                       PROJECT(INV:ClientReference)
                       PROJECT(INV:ClientName)
                       PROJECT(INV:ICID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
INV:IID                LIKE(INV:IID)                  !List box control field - type derived from field
INV:Weight             LIKE(INV:Weight)               !List box control field - type derived from field
INV:DID                LIKE(INV:DID)                  !List box control field - type derived from field
INV:DINo               LIKE(INV:DINo)                 !List box control field - type derived from field
INV:MID                LIKE(INV:MID)                  !List box control field - type derived from field
INV:MIDs               LIKE(INV:MIDs)                 !List box control field - type derived from field
INV:Total              LIKE(INV:Total)                !List box control field - type derived from field
INV:FreightCharge      LIKE(INV:FreightCharge)        !List box control field - type derived from field
INV:POD_IID            LIKE(INV:POD_IID)              !List box control field - type derived from field
INV:CR_IID             LIKE(INV:CR_IID)               !List box control field - type derived from field
INV:BID                LIKE(INV:BID)                  !List box control field - type derived from field
INV:BranchName         LIKE(INV:BranchName)           !List box control field - type derived from field
INV:CID                LIKE(INV:CID)                  !List box control field - type derived from field
INV:ClientNo           LIKE(INV:ClientNo)             !List box control field - type derived from field
INV:ClientReference    LIKE(INV:ClientReference)      !List box control field - type derived from field
INV:ClientName         LIKE(INV:ClientName)           !List box control field - type derived from field
INV:ICID               LIKE(INV:ICID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the _Invoice file'),AT(,,358,198),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Browse_Invoice'),SYSTEM
                       LIST,AT(8,36,342,118),USE(?Browse:1),HVSCROLL,FORMAT('42R(2)|M~Invoice No.~C(0)@n_10@44' & |
  'R(2)|M~Weight~C(0)@n-11.2@40R(2)|M~DID~C(0)@n_10@40R(2)|M~DI No.~C(0)@n_10@40R(2)|M~' & |
  'MID~C(0)@n_10@50L(2)|M~MIDs~C(0)@s20@50R(2)|M~Total~C(0)@n-15.2@50R(2)|M~Freight Cha' & |
  'rge~C(0)@n-15.2@40R(2)|M~POD IID~C(0)@n_10@40R(2)|M~CR IID~C(0)@n_10@40R(2)|M~BID~C(' & |
  '0)@n_10@50L(2)|M~Branch Name~@s35@40R(2)|M~CID~C(0)@n_10@40R(2)|M~Client No.~C(0)@n_' & |
  '10b@50L(2)|M~Client Ref.~C(0)@s60@80L(2)|M~Client Name~@s100@'),FROM(Queue:Browse:1),IMM, |
  MSG('Browsing the _Invoice file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       PROMPT('Locate:'),AT(9,22),USE(?Locate:Prompt)
                       ENTRY(@n_10),AT(36,22,60,10),USE(Locate),RIGHT(1)
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) Invoice ID'),USE(?Tab:2)
                           BUTTON('Select Branches'),AT(8,158,,14),USE(?SelectBranches),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&2) Invoice Comp.'),USE(?Tab:3)
                           BUTTON('Select Invoice Comp.'),AT(8,158,,14),USE(?Select_InvoiceComposition),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&3) Branch'),USE(?Tab:4)
                         END
                         TAB('&4) Client'),USE(?Tab:5)
                           BUTTON('Select Clients'),AT(8,158,,14),USE(?SelectClients),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&5) Delivery'),USE(?Tab:6)
                           BUTTON('Select Deliveries'),AT(8,158,,14),USE(?SelectDeliveries),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&6) DI No.'),USE(?Tab:7)
                         END
                         TAB('&9) Generating MID'),USE(?Tab:10)
                         END
                       END
                       BUTTON('&Close'),AT(252,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 4
BRW1::Sort4:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 5
BRW1::Sort5:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 6
BRW1::Sort8:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 7
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Invoice')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceComposition.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceJournals.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_Invoice,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,INV:FKey_ICID)                        ! Add the sort order for INV:FKey_ICID for sort order 1
  BRW1.AddRange(INV:ICID,Relate:_Invoice,Relate:_InvoiceComposition) ! Add file relationship range limit for sort order 1
  BRW1.AddSortOrder(,INV:FKey_BID)                         ! Add the sort order for INV:FKey_BID for sort order 2
  BRW1.AddRange(INV:BID,Relate:_Invoice,Relate:Branches)   ! Add file relationship range limit for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,INV:BID,1,BRW1)                ! Initialize the browse locator using  using key: INV:FKey_BID , INV:BID
  BRW1.AddSortOrder(,INV:FKey_CID)                         ! Add the sort order for INV:FKey_CID for sort order 3
  BRW1.AddRange(INV:CID,Relate:_Invoice,Relate:Clients)    ! Add file relationship range limit for sort order 3
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort3:Locator.Init(?Locate,INV:CID,1,BRW1)         ! Initialize the browse locator using ?Locate using key: INV:FKey_CID , INV:CID
  BRW1.AddSortOrder(,INV:FKey_DID)                         ! Add the sort order for INV:FKey_DID for sort order 4
  BRW1.AddLocator(BRW1::Sort4:Locator)                     ! Browse has a locator for sort order 4
  BRW1::Sort4:Locator.Init(?Locate,INV:DID,1,BRW1)         ! Initialize the browse locator using ?Locate using key: INV:FKey_DID , INV:DID
  BRW1.AddSortOrder(,INV:SKey_DINo)                        ! Add the sort order for INV:SKey_DINo for sort order 5
  BRW1.AddLocator(BRW1::Sort5:Locator)                     ! Browse has a locator for sort order 5
  BRW1::Sort5:Locator.Init(?Locate,INV:DINo,1,BRW1)        ! Initialize the browse locator using ?Locate using key: INV:SKey_DINo , INV:DINo
  BRW1.AddSortOrder(,INV:FKey_MID)                         ! Add the sort order for INV:FKey_MID for sort order 6
  BRW1.AddLocator(BRW1::Sort8:Locator)                     ! Browse has a locator for sort order 6
  BRW1::Sort8:Locator.Init(?Locate,INV:MID,1,BRW1)         ! Initialize the browse locator using ?Locate using key: INV:FKey_MID , INV:MID
  BRW1.AddSortOrder(,INV:PKey_IID)                         ! Add the sort order for INV:PKey_IID for sort order 7
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 7
  BRW1::Sort0:Locator.Init(?Locate,INV:IID,1,BRW1)         ! Initialize the browse locator using ?Locate using key: INV:PKey_IID , INV:IID
  BRW1.AddField(INV:IID,BRW1.Q.INV:IID)                    ! Field INV:IID is a hot field or requires assignment from browse
  BRW1.AddField(INV:Weight,BRW1.Q.INV:Weight)              ! Field INV:Weight is a hot field or requires assignment from browse
  BRW1.AddField(INV:DID,BRW1.Q.INV:DID)                    ! Field INV:DID is a hot field or requires assignment from browse
  BRW1.AddField(INV:DINo,BRW1.Q.INV:DINo)                  ! Field INV:DINo is a hot field or requires assignment from browse
  BRW1.AddField(INV:MID,BRW1.Q.INV:MID)                    ! Field INV:MID is a hot field or requires assignment from browse
  BRW1.AddField(INV:MIDs,BRW1.Q.INV:MIDs)                  ! Field INV:MIDs is a hot field or requires assignment from browse
  BRW1.AddField(INV:Total,BRW1.Q.INV:Total)                ! Field INV:Total is a hot field or requires assignment from browse
  BRW1.AddField(INV:FreightCharge,BRW1.Q.INV:FreightCharge) ! Field INV:FreightCharge is a hot field or requires assignment from browse
  BRW1.AddField(INV:POD_IID,BRW1.Q.INV:POD_IID)            ! Field INV:POD_IID is a hot field or requires assignment from browse
  BRW1.AddField(INV:CR_IID,BRW1.Q.INV:CR_IID)              ! Field INV:CR_IID is a hot field or requires assignment from browse
  BRW1.AddField(INV:BID,BRW1.Q.INV:BID)                    ! Field INV:BID is a hot field or requires assignment from browse
  BRW1.AddField(INV:BranchName,BRW1.Q.INV:BranchName)      ! Field INV:BranchName is a hot field or requires assignment from browse
  BRW1.AddField(INV:CID,BRW1.Q.INV:CID)                    ! Field INV:CID is a hot field or requires assignment from browse
  BRW1.AddField(INV:ClientNo,BRW1.Q.INV:ClientNo)          ! Field INV:ClientNo is a hot field or requires assignment from browse
  BRW1.AddField(INV:ClientReference,BRW1.Q.INV:ClientReference) ! Field INV:ClientReference is a hot field or requires assignment from browse
  BRW1.AddField(INV:ClientName,BRW1.Q.INV:ClientName)      ! Field INV:ClientName is a hot field or requires assignment from browse
  BRW1.AddField(INV:ICID,BRW1.Q.INV:ICID)                  ! Field INV:ICID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Invoice',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Invoice',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Invoice
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectBranches()
      ThisWindow.Reset
    OF ?Select_InvoiceComposition
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_InvoiceComposition()
      ThisWindow.Reset
    OF ?SelectClients
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectClients()
      ThisWindow.Reset
    OF ?SelectDeliveries
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_Deliveries()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?CurrentTab) = 6
    RETURN SELF.SetSort(5,Force)
  ELSIF CHOICE(?CurrentTab) = 7
    RETURN SELF.SetSort(6,Force)
  ELSE
    RETURN SELF.SetSort(7,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Select a _InvoiceComposition Record
!!! </summary>
Select_InvoiceComposition PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(_InvoiceComposition)
                       PROJECT(INCO:ICID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
INCO:ICID              LIKE(INCO:ICID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a _InvoiceComposition Record'),AT(,,158,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Select_InvoiceComposition'),SYSTEM
                       LIST,AT(8,30,142,124),USE(?Browse:1),HVSCROLL,FROM(Queue:Browse:1),IMM,MSG('Browsing th' & |
  'e _InvoiceComposition file')
                       BUTTON('&Select'),AT(101,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,150,172),USE(?CurrentTab)
                         TAB('&1) Record Order'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(52,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(105,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_InvoiceComposition')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:_InvoiceComposition.Open                          ! File _InvoiceComposition used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_InvoiceComposition,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,)                                     ! Add the sort order for  for sort order 1
  BRW1.AddField(INCO:ICID,BRW1.Q.INCO:ICID)                ! Field INCO:ICID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Select_InvoiceComposition',QuickWindow)    ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_InvoiceComposition.Close
  END
  IF SELF.Opened
    INIMgr.Update('Select_InvoiceComposition',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_InvoiceItems PROCEDURE 

CurrentTab           STRING(80)                            !
Locate               ULONG                                 !
BRW1::View:Browse    VIEW(_InvoiceItems)
                       PROJECT(INI:ITID)
                       PROJECT(INI:IID)
                       PROJECT(INI:DIID)
                       PROJECT(INI:ItemNo)
                       PROJECT(INI:Units)
                       PROJECT(INI:Type)
                       PROJECT(INI:Commodity)
                       PROJECT(INI:Description)
                       PROJECT(INI:CMID)
                       JOIN(DELI:PKey_DIID,INI:DIID)
                         PROJECT(DELI:ItemNo)
                         PROJECT(DELI:Units)
                         PROJECT(DELI:Weight)
                         PROJECT(DELI:DIID)
                         PROJECT(DELI:DID)
                         JOIN(DEL:PKey_DID,DELI:DID)
                           PROJECT(DEL:DID)
                           PROJECT(DEL:DINo)
                           PROJECT(DEL:CID)
                         END
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
INI:ITID               LIKE(INI:ITID)                 !List box control field - type derived from field
INI:IID                LIKE(INI:IID)                  !List box control field - type derived from field
INI:DIID               LIKE(INI:DIID)                 !List box control field - type derived from field
INI:ItemNo             LIKE(INI:ItemNo)               !List box control field - type derived from field
INI:Units              LIKE(INI:Units)                !List box control field - type derived from field
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Units             LIKE(DELI:Units)               !List box control field - type derived from field
DELI:Weight            LIKE(DELI:Weight)              !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !List box control field - type derived from field
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:CID                LIKE(DEL:CID)                  !List box control field - type derived from field
INI:Type               LIKE(INI:Type)                 !List box control field - type derived from field
INI:Commodity          LIKE(INI:Commodity)            !List box control field - type derived from field
INI:Description        LIKE(INI:Description)          !List box control field - type derived from field
INI:CMID               LIKE(INI:CMID)                 !Browse key field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Invoice Items file'),AT(,,332,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_InvoiceItems'),SYSTEM
                       LIST,AT(8,34,316,120),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~ITID~C(0)@n_10@40R(2)|M~' & |
  'IID~C(0)@n_10@40R(2)|M~DIID~C(0)@n_10@30R(2)|M~Item No.~C(0)@n6@24R(2)|M~Units~C(0)@' & |
  'n6@[30R(2)|M~Item No.~C(0)@n_5@24R(2)|M~Units~C(0)@n6@44R(2)|M~Weight~C(0)@n-11.2@40' & |
  'R(2)|M~DID~C(0)@n_10@40R(2)|M~DI No.~C(0)@n_10@40R(2)|M~CID~C(0)@n_10@]|M~DIID & DI~' & |
  '20R(2)|M~Type~C(0)@n3@80L(2)|M~Commodity~@s35@80L(2)|M~Description~@s150@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the _InvoiceItems file')
                       BUTTON('&Select'),AT(63,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(116,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(169,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(222,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(275,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       BUTTON('Open Invocie'),AT(4,180,,14),USE(?Button_IID)
                       BUTTON('Open DIID'),AT(99,180,49,14),USE(?Button_DIID)
                       BUTTON('Open DI'),AT(159,180,49,14),USE(?Button_DI)
                       PROMPT('Locate:'),AT(9,20),USE(?Locate:Prompt)
                       ENTRY(@n_10),AT(36,20,60,10),USE(Locate),RIGHT(1)
                       SHEET,AT(4,4,324,172),USE(?CurrentTab)
                         TAB('&0) By ITID'),USE(?Tab4)
                         END
                         TAB('&1) By Invoice'),USE(?Tab:2)
                           BUTTON('Select Invoice'),AT(8,158,,14),USE(?Select_Invoice),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&2) By Delivery Item'),USE(?Tab:3)
                         END
                         TAB('&3) By Commodity'),USE(?Tab:4)
                           BUTTON('Select Commodities'),AT(8,158,,14),USE(?SelectCommodities),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(226,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(279,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort3:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort1:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_InvoiceItems')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Commodities.SetOpenRelated()
  Relate:Commodities.Open                                  ! File Commodities used by this procedure, so make sure it's RelationManager is open
  Access:DeliveryItemAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItemAlias2.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_InvoiceItems,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,INI:FKey_IID)                         ! Add the sort order for INI:FKey_IID for sort order 1
  BRW1.AddRange(INI:IID,Relate:_InvoiceItems,Relate:_Invoice) ! Add file relationship range limit for sort order 1
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort3:Locator.Init(,INI:IID,1,BRW1)                ! Initialize the browse locator using  using key: INI:FKey_IID , INI:IID
  BRW1.AddSortOrder(,INI:FKey_DIID)                        ! Add the sort order for INI:FKey_DIID for sort order 2
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort1:Locator.Init(?Locate,INI:DIID,1,BRW1)        ! Initialize the browse locator using ?Locate using key: INI:FKey_DIID , INI:DIID
  BRW1.AddSortOrder(,INI:FKey_CMID)                        ! Add the sort order for INI:FKey_CMID for sort order 3
  BRW1.AddRange(INI:CMID,Relate:_InvoiceItems,Relate:Commodities) ! Add file relationship range limit for sort order 3
  BRW1.AddSortOrder(,INI:PKey_ITID)                        ! Add the sort order for INI:PKey_ITID for sort order 4
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 4
  BRW1::Sort0:Locator.Init(?Locate,INI:ITID,1,BRW1)        ! Initialize the browse locator using ?Locate using key: INI:PKey_ITID , INI:ITID
  BRW1.AddField(INI:ITID,BRW1.Q.INI:ITID)                  ! Field INI:ITID is a hot field or requires assignment from browse
  BRW1.AddField(INI:IID,BRW1.Q.INI:IID)                    ! Field INI:IID is a hot field or requires assignment from browse
  BRW1.AddField(INI:DIID,BRW1.Q.INI:DIID)                  ! Field INI:DIID is a hot field or requires assignment from browse
  BRW1.AddField(INI:ItemNo,BRW1.Q.INI:ItemNo)              ! Field INI:ItemNo is a hot field or requires assignment from browse
  BRW1.AddField(INI:Units,BRW1.Q.INI:Units)                ! Field INI:Units is a hot field or requires assignment from browse
  BRW1.AddField(DELI:ItemNo,BRW1.Q.DELI:ItemNo)            ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW1.AddField(DELI:Units,BRW1.Q.DELI:Units)              ! Field DELI:Units is a hot field or requires assignment from browse
  BRW1.AddField(DELI:Weight,BRW1.Q.DELI:Weight)            ! Field DELI:Weight is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DID,BRW1.Q.DEL:DID)                    ! Field DEL:DID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DINo,BRW1.Q.DEL:DINo)                  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW1.AddField(DEL:CID,BRW1.Q.DEL:CID)                    ! Field DEL:CID is a hot field or requires assignment from browse
  BRW1.AddField(INI:Type,BRW1.Q.INI:Type)                  ! Field INI:Type is a hot field or requires assignment from browse
  BRW1.AddField(INI:Commodity,BRW1.Q.INI:Commodity)        ! Field INI:Commodity is a hot field or requires assignment from browse
  BRW1.AddField(INI:Description,BRW1.Q.INI:Description)    ! Field INI:Description is a hot field or requires assignment from browse
  BRW1.AddField(INI:CMID,BRW1.Q.INI:CMID)                  ! Field INI:CMID is a hot field or requires assignment from browse
  BRW1.AddField(DELI:DIID,BRW1.Q.DELI:DIID)                ! Field DELI:DIID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_InvoiceItems',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Commodities.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_InvoiceItems',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_InvoiceItems
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_DIID
      BRW1.UpdateViewRecord()
    OF ?Button_DI
      BRW1.UpdateViewRecord()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_IID
      ThisWindow.Update()
      BRW1.UpdateViewRecord()
          INV:IID     = INI:IID
          IF Access:_Invoice.TryFetch(INV:PKey_IID) ~= LEVEL:Benign
             CLEAR(INV:Record)
          ELSE
             GlobalRequest = ChangeRecord
             Update_Invoice
          .
      
      
      BRW1.ResetSort(1)
    OF ?Button_DIID
      ThisWindow.Update()
      GlobalRequest = ChangeRecord
      Update_Delivery_Items()
      ThisWindow.Reset
      BRW1.ResetSort(1)
    OF ?Button_DI
      ThisWindow.Update()
      GlobalRequest = ChangeRecord
      Update_Delivery()
      ThisWindow.Reset
      BRW1.ResetSort(1)
    OF ?Select_Invoice
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_Invoice()
      ThisWindow.Reset
    OF ?SelectCommodities
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectCommodities()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

