

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS005.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('DBUTRANS002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS013.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_InvoiceTransporter PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(_RemittanceItems)
                       PROJECT(REMIT:REMIID)
                       PROJECT(REMIT:REMID)
                       PROJECT(REMIT:InvoiceDate)
                       PROJECT(REMIT:InvoiceTime)
                       PROJECT(REMIT:MID)
                       PROJECT(REMIT:AmountPaid)
                       PROJECT(REMIT:TIN)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
REMIT:REMIID           LIKE(REMIT:REMIID)             !List box control field - type derived from field
REMIT:REMID            LIKE(REMIT:REMID)              !List box control field - type derived from field
REMIT:InvoiceDate      LIKE(REMIT:InvoiceDate)        !List box control field - type derived from field
REMIT:InvoiceTime      LIKE(REMIT:InvoiceTime)        !List box control field - type derived from field
REMIT:MID              LIKE(REMIT:MID)                !List box control field - type derived from field
REMIT:AmountPaid       LIKE(REMIT:AmountPaid)         !List box control field - type derived from field
REMIT:TIN              LIKE(REMIT:TIN)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(TransporterPaymentsAllocations)
                       PROJECT(TRAPA:TRPAID)
                       PROJECT(TRAPA:TPID)
                       PROJECT(TRAPA:AllocationNo)
                       PROJECT(TRAPA:AllocationDate)
                       PROJECT(TRAPA:AllocationTime)
                       PROJECT(TRAPA:Amount)
                       PROJECT(TRAPA:Comment)
                       PROJECT(TRAPA:MID)
                       PROJECT(TRAPA:StatusUpToDate)
                       PROJECT(TRAPA:TIN)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
TRAPA:TRPAID           LIKE(TRAPA:TRPAID)             !List box control field - type derived from field
TRAPA:TPID             LIKE(TRAPA:TPID)               !List box control field - type derived from field
TRAPA:AllocationNo     LIKE(TRAPA:AllocationNo)       !List box control field - type derived from field
TRAPA:AllocationDate   LIKE(TRAPA:AllocationDate)     !List box control field - type derived from field
TRAPA:AllocationTime   LIKE(TRAPA:AllocationTime)     !List box control field - type derived from field
TRAPA:Amount           LIKE(TRAPA:Amount)             !List box control field - type derived from field
TRAPA:Comment          LIKE(TRAPA:Comment)            !List box control field - type derived from field
TRAPA:MID              LIKE(TRAPA:MID)                !List box control field - type derived from field
TRAPA:StatusUpToDate   LIKE(TRAPA:StatusUpToDate)     !List box control field - type derived from field
TRAPA:TIN              LIKE(TRAPA:TIN)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::INT:Record  LIKE(INT:RECORD),THREAD
QuickWindow          WINDOW('Form _InvoiceTransporter'),AT(,,358,182),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Update_InvoiceTransporter'),SYSTEM
                       SHEET,AT(4,4,350,156),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('TIN:'),AT(8,21),USE(?INT:TID:Prompt:2),TRN
                           ENTRY(@n_10),AT(84,21,60,10),USE(INT:TIN),RIGHT(1),MSG('Transporter Invoice No.')
                           PROMPT('CR TIN:'),AT(213,21),USE(?Prompt17)
                           ENTRY(@n_10),AT(263,21,60,10),USE(INT:CR_TIN),RIGHT(1),MSG('Transporter Invoice No.')
                           PROMPT('BID:'),AT(8,34),USE(?INT:BID:Prompt:2),TRN
                           ENTRY(@n_10),AT(84,34,60,10),USE(INT:BID),RIGHT(1),MSG('Branch ID')
                           PROMPT('MID:'),AT(8,48),USE(?INT:MID:Prompt:2),TRN
                           ENTRY(@n_10),AT(84,48,60,10),USE(INT:MID),RIGHT(1),MSG('Manifest ID')
                           PROMPT('DI No.:'),AT(8,62),USE(?INT:DINo:Prompt),TRN
                           ENTRY(@n_10),AT(84,62,60,10),USE(INT:DINo),RIGHT(1),MSG('Delivery Instruction Number'),TIP('Delivery I' & |
  'nstruction Number')
                           PROMPT('TID:'),AT(213,62),USE(?Prompt17:2)
                           ENTRY(@n_10),AT(263,62,60,10),USE(INT:TID),RIGHT(1),MSG('Transporter ID')
                           PROMPT('Leg:'),AT(8,76),USE(?INT:Leg:Prompt),TRN
                           SPIN(@n5),AT(84,76,40,10),USE(INT:Leg),RIGHT(1),MSG('Leg Number'),TIP('Leg Number')
                           PROMPT('JID:'),AT(213,78),USE(?Prompt17:3)
                           PROMPT('Cost:'),AT(8,90),USE(?INT:Cost:Prompt),TRN
                           ENTRY(@n-14.2),AT(84,90,60,10),USE(INT:Cost),RIGHT(1)
                           PROMPT('DID:'),AT(213,92),USE(?Prompt17:4)
                           BUTTON('Get Next ID'),AT(262,38,49,14),USE(?Button_ID)
                           PROMPT('VAT:'),AT(8,104),USE(?INT:VAT:Prompt),TRN
                           ENTRY(@n-14.2),AT(84,104,60,10),USE(INT:VAT),RIGHT(1),MSG('VAT amount'),TIP('VAT amount')
                           PROMPT('DLID:'),AT(213,106),USE(?Prompt17:5)
                           PROMPT('Rate:'),AT(8,118),USE(?INT:Rate:Prompt),TRN
                           ENTRY(@n-13.4),AT(84,118,60,10),USE(INT:Rate),RIGHT(1),MSG('Rate per Kg'),TIP('Rate per Kg')
                           ENTRY(@n_10),AT(263,106,60,10),USE(INT:DLID),RIGHT(1),MSG('Delivery Leg ID')
                           ENTRY(@n_10),AT(263,78,60,10),USE(INT:JID),RIGHT(1),MSG('Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change')
                           PROMPT('VAT Rate:'),AT(8,132),USE(?INT:VATRate:Prompt),TRN
                           ENTRY(@n-7.2),AT(84,132,60,10),USE(INT:VATRate),RIGHT(1),MSG('VAT rate'),TIP('VAT rate')
                           ENTRY(@n_10),AT(263,92,60,10),USE(INT:DID),RIGHT(1),MSG('Delivery ID')
                           PROMPT('Invoice Date:'),AT(8,146),USE(?INT:InvoiceDate:Prompt),TRN
                           ENTRY(@d5b),AT(84,146,60,10),USE(INT:InvoiceDate)
                         END
                         TAB('&2) General (cont.)'),USE(?Tab:2)
                           PROMPT('Invoice Time:'),AT(8,20),USE(?INT:InvoiceTime:Prompt),TRN
                           ENTRY(@t7),AT(84,20,61,10),USE(INT:InvoiceTime)
                           PROMPT('Created Date:'),AT(8,34),USE(?INT:CreatedDate:Prompt),TRN
                           STRING(@d5b),AT(84,34,61,10),USE(INT:CreatedDate),RIGHT(1),TRN
                           PROMPT('Created Time:'),AT(8,48),USE(?INT:CreatedTime:Prompt),TRN
                           STRING(@t7),AT(84,48,61,10),USE(INT:CreatedTime),RIGHT(1),TRN
                           CHECK('Broking'),AT(84,62,61,8),USE(INT:Broking),MSG('Broking Invoice'),TIP('Broking Invoice')
                           CHECK('Manifest'),AT(158,62,61,8),USE(INT:Manifest),MSG('This is the Manifest invoice'),TIP('This is th' & |
  'e Manifest invoice')
                           PROMPT('Status:'),AT(8,74),USE(?INT:Status:Prompt),TRN
                           LIST,AT(84,74,61,10),USE(INT:Status),DROP(5),FROM('No Payments|#0|Partially Paid|#1|Cre' & |
  'dit Note|#2|Fully Paid|#3'),TIP('No Payments, Partially Paid, Credit Note, Fully Paid'), |
  MSG('used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid')
                           PROMPT('Status Up To Date:'),AT(8,88),USE(?INT:StatusUpToDate:Prompt),TRN
                           ENTRY(@n3),AT(84,88,40,10),USE(INT:StatusUpToDate),MSG('Has'),TIP('Has')
                           PROMPT('Comment:'),AT(8,102),USE(?INT:Comment:Prompt),TRN
                           ENTRY(@s255),AT(84,102,266,10),USE(INT:Comment)
                           CHECK('VAT Specified'),AT(84,116,,8),USE(INT:VAT_Specified),MSG('The user has chosen to' & |
  ' specify the VAT amount'),TIP('The user has chosen to specify the VAT amount')
                           CHECK('Extra Inv'),AT(158,116,61,8),USE(INT:ExtraInv),MSG('Is this Invoice an Extra Invoice'), |
  TIP('Is this Invoice an Extra Invoice')
                         END
                         TAB('&3) Remittance Items'),USE(?Tab:3)
                           LIST,AT(8,20,342,118),USE(?Browse:2),HVSCROLL,FORMAT('40R(2)|M~REMIID~C(0)@n_10@40R(2)|' & |
  'M~REMID~C(0)@n_10@52R(2)|M~Invoice Date~C(0)@d17@40R(2)|M~Invoice Time~C(0)@t7@40R(2' & |
  ')|M~MID~C(0)@n_10@64R(1)|M~Amount Paid~C(0)@n-14.2@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he TransporterPaymentsAllocations file')
                           BUTTON('&Insert'),AT(195,142,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(248,142,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(301,142,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&4) Transporter Payments Allocations'),USE(?Tab:4)
                           LIST,AT(8,20,342,118),USE(?Browse:4),HVSCROLL,FORMAT('40R(2)|M~TRPAID~C(0)@n_10@40R(2)|' & |
  'M~TPID~C(0)@n_10@40R(2)|M~Allocation No~C(0)@n_5@50R(2)|M~Allocation Date~C(0)@d17@5' & |
  '0R(2)|M~Allocation Time~C(0)@t7@40R(1)|M~Amount~C(0)@n-14.2@80L(2)|M~Comment~@s255@4' & |
  '0R(2)|M~MID~C(0)@n_10@72R(2)|M~Status Up To Date~C(0)@n3@'),FROM(Queue:Browse:4),IMM,MSG('Browsing t' & |
  'he TransporterPaymentsAllocations file')
                           BUTTON('&Insert'),AT(195,142,49,14),USE(?Insert:5),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(248,142,49,14),USE(?Change:5),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(301,142,49,14),USE(?Delete:5),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(199,164,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(252,164,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(305,164,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Fuel Surcharge Record'
  OF InsertRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_InvoiceTransporter')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?INT:TID:Prompt:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:_InvoiceTransporter)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(INT:Record,History::INT:Record)
  SELF.AddHistoryField(?INT:TIN,1)
  SELF.AddHistoryField(?INT:CR_TIN,2)
  SELF.AddHistoryField(?INT:BID,4)
  SELF.AddHistoryField(?INT:MID,5)
  SELF.AddHistoryField(?INT:DINo,9)
  SELF.AddHistoryField(?INT:TID,3)
  SELF.AddHistoryField(?INT:Leg,11)
  SELF.AddHistoryField(?INT:Cost,15)
  SELF.AddHistoryField(?INT:VAT,16)
  SELF.AddHistoryField(?INT:Rate,17)
  SELF.AddHistoryField(?INT:DLID,10)
  SELF.AddHistoryField(?INT:JID,6)
  SELF.AddHistoryField(?INT:VATRate,18)
  SELF.AddHistoryField(?INT:DID,8)
  SELF.AddHistoryField(?INT:InvoiceDate,21)
  SELF.AddHistoryField(?INT:InvoiceTime,22)
  SELF.AddHistoryField(?INT:CreatedDate,25)
  SELF.AddHistoryField(?INT:CreatedTime,26)
  SELF.AddHistoryField(?INT:Broking,28)
  SELF.AddHistoryField(?INT:Manifest,29)
  SELF.AddHistoryField(?INT:Status,30)
  SELF.AddHistoryField(?INT:StatusUpToDate,31)
  SELF.AddHistoryField(?INT:Comment,32)
  SELF.AddHistoryField(?INT:VAT_Specified,33)
  SELF.AddHistoryField(?INT:ExtraInv,34)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:TransporterPaymentsAllocations.SetOpenRelated()
  Relate:TransporterPaymentsAllocations.Open               ! File TransporterPaymentsAllocations used by this procedure, so make sure it's RelationManager is open
  Access:_InvoiceTransporter.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_InvoiceTransporter
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:_RemittanceItems,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:TransporterPaymentsAllocations,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?INT:TIN{PROP:ReadOnly} = True
    ?INT:CR_TIN{PROP:ReadOnly} = True
    ?INT:DINo{PROP:ReadOnly} = True
    ?INT:TID{PROP:ReadOnly} = True
    ?INT:Cost{PROP:ReadOnly} = True
    DISABLE(?Button_ID)
    ?INT:VAT{PROP:ReadOnly} = True
    ?INT:Rate{PROP:ReadOnly} = True
    ?INT:DLID{PROP:ReadOnly} = True
    ?INT:JID{PROP:ReadOnly} = True
    ?INT:VATRate{PROP:ReadOnly} = True
    ?INT:DID{PROP:ReadOnly} = True
    ?INT:InvoiceDate{PROP:ReadOnly} = True
    ?INT:InvoiceTime{PROP:ReadOnly} = True
    DISABLE(?INT:Status)
    ?INT:StatusUpToDate{PROP:ReadOnly} = True
    ?INT:Comment{PROP:ReadOnly} = True
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    DISABLE(?Insert:5)
    DISABLE(?Change:5)
    DISABLE(?Delete:5)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,REMIT:FKey_TIN)                       ! Add the sort order for REMIT:FKey_TIN for sort order 1
  BRW2.AddRange(REMIT:TIN,Relate:_RemittanceItems,Relate:_InvoiceTransporter) ! Add file relationship range limit for sort order 1
  BRW2.AddField(REMIT:REMIID,BRW2.Q.REMIT:REMIID)          ! Field REMIT:REMIID is a hot field or requires assignment from browse
  BRW2.AddField(REMIT:REMID,BRW2.Q.REMIT:REMID)            ! Field REMIT:REMID is a hot field or requires assignment from browse
  BRW2.AddField(REMIT:InvoiceDate,BRW2.Q.REMIT:InvoiceDate) ! Field REMIT:InvoiceDate is a hot field or requires assignment from browse
  BRW2.AddField(REMIT:InvoiceTime,BRW2.Q.REMIT:InvoiceTime) ! Field REMIT:InvoiceTime is a hot field or requires assignment from browse
  BRW2.AddField(REMIT:MID,BRW2.Q.REMIT:MID)                ! Field REMIT:MID is a hot field or requires assignment from browse
  BRW2.AddField(REMIT:AmountPaid,BRW2.Q.REMIT:AmountPaid)  ! Field REMIT:AmountPaid is a hot field or requires assignment from browse
  BRW2.AddField(REMIT:TIN,BRW2.Q.REMIT:TIN)                ! Field REMIT:TIN is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4.AddSortOrder(,TRAPA:FKey_TIN)                       ! Add the sort order for TRAPA:FKey_TIN for sort order 1
  BRW4.AddRange(TRAPA:TIN,Relate:TransporterPaymentsAllocations,Relate:_InvoiceTransporter) ! Add file relationship range limit for sort order 1
  BRW4.AddField(TRAPA:TRPAID,BRW4.Q.TRAPA:TRPAID)          ! Field TRAPA:TRPAID is a hot field or requires assignment from browse
  BRW4.AddField(TRAPA:TPID,BRW4.Q.TRAPA:TPID)              ! Field TRAPA:TPID is a hot field or requires assignment from browse
  BRW4.AddField(TRAPA:AllocationNo,BRW4.Q.TRAPA:AllocationNo) ! Field TRAPA:AllocationNo is a hot field or requires assignment from browse
  BRW4.AddField(TRAPA:AllocationDate,BRW4.Q.TRAPA:AllocationDate) ! Field TRAPA:AllocationDate is a hot field or requires assignment from browse
  BRW4.AddField(TRAPA:AllocationTime,BRW4.Q.TRAPA:AllocationTime) ! Field TRAPA:AllocationTime is a hot field or requires assignment from browse
  BRW4.AddField(TRAPA:Amount,BRW4.Q.TRAPA:Amount)          ! Field TRAPA:Amount is a hot field or requires assignment from browse
  BRW4.AddField(TRAPA:Comment,BRW4.Q.TRAPA:Comment)        ! Field TRAPA:Comment is a hot field or requires assignment from browse
  BRW4.AddField(TRAPA:MID,BRW4.Q.TRAPA:MID)                ! Field TRAPA:MID is a hot field or requires assignment from browse
  BRW4.AddField(TRAPA:StatusUpToDate,BRW4.Q.TRAPA:StatusUpToDate) ! Field TRAPA:StatusUpToDate is a hot field or requires assignment from browse
  BRW4.AddField(TRAPA:TIN,BRW4.Q.TRAPA:TIN)                ! Field TRAPA:TIN is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_InvoiceTransporter',QuickWindow)    ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1
  BRW4.AskProcedure = 2
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TransporterPaymentsAllocations.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_InvoiceTransporter',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Update_RemittanceItems
      UpdateTransporterPaymentsAllocations
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_ID
      ThisWindow.Update()
      !    IF GLO:ReplicatedDatabaseID = 0
      !       GLO:ReplicatedDatabaseID = INV:BID
      !    .
      
          ! (FileManager FM, Key PrimaryKey, *? RecordID)
          Rep_GetNextID(Access:_InvoiceTransporter, INT:PKey_TIN, My_ID#)
      
      
          INT:TIN     = My_ID#
      
      
          DISPLAY
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:5
    SELF.ChangeControl=?Change:5
    SELF.DeleteControl=?Delete:5
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_ManifestLoad PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
b1          byte
b2          byte
BRW2::View:Browse    VIEW(ManifestLoadDeliveries)
                       PROJECT(MALD:MLDID)
                       PROJECT(MALD:MLID)
                       PROJECT(MALD:DIID)
                       PROJECT(MALD:UnitsLoaded)
                       JOIN(DELI:PKey_DIID,MALD:DIID)
                         PROJECT(DELI:DIID)
                         PROJECT(DELI:DID)
                         JOIN(DEL:PKey_DID,DELI:DID)
                           PROJECT(DEL:DINo)
                           PROJECT(DEL:DID)
                         END
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
MALD:MLDID             LIKE(MALD:MLDID)               !List box control field - type derived from field
MALD:MLID              LIKE(MALD:MLID)                !List box control field - type derived from field
MALD:DIID              LIKE(MALD:DIID)                !List box control field - type derived from field
MALD:UnitsLoaded       LIKE(MALD:UnitsLoaded)         !List box control field - type derived from field
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !List box control field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::MAL:Record  LIKE(MAL:RECORD),THREAD
QuickWindow          WINDOW('Form ManifestLoad'),AT(,,330,220),FONT('MS Sans Serif',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateManifestLoad'),SYSTEM
                       SHEET,AT(4,4,323,196),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('MID:'),AT(12,24),USE(?MAL:MID:Prompt),TRN
                           STRING(@n_10),AT(64,24,104,10),USE(MAL:MID),RIGHT(1),TRN
                           BUTTON('Get Next ID'),AT(213,44,49,14),USE(?Button_ID)
                           PROMPT('MLID:'),AT(12,44),USE(?MAL:MLID:Prompt:2)
                           ENTRY(@n_10),AT(108,44,60,10),USE(MAL:MLID),RIGHT(1),MSG('Manifest Load ID')
                         END
                         TAB('&2) Manifest Load Deliveries'),USE(?Tab:2)
                           LIST,AT(8,20,315,159),USE(?Browse:2),HVSCROLL,FORMAT('40R(2)|M~MLDID~C(0)@n_10@40R(2)|M' & |
  '~MLID~C(0)@n_10@40R(2)|M~DIID~C(0)@n_10@52R(2)|M~Units Loaded~C(0)@n6@50R(2)|M~DI No' & |
  '.~C(0)@n_10@50R(2)|M~DID~C(0)@n_10@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the Man' & |
  'ifestLoadDeliveries file')
                           BUTTON('Change DI'),AT(111,182,49,14),USE(?Button_ChangeID)
                           BUTTON('&Insert'),AT(168,182,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(220,182,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(274,182,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(172,202,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(226,202,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(278,202,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW2::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Manifest Load Record'
  OF InsertRecord
    ActionMessage = 'Manifest Load Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Manifest Load Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ManifestLoad')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?MAL:MID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:ManifestLoad)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(MAL:Record,History::MAL:Record)
  SELF.AddHistoryField(?MAL:MID,2)
  SELF.AddHistoryField(?MAL:MLID,1)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ManifestLoad.Open                                 ! File ManifestLoad used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ManifestLoad
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:ManifestLoadDeliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?Button_ID)
    ?MAL:MLID{PROP:ReadOnly} = True
    DISABLE(?Button_ChangeID)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW2.AddSortOrder(,MALD:FSKey_MLID_DIID)                 ! Add the sort order for MALD:FSKey_MLID_DIID for sort order 1
  BRW2.AddRange(MALD:MLID,Relate:ManifestLoadDeliveries,Relate:ManifestLoad) ! Add file relationship range limit for sort order 1
  BRW2.AddLocator(BRW2::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW2::Sort0:Locator.Init(,MALD:DIID,1,BRW2)              ! Initialize the browse locator using  using key: MALD:FSKey_MLID_DIID , MALD:DIID
  BRW2.AppendOrder('+MALD:MLDID')                          ! Append an additional sort order
  BRW2.AddField(MALD:MLDID,BRW2.Q.MALD:MLDID)              ! Field MALD:MLDID is a hot field or requires assignment from browse
  BRW2.AddField(MALD:MLID,BRW2.Q.MALD:MLID)                ! Field MALD:MLID is a hot field or requires assignment from browse
  BRW2.AddField(MALD:DIID,BRW2.Q.MALD:DIID)                ! Field MALD:DIID is a hot field or requires assignment from browse
  BRW2.AddField(MALD:UnitsLoaded,BRW2.Q.MALD:UnitsLoaded)  ! Field MALD:UnitsLoaded is a hot field or requires assignment from browse
  BRW2.AddField(DEL:DINo,BRW2.Q.DEL:DINo)                  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW2.AddField(DEL:DID,BRW2.Q.DEL:DID)                    ! Field DEL:DID is a hot field or requires assignment from browse
  BRW2.AddField(DELI:DIID,BRW2.Q.DELI:DIID)                ! Field DELI:DIID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_ManifestLoad',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ManifestLoad.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_ManifestLoad',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Manifest_Load_Deliveries
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_ChangeID
      BRW2.UpdateViewRecord()
          IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
          .
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_ID
      ThisWindow.Update()
      !    IF GLO:ReplicatedDatabaseID = 0
      !       GLO:ReplicatedDatabaseID = DEL:BID
      !    .
      
          ! (FileManager FM, Key PrimaryKey, *? RecordID)
          Rep_GetNextID(Access:ManifestLoad, MAL:PKey_MLID, My_ID#)
      
      !    message('My_ID#:  ' & My_ID# & '||GLO:ReplicatedDatabaseID: ' & GLO:ReplicatedDatabaseID)
      
          MAL:MLID     = My_ID#
      
      
          DISPLAY
    OF ?Button_ChangeID
      ThisWindow.Update()
      GlobalRequest = ChangeRecord
      Update_Delivery()
      ThisWindow.Reset
      !    b1    = 2
      !    b2    = 1
      !    BRW2.ResetFromAsk(b1,b2)
          BRW2.ResetSort(1)
      
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Form _RemittanceItems
!!! </summary>
Update_RemittanceItems PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::REMIT:Record LIKE(REMIT:RECORD),THREAD
QuickWindow          WINDOW('Form _RemittanceItems'),AT(,,176,112),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Update_RemittanceItems'),SYSTEM
                       SHEET,AT(4,4,168,86),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('REMID:'),AT(8,20),USE(?REMIT:REMID:Prompt),TRN
                           STRING(@n_10),AT(64,20,104,10),USE(REMIT:REMID),RIGHT(1),TRN
                           PROMPT('Invoice Date:'),AT(8,34),USE(?REMIT:InvoiceDate:Prompt),TRN
                           ENTRY(@d17),AT(64,34,104,10),USE(REMIT:InvoiceDate)
                           PROMPT('Invoice Time:'),AT(8,48),USE(?REMIT:InvoiceTime:Prompt),TRN
                           ENTRY(@t7),AT(64,48,104,10),USE(REMIT:InvoiceTime)
                           PROMPT('MID:'),AT(8,62),USE(?REMIT:MID:Prompt),TRN
                           STRING(@n_10),AT(64,62,104,10),USE(REMIT:MID),RIGHT(1),TRN
                           PROMPT('Amount Paid:'),AT(8,76),USE(?REMIT:AmountPaid:Prompt),TRN
                           ENTRY(@n-14.2),AT(64,76,64,10),USE(REMIT:AmountPaid),RIGHT(1)
                         END
                       END
                       BUTTON('&OK'),AT(17,94,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(70,94,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(123,94,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Fuel Surcharge Record'
  OF InsertRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_RemittanceItems')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REMIT:REMID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:_RemittanceItems)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(REMIT:Record,History::REMIT:Record)
  SELF.AddHistoryField(?REMIT:REMID,2)
  SELF.AddHistoryField(?REMIT:InvoiceDate,5)
  SELF.AddHistoryField(?REMIT:InvoiceTime,6)
  SELF.AddHistoryField(?REMIT:MID,8)
  SELF.AddHistoryField(?REMIT:AmountPaid,9)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:_RemittanceItems.SetOpenRelated()
  Relate:_RemittanceItems.Open                             ! File _RemittanceItems used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_RemittanceItems
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?REMIT:InvoiceDate{PROP:ReadOnly} = True
    ?REMIT:InvoiceTime{PROP:ReadOnly} = True
    ?REMIT:AmountPaid{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_RemittanceItems',QuickWindow)       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_RemittanceItems.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_RemittanceItems',QuickWindow)    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Manifest_Load_Deliveries PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::MALD:Record LIKE(MALD:RECORD),THREAD
QuickWindow          WINDOW('Form Manifest Load Deliveries'),AT(,,163,140),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('UpdateManifestLoadDeliveries'),SYSTEM
                       SHEET,AT(4,4,155,113),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Units Loaded:'),AT(8,20),USE(?MALD:UnitsLoaded:Prompt),TRN
                           SPIN(@n6),AT(110,20,43,10),USE(MALD:UnitsLoaded),MSG('Number of units'),TIP('Number of units')
                           ENTRY(@n_10),AT(94,36,60,10),USE(MALD:MLDID),RIGHT(1),MSG('Manifest Load Delivery Items ID')
                           BUTTON('Get Next ID'),AT(103,50,49,14),USE(?Button_ID)
                           PROMPT('MLID:'),AT(8,78),USE(?Prompt2:2)
                           BUTTON('...'),AT(76,76,12,12),USE(?CallLookup:2)
                           ENTRY(@n_10),AT(94,78,60,10),USE(MALD:MLID),RIGHT(1),MSG('Manifest Load ID')
                           PROMPT('DIID:'),AT(8,100),USE(?Prompt2:3)
                           BUTTON('...'),AT(76,98,12,12),USE(?CallLookup)
                           ENTRY(@n_10),AT(94,100,60,10),USE(MALD:DIID),RIGHT(1),MSG('Delivery Item ID')
                           PROMPT('MLDID:'),AT(8,36),USE(?Prompt2)
                         END
                       END
                       BUTTON('&OK'),AT(8,120,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(60,120,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(114,120,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Manifest Load Deliveries Record'
  OF InsertRecord
    ActionMessage = 'Manifest Load Deliveries Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Manifest Load Deliveries Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Manifest_Load_Deliveries')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?MALD:UnitsLoaded:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:ManifestLoadDeliveries)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(MALD:Record,History::MALD:Record)
  SELF.AddHistoryField(?MALD:UnitsLoaded,4)
  SELF.AddHistoryField(?MALD:MLDID,1)
  SELF.AddHistoryField(?MALD:MLID,2)
  SELF.AddHistoryField(?MALD:DIID,3)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:DeliveryItems.SetOpenRelated()
  Relate:DeliveryItems.Open                                ! File DeliveryItems used by this procedure, so make sure it's RelationManager is open
  Access:ManifestLoad.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ManifestLoadDeliveries
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?MALD:MLDID{PROP:ReadOnly} = True
    DISABLE(?Button_ID)
    DISABLE(?CallLookup:2)
    ?MALD:MLID{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?MALD:DIID{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Manifest_Load_Deliveries',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryItems.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Manifest_Load_Deliveries',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_ManifestLoads
      Browse_DeliveryItems
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_ID
      ThisWindow.Update()
      !    IF GLO:ReplicatedDatabaseID = 0
      !       GLO:ReplicatedDatabaseID = DEL:BID
      !    .
      
          ! (FileManager FM, Key PrimaryKey, *? RecordID)
          Rep_GetNextID(Access:ManifestLoadDeliveries, MALD:PKey_MLDID, My_ID#)
      
      !    message('My_ID#:  ' & My_ID# & '||GLO:ReplicatedDatabaseID: ' & GLO:ReplicatedDatabaseID)
      
          MALD:MLDID     = My_ID#
      
      
          DISPLAY
    OF ?CallLookup:2
      ThisWindow.Update()
      MAL:MLID = MALD:MLID
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        MALD:MLID = MAL:MLID
      END
      ThisWindow.Reset(1)
    OF ?MALD:MLID
      IF NOT QuickWindow{PROP:AcceptAll}
        IF MALD:MLID OR ?MALD:MLID{PROP:Req}
          MAL:MLID = MALD:MLID
          IF Access:ManifestLoad.TryFetch(MAL:PKey_MLID)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              MALD:MLID = MAL:MLID
            ELSE
              SELECT(?MALD:MLID)
              CYCLE
            END
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      DELI:DIID = MALD:DIID
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        MALD:DIID = DELI:DIID
      END
      ThisWindow.Reset(1)
    OF ?MALD:DIID
      IF NOT QuickWindow{PROP:AcceptAll}
        IF MALD:DIID OR ?MALD:DIID{PROP:Req}
          DELI:DIID = MALD:DIID
          IF Access:DeliveryItems.TryFetch(DELI:PKey_DIID)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              MALD:DIID = DELI:DIID
            ELSE
              SELECT(?MALD:DIID)
              CYCLE
            END
          END
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Rep_GetNextID        PROCEDURE  (FileManager FM, Key PrimaryKey, *? RecordID) ! Declare Procedure
LOC:Group            GROUP,PRE(LG)                         !
RangeFrom            ULONG                                 !
RangeTo              ULONG                                 !
RepCID               ULONG                                 !
                     END                                   !
NextId               LONG                                  !
SpecialCase_ReplicationTable BYTE,STATIC                   !
LOC:Exit             BYTE                                  !
Tek_Failed_File     STRING(100)

LastID Long(0)

TableName  CString(61)
FieldName  CString(61)
SQLRequest CString(256)
FileState  UShort
MyC     CLASS

Get_Range   PROCEDURE()
Get_Max     PROCEDURE()
Upd_Last    PROCEDURE(),LONG

    .
MyLock  CriticalSection


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ReplicationIDControl.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_SQLTemp.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ReplicationTableIDs.Open()
     .
     Access:ReplicationIDControl.UseFile()
     Access:_SQLTemp.UseFile()
     Access:ReplicationTableIDs.UseFile()
       ! (FileManager, Key, *?),ULong
       ! (FileManager FM, Key PrimaryKey, *? RecordID)

!    db.debugout('Rep_GetNextID - in - GLO:ReplicatedDatabaseID: ' & GLO:ReplicatedDatabaseID)

    ! Get the applicable range
    IF GLO:ReplicatedDatabaseID ~= 0                ! On mode
       TableName   = FM.GetName()                   ! Get table name

!        db.debugout('Table: ' & CLIP(TableName))

       IF CLIP(UPPER(TableName)) = UPPER('dbo.ReplicationTableIDs')
          MyLock.Wait()
          IF SpecialCase_ReplicationTable = FALSE
             SpecialCase_ReplicationTable = TRUE
          ELSE
             LOC:Exit   = TRUE
          .
          MyLock.Release()
       .

       IF LOC:Exit = FALSE
          ! Get available range and Last used ID
          MyC.Get_Range()

          IF LG:RangeTo = 0                           ! We have NO range
             MESSAGE('There is no Range specified for Replication Database ID: ' & GLO:ReplicatedDatabaseID,'Replication ID Error',ICON:Hand)
          ELSE
             MyC.Get_Max()

             IF NextID = -1
                ! ??? send admin email?
                MESSAGE('Could not allocate an ID, please try again.','Replication ID Error',ICON:Hand)
             ELSIF NextID > LG:RangeTo OR NextID = 0
                NextID = -2
                ! ??? send admin email?
                MESSAGE('The Range specified for Replication Database ID: ' & GLO:ReplicatedDatabaseID & ' has been exceeded.','Replication ID Error',ICON:Hand)
             .

             IF NextID > 0
                RecordID   = NextID
             ELSE
                RecordID   = -1
    .  .  .  .


    IF CLIP(UPPER(TableName)) = UPPER('dbo.ReplicationTableIDs') AND LOC:Exit = FALSE
       MyLock.Wait()
       SpecialCase_ReplicationTable = FALSE
       MyLock.Release()
    .
!    original
!
!    ! (FileManager, Key, *?, String),ULong
!    ! (FileManager FM, Key PrimaryKey, *? RecordID, String EssenceType)
!
!    FileState   = FM.SaveBuffer()
!
!
!    cf#         = PrimaryKey{PROP:Field,1}      ! 1st field of primary key
!    FieldName   = FM.File{PROP:Label,cf#}       ! Field name for this field
!    l#          = Len(Clip(FieldName))          ! Length of this field name
!    p#          = Instring(':',FieldName,1,1)   ! Prefix end position
!    FieldName   = FieldName[p# + 1 : l#]        ! Extract actual field name (external label??)
!
!    TableName   = FM.GetName()                  ! Get table name
!
!    ! Why > 0?
!    SQLRequest  = 'SELECT Max(' & FieldName & ') FROM ' & TableName & ' WHERE ' & FieldName & ' >0'
!
!    NextID      = 1
!
!  !  message(NextID)
!
!    Access:SQL_File.Open()
!    Access:SQL_File.UseFile()
!
!  !  lock(SQL_FILE)
!  !  EMPTY(SQL_FILE)
!
!    RecordID    = 0
!
!    SQL_File{Prop:SQL} = SQLRequest
!    Access:SQL_File.Next()
!    IF ~ERRORCODE()
!       RecordID = SQL:Col_1
!       IF RecordID > 0 THEN NextID = RecordID + 1.
!    .
!
!    Access:SQL_File.Close()
!  !  message('out - ' & NextID)
!
!    ! Compare new ID with the pool
!    Access:LocalIDPool.Open()
!    Access:LocalIDPool.UseFile()
!    LIP:EssenceType = EssenceType
!    ans#            = Access:LocalIDPool.Fetch(LIP:By_EssenceType)
!    LastID = LIP:LastID
!    IF NextID <= LastID
!       NextID = LastID + 1
!    .
!    ! Updating the pool
!    LIP:EssenceType = EssenceType
!    LIP:LastID      = NextID
!    IF ans# = Level:Benign then
!       Access:LocalIDPool.Update()
!    ELSE
!       Access:LocalIDPool.Insert()
!    .
!    Access:LocalIDPool.Close()
!
!    FM.RestoreBuffer(FileState)
!
!    RETURN NextId
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(NextId)

    Exit

CloseFiles     Routine
    Relate:ReplicationIDControl.Close()
    Relate:_SQLTemp.Close()
    Relate:ReplicationTableIDs.Close()
    Exit
MyC.Get_Range   PROCEDURE()
    CODE
    TableName   = FM.GetName()                  ! Get table name

    ! Get available range and Last used ID

    _SQLTemp{PROP:SQL}  = 'SELECT RangeFrom, RangeTo '  & |
                 'FROM ReplicationIDControl '           & |
                 'WHERE ReplicatedDatabaseID = '        & GLO:ReplicatedDatabaseID & ' ' & |
                 'ORDER BY RepCID DESC'

    IF Access:_SQLTemp.Next() = LEVEL:Benign
       LG:RangeFrom = _SQ:S1
       LG:RangeTo   = _SQ:S2
    .

    _SQLTemp{PROP:SQL}  = 'SELECT LastID '      & |
                 'FROM ReplicationTableIDs '    & |
                 'WHERE TableName = <39>'       & TableName & '<39> '   & |
                 'AND ReplicatedDatabaseID = '  & GLO:ReplicatedDatabaseID
    IF Access:_SQLTemp.Next() = LEVEL:Benign
       LastID       = _SQ:S1
    .
    RETURN


MyC.Get_Max     PROCEDURE()

RecID       LONG
    CODE
    FileState   = FM.SaveBuffer()

    cf#         = PrimaryKey{PROP:Field,1}      ! 1st field of primary key
    FieldName   = FM.File{PROP:Label,cf#}       ! Field name for this field
    l#          = LEN(CLIP(FieldName))          ! Length of this field name
    p#          = INSTRING(':',FieldName,1,1)   ! Prefix end position
    FieldName   = FieldName[p# + 1 : l#]        ! Extract actual field name (external label??)

    LOOP 3 TIMES
       IF LG:RangeFrom < LastID
          LG:RangeFrom  = LastID
       .

       SQLRequest  = 'SELECT Max(' & FieldName & ') FROM ' & TableName & ' ' & |
                    'WHERE ' & FieldName & ' >= ' & LG:RangeFrom & ' AND ' & FieldName & ' <= ' & LG:RangeTo

       NextID      = 1
       RecID       = 0

       _SQLTemp{Prop:SQL} = SQLRequest
       IF Access:_SQLTemp.Next() = LEVEL:Benign
!       IF ~ERRORCODE()
          RecID     = _SQ:S1
          IF RecID > 0
             NextID = RecID + 1
       .  .

       IF NextID = 1
          NextID    = LG:RangeFrom
          IF NextID = 0
            !db.debugout('[Rep_GetNextID]  RangeFrom is 0!!!!!!!!!!!!!!!!!!!')
       .  .

    ! TEst code!   this is to purposefully make it fail

    !db.debugout('[Rep_GetNextID]  NextID: ' & NextID & ',  LastID: ' & LastID)

!    _SQLTemp{Prop:SQL} = 'UPDATE ReplicationTableIDs SET LastID = ' & NextID & ' WHERE TableName = <39>' & TableName & |
!                            '<39> AND LastID = ' & LastID

       IF SELF.Upd_Last() >= 0
          BREAK
       .
       NextID       = -1
    .

    FM.RestoreBuffer(FileState)
    RETURN



MyC.Upd_Last        PROCEDURE()
L:Ret   LONG
    CODE
    ! Check that the Table is in the table
    REPT:ReplicatedDatabaseID   = GLO:ReplicatedDatabaseID
    REPT:TableName              = TableName
    IF Access:ReplicationTableIDs.TryFetch(REPT:Key_ReplicationDatabaseIDTableName) ~= LEVEL:Benign
       CLEAR(REPT:Record)
       IF Access:ReplicationTableIDs.PrimeRecord() = LEVEL:Benign
          REPT:ReplicatedDatabaseID = GLO:ReplicatedDatabaseID
          REPT:TableName            = TableName
          IF Access:ReplicationTableIDs.Insert() = LEVEL:Benign
    .  .  .


    _SQLTemp{Prop:SQL} = 'SELECT LastID FROM ReplicationTableIDs WHERE TableName = <39>' & TableName & |
                            '<39> AND LastID = ' & LastID & ' AND ReplicatedDatabaseID = '  & GLO:ReplicatedDatabaseID
    IF Access:_SQLTemp.Next() ~= LEVEL:Benign
       L:Ret        = -1                        ! Already taken
    ELSE
       _SQLTemp{Prop:SQL} = 'UPDATE ReplicationTableIDs SET LastID = ' & NextID & ' WHERE TableName = <39>' & TableName & |
                               '<39> AND LastID = ' & LastID & ' AND ReplicatedDatabaseID = '  & GLO:ReplicatedDatabaseID
       IF ERRORCODE()
          L:Ret     = -2
       ELSE
          _SQLTemp{Prop:SQL} = 'SELECT LastID FROM ReplicationTableIDs WHERE TableName = <39>' & TableName & '<39> AND ReplicatedDatabaseID = '  & GLO:ReplicatedDatabaseID
          IF Access:_SQLTemp.Next() ~= LEVEL:Benign
             L:Ret  = -3
          ELSIF DEFORMAT(_SQ:S1) ~= NextID
             L:Ret  = -4
    .  .  .
    RETURN(L:Ret)



