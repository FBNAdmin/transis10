

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS018.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('DBUTRANS004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS019.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Update_Change_EmailAddress_Relationship PROCEDURE          ! Declare Procedure
LOC:AID              ULONG                                 !Address ID - note once used certain information should not be changeable, such as the suburb
!Views_Client_Add        VIEW(Addresses)
!        JOIN(CLI:FKey_AID, ADD:AID)
!    .
!
!CA_View         ViewManager
View_EmailAdd       VIEW(EmailAddresses)
    .

EA_View             ViewManager

  CODE
    EA_View.Init(View_EmailAdd, Relate:EmailAddresses)
    EA_View.AddSortOrder(EMAI:FKey_AID)

    _SQLTemp{PROP:SQL}  = 'select a.aid, c.cid '    & |
                            'from addresses as a '  & |
                            'join clients as c '    & |
                            'on a.aid = c.aid '     & |
                            'order by c.cid'
    IF ~ERRORCODE()
       LOOP
          NEXT(_SQLTemp)
          IF ERRORCODE()
             BREAK
          .

          LOC:AID   = CLIP(_SQLTemp._SQ:S1)

          EA_View.AddRange(EMAI:AID, LOC:AID)
          EA_View.Reset()
          LOOP
             IF EA_View.Next() ~= LEVEL:Benign
                BREAK
             .

             IF Access:EmailAddresses.TryFetch(EMAI:PKey_EAID) = LEVEL:Benign
                EMAI:CID   = CLIP(_SQLTemp._SQ:S2)
                Access:EmailAddresses.Update()
    .  .  .  .

    EA_View.Kill()


    
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_EmailAddresses PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(EmailAddresses)
                       PROJECT(EMAI:EmailName)
                       PROJECT(EMAI:EmailAddress)
                       PROJECT(EMAI:RateLetter)
                       PROJECT(EMAI:CID)
                       PROJECT(EMAI:EAID)
                       PROJECT(EMAI:AID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
EMAI:EmailName         LIKE(EMAI:EmailName)           !List box control field - type derived from field
EMAI:EmailAddress      LIKE(EMAI:EmailAddress)        !List box control field - type derived from field
EMAI:RateLetter        LIKE(EMAI:RateLetter)          !List box control field - type derived from field
EMAI:CID               LIKE(EMAI:CID)                 !List box control field - type derived from field
EMAI:EAID              LIKE(EMAI:EAID)                !Primary key field - type derived from field
EMAI:AID               LIKE(EMAI:AID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the EmailAddresses file'),AT(,,330,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_EmailAddresses'),SYSTEM
                       LIST,AT(8,30,314,124),USE(?Browse:1),HVSCROLL,FORMAT('70L(2)|M~Email Name~@s35@150L(2)|' & |
  'M~Email Address~@s255@40R(2)|M~Rate Letter~C(0)@n3@80R(2)|M~CID~C(0)@n_10@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the EmailAddresses file')
                       BUTTON('&Select'),AT(61,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(114,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(167,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(220,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(273,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,322,172),USE(?CurrentTab)
                         TAB('&1) By Address ID'),USE(?Tab:2)
                           BUTTON('Select Addresses'),AT(8,158,49,14),USE(?SelectAddresses),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&2) By Client'),USE(?Tab:3)
                           BUTTON('Select Clients'),AT(8,158,49,14),USE(?SelectClients),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(224,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(277,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_EmailAddresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EmailAddresses,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,EMAI:FKey_CID)                        ! Add the sort order for EMAI:FKey_CID for sort order 1
  BRW1.AddRange(EMAI:CID,Relate:EmailAddresses,Relate:Clients) ! Add file relationship range limit for sort order 1
  BRW1.AddSortOrder(,EMAI:FKey_AID)                        ! Add the sort order for EMAI:FKey_AID for sort order 2
  BRW1.AddRange(EMAI:AID,Relate:EmailAddresses,Relate:Addresses) ! Add file relationship range limit for sort order 2
  BRW1.AddField(EMAI:EmailName,BRW1.Q.EMAI:EmailName)      ! Field EMAI:EmailName is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:EmailAddress,BRW1.Q.EMAI:EmailAddress) ! Field EMAI:EmailAddress is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:RateLetter,BRW1.Q.EMAI:RateLetter)    ! Field EMAI:RateLetter is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:CID,BRW1.Q.EMAI:CID)                  ! Field EMAI:CID is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:EAID,BRW1.Q.EMAI:EAID)                ! Field EMAI:EAID is a hot field or requires assignment from browse
  BRW1.AddField(EMAI:AID,BRW1.Q.EMAI:AID)                  ! Field EMAI:AID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_EmailAddresses',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_EmailAddresses',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    UpdateEmailAddresses
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectAddresses
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectAddresses()
      ThisWindow.Reset
    OF ?SelectClients
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Clients()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

