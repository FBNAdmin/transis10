

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS009.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('DBUTRANS003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS017.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Select a Commodities Record
!!! </summary>
SelectCommodities PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Commodities)
                       PROJECT(COM:Commodity)
                       PROJECT(COM:CMID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Commodities Record'),AT(,,158,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('SelectCommodities'),SYSTEM
                       LIST,AT(8,30,142,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Commodity~L(2)@s35@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the Commodities file')
                       BUTTON('&Select'),AT(101,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,150,172),USE(?CurrentTab)
                         TAB('&1) By Commodity'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(52,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(105,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectCommodities')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Commodities.Open                                  ! File Commodities used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Commodities,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,COM:Key_Commodity)                    ! Add the sort order for COM:Key_Commodity for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,COM:Commodity,1,BRW1)          ! Initialize the browse locator using  using key: COM:Key_Commodity , COM:Commodity
  BRW1.AddField(COM:Commodity,BRW1.Q.COM:Commodity)        ! Field COM:Commodity is a hot field or requires assignment from browse
  BRW1.AddField(COM:CMID,BRW1.Q.COM:CMID)                  ! Field COM:CMID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('SelectCommodities',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Commodities.Close
  END
  IF SELF.Opened
    INIMgr.Update('SelectCommodities',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Delivery_Items PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(DeliveryItems_Components)
                       PROJECT(DELIC:DICID)
                       PROJECT(DELIC:Length)
                       PROJECT(DELIC:Breadth)
                       PROJECT(DELIC:Height)
                       PROJECT(DELIC:Volume)
                       PROJECT(DELIC:Volume_Unit)
                       PROJECT(DELIC:Units)
                       PROJECT(DELIC:Weight)
                       PROJECT(DELIC:VolumetricWeight)
                       PROJECT(DELIC:DIID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
DELIC:DICID            LIKE(DELIC:DICID)              !List box control field - type derived from field
DELIC:Length           LIKE(DELIC:Length)             !List box control field - type derived from field
DELIC:Breadth          LIKE(DELIC:Breadth)            !List box control field - type derived from field
DELIC:Height           LIKE(DELIC:Height)             !List box control field - type derived from field
DELIC:Volume           LIKE(DELIC:Volume)             !List box control field - type derived from field
DELIC:Volume_Unit      LIKE(DELIC:Volume_Unit)        !List box control field - type derived from field
DELIC:Units            LIKE(DELIC:Units)              !List box control field - type derived from field
DELIC:Weight           LIKE(DELIC:Weight)             !List box control field - type derived from field
DELIC:VolumetricWeight LIKE(DELIC:VolumetricWeight)   !List box control field - type derived from field
DELIC:DIID             LIKE(DELIC:DIID)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(_InvoiceItems)
                       PROJECT(INI:ITID)
                       PROJECT(INI:IID)
                       PROJECT(INI:ItemNo)
                       PROJECT(INI:Units)
                       PROJECT(INI:Commodity)
                       PROJECT(INI:Description)
                       PROJECT(INI:Type)
                       PROJECT(INI:DIID)
                       JOIN(INV:PKey_IID,INI:IID)
                         PROJECT(INV:ClientNo)
                         PROJECT(INV:ClientName)
                         PROJECT(INV:DID)
                         PROJECT(INV:DINo)
                         PROJECT(INV:MIDs)
                         PROJECT(INV:MID)
                         PROJECT(INV:IID)
                       END
                     END
Queue:Browse:InvoiceItems QUEUE                       !Queue declaration for browse/combo box using ?Browse:InvoiceItems
INI:ITID               LIKE(INI:ITID)                 !List box control field - type derived from field
INI:IID                LIKE(INI:IID)                  !List box control field - type derived from field
INI:ItemNo             LIKE(INI:ItemNo)               !List box control field - type derived from field
INI:Units              LIKE(INI:Units)                !List box control field - type derived from field
INV:ClientNo           LIKE(INV:ClientNo)             !List box control field - type derived from field
INV:ClientName         LIKE(INV:ClientName)           !List box control field - type derived from field
INV:DID                LIKE(INV:DID)                  !List box control field - type derived from field
INV:DINo               LIKE(INV:DINo)                 !List box control field - type derived from field
INV:MIDs               LIKE(INV:MIDs)                 !List box control field - type derived from field
INV:MID                LIKE(INV:MID)                  !List box control field - type derived from field
INI:Commodity          LIKE(INI:Commodity)            !List box control field - type derived from field
INI:Description        LIKE(INI:Description)          !List box control field - type derived from field
INI:Type               LIKE(INI:Type)                 !List box control field - type derived from field
INI:DIID               LIKE(INI:DIID)                 !List box control field - type derived from field
INV:IID                LIKE(INV:IID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(TripSheetDeliveries)
                       PROJECT(TRDI:TDID)
                       PROJECT(TRDI:TRID)
                       PROJECT(TRDI:DIID)
                       PROJECT(TRDI:UnitsLoaded)
                       PROJECT(TRDI:DeliveredDate)
                       PROJECT(TRDI:DeliveredTime)
                       PROJECT(TRDI:UnitsDelivered)
                       PROJECT(TRDI:UnitsNotAccepted)
                       PROJECT(TRDI:ClearSignature)
                       PROJECT(TRDI:Notes)
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?Browse:6
TRDI:TDID              LIKE(TRDI:TDID)                !List box control field - type derived from field
TRDI:TRID              LIKE(TRDI:TRID)                !List box control field - type derived from field
TRDI:DIID              LIKE(TRDI:DIID)                !List box control field - type derived from field
TRDI:UnitsLoaded       LIKE(TRDI:UnitsLoaded)         !List box control field - type derived from field
TRDI:DeliveredDate     LIKE(TRDI:DeliveredDate)       !List box control field - type derived from field
TRDI:DeliveredTime     LIKE(TRDI:DeliveredTime)       !List box control field - type derived from field
TRDI:UnitsDelivered    LIKE(TRDI:UnitsDelivered)      !List box control field - type derived from field
TRDI:UnitsNotAccepted  LIKE(TRDI:UnitsNotAccepted)    !List box control field - type derived from field
TRDI:ClearSignature    LIKE(TRDI:ClearSignature)      !List box control field - type derived from field
TRDI:Notes             LIKE(TRDI:Notes)               !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(ManifestLoadDeliveries)
                       PROJECT(MALD:MLDID)
                       PROJECT(MALD:MLID)
                       PROJECT(MALD:DIID)
                       PROJECT(MALD:UnitsLoaded)
                       JOIN(MAL:PKey_MLID,MALD:MLID)
                         PROJECT(MAL:MID)
                         PROJECT(MAL:TTID)
                         PROJECT(MAL:MLID)
                       END
                     END
Queue:Browse:8       QUEUE                            !Queue declaration for browse/combo box using ?Browse:8
MALD:MLDID             LIKE(MALD:MLDID)               !List box control field - type derived from field
MALD:MLID              LIKE(MALD:MLID)                !List box control field - type derived from field
MALD:DIID              LIKE(MALD:DIID)                !List box control field - type derived from field
MALD:UnitsLoaded       LIKE(MALD:UnitsLoaded)         !List box control field - type derived from field
MAL:MID                LIKE(MAL:MID)                  !List box control field - type derived from field
MAL:TTID               LIKE(MAL:TTID)                 !List box control field - type derived from field
MAL:MLID               LIKE(MAL:MLID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DELI:Record LIKE(DELI:RECORD),THREAD
QuickWindow          WINDOW('Form Delivery Items'),AT(,,381,239),FONT('MS Sans Serif',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateDeliveryItems'),SYSTEM
                       SHEET,AT(4,4,373,217),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('DIID:'),AT(8,21),USE(?Prompt18)
                           ENTRY(@n_10),AT(84,21,70,10),USE(DELI:DIID),RIGHT(1),MSG('Delivery Item ID')
                           BUTTON('Get Next ID'),AT(294,23,49,14),USE(?Button_ID)
                           PROMPT('DID:'),AT(8,34),USE(?DELI:DID:Prompt:2)
                           ENTRY(@n_10),AT(84,34,70,10),USE(DELI:DID),RIGHT(1),MSG('Delivery ID')
                           PROMPT('Item No.:'),AT(8,50),USE(?DELI:ItemNo:Prompt),TRN
                           SPIN(@n_5),AT(84,50,70,10),USE(DELI:ItemNo),RIGHT(1),MSG('Item Number'),TIP('Item Number')
                           PROMPT('Type:'),AT(8,64),USE(?DELI:Type:Prompt),TRN
                           LIST,AT(84,64,70,10),USE(DELI:Type),VSCROLL,DROP(5),FROM('Container|#0|Loose|#1'),MSG('Type of Item'), |
  TIP('Type of Item')
                           PROMPT('Container No.:'),AT(8,78),USE(?DELI:ContainerNo:Prompt),TRN
                           ENTRY(@s35),AT(84,78,144,10),USE(DELI:ContainerNo)
                           PROMPT('Container Vessel:'),AT(8,92),USE(?DELI:ContainerVessel:Prompt),TRN
                           ENTRY(@s35),AT(84,92,144,10),USE(DELI:ContainerVessel),MSG('Vessel this container arrived on'), |
  TIP('Vessel this container arrived on')
                           PROMPT('Seal No:'),AT(8,106),USE(?DELI:SealNo:Prompt),TRN
                           ENTRY(@s35),AT(84,106,144,10),USE(DELI:SealNo),MSG('Container Seal no.'),TIP('Container Seal no.')
                           PROMPT('CMID:'),AT(253,110),USE(?DELI:CMID:Prompt)
                           STRING(@n_10),AT(303,110),USE(DELI:CMID),RIGHT(1)
                           PROMPT('ETA:'),AT(8,124),USE(?DELI:ETA:Prompt),TRN
                           SPIN(@d6),AT(84,124,70,10),USE(DELI:ETA),MSG('Estimated time of arrival for this Vessel'), |
  TIP('Estimated time of arrival for this Vessel')
                           PROMPT('PTID:'),AT(255,124),USE(?DELI:PTID:Prompt)
                           STRING(@n_10),AT(305,124),USE(DELI:PTID),RIGHT(1)
                           CHECK('By Container'),AT(84,138,70,8),USE(DELI:ByContainer),MSG('Are the items arriving' & |
  ' by container'),TIP('Are the items arriving by container')
                           PROMPT('Length:'),AT(8,150),USE(?DELI:Length:Prompt),TRN
                           ENTRY(@n-7.3),AT(84,150,52,10),USE(DELI:Length),RIGHT(1),MSG('Length in metres'),TIP('Length in metres')
                           PROMPT('Breadth:'),AT(8,164),USE(?DELI:Breadth:Prompt),TRN
                           ENTRY(@n-7.3),AT(84,164,52,10),USE(DELI:Breadth),RIGHT(1),MSG('Breadth in metres'),TIP('Breadth in metres')
                           PROMPT('Height:'),AT(8,178),USE(?DELI:Height:Prompt),TRN
                           ENTRY(@n-7.3),AT(84,178,52,10),USE(DELI:Height),RIGHT(1),MSG('Height in metres'),TIP('Height in metres')
                           PROMPT('Volume:'),AT(8,192),USE(?DELI:Volume:Prompt),TRN
                           ENTRY(@n-11.3),AT(84,192,52,10),USE(DELI:Volume),RIGHT(1),MSG('Volume for manual entry'),TIP('Volume for' & |
  ' manual entry (metres cubed)')
                           PROMPT('Volume:'),AT(8,206),USE(?DELI:Volume_Unit:Prompt),TRN
                           ENTRY(@n-11.3),AT(84,206,52,10),USE(DELI:Volume_Unit),RIGHT(1),MSG('Volume of 1 unit'),TIP('Volume of 1 unit')
                           PROMPT('Units:'),AT(218,150),USE(?DELI:Units:Prompt),TRN
                           SPIN(@n6),AT(294,150,52,10),USE(DELI:Units),MSG('Number of units'),TIP('Number of units')
                           PROMPT('Weight:'),AT(218,162),USE(?DELI:Weight:Prompt),TRN
                           ENTRY(@n-11.2),AT(294,162,52,10),USE(DELI:Weight),RIGHT(1),MSG('In kg''s'),TIP('In kg''s')
                           PROMPT('Volumetric Ratio:'),AT(218,176),USE(?DELI:VolumetricRatio:Prompt),TRN
                           ENTRY(@n-11.2),AT(294,176,52,10),USE(DELI:VolumetricRatio),RIGHT(1),MSG('x square cubes' & |
  ' weigh this amount'),TIP('x square cubes weigh this amount')
                           PROMPT('Volumetric Weight:'),AT(218,190),USE(?DELI:VolumetricWeight:Prompt),TRN
                           ENTRY(@n-11.2),AT(294,190,52,10),USE(DELI:VolumetricWeight),RIGHT(1),MSG('Weight based ' & |
  'on Volumetric calculation'),TIP('Weight based on Volumetric calculation (in kgs)')
                           PROMPT('Delivered Units:'),AT(218,206),USE(?DELI:DeliveredUnits:Prompt),TRN
                           SPIN(@n6),AT(294,206,52,10),USE(DELI:DeliveredUnits),MSG('Units delivered'),TIP('Units delivered')
                         END
                         TAB('&4) Invoice Items'),USE(?Tab:4)
                           LIST,AT(8,30,365,168),USE(?Browse:InvoiceItems),HVSCROLL,FORMAT('40R(2)|M~ITID~C(0)@n_1' & |
  '0@40R(2)|M~IID~C(0)@n_10@30R(2)|M~Item No.~C(0)@n6@28R(2)|M~Units~C(0)@n6@[34R(2)|M~' & |
  'Client No.~C(0)@n_10b@50L(2)|M~Client Name~C(0)@s100@40R(2)|M~DID~C(0)@n_10@40R(2)|M' & |
  '~DI No.~C(0)@n_10@40L(2)|M~MIDs~C(0)@s20@40R(2)|M~MID~C(0)@n_10@]|M~Invoice~80L(2)|M' & |
  '~Commodity~@s35@80L(2)|M~Description~@s150@20R(2)|M~Type~C(0)@n3@40R(2)|M~DIID~C(0)@n_10@'), |
  FROM(Queue:Browse:InvoiceItems),IMM,MSG('Browsing the ManifestLoadDeliveries file')
                           BUTTON('Open Invoice'),AT(11,204,,14),USE(?Button_Invoice)
                           BUTTON('&Insert'),AT(218,202,49,14),USE(?Insert:5),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(272,202,49,14),USE(?Change:5),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(324,202,49,14),USE(?Delete:5),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&5) Trip Sheet Deliveries'),USE(?Tab:5)
                           LIST,AT(8,30,365,167),USE(?Browse:6),HVSCROLL,FORMAT('40R(2)|M~TDID~C(0)@n_10@40R(2)|M~' & |
  'TRID~C(0)@N_10@40R(2)|M~DIID~C(0)@n_10@52R(2)|M~Units Loaded~C(0)@n6@60R(2)|M~Delive' & |
  'red Date~C(0)@d6@56R(2)|M~Delivered Time~C(0)@t7@64R(2)|M~Units Delivered~C(0)@n6@76' & |
  'R(2)|M~Units Not Accepted~C(0)@n6@64R(2)|M~Clear Signature~C(0)@n3@80L(2)|M~Notes~@s255@'), |
  FROM(Queue:Browse:6),IMM,MSG('Browsing the ManifestLoadDeliveries file')
                           BUTTON('&Insert'),AT(218,202,49,14),USE(?Insert:7),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(272,202,49,14),USE(?Change:7),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(324,202,49,14),USE(?Delete:7),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&6) Manifest Load Deliveries'),USE(?Tab:6)
                           LIST,AT(8,30,278,116),USE(?Browse:8),HVSCROLL,FORMAT('40R(2)|M~MLDID~C(0)@n_10@40R(2)|M' & |
  '~MLID~C(0)@n_10@40R(2)|M~DIID~C(0)@n_10@52R(2)|M~Units Loaded~C(0)@n6@40R(2)|M~MID~C' & |
  '(0)@n_10@40R(2)|M~TTID~C(0)@n_10@'),FROM(Queue:Browse:8),IMM,MSG('Browsing the Manif' & |
  'estLoadDeliveries file')
                           BUTTON('&Insert'),AT(134,150,49,14),USE(?Insert:9),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(188,150,49,14),USE(?Change:9),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(240,150,49,14),USE(?Delete:9),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&3) Delivery Items Components'),USE(?Tab:3)
                           LIST,AT(8,30,365,116),USE(?Browse:2),HVSCROLL,FORMAT('40R(2)|M~DICID~C(0)@n_10@36R(2)|M' & |
  '~Length~C(0)@n8.0@36R(2)|M~Breadth~C(0)@n8.0@36R(2)|M~Height~C(0)@n8.0@52D(25)|M~Vol' & |
  'ume~C(0)@n-11.3@52D(15)|M~Volume Unit~C(0)@n-11.3@28R(2)|M~Units~C(0)@n6@52D(20)|M~W' & |
  'eight~C(0)@n-11.2@72D(12)|M~Volumetric Weight~C(0)@n-11.2@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he ManifestLoadDeliveries file')
                           BUTTON('&Insert'),AT(73,150,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(126,150,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(179,150,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(222,224,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(274,224,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(328,224,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW_InvoiceItems     CLASS(BrowseClass)                    ! Browse using ?Browse:InvoiceItems
Q                      &Queue:Browse:InvoiceItems     !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW6                 CLASS(BrowseClass)                    ! Browse using ?Browse:6
Q                      &Queue:Browse:6                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW8                 CLASS(BrowseClass)                    ! Browse using ?Browse:8
Q                      &Queue:Browse:8                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Delivery Items Record'
  OF InsertRecord
    ActionMessage = 'Delivery Items Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Delivery Items Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Delivery_Items')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt18
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:DeliveryItems)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DELI:Record,History::DELI:Record)
  SELF.AddHistoryField(?DELI:DIID,1)
  SELF.AddHistoryField(?DELI:DID,2)
  SELF.AddHistoryField(?DELI:ItemNo,3)
  SELF.AddHistoryField(?DELI:Type,5)
  SELF.AddHistoryField(?DELI:ContainerNo,10)
  SELF.AddHistoryField(?DELI:ContainerVessel,12)
  SELF.AddHistoryField(?DELI:SealNo,13)
  SELF.AddHistoryField(?DELI:CMID,4)
  SELF.AddHistoryField(?DELI:ETA,16)
  SELF.AddHistoryField(?DELI:PTID,26)
  SELF.AddHistoryField(?DELI:ByContainer,19)
  SELF.AddHistoryField(?DELI:Length,20)
  SELF.AddHistoryField(?DELI:Breadth,21)
  SELF.AddHistoryField(?DELI:Height,22)
  SELF.AddHistoryField(?DELI:Volume,23)
  SELF.AddHistoryField(?DELI:Volume_Unit,24)
  SELF.AddHistoryField(?DELI:Units,25)
  SELF.AddHistoryField(?DELI:Weight,27)
  SELF.AddHistoryField(?DELI:VolumetricRatio,28)
  SELF.AddHistoryField(?DELI:VolumetricWeight,29)
  SELF.AddHistoryField(?DELI:DeliveredUnits,31)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:DeliveryItemAlias.SetOpenRelated()
  Relate:DeliveryItemAlias.Open                            ! File DeliveryItemAlias used by this procedure, so make sure it's RelationManager is open
  Access:DeliveryItemAlias2.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveryItems
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:DeliveryItems_Components,SELF) ! Initialize the browse manager
  BRW_InvoiceItems.Init(?Browse:InvoiceItems,Queue:Browse:InvoiceItems.ViewPosition,BRW4::View:Browse,Queue:Browse:InvoiceItems,Relate:_InvoiceItems,SELF) ! Initialize the browse manager
  BRW6.Init(?Browse:6,Queue:Browse:6.ViewPosition,BRW6::View:Browse,Queue:Browse:6,Relate:TripSheetDeliveries,SELF) ! Initialize the browse manager
  BRW8.Init(?Browse:8,Queue:Browse:8.ViewPosition,BRW8::View:Browse,Queue:Browse:8,Relate:ManifestLoadDeliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?DELI:DIID{PROP:ReadOnly} = True
    DISABLE(?Button_ID)
    DISABLE(?DELI:Type)
    ?DELI:ContainerNo{PROP:ReadOnly} = True
    ?DELI:ContainerVessel{PROP:ReadOnly} = True
    ?DELI:SealNo{PROP:ReadOnly} = True
    ?DELI:Length{PROP:ReadOnly} = True
    ?DELI:Breadth{PROP:ReadOnly} = True
    ?DELI:Height{PROP:ReadOnly} = True
    ?DELI:Volume{PROP:ReadOnly} = True
    ?DELI:Volume_Unit{PROP:ReadOnly} = True
    ?DELI:Weight{PROP:ReadOnly} = True
    ?DELI:VolumetricRatio{PROP:ReadOnly} = True
    ?DELI:VolumetricWeight{PROP:ReadOnly} = True
    DISABLE(?Button_Invoice)
    DISABLE(?Insert:5)
    DISABLE(?Change:5)
    DISABLE(?Delete:5)
    DISABLE(?Insert:7)
    DISABLE(?Change:7)
    DISABLE(?Delete:7)
    DISABLE(?Insert:9)
    DISABLE(?Change:9)
    DISABLE(?Delete:9)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,DELIC:FKey_DIID)                      ! Add the sort order for DELIC:FKey_DIID for sort order 1
  BRW2.AddRange(DELIC:DIID,Relate:DeliveryItems_Components,Relate:DeliveryItems) ! Add file relationship range limit for sort order 1
  BRW2.AddField(DELIC:DICID,BRW2.Q.DELIC:DICID)            ! Field DELIC:DICID is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Length,BRW2.Q.DELIC:Length)          ! Field DELIC:Length is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Breadth,BRW2.Q.DELIC:Breadth)        ! Field DELIC:Breadth is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Height,BRW2.Q.DELIC:Height)          ! Field DELIC:Height is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Volume,BRW2.Q.DELIC:Volume)          ! Field DELIC:Volume is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Volume_Unit,BRW2.Q.DELIC:Volume_Unit) ! Field DELIC:Volume_Unit is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Units,BRW2.Q.DELIC:Units)            ! Field DELIC:Units is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:Weight,BRW2.Q.DELIC:Weight)          ! Field DELIC:Weight is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:VolumetricWeight,BRW2.Q.DELIC:VolumetricWeight) ! Field DELIC:VolumetricWeight is a hot field or requires assignment from browse
  BRW2.AddField(DELIC:DIID,BRW2.Q.DELIC:DIID)              ! Field DELIC:DIID is a hot field or requires assignment from browse
  BRW_InvoiceItems.Q &= Queue:Browse:InvoiceItems
  BRW_InvoiceItems.FileLoaded = 1                          ! This is a 'file loaded' browse
  BRW_InvoiceItems.AddSortOrder(,INI:FKey_DIID)            ! Add the sort order for INI:FKey_DIID for sort order 1
  BRW_InvoiceItems.AddRange(INI:DIID,DELI:DIID)            ! Add single value range limit for sort order 1
  BRW_InvoiceItems.AppendOrder('+INI:ITID')                ! Append an additional sort order
  BRW_InvoiceItems.AddField(INI:ITID,BRW_InvoiceItems.Q.INI:ITID) ! Field INI:ITID is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INI:IID,BRW_InvoiceItems.Q.INI:IID) ! Field INI:IID is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INI:ItemNo,BRW_InvoiceItems.Q.INI:ItemNo) ! Field INI:ItemNo is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INI:Units,BRW_InvoiceItems.Q.INI:Units) ! Field INI:Units is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INV:ClientNo,BRW_InvoiceItems.Q.INV:ClientNo) ! Field INV:ClientNo is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INV:ClientName,BRW_InvoiceItems.Q.INV:ClientName) ! Field INV:ClientName is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INV:DID,BRW_InvoiceItems.Q.INV:DID) ! Field INV:DID is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INV:DINo,BRW_InvoiceItems.Q.INV:DINo) ! Field INV:DINo is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INV:MIDs,BRW_InvoiceItems.Q.INV:MIDs) ! Field INV:MIDs is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INV:MID,BRW_InvoiceItems.Q.INV:MID) ! Field INV:MID is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INI:Commodity,BRW_InvoiceItems.Q.INI:Commodity) ! Field INI:Commodity is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INI:Description,BRW_InvoiceItems.Q.INI:Description) ! Field INI:Description is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INI:Type,BRW_InvoiceItems.Q.INI:Type) ! Field INI:Type is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INI:DIID,BRW_InvoiceItems.Q.INI:DIID) ! Field INI:DIID is a hot field or requires assignment from browse
  BRW_InvoiceItems.AddField(INV:IID,BRW_InvoiceItems.Q.INV:IID) ! Field INV:IID is a hot field or requires assignment from browse
  BRW6.Q &= Queue:Browse:6
  BRW6.AddSortOrder(,TRDI:FKey_DIID)                       ! Add the sort order for TRDI:FKey_DIID for sort order 1
  BRW6.AddRange(TRDI:DIID,Relate:TripSheetDeliveries,Relate:DeliveryItems) ! Add file relationship range limit for sort order 1
  BRW6.AddField(TRDI:TDID,BRW6.Q.TRDI:TDID)                ! Field TRDI:TDID is a hot field or requires assignment from browse
  BRW6.AddField(TRDI:TRID,BRW6.Q.TRDI:TRID)                ! Field TRDI:TRID is a hot field or requires assignment from browse
  BRW6.AddField(TRDI:DIID,BRW6.Q.TRDI:DIID)                ! Field TRDI:DIID is a hot field or requires assignment from browse
  BRW6.AddField(TRDI:UnitsLoaded,BRW6.Q.TRDI:UnitsLoaded)  ! Field TRDI:UnitsLoaded is a hot field or requires assignment from browse
  BRW6.AddField(TRDI:DeliveredDate,BRW6.Q.TRDI:DeliveredDate) ! Field TRDI:DeliveredDate is a hot field or requires assignment from browse
  BRW6.AddField(TRDI:DeliveredTime,BRW6.Q.TRDI:DeliveredTime) ! Field TRDI:DeliveredTime is a hot field or requires assignment from browse
  BRW6.AddField(TRDI:UnitsDelivered,BRW6.Q.TRDI:UnitsDelivered) ! Field TRDI:UnitsDelivered is a hot field or requires assignment from browse
  BRW6.AddField(TRDI:UnitsNotAccepted,BRW6.Q.TRDI:UnitsNotAccepted) ! Field TRDI:UnitsNotAccepted is a hot field or requires assignment from browse
  BRW6.AddField(TRDI:ClearSignature,BRW6.Q.TRDI:ClearSignature) ! Field TRDI:ClearSignature is a hot field or requires assignment from browse
  BRW6.AddField(TRDI:Notes,BRW6.Q.TRDI:Notes)              ! Field TRDI:Notes is a hot field or requires assignment from browse
  BRW8.Q &= Queue:Browse:8
  BRW8.AddSortOrder(,MALD:FKey_DIID)                       ! Add the sort order for MALD:FKey_DIID for sort order 1
  BRW8.AddRange(MALD:DIID,Relate:ManifestLoadDeliveries,Relate:DeliveryItems) ! Add file relationship range limit for sort order 1
  BRW8.AddField(MALD:MLDID,BRW8.Q.MALD:MLDID)              ! Field MALD:MLDID is a hot field or requires assignment from browse
  BRW8.AddField(MALD:MLID,BRW8.Q.MALD:MLID)                ! Field MALD:MLID is a hot field or requires assignment from browse
  BRW8.AddField(MALD:DIID,BRW8.Q.MALD:DIID)                ! Field MALD:DIID is a hot field or requires assignment from browse
  BRW8.AddField(MALD:UnitsLoaded,BRW8.Q.MALD:UnitsLoaded)  ! Field MALD:UnitsLoaded is a hot field or requires assignment from browse
  BRW8.AddField(MAL:MID,BRW8.Q.MAL:MID)                    ! Field MAL:MID is a hot field or requires assignment from browse
  BRW8.AddField(MAL:TTID,BRW8.Q.MAL:TTID)                  ! Field MAL:TTID is a hot field or requires assignment from browse
  BRW8.AddField(MAL:MLID,BRW8.Q.MAL:MLID)                  ! Field MAL:MLID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Delivery_Items',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1
  BRW_InvoiceItems.AskProcedure = 2
  BRW6.AskProcedure = 3
  BRW8.AskProcedure = 4
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryItemAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Delivery_Items',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      UpdateDeliveryItems_Components
      Update_InvoiceItems
      UpdateTripSheetDeliveries
      Update_Manifest_Load_Deliveries
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Invoice
      BRW_InvoiceItems.UpdateViewRecord()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_ID
      ThisWindow.Update()
      !    IF GLO:ReplicatedDatabaseID = 0
      !       GLO:ReplicatedDatabaseID = INV:BID
      !    .
      
          ! (FileManager FM, Key PrimaryKey, *? RecordID)
          Rep_GetNextID(Access:DeliveryItems, DELI:PKey_DIID, My_ID#)
      
      
          DELI:DIID     = My_ID#
      
      
          DISPLAY
    OF ?Button_Invoice
      ThisWindow.Update()
      GlobalRequest = ChangeRecord
      Update_Invoice()
      ThisWindow.Reset
      BRW_InvoiceItems.ResetSort(1)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW_InvoiceItems.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:5
    SELF.ChangeControl=?Change:5
    SELF.DeleteControl=?Delete:5
  END


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:7
    SELF.ChangeControl=?Change:7
    SELF.DeleteControl=?Delete:7
  END


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:9
    SELF.ChangeControl=?Change:9
    SELF.DeleteControl=?Delete:9
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Form DeliveryItems_Components
!!! </summary>
UpdateDeliveryItems_Components PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::DELIC:Record LIKE(DELIC:RECORD),THREAD
QuickWindow          WINDOW('Form DeliveryItems_Components'),AT(,,196,168),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('UpdateDeliveryItems_Components'),SYSTEM
                       SHEET,AT(4,4,188,142),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('DICID:'),AT(8,20),USE(?DELIC:DICID:Prompt),TRN
                           STRING(@n_10),AT(84,20,104,10),USE(DELIC:DICID),RIGHT(1),TRN
                           PROMPT('Length:'),AT(8,34),USE(?DELIC:Length:Prompt),TRN
                           ENTRY(@n8.0),AT(84,34,40,10),USE(DELIC:Length),RIGHT(1),MSG('Length in cm'),TIP('Length in cm')
                           PROMPT('Breadth:'),AT(8,48),USE(?DELIC:Breadth:Prompt),TRN
                           ENTRY(@n8.0),AT(84,48,40,10),USE(DELIC:Breadth),RIGHT(1),MSG('Breadth in cm'),TIP('Breadth in cm')
                           PROMPT('Height:'),AT(8,62),USE(?DELIC:Height:Prompt),TRN
                           ENTRY(@n8.0),AT(84,62,40,10),USE(DELIC:Height),RIGHT(1),MSG('Height in cm'),TIP('Height in cm')
                           PROMPT('Volume:'),AT(8,76),USE(?DELIC:Volume:Prompt),TRN
                           ENTRY(@n-11.3),AT(84,76,52,10),USE(DELIC:Volume),RIGHT(1),MSG('Volume for manual entry'),TIP('Volume for' & |
  ' manual entry (metres cubed)')
                           PROMPT('Volume Unit:'),AT(8,90),USE(?DELIC:Volume_Unit:Prompt),TRN
                           ENTRY(@n-11.3),AT(84,90,52,10),USE(DELIC:Volume_Unit),RIGHT(1),TIP('Volume of 1 unit'),MSG('Volume of 1 unit')
                           PROMPT('Units:'),AT(8,104),USE(?DELIC:Units:Prompt),TRN
                           SPIN(@n6),AT(84,104,43,10),USE(DELIC:Units),MSG('Number of units'),TIP('Number of units')
                           PROMPT('Weight:'),AT(8,118),USE(?DELIC:Weight:Prompt),TRN
                           ENTRY(@n-11.2),AT(84,118,52,10),USE(DELIC:Weight),RIGHT(1),MSG('In kg''s'),TIP('In kg''s')
                           PROMPT('Volumetric Weight:'),AT(8,132),USE(?DELIC:VolumetricWeight:Prompt),TRN
                           ENTRY(@n-11.2),AT(84,132,52,10),USE(DELIC:VolumetricWeight),RIGHT(1),MSG('Weight based ' & |
  'on Volumetric calculation'),TIP('Weight based on Volumetric calculation (in kgs)')
                         END
                       END
                       BUTTON('&OK'),AT(37,150,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(90,150,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(143,150,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Fuel Surcharge Record'
  OF InsertRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateDeliveryItems_Components')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DELIC:DICID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:DeliveryItems_Components)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DELIC:Record,History::DELIC:Record)
  SELF.AddHistoryField(?DELIC:DICID,1)
  SELF.AddHistoryField(?DELIC:Length,3)
  SELF.AddHistoryField(?DELIC:Breadth,4)
  SELF.AddHistoryField(?DELIC:Height,5)
  SELF.AddHistoryField(?DELIC:Volume,6)
  SELF.AddHistoryField(?DELIC:Volume_Unit,7)
  SELF.AddHistoryField(?DELIC:Units,8)
  SELF.AddHistoryField(?DELIC:Weight,9)
  SELF.AddHistoryField(?DELIC:VolumetricWeight,10)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:DeliveryItems_Components.SetOpenRelated()
  Relate:DeliveryItems_Components.Open                     ! File DeliveryItems_Components used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveryItems_Components
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?DELIC:Length{PROP:ReadOnly} = True
    ?DELIC:Breadth{PROP:ReadOnly} = True
    ?DELIC:Height{PROP:ReadOnly} = True
    ?DELIC:Volume{PROP:ReadOnly} = True
    ?DELIC:Volume_Unit{PROP:ReadOnly} = True
    ?DELIC:Weight{PROP:ReadOnly} = True
    ?DELIC:VolumetricWeight{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('UpdateDeliveryItems_Components',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryItems_Components.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateDeliveryItems_Components',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_InvoiceItems PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::INI:Record  LIKE(INI:RECORD),THREAD
QuickWindow          WINDOW('Form Invoice Items'),AT(,,358,172),FONT('MS Sans Serif',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_InvoiceItems'),SYSTEM
                       SHEET,AT(4,4,350,149),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Item No.:'),AT(8,20),USE(?INI:ItemNo:Prompt),TRN
                           SPIN(@n6),AT(50,20,43,10),USE(INI:ItemNo),MSG('Item Number'),TIP('Item Number')
                           PROMPT('ITID:'),AT(242,20),USE(?INI:ITID:Prompt:2)
                           ENTRY(@n_10),AT(296,20,52,9),USE(INI:ITID),RIGHT(1),MSG('Invoice Item ID')
                           PROMPT('Type:'),AT(8,34),USE(?INI:Type:Prompt),TRN
                           LIST,AT(50,34,100,10),USE(INI:Type),DROP(5),FROM('Container|#0|Loose|#1|Other|#2'),MSG('Type of Item'), |
  TIP('Type of Item')
                           BUTTON('Get Next ID'),AT(299,32,49,14),USE(?Button_ID)
                           PROMPT('Commodity:'),AT(8,48),USE(?INI:Commodity:Prompt),TRN
                           ENTRY(@s35),AT(50,48,144,10),USE(INI:Commodity),MSG('Commosity'),TIP('Commosity')
                           PROMPT('IID:'),AT(242,62),USE(?INI:ITID:Prompt:3)
                           BUTTON('...'),AT(280,60,12,12),USE(?CallLookup)
                           PROMPT('Description:'),AT(8,62),USE(?INI:Description:Prompt),TRN
                           ENTRY(@s150),AT(50,62,144,10),USE(INI:Description),MSG('Description'),TIP('Description')
                           PROMPT('DIID:'),AT(242,76),USE(?INI:ITID:Prompt:4)
                           ENTRY(@n_10),AT(296,76,52,10),USE(INI:DIID),RIGHT(1),MSG('Delivery Item ID')
                           BUTTON('...'),AT(280,76,12,12),USE(?CallLookup:2)
                           ENTRY(@n_10),AT(296,62,52,10),USE(INI:IID),RIGHT(1),MSG('Invoice ID')
                           PROMPT('Units:'),AT(8,76),USE(?INI:Units:Prompt),TRN
                           ENTRY(@n6),AT(50,76,40,10),USE(INI:Units)
                         END
                       END
                       BUTTON('&OK'),AT(199,154,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(252,154,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(305,154,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Invoice Items Record'
  OF InsertRecord
    ActionMessage = 'Invoice Items Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Invoice Items Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_InvoiceItems')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?INI:ItemNo:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:_InvoiceItems)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(INI:Record,History::INI:Record)
  SELF.AddHistoryField(?INI:ItemNo,4)
  SELF.AddHistoryField(?INI:ITID,1)
  SELF.AddHistoryField(?INI:Type,5)
  SELF.AddHistoryField(?INI:Commodity,7)
  SELF.AddHistoryField(?INI:Description,8)
  SELF.AddHistoryField(?INI:DIID,3)
  SELF.AddHistoryField(?INI:IID,2)
  SELF.AddHistoryField(?INI:Units,9)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:DeliveryItems.SetOpenRelated()
  Relate:DeliveryItems.Open                                ! File DeliveryItems used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_InvoiceItems
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?INI:ITID{PROP:ReadOnly} = True
    DISABLE(?INI:Type)
    DISABLE(?Button_ID)
    ?INI:Commodity{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?INI:Description{PROP:ReadOnly} = True
    ?INI:DIID{PROP:ReadOnly} = True
    DISABLE(?CallLookup:2)
    ?INI:IID{PROP:ReadOnly} = True
    ?INI:Units{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_InvoiceItems',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryItems.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_InvoiceItems',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_DeliveryItems
      Browse_Invoice
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_ID
      ThisWindow.Update()
      !    IF GLO:ReplicatedDatabaseID = 0
      !       GLO:ReplicatedDatabaseID = INV:BID
      !    .
      
          ! (FileManager FM, Key PrimaryKey, *? RecordID)
          Rep_GetNextID(Access:_InvoiceItems, INI:PKey_ITID, My_ID#)
      
      
          INI:ITID     = My_ID#
      
      
          DISPLAY
    OF ?CallLookup
      ThisWindow.Update()
      INV:IID = INI:IID
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        INI:IID = INV:IID
      END
      ThisWindow.Reset(1)
    OF ?INI:DIID
      IF NOT QuickWindow{PROP:AcceptAll}
        IF INI:DIID OR ?INI:DIID{PROP:Req}
          DELI:DIID = INI:DIID
          IF Access:DeliveryItems.TryFetch(DELI:PKey_DIID)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              INI:DIID = DELI:DIID
            ELSE
              SELECT(?INI:DIID)
              CYCLE
            END
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update()
      DELI:DIID = INI:DIID
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        INI:DIID = DELI:DIID
      END
      ThisWindow.Reset(1)
    OF ?INI:IID
      IF NOT QuickWindow{PROP:AcceptAll}
        IF INI:IID OR ?INI:IID{PROP:Req}
          INV:IID = INI:IID
          IF Access:_Invoice.TryFetch(INV:PKey_IID)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              INI:IID = INV:IID
            ELSE
              SELECT(?INI:IID)
              CYCLE
            END
          END
        END
      END
      ThisWindow.Reset()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Form TripSheetDeliveries
!!! </summary>
UpdateTripSheetDeliveries PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::TRDI:Record LIKE(TRDI:RECORD),THREAD
QuickWindow          WINDOW('Form TripSheetDeliveries'),AT(,,215,158),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('UpdateTripSheetDeliveries'),SYSTEM
                       SHEET,AT(4,4,207,132),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Units Loaded:'),AT(8,20),USE(?TRDI:UnitsLoaded:Prompt),TRN
                           SPIN(@n6),AT(88,20,43,10),USE(TRDI:UnitsLoaded),MSG('Number of units'),TIP('Number of units')
                           PROMPT('Delivered Date:'),AT(8,34),USE(?TRDI:DeliveredDate:Prompt),TRN
                           SPIN(@d6),AT(88,34,119,10),USE(TRDI:DeliveredDate),MSG('Date delivered'),TIP('Date delivered')
                           PROMPT('Delivered Time:'),AT(8,48),USE(?TRDI:DeliveredTime:Prompt),TRN
                           ENTRY(@t7),AT(88,48,104,10),USE(TRDI:DeliveredTime),MSG('Time delivered'),TIP('Time delivered')
                           PROMPT('Units Delivered:'),AT(8,62),USE(?TRDI:UnitsDelivered:Prompt),TRN
                           SPIN(@n6),AT(88,62,43,10),USE(TRDI:UnitsDelivered)
                           PROMPT('Units Not Accepted:'),AT(8,76),USE(?TRDI:UnitsNotAccepted:Prompt),TRN
                           SPIN(@n6),AT(88,76,43,10),USE(TRDI:UnitsNotAccepted),MSG('Units that were not accepted ' & |
  'for delivery'),TIP('Units that were not accepted for delivery')
                           CHECK('Clear Signature'),AT(88,90,72,8),USE(TRDI:ClearSignature),MSG('Does the POD have' & |
  ' a clear signature'),TIP('Does the POD have a clear signature')
                           PROMPT('Notes:'),AT(8,102),USE(?TRDI:Notes:Prompt),TRN
                           TEXT,AT(88,102,119,30),USE(TRDI:Notes),VSCROLL,MSG('Notes'),TIP('Notes')
                         END
                       END
                       BUTTON('&OK'),AT(56,140,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(109,140,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(162,140,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Fuel Surcharge Record'
  OF InsertRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateTripSheetDeliveries')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRDI:UnitsLoaded:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:TripSheetDeliveries)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(TRDI:Record,History::TRDI:Record)
  SELF.AddHistoryField(?TRDI:UnitsLoaded,4)
  SELF.AddHistoryField(?TRDI:DeliveredDate,8)
  SELF.AddHistoryField(?TRDI:DeliveredTime,9)
  SELF.AddHistoryField(?TRDI:UnitsDelivered,10)
  SELF.AddHistoryField(?TRDI:UnitsNotAccepted,11)
  SELF.AddHistoryField(?TRDI:ClearSignature,12)
  SELF.AddHistoryField(?TRDI:Notes,13)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:TripSheetDeliveries.Open                          ! File TripSheetDeliveries used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TripSheetDeliveries
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?TRDI:DeliveredTime{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('UpdateTripSheetDeliveries',QuickWindow)    ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TripSheetDeliveries.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateTripSheetDeliveries',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

