

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS014.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('DBUTRANS004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS015.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS019.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_DeliveryLegs PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(_InvoiceTransporter)
                       PROJECT(INT:TIN)
                       PROJECT(INT:TID)
                       PROJECT(INT:BID)
                       PROJECT(INT:MID)
                       PROJECT(INT:DINo)
                       PROJECT(INT:Leg)
                       PROJECT(INT:Cost)
                       PROJECT(INT:VAT)
                       PROJECT(INT:Rate)
                       PROJECT(INT:VATRate)
                       PROJECT(INT:DLID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
INT:TIN                LIKE(INT:TIN)                  !List box control field - type derived from field
INT:TID                LIKE(INT:TID)                  !List box control field - type derived from field
INT:BID                LIKE(INT:BID)                  !List box control field - type derived from field
INT:MID                LIKE(INT:MID)                  !List box control field - type derived from field
INT:DINo               LIKE(INT:DINo)                 !List box control field - type derived from field
INT:Leg                LIKE(INT:Leg)                  !List box control field - type derived from field
INT:Cost               LIKE(INT:Cost)                 !List box control field - type derived from field
INT:VAT                LIKE(INT:VAT)                  !List box control field - type derived from field
INT:Rate               LIKE(INT:Rate)                 !List box control field - type derived from field
INT:VATRate            LIKE(INT:VATRate)              !List box control field - type derived from field
INT:DLID               LIKE(INT:DLID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DELL:Record LIKE(DELL:RECORD),THREAD
QuickWindow          WINDOW('Form DeliveryLegs'),AT(,,173,194),FONT('MS Sans Serif',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateDeliveryLegs'),SYSTEM
                       SHEET,AT(4,4,165,169),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Leg:'),AT(8,20),USE(?DELL:Leg:Prompt),TRN
                           SPIN(@n5),AT(61,20,40,10),USE(DELL:Leg),RIGHT(1),MSG('Leg Number'),TIP('Leg Number')
                           PROMPT('TID:'),AT(8,34),USE(?DELL:TID:Prompt:2),TRN
                           ENTRY(@n_10),AT(61,34,60,10),USE(DELL:TID),RIGHT(1),MSG('Transporter ID')
                           PROMPT('Cost:'),AT(8,48),USE(?DELL:Cost:Prompt),TRN
                           ENTRY(@n-13.2),AT(61,48,60,10),USE(DELL:Cost),DECIMAL(12),MSG('Cost for this leg (from ' & |
  'Transporter)'),TIP('Cost for this leg (from Transporter)')
                           PROMPT('VAT Rate:'),AT(8,62),USE(?DELL:VATRate:Prompt),TRN
                           ENTRY(@n-7.2),AT(61,62,60,10),USE(DELL:VATRate),RIGHT(1),MSG('VAT rate'),TIP('VAT rate')
                           PROMPT('DLID:'),AT(8,82),USE(?Prompt5)
                           ENTRY(@n_10),AT(61,84,60,10),USE(DELL:DLID),RIGHT(1),MSG('Delivery Leg ID')
                           BUTTON('Get Next ID'),AT(67,98,49,14),USE(?Button_ID)
                           PROMPT('DID:'),AT(8,119),USE(?Prompt6)
                           ENTRY(@n_10),AT(61,119,60,10),USE(DELL:DID),RIGHT(1),MSG('Delivery ID')
                           PROMPT('JID:'),AT(8,135),USE(?Prompt6:2)
                           ENTRY(@n_10),AT(61,135,60,10),USE(DELL:JID),RIGHT(1),MSG('Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change')
                         END
                         TAB('&2) Invoice Transporter'),USE(?Tab:2)
                           LIST,AT(8,20,157,74),USE(?Browse:2),HVSCROLL,FORMAT('40R(2)|M~TIN~C(0)@n_10@40R(2)|M~TI' & |
  'D~C(0)@n_10@40R(2)|M~BID~C(0)@n_10@40R(2)|M~MID~C(0)@n_10@40R(2)|M~DI No.~C(0)@n_10@' & |
  '24R(2)|M~Leg~C(0)@n5@64R(1)|M~Cost~C(0)@n-14.2@64R(1)|M~VAT~C(0)@n-14.2@60R(1)|M~Rat' & |
  'e~C(0)@n-13.4@36R(1)|M~VAT Rate~C(0)@n-7.2@'),FROM(Queue:Browse:2),IMM,MSG('Browsing t' & |
  'he _InvoiceTransporter file')
                           BUTTON('&Insert'),AT(10,98,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(63,98,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(116,98,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(14,176,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(67,176,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(120,176,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Delivery Legs Record'
  OF InsertRecord
    ActionMessage = 'Delivery Legs Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Delivery Legs Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_DeliveryLegs')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DELL:Leg:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:DeliveryLegs)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DELL:Record,History::DELL:Record)
  SELF.AddHistoryField(?DELL:Leg,3)
  SELF.AddHistoryField(?DELL:TID,4)
  SELF.AddHistoryField(?DELL:Cost,9)
  SELF.AddHistoryField(?DELL:VATRate,10)
  SELF.AddHistoryField(?DELL:DLID,1)
  SELF.AddHistoryField(?DELL:DID,2)
  SELF.AddHistoryField(?DELL:JID,5)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:DeliveryLegs.SetOpenRelated()
  Relate:DeliveryLegs.Open                                 ! File DeliveryLegs used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveryLegs
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:_InvoiceTransporter,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?DELL:Cost{PROP:ReadOnly} = True
    ?DELL:VATRate{PROP:ReadOnly} = True
    ?DELL:DLID{PROP:ReadOnly} = True
    DISABLE(?Button_ID)
    ?DELL:DID{PROP:ReadOnly} = True
    ?DELL:JID{PROP:ReadOnly} = True
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,INT:FKey_DLID)                        ! Add the sort order for INT:FKey_DLID for sort order 1
  BRW2.AddRange(INT:DLID,Relate:_InvoiceTransporter,Relate:DeliveryLegs) ! Add file relationship range limit for sort order 1
  BRW2.AddField(INT:TIN,BRW2.Q.INT:TIN)                    ! Field INT:TIN is a hot field or requires assignment from browse
  BRW2.AddField(INT:TID,BRW2.Q.INT:TID)                    ! Field INT:TID is a hot field or requires assignment from browse
  BRW2.AddField(INT:BID,BRW2.Q.INT:BID)                    ! Field INT:BID is a hot field or requires assignment from browse
  BRW2.AddField(INT:MID,BRW2.Q.INT:MID)                    ! Field INT:MID is a hot field or requires assignment from browse
  BRW2.AddField(INT:DINo,BRW2.Q.INT:DINo)                  ! Field INT:DINo is a hot field or requires assignment from browse
  BRW2.AddField(INT:Leg,BRW2.Q.INT:Leg)                    ! Field INT:Leg is a hot field or requires assignment from browse
  BRW2.AddField(INT:Cost,BRW2.Q.INT:Cost)                  ! Field INT:Cost is a hot field or requires assignment from browse
  BRW2.AddField(INT:VAT,BRW2.Q.INT:VAT)                    ! Field INT:VAT is a hot field or requires assignment from browse
  BRW2.AddField(INT:Rate,BRW2.Q.INT:Rate)                  ! Field INT:Rate is a hot field or requires assignment from browse
  BRW2.AddField(INT:VATRate,BRW2.Q.INT:VATRate)            ! Field INT:VATRate is a hot field or requires assignment from browse
  BRW2.AddField(INT:DLID,BRW2.Q.INT:DLID)                  ! Field INT:DLID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_DeliveryLegs',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryLegs.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_DeliveryLegs',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_InvoiceTransporter
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_ID
      ThisWindow.Update()
      !    IF GLO:ReplicatedDatabaseID = 0
      !       GLO:ReplicatedDatabaseID = INV:BID
      !    .
      
          ! (FileManager FM, Key PrimaryKey, *? RecordID)
          Rep_GetNextID(Access:DeliveryLegs, DELL:PKey_DLID, My_ID#)
      
      
          DELL:DLID     = My_ID#
      
      
          DISPLAY
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Form DeliveryProgress
!!! </summary>
UpdateDeliveryProgress PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::DELP:Record LIKE(DELP:RECORD),THREAD
QuickWindow          WINDOW('Form DeliveryProgress'),AT(,,188,98),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('UpdateDeliveryProgress'),SYSTEM
                       SHEET,AT(4,4,180,72),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Status Date:'),AT(8,20),USE(?DELP:StatusDate:Prompt),TRN
                           SPIN(@d6),AT(61,20,119,10),USE(DELP:StatusDate),MSG('Date of status entry'),TIP('Date of st' & |
  'atus entry')
                           PROMPT('Status Time:'),AT(8,34),USE(?DELP:StatusTime:Prompt),TRN
                           ENTRY(@t7),AT(61,34,104,10),USE(DELP:StatusTime),MSG('Time of status entry'),TIP('Time of st' & |
  'atus entry')
                           PROMPT('Action Date:'),AT(8,48),USE(?DELP:ActionDate:Prompt),TRN
                           SPIN(@d6b),AT(61,48,119,10),USE(DELP:ActionDate)
                           PROMPT('Action Time:'),AT(8,62),USE(?DELP:ActionTime:Prompt),TRN
                           ENTRY(@t7),AT(61,62,104,10),USE(DELP:ActionTime)
                         END
                       END
                       BUTTON('&OK'),AT(29,80,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(82,80,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(135,80,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Shortages & Damages Record'
  OF InsertRecord
    ActionMessage = 'Shortages & Damages Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Shortages & Damages Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateDeliveryProgress')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DELP:StatusDate:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:DeliveryProgress)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DELP:Record,History::DELP:Record)
  SELF.AddHistoryField(?DELP:StatusDate,8)
  SELF.AddHistoryField(?DELP:StatusTime,9)
  SELF.AddHistoryField(?DELP:ActionDate,12)
  SELF.AddHistoryField(?DELP:ActionTime,13)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:DeliveryProgress.SetOpenRelated()
  Relate:DeliveryProgress.Open                             ! File DeliveryProgress used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveryProgress
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?DELP:StatusTime{PROP:ReadOnly} = True
    ?DELP:ActionTime{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('UpdateDeliveryProgress',QuickWindow)       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryProgress.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateDeliveryProgress',QuickWindow)    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Form _StatementItems
!!! </summary>
Update_StatementItems PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::STAI:Record LIKE(STAI:RECORD),THREAD
QuickWindow          WINDOW('Form _StatementItems'),AT(,,184,154),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Update_StatementItems'),SYSTEM
                       SHEET,AT(4,4,176,128),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('STID:'),AT(8,20),USE(?STAI:STID:Prompt),TRN
                           STRING(@n_10),AT(72,20,104,10),USE(STAI:STID),RIGHT(1),TRN
                           PROMPT('Invoice Date:'),AT(8,34),USE(?STAI:InvoiceDate:Prompt),TRN
                           ENTRY(@d17),AT(72,34,104,10),USE(STAI:InvoiceDate)
                           PROMPT('Invoice Time:'),AT(8,48),USE(?STAI:InvoiceTime:Prompt),TRN
                           ENTRY(@t7),AT(72,48,104,10),USE(STAI:InvoiceTime)
                           PROMPT('Invoice Number:'),AT(8,62),USE(?STAI:IID:Prompt),TRN
                           STRING(@n_10),AT(72,62,104,10),USE(STAI:IID),RIGHT(1),TRN
                           PROMPT('DI No.:'),AT(8,76),USE(?STAI:DINo:Prompt),TRN
                           ENTRY(@n_10),AT(72,76,104,10),USE(STAI:DINo),RIGHT(1),MSG('Delivery Instruction Number'),REQ, |
  TIP('Delivery Instruction Number')
                           PROMPT('Debit:'),AT(8,90),USE(?STAI:Debit:Prompt),TRN
                           ENTRY(@n-14.2),AT(72,90,64,10),USE(STAI:Debit),RIGHT(1)
                           PROMPT('Credit:'),AT(8,104),USE(?STAI:Credit:Prompt),TRN
                           ENTRY(@n-14.2),AT(72,104,64,10),USE(STAI:Credit),RIGHT(1)
                           PROMPT('Amount:'),AT(8,118),USE(?STAI:Amount:Prompt),TRN
                           ENTRY(@n-14.2),AT(72,118,64,10),USE(STAI:Amount),RIGHT(1)
                         END
                       END
                       BUTTON('&OK'),AT(25,136,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(78,136,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(131,136,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Shortages & Damages Record'
  OF InsertRecord
    ActionMessage = 'Shortages & Damages Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Shortages & Damages Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_StatementItems')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STAI:STID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:_StatementItems)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(STAI:Record,History::STAI:Record)
  SELF.AddHistoryField(?STAI:STID,2)
  SELF.AddHistoryField(?STAI:InvoiceDate,5)
  SELF.AddHistoryField(?STAI:InvoiceTime,6)
  SELF.AddHistoryField(?STAI:IID,7)
  SELF.AddHistoryField(?STAI:DINo,9)
  SELF.AddHistoryField(?STAI:Debit,10)
  SELF.AddHistoryField(?STAI:Credit,11)
  SELF.AddHistoryField(?STAI:Amount,12)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:_StatementItems.Open                              ! File _StatementItems used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_StatementItems
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?STAI:InvoiceDate{PROP:ReadOnly} = True
    ?STAI:InvoiceTime{PROP:ReadOnly} = True
    ?STAI:DINo{PROP:ReadOnly} = True
    ?STAI:Debit{PROP:ReadOnly} = True
    ?STAI:Credit{PROP:ReadOnly} = True
    ?STAI:Amount{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_StatementItems',QuickWindow)        ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_StatementItems.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_StatementItems',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Form ClientsPaymentsAllocation
!!! </summary>
UpdateClientsPaymentsAllocation PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::CLIPA:Record LIKE(CLIPA:RECORD),THREAD
QuickWindow          WINDOW('Form ClientsPaymentsAllocation'),AT(,,358,154),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('UpdateClientsPaymentsAllocation'),SYSTEM
                       SHEET,AT(4,4,350,128),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('CPID:'),AT(8,20),USE(?CLIPA:CPID:Prompt),TRN
                           STRING(@n_10),AT(84,20,104,10),USE(CLIPA:CPID),RIGHT(1),TRN
                           PROMPT('Allocation No:'),AT(8,34),USE(?CLIPA:AllocationNo:Prompt),TRN
                           STRING(@n_5),AT(84,34,104,10),USE(CLIPA:AllocationNo),RIGHT(1),TRN
                           PROMPT('Invoice Number:'),AT(8,48),USE(?CLIPA:IID:Prompt),TRN
                           STRING(@n_10),AT(84,48,104,10),USE(CLIPA:IID),RIGHT(1),TRN
                           PROMPT('Amount:'),AT(8,62),USE(?CLIPA:Amount:Prompt),TRN
                           ENTRY(@n-14.2),AT(84,62,64,10),USE(CLIPA:Amount),RIGHT(1),MSG('Amount of payment'),TIP('Amount of ' & |
  'payment allocated to this Invoice')
                           PROMPT('Allocation Date:'),AT(8,76),USE(?CLIPA:AllocationDate:Prompt),TRN
                           SPIN(@d17),AT(84,76,119,10),USE(CLIPA:AllocationDate),MSG('Date allocation made'),TIP('Date alloc' & |
  'ation made')
                           PROMPT('Allocation Time:'),AT(8,90),USE(?CLIPA:AllocationTime:Prompt),TRN
                           ENTRY(@t7),AT(84,90,104,10),USE(CLIPA:AllocationTime)
                           PROMPT('Comment:'),AT(8,104),USE(?CLIPA:Comment:Prompt),TRN
                           ENTRY(@s255),AT(84,104,266,10),USE(CLIPA:Comment),MSG('Comment'),TIP('Comment')
                           PROMPT('Status Up To Date:'),AT(8,118),USE(?CLIPA:StatusUpToDate:Prompt),TRN
                           ENTRY(@n3),AT(84,118,40,10),USE(CLIPA:StatusUpToDate),MSG('Has'),TIP('Has')
                         END
                       END
                       BUTTON('&OK'),AT(199,136,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(252,136,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(305,136,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Shortages & Damages Record'
  OF InsertRecord
    ActionMessage = 'Shortages & Damages Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Shortages & Damages Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateClientsPaymentsAllocation')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CLIPA:CPID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:ClientsPaymentsAllocation)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CLIPA:Record,History::CLIPA:Record)
  SELF.AddHistoryField(?CLIPA:CPID,2)
  SELF.AddHistoryField(?CLIPA:AllocationNo,3)
  SELF.AddHistoryField(?CLIPA:IID,4)
  SELF.AddHistoryField(?CLIPA:Amount,5)
  SELF.AddHistoryField(?CLIPA:AllocationDate,8)
  SELF.AddHistoryField(?CLIPA:AllocationTime,9)
  SELF.AddHistoryField(?CLIPA:Comment,10)
  SELF.AddHistoryField(?CLIPA:StatusUpToDate,11)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ClientsPaymentsAllocation.Open                    ! File ClientsPaymentsAllocation used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ClientsPaymentsAllocation
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?CLIPA:Amount{PROP:ReadOnly} = True
    ?CLIPA:AllocationTime{PROP:ReadOnly} = True
    ?CLIPA:Comment{PROP:ReadOnly} = True
    ?CLIPA:StatusUpToDate{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('UpdateClientsPaymentsAllocation',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ClientsPaymentsAllocation.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateClientsPaymentsAllocation',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Deliveries PROCEDURE 

CurrentTab           STRING(80)                            !
Locate               ULONG                                 !
BRW1::View:Browse    VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:BID)
                       PROJECT(DEL:CID)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:Rate)
                       PROJECT(DEL:DocumentCharge)
                       PROJECT(DEL:FuelSurcharge)
                       PROJECT(DEL:Charge)
                       PROJECT(DEL:FID)
                       PROJECT(DEL:DELCID)
                       PROJECT(DEL:UID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !List box control field - type derived from field
DEL:DIDate             LIKE(DEL:DIDate)               !List box control field - type derived from field
DEL:BID                LIKE(DEL:BID)                  !List box control field - type derived from field
DEL:CID                LIKE(DEL:CID)                  !List box control field - type derived from field
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
DEL:Rate               LIKE(DEL:Rate)                 !List box control field - type derived from field
DEL:DocumentCharge     LIKE(DEL:DocumentCharge)       !List box control field - type derived from field
DEL:FuelSurcharge      LIKE(DEL:FuelSurcharge)        !List box control field - type derived from field
DEL:Charge             LIKE(DEL:Charge)               !List box control field - type derived from field
DEL:FID                LIKE(DEL:FID)                  !Browse key field - type derived from field
DEL:DELCID             LIKE(DEL:DELCID)               !Browse key field - type derived from field
DEL:UID                LIKE(DEL:UID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Deliveries file'),AT(,,402,218),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Browse_Deliveries'),SYSTEM
                       LIST,AT(8,32,387,142),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~DI No.~C(0)@n_10@40R(2)|' & |
  'M~DID~C(0)@n_10@48R(2)|M~DI Date~C(0)@d6b@40R(2)|M~BID~C(0)@n_10@40R(2)|M~CID~C(0)@n' & |
  '_10@80L(2)|M~Client Reference~@s60@60R(1)|M~Rate~C(0)@n-13.4@64R(1)|M~Document Charg' & |
  'e~C(0)@n-11.2@60R(1)|M~Fuel Surcharge~C(0)@n-11.2@68R(1)|M~Charge~C(0)@n-15.2@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the Deliveries file')
                       BUTTON('&Select'),AT(134,178,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(188,178,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(240,178,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(294,178,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(346,178,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       PROMPT('Locate:'),AT(7,20),USE(?Locate:Prompt)
                       ENTRY(@n_10),AT(36,20,60,10),USE(Locate),RIGHT(1)
                       SHEET,AT(4,4,395,192),USE(?CurrentTab)
                         TAB('&1) DI No.'),USE(?Tab:2)
                         END
                         TAB('&2) DID'),USE(?Tab8)
                         END
                         TAB('&3) Client Ref.'),USE(?Tab:3)
                         END
                         TAB('&4) Branch ID'),USE(?Tab:4)
                           BUTTON('Select Branches'),AT(8,178,118,14),USE(?SelectBranches),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&5) Client ID'),USE(?Tab:5)
                           BUTTON('Select Clients'),AT(8,178,118,14),USE(?SelectClients),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&6) Floor'),USE(?Tab:13)
                           BUTTON('Select Floors'),AT(8,178,118,14),USE(?SelectFloors),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&7) Delivery Comp.'),USE(?Tab:15)
                           BUTTON('Select DeliveryComposition'),AT(8,178,118,14),USE(?SelectDeliveryComposition),LEFT, |
  ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&8) User'),USE(?Tab:17)
                           BUTTON('Select Users'),AT(8,178,118,14),USE(?SelectUsers),LEFT,ICON('WAPARENT.ICO'),FLAT,MSG('Select Parent Field'), |
  TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(298,200,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(350,200,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort16:Locator IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort1:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort2:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 4
BRW1::Sort3:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 5
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Deliveries')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:AddressAlias.Open                                 ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Branches.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ClientsRateTypes.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryComposition.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Delivery_CODAddresses.UseFile                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Floors.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ServiceRequirements.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,DEL:PKey_DID)                         ! Add the sort order for DEL:PKey_DID for sort order 1
  BRW1.AddLocator(BRW1::Sort16:Locator)                    ! Browse has a locator for sort order 1
  BRW1::Sort16:Locator.Init(?Locate,DEL:DID,1,BRW1)        ! Initialize the browse locator using ?Locate using key: DEL:PKey_DID , DEL:DID
  BRW1.AddSortOrder(,DEL:SKey_ClientReference)             ! Add the sort order for DEL:SKey_ClientReference for sort order 2
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort1:Locator.Init(?Locate,DEL:ClientReference,1,BRW1) ! Initialize the browse locator using ?Locate using key: DEL:SKey_ClientReference , DEL:ClientReference
  BRW1.AddSortOrder(,DEL:FKey_BID)                         ! Add the sort order for DEL:FKey_BID for sort order 3
  BRW1.AddRange(DEL:BID,Relate:Deliveries,Relate:Branches) ! Add file relationship range limit for sort order 3
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort2:Locator.Init(,DEL:BID,1,BRW1)                ! Initialize the browse locator using  using key: DEL:FKey_BID , DEL:BID
  BRW1.AddSortOrder(,DEL:FKey_CID)                         ! Add the sort order for DEL:FKey_CID for sort order 4
  BRW1.AddRange(DEL:CID,Relate:Deliveries,Relate:Clients)  ! Add file relationship range limit for sort order 4
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 4
  BRW1::Sort3:Locator.Init(?Locate,DEL:CID,1,BRW1)         ! Initialize the browse locator using ?Locate using key: DEL:FKey_CID , DEL:CID
  BRW1.AddSortOrder(,DEL:FKey_FID)                         ! Add the sort order for DEL:FKey_FID for sort order 5
  BRW1.AddRange(DEL:FID,Relate:Deliveries,Relate:Floors)   ! Add file relationship range limit for sort order 5
  BRW1.AddSortOrder(,DEL:FKey_DELCID)                      ! Add the sort order for DEL:FKey_DELCID for sort order 6
  BRW1.AddRange(DEL:DELCID,Relate:Deliveries,Relate:DeliveryComposition) ! Add file relationship range limit for sort order 6
  BRW1.AddSortOrder(,DEL:FKey_UID)                         ! Add the sort order for DEL:FKey_UID for sort order 7
  BRW1.AddRange(DEL:UID,Relate:Deliveries,Relate:Users)    ! Add file relationship range limit for sort order 7
  BRW1.AddSortOrder(,DEL:Key_DINo)                         ! Add the sort order for DEL:Key_DINo for sort order 8
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 8
  BRW1::Sort0:Locator.Init(?Locate,DEL:DINo,1,BRW1)        ! Initialize the browse locator using ?Locate using key: DEL:Key_DINo , DEL:DINo
  BRW1.AddField(DEL:DINo,BRW1.Q.DEL:DINo)                  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DID,BRW1.Q.DEL:DID)                    ! Field DEL:DID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DIDate,BRW1.Q.DEL:DIDate)              ! Field DEL:DIDate is a hot field or requires assignment from browse
  BRW1.AddField(DEL:BID,BRW1.Q.DEL:BID)                    ! Field DEL:BID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:CID,BRW1.Q.DEL:CID)                    ! Field DEL:CID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:ClientReference,BRW1.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW1.AddField(DEL:Rate,BRW1.Q.DEL:Rate)                  ! Field DEL:Rate is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DocumentCharge,BRW1.Q.DEL:DocumentCharge) ! Field DEL:DocumentCharge is a hot field or requires assignment from browse
  BRW1.AddField(DEL:FuelSurcharge,BRW1.Q.DEL:FuelSurcharge) ! Field DEL:FuelSurcharge is a hot field or requires assignment from browse
  BRW1.AddField(DEL:Charge,BRW1.Q.DEL:Charge)              ! Field DEL:Charge is a hot field or requires assignment from browse
  BRW1.AddField(DEL:FID,BRW1.Q.DEL:FID)                    ! Field DEL:FID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DELCID,BRW1.Q.DEL:DELCID)              ! Field DEL:DELCID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:UID,BRW1.Q.DEL:UID)                    ! Field DEL:UID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Deliveries',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Deliveries',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Delivery
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectBranches()
      ThisWindow.Reset
    OF ?SelectClients
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Clients()
      ThisWindow.Reset
    OF ?SelectFloors
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectFloors()
      ThisWindow.Reset
    OF ?SelectDeliveryComposition
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectDeliveryComposition()
      ThisWindow.Reset
    OF ?SelectUsers
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectUsers()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?CurrentTab) = 6
    RETURN SELF.SetSort(5,Force)
  ELSIF CHOICE(?CurrentTab) = 7
    RETURN SELF.SetSort(6,Force)
  ELSIF CHOICE(?CurrentTab) = 8
    RETURN SELF.SetSort(7,Force)
  ELSE
    RETURN SELF.SetSort(8,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

