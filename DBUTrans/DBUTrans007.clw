

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS007.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Select a TruckTrailer Record
!!! </summary>
SelectTruckTrailer PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(TruckTrailer)
                       PROJECT(TRU:TID)
                       PROJECT(TRU:Type)
                       PROJECT(TRU:Registration)
                       PROJECT(TRU:Capacity)
                       PROJECT(TRU:LicenseExpiryDate)
                       PROJECT(TRU:LicenseInfo)
                       PROJECT(TRU:TTID)
                       PROJECT(TRU:VMMID)
                       PROJECT(TRU:DRID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
TRU:TID                LIKE(TRU:TID)                  !List box control field - type derived from field
TRU:Type               LIKE(TRU:Type)                 !List box control field - type derived from field
TRU:Registration       LIKE(TRU:Registration)         !List box control field - type derived from field
TRU:Capacity           LIKE(TRU:Capacity)             !List box control field - type derived from field
TRU:LicenseExpiryDate  LIKE(TRU:LicenseExpiryDate)    !List box control field - type derived from field
TRU:LicenseInfo        LIKE(TRU:LicenseInfo)          !List box control field - type derived from field
TRU:TTID               LIKE(TRU:TTID)                 !Primary key field - type derived from field
TRU:VMMID              LIKE(TRU:VMMID)                !Browse key field - type derived from field
TRU:DRID               LIKE(TRU:DRID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a TruckTrailer Record'),AT(,,358,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('SelectTruckTrailer'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80R(2)|M~TID~C(0)@n_10@20R(2)|M~T' & |
  'ype~C(0)@n3@80L(2)|M~Registration~L(2)@s20@40R(2)|M~Capacity~C(0)@n-8.0@80R(2)|M~Lic' & |
  'ense Expiry Date~C(0)@d6b@80L(2)|M~License Info~L(2)@s250@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he TruckTrailer file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Tansporter'),USE(?Tab:2)
                         END
                         TAB('&2) By Vehicle Make && Model'),USE(?Tab:3)
                         END
                         TAB('&3) By Registration'),USE(?Tab:4)
                         END
                         TAB('&4) By Driver'),USE(?Tab:5)
                         END
                       END
                       BUTTON('&Close'),AT(252,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 4
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectTruckTrailer')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:TruckTrailer.Open                                 ! File TruckTrailer used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TruckTrailer,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,TRU:FKey_VMMID)                       ! Add the sort order for TRU:FKey_VMMID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,TRU:VMMID,1,BRW1)              ! Initialize the browse locator using  using key: TRU:FKey_VMMID , TRU:VMMID
  BRW1.AddSortOrder(,TRU:Key_Registration)                 ! Add the sort order for TRU:Key_Registration for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,TRU:Registration,1,BRW1)       ! Initialize the browse locator using  using key: TRU:Key_Registration , TRU:Registration
  BRW1.AddSortOrder(,TRU:FKey_DRID)                        ! Add the sort order for TRU:FKey_DRID for sort order 3
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort3:Locator.Init(,TRU:DRID,1,BRW1)               ! Initialize the browse locator using  using key: TRU:FKey_DRID , TRU:DRID
  BRW1.AddSortOrder(,TRU:FKey_TID)                         ! Add the sort order for TRU:FKey_TID for sort order 4
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 4
  BRW1::Sort0:Locator.Init(,TRU:TID,1,BRW1)                ! Initialize the browse locator using  using key: TRU:FKey_TID , TRU:TID
  BRW1.AddField(TRU:TID,BRW1.Q.TRU:TID)                    ! Field TRU:TID is a hot field or requires assignment from browse
  BRW1.AddField(TRU:Type,BRW1.Q.TRU:Type)                  ! Field TRU:Type is a hot field or requires assignment from browse
  BRW1.AddField(TRU:Registration,BRW1.Q.TRU:Registration)  ! Field TRU:Registration is a hot field or requires assignment from browse
  BRW1.AddField(TRU:Capacity,BRW1.Q.TRU:Capacity)          ! Field TRU:Capacity is a hot field or requires assignment from browse
  BRW1.AddField(TRU:LicenseExpiryDate,BRW1.Q.TRU:LicenseExpiryDate) ! Field TRU:LicenseExpiryDate is a hot field or requires assignment from browse
  BRW1.AddField(TRU:LicenseInfo,BRW1.Q.TRU:LicenseInfo)    ! Field TRU:LicenseInfo is a hot field or requires assignment from browse
  BRW1.AddField(TRU:TTID,BRW1.Q.TRU:TTID)                  ! Field TRU:TTID is a hot field or requires assignment from browse
  BRW1.AddField(TRU:VMMID,BRW1.Q.TRU:VMMID)                ! Field TRU:VMMID is a hot field or requires assignment from browse
  BRW1.AddField(TRU:DRID,BRW1.Q.TRU:DRID)                  ! Field TRU:DRID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('SelectTruckTrailer',QuickWindow)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TruckTrailer.Close
  END
  IF SELF.Opened
    INIMgr.Update('SelectTruckTrailer',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

