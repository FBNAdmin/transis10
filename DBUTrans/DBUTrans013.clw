

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS013.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('DBUTRANS009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS014.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Delivery PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(Shortages_Damages)
                       PROJECT(SHO:SDID)
                       PROJECT(SHO:DINo)
                       PROJECT(SHO:Items)
                       PROJECT(SHO:TRID)
                       PROJECT(SHO:TripsheetItems)
                       PROJECT(SHO:IID)
                       PROJECT(SHO:POD_IID)
                       PROJECT(SHO:DeliveryDepot)
                       PROJECT(SHO:CollectionBID)
                       PROJECT(SHO:DID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
SHO:SDID               LIKE(SHO:SDID)                 !List box control field - type derived from field
SHO:DINo               LIKE(SHO:DINo)                 !List box control field - type derived from field
SHO:Items              LIKE(SHO:Items)                !List box control field - type derived from field
SHO:TRID               LIKE(SHO:TRID)                 !List box control field - type derived from field
SHO:TripsheetItems     LIKE(SHO:TripsheetItems)       !List box control field - type derived from field
SHO:IID                LIKE(SHO:IID)                  !List box control field - type derived from field
SHO:POD_IID            LIKE(SHO:POD_IID)              !List box control field - type derived from field
SHO:DeliveryDepot      LIKE(SHO:DeliveryDepot)        !List box control field - type derived from field
SHO:CollectionBID      LIKE(SHO:CollectionBID)        !List box control field - type derived from field
SHO:DID                LIKE(SHO:DID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(DeliveriesAdditionalCharges)
                       PROJECT(DELA:DAID)
                       PROJECT(DELA:AdditionalCharge)
                       PROJECT(DELA:Charge)
                       PROJECT(DELA:DID)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
DELA:DAID              LIKE(DELA:DAID)                !List box control field - type derived from field
DELA:AdditionalCharge  LIKE(DELA:AdditionalCharge)    !List box control field - type derived from field
DELA:Charge            LIKE(DELA:Charge)              !List box control field - type derived from field
DELA:DID               LIKE(DELA:DID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(DeliveryLegsAlias)
                       PROJECT(A_DELL:DLID)
                       PROJECT(A_DELL:Leg)
                       PROJECT(A_DELL:TID)
                       PROJECT(A_DELL:Cost)
                       PROJECT(A_DELL:VATRate)
                       PROJECT(A_DELL:DID)
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?Browse:6
A_DELL:DLID            LIKE(A_DELL:DLID)              !List box control field - type derived from field
A_DELL:Leg             LIKE(A_DELL:Leg)               !List box control field - type derived from field
A_DELL:TID             LIKE(A_DELL:TID)               !List box control field - type derived from field
A_DELL:Cost            LIKE(A_DELL:Cost)              !List box control field - type derived from field
A_DELL:VATRate         LIKE(A_DELL:VATRate)           !List box control field - type derived from field
A_DELL:DID             LIKE(A_DELL:DID)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(_Invoice)
                       PROJECT(INV:IID)
                       PROJECT(INV:DINo)
                       PROJECT(INV:POD_IID)
                       PROJECT(INV:CR_IID)
                       PROJECT(INV:BID)
                       PROJECT(INV:BranchName)
                       PROJECT(INV:CID)
                       PROJECT(INV:ClientNo)
                       PROJECT(INV:ClientName)
                       PROJECT(INV:DID)
                     END
Queue:Browse:8       QUEUE                            !Queue declaration for browse/combo box using ?Browse:8
INV:IID                LIKE(INV:IID)                  !List box control field - type derived from field
INV:DINo               LIKE(INV:DINo)                 !List box control field - type derived from field
INV:POD_IID            LIKE(INV:POD_IID)              !List box control field - type derived from field
INV:CR_IID             LIKE(INV:CR_IID)               !List box control field - type derived from field
INV:BID                LIKE(INV:BID)                  !List box control field - type derived from field
INV:BranchName         LIKE(INV:BranchName)           !List box control field - type derived from field
INV:CID                LIKE(INV:CID)                  !List box control field - type derived from field
INV:ClientNo           LIKE(INV:ClientNo)             !List box control field - type derived from field
INV:ClientName         LIKE(INV:ClientName)           !List box control field - type derived from field
INV:DID                LIKE(INV:DID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(DeliveryItems)
                       PROJECT(DELI:DIID)
                       PROJECT(DELI:ItemNo)
                       PROJECT(DELI:Units)
                       PROJECT(DELI:Weight)
                       PROJECT(DELI:Type)
                       PROJECT(DELI:ShowOnInvoice)
                       PROJECT(DELI:ContainerNo)
                       PROJECT(DELI:ContainerVessel)
                       PROJECT(DELI:SealNo)
                       PROJECT(DELI:ByContainer)
                       PROJECT(DELI:DID)
                       PROJECT(DELI:PTID)
                       PROJECT(DELI:CMID)
                       JOIN(PACK:PKey_PTID,DELI:PTID)
                         PROJECT(PACK:Packaging)
                         PROJECT(PACK:PTID)
                       END
                       JOIN(COM:PKey_CMID,DELI:CMID)
                         PROJECT(COM:Commodity)
                         PROJECT(COM:CMID)
                       END
                     END
Queue:Browse:10      QUEUE                            !Queue declaration for browse/combo box using ?Browse:10
DELI:DIID              LIKE(DELI:DIID)                !List box control field - type derived from field
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Units             LIKE(DELI:Units)               !List box control field - type derived from field
DELI:Weight            LIKE(DELI:Weight)              !List box control field - type derived from field
DELI:Type              LIKE(DELI:Type)                !List box control field - type derived from field
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
PACK:Packaging         LIKE(PACK:Packaging)           !List box control field - type derived from field
DELI:ShowOnInvoice     LIKE(DELI:ShowOnInvoice)       !List box control field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !List box control field - type derived from field
DELI:ContainerVessel   LIKE(DELI:ContainerVessel)     !List box control field - type derived from field
DELI:SealNo            LIKE(DELI:SealNo)              !List box control field - type derived from field
DELI:ByContainer       LIKE(DELI:ByContainer)         !List box control field - type derived from field
DELI:DID               LIKE(DELI:DID)                 !Browse key field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Related join file key field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(DeliveryLegs)
                       PROJECT(DELL:DLID)
                       PROJECT(DELL:Leg)
                       PROJECT(DELL:TID)
                       PROJECT(DELL:Cost)
                       PROJECT(DELL:VATRate)
                       PROJECT(DELL:DID)
                     END
Queue:Browse:12      QUEUE                            !Queue declaration for browse/combo box using ?Browse:12
DELL:DLID              LIKE(DELL:DLID)                !List box control field - type derived from field
DELL:Leg               LIKE(DELL:Leg)                 !List box control field - type derived from field
DELL:TID               LIKE(DELL:TID)                 !List box control field - type derived from field
DELL:Cost              LIKE(DELL:Cost)                !List box control field - type derived from field
DELL:VATRate           LIKE(DELL:VATRate)             !List box control field - type derived from field
DELL:DID               LIKE(DELL:DID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(DeliveryProgress)
                       PROJECT(DELP:StatusDate)
                       PROJECT(DELP:StatusTime)
                       PROJECT(DELP:ActionDate)
                       PROJECT(DELP:ActionTime)
                       PROJECT(DELP:DPID)
                       PROJECT(DELP:DID)
                     END
Queue:Browse:14      QUEUE                            !Queue declaration for browse/combo box using ?Browse:14
DELP:StatusDate        LIKE(DELP:StatusDate)          !List box control field - type derived from field
DELP:StatusTime        LIKE(DELP:StatusTime)          !List box control field - type derived from field
DELP:ActionDate        LIKE(DELP:ActionDate)          !List box control field - type derived from field
DELP:ActionTime        LIKE(DELP:ActionTime)          !List box control field - type derived from field
DELP:DPID              LIKE(DELP:DPID)                !Primary key field - type derived from field
DELP:DID               LIKE(DELP:DID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::DEL:Record  LIKE(DEL:RECORD),THREAD
QuickWindow          WINDOW('Form Deliveries'),AT(,,415,190),FONT('MS Sans Serif',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_Delivery'),SYSTEM
                       SHEET,AT(4,4,408,164),USE(?CurrentTab),JOIN
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('DI No.:'),AT(8,30),USE(?DEL:DINo:Prompt),TRN
                           ENTRY(@n_10),AT(132,30,60,10),USE(DEL:DINo),RIGHT(1),MSG('Delivery Instruction Number'),REQ, |
  TIP('Delivery Instruction Number')
                           PROMPT('DID:'),AT(217,30),USE(?DEL:DID:Prompt:2)
                           ENTRY(@n_10),AT(241,30,60,10),USE(DEL:DID),RIGHT(1),MSG('Delivery ID')
                           PROMPT('DI Date:'),AT(8,44),USE(?DEL:DIDate:Prompt),TRN
                           SPIN(@d6b),AT(132,44,60,10),USE(DEL:DIDate),MSG('DI Date'),TIP('DI Date')
                           BUTTON('Get Next ID'),AT(241,47,49,14),USE(?Button_GetDID),TIP('Based on BID')
                           PROMPT('BID:'),AT(8,60),USE(?DEL:BID:Prompt:2),TRN
                           ENTRY(@n_10),AT(132,58,60,10),USE(DEL:BID),RIGHT(1),MSG('Branch ID')
                           ENTRY(@n_10),AT(132,72,60,10),USE(DEL:CID),RIGHT(1),MSG('Client ID')
                           PROMPT('CID:'),AT(8,74),USE(?DEL:CID:Prompt:2),TRN
                           PROMPT('Client Reference:'),AT(8,86),USE(?DEL:ClientReference:Prompt),TRN
                           ENTRY(@s60),AT(132,86,218,10),USE(DEL:ClientReference),MSG('Client Reference'),TIP('Client Reference')
                           PROMPT('Rate:'),AT(8,100),USE(?DEL:Rate:Prompt),TRN
                           ENTRY(@n-13.4),AT(132,100,60,10),USE(DEL:Rate),RIGHT(1),MSG('Rate used on this DI'),TIP('Rate used ' & |
  'on this DI<0DH,0AH>Charge will be this rate multiplied by the total weight in kgs.')
                           PROMPT('Document Charge:'),AT(8,114),USE(?DEL:DocumentCharge:Prompt),TRN
                           ENTRY(@n-11.2),AT(132,114,60,10),USE(DEL:DocumentCharge),RIGHT(1),MSG('Document Charge'),TIP('Document C' & |
  'harge applied to this DI')
                           PROMPT('Fuel Surcharge:'),AT(8,128),USE(?DEL:FuelSurcharge:Prompt),TRN
                           ENTRY(@n-11.2),AT(132,128,60,10),USE(DEL:FuelSurcharge),RIGHT(1),MSG('Fuel Surcharge'),TIP('Fuel Surch' & |
  'arge applied to this DI')
                           PROMPT('Charge:'),AT(8,142),USE(?DEL:Charge:Prompt),TRN
                           ENTRY(@n-15.2),AT(132,142,60,10),USE(DEL:Charge),RIGHT(1),MSG('Charge for the DI'),TIP('Charge for' & |
  ' the DI<0DH,0AH>Excludes VAT, Docs, Fuel and Insurance charges')
                           CHECK('Additional Charge Calculate'),AT(132,156,120,8),USE(DEL:AdditionalCharge_Calculate), |
  MSG('Calculate the Additional Charge'),TIP('Calculate the Additional Charge')
                         END
                         TAB('&2) Gen'),USE(?Tab:2)
                           PROMPT('Additional Charge:'),AT(8,30),USE(?DEL:AdditionalCharge:Prompt),TRN
                           ENTRY(@n-15.2),AT(132,30,40,10),USE(DEL:AdditionalCharge),DECIMAL(12),MSG('Addi'),TIP('Addi')
                           PROMPT('VAT Rate:'),AT(8,44),USE(?DEL:VATRate:Prompt),TRN
                           ENTRY(@n7.2),AT(132,44,40,10),USE(DEL:VATRate),DECIMAL(12),MSG('VAT Rate'),TIP('VAT Rate')
                           CHECK('Insure'),AT(132,58,40,8),USE(DEL:Insure),MSG('Insure'),TIP('Insure the goods on ' & |
  'this DI.<0DH,0AH>Note this is in addition to any insurance that may be part of this ' & |
  'clients rate.')
                           PROMPT('Total Consignment Value:'),AT(8,70),USE(?DEL:TotalConsignmentValue:Prompt),TRN
                           ENTRY(@n-15.2),AT(132,70,40,10),USE(DEL:TotalConsignmentValue),RIGHT(1),MSG('Total valu' & |
  'e of the cargo on this DI - required if additional insurance required'),TIP('Total valu' & |
  'e of the cargo on this DI - required if additional insurance required')
                           PROMPT('Insurance Rate:'),AT(8,84),USE(?DEL:InsuranceRate:Prompt),TRN
                           ENTRY(@n-11.6),AT(132,84,40,10),USE(DEL:InsuranceRate),RIGHT(1),MSG('Insurance rate per ton'), |
  TIP('Insurance rate per ton')
                           PROMPT('Special Delivery Instructions:'),AT(8,98),USE(?DEL:SpecialDeliveryInstructions:Prompt), |
  TRN
                           TEXT,AT(132,98,218,30),USE(DEL:SpecialDeliveryInstructions),MSG('This will print on the' & |
  ' DI and Delivery Note (& POD)'),TIP('This will print on the DI and Delivery Note (& POD)')
                           PROMPT('Notes:'),AT(8,132),USE(?DEL:Notes:Prompt),TRN
                           TEXT,AT(132,132,218,30),USE(DEL:Notes),MSG('General Notes'),TIP('General Notes')
                         END
                         TAB('&3) Gen'),USE(?Tab:3)
                           PROMPT('Received Date:'),AT(8,30),USE(?DEL:ReceivedDate:Prompt),TRN
                           SPIN(@d6b),AT(132,30,119,10),USE(DEL:ReceivedDate),MSG('Received Date'),TIP('Received Date')
                           CHECK('Multiple Manifests Allowed'),AT(132,44,116,8),USE(DEL:MultipleManifestsAllowed),MSG('Allow this' & |
  ' DI to be manifested on multiple manifests'),TIP('Allow this DI to be manifested on ' & |
  'multiple manifests')
                           PROMPT('Manifested:'),AT(8,56),USE(?DEL:Manifested:Prompt),TRN
                           LIST,AT(132,56,100,10),USE(DEL:Manifested),DROP(5),FROM('Not Manifested|#0|Partially Ma' & |
  'nifested|#1|Partially Manifested Multiple|#2|Fully Manifested|#3|Fully Manifested Multiple|#4'), |
  MSG('Manifested status'),TIP('Manifested status')
                           PROMPT('Delivered:'),AT(8,70),USE(?DEL:Delivered:Prompt),TRN
                           LIST,AT(132,70,100,10),USE(DEL:Delivered),DROP(5),FROM('Not Delivered|#0|Partially Deli' & |
  'vered|#1|Delivered|#2|Delivered (manual)|#3'),MSG('Delivered Status'),TIP('Delivered Status')
                           PROMPT('Terms:'),AT(8,84),USE(?DEL:Terms:Prompt),TRN
                           LIST,AT(132,84,100,10),USE(DEL:Terms),DROP(5),FROM('Pre Paid|#0|COD|#1|Account|#2|On St' & |
  'atement|#3'),MSG('Terms - Pre Paid, COD, Account, On Statement'),TIP('Terms - Pre Pa' & |
  'id, COD, Account, On Statement')
                           PROMPT('Created Date:'),AT(8,98),USE(?DEL:CreatedDate:Prompt),TRN
                           ENTRY(@d6b),AT(132,98,104,10),USE(DEL:CreatedDate),MSG('Entry created on'),TIP('Entry created on')
                           PROMPT('Created Time:'),AT(8,112),USE(?DEL:CreatedTime:Prompt),TRN
                           ENTRY(@t7b),AT(132,112,104,10),USE(DEL:CreatedTime),MSG('Entry created at'),TIP('Entry created at')
                           PROMPT('Released UID:'),AT(8,126),USE(?DEL:ReleasedUID:Prompt),TRN
                           ENTRY(@n_10),AT(132,126,104,10),USE(DEL:ReleasedUID),MSG('If requiring release, this is' & |
  ' the User that has released it'),TIP('If requiring release, this is the User that ha' & |
  's released it')
                         END
                         TAB('&7) Invoices'),USE(?Tab:Invoices)
                           LIST,AT(8,30,399,116),USE(?Browse:8),HVSCROLL,FORMAT('44R(2)|M~Invoice No.~C(0)@n_10@40' & |
  'R(2)|M~DI No.~C(0)@n_10@50R(2)|M~POD IID~C(0)@n_10@50R(2)|M~CR IID~C(0)@n_10@50R(2)|' & |
  'M~BID~C(0)@n_10@50L(2)|M~Branch Name~@s35@50R(2)|M~CID~C(0)@n_10@50R(2)|M~Client No.' & |
  '~C(0)@n_10b@80L(2)|M~Client Name~@s100@'),FROM(Queue:Browse:8),IMM,MSG('Browsing the' & |
  ' DeliveryProgress file')
                           BUTTON('&Insert'),AT(252,150,49,14),USE(?Insert:9),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(306,150,49,14),USE(?Change:9),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(358,150,49,14),USE(?Delete:9),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&8) Delivery Items'),USE(?Tab:DeliveryItems)
                           LIST,AT(8,30,399,116),USE(?Browse:10),HVSCROLL,FORMAT('40R(2)|M~DIID~C(0)@n_10@32R(2)|M' & |
  '~Item No.~C(0)@n_5@28R(2)|M~Units~C(0)@n6@44R(2)|M~Weight~C(0)@n-11.2@20R(2)|M~Type~' & |
  'C(0)@n3@70L(2)|M~Commodity~C(0)@s35@70L(2)|M~Packaging~C(0)@s35@64R(2)|M~Show On Inv' & |
  'oice~C(0)@n3@80L(2)|M~Container No.~@s35@80L(2)|M~Container Vessel~@s35@80L(2)|M~Sea' & |
  'l No~@s35@52R(2)|M~By Container~C(0)@n3@'),FROM(Queue:Browse:10),IMM,MSG('Browsing t' & |
  'he DeliveryProgress file')
                           BUTTON('&Insert'),AT(252,150,49,14),USE(?Insert:11),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(306,150,49,14),USE(?Change:11),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(358,150,49,14),USE(?Delete:11),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&9) Delivery Legs'),USE(?Tab:DeliveryLegs2)
                           LIST,AT(8,30,399,116),USE(?Browse:12),HVSCROLL,FORMAT('40R(2)|M~DLID~C(0)@n_10@24R(2)|M' & |
  '~Leg~C(0)@n5@50R(2)|M~TID~C(0)@n_10@60R(1)|M~Cost~C(0)@n-13.2@36R(1)|M~VAT Rate~C(0)@n-7.2@'), |
  FROM(Queue:Browse:12),IMM,MSG('Browsing the DeliveryProgress file')
                           BUTTON('&Insert'),AT(252,150,49,14),USE(?Insert:13),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(306,150,49,14),USE(?Change:13),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(358,150,49,14),USE(?Delete:13),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&6) Delivery Legs Alias'),USE(?Tab:DeliveryLegs)
                           LIST,AT(8,30,399,116),USE(?Browse:6),HVSCROLL,FORMAT('40R(2)|M~DLID~C(0)@n_10@24R(2)|M~' & |
  'Leg~C(0)@n5@40R(2)|M~TID~C(0)@n_10@60R(1)|M~Cost~C(0)@n-13.2@36R(1)|M~VAT Rate~C(0)@n-7.2@'), |
  FROM(Queue:Browse:6),IMM,MSG('Browsing the DeliveryProgress file')
                           BUTTON('&Insert'),AT(252,150,49,14),USE(?Insert:7),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(306,150,49,14),USE(?Change:7),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(358,150,49,14),USE(?Delete:7),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&5) Add. Charges'),USE(?Tab:AdditionalCharge)
                           LIST,AT(8,30,399,116),USE(?Browse:4),HVSCROLL,FORMAT('40R(2)|M~DAID~L@n_10@80L(2)|M~Add' & |
  'itional Charge~@s35@68R(1)|M~Charge~C(0)@n-15.2@'),FROM(Queue:Browse:4),IMM,MSG('Browsing t' & |
  'he DeliveryProgress file')
                           BUTTON('&Insert'),AT(252,150,49,14),USE(?Insert:5),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(306,150,49,14),USE(?Change:5),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(358,150,49,14),USE(?Delete:5),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&4) Short. / Dam.'),USE(?Tab:ShortDama)
                           LIST,AT(8,30,342,116),USE(?Browse:2),HVSCROLL,FORMAT('80R(10)|M~Shortages && Damages ID' & |
  '~C(0)@n_10@80R(2)|M~DI No.~C(0)@n_10@80L(2)|M~Items~L(2)@s35@80R(2)|M~TRID~C(0)@N_10' & |
  '@80L(2)|M~Tripsheet Items~L(2)@s35@80R(2)|M~Invoice Number~C(0)@n_10@80R(2)|M~POD II' & |
  'D~C(0)@n_10@80L(2)|M~Delivery Depot~L(2)@s35@80R(2)|M~Collection BID~C(0)@n_10@'),FROM(Queue:Browse:2), |
  IMM,MSG('Browsing the DeliveryProgress file')
                           BUTTON('&Insert'),AT(195,150,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(248,150,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(301,150,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&10) DeliveryProgress'),USE(?Tab:DeliveryProgress)
                           LIST,AT(8,30,342,116),USE(?Browse:14),HVSCROLL,FORMAT('80R(2)|M~Status Date~C(0)@d6@80R' & |
  '(2)|M~Status Time~C(0)@t7@80R(2)|M~Action Date~C(0)@d6b@80R(2)|M~Action Time~C(0)@t7@'), |
  FROM(Queue:Browse:14),IMM,MSG('Browsing the DeliveryProgress file')
                           BUTTON('&Insert'),AT(195,150,49,14),USE(?Insert:15),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(248,150,49,14),USE(?Change:15),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(301,150,49,14),USE(?Delete:15),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(256,172,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(310,172,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(362,172,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW6                 CLASS(BrowseClass)                    ! Browse using ?Browse:6
Q                      &Queue:Browse:6                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW6::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW8                 CLASS(BrowseClass)                    ! Browse using ?Browse:8
Q                      &Queue:Browse:8                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW10                CLASS(BrowseClass)                    ! Browse using ?Browse:10
Q                      &Queue:Browse:10               !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW10::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW12                CLASS(BrowseClass)                    ! Browse using ?Browse:12
Q                      &Queue:Browse:12               !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW12::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW14                CLASS(BrowseClass)                    ! Browse using ?Browse:14
Q                      &Queue:Browse:14               !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Delivery Record'
  OF InsertRecord
    ActionMessage = 'Delivery Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Delivery Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Delivery')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DEL:DINo:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Deliveries)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DEL:Record,History::DEL:Record)
  SELF.AddHistoryField(?DEL:DINo,2)
  SELF.AddHistoryField(?DEL:DID,1)
  SELF.AddHistoryField(?DEL:DIDate,5)
  SELF.AddHistoryField(?DEL:BID,7)
  SELF.AddHistoryField(?DEL:CID,8)
  SELF.AddHistoryField(?DEL:ClientReference,9)
  SELF.AddHistoryField(?DEL:Rate,20)
  SELF.AddHistoryField(?DEL:DocumentCharge,21)
  SELF.AddHistoryField(?DEL:FuelSurcharge,22)
  SELF.AddHistoryField(?DEL:Charge,23)
  SELF.AddHistoryField(?DEL:AdditionalCharge_Calculate,24)
  SELF.AddHistoryField(?DEL:AdditionalCharge,25)
  SELF.AddHistoryField(?DEL:VATRate,26)
  SELF.AddHistoryField(?DEL:Insure,29)
  SELF.AddHistoryField(?DEL:TotalConsignmentValue,30)
  SELF.AddHistoryField(?DEL:InsuranceRate,31)
  SELF.AddHistoryField(?DEL:SpecialDeliveryInstructions,34)
  SELF.AddHistoryField(?DEL:Notes,35)
  SELF.AddHistoryField(?DEL:ReceivedDate,38)
  SELF.AddHistoryField(?DEL:MultipleManifestsAllowed,40)
  SELF.AddHistoryField(?DEL:Manifested,41)
  SELF.AddHistoryField(?DEL:Delivered,42)
  SELF.AddHistoryField(?DEL:Terms,44)
  SELF.AddHistoryField(?DEL:CreatedDate,48)
  SELF.AddHistoryField(?DEL:CreatedTime,49)
  SELF.AddHistoryField(?DEL:ReleasedUID,52)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Relate:Shortages_Damages.Open                            ! File Shortages_Damages used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Deliveries
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:Shortages_Damages,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:DeliveriesAdditionalCharges,SELF) ! Initialize the browse manager
  BRW6.Init(?Browse:6,Queue:Browse:6.ViewPosition,BRW6::View:Browse,Queue:Browse:6,Relate:DeliveryLegsAlias,SELF) ! Initialize the browse manager
  BRW8.Init(?Browse:8,Queue:Browse:8.ViewPosition,BRW8::View:Browse,Queue:Browse:8,Relate:_Invoice,SELF) ! Initialize the browse manager
  BRW10.Init(?Browse:10,Queue:Browse:10.ViewPosition,BRW10::View:Browse,Queue:Browse:10,Relate:DeliveryItems,SELF) ! Initialize the browse manager
  BRW12.Init(?Browse:12,Queue:Browse:12.ViewPosition,BRW12::View:Browse,Queue:Browse:12,Relate:DeliveryLegs,SELF) ! Initialize the browse manager
  BRW14.Init(?Browse:14,Queue:Browse:14.ViewPosition,BRW14::View:Browse,Queue:Browse:14,Relate:DeliveryProgress,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?DEL:DINo{PROP:ReadOnly} = True
    ?DEL:DID{PROP:ReadOnly} = True
    DISABLE(?Button_GetDID)
    ?DEL:ClientReference{PROP:ReadOnly} = True
    ?DEL:Rate{PROP:ReadOnly} = True
    ?DEL:DocumentCharge{PROP:ReadOnly} = True
    ?DEL:FuelSurcharge{PROP:ReadOnly} = True
    ?DEL:Charge{PROP:ReadOnly} = True
    ?DEL:AdditionalCharge{PROP:ReadOnly} = True
    ?DEL:VATRate{PROP:ReadOnly} = True
    ?DEL:TotalConsignmentValue{PROP:ReadOnly} = True
    ?DEL:InsuranceRate{PROP:ReadOnly} = True
    DISABLE(?DEL:Manifested)
    DISABLE(?DEL:Delivered)
    DISABLE(?DEL:Terms)
    ?DEL:CreatedDate{PROP:ReadOnly} = True
    ?DEL:CreatedTime{PROP:ReadOnly} = True
    ?DEL:ReleasedUID{PROP:ReadOnly} = True
    DISABLE(?Insert:9)
    DISABLE(?Change:9)
    DISABLE(?Delete:9)
    DISABLE(?Insert:11)
    DISABLE(?Change:11)
    DISABLE(?Delete:11)
    DISABLE(?Insert:13)
    DISABLE(?Change:13)
    DISABLE(?Delete:13)
    DISABLE(?Insert:7)
    DISABLE(?Change:7)
    DISABLE(?Delete:7)
    DISABLE(?Insert:5)
    DISABLE(?Change:5)
    DISABLE(?Delete:5)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    DISABLE(?Insert:15)
    DISABLE(?Change:15)
    DISABLE(?Delete:15)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,SHO:FKey_DID)                         ! Add the sort order for SHO:FKey_DID for sort order 1
  BRW2.AddRange(SHO:DID,Relate:Shortages_Damages,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW2.AddField(SHO:SDID,BRW2.Q.SHO:SDID)                  ! Field SHO:SDID is a hot field or requires assignment from browse
  BRW2.AddField(SHO:DINo,BRW2.Q.SHO:DINo)                  ! Field SHO:DINo is a hot field or requires assignment from browse
  BRW2.AddField(SHO:Items,BRW2.Q.SHO:Items)                ! Field SHO:Items is a hot field or requires assignment from browse
  BRW2.AddField(SHO:TRID,BRW2.Q.SHO:TRID)                  ! Field SHO:TRID is a hot field or requires assignment from browse
  BRW2.AddField(SHO:TripsheetItems,BRW2.Q.SHO:TripsheetItems) ! Field SHO:TripsheetItems is a hot field or requires assignment from browse
  BRW2.AddField(SHO:IID,BRW2.Q.SHO:IID)                    ! Field SHO:IID is a hot field or requires assignment from browse
  BRW2.AddField(SHO:POD_IID,BRW2.Q.SHO:POD_IID)            ! Field SHO:POD_IID is a hot field or requires assignment from browse
  BRW2.AddField(SHO:DeliveryDepot,BRW2.Q.SHO:DeliveryDepot) ! Field SHO:DeliveryDepot is a hot field or requires assignment from browse
  BRW2.AddField(SHO:CollectionBID,BRW2.Q.SHO:CollectionBID) ! Field SHO:CollectionBID is a hot field or requires assignment from browse
  BRW2.AddField(SHO:DID,BRW2.Q.SHO:DID)                    ! Field SHO:DID is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4.AddSortOrder(,DELA:FKey_DID)                        ! Add the sort order for DELA:FKey_DID for sort order 1
  BRW4.AddRange(DELA:DID,Relate:DeliveriesAdditionalCharges,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW4.AddField(DELA:DAID,BRW4.Q.DELA:DAID)                ! Field DELA:DAID is a hot field or requires assignment from browse
  BRW4.AddField(DELA:AdditionalCharge,BRW4.Q.DELA:AdditionalCharge) ! Field DELA:AdditionalCharge is a hot field or requires assignment from browse
  BRW4.AddField(DELA:Charge,BRW4.Q.DELA:Charge)            ! Field DELA:Charge is a hot field or requires assignment from browse
  BRW4.AddField(DELA:DID,BRW4.Q.DELA:DID)                  ! Field DELA:DID is a hot field or requires assignment from browse
  BRW6.Q &= Queue:Browse:6
  BRW6.AddSortOrder(,A_DELL:FKey_DID_Leg)                  ! Add the sort order for A_DELL:FKey_DID_Leg for sort order 1
  BRW6.AddRange(A_DELL:DID,Relate:DeliveryLegsAlias,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW6.AddLocator(BRW6::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW6::Sort0:Locator.Init(,A_DELL:Leg,1,BRW6)             ! Initialize the browse locator using  using key: A_DELL:FKey_DID_Leg , A_DELL:Leg
  BRW6.AddField(A_DELL:DLID,BRW6.Q.A_DELL:DLID)            ! Field A_DELL:DLID is a hot field or requires assignment from browse
  BRW6.AddField(A_DELL:Leg,BRW6.Q.A_DELL:Leg)              ! Field A_DELL:Leg is a hot field or requires assignment from browse
  BRW6.AddField(A_DELL:TID,BRW6.Q.A_DELL:TID)              ! Field A_DELL:TID is a hot field or requires assignment from browse
  BRW6.AddField(A_DELL:Cost,BRW6.Q.A_DELL:Cost)            ! Field A_DELL:Cost is a hot field or requires assignment from browse
  BRW6.AddField(A_DELL:VATRate,BRW6.Q.A_DELL:VATRate)      ! Field A_DELL:VATRate is a hot field or requires assignment from browse
  BRW6.AddField(A_DELL:DID,BRW6.Q.A_DELL:DID)              ! Field A_DELL:DID is a hot field or requires assignment from browse
  BRW8.Q &= Queue:Browse:8
  BRW8.AddSortOrder(,INV:FKey_DID)                         ! Add the sort order for INV:FKey_DID for sort order 1
  BRW8.AddRange(INV:DID,Relate:_Invoice,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW8.AppendOrder('+INV:IID')                             ! Append an additional sort order
  BRW8.AddField(INV:IID,BRW8.Q.INV:IID)                    ! Field INV:IID is a hot field or requires assignment from browse
  BRW8.AddField(INV:DINo,BRW8.Q.INV:DINo)                  ! Field INV:DINo is a hot field or requires assignment from browse
  BRW8.AddField(INV:POD_IID,BRW8.Q.INV:POD_IID)            ! Field INV:POD_IID is a hot field or requires assignment from browse
  BRW8.AddField(INV:CR_IID,BRW8.Q.INV:CR_IID)              ! Field INV:CR_IID is a hot field or requires assignment from browse
  BRW8.AddField(INV:BID,BRW8.Q.INV:BID)                    ! Field INV:BID is a hot field or requires assignment from browse
  BRW8.AddField(INV:BranchName,BRW8.Q.INV:BranchName)      ! Field INV:BranchName is a hot field or requires assignment from browse
  BRW8.AddField(INV:CID,BRW8.Q.INV:CID)                    ! Field INV:CID is a hot field or requires assignment from browse
  BRW8.AddField(INV:ClientNo,BRW8.Q.INV:ClientNo)          ! Field INV:ClientNo is a hot field or requires assignment from browse
  BRW8.AddField(INV:ClientName,BRW8.Q.INV:ClientName)      ! Field INV:ClientName is a hot field or requires assignment from browse
  BRW8.AddField(INV:DID,BRW8.Q.INV:DID)                    ! Field INV:DID is a hot field or requires assignment from browse
  BRW10.Q &= Queue:Browse:10
  BRW10.AddSortOrder(,DELI:FKey_DID_ItemNo)                ! Add the sort order for DELI:FKey_DID_ItemNo for sort order 1
  BRW10.AddRange(DELI:DID,Relate:DeliveryItems,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW10.AddLocator(BRW10::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW10::Sort0:Locator.Init(,DELI:ItemNo,1,BRW10)          ! Initialize the browse locator using  using key: DELI:FKey_DID_ItemNo , DELI:ItemNo
  BRW10.AppendOrder('+DELI:DIID')                          ! Append an additional sort order
  BRW10.AddField(DELI:DIID,BRW10.Q.DELI:DIID)              ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW10.AddField(DELI:ItemNo,BRW10.Q.DELI:ItemNo)          ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW10.AddField(DELI:Units,BRW10.Q.DELI:Units)            ! Field DELI:Units is a hot field or requires assignment from browse
  BRW10.AddField(DELI:Weight,BRW10.Q.DELI:Weight)          ! Field DELI:Weight is a hot field or requires assignment from browse
  BRW10.AddField(DELI:Type,BRW10.Q.DELI:Type)              ! Field DELI:Type is a hot field or requires assignment from browse
  BRW10.AddField(COM:Commodity,BRW10.Q.COM:Commodity)      ! Field COM:Commodity is a hot field or requires assignment from browse
  BRW10.AddField(PACK:Packaging,BRW10.Q.PACK:Packaging)    ! Field PACK:Packaging is a hot field or requires assignment from browse
  BRW10.AddField(DELI:ShowOnInvoice,BRW10.Q.DELI:ShowOnInvoice) ! Field DELI:ShowOnInvoice is a hot field or requires assignment from browse
  BRW10.AddField(DELI:ContainerNo,BRW10.Q.DELI:ContainerNo) ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW10.AddField(DELI:ContainerVessel,BRW10.Q.DELI:ContainerVessel) ! Field DELI:ContainerVessel is a hot field or requires assignment from browse
  BRW10.AddField(DELI:SealNo,BRW10.Q.DELI:SealNo)          ! Field DELI:SealNo is a hot field or requires assignment from browse
  BRW10.AddField(DELI:ByContainer,BRW10.Q.DELI:ByContainer) ! Field DELI:ByContainer is a hot field or requires assignment from browse
  BRW10.AddField(DELI:DID,BRW10.Q.DELI:DID)                ! Field DELI:DID is a hot field or requires assignment from browse
  BRW10.AddField(PACK:PTID,BRW10.Q.PACK:PTID)              ! Field PACK:PTID is a hot field or requires assignment from browse
  BRW10.AddField(COM:CMID,BRW10.Q.COM:CMID)                ! Field COM:CMID is a hot field or requires assignment from browse
  BRW12.Q &= Queue:Browse:12
  BRW12.AddSortOrder(,DELL:FKey_DID_Leg)                   ! Add the sort order for DELL:FKey_DID_Leg for sort order 1
  BRW12.AddRange(DELL:DID,Relate:DeliveryLegs,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW12.AddLocator(BRW12::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW12::Sort0:Locator.Init(,DELL:Leg,1,BRW12)             ! Initialize the browse locator using  using key: DELL:FKey_DID_Leg , DELL:Leg
  BRW12.AddField(DELL:DLID,BRW12.Q.DELL:DLID)              ! Field DELL:DLID is a hot field or requires assignment from browse
  BRW12.AddField(DELL:Leg,BRW12.Q.DELL:Leg)                ! Field DELL:Leg is a hot field or requires assignment from browse
  BRW12.AddField(DELL:TID,BRW12.Q.DELL:TID)                ! Field DELL:TID is a hot field or requires assignment from browse
  BRW12.AddField(DELL:Cost,BRW12.Q.DELL:Cost)              ! Field DELL:Cost is a hot field or requires assignment from browse
  BRW12.AddField(DELL:VATRate,BRW12.Q.DELL:VATRate)        ! Field DELL:VATRate is a hot field or requires assignment from browse
  BRW12.AddField(DELL:DID,BRW12.Q.DELL:DID)                ! Field DELL:DID is a hot field or requires assignment from browse
  BRW14.Q &= Queue:Browse:14
  BRW14.AddSortOrder(,DELP:FKey_DID)                       ! Add the sort order for DELP:FKey_DID for sort order 1
  BRW14.AddRange(DELP:DID,Relate:DeliveryProgress,Relate:Deliveries) ! Add file relationship range limit for sort order 1
  BRW14.AddField(DELP:StatusDate,BRW14.Q.DELP:StatusDate)  ! Field DELP:StatusDate is a hot field or requires assignment from browse
  BRW14.AddField(DELP:StatusTime,BRW14.Q.DELP:StatusTime)  ! Field DELP:StatusTime is a hot field or requires assignment from browse
  BRW14.AddField(DELP:ActionDate,BRW14.Q.DELP:ActionDate)  ! Field DELP:ActionDate is a hot field or requires assignment from browse
  BRW14.AddField(DELP:ActionTime,BRW14.Q.DELP:ActionTime)  ! Field DELP:ActionTime is a hot field or requires assignment from browse
  BRW14.AddField(DELP:DPID,BRW14.Q.DELP:DPID)              ! Field DELP:DPID is a hot field or requires assignment from browse
  BRW14.AddField(DELP:DID,BRW14.Q.DELP:DID)                ! Field DELP:DID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Delivery',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1
  BRW4.AskProcedure = 2
  BRW6.AskProcedure = 3
  BRW8.AskProcedure = 4
  BRW10.AskProcedure = 5
  BRW12.AskProcedure = 6
  BRW14.AskProcedure = 7
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
    Relate:Shortages_Damages.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Delivery',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      UpdateShortages_Damages
      UpdateDeliveriesAdditionalCharges
      UpdateDeliveryLegsAlias
      Update_Invoice
      Update_Delivery_Items
      Update_DeliveryLegs
      UpdateDeliveryProgress
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_GetDID
      ThisWindow.Update()
          IF GLO:ReplicatedDatabaseID = 0
             GLO:ReplicatedDatabaseID = DEL:BID
          .
      
          ! (FileManager FM, Key PrimaryKey, *? RecordID)
          Rep_GetNextID(Access:Deliveries, DEL:PKey_DID, My_ID#)
      
      !    message('My_ID#:  ' & My_ID# & '||GLO:ReplicatedDatabaseID: ' & GLO:ReplicatedDatabaseID)
      
          DEL:DID     = My_ID#
      
      
          DISPLAY
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:5
    SELF.ChangeControl=?Change:5
    SELF.DeleteControl=?Delete:5
  END


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:7
    SELF.ChangeControl=?Change:7
    SELF.DeleteControl=?Delete:7
  END


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:9
    SELF.ChangeControl=?Change:9
    SELF.DeleteControl=?Delete:9
  END


BRW10.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:11
    SELF.ChangeControl=?Change:11
    SELF.DeleteControl=?Delete:11
  END


BRW12.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:13
    SELF.ChangeControl=?Change:13
    SELF.DeleteControl=?Delete:13
  END


BRW14.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:15
    SELF.ChangeControl=?Change:15
    SELF.DeleteControl=?Delete:15
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Procedure not yet defined
!!! </summary>
UpdateShortages_Damages PROCEDURE !Procedure not yet defined
  CODE
  GlobalErrors.ThrowMessage(Msg:ProcedureToDo,'UpdateShortages_Damages') ! This procedure acts as a place holder for a procedure yet to be defined
  SETKEYCODE(0)
  GlobalResponse = RequestCancelled                        ! Request cancelled is the implied action
!!! <summary>
!!! Generated from procedure template - Window
!!! Form DeliveriesAdditionalCharges
!!! </summary>
UpdateDeliveriesAdditionalCharges PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::DELA:Record LIKE(DELA:RECORD),THREAD
QuickWindow          WINDOW('Form DeliveriesAdditionalCharges'),AT(,,236,70),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('UpdateDeliveriesAdditionalCharges'),SYSTEM
                       SHEET,AT(4,4,228,44),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Additional Charge:'),AT(8,20),USE(?DELA:AdditionalCharge:Prompt),TRN
                           ENTRY(@s35),AT(84,20,144,10),USE(DELA:AdditionalCharge),MSG('Description'),TIP('Description')
                           PROMPT('Charge:'),AT(8,34),USE(?DELA:Charge:Prompt),TRN
                           ENTRY(@n-15.2),AT(84,34,68,10),USE(DELA:Charge),DECIMAL(12)
                         END
                       END
                       BUTTON('&OK'),AT(77,52,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(130,52,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(183,52,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Shortages & Damages Record'
  OF InsertRecord
    ActionMessage = 'Shortages & Damages Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Shortages & Damages Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateDeliveriesAdditionalCharges')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DELA:AdditionalCharge:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:DeliveriesAdditionalCharges)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DELA:Record,History::DELA:Record)
  SELF.AddHistoryField(?DELA:AdditionalCharge,4)
  SELF.AddHistoryField(?DELA:Charge,5)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:DeliveriesAdditionalCharges.Open                  ! File DeliveriesAdditionalCharges used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveriesAdditionalCharges
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?DELA:AdditionalCharge{PROP:ReadOnly} = True
    ?DELA:Charge{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('UpdateDeliveriesAdditionalCharges',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveriesAdditionalCharges.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateDeliveriesAdditionalCharges',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Form DeliveryLegsAlias
!!! </summary>
UpdateDeliveryLegsAlias PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::A_DELL:Record LIKE(A_DELL:RECORD),THREAD
QuickWindow          WINDOW('Form DeliveryLegsAlias'),AT(,,173,98),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('UpdateDeliveryLegsAlias'),SYSTEM
                       SHEET,AT(4,4,165,72),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Leg:'),AT(8,20),USE(?A_DELL:Leg:Prompt),TRN
                           SPIN(@n5),AT(61,20,40,10),USE(A_DELL:Leg),RIGHT(1),MSG('Leg Number'),REQ,TIP('Leg Number')
                           PROMPT('TID:'),AT(8,34),USE(?A_DELL:TID:Prompt),TRN
                           STRING(@n_10),AT(61,34,104,10),USE(A_DELL:TID),RIGHT(1),TRN
                           PROMPT('Cost:'),AT(8,48),USE(?A_DELL:Cost:Prompt),TRN
                           ENTRY(@n-13.2),AT(61,48,60,10),USE(A_DELL:Cost),DECIMAL(12),MSG('Cost for this leg (fro' & |
  'm Transporter)'),TIP('Cost for this leg (from Transporter)')
                           PROMPT('VAT Rate:'),AT(8,62),USE(?A_DELL:VATRate:Prompt),TRN
                           ENTRY(@n-7.2),AT(61,62,40,10),USE(A_DELL:VATRate),RIGHT(1),MSG('VAT rate'),TIP('VAT rate')
                         END
                       END
                       BUTTON('&OK'),AT(14,80,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(67,80,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(120,80,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Shortages & Damages Record'
  OF InsertRecord
    ActionMessage = 'Shortages & Damages Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Shortages & Damages Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateDeliveryLegsAlias')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?A_DELL:Leg:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:DeliveryLegsAlias)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(A_DELL:Record,History::A_DELL:Record)
  SELF.AddHistoryField(?A_DELL:Leg,3)
  SELF.AddHistoryField(?A_DELL:TID,4)
  SELF.AddHistoryField(?A_DELL:Cost,9)
  SELF.AddHistoryField(?A_DELL:VATRate,10)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:DeliveryLegsAlias.Open                            ! File DeliveryLegsAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DeliveryLegsAlias
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?A_DELL:Cost{PROP:ReadOnly} = True
    ?A_DELL:VATRate{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('UpdateDeliveryLegsAlias',QuickWindow)      ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryLegsAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateDeliveryLegsAlias',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Invoice PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(_StatementItems)
                       PROJECT(STAI:STID)
                       PROJECT(STAI:InvoiceDate)
                       PROJECT(STAI:InvoiceTime)
                       PROJECT(STAI:IID)
                       PROJECT(STAI:DINo)
                       PROJECT(STAI:Debit)
                       PROJECT(STAI:Credit)
                       PROJECT(STAI:Amount)
                       PROJECT(STAI:STIID)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
STAI:STID              LIKE(STAI:STID)                !List box control field - type derived from field
STAI:InvoiceDate       LIKE(STAI:InvoiceDate)         !List box control field - type derived from field
STAI:InvoiceTime       LIKE(STAI:InvoiceTime)         !List box control field - type derived from field
STAI:IID               LIKE(STAI:IID)                 !List box control field - type derived from field
STAI:DINo              LIKE(STAI:DINo)                !List box control field - type derived from field
STAI:Debit             LIKE(STAI:Debit)               !List box control field - type derived from field
STAI:Credit            LIKE(STAI:Credit)              !List box control field - type derived from field
STAI:Amount            LIKE(STAI:Amount)              !List box control field - type derived from field
STAI:STIID             LIKE(STAI:STIID)               !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(ClientsPaymentsAllocation)
                       PROJECT(CLIPA:CPID)
                       PROJECT(CLIPA:AllocationNo)
                       PROJECT(CLIPA:IID)
                       PROJECT(CLIPA:Amount)
                       PROJECT(CLIPA:AllocationDate)
                       PROJECT(CLIPA:AllocationTime)
                       PROJECT(CLIPA:Comment)
                       PROJECT(CLIPA:StatusUpToDate)
                       PROJECT(CLIPA:CPAID)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
CLIPA:CPID             LIKE(CLIPA:CPID)               !List box control field - type derived from field
CLIPA:AllocationNo     LIKE(CLIPA:AllocationNo)       !List box control field - type derived from field
CLIPA:IID              LIKE(CLIPA:IID)                !List box control field - type derived from field
CLIPA:Amount           LIKE(CLIPA:Amount)             !List box control field - type derived from field
CLIPA:AllocationDate   LIKE(CLIPA:AllocationDate)     !List box control field - type derived from field
CLIPA:AllocationTime   LIKE(CLIPA:AllocationTime)     !List box control field - type derived from field
CLIPA:Comment          LIKE(CLIPA:Comment)            !List box control field - type derived from field
CLIPA:StatusUpToDate   LIKE(CLIPA:StatusUpToDate)     !List box control field - type derived from field
CLIPA:CPAID            LIKE(CLIPA:CPAID)              !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(_InvoiceItems)
                       PROJECT(INI:ITID)
                       PROJECT(INI:ItemNo)
                       PROJECT(INI:Units)
                       PROJECT(INI:Commodity)
                       PROJECT(INI:DIID)
                       PROJECT(INI:Description)
                       PROJECT(INI:Type)
                       PROJECT(INI:ContainerDescription)
                       PROJECT(INI:IID)
                       JOIN(DELI:PKey_DIID,INI:DIID)
                         PROJECT(DELI:DID)
                         PROJECT(DELI:ItemNo)
                         PROJECT(DELI:Units)
                         PROJECT(DELI:Weight)
                         PROJECT(DELI:DIID)
                         JOIN(DEL:PKey_DID,DELI:DID)
                           PROJECT(DEL:DINo)
                           PROJECT(DEL:DID)
                         END
                       END
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?Browse:6
INI:ITID               LIKE(INI:ITID)                 !List box control field - type derived from field
INI:ItemNo             LIKE(INI:ItemNo)               !List box control field - type derived from field
INI:Units              LIKE(INI:Units)                !List box control field - type derived from field
INI:Commodity          LIKE(INI:Commodity)            !List box control field - type derived from field
INI:DIID               LIKE(INI:DIID)                 !List box control field - type derived from field
DELI:DID               LIKE(DELI:DID)                 !List box control field - type derived from field
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Units             LIKE(DELI:Units)               !List box control field - type derived from field
DELI:Weight            LIKE(DELI:Weight)              !List box control field - type derived from field
INI:Description        LIKE(INI:Description)          !List box control field - type derived from field
INI:Type               LIKE(INI:Type)                 !List box control field - type derived from field
INI:ContainerDescription LIKE(INI:ContainerDescription) !List box control field - type derived from field
INI:IID                LIKE(INI:IID)                  !Browse key field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Related join file key field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::INV:Record  LIKE(INV:RECORD),THREAD
QuickWindow          WINDOW('Form _Invoice'),AT(,,358,192),FONT('MS Sans Serif',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_Invoice'),SYSTEM
                       SHEET,AT(4,4,350,166),USE(?CurrentTab),JOIN
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('Invoice Number:'),AT(8,24),USE(?INV:IID:Prompt:2),TRN
                           ENTRY(@n_10),AT(100,24,65,10),USE(INV:IID),RIGHT(1),MSG('Invoice Number')
                           BUTTON('Get Next ID'),AT(223,22,49,14),USE(?Button_GetID)
                           PROMPT('MID:'),AT(235,55),USE(?INV:MID:Prompt:2),TRN
                           ENTRY(@n_10),AT(283,55,65,10),USE(INV:MID),RIGHT(1),MSG('Generating MID')
                           PROMPT('MIDs:'),AT(235,38),USE(?INV:MIDs:Prompt),TRN
                           ENTRY(@s20),AT(283,38,65,10),USE(INV:MIDs),RIGHT(1),MSG('List of Manifest IDs that the ' & |
  'delivery is currently manifested on'),TIP('List of Manifest IDs that the delivery is' & |
  ' currently manifested on')
                           PROMPT('POD IID:'),AT(8,40),USE(?INV:POD_IID:Prompt:2),TRN
                           ENTRY(@n_10),AT(100,40,65,10),USE(INV:POD_IID),RIGHT(1),MSG('POD No. - same as invoice except for Journal debit/credit of invoice')
                           PROMPT('CR IID:'),AT(8,54),USE(?INV:CR_IID:Prompt:2),TRN
                           ENTRY(@n_10),AT(283,100,65,10),USE(INV:DID),RIGHT(1),MSG('Delivery ID')
                           ENTRY(@n_10),AT(100,56,65,10),USE(INV:CR_IID),RIGHT(1),MSG('Credit of Invoice, Invoice IID of credited Invoice')
                           PROMPT('DID:'),AT(235,100),USE(?Prompt48)
                           PROMPT('BID:'),AT(8,100),USE(?INV:BID:Prompt:2),TRN
                           ENTRY(@n_10),AT(100,100,65,10),USE(INV:BID),RIGHT(1),MSG('Branch ID')
                           PROMPT('IJID:'),AT(235,70),USE(?INV:IJID:Prompt:2),TRN
                           ENTRY(@n_10),AT(283,70,65,10),USE(INV:IJID),RIGHT(1),MSG('Invoice Journal ID')
                           PROMPT('DC ID:'),AT(235,86),USE(?INV:IJID:Prompt:3),TRN
                           ENTRY(@n_10),AT(283,86,65,10),USE(INV:DC_ID),RIGHT(1),MSG('Delivery COD Addresses ID')
                           PROMPT('Branch Name:'),AT(8,114),USE(?INV:BranchName:Prompt),TRN
                           ENTRY(@s35),AT(100,114,65,10),USE(INV:BranchName),MSG('Branch Name'),TIP('Branch Name')
                           PROMPT('CID:'),AT(8,130),USE(?INV:CID:Prompt:2),TRN
                           ENTRY(@n_10),AT(100,128,65,10),USE(INV:CID),RIGHT(1),MSG('Client ID')
                           PROMPT('UID:'),AT(235,127),USE(?INV:DINo:Prompt:2),TRN
                           ENTRY(@n_10),AT(283,127,65,10),USE(INV:UID),RIGHT(1),MSG('User ID')
                           PROMPT('Client No:'),AT(8,142),USE(?INV:ClientNo:Prompt),TRN
                           ENTRY(@n_10b),AT(100,142,65,10),USE(INV:ClientNo),RIGHT(1),MSG('Client No.'),TIP('Client No.')
                           PROMPT('DI No.:'),AT(235,114),USE(?INV:DINo:Prompt),TRN
                           ENTRY(@n_10),AT(283,114,65,10),USE(INV:DINo),RIGHT(1),MSG('Delivery Instruction Number'),REQ, |
  TIP('Delivery Instruction Number')
                           PROMPT('Client Reference:'),AT(203,142),USE(?INV:ClientReference:Prompt),TRN
                           ENTRY(@s60),AT(283,142,65,10),USE(INV:ClientReference),MSG('Client Reference'),TIP('Client Reference')
                           PROMPT('Client Name:'),AT(8,156),USE(?INV:ClientName:Prompt),TRN
                           ENTRY(@s100),AT(100,156,250,10),USE(INV:ClientName),TIP('Client name')
                         END
                         TAB('&2) Gen'),USE(?Tab:2)
                           PROMPT('Client Line 1:'),AT(8,34),USE(?INV:ClientLine1:Prompt),TRN
                           ENTRY(@s35),AT(100,34,144,10),USE(INV:ClientLine1),MSG('Address line 1'),TIP('Address line 1')
                           PROMPT('Client Line 2:'),AT(8,48),USE(?INV:ClientLine2:Prompt),TRN
                           ENTRY(@s35),AT(100,48,144,10),USE(INV:ClientLine2),MSG('Address line 2'),TIP('Address line 2')
                           PROMPT('Client Suburb:'),AT(8,68),USE(?INV:ClientSuburb:Prompt),TRN
                           ENTRY(@s50),AT(100,68,144,10),USE(INV:ClientSuburb),MSG('Suburb'),TIP('Suburb')
                           PROMPT('Client Postal Code:'),AT(8,82),USE(?INV:ClientPostalCode:Prompt),TRN
                           ENTRY(@s10),AT(100,82,44,10),USE(INV:ClientPostalCode)
                           PROMPT('VAT No:'),AT(8,96),USE(?INV:VATNo:Prompt),TRN
                           ENTRY(@s20),AT(100,96,84,10),USE(INV:VATNo),MSG('VAT No.'),TIP('VAT No.')
                           PROMPT('Invoice Message:'),AT(8,110),USE(?INV:InvoiceMessage:Prompt),TRN
                           ENTRY(@s255),AT(100,110,250,10),USE(INV:InvoiceMessage)
                           PROMPT('Terms:'),AT(8,128),USE(?INV:Terms:Prompt),TRN
                           LIST,AT(100,128,84,10),USE(INV:Terms),DROP(5),FROM('Pre Paid|#0|COD|#1|Account|#2'),MSG('Terms - Pr' & |
  'e Paid, COD, Account'),TIP('Terms - Pre Paid, COD, Account')
                           PROMPT('Name:'),AT(8,142),USE(?INV:ShipperName:Prompt),TRN
                           ENTRY(@s35),AT(100,142,144,10),USE(INV:ShipperName),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                           PROMPT('Shipper Line 1:'),AT(8,156),USE(?INV:ShipperLine1:Prompt),TRN
                           ENTRY(@s35),AT(100,156,144,10),USE(INV:ShipperLine1),MSG('Address line 1'),TIP('Address line 1')
                         END
                         TAB('&3) Gen'),USE(?Tab:3)
                           PROMPT('Shipper Line 2:'),AT(8,30),USE(?INV:ShipperLine2:Prompt),TRN
                           ENTRY(@s35),AT(100,30,144,10),USE(INV:ShipperLine2),MSG('Address line 2'),TIP('Address line 2')
                           PROMPT('Shipper Suburb:'),AT(8,44),USE(?INV:ShipperSuburb:Prompt),TRN
                           ENTRY(@s50),AT(100,44,144,10),USE(INV:ShipperSuburb),MSG('Suburb'),TIP('Suburb')
                           PROMPT('Shipper Postal Code:'),AT(8,58),USE(?INV:ShipperPostalCode:Prompt),TRN
                           ENTRY(@s10),AT(100,58,44,10),USE(INV:ShipperPostalCode)
                           PROMPT('Consignee Name:'),AT(8,72),USE(?INV:ConsigneeName:Prompt),TRN
                           ENTRY(@s35),AT(100,72,144,10),USE(INV:ConsigneeName),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                           PROMPT('Consignee Line 1:'),AT(8,86),USE(?INV:ConsigneeLine1:Prompt),TRN
                           ENTRY(@s35),AT(100,86,144,10),USE(INV:ConsigneeLine1),MSG('Address line 1'),TIP('Address line 1')
                           PROMPT('Consignee Line 2:'),AT(8,100),USE(?INV:ConsigneeLine2:Prompt),TRN
                           ENTRY(@s35),AT(100,100,144,10),USE(INV:ConsigneeLine2),MSG('Address line 2'),TIP('Address line 2')
                           PROMPT('Consignee Suburb:'),AT(8,114),USE(?INV:ConsigneeSuburb:Prompt),TRN
                           ENTRY(@s50),AT(100,114,144,10),USE(INV:ConsigneeSuburb),MSG('Suburb'),TIP('Suburb')
                           PROMPT('Consignee Postal Code:'),AT(8,128),USE(?INV:ConsigneePostalCode:Prompt),TRN
                           ENTRY(@s10),AT(100,128,44,10),USE(INV:ConsigneePostalCode)
                           PROMPT('Invoice Date:'),AT(8,142),USE(?INV:InvoiceDate:Prompt),TRN
                           SPIN(@d6),AT(100,142,44,10),USE(INV:InvoiceDate),RIGHT(1),MSG('Invoice Date'),TIP('Invoice Date')
                           PROMPT('Invoice Time:'),AT(8,156),USE(?INV:InvoiceTime:Prompt),TRN
                           ENTRY(@t7),AT(100,156,44,10),USE(INV:InvoiceTime),MSG('Invoice Time'),TIP('Invoice Time')
                         END
                         TAB('&4) Gen'),USE(?Tab:4)
                           CHECK(' Printed'),AT(80,30,70,8),USE(INV:Printed),MSG('Printed'),TIP('Printed')
                           PROMPT('Insurance:'),AT(8,42),USE(?INV:Insurance:Prompt),TRN
                           ENTRY(@n-14.2),AT(80,42,64,10),USE(INV:Insurance),RIGHT(1)
                           PROMPT('Documentation:'),AT(8,56),USE(?INV:Documentation:Prompt),TRN
                           ENTRY(@n-14.2),AT(80,56,64,10),USE(INV:Documentation),RIGHT(1)
                           PROMPT('Fuel Surcharge:'),AT(8,70),USE(?INV:FuelSurcharge:Prompt),TRN
                           ENTRY(@n-14.2),AT(80,70,64,10),USE(INV:FuelSurcharge),RIGHT(1)
                           PROMPT('Additional Charge:'),AT(8,84),USE(?INV:AdditionalCharge:Prompt),TRN
                           ENTRY(@n-15.2),AT(80,84,64,10),USE(INV:AdditionalCharge),DECIMAL(12),MSG('Addi'),TIP('Addi')
                           PROMPT('Freight Charge:'),AT(8,98),USE(?INV:FreightCharge:Prompt),TRN
                           ENTRY(@n-15.2),AT(80,98,64,10),USE(INV:FreightCharge),RIGHT(1)
                           PROMPT('VAT:'),AT(8,112),USE(?INV:VAT:Prompt),TRN
                           ENTRY(@n-14.2),AT(80,112,64,10),USE(INV:VAT),RIGHT(1),MSG('VAT'),TIP('VAT')
                           PROMPT('Total:'),AT(8,126),USE(?INV:Total:Prompt),TRN
                           ENTRY(@n-15.2),AT(80,126,64,10),USE(INV:Total),RIGHT(1)
                           PROMPT('VAT Rate:'),AT(8,140),USE(?INV:VATRate:Prompt),TRN
                           ENTRY(@n-7.2),AT(80,140,64,10),USE(INV:VATRate),RIGHT(1),MSG('VAT rate'),TIP('VAT rate')
                           PROMPT('Weight:'),AT(8,154),USE(?INV:Weight:Prompt),TRN
                           ENTRY(@n-11.2),AT(80,154,64,10),USE(INV:Weight),RIGHT(1),MSG('In kg''s'),TIP('In kg''s')
                           PROMPT('Volumetric Weight:'),AT(196,44),USE(?INV:VolumetricWeight:Prompt),TRN
                           ENTRY(@n-11.2),AT(274,44,52,10),USE(INV:VolumetricWeight),RIGHT(1),MSG('Weight based on' & |
  ' Volumetric calculation'),TIP('Weight based on Volumetric calculation (in kgs)')
                           PROMPT('Volume:'),AT(196,58),USE(?INV:Volume:Prompt),TRN
                           ENTRY(@n-11.3),AT(274,58,52,10),USE(INV:Volume),RIGHT(1),MSG('Volume for manual entry'),TIP('Volume for' & |
  ' manual entry (metres cubed)')
                           CHECK('Bad Debt'),AT(274,72,70,8),USE(INV:BadDebt),MSG('This is a Bad Debt Credit Note'),TIP('This is a ' & |
  'Bad Debt Credit Note')
                           PROMPT('Status:'),AT(196,84),USE(?INV:Status:Prompt),TRN
                           LIST,AT(274,84,70,10),USE(INV:Status),DROP(5),FROM('No Payments|#0|Partially Paid|#1|Cr' & |
  'edit Note|#2|Fully Paid - Credit|#3|Fully Paid|#4|Credit Note Shown|#5|Bad Debt|#6|O' & |
  'ver Paid|#7'),MSG('used for filtering - No Payments, Partially Paid, Credit Note, Fu' & |
  'lly Paid - Credit, Fully Paid, Credit Note Shown'),TIP('No Payments, Partially Paid,' & |
  ' Credit Note, Fully Paid - Credit<0DH,0AH,0DH,0AH>Fully Paid, Credit Note Shown on a' & |
  ' client statement')
                           PROMPT('Status Up To Date:'),AT(196,98),USE(?INV:StatusUpToDate:Prompt),TRN
                           ENTRY(@n3),AT(274,98,40,10),USE(INV:StatusUpToDate),MSG('Has'),TIP('Has')
                           PROMPT('Created Date:'),AT(196,140),USE(?INV:Created_Date:Prompt),TRN
                           STRING(@d6b),AT(274,140,70,10),USE(INV:Created_Date),TRN
                           PROMPT('Created Time:'),AT(196,154),USE(?INV:Created_Time:Prompt),TRN
                           STRING(@t4),AT(274,154,70,10),USE(INV:Created_Time),TRN
                         END
                         TAB('&8) Invoice Items'),USE(?Tab:8)
                           LIST,AT(8,30,342,118),USE(?Browse:6),HVSCROLL,FORMAT('40R(2)|M~ITID~C(0)@n_10@30R(2)|M~' & |
  'Item No.~C(0)@n6@22R(2)|M~Units~C(0)@n6@60L(2)|M~Commodity~@s35@[40R(1)|M~DIID~C(0)@' & |
  'n_10@40R(1)|M~DID~C(0)@n_10@40R(1)|M~DI No.~C(0)@n_10@30R(1)|M~Item No.~C(0)@n_5@22R' & |
  '(1)|M~Units~C(0)@n6@44R(1)|M~Weight~C(0)@n-11.2@]|M~DIID~60L(2)|M~Description~@s150@' & |
  '20R(2)|M~Type~C(0)@n3@56L(1)|M~Container Desc.~C(0)@s150@'),FROM(Queue:Browse:6),IMM,MSG('Browsing t' & |
  'he _InvoiceItems file')
                           BUTTON('&Insert'),AT(195,152,49,14),USE(?Insert:7),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(248,152,49,14),USE(?Change:7),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(301,152,49,14),USE(?Delete:7),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&7) Clients Payments Allocation'),USE(?Tab:7)
                           LIST,AT(8,30,342,118),USE(?Browse:4),HVSCROLL,FORMAT('80R(2)|M~CPID~C(0)@n_10@80R(2)|M~' & |
  'Allocation No~C(0)@n_5@80R(2)|M~Invoice Number~C(0)@n_10@64D(26)|M~Amount~C(0)@n-14.' & |
  '2@80R(2)|M~Allocation Date~C(0)@d17@80R(2)|M~Allocation Time~C(0)@t7@80L(2)|M~Commen' & |
  't~L(2)@s255@72R(2)|M~Status Up To Date~C(0)@n3@'),FROM(Queue:Browse:4),IMM,MSG('Browsing t' & |
  'he _InvoiceItems file')
                           BUTTON('&Insert'),AT(195,152,49,14),USE(?Insert:5),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(248,152,49,14),USE(?Change:5),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(301,152,49,14),USE(?Delete:5),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&6) Statement Items'),USE(?Tab:6)
                           LIST,AT(8,30,342,118),USE(?Browse:2),HVSCROLL,FORMAT('80R(2)|M~STID~C(0)@n_10@80R(2)|M~' & |
  'Invoice Date~C(0)@d17@80R(2)|M~Invoice Time~C(0)@t7@80R(2)|M~Invoice Number~C(0)@n_1' & |
  '0@80R(2)|M~DI No.~C(0)@n_10@64D(28)|M~Debit~C(0)@n-14.2@64D(26)|M~Credit~C(0)@n-14.2' & |
  '@64D(26)|M~Amount~C(0)@n-14.2@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the _Invoice' & |
  'Items file')
                           BUTTON('&Insert'),AT(195,152,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(248,152,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(301,152,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(199,174,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(252,174,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(305,174,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW6                 CLASS(BrowseClass)                    ! Browse using ?Browse:6
Q                      &Queue:Browse:6                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Invoice Record'
  OF InsertRecord
    ActionMessage = 'Invoice Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Invoice Rec. Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Invoice')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?INV:IID:Prompt:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:_Invoice)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(INV:Record,History::INV:Record)
  SELF.AddHistoryField(?INV:IID,1)
  SELF.AddHistoryField(?INV:MID,55)
  SELF.AddHistoryField(?INV:MIDs,19)
  SELF.AddHistoryField(?INV:POD_IID,2)
  SELF.AddHistoryField(?INV:DID,16)
  SELF.AddHistoryField(?INV:CR_IID,3)
  SELF.AddHistoryField(?INV:BID,4)
  SELF.AddHistoryField(?INV:IJID,58)
  SELF.AddHistoryField(?INV:DC_ID,57)
  SELF.AddHistoryField(?INV:BranchName,5)
  SELF.AddHistoryField(?INV:CID,6)
  SELF.AddHistoryField(?INV:UID,56)
  SELF.AddHistoryField(?INV:ClientNo,8)
  SELF.AddHistoryField(?INV:DINo,17)
  SELF.AddHistoryField(?INV:ClientReference,18)
  SELF.AddHistoryField(?INV:ClientName,9)
  SELF.AddHistoryField(?INV:ClientLine1,10)
  SELF.AddHistoryField(?INV:ClientLine2,11)
  SELF.AddHistoryField(?INV:ClientSuburb,12)
  SELF.AddHistoryField(?INV:ClientPostalCode,13)
  SELF.AddHistoryField(?INV:VATNo,14)
  SELF.AddHistoryField(?INV:InvoiceMessage,15)
  SELF.AddHistoryField(?INV:Terms,20)
  SELF.AddHistoryField(?INV:ShipperName,23)
  SELF.AddHistoryField(?INV:ShipperLine1,24)
  SELF.AddHistoryField(?INV:ShipperLine2,25)
  SELF.AddHistoryField(?INV:ShipperSuburb,26)
  SELF.AddHistoryField(?INV:ShipperPostalCode,27)
  SELF.AddHistoryField(?INV:ConsigneeName,29)
  SELF.AddHistoryField(?INV:ConsigneeLine1,30)
  SELF.AddHistoryField(?INV:ConsigneeLine2,31)
  SELF.AddHistoryField(?INV:ConsigneeSuburb,32)
  SELF.AddHistoryField(?INV:ConsigneePostalCode,33)
  SELF.AddHistoryField(?INV:InvoiceDate,36)
  SELF.AddHistoryField(?INV:InvoiceTime,37)
  SELF.AddHistoryField(?INV:Printed,38)
  SELF.AddHistoryField(?INV:Insurance,40)
  SELF.AddHistoryField(?INV:Documentation,41)
  SELF.AddHistoryField(?INV:FuelSurcharge,42)
  SELF.AddHistoryField(?INV:AdditionalCharge,43)
  SELF.AddHistoryField(?INV:FreightCharge,44)
  SELF.AddHistoryField(?INV:VAT,45)
  SELF.AddHistoryField(?INV:Total,46)
  SELF.AddHistoryField(?INV:VATRate,48)
  SELF.AddHistoryField(?INV:Weight,49)
  SELF.AddHistoryField(?INV:VolumetricWeight,50)
  SELF.AddHistoryField(?INV:Volume,51)
  SELF.AddHistoryField(?INV:BadDebt,52)
  SELF.AddHistoryField(?INV:Status,53)
  SELF.AddHistoryField(?INV:StatusUpToDate,54)
  SELF.AddHistoryField(?INV:Created_Date,61)
  SELF.AddHistoryField(?INV:Created_Time,62)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ClientsPaymentsAllocation.SetOpenRelated()
  Relate:ClientsPaymentsAllocation.Open                    ! File ClientsPaymentsAllocation used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:_Invoice
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:_StatementItems,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:ClientsPaymentsAllocation,SELF) ! Initialize the browse manager
  BRW6.Init(?Browse:6,Queue:Browse:6.ViewPosition,BRW6::View:Browse,Queue:Browse:6,Relate:_InvoiceItems,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?Button_GetID)
    ?INV:MIDs{PROP:ReadOnly} = True
    ?INV:DID{PROP:ReadOnly} = True
    ?INV:DC_ID{PROP:ReadOnly} = True
    ?INV:BranchName{PROP:ReadOnly} = True
    ?INV:UID{PROP:ReadOnly} = True
    ?INV:ClientNo{PROP:ReadOnly} = True
    ?INV:DINo{PROP:ReadOnly} = True
    ?INV:ClientReference{PROP:ReadOnly} = True
    ?INV:ClientName{PROP:ReadOnly} = True
    ?INV:ClientLine1{PROP:ReadOnly} = True
    ?INV:ClientLine2{PROP:ReadOnly} = True
    ?INV:ClientSuburb{PROP:ReadOnly} = True
    ?INV:ClientPostalCode{PROP:ReadOnly} = True
    ?INV:VATNo{PROP:ReadOnly} = True
    ?INV:InvoiceMessage{PROP:ReadOnly} = True
    DISABLE(?INV:Terms)
    ?INV:ShipperName{PROP:ReadOnly} = True
    ?INV:ShipperLine1{PROP:ReadOnly} = True
    ?INV:ShipperLine2{PROP:ReadOnly} = True
    ?INV:ShipperSuburb{PROP:ReadOnly} = True
    ?INV:ShipperPostalCode{PROP:ReadOnly} = True
    ?INV:ConsigneeName{PROP:ReadOnly} = True
    ?INV:ConsigneeLine1{PROP:ReadOnly} = True
    ?INV:ConsigneeLine2{PROP:ReadOnly} = True
    ?INV:ConsigneeSuburb{PROP:ReadOnly} = True
    ?INV:ConsigneePostalCode{PROP:ReadOnly} = True
    ?INV:InvoiceTime{PROP:ReadOnly} = True
    ?INV:Insurance{PROP:ReadOnly} = True
    ?INV:Documentation{PROP:ReadOnly} = True
    ?INV:FuelSurcharge{PROP:ReadOnly} = True
    ?INV:AdditionalCharge{PROP:ReadOnly} = True
    ?INV:FreightCharge{PROP:ReadOnly} = True
    ?INV:VAT{PROP:ReadOnly} = True
    ?INV:Total{PROP:ReadOnly} = True
    ?INV:VATRate{PROP:ReadOnly} = True
    ?INV:Weight{PROP:ReadOnly} = True
    ?INV:VolumetricWeight{PROP:ReadOnly} = True
    ?INV:Volume{PROP:ReadOnly} = True
    DISABLE(?INV:Status)
    ?INV:StatusUpToDate{PROP:ReadOnly} = True
    DISABLE(?Insert:7)
    DISABLE(?Change:7)
    DISABLE(?Delete:7)
    DISABLE(?Insert:5)
    DISABLE(?Change:5)
    DISABLE(?Delete:5)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,STAI:FKey_IID)                        ! Add the sort order for STAI:FKey_IID for sort order 1
  BRW2.AddRange(STAI:IID,Relate:_StatementItems,Relate:_Invoice) ! Add file relationship range limit for sort order 1
  BRW2.AddField(STAI:STID,BRW2.Q.STAI:STID)                ! Field STAI:STID is a hot field or requires assignment from browse
  BRW2.AddField(STAI:InvoiceDate,BRW2.Q.STAI:InvoiceDate)  ! Field STAI:InvoiceDate is a hot field or requires assignment from browse
  BRW2.AddField(STAI:InvoiceTime,BRW2.Q.STAI:InvoiceTime)  ! Field STAI:InvoiceTime is a hot field or requires assignment from browse
  BRW2.AddField(STAI:IID,BRW2.Q.STAI:IID)                  ! Field STAI:IID is a hot field or requires assignment from browse
  BRW2.AddField(STAI:DINo,BRW2.Q.STAI:DINo)                ! Field STAI:DINo is a hot field or requires assignment from browse
  BRW2.AddField(STAI:Debit,BRW2.Q.STAI:Debit)              ! Field STAI:Debit is a hot field or requires assignment from browse
  BRW2.AddField(STAI:Credit,BRW2.Q.STAI:Credit)            ! Field STAI:Credit is a hot field or requires assignment from browse
  BRW2.AddField(STAI:Amount,BRW2.Q.STAI:Amount)            ! Field STAI:Amount is a hot field or requires assignment from browse
  BRW2.AddField(STAI:STIID,BRW2.Q.STAI:STIID)              ! Field STAI:STIID is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4.AddSortOrder(,CLIPA:Fkey_IID)                       ! Add the sort order for CLIPA:Fkey_IID for sort order 1
  BRW4.AddRange(CLIPA:IID,Relate:ClientsPaymentsAllocation,Relate:_Invoice) ! Add file relationship range limit for sort order 1
  BRW4.AddField(CLIPA:CPID,BRW4.Q.CLIPA:CPID)              ! Field CLIPA:CPID is a hot field or requires assignment from browse
  BRW4.AddField(CLIPA:AllocationNo,BRW4.Q.CLIPA:AllocationNo) ! Field CLIPA:AllocationNo is a hot field or requires assignment from browse
  BRW4.AddField(CLIPA:IID,BRW4.Q.CLIPA:IID)                ! Field CLIPA:IID is a hot field or requires assignment from browse
  BRW4.AddField(CLIPA:Amount,BRW4.Q.CLIPA:Amount)          ! Field CLIPA:Amount is a hot field or requires assignment from browse
  BRW4.AddField(CLIPA:AllocationDate,BRW4.Q.CLIPA:AllocationDate) ! Field CLIPA:AllocationDate is a hot field or requires assignment from browse
  BRW4.AddField(CLIPA:AllocationTime,BRW4.Q.CLIPA:AllocationTime) ! Field CLIPA:AllocationTime is a hot field or requires assignment from browse
  BRW4.AddField(CLIPA:Comment,BRW4.Q.CLIPA:Comment)        ! Field CLIPA:Comment is a hot field or requires assignment from browse
  BRW4.AddField(CLIPA:StatusUpToDate,BRW4.Q.CLIPA:StatusUpToDate) ! Field CLIPA:StatusUpToDate is a hot field or requires assignment from browse
  BRW4.AddField(CLIPA:CPAID,BRW4.Q.CLIPA:CPAID)            ! Field CLIPA:CPAID is a hot field or requires assignment from browse
  BRW6.Q &= Queue:Browse:6
  BRW6.FileLoaded = 1                                      ! This is a 'file loaded' browse
  BRW6.AddSortOrder(,INI:FKey_IID)                         ! Add the sort order for INI:FKey_IID for sort order 1
  BRW6.AddRange(INI:IID,Relate:_InvoiceItems,Relate:_Invoice) ! Add file relationship range limit for sort order 1
  BRW6.AppendOrder('+INI:ITID')                            ! Append an additional sort order
  BRW6.AddField(INI:ITID,BRW6.Q.INI:ITID)                  ! Field INI:ITID is a hot field or requires assignment from browse
  BRW6.AddField(INI:ItemNo,BRW6.Q.INI:ItemNo)              ! Field INI:ItemNo is a hot field or requires assignment from browse
  BRW6.AddField(INI:Units,BRW6.Q.INI:Units)                ! Field INI:Units is a hot field or requires assignment from browse
  BRW6.AddField(INI:Commodity,BRW6.Q.INI:Commodity)        ! Field INI:Commodity is a hot field or requires assignment from browse
  BRW6.AddField(INI:DIID,BRW6.Q.INI:DIID)                  ! Field INI:DIID is a hot field or requires assignment from browse
  BRW6.AddField(DELI:DID,BRW6.Q.DELI:DID)                  ! Field DELI:DID is a hot field or requires assignment from browse
  BRW6.AddField(DEL:DINo,BRW6.Q.DEL:DINo)                  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW6.AddField(DELI:ItemNo,BRW6.Q.DELI:ItemNo)            ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW6.AddField(DELI:Units,BRW6.Q.DELI:Units)              ! Field DELI:Units is a hot field or requires assignment from browse
  BRW6.AddField(DELI:Weight,BRW6.Q.DELI:Weight)            ! Field DELI:Weight is a hot field or requires assignment from browse
  BRW6.AddField(INI:Description,BRW6.Q.INI:Description)    ! Field INI:Description is a hot field or requires assignment from browse
  BRW6.AddField(INI:Type,BRW6.Q.INI:Type)                  ! Field INI:Type is a hot field or requires assignment from browse
  BRW6.AddField(INI:ContainerDescription,BRW6.Q.INI:ContainerDescription) ! Field INI:ContainerDescription is a hot field or requires assignment from browse
  BRW6.AddField(INI:IID,BRW6.Q.INI:IID)                    ! Field INI:IID is a hot field or requires assignment from browse
  BRW6.AddField(DELI:DIID,BRW6.Q.DELI:DIID)                ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW6.AddField(DEL:DID,BRW6.Q.DEL:DID)                    ! Field DEL:DID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Invoice',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1
  BRW4.AskProcedure = 2
  BRW6.AskProcedure = 3
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ClientsPaymentsAllocation.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Invoice',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Update_StatementItems
      UpdateClientsPaymentsAllocation
      Update_InvoiceItems
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_GetID
      ThisWindow.Update()
          IF GLO:ReplicatedDatabaseID = 0
             GLO:ReplicatedDatabaseID = INV:BID
          .
      
          ! (FileManager FM, Key PrimaryKey, *? RecordID)
          Rep_GetNextID(Access:_Invoice, INV:PKey_IID, My_ID#)
      
      
          INV:IID     = My_ID#
          INV:POD_IID = INV:IID
      
          DISPLAY
    OF ?INV:MIDs
          INV:MID     = INV:MIDs
          DISPLAY
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:5
    SELF.ChangeControl=?Change:5
    SELF.DeleteControl=?Delete:5
  END


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:7
    SELF.ChangeControl=?Change:7
    SELF.DeleteControl=?Delete:7
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

