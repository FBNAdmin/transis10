

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS011.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('DBUTRANS019.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Select a Add_Suburbs Record
!!! </summary>
SelectAdd_Suburbs PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Add_Suburbs)
                       PROJECT(SUBU:Suburb)
                       PROJECT(SUBU:PostalCode)
                       PROJECT(SUBU:SUID)
                       PROJECT(SUBU:CIID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
SUBU:Suburb            LIKE(SUBU:Suburb)              !List box control field - type derived from field
SUBU:PostalCode        LIKE(SUBU:PostalCode)          !List box control field - type derived from field
SUBU:SUID              LIKE(SUBU:SUID)                !Primary key field - type derived from field
SUBU:CIID              LIKE(SUBU:CIID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Add_Suburbs Record'),AT(,,158,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('SelectAdd_Suburbs'),SYSTEM
                       LIST,AT(8,30,142,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Suburb~L(2)@s50@48L(2)|M' & |
  '~Postal Code~L(2)@s10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Add_Suburbs file')
                       BUTTON('&Select'),AT(101,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,150,172),USE(?CurrentTab)
                         TAB('&1) By Suburb'),USE(?Tab:2)
                         END
                         TAB('&2) By City'),USE(?Tab:3)
                         END
                       END
                       BUTTON('&Close'),AT(52,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(105,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectAdd_Suburbs')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Add_Suburbs.SetOpenRelated()
  Relate:Add_Suburbs.Open                                  ! File Add_Suburbs used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Add_Suburbs,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,SUBU:FKey_CIID)                       ! Add the sort order for SUBU:FKey_CIID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,SUBU:CIID,1,BRW1)              ! Initialize the browse locator using  using key: SUBU:FKey_CIID , SUBU:CIID
  BRW1.AddSortOrder(,SUBU:Key_Suburb)                      ! Add the sort order for SUBU:Key_Suburb for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,SUBU:Suburb,1,BRW1)            ! Initialize the browse locator using  using key: SUBU:Key_Suburb , SUBU:Suburb
  BRW1.AddField(SUBU:Suburb,BRW1.Q.SUBU:Suburb)            ! Field SUBU:Suburb is a hot field or requires assignment from browse
  BRW1.AddField(SUBU:PostalCode,BRW1.Q.SUBU:PostalCode)    ! Field SUBU:PostalCode is a hot field or requires assignment from browse
  BRW1.AddField(SUBU:SUID,BRW1.Q.SUBU:SUID)                ! Field SUBU:SUID is a hot field or requires assignment from browse
  BRW1.AddField(SUBU:CIID,BRW1.Q.SUBU:CIID)                ! Field SUBU:CIID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('SelectAdd_Suburbs',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Suburbs.Close
  END
  IF SELF.Opened
    INIMgr.Update('SelectAdd_Suburbs',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Addresses PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW7::View:Browse    VIEW(Clients)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:CID)
                       PROJECT(CLI:AID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Primary key field - type derived from field
CLI:AID                LIKE(CLI:AID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::ADD:Record  LIKE(ADD:RECORD),THREAD
QuickWindow          WINDOW('Form Addresses'),AT(,,303,251),FONT('MS Sans Serif',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Update_Addresses'),SYSTEM
                       SHEET,AT(4,4,296,229),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           ENTRY(@n_10),AT(92,23,84,10),USE(ADD:AID),RIGHT(1),MSG('Address ID - note once used certain information should not be changeable, such as the suburb')
                           PROMPT('AID:'),AT(8,23),USE(?Prompt9)
                           PROMPT('BID:'),AT(8,36),USE(?ADD:BID:Prompt),TRN
                           STRING(@n_10),AT(92,36,84,10),USE(ADD:BID),RIGHT(1),TRN
                           PROMPT('Address Name:'),AT(8,50),USE(?ADD:AddressName:Prompt),TRN
                           ENTRY(@s35),AT(92,50,144,10),USE(ADD:AddressName),MSG('Name of this address'),REQ,TIP('Name of th' & |
  'is address')
                           PROMPT('Address Name Suburb:'),AT(8,64),USE(?ADD:AddressNameSuburb:Prompt),TRN
                           ENTRY(@s50),AT(92,64,204,10),USE(ADD:AddressNameSuburb),MSG('Address Name & Suburb'),TIP('Address Na' & |
  'me & Suburb')
                           PROMPT('Line 1:'),AT(8,78),USE(?ADD:Line1:Prompt),TRN
                           ENTRY(@s35),AT(92,78,144,10),USE(ADD:Line1),MSG('Address line 1'),TIP('Address line 1')
                           PROMPT('Line 2:'),AT(8,92),USE(?ADD:Line2:Prompt),TRN
                           ENTRY(@s35),AT(92,92,144,10),USE(ADD:Line2),MSG('Address line 2'),TIP('Address line 2')
                           PROMPT('Phone No:'),AT(8,106),USE(?ADD:PhoneNo:Prompt),TRN
                           ENTRY(@s20),AT(92,106,84,10),USE(ADD:PhoneNo),MSG('Phone no.'),TIP('Phone no.')
                           PROMPT('Phone No 2:'),AT(8,120),USE(?ADD:PhoneNo2:Prompt),TRN
                           ENTRY(@s20),AT(92,120,84,10),USE(ADD:PhoneNo2),MSG('Phone no. 2'),TIP('Phone no. 2')
                           PROMPT('Fax:'),AT(8,134),USE(?ADD:Fax:Prompt),TRN
                           ENTRY(@s20),AT(92,134,84,10),USE(ADD:Fax),MSG('Fax'),TIP('Fax')
                           CHECK('Show for all Branches'),AT(92,148,96,8),USE(ADD:ShowForAllBranches),MSG('Show this ' & |
  'address for all branches'),TIP('Show this address for all branches')
                           CHECK('Branch'),AT(92,160,70,8),USE(ADD:Branch),MSG('This is used for a branch'),TIP('This is us' & |
  'ed for a branch')
                           CHECK('Client'),AT(166,160,70,8),USE(ADD:Client),MSG('This is used for a client'),TIP('This is us' & |
  'ed for a client')
                           CHECK('Accountant'),AT(240,160,56,8),USE(ADD:Accountant),MSG('This is used for an accountant'), |
  TIP('This is used for an accountant')
                           CHECK('Delivery'),AT(92,180,70,8),USE(ADD:Delivery),MSG('This is used for a delivery'),TIP('This is us' & |
  'ed for a delivery')
                           CHECK('Container Turn In'),AT(166,180,80,8),USE(ADD:ContainerTurnIn),MSG('This is used ' & |
  'for a Container Turn In'),TIP('This is used for a Container Turn In')
                           CHECK('Transporter'),AT(92,192,70,8),USE(ADD:Transporter),MSG('This is used for a Transporter'), |
  TIP('This is used for a Transporter')
                           CHECK('Journey'),AT(166,192,70,8),USE(ADD:Journey),MSG('This is used for a Journey'),TIP('This is us' & |
  'ed for a Journey')
                         END
                         TAB('&2) Clients'),USE(?Tab2)
                           LIST,AT(8,20,287,193),USE(?List),VSCROLL,FORMAT('40R(2)|M~Client No.~L@n_10b@400L(2)|M~' & |
  'Client Name~@s100@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(169,216,42,12),USE(?Insert)
                           BUTTON('&Change'),AT(211,216,42,12),USE(?Change)
                           BUTTON('&Delete'),AT(253,216,42,12),USE(?Delete)
                         END
                       END
                       BUTTON('&OK'),AT(145,236,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(198,236,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(251,236,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW7                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW7::Sort0:Locator  StepLocatorClass                      ! Default Locator
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Fuel Surcharge Record'
  OF InsertRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Addresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ADD:AID
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Addresses)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(ADD:Record,History::ADD:Record)
  SELF.AddHistoryField(?ADD:AID,1)
  SELF.AddHistoryField(?ADD:BID,2)
  SELF.AddHistoryField(?ADD:AddressName,3)
  SELF.AddHistoryField(?ADD:AddressNameSuburb,4)
  SELF.AddHistoryField(?ADD:Line1,5)
  SELF.AddHistoryField(?ADD:Line2,6)
  SELF.AddHistoryField(?ADD:PhoneNo,8)
  SELF.AddHistoryField(?ADD:PhoneNo2,9)
  SELF.AddHistoryField(?ADD:Fax,10)
  SELF.AddHistoryField(?ADD:ShowForAllBranches,11)
  SELF.AddHistoryField(?ADD:Branch,13)
  SELF.AddHistoryField(?ADD:Client,14)
  SELF.AddHistoryField(?ADD:Accountant,15)
  SELF.AddHistoryField(?ADD:Delivery,16)
  SELF.AddHistoryField(?ADD:ContainerTurnIn,17)
  SELF.AddHistoryField(?ADD:Transporter,18)
  SELF.AddHistoryField(?ADD:Journey,19)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Addresses
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:Clients,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?ADD:AID{PROP:ReadOnly} = True
    ?ADD:AddressName{PROP:ReadOnly} = True
    ?ADD:AddressNameSuburb{PROP:ReadOnly} = True
    ?ADD:Line1{PROP:ReadOnly} = True
    ?ADD:Line2{PROP:ReadOnly} = True
    ?ADD:PhoneNo{PROP:ReadOnly} = True
    ?ADD:PhoneNo2{PROP:ReadOnly} = True
    ?ADD:Fax{PROP:ReadOnly} = True
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,CLI:FKey_AID)                         ! Add the sort order for CLI:FKey_AID for sort order 1
  BRW7.AddRange(CLI:AID,ADD:AID)                           ! Add single value range limit for sort order 1
  BRW7.AddLocator(BRW7::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW7::Sort0:Locator.Init(,CLI:AID,1,BRW7)                ! Initialize the browse locator using  using key: CLI:FKey_AID , CLI:AID
  BRW7.AppendOrder('+CLI:ClientName')                      ! Append an additional sort order
  BRW7.AddField(CLI:ClientNo,BRW7.Q.CLI:ClientNo)          ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW7.AddField(CLI:ClientName,BRW7.Q.CLI:ClientName)      ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW7.AddField(CLI:CID,BRW7.Q.CLI:CID)                    ! Field CLI:CID is a hot field or requires assignment from browse
  BRW7.AddField(CLI:AID,BRW7.Q.CLI:AID)                    ! Field CLI:AID is a hot field or requires assignment from browse
  INIMgr.Fetch('Update_Addresses',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW7.AskProcedure = 1
  BRW7.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW7.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Addresses',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW7.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END

