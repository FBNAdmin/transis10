

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('DBUTRANS003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS007.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Splash
!!! </summary>
SplashWindow PROCEDURE 

Window               WINDOW,AT(,,212,89),FONT('MS Sans Serif',8,,FONT:regular),TILED,CENTER,ICON(ICON:Application), |
  HLP('~SplashWindow'),PALETTE(256),SYSTEM,WALLPAPER('wizBck05.jpg'),IMM
                       BOX,AT(8,0,197,42),USE(?BoxTop),FILL(COLOR:ACTIVECAPTION),LINEWIDTH(1)
                       BOX,AT(8,0,51,15),USE(?BoxTopInternal),FILL(COLOR:WINDOWTEXT),LINEWIDTH(1)
                       STRING('InfoSolutions'),AT(12,2,,11),USE(?SplashHeadingSmall),FONT('Arial',,COLOR:White),TRN
                       STRING('FBN Transport'),AT(40,15,,27),USE(?SplashHeadingTop),FONT('Arial',26,COLOR:CAPTIONTEXT, |
  FONT:bold),TRN
                       STRING('TransIS'),AT(89,42,123,27),USE(?SplashHeadingBottom),FONT('Arial',26,COLOR:ACTIVECAPTION, |
  FONT:bold),TRN
                       STRING('Copyright 2004 - 2014'),AT(102,69,110,10),USE(?CopyrightDate),FONT(,,COLOR:ACTIVECAPTION), |
  TRN
                       STRING('by InfoSolutions'),AT(102,79,110,10),USE(?Developer),FONT(,,COLOR:ACTIVECAPTION),TRN
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SplashWindow')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?BoxTop
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  TARGET{Prop:Timer} = 200                                 ! Close window on timer event, so configure timer
  TARGET{Prop:Alrt,255} = MouseLeft                        ! Alert mouse clicks that will close window
  TARGET{Prop:Alrt,254} = MouseLeft2
  TARGET{Prop:Alrt,253} = MouseRight
  	?CopyrightDate{PROP:Text}	= 'Copyright 2004 - ' & YEAR(TODAY())
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      CASE KEYCODE()
      OF MouseLeft
      OROF MouseLeft2
      OROF MouseRight
        POST(Event:CloseWindow)                            ! Splash window will close on mouse click
      END
    OF EVENT:LoseFocus
        POST(Event:CloseWindow)                            ! Splash window will close when focus is lost
    OF Event:Timer
      POST(Event:CloseWindow)                              ! Splash window will close on event timer
    OF Event:AlertKey
      CASE KEYCODE()                                       ! Splash window will close on mouse click
      OF MouseLeft
      OROF MouseLeft2
      OROF MouseRight
        POST(Event:CloseWindow)
      END
    ELSE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Set_Connection PROCEDURE 

LOC:Connection_Group GROUP,PRE(L_CG)                       !
Database             STRING(100)                           !Database
User                 STRING(100)                           !User
Password             STRING(100)                           !
Server               STRING(100)                           !
DBOwner              STRING(255)                           !
Trusted_Connection   BYTE                                  !Use Trusted Connection
                     END                                   !
LOC:Test_Database    BYTE                                  !This is a test database - ONLY set this to true in the case of a TEST database
Window               WINDOW('TransIS - Set Database Connection Info.'),AT(,,310,243),FONT('MS Sans Serif',8,,FONT:regular), |
  DOUBLE,GRAY
                       SHEET,AT(4,4,301,214),USE(?Sheet1)
                         TAB('Connection'),USE(?Tab1)
                           PROMPT('Database:'),AT(10,21),USE(?Database:Prompt)
                           ENTRY(@s100),AT(67,21,126,10),USE(L_CG:Database),MSG('Database'),TIP('Database')
                           PROMPT('User:'),AT(10,36),USE(?User:Prompt)
                           ENTRY(@s100),AT(67,36,126,10),USE(L_CG:User),MSG('User'),TIP('User')
                           PROMPT('Password:'),AT(10,50),USE(?Password:Prompt)
                           ENTRY(@s100),AT(67,50,126,10),USE(L_CG:Password)
                           PROMPT('Server:'),AT(10,64),USE(?Server:Prompt)
                           ENTRY(@s100),AT(67,64,126,10),USE(L_CG:Server)
                           BUTTON('&Build'),AT(67,78,48,14),USE(?Button_Build)
                           CHECK(' &Trusted Connection'),AT(67,168),USE(L_CG:Trusted_Connection),MSG('Use Trusted ' & |
  'Connection'),TIP('Use Trusted Connection')
                           LINE,AT(67,190,233,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           CHECK(' Test &Database'),AT(67,204),USE(LOC:Test_Database),MSG('This is a test database' & |
  ' - ONLY set this to true in the case of a TEST database'),TIP('This is a test databa' & |
  'se - ONLY set this to true in the case of a TEST database')
                           PROMPT('DB Owner:'),AT(10,100),USE(?L_CG:DBOwner:Prompt)
                           TEXT,AT(67,100,231,62),USE(L_CG:DBOwner),VSCROLL,BOXED
                         END
                       END
                       BUTTON('&OK'),AT(196,226,,14),USE(?OkButton),LEFT,ICON('waok.ico'),DEFAULT,FLAT
                       BUTTON('&Cancel'),AT(252,226,,14),USE(?CancelButton),LEFT,ICON('wacancel.ico'),FLAT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Set_Connection')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Database:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Set_Connection',Window)                    ! Restore window settings from non-volatile store
      L_CG:DBOwner    = GETINI('Setup', 'DBOwner', 'TransIS;UID=sa;PWD=sa;DATABASE=TransIS', '.\TransIS.INI')
  
      L_CG:Server     = Get_1st_Element_From_Delim_Str(L_CG:DBOwner, ';', TRUE)
  
      L_CG:User       = Get_1st_Element_From_Delim_Str(L_CG:DBOwner, ';', TRUE)
      L_CG:User       = LEFT(SUB(L_CG:User, 5, LEN(CLIP(L_CG:User))))
  
      L_CG:Password   = Get_1st_Element_From_Delim_Str(L_CG:DBOwner, ';', TRUE)
      L_CG:Password   = LEFT(SUB(L_CG:Password, 5, LEN(CLIP(L_CG:Password))))
  
      L_CG:Database   = Get_1st_Element_From_Delim_Str(L_CG:DBOwner, ';', TRUE)
      L_CG:Database   = LEFT(SUB(L_CG:Database, 10, LEN(CLIP(L_CG:Database))))
  
  
      L_CG:DBOwner    = GETINI('Setup', 'DBOwner', 'TransIS;UID=sa;PWD=sa;DATABASE=TransIS', '.\TransIS.INI')
  
      L_CG:Trusted_Connection = GETINI('Setup', 'Trusted_Connection', '1', '.\TransIS.INI')
  
  
  
      LOC:Test_Database   = GETINI('Setup', 'Test_Database', 0, '.\TransIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('Set_Connection',Window)                 ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Build
      ThisWindow.Update()
          L_CG:DBOwner        = CLIP(L_CG:Server) & ';UID=' & CLIP(L_CG:User) & ';PWD=' & CLIP(L_CG:Password) & ';DATABASE=' & CLIP(L_CG:Database)
          DISPLAY(?L_CG:DBOwner)
    OF ?OkButton
      ThisWindow.Update()
          IF LOC:Test_Database = TRUE
             CASE MESSAGE('You have checked the Test Database option.||If the specified database is not for TEST ONLY purposes please un-check this.||Are you sure?', 'Confirm Test Database', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:No
                Window{PROP:AcceptAll}   = 0
                CYCLE
          .  .
      
          PUTINI('Setup', 'Test_Database', LOC:Test_Database, '.\TransIS.INI')
      
          PUTINI('Setup', 'DBOwner', CLIP(L_CG:DBOwner), '.\TransIS.INI')
          PUTINI('Setup', 'Trusted_Connection', L_CG:Trusted_Connection, '.\TransIS.INI')
      
      
       POST(EVENT:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Manifest PROCEDURE 

CurrentTab           STRING(80)                            !
Locate               ULONG                                 !
BRW1::View:Browse    VIEW(Manifest)
                       PROJECT(MAN:MID)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:TID)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:Rate)
                       PROJECT(MAN:VATRate)
                       PROJECT(MAN:State)
                       PROJECT(MAN:CreatedDate)
                       PROJECT(MAN:CreatedTime)
                       PROJECT(MAN:VCID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
MAN:MID                LIKE(MAN:MID)                  !List box control field - type derived from field
MAN:BID                LIKE(MAN:BID)                  !List box control field - type derived from field
MAN:TID                LIKE(MAN:TID)                  !List box control field - type derived from field
MAN:Cost               LIKE(MAN:Cost)                 !List box control field - type derived from field
MAN:Rate               LIKE(MAN:Rate)                 !List box control field - type derived from field
MAN:VATRate            LIKE(MAN:VATRate)              !List box control field - type derived from field
MAN:State              LIKE(MAN:State)                !List box control field - type derived from field
MAN:CreatedDate        LIKE(MAN:CreatedDate)          !List box control field - type derived from field
MAN:CreatedTime        LIKE(MAN:CreatedTime)          !List box control field - type derived from field
MAN:VCID               LIKE(MAN:VCID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Manifest file'),AT(,,358,198),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Browse_Manifest'),SYSTEM
                       LIST,AT(8,34,342,120),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~MID~C(0)@n_10@40R(2)|M~B' & |
  'ID~C(0)@n_10@40R(2)|M~TID~C(0)@n_10@64R(1)|M~Cost~C(0)@n-14.2@60R(1)|M~Rate~C(0)@n-1' & |
  '3.4@36R(1)|M~VAT Rate~C(0)@n-7.2@24R(2)|M~State~C(0)@n3@40R(2)|M~Created Date~C(0)@d' & |
  '6@40R(2)|M~Created Time~C(0)@t7@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Manifest file')
                       BUTTON('&Select'),AT(89,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('by MID'),USE(?Tab4)
                         END
                         TAB('&1) By Branch'),USE(?Tab:2)
                           BUTTON('Select Branches'),AT(8,158,118,14),USE(?SelectBranches),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&2) By Transporter'),USE(?Tab:3)
                           BUTTON('Select Transporter'),AT(8,158,118,14),USE(?SelectTransporter),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&3) By Vehicle Composition'),USE(?Tab:4)
                           BUTTON('Select VehicleComposition'),AT(8,158,118,14),USE(?SelectVehicleComposition),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(252,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       PROMPT('Locate:'),AT(8,20),USE(?Locate:Prompt)
                       ENTRY(@n_10),AT(36,20,60,10),USE(Locate),RIGHT(1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort3:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Manifest')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:AddressAlias.SetOpenRelated()
  Relate:AddressAlias.Open                                 ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Branches.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleComposition.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Manifest,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,MAN:FKey_BID)                         ! Add the sort order for MAN:FKey_BID for sort order 1
  BRW1.AddRange(MAN:BID,Relate:Manifest,Relate:Branches)   ! Add file relationship range limit for sort order 1
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort3:Locator.Init(?Locate,MAN:BID,1,BRW1)         ! Initialize the browse locator using ?Locate using key: MAN:FKey_BID , MAN:BID
  BRW1.AppendOrder('+MAN:MID')                             ! Append an additional sort order
  BRW1.AddSortOrder(,MAN:FKey_TID)                         ! Add the sort order for MAN:FKey_TID for sort order 2
  BRW1.AddRange(MAN:TID,Relate:Manifest,Relate:Transporter) ! Add file relationship range limit for sort order 2
  BRW1.AppendOrder('+MAN:MID')                             ! Append an additional sort order
  BRW1.AddSortOrder(,MAN:FKey_VCID)                        ! Add the sort order for MAN:FKey_VCID for sort order 3
  BRW1.AddRange(MAN:VCID,Relate:Manifest,Relate:VehicleComposition) ! Add file relationship range limit for sort order 3
  BRW1.AppendOrder('+MAN:MID')                             ! Append an additional sort order
  BRW1.AddSortOrder(,MAN:PKey_MID)                         ! Add the sort order for MAN:PKey_MID for sort order 4
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 4
  BRW1::Sort0:Locator.Init(?Locate,MAN:MID,1,BRW1)         ! Initialize the browse locator using ?Locate using key: MAN:PKey_MID , MAN:MID
  BRW1.AddField(MAN:MID,BRW1.Q.MAN:MID)                    ! Field MAN:MID is a hot field or requires assignment from browse
  BRW1.AddField(MAN:BID,BRW1.Q.MAN:BID)                    ! Field MAN:BID is a hot field or requires assignment from browse
  BRW1.AddField(MAN:TID,BRW1.Q.MAN:TID)                    ! Field MAN:TID is a hot field or requires assignment from browse
  BRW1.AddField(MAN:Cost,BRW1.Q.MAN:Cost)                  ! Field MAN:Cost is a hot field or requires assignment from browse
  BRW1.AddField(MAN:Rate,BRW1.Q.MAN:Rate)                  ! Field MAN:Rate is a hot field or requires assignment from browse
  BRW1.AddField(MAN:VATRate,BRW1.Q.MAN:VATRate)            ! Field MAN:VATRate is a hot field or requires assignment from browse
  BRW1.AddField(MAN:State,BRW1.Q.MAN:State)                ! Field MAN:State is a hot field or requires assignment from browse
  BRW1.AddField(MAN:CreatedDate,BRW1.Q.MAN:CreatedDate)    ! Field MAN:CreatedDate is a hot field or requires assignment from browse
  BRW1.AddField(MAN:CreatedTime,BRW1.Q.MAN:CreatedTime)    ! Field MAN:CreatedTime is a hot field or requires assignment from browse
  BRW1.AddField(MAN:VCID,BRW1.Q.MAN:VCID)                  ! Field MAN:VCID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Manifest',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Manifest',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Manifest
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectBranches()
      ThisWindow.Reset
    OF ?SelectTransporter
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectTransporter()
      ThisWindow.Reset
    OF ?SelectVehicleComposition
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectVehicleComposition()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_ManifestLoads PROCEDURE 

CurrentTab           STRING(80)                            !
Locate               ULONG                                 !
BRW1::View:Browse    VIEW(ManifestLoad)
                       PROJECT(MAL:MLID)
                       PROJECT(MAL:MID)
                       PROJECT(MAL:TTID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
MAL:MLID               LIKE(MAL:MLID)                 !List box control field - type derived from field
MAL:MID                LIKE(MAL:MID)                  !List box control field - type derived from field
MAL:TTID               LIKE(MAL:TTID)                 !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Manifest Load file'),AT(,,330,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_ManifestLoads'),SYSTEM
                       LIST,AT(8,34,314,120),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~MLID~C(0)@n_10@40R(2)|M~' & |
  'MID~C(0)@n_10@40R(2)|M~TTID~C(0)@n_10@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the ' & |
  'ManifestLoad file')
                       BUTTON('&Select'),AT(61,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(114,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(167,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(220,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(273,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       PROMPT('Locate:'),AT(9,20),USE(?Locate:Prompt)
                       ENTRY(@n_10),AT(40,20,60,10),USE(Locate),RIGHT(1)
                       SHEET,AT(4,4,322,172),USE(?CurrentTab)
                         TAB('&1) By Manifest'),USE(?Tab:2)
                         END
                         TAB('&2) By Truck or Trailer'),USE(?Tab:3)
                           BUTTON('Select TruckTrailer'),AT(8,158,49,14),USE(?SelectTruckTrailer),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&3) By Manifest && Truck /  Trailer'),USE(?Tab:4)
                         END
                         TAB('by MLID'),USE(?Tab4)
                         END
                       END
                       BUTTON('&Close'),AT(224,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(277,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 4
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_ManifestLoads')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:ManifestLoad.SetOpenRelated()
  Relate:ManifestLoad.Open                                 ! File ManifestLoad used by this procedure, so make sure it's RelationManager is open
  Access:TruckTrailer.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ManifestLoad,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,MAL:FKey_TTID)                        ! Add the sort order for MAL:FKey_TTID for sort order 1
  BRW1.AddRange(MAL:TTID,Relate:ManifestLoad,Relate:TruckTrailer) ! Add file relationship range limit for sort order 1
  BRW1.AddSortOrder(,MAL:SKey_MID_TTID)                    ! Add the sort order for MAL:SKey_MID_TTID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,MAL:MID,1,BRW1)                ! Initialize the browse locator using  using key: MAL:SKey_MID_TTID , MAL:MID
  BRW1.AddSortOrder(,MAL:PKey_MLID)                        ! Add the sort order for MAL:PKey_MLID for sort order 3
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort3:Locator.Init(?Locate,MAL:MLID,1,BRW1)        ! Initialize the browse locator using ?Locate using key: MAL:PKey_MLID , MAL:MLID
  BRW1.AddSortOrder(,MAL:FKey_MID)                         ! Add the sort order for MAL:FKey_MID for sort order 4
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 4
  BRW1::Sort0:Locator.Init(?Locate,MAL:MID,1,BRW1)         ! Initialize the browse locator using ?Locate using key: MAL:FKey_MID , MAL:MID
  BRW1.AppendOrder('+MAL:MLID')                            ! Append an additional sort order
  BRW1.AddField(MAL:MLID,BRW1.Q.MAL:MLID)                  ! Field MAL:MLID is a hot field or requires assignment from browse
  BRW1.AddField(MAL:MID,BRW1.Q.MAL:MID)                    ! Field MAL:MID is a hot field or requires assignment from browse
  BRW1.AddField(MAL:TTID,BRW1.Q.MAL:TTID)                  ! Field MAL:TTID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_ManifestLoads',QuickWindow)         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ManifestLoad.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_ManifestLoads',QuickWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_ManifestLoad
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectTruckTrailer
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectTruckTrailer()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_ManidfestLoadItems PROCEDURE 

CurrentTab           STRING(80)                            !
Locate               ULONG                                 !
BRW1::View:Browse    VIEW(ManifestLoadDeliveries)
                       PROJECT(MALD:MLDID)
                       PROJECT(MALD:MLID)
                       PROJECT(MALD:DIID)
                       PROJECT(MALD:UnitsLoaded)
                       JOIN(MAL:PKey_MLID,MALD:MLID)
                         PROJECT(MAL:MLID)
                         PROJECT(MAL:MID)
                         JOIN(MAN:PKey_MID,MAL:MID)
                           PROJECT(MAN:MID)
                         END
                       END
                       JOIN(DELI:PKey_DIID,MALD:DIID)
                         PROJECT(DELI:ItemNo)
                         PROJECT(DELI:Units)
                         PROJECT(DELI:DIID)
                         PROJECT(DELI:DID)
                         JOIN(DEL:PKey_DID,DELI:DID)
                           PROJECT(DEL:DID)
                           PROJECT(DEL:DINo)
                         END
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
MALD:MLDID             LIKE(MALD:MLDID)               !List box control field - type derived from field
MALD:MLID              LIKE(MALD:MLID)                !List box control field - type derived from field
MALD:DIID              LIKE(MALD:DIID)                !List box control field - type derived from field
MALD:UnitsLoaded       LIKE(MALD:UnitsLoaded)         !List box control field - type derived from field
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Units             LIKE(DELI:Units)               !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !List box control field - type derived from field
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
MAN:MID                LIKE(MAN:MID)                  !List box control field - type derived from field
MAL:MLID               LIKE(MAL:MLID)                 !Related join file key field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Manifest Load Deliveries'),AT(,,330,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_ManifestLoads'),SYSTEM
                       LIST,AT(8,34,314,120),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~MLDID~C(0)@n_10@40R(2)|M' & |
  '~MLID~C(0)@n_10@40R(2)|M~DIID~C(0)@n_10@46R(2)|M~Units Loaded~C(0)@n6@[28R(2)|M~Item' & |
  ' No.~C(0)@n_5@24R(2)|M~Units~C(0)@n6@40R(2)|M~DID~C(0)@n_10@40R(2)|M~DI No.~C(0)@n_1' & |
  '0@40R(2)|M~MID~C(0)@n_10@]|M~DI Item, DI, Manifest~'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he ManifestLoadDeliveries file')
                       BUTTON('&Select'),AT(61,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(114,158,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(167,158,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(220,158,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(273,158,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       PROMPT('Locate:'),AT(7,20),USE(?Locate:Prompt)
                       ENTRY(@n_10),AT(36,20,60,10),USE(Locate),RIGHT(1)
                       SHEET,AT(4,4,322,172),USE(?CurrentTab)
                         TAB('&1) By Manifest Load && Delivery Item'),USE(?Tab:2)
                         END
                         TAB('&2) By Delivery Items ID'),USE(?Tab:3)
                           BUTTON('DeliveryI...'),AT(8,158,16,14),USE(?SelectDeliveryItems),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('by MLDID'),USE(?Tab3)
                         END
                       END
                       BUTTON('&Close'),AT(224,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(277,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_ManidfestLoadItems')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:DeliveryItemAlias.SetOpenRelated()
  Relate:DeliveryItemAlias.Open                            ! File DeliveryItemAlias used by this procedure, so make sure it's RelationManager is open
  Access:DeliveryItemAlias2.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ManifestLoadDeliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,MALD:FKey_DIID)                       ! Add the sort order for MALD:FKey_DIID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(?Locate,MALD:DIID,1,BRW1)       ! Initialize the browse locator using ?Locate using key: MALD:FKey_DIID , MALD:DIID
  BRW1.AddSortOrder(,MALD:PKey_MLDID)                      ! Add the sort order for MALD:PKey_MLDID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(?Locate,MALD:MLDID,1,BRW1)      ! Initialize the browse locator using ?Locate using key: MALD:PKey_MLDID , MALD:MLDID
  BRW1.AddSortOrder(,MALD:FSKey_MLID_DIID)                 ! Add the sort order for MALD:FSKey_MLID_DIID for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(?Locate,MALD:MLID,1,BRW1)       ! Initialize the browse locator using ?Locate using key: MALD:FSKey_MLID_DIID , MALD:MLID
  BRW1.AppendOrder('+MALD:MLDID')                          ! Append an additional sort order
  BRW1.AddField(MALD:MLDID,BRW1.Q.MALD:MLDID)              ! Field MALD:MLDID is a hot field or requires assignment from browse
  BRW1.AddField(MALD:MLID,BRW1.Q.MALD:MLID)                ! Field MALD:MLID is a hot field or requires assignment from browse
  BRW1.AddField(MALD:DIID,BRW1.Q.MALD:DIID)                ! Field MALD:DIID is a hot field or requires assignment from browse
  BRW1.AddField(MALD:UnitsLoaded,BRW1.Q.MALD:UnitsLoaded)  ! Field MALD:UnitsLoaded is a hot field or requires assignment from browse
  BRW1.AddField(DELI:ItemNo,BRW1.Q.DELI:ItemNo)            ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW1.AddField(DELI:Units,BRW1.Q.DELI:Units)              ! Field DELI:Units is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DID,BRW1.Q.DEL:DID)                    ! Field DEL:DID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DINo,BRW1.Q.DEL:DINo)                  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW1.AddField(MAN:MID,BRW1.Q.MAN:MID)                    ! Field MAN:MID is a hot field or requires assignment from browse
  BRW1.AddField(MAL:MLID,BRW1.Q.MAL:MLID)                  ! Field MAL:MLID is a hot field or requires assignment from browse
  BRW1.AddField(DELI:DIID,BRW1.Q.DELI:DIID)                ! Field DELI:DIID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_ManidfestLoadItems',QuickWindow)    ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryItemAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_ManidfestLoadItems',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Manifest_Load_Deliveries
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectDeliveryItems
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_DeliveryItems()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

