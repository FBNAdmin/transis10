

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS016.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Procedure not yet defined
!!! </summary>
SelectClients PROCEDURE !Procedure not yet defined
  CODE
  GlobalErrors.ThrowMessage(Msg:ProcedureToDo,'SelectClients') ! This procedure acts as a place holder for a procedure yet to be defined
  SETKEYCODE(0)
  GlobalResponse = RequestCancelled                        ! Request cancelled is the implied action
!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
UserLogin PROCEDURE 

LOC:Group            GROUP,PRE(L_G)                        !
Login                STRING(20)                            !User Login
Password             STRING(35)                            !User Password
Remember_Login       BYTE                                  !Remember my login on this computer
                     END                                   !
QuickWindow          WINDOW('Login'),AT(,,260,120),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER,GRAY,IMM, |
  MODAL,HLP('Login')
                       SHEET,AT(4,4,251,97),USE(?Sheet1)
                         TAB('User Login'),USE(?Tab1)
                           PROMPT('Login:'),AT(10,28),USE(?Login:Prompt),FONT(,10,,,CHARSET:ANSI)
                           ENTRY(@s20),AT(70,28),USE(L_G:Login),FONT(,10,,,CHARSET:ANSI),MSG('User Login'),REQ,TIP('User Login')
                           PROMPT('Password:'),AT(10,48,,12),USE(?Password:Prompt),FONT(,10,,,CHARSET:ANSI)
                           ENTRY(@s35),AT(70,48),USE(L_G:Password),FONT(,10,,,CHARSET:ANSI),MSG('User Password'),PASSWORD, |
  TIP('User Password')
                           CHECK(' Remember Login'),AT(70,78),USE(L_G:Remember_Login),FONT(,10,,,CHARSET:ANSI),HIDE,MSG('Remember m' & |
  'y login on this computer'),TIP('Remember my login on this computer')
                         END
                       END
                       BUTTON('&Cancel'),AT(206,104,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(4,104,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('&OK'),AT(154,104,49,14),USE(?Ok:2),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UserLogin')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Login:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  !    Message('User: ' & Users{PROP:Owner})
      
  Relate:Control.Open                                      ! File Control used by this procedure, so make sure it's RelationManager is open
  Relate:Users.SetOpenRelated()
  Relate:Users.Open                                        ! File Users used by this procedure, so make sure it's RelationManager is open
  Access:UsersGroups.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
      L_G:Login           = GETINI('Login', 'LastLogin', '', '.\TransIS.INI')
      L_G:Remember_Login  = GETINI('Login', 'RememberPassword_New', 0, '.\TransIS.INI')
  
      IF L_G:Remember_Login > 0
         SET(Control)
         NEXT(Control)
         IF ~ERRORCODE()
            L_G:Password  = CTR:RememberPassword
      .  .
  
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Control.Close
    Relate:Users.Close
  END
      POST(EVENT:User,, 1)
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Cancel
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    OF ?Ok:2
      ThisWindow.Update()
          ! Check password
          IF CLIP(L_G:Login) = ''
             MESSAGE('Please supply a login.', 'Login', ICON:Exclamation)
          ELSE
             CLEAR(USE:Record)
             USE:Login        = L_G:Login
             IF Access:Users.TryFetch(USE:Key_Login) ~= LEVEL:Benign
                MESSAGE('Unable to load user. (1)', 'Login', ICON:Exclamation)
             ELSE
                IF CLIP(USE:Password) = CLIP(L_G:Password)
                   IF USE:AccessLevel < 10
                      Add_Audit(10, GlobalErrors.GetProcedureName(), 'Login for user with no authority (DBU): ' & CLIP(L_G:Login), 'No Authority.', 10)
                      MESSAGE('You are not authorised to login here.','Login',ICON:Hand)
      
                      POST(EVENT:CloseWindow)
                   ELSE
                      ! Ok login the user
                      PUTINI('Login', 'LastLogin', L_G:Login, '.\TransIS.INI')
                      PUTINI('Login', 'RememberPassword_New', L_G:Remember_Login, '.\TransIS.INI')
      
                      SET(CTR:PKey_CID)
                      NEXT(Control)
                      IF ~ERRORCODE()
                         IF L_G:Remember_Login = TRUE
                            CTR:RememberPassword = L_G:Password
                         ELSE
                            CTR:RememberPassword = ''
                         .
                         PUT(Control)
                      ELSE
                         IF L_G:Remember_Login = TRUE
                            CTR:RememberPassword = L_G:Password
                            ADD(Control)
                      .  .
      
                      Add_Audit(10, GlobalErrors.GetProcedureName(), 'User logged in (DBU): ' & CLIP(L_G:Login), 'Password correct.', 10)
      
                      GLO:UID            = USE:UID
                      GLO:Login          = USE:Login
                      GLO:AccessLevel    = USE:AccessLevel
                      GLO:LoggedInDate   = TODAY()
                      GLO:LoggedInTime   = CLOCK()
      
                      USEBG:UID          = USE:UID
                      SET(USEBG:Key_UID, USEBG:Key_UID)
                      LOOP
                         IF Access:UsersGroups.TryNext() ~= LEVEL:Benign
                            BREAK
                         .
                         IF USEBG:UID ~= USE:UID
                            BREAK
                         .
      
                         IF CLIP(GLO:UGIDs) = ''
                            GLO:UGIDs    = USEBG:UGID
                         ELSE
                            GLO:UGIDs    = CLIP(GLO:UGIDs) & ',' & USEBG:UGID
                      .  .
      
                      POST(EVENT:CloseWindow)
      
                      ! Check password is sufficient
                      IF LEN(CLIP(L_G:Password)) < 3
                         MESSAGE('The new password requirements mean that you need to change your password.||Please do so on the User screen.', 'User Login', ICON:Exclamation)
                         !START(Update_User_h,,CLIP(GLO:UID))
                   .  .
                ELSE
                   ! Store this in audit
                   Add_Audit(10, GlobalErrors.GetProcedureName(), 'Bad login for user (DBU): ' & CLIP(L_G:Login), 'Password failed.', 10)
      
                   MESSAGE('The Login and Password do not match.', 'Login', ICON:Hand)
                   SELECT(?L_G:Password)
          .  .  .
      
          CYCLE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          IF CLIP(L_G:Login) ~= ''
             SELECT(?L_G:Password)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Add_Log_h            PROCEDURE  (p1,p2,p3)                 ! Declare Procedure

  CODE
    Add_Log(p1,p2,p3)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)  -  Global
!!! </summary>
Add_Log              PROCEDURE  (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt) ! Declare Procedure
LOC:Date             LONG,STATIC                           !
LOC:Name             STRING(255)                           !
LOC:No               LONG                                  !
LOC:Extension        STRING(4)                             !
LOC:Name_Static      STRING('TrIS_Def {247}'),STATIC       !
LOC:Debug            BYTE                                  !On or Off`
Log_File        FILE,DRIVER('ASCII'),CREATE,PRE(LF),THREAD
Rec         RECORD
Text            STRING(2000)
            .   .

  CODE
    ! (p:Text, p:Heading, p:Name  , p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt)
    ! (STRING, STRING   , <STRING>, BYTE=0     , BYTE=0    , BYTE=1    , BYTE=1         )
    ! p:Name_Opt
    !   0.  - None
    !   1.  - Set static name to p:Name
    !   2.  - Use static name
    ! p:Rollover
    !   0.  - No rollover of file allowed
    !   1.  - Rollover allowed (default)
    ! p:Date_Time_Opt
    !   0.  - Off
    !   1.  - On (default)

    LOC:Debug   = 0

    IF OMITTED(3) OR CLIP(p:Name) = ''
       LOC:Name         = 'TransIS'
    ELSE
       LOC:Name         = CLIP(p:Name)
    .

    EXECUTE p:Name_Opt
       LOC:Name_Static  = LOC:Name                  ! p:Name - cant use this as it may be blank, Name will contain it
       LOC:Name         = LOC:Name_Static           ! LOC:Name_Static could be empty, we will default it
    .

    LOC:Extension       = '.log'

    len_#       = LEN(CLIP(LOC:Name))
    dot_pos_#   = INSTRING('.', LOC:Name, -1, len_#)
    IF len_# - dot_pos_# <= 3
       ! Extension is specified!
       LOC:Extension        = SUB(LOC:Name, dot_pos_#, 4)
       LOC:Name             = SUB(LOC:Name, 1, dot_pos_# - 1)
    .

    Log_File{PROP:Name}     = CLIP(LOC:Name) & LOC:Extension
    IF EXISTS(CLIP(Log_File{PROP:Name})) <= 0 OR p:Overwrite = TRUE
       LOC:Date             = 0
       CREATE(Log_File)
       IF ERRORCODE()
          IF OMITTED(3) OR CLIP(p:Name) = ''
             MESSAGE('Failed to create file.||File: ' & CLIP(Log_File{PROP:Name}) & '||Error (' & ERRORCODE() & '): ' & ERROR(), 'Add_Log', ICON:Hand)
          ELSE
             MESSAGE('Failed to create file.||File: ' & CLIP(Log_File{PROP:Name}) & '|(p:Name was: ' & CLIP(p:Name) & ')||Error: ' & ERROR(), 'Add_Log', ICON:Hand)
    .  .  .

    LOOP 10 TIMES
       LOC:No   += 1

       SHARE(Log_File)
       IF ERRORCODE()
          IF LOC:No = 1
             ! What now? Try new name
             Log_File{PROP:Name}    = CLIP(LOC:Name) & '-' & LOC:No & LOC:Extension

             IF EXISTS(CLIP(Log_File{PROP:Name})) <= 0
                LOC:Date            = 0
                CREATE(Log_File)
                IF ERRORCODE()
                   MESSAGE('Failed to create file.||File: ' & CLIP(Log_File{PROP:Name}) & '||Error: ' & ERROR(), 'Add_Log', ICON:Hand)
             .  .
             CYCLE
          ELSE
             MESSAGE('Failed to create / open log file.||Error: ' & ERROR() & '||File: ' & CLIP(Log_File{PROP:Name}), 'Log File Error', ICON:Hand)
             BREAK
       .  .

       IF p:Rollover = TRUE AND BYTES(Log_File) > 250000
          LOC:Date              = 0

          ! Create a new file: -1 -2 etc
          CLOSE(Log_File)

          Log_File{PROP:Name}   = CLIP(LOC:Name) & '-' & LOC:No & LOC:Extension

          IF EXISTS(CLIP(Log_File{PROP:Name})) <= 0
             LOC:Date           = 0
             CREATE(Log_File)
             IF ERRORCODE()
                MESSAGE('Failed to create next file.||File: ' & CLIP(Log_File{PROP:Name}) & '||Error: ' & ERROR(), 'Add_Log', ICON:Hand)
          .  .
          CYCLE
       .

       ! If we get here we have an open file less than 250000 bytes
       IF p:Date_Time_Opt = TRUE
          IF LOC:Date ~= TODAY()
             LOC:Date      = TODAY()
             LF:Text       = '[--' & FORMAT(LOC:Date, @d6) & '--]'
             ADD(Log_File)
          .

          LF:Text          = FORMAT(CLOCK(), @t4) & ',[' & CLIP(p:Heading) & '],' & CLIP(p:Text)
       ELSE
          LF:Text          = CLIP(p:Text)
       .

       ADD(Log_File)
!       IF ERRORCODE()
!          MESSAGE('Failed to add to file.||File: ' & CLIP(Log_File{PROP:Name}) & '||Error: ' & ERROR(), 'Add_Log', ICON:Hand)
!       .

       IF LOC:Debug = 1
          !db.debugout('[Add_Log]  Error: ' & CLIP(ERROR()) & ',  Name: ' & Log_File{PROP:Name})
       .

       CLOSE(Log_File)
       BREAK
    .

    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! ** global **
!!! </summary>
Add_Audit            PROCEDURE  (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel) ! Declare Procedure
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Audit.Open()
     .
     Access:Audit.UseFile()
    ! (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel)
    ! (BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0)

    IF Access:Audit.PrimeRecord() = LEVEL:Benign
       AUD:Severity         = p:Severity

       IF ~OMITTED(2)
          AUD:AppSection    = p:AppSection & '  [' & GlobalErrors.GetProcedureName() & ']'
       ELSE
          AUD:AppSection    = GlobalErrors.GetProcedureName()        ! Set it to last Window
       .
       IF ~OMITTED(3)
          AUD:Data1         = p:Data1
       .
       IF ~OMITTED(4)
          AUD:Data2         = p:Data2
       .

       AUD:AccessLevel      = p:AccessLevel

       IF Access:Audit.TryInsert() = LEVEL:Benign
          ! Added
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:Audit.Close()
    Exit
