

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module

                     MAP
                       INCLUDE('DBUTRANS006.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Upd_Invoice_Paid_Status PROCEDURE  (p:IID, p:Option)       ! Declare Procedure
LOC:Total_Paid       DECIMAL(15,2)                         !
LOC:Total_Credit     DECIMAL(15,2)                         !
LOC:Status           BYTE                                  !used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown

  CODE
!!! <summary>
!!! Generated from procedure template - Source
!!! ** global **  on trigger
!!! </summary>
Upd_Delivery_Man_Status PROCEDURE  (p:DID, p:DIID, p:Option) ! Declare Procedure
LOC:DID              ULONG                                 !Delivery ID
LOC:No_MIDs          ULONG                                 !
LOC:No_Unloaded_Items ULONG                                !
LOC:Manifested       BYTE                                  !Manifested status - this has maintenance function - Not Manifested, Partially Manifested, Partially Manifested Multiple, Fully Manifested, Fully Manifested Multiple
LOC:User_Text        STRING(255)                           !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
     Access:DeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Manifested)

    Exit

CloseFiles     Routine
    Relate:DeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! on trigger
!!! </summary>
Upd_Delivery_Del_Status PROCEDURE  (p:DID, p:DIID, p:Silent, p:NoCache) ! Declare Procedure
LOC:No_Unloaded_Items LONG                                 !
LOC:DID              ULONG                                 !Delivery ID
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
     Access:DeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(A_DEL:Delivered)

    Exit

CloseFiles     Routine
    Relate:DeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Upd_InvoiceTransporter_Paid_Status PROCEDURE  (p:TIN)      ! Declare Procedure
LOC:Total_Paid       DECIMAL(15,2)                         !
LOC:Total_Credit     DECIMAL(15,2)                         !
LOC:Status           BYTE                                  !used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_InvoiceTransporter.Open()
     .
     Access:_InvoiceTransporter.UseFile()
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Status)

    Exit

CloseFiles     Routine
    Relate:_InvoiceTransporter.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! (p:CPID, p:Amt_Allocated)
!!! </summary>
Upd_ClientPayments_Status PROCEDURE  (p:CPID, p:Amt_Allocated) ! Declare Procedure
LOC:Amount           DECIMAL(10,2)                         !Amount of payment
LOC:Return           LONG                                  !
Tek_Failed_File     STRING(100)

View_CPA        VIEW(ClientsPaymentsAllocationAlias)
    PROJECT(A_CLIPA:Amount)
    .


CPA_View        ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAllocationAlias.Open()
     .
     Access:ClientsPaymentsAlias.UseFile()
     Access:ClientsPaymentsAllocationAlias.UseFile()
    ! (p:CPID, p:Amt_Allocated)
    ! (ULONG, <*DECIMAL>),LONG,PROC
    A_CLIP:CPID             = p:CPID
    IF Access:ClientsPaymentsAlias.TryFetch(A_CLIP:PKey_CPID) ~= LEVEL:Benign
       LOC:Return           = -1
    ELSE
       ! Not Allocated, Partial Allocation, Fully Allocated

       CPA_View.Init(View_CPA, Relate:ClientsPaymentsAllocationAlias)
       CPA_View.AddSortOrder(A_CLIPA:FKey_CPID_AllocationNo)
       CPA_View.AddRange(A_CLIPA:CPID, A_CLIP:CPID)
       CPA_View.Reset()
       LOOP
          IF CPA_View.Next() ~= LEVEL:Benign
             BREAK
          .

          LOC:Amount       += A_CLIPA:Amount
       .
       CPA_View.Kill()

       A_CLIP:Status        = 0
       IF LOC:Amount >= A_CLIP:Amount
          A_CLIP:Status     = 2
       ELSIF LOC:Amount > 0
          A_CLIP:Status     = 1
       .

       A_CLIP:StatusUpToDate    = TRUE
       IF Access:ClientsPaymentsAlias.TryUpdate() = LEVEL:Benign
       .

       IF LOC:Amount > A_CLIP:Amount          ! Complain!
          MESSAGE('Allocated amount exceeds Payment amount!||Client Payment ID - CPID: ' & A_CLIP:CPID, 'Upd_ClientPayments_Status', ICON:Hand)
       .

       LOC:Return           = A_CLIP:Status

       IF ~OMITTED(2)
          p:Amt_Allocated   = LOC:Amount
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:ClientsPaymentsAlias.Close()
    Relate:ClientsPaymentsAllocationAlias.Close()
    Exit
