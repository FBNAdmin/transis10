

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module

                     MAP
                       INCLUDE('DBUTRANS012.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Setting          PROCEDURE  (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip, p:Update) ! Declare Procedure

  CODE
    ! Get_Setting
    ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip, p:Update)
    ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0),STRING
    !   1       2       3           4           5

    SETI:Setting            = p:Setting
    IF Access:Settings.TryFetch(SETI:Key_Setting) = LEVEL:Benign
       ! SETI:Value - returned always
       IF p:Update = TRUE
          SETI:Value        = p:DefValue
          IF Access:Settings.TryUpdate() = LEVEL:Benign
       .  .
    ELSIF p:Create = TRUE
       CLEAR(SETI:Record)
       IF Access:Settings.TryPrimeAutoInc() = LEVEL:Benign
          SETI:Setting      = p:Setting

          IF ~OMITTED(3)
             SETI:Value     = p:DefValue
          .
          IF ~OMITTED(4)
             SETI:Picture   = p:Picture
          .
          IF ~OMITTED(5)
             SETI:Tip       = p:Tip
          .

          IF Access:Settings.TryInsert() ~= LEVEL:Benign
             Access:Settings.CancelAutoInc()
             CLEAR(SETI:Record)
          .
       ELSE
          CLEAR(SETI:Record)
       .
    ELSE
       CLEAR(SETI:Record)
    .

    RETURN(SETI:Value)
