

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS010.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('DBUTRANS002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS011.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS014.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Invoice_Transporter PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(_InvoiceTransporter)
                       PROJECT(INT:TIN)
                       PROJECT(INT:TID)
                       PROJECT(INT:BID)
                       PROJECT(INT:MID)
                       PROJECT(INT:DINo)
                       PROJECT(INT:Leg)
                       PROJECT(INT:Cost)
                       PROJECT(INT:Manifest)
                       PROJECT(INT:VAT)
                       PROJECT(INT:Rate)
                       PROJECT(INT:VATRate)
                       PROJECT(INT:UID)
                       PROJECT(INT:DID)
                       PROJECT(INT:DLID)
                       PROJECT(INT:CR_TIN)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
INT:TIN                LIKE(INT:TIN)                  !List box control field - type derived from field
INT:TID                LIKE(INT:TID)                  !List box control field - type derived from field
INT:BID                LIKE(INT:BID)                  !List box control field - type derived from field
INT:MID                LIKE(INT:MID)                  !List box control field - type derived from field
INT:DINo               LIKE(INT:DINo)                 !List box control field - type derived from field
INT:Leg                LIKE(INT:Leg)                  !List box control field - type derived from field
INT:Cost               LIKE(INT:Cost)                 !List box control field - type derived from field
INT:Manifest           LIKE(INT:Manifest)             !List box control field - type derived from field
INT:VAT                LIKE(INT:VAT)                  !List box control field - type derived from field
INT:Rate               LIKE(INT:Rate)                 !List box control field - type derived from field
INT:VATRate            LIKE(INT:VATRate)              !List box control field - type derived from field
INT:UID                LIKE(INT:UID)                  !Browse key field - type derived from field
INT:DID                LIKE(INT:DID)                  !Browse key field - type derived from field
INT:DLID               LIKE(INT:DLID)                 !Browse key field - type derived from field
INT:CR_TIN             LIKE(INT:CR_TIN)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the _InvoiceTransporter file'),AT(,,358,208),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_Invoice_Transporter'),SYSTEM
                       LIST,AT(8,28,342,136),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~TIN~C(0)@n_10@40R(2)|M~T' & |
  'ID~C(0)@n_10@40R(2)|M~BID~C(0)@n_10@40R(2)|M~MID~C(0)@n_10@40R(2)|M~DI No.~C(0)@n_10' & |
  '@24R(2)|M~Leg~C(0)@n5@40R(1)|M~Cost~C(0)@n-14.2@30R(1)|M~Manifest~C(0)@n3@40R(1)|M~V' & |
  'AT~C(0)@n-14.2@40R(1)|M~Rate~C(0)@n-13.4@36R(1)|M~VAT Rate~C(0)@n-7.2@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the _InvoiceTransporter file')
                       BUTTON('&Select'),AT(89,168,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(142,168,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(195,168,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(248,168,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(301,168,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       SHEET,AT(4,4,350,182),USE(?CurrentTab)
                         TAB('&1) By Transporter Invoice No.'),USE(?Tab:2)
                         END
                         TAB('&2) By Transporter'),USE(?Tab:3)
                           BUTTON('Select Transporter'),AT(8,168,,14),USE(?SelectTransporter),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&3) By Branch'),USE(?Tab:4)
                           BUTTON('Select Branches'),AT(8,168,,14),USE(?SelectBranches),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&4) By Manifest'),USE(?Tab:5)
                           BUTTON('Select Manifest'),AT(8,168,,14),USE(?SelectManifest),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       BUTTON('&Close'),AT(252,190,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,190,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort5:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 6
BRW1::Sort7:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 8
BRW1::Sort8:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 9
BRW1::Sort9:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 10
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Invoice_Transporter')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Access:DeliveryLegs.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Users.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:_InvoiceTransporter,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,INT:FKey_TID)                         ! Add the sort order for INT:FKey_TID for sort order 1
  BRW1.AddRange(INT:TID,Relate:_InvoiceTransporter,Relate:Transporter) ! Add file relationship range limit for sort order 1
  BRW1.AddSortOrder(,INT:FKey_BID)                         ! Add the sort order for INT:FKey_BID for sort order 2
  BRW1.AddRange(INT:BID,Relate:_InvoiceTransporter,Relate:Branches) ! Add file relationship range limit for sort order 2
  BRW1.AddSortOrder(,INT:Fkey_MID)                         ! Add the sort order for INT:Fkey_MID for sort order 3
  BRW1.AddRange(INT:MID,Relate:_InvoiceTransporter,Relate:Manifest) ! Add file relationship range limit for sort order 3
  BRW1.AddSortOrder(,INT:FKey_UID)                         ! Add the sort order for INT:FKey_UID for sort order 4
  BRW1.AddRange(INT:UID,Relate:_InvoiceTransporter,Relate:Users) ! Add file relationship range limit for sort order 4
  BRW1.AddSortOrder(,INT:FKey_DID)                         ! Add the sort order for INT:FKey_DID for sort order 5
  BRW1.AddLocator(BRW1::Sort5:Locator)                     ! Browse has a locator for sort order 5
  BRW1::Sort5:Locator.Init(,INT:DID,1,BRW1)                ! Initialize the browse locator using  using key: INT:FKey_DID , INT:DID
  BRW1.AddSortOrder(,INT:FKey_DLID)                        ! Add the sort order for INT:FKey_DLID for sort order 6
  BRW1.AddRange(INT:DLID,Relate:_InvoiceTransporter,Relate:DeliveryLegs) ! Add file relationship range limit for sort order 6
  BRW1.AddSortOrder(,INT:Key_CR_TIN)                       ! Add the sort order for INT:Key_CR_TIN for sort order 7
  BRW1.AddLocator(BRW1::Sort7:Locator)                     ! Browse has a locator for sort order 7
  BRW1::Sort7:Locator.Init(,INT:CR_TIN,1,BRW1)             ! Initialize the browse locator using  using key: INT:Key_CR_TIN , INT:CR_TIN
  BRW1.AddSortOrder(,INT:SKey_MID_DID)                     ! Add the sort order for INT:SKey_MID_DID for sort order 8
  BRW1.AddLocator(BRW1::Sort8:Locator)                     ! Browse has a locator for sort order 8
  BRW1::Sort8:Locator.Init(,INT:MID,1,BRW1)                ! Initialize the browse locator using  using key: INT:SKey_MID_DID , INT:MID
  BRW1.AddSortOrder(,INT:SKey_MID_Manifest)                ! Add the sort order for INT:SKey_MID_Manifest for sort order 9
  BRW1.AddLocator(BRW1::Sort9:Locator)                     ! Browse has a locator for sort order 9
  BRW1::Sort9:Locator.Init(,INT:MID,1,BRW1)                ! Initialize the browse locator using  using key: INT:SKey_MID_Manifest , INT:MID
  BRW1.AddSortOrder(,INT:PKey_TIN)                         ! Add the sort order for INT:PKey_TIN for sort order 10
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 10
  BRW1::Sort0:Locator.Init(,INT:TIN,1,BRW1)                ! Initialize the browse locator using  using key: INT:PKey_TIN , INT:TIN
  BRW1.AddField(INT:TIN,BRW1.Q.INT:TIN)                    ! Field INT:TIN is a hot field or requires assignment from browse
  BRW1.AddField(INT:TID,BRW1.Q.INT:TID)                    ! Field INT:TID is a hot field or requires assignment from browse
  BRW1.AddField(INT:BID,BRW1.Q.INT:BID)                    ! Field INT:BID is a hot field or requires assignment from browse
  BRW1.AddField(INT:MID,BRW1.Q.INT:MID)                    ! Field INT:MID is a hot field or requires assignment from browse
  BRW1.AddField(INT:DINo,BRW1.Q.INT:DINo)                  ! Field INT:DINo is a hot field or requires assignment from browse
  BRW1.AddField(INT:Leg,BRW1.Q.INT:Leg)                    ! Field INT:Leg is a hot field or requires assignment from browse
  BRW1.AddField(INT:Cost,BRW1.Q.INT:Cost)                  ! Field INT:Cost is a hot field or requires assignment from browse
  BRW1.AddField(INT:Manifest,BRW1.Q.INT:Manifest)          ! Field INT:Manifest is a hot field or requires assignment from browse
  BRW1.AddField(INT:VAT,BRW1.Q.INT:VAT)                    ! Field INT:VAT is a hot field or requires assignment from browse
  BRW1.AddField(INT:Rate,BRW1.Q.INT:Rate)                  ! Field INT:Rate is a hot field or requires assignment from browse
  BRW1.AddField(INT:VATRate,BRW1.Q.INT:VATRate)            ! Field INT:VATRate is a hot field or requires assignment from browse
  BRW1.AddField(INT:UID,BRW1.Q.INT:UID)                    ! Field INT:UID is a hot field or requires assignment from browse
  BRW1.AddField(INT:DID,BRW1.Q.INT:DID)                    ! Field INT:DID is a hot field or requires assignment from browse
  BRW1.AddField(INT:DLID,BRW1.Q.INT:DLID)                  ! Field INT:DLID is a hot field or requires assignment from browse
  BRW1.AddField(INT:CR_TIN,BRW1.Q.INT:CR_TIN)              ! Field INT:CR_TIN is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Invoice_Transporter',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Invoice_Transporter',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_InvoiceTransporter
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectTransporter
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectTransporter()
      ThisWindow.Reset
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectBranches()
      ThisWindow.Reset
    OF ?SelectManifest
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Browse_Manifest()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?CurrentTab) = 6
    RETURN SELF.SetSort(5,Force)
  ELSIF CHOICE(?CurrentTab) = 7
    RETURN SELF.SetSort(6,Force)
  ELSIF CHOICE(?CurrentTab) = 8
    RETURN SELF.SetSort(7,Force)
  ELSIF CHOICE(?CurrentTab) = 9
    RETURN SELF.SetSort(8,Force)
  ELSIF CHOICE(?CurrentTab) = 10
    RETURN SELF.SetSort(9,Force)
  ELSE
    RETURN SELF.SetSort(10,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Select a Users Record
!!! </summary>
SelectUsers PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Users)
                       PROJECT(USE:Login)
                       PROJECT(USE:Password)
                       PROJECT(USE:Name)
                       PROJECT(USE:Surname)
                       PROJECT(USE:AccessLevel)
                       PROJECT(USE:UID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
USE:Login              LIKE(USE:Login)                !List box control field - type derived from field
USE:Password           LIKE(USE:Password)             !List box control field - type derived from field
USE:Name               LIKE(USE:Name)                 !List box control field - type derived from field
USE:Surname            LIKE(USE:Surname)              !List box control field - type derived from field
USE:AccessLevel        LIKE(USE:AccessLevel)          !List box control field - type derived from field
USE:UID                LIKE(USE:UID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Users Record'),AT(,,358,198),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('SelectUsers'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Login~L(2)@s20@80L(2)|M~' & |
  'Password~L(2)@s35@80L(2)|M~Name~L(2)@s35@80L(2)|M~Surname~L(2)@s35@52R(2)|M~Access L' & |
  'evel~C(0)@n3@'),FROM(Queue:Browse:1),IMM,MSG('Browsing the Users file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Login'),USE(?Tab:2)
                         END
                         TAB('&2) By Name && Surname'),USE(?Tab:3)
                         END
                       END
                       BUTTON('&Close'),AT(252,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectUsers')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Users.SetOpenRelated()
  Relate:Users.Open                                        ! File Users used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Users,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,USE:SKey_Name_Surname)                ! Add the sort order for USE:SKey_Name_Surname for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,USE:Name,1,BRW1)               ! Initialize the browse locator using  using key: USE:SKey_Name_Surname , USE:Name
  BRW1.AddSortOrder(,USE:Key_Login)                        ! Add the sort order for USE:Key_Login for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,USE:Login,1,BRW1)              ! Initialize the browse locator using  using key: USE:Key_Login , USE:Login
  BRW1.AddField(USE:Login,BRW1.Q.USE:Login)                ! Field USE:Login is a hot field or requires assignment from browse
  BRW1.AddField(USE:Password,BRW1.Q.USE:Password)          ! Field USE:Password is a hot field or requires assignment from browse
  BRW1.AddField(USE:Name,BRW1.Q.USE:Name)                  ! Field USE:Name is a hot field or requires assignment from browse
  BRW1.AddField(USE:Surname,BRW1.Q.USE:Surname)            ! Field USE:Surname is a hot field or requires assignment from browse
  BRW1.AddField(USE:AccessLevel,BRW1.Q.USE:AccessLevel)    ! Field USE:AccessLevel is a hot field or requires assignment from browse
  BRW1.AddField(USE:UID,BRW1.Q.USE:UID)                    ! Field USE:UID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('SelectUsers',QuickWindow)                  ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Users.Close
  END
  IF SELF.Opened
    INIMgr.Update('SelectUsers',QuickWindow)               ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_DeliveryLegs PROCEDURE 

CurrentTab           STRING(80)                            !
Locator              ULONG                                 !
BRW1::View:Browse    VIEW(DeliveryLegs)
                       PROJECT(DELL:DLID)
                       PROJECT(DELL:DID)
                       PROJECT(DELL:TID)
                       PROJECT(DELL:Leg)
                       PROJECT(DELL:Cost)
                       PROJECT(DELL:VATRate)
                       PROJECT(DELL:JID)
                       JOIN(DEL:PKey_DID,DELL:DID)
                         PROJECT(DEL:DINo)
                         PROJECT(DEL:BID)
                         PROJECT(DEL:DID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DELL:DLID              LIKE(DELL:DLID)                !List box control field - type derived from field
DELL:DID               LIKE(DELL:DID)                 !List box control field - type derived from field
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:BID                LIKE(DEL:BID)                  !List box control field - type derived from field
DELL:TID               LIKE(DELL:TID)                 !List box control field - type derived from field
DELL:Leg               LIKE(DELL:Leg)                 !List box control field - type derived from field
DELL:Cost              LIKE(DELL:Cost)                !List box control field - type derived from field
DELL:VATRate           LIKE(DELL:VATRate)             !List box control field - type derived from field
DELL:JID               LIKE(DELL:JID)                 !Browse key field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Delivery Legs file'),AT(,,315,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('Browse_DeliveryLegs'),SYSTEM
                       LIST,AT(8,30,291,124),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~DLID~C(0)@n_10@40R(2)|M~' & |
  'DID~C(0)@n_10@40R(2)|M~DI No.~C(0)@n_10@40R(2)|M~BID~C(0)@n_10@40R(2)|M~TID~C(0)@n_1' & |
  '0@24R(2)|M~Leg~C(0)@n5@60R(1)|M~Cost~C(0)@n-13.2@36R(1)|M~VAT Rate~C(0)@n-7.2@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the DeliveryLegs file')
                       BUTTON('&Select'),AT(90,156,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,2,306,172),USE(?CurrentTab)
                         TAB('&1) By Delivery ID && Leg'),USE(?Tab:2)
                           PROMPT('Locator:'),AT(8,18),USE(?Locator:Prompt)
                           ENTRY(@n_10),AT(38,18,60,10),USE(Locator),RIGHT(1)
                           BUTTON('&Insert'),AT(174,158,42,12),USE(?Insert)
                           BUTTON('&Change'),AT(216,158,42,12),USE(?Change)
                           BUTTON('&Delete'),AT(258,158,42,12),USE(?Delete)
                         END
                         TAB('&3) By Journey'),USE(?Tab:4)
                           BUTTON('Select Journeys'),AT(12,156,,14),USE(?SelectJourneys),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('by DLID'),USE(?Tab3)
                         END
                       END
                       BUTTON('&Close'),AT(206,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(260,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort3:Locator  IncrementalLocatorClass               ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_DeliveryLegs')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:DeliveryLegs.SetOpenRelated()
  Relate:DeliveryLegs.Open                                 ! File DeliveryLegs used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterAlias.Open                             ! File TransporterAlias used by this procedure, so make sure it's RelationManager is open
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:DeliveryLegs,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,DELL:FKey_JID)                        ! Add the sort order for DELL:FKey_JID for sort order 1
  BRW1.AddRange(DELL:JID,Relate:DeliveryLegs,Relate:Journeys) ! Add file relationship range limit for sort order 1
  BRW1.AddSortOrder(,DELL:PKey_DLID)                       ! Add the sort order for DELL:PKey_DLID for sort order 2
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort3:Locator.Init(?Locator,DELL:DLID,1,BRW1)      ! Initialize the browse locator using ?Locator using key: DELL:PKey_DLID , DELL:DLID
  BRW1.AddSortOrder(,DELL:FKey_DID_Leg)                    ! Add the sort order for DELL:FKey_DID_Leg for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(?Locator,DELL:DID,1,BRW1)       ! Initialize the browse locator using ?Locator using key: DELL:FKey_DID_Leg , DELL:DID
  BRW1.AppendOrder('+DELL:DLID')                           ! Append an additional sort order
  BRW1.AddField(DELL:DLID,BRW1.Q.DELL:DLID)                ! Field DELL:DLID is a hot field or requires assignment from browse
  BRW1.AddField(DELL:DID,BRW1.Q.DELL:DID)                  ! Field DELL:DID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DINo,BRW1.Q.DEL:DINo)                  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW1.AddField(DEL:BID,BRW1.Q.DEL:BID)                    ! Field DEL:BID is a hot field or requires assignment from browse
  BRW1.AddField(DELL:TID,BRW1.Q.DELL:TID)                  ! Field DELL:TID is a hot field or requires assignment from browse
  BRW1.AddField(DELL:Leg,BRW1.Q.DELL:Leg)                  ! Field DELL:Leg is a hot field or requires assignment from browse
  BRW1.AddField(DELL:Cost,BRW1.Q.DELL:Cost)                ! Field DELL:Cost is a hot field or requires assignment from browse
  BRW1.AddField(DELL:VATRate,BRW1.Q.DELL:VATRate)          ! Field DELL:VATRate is a hot field or requires assignment from browse
  BRW1.AddField(DELL:JID,BRW1.Q.DELL:JID)                  ! Field DELL:JID is a hot field or requires assignment from browse
  BRW1.AddField(DEL:DID,BRW1.Q.DEL:DID)                    ! Field DEL:DID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_DeliveryLegs',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryLegs.Close
    Relate:TransporterAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_DeliveryLegs',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_DeliveryLegs
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectJourneys
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectJourneys()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Addresses PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Addresses)
                       PROJECT(ADD:AID)
                       PROJECT(ADD:BID)
                       PROJECT(ADD:AddressName)
                       PROJECT(ADD:AddressNameSuburb)
                       PROJECT(ADD:Line1)
                       PROJECT(ADD:Line2)
                       PROJECT(ADD:PhoneNo)
                       PROJECT(ADD:PhoneNo2)
                       PROJECT(ADD:Fax)
                       PROJECT(ADD:ShowForAllBranches)
                       PROJECT(ADD:SUID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ADD:AID                LIKE(ADD:AID)                  !List box control field - type derived from field
ADD:BID                LIKE(ADD:BID)                  !List box control field - type derived from field
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
ADD:AddressNameSuburb  LIKE(ADD:AddressNameSuburb)    !List box control field - type derived from field
ADD:Line1              LIKE(ADD:Line1)                !List box control field - type derived from field
ADD:Line2              LIKE(ADD:Line2)                !List box control field - type derived from field
ADD:PhoneNo            LIKE(ADD:PhoneNo)              !List box control field - type derived from field
ADD:PhoneNo2           LIKE(ADD:PhoneNo2)             !List box control field - type derived from field
ADD:Fax                LIKE(ADD:Fax)                  !List box control field - type derived from field
ADD:ShowForAllBranches LIKE(ADD:ShowForAllBranches)   !List box control field - type derived from field
ADD:SUID               LIKE(ADD:SUID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Addresses file'),AT(,,358,198),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_Addresses'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('40R(2)|M~AID~C(0)@n_10@40R(2)|M~B' & |
  'ID~C(0)@n_10@80L(2)|M~Address Name~@s35@80L(2)|M~Address Name Suburb~@s50@80L(2)|M~L' & |
  'ine 1~@s35@80L(2)|M~Line 2~@s35@80L(2)|M~Phone No~@s20@80L(2)|M~Phone No 2~@s20@80L(' & |
  '2)|M~Fax~@s20@80R(6)|M~Show for all Branches~C(0)@n3@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he Addresses file')
                       BUTTON('&Select'),AT(8,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&Insert'),AT(220,159,42,12),USE(?Insert)
                       BUTTON('&Change'),AT(262,159,42,12),USE(?Change)
                       BUTTON('&Delete'),AT(304,159,42,12),USE(?Delete)
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Name'),USE(?Tab:2)
                         END
                         TAB('&2) By Suburb'),USE(?Tab:3)
                           BUTTON('Select Add_Suburbs'),AT(8,158,118,14),USE(?SelectAdd_Suburbs),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&3) By Branch'),USE(?Tab:4)
                           BUTTON('Select Branches'),AT(8,158,118,14),USE(?SelectBranches),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('by AID'),USE(?Tab4)
                         END
                       END
                       BUTTON('&Close'),AT(252,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort3:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 4
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Addresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Add_Suburbs.SetOpenRelated()
  Relate:Add_Suburbs.Open                                  ! File Add_Suburbs used by this procedure, so make sure it's RelationManager is open
  Access:Branches.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Addresses,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,ADD:FKey_SUID)                        ! Add the sort order for ADD:FKey_SUID for sort order 1
  BRW1.AddRange(ADD:SUID,Relate:Addresses,Relate:Add_Suburbs) ! Add file relationship range limit for sort order 1
  BRW1.AddSortOrder(,ADD:FKey_BID)                         ! Add the sort order for ADD:FKey_BID for sort order 2
  BRW1.AddRange(ADD:BID,Relate:Addresses,Relate:Branches)  ! Add file relationship range limit for sort order 2
  BRW1.AddSortOrder(,ADD:PKey_AID)                         ! Add the sort order for ADD:PKey_AID for sort order 3
  BRW1.AddLocator(BRW1::Sort3:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort3:Locator.Init(,ADD:AID,1,BRW1)                ! Initialize the browse locator using  using key: ADD:PKey_AID , ADD:AID
  BRW1.AddSortOrder(,ADD:Key_Name)                         ! Add the sort order for ADD:Key_Name for sort order 4
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 4
  BRW1::Sort0:Locator.Init(,ADD:AddressName,1,BRW1)        ! Initialize the browse locator using  using key: ADD:Key_Name , ADD:AddressName
  BRW1.AddField(ADD:AID,BRW1.Q.ADD:AID)                    ! Field ADD:AID is a hot field or requires assignment from browse
  BRW1.AddField(ADD:BID,BRW1.Q.ADD:BID)                    ! Field ADD:BID is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AddressName,BRW1.Q.ADD:AddressName)    ! Field ADD:AddressName is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AddressNameSuburb,BRW1.Q.ADD:AddressNameSuburb) ! Field ADD:AddressNameSuburb is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Line1,BRW1.Q.ADD:Line1)                ! Field ADD:Line1 is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Line2,BRW1.Q.ADD:Line2)                ! Field ADD:Line2 is a hot field or requires assignment from browse
  BRW1.AddField(ADD:PhoneNo,BRW1.Q.ADD:PhoneNo)            ! Field ADD:PhoneNo is a hot field or requires assignment from browse
  BRW1.AddField(ADD:PhoneNo2,BRW1.Q.ADD:PhoneNo2)          ! Field ADD:PhoneNo2 is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Fax,BRW1.Q.ADD:Fax)                    ! Field ADD:Fax is a hot field or requires assignment from browse
  BRW1.AddField(ADD:ShowForAllBranches,BRW1.Q.ADD:ShowForAllBranches) ! Field ADD:ShowForAllBranches is a hot field or requires assignment from browse
  BRW1.AddField(ADD:SUID,BRW1.Q.ADD:SUID)                  ! Field ADD:SUID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Addresses',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Suburbs.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Addresses',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Addresses
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SelectAdd_Suburbs
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectAdd_Suburbs()
      ThisWindow.Reset
    OF ?SelectBranches
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      SelectBranches()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

