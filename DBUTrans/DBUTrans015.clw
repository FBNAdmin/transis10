

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS015.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Select a DeliveryComposition Record
!!! </summary>
SelectDeliveryComposition PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(DeliveryComposition)
                       PROJECT(DELC:CID)
                       PROJECT(DELC:Reference)
                       PROJECT(DELC:DateAdded)
                       PROJECT(DELC:TimeAdded)
                       PROJECT(DELC:Items)
                       PROJECT(DELC:Weight)
                       PROJECT(DELC:Volume)
                       PROJECT(DELC:Deliveries)
                       PROJECT(DELC:DeliveriesLoaded)
                       PROJECT(DELC:DELCID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
DELC:CID               LIKE(DELC:CID)                 !List box control field - type derived from field
DELC:Reference         LIKE(DELC:Reference)           !List box control field - type derived from field
DELC:DateAdded         LIKE(DELC:DateAdded)           !List box control field - type derived from field
DELC:TimeAdded         LIKE(DELC:TimeAdded)           !List box control field - type derived from field
DELC:Items             LIKE(DELC:Items)               !List box control field - type derived from field
DELC:Weight            LIKE(DELC:Weight)              !List box control field - type derived from field
DELC:Volume            LIKE(DELC:Volume)              !List box control field - type derived from field
DELC:Deliveries        LIKE(DELC:Deliveries)          !List box control field - type derived from field
DELC:DeliveriesLoaded  LIKE(DELC:DeliveriesLoaded)    !List box control field - type derived from field
DELC:DELCID            LIKE(DELC:DELCID)              !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a DeliveryComposition Record'),AT(,,358,198),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('SelectDeliveryComposition'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80R(2)|M~CID~C(0)@n_10@80L(2)|M~R' & |
  'eference~L(2)@s35@80R(2)|M~Date Added~C(0)@d5@80R(2)|M~Time Added~C(0)@t7@28R(2)|M~I' & |
  'tems~C(0)@n6@80D(34)|M~Weight~C(0)@n-21.2@72D(35)|M~Volume~C(0)@n-16.3@44R(2)|M~Deli' & |
  'veries~C(0)@n6@72R(2)|M~Deliveries Loaded~C(0)@n3@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he DeliveryComposition file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Client'),USE(?Tab:2)
                         END
                       END
                       BUTTON('&Close'),AT(252,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectDeliveryComposition')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:DeliveryComposition.Open                          ! File DeliveryComposition used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:DeliveryComposition,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,DELC:FKey_CID)                        ! Add the sort order for DELC:FKey_CID for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,DELC:CID,1,BRW1)               ! Initialize the browse locator using  using key: DELC:FKey_CID , DELC:CID
  BRW1.AddField(DELC:CID,BRW1.Q.DELC:CID)                  ! Field DELC:CID is a hot field or requires assignment from browse
  BRW1.AddField(DELC:Reference,BRW1.Q.DELC:Reference)      ! Field DELC:Reference is a hot field or requires assignment from browse
  BRW1.AddField(DELC:DateAdded,BRW1.Q.DELC:DateAdded)      ! Field DELC:DateAdded is a hot field or requires assignment from browse
  BRW1.AddField(DELC:TimeAdded,BRW1.Q.DELC:TimeAdded)      ! Field DELC:TimeAdded is a hot field or requires assignment from browse
  BRW1.AddField(DELC:Items,BRW1.Q.DELC:Items)              ! Field DELC:Items is a hot field or requires assignment from browse
  BRW1.AddField(DELC:Weight,BRW1.Q.DELC:Weight)            ! Field DELC:Weight is a hot field or requires assignment from browse
  BRW1.AddField(DELC:Volume,BRW1.Q.DELC:Volume)            ! Field DELC:Volume is a hot field or requires assignment from browse
  BRW1.AddField(DELC:Deliveries,BRW1.Q.DELC:Deliveries)    ! Field DELC:Deliveries is a hot field or requires assignment from browse
  BRW1.AddField(DELC:DeliveriesLoaded,BRW1.Q.DELC:DeliveriesLoaded) ! Field DELC:DeliveriesLoaded is a hot field or requires assignment from browse
  BRW1.AddField(DELC:DELCID,BRW1.Q.DELC:DELCID)            ! Field DELC:DELCID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('SelectDeliveryComposition',QuickWindow)    ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryComposition.Close
  END
  IF SELF.Opened
    INIMgr.Update('SelectDeliveryComposition',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Select a Floors Record
!!! </summary>
SelectFloors PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FBNFloor)
                       PROJECT(FLO:Print_Rates)
                       PROJECT(FLO:TripSheetLoading)
                       PROJECT(FLO:FID)
                       PROJECT(FLO:AID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:FBNFloor           LIKE(FLO:FBNFloor)             !List box control field - type derived from field
FLO:Print_Rates        LIKE(FLO:Print_Rates)          !List box control field - type derived from field
FLO:TripSheetLoading   LIKE(FLO:TripSheetLoading)     !List box control field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Primary key field - type derived from field
FLO:AID                LIKE(FLO:AID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Floors Record'),AT(,,252,198),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('SelectFloors'),SYSTEM
                       LIST,AT(8,30,236,124),USE(?Browse:1),HVSCROLL,FORMAT('80L(2)|M~Floor~L(2)@s35@40R(2)|M~' & |
  'FBN Floor~C(0)@n3@48R(2)|M~Print Rates~C(0)@n3@76R(2)|M~Trip Sheet Loading~C(0)@n3@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the Floors file')
                       BUTTON('&Select'),AT(195,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,244,172),USE(?CurrentTab)
                         TAB('&1) By Floor'),USE(?Tab:2)
                         END
                         TAB('&2) By Address'),USE(?Tab:3)
                         END
                       END
                       BUTTON('&Close'),AT(146,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(199,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectFloors')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Floors.Open                                       ! File Floors used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Floors,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,FLO:FKey_AID)                         ! Add the sort order for FLO:FKey_AID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,FLO:AID,1,BRW1)                ! Initialize the browse locator using  using key: FLO:FKey_AID , FLO:AID
  BRW1.AddSortOrder(,FLO:Key_Floor)                        ! Add the sort order for FLO:Key_Floor for sort order 2
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort0:Locator.Init(,FLO:Floor,1,BRW1)              ! Initialize the browse locator using  using key: FLO:Key_Floor , FLO:Floor
  BRW1.AddField(FLO:Floor,BRW1.Q.FLO:Floor)                ! Field FLO:Floor is a hot field or requires assignment from browse
  BRW1.AddField(FLO:FBNFloor,BRW1.Q.FLO:FBNFloor)          ! Field FLO:FBNFloor is a hot field or requires assignment from browse
  BRW1.AddField(FLO:Print_Rates,BRW1.Q.FLO:Print_Rates)    ! Field FLO:Print_Rates is a hot field or requires assignment from browse
  BRW1.AddField(FLO:TripSheetLoading,BRW1.Q.FLO:TripSheetLoading) ! Field FLO:TripSheetLoading is a hot field or requires assignment from browse
  BRW1.AddField(FLO:FID,BRW1.Q.FLO:FID)                    ! Field FLO:FID is a hot field or requires assignment from browse
  BRW1.AddField(FLO:AID,BRW1.Q.FLO:AID)                    ! Field FLO:AID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('SelectFloors',QuickWindow)                 ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Floors.Close
  END
  IF SELF.Opened
    INIMgr.Update('SelectFloors',QuickWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

