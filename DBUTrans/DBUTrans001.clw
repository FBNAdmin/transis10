

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('DBUTRANS002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS014.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS016.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS017.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS018.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Frame
!!! </summary>
Main PROCEDURE 

LOC:Orig_Title       STRING(35)                            !
LOC:Timer_It         SHORT                                 !
LOC:Logged_In        BYTE                                  !
LOC:Closing          BYTE                                  !
LOC:Test_Database    BYTE                                  !
SplashProcedureThread LONG
AppFrame             APPLICATION('TransIS - Database Update Utilitie'),AT(0,0,493,288),FONT('MS Sans Serif',8,, |
  FONT:regular),RESIZE,CENTERED,HVSCROLL,ICON('BlockBit.ico'),MAX,HLP('~Main'),PALETTE(256), |
  SYSTEM,TIMER(100),WALLPAPER('fbn_logo_small2.gif'),IMM
                       MENUBAR,USE(?Menubar)
                         MENU('&File'),USE(?File)
                           ITEM('Database Connection'),USE(?UtilitiesDatabaseConnection)
                           ITEM('Update Data Structure'),USE(?FileUpdateDataStructure)
                           ITEM,SEPARATOR
                           ITEM('&Print Setup ...'),USE(?PrintSetup),MSG('Setup printer'),STD(STD:PrintSetup)
                           ITEM,SEPARATOR
                           ITEM('E&xit'),USE(?Abort),STD(STD:Close)
                         END
                         MENU('&Raw Browses'),USE(?RawBrowse)
                           ITEM('Manifest'),USE(?RawBrowseManifest)
                           ITEM('Manifest Loads'),USE(?RawBrowseManifestLoads)
                           ITEM('Manifest Load Items'),USE(?RawBrowseManifestLoadItems)
                           ITEM,SEPARATOR
                           ITEM('Deliveries'),USE(?RawBrowseDeliveries)
                           ITEM('Delivery Items'),USE(?RawBrowseDeliveryItems)
                           ITEM('Delivery Legs'),USE(?RawBrowseDeliveryLegs)
                           ITEM,SEPARATOR
                           ITEM('Invoices'),USE(?RawBrowseInvoices)
                           ITEM('Invoice Items'),USE(?RawBrowseInvoiceItems)
                           ITEM('Invoice Transporter'),USE(?RawBrowseInvoiceTransporter)
                           ITEM,SEPARATOR
                           ITEM('Addresses'),USE(?RawBrowseAddresses)
                           ITEM('Email Addresses'),USE(?RawBrowsesEmailAddresses)
                         END
                         MENU('&Edit'),USE(?Edit)
                           ITEM('C&ut'),USE(?Cut),STD(STD:Cut)
                           ITEM('&Copy'),USE(?Copy),MSG('Copy item to Windows Clipboard'),STD(STD:Copy)
                           ITEM('&Paste'),USE(?Paste),MSG('Paste contents of Windows Clipboard'),STD(STD:Paste)
                         END
                         MENU('&Window'),USE(?Window),STD(STD:WindowList)
                           ITEM('&Cascade'),USE(?Cascade),MSG('Stack all open windows'),STD(STD:CascadeWindow)
                           ITEM('Tile &Horizontal'),USE(?TileHorizontal),MSG('Make all open windows visible'),STD(STD:TileHorizontal)
                           ITEM('Tile &Vertical'),USE(?TileVertical),MSG('Make all open windows visible'),STD(STD:TileVertical)
                           ITEM('&Arrange Icons'),USE(?Arrange),MSG('Align all window icons'),STD(STD:ArrangeIcons)
                         END
                         MENU('&Help'),USE(?Help)
                           ITEM('&Contents'),USE(?Helpindex),MSG('View the contents of the help file'),STD(STD:HelpIndex)
                           ITEM('&Search for Help On...'),USE(?HelpSearch),MSG('Search for help on a subject'),STD(STD:HelpSearch)
                           ITEM('&How to Use Help'),USE(?HelpOnHelp),MSG('How to use Windows Help'),STD(STD:HelpOnHelp)
                           ITEM('&About...'),USE(?About)
                         END
                       END
                       TOOLBAR,AT(0,0,,17),USE(?Tools)
                         BUTTON('&Update'),AT(2,1,45,14),USE(?Button_Update)
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Menu::Menubar ROUTINE                                      ! Code for menu items on ?Menubar
Menu::File ROUTINE                                         ! Code for menu items on ?File
  CASE ACCEPTED()
  OF ?UtilitiesDatabaseConnection
    START(Set_Connection, 25000)
  OF ?FileUpdateDataStructure
    START(Update_DBStructure, 25000, '')
  END
Menu::RawBrowse ROUTINE                                    ! Code for menu items on ?RawBrowse
  CASE ACCEPTED()
  OF ?RawBrowseManifest
    START(Browse_Manifest, 25000)
  OF ?RawBrowseManifestLoads
    START(Browse_ManifestLoads, 25000)
  OF ?RawBrowseManifestLoadItems
    START(Browse_ManidfestLoadItems, 25000)
  OF ?RawBrowseDeliveries
    START(Browse_Deliveries, 25000)
  OF ?RawBrowseDeliveryItems
    START(Browse_DeliveryItems, 25000)
  OF ?RawBrowseDeliveryLegs
    START(Browse_DeliveryLegs, 25000)
  OF ?RawBrowseInvoices
    START(Browse_Invoice, 25000)
  OF ?RawBrowseInvoiceItems
    START(Browse_InvoiceItems, 25000)
  OF ?RawBrowseInvoiceTransporter
    START(Browse_Invoice_Transporter, 25000)
  OF ?RawBrowseAddresses
    START(Browse_Addresses, 25000)
  OF ?RawBrowsesEmailAddresses
    START(Browse_EmailAddresses, 25000)
  END
Menu::Edit ROUTINE                                         ! Code for menu items on ?Edit
Menu::Window ROUTINE                                       ! Code for menu items on ?Window
Menu::Help ROUTINE                                         ! Code for menu items on ?Help
  CASE ACCEPTED()
  OF ?About
    AboutWindow()
  END

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.Open(AppFrame)                                      ! Open window
      AppFrame{PROP:Timer} = 0
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Main',AppFrame)                            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
      AppFrame{PROP:TabBarVisible}  = False
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('Main',AppFrame)                         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    ELSE
      DO Menu::Menubar                                     ! Process menu items on ?Menubar menu
      DO Menu::File                                        ! Process menu items on ?File menu
      DO Menu::RawBrowse                                   ! Process menu items on ?RawBrowse menu
      DO Menu::Edit                                        ! Process menu items on ?Edit menu
      DO Menu::Window                                      ! Process menu items on ?Window menu
      DO Menu::Help                                        ! Process menu items on ?Help menu
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Update
      START(Update_DBStructure, 25000, '')
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          LOC:Test_Database               = GETINI('Setup', 'Test_Database', 0, '.\TransIS.INI')
          IF LOC:Test_Database = TRUE
             AppFrame{PROP:Text}          = CLIP(AppFrame{PROP:Text}) & ' *** Test Database ***'
             ?Tools{PROP:Background}      = 09191FFH      !COLOR:Red
             
          .
      
          LOC:Orig_Title  = AppFrame{PROP:Text}
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
             UserLogin()
             IF GLO:UID > 0
                !AppFrame{PROP:Text}   = CLIP(LOC:Orig_Title) & ' - ' & CLIP(GLO:Login)
                LOC:Logged_In     = TRUE
             ELSE
                POST(EVENT:CloseWindow)
             .
      
            IF LOC:Logged_In = TRUE
      SplashProcedureThread = START(SplashWindow)          ! Run the splash window procedure
            .
    ELSE
      IF SplashProcedureThread
        IF EVENT() = Event:Accepted
          POST(Event:CloseWindow,,SplashProcedureThread)   ! Close the splash window
          SplashPRocedureThread = 0
        END
     END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Window 1
!!! </summary>
Update_DBStructure PROCEDURE (p_Qry)

LOC:Update_Commands_Q QUEUE,PRE(L_UQ)                      !
No                   ULONG                                 !
Command              STRING(2550)                          !
                     END                                   !
LOC:Errors           LONG                                  !
LOC:Results          STRING(50000)                         !
LOC:Idx              ULONG                                 !
LOC:Complete         BYTE                                  !
LOC:Top_No           ULONG                                 !
LOC:Settings_No      LONG                                  !No done up to
LOC:LogLine          STRING(2000)                          !
QuickWindow          WINDOW('Update Database Structure'),AT(,,371,160),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Update_DBStructure'),TIMER(100)
                       SHEET,AT(3,3,365,137),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           TEXT,AT(7,20,357,116),USE(LOC:Results),FONT('Arial',10,,FONT:regular,CHARSET:ANSI),VSCROLL, |
  BOXED,COLOR(00E9E9E9h),READONLY,SKIP
                         END
                       END
                       BUTTON('&Close'),AT(319,142,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                       BUTTON('&Help'),AT(3,142,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_DBStructure')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Results
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
      IF Setup{PROP:SQLDriver} = TRUE
         ! 1st file to access the DB..
         IF GETINI('Setup', 'Trusted_Connection', '1', '.\TransIS.INI') = TRUE
            SEND(_SQLTemp, '/TRUSTEDCONNECTION = TRUE' )
         .
      .
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_DBStructure',QuickWindow)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_DBStructure',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          !                       old 4
      
      !    IF CLIP(p_Qry) = ''
      !       ! ----------   Always Execute Section   -------------
      !       ! 15 May 06
      !       L_UQ:No                 = 0
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'DROP TABLE _SQLTemp'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No                 = 0
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'DROP TABLE Audit_ManagementProfit'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! Get_Setting
      !       ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip, p:Update)
      !       ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0),STRING
      !       !   1       2       3           4           5
      !       LOC:Settings_No  = Get_Setting('DB Structure Point', 1, '64', '@n_3','The point that this database has been updated to.')
      !
      !
      !       ! ----------   Once Execute Section   -------------
      !       L_UQ:No              = 65
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'ALTER TABLE TripSheets ADD TID INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 66
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'UPDATE TripSheets SET TID = 0 WHERE TID IS NULL'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 67
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'UPDATE TripSheets SET TID = (SELECT TID FROM VehicleComposition WHERE VehicleComposition.VCID = TripSheets.VCID)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 68
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'ALTER TABLE DeliveryStatuses ADD ReportColumn INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 69
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'UPDATE DeliveryStatuses SET ReportColumn = 0 WHERE ReportColumn IS NULL'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 70
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'ALTER TABLE Shortages_Damages ADD ConsigneeAID INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 71
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'UPDATE Shortages_Damages SET ConsigneeAID = 0 WHERE ConsigneeAID IS NULL'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 72
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'ALTER TABLE Shortages_Damages ADD DestClientReportedDamages TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 73
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'ALTER TABLE Shortages_Damages ADD DestClientReportedDamagesComments VARCHAR(255) DEFAULT <39><39>'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 74
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'UPDATE Shortages_Damages SET DestClientReportedDamages = 0 WHERE DestClientReportedDamages IS NULL'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 75
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'UPDATE Shortages_Damages SET DestClientReportedDamagesComments = <39><39> WHERE DestClientReportedDamagesComments IS NULL'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No              = 76
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'ALTER TABLE Reminders ADD Reoccurs TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 77
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'ALTER TABLE Reminders ADD Reoccur_Option TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 78
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'ALTER TABLE Reminders ADD Reoccur_Period INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 79
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command      = 'UPDATE Reminders SET Reoccurs = 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! 04 Jan 08
      !       L_UQ:No              = 80
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'ALTER TABLE VehicleComposition ADD Archived TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 81
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'UPDATE VehicleComposition SET Archived = 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! 26 Apr 08
      !       L_UQ:No              = 82
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'ALTER TABLE EmailAddresses ADD CID INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 83
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'UPDATE EmailAddresses SET CID = 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 84
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'ALTER TABLE EmailAddresses ADD DefaultAddress INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 85
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'UPDATE EmailAddresses SET DefaultAddress = 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 86
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'ALTER TABLE Clients ADD FuelSurchargeActive TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 87
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'UPDATE Clients SET FuelSurchargeActive = 1 WHERE CID IN ' & |
      !                                    '(SELECT TOP 1 CID FROM __RatesFuelSurcharge WHERE CID = Clients.CID)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 88
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'ALTER TABLE Clients DROP COLUMN FuelSurcharge'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 89
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'ALTER TABLE _Invoice ALTER COLUMN BranchName VARCHAR(35);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ClientName VARCHAR(100);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ClientLine1 VARCHAR(35);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ClientLine2 VARCHAR(35);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ClientSuburb VARCHAR(50);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ClientPostalCode VARCHAR(10);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN VATNo VARCHAR(20);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN InvoiceMessage VARCHAR(255);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ClientReference VARCHAR(60);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN MIDs VARCHAR(20);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ShipperName VARCHAR(35);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ShipperLine1 VARCHAR(35);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ShipperLine2 VARCHAR(35);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ShipperSuburb VARCHAR(50);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ShipperPostalCode VARCHAR(10);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ConsigneeName VARCHAR(35);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ConsigneeLine1 VARCHAR(35);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ConsigneeLine2 VARCHAR(35);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ConsigneeSuburb VARCHAR(50);<13,10>'&|
      !           'ALTER TABLE _Invoice ALTER COLUMN ConsigneePostalCode VARCHAR(10)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 90
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'UPDATE _Invoice SET BranchName =RTRIM(BranchName);<13,10>'&|
      !           'UPDATE _Invoice SET ClientName =RTRIM(ClientName);<13,10>'&|
      !           'UPDATE _Invoice SET ClientLine1 =RTRIM(ClientLine1);<13,10>'&|
      !           'UPDATE _Invoice SET ClientLine2 =RTRIM(ClientLine2);<13,10>'&|
      !           'UPDATE _Invoice SET ClientSuburb =RTRIM(ClientSuburb);<13,10>'&|
      !           'UPDATE _Invoice SET ClientPostalCode =RTRIM(ClientPostalCode);<13,10>'&|
      !           'UPDATE _Invoice SET VATNo =RTRIM(VATNo);<13,10>'&|
      !           'UPDATE _Invoice SET InvoiceMessage =RTRIM(InvoiceMessage);<13,10>'&|
      !           'UPDATE _Invoice SET ClientReference =RTRIM(ClientReference);<13,10>'&|
      !           'UPDATE _Invoice SET MIDs =RTRIM(MIDs);<13,10>'&|
      !           'UPDATE _Invoice SET ShipperName =RTRIM(ShipperName);<13,10>'&|
      !           'UPDATE _Invoice SET ShipperLine1 =RTRIM(ShipperLine1);<13,10>'&|
      !           'UPDATE _Invoice SET ShipperLine2 =RTRIM(ShipperLine2);<13,10>'&|
      !           'UPDATE _Invoice SET ShipperSuburb =RTRIM(ShipperSuburb);<13,10>'&|
      !           'UPDATE _Invoice SET ShipperPostalCode =RTRIM(ShipperPostalCode);<13,10>'&|
      !           'UPDATE _Invoice SET ConsigneeName =RTRIM(ConsigneeName);<13,10>'&|
      !           'UPDATE _Invoice SET ConsigneeLine1 =RTRIM(ConsigneeLine1);<13,10>'&|
      !           'UPDATE _Invoice SET ConsigneeLine2 =RTRIM(ConsigneeLine2);<13,10>'&|
      !           'UPDATE _Invoice SET ConsigneeSuburb =RTRIM(ConsigneeSuburb);<13,10>'&|
      !           'UPDATE _Invoice SET ConsigneePostalCode =RTRIM(ConsigneePostalCode)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 91
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'ALTER TABLE Deliveries ALTER COLUMN ClientReference VARCHAR(60);<13,10>'&|
      !           'ALTER TABLE Deliveries ALTER COLUMN SpecialDeliveryInstructions VARCHAR(255);<13,10>'&|
      !           'ALTER TABLE Deliveries ALTER COLUMN Notes VARCHAR(255)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 92
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'UPDATE Deliveries SET ClientReference=RTRIM(ClientReference);<13,10>'&|
      !           'UPDATE Deliveries SET SpecialDeliveryInstructions=RTRIM(SpecialDeliveryInstructions);<13,10>'&|
      !           'UPDATE Deliveries SET Notes=RTRIM(Notes)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 93
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'ALTER TABLE _InvoiceItems ALTER COLUMN Commodity VARCHAR(35);<13,10>'&|
      !           'ALTER TABLE _InvoiceItems ALTER COLUMN Description VARCHAR(150);<13,10>'&|
      !           'ALTER TABLE _InvoiceItems ALTER COLUMN ContainerDescription VARCHAR(150)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 94
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'UPDATE _InvoiceItems SET Commodity=RTRIM(Commodity);<13,10>'&|
      !           'UPDATE _InvoiceItems SET Description=RTRIM(Description);<13,10>'&|
      !           'UPDATE _InvoiceItems SET ContainerDescription=RTRIM(ContainerDescription)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No              = 95
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'ALTER TABLE ClientsPaymentsAllocation ALTER COLUMN Comment VARCHAR(255)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No              = 96
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'UPDATE ClientsPaymentsAllocation SET Comment=RTRIM(Comment)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No              = 97
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'ALTER TABLE DeliveryItems ALTER COLUMN ContainerNo VARCHAR(35);<13,10>'&|
      !           'ALTER TABLE DeliveryItems ALTER COLUMN ContainerVessel VARCHAR(35);<13,10>'&|
      !           'ALTER TABLE DeliveryItems ALTER COLUMN SealNo VARCHAR(35)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No              = 98
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'UPDATE DeliveryItems SET ContainerNo=RTRIM(ContainerNo);<13,10>'&|
      !           'UPDATE DeliveryItems SET ContainerVessel=RTRIM(ContainerVessel);<13,10>'&|
      !           'UPDATE DeliveryItems SET SealNo=RTRIM(SealNo)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No              = 99
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'ALTER TABLE Audit ALTER COLUMN AppSection VARCHAR(200);<13,10>'&|
      !           'ALTER TABLE Audit ALTER COLUMN Data1 VARCHAR(255);<13,10>'&|
      !           'ALTER TABLE Audit ALTER COLUMN Data2 VARCHAR(255)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No              = 100
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'UPDATE Audit SET AppSection=RTRIM(AppSection);<13,10>'&|
      !           'UPDATE Audit SET Data1=RTRIM(Data1);<13,10>'&|
      !           'UPDATE Audit SET Data2=RTRIM(Data2)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No              = 101
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'ALTER TABLE TripSheetDeliveries ALTER COLUMN Notes VARCHAR(255)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No              = 102
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'UPDATE TripSheetDeliveries SET Notes=RTRIM(Notes)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No              = 103
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'ALTER TABLE TripSheets ALTER COLUMN Notes VARCHAR(500)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No              = 104
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'UPDATE TripSheets SET Notes=RTRIM(Notes)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 105
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'ALTER TABLE _InvoiceTransporter ALTER COLUMN Comment VARCHAR(255);<13,10>'&|
      !           'ALTER TABLE _InvoiceTransporter ALTER COLUMN CIN VARCHAR(30)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No              = 106
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = |
      !           'UPDATE _InvoiceTransporter SET Comment=RTRIM(Comment);<13,10>'&|
      !           'UPDATE _InvoiceTransporter SET CIN=RTRIM(CIN)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       ! 26 May 08
      !       L_UQ:No              = 107
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'ALTER TABLE Drivers ADD Archived TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 108
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'UPDATE Drivers SET Archived = 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 109
      !       IF LOC:Settings_No < L_UQ:No
      !        !GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Drivers ADD Archived_DateTime DATETIME DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! 09 July 08
      !       L_UQ:No              = 110
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'ALTER TABLE Branches ADD GeneralFuelSurchargeClientID INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !    
      !       ! 05 Oct 08
      !       L_UQ:No              = 111
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'ALTER TABLE _FuelCost ADD FuelCostType INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 112
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'UPDATE _FuelCost SET FuelCostType = 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !
      !       IF ERRORCODE()
      !          MESSAGE('Q add error: ' & ERROR())
      !       .
      !
      !       LOC:Top_No              = L_UQ:No
      !    ELSE
      !       L_UQ:No                 = 0
      !       L_UQ:Command            = CLIP(p_Qry)
      !       ADD(LOC:Update_Commands_Q)
      !    .
      !                       old 3
      !       ! 10 Jan 07
      !       L_UQ:No                 = 46
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE Deliveries SET DELIVERED = 3 WHERE DIDATEANDTIME < <39>01/10/2006<39> AND DELIVERED < 2'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No                 = 47
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Addresses ALTER COLUMN Fax VARCHAR(61)'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 48
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _Invoice ADD IJID INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 49
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _Invoice ADD Created_DateTime DATETIME DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !       L_UQ:No                 = 50
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _Invoice SET BadDebt = 0 WHERE BadDebt IS NULL'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 51
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _Invoice ADD BadDebt2 INT'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 52
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _Invoice SET BadDebt2 = BadDebt'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 53
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _Invoice DROP COLUMN BadDebt'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 54
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _Invoice ADD BadDebt INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 55
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _Invoice SET BadDebt = BadDebt2'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 56
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _Invoice DROP COLUMN BadDebt2'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !
      !       L_UQ:No                 = 57
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _Invoice SET IJID = 0 WHERE IJID IS NULL'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 58
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _Invoice SET Created_DateTime = 0 WHERE Created_DateTime IS NULL'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 59
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _Invoice SET POD_IID = 0 WHERE POD_IID IS NULL'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 60
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _Invoice SET CR_IID = 0 WHERE CR_IID IS NULL'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 61
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _Invoice SET ClientNo = 0 WHERE ClientNo IS NULL'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 62
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _Invoice SET UID = 0 WHERE UID IS NULL'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 63
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _Invoice SET DC_ID = 0 WHERE DC_ID IS NULL'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      
      
      
      
      
      !                   old 2
      !       L_UQ:No                 = 31
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Application_Section_Usage DROP UQ__Application_Sect__53D770D6'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !       ! 11 Aug 06
      !       L_UQ:No                 = 32
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _InvoiceItems ADD ContainerDescription CHAR(150)'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !       ! 15 Aug 06
      !       L_UQ:No                 = 33
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE TripSheets ADD FID TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !       ! 02 Oct 06
      !       L_UQ:No                 = 34
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Deliveries ADD VATRate_OverriddenUserID INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !       ! 06 Oct 06
      !       L_UQ:No                 = 35
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE TripSheets ADD Assistant_DRID INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 36
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE TripSheets ADD Collections TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       
      !
      !       ! 10 Oct 06
      !       L_UQ:No                 = 37
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE TripSheets ADD SUID INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !       ! 19 Oct 06
      !       L_UQ:No                 = 38
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _InvoiceTransporter ADD CIN_DateTimeReceived DATETIME DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !       ! 22 Oct 06
      !       L_UQ:No                 = 39
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Reminders ADD Reference VARCHAR(36) DEFAULT <39><39>'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 40
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Reminders ADD SystemGenerated TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !       ! 08 Nov 06
      !       L_UQ:No                 = 41
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE DeliveryProgress ADD ActionDateAndTime DATETIME DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !       ! 10 Nov 06
      !       L_UQ:No                 = 42
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Floors ADD TripSheetLoading TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 43
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'Update Floors SET TripSheetLoading = 1'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !       ! 13 Nov 06
      !       L_UQ:No                 = 44
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Shortages_Damages ALTER COLUMN CorrectiveActionTaken VARCHAR(256)'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !       ! 28 Nov 06
      !       L_UQ:No                 = 45
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE DeliveryComposition ADD Reference VARCHAR(36) DEFAULT <39><39>'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      
      
      
      
      
      
      
      
      
      
      !                   old
      !    IF CLIP(p_Qry) = ''
      !       L_UQ:No                 = 1
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command         = 'ALTER TABLE Reminders ADD Created_UID INT'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No                 = 2
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Reminders ADD Created_DateTime DateTime NULL'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       ! 8 March 06
      !       L_UQ:No                 = 3
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE __Rates  SET CTID = 0 WHERE (CTID <> 0) AND (LTID NOT IN ' & |
      !                                '(SELECT LTID FROM loadtypes2 WHERE loadoption = 2))'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No                 = 4
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE __Rates  SET LTID = (SELECT LTID FROM clientsratetypes ' & |
      !                                'WHERE clientsratetypes.CRTID = __Rates.CRTID)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      
      
      !       ! 9 March 06
      !       L_UQ:No                 = 5
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Setup ADD TestDatabase TINYINT'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! 10 March 06
      !       L_UQ:No                 = 6
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Branches ADD GeneralRatesClientID INT'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No                 = 7
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Branches ADD GeneralRatesTransporterID INT'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! 15 March 06
      !       L_UQ:No                 = 8
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Deliveries ADD ReleasedUID INT'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No                 = 9
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Clients ADD UpdatedDateTime DateTime NULL, BalanceCurrent DECIMAL(11,2), ' & |
      !                                'Balance30Days DECIMAL(11,2), Balance60Days DECIMAL(11,2), Balance90Days DECIMAL(11,2)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! 28 March 06
      !       L_UQ:No                 = 10
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE Manifest SET Broking = 0 WHERE Broking IS NULL'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! 16 April 06
      !       L_UQ:No                 = 11
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _InvoiceTransporter ADD Broking_Manifest TINYINT'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No                 = 12
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _InvoiceTransporter SET Broking_Manifest = 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No                 = 13
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _InvoiceTransporter SET Broking_Manifest = 1 ' & |
      !                                'WHERE MID IN  (SELECT MID FROM Manifest WHERE Broking = 1)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! 22 May 06
      !       L_UQ:No                 = 14
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _InvoiceTransporter ADD Manifest TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No                 = 15
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _InvoiceTransporter SET Manifest = Broking_Manifest'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No                 = 16
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _InvoiceTransporter DROP COLUMN Broking_Manifest'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       
      !       ! 25 May 06
      !       L_UQ:No                 = 17
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _InvoiceTransporter SET ExtraInv = 0 WHERE ExtraInv IS NULL'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No                 = 18
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _InvoiceTransporter SET VAT_Specified = 0 WHERE VAT_Specified IS NULL'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No                 = 19
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _InvoiceTransporter SET CR_TIN = 0 WHERE CR_TIN IS NULL'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       ! Set all these invoices to "manifest"
      !       L_UQ:No                 = 20
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _InvoiceTransporter SET Manifest = 1 WHERE TIN IN ' & |
      !                        '(SELECT TIN  FROM _InvoiceTransporter '        & |
      !                        'WHERE     (MID IN '                            & |
      !                        ' (SELECT MID FROM _InvoiceTransporter AS a '   & |
      !                        ' WHERE     (MID IN '                           & |
      !                        '  (SELECT MID FROM Manifest '                  & |
      !                        '  WHERE     (COST = a.COST) AND (MID = a.MID))) AND (BROKING = BROKING) ' & |
      !                        '    GROUP BY MID '                             & |
      !                        '    HAVING      (COUNT(*) = 1))))'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      
      
      !       ! 30 June 06
      !       L_UQ:No                 = 21
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _Invoice SET MID = 0, MIDs = <39><39> WHERE CR_IID <> 0 AND CR_IID IS NOT NULL'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! 3 July 06
      !       L_UQ:No                 = 22
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE Deliveries SET ReleasedUID = 0 WHERE ReleasedUID IS NULL'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! 4 July 06
      !       L_UQ:No                 = 23
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _InvoiceTransporter ADD CIN CHAR(30) DEFAULT <39><39>'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No                 = 24
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'UPDATE _InvoiceTransporter SET CIN = <39><39>'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       ! 10 July 06
      !       L_UQ:No                 = 25
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _Invoice ADD DC_ID INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! 11 July 06
      !       L_UQ:No                 = 26
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE _Invoice ALTER COLUMN ClientReference CHAR(60)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       L_UQ:No                 = 27
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'DROP INDEX DEL_SKEY_CLIENTREFERENCE ON Deliveries'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !
      !       L_UQ:No                 = 28
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE Deliveries ALTER COLUMN ClientReference CHAR(60)'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      !       L_UQ:No                 = 29
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'CREATE NONCLUSTERED INDEX [DEL_SKEY_CLIENTREFERENCE] ON [dbo].[Deliveries] ' & |
      !                            '([CLIENTREFERENCE] ASC)'
      !          ADD(LOC:Update_Commands_Q)                               
      !       .
      
      
      !       L_UQ:No                 = 30
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'ALTER TABLE DeliveryItems ADD ShowOnInvoice TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      
      !                       old 5
      !    IF CLIP(p_Qry) = ''
      !       ! ----------   Always Execute Section   -------------
      !       ! 15 May 06
      !       L_UQ:No                 = 0
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'DROP TABLE _SQLTemp'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No                 = 0
      !       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !          L_UQ:Command        = 'DROP TABLE Audit_ManagementProfit'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       ! Get_Setting
      !       ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip, p:Update)
      !       ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0),STRING
      !       !   1       2       3           4           5
      !       LOC:Settings_No  = Get_Setting('DB Structure Point', 1, '64', '@n_3','The point that this database has been updated to.')
      !
      !
      !       ! ----------   Once Execute Section   -------------
      !
      !       ! 04 Feb 10
      !       L_UQ:No              = 113
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'ALTER TABLE __RatesFuelCost ADD CostBase_X DECIMAL(11,3)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 114
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'UPDATE __RatesFuelCost SET CostBase_X=0.0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !       L_UQ:No              = 115
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'ALTER TABLE Clients ADD PODMessage VARCHAR(255)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !
      !
      !       IF ERRORCODE()
      !          MESSAGE('Q add error: ' & ERROR())
      !       .
      !
      !       LOC:Top_No              = L_UQ:No
      !    ELSE
      !       L_UQ:No                 = 0
      !       L_UQ:Command            = CLIP(p_Qry)
      !       ADD(LOC:Update_Commands_Q)
      !    .
      !!                       old 6
      !!IF CLIP(p_Qry) = ''
      !!       ! ----------   Always Execute Section   -------------
      !!       ! 15 May 06
      !!       L_UQ:No                 = 0
      !!       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !!          L_UQ:Command        = 'DROP TABLE _SQLTemp'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!       L_UQ:No                 = 0
      !!       IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
      !!          L_UQ:Command        = 'DROP TABLE Audit_ManagementProfit'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!
      !!       ! Get_Setting
      !!       ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip, p:Update)
      !!       ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0),STRING
      !!       !   1       2       3           4           5
      !!       LOC:Settings_No  = Get_Setting('DB Structure Point', 1, '64', '@n_3','The point that this database has been updated to.')
      !!
      !!
      !!       ! ----------   Once Execute Section   -------------
      !!
      !!       ! 07 June 12
      !!       L_UQ:No              = 116
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE Addresses ADD Archived TINYINT DEFAULT 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!       L_UQ:No              = 117
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'UPDATE Addresses SET Archived = 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!       L_UQ:No              = 118
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE Addresses ADD Archived_DateTime DATETIME DEFAULT 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .      
      !!      
      !!       L_UQ:No              = 119
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE PackagingTypes ADD Archived TINYINT DEFAULT 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!       L_UQ:No              = 120
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'UPDATE PackagingTypes SET Archived = 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!       L_UQ:No              = 121
      !!       IF LOC:Settings_No < L_UQ:No        
      !!          L_UQ:Command        = 'ALTER TABLE PackagingTypes ADD Archived_DateTime DATETIME DEFAULT 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .      
      !!      
      !!       L_UQ:No              = 122
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE VehicleComposition ADD Archived_DateTime DATETIME DEFAULT 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .      
      !!
      !!       L_UQ:No              = 123
      !!       ! need to manually drop the index that already exists for unique names
      !!       ! need to manually run this in JHB and the drop above too
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'CREATE UNIQUE NONCLUSTERED INDEX UniqueNamesPerAddress ON [dbo].[AddressContacts] (	[CONTACTNAME] ASC,	[AID] ASC )'
      !!       .      
      !!      
      !!       L_UQ:No              = 124
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE EmailAddresses ADD Operations TINYINT DEFAULT 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!       L_UQ:No              = 125
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'UPDATE EmailAddresses SET Operations = 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!
      !!       L_UQ:No              = 126
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE EmailAddresses ADD OperationsReference VARCHAR(255)'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!       L_UQ:No              = 127
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'UPDATE EmailAddresses SET OperationsReference = <39><39>'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!
      !!       L_UQ:No              = 128
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE Transporter ADD Archived TINYINT DEFAULT 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!       L_UQ:No              = 129
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'UPDATE Transporter SET Archived = 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!       L_UQ:No              = 130
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE Transporter ADD Archived_DateTime DATETIME DEFAULT 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .      
      !!      
      !!       L_UQ:No              = 131
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE Journeys ALTER COLUMN Journey VARCHAR(701)'
      !!          ADD(LOC:Update_Commands_Q)                               
      !!       .      
      !!       L_UQ:No              = 132
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE Journeys ALTER COLUMN Description VARCHAR(1501)'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .      
      !!
      !!      
      !!       L_UQ:No              = 133
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE Clients ADD LiabilityCover TINYINT DEFAULT 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!       L_UQ:No              = 134
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'UPDATE Clients SET LiabilityCover = 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!      
      !!       L_UQ:No              = 135
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE Clients ADD LiabilityCrossBorder TINYINT DEFAULT 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!       L_UQ:No              = 136
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'UPDATE Clients SET LiabilityCrossBorder = 0'
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!      
      !!      
      !!       L_UQ:No              = 137     ! 27 Sep 13
      !!       IF LOC:Settings_No < L_UQ:No
      !!          L_UQ:Command        = 'ALTER TABLE Deliveries ADD NoticeEmailAddresses VARCHAR(1024)'                 
      !!          ADD(LOC:Update_Commands_Q)
      !!       .
      !!
      !!
      !       L_UQ:No              = 138     ! 05 Jan 14
      !       IF LOC:Settings_No < L_UQ:No      
      !          L_UQ:Command        = 'ALTER TABLE Branches ADD GeneralTollChargeClientID INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 139     ! 05 Jan 14
      !       IF LOC:Settings_No < L_UQ:No      
      !          L_UQ:Command        = 'ALTER TABLE Journeys ADD EToll INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 140     ! 05 Jan 14
      !       IF LOC:Settings_No < L_UQ:No      
      !          L_UQ:Command        = 'ALTER TABLE Deliveries ADD TollRate DECIMAL(11,3)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 141
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'UPDATE Deliveries SET TollRate=0.0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 142     ! 05 Jan 14
      !       IF LOC:Settings_No < L_UQ:No      
      !          L_UQ:Command        = 'ALTER TABLE Deliveries ADD TollCharge DECIMAL(11,2)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 143
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'UPDATE Deliveries SET TollCharge=0.0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 144
      !       IF LOC:Settings_No < L_UQ:No      
      !          L_UQ:Command        = 'ALTER TABLE Deliveries ADD Tolls INT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 145
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'UPDATE Deliveries SET Tolls=0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 146     ! 05 Jan 14
      !       IF LOC:Settings_No < L_UQ:No      
      !          L_UQ:Command        = 'ALTER TABLE _Invoice ADD TollCharge DECIMAL(11,2)'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 147
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'UPDATE _Invoice SET TollCharge=0.0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !     
      !        
      !       L_UQ:No              = 148
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'ALTER TABLE Clients ADD TollChargeActive TINYINT DEFAULT 0'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !       L_UQ:No              = 149
      !       IF LOC:Settings_No < L_UQ:No
      !          L_UQ:Command        = 'UPDATE Clients SET TollChargeActive = 1'
      !          ADD(LOC:Update_Commands_Q)
      !       .
      !        
      !      IF ERRORCODE()
      !          MESSAGE('Q add error: ' & ERROR())
      !       .
      !
      !       LOC:Top_No              = L_UQ:No
      !    ELSE
      !       L_UQ:No                 = 0
      !       L_UQ:Command            = CLIP(p_Qry)
      !       ADD(LOC:Update_Commands_Q)
      !    .
          IF CLIP(p_Qry) = ''
             ! ----------   Always Execute Section   -------------
             ! 15 May 06
             L_UQ:No                 = 0
             IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
                L_UQ:Command        = 'DROP TABLE _SQLTemp'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No                 = 0
             IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\DBUTrans.INI') = '0'
                L_UQ:Command        = 'DROP TABLE Audit_ManagementProfit'
                ADD(LOC:Update_Commands_Q)
             .
      
             ! Get_Setting
             ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip, p:Update)
             ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0),STRING
             !   1       2       3           4           5
             LOC:Settings_No  = Get_Setting('DB Structure Point', 1, '64', '@n_3','The point that this database has been updated to.')
      
      
             ! ----------   Once Execute Section   -------------
             L_UQ:No              = 138     ! 05 Jan 14
             IF LOC:Settings_No < L_UQ:No      
                L_UQ:Command        = 'ALTER TABLE Branches ADD GeneralTollChargeClientID INT DEFAULT 0'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 139     ! 05 Jan 14
             IF LOC:Settings_No < L_UQ:No      
                L_UQ:Command        = 'ALTER TABLE Journeys ADD EToll INT DEFAULT 0'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 140     ! 05 Jan 14
             IF LOC:Settings_No < L_UQ:No      
                L_UQ:Command        = 'ALTER TABLE Deliveries ADD TollRate DECIMAL(11,3)'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 141
             IF LOC:Settings_No < L_UQ:No
                L_UQ:Command        = 'UPDATE Deliveries SET TollRate=0.0'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 142     ! 05 Jan 14
             IF LOC:Settings_No < L_UQ:No      
                L_UQ:Command        = 'ALTER TABLE Deliveries ADD TollCharge DECIMAL(11,2)'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 143
             IF LOC:Settings_No < L_UQ:No
                L_UQ:Command        = 'UPDATE Deliveries SET TollCharge=0.0'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 144
             IF LOC:Settings_No < L_UQ:No      
                L_UQ:Command        = 'ALTER TABLE Deliveries ADD Tolls INT DEFAULT 0'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 145
             IF LOC:Settings_No < L_UQ:No
                L_UQ:Command        = 'UPDATE Deliveries SET Tolls=0'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 146     ! 05 Jan 14
             IF LOC:Settings_No < L_UQ:No      
                L_UQ:Command        = 'ALTER TABLE _Invoice ADD TollCharge DECIMAL(11,2)'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 147
             IF LOC:Settings_No < L_UQ:No
                L_UQ:Command        = 'UPDATE _Invoice SET TollCharge=0.0'
                ADD(LOC:Update_Commands_Q)
             .
           
              
             L_UQ:No              = 148
             IF LOC:Settings_No < L_UQ:No
                L_UQ:Command        = 'ALTER TABLE Clients ADD TollChargeActive TINYINT DEFAULT 0'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 149
             IF LOC:Settings_No < L_UQ:No
                L_UQ:Command        = 'UPDATE Clients SET TollChargeActive = 1'
                ADD(LOC:Update_Commands_Q)
             .
             ! 16 Feb 14
             L_UQ:No              = 150
             IF LOC:Settings_No < L_UQ:No          
                L_UQ:Command        = 'ALTER TABLE Manifest ADD TTID_FreightLiner INT DEFAULT 0'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 151
             IF LOC:Settings_No < L_UQ:No          
                L_UQ:Command        = 'ALTER TABLE Manifest ADD TTID_Trailer INT DEFAULT 0'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 152
             IF LOC:Settings_No < L_UQ:No          
                L_UQ:Command        = 'ALTER TABLE Manifest ADD TTID_SuperLink INT DEFAULT 0'
                ADD(LOC:Update_Commands_Q)
             .
              
             L_UQ:No              = 153
             IF LOC:Settings_No < L_UQ:No          
                L_UQ:Command        = 'ALTER TABLE Journeys ADD FID2 INT DEFAULT 0'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 154
             IF LOC:Settings_No < L_UQ:No          
                L_UQ:Command        = 'UPDATE Journeys SET FID2 = 0'
                ADD(LOC:Update_Commands_Q)
             .
               
             L_UQ:No              = 155
             IF LOC:Settings_No < L_UQ:No          
                L_UQ:Command        = 'ALTER TABLE Manifest ADD Tracking_BID INT DEFAULT 0'
                ADD(LOC:Update_Commands_Q)
             .
               
             L_UQ:No              = 156
             IF LOC:Settings_No < L_UQ:No          
                L_UQ:Command        = 'UPDATE Manifest SET Tracking_BID = 0'
                ADD(LOC:Update_Commands_Q)
             .
               
             L_UQ:No              = 157
             IF LOC:Settings_No < L_UQ:No          
                L_UQ:Command        = 'ALTER TABLE EmailAddresses ADD DefaultOnDI INT DEFAULT 0'
                ADD(LOC:Update_Commands_Q)
             .
             L_UQ:No              = 158                     ! 20.07.14
             IF LOC:Settings_No < L_UQ:No          
                L_UQ:Command        = 'UPDATE EmailAddresses SET DefaultOnDI = 0'
                ADD(LOC:Update_Commands_Q)
             .
               
             L_UQ:No              = 159
             IF LOC:Settings_No < L_UQ:No
                L_UQ:Command        = 'ALTER TABLE Transporter ADD Comments VARCHAR(1001)'
                ADD(LOC:Update_Commands_Q)
             .
      
             L_UQ:No              = 160
             IF LOC:Settings_No < L_UQ:No
                L_UQ:Command        = 'ALTER TABLE Transporter ADD Status TINYINT DEFAULT 0'
                ADD(LOC:Update_Commands_Q)
             .
      
             L_UQ:No              = 161
             IF LOC:Settings_No < L_UQ:No
                L_UQ:Command        = 'UPDATE Transporter SET Status = 0'
                ADD(LOC:Update_Commands_Q)
             .
               
               IF ERRORCODE()
                MESSAGE('Q add error: ' & ERROR())
             .
      
             LOC:Top_No              = L_UQ:No
          ELSE
             L_UQ:No                 = 0
             L_UQ:Command            = CLIP(p_Qry)
             ADD(LOC:Update_Commands_Q)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
          QuickWindow{PROP:Timer} = 0
      
          LOC:Idx += 1
          GET(LOC:Update_Commands_Q, LOC:Idx)
          IF ERRORCODE()
             IF ERRORCODE() ~= 30         ! Entry not found
                MESSAGE('Q get error: ' & ERROR())
             .
      
             IF L_UQ:No > LOC:Settings_No
                ! Get_Setting
                ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip, p:Update)
                ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0),STRING
                !   1       2       3           4           5
                LOC:Settings_No      = Get_Setting('DB Structure Point',, L_UQ:No,,, 1)
             .
      
             LOC:LogLine  = '---'
             Add_to_List(CLIP(LOC:LogLine), LOC:Results, '<13,10>',,,TRUE)
             Add_Log(CLIP(LOC:LogLine), '', 'TransIS_DB_Update.Log')
      
             LOC:LogLine  = 'Complete.'
             Add_to_List(CLIP(LOC:LogLine), LOC:Results, '<13,10>',,,TRUE)
             Add_Log(CLIP(LOC:LogLine), '', 'TransIS_DB_Update.Log')
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)  -  Global
             ! (STRING, STRING, <STRING>, BYTE=0, BYTE=0, BYTE=1, BYTE=1)
      
             IF LOC:Errors > 0
                MESSAGE('There were ' & LOC:Errors & ' errors, please review them','Results',ICON:Hand)
             .
          ELSE
             _SQLTemp{PROP:SQL}   = CLIP(L_UQ:Command)
             IF ERRORCODE()
                LOC:Errors       += 1
                Err_"             = FILEERROR()   
                  
      
                ! (p:Add, p:List, p:Delim, p:Option, p:Prefix, p:Beginning)
                LOC:LogLine  = 'Error on DB update (' & L_UQ:No & '/' & LOC:Top_No & '): "' & CLIP(L_UQ:Command) & '"'
                Add_to_List(CLIP(LOC:LogLine), LOC:Results, '<13,10>',,,TRUE)
                Add_Log(CLIP(LOC:LogLine), '', 'TransIS_DB_Update.Log')
      
                LOC:LogLine  = '**  File Error: "' & CLIP(Err_") & '"'
                Add_to_List(CLIP(LOC:LogLine), LOC:Results, '<13,10>',,,TRUE)
                Add_Log(CLIP(LOC:LogLine), '', 'TransIS_DB_Update.Log')
      
                LOC:LogLine  = '---'
                Add_to_List(CLIP(LOC:LogLine), LOC:Results, '<13,10>',,,TRUE)
                Add_Log(CLIP(LOC:LogLine), '', 'TransIS_DB_Update.Log')
             ELSE
                IF L_UQ:No = 85
                   Update_Change_EmailAddress_Relationship()
                .
      
                LOC:LogLine  = 'DB updated (' & L_UQ:No & '/' & LOC:Top_No & ')<13,10>"' & CLIP(L_UQ:Command) & '"'
                Add_to_List(CLIP(LOC:LogLine), LOC:Results, '<13,10>',,,TRUE)
                Add_Log(CLIP(LOC:LogLine), '', 'TransIS_DB_Update.Log')
      
                LOC:LogLine  = '---'
                Add_to_List(CLIP(LOC:LogLine), LOC:Results, '<13,10>',,,TRUE)
                Add_Log(CLIP(LOC:LogLine), '', 'TransIS_DB_Update.Log')
      
                IF L_UQ:No > 0
                   PUTINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '1', '.\DBUTrans.INI')
             .  .
      
             QuickWindow{PROP:Timer} = 100
          .
      
          DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! ** global **
!!! </summary>
Global_Assign        PROCEDURE                             ! Declare Procedure
LOC:Failed           BYTE                                  !
Tek_Failed_File     STRING(100)


  CODE

    IF Setup{PROP:SQLDriver} = TRUE
      IF GETINI('Setup', 'Trusted_Connection', '1', '.\TransIS.INI') = TRUE
         SEND(Setup, '/TRUSTEDCONNECTION = TRUE' )
      .

      GLO:DBOwner  = GETINI('Setup', 'DBOwner', 'TransIS;UID=sa;PWD=sa;DATABASE=TransIS', '.\TransIS.INI')
    .
    
!    Message('DBOwner: ' & GLO:DBOwner)
    ! Called in Global Startup
    GLO:Local_INI       = '.\TransISL.INI'

    ! General Globals - after file opens and use test
    GLO:ReplicatedDatabaseID    = GETINI('Replication_Setup', 'ReplicatedDatabaseID', , GLO:Global_INI)
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Exit
!!! <summary>
!!! Generated from procedure template - Splash
!!! </summary>
AboutWindow PROCEDURE 

Build_No             ULONG                                 !
Build_Date           LONG                                  !
Build_Time           LONG                                  !
Window               WINDOW('About...'),AT(,,227,124),FONT('MS Sans Serif',8,,FONT:regular),NOFRAME,CENTER,HLP('~AboutWindow'), |
  PALETTE(256)
                       IMAGE('InfoSolutions_Logo.bmp'),AT(5,4),USE(?Image),CENTERED
                       PROMPT('Build No.'),AT(6,40,87,10),USE(?Prompt_Build)
                       STRING('Copyright 2004 - 2012'),AT(111,40,110,10),USE(?CopyrightDate),TRN
                       PROMPT('Build_Date'),AT(6,54,87,10),USE(?Prompt_BDate)
                       STRING('Produced by InfoSolutions'),AT(111,54,110,10),USE(?Developer),TRN
                       PROMPT('Build_Time'),AT(6,68,87,10),USE(?Prompt_BTime)
                       STRING('for FBN Transport'),AT(111,68,110,10),USE(?AboutHeading),TRN
                       PANEL,AT(5,82,215,4),USE(?Panel:6),BEVEL(1,0,1536)
                       PROMPT('Warning:  This computer program is protected by copyright law and international' & |
  ' treaties.  Unauthorised reproduction or distribution of this program, or any portio' & |
  'n of it, may result in severe civil and criminal penalties. Known violators will be ' & |
  'prosecuted to the maximum extent possible under the law.'),AT(5,88,163,34),USE(?PromptCopyright), |
  FONT('Small Fonts',6)
                       BUTTON('&Close'),AT(175,104,45,14),USE(?Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AboutWindow')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
      ?Prompt_Build{Prop:Text} = 'Build No.: ' & ?Prompt_Build{Prop:Text}
      ?Prompt_BDate{Prop:Text} = 'Build Date: ' & ?Prompt_BDate{Prop:Text}
      ?Prompt_BTime{Prop:Text} = 'Build Time: ' & ?Prompt_BTime{Prop:Text}

      !Build_No        = ?Build_No{Prop:Text}
      !Build_Date      = DEFORMAT(?Build_Date{Prop:Text}, @d6)
      !Build_Time      = DEFORMAT(?Build_Time{Prop:Text}, @t7)

  
!      Str_1"          = EQUATE:BuildTime
!      Str_"           = ''
!      LOOP
!         Str_"        = CLIP(Str_") & Get_1st_Element_From_Delim_Str(Str_1", ' ', TRUE)
!         IF CLIP(Str_1") = ''
!            BREAK
!      .  .
  
!      Build_Time    = DEFORMAT(Str_", @t3)
  
      ! EQUATE:BuildDate   EQUATE('06/02/2005')
      ! EQUATE:BuildTime   EQUATE(' 8:19 PM')


	?CopyrightDate{PROP:Text}	= 'Copyright 2004 - ' & YEAR(TODAY())
  INIMgr.Fetch('AboutWindow',Window)                       ! Restore window settings from non-volatile store
  TARGET{Prop:Timer} = 0                                   ! Close window on timer event, so configure timer
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('AboutWindow',Window)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:LoseFocus
        POST(Event:CloseWindow)                            ! Splash window will close when focus is lost
    OF Event:Timer
      POST(Event:CloseWindow)                              ! Splash window will close on event timer
    ELSE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

