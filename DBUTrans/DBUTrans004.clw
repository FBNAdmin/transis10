

   MEMBER('DBUTrans.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DBUTRANS004.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('DBUTRANS005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('DBUTRANS013.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Select a Branches Record
!!! </summary>
SelectBranches PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Branches)
                       PROJECT(BRA:BID)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:GeneralRatesClientID)
                       PROJECT(BRA:GeneralRatesTransporterID)
                       PROJECT(BRA:AID)
                       PROJECT(BRA:FID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
BRA:BID                LIKE(BRA:BID)                  !List box control field - type derived from field
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:GeneralRatesClientID LIKE(BRA:GeneralRatesClientID) !List box control field - type derived from field
BRA:GeneralRatesTransporterID LIKE(BRA:GeneralRatesTransporterID) !List box control field - type derived from field
BRA:AID                LIKE(BRA:AID)                  !Browse key field - type derived from field
BRA:FID                LIKE(BRA:FID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Branches Record'),AT(,,328,198),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('SelectBranches'),SYSTEM
                       LIST,AT(8,30,312,124),USE(?Browse:1),HVSCROLL,FORMAT('80R(2)|M~BID~C(0)@n_10@80L(2)|M~B' & |
  'ranch Name~L(2)@s35@80R(2)|M~CID~C(0)@n_10@80R(2)|M~TID~C(0)@n_10@'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the Branches file')
                       BUTTON('&Select'),AT(271,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,320,172),USE(?CurrentTab)
                         TAB('&1) By Branch Name'),USE(?Tab:2)
                         END
                         TAB('&2) By Address ID'),USE(?Tab:3)
                         END
                         TAB('&3) By Floor'),USE(?Tab:4)
                         END
                       END
                       BUTTON('&Close'),AT(222,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(275,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectBranches')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Branches,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,BRA:FKey_AID)                         ! Add the sort order for BRA:FKey_AID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,BRA:AID,1,BRW1)                ! Initialize the browse locator using  using key: BRA:FKey_AID , BRA:AID
  BRW1.AddSortOrder(,BRA:FKey_FID)                         ! Add the sort order for BRA:FKey_FID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,BRA:FID,1,BRW1)                ! Initialize the browse locator using  using key: BRA:FKey_FID , BRA:FID
  BRW1.AddSortOrder(,BRA:Key_BranchName)                   ! Add the sort order for BRA:Key_BranchName for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(,BRA:BranchName,1,BRW1)         ! Initialize the browse locator using  using key: BRA:Key_BranchName , BRA:BranchName
  BRW1.AddField(BRA:BID,BRW1.Q.BRA:BID)                    ! Field BRA:BID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:BranchName,BRW1.Q.BRA:BranchName)      ! Field BRA:BranchName is a hot field or requires assignment from browse
  BRW1.AddField(BRA:GeneralRatesClientID,BRW1.Q.BRA:GeneralRatesClientID) ! Field BRA:GeneralRatesClientID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:GeneralRatesTransporterID,BRW1.Q.BRA:GeneralRatesTransporterID) ! Field BRA:GeneralRatesTransporterID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:AID,BRW1.Q.BRA:AID)                    ! Field BRA:AID is a hot field or requires assignment from browse
  BRW1.AddField(BRA:FID,BRW1.Q.BRA:FID)                    ! Field BRA:FID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('SelectBranches',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('SelectBranches',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! Select a Addresses Record
!!! </summary>
SelectAddresses PROCEDURE 

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(Addresses)
                       PROJECT(ADD:BID)
                       PROJECT(ADD:AddressName)
                       PROJECT(ADD:AddressNameSuburb)
                       PROJECT(ADD:Line1)
                       PROJECT(ADD:Line2)
                       PROJECT(ADD:PhoneNo)
                       PROJECT(ADD:PhoneNo2)
                       PROJECT(ADD:Fax)
                       PROJECT(ADD:ShowForAllBranches)
                       PROJECT(ADD:AID)
                       PROJECT(ADD:SUID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ADD:BID                LIKE(ADD:BID)                  !List box control field - type derived from field
ADD:AddressName        LIKE(ADD:AddressName)          !List box control field - type derived from field
ADD:AddressNameSuburb  LIKE(ADD:AddressNameSuburb)    !List box control field - type derived from field
ADD:Line1              LIKE(ADD:Line1)                !List box control field - type derived from field
ADD:Line2              LIKE(ADD:Line2)                !List box control field - type derived from field
ADD:PhoneNo            LIKE(ADD:PhoneNo)              !List box control field - type derived from field
ADD:PhoneNo2           LIKE(ADD:PhoneNo2)             !List box control field - type derived from field
ADD:Fax                LIKE(ADD:Fax)                  !List box control field - type derived from field
ADD:ShowForAllBranches LIKE(ADD:ShowForAllBranches)   !List box control field - type derived from field
ADD:AID                LIKE(ADD:AID)                  !Primary key field - type derived from field
ADD:SUID               LIKE(ADD:SUID)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a Addresses Record'),AT(,,358,198),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('SelectAddresses'),SYSTEM
                       LIST,AT(8,30,342,124),USE(?Browse:1),HVSCROLL,FORMAT('80R(2)|M~BID~C(0)@n_10@80L(2)|M~A' & |
  'ddress Name~L(2)@s35@80L(2)|M~Address Name Suburb~L(2)@s50@80L(2)|M~Line 1~L(2)@s35@' & |
  '80L(2)|M~Line 2~L(2)@s35@80L(2)|M~Phone No~L(2)@s20@80L(2)|M~Phone No 2~L(2)@s20@80L' & |
  '(2)|M~Fax~L(2)@s20@80R(6)|M~Show for all Branches~C(0)@n3@'),FROM(Queue:Browse:1),IMM,MSG('Browsing t' & |
  'he Addresses file')
                       BUTTON('&Select'),AT(301,158,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) By Name'),USE(?Tab:2)
                         END
                         TAB('&2) By Suburb'),USE(?Tab:3)
                         END
                         TAB('&3) By Branch'),USE(?Tab:4)
                         END
                       END
                       BUTTON('&Close'),AT(252,180,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(305,180,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW1::Sort1:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                      ! Conditional Locator - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectAddresses')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Addresses,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,ADD:FKey_SUID)                        ! Add the sort order for ADD:FKey_SUID for sort order 1
  BRW1.AddLocator(BRW1::Sort1:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort1:Locator.Init(,ADD:SUID,1,BRW1)               ! Initialize the browse locator using  using key: ADD:FKey_SUID , ADD:SUID
  BRW1.AddSortOrder(,ADD:FKey_BID)                         ! Add the sort order for ADD:FKey_BID for sort order 2
  BRW1.AddLocator(BRW1::Sort2:Locator)                     ! Browse has a locator for sort order 2
  BRW1::Sort2:Locator.Init(,ADD:BID,1,BRW1)                ! Initialize the browse locator using  using key: ADD:FKey_BID , ADD:BID
  BRW1.AddSortOrder(,ADD:Key_Name)                         ! Add the sort order for ADD:Key_Name for sort order 3
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 3
  BRW1::Sort0:Locator.Init(,ADD:AddressName,1,BRW1)        ! Initialize the browse locator using  using key: ADD:Key_Name , ADD:AddressName
  BRW1.AddField(ADD:BID,BRW1.Q.ADD:BID)                    ! Field ADD:BID is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AddressName,BRW1.Q.ADD:AddressName)    ! Field ADD:AddressName is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AddressNameSuburb,BRW1.Q.ADD:AddressNameSuburb) ! Field ADD:AddressNameSuburb is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Line1,BRW1.Q.ADD:Line1)                ! Field ADD:Line1 is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Line2,BRW1.Q.ADD:Line2)                ! Field ADD:Line2 is a hot field or requires assignment from browse
  BRW1.AddField(ADD:PhoneNo,BRW1.Q.ADD:PhoneNo)            ! Field ADD:PhoneNo is a hot field or requires assignment from browse
  BRW1.AddField(ADD:PhoneNo2,BRW1.Q.ADD:PhoneNo2)          ! Field ADD:PhoneNo2 is a hot field or requires assignment from browse
  BRW1.AddField(ADD:Fax,BRW1.Q.ADD:Fax)                    ! Field ADD:Fax is a hot field or requires assignment from browse
  BRW1.AddField(ADD:ShowForAllBranches,BRW1.Q.ADD:ShowForAllBranches) ! Field ADD:ShowForAllBranches is a hot field or requires assignment from browse
  BRW1.AddField(ADD:AID,BRW1.Q.ADD:AID)                    ! Field ADD:AID is a hot field or requires assignment from browse
  BRW1.AddField(ADD:SUID,BRW1.Q.ADD:SUID)                  ! Field ADD:SUID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('SelectAddresses',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('SelectAddresses',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Manifest PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
BRW2::View:Browse    VIEW(TransporterPaymentsAllocations)
                       PROJECT(TRAPA:TRPAID)
                       PROJECT(TRAPA:TPID)
                       PROJECT(TRAPA:AllocationNo)
                       PROJECT(TRAPA:AllocationDate)
                       PROJECT(TRAPA:AllocationTime)
                       PROJECT(TRAPA:Amount)
                       PROJECT(TRAPA:Comment)
                       PROJECT(TRAPA:MID)
                       PROJECT(TRAPA:StatusUpToDate)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
TRAPA:TRPAID           LIKE(TRAPA:TRPAID)             !List box control field - type derived from field
TRAPA:TPID             LIKE(TRAPA:TPID)               !List box control field - type derived from field
TRAPA:AllocationNo     LIKE(TRAPA:AllocationNo)       !List box control field - type derived from field
TRAPA:AllocationDate   LIKE(TRAPA:AllocationDate)     !List box control field - type derived from field
TRAPA:AllocationTime   LIKE(TRAPA:AllocationTime)     !List box control field - type derived from field
TRAPA:Amount           LIKE(TRAPA:Amount)             !List box control field - type derived from field
TRAPA:Comment          LIKE(TRAPA:Comment)            !List box control field - type derived from field
TRAPA:MID              LIKE(TRAPA:MID)                !List box control field - type derived from field
TRAPA:StatusUpToDate   LIKE(TRAPA:StatusUpToDate)     !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW13::View:Browse   VIEW(ManifestLoadDeliveries)
                       PROJECT(MALD:MLDID)
                       PROJECT(MALD:DIID)
                       PROJECT(MALD:UnitsLoaded)
                       PROJECT(MALD:MLID)
                       JOIN(DELI:PKey_DIID,MALD:DIID)
                         PROJECT(DELI:Units)
                         PROJECT(DELI:DID)
                         PROJECT(DELI:ItemNo)
                         PROJECT(DELI:Weight)
                         PROJECT(DELI:DIID)
                         JOIN(DEL:PKey_DID,DELI:DID)
                           PROJECT(DEL:DINo)
                           PROJECT(DEL:DID)
                         END
                       END
                     END
Queue:Browse_ManLoadDel QUEUE                         !Queue declaration for browse/combo box using ?List_ManLoadDel
MALD:MLDID             LIKE(MALD:MLDID)               !List box control field - type derived from field
MALD:DIID              LIKE(MALD:DIID)                !List box control field - type derived from field
MALD:UnitsLoaded       LIKE(MALD:UnitsLoaded)         !List box control field - type derived from field
DELI:Units             LIKE(DELI:Units)               !List box control field - type derived from field
DELI:DID               LIKE(DELI:DID)                 !List box control field - type derived from field
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Weight            LIKE(DELI:Weight)              !List box control field - type derived from field
MALD:MLID              LIKE(MALD:MLID)                !List box control field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Related join file key field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(_InvoiceTransporter)
                       PROJECT(INT:TIN)
                       PROJECT(INT:TID)
                       PROJECT(INT:BID)
                       PROJECT(INT:MID)
                       PROJECT(INT:DINo)
                       PROJECT(INT:DID)
                       PROJECT(INT:Leg)
                       PROJECT(INT:DLID)
                       PROJECT(INT:Cost)
                       PROJECT(INT:VAT)
                       PROJECT(INT:Rate)
                       PROJECT(INT:VATRate)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
INT:TIN                LIKE(INT:TIN)                  !List box control field - type derived from field
INT:TID                LIKE(INT:TID)                  !List box control field - type derived from field
INT:BID                LIKE(INT:BID)                  !List box control field - type derived from field
INT:MID                LIKE(INT:MID)                  !List box control field - type derived from field
INT:DINo               LIKE(INT:DINo)                 !List box control field - type derived from field
INT:DID                LIKE(INT:DID)                  !List box control field - type derived from field
INT:Leg                LIKE(INT:Leg)                  !List box control field - type derived from field
INT:DLID               LIKE(INT:DLID)                 !List box control field - type derived from field
INT:Cost               LIKE(INT:Cost)                 !List box control field - type derived from field
INT:VAT                LIKE(INT:VAT)                  !List box control field - type derived from field
INT:Rate               LIKE(INT:Rate)                 !List box control field - type derived from field
INT:VATRate            LIKE(INT:VATRate)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(ManifestLoad)
                       PROJECT(MAL:MLID)
                       PROJECT(MAL:TTID)
                       PROJECT(MAL:MID)
                     END
Queue:Browse:ManLoad QUEUE                            !Queue declaration for browse/combo box using ?Browse:ManLoad
MAL:MLID               LIKE(MAL:MLID)                 !List box control field - type derived from field
MAL:TTID               LIKE(MAL:TTID)                 !List box control field - type derived from field
MAL:MID                LIKE(MAL:MID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::MAN:Record  LIKE(MAN:RECORD),THREAD
QuickWindow          WINDOW('Form Manifest'),AT(,,374,202),FONT('MS Sans Serif',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateManifest'),SYSTEM
                       SHEET,AT(4,4,367,176),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('MID:'),AT(8,36),USE(?MAN:MID:Prompt:2),TRN
                           ENTRY(@n_10),AT(64,36,60,10),USE(MAN:MID),RIGHT(1),MSG('Manifest ID')
                           BUTTON('Get Next ID'),AT(248,36,49,14),USE(?Button_ID)
                           PROMPT('BID:'),AT(8,54),USE(?MAN:BID:Prompt:2),TRN
                           ENTRY(@n_10),AT(64,54,60,10),USE(MAN:BID),RIGHT(1),MSG('Branch ID')
                           PROMPT('TID:'),AT(8,68),USE(?MAN:TID:Prompt:2),TRN
                           ENTRY(@n_10),AT(64,68,60,10),USE(MAN:TID),RIGHT(1),MSG('Transporter ID')
                           BUTTON('Do Child IDs'),AT(248,66,49,14),USE(?Button14)
                           PROMPT('Cost:'),AT(8,82),USE(?MAN:Cost:Prompt),TRN
                           ENTRY(@n-14.2),AT(64,82,60,10),USE(MAN:Cost),RIGHT(1)
                           PROMPT('Rate:'),AT(8,96),USE(?MAN:Rate:Prompt),TRN
                           ENTRY(@n-13.4),AT(64,96,60,10),USE(MAN:Rate),RIGHT(1),MSG('Rate per Kg'),TIP('Rate per Kg')
                           PROMPT('VAT Rate:'),AT(8,110),USE(?MAN:VATRate:Prompt),TRN
                           ENTRY(@n-7.2),AT(64,110,60,10),USE(MAN:VATRate),RIGHT(1),MSG('VAT rate'),TIP('VAT rate')
                           PROMPT('State:'),AT(8,124),USE(?MAN:State:Prompt),TRN
                           LIST,AT(64,124,93,10),USE(MAN:State),DROP(5),FROM('Loading|#0|Loaded|#1|On Route|#2|Tra' & |
  'nsferred|#3'),MSG('State of this manifest'),TIP('State of this manifest')
                           PROMPT('Created Date:'),AT(8,138),USE(?MAN:CreatedDate:Prompt),TRN
                           ENTRY(@d6),AT(64,138,60,10),USE(MAN:CreatedDate),MSG('Created Date'),TIP('Created Date')
                           PROMPT('Created Time:'),AT(8,152),USE(?MAN:CreatedTime:Prompt),TRN
                           ENTRY(@t7),AT(64,152,60,10),USE(MAN:CreatedTime)
                           PROMPT('Depart Date:'),AT(8,166),USE(?MAN:DepartDate:Prompt),TRN
                           SPIN(@d6),AT(64,166,60,10),USE(MAN:DepartDate),MSG('Depated Date'),TIP('Depated Date')
                           PROMPT('Depart Time:'),AT(192,126),USE(?MAN:DepartTime:Prompt),TRN
                           ENTRY(@t7),AT(248,126,60,10),USE(MAN:DepartTime),MSG('Departed Time'),TIP('Departed Time')
                           PROMPT('ETA Date:'),AT(192,140),USE(?MAN:ETADate:Prompt),TRN
                           SPIN(@d6),AT(248,140,60,10),USE(MAN:ETADate),MSG('Estimated arrival date'),TIP('Estimated ' & |
  'arrival date')
                           PROMPT('ETA Time:'),AT(192,154),USE(?MAN:ETATime:Prompt),TRN
                           ENTRY(@t7),AT(248,154,60,10),USE(MAN:ETATime),MSG('Estimated arrival time'),TIP('Estimated ' & |
  'arrival time')
                           CHECK('Broking'),AT(248,168,70,8),USE(MAN:Broking),MSG('Broking Manifest'),TIP('Broking Manifest')
                         END
                         TAB('&2) Manifest Load'),USE(?Tab:5)
                           LIST,AT(8,24,101,118),USE(?Browse:ManLoad),HVSCROLL,FORMAT('40R(2)|M~MLID~C(0)@n_10@40R' & |
  '(2)|M~TTID~C(0)@n_10@'),FROM(Queue:Browse:ManLoad),IMM,MSG('Browsing the ManifestLoad file')
                           LINE,AT(114,22,0,154),USE(?Line1),COLOR(COLOR:Black)
                           LIST,AT(120,24,246,133),USE(?List_ManLoadDel),FORMAT('40R(2)|M~MLDID~L@n_10@40R(2)|M~DI' & |
  'ID~L@n_10@28R(2)|M~Units Loaded~L@n6@28R(2)|M~Units~L@n6@40R(2)|M~DID~L@n_10@40R(2)|' & |
  'M~DI No.~L@n_10@26R(2)|M~Item No.~L@n_5@44R(2)|M~Weight~L@n-11.2@40R(2)|M~MLID~L@n_10@'), |
  FROM(Queue:Browse_ManLoadDel),IMM,MSG('Browsing Records')
                           BUTTON('&Insert'),AT(8,146,49,14),USE(?Insert:7),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(62,146,49,14),USE(?Change:7),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(8,162,49,14),USE(?Delete:7),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                           BUTTON('Open DI'),AT(120,162,49,14),USE(?Button_DI)
                           BUTTON('Open DIID'),AT(172,162,49,14),USE(?Button_DI:2)
                           BUTTON('&Insert'),AT(240,162,42,14),USE(?Insert)
                           BUTTON('&Change'),AT(282,162,42,14),USE(?Change)
                           BUTTON('&Delete'),AT(324,162,42,14),USE(?Delete)
                         END
                         TAB('&3) Transporter Payments Allocations'),USE(?Tab:3)
                           LIST,AT(8,40,358,118),USE(?Browse:2),HVSCROLL,FORMAT('80R(2)|M~TRPAID~C(0)@n_10@80R(2)|' & |
  'M~TPID~C(0)@n_10@80R(2)|M~Allocation No~C(0)@n_5@80R(2)|M~Allocation Date~C(0)@d17@8' & |
  '0R(2)|M~Allocation Time~C(0)@t7@64D(26)|M~Amount~C(0)@n-14.2@80L(2)|M~Comment~L(2)@s' & |
  '255@80R(2)|M~MID~C(0)@n_10@72R(2)|M~Status Up To Date~C(0)@n3@'),FROM(Queue:Browse:2),IMM, |
  MSG('Browsing the ManifestLoad file')
                           BUTTON('&Insert'),AT(28,162,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(81,162,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(134,162,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                         TAB('&4) Invoice Transporter'),USE(?Tab:4)
                           LIST,AT(8,24,359,134),USE(?Browse:4),HVSCROLL,FORMAT('40R(2)|M~TIN~C(0)@n_10@40R(2)|M~T' & |
  'ID~C(0)@n_10@40R(2)|M~BID~C(0)@n_10@40R(2)|M~MID~C(0)@n_10@40R(2)|M~DI No.~C(0)@n_10' & |
  '@40R(2)|M~DID~C(0)@n_10@24R(2)|M~Leg~C(0)@n5@40R(2)|M~DLID~C(0)@n_10@52R(1)|M~Cost~C' & |
  '(0)@n-14.2@52R(1)|M~VAT~C(0)@n-14.2@52R(1)|M~Rate~C(0)@n-13.4@36R(1)|M~VAT Rate~C(0)@n-7.2@'), |
  FROM(Queue:Browse:4),IMM,MSG('Browsing the ManifestLoad file')
                           BUTTON('&Insert'),AT(212,162,49,14),USE(?Insert:5),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                           BUTTON('&Change'),AT(264,162,49,14),USE(?Change:5),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(318,162,49,14),USE(?Delete:5),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                         END
                       END
                       BUTTON('&OK'),AT(216,184,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(268,184,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(322,184,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)                    ! Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW_ManLoadDel       CLASS(BrowseClass)                    ! Browse using ?List_ManLoadDel
Q                      &Queue:Browse_ManLoadDel       !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW13::Sort0:Locator StepLocatorClass                      ! Default Locator
BRW4                 CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW_ManLoad          CLASS(BrowseClass)                    ! Browse using ?Browse:ManLoad
Q                      &Queue:Browse:ManLoad          !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Manifest'
  OF InsertRecord
    ActionMessage = 'Manifest Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Manifest Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Manifest')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?MAN:MID:Prompt:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:Manifest)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(MAN:Record,History::MAN:Record)
  SELF.AddHistoryField(?MAN:MID,1)
  SELF.AddHistoryField(?MAN:BID,2)
  SELF.AddHistoryField(?MAN:TID,3)
  SELF.AddHistoryField(?MAN:Cost,8)
  SELF.AddHistoryField(?MAN:Rate,9)
  SELF.AddHistoryField(?MAN:VATRate,10)
  SELF.AddHistoryField(?MAN:State,11)
  SELF.AddHistoryField(?MAN:CreatedDate,14)
  SELF.AddHistoryField(?MAN:CreatedTime,15)
  SELF.AddHistoryField(?MAN:DepartDate,18)
  SELF.AddHistoryField(?MAN:DepartTime,19)
  SELF.AddHistoryField(?MAN:ETADate,22)
  SELF.AddHistoryField(?MAN:ETATime,23)
  SELF.AddHistoryField(?MAN:Broking,25)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Manifest.SetOpenRelated()
  Relate:Manifest.Open                                     ! File Manifest used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Manifest
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:TransporterPaymentsAllocations,SELF) ! Initialize the browse manager
  BRW_ManLoadDel.Init(?List_ManLoadDel,Queue:Browse_ManLoadDel.ViewPosition,BRW13::View:Browse,Queue:Browse_ManLoadDel,Relate:ManifestLoadDeliveries,SELF) ! Initialize the browse manager
  BRW4.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:_InvoiceTransporter,SELF) ! Initialize the browse manager
  BRW_ManLoad.Init(?Browse:ManLoad,Queue:Browse:ManLoad.ViewPosition,BRW6::View:Browse,Queue:Browse:ManLoad,Relate:ManifestLoad,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?Button_ID)
    DISABLE(?Button14)
    ?MAN:Cost{PROP:ReadOnly} = True
    ?MAN:Rate{PROP:ReadOnly} = True
    ?MAN:VATRate{PROP:ReadOnly} = True
    DISABLE(?MAN:State)
    ?MAN:CreatedDate{PROP:ReadOnly} = True
    ?MAN:CreatedTime{PROP:ReadOnly} = True
    ?MAN:DepartTime{PROP:ReadOnly} = True
    ?MAN:ETATime{PROP:ReadOnly} = True
    DISABLE(?Insert:7)
    DISABLE(?Change:7)
    DISABLE(?Delete:7)
    DISABLE(?Button_DI)
    DISABLE(?Button_DI:2)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    DISABLE(?Insert:5)
    DISABLE(?Change:5)
    DISABLE(?Delete:5)
  END
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,TRAPA:FKey_MID)                       ! Add the sort order for TRAPA:FKey_MID for sort order 1
  BRW2.AddRange(TRAPA:MID,Relate:TransporterPaymentsAllocations,Relate:Manifest) ! Add file relationship range limit for sort order 1
  BRW2.AddField(TRAPA:TRPAID,BRW2.Q.TRAPA:TRPAID)          ! Field TRAPA:TRPAID is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:TPID,BRW2.Q.TRAPA:TPID)              ! Field TRAPA:TPID is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:AllocationNo,BRW2.Q.TRAPA:AllocationNo) ! Field TRAPA:AllocationNo is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:AllocationDate,BRW2.Q.TRAPA:AllocationDate) ! Field TRAPA:AllocationDate is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:AllocationTime,BRW2.Q.TRAPA:AllocationTime) ! Field TRAPA:AllocationTime is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:Amount,BRW2.Q.TRAPA:Amount)          ! Field TRAPA:Amount is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:Comment,BRW2.Q.TRAPA:Comment)        ! Field TRAPA:Comment is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:MID,BRW2.Q.TRAPA:MID)                ! Field TRAPA:MID is a hot field or requires assignment from browse
  BRW2.AddField(TRAPA:StatusUpToDate,BRW2.Q.TRAPA:StatusUpToDate) ! Field TRAPA:StatusUpToDate is a hot field or requires assignment from browse
  BRW_ManLoadDel.Q &= Queue:Browse_ManLoadDel
  BRW_ManLoadDel.FileLoaded = 1                            ! This is a 'file loaded' browse
  BRW_ManLoadDel.AddSortOrder(,MALD:FSKey_MLID_DIID)       ! Add the sort order for MALD:FSKey_MLID_DIID for sort order 1
  BRW_ManLoadDel.AddRange(MALD:MLID,Relate:ManifestLoadDeliveries,Relate:ManifestLoad) ! Add file relationship range limit for sort order 1
  BRW_ManLoadDel.AddLocator(BRW13::Sort0:Locator)          ! Browse has a locator for sort order 1
  BRW13::Sort0:Locator.Init(,MALD:DIID,1,BRW_ManLoadDel)   ! Initialize the browse locator using  using key: MALD:FSKey_MLID_DIID , MALD:DIID
  BRW_ManLoadDel.AddResetField(MAL:MLID)                   ! Apply the reset field
  BRW_ManLoadDel.AddField(MALD:MLDID,BRW_ManLoadDel.Q.MALD:MLDID) ! Field MALD:MLDID is a hot field or requires assignment from browse
  BRW_ManLoadDel.AddField(MALD:DIID,BRW_ManLoadDel.Q.MALD:DIID) ! Field MALD:DIID is a hot field or requires assignment from browse
  BRW_ManLoadDel.AddField(MALD:UnitsLoaded,BRW_ManLoadDel.Q.MALD:UnitsLoaded) ! Field MALD:UnitsLoaded is a hot field or requires assignment from browse
  BRW_ManLoadDel.AddField(DELI:Units,BRW_ManLoadDel.Q.DELI:Units) ! Field DELI:Units is a hot field or requires assignment from browse
  BRW_ManLoadDel.AddField(DELI:DID,BRW_ManLoadDel.Q.DELI:DID) ! Field DELI:DID is a hot field or requires assignment from browse
  BRW_ManLoadDel.AddField(DEL:DINo,BRW_ManLoadDel.Q.DEL:DINo) ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW_ManLoadDel.AddField(DELI:ItemNo,BRW_ManLoadDel.Q.DELI:ItemNo) ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW_ManLoadDel.AddField(DELI:Weight,BRW_ManLoadDel.Q.DELI:Weight) ! Field DELI:Weight is a hot field or requires assignment from browse
  BRW_ManLoadDel.AddField(MALD:MLID,BRW_ManLoadDel.Q.MALD:MLID) ! Field MALD:MLID is a hot field or requires assignment from browse
  BRW_ManLoadDel.AddField(DELI:DIID,BRW_ManLoadDel.Q.DELI:DIID) ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW_ManLoadDel.AddField(DEL:DID,BRW_ManLoadDel.Q.DEL:DID) ! Field DEL:DID is a hot field or requires assignment from browse
  BRW4.Q &= Queue:Browse:4
  BRW4.AddSortOrder(,INT:Fkey_MID)                         ! Add the sort order for INT:Fkey_MID for sort order 1
  BRW4.AddRange(INT:MID,Relate:_InvoiceTransporter,Relate:Manifest) ! Add file relationship range limit for sort order 1
  BRW4.AppendOrder('+INT:TIN')                             ! Append an additional sort order
  BRW4.AddField(INT:TIN,BRW4.Q.INT:TIN)                    ! Field INT:TIN is a hot field or requires assignment from browse
  BRW4.AddField(INT:TID,BRW4.Q.INT:TID)                    ! Field INT:TID is a hot field or requires assignment from browse
  BRW4.AddField(INT:BID,BRW4.Q.INT:BID)                    ! Field INT:BID is a hot field or requires assignment from browse
  BRW4.AddField(INT:MID,BRW4.Q.INT:MID)                    ! Field INT:MID is a hot field or requires assignment from browse
  BRW4.AddField(INT:DINo,BRW4.Q.INT:DINo)                  ! Field INT:DINo is a hot field or requires assignment from browse
  BRW4.AddField(INT:DID,BRW4.Q.INT:DID)                    ! Field INT:DID is a hot field or requires assignment from browse
  BRW4.AddField(INT:Leg,BRW4.Q.INT:Leg)                    ! Field INT:Leg is a hot field or requires assignment from browse
  BRW4.AddField(INT:DLID,BRW4.Q.INT:DLID)                  ! Field INT:DLID is a hot field or requires assignment from browse
  BRW4.AddField(INT:Cost,BRW4.Q.INT:Cost)                  ! Field INT:Cost is a hot field or requires assignment from browse
  BRW4.AddField(INT:VAT,BRW4.Q.INT:VAT)                    ! Field INT:VAT is a hot field or requires assignment from browse
  BRW4.AddField(INT:Rate,BRW4.Q.INT:Rate)                  ! Field INT:Rate is a hot field or requires assignment from browse
  BRW4.AddField(INT:VATRate,BRW4.Q.INT:VATRate)            ! Field INT:VATRate is a hot field or requires assignment from browse
  BRW_ManLoad.Q &= Queue:Browse:ManLoad
  BRW_ManLoad.AddSortOrder(,MAL:FKey_MID)                  ! Add the sort order for MAL:FKey_MID for sort order 1
  BRW_ManLoad.AddRange(MAL:MID,Relate:ManifestLoad,Relate:Manifest) ! Add file relationship range limit for sort order 1
  BRW_ManLoad.AppendOrder('+MAL:MLID')                     ! Append an additional sort order
  BRW_ManLoad.AddField(MAL:MLID,BRW_ManLoad.Q.MAL:MLID)    ! Field MAL:MLID is a hot field or requires assignment from browse
  BRW_ManLoad.AddField(MAL:TTID,BRW_ManLoad.Q.MAL:TTID)    ! Field MAL:TTID is a hot field or requires assignment from browse
  BRW_ManLoad.AddField(MAL:MID,BRW_ManLoad.Q.MAL:MID)      ! Field MAL:MID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_Manifest',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  BRW2.AskProcedure = 1
  BRW_ManLoadDel.AskProcedure = 2
  BRW4.AskProcedure = 3
  BRW_ManLoad.AskProcedure = 4
  BRW_ManLoadDel.AddToolbarTarget(Toolbar)                 ! Browse accepts toolbar control
  BRW_ManLoadDel.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Manifest.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Manifest',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      UpdateTransporterPaymentsAllocations
      Update_Manifest_Load_Deliveries
      Update_InvoiceTransporter
      Update_ManifestLoad
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_DI
      BRW_ManLoadDel.UpdateViewRecord()
    OF ?Button_DI:2
      BRW_ManLoadDel.UpdateViewRecord()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_ID
      ThisWindow.Update()
          IF GLO:ReplicatedDatabaseID = 0
             GLO:ReplicatedDatabaseID = MAN:BID
          .
      
          ! (FileManager FM, Key PrimaryKey, *? RecordID)
          Rep_GetNextID(Access:Manifest, MAN:PKey_MID, My_ID#)
      
      !    message('My_ID#:  ' & My_ID# & '||GLO:ReplicatedDatabaseID: ' & GLO:ReplicatedDatabaseID)
      
          MAN:MID     = My_ID#
      
      
          DISPLAY
    OF ?Button_DI
      ThisWindow.Update()
      GlobalRequest = ChangeRecord
      Update_Delivery()
      ThisWindow.Reset
      BRW_ManLoadDel.ResetSort(1)
    OF ?Button_DI:2
      ThisWindow.Update()
      GlobalRequest = ChangeRecord
      Update_Delivery_Items()
      ThisWindow.Reset
      BRW_ManLoadDel.ResetSort(1)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW_ManLoadDel.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:5
    SELF.ChangeControl=?Change:5
    SELF.DeleteControl=?Delete:5
  END


BRW_ManLoad.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:7
    SELF.ChangeControl=?Change:7
    SELF.DeleteControl=?Delete:7
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Browse:ManLoad, Resize:FixLeft+Resize:FixTop, Resize:LockWidth) ! Override strategy for ?Browse:ManLoad
  SELF.SetStrategy(?Line1, Resize:FixLeft+Resize:FixTop, Resize:LockWidth) ! Override strategy for ?Line1

!!! <summary>
!!! Generated from procedure template - Window
!!! Form TransporterPaymentsAllocations
!!! </summary>
UpdateTransporterPaymentsAllocations PROCEDURE 

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
History::TRAPA:Record LIKE(TRAPA:RECORD),THREAD
QuickWindow          WINDOW('Form TransporterPaymentsAllocations'),AT(,,358,168),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MDI,HLP('UpdateTransporterPaymentsAllocations'),SYSTEM
                       SHEET,AT(4,4,350,142),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('TRPAID:'),AT(8,20),USE(?TRAPA:TRPAID:Prompt),TRN
                           STRING(@n_10),AT(84,20,104,10),USE(TRAPA:TRPAID),RIGHT(1),TRN
                           PROMPT('TPID:'),AT(8,34),USE(?TRAPA:TPID:Prompt),TRN
                           STRING(@n_10),AT(84,34,104,10),USE(TRAPA:TPID),RIGHT(1),TRN
                           PROMPT('Allocation No:'),AT(8,48),USE(?TRAPA:AllocationNo:Prompt),TRN
                           STRING(@n_5),AT(84,48,104,10),USE(TRAPA:AllocationNo),RIGHT(1),TRN
                           PROMPT('Allocation Date:'),AT(8,62),USE(?TRAPA:AllocationDate:Prompt),TRN
                           SPIN(@d17),AT(84,62,119,10),USE(TRAPA:AllocationDate),MSG('Date allocation made'),TIP('Date alloc' & |
  'ation made')
                           PROMPT('Allocation Time:'),AT(8,76),USE(?TRAPA:AllocationTime:Prompt),TRN
                           ENTRY(@t7),AT(84,76,104,10),USE(TRAPA:AllocationTime)
                           PROMPT('Amount:'),AT(8,90),USE(?TRAPA:Amount:Prompt),TRN
                           ENTRY(@n-14.2),AT(84,90,64,10),USE(TRAPA:Amount),RIGHT(1),MSG('Amount of payment'),TIP('Amount of ' & |
  'payment allocated to this Invoice')
                           PROMPT('Comment:'),AT(8,104),USE(?TRAPA:Comment:Prompt),TRN
                           ENTRY(@s255),AT(84,104,266,10),USE(TRAPA:Comment),MSG('Comment'),TIP('Comment')
                           PROMPT('MID:'),AT(8,118),USE(?TRAPA:MID:Prompt),TRN
                           STRING(@n_10),AT(84,118,104,10),USE(TRAPA:MID),RIGHT(1),TRN
                           PROMPT('Status Up To Date:'),AT(8,132),USE(?TRAPA:StatusUpToDate:Prompt),TRN
                           ENTRY(@n3),AT(84,132,40,10),USE(TRAPA:StatusUpToDate),MSG('Has'),TIP('Has')
                         END
                       END
                       BUTTON('&OK'),AT(199,150,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(252,150,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(305,150,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View a Fuel Surcharge Record'
  OF InsertRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Fuel Surcharge Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateTransporterPaymentsAllocations')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRAPA:TRPAID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:TransporterPaymentsAllocations)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(TRAPA:Record,History::TRAPA:Record)
  SELF.AddHistoryField(?TRAPA:TRPAID,1)
  SELF.AddHistoryField(?TRAPA:TPID,2)
  SELF.AddHistoryField(?TRAPA:AllocationNo,3)
  SELF.AddHistoryField(?TRAPA:AllocationDate,6)
  SELF.AddHistoryField(?TRAPA:AllocationTime,7)
  SELF.AddHistoryField(?TRAPA:Amount,8)
  SELF.AddHistoryField(?TRAPA:Comment,9)
  SELF.AddHistoryField(?TRAPA:MID,10)
  SELF.AddHistoryField(?TRAPA:StatusUpToDate,12)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:TransporterPaymentsAllocations.SetOpenRelated()
  Relate:TransporterPaymentsAllocations.Open               ! File TransporterPaymentsAllocations used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TransporterPaymentsAllocations
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?TRAPA:AllocationTime{PROP:ReadOnly} = True
    ?TRAPA:Amount{PROP:ReadOnly} = True
    ?TRAPA:Comment{PROP:ReadOnly} = True
    ?TRAPA:StatusUpToDate{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('UpdateTransporterPaymentsAllocations',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TransporterPaymentsAllocations.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateTransporterPaymentsAllocations',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

