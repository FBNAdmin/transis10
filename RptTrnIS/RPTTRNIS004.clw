

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS004.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_TripSheet PROCEDURE (p:TRID)

Progress:Thermometer BYTE                                  !
LOC:Rep_Fields       GROUP,PRE(L_RF)                       !
Weight               DECIMAL(10,2)                         !In kg's
VolumetricWeight     DECIMAL(10,2)                         !Weight based on Volumetric calculation (in kgs)
Charge               DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
State                STRING(20)                            !State of this manifest
Driver               STRING(50)                            !First Name
Driver_Assistant     STRING(35)                            !
Reg_of_Horse         STRING(20)                            !
Reg_of_Trailer       STRING(20)                            !
COD_Comment          STRING(20)                            !
TransporterName      STRING(55)                            !Transporters Name
Suburb               STRING(50)                            !Suburb
                     END                                   !
LOC:Delivery_Totals  GROUP,PRE(L_DT)                       !
DID                  ULONG                                 !Delivery ID
InsuranceCharge      DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Insurance            DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Docs                 DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Fuel                 DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Charge               DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
                     END                                   !
LOC:Manifest_Totals  GROUP,PRE(L_MT)                       !
Cost                 DECIMAL(10,2)                         !
TRID                 ULONG                                 !Tripsheet ID
Delivery_Charges     DECIMAL(11,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Total_Weight         DECIMAL(10,2)                         !In kg's
Gross_Profit         DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Gross_Profit_Percent DECIMAL(6,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Average_C_Per_Kg     DECIMAL(7,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Legs_Cost            DECIMAL(10,2)                         !
Total_Cost           DECIMAL(10,2)                         !Includes extra legs cost
                     END                                   !
Process:View         VIEW(TripSheetDeliveries)
                       PROJECT(TRDI:TRID)
                       PROJECT(TRDI:UnitsLoaded)
                       PROJECT(TRDI:DIID)
                       JOIN(DELI:PKey_DIID,TRDI:DIID)
                         PROJECT(DELI:DID)
                         PROJECT(DELI:Units)
                         PROJECT(DELI:VolumetricWeight)
                         PROJECT(DELI:Weight)
                         PROJECT(DELI:DIID)
                         JOIN(MALD:FKey_DIID,DELI:DIID)
                           PROJECT(MALD:MLID)
                           JOIN(MAL:PKey_MLID,MALD:MLID)
                             PROJECT(MAL:MID)
                             JOIN(MAN:PKey_MID,MAL:MID)
                             END
                           END
                         END
                         JOIN(DEL:PKey_DID,DELI:DID)
                           PROJECT(DEL:DID)
                           PROJECT(DEL:DINo)
                           PROJECT(DEL:InsuranceRate)
                           PROJECT(DEL:Insure)
                           PROJECT(DEL:TotalConsignmentValue)
                           PROJECT(DEL:CID)
                           PROJECT(DEL:DeliveryAID)
                           PROJECT(DEL:CollectionAID)
                           JOIN(CLI:PKey_CID,DEL:CID)
                           END
                           JOIN(A_ADD:PKey_AID,DEL:DeliveryAID)
                             PROJECT(A_ADD:AddressName)
                           END
                           JOIN(ADD:PKey_AID,DEL:CollectionAID)
                             PROJECT(ADD:AddressName)
                           END
                         END
                       END
                       JOIN(TRI:PKey_TID,TRDI:TRID)
                         PROJECT(TRI:DepartDate)
                         PROJECT(TRI:DepartTime)
                         PROJECT(TRI:TRID)
                         PROJECT(TRI:BID)
                         PROJECT(TRI:VCID)
                         JOIN(BRA:PKey_BID,TRI:BID)
                           PROJECT(BRA:BranchName)
                         END
                         JOIN(VCO:PKey_VCID,TRI:VCID)
                           PROJECT(VCO:CompositionName)
                           PROJECT(VCO:TID)
                         END
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(615,2417,10396,5302),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('Arial',10,,FONT:regular, |
  CHARSET:ANSI),THOUS
                       HEADER,AT(615,1000,10396,1417),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Trip Sheet No.:'),AT(7615,63),USE(?String31),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@N_10),AT(9135,63,1021,167),USE(TRI:TRID),FONT(,,,FONT:bold,CHARSET:ANSI),RIGHT(1), |
  TRN
                         STRING('Trip Sheet'),AT(458,52,9479,417),USE(?String55),FONT(,24,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         IMAGE('fbn_logo_small.jpg'),AT(52,52,2302,458),USE(?Image1)
                         STRING('Branch:'),AT(63,563,1083,167),USE(?String51),TRN
                         STRING(@s35),AT(1063,563),USE(BRA:BranchName)
                         STRING('Arrival Time Depot: _{25}'),AT(7615,323),USE(?String52:3),TRN
                         STRING('Driver:'),AT(63,771,1083,167),USE(?String69:2),TRN
                         STRING(@s35),AT(1063,771,1583,167),USE(L_RF:Driver)
                         STRING('Assistant:'),AT(5073,625),USE(?String52:7),TRN
                         STRING(@s35),AT(5719,625,1823,156),USE(L_RF:Driver_Assistant),TRN
                         STRING('Supervisor Signature: _{23}'),AT(7615,625),USE(?String52:4),TRN
                         STRING('Date Despatched:'),AT(63,979,1083,167),USE(?String3:11),LEFT,TRN
                         STRING('Time Out:'),AT(63,1198,1083),USE(?String3:10),LEFT,TRN
                         STRING(@d6),AT(1063,979,,167),USE(TRI:DepartDate),RIGHT(1)
                         STRING(@t7),AT(1063,1198,729,167),USE(TRI:DepartTime),RIGHT(1)
                         STRING('Drivers Signature: _{26}'),AT(7615,927),USE(?String52:5),TRN
                         STRING('Starting Kms: _{23}'),AT(5073,927),USE(?String52),TRN
                         STRING('Manager Signature: _{25}'),AT(7615,1240),USE(?String52:6),TRN
                         STRING('Closing Kms: _{23}'),AT(5073,1240),USE(?String52:2),TRN
                       END
break_tripsheet        BREAK(TRDI:TRID),USE(?BREAK1)
                         HEADER,AT(0,0,,740),USE(?GROUPHEADER1),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  PAGEBEFORE(-1)
                           STRING('Transporter:'),AT(63,52,1083),USE(?String3:12),LEFT,TRN
                           STRING(@s55),AT(1063,52,3073,156),USE(L_RF:TransporterName),LEFT
                           LINE,AT(42,250,10350,0),USE(?Line8:4),COLOR(COLOR:Black)
                           STRING(@s35),AT(938,302,1583,167),USE(VCO:CompositionName),TRN
                           LINE,AT(42,500,10350,0),USE(?Line8:6),COLOR(COLOR:Black)
                           STRING('Comments'),AT(7865,531,646,167),USE(?String3:8),CENTER,TRN
                           LINE,AT(42,719,10350,0),USE(?Line8:2),COLOR(COLOR:Black)
                           STRING('Signature'),AT(9281,531,781,167),USE(?String3:9),CENTER,TRN
                           STRING('Time Out'),AT(6813,531,542,167),USE(?String3:7),CENTER,TRN
                           STRING('Reg. Horse:'),AT(2969,302),USE(?String69:3),TRN
                           STRING(@s20),AT(3646,302,979,167),USE(L_RF:Reg_of_Horse)
                           STRING('Trailer:'),AT(5188,302),USE(?String69:4),TRN
                           STRING(@s20),AT(5625,302),USE(L_RF:Reg_of_Trailer)
                           STRING('Vehicle Comp.:'),AT(125,302),USE(?String69),TRN
                           STRING('Act Kg'),AT(5219,531,542,167),USE(?String3:5),CENTER,TRN
                           STRING('Time In'),AT(5927,531,542,167),USE(?String3:6),CENTER,TRN
                           STRING('No. of Items'),AT(4573,531,594,167),USE(?String3:4),TRN
                           STRING('Sender'),AT(1073,531,1000,167),USE(?String3:2),CENTER,TRN
                           STRING('Deliver To'),AT(3042,531,1000,167),USE(?String3:3),CENTER,TRN
                           STRING('DN No.'),AT(42,531,,167),USE(?String3),TRN
                           STRING('State:'),AT(8563,63),USE(?String54),FONT(,,,FONT:regular,CHARSET:ANSI),TRN
                           STRING(@s20),AT(8917,63,,167),USE(L_RF:State),FONT(,,,FONT:regular,CHARSET:ANSI),RIGHT(1), |
  TRN
                           STRING(@s50),AT(8135,292),USE(L_RF:Suburb)
                           STRING('Suburb:'),AT(7698,292),USE(?String54:2),FONT(,,,FONT:regular,CHARSET:ANSI),TRN
                         END
break_delivery           BREAK(DEL:DID),USE(?BREAK2)
Detail                     DETAIL,AT(0,0,,0),USE(?Detail),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                             GROUP,AT(31,-21,9650,281),USE(?Group1),BOXED,HIDE,TRN
                               STRING(@n6),AT(5344,73,406,167),USE(TRDI:UnitsLoaded)
                               STRING('Not printing detail lines at all'),AT(52,73,885,167),USE(?String1),TRN
                               STRING(@n~AW~-13.2),AT(1552,73,875,167),USE(DELI:Weight),RIGHT(1)
                               STRING(@n-11.2),AT(4594,73,677,167),USE(L_RF:VolumetricWeight),RIGHT(1),HIDE
                               STRING(@n6),AT(5813,73),USE(DELI:Units),HIDE
                               STRING(@n_10),AT(1042,73,458,167),USE(DELI:DID),RIGHT(1),TRN
                               STRING(@n~CW~-13.2),AT(2469,73,875,167),USE(L_RF:Weight),RIGHT(1)
                             END
                           END
                           FOOTER,AT(0,0,,188),USE(?GROUPFOOTER1),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                             STRING(@n-11),AT(5219,10,542,167),USE(L_RF:Weight,,?L_RF:Weight:2),RIGHT(1),SUM,RESET(break_delivery), |
  TRN
                             STRING(@s20),AT(7604,10,1302,208),USE(L_RF:COD_Comment)
                             STRING(@n6),AT(4760,10),USE(TRDI:UnitsLoaded,,?TRDI:UnitsLoaded:2),RIGHT(1),SUM,RESET(break_delivery), |
  TRN
                             STRING(@s35),AT(2646,10,2031,167),USE(A_ADD:AddressName)
                             STRING(@n_10),AT(-63,10,479,167),USE(DEL:DINo),RIGHT(1)
                             STRING(@s35),AT(521,10,2031,208),USE(ADD:AddressName)
                             LINE,AT(448,0,0,198),USE(?Line1:9),COLOR(COLOR:Black)
                             LINE,AT(2583,0,0,198),USE(?Line1:2),COLOR(COLOR:Black)
                             LINE,AT(4750,0,0,198),USE(?Line1:3),COLOR(COLOR:Black)
                             LINE,AT(5208,0,0,198),USE(?Line1:6),COLOR(COLOR:Black)
                             LINE,AT(5812,0,0,198),USE(?Line1:4),COLOR(COLOR:Black)
                             LINE,AT(6656,0,0,198),USE(?Line1:7),COLOR(COLOR:Black)
                             LINE,AT(7542,0,0,198),USE(?Line1:8),COLOR(COLOR:Black)
                             LINE,AT(8948,0,0,198),USE(?Line1:5),COLOR(COLOR:Black)
                             LINE,AT(42,187,10350,0),USE(?Line12),COLOR(COLOR:Black),LINEWIDTH(3)
                           END
                         END
                         FOOTER,AT(0,0,,365),USE(?GROUPFOOTER2)
                           LINE,AT(42,52,10350,0),USE(?Line8:3),COLOR(COLOR:Black)
                           STRING('Total Weight:'),AT(2094,104),USE(?String65:2),TRN
                           STRING(@n-11.2),AT(2979,104),USE(L_RF:Weight,,?L_RF:Weight:3),RIGHT(1),SUM,RESET(break_tripsheet), |
  TRN
                           STRING(@n6),AT(1240,104),USE(TRDI:UnitsLoaded,,?TRDI:UnitsLoaded:3),RIGHT(1),SUM,RESET(break_tripsheet), |
  TRN
                           STRING('Units:'),AT(844,104),USE(?String65),TRN
                         END
                       END
Collections            DETAIL,AT(0,0,,2125),USE(?collections),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI), |
  TOGETHER
                         STRING('Collections'),AT(52,0,9531,260),USE(?String66),FONT(,12,,FONT:bold,CHARSET:ANSI),CENTER, |
  TRN
                         STRING('No.'),AT(63,252),USE(?String58),TRN
                         STRING('DI Number'),AT(363,252),USE(?String58:9),TRN
                         STRING('From'),AT(1292,260,1563,156),USE(?String58:2),CENTER,TRN
                         STRING('To'),AT(3021,260,1823,156),USE(?String58:3),CENTER,TRN
                         STRING('Weight (kg)'),AT(4979,250,938,167),USE(?String58:4),CENTER,TRN
                         STRING('No. of Packages'),AT(6094,250,938,167),USE(?String58:5),CENTER,TRN
                         STRING('Time In'),AT(7063,250,938,167),USE(?String58:6),CENTER,TRN
                         STRING('Time Out'),AT(8000,250,938,167),USE(?String58:7),CENTER,TRN
                         STRING('Signature'),AT(9281,260,729,156),USE(?String58:8),CENTER,TRN
                         LINE,AT(302,458,0,1590),USE(?Line14),COLOR(COLOR:Black)
                         LINE,AT(1167,458,0,1590),USE(?Line141),COLOR(COLOR:Black)
                         LINE,AT(2948,458,0,1590),USE(?Line14:3),COLOR(COLOR:Black)
                         LINE,AT(4865,458,0,1590),USE(?Line14:7),COLOR(COLOR:Black)
                         LINE,AT(6062,458,0,1590),USE(?Line14:6),COLOR(COLOR:Black)
                         LINE,AT(7073,458,0,1590),USE(?Line14:5),COLOR(COLOR:Black)
                         LINE,AT(8000,458,0,1590),USE(?Line14:4),COLOR(COLOR:Black)
                         LINE,AT(8948,458,0,1590),USE(?Line14:2),COLOR(COLOR:Black)
                         STRING('1'),AT(63,479,208,156),USE(?String58:10),TRN
                         STRING('2'),AT(63,677,208,156),USE(?String58:11),TRN
                         STRING('3'),AT(63,885,208,156),USE(?String58:12),TRN
                         STRING('4'),AT(63,1083,208,156),USE(?String58:13),TRN
                         STRING('5'),AT(63,1281,208,156),USE(?String58:14),TRN
                         STRING('6'),AT(63,1479,208,156),USE(?String58:15),TRN
                         STRING('7'),AT(63,1677,208,156),USE(?String58:16),TRN
                         STRING('8'),AT(63,1885,208,156),USE(?String58:17),TRN
                         LINE,AT(42,448,10350,0),USE(?Line13),COLOR(COLOR:Black)
                         LINE,AT(42,646,10350,0),USE(?Line13:2),COLOR(COLOR:Black)
                         LINE,AT(42,854,10350,0),USE(?Line13:3),COLOR(COLOR:Black)
                         LINE,AT(42,1052,10350,0),USE(?Line13:4),COLOR(COLOR:Black)
                         LINE,AT(42,1250,10350,0),USE(?Line13:41),COLOR(COLOR:Black)
                         LINE,AT(42,1448,10350,0),USE(?Line13:5),COLOR(COLOR:Black)
                         LINE,AT(42,1646,10350,0),USE(?Line13:6),COLOR(COLOR:Black)
                         LINE,AT(42,1854,10350,0),USE(?Line13:7),COLOR(COLOR:Black)
                         LINE,AT(42,2052,10350,0),USE(?Line13:8),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(615,7740,10396,240),USE(?Footer),FONT('MS Sans Serif',,COLOR:Black,,CHARSET:ANSI)
                         STRING('Report Date:'),AT(73,31),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(875,31),USE(?ReportDateStamp),TRN
                         STRING('Report Time:'),AT(2250,31),USE(?ReportTimePrompt),TRN
                         STRING('<<-- Time Stamp -->'),AT(3052,31),USE(?ReportTimeStamp),TRN
                         STRING(@N3),AT(10031,31),USE(ReportPageNumber)
                         STRING('Page:'),AT(9615,31),USE(?String61),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.AskPreview PROCEDURE

  CODE
          PRINT(RPT:Collections)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_TripSheet')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Add_Suburbs.SetOpenRelated()
  Relate:Add_Suburbs.Open                                  ! File Add_Suburbs used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterAlias.Open                             ! File TransporterAlias used by this procedure, so make sure it's RelationManager is open
  Access:DeliveryLegs.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailerAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoad.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoadDeliveries.UseFile                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:TripSheetDeliveries, ?Progress:PctText, Progress:Thermometer, ProgressMgr, TRDI:TRID)
  ThisReport.AddSortOrder(TRDI:FKey_TRID)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:TripSheetDeliveries.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      IF p:TRID ~= 0
         ThisReport.AddRange(TRDI:TRID, p:TRID)
      .
  
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Suburbs.Close
    Relate:TransporterAlias.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      CLEAR(L_DT:InsuranceCharge)
      IF DEL:Insure = TRUE
         L_DT:InsuranceCharge    = (DEL:InsuranceRate / 100) * DEL:TotalConsignmentValue
      .
  
  !       message('DELI:DID||' & DELI:DID)
  
      A_TRU:TTID              = VCO:TTID0
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Horse    = A_TRU:Registration
      .
      A_TRU:TTID              = VCO:TTID1
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Trailer  = A_TRU:Registration
      .
  
      DRI:DRID                = TRI:DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RF:Driver          = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
      DRI:DRID                    = TRI:Assistant_DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RF:Driver_Assistant    = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
  
  
      !A_TRA:TID            = VCO:TID
      A_TRA:TID            = TRI:TID
      IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) = LEVEL:Benign
         L_RF:TransporterName = A_TRA:TransporterName
      .
  
      SUBU:SUID           = TRI:SUID
      IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) = LEVEL:Benign
         L_RF:Suburb      = SUBU:Suburb
      .
  
  
      CLEAR(L_RF:COD_Comment)
      CASE DEL:Terms              ! Pre Paid|COD|Account
      OF 0
      OF 1
         L_RF:COD_Comment     = 'COD'
      OF 2
      .
      ! Get Invoice
      str_"       = Get_Invoices(0, 4, 1,, DEL:DID,,, 2)
      INV:IID     = Get_1st_Element_From_Delim_Str(str_", ',', TRUE)
      IF Access:_Invoice.TryFetch(INV:PKey_IID) ~= LEVEL:Benign
         CLEAR(INV:Record)
      .
      
  
      ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info  , p:LogName)
      !   1           2       3           4          5       6          7        8            9                 10        11          12
      ! (LONG  , BYTE    , BYTE=0, BYTE=0        , ULONG=0, <LONG>  , BYTE=0  , BYTE=0    , BYTE=0           , BYTE=0  , <STRING>, <STRING>),STRING
      ! p:Option
      !   0.  - Total Charges inc.    (default)
      !   1.  - Excl VAT
      !   2.  - VAT
      !   3.  - Kgs
      !   4.  - IIDs
      ! p:Type
      !   0.  - All                   (default)
      !   1.  - Invoices                                  Total >= 0.0
      !   2.  - Credit Notes                              Total < 0.0    Instead of <, should be invoices with A_INV:CR_IID
      !   3.  - Credit Notes excl Bad Debts               Total < 0.0
      !   4.  - Credit Notes excl Journals                Total < 0.0
      !   5.  - Credit Notes excl Bad Debts & Journals    Total < 0.0
      ! p:Limit_On    &    p:ID
      !   0.  - Date [& Branch]       (default)
      !   1.  - MID
      !   2.  - DID
      ! p:Manifest_Option
      !   0.  - None                  (default)
      !   1.  - Overnight
      !   2.  - Broking
      ! p:Not_Manifest
      !   0.  - All                   (default)
      !   1.  - Not Manifest
      !   2.  - Manifest
  L_RF:Weight = DELI:Weight * (TRDI:UnitsLoaded / DELI:Units)
  L_RF:VolumetricWeight = DELI:VolumetricWeight * (TRDI:UnitsLoaded / DELI:Units)
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:Collections)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF L_DT:DID ~= DEL:DID
         L_DT:DID      = DEL:DID
         ! Total loaded items from DID on MLID   /   Total Items on DID
         L_RF:Charge   = (DEL:DocumentCharge + DEL:FuelSurcharge + DEL:Charge) * |
                         Get_ManLoadItems_Info(MAL:MLID, 1, DEL:DID) / Get_DelItem_s_Totals(DEL:DID, 3)
  
         L_RF:Charge   = L_RF:Charge * (DEL:VATRate / 100)
      .
  
  
  
      IF L_MT:TRID ~= TRDI:TRID
         !L_MT:MID                     = MAL:MID
         L_MT:TRID                    = TRDI:TRID
  
         L_MT:Delivery_Charges        = Get_Manifest_Info(MAL:MID, 4)
  
         L_MT:Cost                    = MAN:Cost * (MAN:VATRate / 100)
         L_MT:Legs_Cost               = Get_Manifest_Info(MAL:MID, 3)                         ! Legs cost
  
         L_MT:Total_Cost              = (L_MT:Legs_Cost + MAN:Cost) * (MAN:VATRate / 100)
  
         L_MT:Gross_Profit            = L_MT:Delivery_Charges - L_MT:Total_Cost
  
         L_MT:Gross_Profit_Percent    = (L_MT:Gross_Profit / L_MT:Delivery_Charges) * 100     ! %
  
  !       MESSAGE('MAL:MID: ' & MAL:MID)
  
         L_MT:Total_Weight            = Get_Manifest_Info(MAL:MID, 2)
  
         L_MT:Average_C_Per_Kg        = (L_MT:Delivery_Charges / L_MT:Total_Weight) * 100     ! in cents (not rands)
  
         EXECUTE TRI:State
            L_RF:State  = 'Loaded'
            L_RF:State  = 'On Route'
            L_RF:State  = 'Transferred'
         ELSE
            L_RF:State  = 'Loading'
      .  .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_TripSheet','Print_TripSheet','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Debtor_Age_Analysis PROCEDURE (p:SRID, p:Option, p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:CID, p:Credit_Limit_Option, p:ClientStatus, p:BID)

Progress:Thermometer BYTE                                  !
LOC:Locals           GROUP,PRE(LOC)                        !
Date                 DATE                                  !
Time                 TIME                                  !
                     END                                   !
LOC:STRID            ULONG                                 !Statement Run ID
LOC:Statement_Q      QUEUE,PRE(L_SQ)                       !Statement ID
STID                 ULONG                                 !Statement ID
CID                  ULONG                                 !Client ID
                     END                                   !
LOC:Info             STRING(35)                            !
LOC:Progress_Group   GROUP,PRE(L_PG)                       !
Clients              LONG                                  !
No_Done              LONG                                  !
Win_Txt              CSTRING(50)                           !
TimeIn               LONG                                  !
                     END                                   !
LOC:Grand_Totals     GROUP,PRE(L_GT)                       !
Total                DECIMAL(10,2)                         !Total
Current              DECIMAL(10,2)                         !Current
Days30               DECIMAL(10,2)                         !30 Days
Days60               DECIMAL(10,2)                         !60 Days
Days90               DECIMAL(10,2)                         !90 Days
Number               ULONG                                 !Number of entries
Current_PerTot       DECIMAL(6,1)                          !
Days30_PerTot        DECIMAL(6,1)                          !Current
Days60_PerTot        DECIMAL(6,1)                          !Current
Days90_PerTot        DECIMAL(6,1)                          !Current
                     END                                   !
LOC:Statement_Info   GROUP,PRE(L_SI)                       !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
                     END                                   !
LOC:Options_Group    GROUP,PRE(L_OG)                       !
Status               BYTE(255)                             !Normal, On Hold, Closed, Dormant
Report_Status        STRING(50)                            !can be more than one
BranchName           STRING(35)                            !Branch Name
BID                  ULONG                                 !Branch ID
BID_FirstTime        BYTE(1)                               !
Client_Normal        BYTE                                  !
Client_OnHold        BYTE                                  !
Client_Closed        BYTE                                  !
Client_Dormant       BYTE                                  !
                     END                                   !
Process:View         VIEW(Clients)
                       PROJECT(CLI:AID)
                       PROJECT(CLI:AccountLimit)
                       PROJECT(CLI:BID)
                       PROJECT(CLI:CID)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:Status)
                     END
ProgressWindow       WINDOW('Clients Age Analysis'),AT(,,255,156),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       SHEET,AT(4,2,248,135),USE(?Sheet1)
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(24,71,205,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(14,59,227,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(14,87,227,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Pause'),AT(199,138,,15),USE(?Pause),LEFT,ICON('waok.ico'),DISABLE,FLAT
                       BUTTON('Cancel'),AT(146,138,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Clients Report'),AT(250,1156,7823,9344),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7823,917),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Client Age Analysis'),AT(2740,0,,365),USE(?ReportTitle),FONT('Arial',18,,FONT:bold), |
  CENTER
                         STRING('Branch:'),AT(52,396,469,198),USE(?String36:2),FONT('Arial',10,,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('Client Status:'),AT(4583,396),USE(?String36),FONT('Arial',10,,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s50),AT(5521,396,2292,198),USE(L_OG:Report_Status),FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         STRING(@s35),AT(573,396,1823,198),USE(L_OG:BranchName),FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         BOX,AT(0,698,7813,208),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(417,708,0,200),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2604,708,0,200),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(3563,708,0,200),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(4427,708,0,200),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(5281,708,0,200),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(6115,708,0,200),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(6969,708,0,200),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         STRING('No.'),AT(156,719,,170),USE(?HeaderTitle:1),TRN
                         STRING('Debtors Name'),AT(479,719,900,170),USE(?HeaderTitle:2),TRN
                         STRING('Limit'),AT(2677,719,833,167),USE(?HeaderTitle:9),CENTER,TRN
                         STRING('90 Days'),AT(3563,719,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('60 Days'),AT(4396,719,833,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('30 Days'),AT(5240,719,833,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Current'),AT(6094,719,833,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Total'),AT(6958,719,833,167),USE(?HeaderTitle:8),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,7823,479),USE(?Detail)
                         LINE,AT(0,0,0,479),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(417,0,0,240),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(2604,0,0,240),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(3563,0,0,240),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(4427,0,0,240),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(5281,0,0,240),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(6115,0,0,240),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(6969,0,0,240),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(7813,0,0,479),USE(?DetailLine:8),COLOR(COLOR:Black)
                         STRING(@s35),AT(469,281,2813,167),USE(LOC:Info),TRN
                         STRING(@n-17),AT(2646,31,885,167),USE(CLI:AccountLimit),RIGHT(1),TRN
                         STRING(@n-14.2),AT(3563,31),USE(L_SI:Days90),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4396,31),USE(L_SI:Days60),RIGHT(1),TRN
                         STRING(@n-14.2),AT(5240,31),USE(L_SI:Days30),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6094,31),USE(L_SI:Current),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6958,31),USE(L_SI:Total),RIGHT(1),TRN
                         STRING(@n_10b),AT(-313,31,,170),USE(CLI:ClientNo),RIGHT,TRN
                         STRING(@s35),AT(469,31,2100,167),USE(CLI:ClientName),LEFT
                         LINE,AT(417,240,7400,0),USE(?DetailEndLine:2),COLOR(COLOR:Black)
                         LINE,AT(0,479,7813,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
detail_totals          DETAIL,AT(,,,625),USE(?detail_totals)
                         STRING(@n-14.2),AT(5240,146,833,167),USE(L_GT:Days30),RIGHT(1)
                         STRING(@n-14.2),AT(3563,146),USE(L_GT:Days90),RIGHT(1)
                         STRING(@n-14.2),AT(4396,146,833,167),USE(L_GT:Days60),RIGHT(1)
                         STRING(@n13),AT(1979,146),USE(L_GT:Number),RIGHT(1)
                         STRING(@n-14.2),AT(6094,146,833,167),USE(L_GT:Current),RIGHT(1)
                         STRING(@n-14.2),AT(6958,146,833,167),USE(L_GT:Total),RIGHT(1)
                         STRING('% of Total:'),AT(1188,375),USE(?String29:2),TRN
                         STRING(@n-9.1),AT(5240,375,833,167),USE(L_GT:Days30_PerTot),RIGHT(1)
                         STRING(@n-9.1),AT(4396,375,833,167),USE(L_GT:Days60_PerTot),RIGHT(1)
                         STRING(@n-9.1),AT(3563,375,833,167),USE(L_GT:Days90_PerTot),RIGHT(1)
                         STRING(@n-9.1),AT(6094,375,833,167),USE(L_GT:Current_PerTot),RIGHT(4)
                         STRING('Debtors Listed:'),AT(1188,146),USE(?String29),TRN
                       END
                       FOOTER,AT(250,10500,7823,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(7021,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

View_State      VIEW(_Statements)
    PROJECT(STA:STID, STA:STRID, STA:CID)
    .

State_View      ViewManager





View_Addr       VIEW(AddressContacts)
    !PROJECT()
    .


Addr_View       ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_Statement_Run              ROUTINE
    SETCURSOR(CURSOR:Wait)
    FREE(LOC:Statement_Q)

    State_View.Init(View_State, Relate:_Statements)
    State_View.AddSortOrder(STA:FKey_STRID)
    !State_View.AppendOrder()
    State_View.AddRange(STA:STRID, LOC:STRID)
    !State_View.SetFilter()

    State_View.Reset()
    LOOP
       IF State_View.Next() ~= LEVEL:Benign
          BREAK
       .

       L_SQ:STID    = STA:STID
       L_SQ:CID     = STA:CID
       ADD(LOC:Statement_Q)
    .
    State_View.Kill()
    SETCURSOR()

    ?Progress:UserString{PROP:TExt} = 'Statements: ' & RECORDS(LOC:Statement_Q)
    EXIT
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Client No.' & |
      '|' & 'By Client Name' & |
      '|' & 'By Branch ID' & |
      '|' & 'By Client ID' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
        PRINT(RPT:detail_totals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Debtor_Age_Analysis')
      ! (p:SRID, p:Option, p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:CID, p:Credit_Limit_Option, p:ClientStatus, p:BID)
      ! (ULONG, BYTE, LONG=0, BYTE=0, BYTE=0, BYTE=0, ULONG=0, BYTE=0, BYTE=255, ULONG=0)
  
  
  
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:BID',L_OG:BID)                                ! Added by: Report
  BIND('L_OG:Status',L_OG:Status)                          ! Added by: Report
  BIND('L_OG:Client_Normal',L_OG:Client_Normal)            ! Added by: Report
  BIND('L_OG:Client_OnHold',L_OG:Client_OnHold)            ! Added by: Report
  BIND('L_OG:Client_Closed',L_OG:Client_Closed)            ! Added by: Report
  BIND('L_OG:Client_Dormant',L_OG:Client_Dormant)          ! Added by: Report
      BIND('p:CID', p:CID)
         Addr_View.Init(View_Addr, Relate:AddressContacts)
         Addr_View.AddSortOrder(ADDC:FKey_AID)
         Addr_View.AppendOrder('ADDC:ContactName')
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
    IF p:CID = 0
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
    ELSE
       ! No need for selection when we have only 1 client
       ProcessSortSelectionVariable     = 'By Client No.'
    .
  Relate:AddressContacts.SetOpenRelated()
  Relate:AddressContacts.Open                              ! File AddressContacts used by this procedure, so make sure it's RelationManager is open
  Access:_Statements.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Statement_Runs.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
        L_OG:BID    = p:BID
  
        IF L_OG:BID = 0
           L_OG:BranchName    = 'All'
        ELSE
           BRA:BID    = L_OG:BID
           IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
              L_OG:BranchName    = CLIP(BRA:BranchName)
        .  .
      L_OG:Status         = 254       ! none
  
      L_OG:Client_Normal  = 255
      L_OG:Client_OnHold  = 255
      L_OG:Client_Closed  = 255
      L_OG:Client_Dormant = 255
  
  
      IF BAND(p:ClientStatus, 0001b) > 0
         L_OG:Client_Normal   = 0
         L_OG:Report_Status   = 'Normal'
      .
  
      IF BAND(p:ClientStatus, 0010b) > 0
         L_OG:Client_OnHold   = 1
         ! (p:Add, p:List, p:Delim, p:Option, p:Prefix)
         Add_to_List('On Hold', L_OG:Report_Status, ', ')
      .
  
      IF BAND(p:ClientStatus, 0100b) > 0
         L_OG:Client_Closed   = 2
         Add_to_List('Closed', L_OG:Report_Status, ', ')
      .
  
      IF BAND(p:ClientStatus, 1000b) > 0
         L_OG:Client_Dormant  = 3
         Add_to_List('Dormant', L_OG:Report_Status, ', ')
      .
  
      IF p:ClientStatus = 0
         L_OG:Report_Status   = 'All'
         L_OG:Status          = 255
      .
  ThisReport.Init(Process:View, Relate:Clients, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientNo')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client Name')) THEN
     ThisReport.AppendOrder('+CLI:ClientName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch ID')) THEN
     ThisReport.AppendOrder('+CLI:BID,+CLI:ClientName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client ID')) THEN
     ThisReport.AppendOrder('+CLI:CID')
  END
  ThisReport.SetFilter('(p:CID = 0 OR p:CID = CLI:CID) AND (L_OG:BID = 0 OR L_OG:BID = CLI:BID) AND (L_OG:Status = 255 OR L_OG:Client_Normal = CLI:Status OR L_OG:Client_OnHold = CLI:Status OR L_OG:Client_Closed = CLI:Status OR L_OG:Client_Dormant = CLI:Status)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = 'Processing Age Analysis'
  Relate:Clients.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      ! Show statement info for which the age analysis will be used - offer to run new one
      LOC:Date        = TODAY()
      LOC:Time        = CLOCK()
  
  
  
      IF p:SRID ~= 0
         LOC:STRID    = p:SRID
         DO Load_Statement_Run
      ELSE
         LOC:Date     = p:Date
         LOC:Time     = DEFORMAT('12:00:00',@t4)
      .
  
  
      ! Both of these set Invoice UpToDate status to 0 if there is a new Credit Note or Client Payment that has not
      ! had its UpToDate status set yet.   At the same time that the related Invoies are set to 0 the processed
      ! Invoice or Client Payment is set to UpToDate.  In otherwords this should be quick.
  
      Process_Invoice_StatusUpToDate('0')
      Process_ClientPaymentsAllocation_StatusUpToDate('0')
      
      IF p:OutPut > 0       ! (p:Text, p:Heading, p:Name)
         Add_Log(',Client No.,Invoice No.,Invoice Date,Invoice Total,Credits,Payments', 'Headings', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv', TRUE)
      .
  
  
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
      IF p:CID = 0
         L_PG:Clients     = RECORDS(Clients)
         L_PG:Win_Txt     = ProgressWindow{PROP:Text}
      .
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      UNBIND('p:CID')
         Addr_View.Kill()
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressContacts.Close
  END
      IF p:OutPut > 0
         CASE MESSAGE('Would you like to load the accumulation details now?|Note that if the file does not load automatically you can find it at the location shown below.||File: ' & PATH() & '\Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv', 'Age Analysis', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Pause
      ThisWindow.Update()
          EXECUTE L_OG:Status + 1
             L_OG:Report_Status  = 'Normal'
             L_OG:Report_Status  = 'On Hold'
             L_OG:Report_Status  = 'Closed'
             L_OG:Report_Status  = 'Dormant'
          ELSE
             L_OG:Report_Status  = 'All'
          .
          SELECT(?Tab1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      ! Also see Validation - Section moved there, as needed to filter for new Credit Limit option
  
      IF CLIP(LOC:Info) ~= '<no statement>'
         ! Put contact details in LOC:Info
         Addr_View.AddRange(ADDC:AID, CLI:AID)
         Addr_View.Reset()
         IF Addr_View.Next() = LEVEL:Benign
            LOC:Info      = ADDC:ContactName
      .  .
  
  
      ADD:AID             = CLI:AID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
         IF CLIP(LOC:Info) = ''
            LOC:Info      = CLIP(ADD:PhoneNo)
         ELSE
            LOC:Info      = CLIP(ADD:PhoneNo) & ', ' & LOC:Info
      .  .
  
  
      L_GT:Current_PerTot = L_GT:Current / L_GT:Total * 100
      L_GT:Days30_PerTot  = L_GT:Days30  / L_GT:Total * 100
      L_GT:Days60_PerTot  = L_GT:Days60  / L_GT:Total * 100
      L_GT:Days90_PerTot  = L_GT:Days90  / L_GT:Total * 100
        IF p:Ignore_Zero_Bal = TRUE
           IF (L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current + L_SI:Total) = 0.0
  
              !L_GT:Total = 0.0 AND L_GT:Current = 0.0 AND L_GT:Days30 = 0.0 AND L_GT:Days60 = 0.0 AND L_GT:Days90 = 0.0
              !db.debugout('[Print_Debtors_Age_Analysis]  0.0 on all values')
              
  
              SkipDetails  = TRUE
        .  .
  IF 0
    PRINT(RPT:detail_totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      ! All|Last Statement|None
      IF p:Option = 1
         L_SQ:CID  = CLI:CID
         GET(LOC:Statement_Q, L_SQ:CID)
         IF ERRORCODE()
            ReturnValue       = Record:Filtered
      .  .
  
      IF p:Option < 2
         IF LOC:STRID = 0
            ReturnValue       = LEVEL:Fatal
      .  .
  
  
  
  
      ! This Section is from TakeRecord - needed here for additional option - p:Credit_Limit_Option
      CLEAR(LOC:Info)
  
      ! p:SRID = 0    - not from statement
  
      IF p:SRID = 0
         ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
         L_PG:No_Done    += 1
  
         ProgressWindow{PROP:Text}        = L_PG:Win_Txt & ' - ' & L_PG:No_Done & '/' & L_PG:Clients
         ?Progress:UserString{Prop:Text}  = CLIP(CLI:ClientName)
  
         L_PG:TimeIn                      = CLOCK()
         Junk_#           = Gen_Statement(0, CLI:CID, LOC:Date, LOC:Time,, TRUE, LOC:Statement_Info,, p:MonthEndDay, p:OutPut)
         IF CLOCK() - L_PG:TimeIn > 100       ! 1 second
            db.debugout('[Print_Debtor_Age_Analysis]  CLI:ClientNo: ' & CLI:ClientNo & ',  Gen_Statement > 1 sec: ' & CLOCK() - L_PG:TimeIn)
         .
      ELSE
         ! Get the suitable Statement
         CLEAR(LOC:Info)
         CLEAR(STA:Record)
  
         L_SQ:CID  = CLI:CID
         GET(LOC:Statement_Q, L_SQ:CID)
         IF ~ERRORCODE()
            STA:STID      = L_SQ:STID
            IF Access:_Statements.TryFetch(STA:PKey_STID) = LEVEL:Benign
            ELSE
               LOC:Info   = '<no statement>'
            .
         ELSE
            LOC:Info      = '<no statement>'
         .
  
  
         L_SI:Days90      = STA:Days90
         L_SI:Days60      = STA:Days60
         L_SI:Days30      = STA:Days30
         L_SI:Current     = STA:Current
         L_SI:Total       = STA:Total
      .
  
  
      IF p:Credit_Limit_Option = TRUE
         IF L_SI:Total <= CLI:AccountLimit
            ReturnValue   = Record:Filtered
      .  .
  
  
      IF ReturnValue = Record:OK
         L_GT:Total      += L_SI:Total
         L_GT:Current    += L_SI:Current
         L_GT:Days30     += L_SI:Days30
         L_GT:Days60     += L_SI:Days60
         L_GT:Days90     += L_SI:Days90
         L_GT:Number     += 1
      .
  !    old
  !       only changed this section on the 1st of Dec 06
  !
  !
  !       IF p:SRID = 0                    ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay)
  !          L_GT:Total      += L_SI:Total
  !          L_GT:Current    += L_SI:Current
  !          L_GT:Days30     += L_SI:Days30
  !          L_GT:Days60     += L_SI:Days60
  !          L_GT:Days90     += L_SI:Days90
  !          L_GT:Number     += 1
  !       ELSE
  !          L_GT:Total      += L_SI:Total
  !          L_GT:Current    += L_SI:Current
  !          L_GT:Days30     += L_SI:Days30
  !          L_GT:Days60     += L_SI:Days60
  !          L_GT:Days90     += L_SI:Days90
  !          L_GT:Number     += 1
  !    .  .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Debtor_Age_Analysis','Print_Debtor_Age_Analysis','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! ..
!!! </summary>
Print_Deliveries_Rates_Check PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Locals           GROUP,PRE(L_L)                        !
Weight               DECIMAL(12,2)                         !In kg's
MinimiumCharge       DECIMAL(10,2)                         !
SetupRate            BYTE                                  !
RID                  ULONG                                 !Rate ID
Setup_Rate           STRING(2)                             !
Rate_Problem         BYTE                                  !
Font_Style           LONG                                  !
Rpt_Desc             STRING(150)                           !
Key                  STRING(255)                           !
Rate_Effective_Date  LONG                                  !
                     END                                   !
LOC:RG               GROUP,PRE(L_RG)                       !
RatePerKg            LIKE(RAT:RatePerKg)                   !Rate per Kg
ClientRateType       STRING(100)                           !Load Type
                     END                                   !
LOC:MIDs             STRING(20)                            !List of Manifest IDs that the delivery is currently manifested on
LOC:Output_Text      STRING(10000)                         !
LOC:Options          GROUP,PRE(L_O)                        !
Run_Option           BYTE(1)                               !Show rate variance or Show summary of all DI's without validating rates
Date_Option          BYTE                                  !DI Date, Created Date, All
From_Date            LONG                                  !
To_Date              LONG                                  !
Show_Setup_Rates     BYTE                                  !Show setup rates
Show_When_DI_Rate    BYTE                                  !When DI Rate used is Higher or Lower than specified
IncludeNoRates       BYTE                                  !
IncludeZeroRates     BYTE                                  !
                     END                                   !
Process:View         VIEW(Deliveries)
                       PROJECT(DEL:CID)
                       PROJECT(DEL:Charge)
                       PROJECT(DEL:CreatedDate)
                       PROJECT(DEL:CreatedDateTime)
                       PROJECT(DEL:CreatedTime)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:DIDateAndTime)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:DITime)
                       PROJECT(DEL:FIDRate)
                       PROJECT(DEL:JID)
                       PROJECT(DEL:LTID)
                       PROJECT(DEL:Rate)
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                       END
                     END
ProgressWindow       WINDOW('Report Deliveries'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Deliveries Report'),AT(250,1021,7750,10167),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,771),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Deliveries Rate Check'),AT(0,20,7750),USE(?ReportTitle),FONT('Arial',14,,FONT:bold), |
  CENTER
                         STRING(@s150),AT(521,344,6719,167),USE(L_L:Rpt_Desc),CENTER
                         BOX,AT(-10,542,7750,220),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Client'),AT(1365,573,823,167),USE(?HeaderTitle:3),TRN
                         LINE,AT(625,552,0,220),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1312,552,0,220),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(3292,552,0,220),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(4094,552,0,220),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(5073,552,0,220),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(6052,552,0,220),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(6875,552,0,220),USE(?HeaderLine:61),COLOR(COLOR:Black)
                         STRING('Freight Charge'),AT(5115,573,,167),USE(?HeaderTitle:8),TRN
                         STRING('Weight'),AT(6927,573,771,167),USE(?HeaderTitle:7),TRN
                         STRING('Min. Charge'),AT(6146,573,,167),USE(?HeaderTitle:6),TRN
                         STRING('Rate Set'),AT(4125,573,698,167),USE(?HeaderTitle:5),TRN
                         STRING('DI No.'),AT(52,573,573,167),USE(?HeaderTitle:1),TRN
                         STRING('DI Date'),AT(688,573,583,167),USE(?HeaderTitle:2),TRN
                         STRING('Rate Used'),AT(3344,573,698,167),USE(?HeaderTitle:41),TRN
                       END
Detail                 DETAIL,AT(0,0,7750,500),USE(?Detail)
                         STRING(@t7b),AT(1177,250,365),USE(DEL:CreatedTime)
                         STRING('MID''s:'),AT(5417,250),USE(?String31),TRN
                         STRING(@s20),AT(5781,250,1000),USE(LOC:MIDs)
                         STRING(@d5b),AT(552,250),USE(DEL:CreatedDate),RIGHT(1)
                         LINE,AT(0,0,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         LINE,AT(0,0,0,420),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(0,208,7750,0),USE(?DetailEndLine1),COLOR(COLOR:Black)
                         LINE,AT(0,417,7750,0),USE(?DetailEndLine:2),COLOR(COLOR:Black)
                         LINE,AT(625,0,0,210),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(1312,0,0,210),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(3292,0,0,210),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(4094,0,0,210),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(5073,0,0,210),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(6052,0,0,210),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(6875,0,0,210),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,420),USE(?DetailLine:61),COLOR(COLOR:Black)
                         LINE,AT(1583,208,0,210),USE(?DetailLine:7),COLOR(COLOR:Black)
                         STRING(@n_10),AT(-125,31,,170),USE(DEL:DINo),RIGHT,TRN
                         STRING(@d5),AT(677,31,,170),USE(DEL:DIDate),RIGHT(1)
                         STRING(@s35),AT(1365,31,1875,156),USE(CLI:ClientName),TRN
                         STRING(@n-11.4),AT(3344,31,,170),USE(DEL:Rate),RIGHT(1)
                         STRING(@n-11.4),AT(4125,31),USE(L_RG:RatePerKg),RIGHT(1)
                         STRING(@s2),AT(4865,31),USE(L_L:Setup_Rate),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@n-15.2),AT(5115,31),USE(DEL:Charge),RIGHT(1)
                         STRING(@n-11.2),AT(6146,31),USE(L_L:MinimiumCharge),RIGHT(1)
                         STRING(@n-13.2),AT(6927,31),USE(L_L:Weight),RIGHT(1),TRN
                         STRING('Created:'),AT(42,250),USE(?String30),TRN
                         STRING('Client:'),AT(6865,250),USE(?STRING2)
                         STRING(@n_10b),AT(7187,250,510),USE(CLI:ClientNo),RIGHT(1)
                         STRING(@s100),AT(2135,250,3250),USE(L_RG:ClientRateType)
                         STRING('R. Type:'),AT(1677,250),USE(?STRING3)
                       END
repfooter              DETAIL,AT(0,0,7750,490),USE(?repfooter),ALONE
                         STRING('Key:'),AT(385,62),USE(?STRING1)
                         TEXT,AT(677,62,6854,354),USE(L_L:Key)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('NR = No Rate'),AT(3885,52),USE(?String24),TRN
                         STRING('SR = Setup Rate'),AT(4938,52),USE(?String24:2),TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'DI Date' & |
      '|' & 'Client Name' & |
      '|' & 'Client No.' & |
      '|' & 'Created Date & Time' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
  PRINT(RPT:repfooter)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Deliveries_Rates_Check')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_O:Date_Option',L_O:Date_Option)                  ! Added by: Report
  BIND('L_O:From_Date',L_O:From_Date)                      ! Added by: Report
  BIND('L_O:To_Date',L_O:To_Date)                          ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
      LOC:Options     = Ask_Rates_Check()
      IF L_O:From_Date = -1
         RETURN LEVEL:Fatal
      .
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
      IF L_O:Run_Option = 0   ! All
         L_L:Rpt_Desc     = 'All Entries'
      ELSE
         L_L:Rpt_Desc     = 'Variance'
      .
  
      CASE L_O:Date_Option
      OF 0                    ! DI Date
         L_L:Rpt_Desc     = CLIP(L_L:Rpt_Desc) & ' - Based on DI Dates - ' & FORMAT(L_O:From_Date,@d5) & ' to ' & FORMAT(L_O:To_Date,@d5)
      OF 1                    ! Created Date
         L_L:Rpt_Desc     = CLIP(L_L:Rpt_Desc) & ' - Based on Created Dates - ' & FORMAT(L_O:From_Date,@d5) & ' to ' & FORMAT(L_O:To_Date,@d5)
      OF 2                    ! All Entries
         L_L:Rpt_Desc     = CLIP(L_L:Rpt_Desc) & ' - All Dates'
      .
      ! Key
      ! L_L:Rate_Problem
      !   0 - No problems
      !   1 - Setup rate used
      !   2 - Missing Rate
      !   3 - Problem
  
      L_L:Key = 'SR = Setup Rate used,  NR = No Rate found,  PR = Problem with Rate'
    Output_Text('Key: SR = Setup Rate used, NR = No Rate found, PR = Problem with Rate',, '.\DI_Rates_Check.csv')
  
    Output_Text('"DI No.","DI Date","Client","DI Rate","Rate Set","Check Result","Freight","Min Charge"' & |
      ',"Weight","Created Time","Created Date","Rate Type","MIDs","Client No."',, '.\DI_Rates_Check.csv',0)
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF NOT TargetSelector.ASK(1) THEN
     SELF.Kill()
     RETURN Level:Fatal
  END
  IF NOT TargetSelector.GetPrintSelected() THEN
     SELF.SetReportTarget(TargetSelector.GetReportSelected())
  END
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Deliveries, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('DI Date')) THEN
     ThisReport.AppendOrder('+DEL:DIDateAndTime,+DEL:DINo')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('Client Name')) THEN
     ThisReport.AppendOrder('+CLI:ClientName,+DEL:DINo')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('Client No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientNo,+DEL:DINo')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('Created Date & Time')) THEN
     ThisReport.AppendOrder('+DEL:CreatedDateTime,+DEL:DINo')
  END
  ThisReport.SetFilter('L_O:Date_Option = 2 OR (L_O:Date_Option = 0 AND (DEL:DIDate >= L_O:From_Date AND DEL:DIDate << L_O:To_Date + 1) OR DEL:DIDate = 0) OR (L_O:Date_Option = 1 AND (DEL:CreatedDate >= L_O:From_Date AND DEL:CreatedDate << L_O:To_Date + 1) OR DEL:CreatedDate = 0)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Deliveries.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = 100
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      ! L_L:Rate_Problem
      !   0 - No problems
      !   1 - Setup rate used
      !   2 - Missing Rate
      !   3 - Problem
  
      L_L:Setup_Rate      = ''
  
      CASE L_L:Rate_Problem
      OF 1
         L_L:Setup_Rate   = 'SR'
      OF 2
         L_L:Setup_Rate   = 'NR'
      OF 3
         L_L:Setup_Rate   = 'PR'
      .
         
  
  
  
      IF L_L:Rate_Problem ~= 0
         IF L_L:Font_Style ~= 1
            SETTARGET(Report)
            ?DEL:Rate{PROP:FontStyle} = FONT:Bold
            SETTARGET()
            L_L:Font_Style    = 1
         .
      ELSIF L_L:Font_Style ~= 0
         SETTARGET(Report)
         ?DEL:Rate{PROP:FontStyle}    = FONT:Regular
         SETTARGET()
         L_L:Font_Style       = 0
      .
  
  
  
      CLEAR(LOC:MIDs)
                             ! (p:DID, p:No, p:MAN_DIID_List, p:List_Option, p:DIID)
      X_# = Get_Delivery_ManIDs(DEL:DID, 0, LOC:MIDs, 1)
  
  
      ! ---- Add to queue for output to file?  or output to file direct?
      !(p:Add, p:List, p:Delim, p:Option, p:Prefix)
      !(STRING, *STRING, STRING, BYTE=0, <STRING>)
      LOC:Output_Text = ''
      Add_To_List('"' & CLIP(DEL:DINo) & '"', LOC:Output_Text, ',')
      Add_To_List('"' & FORMAT(DEL:DIDate, @D06) & '"', LOC:Output_Text, ',')
      Add_To_List('"' & CLIP(CLI:ClientName) & '"', LOC:Output_Text, ',')
      Add_To_List('"' & CLIP(DEL:Rate) & '"', LOC:Output_Text, ',')
      Add_To_List('"' & CLIP(L_RG:RatePerKg) & '"', LOC:Output_Text, ',')
      Add_To_List('"' & CLIP(L_L:Setup_Rate) & '"', LOC:Output_Text, ',')    
      Add_To_List('"' & CLIP(DEL:Charge) & '"', LOC:Output_Text, ',')
      Add_To_List('"' & CLIP(L_L:MinimiumCharge) & '"', LOC:Output_Text, ',')
      Add_To_List('"' & CLIP(L_L:Weight) & '"', LOC:Output_Text, ',')  
  
      Add_To_List('"' & FORMAT(DEL:CreatedTime, @t04) & '"', LOC:Output_Text, ',')
      Add_To_List('"' & FORMAT(DEL:CreatedDate, @D06) & '"', LOC:Output_Text, ',')
      Add_To_List('"' & CLIP(L_RG:ClientRateType) & '"', LOC:Output_Text, ',')
      Add_To_List('"' & CLIP(LOC:MIDs) & '"', LOC:Output_Text, ',')
      Add_To_List('"' & CLIP(CLI:ClientNo) & '"', LOC:Output_Text, ',')
  
      ! (STRING, USHORT=0, STRING, BYTE=1)
      ! (P:Text, P:Chars, P:FileName, p:Create)
      Output_Text(LOC:Output_Text,, '.\DI_Rates_Check.csv',0)
  PRINT(RPT:Detail)
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      ! (ULONG, ULONG, ULONG, ULONG, ULONG, *DECIMAL, LONG=0, <*DECIMAL>, <BYTE>, <*DECIMAL>, <*LONG>, <*ULONG>, BYTE=0), STRING
      ! (p:DID, p:CID, p:JID, p:LTID, p:FID, p:Mass, p:Eff_Date, <*p:Mincharge>, <p:Setup>, <p:To_Weight>, <p:Effective_Date>,
      ! <p:RateID>, p:Quiet)
  
      ! L_L:Rate_Problem
      !   0 - No problems
      !   1 - Setup rate used
      !   2 - Missing Rate
      !   3 - Problem
  
      CLEAR(L_L:Rate_Problem)
  
      L_L:Weight      = Get_DelItem_s_Totals(DEL:DID,0,,,TRUE) / 100          ! Returns a Ulong
  
      LOC:RG          = Get_Delivery_Rate(DEL:DID, DEL:CID, DEL:JID, DEL:CRTID, DEL:FIDRate, L_L:Weight, DEL:DIDate, |
                                          L_L:MinimiumCharge, L_L:SetupRate, , L_L:Rate_Effective_Date, L_L:RID, TRUE)
  
      ! Add date to this string, might now be enough space...
      IF L_L:Rate_Effective_Date ~= 0
         L_RG:ClientRateType = FORMAT(L_L:Rate_Effective_Date, @d5) & ' - ' & CLIP(LEFT(L_RG:ClientRateType))
      .
  
      ! Should we show setup rates - L_L:SetupRate = TRUE
      IF L_O:Run_Option = 1
         IF L_O:Show_Setup_Rates = TRUE AND L_L:SetupRate = TRUE
            ! We used a setup rate, so show it
            L_L:Rate_Problem          = 1
         ELSE
            IF L_L:RID = 0
               ! We dont have a rate
               IF L_O:IncludeNoRates = FALSE
                  ReturnValue         = Record:Filtered
               .   
               L_L:Rate_Problem       = 2
            ELSIF L_RG:RatePerKg = 0.0
               IF L_O:IncludeZeroRates = FALSE              
                  ReturnValue            = Record:Filtered
               .   
            ELSIF L_RG:RatePerKg = DEL:Rate
               ReturnValue            = Record:Filtered
            ELSIF L_O:Show_When_DI_Rate = 1 AND DEL:Rate < L_RG:RatePerKg   ! 1 = higher only
               ReturnValue            = Record:Filtered
            ELSIF L_O:Show_When_DI_Rate = 2 AND DEL:Rate > L_RG:RatePerKg   ! 2 = lower only
               ReturnValue            = Record:Filtered
            ELSE
               ! Check if the Min charge was used
               IF L_L:MinimiumCharge = DEL:Charge
                  ReturnValue         = Record:Filtered
               ELSE
                  ! We have an extra ordinary rate  - show it
                  L_L:Rate_Problem    = 3
      .  .  .  .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Deliveries_Rates_Check','Print_Deliveries_Rates_Check','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Debtor_Listing PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:DefaultEmail     STRING(255)                           !
LOC:Open             BYTE                                  !
LOC:OnHold           BYTE                                  !
LOC:Closed           BYTE                                  !
LOC:Dormant          BYTE                                  !
LOC:ExportToCSV      BYTE                                  !Export the data to a CSV file as well (can use in spreadsheets etc)
LOC:Export_File_Group GROUP,PRE(LFG)                       !
FileName             STRING(500)                           !
FileOpen             BYTE                                  !
                     END                                   !
Process:View         VIEW(Clients)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:Status)
                       PROJECT(CLI:VATNo)
                       PROJECT(CLI:AID)
                       JOIN(ADD:PKey_AID,CLI:AID)
                         PROJECT(ADD:Fax)
                         PROJECT(ADD:Line1)
                         PROJECT(ADD:PhoneNo)
                         PROJECT(ADD:SUID)
                         JOIN(SUBU:PKey_SUID,ADD:SUID)
                           PROJECT(SUBU:PostalCode)
                           PROJECT(SUBU:Suburb)
                         END
                       END
                     END
ProgressWindow       WINDOW('Report Clients'),AT(,,229,174),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       SHEET,AT(2,2,225,151),USE(?Sheet1)
                         TAB('Options'),USE(?Tab_Options)
                           GROUP('Account Statuses to Include'),AT(31,25,167,73),USE(?Group1),BOXED
                             CHECK(' Open'),AT(46,39),USE(LOC:Open)
                             CHECK(' On Hold'),AT(46,54),USE(LOC:OnHold)
                             CHECK(' Closed'),AT(46,68),USE(LOC:Closed)
                             CHECK(' Dormant'),AT(46,82),USE(LOC:Dormant)
                           END
                           CHECK(' Export To CSV'),AT(31,119),USE(LOC:ExportToCSV),MSG('Export the data to a CSV f' & |
  'ile as well (can use in spreadsheets etc)'),TIP('Export the data to a CSV file as we' & |
  'll (can use in spreadsheets etc)')
                         END
                         TAB('Run'),USE(?Tab_Run),HIDE
                           PROGRESS,AT(59,56,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(43,44,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(43,70,141,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Pause'),AT(126,156,49,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                       BUTTON('Cancel'),AT(178,156,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Clients Report'),AT(250,854,7656,9646),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7656,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Client Listing'),AT(-21,21,7700),USE(?ReportTitle),FONT('Arial',18,,FONT:regular), |
  CENTER
                         BOX,AT(0,350,8000,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(604,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2927,350,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(6458,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         STRING('Client No.'),AT(73,385,,167),USE(?HeaderTitle:1),TRN
                         STRING('Client Name'),AT(667,385,1500,170),USE(?HeaderTitle:2),TRN
                         STRING('VAT No.'),AT(6563,385,990,156),USE(?HeaderTitle:6),TRN
                         STRING('Address'),AT(2979,385,1500,170),USE(?HeaderTitle:3),TRN
                       END
Detail                 DETAIL,AT(,,7656,417),USE(?Detail)
                         LINE,AT(0,0,0,427),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(604,-10,0,427),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(2927,0,0,218),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(6458,0,0,208),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(8000,0,0,427),USE(?DetailLine:5),COLOR(COLOR:Black)
                         STRING(@s20),AT(6563,31,990,156),USE(CLI:VATNo),LEFT
                         LINE,AT(3948,208,0,219),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(604,208,7656,0),USE(?Line10),COLOR(COLOR:Black)
                         STRING(@s10),AT(5729,31,625,156),USE(SUBU:PostalCode)
                         STRING(@n_10b),AT(-135,31,,170),USE(CLI:ClientNo),RIGHT,TRN
                         STRING(@s35),AT(667,31,2219,167),USE(CLI:ClientName),LEFT
                         STRING(@s35),AT(2969,31,1510,156),USE(ADD:Line1),LEFT
                         STRING(@s50),AT(4583,31,1042,156),USE(SUBU:Suburb),LEFT
                         STRING('Tel.:'),AT(677,240),USE(?String19),TRN
                         STRING(@s20),AT(927,240),USE(ADD:PhoneNo),TRN
                         STRING('Fax:'),AT(2344,240),USE(?String20),TRN
                         STRING(@s20),AT(2604,240),USE(ADD:Fax),TRN
                         STRING('Email:'),AT(4010,240),USE(?String22),TRN
                         STRING(@s255),AT(4375,240,3177,156),USE(LOC:DefaultEmail)
                         LINE,AT(0,427,8000,0),USE(?DetailEndLine:3),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,10500,7656,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

pfi        FILE,DRIVER('ASCII'),OWNER(GLO:DBOwner),NAME(''),PRE(pf),CREATE
record            RECORD
S1                  STRING(2550)
                  END
                END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
File_Setup                ROUTINE
  LOOP
    IF LOC:ExportToCSV = TRUE
      IF FILEDIALOG('File to Save list to (will be overwritten)', LFG:FileName, '*.csv', 3) = 0
        CASE MESSAGE('Cancel the file export?','Cancel',ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
        OF BUTTON:No
          CYCLE
        ELSE  
          LOC:ExportToCSV = FALSE
          BREAK
      . .
      
      pfi{PROP:Name}  = CLIP(LFG:FileName)
      CREATE(pfi)
      OPEN(pfi)
    
      IF ERRORCODE()
        CASE MESSAGE('Failed to create the file.||Error: ' & ERROR() & '||Try again?','',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:YES)
        OF BUTTON:NO
          LOC:ExportToCSV = FALSE
          BREAK
        .
      ELSE
        LFG:FileOpen      = TRUE        
        BREAK
      .
    ELSE
      BREAK
  . .

  IF LFG:FileOpen = TRUE
    CLEAR(PF:RECORD)
    pf:S1 = 'Client No., Client Name, Phone 1, Phone 2, Default Email Address'
    ADD(pfi)
  .
  EXIT
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Client Name' & |
      '|' & 'By Client No.' & |
      '|' & 'By Branch' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Debtor_Listing')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Open
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Open',LOC:Open)                                ! Added by: Report
  BIND('LOC:OnHold',LOC:OnHold)                            ! Added by: Report
  BIND('LOC:Closed',LOC:Closed)                            ! Added by: Report
  BIND('LOC:Dormant',LOC:Dormant)                          ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:EmailAddresses.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
      LOC:Open    = GETINI('Print_Debtor_Listing', 'Open', 1, GLO:Local_INI)
      LOC:OnHold  = GETINI('Print_Debtor_Listing', 'OnHold', 1, GLO:Local_INI)
      LOC:Closed  = GETINI('Print_Debtor_Listing', 'Closed', , GLO:Local_INI)
      LOC:Dormant = GETINI('Print_Debtor_Listing', 'Dormant', , GLO:Local_INI)
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF NOT TargetSelector.ASK(1) THEN
     SELF.Kill()
     RETURN Level:Fatal
  END
  IF NOT TargetSelector.GetPrintSelected() THEN
     SELF.SetReportTarget(TargetSelector.GetReportSelected())
  END
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Clients, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client Name')) THEN
     ThisReport.AppendOrder('+CLI:ClientName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientNo')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch')) THEN
     ThisReport.AppendOrder('+CLI:BID')
  END
  ThisReport.SetFilter('(LOC:Open = 1 AND CLI:Status = 0) OR (LOC:OnHold = 1 AND CLI:Status = 1) OR (LOC:Closed = 1 AND CLI:Status = 2) OR(LOC:Dormant = 1 AND CLI:Status = 3)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Clients.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Pause
      ThisWindow.Update()
        UNHIDE(?Tab_Run)
        HIDE(?Tab_Options)
      
      
        PUTINI('Print_Debtor_Listing', 'Open', LOC:Open, GLO:Local_INI)
        PUTINI('Print_Debtor_Listing', 'OnHold', LOC:OnHold, GLO:Local_INI)
        PUTINI('Print_Debtor_Listing', 'Closed', LOC:Closed, GLO:Local_INI)
        PUTINI('Print_Debtor_Listing', 'Dormant', LOC:Dormant, GLO:Local_INI)
      
      
        DO File_Setup
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
      IF LFG:FileOpen = TRUE
        CLOSE(pfi)
      .
      
        
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
    CLEAR(LOC:DefaultEmail)
  
    CLEAR(EMAI:Record, -1)
    EMAI:CID    = CLI:CID
    SET(EMAI:FKey_CID, EMAI:FKey_CID)
    LOOP
       NEXT(EmailAddresses)
       IF ERRORCODE()
          BREAK
       .
       IF EMAI:CID ~= CLI:CID
          BREAK
       .
  
       IF EMAI:RateLetter = TRUE
          LOC:DefaultEmail  = EMAI:EmailAddress
       .
  
       IF EMAI:DefaultAddress = TRUE
          LOC:DefaultEmail  = EMAI:EmailAddress
          BREAK
    .  .
  
      
    IF LFG:FileOpen = TRUE
      CLEAR(PF:RECORD)
      
      Add_to_List('"' & CLI:ClientNo & '"', pf:S1, ',')
      Add_to_List('"' & CLI:ClientName & '"', pf:S1, ',')
  
      Add_to_List('"' & ADD:PhoneNo & '"', pf:S1, ',')
      Add_to_List('"' & ADD:PhoneNo2 & '"', pf:S1, ',')
      Add_to_List('"' & LOC:DefaultEmail & '"', pf:S1, ',')
  
      ADD(pfi)
    .
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Debtor_Listing','Print_Debtor_Listing','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Journeys PROCEDURE 

Progress:Thermometer BYTE                                  !
Process:View         VIEW(Journeys)
                       PROJECT(JOU:Description)
                       PROJECT(JOU:Journey)
                       PROJECT(JOU:BID)
                       JOIN(BRA:PKey_BID,JOU:BID)
                         PROJECT(BRA:BranchName)
                       END
                     END
ProgressWindow       WINDOW('Report Journeys'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Journeys Report'),AT(250,854,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Journeys Listing'),AT(0,20,7750),USE(?ReportTitle),FONT('Arial',14,,FONT:bold),CENTER
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(5510,350,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         STRING('Journey'),AT(50,390,3775,170),USE(?HeaderTitle:1),TRN
                         STRING('Branch'),AT(5615,385,2083,167),USE(?HeaderTitle:2),TRN
                       END
Detail                 DETAIL,AT(,,7750,219),USE(?Detail)
                         LINE,AT(0,0,7750,0),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(0,0,0,220),USE(?DetailLine:01),COLOR(COLOR:Black)
                         LINE,AT(5510,0,0,220),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,220),USE(?DetailLine:2),COLOR(COLOR:Black)
                         STRING(@s70),AT(52,31,5406,167),USE(JOU:Journey),LEFT
                         STRING(@s35),AT(5615,31,,170),USE(BRA:BranchName)
                         LINE,AT(0,219,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
detail1                DETAIL,AT(,,,260),USE(?detail1)
                         TEXT,AT(885,21,6771,198),USE(JOU:Description),RESIZE
                         STRING('Description:'),AT(52,31),USE(?String11),TRN
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Journey' & |
      '|' & 'By Branch' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Journeys')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Journeys.Open                                     ! File Journeys used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Journeys',ProgressWindow)            ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Journeys, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Journey')) THEN
     ThisReport.AppendOrder('+JOU:Journey')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch')) THEN
     ThisReport.AppendOrder('+JOU:BID')
  END
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Journeys.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Journeys.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Journeys',ProgressWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:detail1)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
      IF CLIP(JOU:Description) ~= ''
         PRINT(RPT:detail1)
      .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Journeys','Print_Journeys','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

