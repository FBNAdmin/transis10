

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS002.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Transporters
!!! </summary>
Print_Creditor_Reciepts PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
Process:View         VIEW(TransporterPaymentsAllocations)
                       PROJECT(TRAPA:AllocationDate)
                       PROJECT(TRAPA:AllocationNo)
                       PROJECT(TRAPA:Amount)
                       PROJECT(TRAPA:Comment)
                       PROJECT(TRAPA:TPID)
                       JOIN(TRAP:PKey_TPID,TRAPA:TPID)
                         PROJECT(TRAP:Amount)
                         PROJECT(TRAP:DateCaptured)
                         PROJECT(TRAP:DateMade)
                         PROJECT(TRAP:Notes)
                         PROJECT(TRAP:TPID)
                         PROJECT(TRAP:TID)
                         JOIN(TRA:PKey_TID,TRAP:TID)
                           PROJECT(TRA:TID)
                           PROJECT(TRA:TransporterName)
                         END
                       END
                     END
ProgressWindow       WINDOW('Report Transporter Payments Allocations'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('TransporterPaymentsAllocations Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4), |
  FONT('MS Sans Serif',8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Transporter Payments'),AT(0,52,4844,260),USE(?ReportTitle),FONT(,14,,FONT:bold),CENTER
                         STRING(@d5b),AT(5417,104),USE(LO:From_Date),RIGHT(1)
                         STRING('-    '),AT(6198,104,208,156),USE(?String24),TRN
                         STRING(@d5b),AT(6615,104),USE(LO:To_Date),RIGHT(1)
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(885,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2052,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         STRING('Allocation No.'),AT(2083,396,833,167),USE(?HeaderTitle:3),CENTER,TRN
                         LINE,AT(2969,350,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         STRING('Allocation Date'),AT(52,396,750,167),USE(?HeaderTitle:1),TRN
                         STRING('Amount'),AT(990,396,990,167),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Comment'),AT(3073,396,1450,170),USE(?HeaderTitle:5),TRN
                       END
Transporter            BREAK(TRAP:TID)
                         HEADER,AT(0,0,,354),PAGEBEFORE(-1)
                           STRING(@s35),AT(1146,125),USE(TRA:TransporterName),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@n_10),AT(4167,125),USE(TRA:TID),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           STRING('Transporter:'),AT(260,125),USE(?String25),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         END
Payment                  BREAK(TRAP:TPID)
                           HEADER,AT(0,0,,490)
                             LINE,AT(52,31,7604,0),USE(?Line11:2),COLOR(COLOR:Black),LINEWIDTH(2)
                             STRING(@d17),AT(6802,281),USE(TRAP:DateMade)
                             STRING('Comment:'),AT(260,281),USE(?String21:2),TRN
                             STRING(@d17),AT(6802,73),USE(TRAP:DateCaptured)
                             STRING('Payment ID (TPID):'),AT(4063,73),USE(?String23:3),TRN
                             STRING('Date Made:'),AT(5896,281),USE(?String23:2),TRN
                             STRING('Pay Amount:'),AT(260,73),USE(?String21),TRN
                             STRING(@n-14.2),AT(979,73,1000,167),USE(TRAP:Amount),RIGHT(1)
                             STRING(@n_10),AT(4990,73),USE(TRAP:TPID),RIGHT(1),TRN
                             STRING('Date Captured:'),AT(5896,73),USE(?String23),TRN
                             STRING(@s255),AT(1042,281,4635,156),USE(TRAP:Notes)
                             LINE,AT(0,490,7750,0),USE(?DetailEndLine2),COLOR(COLOR:Black)
                           END
Detail                     DETAIL,AT(,,7750,250),USE(?Detail)
                             LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                             LINE,AT(885,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                             LINE,AT(2052,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                             LINE,AT(2969,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                             LINE,AT(7750,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                             STRING(@d17),AT(52,52,750,167),USE(TRAPA:AllocationDate),RIGHT(1)
                             STRING(@n-14.2),AT(979,52,1000,167),USE(TRAPA:Amount),RIGHT
                             STRING(@n_5),AT(2167,52,729,156),USE(TRAPA:AllocationNo),RIGHT
                             STRING(@s255),AT(3073,52,4635,156),USE(TRAPA:Comment),LEFT
                             LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                           END
                           FOOTER,AT(0,0,,302)
                             STRING('Allocated Sub Total:'),AT(104,31),USE(?String30),TRN
                             STRING(@n-14.2),AT(979,31,1000,167),USE(TRAPA:Amount,,?TRAPA:Amount:2),RIGHT,SUM,RESET(Payment), |
  TRN
                           END
                         END
                         FOOTER,AT(0,0,,510),USE(?Transporter_Tot)
                           STRING('Payment Total:'),AT(104,156),USE(?String30:2),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@n-14.2),AT(979,156),USE(TRAP:Amount,,?TRAP:Amount:2),FONT(,,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),SUM,RESET(Transporter),TRN
                           STRING('Allocated Total:'),AT(2458,156),USE(?String30:5),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@n-14.2),AT(3344,156,1000,167),USE(TRAPA:Amount,,?TRAPA:Amount:4),FONT(,,,FONT:bold, |
  CHARSET:ANSI),RIGHT,SUM,TALLY(Detail),RESET(Transporter),TRN
                           STRING('Transporter:'),AT(4802,156),USE(?String25:2),FONT(,,,FONT:regular,CHARSET:ANSI),TRN
                           STRING(@s35),AT(5542,156),USE(TRA:TransporterName,,?TRA:TransporterName:2),FONT(,,,FONT:regular, |
  CHARSET:ANSI),TRN
                           LINE,AT(52,396,7604,0),USE(?Line11:4),COLOR(COLOR:Black),LINEWIDTH(2)
                           LINE,AT(52,52,7604,0),USE(?Line11:3),COLOR(COLOR:Black),LINEWIDTH(2)
                         END
                       END
detail_totals          DETAIL,AT(,,,854),USE(?detail_totals)
                         STRING('Allocated Grand Total:'),AT(792,240),USE(?String30:4),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Payment Grand Total:'),AT(792,500),USE(?String30:3),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@n-14.2),AT(2219,240,990,156),USE(TRAPA:Amount,,?TRAPA:Amount:3),FONT(,,,FONT:bold, |
  CHARSET:ANSI),RIGHT,SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(2219,500,990,156),USE(TRAP:Amount,,?TRAP:Amount:3),FONT(,,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),SUM,TALLY(Payment),TRN
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Transporter' & |
      '|' & 'By Transporter & Payment Date' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:detail_totals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Creditor_Reciepts')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:TransporterPaymentsAllocations.SetOpenRelated()
  Relate:TransporterPaymentsAllocations.Open               ! File TransporterPaymentsAllocations used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Creditor_Reciepts',ProgressWindow)   ! Restore window settings from non-volatile store
      LO:From_Date    = GETINI('Print_Reciepts_Creditor', 'From_Date', '', GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Reciepts_Creditor', 'To_Date', '', GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      IF LO:To_Date > 0
         PUTINI('Print_Reciepts_Creditor', 'From_Date', LO:From_Date, GLO:Local_INI)
         PUTINI('Print_Reciepts_Creditor', 'To_Date', LO:To_Date, GLO:Local_INI)
      ELSE
         ReturnValue  = LEVEL:Fatal
      .
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:TransporterPaymentsAllocations, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter')) THEN
     ThisReport.AppendOrder('+TRA:TransporterName,+TRAPA:TPID,+TRAPA:AllocationNo')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter & Payment Date')) THEN
     ThisReport.AppendOrder('+TRA:TransporterName,+TRAP:DateMade,+TRAPA:AllocationNo')
  END
  ThisReport.SetFilter('TRAP:DateMade >= LO:From_Date AND TRAP:DateMade << (LO:To_Date + 1)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:TransporterPaymentsAllocations.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TransporterPaymentsAllocations.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Creditor_Reciepts',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:detail_totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Creditor_Reciepts','Print_Creditor_Reciepts','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! Transporters
!!! </summary>
Print_Creditor_Reciepts_Simple PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
Process:View         VIEW(TransporterPayments)
                       PROJECT(TRAP:Amount)
                       PROJECT(TRAP:DateMade)
                       PROJECT(TRAP:Notes)
                       PROJECT(TRAP:TPID)
                       PROJECT(TRAP:TID)
                       JOIN(TRA:PKey_TID,TRAP:TID)
                         PROJECT(TRA:TID)
                         PROJECT(TRA:TransporterName)
                       END
                     END
ProgressWindow       WINDOW('Report Transporter Payments Allocations'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('TransporterPaymentsAllocations Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4), |
  FONT('MS Sans Serif',8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Transporter Payments'),AT(0,52,4844,260),USE(?ReportTitle),FONT(,14,,FONT:bold),CENTER
                         STRING(@d5b),AT(5417,104),USE(LO:From_Date),RIGHT(1)
                         STRING('-    '),AT(6198,104,208,156),USE(?String24),TRN
                         STRING(@d5b),AT(6615,104),USE(LO:To_Date),RIGHT(1)
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(885,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2052,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         STRING('Payment ID'),AT(2083,396,833,167),USE(?HeaderTitle:3),CENTER,TRN
                         LINE,AT(2969,350,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         STRING('Date Made'),AT(52,396,750,167),USE(?HeaderTitle:1),TRN
                         STRING('Amount'),AT(990,396,990,167),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Comment'),AT(3073,396,1450,170),USE(?HeaderTitle:5),TRN
                       END
Transporter            BREAK(TRAP:TID)
                         HEADER,AT(0,0,,354)
                           STRING(@s35),AT(1146,125),USE(TRA:TransporterName),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@n_10),AT(4167,125),USE(TRA:TID),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           LINE,AT(0,354,7750,0),USE(?Line12),COLOR(COLOR:Black)
                           STRING('Transporter:'),AT(260,125),USE(?String25),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         END
Detail                   DETAIL,AT(,,7750,250),USE(?Detail)
                           LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                           LINE,AT(885,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                           LINE,AT(2052,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                           LINE,AT(2969,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                           LINE,AT(7750,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                           STRING(@d17),AT(52,52),USE(TRAP:DateMade)
                           STRING(@n-14.2),AT(938,52,1000,167),USE(TRAP:Amount),RIGHT(1)
                           STRING(@n_10),AT(2240,52),USE(TRAP:TPID),RIGHT(1),TRN
                           STRING(@s255),AT(3073,52,4635,156),USE(TRAP:Notes)
                           LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         END
                         FOOTER,AT(0,0,,417),USE(?Transporter_Tot)
                           STRING('Payment Total:'),AT(104,104),USE(?String30:2),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@n-14.2),AT(979,104),USE(TRAP:Amount,,?TRAP:Amount:2),FONT(,,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),SUM,TALLY(Detail),RESET(Transporter),TRN
                           STRING('Transporter:'),AT(4802,104),USE(?String25:2),FONT(,,,FONT:regular,CHARSET:ANSI),TRN
                           STRING(@s35),AT(5542,104),USE(TRA:TransporterName,,?TRA:TransporterName:2),FONT(,,,FONT:regular, |
  CHARSET:ANSI),TRN
                           LINE,AT(52,292,7604,0),USE(?Line11:4),COLOR(COLOR:Black),LINEWIDTH(2)
                           LINE,AT(52,52,7604,0),USE(?Line11:3),COLOR(COLOR:Black),LINEWIDTH(2)
                         END
                       END
detail_totals          DETAIL,AT(,,,896),USE(?detail_totals)
                         STRING('Payment Grand Total:'),AT(792,458),USE(?String30:3),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@n-14.2),AT(2219,458,990,156),USE(TRAP:Amount,,?TRAP:Amount:3),FONT(,,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),SUM,TALLY(Detail),TRN
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:detail_totals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Creditor_Reciepts_Simple')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:TransporterPayments.SetOpenRelated()
  Relate:TransporterPayments.Open                          ! File TransporterPayments used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Creditor_Reciepts_Simple',ProgressWindow) ! Restore window settings from non-volatile store
      LO:From_Date    = GETINI('Print_Reciepts_Creditor_Simple', 'From_Date', '', GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Reciepts_Creditor_Simple', 'To_Date', '', GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      IF LO:To_Date > 0
         PUTINI('Print_Reciepts_Creditor_Simple', 'From_Date', LO:From_Date, GLO:Local_INI)
         PUTINI('Print_Reciepts_Creditor_Simple', 'To_Date', LO:To_Date, GLO:Local_INI)
      ELSE
         ReturnValue  = LEVEL:Fatal
      .
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:TransporterPayments, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder('+TRA:TransporterName,+TRAP:DateMade')
  ThisReport.SetFilter('TRAP:DateMade >= LO:From_Date AND TRAP:DateMade << (LO:To_Date + 1)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:TransporterPayments.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TransporterPayments.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Creditor_Reciepts_Simple',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:detail_totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Creditor_Reciepts_Simple','Print_Creditor_Reciepts_Simple','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

