

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abbreak.inc'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS018.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!!  
!!! </summary>
Print_Creditor_Invoices_21_June_07 PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
LOC:Report_Group     GROUP,PRE(L_RG)                       !
Total                DECIMAL(12,2)                         !
Owing                DECIMAL(11,2)                         !Owing amount
                     END                                   !
LOC:Options_Extra    GROUP,PRE(L_OE)                       !
Run_Type             BYTE                                  !
Payment              BYTE(255)                             !Payment options
BranchName           STRING(35)                            !Branch Name
BID                  ULONG                                 !Branch ID
BID_FirstTime        BYTE(1)                               !
                     END                                   !
Process:View         VIEW(_InvoiceTransporter)
                       PROJECT(INT:BID)
                       PROJECT(INT:Broking)
                       PROJECT(INT:CR_TIN)
                       PROJECT(INT:Cost)
                       PROJECT(INT:InvoiceDate)
                       PROJECT(INT:MID)
                       PROJECT(INT:Status)
                       PROJECT(INT:TID)
                       PROJECT(INT:TIN)
                       PROJECT(INT:VAT)
                       JOIN(TRA:PKey_TID,INT:TID)
                         PROJECT(TRA:TransporterName)
                       END
                       JOIN(BRA:PKey_BID,INT:BID)
                         PROJECT(BRA:BranchName)
                       END
                     END
FDB5::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop_Branch QUEUE                           !Queue declaration for browse/combo box using ?L_OE:BranchName
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
ProgressWindow       WINDOW('Report Invoice Transporter'),AT(,,210,139),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,MDI,TIMER(1)
                       SHEET,AT(4,4,203,114),USE(?Sheet1)
                         TAB('Process'),USE(?Tab1),HIDE
                           PROGRESS,AT(51,57,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(35,43,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(35,74,141,10),USE(?Progress:PctText),CENTER
                         END
                         TAB('Options'),USE(?Tab2)
                           PROMPT('Run Type:'),AT(13,26),USE(?L_OE:Run_Type:Prompt)
                           LIST,AT(77,26,122,10),USE(L_OE:Run_Type),VSCROLL,DROP(15),FROM('All|#0|Invoices  (MID)|' & |
  '#1|Additionals  (MID & Adjustment Invoice No.)|#2|Office Gen.  (MID)|#3|Credit Notes' & |
  '|#4|Extra Invoices (flag)|#5')
                           GROUP,AT(11,53,188,10),USE(?Group1)
                             PROMPT('Payment:'),AT(13,53),USE(?L_OE:Payment:Prompt)
                             LIST,AT(77,53,122,10),USE(L_OE:Payment),VSCROLL,DROP(15),FROM('All|#255|No Payments|#0|' & |
  'Partially Paid|#1|Amount Owing|#250|Fully Paid|#3'),MSG('Payment options'),TIP('Payment options')
                           END
                           PROMPT('Branch Name:'),AT(13,82),USE(?BranchName:Prompt:2)
                           LIST,AT(77,82,122,10),USE(L_OE:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop_Branch)
                         END
                       END
                       BUTTON('Cancel'),AT(158,122,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                       BUTTON('Pause'),AT(102,122,,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                     END

Report               REPORT('_InvoiceTransporter Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Transporter Invoices'),AT(0,52,4844,260),USE(?ReportTitle),FONT(,14,,FONT:regular), |
  CENTER
                         STRING(@d5b),AT(5469,104),USE(LO:From_Date),RIGHT(1)
                         STRING(' - '),AT(6177,104,156,156),USE(?String24),TRN
                         STRING(@d5b),AT(6458,104),USE(LO:To_Date),RIGHT(1)
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(711,350,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1354,365,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(2948,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(3688,350,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(4521,350,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(5417,350,0,250),USE(?HeaderLine:8),COLOR(COLOR:Black)
                         LINE,AT(6344,350,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         LINE,AT(7000,350,0,250),USE(?HeaderLine:71),COLOR(COLOR:Black)
                         STRING('Inv. Date'),AT(52,406,583,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Total'),AT(5385,406,896,167),USE(?HeaderTitle:8),CENTER,TRN
                         STRING('Branch'),AT(6365,406,552,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Transporter'),AT(1406,406,1510,156),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('MID'),AT(2958,406,688,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('Cost'),AT(3646,406,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('VAT'),AT(4531,406,833,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('Broking'),AT(7031,406,656,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('TIN'),AT(688,406,688,167),USE(?HeaderTitle:10),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(711,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(1354,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(2948,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(3688,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(4521,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                         LINE,AT(5417,0,0,250),USE(?DetailLine:9),COLOR(COLOR:Black)
                         LINE,AT(6344,0,0,250),USE(?DetailLine:10),COLOR(COLOR:Black)
                         LINE,AT(7000,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         STRING(@d5b),AT(52,52,,167),USE(INT:InvoiceDate),RIGHT(2)
                         STRING(@n_10),AT(646,52),USE(INT:TIN),RIGHT(1),TRN
                         STRING(@s35),AT(1406,52,1510,156),USE(TRA:TransporterName),LEFT(1),TRN
                         STRING(@n_10),AT(2958,52,688,167),USE(INT:MID),RIGHT,TRN
                         STRING(@n-14.2),AT(3646,52,833,167),USE(INT:Cost),RIGHT,TRN
                         STRING(@n-14.2),AT(4531,52,833,167),USE(INT:VAT),RIGHT(1),TRN
                         STRING(@n-15.2),AT(5385,52),USE(L_RG:Total),RIGHT(1),TRN
                         STRING(@s8),AT(6406,52,,167),USE(BRA:BranchName)
                         CHECK('Broking'),AT(7083,52,,167),USE(INT:Broking)
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
totals                 DETAIL,AT(,,,396),USE(?totals)
                         LINE,AT(625,52,5781,0),USE(?Line18),COLOR(COLOR:Black)
                         STRING('Totals:'),AT(52,104),USE(?String29),TRN
                         STRING(@n_10),AT(646,104,688,167),USE(INT:TID,,?INT:TID:2),RIGHT,CNT,TRN
                         STRING(@n-16.2),AT(3521,104,958,167),USE(INT:Cost,,?INT:Cost:2),RIGHT,SUM,TRN
                         STRING(@n-16.2),AT(4406,104,958,167),USE(INT:VAT,,?INT:VAT:2),RIGHT(1),SUM,TRN
                         STRING(@n-16.2),AT(5323,104),USE(L_RG:Total,,?L_RG:Total:2),RIGHT(1),SUM,TRN
                         LINE,AT(625,313,5781,0),USE(?Line18:2),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
BreakMgr             BreakManagerClass                     ! Break Manager
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

FDB5                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop_Branch         !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Invoice Date' & |
      '|' & 'By Transporter Invoice No.' & |
      '|' & 'By Transporter & Date' & |
      '|' & 'By Branch & Date' & |
      '|' & 'By Manifest & Date' & |
      '|' & 'By User & Date' & |
      '|' & 'By Delivery & Date (extra legs)' & |
      '|' & 'By Credited Invoice & Date' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:totals)
  
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Creditor_Invoices_21_June_07')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  BIND('L_OE:BID',L_OE:BID)                                ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BreakMgr.Init()
  BreakMgr.AddBreak()
  BreakMgr.AddLevel() !TransporterFooter
  BreakMgr.AddResetField(INT:TID)
  SELF.AddItem(BreakMgr)
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Creditor_Invoices_21_June_07',ProgressWindow) ! Restore window settings from non-volatile store
      LO:From_Date    = GETINI('Print_Creditor_Invoices', 'From_Date', '', GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Creditor_Invoices', 'To_Date', '', GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      IF LO:To_Date > 0
         PUTINI('Print_Creditor_Invoices', 'From_Date', LO:From_Date, GLO:Local_INI)
         PUTINI('Print_Creditor_Invoices', 'To_Date', LO:To_Date, GLO:Local_INI)
      ELSE
         ReturnValue  = LEVEL:Fatal
      .
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:_InvoiceTransporter, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice Date')) THEN
     ThisReport.AppendOrder('+INT:InvoiceDate')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter Invoice No.')) THEN
     ThisReport.AppendOrder('+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter & Date')) THEN
     ThisReport.AppendOrder('+INT:TID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch & Date')) THEN
     ThisReport.AppendOrder('+INT:BID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Manifest & Date')) THEN
     ThisReport.AppendOrder('+INT:MID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By User & Date')) THEN
     ThisReport.AppendOrder('+INT:UID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Delivery & Date (extra legs)')) THEN
     ThisReport.AppendOrder('+INT:DID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Credited Invoice & Date')) THEN
     ThisReport.AppendOrder('+INT:CR_TIN,+INT:InvoiceDate,+INT:TIN')
  END
  ThisReport.SetFilter('INT:InvoiceDate >= LO:From_Date AND INT:InvoiceDate << (LO:To_Date + 1) AND (L_OE:BID = 0 OR L_OE:BID = INT:BID)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_InvoiceTransporter.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  FDB5.Init(?L_OE:BranchName,Queue:FileDrop_Branch.ViewPosition,FDB5::View:FileDrop,Queue:FileDrop_Branch,Relate:Branches,ThisWindow)
  FDB5.Q &= Queue:FileDrop_Branch
  FDB5.AddSortOrder(BRA:Key_BranchName)
  FDB5.AddField(BRA:BranchName,FDB5.Q.BRA:BranchName) !List box control field - type derived from field
  FDB5.AddField(BRA:BID,FDB5.Q.BRA:BID) !Primary key field - type derived from field
  FDB5.AddUpdateField(BRA:BID,L_OE:BID)
  ThisWindow.AddItem(FDB5.WindowComponent)
  FDB5.DefaultFill = 0
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Creditor_Invoices_21_June_07',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_OE:Run_Type
          CASE L_OE:Run_Type + 1
          OF 1            ! All
             ENABLE(?Group1)
          OF 2            ! Invoices
             ENABLE(?Group1)
          OF 3            ! Additionals
             ENABLE(?Group1)
          OF 4            ! Office Gen.
             ENABLE(?Group1)
          OF 5            ! Credit Notes
             DISABLE(?Group1)
          OF 6            ! Extra Invoices (flag)
             ENABLE(?Group1)
          ELSE
             DISABLE(?Group1)
          .
    OF ?Pause
      ThisWindow.Update()
          UNHIDE(?Tab1)
          HIDE(?Tab2)
      
      
          ! 'All|#0|Invoices|#1|Additionals|#2|Office Gen.|#3|Credit Notes|#4'
      
          CASE L_OE:Run_Type + 1
          OF 1            ! All
          OF 2            ! Invoices
             ThisReport.SetFilter('INT:Cost >= 0.0 AND INT:MID <> 0 AND (INT:CR_TIN = 0 OR SQL(CR_TIN IS NULL))', 'ikb')
          OF 3            ! Additionals
             ThisReport.SetFilter('INT:MID <> 0 AND INT:CR_TIN <> 0', 'ikb')
          OF 4            ! Office Gen.
             ThisReport.SetFilter('INT:MID = 0', 'ikb')
          OF 5            ! Credit Notes
             ThisReport.SetFilter('INT:Cost < 0.0', 'ikb')
          OF 6            ! Extra Invoices
             ThisReport.SetFilter('INT:ExtraInv = 1', 'ikb')
          .
      
      
          ! All|No Payments|Partially Paid|Amount Owing|Fully Paid
          ! 255|0|1|250|3
      
          CASE L_OE:Payment
          OF 255
          OF 250
             ThisReport.SetFilter('INT:Status <= 1', 'ikb2')
          OF 0
             ThisReport.SetFilter('INT:Status = 0', 'ikb2')
          OF 1
             ThisReport.SetFilter('INT:Status = 1', 'ikb2')
          OF 3
             ThisReport.SetFilter('INT:Status = 3', 'ikb2')
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      L_RG:Total      = INT:Cost + INT:VAT
  
  
      L_RG:Owing      = Get_InvTransporter_Outstanding(INT:TIN)
  ReturnValue = PARENT.TakeRecord()
    IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter Invoice No.')) OR |
            (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter & Date'))
       PRINT(RPT:totals)
    .
  IF 0
    PRINT(RPT:totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Creditor_Invoices','Print_Creditor_Invoices','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True


FDB5.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
        Queue:FileDrop_Branch.BRA:BranchName       = 'All'
        GET(Queue:FileDrop_Branch, Queue:FileDrop_Branch.BRA:BranchName)
        IF ERRORCODE()
           CLEAR(Queue:FileDrop_Branch)
           Queue:FileDrop_Branch.BRA:BranchName    = 'All'
           Queue:FileDrop_Branch.BRA:BID           = 0
           ADD(Queue:FileDrop_Branch)
        .
    
    
        IF L_OE:BID_FirstTime = TRUE
           SELECT(?L_OE:BranchName, RECORDS(Queue:FileDrop_Branch))
    
           L_OE:BID         = 0
           L_OE:BranchName  = 'All'
        .
    
        L_OE:BID_FirstTime     = FALSE
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Process
!!! Process the Clients File
!!! </summary>
Process_Clients PROCEDURE 

Progress:Thermometer BYTE                                  !
Process:View         VIEW(Clients)
                     END
ProgressWindow       WINDOW('Process Clients'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_Clients')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:Clients, ?Progress:PctText, Progress:Thermometer, ProgressMgr, CLI:CID)
  ThisProcess.AddSortOrder(CLI:PKey_CID)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  SELF.SetUseMRP(False)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(Clients,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      CLI:ClientSearch    = Get_Client_Search(CLI:ClientName)
  PUT(Process:View)
  IF ERRORCODE()
    GlobalErrors.ThrowFile(Msg:PutFailed,'Process:View')
    ThisWindow.Response = RequestCompleted
    ReturnValue = Level:Fatal
  END
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Transporters PROCEDURE 

Progress:Thermometer BYTE                                  !
Address              STRING(350)                           !
Telephone            STRING(14)                            !
Fax                  STRING(14)                            !
Mobile               STRING(14)                            !
LOC:Address_Option   BYTE(1)                               !
Process:View         VIEW(Transporter)
                       PROJECT(TRA:TransporterName)
                     END
ProgressWindow       WINDOW('Report Transporter'),AT(,,259,174),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       SHEET,AT(4,4,251,151),USE(?Sheet1)
                         TAB('Progress'),USE(?Tab1),HIDE
                           PROGRESS,AT(75,68,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(59,56,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(59,84,141,10),USE(?Progress:PctText),CENTER
                         END
                         TAB('Options'),USE(?Tab2)
                           PROMPT('Address Option:'),AT(30,48),USE(?LOC:Address_Option:Prompt)
                           LIST,AT(86,48,121,10),USE(LOC:Address_Option),DROP(5),FROM('Full Address|#0|Exclude Cit' & |
  'y & Province|#1')
                         END
                       END
                       BUTTON('&Pause'),AT(206,156,49,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                       BUTTON('Cancel'),AT(154,156,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Transporter Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Transporter Details'),AT(0,20,7750),USE(?ReportTitle),FONT('Arial',18,,FONT:regular), |
  CENTER
                         BOX,AT(0,344,7750,250),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(1667,354,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(4792,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(5792,354,0,250),USE(?HeaderLine:9),COLOR(COLOR:Black)
                         LINE,AT(6771,354,0,250),USE(?HeaderLine:10),COLOR(COLOR:Black)
                         STRING('Fax'),AT(5844,385,,170),USE(?HeaderTitle:5),TRN
                         STRING('Cell / Mobile'),AT(6792,385,,170),USE(?HeaderTitle:6),TRN
                         STRING('Address'),AT(1719,417,1198,156),USE(?HeaderTitle:7),TRN
                         STRING('Transporter Name'),AT(83,385,1563,156),USE(?HeaderTitle:2),TRN
                         STRING('Phone'),AT(4865,385,,170),USE(?HeaderTitle:4),TRN
                       END
Detail                 DETAIL,AT(0,0,7750,260),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(1667,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(4792,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(5792,0,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         STRING(@s35),AT(83,52,1563,156),USE(TRA:TransporterName),LEFT
                         STRING(@s14),AT(4865,52),USE(Telephone),TRN
                         STRING(@s14),AT(5844,52),USE(Fax),TRN
                         LINE,AT(6771,0,0,250),USE(?HeaderLine:8),COLOR(COLOR:Black)
                         STRING(@s14),AT(6792,52),USE(Mobile),TRN
                         TEXT,AT(1719,52,3021,156),USE(Address),BOXED,RESIZE
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Transporters')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Transporter, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder('+TRA:TransporterName')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Transporter.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
          UNHIDE(?Tab1)
          HIDE(?Tab2)
      
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      ! (p:AID, p:Type, , BYTE=0)
      ! (ULONG, BYTE=0, BYTE=0),STRING
      ! p:Type
      !   0 - Comma delimited string
      !   1 - Block
      !   2 - Comma delimited string - including empty Line 1 & 2 where they are empty
      ! p:Details
      !   0 - all
      !   1 - without City and Province
      !
      ! Returns
      !   Address Name, Line1 (if), Line2 (if), Post Code, City, Province
      !   or
      !   Address Name, Line1 (if), Line2 (if), Post Code
      !   or
      !   Address Name, Line1 (if), Line2 (if), <missing info string>
  
      Address     = Get_Address(TRA:AID,, LOC:Address_Option)
  
      ADD:AID     = TRA:AID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) ~= LEVEL:Benign
         CLEAR(ADD:Record)
      .
  
      Telephone   = ADD:PhoneNo
  
      Fax         = ADD:Fax
  
      Mobile      = ADD:PhoneNo2
  
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Transporters','Print_Transporters','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

