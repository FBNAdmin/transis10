

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS011.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Type is for Continuous or Laser,  Option is for COD
!!! </summary>
Print_Man_Invs PROCEDURE (p:MID, p:Type, p:Option, p:Preview)

LOC:Complete         BYTE                                  !
LOC:Pause            BYTE                                  !
LOC:State            STRING(100)                           !
LOC:Info             STRING(255)                           !
LOC:ChangeTime       LONG                                  !
LOC:No_Printed       ULONG                                 !
LOC:Result           LONG                                  !
LOC:SQL_Str          STRING(2000)                          !
LOC:Total_Invoices   LONG                                  !
LOC:Inv_No           LONG                                  !
LOC:Page_Print_Time  SHORT                                 !In seconds
LOC:Print_Now        BYTE                                  !
LOC:TimeOut          LONG                                  !
LOC:Last_Print_Time  LONG                                  !
LOC:DIDs_Q           QUEUE,PRE(L_DQ)                       !
DID                  ULONG                                 !Delivery ID
                     END                                   !
LOC:Idx              ULONG                                 !
LOC:Preview_Option   BYTE                                  !
LOC:Print_Result     LONG                                  !
QuickWindow          WINDOW('Print Manifest Invoices'),AT(,,225,152),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,IMM,MDI,HLP('Print_DN_POD_Cont'),SYSTEM,TIMER(50)
                       SHEET,AT(4,4,217,130),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           STRING(@s100),AT(18,22,189,10),USE(LOC:State),FONT(,,,FONT:bold,CHARSET:ANSI),CENTER,TRN
                           PROMPT(''),AT(18,36,189,56),USE(?Prompt1),LEFT,TRN
                           LINE,AT(10,97,205,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Page Print Time (seconds):'),AT(18,103),USE(?LOC:Page_Print_Time:Prompt),TRN
                           SPIN(@n5),AT(114,103,48,9),USE(LOC:Page_Print_Time),RIGHT(1),MSG('In seconds'),SKIP,TIP('In seconds')
                           PROMPT(''),AT(18,120,189,9),USE(?Prompt_Time),CENTER,TRN
                         END
                       END
                       BUTTON('&Help'),AT(4,136,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       CHECK('Pause'),AT(118,136,49,14),USE(LOC:Pause),LEFT,ICON('VCR_Ps.ico'),FLAT
                       BUTTON('&Cancel'),AT(172,136,49,14),USE(?Cancel:2),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

MAN_Del         VIEW(ManifestLoadAlias)
    PROJECT(A_MAL:MLID, A_MAL:MID)
       JOIN(A_MALD:FSKey_MLID_DIID, A_MAL:MLID)
       PROJECT(A_MALD:MLID, A_MALD:DIID)
          JOIN(A_DELI:PKey_DIID, A_MALD:DIID)
          PROJECT(A_DELI:DIID, A_DELI:DID)
             JOIN(A_DEL:PKey_DID, A_DELI:DID)
             PROJECT(A_DEL:DID, A_DEL:DINo, A_DEL:DIDate, A_DEL:Terms)
    .  .  .  .


Del_View        ViewManager


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Man_Invs')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:State
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:ManifestLoadAlias.Open                            ! File ManifestLoadAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Print_Man_Invs',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ManifestLoadAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Man_Invs',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Cancel:2
      ThisWindow.Update()
          QuickWindow{PROP:Timer} = 0
      
          CASE MESSAGE('Cancel the Printing now?', 'Confirm Cancel', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
          OF BUTTON:No
             QuickWindow{PROP:Timer} = 50
          OF BUTTON:Yes
             POST(EVENT:CloseWindow)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          LOC:Page_Print_Time = GETINI('Print_Man_Invs', 'Page_Print_Time', 8, GLO:Local_INI)
          IF LOC:Page_Print_Time < 2
             LOC:Page_Print_Time  = 2
          .
      
          ! Set it up to always wait before printing if Last Print Time was recent... from ANY cont print!
          LOC:Last_Print_Time = GETINI('Print_DN_POD_Cont', 'Last_Cont_Print_Time', 0, GLO:Local_INI)
      
      !    db.debugout('LOC:Last_Print_Time: ' & FORMAT(LOC:Last_Print_Time, @t4) & '||Clock: ' & FORMAT(CLOCK(),@t4))
      
          LOC:Print_Now       = FALSE
          ! (p:MID, p:Type, p:Option, p:Preview)
          ! (ULONG, BYTE, BYTE=0, BYTE=1),LONG,PROC
          !   p:Option
          !       0 - COD invoices
          !       1 - All
          !   p:Type
          !       0 - Continuous
          !       1 - Page printer
      
      
          ! Print_Man_Invs(MAN:MID, 0,, 0)       ! No preview on auto print
      
          !       Print_Man_Invs(MAN:MID, 0, 1)
      
      
      
          PUSHBIND
          BIND('A_DEL:Terms', A_DEL:Terms)
      
          Del_View.Init(MAN_Del, Relate:ManifestLoadAlias)
          Del_View.AddSortOrder(A_MAL:FKey_MID)
          Del_View.AppendOrder('A_DEL:DID,A_DELI:DIID')
          Del_View.AddRange(A_MAL:MID, p:MID)
      
          IF p:Option = 0
             ! Pre Paid|COD|Account
             Del_View.SetFilter('A_DEL:Terms = 1')
          .
      
      
          !MAN_Del{PROP:Filter}    = 'A_MAL:MID = ' & p:MID
          !MAN_Del{PROP:Order}     = 'A_DEL:DID,A_DELI:DIID'
          !OPEN(MAN_Del)
          !SET(MAN_Del)
          Del_View.Reset()
          LOOP
             IF Del_View.Next() ~= LEVEL:Benign
                BREAK
             .
      
             ! We will have every Item entry listed when we really just want the DIDs.
             ! DID will not be unique
             L_DQ:DID     = A_DEL:DID
             GET(LOC:DIDs_Q, L_DQ:DID)
             IF ERRORCODE()
                L_DQ:DID  = A_DEL:DID
                ADD(LOC:DIDs_Q)
          .  .
          Del_View.Kill()
      
          UNBIND('A_DEL:Terms')
          POPBIND
      
      
      
          LOC:Total_Invoices  = RECORDS(LOC:DIDs_Q)
      
          LOC:Preview_Option  = 2
      
          LOC:Idx             = 0
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
          PUTINI('Print_Man_Invs', 'Page_Print_Time', LOC:Page_Print_Time, GLO:Local_INI)
          PUTINI('Print_DN_POD_Cont', 'Last_Cont_Print_Time', CLOCK(), GLO:Local_INI)
    OF EVENT:Timer
          IF LOC:Print_Now = FALSE
             LOC:TimeOut  = LOC:Page_Print_Time - ((CLOCK() - LOC:Last_Print_Time) / 100)
             ?Prompt_Time{PROP:Text}  = 'Printing in ' & LOC:TimeOut & ' seconds'
      
             IF LOC:Page_Print_Time <= ABS((CLOCK() - LOC:Last_Print_Time) / 100)
                LOC:Print_Now = TRUE
          .  .
      
      
          
          IF LOC:Print_Now = TRUE
             ?Prompt_Time{PROP:Text}  = 'Printing...'
             QuickWindow{PROP:Timer}  = 0
      
             IF LOC:Pause = TRUE
                IF CLOCK() - LOC:ChangeTime > 50
                   LOC:ChangeTime    = CLOCK()
                   IF LOC:State = 'Paused'
                      LOC:State  = ''
                   ELSE
                      LOC:State  = 'Paused'
                .  .
             ELSE
                LOC:Idx  += 1
                GET(LOC:DIDs_Q, LOC:Idx)
                IF ERRORCODE()
                   LOC:Complete   = TRUE
                .
      
                IF LOC:Complete ~= TRUE
                   LOC:Inv_No    += 1
                   LOC:State      = 'Printing Invoices ' & LOC:Inv_No & ' of ' & LOC:Total_Invoices
      
                   ?Prompt1{PROP:Text}   = 'DID: ' & L_DQ:DID
      
                   LOC:No_Printed   += 1
      
                   CASE p:Type
                   OF 0
                              ! (p:ID, p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow)
                              ! (ULONG, BYTE      , BYTE=0      , BYTE=1          , BYTE=0), LONG, PROC
                      LOC:Print_Result        = Print_Cont(L_DQ:DID, 0, 1, LOC:Preview_Option)
                      IF LOC:Print_Result < 0
                         LOC:Complete  = TRUE
                      ELSIF LOC:Print_Result = 2
                         LOC:Preview_Option   = 0       ! No preview
                      .
                   OF 1
                      Print_Invoices(L_DQ:DID)
             .  .  .
      
             LOC:Last_Print_Time      = CLOCK()
      
             IF LOC:Idx >= LOC:Total_Invoices
                LOC:Complete = TRUE
             .
      
             IF LOC:Complete = FALSE
                QuickWindow{PROP:Timer}  = 50
             ELSE
                IF LOC:No_Printed <= 0 AND LOC:Result >= 0                           ! And not cancelled
                   MESSAGE('No Invoices were found to print.', 'Print Invoices', ICON:Exclamation)
                .
      
                POST(EVENT:CloseWindow)
          .  .
      
          LOC:Print_Now   = FALSE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Print_DN_POD_h       PROCEDURE                             ! Declare Procedure

  CODE
    Print_DN_POD()
    RETURN

!!! <summary>
!!! Generated from procedure template - Window
!!! Type is for Continuous or Laser
!!! </summary>
Print_Invoices_Multiple PROCEDURE (p:Type, p:Preview, p:From, p:To, p:Un_Printed, <shpTagClass p_Client_Tags>)

LOC:Complete         BYTE                                  !
LOC:Pause            BYTE                                  !
LOC:State            STRING(100)                           !
LOC:Info             STRING(255)                           !
LOC:ChangeTime       LONG                                  !
LOC:No_Printed       ULONG                                 !
LOC:Result           LONG                                  !
LOC:SQL_Str          STRING(2000)                          !
LOC:Total_Invoices   LONG                                  !
LOC:Inv_No           LONG                                  !
LOC:Page_Print_Time  SHORT                                 !In seconds
LOC:Print_Now        BYTE                                  !
LOC:TimeOut          LONG                                  !
LOC:Last_Print_Time  LONG                                  !
LOC:Omitted_6        BYTE                                  !
LOC:Tag_Idx          LONG                                  !
QuickWindow          WINDOW('Print Invoices'),AT(,,225,152),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY,IMM, |
  MDI,HLP('Print_Cont_Invs'),SYSTEM,TIMER(50)
                       SHEET,AT(4,4,217,130),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           STRING(@s100),AT(18,22,189,10),USE(LOC:State),FONT(,,,FONT:bold,CHARSET:ANSI),CENTER,TRN
                           PROMPT(''),AT(18,36,189,56),USE(?Prompt1),LEFT,TRN
                           LINE,AT(10,97,205,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Page Print Time (seconds):'),AT(18,103),USE(?LOC:Page_Print_Time:Prompt),TRN
                           SPIN(@n5),AT(114,103,48,9),USE(LOC:Page_Print_Time),RIGHT(1),MSG('In seconds'),SKIP,TIP('In seconds')
                           PROMPT(''),AT(18,120,189,9),USE(?Prompt_Time),CENTER,TRN
                         END
                       END
                       BUTTON('&Help'),AT(4,136,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       CHECK('Pause'),AT(118,136,49,14),USE(LOC:Pause),LEFT,ICON('VCR_Ps.ico'),FLAT
                       BUTTON('&Cancel'),AT(172,136,49,14),USE(?Cancel:2),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

View_Inv            VIEW(_Invoice)
    PROJECT(INV:IID,INV:ClientNo,INV:ClientName,INV:Total,INV:Weight,INV:DINo,INV:InvoiceDate)
    .



Inv_View            ViewManager
sql_        SQLQueryClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Records           ROUTINE
    ! (p:Type, p:Preview, p:From, p:To, p:Un_Printed)
    SETCURSOR(CURSOR:WAIT)

    IF ~LOC:Omitted_6
       LOC:Total_Invoices   = p_Client_Tags.NumberTagged()
    ELSE
       sql_.Init(GLO:DBOwner, '_SQLTemp2')
       CLEAR(LOC:SQL_Str)

       IF p:From > 0
          LOC:SQL_Str     = 'InvoiceDateAndTime >= ' & SQL_Get_DateT_G(p:From,,1)
       .
       IF p:To > 0
          !    Add_to_List
          !    (p:Add, p:List, p:Delim, p:Option, p:Prefix, p:Beginning)
          !    (STRING, *STRING, STRING, BYTE=0, <STRING>, BYTE=0)

          Add_to_List('InvoiceDateAndTime < ' & SQL_Get_DateT_G(p:To + 1,,1), LOC:SQL_Str, ' AND ')
       .

       IF p:Un_Printed = TRUE
          Add_to_List('Printed <> 1', LOC:SQL_Str, ' AND ')
       .

       IF CLIP(LOC:SQL_Str) = ''
          LOC:SQL_Str     = 'SELECT COUNT(*) FROM _Invoice'
       ELSE
          LOC:SQL_Str     = 'SELECT COUNT(*) FROM _Invoice WHERE ' & CLIP(LOC:SQL_Str)
       .

       !db.debugout('[Print_Invoices_Window]  SQL: ' & CLIP(LOC:SQL_Str))

       sql_.PropSQL(CLIP(LOC:SQL_Str))
       IF sql_.Next_Q() > 0
          LOC:Total_Invoices   = sql_.Data_G.F1
       ELSE
    .  .
    SETCURSOR()
    EXIT
Check_Omitted            ROUTINE
    LOC:Omitted_6   = OMITTED(6)
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Invoices_Multiple')
  SELF.Request = GlobalRequest                             ! Store the incoming request
      DO Check_Omitted
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:State
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
      IF LOC:Omitted_6
         PUSHBIND
         BIND('INV:InvoiceDate', INV:InvoiceDate)
         BIND('INV:Printed', INV:Printed)
      .
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:_Invoice.SetOpenRelated()
  Relate:_Invoice.Open                                     ! File _Invoice used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Print_Invoices_Multiple',QuickWindow)      ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      IF LOC:Omitted_6
         Inv_View.Kill()
  
         UNBIND('INV:InvoiceDate')
         UNBIND('INV:Printed')
  
         POPBIND
      .
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Invoice.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Invoices_Multiple',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Cancel:2
      ThisWindow.Update()
          QuickWindow{PROP:Timer} = 0
      
          CASE MESSAGE('Cancel the Printing now?', 'Confirm Cancel', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
          OF BUTTON:No
             QuickWindow{PROP:Timer} = 50
          OF BUTTON:Yes
             POST(EVENT:CloseWindow)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          LOC:Page_Print_Time     = GETINI('Print_Cont_Invs', 'Page_Print_Time', 8, GLO:Local_INI)
          IF LOC:Page_Print_Time < 2
             LOC:Page_Print_Time  = 2
          .
      
          ! Set it up to always wait before printing if Last Print Time was recent... from ANY cont print!
          LOC:Last_Print_Time = GETINI('Print_DN_POD_Cont', 'Last_Cont_Print_Time', 0, GLO:Local_INI)
      
      !    db.debugout('LOC:Last_Print_Time: ' & FORMAT(LOC:Last_Print_Time, @t4) & '||Clock: ' & FORMAT(CLOCK(),@t4))
      
          LOC:Print_Now       = FALSE
          ! (p:Type, p:Preview, p:From, p:To, p:Un_Printed, <shpTagClass p_Client_Tags>)
          ! (BYTE=0, BYTE=1, LONG=0, LONG=0),LONG,PROC
          !   1       2           3       4       5                   6
          !   p:Type
          !       0 - Continuous
          !       1 - Page printer
          !   p:Un_Printed
          !       0 - All
          !       1 - Un-Printed only
      
      
          ! Print_Man_Invs(MAN:MID, 0,, 0)       ! No preview on auto print
      
          DO Check_Records
      
          IF ~LOC:Omitted_6
             ! Using tags
             LOC:Tag_Idx  = 0
          ELSE
             Inv_View.Init(View_Inv, Relate:_Invoice)
             Inv_View.AddSortOrder()
             Inv_View.AppendOrder('INV:InvoiceDate')
             !Inv_View.AddRange()
             Inv_View.SetFilter('INV:InvoiceDate >= ' & p:From & ' AND INV:InvoiceDate < ' & p:To + 1,'1')
             IF p:Un_Printed = TRUE
                Inv_View.SetFilter('INV:Printed <> 1','2')
             .
      
             Inv_View.Reset()
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
          PUTINI('Print_Cont_Invs', 'Page_Print_Time', LOC:Page_Print_Time, GLO:Local_INI)
          PUTINI('Print_DN_POD_Cont', 'Last_Cont_Print_Time', CLOCK(), GLO:Local_INI)
    OF EVENT:Timer
          IF LOC:Print_Now = FALSE
             LOC:TimeOut  = LOC:Page_Print_Time - ((CLOCK() - LOC:Last_Print_Time) / 100)
             ?Prompt_Time{PROP:Text}  = 'Printing in ' & LOC:TimeOut & ' seconds'
      
             IF LOC:Page_Print_Time <= ABS((CLOCK() - LOC:Last_Print_Time) / 100)
                LOC:Print_Now = TRUE
          .  .
      
      
          
          IF LOC:Print_Now = TRUE
             ?Prompt_Time{PROP:Text}  = 'Printing...'
             QuickWindow{PROP:Timer}  = 0
      
             IF LOC:Pause = TRUE
                IF CLOCK() - LOC:ChangeTime > 50
                   LOC:ChangeTime    = CLOCK()
                   IF LOC:State = 'Paused'
                      LOC:State  = ''
                   ELSE
                      LOC:State  = 'Paused'
                .  .
             ELSE
                ! Get record
                IF ~LOC:Omitted_6
                   LOOP
                      LOC:Tag_Idx    += 1
                      GET(p_Client_Tags.TagQueue, LOC:Tag_Idx)
                      IF ERRORCODE()
                         LOC:Complete = TRUE
                         BREAK
                      ELSE
                         INV:IID      = p_Client_Tags.TagQueue.Ptr
                         IF Access:_Invoice.TryFetch(INV:PKey_IID) = LEVEL:Benign
                            BREAK
                         ELSE
                            MESSAGE('Failed to find invoice - ' & INV:IID & '||Printing from tagged records.', 'Invoice Print', ICON:Exclamation)
                            CLEAR(INV:Record)
                   .  .  .
                ELSE
                   IF Inv_View.Next() ~= LEVEL:Benign
                      LOC:Complete    = TRUE
                .  .
      
      
                ! Print
                IF LOC:Complete = FALSE
                   LOC:Inv_No   += 1
                   LOC:State     = 'Printing Invoice ' & LOC:Inv_No & ' of ' & LOC:Total_Invoices & ' - IID: ' & INV:IID
      
                   ?Prompt1{PROP:Text}   = 'Client No.: ' & INV:ClientNo & '<13,10>Name: ' & CLIP(INV:ClientName)        & |
                         '<13,10>Amount: ' & FORMAT(INV:Total,@n-12.2) & '<13,10>Weight: ' & FORMAT(INV:Weight,@n-12.2)  & |
                         '<13,10>DI No.: ' & INV:DINo & '<13,10>Invoice Date: ' & FORMAT(INV:InvoiceDate,@d6)
      
                   LOC:No_Printed   += 1
      
                   IF p:Type = 0
                      ! (p:ID , p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow)
                      ! (ULONG, BYTE, BYTE=0, BYTE=1, BYTE=0), LONG, PROC
                      IF p:Preview = FALSE
                         LOC:Result  = Print_Cont(INV:IID, 0,, p:Preview, TRUE)      ! Hide the window
                      ELSE
                         LOC:Result  = Print_Cont(INV:IID, 0,, p:Preview)
                      .
      
                      IF LOC:Result = 2                                              ! No preview
                         p:Preview       = FALSE
                      ELSIF LOC:Result < 0                                           ! Cancel
                         LOC:Complete    = TRUE
                      .
                   ELSE
                      ! (p:DID, p:IID, p:From, p:To, p:Un_Printed, p:Preview, p:Original)
                      IF p:Type = 3 
                         Print_Invoices(, INV:IID,,,, p:Preview, true)
                      ELSE 
                         Print_Invoices(, INV:IID,,,, p:Preview)
                      .                        
             .  .  .
      
             LOC:Last_Print_Time      = CLOCK()
      
      
             ! Check next tagged, if not there then complete now
             IF ~LOC:Omitted_6
                GET(p_Client_Tags.TagQueue, LOC:Tag_Idx + 1)
                IF ERRORCODE()
                   LOC:Complete = TRUE
             .  .
      
      
             IF LOC:Complete = FALSE
                QuickWindow{PROP:Timer}  = 50
             ELSE
                IF LOC:No_Printed <= 0 AND LOC:Result >= 0                           ! And not cancelled
                   MESSAGE('No Invoices were found to print with the specified Date Range and Printed status.', 'Print Invoices', ICON:Exclamation)
                .
      
                POST(EVENT:CloseWindow)
          .  .
      
          LOC:Print_Now   = FALSE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Print_Invoices_Window PROCEDURE 

LOC:Options          GROUP,PRE(LO)                         !
Type                 BYTE(1)                               !
From_Date            DATE                                  !
To_Date              DATE                                  !
Un_Printed           BYTE(1)                               !Print Un-Printed invoices only
Preview              BYTE                                  !
                     END                                   !
LOC:SQL_Str          STRING(2000)                          !
QuickWindow          WINDOW('Print Invoices'),AT(,,213,160),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('Print_Invoices_Window'),SYSTEM
                       SHEET,AT(4,4,205,135),USE(?Sheet1)
                         TAB('Options'),USE(?Tab1)
                           PROMPT('Type:'),AT(13,23),USE(?Type:Prompt),TRN
                           LIST,AT(69,23,77,10),USE(LO:Type),DROP(5),FROM('Laser|#0|Continuous|#1')
                           PROMPT('From Date:'),AT(13,42),USE(?From_Date:Prompt),TRN
                           SPIN(@d5b),AT(69,42,77,10),USE(LO:From_Date),RIGHT(1)
                           BUTTON('...'),AT(154,42,12,10),USE(?Calendar)
                           PROMPT('To Date:'),AT(13,58),USE(?To_Date:Prompt),TRN
                           SPIN(@d5b),AT(69,58,77,10),USE(LO:To_Date),RIGHT(1)
                           BUTTON('...'),AT(154,58,12,10),USE(?Calendar:2)
                           CHECK(' &Un-Printed'),AT(69,77),USE(LO:Un_Printed),MSG('Print Un-Printed invoices only'),TIP('Print Un-P' & |
  'rinted invoices only'),TRN
                           CHECK(' &Preview'),AT(69,95),USE(LO:Preview),TRN,VALUE('2','0')
                           PANEL,AT(9,114,198,22),USE(?Panel1),BEVEL(-1,-1)
                           PROMPT(''),AT(10,120,191,10),USE(?Prompt_Info),TRN
                         END
                       END
                       BUTTON('&OK'),AT(108,142,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(160,142,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(8,142,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass
sql_        SQLQueryClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Records           ROUTINE
    SETCURSOR(CURSOR:WAIT)
    CLEAR(LOC:SQL_Str)

    IF LO:From_Date > 0
       LOC:SQL_Str     = 'InvoiceDateAndTime >= ' & SQL_Get_DateT_G(LO:From_Date,,1)
    .
    IF LO:To_Date > 0
       !    Add_to_List
       !    (p:Add, p:List, p:Delim, p:Option, p:Prefix, p:Beginning)
       !    (STRING, *STRING, STRING, BYTE=0, <STRING>, BYTE=0)

       Add_to_List('InvoiceDateAndTime < ' & SQL_Get_DateT_G(LO:To_Date + 1,,1), LOC:SQL_Str, ' AND ')
    .

    IF LO:Un_Printed = TRUE
       Add_to_List('Printed <> 1', LOC:SQL_Str, ' AND ')
    .

    IF CLIP(LOC:SQL_Str) = ''
       LOC:SQL_Str     = 'SELECT COUNT(*) FROM _Invoice'
    ELSE
       LOC:SQL_Str     = 'SELECT COUNT(*) FROM _Invoice WHERE ' & CLIP(LOC:SQL_Str)
    .

    !db.debugout('[Print_Invoices_Window]  SQL: ' & CLIP(LOC:SQL_Str))

    sql_.PropSQL(CLIP(LOC:SQL_Str))
    IF sql_.Next_Q() > 0
       ?Prompt_Info{PROP:Text}  = 'Selected criteria has ' & CLIP(sql_.Data_G.F1) & ' invoices.'
    ELSE
       ?Prompt_Info{PROP:Text}  = 'Selected criteria has no invoices.'
    .
    SETCURSOR()
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Invoices_Window')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Type:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Print_Invoices_Window',QuickWindow)        ! Restore window settings from non-volatile store
      LO:From_Date    = GETINI('Print_Invoice_Window', 'From_Date', , GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Invoice_Window', 'To_Date', , GLO:Local_INI)
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Invoices_Window',QuickWindow)     ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Ok
          ! Print_Invoices
          ! (p:DID, p:IID, p:From, p:To, p:Un_Printed)
          !
          ! Print_Cont_Invs(0, 0, LO:From_Date, LO:To_Date, LO:Un_Printed)
          ! (p:Type, p:Preview, p:From, p:To, p:Un_Printed)
      
          db.debugout('[Print_Invoice_Window]   Type: ' & LO:Type)
      
          PUTINI('Print_Invoice_Window', 'From_Date', LO:From_Date, GLO:Local_INI)
          PUTINI('Print_Invoice_Window', 'To_Date', LO:To_Date, GLO:Local_INI)
      
          EXECUTE LO:Type + 1
             Print_Invoices(, , LO:From_Date, LO:To_Date)
             Print_Invoices_Multiple(0, LO:Preview, LO:From_Date, LO:To_Date, LO:Un_Printed)
          .
      
          ! (p:Type, p:Preview, p:From, p:To)
          !   p:Type
          !       0 - Continuous
          !       1 - Page printer
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LO:Type
          DO Check_Records
    OF ?LO:From_Date
          IF LO:From_Date > LO:To_Date
             LO:To_Date   = LO:From_Date
             DISPLAY
          .
          DO Check_Records
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',LO:From_Date)
      IF Calendar5.Response = RequestCompleted THEN
      LO:From_Date=Calendar5.SelectedDate
      DISPLAY(?LO:From_Date)
      END
      ThisWindow.Reset(True)
          IF LO:From_Date > LO:To_Date
             LO:To_Date   = LO:From_Date
             DISPLAY
          .
          DO Check_Records
    OF ?LO:To_Date
          IF LO:From_Date > LO:To_Date
             LO:From_Date = LO:To_Date
             DISPLAY
          .
          DO Check_Records
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',LO:To_Date)
      IF Calendar6.Response = RequestCompleted THEN
      LO:To_Date=Calendar6.SelectedDate
      DISPLAY(?LO:To_Date)
      END
      ThisWindow.Reset(True)
          IF LO:From_Date > LO:To_Date
             LO:From_Date = LO:To_Date
             DISPLAY
          .
          DO Check_Records
    OF ?LO:Un_Printed
          DO Check_Records
    OF ?Ok
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    OF ?Cancel
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LO:Type
          DO Check_Records
    OF ?LO:From_Date
          IF LO:From_Date > LO:To_Date
             LO:To_Date   = LO:From_Date
             DISPLAY
          .
          DO Check_Records
    OF ?LO:To_Date
          IF LO:From_Date > LO:To_Date
             LO:From_Date = LO:To_Date
             DISPLAY
          .
          DO Check_Records
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          sql_.Init(GLO:DBOwner, '_SQLTemp2')
      
          DO Check_Records
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Print_Manifest_h     PROCEDURE                             ! Declare Procedure

  CODE
    Print_Manifest()
    RETURN
