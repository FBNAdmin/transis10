

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS016.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Print_Turnover_Calc  PROCEDURE                             ! Declare Procedure
LOC:Errors           ULONG                                 !
LOC:Settings         GROUP,PRE(L_SG)                       !
VAT                  BYTE                                  !
BID                  ULONG                                 !Branch ID
BranchName           STRING(35)                            !Branch Name
ExcludeBadDebts      BYTE(1)                               !Exclude Bad Debt Credit Notes
Output               BYTE                                  !Output trail of items
                     END                                   !
LOC:Values           GROUP,PRE(L_VG)                       !
Kgs_Broking          DECIMAL(7)                            !
Kgs_O_Night          DECIMAL(7)                            !
T_O_Broking          DECIMAL(10,2)                         !
T_O_O_Night          DECIMAL(10,2)                         !
C_Note               DECIMAL(10,2)                         !
Total_T_O            DECIMAL(10,2)                         !
Loadmaster           DECIMAL(10,2)                         !
Trans_Broking        DECIMAL(10,2)                         !
Extra_Inv            DECIMAL(10,2)                         !
Trans_Credit         DECIMAL(10,2)                         !
Total_Cost           DECIMAL(10,2)                         !
Trans_Debit          DECIMAL(10,2)                         !
Total_Cost_Less_Deb  DECIMAL(10,2)                         !
GP                   DECIMAL(10)                           !
GP_Per               DECIMAL(10,2)                         !
MID                  ULONG                                 !Manifest ID
CreatedDate          DATE                                  !Created Date
Ext_Invocie_TO       DECIMAL(12,2)                         !Turn over extra invoice
                     END                                   !
Totals_Group         GROUP,PRE(L_TG)                       !
GP                   DECIMAL(12,2)                         !
GP_Per               DECIMAL(7,2)                          !
Total_TO             DECIMAL(12,2)                         !
Total_Cost           DECIMAL(12,2)                         !
                     END                                   !
LOC:Dates_Done       QUEUE,PRE(L_DQ)                       !
Date                 DATE                                  !
                     END                                   !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
LOC:Cur_Date         DATE                                  !
LOC:Report_Values    GROUP,PRE(L_RG)                       !
VAT_Option           STRING(20)                            !
                     END                                   !

  CODE
!Manifest_Invoice                                ROUTINE
!
!
!            ! need to check for un-manifested invoices
!            ! need to add in dates from too check to make sure all dates are included
!
!    ! We have a Q of done dates, we need to check if the current date is the date after the last date
!    ! or the date after the start date
!    ! This will work as all orders are by date
!
!    ! Check if the current record date is the next record
!    DO Check_Missing_Invs
!
!
!
!
!
!    ! ==============   normal processing   ==============
!    CLEAR(LOC:Values)
!
!    L_VG:MID                    = MAN:MID
!    L_VG:CreatedDate            = MAN:CreatedDate
!
!
!    ! Get_Manifest_Info
!    ! (p:MID, p:Option, p:DI_Type, p:Inv_Totals, p:Output, p:Info  )
!    ! (ULONG, BYTE    , BYTE=0   , BYTE=0      , BYTE=0  , <STRING>),STRING
!    !   1       2           3           4           5           6
!    !   p:Option
!    !       0.  Charges total
!    !       1.  Insurance total
!    !       2.  Weight total, for this Manifest!    Uses Units_Loaded
!    !       3.  Extra Legs cost
!    !       4.  Charges total incl VAT
!    !       5.  No. DI's
!    !       6.  Extra Legs cost - inc VAT
!    !   p:DI_Type
!    !       0.  All             (default)
!    !       1.  Non-Broking
!    !       2.  Broking
!    !   p:Inv_Totals
!    !       0.  Collect from Delivery info.
!    !       1.  Collect from Invoiced info.     (only Manifest invoices)
!
!!    IF MAN:Broking = TRUE
!       L_VG:Kgs_Broking         = Get_Manifest_Info(MAN:MID, 2, 2,, L_SG:Output)
!!    ELSE
!       L_VG:Kgs_O_Night         = Get_Manifest_Info(MAN:MID, 2, 1,, L_SG:Output)
!!    .
!
!    ! Below is no longer possible, the Broking status is set on the Manifest
!
!!    IF L_VG:Kgs_Broking ~= 0.0 AND L_VG:Kgs_O_Night ~= 0.0
!!       ! Error - some DIs are overnight some are broking...
!!       
!!       ! (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel)       (BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0)
!!       Add_SystemLog(100, 'Print_Turnover', 'MID: ' & MAN:MID & ', has both Broking and O/Night DIs attached to it!', 'Please correct.')
!!       LOC:Errors              += 1
!!    .
!
!    IF MAN:Broking = FALSE                !L_VG:Kgs_Broking = 0.0
!       IF L_SG:VAT = TRUE
!          L_VG:T_O_O_Night      = Get_Manifest_Info(MAN:MID, 4,,1, L_SG:Output, 'ON')             ! All charges - from invoice    inc
!          !L_VG:T_O_O_Night      = Get_Invoices(0, 0, 1, FALSE, MAN:MID,, L_SG:Output, 1)   - this would also work
!
!          L_VG:T_O_Broking      = Get_InvoicesTransporter(0, 0, 1, 0, 1, MAN:MID,,,, 2, L_SG:Output,1, 'EL')           ! Del legs, broking - inc vat
!
!          ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
!          IF L_VG:T_O_Broking > 0.0
!             Add_Log('MID: ' & MAN:MID & ',  L_VG:T_O_Broking: ' & L_VG:T_O_Broking & ',  MAN:CreatedDate: ' & FORMAT(MAN:CreatedDate,@d6), 'Get_Manifest_Info-Extra', 'Extra.log',,, 0)
!          .
!       ELSE
!          L_VG:T_O_O_Night      = Get_Manifest_Info(MAN:MID, 0,,1, L_SG:Output, 'ON')             ! All charges - from invoice
!          L_VG:T_O_Broking      = Get_InvoicesTransporter(0, 1, 1, 0, 1, MAN:MID,,,, 2, L_SG:Output,1, 'EL')           ! Del legs, broking - ex vat
!       .
!
!   db.debugout('[Print_Turnover]  MAN:MID: ' & MAN:MID & ', T_O_O_Night: ' & L_VG:T_O_O_Night & ',  L_VG:T_O_Broking: ' & L_VG:T_O_Broking)
!
!       L_VG:T_O_O_Night        -= L_VG:T_O_Broking                                          ! Legs cost off total DI value (included)
!    ELSE
!       L_VG:T_O_O_Night         = 0.0
!       IF L_SG:VAT = TRUE
!          L_VG:T_O_Broking      = Get_Manifest_Info(MAN:MID, 4,,1, L_SG:Output, 'BR')             ! All charges - from invoice    inc
!
!          IF L_VG:T_O_Broking > 0.0
!             Add_Log('MID: ' & MAN:MID & ',  L_VG:T_O_Broking: ' & L_VG:T_O_Broking  & ',  MAN:CreatedDate: ' & FORMAT(MAN:CreatedDate,@d6), 'Get_Manifest_Info-Extra', 'Extra.log',,, 0)
!          .
!       ELSE
!          L_VG:T_O_Broking      = Get_Manifest_Info(MAN:MID, 0,,1, L_SG:Output, 'BR')             ! All charges - from invoice
!    .  .
!
!    
!    ! Get_Invoices          (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source)
!    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source)
!    ! (LONG  , BYTE    , BYTE=0, BYTE=0        , ULONG=0, <LONG>  , BYTE=0  , BYTE=0    , BYTE=0           , BYTE=0  ),STRING
!    !   1          2       3         4               5       6        7         8               9            10 
!    ! p:Option
!    !   0.  - Total Charges inc.
!    !   1.  - Excl VAT
!    !   2.  - VAT
!    !   3.  - Kgs
!    ! p:Type
!    !   0.  - All
!    !   1.  - Invoices                      Total >= 0.0
!    !   2.  - Credit Notes                  Total < 0.0
!    !   3.  - Credit Notes excl Bad Debts   Total < 0.0
!    ! p:Limit_On
!    !   0.  - Date [& Branch]
!    !   1.  - MID
!    ! p:Manifest_Option
!    !   0.  - None
!    !   1.  - Overnight
!    !   2.  - Broking
!
!    L_DQ:Date                   = MAN:CreatedDate
!    GET(LOC:Dates_Done, L_DQ:Date)
!    IF ERRORCODE()
!       Cred_Type_#      = 2
!       IF L_SG:ExcludeBadDebts = TRUE
!          Cred_Type_#   = 3
!       .
!
!       IF L_SG:VAT = TRUE
!          ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID, p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source)
!          L_VG:C_Note           = -Get_Invoices(MAN:CreatedDate, 0, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1)    ! Any passed on this day
!       ELSE
!          L_VG:C_Note           = -Get_Invoices(MAN:CreatedDate, 1, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1)    ! Any passed on this day
!    .  .
!
!    IF L_SG:VAT = TRUE
!       ! Get_InvoicesTransporter
!       ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type, p:Output, p:Source)
!       ! (LONG  , BYTE    , BYTE=0, BYTE=0        , BYTE=0        , <ULONG>, ULONG=0, <LONG>  , BYTE=0     , BYTE=0         , BYTE=0  , BYTE=0),STRING
!       !   1           2       3         4               5           6        7         8          9           10              11     , 12    
!       ! p:Option                                                2
!       !   0.  - Total Charges inc.
!       !   1.  - Excl VAT
!       !   2.  - VAT
!       ! p:Type                                                  3
!       !   0.  - All
!       !   1.  - Invoices                      Total >= 0.0
!       !   2.  - Credit Notes                  Total < 0.0
!       ! p:DeliveryLegs                                          5
!       !   0.  - Both
!       !   1.  - Del Legs only
!       !   2.  - No Del Legs
!       ! p:MID                                                   6
!       !   0   = Not for a MID
!       !   x   = for a MID
!       ! p:Extra_Inv                                             9
!       !   Only
!       ! p:Invoice_Type (was Manifest_Type)                      10
!       !   0   - All
!       !   1   - Overnight
!       !   2   - Broking
!       ! p:Source                                                12
!       !   1   - Management Profit
!       !   2   - Management Profit Summary
!       L_VG:Loadmaster       = Get_InvoicesTransporter(0, 0, 1, 0, 0, MAN:MID,,,, 1, L_SG:Output,1)
!       L_VG:Trans_Broking    = Get_InvoicesTransporter(0, 0, 1, 0, 0, MAN:MID,,,, 2, L_SG:Output,1)
!    ELSE
!       L_VG:Loadmaster       = Get_InvoicesTransporter(0, 1, 1, 0, 0, MAN:MID,,,, 1, L_SG:Output,1)
!       L_VG:Trans_Broking    = Get_InvoicesTransporter(0, 1, 1, 0, 0, MAN:MID,,,, 2, L_SG:Output,1)
!    .
!
!
!    ! Get all Extra Invoices not associated to a MID
!    GET(LOC:Dates_Done, L_DQ:Date)
!    IF ERRORCODE()
!       IF L_SG:VAT = TRUE
!          L_VG:Ext_Invocie_TO   = Get_Invoices(MAN:CreatedDate, 0, 1, TRUE, L_SG:BID,, L_SG:Output,,,1, 'EI')   ! Invoice for day not related to MID
!
!          IF L_VG:Ext_Invocie_TO > 0.0
!             Add_Log('MID: ' & MAN:MID & ',  L_VG:Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',   MAN:CreatedDate: ' & FORMAT(MAN:CreatedDate,@d6), 'Get_Manifest_Info-Extra', 'Extra.log',,, 0)
!          .
!
!          ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID, p:BID, p:ToDate, p:Extra_Inv, p:Manifest_Type, p:Output)
!          L_VG:Extra_Inv        = Get_InvoicesTransporter(MAN:CreatedDate, 0, 1, FALSE,,, L_SG:BID,,TRUE,, L_SG:Output,1)    ! Credits for day
!          L_VG:Trans_Credit     = Get_InvoicesTransporter(MAN:CreatedDate, 0, 1, TRUE,,, L_SG:BID,,,, L_SG:Output,1)         ! Credits for day - Transporter not MID
!
!          L_VG:Trans_Debit      = -Get_InvoicesTransporter(MAN:CreatedDate, 0, 2,,,, L_SG:BID,,,, L_SG:Output,1)
!
!   db.debugout('[Print_Turnover]  Extra invoices - Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',  Extra_Inv: ' & L_VG:Extra_Inv & ',  Trans_Credit: ' & L_VG:Trans_Credit & ',  Trans_Debit: ' & L_VG:Trans_Debit)
!
!       ELSE
!          L_VG:Ext_Invocie_TO   = Get_Invoices(MAN:CreatedDate, 1, 1, TRUE, L_SG:BID,, L_SG:Output,,,1, 'EI')   ! Invoice for day not related to MID
!
!          L_VG:Extra_Inv        = Get_InvoicesTransporter(MAN:CreatedDate, 1, 1, FALSE,,, L_SG:BID,,TRUE,, L_SG:Output,1)      ! Credits for day
!          L_VG:Trans_Credit     = Get_InvoicesTransporter(MAN:CreatedDate, 1, 1, TRUE,,, L_SG:BID,,,, L_SG:Output,1)     ! Credits for day - Transporter not MID
!
!          L_VG:Trans_Debit      = -Get_InvoicesTransporter(MAN:CreatedDate, 1, 2,,,, L_SG:BID,,,, L_SG:Output,1)
!    .  .
!
!    L_VG:Total_Cost             = L_VG:Loadmaster + L_VG:Trans_Broking + L_VG:Extra_Inv + L_VG:Trans_Credit
!
!    L_VG:Total_Cost_Less_Deb    = L_VG:Total_Cost - L_VG:Trans_Debit
!
!   db.debugout('[Print_Turnover]  Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',  T_O_Broking: ' & L_VG:T_O_Broking & ',  C_Note: ' & L_VG:C_Note)
!
!    L_VG:T_O_Broking           += L_VG:Ext_Invocie_TO                                                   ! Add extras to BROKING ALWAYS
!
!    L_VG:Total_T_O              = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note
!
!
!    L_VG:GP                     = L_VG:Total_T_O - L_VG:Total_Cost_Less_Deb
!    L_VG:GP_Per                 = (L_VG:GP / L_VG:Total_T_O) * 100
!
!
!
!    L_DQ:Date                   = MAN:CreatedDate
!    GET(LOC:Dates_Done, L_DQ:Date)
!    IF ERRORCODE()
!       L_DQ:Date                = MAN:CreatedDate
!       ADD(LOC:Dates_Done, L_DQ:Date)
!    .
!Check_Missing_Invs                          ROUTINE
!    CLEAR(LOC:Dates_Done)
!    GET(LOC:Dates_Done, RECORDS(LOC:Dates_Done))
!    ! The last date done - and we are not on 1st date of range
!    IF MAN:CreatedDate > L_DQ:Date + 1 AND MAN:CreatedDate ~= LO:From_Date
!       ! Check for invoices not associated with a Manifest in this period
!       IF L_DQ:Date = 0
!          LOC:Cur_Date             = LO:From_Date
!       ELSE
!          LOC:Cur_Date             = L_DQ:Date + 1
!       .
!
!       LOOP
!          CLEAR(LOC:Values)
!
!          Cred_Type_#      = 2
!          IF L_SG:ExcludeBadDebts = TRUE
!             Cred_Type_#   = 3
!          .
!          IF L_SG:VAT = TRUE
!             ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID, p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info)
!             L_VG:C_Note           = -Get_Invoices(LOC:Cur_Date, 0, Cred_Type_#,, L_SG:BID,,,,,1)          ! Any passed on this day
!          ELSE
!             L_VG:C_Note           = -Get_Invoices(LOC:Cur_Date, 1, Cred_Type_#,, L_SG:BID,,,,,1)          ! Any passed on this day
!          .
!
!          L_VG:Total_T_O           = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note
!
!!          IF L_SG:VAT = TRUE
!!             L_VG:Extra_Inv        = Get_Invoices(LOC:Cur_Date, 0, 0, TRUE, L_SG:BID)                ! Invoice for day not related to MID
!!             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 0, 2, TRUE,,, L_SG:BID)  ! Credits for day - Transporter not MID
!!          ELSE
!!             L_VG:Extra_Inv        = Get_Invoices(LOC:Cur_Date, 1, 0, TRUE, L_SG:BID)                ! Invoice for day not related to MID
!!             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 1, 2, TRUE,,, L_SG:BID)  ! Credits for day - Transporter not MID
!!          .
!
!          IF L_SG:VAT = TRUE
!             L_VG:Ext_Invocie_TO   = Get_Invoices(LOC:Cur_Date, 0, 1, TRUE, L_SG:BID,, L_SG:Output,,,1, 'EI')   ! Invoice for day not related to MID
!
!             ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID, p:BID, p:ToDate, p:Extra_Inv, p:Manifest_Type, p:Output, p:Source)
!
!             L_VG:Extra_Inv        = Get_InvoicesTransporter(LOC:Cur_Date, 0, 1, TRUE,,, L_SG:BID,,TRUE,,,1)      ! Credits for day - Transporter not MID
!             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 0, 2, TRUE,,, L_SG:BID,,,,,1)     ! Credits for day
!
!             L_VG:Trans_Debit      = Get_InvoicesTransporter(LOC:Cur_Date, 0, 2,,,, L_SG:BID,,,,,1)
!          ELSE
!             L_VG:Ext_Invocie_TO   = Get_Invoices(LOC:Cur_Date, 1, 1, TRUE, L_SG:BID,, L_SG:Output,,,1, 'EI')   ! Invoice for day not related to MID
!
!             L_VG:Extra_Inv        = Get_InvoicesTransporter(LOC:Cur_Date, 1, 1, TRUE,,, L_SG:BID,,TRUE,,,1)      ! Credits for day - Transporter not MID
!             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 1, 2, TRUE,,, L_SG:BID,,,,,1)     ! Credits for day
!
!             L_VG:Trans_Debit      = Get_InvoicesTransporter(LOC:Cur_Date, 1, 2,,,, L_SG:BID,,,,,1)
!          .
!
!          L_VG:Total_Cost             = L_VG:Loadmaster + L_VG:Trans_Broking + L_VG:Extra_Inv - L_VG:Trans_Credit
!
!          L_VG:Total_Cost_Less_Deb    = L_VG:Total_Cost - L_VG:Trans_Debit
!
!          L_VG:T_O_Broking           += L_VG:Ext_Invocie_TO                                                   ! Add extras to BROKING ALWAYS
!
!          L_VG:Total_T_O              = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note
!
!          L_VG:GP                     = L_VG:Total_T_O - L_VG:Total_Cost_Less_Deb
!          L_VG:GP_Per                 = (L_VG:GP / L_VG:Total_T_O) * 100
!
!
!          L_DQ:Date                = LOC:Cur_Date
!          ADD(LOC:Dates_Done, L_DQ:Date)
!
!          L_VG:MID                 = 0
!          L_VG:CreatedDate         = LOC:Cur_Date
!
!          IF L_VG:Total_Cost ~= 0.0 OR L_VG:Total_T_O ~= 0.0 OR L_VG:Trans_Debit ~= 0.0 OR L_VG:Extra_Inv ~= 0.0 OR |
!                L_VG:Trans_Credit ~= 0.0 OR L_VG:C_Note ~= 0.0 OR L_VG:Ext_Invocie_TO ~= 0.0
!
!             db.debugout('[Print_Turnover]  Total_Cost: ' & L_VG:Total_Cost & ',  Total_T_O: ' & L_VG:Total_T_O & ',  Trans_Debit: ' & L_VG:Trans_Debit & |
!                         ',  Extra_Inv: ' & L_VG:Extra_Inv & ',  Trans_Credit: ' & L_VG:Trans_Credit & '  C_Note: ' & L_VG:C_Note)
!
!             !PRINT(RPT:Detail)
!          .
!
!          LOC:Cur_Date            += 1
!          IF LOC:Cur_Date >= MAN:CreatedDate
!             BREAK
!    .  .  .
!
!
!    EXIT
!!! <summary>
!!! Generated from procedure template - Process
!!! </summary>
Check_ScheduledRuns PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:ExitCode         LONG                                  !
Process:View         VIEW(ScheduledRuns)
                     END
ProgressWindow       WINDOW('Process Scheduled Runs'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,MDI,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Check_ScheduledRuns')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:ScheduledRuns.Open                                ! File ScheduledRuns used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:ScheduledRuns, ?Progress:PctText, Progress:Thermometer, ProgressMgr, SCH:SRID)
  ThisProcess.AddSortOrder(SCH:PKey_SRID)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='Processing Scheduled Runs'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SELF.DeferWindow = 3
  SEND(ScheduledRuns,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ScheduledRuns.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      IF SCH:ScheduleOption = 1                ! Daily
         IF SCH:LastExecuted_Date <> TODAY()
            ! Excute!
            EXECUTE SCH:ScheduleType
               LOC:ExitCode   = Process_Client_Balances()       ! Client Update
            .
  
  
            IF LOC:ExitCode > 0
               IF Access:ScheduledRuns.TryFetch(SCH:PKey_SRID) = LEVEL:Benign
                  SCH:LastExecuted_Date  = TODAY()
                  SCH:LastExecuted_Time  = CLOCK()
  
                  IF Access:ScheduledRuns.Update() = LEVEL:Benign
                     ! ok
      .  .  .  .  .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Check_COD_Invoices   PROCEDURE  (p:TRID, p:Option)         ! Declare Procedure
LOC:Count            ULONG                                 !
Tek_Failed_File     STRING(100)

Trip_View           VIEW(TripSheetDeliveriesAlias)
    PROJECT(A_TRDI:TDID, A_TRDI:DIID)
       JOIN(A_DELI:PKey_DIID, A_TRDI:DIID)
       PROJECT(A_DELI:DIID, A_DELI:DID)
          JOIN(A_DEL:PKey_DID, A_DELI:DID)
          PROJECT(A_DEL:DINo, A_DEL:Terms)
    .  .  .

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
     Access:TripSheetDeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
     Access:DeliveriesAlias.UseFile()
    ! p:Option
    !   0.  Check form some
    !   1.  Print them

    BIND('p:TRID', p:TRID)
    BIND('A_TRDI:TRID', A_TRDI:TRID)

    OPEN(Trip_View)
    Trip_View{PROP:Filter}  = 'A_TRDI:TRID = ' & p:TRID

    SET(Trip_View)
    LOOP
       NEXT(Trip_View)
       IF ERRORCODE()
          BREAK
       .

       IF A_DEL:Terms = 1           ! COD
          LOC:Count += 1

          IF p:Option = 0
          ELSE
             ! Print this Invoice
             Print_Invoices(A_DEL:DID)
    .  .  .
    CLOSE(Trip_View)

    UNBIND('p:TRID')
    UNBIND('A_TRDI:TRID')
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Count)

    Exit

CloseFiles     Routine
    Relate:TripSheetDeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Relate:DeliveriesAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Shortages_Damages PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Report_Group     GROUP,PRE(L_RG)                       !
Delivery_Driver      STRING(35)                            !
TripSheet_Driver     STRING(35)                            !
Collection_Branch    STRING(35)                            !
Consignee            STRING(35)                            !
                     END                                   !
Delivery_Address     STRING(100)                           !
Process:View         VIEW(Shortages_Damages)
                       PROJECT(SHO:AnyOtherRelevantInformation)
                       PROJECT(SHO:BranchResponsible)
                       PROJECT(SHO:CauseOfDamage)
                       PROJECT(SHO:ClientPhoned)
                       PROJECT(SHO:ClientPhoned_Information)
                       PROJECT(SHO:ClientResponsible)
                       PROJECT(SHO:ConditionComment)
                       PROJECT(SHO:ConsignmentCollectedBy)
                       PROJECT(SHO:ConsignmentDeliveredBy)
                       PROJECT(SHO:CorrectiveActionTaken)
                       PROJECT(SHO:Create_Date)
                       PROJECT(SHO:DINo)
                       PROJECT(SHO:DI_Date)
                       PROJECT(SHO:Damaged)
                       PROJECT(SHO:DateInvestigationCompleted)
                       PROJECT(SHO:DeliveryDepot)
                       PROJECT(SHO:DepotConditionReceived)
                       PROJECT(SHO:DepotConditionReceivedDispatch)
                       PROJECT(SHO:DepotRemarks)
                       PROJECT(SHO:DepotSupervisorNotation)
                       PROJECT(SHO:DepotWarningIssuedDriver)
                       PROJECT(SHO:DestConditionReceived)
                       PROJECT(SHO:DestDamaged)
                       PROJECT(SHO:DestFurtherDamages_Materials_B_Tape)
                       PROJECT(SHO:DestFurtherDamages_Materials_Boxes)
                       PROJECT(SHO:DestFurtherDamages_Materials_ClearTape)
                       PROJECT(SHO:DestFurtherDamages_Materials_Other)
                       PROJECT(SHO:DestFurtherDamages_Materials_Straps)
                       PROJECT(SHO:DestFurtherDamages_Remarks)
                       PROJECT(SHO:DestFurtherDamages_Repacked)
                       PROJECT(SHO:DestFurtherDamages_Retaped)
                       PROJECT(SHO:DestLoose)
                       PROJECT(SHO:DestPalletised)
                       PROJECT(SHO:DestShort)
                       PROJECT(SHO:DestTaped)
                       PROJECT(SHO:DestUnPacked)
                       PROJECT(SHO:DriverAdvised_POD_Endorsed)
                       PROJECT(SHO:DriverAdvised_Person)
                       PROJECT(SHO:DriverComments)
                       PROJECT(SHO:DriverCommentsEntered)
                       PROJECT(SHO:DriverSigned)
                       PROJECT(SHO:DriversNotationDI)
                       PROJECT(SHO:FLDriver1)
                       PROJECT(SHO:FLDriver2)
                       PROJECT(SHO:FinalDestination_Condition)
                       PROJECT(SHO:FinalDestination_DriversCommentsOffLoading)
                       PROJECT(SHO:FinalDestination_Reported)
                       PROJECT(SHO:HaveWarningsBeenIssued)
                       PROJECT(SHO:IID)
                       PROJECT(SHO:Items)
                       PROJECT(SHO:LocalDriverResponsible)
                       PROJECT(SHO:LongDistanceDriverResponsible)
                       PROJECT(SHO:Loose)
                       PROJECT(SHO:Other)
                       PROJECT(SHO:POD_IID)
                       PROJECT(SHO:Palletised)
                       PROJECT(SHO:PoliceReference)
                       PROJECT(SHO:RemarksOnPOD)
                       PROJECT(SHO:ReportComments)
                       PROJECT(SHO:ReportNo)
                       PROJECT(SHO:SDID)
                       PROJECT(SHO:Short)
                       PROJECT(SHO:StolenGoods_ReportedToPolice)
                       PROJECT(SHO:Supervisor1)
                       PROJECT(SHO:Supervisor2)
                       PROJECT(SHO:SupervisorAcceptedDamagedCargo)
                       PROJECT(SHO:SupervisorInvestigatingDamages)
                       PROJECT(SHO:SupervisorSignedForCargo)
                       PROJECT(SHO:SupervisorSignedForCargoDelivered)
                       PROJECT(SHO:TRID)
                       PROJECT(SHO:Taped)
                       PROJECT(SHO:TripsheetItems)
                       PROJECT(SHO:Unpacked)
                       PROJECT(SHO:WarningIssuedTo)
                       PROJECT(SHO:WarningsIssued1)
                       PROJECT(SHO:WarningsIssued2)
                       PROJECT(SHO:WarningsIssued3)
                       PROJECT(SHO:Wet)
                       PROJECT(SHO:WhatActionShouldHaveBeenTaken)
                       PROJECT(SHO:WhoThoughtReponsible)
                     END
ProgressWindow       WINDOW('Report Shortages & Damages'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Shortages_Damages Report'),AT(250,646,7750,10542),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,385),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Shortages & Damages'),AT(0,20,7750),USE(?ReportTitle),FONT('MS Sans Serif',18,,FONT:regular), |
  CENTER
                       END
Detail                 DETAIL,AT(,,7750,10000),USE(?Detail)
                         STRING(@n_10),AT(1781,73,,170),USE(SHO:SDID),RIGHT(1)
                         STRING(@d6),AT(3552,73),USE(SHO:Create_Date)
                         STRING('DI No.'),AT(115,302,1191,170),USE(?HeaderTitle:2),TRN
                         STRING(@n_10),AT(1781,302,,170),USE(SHO:DINo),RIGHT(1)
                         STRING('Delivery Items'),AT(115,531,1191,170),USE(?HeaderTitle:3),TRN
                         STRING(@s35),AT(1281,531,1191,170),USE(SHO:Items),RIGHT(1)
                         STRING('TRID'),AT(115,760,1191,170),USE(?HeaderTitle:4),TRN
                         STRING(@N_10),AT(1781,760,,170),USE(SHO:TRID),RIGHT(1)
                         STRING(@s35),AT(5885,750),USE(SHO:Supervisor1)
                         STRING('Tripsheet Items'),AT(115,979,1191,170),USE(?HeaderTitle:5),TRN
                         STRING(@s35),AT(1281,979,1191,170),USE(SHO:TripsheetItems),RIGHT(1)
                         STRING(@s35),AT(5885,958),USE(SHO:FLDriver2)
                         STRING(@n_10),AT(1781,1198,,170),USE(SHO:IID),RIGHT(1)
                         STRING(@s35),AT(5885,1167),USE(SHO:Supervisor2)
                         STRING('POD (IID):'),AT(115,1458,1191,170),USE(?HeaderTitle:7),TRN
                         STRING(@n_10),AT(1781,1458),USE(SHO:POD_IID),RIGHT(1)
                         STRING('Delivery Depot:'),AT(125,1688,1191,170),USE(?HeaderTitle:8),TRN
                         STRING(@s35),AT(1458,1688),USE(SHO:DeliveryDepot)
                         STRING(@s100),AT(5885,1688),USE(Delivery_Address)
                         STRING('Collection Branch:'),AT(115,1906,1191,170),USE(?HeaderTitle:9),TRN
                         STRING(@s35),AT(1458,1927),USE(L_RG:Collection_Branch)
                         STRING('Trip Sheet Driver:'),AT(4615,229,1191,170),USE(?HeaderTitle:10),TRN
                         STRING('Fork Lift Driver 1:'),AT(4615,542,1191,170),USE(?HeaderTitle:11),TRN
                         STRING(@s35),AT(5885,542),USE(SHO:FLDriver1)
                         STRING('Supervisor 1:'),AT(4615,750,1191,170),USE(?HeaderTitle:12),TRN
                         STRING('Fork Lift Driver 2:'),AT(4615,958,1191,170),USE(?HeaderTitle:13),TRN
                         STRING('Supervisor 2:'),AT(4615,1167,1191,170),USE(?HeaderTitle:14),TRN
                         STRING('Delivery Driver:'),AT(4615,21,1191,170),USE(?HeaderTitle:15),TRN
                         STRING(@s35),AT(5885,52),USE(L_RG:Delivery_Driver)
                         STRING('DI Date:'),AT(2781,302,1191,170),USE(?HeaderTitle:16),TRN
                         STRING(@d6),AT(3552,302),USE(SHO:DI_Date)
                         STRING(@s35),AT(5885,313),USE(L_RG:TripSheet_Driver)
                         STRING('Created Date:'),AT(2771,73,1191,170),USE(?HeaderTitle:17),TRN
                         STRING('Consignee:'),AT(4615,1458,1191,170),USE(?HeaderTitle:18),TRN
                         STRING(@s35),AT(5885,1458),USE(L_RG:Consignee)
                         STRING('Delivery Address:'),AT(4615,1688,1191,170),USE(?HeaderTitle:25),TRN
                         GROUP('Uplift Condition'),AT(52,2458,5365,729),USE(?Group1),BOXED
                           CHECK('Damaged'),AT(208,2667),USE(SHO:Damaged)
                           CHECK('Unpacked'),AT(1042,2667),USE(SHO:Unpacked)
                           CHECK('Short'),AT(1927,2667),USE(SHO:Short)
                           CHECK('Wet'),AT(2604,2667),USE(SHO:Wet)
                           TEXT,AT(3229,2667,2031,417),USE(SHO:ConditionComment),BOXED
                           CHECK('Taped'),AT(208,2917),USE(SHO:Taped)
                           CHECK('Palletised'),AT(1042,2917),USE(SHO:Palletised)
                           CHECK('Loose'),AT(1927,2917),USE(SHO:Loose)
                           CHECK('Other'),AT(2604,2917),USE(SHO:Other)
                         END
                         STRING('Remarks on POD:'),AT(4615,1927,1191,170),USE(?HeaderTitle:19),TRN
                         STRING(@s255),AT(5885,1927),USE(SHO:RemarksOnPOD)
                         STRING('Report No.:'),AT(115,2177,1191,170),USE(?HeaderTitle:20),TRN
                         STRING(@s35),AT(1458,2177),USE(SHO:ReportNo)
                         TEXT,AT(5667,2229,2031,417),USE(SHO:ReportComments),BOXED
                         STRING('Report Comments:'),AT(4615,2229,1191,170),USE(?HeaderTitle:21),TRN
                         STRING('Drivers Notation DI:'),AT(115,3302,1191,170),USE(?HeaderTitle:22),TRN
                         STRING(@s255),AT(1458,3302),USE(SHO:DriversNotationDI)
                         CHECK('Driver Signed'),AT(3750,3302),USE(SHO:DriverSigned)
                         CHECK('Driver Comments Entered'),AT(3750,3552),USE(SHO:DriverCommentsEntered)
                         GROUP('Depot'),AT(52,3781,7604,1094),USE(?Group5),BOXED
                           TEXT,AT(1552,3958,2031,417),USE(SHO:DepotConditionReceivedDispatch),BOXED
                           CHECK('Depot Condition Received'),AT(3906,3958),USE(SHO:DepotConditionReceived)
                           STRING('Condition Dispatch:'),AT(146,3958,1191,170),USE(?HeaderTitle:23),TRN
                           STRING('Depot Remarks:'),AT(156,4417,1191,170),USE(?HeaderTitle:24),TRN
                           CHECK('Depot Supervisor Notation'),AT(3906,4188),USE(SHO:DepotSupervisorNotation)
                           TEXT,AT(1552,4396,2031,417),USE(SHO:DepotRemarks),BOXED
                           CHECK('Depot Warning Issued Driver'),AT(3906,4417),USE(SHO:DepotWarningIssuedDriver)
                         END
                         GROUP('Destination Depot'),AT(52,5104,7604,2396),USE(?Group3),BOXED
                           CHECK('Condition Received'),AT(104,5313),USE(SHO:DestConditionReceived)
                           CHECK('Short'),AT(1885,5313),USE(SHO:DestShort)
                           CHECK('Damaged'),AT(2688,5313),USE(SHO:DestDamaged)
                           CHECK('Un Packed'),AT(3708,5313),USE(SHO:DestUnPacked)
                           CHECK('Loose'),AT(4813,5313),USE(SHO:DestLoose)
                           CHECK('Taped'),AT(5656,5313),USE(SHO:DestTaped)
                           CHECK('Palletised'),AT(6563,5313),USE(SHO:DestPalletised)
                           GROUP('Destination Further Damages'),AT(156,5573,7344,1198),USE(?Group2),BOXED
                             CHECK('Further Damages Repacked'),AT(260,5781),USE(SHO:DestFurtherDamages_Repacked)
                             CHECK('Further Damages Retaped'),AT(2396,5781),USE(SHO:DestFurtherDamages_Retaped)
                             STRING('Remarks:'),AT(4740,5781),USE(?String52),TRN
                             TEXT,AT(5365,5781,2031,417),USE(SHO:DestFurtherDamages_Remarks),BOXED
                             CHECK('Further Damages Materials Straps'),AT(260,6042),USE(SHO:DestFurtherDamages_Materials_Straps)
                             CHECK('Further Damages Materials Clear Tape'),AT(2396,6042),USE(SHO:DestFurtherDamages_Materials_ClearTape)
                             CHECK('Further Damages Materials B Tape'),AT(260,6292),USE(SHO:DestFurtherDamages_Materials_B_Tape)
                             CHECK('Further Damages Materials Boxes'),AT(2396,6292),USE(SHO:DestFurtherDamages_Materials_Boxes)
                             STRING('Other:'),AT(4740,6260),USE(?String52:2),TRN
                             TEXT,AT(5365,6260,2031,417),USE(SHO:DestFurtherDamages_Materials_Other),BOXED
                           END
                           STRING('Reported:'),AT(2781,6823),USE(?String54:2),TRN
                           STRING('Driver Comments Off Loading:'),AT(5365,6823),USE(?String54:3),TRN
                           STRING('Condition:'),AT(208,6823),USE(?String54),TRN
                           TEXT,AT(208,7000,2031,417),USE(SHO:FinalDestination_Condition),BOXED
                           TEXT,AT(2781,7000,2031,417),USE(SHO:FinalDestination_Reported),BOXED
                           TEXT,AT(5365,7000,2031,417),USE(SHO:FinalDestination_DriversCommentsOffLoading),BOXED
                         END
                         GROUP('Office Group'),AT(52,7552,7604,2396),USE(?Group4),BOXED
                           CHECK('Driver Advised POD Endorsed'),AT(156,7771),USE(SHO:DriverAdvised_POD_Endorsed)
                           STRING(@s35),AT(3469,7771),USE(SHO:DriverAdvised_Person)
                           CHECK('Client Phoned'),AT(156,8031),USE(SHO:ClientPhoned)
                           STRING('Client Information:'),AT(1250,8031),USE(?String59),TRN
                           TEXT,AT(2229,8031,2031,417),USE(SHO:ClientPhoned_Information),BOXED
                           STRING('Action Should Have Been Taken:'),AT(156,8490),USE(?String59:2),TRN
                           TEXT,AT(2229,8490,2031,417),USE(SHO:WhatActionShouldHaveBeenTaken),BOXED
                           STRING('Relevant Info.:'),AT(4583,8490),USE(?String67),TRN
                           CHECK('Have Warnings Been Issued'),AT(156,9000),USE(SHO:HaveWarningsBeenIssued)
                           STRING(@s35),AT(3469,9000),USE(SHO:WarningIssuedTo)
                           STRING('Police Reference:'),AT(2240,9219),USE(?String59:4),TRN
                           CHECK('Stolen Goods Reported To Police'),AT(156,9219),USE(SHO:StolenGoods_ReportedToPolice)
                           STRING(@s35),AT(3469,9219),USE(SHO:PoliceReference)
                           STRING('Who thought Responsible:'),AT(156,9479),USE(?String59:5),TRN
                           STRING(@s35),AT(2240,9479),USE(SHO:WhoThoughtReponsible)
                           TEXT,AT(5365,8490,2031,417),USE(SHO:AnyOtherRelevantInformation),BOXED
                           STRING('Warning Issued to:'),AT(2240,9000),USE(?String59:3),TRN
                           STRING('Driver Advised Person:'),AT(2229,7771),USE(?String58),TRN
                         END
                         TEXT,AT(5365,3302,2031,417),USE(SHO:DriverComments),BOXED
                         STRING('Invoice Number'),AT(115,1198,1191,170),USE(?HeaderTitle:6),TRN
                         STRING('Shortages & Damages ID:'),AT(115,73,,170),USE(?HeaderTitle:1),TRN
                       END
detail1                DETAIL,AT(,,,4792),USE(?secondpage)
                         GROUP('Final Comments'),AT(52,104,7604,4635),USE(?Group6),BOXED
                           STRING(@d5b),AT(2240,313),USE(SHO:DateInvestigationCompleted)
                           STRING('Collected By::'),AT(156,573),USE(?String59:6),TRN
                           STRING(@s35),AT(2240,573,3542,156),USE(SHO:ConsignmentCollectedBy)
                           STRING('Supervisor Signed for Cargo:'),AT(156,875),USE(?String59:7),TRN
                           STRING(@s35),AT(2240,875,3542,167),USE(SHO:SupervisorSignedForCargo)
                           STRING(@s35),AT(2240,1125,3542,167),USE(SHO:ConsignmentDeliveredBy)
                           STRING('Consignment Delivered By::'),AT(156,1125),USE(?String59:8),TRN
                           STRING('Supervisor Signed for Delivered:'),AT(156,1354),USE(?String59:9),TRN
                           STRING(@s35),AT(2240,1354,3542,167),USE(SHO:SupervisorSignedForCargoDelivered)
                           STRING('Cause of Damage:'),AT(156,1573),USE(?String59:10),TRN
                           STRING(@s50),AT(2240,1573,3542,167),USE(SHO:CauseOfDamage)
                           STRING('Branch Responsible:'),AT(156,1781),USE(?String59:11),TRN
                           STRING(@s35),AT(2240,1781,3542,167),USE(SHO:BranchResponsible)
                           STRING('Local Driver Responsible:'),AT(156,1990),USE(?String59:12),TRN
                           STRING(@s35),AT(2240,1990,3542,167),USE(SHO:LocalDriverResponsible)
                           STRING('Long Distance Driver Responsible:'),AT(156,2198),USE(?String59:13),TRN
                           STRING(@s35),AT(2240,2198,3542,167),USE(SHO:LongDistanceDriverResponsible)
                           STRING('Client Responsible:'),AT(156,2417),USE(?String59:14),TRN
                           STRING(@s35),AT(2240,2417,3542,167),USE(SHO:ClientResponsible)
                           STRING('Supervisor Accepted Damaged:'),AT(156,2656),USE(?String59:15),TRN
                           STRING(@s35),AT(2240,2656,3542,167),USE(SHO:SupervisorAcceptedDamagedCargo)
                           STRING('Supervisor Investigating:'),AT(156,2885),USE(?String59:16),TRN
                           STRING(@s35),AT(2240,2885,3542,167),USE(SHO:SupervisorInvestigatingDamages)
                           STRING('Correctove Action Taken:'),AT(156,3125),USE(?String59:17),TRN
                           TEXT,AT(2240,3125,5260,729),USE(SHO:CorrectiveActionTaken),RESIZE
                           STRING('Warnings Issued 1:'),AT(156,3917),USE(?String59:18),TRN
                           STRING(@s50),AT(2240,3917,3542,167),USE(SHO:WarningsIssued1)
                           STRING('Warnings Issued 2:'),AT(156,4125),USE(?String59:19),TRN
                           STRING(@s50),AT(2240,4125,3542,167),USE(SHO:WarningsIssued2)
                           STRING('Date Completed:'),AT(156,313),USE(?String69),TRN
                           STRING('Warnings Issued 3:'),AT(156,4333),USE(?String59:20),TRN
                           STRING(@s50),AT(2240,4333,3542,167),USE(SHO:WarningsIssued3)
                         END
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Shortages_Damages')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:Shortages_Damages.Open                            ! File Shortages_Damages used by this procedure, so make sure it's RelationManager is open
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Shortages_Damages',ProgressWindow)   ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:Shortages_Damages, ?Progress:PctText, Progress:Thermometer, ProgressMgr, SHO:SDID)
  ThisReport.AddSortOrder(SHO:PKey_SDID)
  ThisReport.AddRange(SHO:SDID)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Shortages_Damages.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:Shortages_Damages.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Shortages_Damages',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      BRA:BID     = SHO:CollectionBID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         L_RG:Collection_Branch   = BRA:BranchName
      .
  
      DRI:DRID    = SHO:TripSheetDriverID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RG:TripSheet_Driver    = DRI:FirstNameSurname
      .
  
      DRI:DRID    = SHO:DeliveryDriverID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RG:Delivery_Driver     = DRI:FirstNameSurname
      .
      
  !    CLI:CID     = SHO:ConsigneeCID
  !    IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
  !       L_RG:Consignee           = CLI:ClientName
  !    .
  
      L_RG:Consignee              = Get_Address(SHO:ConsigneeAID)
  
  
      DEL:DID  = SHO:DID
      IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
         Delivery_Address         = Get_Address(DEL:DeliveryAID)
      .
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  PRINT(RPT:detail1)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Shortages_Damages','Print_Shortages_Damages','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

