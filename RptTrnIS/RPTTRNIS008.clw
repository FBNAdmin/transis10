

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS008.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_TripSheet_Collections PROCEDURE (p:TRID)

ReportPageNumber     SHORT                                 !
Progress:Thermometer BYTE                                  !
LOC:Rep_Fields       GROUP,PRE(L_RF)                       !
Weight               DECIMAL(10,2)                         !In kg's
VolumetricWeight     DECIMAL(10,2)                         !Weight based on Volumetric calculation (in kgs)
Charge               DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
State                STRING(20)                            !State of this manifest
Driver               STRING(50)                            !First Name
Driver_Assistant     STRING(35)                            !
Reg_of_Horse         STRING(20)                            !
Reg_of_Trailer       STRING(20)                            !
COD_Comment          STRING(20)                            !
TransporterName      STRING(55)                            !Transporters Name
Suburb               STRING(50)                            !Suburb
                     END                                   !
LOC:Delivery_Totals  GROUP,PRE(L_DT)                       !
DID                  ULONG                                 !Delivery ID
InsuranceCharge      DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Insurance            DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Docs                 DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Fuel                 DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Charge               DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
                     END                                   !
LOC:Manifest_Totals  GROUP,PRE(L_MT)                       !
Cost                 DECIMAL(10,2)                         !
TRID                 ULONG                                 !Tripsheet ID
Delivery_Charges     DECIMAL(11,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Total_Weight         DECIMAL(10,2)                         !In kg's
Gross_Profit         DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Gross_Profit_Percent DECIMAL(6,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Average_C_Per_Kg     DECIMAL(7,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Legs_Cost            DECIMAL(10,2)                         !
Total_Cost           DECIMAL(10,2)                         !Includes extra legs cost
                     END                                   !
Process:View         VIEW(TripSheets)
                       PROJECT(TRI:DepartDate)
                       PROJECT(TRI:DepartTime)
                       PROJECT(TRI:TRID)
                       PROJECT(TRI:VCID)
                       PROJECT(TRI:BID)
                       JOIN(VCO:PKey_VCID,TRI:VCID)
                         PROJECT(VCO:CompositionName)
                         PROJECT(VCO:TID)
                       END
                       JOIN(BRA:PKey_BID,TRI:BID)
                         PROJECT(BRA:BranchName)
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(615,2417,10396,5302),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('Arial',10,,FONT:regular, |
  CHARSET:ANSI),THOUS
                       HEADER,AT(615,1000,10396,1417),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Trip Sheet No.:'),AT(7615,63),USE(?String31),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@N_10),AT(9135,63,1021,167),USE(TRI:TRID),FONT(,,,FONT:bold,CHARSET:ANSI),RIGHT(1), |
  TRN
                         STRING('Trip Sheet'),AT(458,52,9479,417),USE(?String55),FONT(,24,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         IMAGE('fbn_logo_small.jpg'),AT(52,52,2302,458),USE(?Image1)
                         STRING('Branch:'),AT(63,563,1083,167),USE(?String51),TRN
                         STRING(@s35),AT(1063,563),USE(BRA:BranchName)
                         STRING('Arrival Time Depot: _{25}'),AT(7615,323),USE(?String52:3),TRN
                         STRING('Driver:'),AT(63,771,1083,167),USE(?String69:2),TRN
                         STRING(@s35),AT(1063,771,1583,167),USE(L_RF:Driver)
                         STRING('Assistant:'),AT(5073,625),USE(?String52:7),TRN
                         STRING(@s35),AT(5719,625,1823,156),USE(L_RF:Driver_Assistant),TRN
                         STRING('Supervisor Signature: _{23}'),AT(7615,625),USE(?String52:4),TRN
                         STRING('Date Despatched:'),AT(63,979,1083,167),USE(?String3:11),LEFT,TRN
                         STRING('Time Out:'),AT(63,1198,1083),USE(?String3:10),LEFT,TRN
                         STRING(@d6),AT(1063,979,,167),USE(TRI:DepartDate),RIGHT(1)
                         STRING(@t7),AT(1063,1198,729,167),USE(TRI:DepartTime),RIGHT(1)
                         STRING('Drivers Signature: _{26}'),AT(7615,927),USE(?String52:5),TRN
                         STRING('Starting Kms: _{23}'),AT(5073,927),USE(?String52),TRN
                         STRING('Manager Signature: _{25}'),AT(7615,1240),USE(?String52:6),TRN
                         STRING('Closing Kms: _{23}'),AT(5073,1240),USE(?String52:2),TRN
                       END
break_tripsheet        BREAK(TRDI:TRID)
                         HEADER,AT(0,0,,521),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI),PAGEBEFORE(-1)
                           STRING('Transporter:'),AT(63,52,1083),USE(?String3:2),LEFT,TRN
                           STRING(@s55),AT(1063,52,3073,156),USE(L_RF:TransporterName),LEFT
                           LINE,AT(52,240,10350,0),USE(?Line8:4),COLOR(COLOR:Black)
                           STRING(@s35),AT(938,292,1583,167),USE(VCO:CompositionName),TRN
                           LINE,AT(52,510,10350,0),USE(?Line8:6),COLOR(COLOR:Black)
                           STRING('Reg. Horse:'),AT(2969,292),USE(?String69:3),TRN
                           STRING(@s20),AT(3646,292,979,167),USE(L_RF:Reg_of_Horse)
                           STRING('Trailer:'),AT(5188,292),USE(?String69:4),TRN
                           STRING(@s20),AT(5625,292),USE(L_RF:Reg_of_Trailer)
                           STRING('Vehicle Comp.:'),AT(125,292),USE(?String69),TRN
                           STRING('State:'),AT(8563,63),USE(?String54),FONT(,,,FONT:regular,CHARSET:ANSI),TRN
                           STRING(@s20),AT(8917,63,,167),USE(L_RF:State),FONT(,,,FONT:regular,CHARSET:ANSI),RIGHT(1), |
  TRN
                           STRING(@s50),AT(8135,292),USE(L_RF:Suburb)
                           STRING('Suburb:'),AT(7698,292),USE(?String54:2),FONT(,,,FONT:regular,CHARSET:ANSI),TRN
                         END
break_delivery           BREAK(DEL:DID)
Detail                     DETAIL,AT(,,,0),USE(?Detail),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                             GROUP,AT(31,-21,9650,281),USE(?Group1),BOXED,HIDE,TRN
                               STRING('Not printing detail lines at all'),AT(52,73,885,167),USE(?String1),TRN
                               STRING(@n-11.2),AT(4594,73,677,167),USE(L_RF:VolumetricWeight),RIGHT(1),HIDE
                               STRING(@n~CW~-13.2),AT(2469,73,875,167),USE(L_RF:Weight),RIGHT(1)
                             END
                           END
                         END
                       END
Collections            DETAIL,AT(,,,4667),USE(?collections),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI),TOGETHER
                         STRING('Collections'),AT(52,0,10313,260),USE(?String66),FONT(,12,,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         STRING('No.'),AT(63,252),USE(?String58),TRN
                         STRING('DI Number'),AT(363,252),USE(?String58:9),TRN
                         STRING('From'),AT(1292,260,1563,156),USE(?String58:2),CENTER,TRN
                         STRING('To'),AT(3021,260,1823,156),USE(?String58:3),CENTER,TRN
                         STRING('Weight (kg)'),AT(4979,250,938,167),USE(?String58:4),CENTER,TRN
                         STRING('No. of Packages'),AT(6094,250,938,167),USE(?String58:5),CENTER,TRN
                         STRING('Time In'),AT(7063,250,938,167),USE(?String58:6),CENTER,TRN
                         STRING('Time Out'),AT(8000,250,938,167),USE(?String58:7),CENTER,TRN
                         STRING('Signature'),AT(9281,260,729,156),USE(?String58:8),CENTER,TRN
                         LINE,AT(300,430,0,4200),USE(?Line14),COLOR(COLOR:Black)
                         LINE,AT(1165,430,0,4200),USE(?Line141),COLOR(COLOR:Black)
                         LINE,AT(2948,430,0,4200),USE(?Line14:3),COLOR(COLOR:Black)
                         LINE,AT(4865,430,0,4200),USE(?Line14:7),COLOR(COLOR:Black)
                         LINE,AT(6063,430,0,4200),USE(?Line14:6),COLOR(COLOR:Black)
                         LINE,AT(7073,430,0,4200),USE(?Line14:5),COLOR(COLOR:Black)
                         LINE,AT(8000,430,0,4200),USE(?Line14:4),COLOR(COLOR:Black)
                         LINE,AT(8946,430,0,4200),USE(?Line14:2),COLOR(COLOR:Black)
                         STRING('1'),AT(63,500,208,156),USE(?String58:10),TRN
                         STRING('2'),AT(63,800,208,156),USE(?String58:11),TRN
                         STRING('3'),AT(63,1100,208,156),USE(?String58:12),TRN
                         STRING('4'),AT(63,1400,208,156),USE(?String58:13),TRN
                         STRING('5'),AT(63,1700,208,156),USE(?String58:14),TRN
                         STRING('6'),AT(63,2000,208,156),USE(?String58:15),TRN
                         STRING('7'),AT(63,2300,208,156),USE(?String58:16),TRN
                         STRING('8'),AT(63,2600,208,156),USE(?String58:17),TRN
                         STRING('9'),AT(63,2900,208,156),USE(?String58:171),TRN
                         STRING('10'),AT(63,3200,208,156),USE(?String58:172),TRN
                         STRING('11'),AT(63,3500,208,156),USE(?String58:173),TRN
                         STRING('12'),AT(63,3800,208,156),USE(?String58:174),TRN
                         STRING('13'),AT(63,4100,208,156),USE(?String58:175),TRN
                         STRING('14'),AT(63,4400,208,156),USE(?String58:176),TRN
                         LINE,AT(42,430,10350,0),USE(?Line13),COLOR(COLOR:Black)
                         LINE,AT(42,730,10350,0),USE(?Line13:2),COLOR(COLOR:Black)
                         LINE,AT(42,1030,10350,0),USE(?Line13:3),COLOR(COLOR:Black)
                         LINE,AT(42,1330,10350,0),USE(?Line13:4),COLOR(COLOR:Black)
                         LINE,AT(42,1630,10350,0),USE(?Line13:41),COLOR(COLOR:Black)
                         LINE,AT(42,1930,10350,0),USE(?Line13:5),COLOR(COLOR:Black)
                         LINE,AT(42,2230,10350,0),USE(?Line13:6),COLOR(COLOR:Black)
                         LINE,AT(42,2530,10350,0),USE(?Line13:7),COLOR(COLOR:Black)
                         LINE,AT(42,2830,10350,0),USE(?Line13:8),COLOR(COLOR:Black)
                         LINE,AT(42,3130,10350,0),USE(?Line13:81),COLOR(COLOR:Black)
                         LINE,AT(42,3430,10350,0),USE(?Line13:812),COLOR(COLOR:Black)
                         LINE,AT(42,3730,10350,0),USE(?Line13:832),COLOR(COLOR:Black)
                         LINE,AT(42,4030,10350,0),USE(?Line13:842),COLOR(COLOR:Black)
                         LINE,AT(42,4330,10350,0),USE(?Line13:852),COLOR(COLOR:Black)
                         LINE,AT(42,4630,10350,0),USE(?Line13:862),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(615,7740,10396,240),USE(?Footer),FONT('MS Sans Serif',,COLOR:Black,,CHARSET:ANSI)
                         STRING('Report Date:'),AT(73,31),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(875,31),USE(?ReportDateStamp),TRN
                         STRING('Report Time:'),AT(2250,31),USE(?ReportTimePrompt),TRN
                         STRING('<<-- Time Stamp -->'),AT(3052,31),USE(?ReportTimeStamp),TRN
                         STRING(@N3),AT(10031,31),USE(ReportPageNumber)
                         STRING('Page:'),AT(9615,31),USE(?String61),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.AskPreview PROCEDURE

  CODE
          PRINT(RPT:Collections)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_TripSheet_Collections')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Add_Suburbs.SetOpenRelated()
  Relate:Add_Suburbs.Open                                  ! File Add_Suburbs used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterAlias.Open                             ! File TransporterAlias used by this procedure, so make sure it's RelationManager is open
  Access:TripSheetDeliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailerAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_TripSheet_Collections',ProgressWindow) ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:TripSheets, ?Progress:PctText, Progress:Thermometer, ProgressMgr, TRI:TRID)
  ThisReport.AddSortOrder(TRI:PKey_TID)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:TripSheets.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      IF p:TRID ~= 0
         ThisReport.AddRange(TRI:TRID, p:TRID)
      .
  
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Suburbs.Close
    Relate:TransporterAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_TripSheet_Collections',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      A_TRU:TTID              = VCO:TTID0
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Horse    = A_TRU:Registration
      .
      A_TRU:TTID              = VCO:TTID1
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Trailer  = A_TRU:Registration
      .
  
      DRI:DRID                = TRI:DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RF:Driver          = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
      DRI:DRID                    = TRI:Assistant_DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RF:Driver_Assistant    = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
  
  
      !A_TRA:TID            = VCO:TID
      A_TRA:TID            = TRI:TID
      IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) = LEVEL:Benign
         L_RF:TransporterName = A_TRA:TransporterName
      .
  
      SUBU:SUID           = TRI:SUID
      IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) = LEVEL:Benign
         L_RF:Suburb      = SUBU:Suburb
      .
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  PRINT(RPT:Collections)
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF L_MT:TRID ~= TRI:TRID
         !L_MT:MID                     = MAL:MID
         L_MT:TRID                    = TRI:TRID
  
         EXECUTE TRI:State
            L_RF:State  = 'Loaded'
            L_RF:State  = 'On Route'
            L_RF:State  = 'Transferred'
         ELSE
            L_RF:State  = 'Loading'
      .  .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_TripSheet_Collections','Print_TripSheet_Collections','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! ---------------------  uses SQL Query class
!!! </summary>
Print_ManagementProfit_Summary PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Errors           ULONG                                 !
LOC:Transaction_Log_Name STRING(255)                       !
LOC:Settings         GROUP,PRE(L_SG)                       !
PDF                  BYTE                                  !
ExcludeBadDebts      BYTE(1)                               !Exclude Bad Debt Credit Notes
Output               BYTE                                  !Output trail of items
Branch1              ULONG                                 !Branch ID
BranchName1          STRING(35)                            !Branch Name
Branch2              ULONG                                 !Branch ID
BranchName2          STRING(35)                            !Branch Name
VAT                  BYTE                                  !
VAT_Msg              STRING(20)                            !for report display
                     END                                   !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
LOC:Summary_Group    GROUP,PRE(L_SG)                       !
Turnover_DBN         DECIMAL(10,2)                         !
Turnover_JHB         DECIMAL(10,2)                         !
Turnover_Total       DECIMAL(10,2)                         !
Credits_DBN          DECIMAL(10,2)                         !
Credits_JHB          DECIMAL(10,2)                         !
Credits_Total        DECIMAL(10,2)                         !
Turnover_less_Credits_DBN DECIMAL(10,2)                    !
Turnover_less_Credits_JHB DECIMAL(10,2)                    !
Work_Days            LONG                                  !
Broking_Kg_DBN       DECIMAL(8)                            !
Broking_Kg_JHB       DECIMAL(8)                            !
Broking_Kg_Total     DECIMAL(8)                            !
Overnight_Kg_DBN     DECIMAL(8)                            !
Overnight_Kg_JHB     DECIMAL(8)                            !
Overnight_Kg_Total   DECIMAL(8)                            !
Kg_Total             DECIMAL(8)                            !
Kg_Per_Day           DECIMAL(8)                            !
Kg_Per_Day_DBN       DECIMAL(8)                            !
Kg_Per_Day_JHB       DECIMAL(8)                            !
Average_Cost_Per_Kg  DECIMAL(7,2)                          !
Gross_GP_DBN         DECIMAL(10,2)                         !
Gross_GP_JHB         DECIMAL(10,2)                         !
Gross_GP_Total       DECIMAL(10,2)                         !
Gross_GP_Per_Day     DECIMAL(10,2)                         !
Loadmaster_DBN       DECIMAL(10,2)                         !
Loadmaster_JHB       DECIMAL(10,2)                         !
Loadmaster_Total     DECIMAL(10,2)                         !
Loadmaster_Per_Day   DECIMAL(10,2)                         !
Loadmaster_GP_Total  DECIMAL(10,2)                         !
Loadmaster_GP_Per_Day DECIMAL(10,2)                        !
Turnover_Per_Day     DECIMAL(10,2)                         !
Turnover_Per_Day_DBN DECIMAL(10,2)                         !
Turnover_Per_Day_JHB DECIMAL(10,2)                         !
Sales_Broking_DBN    DECIMAL(10,2)                         !
Sales_Broking_JHB    DECIMAL(10,2)                         !
Sales_Overnight_DBN  DECIMAL(10,2)                         !
Sales_Overnight_JHB  DECIMAL(10,2)                         !
Costs_Broking_DBN    DECIMAL(10,2)                         !
Costs_Broking_JHB    DECIMAL(10,2)                         !
GP_Broking_DBN       DECIMAL(10,2)                         !
GP_Broking_JHB       DECIMAL(10,2)                         !
GP_Per_Broking_DBN   DECIMAL(6,2)                          !
GP_Per_Broking_JHB   DECIMAL(6,2)                          !
GP_Overnight_DBN     DECIMAL(10,2)                         !
GP_Overnight_JHB     DECIMAL(10,2)                         !
GP_Per_Overnight_DBN DECIMAL(6,2)                          !
GP_Per_Overnight_JHB DECIMAL(6,2)                          !
Extra_Invoices_DBN   DECIMAL(10,2)                         !Transporter
Extra_Invoices_JHB   DECIMAL(10,2)                         !Transporter
Transporter_Credits_DBN DECIMAL(10,2)                      !
Transporter_Credits_JHB DECIMAL(10,2)                      !
Transporter_Debits_DBN DECIMAL(10,2)                       !
Transporter_Debits_JHB DECIMAL(10,2)                       !
Sales_Broking_DBN_Total DECIMAL(10,2)                      !
Sales_Broking_JHB_Total DECIMAL(10,2)                      !
Extra_Leg_DBN        DECIMAL(10,2)                         !
Extra_Leg_Broking_DBN DECIMAL(10,2)                        !
Extra_Leg_ON_DBN     DECIMAL(10,2)                         !
Extra_Leg_JHB        DECIMAL(10,2)                         !
Extra_Leg_Broking_JHB DECIMAL(10,2)                        !
Extra_Leg_ON_JHB     DECIMAL(10,2)                         !
Transporter_Costs_DBN DECIMAL(10,2)                        !
Transporter_Costs_JHB DECIMAL(10,2)                        !
                     END                                   !
Process:View         VIEW(Setup)
                     END
FDB4::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
FDB7::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?L_SG:BranchName1
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !Queue declaration for browse/combo box using ?L_SG:BranchName2
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
ProgressWindow       WINDOW('Report - Management Profit Summary'),AT(,,198,180),FONT('MS Sans Serif',8,,FONT:regular), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       SHEET,AT(4,2,188,154),USE(?Sheet1)
                         TAB('Settings'),USE(?Tab2)
                           CHECK(' Exclude Bad Debts'),AT(78,24),USE(L_SG:ExcludeBadDebts),MSG('Exclude Bad Debt C' & |
  'redit Notes'),TIP('Exclude Bad Debt Credit Notes')
                           PROMPT('Audit Output:'),AT(10,42),USE(?L_SG:Output_Trail:Prompt)
                           LIST,AT(78,42,60,10),USE(L_SG:Output),DROP(5),FROM('None|#0|All|#1'),MSG('Output trail of items'), |
  TIP('Output trail of items')
                           PROMPT('Branch Name 1:'),AT(10,65),USE(?L_SG:BranchName1:Prompt:2)
                           LIST,AT(78,66,109,10),USE(L_SG:BranchName1),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                           PROMPT('Branch Name 2:'),AT(10,81),USE(?BranchName2:Prompt:2)
                           LIST,AT(78,81,109,10),USE(L_SG:BranchName2),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop:1)
                           PROMPT('VAT:'),AT(10,110),USE(?L_SG:VAT:Prompt:2)
                           LIST,AT(78,110,94,10),USE(L_SG:VAT),DROP(5),FROM('Exclusive of VAT|#0|Inclusive of VAT|#1'), |
  MSG('Output trail of items'),TIP('Output trail of items')
                         END
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(22,34,151,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(15,22,165,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(15,50,165,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Cancel'),AT(142,159,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                       BUTTON('Pause'),AT(88,159,49,15),USE(?Pause),LEFT,ICON('Blue_R.ico'),FLAT
                     END

Report               REPORT('Manifest Report'),AT(250,833,9302,6979),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,9302,583),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Management Profit Summary'),AT(0,21,9300),USE(?ReportTitle),FONT(,12,,FONT:bold),CENTER
                         STRING('To:'),AT(4979,313),USE(?String19:49),FONT(,10,,,CHARSET:ANSI),TRN
                         STRING('From:'),AT(3469,313),USE(?String19:48),FONT(,10,,,CHARSET:ANSI),TRN
                         STRING(@d5b),AT(3854,313),USE(LO:From_Date),FONT(,10,,,CHARSET:ANSI),RIGHT(1)
                         STRING(@d5b),AT(5240,313),USE(LO:To_Date),FONT(,10,,,CHARSET:ANSI),RIGHT(1)
                         STRING(@s20),AT(7448,312),USE(L_SG:VAT_Msg)
                       END
Detail                 DETAIL,AT(10,10,10604,6833),USE(?Detail)
                         BOX,AT(6458,3448,2500,2604),USE(?Box_Branch2),COLOR(COLOR:Black),LINEWIDTH(2)
                         STRING('Johannesburg'),AT(6563,3375,1094,208),USE(?String_Branch2),CENTER
                         STRING(@n-14.2),AT(7927,4323,885,167),USE(L_SG:GP_Broking_JHB),RIGHT(1)
                         STRING(@N-9.2~%~),AT(7927,4552,885,167),USE(L_SG:GP_Per_Broking_JHB),RIGHT(1)
                         LINE,AT(6510,4771,2400,0),USE(?Line2),COLOR(COLOR:Black)
                         STRING('GP % Broking:'),AT(6563,4552),USE(?String19:43),TRN
                         STRING(@n-14.2),AT(7927,5531,885,167),USE(L_SG:GP_Overnight_JHB),RIGHT(1)
                         STRING(@n-14.2),AT(7927,5062,885,167),USE(L_SG:Sales_Overnight_JHB),RIGHT(1)
                         STRING('Loadmaster:'),AT(6563,5271),USE(?String19:72),TRN
                         STRING(@n-14.2),AT(7927,5271),USE(L_SG:Loadmaster_JHB,,?L_SG:Loadmaster_JHB:2),RIGHT(1),TRN
                         STRING('Sales Overnight:'),AT(6563,5063),USE(?String19:37),TRN
                         STRING(@N-9.2~%~),AT(7927,5781,885,167),USE(L_SG:GP_Per_Overnight_JHB),RIGHT(1)
                         STRING('GP Broking:'),AT(6563,4323),USE(?String19:41),TRN
                         STRING(@n-14.2),AT(7927,3865),USE(L_SG:Sales_Broking_JHB_Total),RIGHT(1)
                         STRING('Sales Broking Total:'),AT(6563,3865),USE(?String19:57)
                         STRING('GP Overnight:'),AT(6563,5531),USE(?String19:45),TRN
                         STRING('Total Cost Broking:'),AT(6563,4094),USE(?String19:63),TRN
                         STRING(@n-14.2),AT(7927,4094,885,167),USE(L_SG:Transporter_Costs_JHB),RIGHT(1),TRN
                         STRING('GP % Overnight:'),AT(6563,5781),USE(?String19:47),TRN
                         STRING('Broking Kg:'),AT(6563,3635),USE(?String19:70),TRN
                         STRING(@n-11.0),AT(7927,3635,885),USE(L_SG:Broking_Kg_JHB,,?L_SG:Broking_Kg_JHB:2),RIGHT(1), |
  TRN
                         STRING('Overnight Kg:'),AT(6563,4833),USE(?String19:71),TRN
                         STRING(@n-11.0),AT(7927,4833,885),USE(L_SG:Overnight_Kg_JHB,,?L_SG:Overnight_Kg_JHB:2),RIGHT(1), |
  TRN
                         BOX,AT(6458,104,2500,2604),USE(?Box_Branch1),COLOR(COLOR:Black),LINEWIDTH(2)
                         STRING('Durban'),AT(6563,10,1094,208),USE(?String_Branch1),CENTER
                         STRING('Broking Kg:'),AT(6563,302),USE(?String19:67),TRN
                         STRING(@n-11.0),AT(7927,302,885,167),USE(L_SG:Broking_Kg_DBN,,?L_SG:Broking_Kg_DBN:2),RIGHT(1), |
  TRN
                         LINE,AT(6521,1490,2400,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Overnight Kg:'),AT(6563,1552),USE(?String19:68),TRN
                         STRING(@n-11.0),AT(7927,1552,885),USE(L_SG:Overnight_Kg_DBN,,?L_SG:Overnight_Kg_DBN:2),RIGHT(1), |
  TRN
                         STRING(@n-14.2),AT(7927,1021,885,167),USE(L_SG:GP_Broking_DBN),RIGHT(1)
                         STRING('Sales Broking Total:'),AT(6563,542,990,167),USE(?String19:56),TRN
                         STRING(@n-14.2),AT(7927,542,885,167),USE(L_SG:Sales_Broking_DBN_Total),RIGHT(1)
                         STRING(@N-9.2~%~),AT(7927,1260,885,167),USE(L_SG:GP_Per_Broking_DBN),RIGHT(1)
                         STRING('GP % Broking:'),AT(6563,1260),USE(?String19:42),TRN
                         STRING('Sales Overnight:'),AT(6563,1781),USE(?String19:36),TRN
                         STRING(@n-14.2),AT(7927,2240,885,167),USE(L_SG:GP_Overnight_DBN),RIGHT(1)
                         STRING('GP Overnight:'),AT(6563,2240),USE(?String19:44),TRN
                         STRING(@n-14.2),AT(7927,1781,885,167),USE(L_SG:Sales_Overnight_DBN),RIGHT(1)
                         STRING(@N-9.2~%~),AT(7927,2458,885,167),USE(L_SG:GP_Per_Overnight_DBN),RIGHT(1)
                         STRING('GP Broking:'),AT(6563,1021),USE(?String19:40),TRN
                         STRING('Loadmaster:'),AT(6563,2000),USE(?String19:69),TRN
                         STRING(@n-14.2),AT(7927,2000),USE(L_SG:Loadmaster_DBN,,?L_SG:Loadmaster_DBN:2),RIGHT(1),TRN
                         STRING('Total Cost Broking:'),AT(6563,781),USE(?String19:62),TRN
                         STRING('GP % Overnight:'),AT(6563,2458),USE(?String19:46),TRN
                         STRING(@n-14.2),AT(7927,781,885,167),USE(L_SG:Transporter_Costs_DBN),RIGHT(1)
                         STRING('Turnover DBN:'),AT(135,167),USE(?String19),TRN
                         STRING(@n-14.2),AT(1406,167),USE(L_SG:Turnover_DBN),RIGHT(1)
                         STRING(@n-14.2),AT(3927,5792),USE(L_SG:Loadmaster_GP_Per_Day),RIGHT(1)
                         BOX,AT(52,1458,4844,833),USE(?Box5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(4896,833,885,573),USE(?Box4),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Turnover JHB:'),AT(135,646),USE(?String19:2),TRN
                         STRING(@n-14.2),AT(1406,646),USE(L_SG:Turnover_JHB),RIGHT(1)
                         BOX,AT(52,2344,2344,833),USE(?Box5:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@n-14.2),AT(1417,6177),USE(L_SG:Turnover_Per_Day),RIGHT(1)
                         STRING('Turnover Per Day:'),AT(135,6177),USE(?String19:31),TRN
                         STRING('Turnover Total:'),AT(2521,1135),USE(?String19:3),TRN
                         STRING('Turnover Per Day JHB:'),AT(2667,6448),USE(?String19:33),TRN
                         STRING('Gross GP DBN:'),AT(135,4042),USE(?String19:21),TRN
                         STRING(@n-14.2),AT(3927,1135),USE(L_SG:Turnover_Total),RIGHT(1)
                         STRING(@n-14.2),AT(3927,6198),USE(L_SG:Turnover_Per_Day_DBN),RIGHT(1)
                         BOX,AT(2552,2344,2344,833),USE(?Box5:6),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Credits DBN:'),AT(135,406),USE(?String19:4),TRN
                         STRING(@n-14.2),AT(1406,406),USE(L_SG:Credits_DBN),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1)
                         STRING(@n-14.2),AT(3927,6448),USE(L_SG:Turnover_Per_Day_JHB),RIGHT(1)
                         STRING('Gross GP JHB:'),AT(135,4281),USE(?String19:22),TRN
                         BOX,AT(52,5729,2344,302),USE(?Box5:10),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(2552,5729,2344,302),USE(?Box5:11),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Credits JHB:'),AT(135,885),USE(?String19:5),TRN
                         STRING(@n-14.2),AT(1406,885),USE(L_SG:Credits_JHB),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1)
                         STRING('Gross GP Total:'),AT(135,4531),USE(?String19:23),TRN
                         STRING('Credits Total:'),AT(135,1135),USE(?String19:6),TRN
                         BOX,AT(52,52,4844,1354),USE(?Box3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@n-14.2),AT(1406,1135),USE(L_SG:Credits_Total),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1)
                         BOX,AT(52,4844,2344,833),USE(?Box5:8),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(2552,4844,2344,833),USE(?Box5:9),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Turnover less Credits DBN:'),AT(2521,406),USE(?String19:7),TRN
                         STRING(@n-14.2),AT(3927,406),USE(L_SG:Turnover_less_Credits_DBN),RIGHT(1)
                         STRING('Loadmaster DBN:'),AT(135,4917),USE(?String19:25),TRN
                         STRING('Turnover less Credits JHB:'),AT(2521,885),USE(?String19:8),TRN
                         STRING(@n-14.2),AT(3927,885),USE(L_SG:Turnover_less_Credits_JHB),RIGHT(1)
                         BOX,AT(52,6094,2344,302),USE(?Box5:12),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(2552,6094,2344,573),USE(?Box5:13),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@n-14),AT(5031,1156,583,167),USE(L_SG:Work_Days),CENTER,TRN
                         STRING('Work Days:'),AT(5031,917),USE(?String19:9),TRN
                         STRING('Loadmaster Total:'),AT(135,5406),USE(?String19:27),TRN
                         STRING('Broking Kg DBN:'),AT(135,1542),USE(?String19:10),TRN
                         STRING(@n-11.0),AT(1594,1542),USE(L_SG:Broking_Kg_DBN),RIGHT(1)
                         STRING(@n-14.2),AT(1417,4042),USE(L_SG:Gross_GP_DBN),RIGHT(1)
                         STRING('Broking Kg JHB:'),AT(156,1771),USE(?String19:11),TRN
                         STRING(@n-11.0),AT(1594,1792),USE(L_SG:Broking_Kg_JHB),RIGHT(1)
                         STRING('Loadmaster + GP Total:'),AT(135,5792),USE(?String19:29),TRN
                         STRING(@n-14.2),AT(1417,4281),USE(L_SG:Gross_GP_JHB),RIGHT(1)
                         STRING('Broking Kg Total:'),AT(135,2052),USE(?String19:12),TRN
                         STRING('Loadmaster GP Per Day:'),AT(2667,5792),USE(?String19:30),TRN
                         STRING(@n-11.0),AT(1594,2052),USE(L_SG:Broking_Kg_Total),RIGHT(1)
                         STRING(@n-14.2),AT(1417,4531),USE(L_SG:Gross_GP_Total),RIGHT(1)
                         STRING('Overnight Kg DBN:'),AT(135,2438),USE(?String19:13),TRN
                         STRING(@n-11.0),AT(1594,2438),USE(L_SG:Overnight_Kg_DBN),RIGHT(1)
                         STRING(@n-14.2),AT(3927,4531),USE(L_SG:Gross_GP_Per_Day),RIGHT(1)
                         STRING('Gross GP Per Day:'),AT(2667,4531),USE(?String19:24),TRN
                         STRING(@n-11.0),AT(1594,2677),USE(L_SG:Overnight_Kg_JHB),RIGHT(1)
                         STRING(@n-14.2),AT(1417,4917),USE(L_SG:Loadmaster_DBN),RIGHT(1)
                         STRING('Kg Per Day DBN:'),AT(2656,2438),USE(?String19:18),TRN
                         STRING('Overnight Kg JHB:'),AT(135,2677),USE(?String19:14),TRN
                         STRING(@n-11.0),AT(4104,2438),USE(L_SG:Kg_Per_Day_DBN),RIGHT(1)
                         STRING(@n-11.0),AT(1594,2917),USE(L_SG:Overnight_Kg_Total),RIGHT(1)
                         STRING(@n-14.2),AT(1417,5156),USE(L_SG:Loadmaster_JHB),RIGHT(1)
                         STRING('Loadmaster JHB:'),AT(135,5156),USE(?String19:26),TRN
                         STRING('Overnight Kg Total:'),AT(135,2917),USE(?String19:15),TRN
                         STRING(@n-11.0),AT(4104,2677),USE(L_SG:Kg_Per_Day_JHB),RIGHT(1)
                         STRING(@n-11.0),AT(1583,3312),USE(L_SG:Kg_Total),RIGHT(1)
                         GROUP('Group - Hide'),AT(104,6667,4792,2083),USE(?Group1),BOXED,HIDE
                           STRING(@n-14.2),AT(885,7063),USE(L_SG:Extra_Leg_ON_JHB),RIGHT(1)
                           STRING('Sales Broking:'),AT(2542,7438),USE(?String19:35),TRN
                           STRING(@n-14.2),AT(3948,7438,833,167),USE(L_SG:Sales_Broking_JHB),RIGHT(1)
                           STRING('EL ON:'),AT(156,7063),USE(?String19:65),TRN
                           STRING('EL:'),AT(156,7271),USE(?String19:66),TRN
                           STRING(@n-14.2),AT(885,7271),USE(L_SG:Extra_Leg_JHB,,?L_SG:Extra_Leg_JHB:2),RIGHT(1)
                           STRING('Extra Legs:'),AT(2542,7656),USE(?String19:60)
                           STRING(@n-14.2),AT(3948,7656,833,167),USE(L_SG:Extra_Leg_JHB),RIGHT(1)
                           STRING('Sales Broking:'),AT(156,7490),USE(?String19:34),TRN
                           STRING(@n-14.2),AT(1563,7490),USE(L_SG:Sales_Broking_DBN),RIGHT(1)
                           STRING('Extra Legs:'),AT(156,7708,552,167),USE(?String19:59),TRN
                           STRING(@n-14.2),AT(1552,7708,833,167),USE(L_SG:Extra_Leg_DBN),RIGHT(1)
                           STRING('Costs Broking (incl. EL):'),AT(2542,7896),USE(?String19:39),TRN
                           STRING(@n-14.2),AT(3948,7896,833,167),USE(L_SG:Costs_Broking_JHB),RIGHT(1)
                           STRING('Costs Broking (incl. EL):'),AT(156,7917),USE(?String19:38),TRN
                           STRING(@n-14.2),AT(1552,7917,833,167),USE(L_SG:Costs_Broking_DBN),RIGHT(1)
                           STRING('Extra Transporter Invoices:'),AT(2542,8115),USE(?String19:54),TRN
                           STRING(@n-14.2),AT(3948,8115,833,167),USE(L_SG:Extra_Invoices_JHB),RIGHT(1)
                           STRING('Extra Transporter Invoices:'),AT(156,8115),USE(?String19:50),TRN
                           STRING(@n-14.2),AT(1552,8115,833,167),USE(L_SG:Extra_Invoices_DBN),RIGHT(1)
                           STRING('Transporter Credits:'),AT(2542,8323),USE(?String19:55),TRN
                           STRING(@n-14.2),AT(3948,8323,833,167),USE(L_SG:Transporter_Debits_JHB),RIGHT(1)
                           STRING('Transporter Credits:'),AT(156,8323,1219,167),USE(?String19:51),TRN
                           STRING(@n-14.2),AT(1563,8323,833,167),USE(L_SG:Transporter_Debits_DBN),RIGHT(1)
                           STRING('Transporter Debits:'),AT(2542,8521),USE(?String19:53),TRN
                           STRING(@n-14.2),AT(3948,8521,833,167),USE(L_SG:Transporter_Credits_JHB),FONT(,,COLOR:Red,, |
  CHARSET:ANSI),RIGHT(1)
                           STRING('Transporter Debits:'),AT(156,8531,958,167),USE(?String19:52),TRN
                           STRING(@n-14.2),AT(1563,8531,833,167),USE(L_SG:Transporter_Credits_DBN),FONT(,,COLOR:Red,, |
  CHARSET:ANSI),RIGHT(1)
                           STRING('EL Broking:'),AT(156,6844),USE(?String19:64),TRN
                           STRING(@n-14.2),AT(885,6844),USE(L_SG:Extra_Leg_Broking_JHB),RIGHT(1)
                         END
                         BOX,AT(52,3229,4844,300),USE(?Box5:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Kg Per Day JHB:'),AT(2656,2677),USE(?String19:19),TRN
                         STRING('Turnover Per Day DBN:'),AT(2667,6198),USE(?String19:32),TRN
                         STRING('Kg Total:'),AT(135,3313),USE(?String19:16),TRN
                         STRING(@n-14.2),AT(1417,5406),USE(L_SG:Loadmaster_Total),RIGHT(1)
                         STRING(@n-10.2),AT(1656,3656),USE(L_SG:Average_Cost_Per_Kg),RIGHT(1)
                         BOX,AT(52,3594,4844,302),USE(?Box5:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,3958,2344,833),USE(?Box5:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(2552,3958,2344,833),USE(?Box5:7),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@n-11.0),AT(4104,2917),USE(L_SG:Kg_Per_Day),RIGHT(1)
                         STRING('Kg Per Day Total:'),AT(2667,2917),USE(?String19:17),TRN
                         STRING(@n-14.2),AT(3927,5406),USE(L_SG:Loadmaster_Per_Day),RIGHT(1)
                         STRING('Loadmaster Per Day:'),AT(2667,5406),USE(?String19:28),TRN
                         STRING('Avg. Cent Cost Per Kg:'),AT(135,3656),USE(?String19:20),TRN
                         STRING(@n-14.2),AT(1417,5792),USE(L_SG:Loadmaster_GP_Total),RIGHT(1)
                       END
                       FOOTER,AT(250,7813,9302,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(8365,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ReportMemoryRecords     BYTE(0)                            ! Used to do the first Next call
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Next                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END

TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

Turnover_Summary        CLASS,TYPE

Get_Inv_Tots        PROCEDURE
    .



Calc        Turnover_Summary


sql_        SQLQueryClass
sql_2       SQLQueryClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Audit_Check             ROUTINE
    DATA

R:Date      DATE
R:Log       BYTE
R:Count     LONG
R:Dates     STRING(255)

    CODE
    R:Log   = L_SG:Output
    LOOP 2 TIMES
       R:Count  = 0
       R:Dates  = ''

       IF R:Log = 1
          ! Audit is for all - run query to show all DIs being included in the above date range that fall on a weekend or public holiday
          Add_Log('[DIs on Weekends and Public Holidays]', 'Print_Turnover_Summary', 'Print Turnover Summary - Holiday DIs - ' & FORMAT(TODAY(), @d7) & '.csv',, 1)
       .

       sql_.PropSQL('SELECT DISTINCT LTRIM(DATEPART([year], INVOICEDATEANDTIME)) + <39>/<39> + LTRIM(DATEPART([month], INVOICEDATEANDTIME)) + ' & |
                    '<39>/<39> + LTRIM(DATEPART([day], INVOICEDATEANDTIME)) ' & |
                    'FROM _Invoice WHERE InvoiceDateAndTime >= ' & |
                   SQL_Get_DateT_G(LO:From_Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(LO:To_Date + 1,,1))
       LOOP
          IF sql_.Next_Q() <= 0
             BREAK
          .

          R:Date        = DEFORMAT(sql_.Data_G.F1, @d10)

          ! Weekends and public holidays
          IF Check_PublicHoliday(R:Date) > 0 OR R:Date % 7 = 0 OR R:Date % 7 = 6
             R:Dates    = CLIP(R:Dates) & ', ' & FORMAT(R:Date,@d6)

             ! Output all these DI No.
             sql_2.PropSQL('SELECT DINo, Total FROM _Invoice WHERE InvoiceDateAndTime >= ' & |
                         SQL_Get_DateT_G(R:Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(R:Date + 1,,1))

             LOOP
                IF sql_2.Next_Q() <= 0
                   BREAK
                .

                R:Count += 1

                IF R:Log = 1
                   ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
                   Add_Log('Public Holiday / WEnd - DI No.: ' & CLIP(sql_2.Data_G.F1) & ', Date: ' & FORMAT(R:Date, @d6) & ', Amt: ' & CLIP(sql_2.Data_G.F2), |
                           'Print_Turnover_Summary', 'Print Turnover Summary - Holiday DIs - ' & FORMAT(TODAY(), @d7) & '.csv',, 1)
       .  .  .  .

       IF R:Count > 0 AND R:Log = 0
          CASE MESSAGE('There are ' & R:Count & ' DIs with a Public Holiday or Weekend Date.||Dates: ' & CLIP(R:Dates) & '||Would you like to output these to a log file?', 'Print Turnover Summary - DI Date Check', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
          OF BUTTON:Yes
             ! Ok
          OF BUTTON:No
             BREAK
          .
       ELSIF R:Log = 1 AND R:Count > 0
          CASE MESSAGE('There are ' & R:Count & ' DIs with a Public Holiday or Weekend Date.||Dates: ' & CLIP(R:Dates) & '||A log of these was created: ' & 'Print Turnover Summary - Holiday DIs - ' & FORMAT(TODAY(), @d7) & '.csv||Would you like to view this now?', 'Print Turnover Summary - DI Date Check', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
          OF BUTTON:Yes
             ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Print Turnover Summary - Holiday DIs - ' & FORMAT(TODAY(), @d7) & '.csv')
          .
          BREAK
    .  .
    EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      IF L_SG:Output > 0
         CASE MESSAGE('Would you like to load the accumulation details now?|Note that if the file does not load automatically you can find it at the location shown below.||File: ' & PATH() & '\Print Turnover Summary - ' & FORMAT(TODAY(), @d7) & '.csv', 'Print Turnover Summary', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Print Turnover Summary - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  
  PARENT.AskPreview
      ! Doesn't work, using Tin Tools
  
  !  LOOP
  !     CASE MESSAGE('Print another copy?', 'Print', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
  !     OF BUTTON:Yes
  !        SELF.PrintReport()
  !     ELSE
  !        BREAK
  !  .  .
  !


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_ManagementProfit_Summary')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?L_SG:ExcludeBadDebts
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:Setup.Open                                        ! File Setup used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceTransporter.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF NOT TargetSelector.ASK(1) THEN
     SELF.Kill()
     RETURN Level:Fatal
  END
  IF NOT TargetSelector.GetPrintSelected() THEN
     SELF.SetReportTarget(TargetSelector.GetReportSelected())
  END
  SELF.AddItem(TargetSelector)
  !    IF NOT TargetSelector.GetPrintSelected() THEN
  !       L_SG:PDF  = TRUE
  !    . 
      
  ThisReport.Init(Process:View, Relate:Setup, ?Progress:PctText, Progress:Thermometer, 0)
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  FDB4.Init(?L_SG:BranchName1,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(BRA:Key_BranchName)
  FDB4.AddField(BRA:BranchName,FDB4.Q.BRA:BranchName) !List box control field - type derived from field
  FDB4.AddField(BRA:BID,FDB4.Q.BRA:BID) !Primary key field - type derived from field
  FDB4.AddUpdateField(BRA:BID,L_SG:Branch1)
  ThisWindow.AddItem(FDB4.WindowComponent)
  FDB4.DefaultFill = 0
  FDB7.Init(?L_SG:BranchName2,Queue:FileDrop:1.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop:1,Relate:Branches,ThisWindow)
  FDB7.Q &= Queue:FileDrop:1
  FDB7.AddSortOrder(BRA:Key_BranchName)
  FDB7.AddField(BRA:BranchName,FDB7.Q.BRA:BranchName) !List box control field - type derived from field
  FDB7.AddField(BRA:BID,FDB7.Q.BRA:BID) !Primary key field - type derived from field
  FDB7.AddUpdateField(BRA:BID,L_SG:Branch2)
  ThisWindow.AddItem(FDB7.WindowComponent)
  FDB7.DefaultFill = 0
      LO:From_Date    = GETINI('Print_Turnover', 'From_Date', , GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Turnover', 'To_Date', , GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      PUTINI('Print_Turnover', 'From_Date', LO:From_Date, GLO:Local_INI)
      PUTINI('Print_Turnover', 'To_Date', LO:To_Date, GLO:Local_INI)
  
      
      sql_.Init(GLO:DBOwner, '_SQLTemp2')
      sql_2.Init(GLO:DBOwner, '_SQLTemp2')
      
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:Setup.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Next PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    IF ReportMemoryRecords=0 THEN
       ReportMemoryRecords+=1
       RETURN Level:Benign
    ELSE
       SELF.Response = RequestCompleted
       POST(EVENT:CloseWindow)
       RETURN Level:Notify
    END
  ReturnValue = PARENT.Next()
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
          SELECT(?Tab1)
          DISABLE(?Pause)
          DISPLAY
          IF L_SG:Output > 0
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             LOC:Transaction_Log_Name   = 'Print Turnover Summary - ' & FORMAT(TODAY(), @d7) & '.csv'
             Add_Log('Start', 'Print_Turnover_Summary', LOC:Transaction_Log_Name, TRUE, 1)
      
      
      
             CASE MESSAGE('Would you like to empty the Audit Management Profit table?','Print Summary', ICON:Question, 'All|Summary|None', 1)
             OF 1
                _SQLTemp{PROP:SQL}    = 'DELETE FROM Audit_ManagementProfit'
             OF 2
                _SQLTemp{PROP:SQL}    = 'DELETE FROM Audit_ManagementProfit WHERE source = 2'
          .  .
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      Calc.Get_Inv_Tots()
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          CLEAR(BRA:Record, -1)
          SET(BRA:PKey_BID)
          NEXT(Branches)
          L_SG:Branch1        = BRA:BID
          L_SG:BranchName1    = BRA:BranchName
      
          NEXT(Branches)
          L_SG:Branch2        = BRA:BID
          L_SG:BranchName2    = BRA:BranchName
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Turnover_Summary.Get_Inv_Tots        PROCEDURE
R:Type            BYTE
R:VAT             BYTE

    CODE
    ! Loop through the Invoice file and gather all information
    ! Flaw in this code is that it can only deal with 2 branches.

    ! Get_Invoices
    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info  , p:LogName)
    !   1           2       3           4          5       6          7        8            9                 10        11          12
    ! (LONG  , BYTE    , BYTE=0, BYTE=0        , ULONG=0, <LONG>  , BYTE=0  , BYTE=0    , BYTE=0           , BYTE=0  , <STRING>, <STRING>),STRING
    ! p:Option
    !   0.  - Total Charges inc.
    !   1.  - Excl VAT
    !   2.  - VAT
    !   3.  - Kgs
    ! p:Type
    !   0.  - All
    !   1.  - Invoices                                  Total >= 0.0
    !   2.  - Credit Notes                              Total < 0.0    Instead of <, should be invoices with A_INV:CR_IID
    !   3.  - Credit Notes excl Bad Debts               Total < 0.0
    !   4.  - Credit Notes excl Journals                Total < 0.0
    !   5.  - Credit Notes excl Bad Debts & Journals    Total < 0.0
    ! p:Limit_On    &    p:ID
    !   0.  - Date [& Branch]
    !   1.  - MID
    ! p:Manifest_Option
    !   0.  - None
    !   1.  - Overnight
    !   2.  - Broking
    ! p:Not_Manifest
    !   0.  - All
    !   1.  - Not Manifest
    !   2.  - Manifest

    R:Type      = 2
    IF L_SG:ExcludeBadDebts = TRUE
       R:Type   = 3
    .
    R:VAT = 0
    L_SG:VAT_Msg = 'Inclusive of VAT'
    IF L_SG:VAT = 0   ! Exclusive of
      R:VAT = 1
      L_SG:VAT_Msg = 'Exclusive of VAT'
    .

    ! Total inc., Invoices, filter on dates, overnight
    L_SG:Sales_Overnight_DBN    = Get_Invoices(LO:From_Date, R:VAT, 1, 2, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 1, 2, 'I-ON', LOC:Transaction_Log_Name)       ! Overnight
    L_SG:Sales_Overnight_DBN   += Get_Invoices(LO:From_Date, R:VAT, 1, 1, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 1, 2, 'I-EI-O', LOC:Transaction_Log_Name)       ! Overnight
    L_SG:Sales_Overnight_JHB    = Get_Invoices(LO:From_Date, R:VAT, 1, 2, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 1, 2, 'I-ON', LOC:Transaction_Log_Name)       ! Overnight
    L_SG:Sales_Overnight_JHB   += Get_Invoices(LO:From_Date, R:VAT, 1, 1, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 1, 2, 'I-EI-O', LOC:Transaction_Log_Name)       ! Overnight

    L_SG:Sales_Broking_DBN      = Get_Invoices(LO:From_Date, R:VAT, 1, 2, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 2, 2, 'I-BR', LOC:Transaction_Log_Name)       ! Broking
    L_SG:Sales_Broking_DBN     += Get_Invoices(LO:From_Date, R:VAT, 1, 1, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 2, 2, 'I-EI-B', LOC:Transaction_Log_Name)       ! Broking
    L_SG:Sales_Broking_JHB      = Get_Invoices(LO:From_Date, R:VAT, 1, 2, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 2, 2, 'I-BR', LOC:Transaction_Log_Name)       ! Broking
    L_SG:Sales_Broking_JHB     += Get_Invoices(LO:From_Date, R:VAT, 1, 1, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 2, 2, 'I-EI-B', LOC:Transaction_Log_Name)       ! Broking
                                                                          
	  ! Get_InvoicesTransporter
    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type, p:Output, p:Source)
    !   1           2       3         4               5           6        7         8          9           10              11     , 12
    ! p:Option
    !   0.  - Total Charges inc.
    !   1.  - Excl VAT
    !   2.  - VAT
    ! p:Type
    !   0.  - All
    !   1.  - Invoices                      Total >= 0.0
    !   2.  - Credit Notes                  Total < 0.0
    ! p:DeliveryLegs
    !   0.  - Both
    !   1.  - Del Legs only
    !   2.  - No Del Legs
    ! p:MID
    !   0   = Not for a MID
    !   x   = for a MID
    ! p:Invoice_Type (was Manifest_Type)
    !   0   - All
    !   1   - Overnight
    !   2   - Broking
    ! p:Manifest_Type
    !   0   - All
    !   1   - Overnight
    !   2   - Broking
    ! p:Extra_Inv
    !   Only
    ! p:Source
    !   1   - Management Profit
    !   2   - Management Profit Summary


    L_SG:Extra_Invoices_DBN      = Get_InvoicesTransporter(LO:From_Date, R:VAT, 1, FALSE,,, L_SG:Branch1, LO:To_Date,TRUE,, L_SG:Output,2,'T-EI', LOC:Transaction_Log_Name)    ! Credits for day
    L_SG:Extra_Invoices_JHB      = Get_InvoicesTransporter(LO:From_Date, R:VAT, 1, FALSE,,, L_SG:Branch2, LO:To_Date,TRUE,, L_SG:Output,2,'T-EI', LOC:Transaction_Log_Name)    ! Credits for day

    L_SG:Transporter_Debits_DBN  = Get_InvoicesTransporter(LO:From_Date, R:VAT, 1, TRUE,,, L_SG:Branch1, LO:To_Date,,, L_SG:Output,2,'T-TD', LOC:Transaction_Log_Name)         ! Credits for day - Transporter not MID
    L_SG:Transporter_Debits_JHB  = Get_InvoicesTransporter(LO:From_Date, R:VAT, 1, TRUE,,, L_SG:Branch2, LO:To_Date,,, L_SG:Output,2,'T-TD', LOC:Transaction_Log_Name)         ! Credits for day - Transporter not MID

    L_SG:Transporter_Credits_DBN = -Get_InvoicesTransporter(LO:From_Date, R:VAT, 2,,,, L_SG:Branch1, LO:To_Date,,, L_SG:Output,2,'T-TC', LOC:Transaction_Log_Name)
    L_SG:Transporter_Credits_JHB = -Get_InvoicesTransporter(LO:From_Date, R:VAT, 2,,,, L_SG:Branch2, LO:To_Date,,, L_SG:Output,2,'T-TC', LOC:Transaction_Log_Name)

    ! All extra legs are broking but some are from broking manifests and some are from loadmaster manifests
    L_SG:Extra_Leg_ON_DBN          = Get_InvoicesTransporter(LO:From_Date, R:VAT, 0,,1,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2, 'T-EL', LOC:Transaction_Log_Name, 1)
    ! Turnover - Invoices (p3 = 1), Del Leg Only (p5 = 1), Broking (p10 = 2)
    ! Invoices & CNs (p3 = 0), Del Leg Only (p5 = 1), Broking Invoice (p10 = 2), Broking Manifest (p15 = 2)
    L_SG:Extra_Leg_Broking_DBN     = Get_InvoicesTransporter(LO:From_Date, R:VAT, 0,,1,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2, 'T-EL', LOC:Transaction_Log_Name, 2)
    L_SG:Extra_Leg_ON_JHB          = Get_InvoicesTransporter(LO:From_Date, R:VAT, 0,,1,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2, 'T-EL', LOC:Transaction_Log_Name, 1)
    L_SG:Extra_Leg_Broking_JHB     = Get_InvoicesTransporter(LO:From_Date, R:VAT, 0,,1,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2, 'T-EL', LOC:Transaction_Log_Name, 2)

    L_SG:Extra_Leg_DBN             = L_SG:Extra_Leg_ON_DBN + L_SG:Extra_Leg_Broking_DBN
    L_SG:Extra_Leg_JHB             = L_SG:Extra_Leg_ON_JHB + L_SG:Extra_Leg_Broking_JHB

    L_SG:Sales_Broking_DBN        -= L_SG:Extra_Leg_Broking_DBN     ! 22 Aug - take off broking extra legs, ON are not included
    L_SG:Sales_Broking_JHB        -= L_SG:Extra_Leg_Broking_JHB

    L_SG:Sales_Broking_DBN_Total   = L_SG:Extra_Leg_DBN + L_SG:Sales_Broking_DBN
    L_SG:Sales_Broking_JHB_Total   = L_SG:Extra_Leg_JHB + L_SG:Sales_Broking_JHB

    L_SG:Sales_Overnight_DBN      -= L_SG:Extra_Leg_ON_DBN        ! Only remove the overnight extra legs
    L_SG:Sales_Overnight_JHB      -= L_SG:Extra_Leg_ON_JHB

!    L_SG:Sales_Broking_DBN_Total     += L_SG:Extra_Invoices_DBN + L_SG:Transporter_Credits_DBN - L_SG:Transporter_Debits_DBN
!    L_SG:Sales_Broking_JHB_Total     += L_SG:Extra_Invoices_JHB + L_SG:Transporter_Credits_JHB - L_SG:Transporter_Debits_JHB
!    L_SG:Sales_Broking_DBN_Total     += L_SG:Transporter_Credits_DBN - L_SG:Transporter_Debits_DBN
!    L_SG:Sales_Broking_JHB_Total     += L_SG:Transporter_Credits_JHB - L_SG:Transporter_Debits_JHB

    L_SG:Credits_DBN            = Get_Invoices(LO:From_Date, R:VAT, R:Type,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 0, 2,'I-CR', LOC:Transaction_Log_Name)    ! total
    L_SG:Credits_JHB            = Get_Invoices(LO:From_Date, R:VAT, R:Type,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 0, 2,'I-CR', LOC:Transaction_Log_Name)    ! total

    L_SG:Sales_Overnight_DBN   += L_SG:Credits_DBN       ! neg - excluding credits
    L_SG:Sales_Overnight_JHB   += L_SG:Credits_JHB       ! neg - excluding credits

    L_SG:Credits_Total          = L_SG:Credits_DBN + L_SG:Credits_JHB

    L_SG:Turnover_DBN           = L_SG:Sales_Broking_DBN_Total + L_SG:Sales_Overnight_DBN  - L_SG:Credits_DBN
    L_SG:Turnover_JHB           = L_SG:Sales_Broking_JHB_Total + L_SG:Sales_Overnight_JHB  - L_SG:Credits_JHB

    L_SG:Turnover_less_Credits_DBN = L_SG:Turnover_DBN + L_SG:Credits_DBN
    L_SG:Turnover_less_Credits_JHB = L_SG:Turnover_JHB + L_SG:Credits_JHB

    L_SG:Turnover_Total         = L_SG:Turnover_less_Credits_DBN + L_SG:Turnover_less_Credits_JHB

    ! Not of invoiced days (working days) - of course will be wrong if people invoice in weekend or public holiday...
    sql_.PropSQL('SELECT COUNT(DISTINCT LTRIM(DATEPART([year], INVOICEDATEANDTIME)) + <39>/<39> + LTRIM(DATEPART([month], INVOICEDATEANDTIME)) + ' & |
                 '<39>/<39> + LTRIM(DATEPART([day], INVOICEDATEANDTIME))) ' & |
                 'FROM _Invoice WHERE InvoiceDateAndTime >= ' & |
                SQL_Get_DateT_G(LO:From_Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(LO:To_Date + 1,,1))
    IF sql_.Next_Q() > 0
       L_SG:Work_Days           = sql_.Data_G.F1
    .
    DO Audit_Check              ! Checks and outputs DIs on WEnd or Public holidays


    L_SG:Broking_Kg_DBN         = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 2, 2,, LOC:Transaction_Log_Name)       ! Broking
    L_SG:Broking_Kg_JHB         = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 2, 2,, LOC:Transaction_Log_Name)       ! Broking
    L_SG:Broking_Kg_Total       = L_SG:Broking_Kg_DBN + L_SG:Broking_Kg_JHB

    L_SG:Overnight_Kg_DBN       = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 1, 2,, LOC:Transaction_Log_Name)       ! Broking
    L_SG:Overnight_Kg_JHB       = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 1, 2,, LOC:Transaction_Log_Name)       ! Broking
    L_SG:Overnight_Kg_Total     = L_SG:Overnight_Kg_DBN + L_SG:Overnight_Kg_JHB

    L_SG:Kg_Total               = L_SG:Broking_Kg_Total + L_SG:Overnight_Kg_Total

    L_SG:Kg_Per_Day             = L_SG:Kg_Total / L_SG:Work_Days
    L_SG:Kg_Per_Day_DBN         = (L_SG:Broking_Kg_DBN + L_SG:Overnight_Kg_DBN) / L_SG:Work_Days
    L_SG:Kg_Per_Day_JHB         = (L_SG:Broking_Kg_JHB + L_SG:Overnight_Kg_JHB) / L_SG:Work_Days


    ! Get_InvoicesTransporter
    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type,
    ! (LONG  , BYTE    , BYTE=0, BYTE=0        , BYTE=0        , <ULONG>, ULONG=0, <LONG>  , BYTE=0     , BYTE=0        ,
    !   1           2       3         4               5           6        7         8          9           10
    !  p:Output, p:Source, p:Info  , p:LogName, p:Manifest_Type)
    !  BYTE=0  , BYTE=0  , <STRING>, <STRING> , BYTE=0 ),STRING
    !    11        12        13        14         15
    ! p:Option                                                  2
    !   0.  - Total Charges inc.
    !   1.  - Excl VAT
    !   2.  - VAT
    ! p:Type                                                    3
    !   0.  - All
    !   1.  - Invoices                      Total >= 0.0
    !   2.  - Credit Notes                  Total < 0.0
    ! p:DeliveryLegs                                            5
    !   0.  - Both
    !   1.  - Del Legs only
    !   2.  - No Del Legs
    ! p:MID                                                     6
    !   0   = Not for a MID
    !   x   = for a MID
    ! p:Invoice_Type (was Manifest_Type)                        10
    !   0   - All
    !   1   - Overnight
    !   2   - Broking
    ! p:Manifest_Type                                           15
    !   0   - All
    !   1   - Overnight
    !   2   - Broking
    ! p:Extra_Inv                                               9
    !   Only
    ! p:Source                                                  12
    !   1   - Management Profit
    !   2   - Management Profit Summary

    ! Delivery legs (1), Broking Manifest type
!   L_SG:Sales_Broking_DBN     += Get_InvoicesTransporter(LO:From_Date, R:VAT, 0,,1,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2)
!   L_SG:Sales_Broking_JHB     += Get_InvoicesTransporter(LO:From_Date, R:VAT, 0,,1,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2)

    ! Turnover - Invoices (p3 = 1), Broking (p10 = 2), Legs & Other (p5 = 0)

    ! Invoices & Credits (p3 = 0), No Del Legs (p5 = 2), Broking (p10 = 2)
    L_SG:Costs_Broking_DBN      = Get_InvoicesTransporter(LO:From_Date, R:VAT, 0,,2,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2,'T-CB', LOC:Transaction_Log_Name)
!    L_SG:Costs_Broking_DBN     += Get_InvoicesTransporter(LO:From_Date, R:VAT, 0,,2,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2,'T-CB', LOC:Transaction_Log_Name)
    L_SG:Costs_Broking_JHB      = Get_InvoicesTransporter(LO:From_Date, R:VAT, 0,,2,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2,'T-CB', LOC:Transaction_Log_Name)
!    L_SG:Costs_Broking_JHB     += Get_InvoicesTransporter(LO:From_Date, R:VAT, 0,,2,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2,'T-CB', LOC:Transaction_Log_Name)

    L_SG:Costs_Broking_DBN     += L_SG:Extra_Leg_DBN
    L_SG:Costs_Broking_JHB     += L_SG:Extra_Leg_JHB

    ! 4 July 12 - Yvonne noticed double credit applied here, above we are getting Costs_Broking including ALL, so we dont need to aagin deduct the credit here
    !            in theory anyway.  So next 2 lines changed
!    L_SG:Transporter_Costs_DBN  = L_SG:Costs_Broking_DBN + L_SG:Extra_Invoices_DBN + L_SG:Transporter_Debits_DBN - L_SG:Transporter_Credits_DBN
!    L_SG:Transporter_Costs_JHB  = L_SG:Costs_Broking_JHB + L_SG:Extra_Invoices_JHB + L_SG:Transporter_Debits_JHB - L_SG:Transporter_Credits_JHB
    L_SG:Transporter_Costs_DBN  = L_SG:Costs_Broking_DBN + L_SG:Extra_Invoices_DBN + L_SG:Transporter_Debits_DBN 
    L_SG:Transporter_Costs_JHB  = L_SG:Costs_Broking_JHB + L_SG:Extra_Invoices_JHB + L_SG:Transporter_Debits_JHB 

    ! Overnight Manifest type
    ! 27 May 13 - changed from Get_InvoicesTransporter(LO:From_Date, R:VAT, 1  to include Cnotes as well with 0
    L_SG:Loadmaster_DBN         = Get_InvoicesTransporter(LO:From_Date, R:VAT, 0,,,, L_SG:Branch1, LO:To_Date,, 1, L_SG:Output, 2,'T-CO', LOC:Transaction_Log_Name)
    L_SG:Loadmaster_JHB         = Get_InvoicesTransporter(LO:From_Date, R:VAT, 0,,,, L_SG:Branch2, LO:To_Date,, 1, L_SG:Output, 2,'T-CO', LOC:Transaction_Log_Name)



    L_SG:Average_Cost_Per_Kg    = L_SG:Turnover_Total / L_SG:Kg_Total
            !(L_SG:Costs_Broking_DBN + L_SG:Costs_Broking_JHB + L_SG:Loadmaster_DBN + L_SG:Loadmaster_JHB) / L_SG:Kg_Total

    !L_SG:Turnover_Per_Day       = L_SG:Turnover_Total / L_SG:Work_Days
    L_SG:Turnover_Per_Day       = (L_SG:Turnover_less_Credits_DBN + L_SG:Turnover_less_Credits_JHB) / L_SG:Work_Days
    L_SG:Turnover_Per_Day_DBN   = L_SG:Turnover_less_Credits_DBN / L_SG:Work_Days
    L_SG:Turnover_Per_Day_JHB   = L_SG:Turnover_less_Credits_JHB / L_SG:Work_Days

    L_SG:GP_Broking_DBN         = L_SG:Sales_Broking_DBN_Total - L_SG:Transporter_Costs_DBN
    L_SG:GP_Broking_JHB         = L_SG:Sales_Broking_JHB_Total - L_SG:Transporter_Costs_JHB

    L_SG:GP_Per_Broking_DBN     = (L_SG:GP_Broking_DBN / L_SG:Sales_Broking_DBN_Total) * 100
    L_SG:GP_Per_Broking_JHB     = (L_SG:GP_Broking_JHB / L_SG:Sales_Broking_JHB_Total) * 100

    L_SG:GP_Overnight_DBN       = L_SG:Sales_Overnight_DBN - L_SG:Loadmaster_DBN
    !L_SG:GP_Overnight_DBN      += L_SG:Extra_Invoices_DBN + L_SG:Transporter_Credits_DBN - L_SG:Transporter_Debits_DBN
    L_SG:GP_Overnight_JHB       = L_SG:Sales_Overnight_JHB - L_SG:Loadmaster_JHB
    !L_SG:GP_Overnight_JHB      += L_SG:Extra_Invoices_JHB + L_SG:Transporter_Credits_JHB - L_SG:Transporter_Debits_JHB

    L_SG:GP_Per_Overnight_DBN   = (L_SG:GP_Overnight_DBN / L_SG:Sales_Overnight_DBN) * 100
    L_SG:GP_Per_Overnight_JHB   = (L_SG:GP_Overnight_JHB / L_SG:Sales_Overnight_JHB) * 100


!    L_SG:Gross_GP_DBN           = L_SG:Turnover_less_Credits_DBN - (L_SG:Costs_Broking_DBN + L_SG:Loadmaster_DBN)
!    L_SG:Gross_GP_JHB           = L_SG:Turnover_less_Credits_JHB - (L_SG:Costs_Broking_JHB + L_SG:Loadmaster_JHB)

    L_SG:Gross_GP_DBN           = L_SG:GP_Overnight_DBN + L_SG:GP_Broking_DBN
    L_SG:Gross_GP_JHB           = L_SG:GP_Overnight_JHB + L_SG:GP_Broking_JHB

    L_SG:Gross_GP_Total         = L_SG:Gross_GP_DBN + L_SG:Gross_GP_JHB
    L_SG:Gross_GP_Per_Day       = L_SG:Gross_GP_Total / L_SG:Work_Days


    L_SG:Loadmaster_Total       = L_SG:Loadmaster_DBN + L_SG:Loadmaster_JHB
    L_SG:Loadmaster_Per_Day     = L_SG:Loadmaster_Total / L_SG:Work_Days

    L_SG:Loadmaster_GP_Total    = L_SG:Loadmaster_Total + L_SG:Gross_GP_Total   ! Note loadmaster is not seen as a cost here but an income
    L_SG:Loadmaster_GP_Per_Day  = L_SG:Loadmaster_GP_Total / L_SG:Work_Days
    RETURN




ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
      IF LOC:Errors > 0
         MESSAGE('Completed with Errors: ' & LOC:Errors & '||Please review the System Log.', 'Print Turnover', ICON:Exclamation)
      .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_ManagementProfit_Summary','Print_ManagementProfit_Summary','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! (Manifest based)  Management Profit Analysis report
!!! </summary>
Print_Turnover PROCEDURE 

!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
TintRptViewer  TinReportViewer
!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
Progress:Thermometer BYTE                                  !
LOC:Errors           ULONG                                 !
LOC:Settings         GROUP,PRE(L_SG)                       !
VAT                  BYTE                                  !
BID                  ULONG                                 !Branch ID
BranchName           STRING(35)                            !Branch Name
ExcludeBadDebts      BYTE(1)                               !Exclude Bad Debt Credit Notes
Output               BYTE                                  !Output trail of items
Print_DetailInfo     BYTE                                  !
PDF                  BYTE                                  !
ManifestedState      BYTE(1)                               !State of this manifest
                     END                                   !
LOC:Values           GROUP,PRE(L_VG)                       !
Kgs_Broking          DECIMAL(7)                            !
Kgs_O_Night          DECIMAL(7)                            !
T_O_Broking          DECIMAL(10,2)                         !
T_O_O_Night          DECIMAL(10,2)                         !
C_Note               DECIMAL(10,2)                         !
Total_T_O            DECIMAL(10,2)                         !
Loadmaster           DECIMAL(10,2)                         !
Trans_Broking        DECIMAL(10,2)                         !
Extra_Inv            DECIMAL(10,2)                         !
Trans_Credit         DECIMAL(10,2)                         !
Total_Cost           DECIMAL(10,2)                         !
Trans_Debit          DECIMAL(10,2)                         !
Total_Cost_Less_Deb  DECIMAL(10,2)                         !
GP                   DECIMAL(10)                           !
GP_Per               DECIMAL(10,2)                         !
MID                  ULONG                                 !Manifest ID
CreatedDate          DATE                                  !Created Date
Ext_Invocie_TO       DECIMAL(12,2)                         !Turn over extra invoice
C_Note_CRIIDs        STRING(2001)                          !
C_Note_InvWeight     DECIMAL(16,4)                         !
C_Note_InvTot        DECIMAL(14,2)                         !
C_Note_Cred_Per      DECIMAL(14,2)                         !
DetailInfo           GROUP,PRE(L_VGDI)                     !
DetailInfo_Print     BYTE                                  !
Line1                STRING(500)                           !
Line2                STRING(500)                           !
Line3                STRING(500)                           !
                     END                                   !
                     END                                   !
Totals_Group         GROUP,PRE(L_TG)                       !
GP                   DECIMAL(12,2)                         !
GP_Per               DECIMAL(7,2)                          !
Total_TO             DECIMAL(12,2)                         !
Total_Cost           DECIMAL(12,2)                         !
                     END                                   !
LOC:Dates_Done       QUEUE,PRE(L_DQ)                       !
Date                 DATE                                  !
                     END                                   !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
LOC:Cur_Date         DATE                                  !
LOC:Report_Values    GROUP,PRE(L_RG)                       !
VAT_Option           STRING(20)                            !
                     END                                   !
Process:View         VIEW(Manifest)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:Broking)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:CreatedDate)
                       PROJECT(MAN:State)
                       PROJECT(MAN:VATRate)
                     END
FDB4::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?L_SG:BranchName
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
ProgressWindow       WINDOW('Report Turnover (Manifest)'),AT(,,267,173),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,MDI,TIMER(1)
                       SHEET,AT(6,2,259,151),USE(?Sheet1)
                         TAB('Settings'),USE(?Tab2)
                           PROMPT('Branch:'),AT(26,43),USE(?Prompt2)
                           PROMPT('VAT:'),AT(26,23),USE(?L_SG:VAT:Prompt)
                           LIST,AT(57,23,60,10),USE(L_SG:VAT),DROP(5),FROM('Excluding|#0|Including|#1')
                           LIST,AT(57,43,105,10),USE(L_SG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                           CHECK(' Exclude Bad Debts'),AT(57,62),USE(L_SG:ExcludeBadDebts),MSG('Exclude Bad Debt C' & |
  'redit Notes'),TIP('Exclude Bad Debt Credit Notes')
                           PROMPT('Output:'),AT(26,85),USE(?L_SG:Output_Trail:Prompt)
                           LIST,AT(57,85,60,10),USE(L_SG:Output),VSCROLL,DROP(5),FROM('None|#0|All|#1'),MSG('Output tra' & |
  'il of items'),TIP('Output trail of items')
                           PROMPT('State:'),AT(26,112),USE(?L_SG:ManifestedState:Prompt)
                           LIST,AT(57,112,105,10),USE(L_SG:ManifestedState),DROP(5),FROM('Loading|#0|Loaded|#1|On ' & |
  'Route|#2|Transferred|#3'),MSG('State of the manifests to show'),TIP('State of the ma' & |
  'nifests to show')
                         END
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(33,75,201,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(20,22,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(20,50,141,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Cancel'),AT(214,156,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                       BUTTON('Pause'),AT(160,156,49,15),USE(?Pause),LEFT,ICON('Blue_R.ico'),FLAT
                     END

Report               REPORT('Manifest Report'),AT(250,958,10604,6854),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,10604,698),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Management Profit Report'),AT(4198,10),USE(?ReportTitle),FONT(,12,,FONT:bold),CENTER
                         STRING('Branch:'),AT(7927,42),USE(?String57),TRN
                         STRING(@s35),AT(8396,42),USE(L_SG:BranchName)
                         STRING(@s20),AT(104,42),USE(L_RG:VAT_Option)
                         BOX,AT(0,469,10573,260),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(0,260,10573,219),USE(?HeaderBox:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Total'),AT(8750,292,833,167),USE(?HeaderTitle:15),CENTER,TRN
                         STRING('GP %'),AT(9635,500,833,167),USE(?HeaderTitle:17),CENTER,TRN
                         STRING('Trans Dr.'),AT(7969,292,833,167),USE(?HeaderTitle:14),CENTER,TRN
                         STRING('GP'),AT(9635,292,833,167),USE(?HeaderTitle:16),CENTER,TRN
                         STRING('Trans Cr.'),AT(6333,292,833,167),USE(?HeaderTitle:12),CENTER,TRN
                         STRING('Total'),AT(7156,292,833,167),USE(?HeaderTitle:13),CENTER,TRN
                         STRING('T/O O/Night'),AT(2042,521,833,167),USE(?HeaderTitle:8),CENTER,TRN
                         STRING('Trans Brok'),AT(4594,292,833,167),USE(?HeaderTitle:10),CENTER,TRN
                         STRING('Loadmaster'),AT(4594,521,833,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Ext. Inv.'),AT(5490,292,833,167),USE(?HeaderTitle:11),CENTER,TRN
                         STRING('C/Note'),AT(2927,292,833,167),USE(?HeaderTitle:9),CENTER,TRN
                         STRING('MID'),AT(646,292,688,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Kgs Brok'),AT(1396,292,594,167),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Kgs O/N'),AT(1396,521,594,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Date'),AT(52,292,583,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('T/O Brok'),AT(2042,292,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Total T/O'),AT(3760,292,833,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('To:'),AT(2708,42,167,167),USE(?String19:49),TRN
                         STRING('From:'),AT(1573,42,271,167),USE(?String19:48),TRN
                         STRING(@d5b),AT(1938,42,583,167),USE(LO:From_Date),RIGHT(1)
                         STRING(@d5b),AT(2969,42),USE(LO:To_Date),RIGHT(1)
                         STRING('Kgs C/Note'),AT(3073,521,594,167),USE(?HeaderTitle:18),CENTER,TRN
                       END
Detail                 DETAIL,AT(0,0,10604,396),USE(?Detail)
                         LINE,AT(0,406,10333,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         STRING(@n_10),AT(646,31,,170),USE(L_VG:MID),RIGHT(1),TRN
                         STRING(@n-10.0),AT(1396,31),USE(L_VG:Kgs_Broking),RIGHT(1),TRN
                         STRING(@n-10.0),AT(1396,208),USE(L_VG:Kgs_O_Night),RIGHT(1),TRN
                         STRING(@n-14.2),AT(2042,31),USE(L_VG:T_O_Broking),RIGHT(1),TRN
                         STRING(@n-14.2),AT(2042,208),USE(L_VG:T_O_O_Night),RIGHT(1),TRN
                         STRING(@n-14.2),AT(2927,31),USE(L_VG:C_Note),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1),TRN
                         STRING(@n-14.2),AT(3760,31),USE(L_VG:Total_T_O),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4594,208),USE(L_VG:Loadmaster),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4594,31),USE(L_VG:Trans_Broking),RIGHT(1),TRN
                         STRING(@n-14.2),AT(5490,31),USE(L_VG:Extra_Inv),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6333,31),USE(L_VG:Trans_Credit),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1), |
  TRN
                         STRING(@d5),AT(52,31,,170),USE(L_VG:CreatedDate),RIGHT(1),TRN
                         STRING(@n-14.2),AT(7156,31),USE(L_VG:Total_Cost),RIGHT(1),TRN
                         STRING(@n-14.2),AT(7969,31),USE(L_VG:Trans_Debit),RIGHT(1),TRN
                         STRING(@n-14.2),AT(8750,31),USE(L_VG:Total_Cost_Less_Deb),RIGHT(1),TRN
                         STRING(@n-14.0),AT(9375,31,1094,156),USE(L_VG:GP),RIGHT(1),TRN
                         STRING(@n-14.2),AT(9375,208,1094,167),USE(L_VG:GP_Per),RIGHT(1),TRN
                         STRING(@n-14),AT(2927,208),USE(L_VG:C_Note_Cred_Per),FONT(,,COLOR:Red),RIGHT(12),TRN
                       END
detailInfo             DETAIL,AT(0,0,10594,792),USE(?DETAILinfo)
                         STRING(@s255),AT(135,52,10208),USE(L_VG:Line1)
                         STRING(@s255),AT(135,281,10208,167),USE(L_VG:Line2)
                         STRING(@s255),AT(135,510,10208,167),USE(L_VG:Line3)
                       END
totals                 DETAIL,AT(0,0,,542),USE(?totals)
                         STRING('Totals'),AT(104,73,885),USE(?String56),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@n-10.0),AT(1396,73),USE(L_VG:Kgs_Broking,,?L_VG:Kgs_Broking:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-10.0),AT(1396,260),USE(L_VG:Kgs_O_Night,,?L_VG:Kgs_O_Night:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(2042,73),USE(L_VG:T_O_Broking,,?L_VG:T_O_Broking:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(2042,260),USE(L_VG:T_O_O_Night,,?L_VG:T_O_O_Night:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(2927,73),USE(L_VG:C_Note,,?L_VG:C_Note:2),FONT(,,COLOR:Red,,CHARSET:ANSI), |
  RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(3760,73),USE(L_VG:Total_T_O,,?L_VG:Total_T_O:2),RIGHT(1),SUM(L_TG:Total_TO), |
  TALLY(Detail),TRN
                         STRING(@n-14.2),AT(4594,260),USE(L_VG:Loadmaster,,?L_VG:Loadmaster:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(4594,73),USE(L_VG:Trans_Broking,,?L_VG:Trans_Broking:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(5490,73),USE(L_VG:Extra_Inv,,?L_VG:Extra_Inv:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(6333,73),USE(L_VG:Trans_Credit,,?L_VG:Trans_Credit:2),FONT(,,COLOR:Red, |
  ,CHARSET:ANSI),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(7156,73),USE(L_VG:Total_Cost,,?L_VG:Total_Cost:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(7969,73),USE(L_VG:Trans_Debit,,?L_VG:Trans_Debit:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(8750,73),USE(L_VG:Total_Cost_Less_Deb,,?L_VG:Total_Cost_Less_Deb:2),RIGHT(1), |
  SUM(L_TG:Total_Cost),TALLY(Detail),TRN
                         STRING(@n-14.2),AT(9375,73,1094,167),USE(L_TG:GP),RIGHT(1),TRN
                         STRING(@n-10.2),AT(9375,260,1094,167),USE(L_TG:GP_Per),RIGHT(1),TRN
                       END
                       FOOTER,AT(250,7813,10604,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(9521,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END

TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Missing_Invs  ROUTINE
    CLEAR(LOC:Dates_Done)
    GET(LOC:Dates_Done, RECORDS(LOC:Dates_Done))
    ! The last date done - and we are not on 1st date of range
    IF MAN:CreatedDate > L_DQ:Date + 1 AND MAN:CreatedDate ~= LO:From_Date
       ! Check for invoices not associated with a Manifest in this period
       IF L_DQ:Date = 0
          LOC:Cur_Date             = LO:From_Date
       ELSE
          LOC:Cur_Date             = L_DQ:Date + 1
       .

       LOOP
          CLEAR(LOC:Values)

          Cred_Type_#      = 4          ! Changed from 2 & 3 on 12 Feb 07
          IF L_SG:ExcludeBadDebts = TRUE
             Cred_Type_#   = 5
          .

          IF L_SG:VAT = TRUE
             ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info  , p:LogName)
             L_VG:C_Note           = -Get_Invoices(LOC:Cur_Date, 0, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1, 'I-CR')          ! Any passed on this day
          ELSE
             L_VG:C_Note           = -Get_Invoices(LOC:Cur_Date, 1, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1, 'I-CR')          ! Any passed on this day
          .

          IF L_VG:C_Note <> 0.0
            L_VG:C_Note_CRIIDs    = Get_Invoices(MAN:CreatedDate, 5, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1, 'I-CRi')    ! Any passed on this day            

            L_VG:Line1  = 'CN IIDs: ' & L_VG:C_Note_CRIIDs
            
            Get_InvTotal_Weight(L_VG:C_Note_CRIIDs, L_VG:C_Note_InvTot, L_VG:C_Note_InvWeight)
              
            L_VG:C_Note_Cred_Per = (L_VG:C_Note / L_VG:C_Note_InvTot) * L_VG:C_Note_InvWeight
            
            db.debugout('[Print_Turnover]  IIDs: ' & L_VG:C_Note_CRIIDs)
            db.debugout('[Print_Turnover]  ' & L_VG:C_Note_Cred_Per & ' = (' & L_VG:C_Note & ' / ' & L_VG:C_Note_InvTot & ') * ' & L_VG:C_Note_InvWeight)

            L_VG:Line2 = '(C Note tot (' & L_VG:C_Note & ') / Inv Tot (' & L_VG:C_Note_InvTot & ')) X Inv Weight (' & L_VG:C_Note_InvWeight & ')'
            
            L_VG:Line3 = 'CN IID->IID: ' & Get_Invoices(MAN:CreatedDate, 6, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1, 'I-CRi2')    ! Any passed on this day
          .
          
          L_VG:Total_T_O           = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note

!          IF L_SG:VAT = TRUE
!             L_VG:Extra_Inv        = Get_Invoices(LOC:Cur_Date, 0, 0, TRUE, L_SG:BID)                ! Invoice for day not related to MID
!             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 0, 2, TRUE,,, L_SG:BID)  ! Credits for day - Transporter not MID
!          ELSE
!             L_VG:Extra_Inv        = Get_Invoices(LOC:Cur_Date, 1, 0, TRUE, L_SG:BID)                ! Invoice for day not related to MID
!             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 1, 2, TRUE,,, L_SG:BID)  ! Credits for day - Transporter not MID
!          .

          IF L_SG:VAT = TRUE
             L_VG:Ext_Invocie_TO   = Get_Invoices(LOC:Cur_Date, 0, 1, TRUE, L_SG:BID,, L_SG:Output,,,1, 'I-EI')   ! Invoice for day not related to MID

             ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID, p:BID, p:ToDate, p:Extra_Inv, p:Manifest_Type, p:Output, p:Source)

             L_VG:Extra_Inv        = Get_InvoicesTransporter(LOC:Cur_Date, 0, 1, TRUE,,, L_SG:BID,,TRUE,,,1,'T-EI')      ! Credits for day - Transporter not MID
             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 0, 2, TRUE,,, L_SG:BID,,,,,1,'T-TC')     ! Credits for day

             L_VG:Trans_Debit      = Get_InvoicesTransporter(LOC:Cur_Date, 0, 2,,,, L_SG:BID,,,,,1,'T-TD')
          ELSE
             L_VG:Ext_Invocie_TO   = Get_Invoices(LOC:Cur_Date, 1, 1, TRUE, L_SG:BID,, L_SG:Output,,,1, 'I-EI')   ! Invoice for day not related to MID

             L_VG:Extra_Inv        = Get_InvoicesTransporter(LOC:Cur_Date, 1, 1, TRUE,,, L_SG:BID,,TRUE,,,1,'T-EI')      ! Credits for day - Transporter not MID
             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 1, 2, TRUE,,, L_SG:BID,,,,,1,'T-TC')     ! Credits for day

             L_VG:Trans_Debit      = Get_InvoicesTransporter(LOC:Cur_Date, 1, 2,,,, L_SG:BID,,,,,1,'T-TD')
          .

          L_VG:Total_Cost             = L_VG:Loadmaster + L_VG:Trans_Broking + L_VG:Extra_Inv - L_VG:Trans_Credit

          L_VG:Total_Cost_Less_Deb    = L_VG:Total_Cost - L_VG:Trans_Debit

          L_VG:T_O_Broking           += L_VG:Ext_Invocie_TO                                                   ! Add extras to BROKING ALWAYS

          L_VG:Total_T_O              = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note

          L_VG:GP                     = L_VG:Total_T_O - L_VG:Total_Cost_Less_Deb
          L_VG:GP_Per                 = (L_VG:GP / L_VG:Total_T_O) * 100


          L_DQ:Date                = LOC:Cur_Date
          ADD(LOC:Dates_Done, L_DQ:Date)

          L_VG:MID                 = 0
          L_VG:CreatedDate         = LOC:Cur_Date

          IF L_VG:Total_Cost ~= 0.0 OR L_VG:Total_T_O ~= 0.0 OR L_VG:Trans_Debit ~= 0.0 OR L_VG:Extra_Inv ~= 0.0 OR |
                L_VG:Trans_Credit ~= 0.0 OR L_VG:C_Note ~= 0.0 OR L_VG:Ext_Invocie_TO ~= 0.0

!    message('Stop')

             db.debugout('[Print_Turnover]  Check_Missing_Invs  -  Total_Cost: ' & L_VG:Total_Cost & ',  Total_T_O: ' & L_VG:Total_T_O & ',  Trans_Debit: ' & L_VG:Trans_Debit & |
                         ',  Extra_Inv: ' & L_VG:Extra_Inv & ',  Trans_Credit: ' & L_VG:Trans_Credit & '  C_Note: ' & L_VG:C_Note & '  C_Note CR IID: ' & L_VG:C_Note_CRIIDs)

             PRINT(RPT:Detail)
          .

          LOC:Cur_Date            += 1
          IF LOC:Cur_Date >= MAN:CreatedDate
             BREAK
    .  .  .


    EXIT
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Manifest ID' & |
      '|' & 'By Branch' & |
      '|' & 'By Transporter' & |
      '|' & 'By Vehicle Composition' & |
      '|' & 'By Journey' & |
      '|' & 'By Driver' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      ! Print Totals here
      db.debugout('[Print_Turnover]  Total GP = L_TG:Total_TO (' & L_TG:Total_TO & ') - L_TG:Total_Cost (' & L_TG:Total_Cost & ')')
      L_TG:GP       = L_TG:Total_TO - L_TG:Total_Cost
      L_TG:GP_Per   = (L_TG:GP / L_TG:Total_TO) * 100
  
      PRINT(RPT:totals)
  
      IF L_SG:Output > 0
         CASE MESSAGE('Would you like to load the accumulation details now?|Note that if the file does not load automatically you can find it at the location shown below.||File: ' & PATH() & '\Print Turnover - ' & FORMAT(TODAY(), @d7) & '.csv', 'Print Turnover', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Print Turnover - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
        IF L_SG:PDF  = TRUE
           PARENT.AskPreview
        ELSE
        
  IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
    ENDPAGE(SELF.Report)
    IF SELF.SkipPreview
      TintRptViewer.CheckForPageNum(Self.PreviewQueue)
      TintRptViewer.SetupCopiesCollate(Self.PreviewQueue)
      if (TinTRptViewer.GetEjectDir())
        TinTRptViewer.ReversePreviewQueue(Self.PreviewQueue)
      end
      SELF.Report{PROP:FlushPreview} = True
      GlobalResponse = RequestCompleted
    ELSE
      GlobalResponse = TintRptViewer.Display(Report,Self.PreviewQueue,Report{Prop:Text},'Page Width',0)
      SELF.SkipPreview = TRUE
    END
    FREE(SELF.PreviewQueue)
   END
  .
        
  ! You can place code in this embed point to test GlobalResponse to see if report printed or not.
  ! CASE GlobalResponse
  ! OF RequestCompleted     ! Report Was Printed
  !   ! Do Something
  ! OF RequestCancelled     ! Report Was NOT Printed
  !   ! Do Something
  ! END
   OMIT('***TINTOOLS***')
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  PARENT.AskPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  ***TINTOOLS***
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
      ! Doesn't work, using Tin Tools
  
  !  LOOP
  !     CASE MESSAGE('Print another copy?', 'Print', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
  !     OF BUTTON:Yes
  !        SELF.PrintReport()
  !     ELSE
  !        BREAK
  !  .  .
  !


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Turnover')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_SG:ManifestedState',L_SG:ManifestedState)        ! Added by: Report
  BIND('L_SG:BID',L_SG:BID)                                ! Added by: Report
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF NOT TargetSelector.ASK(1) THEN
     SELF.Kill()
     RETURN Level:Fatal
  END
  IF NOT TargetSelector.GetPrintSelected() THEN
     SELF.SetReportTarget(TargetSelector.GetReportSelected())
  END
  SELF.AddItem(TargetSelector)
    IF NOT TargetSelector.GetPrintSelected() THEN
       L_SG:PDF  = TRUE    
    .
  
    
      
  ThisReport.Init(Process:View, Relate:Manifest, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Manifest ID')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:BID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:TID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Vehicle Composition')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:VCID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Journey')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:JID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Driver')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:DRID,+MAN:MID')
  END
  ThisReport.SetFilter('MAN:State >= L_SG:ManifestedState AND ((L_SG:BID = 0 OR L_SG:BID = MAN:BID) AND MAN:CreatedDate >= LO:From_Date AND MAN:CreatedDate << LO:To_Date + 1)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = 'Turnover Report'
  Relate:Manifest.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:MDI} = True                          ! Make progress window an MDI child window
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  FDB4.Init(?L_SG:BranchName,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(BRA:Key_BranchName)
  FDB4.AddField(BRA:BranchName,FDB4.Q.BRA:BranchName) !List box control field - type derived from field
  FDB4.AddField(BRA:BID,FDB4.Q.BRA:BID) !Primary key field - type derived from field
  FDB4.AddUpdateField(BRA:BID,L_SG:BID)
  ThisWindow.AddItem(FDB4.WindowComponent)
      LO:From_Date    = GETINI('Print_Turnover', 'From_Date', , GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Turnover', 'To_Date', , GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      PUTINI('Print_Turnover', 'From_Date', LO:From_Date, GLO:Local_INI)
      PUTINI('Print_Turnover', 'To_Date', LO:To_Date, GLO:Local_INI)
  
      
      L_SG:VAT    = GETINI('Print_Turnover', 'VAT', , GLO:Local_INI)
      L_SG:BID    = GETINI('Print_Turnover', 'BID', , GLO:Local_INI)
  
      IF L_SG:BID ~= 0
         BRA:BID  = L_SG:BID
         IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
            L_SG:BranchName   = BRA:BranchName
         .
      ELSE
         L_SG:BranchName   = 'All'
      .
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
    ! set printer eject style
    TinTRptViewer.SetEjectDir(TinEject:PageFaceUp)
    IF TintRptViewer.PrintViewerSetup() THEN
      Self:Response = RequestCancelled       ! Changed by LH 4/19/98
      POST(EVENT:CloseDown)                  ! Changed by LH 4/19/98
      RETURN Level:NOTIFY                    ! Changed by LH 4/19/98
    END
    SELF.SkipPreview = TintRptViewer.SkipPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:_SQLTemp.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
          ! ****---*** ???    Note: there is something wrong here - the L_SG:BID var is not being set by ABC templates, will use cludge below for now
          !                   Could it be Tintools?  or what?
      
          IF L_SG:BranchName ~= 'All'
             BRA:BranchName   = L_SG:BranchName
             IF Access:Branches.Fetch(BRA:Key_BranchName) = LEVEL:Benign
                L_SG:BID      = BRA:BID
          .  .
      
      
             
          SELECT(?Tab1)
          DISABLE(?Pause)
          DISPLAY
          PUTINI('Print_Turnover', 'VAT', L_SG:VAT, GLO:Local_INI)
          PUTINI('Print_Turnover', 'BID', L_SG:BID, GLO:Local_INI)
      
      
          EXECUTE L_SG:VAT + 1
             L_RG:VAT_Option  = 'Excluding VAT'
             L_RG:VAT_Option  = 'Including VAT'
          .
          IF L_SG:Output > 0
             L_SG:Print_DetailInfo = TRUE  
        
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             Add_Log('Start', 'Print_Turnover', 'Print Turnover - ' & FORMAT(TODAY(), @d7) & '.csv', TRUE, 1)
      
             CASE MESSAGE('Would you like to empty the Audit Management Profit table?','Print Turnover', ICON:Question, 'All|Turnover|None', 1)
             OF 1
                _SQLTemp{PROP:SQL}    = 'DELETE FROM Audit_ManagementProfit'
             OF 2
                _SQLTemp{PROP:SQL}    = 'DELETE FROM Audit_ManagementProfit WHERE source = 1'
          .  .
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_SG:BranchName
          MESSAGE('L_SG:BID: ' & L_SG:BID)
          
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          ! Add an All
          Queue:FileDrop.BRA:BranchName           = 'All'
          GET(Queue:FileDrop, Queue:FileDrop.BRA:BranchName)
          IF ERRORCODE()
             CLEAR(Queue:FileDrop)
             Queue:FileDrop.BRA:BranchName        = 'All'
             Queue:FileDrop.BRA:BID               = 0
             ADD(Queue:FileDrop,1)
          .
      
      
      
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          Queue:FileDrop.BRA:BID               = L_SG:BID
          GET(Queue:FileDrop, Queue:FileDrop.BRA:BID)
          IF ERRORCODE()
             L_SG:BranchName   = Queue:FileDrop.BRA:BranchName
          .
      
          DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
              ! need to check for un-manifested invoices
              ! need to add in dates from too check to make sure all dates are included
  
      ! We have a Q of done dates, we need to check if the current date is the date after the last date
      ! or the date after the start date
      ! This will work as all orders are by date
  
      ! Check if the current record date is the next record
      DO Check_Missing_Invs
  
  
  
  
  
      ! ==============   normal processing   ==============
      CLEAR(LOC:Values)
  
      L_VG:MID                    = MAN:MID
      L_VG:CreatedDate            = MAN:CreatedDate
  
  
      ! Get_Manifest_Info
      ! (p:MID, p:Option, p:DI_Type, p:Inv_Totals, p:Output, p:Info  )
      ! (ULONG, BYTE    , BYTE=0   , BYTE=0      , BYTE=0  , <STRING>),STRING
      !   1       2           3           4           5           6
      !   p:Option
      !       0.  Charges total
      !       1.  Insurance total
      !       2.  Weight total, for this Manifest!    Uses Units_Loaded
      !       3.  Extra Legs cost
      !       4.  Charges total incl VAT
      !       5.  No. DI's
      !       6.  Extra Legs cost - inc VAT
      !   p:DI_Type
      !       0.  All             (default)
      !       1.  Non-Broking
      !       2.  Broking
      !   p:Inv_Totals
      !       0.  Collect from Delivery info.
      !       1.  Collect from Invoiced info.     (only Manifest invoices)
  
      L_VG:Kgs_Broking            = Get_Manifest_Info(MAN:MID, 2, 2,, L_SG:Output)
      L_VG:Kgs_O_Night            = Get_Manifest_Info(MAN:MID, 2, 1,, L_SG:Output)
  
      ! Below is no longer possible, the Broking status is set on the Manifest
  
  !    IF L_VG:Kgs_Broking ~= 0.0 AND L_VG:Kgs_O_Night ~= 0.0
  !       ! Error - some DIs are overnight some are broking...
  !       
  !       ! (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel)       (BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0)
  !       Add_SystemLog(100, 'Print_Turnover', 'MID: ' & MAN:MID & ', has both Broking and O/Night DIs attached to it!', 'Please correct.')
  !       LOC:Errors              += 1
  !    .
  
      IF MAN:Broking = FALSE                !L_VG:Kgs_Broking = 0.0
         IF L_SG:VAT = TRUE
            L_VG:T_O_O_Night      = Get_Manifest_Info(MAN:MID, 4,,1, L_SG:Output, 'ON')             ! All charges - from invoice    inc
            !L_VG:T_O_O_Night      = Get_Invoices(0, 0, 1, FALSE, MAN:MID,, L_SG:Output, 1)   - this would also work
  
            ! Invoices (p3 = 1), Del Leg Only (p5 = 1), Broking Invoice (p10 = 2), Broking & ON Manifests (p15 = 0)
            L_VG:T_O_Broking      = Get_InvoicesTransporter(0, 0, 1, 0, 1, MAN:MID,,,, 2, L_SG:Output,1, 'T-EL')           ! Del legs, broking - inc vat
  
            ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
            IF L_VG:T_O_Broking > 0.0
               Add_Log('MID: ' & MAN:MID & ',  L_VG:T_O_Broking: ' & L_VG:T_O_Broking & ',  MAN:CreatedDate: ' & FORMAT(MAN:CreatedDate,@d6), 'Get_Manifest_Info-Extra', 'Extra.log',,, 0)
            .
         ELSE
            L_VG:T_O_O_Night      = Get_Manifest_Info(MAN:MID, 0,,1, L_SG:Output, 'ON')             ! All charges - from invoice
            L_VG:T_O_Broking      = Get_InvoicesTransporter(0, 1, 1, 0, 1, MAN:MID,,,, 2, L_SG:Output,1, 'T-EL')           ! Del legs, broking - ex vat
         .
  
     db.debugout('[Print_Turnover]  MAN:MID: ' & MAN:MID & ', T_O_O_Night: ' & L_VG:T_O_O_Night & ',  L_VG:T_O_Broking: ' & L_VG:T_O_Broking)
  
         L_VG:T_O_O_Night        -= L_VG:T_O_Broking                                          ! Legs cost off total DI value (included)
      ELSE
         L_VG:T_O_O_Night         = 0.0
         IF L_SG:VAT = TRUE
            L_VG:T_O_Broking      = Get_Manifest_Info(MAN:MID, 4,,1, L_SG:Output, 'BR')             ! All charges - from invoice    inc
  
            IF L_VG:T_O_Broking > 0.0
               Add_Log('MID: ' & MAN:MID & ',  L_VG:T_O_Broking: ' & L_VG:T_O_Broking  & ',  MAN:CreatedDate: ' & FORMAT(MAN:CreatedDate,@d6), 'Get_Manifest_Info-Extra', 'Extra.log',,, 0)
            .
         ELSE
            L_VG:T_O_Broking      = Get_Manifest_Info(MAN:MID, 0,,1, L_SG:Output, 'BR')             ! All charges - from invoice
      .  .
  
      ! Get_Invoices
      ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info  , p:LogName)
      !   1           2       3           4          5       6          7        8            9                 10        11          12
      ! (LONG  , BYTE    , BYTE=0, BYTE=0        , ULONG=0, <LONG>  , BYTE=0  , BYTE=0    , BYTE=0           , BYTE=0  , <STRING>, <STRING>),STRING
      ! p:Option
      !   0.  - Total Charges inc.    (default)
      !   1.  - Excl VAT
      !   2.  - VAT
      !   3.  - Kgs
      !   4.  - IIDs
      !   5.  - CR IIDs
      ! p:Type
      !   0.  - All                   (default)
      !   1.  - Invoices                                  Total >= 0.0
      !   2.  - Credit Notes                              Total < 0.0    Instead of <, should be invoices with A_INV:CR_IID
      !   3.  - Credit Notes excl Bad Debts               Total < 0.0
      !   4.  - Credit Notes excl Journals                Total < 0.0
      !   5.  - Credit Notes excl Bad Debts & Journals    Total < 0.0
      ! p:Limit_On    &    p:ID
      !   0.  - Date [& Branch]
      !   1.  - MID
      ! p:Manifest_Option
      !   0.  - None
      !   1.  - Overnight
      !   2.  - Broking
      ! p:Not_Manifest
      !   0.  - All
      !   1.  - Not Manifest
      !   2.  - Manifest
  
      L_DQ:Date                   = MAN:CreatedDate
      GET(LOC:Dates_Done, L_DQ:Date)
      IF ERRORCODE()
         Cred_Type_#      = 4             ! Changed from 2 & 3 on 12 Feb 07
         IF L_SG:ExcludeBadDebts = TRUE
            Cred_Type_#   = 5
         .
  
         IF L_SG:VAT = TRUE
            ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info  , p:LogName)
            L_VG:C_Note           = -Get_Invoices(MAN:CreatedDate, 0, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1, 'I-CR')    ! Any passed on this day
         ELSE
            L_VG:C_Note           = -Get_Invoices(MAN:CreatedDate, 1, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1, 'I-CR')    ! Any passed on this day
         .      
         
         IF L_VG:C_Note <> 0.0      ! 10 July 13
            L_VG:C_Note_CRIIDs = Get_Invoices(MAN:CreatedDate, 5, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1, 'I-CRi')    ! Any passed on this day
            L_VG:Line1  = 'CN IIDs: ' & L_VG:C_Note_CRIIDs
            
            Get_InvTotal_Weight(L_VG:C_Note_CRIIDs, L_VG:C_Note_InvTot, L_VG:C_Note_InvWeight)
              
            L_VG:C_Note_Cred_Per = (L_VG:C_Note / L_VG:C_Note_InvTot) * L_VG:C_Note_InvWeight
            
            db.debugout('[Print_Turnover]  IIDs: ' & L_VG:C_Note_CRIIDs)
            db.debugout('[Print_Turnover]  ' & L_VG:C_Note_Cred_Per & ' = (' & L_VG:C_Note & ' / ' & L_VG:C_Note_InvTot & ') * ' & L_VG:C_Note_InvWeight)
  
            L_VG:Line2 = '(C Note tot (' & L_VG:C_Note & ') / Inv Tot (' & L_VG:C_Note_InvTot & ')) X Inv Weight (' & L_VG:C_Note_InvWeight & ')'
            
            L_VG:Line3 = 'CN IID->IID: ' & Get_Invoices(MAN:CreatedDate, 6, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1, 'I-CRi2')    ! Any passed on this day
      .  .
  
      IF L_SG:VAT = TRUE
         ! Get_InvoicesTransporter
         ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type,
         ! (LONG  , BYTE    , BYTE=0, BYTE=0        , BYTE=0        , <ULONG>, ULONG=0, <LONG>  , BYTE=0     , BYTE=0        ,
         !   1           2       3         4               5           6        7         8          9           10              11         12       13        14         15
         !  p:Output, p:Source, p:Info  , p:LogName, p:Manifest_Type)
         !  BYTE=0  , BYTE=0  , <STRING>, <STRING> , BYTE=0         ),STRING
         !     11         12       13        14         15
         ! p:Option                                                2
         !   0.  - Total Charges inc.
         !   1.  - Excl VAT
         !   2.  - VAT
         ! p:Type                                                  3
         !   0.  - All
         !   1.  - Invoices                      Total >= 0.0
         !   2.  - Credit Notes                  Total < 0.0
         ! p:DeliveryLegs                                          5
         !   0.  - Both
         !   1.  - Del Legs only
         !   2.  - No Del Legs
         ! p:MID                                                   6
         !   0   = Not for a MID
         !   x   = for a MID
         ! p:Extra_Inv                                             9
         !   Only
         ! p:Invoice_Type (was Manifest_Type)                      10
         !   0   - All
         !   1   - Overnight
         !   2   - Broking
         ! p:Manifest_Type                                         15
         !   0   - All
         !   1   - Overnight
         !   2   - Broking
         ! p:Extra_Inv                                             9
         !   Only
         ! p:Source                                                12
         !   1   - Management Profit
         !   2   - Management Profit Summary
  
         L_VG:Loadmaster       = Get_InvoicesTransporter(0, 0, 1, 0, 0, MAN:MID,,,, 1, L_SG:Output,1, 'T-CO')
         ! Invoices (p3 = 1), Broking (p10 = 2), Legs & Other (p5 = 0)
         L_VG:Trans_Broking    = Get_InvoicesTransporter(0, 0, 1, 0, 0, MAN:MID,,,, 2, L_SG:Output,1, 'T-CB')
      ELSE
         L_VG:Loadmaster       = Get_InvoicesTransporter(0, 1, 1, 0, 0, MAN:MID,,,, 1, L_SG:Output,1, 'T-CO')
         L_VG:Trans_Broking    = Get_InvoicesTransporter(0, 1, 1, 0, 0, MAN:MID,,,, 2, L_SG:Output,1, 'T-CB')
      .
  
  
      ! Get all Extra Invoices not associated to a MID
      GET(LOC:Dates_Done, L_DQ:Date)
      IF ERRORCODE()
         IF L_SG:VAT = TRUE
            L_VG:Ext_Invocie_TO   = Get_Invoices(MAN:CreatedDate, 0, 1, TRUE, L_SG:BID,, L_SG:Output,,,1, 'I-EI')   ! Invoice for day not related to MID
  
            IF L_VG:Ext_Invocie_TO > 0.0
               Add_Log('MID: ' & MAN:MID & ',  L_VG:Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',   MAN:CreatedDate: ' & FORMAT(MAN:CreatedDate,@d6), 'Get_Manifest_Info-Extra', 'Extra.log',,, 0)
            .
  
            ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID, p:BID, p:ToDate, p:Extra_Inv, p:Manifest_Type, p:Output)
            L_VG:Extra_Inv        = Get_InvoicesTransporter(MAN:CreatedDate, 0, 1, FALSE,,, L_SG:BID,,TRUE,, L_SG:Output,1, 'T-EI')    ! Credits for day
            L_VG:Trans_Credit     = Get_InvoicesTransporter(MAN:CreatedDate, 0, 1, TRUE,,, L_SG:BID,,,, L_SG:Output,1, 'T-TC')         ! Credits for day - Transporter not MID
  
            L_VG:Trans_Debit      = -Get_InvoicesTransporter(MAN:CreatedDate, 0, 2,,,, L_SG:BID,,,, L_SG:Output,1, 'T-TD')
  
     db.debugout('[Print_Turnover]  Extra invoices - Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',  Extra_Inv: ' & L_VG:Extra_Inv & ',  Trans_Credit: ' & L_VG:Trans_Credit & ',  Trans_Debit: ' & L_VG:Trans_Debit)
  
         ELSE
            L_VG:Ext_Invocie_TO   = Get_Invoices(MAN:CreatedDate, 1, 1, TRUE, L_SG:BID,, L_SG:Output,,,1, 'I-EI')   ! Invoice for day not related to MID
  
            L_VG:Extra_Inv        = Get_InvoicesTransporter(MAN:CreatedDate, 1, 1, FALSE,,, L_SG:BID,,TRUE,, L_SG:Output,1,'T-EI')      ! Credits for day
            L_VG:Trans_Credit     = Get_InvoicesTransporter(MAN:CreatedDate, 1, 1, TRUE,,, L_SG:BID,,,, L_SG:Output,1,'T-TC')     ! Credits for day - Transporter not MID
  
            L_VG:Trans_Debit      = -Get_InvoicesTransporter(MAN:CreatedDate, 1, 2,,,, L_SG:BID,,,, L_SG:Output,1,'T-TD')
      .  .
  
      L_VG:Total_Cost             = L_VG:Loadmaster + L_VG:Trans_Broking + L_VG:Extra_Inv + L_VG:Trans_Credit
  
      L_VG:Total_Cost_Less_Deb    = L_VG:Total_Cost - L_VG:Trans_Debit
  
     db.debugout('[Print_Turnover]  Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',  T_O_Broking: ' & L_VG:T_O_Broking & ',  C_Note: ' & L_VG:C_Note  & '  C_Note CR IID: ' & L_VG:C_Note_CRIIDs)
  
      L_VG:T_O_Broking           += L_VG:Ext_Invocie_TO                                                   ! Add extras to BROKING ALWAYS
  
      L_VG:Total_T_O              = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note
  
  
      L_VG:GP                     = L_VG:Total_T_O - L_VG:Total_Cost_Less_Deb
      L_VG:GP_Per                 = (L_VG:GP / L_VG:Total_T_O) * 100
  
  
  
      L_DQ:Date                   = MAN:CreatedDate
      GET(LOC:Dates_Done, L_DQ:Date)
      IF ERRORCODE()
         L_DQ:Date                = MAN:CreatedDate
         ADD(LOC:Dates_Done, L_DQ:Date)
      .
  
        
      L_VG:DetailInfo_Print = FALSE
      IF L_SG:Print_DetailInfo = TRUE AND (CLIP((CLIP(L_VG:Line1) & CLIP(L_VG:Line2) & CLIP(L_VG:Line3))) <> '')
         L_VG:DetailInfo_Print = TRUE
      .
        
        
  IF 1
    PRINT(RPT:Detail)
  END
  IF L_VG:DetailInfo_Print
    PRINT(RPT:detailInfo)
  END
  IF 0
    PRINT(RPT:totals)
  END
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
      IF LOC:Errors > 0
         MESSAGE('Completed with Errors: ' & LOC:Errors & '||Please review the System Log.', 'Print Turnover', ICON:Exclamation)
      .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Turnover','Print_Turnover','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True


FDB4.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      ! Add an All
      Queue:FileDrop.BRA:BranchName           = 'All'
      GET(Queue:FileDrop, Queue:FileDrop.BRA:BranchName)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop)
         Queue:FileDrop.BRA:BranchName        = 'All'
         Queue:FileDrop.BRA:BID               = 0
         ADD(Queue:FileDrop,1)
  
         IF L_SG:BID = 0
            L_SG:BranchName   = 'All'
      .  .
  RETURN ReturnValue

