

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('abbreak.inc'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS006.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Creditor_Age_Analysis PROCEDURE (p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:IncludeArchived)

!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
TintRptViewer  TinReportViewer
!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
Progress:Thermometer BYTE                                  !
LOC:Locals           GROUP,PRE(LOC)                        !
Date                 DATE                                  !
Time                 TIME                                  !
                     END                                   !
LOC:IncludeArchived  BYTE                                  !
LOC:STRID            ULONG                                 !Statement Run ID
LOC:Statement_Q      QUEUE,PRE(L_SQ)                       !Statement ID
STID                 ULONG                                 !Statement ID
CID                  ULONG                                 !Client ID
                     END                                   !
LOC:Info             STRING(35)                            !
LOC:Grand_Totals     GROUP,PRE(L_GT)                       !
Total                DECIMAL(10,2)                         !Total
Current              DECIMAL(10,2)                         !Current
Days30               DECIMAL(10,2)                         !30 Days
Days60               DECIMAL(10,2)                         !60 Days
Days90               DECIMAL(10,2)                         !90 Days
Number               ULONG                                 !Number of entries
Current_PerTot       DECIMAL(6,1)                          !
Days30_PerTot        DECIMAL(6,1)                          !Current
Days60_PerTot        DECIMAL(6,1)                          !Current
Days90_PerTot        DECIMAL(6,1)                          !Current
                     END                                   !
LOC:Statement_Info   GROUP,PRE(L_SI)                       !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
                     END                                   !
Process:View         VIEW(Transporter)
                       PROJECT(TRA:AID)
                       PROJECT(TRA:Archived)
                       PROJECT(TRA:Comments)
                       PROJECT(TRA:TID)
                       PROJECT(TRA:TransporterName)
                       PROJECT(TRA:BID)
                       JOIN(BRA:PKey_BID,TRA:BID)
                       END
                     END
ProgressWindow       WINDOW('Transporter Age Analysis'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Clients Report'),AT(250,844,8000,9656),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,8000,583),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Transporter Age Analysis'),AT(0,10,7865,333),USE(?ReportTitle),FONT('Arial',18,,FONT:bold), |
  CENTER
                         LINE,AT(2885,365,0,200),USE(?HeaderLine:8),COLOR(COLOR:Black)
                         BOX,AT(0,354,7865,208),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(417,365,0,200),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1729,365,0,200),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(3573,365,0,200),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(4427,365,0,200),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(5250,365,0,200),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(6094,365,0,200),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(6948,365,0,200),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         STRING('ID'),AT(156,385,,170),USE(?HeaderTitle:1),TRN
                         STRING('Creditor Name'),AT(479,385,900,170),USE(?HeaderTitle:2),TRN
                         STRING('Details'),AT(1781,385,969,167),USE(?HeaderTitle:3),TRN
                         STRING('Comments'),AT(2917,385,625,167),USE(?HeaderTitle:9),CENTER,TRN
                         STRING('90 Days'),AT(3573,385,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('60 Days'),AT(4406,385,833,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('30 Days'),AT(5250,385,833,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Current'),AT(6104,385,833,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Total'),AT(6969,385,833,167),USE(?HeaderTitle:8),CENTER,TRN
                       END
Detail                 DETAIL,AT(0,0,8000,240),USE(?Detail)
                         LINE,AT(0,0,0,238),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(417,0,0,238),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(1729,0,0,238),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(2885,0,0,238),USE(?HeaderLine:9),COLOR(COLOR:Black)
                         LINE,AT(3573,0,0,238),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(4427,0,0,238),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(5250,0,0,238),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(6094,0,0,238),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(6948,0,0,238),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(8000,0,0,238),USE(?DetailLine:8),COLOR(COLOR:Black)
                         STRING(@s35),AT(1771,31,1094,167),USE(LOC:Info),TRN
                         STRING(@n-14.2),AT(3573,31),USE(L_SI:Days90),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4406,31),USE(L_SI:Days60),RIGHT(1),TRN
                         STRING(@n-14.2),AT(5250,31),USE(L_SI:Days30),RIGHT(1),TRN
                         STRING(@s255),AT(2927,31,600),USE(TRA:Comments),FONT('Microsoft Sans Serif',,,FONT:regular)
                         STRING(@n-14.2),AT(6104,31),USE(L_SI:Current),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6969,31),USE(L_SI:Total),RIGHT(1),TRN
                         STRING(@n_10),AT(-354,31,,170),USE(TRA:TID),RIGHT,TRN
                         STRING(@s35),AT(479,31,1219,167),USE(TRA:TransporterName),LEFT
                         LINE,AT(0,240,8000,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
detail_totals          DETAIL,AT(0,0,,781),USE(?detail_totals)
                         LINE,AT(1094,167,6771,0),USE(?Line20),COLOR(COLOR:Black),LINEWIDTH(10)
                         STRING(@n-14.2),AT(5250,246,833,167),USE(L_GT:Days30),RIGHT(1),TRN
                         STRING(@n-14.2),AT(3573,246),USE(L_GT:Days90),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4406,246,833,167),USE(L_GT:Days60),RIGHT(1),TRN
                         STRING(@n13),AT(1979,246),USE(L_GT:Number),RIGHT(1)
                         STRING(@n-14.2),AT(6104,246,833,167),USE(L_GT:Current),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6969,246,833,167),USE(L_GT:Total),RIGHT(1),TRN
                         STRING('% of Total:'),AT(1115,469),USE(?String29:2),TRN
                         STRING(@n-9.1),AT(5250,469,833,167),USE(L_GT:Days30_PerTot),RIGHT(1),TRN
                         STRING(@n-9.1),AT(4406,469,833,167),USE(L_GT:Days60_PerTot),RIGHT(1),TRN
                         STRING(@n-9.1),AT(3573,469,833,167),USE(L_GT:Days90_PerTot),RIGHT(1),TRN
                         STRING(@n-9.1),AT(6104,469,833,167),USE(L_GT:Current_PerTot),RIGHT(1),TRN
                         LINE,AT(1094,677,6771,0),USE(?Line20:2),COLOR(COLOR:Black),LINEWIDTH(10)
                         STRING('Creditors Listed:'),AT(1115,246),USE(?String29),TRN
                       END
                       FOOTER,AT(250,10500,8000,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(7177,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

View_Addr       VIEW(AddressContacts)
    !PROJECT()
    .


Addr_View       ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Transporter Name' & |
      '|' & 'By Transporter ID' & |
      '|' & 'By Branch' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:detail_totals)
  
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
    ENDPAGE(SELF.Report)
    IF SELF.SkipPreview
      TintRptViewer.CheckForPageNum(Self.PreviewQueue)
      TintRptViewer.SetupCopiesCollate(Self.PreviewQueue)
      if (TinTRptViewer.GetEjectDir())
        TinTRptViewer.ReversePreviewQueue(Self.PreviewQueue)
      end
      SELF.Report{PROP:FlushPreview} = True
      GlobalResponse = RequestCompleted
    ELSE
      GlobalResponse = TintRptViewer.Display(Report,Self.PreviewQueue,Report{Prop:Text},'Page Width',0)
      SELF.SkipPreview = TRUE
    END
    FREE(SELF.PreviewQueue)
   END
  ! You can place code in this embed point to test GlobalResponse to see if report printed or not.
  ! CASE GlobalResponse
  ! OF RequestCompleted     ! Report Was Printed
  !   ! Do Something
  ! OF RequestCancelled     ! Report Was NOT Printed
  !   ! Do Something
  ! END
   OMIT('***TINTOOLS***')
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  PARENT.AskPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  ***TINTOOLS***
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Creditor_Age_Analysis')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:IncludeArchived',LOC:IncludeArchived)          ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:AddressContacts.SetOpenRelated()
  Relate:AddressContacts.Open                              ! File AddressContacts used by this procedure, so make sure it's RelationManager is open
  Access:_Statements.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Statement_Runs.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Transporter, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter Name')) THEN
     ThisReport.AppendOrder('+TRA:TransporterName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter ID')) THEN
     ThisReport.AppendOrder('+TRA:TID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch')) THEN
     ThisReport.AppendOrder('+BRA:BranchName,+TRA:TransporterName')
  END
  ThisReport.SetFilter('LOC:IncludeArchived = 1 OR TRA:Archived = 0')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Transporter.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 5                           ! Assign timer interval
      LOC:Date        = p:Date
      LOC:Time        = DEFORMAT('12:00:00',@t4)
  
  
  
  
     ! (p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:IncludeArchived)
     
     LOC:IncludeArchived  = p:IncludeArchived
      Process_InvoiceTransporter_StatusUpToDate('0')
      Process_TransporterPayments_StatusUpToDate('0')
      IF p:OutPut > 0       ! (p:Text, p:Heading, p:Name)
         Add_Log(',Transporter ID,Invoice No.,Invoice Date,Invoice Cost, Invoice VAT,Credits,Payments', 'Headings', 'Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv', TRUE)
      .
  
  
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
    ! set printer eject style
    TinTRptViewer.SetEjectDir(TinEject:PageFaceUp)
    IF TintRptViewer.PrintViewerSetup() THEN
      Self:Response = RequestCancelled       ! Changed by LH 4/19/98
      POST(EVENT:CloseDown)                  ! Changed by LH 4/19/98
      RETURN Level:NOTIFY                    ! Changed by LH 4/19/98
    END
    SELF.SkipPreview = TintRptViewer.SkipPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressContacts.Close
  END
      IF p:OutPut > 0
         CASE MESSAGE('Would you like to load the accumulation details now?||File: ' & PATH() & '\Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv', 'Age Analysis', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(0{PROP:Handle}, PATH() & '\Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      ! Get the suitable Statement
      CLEAR(LOC:Info)
                                               ! (p:TID, p:Date, p:Owing, p:Statement_Info, p:MonthEndDay, p:OutPut)
      Junk_#          = Gen_Aging_Transporter(TRA:TID, LOC:Date, , LOC:Statement_Info, p:MonthEndDay, p:OutPut)
                        
      ! Totals
      L_GT:Total     += L_SI:Total
      L_GT:Current   += L_SI:Current
      L_GT:Days30    += L_SI:Days30
      L_GT:Days60    += L_SI:Days60
      L_GT:Days90    += L_SI:Days90
      L_GT:Number    += 1
  
  
  
      ! Put contact details in LOC:Info
      Addr_View.Init(View_Addr, Relate:AddressContacts)
      Addr_View.AddSortOrder(ADDC:FKey_AID)
      Addr_View.AppendOrder('ADDC:ContactName')
      Addr_View.AddRange(ADDC:AID, TRA:AID)
  
      Addr_View.Reset()
  
      IF Addr_View.Next() = LEVEL:Benign
         LOC:Info     = ADDC:ContactName
      .
  
      Addr_View.Kill()
  
  
      ADD:AID = TRA:AID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
         LOC:Info             = CLIP(ADD:PhoneNo) & ', ' & LOC:Info
      .
  
  
      L_GT:Current_PerTot     = L_GT:Current / L_GT:Total * 100
      L_GT:Days30_PerTot      = L_GT:Days30  / L_GT:Total * 100
      L_GT:Days60_PerTot      = L_GT:Days60  / L_GT:Total * 100
      L_GT:Days90_PerTot      = L_GT:Days90  / L_GT:Total * 100
  ReturnValue = PARENT.TakeRecord()
        IF p:Ignore_Zero_Bal = TRUE
           IF (L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current + L_SI:Total) = 0.0
              L_GT:Number -= 1
  
              !L_GT:Total = 0.0 AND L_GT:Current = 0.0 AND L_GT:Days30 = 0.0 AND L_GT:Days60 = 0.0 AND L_GT:Days90 = 0.0
              !db.debugout('[Print_Debtors_Age_Analysis]  0.0 on all values')
              
  
              SkipDetails  = TRUE
        .  .
  IF 0
    PRINT(RPT:detail_totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Creditor_Age_Analysis','Print_Creditor_Age_Analysis','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Client_Advance_Payments PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Report           GROUP,PRE(L_RG)                       !
Total                DECIMAL(12,2)                         !
Current              DECIMAL(12,2)                         !
Greater_Current      DECIMAL(12,2)                         !
                     END                                   !
LOC:Date_Month_End   DATE                                  !
LOC:Allocated        DECIMAL(10,2)                         !Amount of payment
Process:View         VIEW(Clients)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:AID)
                       JOIN(ADD:PKey_AID,CLI:AID)
                         PROJECT(ADD:PhoneNo)
                       END
                     END
ProgressWindow       WINDOW('Report Clients Advanced Payments'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('ClientsPayments Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Client Advance Payments'),AT(0,52,6094,260),USE(?ReportTitle),FONT(,14,,FONT:regular), |
  CENTER
                         STRING('Un-allocated payment amounts'),AT(6146,156),USE(?String23),TRN
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(2583,350,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(4063,350,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(5313,350,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(6563,350,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         STRING('Client No.'),AT(52,385,688,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Client'),AT(833,417,1667,167),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Phone'),AT(2750,385,1191,170),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('> Current'),AT(4198,385,990,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Current'),AT(5458,385,990,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('Total'),AT(6708,385,990,167),USE(?HeaderTitle:6),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(2583,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(4063,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(5313,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(6563,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         STRING(@n_10b),AT(50,50,,170),USE(CLI:ClientNo),RIGHT
                         STRING(@s100),AT(833,52,1667,156),USE(CLI:ClientName),LEFT,TRN
                         STRING(@s20),AT(2750,52,1191,170),USE(ADD:PhoneNo),LEFT
                         STRING(@n-17.2),AT(4208,52,,170),USE(L_RG:Greater_Current),RIGHT(1)
                         STRING(@n-17.2),AT(5458,52,,170),USE(L_RG:Current),RIGHT(1)
                         STRING(@n-17.2),AT(6708,52,,170),USE(L_RG:Total),RIGHT(1)
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
detail_totals          DETAIL,AT(,,,448),USE(?detail_totals)
                         LINE,AT(104,104,7552,0),USE(?Line12),COLOR(COLOR:Black)
                         STRING('Totals'),AT(510,156),USE(?String19),TRN
                         STRING(@n-17.2),AT(4208,156,,170),USE(L_RG:Greater_Current,,?L_RG:Greater_Current:2),RIGHT(1), |
  SUM,TALLY(Detail),TRN
                         STRING(@n-17.2),AT(5458,156,,170),USE(L_RG:Current,,?L_RG:Current:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-17.2),AT(6708,156,,170),USE(L_RG:Total,,?L_RG:Total:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         LINE,AT(104,365,7552,0),USE(?Line12:2),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Payments              ROUTINE
    ! Check and update statuses on Client Payments - we would need to loop through them 1st....
    ! So we will instead only update the ones that show as being un-allocated...
    CLEAR(LOC:Report)

    ! (p:CID, p:Payment_Status, p:Total, p:Cur, p:Other)
    ! (ULONG, LONG=2, *DECIMAL, <*DECIMAL>, <*DECIMAL>),LONG
    Get_Client_Advance_Payments(CLI:CID, , L_RG:Total, L_RG:Current, L_RG:Greater_Current)


    IF L_RG:Current ~= 0.0 OR L_RG:Greater_Current ~= 0.0
       db.debugout('[Print_Client_Advnace_Payments]  L_RG:Current: ' & L_RG:Current & ',   L_RG:Greater_Current: ' & L_RG:Greater_Current)
    .


    !L_RG:Total      = L_RG:Current + L_RG:Greater_Current
    EXIT
!               old
!Check_Payments              ROUTINE
!    ! Check and update statuses on Client Payments - we would need to loop through them 1st....
!    ! So we will instead only update the ones that show as being un-allocated...
!    CLEAR(LOC:Report)
!
!    CP_View.Init(View_CP, Relate:ClientsPayments)
!    CP_View.AddSortOrder(CLIP:FKey_CID)
!    CP_View.AppendOrder('CLIP:CPID')
!    CP_View.AddRange(CLIP:CID, CLI:CID)
!    CP_View.SetFilter('CLIP:Status < 2')            ! Not fully allocated
!
!    CP_View.Reset()
!    LOOP
!       IF CP_View.Next() ~= LEVEL:Benign
!          BREAK
!       .
!
!       ! (p:CPID, p:Amt_Allocated)
!       IF Upd_ClientPayments_Status(CLIP:CPID, LOC:Allocated) >= 2
!          CYCLE
!       .
!
!       IF CLIP:DateMade <= LOC:Date_Month_End       ! Prior to current
!          L_RG:Greater_Current  += (CLIP:Amount - LOC:Allocated)
!       ELSE
!          L_RG:Current          += (CLIP:Amount - LOC:Allocated)
!    .  .
!
!    CP_View.Kill()
!
!
!    IF L_RG:Current ~= 0.0 OR L_RG:Greater_Current ~= 0.0
!       db.debugout('[Print_Client_Advnace_Payments]  L_RG:Current: ' & L_RG:Current & ',   L_RG:Greater_Current: ' & L_RG:Greater_Current)
!    .
!
!
!    L_RG:Total      = L_RG:Current + L_RG:Greater_Current
!    EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
     PRINT(RPT:detail_totals)
  
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Client_Advance_Payments')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:ClientsPayments.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Client_Advance_Payments',ProgressWindow) ! Restore window settings from non-volatile store
      IF DAY(TODAY()) > 25
         LOC:Date_Month_End       = DATE(MONTH(TODAY()), 25, YEAR(TODAY()))
      ELSE
         IF MONTH(TODAY()) = 1
            LOC:Date_Month_End    = DATE(12, 25, YEAR(TODAY()) - 1)
         ELSE
            LOC:Date_Month_End    = DATE(MONTH(TODAY()) - 1, 25, YEAR(TODAY()))
      .  .
  
      MESSAGE('The current period will be taken to be ' & FORMAT(LOC:Date_Month_End, @d6b), 'Print Client Advance Payments', ICON:Asterisk)
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:Clients, ?Progress:PctText, Progress:Thermometer, ProgressMgr, CLI:ClientName)
  ThisReport.AddSortOrder(CLI:Key_ClientName)
  ThisReport.AppendOrder('+CLI:ClientNo')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Clients.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Client_Advance_Payments',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      DO Check_Payments
      IF L_RG:Total = 0.0
         SkipDetails    = 1
      .
  
  IF 0
    PRINT(RPT:detail_totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Client_Advance_Payments','Print_Client_Advance_Payments','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Rates PROCEDURE (p:CID, p:Option)

Progress:Thermometer BYTE                                  !
LOC:Group            GROUP,PRE(LOC)                        !
CID                  ULONG                                 !Client ID
FID                  ULONG                                 !Floor ID
Printed_CP_Head      BYTE                                  !
Printed_AD_Head      BYTE                                  !
Printed_FS_Head      BYTE                                  !
Printed_TC_Head      BYTE                                  !
Terms                STRING(20)                            !Terms - Pre Paid, COD, Account, On Statement
InsuranceRequired    STRING(500)                           !Insurance Required
ACCID                ULONG                                 !Additional Charge Category ID
Terms_Bold           BYTE                                  !
Journey_Type         BYTE                                  !0 = Journey, 1 = Desc.
Journey              CSTRING(701)                          !Description
VolumetricRatio      DECIMAL(8,2)                          !x square cube weighs this amount
ClientNo             ULONG                                 !Client No.
CID_VR               ULONG                                 !Client ID
Discounted_Rate_Per_Kg DECIMAL(10,4)                       !Used on the Container Park Rates Detail
Client_Status        STRING(20)                            !Normal, On Hold, Closed, Dormant
                     END                                   !
LOC:Print_Rates      BYTE                                  !
L_OG:Options         GROUP,PRE(L_OG)                       !
EffectiveRatesOnly   BYTE                                  !Only print effective rates
EffectiveDateAtDate  DATE                                  !Print Rates that were in effect at this date
EffectiveDateAtDate_Rpt DATE                               !
EffectiveRatesOnly_Rpt BYTE                                !
CreateLog            BYTE                                  !
Include_Stats        BYTE                                  !
FuelSurcharge        BYTE                                  !Type of Fuel Surcharge to print (new or old)
                     END                                   !
L_EQ:Effective_Q     QUEUE,PRE(L_EQ)                       !
LTID                 ULONG                                 !Load Type ID
CRTID                ULONG                                 !Client Rate Type ID
JID                  ULONG                                 !Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
CTID                 ULONG(0)                              !Container Type ID
ToMass               DECIMAL(9)                            !Up to this mass in Kgs
                     END                                   !
L_CPQ:Container_Parks_Discount_Q QUEUE,PRE(L_CPQ)          !
FID                  ULONG                                 !Floor ID
ContainerParkRateDiscount DECIMAL(5,2)                     !Discount % that is applied to the container park rates for this client
                     END                                   !
LOC:Report_Group     GROUP,PRE(L_RG)                       !
T_Year               DECIMAL(8)                            !Tonnage for a year
T_6Months            DECIMAL(8)                            !
T_AvgYear            DECIMAL(8)                            !
Income_Year          DECIMAL(8)                            !
Income_6Months       DECIMAL(8)                            !
Income_AvgYear       DECIMAL(8)                            !
Tonnage_Rank_Year    LONG                                  !Last year
Income_Rank_Year     LONG                                  !
Average_Rate_Rank_Year LONG                                !
Tonnage_Rank_All     LONG                                  !
Income_Rank_All      LONG                                  !
Average_Rate_Rank_All LONG                                 !
Rate_Average_Year    DECIMAL(7,2)                          !
Rate_Average_All     DECIMAL(7,2)                          !
                     END                                   !
Process:View         VIEW(Clients)
                       PROJECT(CLI:AccountLimit)
                       PROJECT(CLI:CID)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:DateOpened)
                       PROJECT(CLI:DocumentCharge)
                       PROJECT(CLI:InsuranceRequired)
                       PROJECT(CLI:Terms)
                       PROJECT(CLI:VolumetricRatio)
                       PROJECT(CLI:BID)
                       JOIN(BRA:PKey_BID,CLI:BID)
                         PROJECT(BRA:BranchName)
                       END
                     END
ProgressWindow       WINDOW('Report Rates'),AT(,,191,188),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  TIMER(1)
                       BUTTON('Pause'),AT(84,170,49,15),USE(?Pause),LEFT,ICON('VCRDOWN.ICO'),FLAT
                       SHEET,AT(4,3,184,166),USE(?Sheet1)
                         TAB('Process'),USE(?Tab_Process)
                           PROGRESS,AT(40,44,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(26,32,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(26,60,141,10),USE(?Progress:PctText),CENTER
                         END
                         TAB('Options'),USE(?Tab_Options)
                           CHECK(' Effective Rates Only'),AT(88,20),USE(L_OG:EffectiveRatesOnly),MSG('Only print e' & |
  'ffective rates'),TIP('Only print effective rates')
                           PROMPT('Effective Rate at Date:'),AT(10,36),USE(?L_OG:EffectiveDateAtDate:Prompt)
                           SPIN(@d5b),AT(88,36,65,10),USE(L_OG:EffectiveDateAtDate),RIGHT(1),MSG('Print Rates that' & |
  ' were in effect at this date'),TIP('Print Rates that were in effect at this date')
                           BUTTON('...'),AT(156,36,12,10),USE(?Calendar)
                           BUTTON,AT(170,36,12,10),USE(?Button_Clear),ICON('Cancel.ico'),TIP('Clear date')
                           PROMPT('To always use the current day as the Effective Date clear the Effective Rate at Date box'), |
  AT(9,52,173,20),USE(?Prompt2)
                           CHECK(' Create &Log'),AT(88,134),USE(L_OG:CreateLog)
                           CHECK(' &Include Stats'),AT(88,114),USE(L_OG:Include_Stats),TIP('Include the statistics' & |
  ' section in the print')
                           PROMPT('Fuel Surcharge:'),AT(10,84),USE(?FuelSurcharge:Prompt)
                           LIST,AT(88,84,65,10),USE(L_OG:FuelSurcharge),DROP(5),FROM('New|#0|Old|#1'),MSG('Type of Fu' & |
  'el Surcharge to print (new or old)'),TIP('Type of Fuel Surcharge to print (new or old)')
                           BUTTON('&Save Options'),AT(88,150,65,15),USE(?Button_Save),TIP('Save options now.<0DH,0AH>' & |
  'Options will also be saved if you print rates but not if you cancel.')
                         END
                       END
                       BUTTON('Cancel'),AT(139,170,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Rates Report'),AT(198,1115,8000,9302),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(198,250,,844),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Rates'),AT(3573,52),USE(?ReportTitle),FONT('Arial',28,,FONT:bold,CHARSET:ANSI),CENTER
                         IMAGE('fbn_logo_small.jpg'),AT(52,52,2300,460),USE(?Image1)
                         STRING(@s100),AT(2396,615,2917,208),USE(CLI:ClientName,,?CLI:ClientName:2),FONT(,10,,FONT:regular, |
  CHARSET:ANSI),TRN
                         STRING('Client:'),AT(1906,615),USE(?String20:2),FONT(,10,,FONT:regular,CHARSET:ANSI),TRN
                         STRING(@n_10b),AT(5323,615,,208),USE(CLI:ClientNo,,?CLI:ClientNo:2),FONT(,10,,FONT:regular, |
  CHARSET:ANSI),RIGHT(4),TRN
                         STRING(@d6b),AT(896,615),USE(L_OG:EffectiveDateAtDate_Rpt),RIGHT(1)
                         STRING('Effective Date:'),AT(104,615),USE(?String62),TRN
                         CHECK(' Effective Rates Only'),AT(6375,615),USE(L_OG:EffectiveRatesOnly_Rpt)
                       END
break_client           BREAK(CLI:CID),USE(?BREAK1)
                         HEADER,AT(0,0,8000,1015),USE(?GROUPHEADER1),PAGEBEFORE(-1),WITHNEXT(1)
                           STRING(@s255),AT(1177,802,6552),USE(LOC:InsuranceRequired)
                           STRING(@d5b),AT(1063,313,677,167),USE(CLI:DateOpened),RIGHT(1)
                           STRING('Insurance:'),AT(365,802),USE(?String54),TRN
                           STRING(@s20),AT(4250,573,1354,156),USE(LOC:Terms),RIGHT(1),TRN
                           STRING(@n-17.2),AT(4615,313),USE(CLI:AccountLimit),RIGHT(1),TRN
                           STRING('Status:'),AT(6010,313),USE(?String28:2),TRN
                           STRING(@s20),AT(6583,313),USE(LOC:Client_Status)
                           STRING(@n-11.2),AT(1063,573),USE(LOC:VolumetricRatio),RIGHT(1)
                           STRING('Terms:'),AT(4021,573),USE(?String51),TRN
                           STRING(@n-11.2),AT(2896,313),USE(CLI:DocumentCharge),RIGHT(1)
                           STRING('Acc. Limit:'),AT(4021,313),USE(?String41:5),TRN
                           STRING('Branch:'),AT(6010,31),USE(?String28),TRN
                           STRING('Client:'),AT(365,31),USE(?String20),FONT(,10,,FONT:regular,CHARSET:ANSI),TRN
                           STRING(@s100),AT(938,31,4010,208),USE(CLI:ClientName),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@s35),AT(6583,31,1354,167),USE(BRA:BranchName)
                           STRING('Date Opened:'),AT(365,313),USE(?String41),TRN
                           STRING('Docs. Charge:'),AT(2115,313),USE(?String41:3),TRN
                           STRING('Vol. Ratio:'),AT(365,573),USE(?String41:2),TRN
                           STRING(@n_10b),AT(4656,31,1094,208),USE(CLI:ClientNo),FONT(,10,,FONT:bold,CHARSET:ANSI),RIGHT(4), |
  TRN
                         END
detail_stats             DETAIL,AT(0,0,,1333),USE(?detail_stats)
                           LINE,AT(104,31,7813,0),USE(?Line67),COLOR(COLOR:Black)
                           STRING(@n6),AT(1313,813,688,167),USE(L_RG:Tonnage_Rank_Year),RIGHT(1)
                           STRING('Tonnage Rank All:'),AT(104,1042),USE(?String41:13),TRN
                           STRING(@n6),AT(1313,1042,688,167),USE(L_RG:Tonnage_Rank_All),RIGHT(1)
                           STRING('Income Rank All:'),AT(2729,1042),USE(?String41:14),TRN
                           STRING('Tonnage for Year:'),AT(104,73),USE(?String41:4),TRN
                           STRING(@n11.0),AT(1313,73),USE(L_RG:T_Year),DECIMAL(12)
                           STRING(@n-11.0),AT(3938,73),USE(L_RG:Income_Year),DECIMAL(12)
                           STRING(@n6),AT(6813,73,615,167),USE(L_RG:Average_Rate_Rank_Year),RIGHT(1)
                           STRING('Income Rank for Year:'),AT(2729,813),USE(?String41:12),TRN
                           STRING('Tonnage Rank for Year:'),AT(104,813),USE(?String41:11),TRN
                           STRING('Income for Year:'),AT(2729,73),USE(?String41:8),TRN
                           STRING('Tonnage for 6 Mths.:'),AT(104,313),USE(?String41:6),TRN
                           STRING(@n11.0),AT(1313,313,688,167),USE(L_RG:T_6Months),DECIMAL(12)
                           STRING('Income for 6 Mths.:'),AT(2729,313),USE(?String41:9),TRN
                           STRING(@n-11.0),AT(3938,313,656,167),USE(L_RG:Income_6Months),DECIMAL(12)
                           STRING('Avg. Rate Rank Year:'),AT(5688,73),USE(?String41:15),TRN
                           STRING(@n6),AT(3938,813,656,167),USE(L_RG:Income_Rank_Year),RIGHT(1)
                           STRING('Avg. Rate All:'),AT(5688,781),USE(?String41:18),TRN
                           STRING(@n-10.2),AT(6813,781,615,167),USE(L_RG:Rate_Average_All),RIGHT(1)
                           STRING(@n6),AT(3938,1042,656,167),USE(L_RG:Income_Rank_All),RIGHT(1)
                           LINE,AT(104,1250,7813,0),USE(?Line67:2),COLOR(COLOR:Black)
                           STRING(@n6),AT(6813,313,615,167),USE(L_RG:Average_Rate_Rank_All),RIGHT(1)
                           STRING(@n11.0),AT(1313,563,688,167),USE(L_RG:T_AvgYear),DECIMAL(12)
                           STRING('Tonnage Avg. / Year:'),AT(104,563),USE(?String41:7),TRN
                           STRING(@n-11.0),AT(3938,563,656,167),USE(L_RG:Income_AvgYear),DECIMAL(12)
                           STRING('Avg. Rate Year:'),AT(5688,563),USE(?String41:17),TRN
                           STRING(@n-10.2),AT(6813,563),USE(L_RG:Rate_Average_Year),RIGHT(1)
                           STRING('Avg. Rate Rank All:'),AT(5688,313),USE(?String41:16),TRN
                           STRING('Income Avg. / Year:'),AT(2729,563),USE(?String41:10),TRN
                         END
detail_cp_head           DETAIL,AT(0,0,,479),USE(?detail_cp_head)
                           STRING('Container Park Discounts'),AT(365,52,5469,156),USE(?String24),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           STRING('Effective Date'),AT(83,292,1250,156),USE(?HeaderTitle:5),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Floor'),AT(1646,292,1500,170),USE(?HeaderTitle:8),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Discount %'),AT(3844,292,1500,170),USE(?HeaderTitle:10),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Minimium Charge'),AT(5448,292,1500,170),USE(?HeaderTitle:11),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           BOX,AT(0,260,7600,220),USE(?HeaderBox:2),COLOR(COLOR:Black),LINEWIDTH(1)
                           LINE,AT(0,260,0,220),USE(?DetailcpLine:0),COLOR(COLOR:Black)
                           LINE,AT(1604,260,0,220),USE(?DetailcpLine:1),COLOR(COLOR:Black)
                           LINE,AT(3760,260,0,220),USE(?DetailcpLine:2),COLOR(COLOR:Black)
                           LINE,AT(5385,260,0,220),USE(?DetailcpLine:6),COLOR(COLOR:Black)
                           LINE,AT(8000,260,0,220),USE(?DetailcpLine:5),COLOR(COLOR:Black)
                         END
detail_cp                DETAIL,AT(0,0,7979,188),USE(?detail_container_parks)
                           LINE,AT(0,0,0,188),USE(?DetailcpLine:01),COLOR(COLOR:Black)
                           LINE,AT(1604,0,0,188),USE(?DetailcpLine:11),COLOR(COLOR:Black)
                           LINE,AT(3760,0,0,188),USE(?DetailcpLine:21),COLOR(COLOR:Black)
                           LINE,AT(5385,0,0,188),USE(?DetailcpLine:61),COLOR(COLOR:Black)
                           LINE,AT(7594,0,0,188),USE(?DetailcpLine:41),COLOR(COLOR:Black)
                           LINE,AT(8000,0,0,188),USE(?DetailcpLine:51),COLOR(COLOR:Black)
                           STRING(@d5),AT(83,21,1500,167),USE(CLI_CP:Effective_Date),LEFT
                           STRING(@s35),AT(1646,21),USE(FLO:Floor),TRN
                           STRING(@n7.2),AT(3844,21,1500,167),USE(CLI_CP:ContainerParkRateDiscount),RIGHT(1)
                           STRING(@n-13.2),AT(5448,21,1500,167),USE(CLI_CP:ContainerParkMinimium),RIGHT(1)
                           LINE,AT(0,187,8000,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         END
detail_cp_rates_hdr      DETAIL,AT(0,0,,490),USE(?detail_cp_rates_hdr)
                           STRING(@s35),AT(2083,52),USE(FLO:Floor,,?FLO:Floor:2),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING('Container Park Rates:'),AT(365,52,1354,156),USE(?String69),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           BOX,AT(0,260,7600,220),USE(?HeaderBox2:2),COLOR(COLOR:Black),LINEWIDTH(1)
                           LINE,AT(1604,260,0,208),USE(?HeaderLine:12),COLOR(COLOR:Black)
                           LINE,AT(3198,260,0,208),USE(?HeaderLine:22),COLOR(COLOR:Black)
                           LINE,AT(4802,260,0,208),USE(?HeaderLine:32),COLOR(COLOR:Black)
                           STRING('Discounted Rate Per Kg'),AT(4854,271,1500,170),USE(?HeaderTitle:13),FONT(,,,FONT:bold, |
  CHARSET:ANSI),LEFT(1),TRN
                           STRING('Effective Date'),AT(83,271,1250,156),USE(?HeaderTitle:123),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('To Mass'),AT(1646,271,1500,170),USE(?HeaderTitle:23),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Rate Per Kg'),AT(3250,271,1500,170),USE(?HeaderTitle:14),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                         END
detail_cp_rates          DETAIL,AT(0,0,,177),USE(?detail_cp_rates)
                           LINE,AT(0,0,0,188),USE(?DetailLine:0),COLOR(COLOR:Black)
                           LINE,AT(1604,0,0,188),USE(?DetailLine:1),COLOR(COLOR:Black)
                           LINE,AT(3198,0,0,188),USE(?DetailLine:2),COLOR(COLOR:Black)
                           LINE,AT(4802,0,0,188),USE(?DetailLine:3),COLOR(COLOR:Black)
                           LINE,AT(7594,0,0,188),USE(?DetailLine:4),COLOR(COLOR:Black)
                           LINE,AT(8000,0,0,188),USE(?DetailLine:5),COLOR(COLOR:Black)
                           STRING(@n-12.0),AT(1646,21,1500,170),USE(CPRA:ToMass),RIGHT
                           STRING(@n-13.2),AT(3250,21,1500,170),USE(CPRA:RatePerKg),RIGHT(1)
                           STRING(@n-13.2),AT(4854,21,1500,167),USE(LOC:Discounted_Rate_Per_Kg),RIGHT(1)
                           STRING(@d5),AT(83,21,1250,156),USE(CPRA:Effective_Date),LEFT
                           LINE,AT(0,187,8000,0),USE(?DetailEndLine3),COLOR(COLOR:Black)
                         END
detail_ad_head           DETAIL,AT(0,0,,479),USE(?detail_ad_head)
                           STRING('Additional Charges'),AT(365,52,5469,156),USE(?String24:2),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           STRING('Description'),AT(1646,292,1500,170),USE(?HeaderTitle:7),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Charge'),AT(5448,292,1500,170),USE(?HeaderTitle:9),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Effective Date'),AT(83,292,1250,156),USE(?HeaderTitle:6),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           BOX,AT(0,260,7600,220),USE(?HeaderBox:3),COLOR(COLOR:Black),LINEWIDTH(1)
                           LINE,AT(0,260,0,220),USE(?DetailcpLine:02),COLOR(COLOR:Black)
                           LINE,AT(1604,260,0,220),USE(?DetailcpLine:12),COLOR(COLOR:Black)
                           LINE,AT(3760,260,0,220),USE(?DetailcpLine:22),COLOR(COLOR:Black)
                           LINE,AT(5385,260,0,220),USE(?DetailcpLine:62),COLOR(COLOR:Black)
                           LINE,AT(8000,260,0,220),USE(?DetailcpLine:52),COLOR(COLOR:Black)
                         END
detail_ad                DETAIL,AT(0,0,,188),USE(?detail_additional)
                           LINE,AT(0,0,0,188),USE(?DetailcpLine:03),COLOR(COLOR:Black)
                           LINE,AT(1604,0,0,188),USE(?DetailcpLine:13),COLOR(COLOR:Black)
                           LINE,AT(3760,0,0,188),USE(?DetailcpLine:23),COLOR(COLOR:Black)
                           LINE,AT(5385,0,0,188),USE(?DetailcpLine:63),COLOR(COLOR:Black)
                           LINE,AT(7594,0,0,188),USE(?DetailcpLine:43),COLOR(COLOR:Black)
                           LINE,AT(8000,0,0,188),USE(?DetailcpLine:53),COLOR(COLOR:Black)
                           STRING(@d5),AT(83,21,1500,167),USE(CARA:Effective_Date),LEFT
                           STRING(@n-15.2),AT(5448,21,1500,167),USE(CARA:Charge),RIGHT(1)
                           STRING(@s35),AT(1646,21),USE(ACCA:Description),TRN
                           LINE,AT(0,187,8000,0),USE(?DetailEndLine2),COLOR(COLOR:Black)
                         END
detail_fs_head           DETAIL,AT(0,0,,479),USE(?detail_fs_head)
                           STRING('Fuel Surcharge'),AT(365,52,5469,156),USE(?String24:23),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           STRING('Description'),AT(1646,292,1500,170),USE(?HeaderTitle:73),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Charge'),AT(5448,292,1500,170),USE(?HeaderTitle:93),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Effective Date'),AT(83,292,1250,156),USE(?HeaderTitle:63),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           BOX,AT(0,260,7600,220),USE(?HeaderBox:4),COLOR(COLOR:Black),LINEWIDTH(1)
                           LINE,AT(0,260,0,220),USE(?DetailcpLine:023),COLOR(COLOR:Black)
                           LINE,AT(1604,260,0,220),USE(?DetailcpLine:123),COLOR(COLOR:Black)
                           LINE,AT(3760,260,0,220),USE(?DetailcpLine:223),COLOR(COLOR:Black)
                           LINE,AT(5385,260,0,220),USE(?DetailcpLine:623),COLOR(COLOR:Black)
                           LINE,AT(8000,260,0,220),USE(?DetailcpLine:523),COLOR(COLOR:Black)
                         END
detail_fs                DETAIL,AT(0,0,,188),USE(?detail_fuelsurcharge)
                           LINE,AT(0,0,0,188),USE(?DetailcpLine:033),COLOR(COLOR:Black)
                           LINE,AT(1604,0,0,188),USE(?DetailcpLine:133),COLOR(COLOR:Black)
                           LINE,AT(3760,0,0,188),USE(?DetailcpLine:233),COLOR(COLOR:Black)
                           LINE,AT(5385,0,0,188),USE(?DetailcpLine:633),COLOR(COLOR:Black)
                           LINE,AT(7594,0,0,188),USE(?DetailcpLine:433),COLOR(COLOR:Black)
                           LINE,AT(8000,0,0,188),USE(?DetailcpLine:533),COLOR(COLOR:Black)
                           STRING(@d5),AT(83,21,1500,167),USE(FSRA:Effective_Date),LEFT
                           STRING(@N~% ~-10.2),AT(5448,21,1500,167),USE(FSRA:FuelSurcharge),RIGHT(1)
                           LINE,AT(0,187,8000,0),USE(?DetailEndLine23),COLOR(COLOR:Black)
                         END
detail_fc_head           DETAIL,AT(0,0,,479),USE(?detail_fc_head)
                           STRING('Fuel Surcharge (c)'),AT(365,52,5469,156),USE(?String24:4),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           STRING('Charge'),AT(5448,292,1500,170),USE(?HeaderTitle),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           STRING('Effective Date'),AT(83,292,1250,156),USE(?HeaderTitle:16),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           BOX,AT(0,260,7600,220),USE(?HeaderBox:5),COLOR(COLOR:Black),LINEWIDTH(1)
                           LINE,AT(0,260,0,220),USE(?DetailcpLine:023:2),COLOR(COLOR:Black)
                           LINE,AT(1604,260,0,220),USE(?DetailcpLine:3),COLOR(COLOR:Black)
                           LINE,AT(3760,260,0,220),USE(?DetailcpLine:4),COLOR(COLOR:Black)
                           LINE,AT(5385,260,0,220),USE(?DetailcpLine:7),COLOR(COLOR:Black)
                           LINE,AT(8000,260,0,220),USE(?DetailcpLine:8),COLOR(COLOR:Black)
                         END
detail_fc                DETAIL,AT(0,0,,188),USE(?detail_fuelcost)
                           LINE,AT(0,0,0,188),USE(?DetailcpLine:033:2),COLOR(COLOR:Black)
                           LINE,AT(1604,0,0,188),USE(?DetailcpLine:9),COLOR(COLOR:Black)
                           LINE,AT(3760,0,0,188),USE(?DetailcpLine:10),COLOR(COLOR:Black)
                           LINE,AT(5385,0,0,188),USE(?DetailcpLine:14),COLOR(COLOR:Black)
                           LINE,AT(7594,0,0,188),USE(?DetailcpLine:15),COLOR(COLOR:Black)
                           LINE,AT(8000,0,0,188),USE(?DetailcpLine:16),COLOR(COLOR:Black)
                           STRING(@d5),AT(83,21,1500,167),USE(FCRA:Effective_Date),LEFT
                           STRING(@N~% ~-10.2),AT(5448,21,1500,167),USE(FCRA:FuelSurcharge),RIGHT(1)
                           LINE,AT(0,187,8000,0),USE(?DetailEndLine23:2),COLOR(COLOR:Black)
                         END
detail_tc_head           DETAIL,AT(0,0,,479),USE(?detail_tc_head)
                           STRING('Toll Charge'),AT(365,52,5469,156),USE(?String24:3),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           STRING('Charge'),AT(5448,292,1500,170),USE(?HeaderTitle:2),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Effective Date'),AT(83,292,1250,156),USE(?HeaderTitle:3),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           BOX,AT(0,260,7600,220),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                           LINE,AT(0,260,0,220),USE(?DetailcpLine:023:3),COLOR(COLOR:Black)
                           LINE,AT(1604,260,0,220),USE(?DetailcpLine:17),COLOR(COLOR:Black)
                           LINE,AT(3760,260,0,220),USE(?DetailcpLine:18),COLOR(COLOR:Black)
                           LINE,AT(5385,260,0,220),USE(?DetailcpLine:19),COLOR(COLOR:Black)
                           LINE,AT(8000,260,0,220),USE(?DetailcpLine:20),COLOR(COLOR:Black)
                         END
detail_tc                DETAIL,AT(0,0,,188),USE(?detail_tollcost)
                           LINE,AT(0,0,0,188),USE(?DetailcpLine:033:3),COLOR(COLOR:Black)
                           LINE,AT(1604,0,0,188),USE(?DetailcpLine:24),COLOR(COLOR:Black)
                           LINE,AT(3760,0,0,188),USE(?DetailcpLine:25),COLOR(COLOR:Black)
                           LINE,AT(5385,0,0,188),USE(?DetailcpLine:26),COLOR(COLOR:Black)
                           LINE,AT(7594,0,0,188),USE(?DetailcpLine:27),COLOR(COLOR:Black)
                           LINE,AT(8000,0,0,188),USE(?DetailcpLine:28),COLOR(COLOR:Black)
                           STRING(@d5),AT(83,21,1500,167),USE(TOL:Effective_Date),LEFT
                           STRING(@N~% ~-10.2),AT(5448,21,1500,167),USE(TOL:TollRate),RIGHT(1)
                           LINE,AT(0,187,8000,0),USE(?DetailEndLine23:3),COLOR(COLOR:Black)
                         END
detail_genhdr            DETAIL,AT(0,0,,333),USE(?detail_genhdr)
                           STRING('General Rates'),AT(365,83,5469,156),USE(?String24:5),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                         END
detail_genhdrcol         DETAIL,AT(0,0,,219),USE(?detail_genhdrcol)
                           BOX,AT(0,0,7600,219),USE(?HeaderBox:6),COLOR(COLOR:Black),LINEWIDTH(1)
                           LINE,AT(1604,0,0,219),USE(?HeaderLine:1),COLOR(COLOR:Black)
                           LINE,AT(3198,0,0,219),USE(?HeaderLine:2),COLOR(COLOR:Black)
                           LINE,AT(4802,0,0,219),USE(?HeaderLine:3),COLOR(COLOR:Black)
                           LINE,AT(6396,0,0,219),USE(?HeaderLine:4),COLOR(COLOR:Black)
                           STRING('Effective Date'),AT(83,35,1250,156),USE(?HeaderTitle:12),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('To Mass'),AT(1650,35,1500,170),USE(?HeaderTitle:4),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Rate Per Kg'),AT(3250,35,1500,170),USE(?HeaderTitle:15),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Minimium Charge'),AT(4850,35,1500,170),USE(?HeaderTitle:17),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                         END
break_journey            BREAK(RAT:JID),USE(?BREAK2)
                           HEADER,AT(0,0,8000,420),USE(?GROUPHEADER2),WITHNEXT(1)
                             STRING('Journey'),AT(417,42),USE(?String17),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                             TEXT,AT(948,42,6656),USE(LOC:Journey),RESIZE
                           END
break_clientratetype       BREAK(RAT:CRTID),USE(?BREAK3)
                             HEADER,AT(0,0,,698),USE(?GROUPHEADER3),WITHNEXT(1)
                               STRING('Client Rate Type'),AT(698,42),USE(?String19),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                               STRING(@s80),AT(1813,42,6198,167),USE(CRT:ClientRateType),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                               STRING(@s100),AT(1813,240,5823,167),USE(LOAD2:LoadType),FONT(,,,FONT:bold,CHARSET:ANSI)
                               BOX,AT(0,458,7600,220),USE(?HeaderBox2),COLOR(COLOR:Black),LINEWIDTH(1)
                               LINE,AT(1604,458,0,219),USE(?HeaderLine:123),COLOR(COLOR:Black)
                               LINE,AT(3198,458,0,219),USE(?HeaderLine:223),COLOR(COLOR:Black)
                               LINE,AT(4802,458,0,219),USE(?HeaderLine:323),COLOR(COLOR:Black)
                               LINE,AT(6396,458,0,219),USE(?HeaderLine:423),COLOR(COLOR:Black)
                               STRING('Load Type'),AT(698,250),USE(?String19:2),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                               STRING('Effective Date'),AT(83,500,1250,156),USE(?HeaderTitle:1234),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                               STRING('To Mass'),AT(1646,500,1500,170),USE(?HeaderTitle:234),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                               STRING('Rate Per Kg'),AT(3250,500,1500,170),USE(?HeaderTitle:33),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                               STRING('Minimium Charge'),AT(4854,500,1500,170),USE(?HeaderTitle:43),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                             END
Detail                       DETAIL,AT(0,0,,188),USE(?Detail)
                               LINE,AT(0,0,0,188),USE(?DetailLine:03),COLOR(COLOR:Black)
                               LINE,AT(1604,0,0,188),USE(?DetailLine:13),COLOR(COLOR:Black)
                               LINE,AT(3198,0,0,188),USE(?DetailLine:23),COLOR(COLOR:Black)
                               LINE,AT(4802,0,0,188),USE(?DetailLine:33),COLOR(COLOR:Black)
                               LINE,AT(6396,0,0,188),USE(?DetailLine:43),COLOR(COLOR:Black)
                               LINE,AT(7594,0,0,188),USE(?DetailLine:6),COLOR(COLOR:Black)
                               LINE,AT(8000,0,0,188),USE(?DetailLine:53),COLOR(COLOR:Black)
                               STRING(@n-12.0),AT(1646,21,1500,170),USE(RAT:ToMass),RIGHT
                               STRING(@n-13.2),AT(3250,21,1500,170),USE(RAT:RatePerKg),RIGHT(1)
                               STRING(@n-14.2b),AT(4854,21,1500,170),USE(RAT:MinimiumCharge),RIGHT
                               CHECK(' Ad Hoc'),AT(6781,10),USE(RAT:AdHoc)
                               STRING(@d5),AT(83,21,1250,156),USE(RAT:Effective_Date),LEFT
                               LINE,AT(0,187,8000,0),USE(?DetailEndLine34),COLOR(COLOR:Black)
                             END
                           END
                         END
                         FOOTER,AT(0,0,,208),USE(?GROUPFOOTER1)
                           LINE,AT(875,62,6000,0),USE(?Line39),COLOR(COLOR:Black),LINEWIDTH(15)
                         END
                       END
                       FOOTER,AT(198,10500,,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
BreakMgr             CLASS(BreakManagerClass)              ! Break Manager
TakeEnd                PROCEDURE(SHORT BreakId,SHORT LevelId),DERIVED
                     END

TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar4            CalendarClass
Clients_View            VIEW(Clients_ContainerParkDiscounts)
       JOIN(FLO:PKey_FID, CLI_CP:FID)
       !PROJECT()
    .  .

View_CPs            ViewManager




Add_View            VIEW(__RatesAdditionalCharges)
        JOIN(ACCA:PKey_ACCID, CARA:ACCID)
    .   .

View_Add            ViewManager


FS_View             VIEW(__RatesFuelSurcharge)
    .
View_FS             ViewManager

FC_View             VIEW(__RatesFuelCost)
    .
View_FC             ViewManager

TC_View             VIEW(__RatesToll)
    .
View_TC             ViewManager


Rates_View              VIEW(__Rates)
    PROJECT(RAT:AdHoc)
    PROJECT(RAT:CRTID)
    PROJECT(RAT:CTID)
    PROJECT(RAT:Effective_Date)
    PROJECT(RAT:JID)
    PROJECT(RAT:LTID)
    PROJECT(RAT:MinimiumCharge)
    PROJECT(RAT:RatePerKg)
    PROJECT(RAT:ToMass)
       JOIN(CRT:PKey_CRTID,RAT:CRTID)
          PROJECT(CRT:ClientRateType)
          PROJECT(CRT:LTID)
          JOIN(LOAD2:PKey_LTID,CRT:LTID)
            PROJECT(LOAD2:LoadType)
       .  .
       JOIN(JOU:PKey_JID,RAT:JID)
          PROJECT(JOU:Description)
          PROJECT(JOU:Journey)
    .  .

View_Rates              ViewManager
View_Rates_CP       VIEW(__RatesContainerPark)
       JOIN(FLO:PKey_FID, CPRA:FID)
          PROJECT(FLO:Print_Rates)
    .  .

Rates_CP_View       ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CP_Rates                        ROUTINE
    LOC:Printed_CP_Head = FALSE
    CLEAR(LOC:FID)

    View_CPs.Init(Clients_View, Relate:Clients_ContainerParkDiscounts)
    View_CPs.AddSortOrder(CLI_CP:FKey_CID)
    View_CPs.AppendOrder('CLI_CP:FID,-CLI_CP:Effective_Date')
    !View_CPs.AddRange(CLI_CP:CID, LOC:CID)
    View_CPs.SetFilter('CLI_CP:CID = ' & LOC:CID)

    IF L_OG:EffectiveRatesOnly = TRUE
       View_CPs.SetFilter('CLI_CP:Effective_Date < ' & L_OG:EffectiveDateAtDate + 1,'2')
    .

    View_CPs.Reset()
    LOOP
       IF View_CPs.Next() ~= LEVEL:Benign
          BREAK
       .

       ! Only want latest Rate from each Floor
       ! ***************   should implement a queue check like on Validate Record for this and Additionals ??????


       IF L_OG:EffectiveRatesOnly = TRUE
          IF LOC:FID = CLI_CP:FID
             CYCLE
          .
          LOC:FID   = CLI_CP:FID
       .

       ! We want this one
       IF LOC:Printed_CP_Head = FALSE
          PRINT(RPT:detail_cp_head)
          LOC:Printed_CP_Head   = TRUE
       .
       PRINT(RPT:detail_cp)

       L_CPQ:FID                        = CLI_CP:FID
       L_CPQ:ContainerParkRateDiscount  = CLI_CP:ContainerParkRateDiscount
       ADD(L_CPQ:Container_Parks_Discount_Q)

       db.debugout('[Print_Rates]   CP_Rates - Print - RPT:detail_cp')
    .

    View_CPs.Kill()
    EXIT
AD_Rates                        ROUTINE
    LOC:Printed_AD_Head = FALSE
    CLEAR(LOC:ACCID)

    View_Add.Init(Add_View, Relate:__RatesAdditionalCharges)
    ! C6.1 bug???  the following produces a string order by clause for the date!  Check if fixed in 6.2....?????
    !View_Add.AddSortOrder(CARA:CKey_CID_EffDate)
    !View_Add.AppendOrder('CLI:FKey_BID')
    !View_Add.AddRange(CARA:CID, LOC:CID)
    !View_Add.SetFilter('')

    View_Add.AddSortOrder(CARA:FKey_CID)
    View_Add.AppendOrder('ACCA:Description,-CARA:Effective_Date')

    View_Add.SetFilter('CARA:CID = ' & LOC:CID)

    IF L_OG:EffectiveRatesOnly = TRUE
       View_Add.SetFilter('CARA:Effective_Date < ' & L_OG:EffectiveDateAtDate + 1,'2')
    .

    View_Add.Reset()
    LOOP
       IF View_Add.Next() ~= LEVEL:Benign
          BREAK
       .

       ! Only want latest effective additional charge
       IF L_OG:EffectiveRatesOnly = TRUE
          IF CARA:ACCID = LOC:ACCID
             CYCLE
          .
          LOC:ACCID  = CARA:ACCID
       .

       ! We want this one
       IF LOC:Printed_AD_Head = FALSE
          PRINT(RPT:detail_ad_head)
          LOC:Printed_AD_Head = TRUE
       .
       PRINT(RPT:detail_ad)
    .

    View_Add.Kill()
    EXIT
FS_Rates                        ROUTINE
    LOC:Printed_FS_Head = FALSE
!    CLEAR(LOC:ACCID)
    CLEAR(FSRA:Record)

    View_FS.Init(FS_View, Relate:__RatesFuelSurcharge)

    View_FS.AddSortOrder(FSRA:FKey_CID)
    View_FS.AppendOrder('-FSRA:Effective_Date')

    View_FS.SetFilter('FSRA:CID = ' & LOC:CID)

    IF L_OG:EffectiveRatesOnly = TRUE
       View_FS.SetFilter('FSRA:Effective_Date < ' & L_OG:EffectiveDateAtDate + 1,'2')
    .

    View_FS.Reset()
    LOOP
       IF View_FS.Next() ~= LEVEL:Benign
          BREAK
       .

       ! We want this one
       IF LOC:Printed_FS_Head = FALSE
          PRINT(RPT:detail_fs_head)
          LOC:Printed_FS_Head = TRUE
       .
       PRINT(RPT:detail_fs)

       ! Only want latest effective additional charge
       BREAK
    .

    View_FS.Kill()
    EXIT
FC_Rates                        ROUTINE         ! Fuel rates surcharge (new)
    DATA
R:CID       ULONG

    CODE
    LOC:Printed_FS_Head = FALSE
    CLEAR(FCRA:Record)

    View_FC.Init(FC_View, Relate:__RatesFuelCost)

    View_FC.AddSortOrder(FCRA:FKey_CID)
    View_FC.AppendOrder('-FCRA:Effective_Date')

    ! Get_Client_FuelSurcharge
    ! (p:CID, p:Date, p:FuelBaseRate, p:CID_Used)
    ! (ULONG, LONG=0, <*DECIMAL>, <ULONG>),STRING
    Get_Client_FuelSurcharge(LOC:CID, TODAY(),, R:CID)
    ! R:CID     - CID should be the passed in CID or the base rate CID
    IF CLI:CID = R:CID
    ELSE
    .
    IF R:CID = 0
       R:CID    = LOC:CID
    .

    View_FC.SetFilter('FCRA:CID = ' & R:CID)

    IF L_OG:EffectiveRatesOnly = TRUE
       View_FC.SetFilter('FCRA:Effective_Date < ' & L_OG:EffectiveDateAtDate + 1,'2')
    .

    View_FC.Reset()
    LOOP
       IF View_FC.Next() ~= LEVEL:Benign
          BREAK
       .

       ! We want this one
       IF LOC:Printed_FS_Head = FALSE
          PRINT(RPT:detail_fc_head)
          LOC:Printed_FS_Head = TRUE
       .

       IF FCRA:FuelSurcharge < 0.0
          FCRA:FuelSurcharge    = 0.0
       .

       PRINT(RPT:detail_fc)

       ! Only want latest effective additional charge
       BREAK
    .

    View_FC.Kill()
    EXIT
TC_Rates                        ROUTINE
    DATA
R:CID       ULONG

    CODE
    LOC:Printed_TC_Head = FALSE
    CLEAR(TOL:Record)

    View_TC.Init(TC_View, Relate:__RatesToll)

    View_TC.AddSortOrder(TOL:FKey_CID)
    View_TC.AppendOrder('-TOL:Effective_Date')

    ! Get_Client_FuelSurcharge
    ! (p:CID, p:Date, p:FuelBaseRate, p:CID_Used)
    ! (ULONG, LONG=0, <*DECIMAL>, <ULONG>),STRING
    Get_Client_ETolls(LOC:CID, TODAY(), R:CID)
    ! R:CID     - CID should be the passed in CID or the base rate CID
    IF CLI:CID = R:CID
    ELSE
    .
    IF R:CID = 0
       R:CID    = LOC:CID
    .

    View_TC.SetFilter('TOL:CID = ' & R:CID)

    IF L_OG:EffectiveRatesOnly = TRUE
       View_TC.SetFilter('TOL:Effective_Date < ' & L_OG:EffectiveDateAtDate + 1,'2')
    .

    View_TC.Reset()
    LOOP
       IF View_TC.Next() ~= LEVEL:Benign
          BREAK
       .

       ! We want this one
       IF LOC:Printed_TC_Head = FALSE
          PRINT(RPT:detail_tc_head)
          LOC:Printed_TC_Head = TRUE
       .

!       IF TOL:TollRate < 0.0
!          TOL:TollRate    = 0.0
!       .

       PRINT(RPT:detail_tc)

       ! Only want latest effective additional charge
       BREAK
    .

    View_TC.Kill()
    EXIT
Print_Others                     ROUTINE
    IF LOC:CID ~= CLI:CID
       !CLEAR(LOC:Print_Rates)
       !IF RAT:CID = CLI:CID
          !LOC:Print_Rates   = TRUE
       !.

       LOC:CID      = CLI:CID

       LOC:VolumetricRatio  = (1000 / CLI:VolumetricRatio)

      EXECUTE CLI:LiabilityCover + 1
        LOC:InsuranceRequired   = 'None'
        LOC:InsuranceRequired   = 'Own Insurance - No Liability Cover'
        LOC:InsuranceRequired   = 'Liability cover for loss attributed to Negligence of FBN'
        LOC:InsuranceRequired   = 'Liability cover for loss however caused'
      .
      EXECUTE CLI:LiabilityCrossBorder + 1
        LOC:InsuranceRequired   = CLIP(LOC:InsuranceRequired)     ! none
        LOC:InsuranceRequired   = CLIP(LOC:InsuranceRequired) & '          Cross Border - Within RSA Borders'
        LOC:InsuranceRequired   = CLIP(LOC:InsuranceRequired) & '          Cross Border - Door to door'
      .
      
      IF FALSE   ! exclude this for now - liability is new mode 20 Sep 12
       EXECUTE CLI:InsuranceRequired + 1        ! None|Included in Rate|Per DI
          LOC:InsuranceRequired     = 'Excluding GIT'
          LOC:InsuranceRequired     = 'Including GIT'
          LOC:InsuranceRequired     = 'Per DI'
       .

       EXECUTE CLI:Terms + 1                    ! Pre Paid|COD|Account
          LOC:Terms                 = 'Pre Paid'
          LOC:Terms                 = 'COD'
          LOC:Terms                 = 'Account ' & CLI:PaymentPeriod & ' days'
          LOC:Terms                 = 'On Statement'
       .
      .
      
       IF CLI:Terms = 2 OR CLI:Terms = 3
          IF CLI:Terms = 2 AND CLI:PaymentPeriod = 0
             LOC:Terms              = 'Account 30 days'
          .

          IF LOC:Terms_Bold         = TRUE
             SETTARGET(REPORT)
             ?LOC:Terms{PROP:Font,4} = FONT:Regular
             LOC:Terms_Bold         = FALSE
             SETTARGET()
          .
       ELSE
          SETTARGET(REPORT)
          ?LOC:Terms{PROP:Font,4}   = FONT:Bold
          LOC:Terms_Bold            = TRUE
          SETTARGET()
       .

       DO AD_Rates

       IF L_OG:FuelSurcharge = 1
          DO FS_Rates
       ELSE
          DO FC_Rates
       .
         
       DO TC_Rates

       DO CP_Rates


       IF LOC:Printed_CP_Head = TRUE OR LOC:Printed_AD_Head = TRUE OR LOC:Printed_FS_Head = TRUE   |        ! Then print the Heading
               OR LOC:Printed_TC_Head = TRUE
          PRINT(RPT:detail_genhdr)
       .

       !PRINT(RPT:detail_genhdrcol)
    .
    EXIT
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Client Name' & |
      '|' & 'By Client No.' & |
      '|' & 'By Branch & Client Name' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Rates')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
      L_OG:EffectiveRatesOnly     = GETINI('Print_Rates', 'EffectiveRatesOnly', 1, GLO:Local_INI)
      L_OG:EffectiveDateAtDate    = GETINI('Print_Rates', 'EffectiveDateAtDate', TODAY(), GLO:Local_INI)
  
      L_OG:CreateLog              = GETINI('Print_Rates', 'CreateLog', 0, GLO:Local_INI)
  
  
      IF L_OG:EffectiveDateAtDate <= 1
         L_OG:EffectiveDateAtDate = TODAY()
      .
  
  
  
      L_OG:EffectiveDateAtDate_Rpt    = L_OG:EffectiveDateAtDate
      L_OG:EffectiveRatesOnly_Rpt     = L_OG:EffectiveRatesOnly
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Pause
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:EffectiveDateAtDate',L_OG:EffectiveDateAtDate) ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        IF p:Option ~= 1
           CASE MESSAGE('Would you like to see the options?', 'Print Rates', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
           OF BUTTON:Yes
              p:Option = 1
        .  .
      ! When single client, ask about effective date if not today
      IF DEFORMAT(p:CID) ~= 0 AND p:Option = 0
         IF L_OG:EffectiveRatesOnly = TRUE AND L_OG:EffectiveDateAtDate ~= TODAY()
            CASE MESSAGE('Only printing effective rates but the Effective date to Print As Of is not today.||' & |
                  'Effective Date for print: ' & FORMAT(L_OG:EffectiveDateAtDate, @d6) & '||' & |
                  'Would you like to use this date or today?||Note you can go and change the effective date for the future' & |
                  ' by going Browse -> Debtors -> Reports -> Rates Report where you will be given an option.', 'Rates Print', ICON:Question, |
                  FORMAT(L_OG:EffectiveDateAtDate,@d6) & '|Today', 2)
            OF 2
               L_OG:EffectiveDateAtDate   = TODAY()
      .  .  .
      ! Ask about Journey or Journey Description
      CASE MESSAGE('Would you like to see the Journey or the Journey Description?', 'Print Rates', ICON:Question,'Journey|Description', 1)
      OF 2
         LOC:Journey_Type = 1
      .
    IF p:CID = 0 OR p:Option = 1
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
    .
  Relate:AdditionalCharges.SetOpenRelated()
  Relate:AdditionalCharges.Open                            ! File AdditionalCharges used by this procedure, so make sure it's RelationManager is open
  Relate:__RatesToll.Open                                  ! File __RatesToll used by this procedure, so make sure it's RelationManager is open
  Access:Clients_ContainerParkDiscounts.UseFile            ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesContainerPark.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesAdditionalCharges.UseFile                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Floors.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__Rates.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ClientsRateTypes.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LoadTypes2.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesFuelSurcharge.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesFuelCost.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BreakMgr.Init()
  BreakMgr.AddBreak()
  BreakMgr.AddLevel() !ContainerPark_Discounts
  BreakMgr.AddResetField(CLI:CID)
  SELF.AddItem(BreakMgr)
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
      L_OG:Include_Stats  = GETINI('Print_Rates', 'Include_Stats', , GLO:Local_INI)
  
      L_OG:FuelSurcharge  = GETINI('Print_Rates', 'FuelSurcharge', , GLO:Local_INI)
  
      IF DEFORMAT(p:CID) ~= 0
         CLI:CID            = DEFORMAT(p:CID)
         IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
            ProcessSortSelectionVariable    = 'By Client No.'
      .  .
  
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Clients, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client Name')) THEN
     ThisReport.AppendOrder('+CLI:ClientName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientNo')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch & Client Name')) THEN
     ThisReport.AppendOrder('+BRA:BranchName,+CLI:ClientName')
  END
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = 'Printing Rates...'
  Relate:Clients.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      IF DEFORMAT(p:CID) ~= 0
         !Cid_#  = DEFORMAT(p:CID)
         ThisReport.SetFilter('CLI:CID=' & DEFORMAT(p:CID),'ikb')
      .
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
    IF p:CID = 0 OR p:Option = 1
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
    .
  
      IF L_OG:CreateLog = TRUE
         Add_Log('EffectiveRatesOnly: ' & L_OG:EffectiveRatesOnly & ',  Effective Date: ' & FORMAT(L_OG:EffectiveDateAtDate,@d6) & ',  CID: ' & p:CID, 'Rates', 'Print_Rates - ' & FORMAT(TODAY(), @d5-), 1)
         Add_Log('RAT:RID,RAT:CID,RAT:JID,RAT:LTID,RAT:CTID,RAT:CRTID,RAT:ToMass,RAT:RUBID,RAT:Effective_Date,RAT:RatePerKg,RAT:MinimiumCharge', 'Rates', 'Print_Rates - ' & FORMAT(TODAY(), @d5-), 0)
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AdditionalCharges.Close
    Relate:__RatesToll.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Pause
      ThisWindow.Update()
          PUTINI('Print_Rates', 'EffectiveRatesOnly', L_OG:EffectiveRatesOnly, GLO:Local_INI)
          PUTINI('Print_Rates', 'EffectiveDateAtDate', L_OG:EffectiveDateAtDate, GLO:Local_INI)
      
          PUTINI('Print_Rates', 'Include_Stats', L_OG:Include_Stats, GLO:Local_INI)
      
      
          L_OG:EffectiveDateAtDate_Rpt    = L_OG:EffectiveDateAtDate
          L_OG:EffectiveRatesOnly_Rpt     = L_OG:EffectiveRatesOnly
      
          ENABLE(?Tab_Process)
          SELECT(?Tab_Process)
          DISABLE(?Tab_Options)
          DISABLE(?Pause)
          
    OF ?Calendar
      ThisWindow.Update()
      Calendar4.SelectOnClose = True
      Calendar4.Ask('Select a Date',L_OG:EffectiveDateAtDate)
      IF Calendar4.Response = RequestCompleted THEN
      L_OG:EffectiveDateAtDate=Calendar4.SelectedDate
      DISPLAY(?L_OG:EffectiveDateAtDate)
      END
      ThisWindow.Reset(True)
    OF ?Button_Clear
      ThisWindow.Update()
          CLEAR(L_OG:EffectiveDateAtDate)
          DISPLAY()
    OF ?Button_Save
      ThisWindow.Update()
          PUTINI('Print_Rates', 'EffectiveRatesOnly', L_OG:EffectiveRatesOnly, GLO:Local_INI)
          PUTINI('Print_Rates', 'EffectiveDateAtDate', L_OG:EffectiveDateAtDate, GLO:Local_INI)
          
          PUTINI('Print_Rates', 'CreateLog', L_OG:CreateLog, GLO:Local_INI)
      
          PUTINI('Print_Rates', 'Include_Stats', L_OG:Include_Stats, GLO:Local_INI)
      
          PUTINI('Print_Rates', 'FuelSurcharge', L_OG:FuelSurcharge, GLO:Local_INI)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF p:CID = 0 OR p:Option = 1
             SELECT(?Tab_Options)
          ELSE
             ENABLE(?Tab_Process)
          .
      
      
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      !IF LOC:Print_Rates
      IF LOC:CID_VR ~= CLI:CID
         LOC:CID_VR           = CLI:CID
         LOC:VolumetricRatio  = (1000 / CLI:VolumetricRatio)
  
         EXECUTE CLI:Status + 1
            LOC:Client_Status     = 'Normal'
            LOC:Client_Status     = 'On Hold'
            LOC:Client_Status     = 'Closed'
            LOC:Client_Status     = 'Dormant'
         ELSE
            LOC:Client_Status     = '<Unknown>'
         .
  
         IF L_OG:Include_Stats = TRUE
            ! (SHORT=0, ULONG=0),STRING
            ! (p:Type_ID, p:Entity_ID)
            LOC:Report_Group      = Get_Client_Stats(, CLI:CID)
            PRINT(RPT:detail_stats)
      .  .
  
  
      DO Print_Others
  
      FREE(L_EQ:Effective_Q)
  
      View_Rates.Init(Rates_View, Relate:__Rates)
      View_Rates.AddSortOrder()
      View_Rates.AppendOrder('+RAT:LTID,+RAT:CRTID,+RAT:JID,+RAT:CTID,-RAT:Effective_Date,+RAT:ToMass')
      View_Rates.SetFilter('RAT:CID = CLI:CID AND (0 = L_OG:EffectiveDateAtDate OR RAT:Effective_Date << (L_OG:EffectiveDateAtDate  + 1))')
      View_Rates.Reset()
      LOOP
         IF View_Rates.Next() ~= LEVEL:Benign
            BREAK
         .
  
         ! Check that we have only the Effective rates...
         IF L_OG:EffectiveRatesOnly = TRUE
            L_EQ:LTID        = RAT:LTID
            L_EQ:CRTID       = RAT:CRTID
            L_EQ:JID         = RAT:JID
            L_EQ:CTID        = RAT:CTID
            L_EQ:ToMass      = RAT:ToMass
  
            GET(L_EQ:Effective_Q, L_EQ:LTID, L_EQ:CRTID, L_EQ:JID, L_EQ:CTID, L_EQ:ToMass)
            IF ERRORCODE()
               L_EQ:LTID     = RAT:LTID
               L_EQ:CRTID    = RAT:CRTID
               L_EQ:JID      = RAT:JID
               L_EQ:CTID     = RAT:CTID
               L_EQ:ToMass   = RAT:ToMass
               ADD(L_EQ:Effective_Q, +L_EQ:LTID, +L_EQ:CRTID, +L_EQ:JID, +L_EQ:CTID, +L_EQ:ToMass)
  
               ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt) - global
               IF L_OG:CreateLog = TRUE
                  Add_Log(RAT:RID & ',' & RAT:CID & ',' & RAT:JID & ',' & RAT:LTID & ',' & RAT:CTID & ',' & RAT:CRTID & ',' & RAT:ToMass & ',' & RAT:RUBID & ',' & FORMAT(RAT:Effective_Date,@d6) & ',' & RAT:RatePerKg & ',' & RAT:MinimiumCharge, 'Rates', 'Print_Rates - ' & FORMAT(TODAY(), @d5-), 0)
               .
            ELSE
               CYCLE
         .  .
  
         EXECUTE LOC:Journey_Type + 1
            LOC:Journey     = JOU:Journey
            LOC:Journey     = JOU:Description
         .
  
         PRINT(RPT:Detail)
      .
      View_Rates.Kill()
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:detail_stats)
  END
  IF 0
    PRINT(RPT:detail_cp_head)
  END
  IF 0
    PRINT(RPT:detail_cp)
  END
  IF 0
    PRINT(RPT:detail_cp_rates_hdr)
  END
  IF 0
    PRINT(RPT:detail_cp_rates)
  END
  IF 0
    PRINT(RPT:detail_ad_head)
  END
  IF 0
    PRINT(RPT:detail_ad)
  END
  IF 0
    PRINT(RPT:detail_fs_head)
  END
  IF 0
    PRINT(RPT:detail_fs)
  END
  IF 0
    PRINT(RPT:detail_fc_head)
  END
  IF 0
    PRINT(RPT:detail_fc)
  END
  IF 0
    PRINT(RPT:detail_tc_head)
  END
  IF 0
    PRINT(RPT:detail_tc)
  END
  IF 0
    PRINT(RPT:detail_genhdr)
  END
  IF 0
    PRINT(RPT:detail_genhdrcol)
  END
  IF LOC:Print_Rates
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


BreakMgr.TakeEnd PROCEDURE(SHORT BreakId,SHORT LevelId)

L:FID_Last          LIKE(FLO:FID)

L:Last_Eff_Date     LIKE(CPRA:Effective_Date)
  CODE
  PARENT.TakeEnd(BreakId,LevelId)
      IF LOC:Printed_CP_Head = TRUE           ! ie if they have container park rates, print the list also
         ! Print Container Park Rates now
         ! Loop through all Floors with discounts
  
         Rates_CP_View.Init(View_Rates_CP, Relate:__RatesContainerPark)
  
         Rates_CP_View.AddSortOrder()    !CPRA:CKey_FID_JID_EffDate_ToMass)
         Rates_CP_View.AppendOrder('CPRA:FID,CPRA:JID,-CPRA:Effective_Date,CPRA:ToMass')
         !Rates_CP_View.AddRange()           - we want all floors one by one
         Rates_CP_View.SetFilter('FLO:Print_Rates = 1 AND (0 = ' & L_OG:EffectiveDateAtDate & ' OR CPRA:Effective_Date << ' & L_OG:EffectiveDateAtDate  + 1 & ')')
                                                                                                   
     !    IF L_OG:EffectiveRatesOnly = TRUE
     !       View_Add.SetFilter('CARA:Effective_Date < ' & L_OG:EffectiveDateAtDate + 1,'2')
     !    .
  
  
  
     !    View_Rates.AppendOrder('+RAT:LTID,+RAT:CRTID,+RAT:JID,+RAT:CTID,-RAT:Effective_Date,+RAT:ToMass')
     !    View_Rates.SetFilter('RAT:CID = CLI:CID AND (0 = L_OG:EffectiveDateAtDate OR RAT:Effective_Date << (L_OG:EffectiveDateAtDate  + 1))')
  
         Rates_CP_View.Reset()
         LOOP
            IF Rates_CP_View.Next() ~= LEVEL:Benign
               BREAK
            .
  
            IF L_OG:EffectiveRatesOnly = TRUE
               ! Check that either our floor has changed or the effective date is the same
               IF L:Last_Eff_Date ~= CPRA:Effective_Date
                  IF L:FID_Last = CPRA:FID
                     CYCLE
            .  .  .
  
            IF L:FID_Last ~= CPRA:FID
               L:FID_Last                = CPRA:FID
               L:Last_Eff_Date           = CPRA:Effective_Date
  
               L_CPQ:FID                 = CLI_CP:FID
               GET(L_CPQ:Container_Parks_Discount_Q, L_CPQ:FID)
  
               ! Heading for next Floor
               PRINT(RPT:detail_cp_rates_hdr)
            .
  
            LOC:Discounted_Rate_Per_Kg   = CPRA:RatePerKg - (CPRA:RatePerKg * (L_CPQ:ContainerParkRateDiscount / 100))
  
            PRINT(RPT:detail_cp_rates)
         .
  
         Rates_CP_View.Kill()
      .


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Rates','Print_Rates','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_RemittanceAdvice PROCEDURE (p:TID, p:REMID)

Progress:Thermometer BYTE                                  !
L_Return_Group       GROUP,PRE(L_RG)                       !
Suburb               STRING(50)                            !Suburb
PostalCode           STRING(10)                            !
Country              STRING(50)                            !Country
Province             STRING(35)                            !Province
City                 STRING(35)                            !City
Found                BYTE                                  !
                     END                                   !
LOC:Statement_Group  GROUP,PRE(L_SG)                       !
STID                 ULONG                                 !Statement ID
ReportPageNumber     ULONG                                 !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
Next                 BYTE                                  !
                     END                                   !
LOC:Locals           GROUP,PRE(L_L)                        !
Date                 LONG                                  !
Time                 LONG                                  !
                     END                                   !
Process:View         VIEW(_Remittance)
                       PROJECT(REMI:REMID)
                       PROJECT(REMI:TID)
                       JOIN(TRA:PKey_TID,REMI:TID)
                         PROJECT(TRA:TransporterName)
                         PROJECT(TRA:VATNo)
                         PROJECT(TRA:AID)
                         JOIN(ADD:PKey_AID,TRA:AID)
                           PROJECT(ADD:AddressName)
                           PROJECT(ADD:Line1)
                           PROJECT(ADD:Line2)
                         END
                       END
                       JOIN(REMIT:FKey_REMID,REMI:REMID)
                         PROJECT(REMIT:AmountPaid)
                         PROJECT(REMIT:InvoiceDate)
                         PROJECT(REMIT:MID)
                         PROJECT(REMIT:TIN)
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(1000,3333,7052,5760),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif',8,,FONT:regular, |
  CHARSET:ANSI),THOUS
                       HEADER,AT(1000,1000,7052,2354),USE(?Header)
                         STRING('Remittance Advice'),AT(83,31,6094),USE(?String7),FONT(,16,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         BOX,AT(4865,104,2021,583),USE(?Box1),COLOR(COLOR:Black),ROUND
                         STRING(@n_10),AT(5260,302,1208,240),USE(REMI:REMID),FONT(,12,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         IMAGE('fbn_logo_small.jpg'),AT(63,323,3552,677),USE(?Image1)
                         STRING(@s35),AT(4510,1063),USE(TRA:TransporterName),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@s35),AT(156,1063),USE(ADD:AddressName),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                         STRING(@s35),AT(156,1250),USE(ADD:Line1)
                         STRING(@s35),AT(156,1417),USE(ADD:Line2)
                         STRING(@s50),AT(167,1604),USE(L_RG:Suburb)
                         STRING(@s10),AT(167,1813),USE(L_RG:PostalCode)
                         STRING(@s20),AT(1583,1813),USE(TRA:VATNo)
                         STRING('VAT No.:'),AT(990,1813),USE(?StringVat),TRN
                         LINE,AT(73,2085,6906,0),USE(?Line11),COLOR(COLOR:Black)
                         LINE,AT(73,2325,6906,0),USE(?Line11:2),COLOR(COLOR:Black)
                         LINE,AT(958,2085,0,250),USE(?Line1:2),COLOR(COLOR:Black)
                         LINE,AT(2052,2085,0,250),USE(?Line1:3),COLOR(COLOR:Black)
                         LINE,AT(3198,2085,0,250),USE(?Line1:4),COLOR(COLOR:Black)
                         LINE,AT(4469,2085,0,250),USE(?Line1:5),COLOR(COLOR:Black)
                         LINE,AT(5740,2085,0,250),USE(?Line1:6),COLOR(COLOR:Black)
                         STRING('Manifest ID'),AT(2229,2115,885,208),USE(?String16:3),FONT(,,COLOR:Black,FONT:bold, |
  CHARSET:ANSI),CENTER,TRN
                         STRING('Amount Paid'),AT(5875,2115,1031),USE(?String16:6),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         STRING('Date'),AT(115,2115,729,208),USE(?String16),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         STRING('Invoice No.'),AT(1094,2115,885,208),USE(?String16:2),FONT(,,COLOR:Black,FONT:bold, |
  CHARSET:ANSI),CENTER,TRN
                       END
break_client           BREAK(REMI:TID)
Detail                   DETAIL,AT(,,,188),USE(?Detail),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                           LINE,AT(958,0,0,188),USE(?Line1),COLOR(COLOR:Black)
                           LINE,AT(2052,0,0,188),USE(?Line1:7),COLOR(COLOR:Black)
                           LINE,AT(3198,0,0,188),USE(?Line1:8),COLOR(COLOR:Black)
                           LINE,AT(4469,0,0,188),USE(?Line1:9),COLOR(COLOR:Black)
                           LINE,AT(5740,0,0,188),USE(?Line1:10),COLOR(COLOR:Black)
                           STRING(@d17),AT(115,10),USE(REMIT:InvoiceDate)
                           STRING(@n-14.2),AT(5875,10,1031,167),USE(REMIT:AmountPaid),RIGHT(1)
                           STRING(@n_10),AT(1094,10,885,167),USE(REMIT:TIN),RIGHT(1)
                           STRING(@n_10),AT(2229,10,885,167),USE(REMIT:MID),RIGHT(1)
                         END
                         FOOTER,AT(0,0)
                           STRING('Total Paid:'),AT(5188,208),USE(?String35:7),TRN
                         END
                       END
                       FOOTER,AT(1000,9104,7052,1313),USE(?Footer),FONT('MS Sans Serif',10,COLOR:Black,FONT:regular, |
  CHARSET:ANSI)
                         STRING('Report Date:'),AT(135,740),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(1135,740),USE(?ReportDateStamp),TRN
                         STRING('Report Time:'),AT(2604,740),USE(?ReportTimePrompt),TRN
                         STRING('<<-- Time Stamp -->'),AT(3604,740),USE(?ReportTimeStamp),TRN
                         STRING('Page:'),AT(6156,740),USE(?String6),TRN
                         STRING(@N3),AT(6615,740),USE(ReportPageNumber)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_RemittanceAdvice')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:_Remittance.SetOpenRelated()
  Relate:_Remittance.Open                                  ! File _Remittance used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_RemittanceAdvice',ProgressWindow)    ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:_Remittance, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_Remittance.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      IF p:TID ~= 0
         ThisReport.AddRange(REMI:TID,p:TID)
      .
  
      IF p:REMID ~= 0
         ThisReport.SetFilter('REMI:REMID = ' & p:REMID)
      .
  
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Remittance.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_RemittanceAdvice',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      L_Return_Group  = Get_Suburb_Details(ADD:SUID)
  
  
  !    IF L_SG:STID ~= STA:STID
  !       L_SG:STID    = STA:STID
  !       L_SG:Next    = TRUE
  !
  !       L_SG:Days90  = STA:Days90
  !       L_SG:Days60  = STA:Days60
  !       L_SG:Days30  = STA:Days30
  !       L_SG:Current = STA:Current
  !       L_SG:Total   = STA:Total
  !    .
  !
  !
  !    IF L_SG:Next = TRUE
  !       L_SG:Next    = FALSE
  !    .
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_RemittanceAdvice','Print_RemittanceAdvice','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! had filter - INV:InvoiceDate >= LO:From_Date AND INV:InvoiceDate <= LO:To_Date + 1
!!! </summary>
Print_VAT_Summary PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
LOC:Report           GROUP,PRE(L_RG)                       !
Description          STRING(150)                           !
Value                DECIMAL(12,2)                         !
Print_Line           BYTE                                  !
Total                DECIMAL(12,2)                         !
                     END                                   !
Process:View         VIEW(Setup)
                     END
ProgressWindow       WINDOW('Report - VAT Summary'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('_Invoice Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('VAT Summary'),AT(0,20,4885),USE(?ReportTitle),FONT(,14,,FONT:bold),CENTER
                         STRING(@d5b),AT(5417,104),USE(LO:From_Date),RIGHT(1)
                         STRING('-    '),AT(6250,104,208,156),USE(?String24),TRN
                         STRING(@d5b),AT(6615,104),USE(LO:To_Date),RIGHT(1)
                         LINE,AT(0,600,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         STRING(@s150),AT(156,52,5417,156),USE(L_RG:Description)
                         LINE,AT(5750,0,0,250),USE(?DetailLine:81),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                         STRING(@n-17.2),AT(6531,52),USE(L_RG:Value),RIGHT(1)
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine1),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_VAT_Summary')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Setup.Open                                        ! File Setup used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
      LOC:Options     = Ask_Date_Range()
  Do DefineListboxStyle
  INIMgr.Fetch('Print_VAT_Summary',ProgressWindow)         ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Setup, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder('+SET:SID')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Setup.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Setup.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_VAT_Summary',ProgressWindow)      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      L_RG:Description    = 'OUT VAT on Debtor Invoices'
  
      ! Get_Invoices
      ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info  , p:LogName)
      !   1           2       3           4          5       6          7        8            9                 10        11          12
      ! (LONG  , BYTE    , BYTE=0, BYTE=0        , ULONG=0, <LONG>  , BYTE=0  , BYTE=0    , BYTE=0           , BYTE=0  , <STRING>, <STRING>),STRING
      ! p:Option
      !   0.  - Total Charges inc.
      !   1.  - Excl VAT
      !   2.  - VAT
      !   3.  - Kgs
      ! p:Type
      !   0.  - All
      !   1.  - Invoices                                  Total >= 0.0
      !   2.  - Credit Notes                              Total < 0.0    Instead of <, should be invoices with A_INV:CR_IID
      !   3.  - Credit Notes excl Bad Debts               Total < 0.0
      !   4.  - Credit Notes excl Journals                Total < 0.0
      !   5.  - Credit Notes excl Bad Debts & Journals    Total < 0.0
      ! p:Limit_On    &    p:ID
      !   0.  - Date [& Branch]
      !   1.  - MID
      ! p:Manifest_Option
      !   0.  - None
      !   1.  - Overnight
      !   2.  - Broking
      ! p:Not_Manifest
      !   0.  - All
      !   1.  - Not Manifest
      !   2.  - Manifest
  
  
      L_RG:Value          = Get_Invoices(LO:From_Date, 2, 1,,, LO:To_Date)
  
      ?Progress:UserString{PROP:Text} = L_RG:Description
      DISPLAY
      PRINT(RPT:Detail)
  
      L_RG:Total         += L_RG:Value
  
  
  
      L_RG:Description    = 'IN VAT on Debtor Credits'
      L_RG:Value          = Get_Invoices(LO:From_Date, 2, 2,,, LO:To_Date)
  
      ?Progress:UserString{PROP:Text} = L_RG:Description
      DISPLAY
      PRINT(RPT:Detail)
  
      L_RG:Total         += L_RG:Value
  
  
  
      L_RG:Description    = 'IN VAT on Creditor Invoices'
  
      ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID, p:BID, p:ToDate)
      !   1           2       3           4               5           6       7       8
      ! p:Option
      !   0.  - Total Charges inc.
      !   1.  - Excl VAT
      !   2.  - VAT
      ! p:Type
      !   0.  - All
      !   1.  - Invoices                      Total >= 0.0
      !   2.  - Credit Notes                  Total < 0.0
      ! p:DeliveryLegs
      !   0.  - None
      !   1.  - Del Legs only
      !   2.  - Both
      ! p:MID
      !   Omitt   - Not for a MID
      !   x       - for a MID
  
      ! Sum of all VAT on Creditor Invoices
      L_RG:Value          = Get_InvoicesTransporter(LO:From_Date, 2, 1,, 2,,, LO:To_Date)
  
      ?Progress:UserString{PROP:Text} = L_RG:Description
      DISPLAY
      PRINT(RPT:Detail)
  
      L_RG:Total         += L_RG:Value
  
  
      ! Sum of all VAT on Creditor Credit Notes
      L_RG:Description    = 'IN VAT on Creditor Credits'
      L_RG:Value          = Get_InvoicesTransporter(LO:From_Date, 2, 2,, 2,,, LO:To_Date)
  
      ?Progress:UserString{PROP:Text} = L_RG:Description
      DISPLAY
      PRINT(RPT:Detail)
  
      L_RG:Total         += L_RG:Value
  
  
  
      ! Sum of all VAT on Creditor Dedits
      L_RG:Description    = 'OUT VAT on Creditor Debits'
      L_RG:Value          = Get_InvoicesTransporter(LO:From_Date, 2, 1, 1, 2,,, LO:To_Date)
  
      ?Progress:UserString{PROP:Text} = L_RG:Description
      DISPLAY
      PRINT(RPT:Detail)
  
      L_RG:Total         += L_RG:Value
  
      L_RG:Description    = 'Total Due:'
      L_RG:Value          = L_RG:Total
  
      ?Progress:UserString{PROP:Text} = L_RG:Description
      DISPLAY
      PRINT(RPT:Detail)
  
  
  
  
      db.debugout('[Print_VAT_Summary]  Complete')
  IF 0
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF L_RG:Total ~= 0.0
         ReturnValue         = Record:Filtered
      .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_VAT_Summary','Print_VAT_Summary','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

