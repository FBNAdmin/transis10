

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS012.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Print_Statement_h    PROCEDURE                             ! Declare Procedure

  CODE
    Print_Statement()
    RETURN
!!! <summary>
!!! Generated from procedure template - Process
!!! </summary>
Process_Invoices PROCEDURE (p:CID)

Progress:Thermometer BYTE                                  !
LOC:Total            ULONG                                 !
LOC:Done             ULONG                                 !
LOC:Per              DECIMAL(4,1)                          !
LOC:Start            LONG                                  !
LOC:Elapsed          LONG                                  !
Process:View         VIEW(_Invoice)
                       PROJECT(INV:IID)
                     END
ProgressWindow       WINDOW('Process Client Invoices'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,40,111,12),USE(Progress:Thermometer),HIDE,RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,50,141,10),USE(?Progress:PctText),CENTER,HIDE
                       STRING(''),AT(0,30,141,10),USE(?String_Progress),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                       PROGRESS,AT(15,16,111,12),USE(?MyProgress2),RANGE(0,100)
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
_sql            SQLQueryClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_Invoices')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
      BIND('INV:CID', INV:CID)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:_Invoice.SetOpenRelated()
  Relate:_Invoice.Open                                     ! File _Invoice used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  ProgressWindow{Prop:MDI} = True                          ! Make progress window an MDI child window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:_Invoice, ?Progress:PctText, Progress:Thermometer, ProgressMgr, INV:IID)
  ThisProcess.AddSortOrder(INV:PKey_IID)
  ThisProcess.SetProgressLimits(0,LOC:Total)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(_Invoice,'QUICKSCAN=on')
      IF CLIP(p:CID) = ''
         LOC:Total                    = RECORDS(_Invoice)
      ELSE
         ThisProcess.SetFilter('INV:CID = ' & CLIP(p:CID))
  
         _SQLTemp{PROP:SQL}           = 'SELECT COUNT(*) FROM _Invoice WHERE CID = ' & CLIP(p:CID)
         NEXT(_SQLTemp)
         LOC:Total                    = _SQ:S1
      .
  
      LOC:Start                       = CLOCK()
  
      ?MyProgress2{PROP:RangeHigh}    = 100
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      UNBIND('INV:CID')
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Invoice.Close
    Relate:_SQLTemp.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      LOC:Done                           += 1
      IF LOC:Done % 30 = 0
         LOC:Per                             = LOC:Done / LOC:Total * 100
  
         ?MyProgress2{PROP:Progress}         = LOC:Per
  
         LOC:Elapsed                         = CLOCK() - LOC:Start
         IF LOC:Elapsed < 0
            LOC:Start = CLOCK()
         .
  
         ?String_Progress{PROP:Text}         = LOC:Per & ' % Complete - Time left: ' & FORMAT(LOC:Elapsed / LOC:Done * (LOC:Total - LOC:Done), @t4)
  
  
         ?Progress:UserString{PROP:Text}     = INV:IID & '  (' & LOC:Done & ' / ' & LOC:Total & ')'
      .
  
      Upd_Invoice_Paid_Status( INV:IID )
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Process
!!! </summary>
Process_Invoices_Check PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Problems         ULONG                                 !
Process:View         VIEW(_Invoice)
                       PROJECT(INV:DID)
                       PROJECT(INV:DINo)
                       PROJECT(INV:IID)
                       JOIN(DEL:PKey_DID,INV:DID)
                         PROJECT(DEL:DID)
                         PROJECT(DEL:DINo)
                       END
                     END
ProgressWindow       WINDOW('Process Invoice'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
Kill                   PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_Invoices_Check')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:_Invoice.SetOpenRelated()
  Relate:_Invoice.Open                                     ! File _Invoice used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Process_Invoices_Check',ProgressWindow)    ! Restore window settings from non-volatile store
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:_Invoice, ?Progress:PctText, Progress:Thermometer, ProgressMgr, INV:IID)
  ThisProcess.AddSortOrder(INV:PKey_IID)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(_Invoice,'QUICKSCAN=on')
  SEND(Deliveries,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Invoice.Close
  END
  IF SELF.Opened
    INIMgr.Update('Process_Invoices_Check',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisProcess.Kill PROCEDURE

  CODE
  PARENT.Kill
      IF LOC:Problems > 0
         MESSAGE(LOC:Problems & ' problems were found.', 'Process Invoices Check', ICON:Hand)
         ISExecute(ProgressWindow{PROP:Handle}, 'Invoice_DIs.log')
      .


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      IF INV:DINo ~= DEL:DINo
         IF DEL:DID = 0
            Add_Log('Invoice ' & INV:IID & ' has a DI No. mismatch with DI No. ' & INV:DINo &'.  Delivery Instruction is missing.', 'Process Invoices Check', 'Invoice_DIs')
         ELSE
            Add_Log('Invoice ' & INV:IID & ' has a DI No. mismatch with DI No. ' & INV:DINo &'.  Delivery Instruction has DI No. ' & DEL:DINo & ' and DID ' & DEL:DID, 'Process Invoices Check', 'Invoice_DIs')
         .
         LOC:Problems += 1
      .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Process
!!! </summary>
Process_ManifestLoads PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Delete           BYTE                                  !
LOC:Count            ULONG                                 !
LOC:Delete_2         ULONG                                 !
LOC:Delete_1         ULONG                                 !
Process:View         VIEW(ManifestLoad)
                       PROJECT(MAL:MID)
                       PROJECT(MAL:MLID)
                       JOIN(MAN:PKey_MID,MAL:MID)
                         PROJECT(MAN:MID)
                       END
                     END
ProgressWindow       WINDOW('Process ManifestLoadDeliveries'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
Kill                   PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_ManifestLoads')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:ManifestLoad.Open                                 ! File ManifestLoad used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Process_ManifestLoads',ProgressWindow)     ! Restore window settings from non-volatile store
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:ManifestLoad, ?Progress:PctText, Progress:Thermometer, ProgressMgr, MAL:MLID)
  ThisProcess.AddSortOrder(MAL:PKey_MLID)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(ManifestLoad,'QUICKSCAN=on')
  SEND(Manifest,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ManifestLoad.Close
  END
  IF SELF.Opened
    INIMgr.Update('Process_ManifestLoads',ProgressWindow)  ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisProcess.Kill PROCEDURE

  CODE
  PARENT.Kill
      IF LOC:Count > 0
         MESSAGE(LOC:Count & ' orphaned Manifest Load Delivery records were removed.  Please see the log file for details: "Orphaned Manifest Load Deliveries.log"', 'Process Manifest Load Deliveries', ICON:Exclamation)
      ELSE
         IF LOC:Delete_1 > 0
            MESSAGE('No orphaned Manifest Load records were deleted.||There were ' & LOC:Delete_1 & ' Maniufest Loads missing Manifests.', 'Process ManifestLoadDeliveries', ICON:Exclamation)
         ELSE
            MESSAGE('No orphaned Manifest Load Delivery records were found.', 'Process Manifest Load Deliveries', ICON:Exclamation)
      .  .


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      LOC:Delete  = FALSE
  
      IF MAN:MID = 0                  ! Related MID is zero
         ! Delete this Item
         LOC:Delete = TRUE
      .
  
      IF LOC:Delete > 0
         Add_Log('Manifest missing - MLID: ' & MAL:MLID & ',  MID: ' & MAL:MID, 'Manifest Load', 'Orphaned Manifest Loads')
  
         Access:ManifestLoad.TryFetch(MALD:PKey_MLDID)
  
         LOC:Delete_1        += 1
         IF Relate:ManifestLoadDeliveries.Delete(0) = LEVEL:Benign
            LOC:Count        += 1
      .  .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Process
!!! </summary>
Process_ManifestLoadDels PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Delete           BYTE                                  !
LOC:Count            ULONG                                 !
LOC:Delete_2         ULONG                                 !
LOC:Delete_1         ULONG                                 !
LOC:No_Delete        BYTE                                  !No Delete
Process:View         VIEW(ManifestLoadDeliveries)
                       PROJECT(MALD:MLDID)
                       PROJECT(MALD:MLID)
                       JOIN(MAL:PKey_MLID,MALD:MLID)
                         PROJECT(MAL:MID)
                         PROJECT(MAL:MLID)
                         JOIN(MAN:PKey_MID,MAL:MID)
                           PROJECT(MAN:MID)
                         END
                       END
                     END
ProgressWindow       WINDOW('Process ManifestLoadDeliveries'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
Kill                   PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_ManifestLoadDels')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:ManifestLoadDeliveries.SetOpenRelated()
  Relate:ManifestLoadDeliveries.Open                       ! File ManifestLoadDeliveries used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Process_ManifestLoadDels',ProgressWindow)  ! Restore window settings from non-volatile store
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:ManifestLoadDeliveries, ?Progress:PctText, Progress:Thermometer, ProgressMgr, MALD:MLDID)
  ThisProcess.AddSortOrder(MALD:PKey_MLDID)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
      CASE MESSAGE('Would you like to delete the entries with missing parents now?', 'Process Manifest Load Deliveries', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
      OF BUTTON:No
         LOC:No_Delete    = TRUE
      .
  SEND(ManifestLoadDeliveries,'QUICKSCAN=on')
  SEND(ManifestLoad,'QUICKSCAN=on')
  SEND(Manifest,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ManifestLoadDeliveries.Close
  END
  IF SELF.Opened
    INIMgr.Update('Process_ManifestLoadDels',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisProcess.Kill PROCEDURE

  CODE
  PARENT.Kill
      IF LOC:Count > 0
         MESSAGE(LOC:Count & ' orphaned ManifestLoadDeliveries records were removed.  Please see the log file for details: "Orphaned Manifest Load Deliveries.log"', 'Process ManifestLoadDeliveries', ICON:Exclamation)
      ELSE
         IF LOC:Delete_2 > 0 OR LOC:Delete_1 > 0
            MESSAGE('No orphaned ManifestLoadDeliveries records were deleted.||There were ' & LOC:Delete_2 & ' Maniufest Loads missing Manifests and ' & LOC:Delete_1 & ' Manifest Load Deliveries missing Manifest Loads.', 'Process ManifestLoadDeliveries', ICON:Exclamation)
         ELSE
            MESSAGE('No orphaned ManifestLoadDeliveries records were found.', 'Process ManifestLoadDeliveries', ICON:Exclamation)
      .  .


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      LOC:Delete  = FALSE
  
      IF MAN:MID = 0
         ! Delete this Item
         LOC:Delete = TRUE
      .
  
      IF MAL:MLID = 0
         ! Delete this Item
         LOC:Delete       = 2
         LOC:Delete_2    += 1
      .
  
      IF LOC:Delete > 0
         IF LOC:Delete = TRUE
            Add_Log('Manifest missing - MLDID: ' & MALD:MLDID & ',  MLID: ' & MALD:MLID & ',  DIID: ' & MALD:DIID & ',  MID: ' & MAN:MID & ',  UnitsLoaded: ' & MALD:UnitsLoaded, 'Item', 'Orphaned Manifest Load Deliveries')
         ELSE
            Add_Log('ManifestLoad missing - MLDID: ' & MALD:MLDID & ',  MLID: ' & MALD:MLID & ',  DIID: ' & MALD:DIID & ',  MID: ' & MAN:MID & ',  UnitsLoaded: ' & MALD:UnitsLoaded, 'Item', 'Orphaned Manifest Load Deliveries')
         .
  
         IF Access:ManifestLoadDeliveries.TryFetch(MALD:PKey_MLDID) ~= LEVEL:Benign
            db.debugout('[Proc_ManLoadDels]  Failed to get Man Load Del for MALD:MLDID: ' & MALD:MLDID)
         .
         IF MAL:MLID ~= 0
            IF Access:ManifestLoad.TryFetch(MAL:PKey_MLID) ~= LEVEL:Benign
               db.debugout('[Proc_ManLoadDels]  Failed to get Man Load for MAL:MLID: ' & MAL:MLID)
         .  .
  
         IF LOC:No_Delete = FALSE
            IF MAL:MLID ~= 0              ! Try delete the Man Load Del.
               IF Relate:ManifestLoad.Delete(0) = LEVEL:Benign
                  LOC:Count     += 1
                  db.debugout('deleted man load: ' & MAL:MLID)
               ELSE
                  db.debugout('could not delete Man Load: ' & MAL:MLID)
               .
            ELSE                          ! Try delete the Man Load with cascade
               IF Access:ManifestLoadDeliveries.DeleteRecord(0) = LEVEL:Benign
                  LOC:Count     += 1
                  db.debugout('deleted man load del: ' & MALD:MLDID)
               ELSE
                  db.debugout('could not delete man load del: ' & MALD:MLDID)
      .  .  .  .
  RETURN ReturnValue

