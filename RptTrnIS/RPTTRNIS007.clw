

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS007.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Client_BadDebts PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
Process:View         VIEW(_Invoice)
                       PROJECT(INV:BadDebt)
                       PROJECT(INV:BranchName)
                       PROJECT(INV:CR_IID)
                       PROJECT(INV:ClientName)
                       PROJECT(INV:ClientNo)
                       PROJECT(INV:InvoiceDate)
                       PROJECT(INV:InvoiceMessage)
                       PROJECT(INV:Total)
                       PROJECT(INV:VAT)
                       PROJECT(INV:CID)
                       PROJECT(INV:IID)
                       JOIN(CLI:PKey_CID,INV:CID)
                       END
                       JOIN(BRA:PKey_BID,INV:IID)
                       END
                     END
ProgressWindow       WINDOW('Report Bad Debts'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('_Invoice Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Debtors Bad Debt Listing'),AT(0,20,4885),USE(?ReportTitle),FONT(,14,,FONT:bold),CENTER
                         BOX,AT(10,354,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         STRING(@d5b),AT(5417,104),USE(LO:From_Date),RIGHT(1)
                         STRING('-    '),AT(6198,104,208,156),USE(?String24),TRN
                         STRING(@d5b),AT(6615,104),USE(LO:To_Date),RIGHT(1)
                         LINE,AT(667,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1458,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(2115,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(3583,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(4271,354,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(6115,354,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(6875,350,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         STRING('Invoice No.'),AT(3688,385,,170),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Branch'),AT(740,385,677,167),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Client No.'),AT(1521,385,563,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('Client'),AT(2198,385,1365,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Date'),AT(42,385,583,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Total'),AT(6938,385,740,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Comments'),AT(4427,385,1563,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('VAT'),AT(6167,385,677,167),USE(?HeaderTitle:9),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(667,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(1458,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(2115,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(3583,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(4271,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(6115,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(6875,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                         STRING(@n_10),AT(3646,52,573,167),USE(INV:CR_IID),RIGHT
                         STRING(@s255),AT(4323,52,1719,156),USE(INV:InvoiceMessage)
                         STRING(@s35),AT(729,52,677,167),USE(INV:BranchName),LEFT
                         STRING(@n_8b),AT(1500,52,,170),USE(INV:ClientNo),RIGHT,TRN
                         STRING(@s100),AT(2177,52,1365,167),USE(INV:ClientName),LEFT
                         STRING(@d5),AT(42,52,,170),USE(INV:InvoiceDate),RIGHT
                         STRING(@n-12.2),AT(6938,52,,170),USE(INV:Total),RIGHT
                         STRING(@n-11.2),AT(6146,52,,170),USE(INV:VAT),RIGHT(1),TRN
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
Totals                 DETAIL,AT(,,,302),USE(?Totals)
                         LINE,AT(104,31,7552,0),USE(?Line18),COLOR(COLOR:Black),LINEWIDTH(20)
                         STRING('Totals:'),AT(5521,83),USE(?String28),TRN
                         STRING(@n-15.2),AT(6781,83,,170),USE(INV:Total,,?INV:Total:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(5990,83,,170),USE(INV:VAT,,?INV:VAT:2),RIGHT(1),SUM,TALLY(Detail),TRN
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Invoice No.' & |
      '|' & 'By Invoice Date' & |
      '|' & 'By Client & Invoice No.' & |
      '|' & 'By DI No.' & |
      '|' & 'By Branch & Invoice No.' & |
      '|' & 'By Invoice Composition' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:Totals)
  
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Client_BadDebts')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:_Invoice.SetOpenRelated()
  Relate:_Invoice.Open                                     ! File _Invoice used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
      LOC:Options     = Ask_Date_Range()
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Client_BadDebts',ProgressWindow)     ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:_Invoice, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice No.')) THEN
     ThisReport.AppendOrder('+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice Date')) THEN
     ThisReport.AppendOrder('+INV:InvoiceDate,+INV:ClientName,+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client & Invoice No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientName,+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By DI No.')) THEN
     ThisReport.AppendOrder('+INV:DINo,+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch & Invoice No.')) THEN
     ThisReport.AppendOrder('+BRA:BranchName,+INV:IID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice Composition')) THEN
     ThisReport.AppendOrder('+INV:ICID,+INV:IID')
  END
  ThisReport.SetFilter('(INV:InvoiceDate >= LO:From_Date AND INV:InvoiceDate <<= LO:To_Date + 1) AND (INV:BadDebt = 1)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_Invoice.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Invoice.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Client_BadDebts',ProgressWindow)  ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:Totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Client_BadDebts','Print_Client_BadDebts','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_TripSheet_Portrait PROCEDURE (p:TRID)

Progress:Thermometer BYTE                                  !
LOC:Rep_Fields       GROUP,PRE(L_RF)                       !
Weight               DECIMAL(10,2)                         !In kg's
VolumetricWeight     DECIMAL(10,2)                         !Weight based on Volumetric calculation (in kgs)
Charge               DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
State                STRING(20)                            !State of this manifest
Driver               STRING(50)                            !First Name
Reg_of_Horse         STRING(20)                            !
Reg_of_Trailer       STRING(20)                            !
COD_Comment          STRING(20)                            !
                     END                                   !
LOC:Delivery_Totals  GROUP,PRE(L_DT)                       !
DID                  ULONG                                 !Delivery ID
InsuranceCharge      DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Insurance            DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Docs                 DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Fuel                 DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Charge               DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
                     END                                   !
LOC:Manifest_Totals  GROUP,PRE(L_MT)                       !
Cost                 DECIMAL(10,2)                         !
TRID                 ULONG                                 !Tripsheet ID
Delivery_Charges     DECIMAL(11,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Total_Weight         DECIMAL(10,2)                         !In kg's
Gross_Profit         DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Gross_Profit_Percent DECIMAL(6,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Average_C_Per_Kg     DECIMAL(7,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Legs_Cost            DECIMAL(10,2)                         !
Total_Cost           DECIMAL(10,2)                         !Includes extra legs cost
                     END                                   !
Process:View         VIEW(TripSheetDeliveries)
                       PROJECT(TRDI:UnitsLoaded)
                       PROJECT(TRDI:DIID)
                       PROJECT(TRDI:TRID)
                       JOIN(DELI:PKey_DIID,TRDI:DIID)
                         PROJECT(DELI:DID)
                         PROJECT(DELI:Units)
                         PROJECT(DELI:VolumetricWeight)
                         PROJECT(DELI:Weight)
                         PROJECT(DELI:DIID)
                         JOIN(MALD:FKey_DIID,DELI:DIID)
                           PROJECT(MALD:MLID)
                           JOIN(MAL:PKey_MLID,MALD:MLID)
                             PROJECT(MAL:MID)
                             JOIN(MAN:PKey_MID,MAL:MID)
                               PROJECT(MAN:DepartDate)
                             END
                           END
                         END
                         JOIN(DEL:PKey_DID,DELI:DID)
                           PROJECT(DEL:DID)
                           PROJECT(DEL:DINo)
                           PROJECT(DEL:InsuranceRate)
                           PROJECT(DEL:Insure)
                           PROJECT(DEL:TotalConsignmentValue)
                           PROJECT(DEL:CID)
                           PROJECT(DEL:DeliveryAID)
                           PROJECT(DEL:CollectionAID)
                           JOIN(CLI:PKey_CID,DEL:CID)
                             PROJECT(CLI:ClientName)
                           END
                           JOIN(A_ADD:PKey_AID,DEL:DeliveryAID)
                             PROJECT(A_ADD:AddressName)
                           END
                           JOIN(ADD:PKey_AID,DEL:CollectionAID)
                           END
                           JOIN(INV:FKey_DID,DEL:DID)
                           END
                         END
                       END
                       JOIN(TRI:PKey_TID,TRDI:TRID)
                         PROJECT(TRI:DepartTime)
                         PROJECT(TRI:TRID)
                         PROJECT(TRI:BID)
                         PROJECT(TRI:VCID)
                         JOIN(BRA:PKey_BID,TRI:BID)
                           PROJECT(BRA:BranchName)
                         END
                         JOIN(VCO:PKey_VCID,TRI:VCID)
                           PROJECT(VCO:CompositionName)
                         END
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(615,2771,7052,6917),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular,CHARSET:ANSI), |
  THOUS
                       HEADER,AT(615,1000,7052,1771),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Trip Sheet'),AT(469,31,6135,271),USE(?String55),FONT(,14,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         IMAGE('fbn_logo_small.jpg'),AT(52,52,2302,458),USE(?Image1)
                         STRING('Branch:'),AT(63,563,1083,167),USE(?String51),TRN
                         STRING(@s35),AT(1063,563),USE(BRA:BranchName)
                         STRING('Arrival Time Depot: _{25}'),AT(4406,677),USE(?String52:3),TRN
                         STRING('Driver:'),AT(63,771,1083,167),USE(?String69:2),TRN
                         STRING(@s35),AT(1063,771,1583,167),USE(L_RF:Driver)
                         STRING('Supervisor Signature: _{23}'),AT(4406,979),USE(?String52:4),TRN
                         STRING('Date Despatched:'),AT(63,979,1083,167),USE(?String3:11),LEFT,TRN
                         STRING('Time Out'),AT(63,1198,1083),USE(?String3:10),LEFT,TRN
                         STRING(@d5),AT(1063,979,573,167),USE(MAN:DepartDate),RIGHT(1)
                         STRING(@t7),AT(1063,1198,573,156),USE(TRI:DepartTime),RIGHT(1)
                         STRING('Checked By: _{30}'),AT(4406,1281),USE(?String52:5),TRN
                         STRING('Starting Kms: _{23}'),AT(63,1396),USE(?String52),TRN
                         STRING('Manager Signature: _{25}'),AT(4406,1594),USE(?String52:6),TRN
                         STRING('Closing Kms: _{23}'),AT(63,1594),USE(?String52:2),TRN
                       END
break_tripsheet        BREAK(TRDI:TRID)
                         HEADER,AT(0,0,,740),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI),PAGEBEFORE(-1)
                           STRING('State:'),AT(5042,31),USE(?String54),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@s20),AT(5396,31,1542,167),USE(L_RF:State),FONT(,,,FONT:bold,CHARSET:ANSI),RIGHT(1), |
  TRN
                           STRING('Trip Sheet No.:'),AT(104,31),USE(?String31),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@N_10),AT(844,31,1021,167),USE(TRI:TRID),FONT(,,,FONT:bold,CHARSET:ANSI),RIGHT(1),TRN
                           LINE,AT(21,250,7000,0),USE(?Line8:4),COLOR(COLOR:Black)
                           STRING(@s35),AT(938,302,1583,167),USE(VCO:CompositionName),TRN
                           LINE,AT(21,490,7000,0),USE(?Line8:6),COLOR(COLOR:Black)
                           STRING('Comments'),AT(5198,531,646,167),USE(?String3:8),CENTER,TRN
                           STRING('Signature'),AT(6146,521,781,167),USE(?String3:9),CENTER,TRN
                           STRING('Time Out'),AT(4510,531,542,167),USE(?String3:7),CENTER,TRN
                           STRING('Reg. Horse:'),AT(2969,302),USE(?String69:3),TRN
                           STRING(@s20),AT(3646,302,979,167),USE(L_RF:Reg_of_Horse)
                           STRING('Trailer:'),AT(5188,302),USE(?String69:4),TRN
                           STRING(@s20),AT(5625,302),USE(L_RF:Reg_of_Trailer)
                           STRING('Vehicle Comp.:'),AT(125,302),USE(?String69),TRN
                           STRING('Act Kg'),AT(3250,531,542,167),USE(?String3:5),CENTER,TRN
                           STRING('Time In'),AT(3854,531,542,167),USE(?String3:6),CENTER,TRN
                           STRING('No. of Items'),AT(2542,531,594,167),USE(?String3:4),TRN
                           STRING('Sender'),AT(500,531,1000,167),USE(?String3:2),CENTER,TRN
                           STRING('Deliver To'),AT(1625,531,1000,167),USE(?String3:3),CENTER,TRN
                           STRING('DN No.'),AT(42,531,,167),USE(?String3),TRN
                         END
break_delivery           BREAK(DEL:DID)
Detail                     DETAIL,AT(,,,0),USE(?Detail),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                             GROUP,AT(31,-21,6990,281),USE(?Group1),BOXED,HIDE,TRN
                               STRING(@n6),AT(5344,73,406,167),USE(TRDI:UnitsLoaded)
                               STRING('Not printing detail lines at all'),AT(52,73,885,167),USE(?String1),TRN
                               STRING(@n~AW~-13.2),AT(1552,73,875,167),USE(DELI:Weight),RIGHT(1)
                               STRING(@n-11.2),AT(4594,73,677,167),USE(L_RF:VolumetricWeight),RIGHT(1),HIDE
                               STRING(@n6),AT(5813,73),USE(DELI:Units),HIDE
                               STRING(@n_10),AT(1042,73,458,167),USE(DELI:DID),RIGHT(1),TRN
                               STRING(@n~CW~-13.2),AT(2469,73,875,167),USE(L_RF:Weight),RIGHT(1)
                             END
                           END
                           FOOTER,AT(0,0,,188),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                             LINE,AT(5104,0,0,198),USE(?Line1:8),COLOR(COLOR:Black)
                             LINE,AT(5990,0,0,198),USE(?Line1:5),COLOR(COLOR:Black)
                             STRING(@n-11),AT(3250,10,542,167),USE(L_RF:Weight,,?L_RF:Weight:2),RIGHT(1),SUM,RESET(break_delivery), |
  TRN
                             STRING(@s20),AT(5146,10,833,167),USE(L_RF:COD_Comment)
                             STRING(@n6),AT(2729,10),USE(TRDI:UnitsLoaded,,?TRDI:UnitsLoaded:2),RIGHT(1),SUM,RESET(break_delivery), |
  TRN
                             STRING(@s35),AT(1625,21,1000,167),USE(A_ADD:AddressName)
                             LINE,AT(21,188,7000,0),USE(?Line12),COLOR(COLOR:Black),LINEWIDTH(3)
                             STRING(@n_10),AT(-63,10,479,167),USE(DEL:DINo),RIGHT(1)
                             LINE,AT(448,0,0,198),USE(?Line1:9),COLOR(COLOR:Black)
                             STRING(@s35),AT(500,10,1000,167),USE(CLI:ClientName)
                             LINE,AT(2677,0,0,198),USE(?Line1:3),COLOR(COLOR:Black)
                             LINE,AT(4458,0,0,198),USE(?Line1:7),COLOR(COLOR:Black)
                             LINE,AT(3833,0,0,198),USE(?Line1:6),COLOR(COLOR:Black)
                             LINE,AT(3177,0,0,198),USE(?Line1:4),COLOR(COLOR:Black)
                             LINE,AT(1563,0,0,198),USE(?Line1:2),COLOR(COLOR:Black)
                           END
                         END
                         FOOTER,AT(0,0,,510)
                           LINE,AT(21,115,7000,0),USE(?Line8:3),COLOR(COLOR:Black)
                           STRING('Total Weight:'),AT(2094,156),USE(?String65:2),TRN
                           STRING(@n-11.2),AT(2979,156),USE(L_RF:Weight,,?L_RF:Weight:3),RIGHT(1),SUM,RESET(break_tripsheet), |
  TRN
                           STRING(@n6),AT(1240,156),USE(TRDI:UnitsLoaded,,?TRDI:UnitsLoaded:3),RIGHT(1),SUM,RESET(break_tripsheet), |
  TRN
                           STRING('Units:'),AT(844,156),USE(?String65),TRN
                         END
                       END
Collections            DETAIL,AT(,,,2125),USE(?collections),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI),TOGETHER
                         STRING('Collections'),AT(63,0,6927,260),USE(?String66),FONT(,12,,FONT:bold,CHARSET:ANSI),CENTER, |
  TRN
                         STRING('DI Number'),AT(63,252),USE(?String58),TRN
                         STRING('From'),AT(677,252,938,156),USE(?String58:2),CENTER,TRN
                         STRING('To'),AT(1594,252,938,167),USE(?String58:3),CENTER,TRN
                         STRING('Weight (kg)'),AT(2479,252,938,167),USE(?String58:4),CENTER,TRN
                         STRING('No. of Packages'),AT(3448,252,938,167),USE(?String58:5),CENTER,TRN
                         STRING('Time In'),AT(4469,252,938,167),USE(?String58:6),CENTER,TRN
                         STRING('Time Out'),AT(5354,252,938,167),USE(?String58:7),CENTER,TRN
                         STRING('Signature'),AT(6250,252,729,156),USE(?String58:8),CENTER,TRN
                         LINE,AT(625,460,0,1590),USE(?Line14),COLOR(COLOR:Black)
                         LINE,AT(1615,460,0,1590),USE(?Line14:3),COLOR(COLOR:Black)
                         LINE,AT(2500,460,0,1590),USE(?Line14:1),COLOR(COLOR:Black)
                         LINE,AT(3400,460,0,1590),USE(?Line14:2),COLOR(COLOR:Black)
                         LINE,AT(4480,460,0,1590),USE(?Line14:4),COLOR(COLOR:Black)
                         LINE,AT(5369,460,0,1590),USE(?Line14:5),COLOR(COLOR:Black)
                         LINE,AT(6254,460,0,1590),USE(?Line14:6),COLOR(COLOR:Black)
                         LINE,AT(42,450,6927,0),USE(?Line13),COLOR(COLOR:Black)
                         LINE,AT(42,650,6927,0),USE(?Line13:2),COLOR(COLOR:Black)
                         LINE,AT(42,850,6927,0),USE(?Line13:3),COLOR(COLOR:Black)
                         LINE,AT(42,1050,6927,0),USE(?Line13:4),COLOR(COLOR:Black)
                         LINE,AT(42,1250,6927,0),USE(?Line13:41),COLOR(COLOR:Black)
                         LINE,AT(52,1450,6927,0),USE(?Line13:5),COLOR(COLOR:Black)
                         LINE,AT(52,1650,6927,0),USE(?Line13:6),COLOR(COLOR:Black)
                         LINE,AT(52,1850,6927,0),USE(?Line13:7),COLOR(COLOR:Black)
                         LINE,AT(52,2050,6927,0),USE(?Line13:8),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(615,9688,7052,302),USE(?Footer),FONT('MS Sans Serif',,COLOR:Black,,CHARSET:ANSI)
                         STRING('Report Date:'),AT(73,31),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(875,31),USE(?ReportDateStamp),TRN
                         STRING('Report Time:'),AT(2250,31),USE(?ReportTimePrompt),TRN
                         STRING('<<-- Time Stamp -->'),AT(3052,31),USE(?ReportTimeStamp),TRN
                         STRING(@N3),AT(6729,31),USE(ReportPageNumber)
                         STRING('Page:'),AT(6313,31),USE(?String61),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.AskPreview PROCEDURE

  CODE
          PRINT(RPT:Collections)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_TripSheet_Portrait')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DeliveryLegs.SetOpenRelated()
  Relate:DeliveryLegs.Open                                 ! File DeliveryLegs used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterAlias.Open                             ! File TransporterAlias used by this procedure, so make sure it's RelationManager is open
  Access:TruckTrailerAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoad.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoadDeliveries.UseFile                    ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_TripSheet_Portrait',ProgressWindow)  ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:TripSheetDeliveries, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  ThisReport.AddRange(TRDI:TRID,L_MT:TRID)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:TripSheetDeliveries.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      IF p:TRID ~= 0
         ThisReport.AddRange(TRDI:TRID, p:TRID)
      .
  
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryLegs.Close
    Relate:TransporterAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_TripSheet_Portrait',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      CLEAR(L_DT:InsuranceCharge)
      IF DEL:Insure = TRUE
         L_DT:InsuranceCharge    = (DEL:InsuranceRate / 100) * DEL:TotalConsignmentValue
      .
  
  !       message('DELI:DID||' & DELI:DID)
  
      A_TRU:TTID              = VCO:TTID0
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Horse    = A_TRU:Registration
      .
      A_TRU:TTID              = VCO:TTID1
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Trailer  = A_TRU:Registration
      .
  
      DRI:DRID                = TRI:DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RF:Driver          = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
  
      CLEAR(L_RF:COD_Comment)
      CASE DEL:Terms              ! Pre Paid|COD|Account
      OF 0
      OF 1
         L_RF:COD_Comment     = 'COD'
      OF 2
      .
  L_RF:Weight = DELI:Weight * (TRDI:UnitsLoaded / DELI:Units)
  L_RF:VolumetricWeight = DELI:VolumetricWeight * (TRDI:UnitsLoaded / DELI:Units)
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:Collections)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF L_DT:DID ~= DEL:DID
         L_DT:DID      = DEL:DID
         ! Total loaded items from DID on MLID   /   Total Items on DID
         L_RF:Charge   = (DEL:DocumentCharge + DEL:FuelSurcharge + DEL:Charge) * |
                         Get_ManLoadItems_Info(MAL:MLID, 1, DEL:DID) / Get_DelItem_s_Totals(DEL:DID, 3)
  
         L_RF:Charge   = L_RF:Charge * (DEL:VATRate / 100)
      .
  
  
  
      IF L_MT:TRID ~= TRDI:TRID
         !L_MT:MID                     = MAL:MID
         L_MT:TRID                    = TRDI:TRID
  
         L_MT:Delivery_Charges        = Get_Manifest_Info(MAL:MID, 4)
  
         L_MT:Cost                    = MAN:Cost * (MAN:VATRate / 100)
         L_MT:Legs_Cost               = Get_Manifest_Info(MAL:MID, 3)                         ! Legs cost
  
         L_MT:Total_Cost              = (L_MT:Legs_Cost + MAN:Cost) * (MAN:VATRate / 100)
  
         L_MT:Gross_Profit            = L_MT:Delivery_Charges - L_MT:Total_Cost
  
         L_MT:Gross_Profit_Percent    = (L_MT:Gross_Profit / L_MT:Delivery_Charges) * 100     ! %
  
  !       MESSAGE('MAL:MID: ' & MAL:MID)
  
         L_MT:Total_Weight            = Get_Manifest_Info(MAL:MID, 2)
  
         L_MT:Average_C_Per_Kg        = (L_MT:Delivery_Charges / L_MT:Total_Weight) * 100     ! in cents (not rands)
  
         EXECUTE TRI:State
            L_RF:State  = 'Loaded'
            L_RF:State  = 'On Route'
            L_RF:State  = 'Transferred'
         ELSE
            L_RF:State  = 'Loading'
      .  .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_TripSheet_Portrait','Print_TripSheet_Portrait','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

