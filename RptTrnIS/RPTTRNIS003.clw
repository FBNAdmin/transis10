

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS003.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_DN_POD PROCEDURE (p:MID, p:DID, p:Only_Invoiced)

Progress:Thermometer BYTE                                  !
LOC:Report_Fields    GROUP,PRE(L_RG)                       !
Packaging_Container  STRING(100)                           !Packaging or Container details
                     END                                   !
LOC:MID              ULONG                                 !Manifest ID
LOC:DID              ULONG                                 !Delivery ID
Process:View         VIEW(ManifestLoad)
                       PROJECT(MAL:MID)
                       PROJECT(MAL:MLID)
                       JOIN(MALD:FSKey_MLID_DIID,MAL:MLID)
                         PROJECT(MALD:UnitsLoaded)
                         PROJECT(MALD:DIID)
                         JOIN(DELI:PKey_DIID,MALD:DIID)
                           PROJECT(DELI:ItemNo)
                           PROJECT(DELI:Units)
                           PROJECT(DELI:Volume)
                           PROJECT(DELI:VolumetricWeight)
                           PROJECT(DELI:Weight)
                           PROJECT(DELI:PTID)
                           PROJECT(DELI:CTID)
                           PROJECT(DELI:CMID)
                           PROJECT(DELI:DID)
                           JOIN(PACK:PKey_PTID,DELI:PTID)
                           END
                           JOIN(CTYP:PKey_CTID,DELI:CTID)
                           END
                           JOIN(COM:PKey_CMID,DELI:CMID)
                             PROJECT(COM:Commodity)
                           END
                           JOIN(DEL:PKey_DID,DELI:DID)
                             PROJECT(DEL:DID)
                             PROJECT(DEL:DINo)
                             PROJECT(DEL:SpecialDeliveryInstructions)
                             PROJECT(DEL:BID)
                             PROJECT(DEL:CID)
                             PROJECT(DEL:SID)
                             PROJECT(DEL:DeliveryAID)
                             PROJECT(DEL:CollectionAID)
                             JOIN(INV:FKey_DID,DEL:DID)
                               PROJECT(INV:IID)
                               PROJECT(INV:VolumetricWeight)
                               PROJECT(INV:Weight)
                             END
                             JOIN(BRA:PKey_BID,DEL:BID)
                               PROJECT(BRA:BranchName)
                             END
                             JOIN(CLI:PKey_CID,DEL:CID)
                               PROJECT(CLI:CID)
                               PROJECT(CLI:ClientName)
                             END
                             JOIN(SERI:PKey_SID,DEL:SID)
                             END
                             JOIN(A_ADD:PKey_AID,DEL:DeliveryAID)
                               PROJECT(A_ADD:AddressName)
                               PROJECT(A_ADD:Line1)
                               PROJECT(A_ADD:Line2)
                               PROJECT(A_ADD:SUID)
                               JOIN(A_SUBU:PKey_SUID,A_ADD:SUID)
                                 PROJECT(A_SUBU:PostalCode)
                                 PROJECT(A_SUBU:Suburb)
                               END
                             END
                             JOIN(ADD:PKey_AID,DEL:CollectionAID)
                               PROJECT(ADD:AddressName)
                               PROJECT(ADD:Line1)
                               PROJECT(ADD:Line2)
                               PROJECT(ADD:SUID)
                               JOIN(SUBU:PKey_SUID,ADD:SUID)
                                 PROJECT(SUBU:PostalCode)
                                 PROJECT(SUBU:Suburb)
                               END
                             END
                           END
                         END
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress... printing Del. Note / POD'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('Deliveries Report'),AT(250,1344,8000,7438),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,,1104),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Delivery Note - Proof of Delivery (POD)'),AT(5146,104,2698),USE(?ReportTitle),FONT('MS Sans Serif', |
  10,COLOR:Black,FONT:regular,CHARSET:ANSI),CENTER
                         LINE,AT(5083,333,2813,0),USE(?Line10),COLOR(COLOR:Black)
                         STRING(@n_10),AT(6063,479),USE(INV:IID),FONT(,10,COLOR:Black,FONT:bold,CHARSET:ANSI),RIGHT(1)
                         STRING(@s35),AT(5365,750,2354,167),USE(BRA:BranchName),CENTER
                         BOX,AT(5083,52,2823,958),USE(?Box5),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         IMAGE('fbn_logo_small.jpg'),AT(94,52,4667,958),USE(?Image1)
                         LINE,AT(62,1052,7885,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                       END
break_Delivery         BREAK(DELI:DID),USE(?BREAK1)
                         HEADER,AT(0,0,,2583),USE(?GROUPHEADER1),PAGEBEFORE(-1)
                           BOX,AT(52,42,7885,760),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                           STRING(@s35),AT(1135,625),USE(CLI:ClientName),FONT(,8,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  HIDE
                           STRING(@n_10),AT(3917,396,2167,167),USE(DEL:DINo),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER
                           STRING(@n_10),AT(6250,396,1635,167),USE(MAL:MID),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                           STRING('<<-- Date Stamp -->'),AT(115,396),USE(?ReportDateStamp),TRN
                           STRING(@n_10),AT(1865,396),USE(CLI:CID),RIGHT(1),TRN
                           LINE,AT(1062,42,0,750),USE(?Line2),COLOR(COLOR:Black)
                           LINE,AT(3792,52,0,750),USE(?Line_tt_mid_down),COLOR(COLOR:Black)
                           LINE,AT(6198,52,0,750),USE(?Line2:3),COLOR(COLOR:Black)
                           STRING('Date'),AT(125,83,625,167),USE(?ReportDatePrompt),CENTER,TRN
                           STRING('DI Number'),AT(3833,83,2323,167),USE(?String17:2),CENTER,TRN
                           STRING('Manifest Number'),AT(6250,83,1635,167),USE(?String17:3),CENTER,TRN
                           STRING('For Account'),AT(1146,83,2573,167),USE(?String17),CENTER,TRN
                           LINE,AT(62,271,7860,0),USE(?Line4),COLOR(COLOR:Black)
                           BOX,AT(52,885,7885,1313),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                           LINE,AT(3792,896,0,1300),USE(?Line_t_mid_down),COLOR(COLOR:Black)
                           STRING('Collected From'),AT(115,927,3625,167),USE(?String23),CENTER,TRN
                           STRING('Deliver To'),AT(3854,927,4021,167),USE(?String23:2),CENTER,TRN
                           LINE,AT(62,1125,7860,0),USE(?Line4:2),COLOR(COLOR:Black)
                           STRING(@s35),AT(229,1188,3323,167),USE(ADD:AddressName)
                           STRING(@s35),AT(3958,1177,3323,167),USE(A_ADD:AddressName),TRN
                           STRING(@s35),AT(229,1396,3323,167),USE(ADD:Line1)
                           STRING(@s35),AT(3958,1385,3323,167),USE(A_ADD:Line1),TRN
                           STRING(@s35),AT(229,1583,3323,167),USE(ADD:Line2)
                           STRING(@s35),AT(3958,1573,3323,167),USE(A_ADD:Line2),TRN
                           STRING(@s50),AT(229,1781,3323,167),USE(SUBU:Suburb)
                           STRING(@s50),AT(3958,1771,3323,167),USE(A_SUBU:Suburb),TRN
                           STRING(@s10),AT(3958,1938),USE(A_SUBU:PostalCode)
                           STRING(@s10),AT(229,1948),USE(SUBU:PostalCode)
                           BOX,AT(52,2271,7885,250),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                           STRING('Volumetric'),AT(6625,2313),USE(?String20:4),TRN
                           STRING('Item No.'),AT(188,2313),USE(?String22),TRN
                           STRING('Description'),AT(792,2313),USE(?String22:3),TRN
                           STRING('Commodity'),AT(3031,2313),USE(?String22:2),TRN
                           STRING('Total Units'),AT(5198,2313),USE(?String20:3),TRN
                           STRING('Volume'),AT(6042,2313),USE(?String20:2),TRN
                           STRING('Weight'),AT(7510,2313),USE(?String20),TRN
                           STRING('Loaded Units'),AT(4417,2313),USE(?String20:5),TRN
                         END
Detail                   DETAIL,AT(0,0,,260),USE(?Detail)
                           STRING(@s35),AT(3042,21,1479,167),USE(COM:Commodity)
                           STRING(@n6),AT(5198,21,531,167),USE(DELI:Units),RIGHT(1)
                           STRING(@n-11.2),AT(7188,21),USE(DELI:Weight),RIGHT(1)
                           STRING(@s100),AT(792,21),USE(L_RG:Packaging_Container)
                           STRING(@n6),AT(4667,21),USE(MALD:UnitsLoaded)
                           STRING(@n-11.2),AT(6458,21),USE(DELI:VolumetricWeight),RIGHT(1)
                           STRING(@n6),AT(198,21),USE(DELI:ItemNo)
                           STRING(@n-11.2),AT(5729,21),USE(DELI:Volume),RIGHT(1)
                         END
                         FOOTER,AT(0,0,,677),USE(?GROUPFOOTER1)
                           TEXT,AT(208,52,4531,573),USE(DEL:SpecialDeliveryInstructions),BOXED
                           STRING('Actual Weight:'),AT(5271,104),USE(?String45),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING(@n-14.2),AT(6396,104),USE(INV:Weight),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI),RIGHT(1)
                           STRING('Volumetric Weight:'),AT(5271,385),USE(?String45:2),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING(@n-14.2),AT(6396,385),USE(INV:VolumetricWeight),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  RIGHT(1)
                         END
                       END
                       FOOTER,AT(250,8802,,1688),USE(?Footer)
                         BOX,AT(52,63,7885,1313),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         LINE,AT(3792,73,0,1300),USE(?Line_b_mid_down),COLOR(COLOR:Black)
                         STRING('Received By'),AT(3854,115,4021,167),USE(?String47),CENTER,TRN
                         LINE,AT(62,312,7860,0),USE(?Line4:3),COLOR(COLOR:Black)
                         STRING('.{113}'),AT(4354,469,3438,167),USE(?String52),TRN
                         STRING('.{113}'),AT(4563,688,3229,167),USE(?String52:2),TRN
                         STRING('Name:'),AT(3948,469),USE(?String48),TRN
                         STRING('Signature:'),AT(3948,688),USE(?String48:2),TRN
                         STRING('Time:'),AT(3948,906),USE(?String48:3),TRN
                         STRING('.{113}'),AT(4313,906,3479,167),USE(?String52:3),TRN
                         STRING('Date:'),AT(3948,1115),USE(?String48:4),TRN
                         STRING('.{113}'),AT(4313,1115,3479,167),USE(?String52:4),TRN
                         STRING(@N3),AT(7688,1417),USE(ReportPageNumber)
                         STRING('Time:'),AT(2083,1417),USE(?ReportTimePrompt),TRN
                         STRING('<<-- Time Stamp -->'),AT(2479,1417),USE(?ReportTimeStamp),TRN
                         STRING('Page:'),AT(7333,1417),USE(?ReportTimePrompt:2),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_DN_POD')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:DID',LOC:DID)                                  ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:ManifestLoad.Open                                 ! File ManifestLoad used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_DN_POD',ProgressWindow)              ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:ManifestLoad, ?Progress:PctText, Progress:Thermometer, ProgressMgr, MAL:MID)
  ThisReport.AddSortOrder(MAL:FKey_MID)
  ThisReport.AppendOrder('+MALD:MLDID')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:ManifestLoad.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      ! (p:MID, p:DID)
      IF p:MID ~= 0
         LOC:MID         = p:MID
         ThisReport.AddRange(MAL:MID,LOC:MID)
      .
  
      IF p:DID ~= 0
         ! We want a DN for just this DID
         LOC:DID      = p:DID
         ThisReport.SetFilter('DELI:DID = LOC:DID')
      .
  
  
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ManifestLoad.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_DN_POD',ProgressWindow)           ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      !DEL:LTID
  
      CASE DELI:Type                      ! Container / Loose
      OF 0
         L_RG:Packaging_Container     = CLIP(DELI:ContainerNo) & ' (' & CLIP(CTYP:ContainerType) & ' - ' & CTYP:Size
         L_RG:Packaging_Container     = CLIP(L_RG:Packaging_Container) & ')'
      OF 1
         !DELI:ByContainer
         L_RG:Packaging_Container     = CLIP(PACK:Packaging)
      .
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      IF p:Only_Invoiced = TRUE AND INV:IID = 0
         ReturnValue = Record:Filtered
      ELSE
  ReturnValue = PARENT.ValidateRecord()
      .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_DN_POD','Print_DN_POD','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Manifest PROCEDURE (p:MID, p:Output, p:SkipPreview)

Progress:Thermometer BYTE                                  !
LOC:Contol_Vals      GROUP,PRE(L_CG)                       !
DID                  ULONG                                 !Delivery ID
SkipPreview          BYTE                                  !
                     END                                   !
LOC:Rep_Fields       GROUP,PRE(L_RF)                       !
Weight               DECIMAL(15,2)                         !In kg's
VolumetricWeight     DECIMAL(15,2)                         !Weight based on Volumetric calculation (in kgs)
Charge               DECIMAL(17,4)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
State                STRING(20)                            !State of this manifest
Driver               STRING(50)                            !First Name
Reg_of_Hours         STRING(20)                            !
Leg_Cost             DECIMAL(12,2)                         !
                     END                                   !
LOC:Delivery_Totals  GROUP,PRE(L_DT)                       !
DID                  ULONG                                 !Delivery ID
DIID                 ULONG                                 !Delivery Item ID
MLID                 ULONG                                 !Manifest Load ID
InsuranceCharge      DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Insurance            DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Docs                 DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Fuel                 DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Charge               DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
                     END                                   !
LOC:Manifest_Totals  GROUP,PRE(L_MT)                       !
Cost                 DECIMAL(10,2)                         !
MID                  ULONG                                 !Manifest ID - last used
Delivery_Charges     DECIMAL(11,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Total_Weight         DECIMAL(10,2)                         !In kg's
Gross_Profit         DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Gross_Profit_Percent DECIMAL(6,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Average_C_Per_Kg     DECIMAL(7,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Legs_Cost            DECIMAL(10,2)                         !
Total_Cost           DECIMAL(10,2)                         !Includes extra legs cost
Out_VAT              DECIMAL(10,2)                         !
Legs_Cost_VAT        DECIMAL(10,2)                         !
Delivery_Charges_Ex  DECIMAL(11,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel, Insurance and Additional Charges charges
                     END                                   !
DINo_IID             CSTRING(21)                           !
Process:View         VIEW(ManifestLoad)
                       PROJECT(MAL:MID)
                       PROJECT(MAL:MLID)
                       PROJECT(MAL:TTID)
                       JOIN(MALD:FSKey_MLID_DIID,MAL:MLID)
                         PROJECT(MALD:UnitsLoaded)
                         PROJECT(MALD:DIID)
                         JOIN(DELI:PKey_DIID,MALD:DIID)
                           PROJECT(DELI:DID)
                           PROJECT(DELI:DIID)
                           PROJECT(DELI:Units)
                           PROJECT(DELI:VolumetricWeight)
                           PROJECT(DELI:Weight)
                           JOIN(DEL:PKey_DID,DELI:DID)
                             PROJECT(DEL:AdditionalCharge)
                             PROJECT(DEL:Charge)
                             PROJECT(DEL:DID)
                             PROJECT(DEL:DocumentCharge)
                             PROJECT(DEL:FuelSurcharge)
                             PROJECT(DEL:InsuranceRate)
                             PROJECT(DEL:Insure)
                             PROJECT(DEL:TollCharge)
                             PROJECT(DEL:TotalConsignmentValue)
                             PROJECT(DEL:VATRate)
                             PROJECT(DEL:DeliveryAID)
                             PROJECT(DEL:CollectionAID)
                             JOIN(A_ADD:PKey_AID,DEL:DeliveryAID)
                               PROJECT(A_ADD:AddressName)
                               PROJECT(A_ADD:SUID)
                               JOIN(A_SUBU:PKey_SUID,A_ADD:SUID)
                                 PROJECT(A_SUBU:Suburb)
                               END
                             END
                             JOIN(ADD:PKey_AID,DEL:CollectionAID)
                               PROJECT(ADD:AddressName)
                             END
                           END
                           JOIN(INI:FKey_DIID,DELI:DIID)
                           END
                         END
                       END
                       JOIN(MAN:PKey_MID,MAL:MID)
                         PROJECT(MAN:Cost)
                         PROJECT(MAN:CreatedDate)
                         PROJECT(MAN:VATRate)
                         PROJECT(MAN:BID)
                         PROJECT(MAN:VCID)
                         PROJECT(MAN:TID)
                         PROJECT(MAN:JID)
                         JOIN(BRA:PKey_BID,MAN:BID)
                           PROJECT(BRA:BranchName)
                         END
                         JOIN(VCO:PKey_VCID,MAN:VCID)
                           PROJECT(VCO:CompositionName)
                         END
                         JOIN(TRA:PKey_TID,MAN:TID)
                           PROJECT(TRA:TransporterName)
                         END
                         JOIN(JOU:PKey_JID,MAN:JID)
                           PROJECT(JOU:Journey)
                         END
                       END
                       JOIN(TRU:PKey_TTID,MAL:TTID)
                         PROJECT(TRU:Registration)
                         PROJECT(TRU:VMMID)
                         JOIN(VMM:PKey_VMMID,TRU:VMMID)
                           PROJECT(VMM:MakeModel)
                         END
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(615,1354,7052,8333),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular,CHARSET:ANSI), |
  THOUS
                       HEADER,AT(615,1000,7052,0),USE(?Header)
                       END
break_manifest         BREAK(MAL:MID),USE(?BREAK1)
                         HEADER,AT(0,0,,2292),USE(?GROUPHEADER1),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  PAGEBEFORE(-1)
                           STRING('Branch:'),AT(4156,31,552,198),USE(?String31:2),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                           STRING('State:'),AT(4354,490),USE(?String54),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@s20),AT(5396,490,1542,167),USE(L_RF:State),FONT(,,,FONT:bold,CHARSET:ANSI),RIGHT(1)
                           STRING(@s35),AT(4792,31,1302,198),USE(BRA:BranchName),FONT(,10,,,CHARSET:ANSI),LEFT(1)
                           STRING('Journey:'),AT(927,271),USE(?String40:5),FONT(,,,FONT:underline,CHARSET:ANSI),TRN
                           STRING(@s70),AT(1615,260,5260,208),USE(JOU:Journey),FONT(,,,FONT:bold+FONT:underline,CHARSET:ANSI), |
  LEFT(1),TRN
                           STRING(@n-14.2),AT(5917,698,1021,167),USE(L_MT:Legs_Cost),RIGHT(1),TRN
                           STRING(@n-14.2),AT(2094,1094,1021,167),USE(L_MT:Out_VAT),RIGHT(1)
                           STRING('Legs Cost (incl.):'),AT(4365,698),USE(?String40:7),TRN
                           STRING('Total Cost:'),AT(104,896),USE(?String40:6),TRN
                           STRING('Output VAT:'),AT(104,1094),USE(?String81),TRN
                           STRING('Manifest No.:'),AT(1125,31),USE(?String31),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@n_10),AT(2073,31,1021,198),USE(MAL:MID),FONT(,10,,FONT:bold,CHARSET:ANSI),RIGHT(1), |
  TRN
                           STRING('Transporter:'),AT(104,490),USE(?String40),TRN
                           STRING(@s35),AT(1104,490,2010,167),USE(TRA:TransporterName),FONT(,,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),TRN
                           STRING('Gross Profit (GP - excl.):'),AT(4354,1125),USE(?String50),TRN
                           STRING(@n-14.2),AT(2094,896,1021,167),USE(L_MT:Total_Cost),RIGHT(1),TRN
                           STRING('Legs Cost - Out VAT:'),AT(4365,896),USE(?String40:8),TRN
                           STRING(@n-14.2),AT(5917,896,1021,167),USE(L_MT:Legs_Cost_VAT),RIGHT(1),TRN
                           STRING('Cost:'),AT(104,698),USE(?String40:2),TRN
                           STRING(@n-14.2),AT(2094,698,1021,167),USE(L_MT:Cost),RIGHT(1)
                           STRING(@n-14.2),AT(2094,1510,1021,167),USE(L_MT:Total_Weight),RIGHT(1)
                           STRING('Average c./kg:'),AT(4354,1510),USE(?String50:3),TRN
                           STRING(@n-10.2),AT(5917,1510,1021,167),USE(L_MT:Average_C_Per_Kg),RIGHT(1)
                           STRING('Closing Kms: {21}_{26}'),AT(4354,1750),USE(?String40:10)
                           STRING('Starting Kms: {33}_{26}'),AT(104,1750),USE(?String40:9)
                           STRING('Deliveries Total Freight Charge:'),AT(104,1292),USE(?String40:3),TRN
                           STRING(@n-15.2),AT(2094,1292,1021,167),USE(L_MT:Delivery_Charges),RIGHT(1)
                           STRING(@n-14.2),AT(5917,1125,1021,167),USE(L_MT:Gross_Profit),RIGHT(1)
                           STRING('GP %:'),AT(4354,1313),USE(?String50:2),TRN
                           STRING('Total Weight:'),AT(104,1510),USE(?String40:4),TRN
                           STRING(@n-9.2),AT(5917,1313,1021,167),USE(L_MT:Gross_Profit_Percent),RIGHT(1)
                           LINE,AT(21,1979,7000,0),USE(?Line8:4),COLOR(COLOR:Black)
                           STRING(@s35),AT(938,2052,1583,167),USE(VCO:CompositionName),TRN
                           STRING('Driver:'),AT(4917,2052),USE(?String69:2),TRN
                           STRING(@s35),AT(5323,2052,1583,167),USE(L_RF:Driver)
                           LINE,AT(21,2281,7000,0),USE(?Line8:6),COLOR(COLOR:Black)
                           STRING('Reg.:'),AT(3292,2052),USE(?String69:3),TRN
                           STRING(@s20),AT(3646,2052,979,167),USE(L_RF:Reg_of_Hours)
                           STRING('Vehicle Comp.:'),AT(125,2052),USE(?String69),TRN
                         END
break_trans              BREAK(MAL:TTID),USE(?BREAK2)
                           HEADER,AT(0,0,,542),USE(?GROUPHEADER2)
                             STRING('Vehicle Reg.:'),AT(167,63),USE(?String33:2),TRN
                             STRING('Model:'),AT(3042,63),USE(?STRING1),TRN
                             STRING(@s35),AT(3552,63,2500,208),USE(VMM:MakeModel),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                             STRING(@s20),AT(1063,63,1708,208),USE(TRU:Registration),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                             STRING('Act Kg'),AT(3396,312,542,167),USE(?String3:5),CENTER,TRN
                             STRING('Freight'),AT(4562,312,615,167),USE(?String3:7),CENTER,TRN
                             STRING('Suburb'),AT(5302,312,813,167),USE(?String3:10),CENTER,TRN
                             STRING('2nd Tran.'),AT(6271,312,729,167),USE(?String3:8),CENTER,TRN
                             STRING('Items'),AT(2990,312,406,167),USE(?String3:4),TRN
                             STRING('Vol Kg'),AT(3958,312,542,167),USE(?String3:6),CENTER,TRN
                             STRING('Sender'),AT(885,312,1000,167),USE(?String3:2),CENTER,TRN
                             STRING('Deliver To'),AT(1937,312,1000,167),USE(?String3:3),CENTER,TRN
                             STRING('DI / Inv. No.'),AT(52,313,771,167),USE(?String3),TRN
                             LINE,AT(854,271,0,229),USE(?Line1),COLOR(COLOR:Black)
                             LINE,AT(1906,271,0,229),USE(?Line1:2),COLOR(COLOR:Black)
                             LINE,AT(2969,271,0,229),USE(?Line1:3),COLOR(COLOR:Black)
                             LINE,AT(3406,271,0,229),USE(?Line1:4),COLOR(COLOR:Black)
                             LINE,AT(3969,271,0,229),USE(?Line1:5),COLOR(COLOR:Black)
                             LINE,AT(4531,271,0,229),USE(?Line1:6),COLOR(COLOR:Black)
                             LINE,AT(5250,271,0,229),USE(?Line1:7),COLOR(COLOR:Black)
                             LINE,AT(6135,271,0,229),USE(?Line1:8),COLOR(COLOR:Black)
                             LINE,AT(21,21,7000,0),USE(?Line8),COLOR(COLOR:Black)
                             LINE,AT(21,271,7000,0),USE(?Line8:2),COLOR(COLOR:Black)
                             LINE,AT(21,500,7000,0),USE(?Line8:22),COLOR(COLOR:Black)
                           END
break_delivery             BREAK(DEL:DINo),USE(?BREAK3)
Detail                       DETAIL,AT(0,0,7052,0),USE(?Detail),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                               GROUP,AT(31,-52,6948,271),USE(?Group1),BOXED,HIDE,TRN
                                 STRING(@n~L ~6),AT(5167,42,406,167),USE(MALD:UnitsLoaded)
                                 STRING('Not printing detail lines at all'),AT(52,42,885,167),USE(?String1:2),TRN
                                 STRING(@n~AW~-13.2),AT(1552,42,875,167),USE(DELI:Weight),RIGHT(1)
                                 STRING(@n-11.2),AT(3781,42,677,167),USE(L_RF:VolumetricWeight),RIGHT(1),HIDE
                                 STRING(@n~U ~6),AT(5813,42),USE(DELI:Units)
                                 STRING(@n_10),AT(1042,42,458,167),USE(DELI:DID),RIGHT(1),TRN
                                 STRING(@n~CW~-13.2),AT(2469,42,875,167),USE(L_RF:Weight),RIGHT(1)
                                 STRING(@n10.2),AT(4490,21,646,167),USE(L_RF:Charge),RIGHT(1)
                                 STRING(@n10.2),AT(4490,21,646,167),USE(L_RF:Leg_Cost,,?L_RF:Leg_Cost:3),RIGHT(1)
                               END
                             END
detail_Trans                 DETAIL,AT(0,0,,271),USE(?detail_Trans),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                               LINE,AT(1583,42,4458,0),USE(?Line8:5),COLOR(COLOR:Black)
                               STRING('Leg:'),AT(1635,73,219,167),USE(?String39),TRN
                               STRING(@n3),AT(1938,73,302,167),USE(DELL:Leg),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                               STRING('2nd Transporter:'),AT(2323,73,813,167),USE(?String35),TRN
                               STRING(@s35),AT(3208,73,1573,167),USE(A_TRA:TransporterName),FONT(,,,FONT:regular,CHARSET:ANSI), |
  TRN
                               STRING('Cost:'),AT(4906,73,323,167),USE(?String35:2),TRN
                               STRING(@n13.2),AT(5208,73,771,167),USE(L_RF:Leg_Cost),FONT(,,,FONT:regular,CHARSET:ANSI),RIGHT(1), |
  TRN
                             END
                             FOOTER,AT(0,0,,198),USE(?GROUPFOOTER1),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                               STRING(@s50),AT(5302,10,813,167),USE(A_SUBU:Suburb)
                               STRING(@s35),AT(6729,10,271,167),USE(A_TRA:TransporterName,,?A_TRA:TransporterName:2),FONT(, |
  ,,FONT:regular,CHARSET:ANSI),TRN
                               STRING(@n-13.2),AT(4583,10,646,167),USE(L_RF:Charge,,?L_RF:Charge:3),RIGHT(1),SUM,TALLY(Detail), |
  RESET(break_delivery),TRN
                               STRING(@n13.2),AT(5927,10,771,167),USE(L_RF:Leg_Cost,,?L_RF:Leg_Cost:2),FONT(,,,FONT:regular, |
  CHARSET:ANSI),RIGHT(1),TALLY(Detail),TRN
                               STRING(@s35),AT(885,10,1000,167),USE(ADD:AddressName),TRN
                               STRING(@s35),AT(1937,10,1000,167),USE(A_ADD:AddressName),TRN
                               STRING(@n-11),AT(3396,10,542,167),USE(L_RF:Weight,,?L_RF:Weight:2),RIGHT(1),SUM,TALLY(Detail), |
  RESET(break_delivery),TRN
                               STRING(@n6),AT(2948,10),USE(MALD:UnitsLoaded,,?MALD:UnitsLoaded:2),RIGHT(1),SUM,TALLY(Detail), |
  RESET(break_delivery),TRN
                               STRING(@n-11),AT(3958,10,542,167),USE(L_RF:VolumetricWeight,,?L_RF:VolumetricWeight:2),RIGHT(1), |
  SUM,TALLY(Detail),RESET(break_delivery),TRN
                               LINE,AT(854,0,0,198),USE(?Line12),COLOR(COLOR:Black)
                               LINE,AT(1906,0,0,198),USE(?Line1:22),COLOR(COLOR:Black)
                               LINE,AT(2969,0,0,198),USE(?Line1:32),COLOR(COLOR:Black)
                               LINE,AT(3406,0,0,198),USE(?Line1:42),COLOR(COLOR:Black)
                               LINE,AT(3969,0,0,198),USE(?Line1:52),COLOR(COLOR:Black)
                               LINE,AT(4531,0,0,198),USE(?Line1:62),COLOR(COLOR:Black)
                               LINE,AT(5250,0,0,198),USE(?Line1:72),COLOR(COLOR:Black)
                               LINE,AT(6135,0,0,198),USE(?Line1:82),COLOR(COLOR:Black)
                               STRING(@s20),AT(52,10,792),USE(DINo_IID)
                             END
                           END
                         END
                         FOOTER,AT(0,0,,396),USE(?GROUPFOOTER2)
                           LINE,AT(21,115,7000,0),USE(?Line8:3),COLOR(COLOR:Black)
                           STRING('Total Weight:'),AT(2094,156),USE(?String65:2),TRN
                           STRING(@n-11.2),AT(2979,156),USE(L_RF:Weight,,?L_RF:Weight:3),RIGHT(1),SUM,TALLY(Detail),RESET(break_manifest), |
  TRN
                           STRING(@n6),AT(1240,156),USE(MALD:UnitsLoaded,,?MALD:UnitsLoaded:3),RIGHT(1),SUM,TALLY(Detail), |
  RESET(break_manifest),TRN
                           STRING('Total Freight:'),AT(4167,156),USE(?String65:3),TRN
                           STRING(@n-13.2),AT(5063,156),USE(L_RF:Charge,,?L_RF:Charge:2),RIGHT(1),SUM,TALLY(Detail),RESET(break_manifest), |
  TRN
                           STRING('Units:'),AT(844,156),USE(?String65),TRN
                         END
                       END
                       FOOTER,AT(615,9688,7052,302),USE(?Footer),FONT('MS Sans Serif',,COLOR:Black,,CHARSET:ANSI)
                         STRING('Report Date:'),AT(2188,31),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(2990,31),USE(?ReportDateStamp),TRN
                         STRING('Report Time:'),AT(4156,31),USE(?ReportTimePrompt),TRN
                         STRING('<<-- Time Stamp -->'),AT(4958,31),USE(?ReportTimeStamp),TRN
                         STRING(@N3),AT(6729,31),USE(ReportPageNumber)
                         STRING('Created Date:'),AT(156,31),USE(?ReportDatePrompt:2),TRN
                         STRING(@d6),AT(938,31),USE(MAN:CreatedDate)
                         STRING('Page:'),AT(6313,31),USE(?String61),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Manifest')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DeliveryLegs.SetOpenRelated()
  Relate:DeliveryLegs.Open                                 ! File DeliveryLegs used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterAlias.Open                             ! File TransporterAlias used by this procedure, so make sure it's RelationManager is open
  Access:TruckTrailerAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  IF L_CG:SkipPreview = FALSE THEN
     TargetSelector.AddItem(PDFReporter.IReportGenerator)
  END
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:ManifestLoad, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder('+MAL:MID,+MAL:TTID,+DEL:DINo,+DELI:ItemNo')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:ManifestLoad.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      IF p:MID ~= 0
         ThisReport.SetFilter('MAL:MID=' & p:MID, 'ikb')
      .
  
      ! (p:MID, p:Output, p:SkipPreview)
      ! (ULONG=0, BYTE=0, BYTE=0)
  
  
      L_CG:SkipPreview    = p:SkipPreview
  SELF.SkipPreview = L_CG:SkipPreview
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryLegs.Close
    Relate:TransporterAlias.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      If INI:IID Then
         DINo_IID = DEL:DINo & ' / ' & INI:IID
      Else
         DINo_IID = DEL:DINo
      End
      
      IF L_CG:DID ~= DELI:DID
         L_CG:DID    = DELI:DID
  
         CLEAR(DELL:Record, -1)
         DELL:DID    = L_CG:DID         !DELI:DID
         SET(DELL:FKey_DID_Leg, DELL:FKey_DID_Leg)
         LOOP
            IF Access:DeliveryLegs.TryNext() ~= LEVEL:Benign
               BREAK
            .
            IF DELL:DID ~= L_CG:DID     !DELI:DID
               BREAK
            .
     
     !       message('DELL:DID ~= DELI:DID||' & DELL:DID & ' ~= ' & DELI:DID)
            ! First leg is included in line 1
            IF DELL:Leg <= 1
               CYCLE
            .
     
            CLEAR(A_TRA:Record)
            A_TRA:TID    = DELL:TID
            IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) ~= LEVEL:Benign
               CLEAR(A_TRA:Record)
            .
     
            L_RF:Leg_Cost = DELL:Cost * (1 + (DELL:VATRate / 100))
     
            PRINT(RPT:detail_Trans)
      .  .
  
      CLEAR(L_DT:InsuranceCharge)
      IF DEL:Insure = TRUE
         L_DT:InsuranceCharge    = (DEL:InsuranceRate / 100) * DEL:TotalConsignmentValue
      .
  
      CLEAR(L_RF:Leg_Cost)
  
      ! Load the Delivery Legs....
      CLEAR(A_TRA:Record)
      CLEAR(DELL:Record, -1)
      DELL:DID    = DELI:DID
      SET(DELL:FKey_DID_Leg, DELL:FKey_DID_Leg)
      LOOP
         IF Access:DeliveryLegs.TryNext() ~= LEVEL:Benign
            CLEAR(DELL:Record)
            BREAK
         .
         IF DELL:DID ~= DELI:DID
            CLEAR(DELL:Record)
            BREAK
         .
  
  !       message('DELL:DID ~= DELI:DID||' & DELL:DID & ' ~= ' & DELI:DID)
         ! First leg is included in line 1
         IF DELL:Leg > 1
            CLEAR(DELL:Record)
            BREAK
         .
  
         CLEAR(A_TRA:Record)
         A_TRA:TID    = DELL:TID
         IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) ~= LEVEL:Benign
            CLEAR(A_TRA:Record)
         .
  
         L_RF:Leg_Cost = DELL:Cost * (1 + (DELL:VATRate / 100))
  
         BREAK
      .
  
  
  
      A_TRU:TTID              = VCO:TTID0
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Hours    = A_TRU:Registration
      .
  
      DRI:DRID                = MAN:DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RF:Driver          = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
  
  L_RF:Weight = DELI:Weight * (MALD:UnitsLoaded / DELI:Units)
  L_RF:VolumetricWeight = DELI:VolumetricWeight * (MALD:UnitsLoaded / DELI:Units)
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:detail_Trans)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF L_DT:DIID ~= DELI:DIID OR L_DT:MLID ~= MAL:MLID      ! L_DT:DID ~= DEL:DID OR
         L_DT:DID      = DEL:DID
         L_DT:DIID     = DELI:DIID
         L_DT:MLID     = MAL:MLID
  
!         ! Total loaded items from DID on MLID   /   Total Items on DID
!         L_RF:Charge   = (DEL:DocumentCharge + DEL:FuelSurcharge + DEL:Charge + DEL:AdditionalCharge) * |
!                          (Get_ManLoadItems_Info(MAL:MLID, 1, DEL:DID, DELI:DIID) / Get_DelItem_s_Totals(DEL:DID, 3))

         ! Total loaded items from DID on MLID   /   Total Items on DID
         L_RF:Charge   = (DEL:DocumentCharge + DEL:FuelSurcharge + DEL:TollCharge + DEL:Charge + DEL:AdditionalCharge) * |
                          (Get_ManLoadItems_Info(MAL:MLID, 1, DEL:DID, DELI:DIID) / Get_DelItem_s_Totals(DEL:DID, 3))

         IF GLO:Testing_Mode = TRUE
       db.debugout('[Print_Manifest]  Docs, fuel, charge, additional: ' & DEL:DocumentCharge & ', ' & DEL:FuelSurcharge & ', ' & DEL:TollCharge & ', ' & DEL:Charge & ', ' & DEL:AdditionalCharge)
       db.debugout('[Print_Manifest]  Man load info: ' & CLIP(Get_ManLoadItems_Info(MAL:MLID, 1, DEL:DID, DELI:DIID)) & ',   Del Items: ' & Get_DelItem_s_Totals(DEL:DID, 3))
       db.debugout('[Print_Manifest]  Total: ' & L_RF:Charge & ' + VAT = ' & L_RF:Charge * (1 + (DEL:VATRate / 100)))
         .

         ! Note: The source for these values is the DI and not the Invoice which can cause discrepancy between reports
         L_RF:Charge   = ROUND(L_RF:Charge, 0.01) + ROUND(L_RF:Charge * (DEL:VATRate / 100), 0.01)                    ! Add VAT

    db.debugout('[Print_Manifest]  DID: ' & DEL:DID & ',  DIID: ' & DELI:DIID & ',  DI No: ' & DEL:DINo & ',   Charge: ' & L_RF:Charge)
      .
  
  
  
      IF L_MT:MID ~= MAL:MID                                                                  ! Once per Manifest
         !Message('Cost:'& L_MT:Cost&'<13,10>'&|
         !        'MID: '& L_MT:MID&'<13,10>'&|
         !        'Delivery_Charges: '& L_MT:Delivery_Charges&'<13,10>'&|
         !        'Total_Weight: '& L_MT:Total_Weight&'<13,10>'&|
         !        'Gross_Profit: '& L_MT:Gross_Profit&'<13,10>'&|
         !        'Gross_Profit_Percent: '& L_MT:Gross_Profit_Percent&'<13,10>'&|
         !        'Average_C_Per_Kg: '& L_MT:Average_C_Per_Kg&'<13,10>'&|
         !        'Legs_Cost: '& L_MT:Legs_Cost&'<13,10>'&|
         !        'Total_Cost: '& L_MT:Total_Cost&'<13,10>'&|
         !        'Out_VAT: '& L_MT:Out_VAT&'<13,10>'&|
         !        'Legs_Cost_VAT: '& L_MT:Legs_Cost_VAT&'<13,10>'&|
         !        'Delivery_Charges_Ex: '& L_MT:Delivery_Charges_Ex |
         !        ,'Debig message - before',,,,2)
         GetManifestTotals(MAL:MID,MAN:Cost,MAN:VATRate,LOC:Manifest_Totals,p:Output)
         !Message('Cost:'& L_MT:Cost&'<13,10>'&|
         !        'MID: '& L_MT:MID&'<13,10>'&|
         !        'Delivery_Charges: '& L_MT:Delivery_Charges&'<13,10>'&|
         !        'Total_Weight: '& L_MT:Total_Weight&'<13,10>'&|
         !        'Gross_Profit: '& L_MT:Gross_Profit&'<13,10>'&|
         !        'Gross_Profit_Percent: '& L_MT:Gross_Profit_Percent&'<13,10>'&|
         !        'Average_C_Per_Kg: '& L_MT:Average_C_Per_Kg&'<13,10>'&|
         !        'Legs_Cost: '& L_MT:Legs_Cost&'<13,10>'&|
         !        'Total_Cost: '& L_MT:Total_Cost&'<13,10>'&|
         !        'Out_VAT: '& L_MT:Out_VAT&'<13,10>'&|
         !        'Legs_Cost_VAT: '& L_MT:Legs_Cost_VAT&'<13,10>'&|
         !        'Delivery_Charges_Ex: '& L_MT:Delivery_Charges_Ex |
         !        ,'Debig message - after',,,,2)
         !L_MT:MID                     = MAL:MID
         !
         !! Get_Manifest_Info
         !! (p:MID, p:Option, p:DI_Type, p:Inv_Totals, p:Output, p:Info  )
         !! (ULONG, BYTE    , BYTE=0   , BYTE=0      , BYTE=0  , <STRING>),STRING
         !!   1       2           3           4           5           6
         !!   p:Option
         !!       0.  Charges total
         !!       1.  Insurance total
         !!       2.  Weight total, for this Manifest!    Uses Units_Loaded
         !!       3.  Extra Legs cost
         !!       4.  Charges total incl VAT
         !!       5.  No. DI's
         !!       6.  Extra Legs cost - inc VAT
         !!   p:DI_Type
         !!       0.  All             (default)
         !!       1.  Non-Broking
         !!       2.  Broking
         !!   p:Inv_Totals
         !!       0.  Collect from Delivery info.
         !!       1.  Collect from Invoiced info.     (only Manifest invoices)
         !
         !L_MT:Delivery_Charges        = Get_Manifest_Info(MAL:MID, 4,,, p:Output)             ! 4 is VAT incl
         !
         !L_MT:Cost                    = MAN:Cost                                              ! VAT is excluded
         !
         !L_MT:Legs_Cost               = Get_Manifest_Info(MAL:MID, 6)                         ! VAT is included
         !L_MT:Legs_Cost_VAT           = L_MT:Legs_Cost - Get_Manifest_Info(MAL:MID, 3,,, p:Output)   ! excluded (3), so minus ex to get VAT
         !
         !! L_MT:Legs_Cost  - Not added on example Manifest D22858
         !
         !L_MT:Out_VAT                 = L_MT:Cost * (MAN:VATRate / 100)
         !L_MT:Total_Cost              = L_MT:Cost + L_MT:Out_VAT                              ! Add VAT
         !
         !L_MT:Delivery_Charges_Ex     = Get_Manifest_Info(MAL:MID, 0,,, p:Output)             ! 4 is VAT EXCL
         !
         !L_MT:Gross_Profit            = L_MT:Delivery_Charges_Ex - L_MT:Cost - (L_MT:Legs_Cost - L_MT:Legs_Cost_VAT)
         !
         !L_MT:Gross_Profit_Percent    = (L_MT:Gross_Profit / L_MT:Delivery_Charges_Ex) * 100  ! %
         !
         !
         !L_MT:Total_Weight            = Get_Manifest_Info(MAL:MID, 2,,, p:Output)
         !
         !L_MT:Average_C_Per_Kg        = (L_MT:Delivery_Charges_Ex / L_MT:Total_Weight) * 100     ! in cents (not rands)
  
         EXECUTE MAN:State
            L_RF:State  = 'Loaded'
            L_RF:State  = 'On Route'
            L_RF:State  = 'Transferred'
         ELSE
            L_RF:State  = 'Loading'
      .  .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('Manifest','TransIS','Print Manifest','Print Manifest','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Manifest_Loading PROCEDURE (p:MID)

Progress:Thermometer BYTE                                  !
LOC:Contol_Vals      GROUP,PRE(L_CG)                       !
DID                  ULONG                                 !Delivery ID
                     END                                   !
LOC:Rep_Fields       GROUP,PRE(L_RF)                       !
Weight               DECIMAL(10,2)                         !In kg's
VolumetricWeight     DECIMAL(10,2)                         !Weight based on Volumetric calculation (in kgs)
Charge               DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
State                STRING(20)                            !State of this manifest
Driver               STRING(50)                            !First Name
Reg_of_Hours         STRING(20)                            !
Leg_Cost             DECIMAL(12,2)                         !
                     END                                   !
LOC:Delivery_Totals  GROUP,PRE(L_DT)                       !
DID                  ULONG                                 !Delivery ID
InsuranceCharge      DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Insurance            DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Docs                 DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Fuel                 DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Charge               DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
DIID                 ULONG                                 !Delivery Item ID
MLID                 ULONG                                 !Manifest Load ID
                     END                                   !
LOC:Manifest_Totals  GROUP,PRE(L_MT)                       !
Cost                 DECIMAL(10,2)                         !
MID                  ULONG                                 !Manifest ID - last used
Delivery_Charges     DECIMAL(11,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Total_Weight         DECIMAL(10,2)                         !In kg's
Gross_Profit         DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Gross_Profit_Percent DECIMAL(6,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Average_C_Per_Kg     DECIMAL(7,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Legs_Cost            DECIMAL(10,2)                         !
Total_Cost           DECIMAL(10,2)                         !Includes extra legs cost
Out_VAT              DECIMAL(10,2)                         !
Legs_Cost_VAT        DECIMAL(10,2)                         !
Delivery_Charges_Ex  DECIMAL(12,2)                         !
                     END                                   !
Process:View         VIEW(ManifestLoad)
                       PROJECT(MAL:MID)
                       PROJECT(MAL:MLID)
                       PROJECT(MAL:TTID)
                       JOIN(MALD:FSKey_MLID_DIID,MAL:MLID)
                         PROJECT(MALD:UnitsLoaded)
                         PROJECT(MALD:DIID)
                         JOIN(DELI:PKey_DIID,MALD:DIID)
                           PROJECT(DELI:DID)
                           PROJECT(DELI:Units)
                           PROJECT(DELI:VolumetricWeight)
                           PROJECT(DELI:Weight)
                           JOIN(DEL:PKey_DID,DELI:DID)
                             PROJECT(DEL:Charge)
                             PROJECT(DEL:DID)
                             PROJECT(DEL:DINo)
                             PROJECT(DEL:DocumentCharge)
                             PROJECT(DEL:FuelSurcharge)
                             PROJECT(DEL:InsuranceRate)
                             PROJECT(DEL:Insure)
                             PROJECT(DEL:TotalConsignmentValue)
                             PROJECT(DEL:VATRate)
                             PROJECT(DEL:DeliveryAID)
                             PROJECT(DEL:CollectionAID)
                             JOIN(A_ADD:PKey_AID,DEL:DeliveryAID)
                               PROJECT(A_ADD:AddressName)
                               PROJECT(A_ADD:SUID)
                               JOIN(A_SUBU:PKey_SUID,A_ADD:SUID)
                                 PROJECT(A_SUBU:Suburb)
                               END
                             END
                             JOIN(ADD:PKey_AID,DEL:CollectionAID)
                               PROJECT(ADD:AddressName)
                             END
                           END
                         END
                       END
                       JOIN(MAN:PKey_MID,MAL:MID)
                         PROJECT(MAN:CreatedDate)
                         PROJECT(MAN:VATRate)
                         PROJECT(MAN:BID)
                         PROJECT(MAN:VCID)
                         PROJECT(MAN:TID)
                         PROJECT(MAN:JID)
                         JOIN(BRA:PKey_BID,MAN:BID)
                           PROJECT(BRA:BranchName)
                         END
                         JOIN(VCO:PKey_VCID,MAN:VCID)
                           PROJECT(VCO:CompositionName)
                         END
                         JOIN(TRA:PKey_TID,MAN:TID)
                           PROJECT(TRA:TransporterName)
                         END
                         JOIN(JOU:PKey_JID,MAN:JID)
                           PROJECT(JOU:Journey)
                         END
                       END
                       JOIN(TRU:PKey_TTID,MAL:TTID)
                         PROJECT(TRU:Registration)
                         PROJECT(TRU:VMMID)
                         JOIN(VMM:PKey_VMMID,TRU:VMMID)
                           PROJECT(VMM:MakeModel)
                         END
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress... printing Manifest Loading'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(615,1354,7052,8333),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular,CHARSET:ANSI), |
  THOUS
                       HEADER,AT(615,1000,7052,0),USE(?Header)
                       END
break_manifest         BREAK(MAL:MID)
                         HEADER,AT(0,0,,1990),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI),PAGEBEFORE(-1)
                           STRING('Branch:'),AT(3490,31,552,198),USE(?String31:2),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                           STRING('State:'),AT(4354,490),USE(?String54),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@s20),AT(5396,490,1542,167),USE(L_RF:State),FONT(,,,FONT:bold,CHARSET:ANSI),RIGHT(1)
                           STRING(@s35),AT(4125,31,1302,198),USE(BRA:BranchName),FONT(,10,,,CHARSET:ANSI),LEFT(1)
                           STRING('Provisional'),AT(6135,31),USE(?String31:3),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                           STRING('Journey:'),AT(927,271),USE(?String40:5),FONT(,,,FONT:underline,CHARSET:ANSI),TRN
                           STRING(@s70),AT(1615,260,5260,208),USE(JOU:Journey),FONT(,,,FONT:bold+FONT:underline,CHARSET:ANSI), |
  LEFT(1),TRN
                           STRING(@n-14.2),AT(5917,698,1021,167),USE(L_MT:Legs_Cost),RIGHT(1),TRN
                           STRING('Legs Cost (incl.):'),AT(4365,698),USE(?String40:7),TRN
                           STRING('Total Cost:'),AT(104,896),USE(?String40:6),TRN
                           STRING('Manifest No.:'),AT(1125,31),USE(?String31),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@n_10),AT(2073,31,1021,198),USE(MAL:MID),FONT(,10,,FONT:bold,CHARSET:ANSI),RIGHT(1), |
  TRN
                           STRING('Transporter:'),AT(104,490),USE(?String40),TRN
                           STRING(@s35),AT(1104,490,2010,167),USE(TRA:TransporterName),FONT(,,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),TRN
                           STRING(@n-14.2),AT(2094,896,1021,167),USE(L_MT:Total_Cost),RIGHT(1),TRN
                           STRING('Legs Cost - Out VAT:'),AT(4365,896),USE(?String40:8),TRN
                           STRING(@n-14.2),AT(5917,896,1021,167),USE(L_MT:Legs_Cost_VAT),RIGHT(1),TRN
                           STRING('Cost:'),AT(104,698),USE(?String40:2),TRN
                           STRING(@n-14.2),AT(2094,698,1021,167),USE(L_MT:Cost),RIGHT(1)
                           STRING(@n-14.2),AT(2094,1510,1021,167),USE(L_MT:Total_Weight),RIGHT(1)
                           STRING('Total Weight:'),AT(104,1510),USE(?String40:4),TRN
                           LINE,AT(21,1688,7000,0),USE(?Line8:4),COLOR(COLOR:Black)
                           STRING(@s35),AT(938,1740,1583,167),USE(VCO:CompositionName),TRN
                           STRING('Driver:'),AT(4917,1740),USE(?String69:2),TRN
                           STRING(@s35),AT(5323,1740,1583,167),USE(L_RF:Driver)
                           LINE,AT(21,1948,7000,0),USE(?Line8:6),COLOR(COLOR:Black)
                           STRING('Reg.:'),AT(3292,1740),USE(?String69:3),TRN
                           STRING(@s20),AT(3646,1740,979,167),USE(L_RF:Reg_of_Hours)
                           STRING('Vehicle Comp.:'),AT(125,1740),USE(?String69),TRN
                           STRING('Closing Kms: {18}_{23}'),AT(4354,1271),USE(?String40:10)
                           STRING('Starting Kms: {30}_{23}'),AT(104,1271),USE(?String40:9)
                         END
break_trans              BREAK(MAL:TTID)
                           HEADER,AT(0,0,,542)
                             STRING('Vehicle Reg.:'),AT(167,63),USE(?String33:2),TRN
                             STRING('Model:'),AT(3042,63),TRN
                             STRING(@s35),AT(3552,63,2500,208),USE(VMM:MakeModel),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                             STRING(@s20),AT(1063,63,1708,208),USE(TRU:Registration),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                             STRING('Act Kg'),AT(2938,313,542,167),USE(?String3:5),CENTER,TRN
                             STRING('Suburb'),AT(4844,313,813,167),USE(?String3:10),CENTER,TRN
                             STRING('2nd Transporter'),AT(5677,313,1323,167),USE(?String3:8),CENTER,TRN
                             STRING('Items'),AT(2531,313,406,167),USE(?String3:4),TRN
                             STRING('Vol Kg'),AT(3500,313,542,167),USE(?String3:6),CENTER,TRN
                             STRING('Sender'),AT(427,313,1000,167),USE(?String3:2),CENTER,TRN
                             STRING('Deliver To'),AT(1479,313,1000,167),USE(?String3:3),CENTER,TRN
                             STRING('DI No.'),AT(52,313,323,167),USE(?String3),TRN
                             LINE,AT(396,271,0,229),USE(?Line1),COLOR(COLOR:Black)
                             LINE,AT(1448,271,0,229),USE(?Line1:2),COLOR(COLOR:Black)
                             LINE,AT(2510,271,0,229),USE(?Line1:3),COLOR(COLOR:Black)
                             LINE,AT(2948,271,0,229),USE(?Line1:4),COLOR(COLOR:Black)
                             LINE,AT(3510,271,0,229),USE(?Line1:5),COLOR(COLOR:Black)
                             LINE,AT(4073,271,0,229),USE(?Line1:6),COLOR(COLOR:Black)
                             LINE,AT(4792,271,0,229),USE(?Line1:7),COLOR(COLOR:Black)
                             LINE,AT(5677,271,0,229),USE(?Line1:8),COLOR(COLOR:Black)
                             LINE,AT(21,21,7000,0),USE(?Line8),COLOR(COLOR:Black)
                             LINE,AT(21,271,7000,0),USE(?Line8:2),COLOR(COLOR:Black)
                             LINE,AT(21,500,7000,0),USE(?Line8:23),COLOR(COLOR:Black)
                           END
break_delivery             BREAK(DEL:DID)
Detail                       DETAIL,AT(,,,0),USE(?Detail),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                               GROUP,AT(31,-52,6948,271),USE(?Group1),BOXED,HIDE,TRN
                                 STRING(@n~L ~6),AT(5167,42,406,167),USE(MALD:UnitsLoaded)
                                 STRING('Not printing detail lines at all'),AT(52,42,885,167),USE(?String1),TRN
                                 STRING(@n~AW~-13.2),AT(1552,42,875,167),USE(DELI:Weight),RIGHT(1)
                                 STRING(@n-11.2),AT(3781,42,677,167),USE(L_RF:VolumetricWeight),RIGHT(1),HIDE
                                 STRING(@n~U ~6),AT(5813,42),USE(DELI:Units)
                                 STRING(@n_10),AT(1042,42,458,167),USE(DELI:DID),RIGHT(1),TRN
                                 STRING(@n~CW~-13.2),AT(2469,42,875,167),USE(L_RF:Weight),RIGHT(1)
                                 STRING(@n~CW~-13.2),AT(2469,42,875,167),USE(L_RF:Leg_Cost,,?L_RF:Leg_Cost:3),RIGHT(1)
                               END
                             END
                             FOOTER,AT(0,0,,198),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                               STRING(@s50),AT(4844,10,813,167),USE(A_SUBU:Suburb)
                               STRING(@s35),AT(6271,10,750,167),USE(A_TRA:TransporterName,,?A_TRA:TransporterName:2),FONT(, |
  ,,FONT:regular,CHARSET:ANSI),TRN
                               STRING(@n13.2),AT(5469,10,771,167),USE(L_RF:Leg_Cost,,?L_RF:Leg_Cost:2),FONT(,,,FONT:regular, |
  CHARSET:ANSI),RIGHT(1),TALLY(Detail),TRN
                               STRING(@s35),AT(427,10,1000,167),USE(ADD:AddressName),TRN
                               STRING(@s35),AT(1479,10,1000,167),USE(A_ADD:AddressName),TRN
                               STRING(@n-11),AT(2938,10,542,167),USE(L_RF:Weight,,?L_RF:Weight:2),RIGHT(1),SUM,TALLY(Detail), |
  RESET(break_delivery),TRN
                               STRING(@n6),AT(2490,10),USE(MALD:UnitsLoaded,,?MALD:UnitsLoaded:2),RIGHT(1),SUM,TALLY(Detail), |
  RESET(break_delivery),TRN
                               STRING(@n-11),AT(3500,10,542,167),USE(L_RF:VolumetricWeight,,?L_RF:VolumetricWeight:2),RIGHT(1), |
  SUM,TALLY(Detail),RESET(break_delivery),TRN
                               STRING(@n_10),AT(-115,10,479,167),USE(DEL:DINo),RIGHT(1)
                               LINE,AT(396,0,0,198),USE(?Line133),COLOR(COLOR:Black)
                               LINE,AT(1448,0,0,198),USE(?Line1:23),COLOR(COLOR:Black)
                               LINE,AT(2510,0,0,198),USE(?Line1:33),COLOR(COLOR:Black)
                               LINE,AT(2948,0,0,198),USE(?Line1:43),COLOR(COLOR:Black)
                               LINE,AT(3510,0,0,198),USE(?Line1:53),COLOR(COLOR:Black)
                               LINE,AT(4073,0,0,198),USE(?Line1:63),COLOR(COLOR:Black)
                               LINE,AT(4792,0,0,198),USE(?Line1:73),COLOR(COLOR:Black)
                               LINE,AT(5677,0,0,198),USE(?Line1:83),COLOR(COLOR:Black)
                             END
detail_Trans                 DETAIL,AT(,,,271),USE(?detail_Trans),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                               LINE,AT(1583,42,4458,0),USE(?Line8:5),COLOR(COLOR:Black)
                               STRING('Leg:'),AT(1635,73,219,167),USE(?String39),TRN
                               STRING(@n3),AT(1938,73,302,167),USE(DELL:Leg),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                               STRING('2nd Transporter:'),AT(2323,73,813,167),USE(?String35),TRN
                               STRING(@s35),AT(3208,73,1573,167),USE(A_TRA:TransporterName),FONT(,,,FONT:regular,CHARSET:ANSI), |
  TRN
                               STRING('Cost:'),AT(4906,73,323,167),USE(?String35:2),TRN
                               STRING(@n13.2),AT(5208,73,771,167),USE(L_RF:Leg_Cost),FONT(,,,FONT:regular,CHARSET:ANSI),RIGHT(1), |
  TRN
                             END
                           END
                         END
                         FOOTER,AT(0,0,,396)
                           LINE,AT(21,115,7000,0),USE(?Line8:3),COLOR(COLOR:Black)
                           STRING('Total Weight:'),AT(2094,156),USE(?String65:2),TRN
                           STRING(@n-11.2),AT(2979,156),USE(L_RF:Weight,,?L_RF:Weight:3),RIGHT(1),SUM,TALLY(Detail),RESET(break_manifest), |
  TRN
                           STRING(@n6),AT(1240,156),USE(MALD:UnitsLoaded,,?MALD:UnitsLoaded:3),RIGHT(1),SUM,TALLY(Detail), |
  RESET(break_manifest),TRN
                           STRING('Units:'),AT(844,156),USE(?String65),TRN
                         END
                       END
                       FOOTER,AT(615,9688,7052,302),USE(?Footer),FONT('MS Sans Serif',,COLOR:Black,,CHARSET:ANSI)
                         STRING('Created Date:'),AT(156,31),USE(?ReportDatePrompt:2),TRN
                         STRING(@d6),AT(938,31),USE(MAN:CreatedDate)
                         STRING('Report Date:'),AT(2125,31),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(2927,31),USE(?ReportDateStamp),TRN
                         STRING('Report Time:'),AT(4302,31),USE(?ReportTimePrompt),TRN
                         STRING('<<-- Time Stamp -->'),AT(5104,31),USE(?ReportTimeStamp),TRN
                         STRING(@N3),AT(6729,31),USE(ReportPageNumber)
                         STRING('Page:'),AT(6313,31),USE(?String61),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Manifest_Loading')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DeliveryLegs.SetOpenRelated()
  Relate:DeliveryLegs.Open                                 ! File DeliveryLegs used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterAlias.Open                             ! File TransporterAlias used by this procedure, so make sure it's RelationManager is open
  Access:TruckTrailerAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Manifest_Loading',ProgressWindow)    ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:ManifestLoad, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder('+MAL:MID,+MAL:TTID,+DEL:DINo,+DELI:ItemNo')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:ManifestLoad.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      IF p:MID ~= 0
         ThisReport.SetFilter('MAL:MID = ' & p:MID, 'ikb')
      .
  
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryLegs.Close
    Relate:TransporterAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Manifest_Loading',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      IF L_CG:DID ~= DELI:DID
         L_CG:DID    = DELI:DID
  
         CLEAR(DELL:Record, -1)
         DELL:DID    = L_CG:DID         !DELI:DID
         SET(DELL:FKey_DID_Leg, DELL:FKey_DID_Leg)
         LOOP
            IF Access:DeliveryLegs.TryNext() ~= LEVEL:Benign
               BREAK
            .
            IF DELL:DID ~= L_CG:DID     !DELI:DID
               BREAK
            .
     
     !       message('DELL:DID ~= DELI:DID||' & DELL:DID & ' ~= ' & DELI:DID)
            ! First leg is included in line 1
            IF DELL:Leg <= 1
               CYCLE
            .
     
            CLEAR(A_TRA:Record)
            A_TRA:TID    = DELL:TID
            IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) ~= LEVEL:Benign
               CLEAR(A_TRA:Record)
            .
     
            L_RF:Leg_Cost = DELL:Cost * (1 + (DELL:VATRate / 100))
     
            PRINT(RPT:detail_Trans)
      .  .
  
      CLEAR(L_DT:InsuranceCharge)
      IF DEL:Insure = TRUE
         L_DT:InsuranceCharge    = (DEL:InsuranceRate / 100) * DEL:TotalConsignmentValue
      .
  
  !       message('DELI:DID||' & DELI:DID)
  
  
      ! Load the Delivery Legs....
      CLEAR(L_RF:Leg_Cost)
  
      CLEAR(A_TRA:Record)
      CLEAR(DELL:Record, -1)
      DELL:DID    = DELI:DID
      SET(DELL:FKey_DID_Leg, DELL:FKey_DID_Leg)
      LOOP
         IF Access:DeliveryLegs.TryNext() ~= LEVEL:Benign
            CLEAR(DELL:Record)
            BREAK
         .
         IF DELL:DID ~= DELI:DID
            CLEAR(DELL:Record)
            BREAK
         .
  
  !       message('DELL:DID ~= DELI:DID||' & DELL:DID & ' ~= ' & DELI:DID)
         ! First leg is included in line 1
         IF DELL:Leg > 1
            CLEAR(DELL:Record)
            BREAK
         .
  
         CLEAR(A_TRA:Record)
         A_TRA:TID    = DELL:TID
         IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) ~= LEVEL:Benign
            CLEAR(A_TRA:Record)
         .
  
         L_RF:Leg_Cost        = DELL:Cost * (1 + (DELL:VATRate / 100))
  
         BREAK
      .
  
  
  
      A_TRU:TTID              = VCO:TTID0
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Hours    = A_TRU:Registration
      .
  
      DRI:DRID                = MAN:DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RF:Driver          = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
  L_RF:Weight = DELI:Weight * (MALD:UnitsLoaded / DELI:Units)
  L_RF:VolumetricWeight = DELI:VolumetricWeight * (MALD:UnitsLoaded / DELI:Units)
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:detail_Trans)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  !               old
  !    CLEAR(DELL:Record, -1)
  !    DELL:DID    = DELI:DID
  !    SET(DELL:FKey_DID_Leg, DELL:FKey_DID_Leg)
  !    LOOP
  !       IF Access:DeliveryLegs.TryNext() ~= LEVEL:Benign
  !          BREAK
  !       .
  !       IF DELL:DID ~= DELI:DID
  !          BREAK
  !       .
  !
  !!       message('DELL:DID ~= DELI:DID||' & DELL:DID & ' ~= ' & DELI:DID)
  !       ! First leg is included in line 1
  !       IF DELL:Leg <= 1
  !          CYCLE
  !       .
  !
  !       CLEAR(A_TRA:Record)
  !       A_TRA:TID    = DELL:TID
  !       IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) ~= LEVEL:Benign
  !          CLEAR(A_TRA:Record)
  !       .
  !
  !       L_RF:Leg_Cost        = DELL:Cost * (1 + (DELL:VATRate / 100))
  !
  !       PRINT(RPT:detail_Trans)
  !    .
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF L_DT:DIID ~= DELI:DIID OR L_DT:MLID ~= MAL:MLID      ! L_DT:DID ~= DEL:DID OR
         L_DT:DID      = DEL:DID
         L_DT:DIID     = DELI:DIID
         L_DT:MLID     = MAL:MLID
  
  !    IF L_DT:DID ~= DEL:DID
  !       L_DT:DID      = DEL:DID
         ! Total loaded items from DID on MLID   /   Total Items on DID
  !       L_RF:Charge   = (DEL:DocumentCharge + DEL:FuelSurcharge + DEL:Charge) * |
  !                        Get_ManLoadItems_Info(MAL:MLID, 1, DEL:DID) / Get_DelItem_s_Totals(DEL:DID, 3)
  
         L_RF:Charge   = (DEL:DocumentCharge + DEL:FuelSurcharge + DEL:Charge + DEL:AdditionalCharge) * |
                          (Get_ManLoadItems_Info(MAL:MLID, 1, DEL:DID, DELI:DIID) / Get_DelItem_s_Totals(DEL:DID, 3))
  
         L_RF:Charge   = L_RF:Charge * (1 + (DEL:VATRate / 100))                              ! Add VAT
  
    db.debugout('[Print_Manifest]  DEL:DID: ' & DEL:DID & ',   Charge: ' & L_RF:Charge)
      .
  
  
  
  !    IF L_MT:MID ~= MAL:MID
  !       L_MT:MID                     = MAL:MID
  !
  !       L_MT:Delivery_Charges        = Get_Manifest_Info(MAL:MID, 4)                         ! 4 is VAT incl
  !!       L_MT:Delivery_Charges        = L_MT:Delivery_Charges * (1 + (DEL:VATRate / 100))     ! Add VAT  - VAT is included!!!!
  !
  !       L_MT:Cost                    = MAN:Cost                                       ! VAT is included
  !       L_MT:Legs_Cost               = Get_Manifest_Info(MAL:MID, 3)
  !       L_MT:Legs_Cost_VAT           = L_MT:Legs_Cost - (L_MT:Legs_Cost / (1 + (MAN:VATRate / 100)))     ! Legs cost, plus VAT
  !
  !       ! L_MT:Legs_Cost  - Not added on example Manifest D22858
  !!       L_MT:Total_Cost              = L_MT:Cost
  !!       L_MT:Out_VAT                 = L_MT:Cost - (L_MT:Cost / (1 + (MAN:VATRate / 100)))
  !
  !       L_MT:Total_Cost              = L_MT:Cost + (L_MT:Cost * (MAN:VATRate / 100))     ! Add VAT
  !       L_MT:Out_VAT                 = L_MT:Cost * (MAN:VATRate / 100)     !L_MT:Cost - (L_MT:Cost / (1 + (MAN:VATRate / 100)))
  !
  !       L_MT:Gross_Profit            = L_MT:Delivery_Charges - L_MT:Total_Cost - L_MT:Legs_Cost
  !
  !       L_MT:Gross_Profit_Percent    = (L_MT:Gross_Profit / L_MT:Delivery_Charges) * 100     ! %
  !
  !!       MESSAGE('MAL:MID: ' & MAL:MID)
  !
  !       L_MT:Total_Weight            = Get_Manifest_Info(MAL:MID, 2)
  !
  !       L_MT:Average_C_Per_Kg        = (L_MT:Delivery_Charges / L_MT:Total_Weight) * 100     ! in cents (not rands)
  !
  !       EXECUTE MAN:State
  !          L_RF:State  = 'Loaded'
  !          L_RF:State  = 'On Route'
  !          L_RF:State  = 'Transferred'
  !       ELSE
  !          L_RF:State  = 'Loading'
  !    .  .
  
  
      IF L_MT:MID ~= MAL:MID
         L_MT:MID                     = MAL:MID
  
         L_MT:Delivery_Charges        = Get_Manifest_Info(MAL:MID, 4)                         ! 4 is VAT incl
  
         L_MT:Cost                    = MAN:Cost                                              ! VAT is excluded
  
         L_MT:Legs_Cost               = Get_Manifest_Info(MAL:MID, 6)                         ! VAT is included
         L_MT:Legs_Cost_VAT           = L_MT:Legs_Cost - Get_Manifest_Info(MAL:MID, 3)        ! excluded (3), so minus ex to get VAT
         
         ! L_MT:Legs_Cost  - Not added on example Manifest D22858
  
         L_MT:Out_VAT                 = L_MT:Cost * (MAN:VATRate / 100)
         L_MT:Total_Cost              = L_MT:Cost + L_MT:Out_VAT                              ! Add VAT
  
         L_MT:Delivery_Charges_Ex     =  Get_Manifest_Info(MAL:MID, 0)                        ! 4 is VAT EXCL
  
         L_MT:Gross_Profit            = L_MT:Delivery_Charges_Ex - L_MT:Cost - L_MT:Legs_Cost
  
         L_MT:Gross_Profit_Percent    = (L_MT:Gross_Profit / L_MT:Delivery_Charges_Ex) * 100  ! %
  
  
         L_MT:Total_Weight            = Get_Manifest_Info(MAL:MID, 2)
  
         L_MT:Average_C_Per_Kg        = (L_MT:Delivery_Charges_Ex / L_MT:Total_Weight) * 100     ! in cents (not rands)
  
         EXECUTE MAN:State
            L_RF:State  = 'Loaded'
            L_RF:State  = 'On Route'
            L_RF:State  = 'Transferred'
         ELSE
            L_RF:State  = 'Loading'
      .  .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Manifest_Loading','Print_Manifest_Loading','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! DID or IID passed to restrict to a Delivery or Invoice
!!! </summary>
Print_Invoices PROCEDURE (ULONG p:DID,ULONG p:IID,LONG p:From,LONG p:To,BYTE p:Un_Printed,BYTE p:Preview,BYTE p:Original)

Progress:Thermometer BYTE                                  !
LOC:Invoice_Type     STRING(28)                            !
LOC:MIDs             STRING(100)                           !
LOC:Last_IID         ULONG                                 !Invoice Number
LOC:Last_DIID        ULONG                                 !Delivery Item ID
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
LOC:SkipPreview      BYTE                                  !
LOC:Commodity_Plus   STRING(150)                           !
LOC:Freight_Additional_Total DECIMAL(14,2)                 !Includes freight + additional charges
Process:View         VIEW(_Invoice)
                       PROJECT(INV:AdditionalCharge)
                       PROJECT(INV:ClientLine1)
                       PROJECT(INV:ClientLine2)
                       PROJECT(INV:ClientName)
                       PROJECT(INV:ClientPostalCode)
                       PROJECT(INV:ClientReference)
                       PROJECT(INV:ClientSuburb)
                       PROJECT(INV:ConsigneeLine1)
                       PROJECT(INV:ConsigneeLine2)
                       PROJECT(INV:ConsigneeName)
                       PROJECT(INV:ConsigneePostalCode)
                       PROJECT(INV:ConsigneeSuburb)
                       PROJECT(INV:DID)
                       PROJECT(INV:Documentation)
                       PROJECT(INV:FreightCharge)
                       PROJECT(INV:FuelSurcharge)
                       PROJECT(INV:ICID)
                       PROJECT(INV:IID)
                       PROJECT(INV:InvoiceDate)
                       PROJECT(INV:InvoiceMessage)
                       PROJECT(INV:MIDs)
                       PROJECT(INV:Printed)
                       PROJECT(INV:ShipperLine1)
                       PROJECT(INV:ShipperLine2)
                       PROJECT(INV:ShipperName)
                       PROJECT(INV:ShipperPostalCode)
                       PROJECT(INV:ShipperSuburb)
                       PROJECT(INV:TollCharge)
                       PROJECT(INV:Total)
                       PROJECT(INV:VAT)
                       PROJECT(INV:VATNo)
                       PROJECT(INV:VolumetricWeight)
                       PROJECT(INV:Weight)
                       PROJECT(INV:CID)
                       JOIN(DEL:PKey_DID,INV:DID)
                         PROJECT(DEL:DINo)
                       END
                       JOIN(BRA:PKey_BID,INV:IID)
                         PROJECT(BRA:BranchName)
                       END
                       JOIN(CLI:PKey_CID,INV:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:InvoiceMessage)
                       END
                       JOIN(INI:FKey_IID,INV:IID)
                         PROJECT(INI:Commodity)
                         PROJECT(INI:Description)
                         PROJECT(INI:ItemNo)
                         PROJECT(INI:DIID)
                         JOIN(DELI:PKey_DIID,INI:DIID)
                           PROJECT(DELI:Units)
                         END
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,201,119),FONT('MS Sans Serif',,,,CHARSET:ANSI),DOUBLE,CENTER,GRAY, |
  TIMER(1)
                       SHEET,AT(3,3,193,94),USE(?Sheet1)
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(45,53,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(29,41,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(29,67,141,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Cancel'),AT(146,100,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('Deliveries Report'),AT(240,1510,8000,8490),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(240,240,8000,1281),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING(@s28),AT(5156,73,2698,240),USE(LOC:Invoice_Type),FONT(,12,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER
                         LINE,AT(5083,323,2802,0),USE(?Line10),COLOR(COLOR:Black)
                         STRING(@n_10),AT(6083,479),USE(INV:IID),FONT(,10,COLOR:Black,FONT:bold,CHARSET:ANSI),RIGHT(1)
                         STRING(@s35),AT(5365,760,2365,156),USE(BRA:BranchName),CENTER
                         BOX,AT(5083,42,2844,958),USE(?Box5),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         IMAGE('fbn_logo_small.jpg'),AT(104,94,2906,875),USE(?Image1)
                         LINE,AT(62,1031,7875,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                         STRING('Phone: +27 (0) 31 205 1705'),AT(3208,521,1812,135),USE(?String60:2),TRN
                         STRING('VAT No.: 4360117727     CK :  1989/001182/23'),AT(83,1094,2729,156),USE(?String60), |
  TRN
                         STRING('Durban Head Office:'),AT(3208,21),USE(?STRING1)
                         STRING('P O Box 1405'),AT(3208,187,1031,167),USE(?STRING1:2)
                         STRING('Hillcrest 3650'),AT(3208,354,1031,167),USE(?STRING1:3)
                         STRING('Phone: +27 (0) 11 824 3067'),AT(3208,687,1812,135),USE(?String60:3),TRN
                         STRING('Email: fbndbn@fbn-transport.co.za'),AT(3208,854,1812,135),USE(?String60:4),TRN
                       END
break_Delivery         BREAK(INV:DID),USE(?BREAK1)
                         HEADER,AT(0,0,,2604),USE(?GROUPHEADER1),PAGEBEFORE(-1)
                           BOX,AT(42,42,7875,646),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                           STRING(@s35),AT(1156,396),USE(CLI:ClientName),FONT(,8,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           STRING(@n_10),AT(3844,323,1563,156),USE(DEL:DINo),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                           STRING(@s60),AT(3844,510,1563,156),USE(INV:ClientReference),CENTER
                           STRING(@d6),AT(188,396),USE(INV:InvoiceDate),RIGHT(1)
                           TEXT,AT(5510,323,2354,300),USE(INV:MIDs),CENTER,BOXED
                           STRING(@n_10b),AT(3042,396),USE(CLI:ClientNo),RIGHT(1),TRN
                           LINE,AT(1083,42,0,635),USE(?Line2),COLOR(COLOR:Black)
                           LINE,AT(3802,42,0,635),USE(?Line_tt_mid_down),COLOR(COLOR:Black)
                           LINE,AT(5437,42,0,635),USE(?Line2:3),COLOR(COLOR:Black)
                           STRING('DI Number / Cust. Ref.'),AT(3844,83,1563,156),USE(?String17:2),CENTER,TRN
                           STRING('Manifest Number (s)'),AT(5510,83,2365,156),USE(?String17:3),CENTER,TRN
                           STRING('Date'),AT(125,83,885,156),USE(?String17:4),CENTER,TRN
                           STRING('For Account / Billing Address'),AT(1156,83,2563,156),USE(?String17),CENTER,TRN
                           LINE,AT(42,281,7875,0),USE(?Line4),COLOR(COLOR:Black)
                           BOX,AT(42,875,7875,1323),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                           LINE,AT(3802,875,0,1323),USE(?Line_t_mid_down),COLOR(COLOR:Black)
                           STRING('Collected From'),AT(125,917,3635,156),USE(?String23),CENTER,TRN
                           STRING('Deliver To'),AT(3844,917,4042,156),USE(?String23:2),CENTER,TRN
                           LINE,AT(42,1125,7875,0),USE(?Line4:2),COLOR(COLOR:Black)
                           STRING(@s35),AT(240,1198,3323,156),USE(INV:ShipperName)
                           STRING(@s35),AT(3958,1198,3323,156),USE(INV:ConsigneeName),TRN
                           STRING(@s35),AT(240,1396,3323,156),USE(INV:ShipperLine1)
                           STRING(@s35),AT(3958,1396,3323,156),USE(INV:ConsigneeLine1),TRN
                           STRING(@s35),AT(240,1604,3323,156),USE(INV:ShipperLine2)
                           STRING(@s35),AT(3958,1604,3323,156),USE(INV:ConsigneeLine2),TRN
                           STRING(@s50),AT(240,1802,3323,156),USE(INV:ShipperSuburb)
                           STRING(@s50),AT(3958,1802,3323,156),USE(INV:ConsigneeSuburb),TRN
                           STRING(@s10),AT(3958,1969),USE(INV:ConsigneePostalCode)
                           STRING(@s10),AT(240,1969),USE(INV:ShipperPostalCode)
                           BOX,AT(42,2281,7875,240),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                           STRING('Item No.'),AT(198,2323),USE(?String22),TRN
                           STRING('Description'),AT(906,2323),USE(?String22:3),TRN
                           STRING('Commodity'),AT(3323,2323),USE(?String22:2),TRN
                           STRING('Units'),AT(7479,2323),USE(?String20:3),TRN
                         END
Detail                   DETAIL,AT(0,0,8000,281),USE(?Detail)
                           STRING(@s150),AT(3333,42,3906,167),USE(LOC:Commodity_Plus)
                           STRING(@n6),AT(7323,42,406,167),USE(DELI:Units),RIGHT(1)
                           STRING(@s150),AT(906,42,2083,167),USE(INI:Description)
                           STRING(@n6),AT(198,42),USE(INI:ItemNo)
                         END
                         FOOTER,AT(177,7802,,2240),USE(?GROUPFOOTER1),ABSOLUTE
                           TEXT,AT(52,1917,3698,313),USE(CLI:InvoiceMessage),BOXED,COLOR(00E9E9E9h)
                           STRING('Actual Weight:'),AT(5281,83),USE(?String45),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING(@n-14.2),AT(6396,83),USE(INV:Weight),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI),RIGHT(1)
                           TEXT,AT(104,52,4635,469),USE(INV:InvoiceMessage),BOXED,COLOR(00E9E9E9h)
                           STRING('Volumetric Weight:'),AT(5281,323),USE(?String45:2),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING(@n-14.2),AT(6396,323),USE(INV:VolumetricWeight),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  RIGHT(1)
                           STRING('E-Toll Charge'),AT(3917,646),USE(?String54),TRN
                           STRING(@s35),AT(198,677),USE(INV:ClientName)
                           STRING(@n-14.2),AT(5333,646,2281,198),USE(INV:TollCharge),RIGHT(1)
                           STRING('Documentation'),AT(3917,885),USE(?String54:2),TRN
                           STRING(@s35),AT(198,917),USE(INV:ClientLine1)
                           STRING('Fuel Surcharge'),AT(3917,1135),USE(?String54:5),TRN
                           STRING('Freight Charge'),AT(3917,1396),USE(?String54:3),TRN
                           STRING(@n-14.2),AT(5333,885,2281,198),USE(INV:Documentation),RIGHT(1)
                           STRING(@s35),AT(198,1125),USE(INV:ClientLine2)
                           STRING('VAT'),AT(3917,1646),USE(?String54:4),TRN
                           STRING(@n-15.2),AT(5333,1135,2281,198),USE(INV:FuelSurcharge),RIGHT(1)
                           STRING(@s50),AT(198,1365),USE(INV:ClientSuburb)
                           STRING(@n-15.2),AT(5333,1396,2281,198),USE(LOC:Freight_Additional_Total),RIGHT(1)
                           STRING(@n-14.2),AT(5333,1646,2281,198),USE(INV:VAT),RIGHT(1)
                           STRING('Total'),AT(3917,1927,250,198),USE(?String59),TRN
                           STRING(@s10),AT(198,1604),USE(INV:ClientPostalCode)
                           STRING('VAT No.:'),AT(1844,1604),USE(?String47),TRN
                           STRING(@s20),AT(2396,1604),USE(INV:VATNo)
                           STRING(@n-14.2),AT(5333,1927,2281,198),USE(INV:Total),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  RIGHT(1)
                           BOX,AT(42,604,7748,1281),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                           LINE,AT(3802,604,0,1281),USE(?Line_b_mid_down),COLOR(COLOR:Black)
                           LINE,AT(3802,854,3990,0),USE(?Line11:2),COLOR(COLOR:Black)
                           LINE,AT(3802,1104,3990,0),USE(?Line11),COLOR(COLOR:Black)
                           LINE,AT(3802,1354,3990,0),USE(?Line11:3),COLOR(COLOR:Black)
                           LINE,AT(3802,1604,3990,0),USE(?Line11:4),COLOR(COLOR:Black)
                           BOX,AT(3802,1880,3990,280),USE(?Box6),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                           LINE,AT(5198,604,0,1555),USE(?Line14),COLOR(COLOR:Black)
                         END
                       END
                       FOOTER,AT(240,10000,8000,281),USE(?Footer)
                         STRING('<<-- Time Stamp -->'),AT(2240,52),USE(?ReportTimeStamp),TRN
                         STRING(@N3),AT(7479,42),USE(ReportPageNumber)
                         STRING('Report Date & Time:'),AT(115,52),USE(?ReportDatePrompt:2),TRN
                         STRING('<<-- Date Stamp -->'),AT(1198,52),USE(?ReportDateStamp),TRN
                         STRING('Page:'),AT(7125,42),USE(?ReportTimePrompt:2),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
IKB_Email              PROCEDURE()                         ! New method added to this class instance
                     END

TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Invoices')
      LOC:SkipPreview     = TRUE
      IF p:Preview = TRUE
         LOC:SkipPreview  = FALSE
      .
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:ManifestLoadAlias.Open                            ! File ManifestLoadAlias used by this procedure, so make sure it's RelationManager is open
  Relate:ManifestLoadDeliveriesAlias.Open                  ! File ManifestLoadDeliveriesAlias used by this procedure, so make sure it's RelationManager is open
  Relate:_Invoice.SetOpenRelated()
  Relate:_Invoice.Open                                     ! File _Invoice used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  !    IF TRUE
  !  !    message('in here')
  !  
  !       IF LOC:SkipPreview = TRUE
  !          LOC:SkipPreview = FALSE                         ! ??? we cant get this to work here, if we allow
  !                                                          ! it to skip then report goes to pdf always.. no idea why
  !                                                          ! so we will switch it off again
  !  
  !  !    message('ps - in here')
  !  !        TargetSelector.PrintSelected   = TRUE                  ! Default to printer
  !  !        TargetSelector.WithPrinter     = TRUE
  !  
  !          IF NOT TargetSelector.ASK(1) THEN
  !             SELF.Kill()
  !             RETURN Level:Fatal
  !          END
  !       ELSE
  !          TargetSelector.AddItem(PDFReporter.IReportGenerator)
  !  
  !          IF NOT TargetSelector.ASK(1) THEN
  !             SELF.Kill()
  !             RETURN Level:Fatal
  !          END
  !       .
  !       
  !  
  !       IF NOT TargetSelector.GetPrintSelected() THEN
  !          !SELF.SetReportTarget(TargetSelector.GetSelected())     6/1/14 doesnt exist anymore it seems.
  !          SELF.SetReportTarget(TargetSelector.GetReportSelected())     ! maybe this will work!?!
  !       END
  !  
  !       SELF.AddItem(TargetSelector)
  !  
  !  
  !       ! [Priority 8450]
  !    ELSE            ! exclude this below.............
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  !      END
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:_Invoice, ?Progress:PctText, Progress:Thermometer, ProgressMgr, INV:ICID)
  ThisReport.AddSortOrder(INV:FKey_ICID)
  ThisReport.SetFilter('(LO:From_Date = 0 OR INV:InvoiceDate >= LO:From_Date) AND (LO:To_Date = 0 OR INV:InvoiceDate <<= LO:To_Date)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_Invoice.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      ! (p:DID, p:IID, p:From, p:To)
  
  !  db.debugout('p:IID: ' & p:IID & ',  p:Un_Printed: ' & p:Un_Printed & ',  LO:From_Date: ' & FORMAT(LO:From_Date,@d5) & |
  !          ',  LO:To_Date: ' & FORMAT(LO:To_Date,@d5) & ',  p:DID: ' & p:DID)
  
      IF p:DID ~= 0
         ThisReport.SetFilter('INV:DID = ' & p:DID,'1')          ! Only 1 of these is active
      .
      IF p:IID ~= 0
         ThisReport.SetFilter('INV:IID = ' & p:IID,'1')          ! This one overrides
      .
  
  
      IF p:Un_Printed = TRUE
         ThisReport.SetFilter('INV:Printed <> 1','2')
      .
  
  
      IF p:DID = 0 AND p:IID = 0
         IF p:From ~= 0 AND p:To ~= 0
            LO:From_Date  = p:From
            LO:To_Date    = p:To
         .
      ELSE
         ThisReport.SetFilter('')          ! This one overrides default
      .
  SELF.SkipPreview = LOC:SkipPreview
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ManifestLoadAlias.Close
    Relate:ManifestLoadDeliveriesAlias.Close
    Relate:_Invoice.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  LOC:Freight_Additional_Total = INV:AdditionalCharge + INV:FreightCharge
  ReturnValue = PARENT.TakeRecord()
       IF INV:Total < 0.0
          ! We remove the original invoice amount... here only - dont want to save it!
          INV:ShipperLine1      = ''
       .
      LOC:Commodity_Plus  = CLIP(INI:Commodity)
      IF CLIP(INI:ContainerDescription) ~= ''
         LOC:Commodity_Plus   = CLIP(LOC:Commodity_Plus) & '  [' & CLIP(INI:ContainerDescription) & ']'
      .
  PRINT(RPT:Detail)
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      IF LOC:Last_IID ~= INV:IID
         LOC:Last_IID         = INV:IID
  
         IF INV:Total < 0.0
            IF INV:Printed = TRUE AND p:Original = FALSE
               LOC:Invoice_Type   = 'Copy Credit Note'
            ELSE
               LOC:Invoice_Type   = 'Credit Note'
            .
         ELSE
            IF INV:Printed = TRUE AND p:Original = FALSE
               LOC:Invoice_Type   = 'Copy Tax Invoice'
            ELSE
               LOC:Invoice_Type   = 'Tax Invoice'
         .  .
  
         INV:Printed      = TRUE
         IF Access:_Invoice.TryUpdate() = LEVEL:Benign        ! ??? Little dangerous saving whole record here...
         .
  
  !       CLEAR(LOC:MIDs)
      .
  
  
  ! taking this out, using the INV:MIDs now, 6/2/10
  !
  !!    DEL:DID             =
  !!    DEL:PKey_DID
  !
  !    IF LOC:Last_DIID ~= DELI:DIID
  !       LOC:Last_DIID        = DELI:DIID
  !       A_MALD:DIID          = DELI:DIID
  !       IF Access:ManifestLoadDeliveriesAlias.TryFetch(A_MALD:FKey_DIID) = LEVEL:Benign
  !          A_MAL:MLID        = A_MALD:MLID
  !          IF Access:ManifestLoadAlias.TryFetch(A_MAL:PKey_MLID) = LEVEL:Benign
  !             IF CLIP(LOC:MIDs) = ''
  !                LOC:MIDs       = A_MAL:MID
  !             ELSE
  !                LOC:MIDs       = CLIP(LOC:MIDs) & A_MAL:MID
  !    .  .  .  .
  !
  !    
  ReturnValue = PARENT.ValidateRecord()
  RETURN ReturnValue

Previewer.IKB_Email PROCEDURE()

  CODE

PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Invoices','Print_Invoices','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! =============== this should be modified to only print last Statement for all clients when called without parameters
!!! </summary>
Print_Statement PROCEDURE (p:CID, p:STID)

Progress:Thermometer BYTE                                  !
L_Return_Group       GROUP,PRE(L_RG)                       !
Suburb               STRING(50)                            !Suburb
PostalCode           STRING(10)                            !
Country              STRING(50)                            !Country
Province             STRING(35)                            !Province
City                 STRING(35)                            !City
Found                BYTE                                  !
                     END                                   !
LOC:Statement_Group  GROUP,PRE(L_SG)                       !
STID                 ULONG                                 !Statement ID
ReportPageNumber     ULONG                                 !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
Next                 BYTE                                  !
                     END                                   !
LOC:Locals           GROUP,PRE(L_L)                        !
Date                 LONG                                  !
Time                 LONG                                  !
                     END                                   !
Process:View         VIEW(_Statements)
                       PROJECT(STA:CID)
                       PROJECT(STA:Current)
                       PROJECT(STA:Days30)
                       PROJECT(STA:Days60)
                       PROJECT(STA:Days90)
                       PROJECT(STA:STID)
                       PROJECT(STA:StatementDate)
                       PROJECT(STA:Total)
                       JOIN(CLI:PKey_CID,STA:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:VATNo)
                         PROJECT(CLI:AID)
                         JOIN(ADD:PKey_AID,CLI:AID)
                           PROJECT(ADD:AddressName)
                           PROJECT(ADD:Line1)
                           PROJECT(ADD:Line2)
                         END
                       END
                       JOIN(STAI:FKey_STID,STA:STID)
                         PROJECT(STAI:Amount)
                         PROJECT(STAI:Credit)
                         PROJECT(STAI:DINo)
                         PROJECT(STAI:Debit)
                         PROJECT(STAI:IID)
                         PROJECT(STAI:InvoiceDate)
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(1000,3333,7052,5760),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif',8,,FONT:regular, |
  CHARSET:ANSI),THOUS
                       HEADER,AT(1000,1000,7052,2354),USE(?Header)
                         STRING('Statement'),AT(479,31,6094),USE(?String7),FONT(,16,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         BOX,AT(4865,104,2021,583),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         STRING(@n_10),AT(5260,302,1208,240),USE(CLI:ClientNo),FONT(,12,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         IMAGE('fbn_logo_small.jpg'),AT(63,323,3552,677),USE(?Image1)
                         STRING(@s35),AT(4510,1063),USE(CLI:ClientName),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@s35),AT(156,1063),USE(ADD:AddressName),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                         STRING(@s35),AT(156,1250),USE(ADD:Line1)
                         STRING('Statement No. & Date:'),AT(4458,1812,1104),USE(?String42),TRN
                         STRING(@s35),AT(156,1417),USE(ADD:Line2)
                         STRING(@n_10),AT(5646,1812,510,167),USE(STA:STID,,?STA:STID:2),FONT(,,COLOR:Black,,CHARSET:ANSI), |
  RIGHT(1),TRN
                         STRING(@s50),AT(167,1604),USE(L_RG:Suburb)
                         STRING(@s10),AT(167,1813),USE(L_RG:PostalCode)
                         STRING(@s20),AT(1583,1813),USE(CLI:VATNo)
                         STRING('VAT No.:'),AT(990,1813),USE(?StringVat),TRN
                         LINE,AT(73,2083,6906,0),USE(?Line11),COLOR(COLOR:Black)
                         LINE,AT(73,2323,6906,0),USE(?Line11:2),COLOR(COLOR:Black)
                         LINE,AT(958,2083,0,250),USE(?Line1:2),COLOR(COLOR:Black)
                         LINE,AT(2052,2083,0,250),USE(?Line1:3),COLOR(COLOR:Black)
                         LINE,AT(3198,2083,0,250),USE(?Line1:4),COLOR(COLOR:Black)
                         LINE,AT(4469,2083,0,250),USE(?Line1:5),COLOR(COLOR:Black)
                         LINE,AT(5740,2083,0,250),USE(?Line1:6),COLOR(COLOR:Black)
                         STRING('DI No.'),AT(2229,2115,885,208),USE(?String16:3),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         STRING('Debit'),AT(3365,2115,1031,208),USE(?String16:4),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         STRING('Amount'),AT(5875,2115,1031),USE(?String16:6),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         STRING('Credit'),AT(4646,2115,1031,208),USE(?String16:5),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         STRING('Date'),AT(115,2115,729,208),USE(?String16),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         STRING('Invoice No.'),AT(1094,2115,885,208),USE(?String16:2),FONT(,,COLOR:Black,FONT:bold, |
  CHARSET:ANSI),CENTER,TRN
                         STRING(@d17),AT(6240,1812,750),USE(STA:StatementDate)
                       END
break_client           BREAK(STA:CID),USE(?BREAK1)
Detail                   DETAIL,AT(0,0,,188),USE(?Detail),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                           LINE,AT(958,0,0,188),USE(?Line1),COLOR(COLOR:Black)
                           LINE,AT(2052,0,0,188),USE(?Line1:7),COLOR(COLOR:Black)
                           LINE,AT(3198,0,0,188),USE(?Line1:8),COLOR(COLOR:Black)
                           LINE,AT(4469,0,0,188),USE(?Line1:9),COLOR(COLOR:Black)
                           LINE,AT(5740,0,0,188),USE(?Line1:10),COLOR(COLOR:Black)
                           STRING(@d17),AT(115,10),USE(STAI:InvoiceDate),RIGHT(1)
                           STRING(@n-14.2),AT(5875,10,1031,167),USE(STAI:Amount),RIGHT(1)
                           STRING(@n_10),AT(1094,10,885,167),USE(STAI:IID),RIGHT(1)
                           STRING(@n_10),AT(2229,10,885,167),USE(STAI:DINo),RIGHT(1)
                           STRING(@n-14.2),AT(4646,10,1031,167),USE(STAI:Credit),RIGHT(1)
                           STRING(@n-14.2),AT(3365,10,1031,167),USE(STAI:Debit),RIGHT(1)
                         END
                         FOOTER,AT(0,0),USE(?GROUPFOOTER1)
                           LINE,AT(73,73,6906,0),USE(?Line11:3),COLOR(COLOR:Black)
                           STRING('Current'),AT(2875,125,938,208),USE(?String35:4),CENTER,TRN
                           STRING(@n-14.2),AT(31,375,938,208),USE(STA:Days90),RIGHT(1)
                           STRING(@n-14.2),AT(979,375),USE(STA:Days60),RIGHT(1)
                           STRING(@n-14.2),AT(1927,375),USE(STA:Days30),RIGHT(1)
                           STRING(@n-14.2),AT(2875,375),USE(STA:Current),RIGHT(1)
                           STRING(@n-14.2),AT(3854,375),USE(STA:Total),RIGHT(1)
                           STRING(@n-14.2),AT(5875,375),USE(STA:Total,,?STA:Total:2),RIGHT(1),TRN
                           STRING('90 Days'),AT(31,125,938,208),USE(?String35),CENTER,TRN
                           STRING('60 Days'),AT(979,125,938,208),USE(?String35:2),CENTER,TRN
                           STRING('Total'),AT(3854,125,938,208),USE(?String35:5),CENTER,TRN
                           STRING('30 Days'),AT(1927,125,938,208),USE(?String35:3),CENTER,TRN
                           STRING('Total Due:'),AT(5188,375),USE(?String35:6),TRN
                         END
                       END
                       FOOTER,AT(1000,9104,7052,1313),USE(?Footer),FONT('MS Sans Serif',10,COLOR:Black,FONT:regular, |
  CHARSET:ANSI)
                         STRING('Report Date:'),AT(135,740),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(1135,740),USE(?ReportDateStamp),TRN
                         STRING('Report Time:'),AT(2604,740),USE(?ReportTimePrompt),TRN
                         STRING('<<-- Time Stamp -->'),AT(3604,740),USE(?ReportTimeStamp),TRN
                         STRING('Page:'),AT(6156,740),USE(?String6),TRN
                         STRING(@N3),AT(6615,740),USE(ReportPageNumber)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Statement')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:_Statements.Open                                  ! File _Statements used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Statement',ProgressWindow)           ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:_Statements, ?Progress:PctText, Progress:Thermometer, ProgressMgr, STA:CID)
  ThisReport.AddSortOrder(STA:FKey_CID)
  ThisReport.AppendOrder('+STAI:STIID,+STAI:InvoiceDate')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_Statements.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      IF p:CID ~= 0
         ThisReport.AddRange(STA:CID,p:CID)
      .
  
      IF p:STID ~= 0
         ThisReport.SetFilter('STA:STID = ' & p:STID)
      .
  
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Statements.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Statement',ProgressWindow)        ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      L_Return_Group  = Get_Suburb_Details(ADD:SUID)
  
  
  !    IF L_SG:STID ~= STA:STID
  !       L_SG:STID    = STA:STID
  !       L_SG:Next    = TRUE
  !
  !       L_SG:Days90  = STA:Days90
  !       L_SG:Days60  = STA:Days60
  !       L_SG:Days30  = STA:Days30
  !       L_SG:Current = STA:Current
  !       L_SG:Total   = STA:Total
  !    .
  !
  !
  !    IF L_SG:Next = TRUE
  !       L_SG:Next    = FALSE
  !    .
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Statement','Print_Statement','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

