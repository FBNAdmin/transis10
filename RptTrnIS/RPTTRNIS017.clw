

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS017.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_WebUsers PROCEDURE 

Progress:Thermometer BYTE                                  !
Process:View         VIEW(WebClients)
                       PROJECT(WCLI:AccessLevel)
                       PROJECT(WCLI:ClientLogin)
                       PROJECT(WCLI:ClientPassword)
                       PROJECT(WCLI:LoginName)
                       PROJECT(WCLI:WebClientID)
                       PROJECT(WCLI:CID)
                       JOIN(CLI:PKey_CID,WCLI:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:ClientNo)
                       END
                     END
ProgressWindow       WINDOW('Report Web Clients'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('WebClients Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Web Clients'),AT(0,20,7750,220),USE(?ReportTitle),FONT(,12,,FONT:bold,CHARSET:ANSI), |
  CENTER
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(1854,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2625,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(3938,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(5229,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(6542,354,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(6979,365,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         STRING('Rec. ID'),AT(7021,385,688,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Client'),AT(52,385,1771,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Client No.'),AT(1885,385,688,167),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Client Login'),AT(2708,385,1191,170),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('Client Password'),AT(4000,385,1191,170),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Login Name'),AT(5292,385,1191,170),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('Level'),AT(6625,385,,170),USE(?HeaderTitle:6),TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(1854,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(2625,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(3948,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(5229,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(6542,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         STRING(@s100),AT(52,52,1771,156),USE(CLI:ClientName)
                         STRING(@n_10),AT(7021,52,,170),USE(WCLI:WebClientID),RIGHT
                         LINE,AT(6979,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                         STRING(@n_10b),AT(1885,52,,170),USE(CLI:ClientNo),RIGHT
                         STRING(@s20),AT(2708,52,1191,170),USE(WCLI:ClientLogin),LEFT
                         STRING(@s20),AT(4000,52,1191,170),USE(WCLI:ClientPassword),LEFT
                         STRING(@s50),AT(5292,52,1191,170),USE(WCLI:LoginName),LEFT
                         STRING(@n3),AT(6646,52,,170),USE(WCLI:AccessLevel),RIGHT
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_WebUsers')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:WebClients.Open                                   ! File WebClients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_WebUsers',ProgressWindow)            ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:WebClients, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder('+CLI:ClientName,+WCLI:ClientLogin,+WCLI:WebClientID')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:WebClients.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:WebClients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_WebUsers',ProgressWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_WebUsers','Print_WebUsers','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Window
!!! Invoices / Credit Notes / Delivery Notes / Statements
!!! </summary>
Print_Cont_old PROCEDURE (p:ID, p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow)

LOC:Fields_Q         QUEUE,PRE(L_FQ)                       !
FieldName            STRING(50)                            !Field Name
XPos                 SHORT                                 !X Position
YPos                 SHORT                                 !Y Position
Length               SHORT                                 !
Alignment            BYTE                                  !Alignment
PLID                 ULONG                                 !
PFID                 ULONG                                 !
                     END                                   !
LOC:Invoice_Q        QUEUE,PRE(L_IQ)                       !
Line                 ULONG                                 !
Text                 STRING(500)                           !
                     END                                   !
LOC:Locals           GROUP,PRE(LOC)                        !
Dot_Empty_Lines      USHORT                                !Print dot on empty lines from this line (0 is off)
Print_Error_Msg      STRING(200)                           !
Page_No              LONG(1)                               !
Print_without_Preview BYTE                                 !
Result               LONG                                  !
Invoice_Type         STRING(20)                            !
Field                STRING(250)                           !
Cancel               BYTE                                  !
Items_Started        BYTE                                  !
Items_XPos           SHORT                                 !
Items_Length         SHORT                                 !
Items_Alignment      BYTE                                  !
Items_Ended          BYTE                                  !
Items_Complete       BYTE                                  !
Idx                  ULONG                                 !
Port                 STRING(255)                           !LPT1, LPT2, etc.
IID                  ULONG                                 !Invoice Number
Complete             LONG                                  !
LineResult           LONG                                  !
RetryTimes           LONG                                  !
RetryDelay           LONG                                  !
                     END                                   !
LOC:Statement_Group  GROUP,PRE(L_STG)                      !Group of statement specific vars
Date_XPos            ULONG                                 !
Date_Length          ULONG                                 !
Date_Align           BYTE                                  !
InvNo_XPos           ULONG                                 !
InvNo_Length         ULONG                                 !
InvNo_Align          BYTE                                  !
DI_XPos              ULONG                                 !
DI_Length            ULONG                                 !
DI_Align             BYTE                                  !
Debit_XPos           ULONG                                 !
Debit_Length         ULONG                                 !
Debit_Align          BYTE                                  !
Credit_XPos          ULONG                                 !
Credit_Length        ULONG                                 !
Credit_Align         BYTE                                  !
InvNo2_XPos          ULONG                                 !
InvNo2_Length        ULONG                                 !
InvNo2_Align         BYTE                                  !
Amount_XPos          ULONG                                 !
Amount_Length        ULONG                                 !
Amount_Align         BYTE                                  !
                     END                                   !
L_Return_Group       GROUP,PRE(L_RG)                       !
Suburb               STRING(50)                            !Suburb
PostalCode           STRING(10)                            !
Country              STRING(50)                            !Country
Province             STRING(35)                            !Province
City                 STRING(35)                            !City
Found                BYTE                                  !
                     END                                   !
QuickWindow          WINDOW('Print Continuous'),AT(,,420,238),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MAX,MDI,HLP('Print_Invoices_Cont'),SYSTEM,TIMER(100)
                       SHEET,AT(3,3,414,214),USE(?Sheet1),FONT('Tahoma')
                         TAB('Print '),USE(?Tab1)
                           LIST,AT(7,19,406,194),USE(?List1),FONT('Courier New',8,,FONT:regular,CHARSET:ANSI),VSCROLL, |
  FORMAT('14R(2)|M~Line~L@n13@1020L(2)|M~Text~@s255@'),FROM(LOC:Invoice_Q)
                         END
                         TAB('Fields'),USE(?Tab2)
                           LIST,AT(6,20,405,182),USE(?List2),VSCROLL,FORMAT('200L(2)|M~Field Name~@s50@20R(2)|M~X ' & |
  'Pos~L@n_5@20R(2)|M~Y Pos~L@n_5@20R(2)|M~Length~L@n_5@12R(2)|M~Alignment~L@n3@'),FROM(LOC:Fields_Q)
                           PROMPT(''),AT(7,205,287,10),USE(?Prompt1),LEFT
                         END
                       END
                       BUTTON('Print 3'),AT(130,222,45,14),USE(?Button_Print_Line:2),HIDE
                       BUTTON('Print 2'),AT(175,222,45,14),USE(?Button_Print_Line),HIDE
                       BUTTON('&Re-Print'),AT(242,222,59,14),USE(?Button_RePrint),LEFT,ICON(ICON:Print),FLAT,HIDE, |
  TIP('Print with option to re-print')
                       CHECK(' &Print subsequent without Preview'),AT(3,226),USE(LOC:Print_without_Preview),HIDE, |
  TIP('Print all subsequent prints without preview')
                       BUTTON('&Print'),AT(308,222,,14),USE(?Button_Print),LEFT,ICON(ICON:Print1),FLAT,TIP('Print and close')
                       BUTTON('&Cancel'),AT(368,222,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

    ! How it all works................
    !   Yeah right.. like I know.  Ref: Celine Dion (of all people), Because you l.......

PL_View         VIEW(PrintLayout)
!    PROJECT(PRIL:PLID, PRIL:PID, PRIL:PFID, PRIL:XPos, PRIL:YPos)
       JOIN(PRIF:PKey_PFID, PRIL:PFID)
       PROJECT(PRIF:FieldName)
    .  .



!MAN_View        VIEW(ManifestLoadDeliveries)
!    PROJECT(MALD:MLID, MALD:DIID)
!       JOIN(MAL:PKey_MLID, MALD:MLID)
!       PROJECT(MAL:MID)
!    .  .
View_Statement      VIEW(_StatementItems)
    .


Statement_View      ViewManager
    MAP
Print_Lines             PROCEDURE
    .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:Result)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_Print                   ROUTINE
    LOC:Cancel    = TRUE

    CLEAR(PRI:Record)
    SET(PRI:PKey_PID)
    LOOP
       NEXT(Prints)
       IF ERRORCODE()
          BREAK
       .

       IF p:PrintType = PRI:PrintType
          LOC:Cancel    = FALSE
          BREAK
    .  .
    EXIT
Load_Field_Q              ROUTINE
    FREE(LOC:Fields_Q)

    PUSHBIND
    BIND('PRIL:PID', PRIL:PID)

    OPEN(PL_View)
    PL_View{PROP:Filter}    = 'PRIL:PID = ' & PRI:PID
    SET(PL_View)

    LOOP
       NEXT(PL_View)
       IF ERRORCODE()
          BREAK
       .

       L_FQ:FieldName   = PRIF:FieldName
       L_FQ:XPos        = PRIL:XPos
       L_FQ:YPos        = PRIL:YPos
       L_FQ:Length      = PRIL:Length
       L_FQ:Alignment   = PRIL:Alignment

       L_FQ:PLID        = PRIL:PLID
       L_FQ:PFID        = PRIL:PFID
       ADD(LOC:Fields_Q)
    .
    CLOSE(PL_View)
    POPBIND

    SORT(LOC:Fields_Q, +L_FQ:FieldName)
    ?Prompt1{PROP:Text} = 'Fileds: ' & RECORDS(LOC:Fields_Q)
    EXIT
Check_Fields               ROUTINE
    ! Scan all Field Q items for items that must print on this line.
    ! Load L_IQ:Text with them.

    LOC:Idx     = 0
    LOOP
       LOC:Idx += 1
       GET(LOC:Fields_Q, LOC:Idx)
       IF ERRORCODE()
          BREAK
       .
       IF L_FQ:YPos ~= L_IQ:Line
          CYCLE
       .

       ! Print this on this line then - we dont check for overlap
       CLEAR(LOC:Field)

       EXECUTE PRI:PrintType             ! Invoices, Credit Notes, Delivery Notes, Statements
          DO Add_Credit
          DO Add_Delivery_Fields
          DO Add_Statement_Fields
       ELSE
          DO Add_Invoice_Fields
       .

       ! Exclude empty fields and 0 line fields
       IF CLIP(LOC:Field) ~= '' AND L_IQ:Line > 0
   db.debugout('[Print_Cont]  Fields: ' & CLIP(L_FQ:FieldName))
          IF L_FQ:XPos <= 0 OR (L_FQ:XPos + L_FQ:Length - 1) < L_FQ:XPos OR (L_FQ:XPos + L_FQ:Length - 1) > SIZE(L_IQ:Text)
             MESSAGE('There is a problem with the field: ' & CLIP(L_FQ:FieldName) & '||XPos: ' & L_FQ:XPos & '|YPos: ' & L_FQ:YPos & '|Length: ' & L_FQ:Length, 'Print_Cont', ICON:Hand)
             IF L_FQ:XPos <= 0
                L_FQ:XPos    = 1
             .
             IF (L_FQ:XPos + L_FQ:Length - 1) < L_FQ:XPos
                L_FQ:Length  = 30
          .  .

          CASE L_FQ:Alignment
          OF 0
             L_IQ:Text[L_FQ:XPos : L_FQ:XPos + L_FQ:Length - 1]  = LEFT(LOC:Field, L_FQ:Length)
          OF 1
             L_IQ:Text[L_FQ:XPos : L_FQ:XPos + L_FQ:Length - 1]  = CENTER(LOC:Field, L_FQ:Length)
          OF 2
             L_IQ:Text[L_FQ:XPos : L_FQ:XPos + L_FQ:Length - 1]  = RIGHT(LOC:Field, L_FQ:Length)
    .  .  .

    EXIT
Populate                     ROUTINE
    ! Populate
    SORT(LOC:Fields_Q, L_FQ:YPos, L_FQ:XPos)

    FREE(LOC:Invoice_Q)
    L_IQ:Line       = 0

    ! Load any zero fields - Note the Items for Statements are loaded here.
    DO Check_Fields

    LOOP            !PRI:PageLines TIMES
       L_IQ:Line           += 1
       IF PRI:PageLines < L_IQ:Line             ! We have page break condition
          IF LOC:Items_Complete = TRUE
             BREAK
          .
          LOC:Page_No      += 1

          L_IQ:Line         = 1                 ! Start at line 1 again
          LOC:Items_Started = FALSE
       .

       CLEAR(L_IQ:Text)
       CLEAR(LOC:Field)

       ! For every line, check through the Fields on this line in X Pos order an populate...
       DO Check_Fields

       IF LOC:Items_Started = TRUE
          ! Every line print for Items
          IF LOC:Items_Ended = TRUE             ! On last Item line
             LOC:Items_Started = FALSE
          .

    db.debugout('[Print_Cont - Populate]  Type: ' & PRI:PrintType & ',   Items')

          IF PRI:PrintType = 3
             DO Add_Statement_Items
          ELSE
             DO Add_Items
          .

          IF CLIP(LOC:Field) ~= ''
             IF LOC:Items_XPos <= 0 OR (LOC:Items_XPos + LOC:Items_Length - 1) < L_FQ:XPos
                MESSAGE('There is a problem with the Items: ' & CLIP(L_FQ:FieldName) & '||XPos: ' & LOC:Items_XPos & '|YPos: ' & L_FQ:YPos & '|Length: ' & LOC:Items_Length, 'Print_Cont', ICON:Hand)
                IF L_FQ:XPos <= 0
                   L_FQ:XPos    = 1
                .
                IF (L_FQ:XPos + L_FQ:Length - 1) < L_FQ:XPos
                   L_FQ:Length  = 30
             .  .
             CASE LOC:Items_Alignment
             OF 0
                L_IQ:Text[LOC:Items_XPos : LOC:Items_XPos + LOC:Items_Length - 1]  = LEFT(LOC:Field, LOC:Items_Length)
             OF 1
                L_IQ:Text[LOC:Items_XPos : LOC:Items_XPos + LOC:Items_Length - 1]  = CENTER(LOC:Field, LOC:Items_Length)
             OF 2
                L_IQ:Text[LOC:Items_XPos : LOC:Items_XPos + LOC:Items_Length - 1]  = RIGHT(LOC:Field, LOC:Items_Length)
       .  .  .

       IF CLIP(L_IQ:Text) ~= ''
          L_IQ:Text  = SUB(L_IQ:Text, 1, PRI:PageColumns)            ! Chop off anything beyond page length
       .
       ADD(LOC:Invoice_Q)
    .

    EXIT
! --------------------------------------------------------------------------------------------
Load_Invoice              ROUTINE
    CLEAR(LOC:IID)

    ! Load the Invoice
    IF p:ID_Options = 1         ! We have been passed the DID - we need to get the invoice for it.
       INV:DID          = p:ID
       IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
          LOC:IID       = INV:IID
       ELSE
          ! We cant find the invoice
          MESSAGE('The Invoice cannot be found using the Delivery ID given - DID: ' & p:ID, 'Print_Cont', ICON:Hand)
          LOC:Cancel    = TRUE
          POST(EVENT:CloseWindow)
          p:ID          = 0
       .
    ELSE
       LOC:IID          = p:ID
    .

    IF LOC:Cancel = FALSE
       INV:IID          = LOC:IID
       IF Access:_Invoice.TryFetch(INV:PKey_IID) ~= LEVEL:Benign
          IF p:ID_Options = 1         ! We have been passed the DID
             MESSAGE('The Invoice cannot be found - IID: ' & LOC:IID & '||DID is: ' & p:ID, 'Print_Cont', ICON:Hand)
          ELSE
             MESSAGE('The Invoice cannot be found - IID: ' & LOC:IID, 'Print_Cont', ICON:Hand)
          .
          LOC:Cancel    = TRUE
          POST(EVENT:CloseWindow)
       ELSE
          CLEAR(INI:Record,-1)
          INI:IID       = LOC:IID
          SET(INI:FKey_IID, INI:FKey_IID)


          ! Set Invoice / Copy Invoice
          IF INV:Printed = TRUE
             LOC:Invoice_Type = 'Copy Tax Invoice'
          ELSE
             !LOC:Invoice_Type = 'Tax Invoice'             ! We only print this string if Copy

             INV:Printed      = TRUE
             IF Access:_Invoice.TryUpdate() = LEVEL:Benign
    .  .  .  .
    EXIT

Add_Invoice_Fields    ROUTINE
    CASE CLIP(L_FQ:FieldName)
    OF 'Invoice No.'
       ! Add the Invoice Number
       LOC:Field            = INV:IID
    OF 'Branch'
       LOC:Field            = INV:BranchName                
    OF 'Date'
       LOC:Field            = FORMAT(INV:InvoiceDate,@d5)
    OF 'For Account'
       LOC:Field            = INV:ClientNo & ' ' & INV:ClientName
    OF 'DI No.'
       LOC:Field            = INV:DINo
    OF 'Manifest No.'         
       LOC:Field            = INV:MIDs
    OF 'Shipper'
       LOC:Field            = INV:ShipperName
    OF 'Ship Line 1'
       LOC:Field            = INV:ShipperLine1
    OF 'Ship Line 2'
       IF CLIP(INV:ShipperLine2) = ''
          LOC:Field         = INV:ShipperSuburb
       ELSE
          LOC:Field         = INV:ShipperLine2
       .
    OF 'Ship Suburb'
       IF CLIP(INV:ShipperLine2) = ''
          LOC:Field         = INV:ShipperPostalCode
       ELSE
          IF CLIP(INV:ShipperSuburb) = ''
             LOC:Field      = INV:ShipperPostalCode
          ELSE
             LOC:Field      = INV:ShipperSuburb
       .  .
    OF 'Ship Post'
       IF CLIP(INV:ShipperLine2) = '' OR CLIP(INV:ShipperSuburb) = ''
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ShipperPostalCode
       .
    OF 'Consignee'
       LOC:Field            = INV:ConsigneeName
    OF 'Con Line 1'
       LOC:Field            = INV:ConsigneeLine1
    OF 'Con Line 2'
       IF CLIP(INV:ConsigneeLine2) = ''
          LOC:Field         = INV:ConsigneeSuburb
       ELSE
          LOC:Field         = INV:ConsigneeLine2
       .
    OF 'Con Suburb'
       IF CLIP(INV:ConsigneeLine2) = ''
          LOC:Field         = INV:ConsigneePostalCode
       ELSE
          IF CLIP(INV:ConsigneeSuburb) = ''
             LOC:Field      = INV:ConsigneePostalCode
          ELSE
             LOC:Field      = INV:ConsigneeSuburb
       .  .
    OF 'Con Post'
       IF CLIP(INV:ConsigneeLine2) = '' OR CLIP(INV:ConsigneeSuburb) = ''
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ConsigneePostalCode
       .
    OF 'Items Start'
       LOC:Items_Started    = TRUE
       LOC:Items_Ended      = FALSE

       LOC:Items_XPos       = L_FQ:XPos
       LOC:Items_Length     = L_FQ:Length
       LOC:Items_Alignment  = L_FQ:Alignment
    OF 'Items End'
       LOC:Items_Ended      = TRUE

    OF 'Vol Weight'
       LOC:Field            = 'Vol Wt : ' & FORMAT(INV:VolumetricWeight, @n9.2)
    OF 'Act Weight'
       LOC:Field            = 'Act Wt : ' & FORMAT(INV:Weight, @n9.2)

    OF 'Debt Name'
       LOC:Field            = INV:ClientName            ! Repeated
    OF 'Debt Line 1'
       LOC:Field            = INV:ClientLine1
    OF 'Debt Line 2'
       IF CLIP(INV:ClientLine2) = ''
          LOC:Field         = INV:ClientSuburb
       ELSE
          LOC:Field         = INV:ClientLine2
       .
    OF 'Debt Suburb'
       IF CLIP(INV:ClientLine2) = ''                    ! Suburb already done
          LOC:Field         = INV:ClientPostalCode      ! Do postcode here
       ELSE
          IF CLIP(INV:ClientSuburb) = ''
             LOC:Field      = INV:ClientPostalCode
          ELSE
             LOC:Field      = INV:ClientSuburb
       .  .
    OF 'Debt Post'
       IF CLIP(INV:ClientSuburb) = '' OR CLIP(INV:ClientLine2) = ''     ! Postcode already done
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ClientPostalCode
       .
    OF 'Debt Vat No.'
       LOC:Field            = 'VAT No. ' & INV:VATNo
    OF 'Debt Ref.'
       LOC:Field            = 'Ref. ' & INV:ClientReference
    OF 'Charge Fuel'
       LOC:Field            = FORMAT(INV:FuelSurcharge, @n-12.2)
    OF 'Charge Insurance'
       LOC:Field            = FORMAT(INV:Insurance, @n-12.2)
    OF 'Charge Docs'
       LOC:Field            = FORMAT(INV:Documentation, @n-12.2)
    OF 'Charge Freight'
       LOC:Field            = FORMAT(INV:FreightCharge, @n-12.2)
    OF 'Charge VAT'
       LOC:Field            = FORMAT(INV:VAT, @n-12.2)
    OF 'Charge Total'
       LOC:Field            = FORMAT(INV:Total, @n-12.2)
    OF 'Copy Invoice'
       LOC:Field            = LOC:Invoice_Type          ! Non invoice field
    OF 'Fuel Surcharge'
       LOC:Field            = 'Fuel Surcharge'
    OF 'Invoice Message'
       LOC:Field            = INV:InvoiceMessage
    .

    EXIT
! --------------------------------------------------------------------------------------------
Add_Items                   ROUTINE
    NEXT(_InvoiceItems)
    IF ERRORCODE() OR INI:IID ~= INV:IID
       LOC:Items_Complete   = TRUE
    ELSE
       LOC:Field            = INI:Units & ' ' & CLIP(INI:Description) & ' of ' & CLIP(INI:Commodity)

        !                L_IQ:Text[LOC:Items_XPos : LOC:Items_XPos + LOC:Items_Length - 1]  = LEFT(LOC:Field, LOC:Items_Length)

       IF CLIP(INI:ContainerDescription) ~= ''
          Len_CD_#          = LEN(CLIP(INI:ContainerDescription))
          Len_F_#           = LEN(CLIP(LOC:Field))

          IF LOC:Items_Length / 2 < Len_CD_#  AND  Len_F_# + Len_CD_# + 2 < LOC:Items_Length
             ! Then right align
             LOC:Field[LOC:Items_Length - Len_CD_# : LOC:Items_Length] = INI:ContainerDescription
          ELSE
             IF Len_F_# > LOC:Items_Length / 2 OR Len_CD_# > LOC:Items_Length / 2        ! Then just add after
                LOC:Field      = CLIP(LOC:Field) & '  ' & INI:ContainerDescription
             ELSE
                LOC:Field[LOC:Items_Length / 2 : LOC:Items_Length] = INI:ContainerDescription
    .  .  .  .
    EXIT
! --------------------------------------------------------------------------------------------
Load_Delivery             ROUTINE
    ! Load the Delivey Note
    DEL:DID         = p:ID
    IF Access:Deliveries.TryFetch(DEL:PKey_DID) ~= LEVEL:Benign
       MESSAGE('The DI cannot be found - ID: ' & p:ID, 'Print_Cont', ICON:Hand)
       LOC:Cancel   = TRUE
       POST(EVENT:CloseWindow)
    ELSE
!       CLEAR(DELI:Record,-1)
!       DELI:DID     = p:ID
!       SET(DELI:FKey_DID_ItemNo, DELI:FKey_DID_ItemNo)
       ! Note: We are going to use the Invoice and Invoice Items for Delivery Note printing

       ! We need the IID - so we set the items file after getting that only...
    .

    IF LOC:Cancel = FALSE
       ! Load the associated invoice
       INV:DID      = DEL:DID
       IF Access:_Invoice.TryFetch(INV:FKey_DID) ~= LEVEL:Benign
          MESSAGE('The DI Invoice cannot be found - DID: ' & p:ID & '||Delivery Note will not be printed.', 'Print_Cont', ICON:Hand)
          LOC:Cancel = TRUE
          CLEAR(INV:Record)
       ELSE
          CLEAR(INI:Record,-1)
          INI:IID    = INV:IID
          SET(INI:FKey_IID, INI:FKey_IID)
    .  .
    EXIT

Add_Delivery_Fields    ROUTINE
    CASE CLIP(L_FQ:FieldName)
    OF 'Invoice No.'
       ! Add the Invoice Number
       IF INV:POD_IID = 0                       ! Old Invoices will be missing PODs - Feb 21 05
          LOC:Field         = INV:IID
       ELSE
          LOC:Field         = INV:POD_IID
       .
    OF 'Branch'
       LOC:Field            = INV:BranchName                
    OF 'Date'
       LOC:Field            = FORMAT(INV:InvoiceDate,@d5)
    OF 'For Account'
       LOC:Field            = INV:ClientNo              ! INV:ClientName
    OF 'DI No.'
       LOC:Field            = INV:DINo
    OF 'Manifest No.'         
       LOC:Field            = INV:MIDs
    OF 'Shipper'
       LOC:Field            = INV:ShipperName
    OF 'Ship Line 1'
       LOC:Field            = INV:ShipperLine1
    OF 'Ship Line 2'
       IF CLIP(INV:ShipperLine2) = ''
          LOC:Field         = INV:ShipperSuburb
       ELSE
          LOC:Field         = INV:ShipperLine2
       .
    OF 'Ship Suburb'
       IF CLIP(INV:ShipperLine2) = ''
          LOC:Field         = INV:ShipperPostalCode
       ELSE
          IF CLIP(INV:ShipperSuburb) = ''
             LOC:Field      = INV:ShipperPostalCode
          ELSE
             LOC:Field      = INV:ShipperSuburb
       .  .
    OF 'Ship Post'
       IF CLIP(INV:ShipperLine2) = '' OR CLIP(INV:ShipperSuburb) = ''
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ShipperPostalCode
       .
    OF 'Consignee'
       LOC:Field            = INV:ConsigneeName
    OF 'Con Line 1'
       LOC:Field            = INV:ConsigneeLine1
    OF 'Con Line 2'
       IF CLIP(INV:ConsigneeLine2) = ''
          LOC:Field         = INV:ConsigneeSuburb
       ELSE
          LOC:Field         = INV:ConsigneeLine2
       .
    OF 'Con Suburb'
       IF CLIP(INV:ConsigneeLine2) = ''
          LOC:Field         = INV:ConsigneePostalCode
       ELSE
          IF CLIP(INV:ConsigneeSuburb) = ''
             LOC:Field      = INV:ConsigneePostalCode
          ELSE
             LOC:Field      = INV:ConsigneeSuburb
       .  .
    OF 'Con Post'
       IF CLIP(INV:ConsigneeLine2) = '' OR CLIP(INV:ConsigneeSuburb) = ''
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ConsigneePostalCode
       .

    OF 'Items Start'
       LOC:Items_Started    = TRUE
       LOC:Items_Ended      = FALSE

       LOC:Items_XPos       = L_FQ:XPos
       LOC:Items_Length     = L_FQ:Length
       LOC:Items_Alignment  = L_FQ:Alignment
    OF 'Items End'
       LOC:Items_Ended      = TRUE
    OF 'Vol Weight'
       IF INV:VolumetricWeight ~= 0.0
          LOC:Field         = 'Vol Wt : ' & FORMAT(INV:VolumetricWeight, @n9.2)
       .
    OF 'Act Weight'
       LOC:Field            = 'Act Wt : ' & FORMAT(INV:Weight, @n9.2)
    OF 'Spec. Inst. 1'
       LOC:Field            = SUB(DEL:SpecialDeliveryInstructions, 1, L_FQ:Length)
    OF 'Spec. Inst. 2'
       LOC:Field            = SUB(DEL:SpecialDeliveryInstructions, L_FQ:Length + 1, L_FQ:Length)
    OF 'Spec. Inst. 3'
       LOC:Field            = SUB(DEL:SpecialDeliveryInstructions, 2 * L_FQ:Length + 1, L_FQ:Length)
    OF 'Debt Ref.'
       LOC:Field            = 'Ref. ' & INV:ClientReference
    .

    EXIT
! --------------------------------------------------------------------------------------------
Load_Statements             ROUTINE
    ! Load the Statement
    STA:STID        = p:ID
    IF Access:_Statements.TryFetch(STA:PKey_STID) ~= LEVEL:Benign
       MESSAGE('The Statement cannot be found - STID: ' & p:ID, 'Print_Cont', ICON:Hand)
       LOC:Cancel   = TRUE
       POST(EVENT:CloseWindow)
    ELSE
       ! Load the Client Record
       CLI:CID  = STA:CID
       IF Access:Clients.TryFetch(CLI:PKey_CID) ~= LEVEL:Benign
          MESSAGE('The Statement client cannot be found - CID: ' & STA:CID & '||Statement will not be printed.', 'Print_Cont', ICON:Hand)
          LOC:Cancel   = TRUE
          POST(EVENT:CloseWindow)
          CLEAR(CLI:Record)
       ELSE
          ! Load Clients address
          ADD:AID       = CLI:AID
          IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
             ! Load Suburb details
             L_Return_Group = Get_Suburb_Details(ADD:SUID)
       .  .

       Statement_View.Init(View_Statement, Relate:_StatementItems)
       Statement_View.AddSortOrder( STAI:FKey_STID )
       Statement_View.AppendOrder( 'STAI:STIID' )
       Statement_View.AddRange( STAI:STID, p:ID )
       !Statement_View.SetFilter( '' )

       Statement_View.Reset(1)

!       CLEAR(STAI:Record,-1)
!       STAI:STID    = p:ID
!       SET(STAI:FKey_STID, STAI:FKey_STID)
    .
    EXIT

Add_Statement_Fields     ROUTINE
    CASE CLIP(L_FQ:FieldName)
    OF 'Statement No.'
       LOC:Field            = STA:STID
    OF 'FBN Name'               ! F.B.N. Transport'
       LOC:Field            = 'F.B.N. Transport'
    OF 'FBN Line 1'             ! P.O.Box 1405'
       LOC:Field            = 'P.O.Box 1405'
    OF 'FBN Line 2'             ! Suburb
       LOC:Field            = 'Hillcrest'
    OF 'FBN Line 3'             ! Postal Code
       LOC:Field            = '3650'
    OF 'VAT No.'
       LOC:Field            = '4360117727'
    OF 'Page No.'
       LOC:Field            = 'Page No.'
    OF 'Page No. Val'
       LOC:Field            = LOC:Page_No
    OF 'FBN Phone'              ! : 031 205 1705'
       LOC:Field            = '+27 (0)31 205 1705'
    OF 'FBN Fax'                ! Fax: 031 205 2098'
       LOC:Field            = '+27 (0)31 205 2098'
    OF 'Client Name Left'
       LOC:Field            = CLI:ClientName
    OF 'Client Line 1'
       LOC:Field            = ADD:Line1
    OF 'Client Line 2'
       LOC:Field            = ADD:Line2
    OF 'Suburb'
       LOC:Field            = L_RG:Suburb
    OF 'Postal'
       LOC:Field            = L_RG:PostalCode
    OF 'Client VAT No.'
       LOC:Field            = CLI:VATNo
    OF 'Client Name Right'
       LOC:Field            = CLI:ClientName
    OF 'Client No.'
       LOC:Field            = CLI:ClientNo
    OF 'Please Post To'
       LOC:Field            = 'Please Post To'
    OF 'FBN 2 Name'               ! F.B.N. Transport'
       LOC:Field            = 'F.B.N. Transport'
    OF 'FBN 2 Line 1'             ! P.O.Box 1405'
       LOC:Field            = 'P.O.Box 1405'
    OF 'FBN 2 Line 2'             ! Suburb
       LOC:Field            = 'Hillcrest'
    OF 'FBN 2 Line 3'             ! Postal Code
       LOC:Field            = '3650'
    OF 'FBN Line 4'             ! Croft & Johnstone Rd.'
       LOC:Field            = 'Croft & Johnstone Rd.'
    OF 'FBN Line 5'             ! 4057'
       LOC:Field            = '4057'
    OF 'Statement for Period Ending:'
       LOC:Field            = 'Statement for Period Ending:'
    OF 'Statement Date'
       LOC:Field            = FORMAT(STA:StatementDate, @d5)
    OF 'Print Date'
       LOC:Field            = 'Date: ' & FORMAT(TODAY(), @d5)
    OF '90 Days'
       LOC:Field            = '90 Days'
    OF '60 Days'
       LOC:Field            = '60 Days'
    OF '30 Days'
       LOC:Field            = '30 Days'
    OF 'Current'
       LOC:Field            = 'Current'
    OF 'Total'
       LOC:Field            = 'Total'
    OF '90 Days Val.'
       LOC:Field            = FORMAT(STA:Days90, @n-11.2)
    OF '60 Days Val.'
       LOC:Field            = FORMAT(STA:Days60, @n-11.2)
    OF '30 Days Val.'
       LOC:Field            = FORMAT(STA:Days30, @n-11.2)
    OF 'Current Val.'
       LOC:Field            = FORMAT(STA:Current, @n-11.2)
    OF 'Total Val.'
       LOC:Field            = FORMAT(STA:Total, @n-11.2)
    OF 'Total Due'
       LOC:Field            = 'Total Due'
    OF 'Total Due Val'
       LOC:Field            = FORMAT(STA:Total, @n-11.2)
    OF 'Last Period Payments'
       LOC:Field            = 'Last Period Payments'
    OF 'Last Period Payments Val'
       LOC:Field            = FORMAT(STA:Paid, @n-11.2)
    OF 'Amount Paid'
       LOC:Field            = 'Amount Paid'
    OF 'Amount Paid Val'      
       LOC:Field            = ''                    ! Customer fills in on remitance
    OF 'Comment Line'
       LOC:Field            = '**ToDo**'            ! Where from?  Provide fields on Statement run???
    OF 'Comment Line 2'
       LOC:Field            = '**ToDo**'


    ! Note: For the statement items we just record the details in the local vars.  These will be used
    !       once we reach the Start Items tag, THIS MEANS THAT THE Ypos for these entries must be
    !       above the Start Items YPos!!!
    OF 'Invoice Date'
       L_STG:Date_XPos      = L_FQ:XPos
       L_STG:Date_Length    = L_FQ:Length
       L_STG:Date_Align     = L_FQ:Alignment
    OF 'Invoice No.'
       L_STG:InvNo_XPos     = L_FQ:XPos
       L_STG:InvNo_Length   = L_FQ:Length
       L_STG:InvNo_Align    = L_FQ:Alignment
    OF 'DI No'
       L_STG:DI_XPos        = L_FQ:XPos
       L_STG:DI_Length      = L_FQ:Length
       L_STG:DI_Align       = L_FQ:Alignment
    OF 'Debit'
       L_STG:Debit_XPos     = L_FQ:XPos
       L_STG:Debit_Length   = L_FQ:Length
       L_STG:Debit_Align    = L_FQ:Alignment
    OF 'Credit'
       L_STG:Credit_XPos    = L_FQ:XPos
       L_STG:Credit_Length  = L_FQ:Length
       L_STG:Credit_Align   = L_FQ:Alignment
    OF 'Invoice No. 2'          ! Required????
       L_STG:InvNo2_XPos    = L_FQ:XPos
       L_STG:InvNo2_Length  = L_FQ:Length
       L_STG:InvNo2_Align   = L_FQ:Alignment
    OF 'Amount'
       L_STG:Amount_XPos    = L_FQ:XPos
       L_STG:Amount_Length  = L_FQ:Length
       L_STG:Amount_Align   = L_FQ:Alignment

    OF 'Items Start'
       LOC:Items_Started    = TRUE
       LOC:Items_Ended      = FALSE

       LOC:Items_XPos       = L_FQ:XPos
       LOC:Items_Length     = L_FQ:Length
       LOC:Items_Alignment  = L_FQ:Alignment

    OF 'Inv Date Label'
       LOC:Field            = 'Date'
    OF 'Inv No. Label'
       LOC:Field            = 'Inv. No.'
    OF 'DI No Label'
       LOC:Field            = 'DI No.'
    OF 'Debit Label'
       LOC:Field            = 'Debit'
    OF 'Credit Label'
       LOC:Field            = 'Credit'
    OF 'Inv No. 2 Label'
       LOC:Field            = 'Inv. No.'
    OF 'Amount Label'
       LOC:Field            = 'Amount'

    OF 'Items End'
       LOC:Items_Ended      = TRUE
    .

    EXIT
Add_Statement_Items      ROUTINE
    DATA
R:Item      LONG
R:Xpos      LONG
R:Align     LONG
R:Length    LONG
R:Field     LIKE(LOC:Field)

    CODE
    IF Statement_View.Next() ~= LEVEL:Benign
       LOC:Items_Complete   = TRUE
    ELSE
       ! Place the fields according to the layout
       R:Item               = 0
       LOOP
          R:Item           += 1
          CASE R:Item
          OF 1
             R:Field          = FORMAT(STAI:InvoiceDate,@d5)
             R:XPos           = L_STG:Date_XPos
             R:Length         = L_STG:Date_Length
             R:Align          = L_STG:Date_Align
          OF 2
             R:Field          = STAI:IID
             R:XPos           = L_STG:InvNo_XPos
             R:Length         = L_STG:InvNo_Length
             R:Align          = L_STG:InvNo_Align
          OF 3
             R:Field          = STAI:DINo
             R:XPos           = L_STG:DI_XPos
             R:Length         = L_STG:DI_Length
             R:Align          = L_STG:DI_Align
          OF 4
             R:Field          = FORMAT(STAI:Debit,@n-12.2)
             R:XPos           = L_STG:Debit_XPos
             R:Length         = L_STG:Debit_Length
             R:Align          = L_STG:Debit_Align
          OF 5
             R:Field          = FORMAT(STAI:Credit,@n-12.2)
             R:XPos           = L_STG:Credit_XPos
             R:Length         = L_STG:Credit_Length
             R:Align          = L_STG:Credit_Align
          OF 6
             R:Field          = STAI:IID
             R:XPos           = L_STG:InvNo2_XPos
             R:Length         = L_STG:InvNo2_Length
             R:Align          = L_STG:InvNo2_Align
          OF 7
             R:Field          = FORMAT(STAI:Amount,@n-12.2)
             R:XPos           = L_STG:Amount_XPos
             R:Length         = L_STG:Amount_Length
             R:Align          = L_STG:Amount_Align
          ELSE
             BREAK
          .

          IF R:Xpos <= 0 OR (R:Xpos + LOC:Items_Length - 1) < R:Xpos
             MESSAGE('There is a problem with the Items (' & R:Item & '): ' & CLIP(L_FQ:FieldName) & '||XPos: ' & R:Xpos & '|Length: ' & R:Length, 'Print_Cont', ICON:Hand)
             IF R:XPos <= 0
                R:XPos    = 1
             .
             IF (R:XPos + R:Length - 1) < R:XPos
                R:Length  = 30
          .  .
          CASE R:Align
          OF 0
             L_IQ:Text[R:Xpos : R:Xpos + R:Length - 1]  = LEFT(R:Field, R:Length)
          OF 1
             L_IQ:Text[R:Xpos : R:Xpos + R:Length - 1]  = CENTER(R:Field, R:Length)
          OF 2
             L_IQ:Text[R:Xpos : R:Xpos + R:Length - 1]  = RIGHT(R:Field, R:Length)
    .  .  .
    EXIT
! --------------------------------------------------------------------------------------------
Load_Credit                 ROUTINE                 ! No code yet
    LOC:Items_Complete  = TRUE                      ! No items

    ! Load the Invoice
    IF LOC:Cancel = FALSE
       INV:IID          = p:ID
       IF Access:_Invoice.TryFetch(INV:PKey_IID) ~= LEVEL:Benign
          MESSAGE('The Credit Note cannot be found - IID: ' & p:ID, 'Print_Cont', ICON:Hand)
          LOC:Cancel    = TRUE
          POST(EVENT:CloseWindow)
       ELSE
          ! Alls well
    .  . 
    EXIT


Add_Credit                  ROUTINE                 ! No code yet
    CASE CLIP(L_FQ:FieldName)
    OF 'Credit Note Text'
       IF INV:Printed = TRUE
          LOC:Field         = 'Copy Credit Note'
       ELSE
          LOC:Field         = 'Credit Note'

          INV:Printed       = TRUE
          IF Access:_Invoice.TryUpdate() = LEVEL:Benign
       .  .
    OF 'Delete Text'
       LOC:Field            = 'XXXXXXXXXXXXXXXXX'


    OF 'Credit Note No.'
       ! Add the Invoice Number
       LOC:Field            = INV:IID
    OF 'Branch'
       LOC:Field            = INV:BranchName                
    OF 'Date'
       LOC:Field            = FORMAT(INV:InvoiceDate,@d5)
    OF 'For Account'
       LOC:Field            = INV:ClientName
    !OF 'DI No.'
    !   LOC:Field            = INV:DINo

    OF 'Debt Credit'
       LOC:Field            = 'Debtor Credited'

    OF 'Debt Name'
       LOC:Field            = INV:ClientName            ! Repeated
    OF 'Debt Line 1'
       LOC:Field            = INV:ClientLine1
    OF 'Debt Line 2'
       LOC:Field            = INV:ClientLine2
    OF 'Debt Suburb'
       LOC:Field            = INV:ClientSuburb
    OF 'Debt Post'
       LOC:Field            = INV:ClientPostalCode
    OF 'Debt Vat No.'
       LOC:Field            = 'VAT No. ' & INV:VATNo

    OF 'Reason'
       LOC:Field            = 'Reason for Credit Note'
    OF 'Invoice Details'
       LOC:Field            = 'Cr Inv ' & INV:CR_IID

    OF 'Reason Line 1'
       LOC:Field            = INV:InvoiceMessage

    OF 'Reason Line 3'
       !LOC:Field            = 'Total cost of Original Invoice: ' & INV:ShipperName

    OF 'Original Invoice'
       LOC:Field            = 'Total cost of Original Invoice: ' & INV:ShipperLine1

    OF 'Credit Amt.'
       LOC:Field            = FORMAT(INV:FreightCharge, @n12.2)
    OF 'Credit VAT'
       LOC:Field            = FORMAT(INV:VAT, @n12.2)
    OF 'Credit Total'
       LOC:Field            = FORMAT(INV:Total, @n12.2)
    .
    EXIT
! --------------------------------------------------------------------------------------------
Print_Now                   ROUTINE
    DATA
R:Opt       BYTE
R:Ini       CSTRING(35)

    CODE
    ! We only have a device option for the Invoices at this stage
    R:Opt               = GETINI('Printer_Devices', 'Invoice_Opt', '', GLO:Local_INI)
!    IF PRI:PrintType = 0 OR PrinterType                ! Only for Invoices
       IF R:Opt = 0
          R:Ini         = 'Printer_Ports'
       ELSE
          R:Ini         = 'Printer_Devices'
       .
!    ELSE
!       R:Ini            = 'Printer_Ports'
!    .


    EXECUTE PRI:PrintType               ! Invoices, Credit Notes, Delivery Notes, Statements
       LOC:Port         = GETINI(R:Ini, 'Credit_Note', 'LPT1', GLO:Local_INI)
       LOC:Port         = GETINI(R:Ini, 'Delivery_Note', 'LPT1', GLO:Local_INI)
       LOC:Port         = GETINI(R:Ini, 'Statement', 'LPT1', GLO:Local_INI)
    ELSE
       LOC:Port         = GETINI(R:Ini, 'Invoice', 'LPT1', GLO:Local_INI)
    .


    IF R:Opt = 0                            ! Line print
       LOC:Idx     = 0
       QuickWindow{PROP:Timer}      = 10
    ELSE                                    ! Device print
       PRINTER{PROPPRINT:Device}    = CLIP(LOC:Port)
       Print_Lines()
       DO Print_Now_Done
    .
    EXIT
Print_Now_Done        ROUTINE
    IF LOC:Print_without_Preview = 1
       LOC:Result   = 2
    .
    IF p:Preview_Option = 2
       PUTINI('Print_Cont', 'Print_without_Preview', LOC:Print_without_Preview, GLO:Local_INI)
    .

    POST(EVENT:CloseWindow)
    EXIT
! --------------------------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Cont_old')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Relate:PrintFields.SetOpenRelated()
  Relate:PrintFields.Open                                  ! File PrintFields used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Prints.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PrintLayout.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoadDeliveries.UseFile                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoad.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Statements.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_StatementItems.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
      IF p:HideWindow = 1
         QuickWindow{PROP:Hide}   = TRUE
      .
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface)                        ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Print_Cont_old',QuickWindow)               ! Restore window settings from non-volatile store
      LOC:Dot_Empty_Lines     = GETINI('Print_Cont', 'Dot_Empty_Lines', 45, GLO:Local_INI)
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
      QuickWindow{PROP:Timer}     = 0
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
    Relate:PrintFields.Close
      Statement_View.Kill()
  END
  IF SELF.Opened
    INIMgr.Update('Print_Cont_old',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Print_Line
          ! Save temp file for printing....
          Over_#  = TRUE
      
          SETCURSOR(CURSOR:WAIT)
          LOC:Idx = 0
          LOOP
             LOC:Idx  += 1
             GET(LOC:Invoice_Q, LOC:Idx)
             IF ERRORCODE()
                BREAK
             .
      
             
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             ! p:Name_Opt
             !   0.  - None
             !   1.  - Set static name to p:Name
             !   2.  - Use static name
      
             Add_Log(CLIP(L_IQ:Text), '', 'Temp_Rpt.txt', Over_#, 1, 0, 0)
             Over_#   = FALSE
          .
          SETCURSOR()
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Print_Line:2
      ThisWindow.Update()
          Print_Lines()
    OF ?Button_Print_Line
      ThisWindow.Update()
      Line_Print('Temp_Rpt.txt')
      ThisWindow.Reset
    OF ?Button_Print
      ThisWindow.Update()
          DO Print_Now
      
      
    OF ?Cancel
      ThisWindow.Update()
          LOC:Result      = -1
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          ! (p:ID , p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow)
          ! (ULONG, BYTE       , BYTE=0      , BYTE=1          , BYTE=0      ), LONG, PROC
          ! p:PrintType         -     0 = Invoices
          !                           1 = Credit Notes
          !                           2 = Delivery Notes
          !                           3 = Statements
          !
          ! p:ID_Options        -     used for Invoices, if 1 then DID is passed not IID
          !
          ! p:Preview_Option    -     0 = none
          !                           1 = preview (standard)
          !                           2 = ask preview all - when this mode return value is next preview response from user
          !                               This only occurs on first call - although callers responsibility
          !
          ! p:HideWindow              Use in conjunction with p:Preview_Option = 0
          !
          ! LOC:Result
      
          IF LOC:Cancel = FALSE
             DO Load_Print
          .
      
          IF LOC:Cancel = TRUE
             MESSAGE('The requested print type was not found.', 'Print Cont.', ICON:Exclamation)
          ELSE
             EXECUTE PRI:PrintType + 1         ! Invoices, Credit Notes, Delivery Notes, Statements
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Invoice'
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Credit Note'
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Delivery Note'
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Statement'
             ELSE
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - <Unknown>'
          .  .
      !    db.debugout('Print_Cont - p:Type: ' & p:PrintType & ',  Loaded Type: ' & PRI:PrintType)
      
          IF LOC:Cancel = FALSE
             EXECUTE PRI:PrintType             ! Invoices, Credit Notes, Delivery Notes, Statements
                DO Load_Credit
                DO Load_Delivery
                DO Load_Statements
             ELSE
                DO Load_Invoice
          .  .
      
          IF LOC:Cancel = FALSE
      !    db.debugout('Print_Cont - Load_Field_Q')
      
             DO Load_Field_Q
      
      !    db.debugout('Print_Cont - Populate')
      
             DO Populate
      
      !       DO Print_Now
      
      !       POST(EVENT:CloseWindow)
          .
      
          IF LOC:Cancel = FALSE
             IF p:Preview_Option <= 0
                POST(EVENT:Accepted, ?Button_Print)
             ELSIF p:Preview_Option = 2
                UNHIDE(?LOC:Print_without_Preview)
                LOC:Print_without_Preview        = GETINI('Print_Cont', 'Print_without_Preview', '', GLO:Local_INI)
             .
          ELSE
             POST(EVENT:CloseWindow)
          .
    OF EVENT:Timer
          QuickWindow{PROP:Timer} = 0
      
          IF LOC:RetryDelay > 0
             IF ABS(CLOCK() - LOC:RetryDelay) > 150
                LOC:RetryDelay    = 0
             .
          ELSE
             LOC:Idx += 1
             GET(LOC:Invoice_Q, LOC:Idx)
             IF ERRORCODE()
                LOC:Complete = TRUE
          .  .
      
          IF LOC:Complete = FALSE
             IF LOC:RetryDelay <= 0
                !  0    Function Succeeded
                !  1    Device or File Open Error
                !  2    Device or File Write Error
                !  3    Device or file Close Error
                !  4    File Seek Error
      
                LOC:LineResult   = LinePrint(CLIP(L_IQ:Text), SUB(LOC:Port,1,4))
      
                IF LOC:LineResult = 0
                   LOC:RetryTimes    = 3
                   LOC:RetryDelay    = 0
                ELSE
                   IF LOC:RetryTimes > 0
                      LOC:RetryTimes -= 1
                      LOC:RetryDelay  = CLOCK()
                   ELSE
                      CASE LOC:LineResult
                      OF 1
                         LOC:Print_Error_Msg  = 'Device / File Open Error'
                      OF 2
                         LOC:Print_Error_Msg  = 'Device / File Write Error'
                      OF 3
                         LOC:Print_Error_Msg  = 'Device / File Close Error'
                      OF 4
                         LOC:Print_Error_Msg  = 'File Seek Error'
                      .
                      CASE MESSAGE('There was a problem printing a line.||Error: ' & CLIP(LOC:Print_Error_Msg) & '||Cancel all printing?', 'Print_Cont', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
                      OF BUTTON:Yes
                         LOC:Result = -1
                      .
      
                      LOC:Complete  = TRUE
          .  .  .  .
      
          IF LOC:Complete = FALSE
             QuickWindow{PROP:Timer} = 10
          ELSE
             DO Print_Now_Done
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Print_Lines                 PROCEDURE()

YieldGrain      EQUATE(10)

CurrentLine     STRING(255),AUTO
ProgressValue   LONG
LineCount       LONG
!PrevQ           PreviewQueue
!Previewer       &PrintPreviewClass

window WINDOW('Printing'),AT(,,219,33),FONT('MS Sans Serif',8,,FONT:regular),CENTER,GRAY,DOUBLE
       PROGRESS,USE(ProgressValue),AT(4,17,212,14),RANGE(0,100)
       STRING('100%'),AT(200,4),USE(?String3),RIGHT
       STRING('0%'),AT(4,4),USE(?String2),LEFT
       STRING('Page Complete'),AT(85,4),USE(?String1),CENTER
     END

Report REPORT,AT(0,0,8000,8698),PRE(RPT),FONT('Arial',10,,),THOUS
Detail DETAIL,AT(,,8000,177)
         STRING(@s100),AT(0,0,8000,208),USE(CurrentLine)
       END
     END

  CODE
  OPEN(Window)
  ?ProgressValue{PROP:RangeHigh}        = RECORDS(LOC:Invoice_Q)
  OPEN(Report)
  !Report{PROP:Preview}                  = PrevQ.Filename
  !IF SELF.PrintPreview
  !  Previewer &= NEW PrintPreviewClass
  !  Previewer.Init(PrevQ)
  !END
  LOC:Idx     = 0
  LOOP
     LOC:Idx += 1
     GET(LOC:Invoice_Q, LOC:Idx)
     IF ERRORCODE()
        BREAK
     .

     IF CLIP(L_IQ:Text) = '' AND LOC:Idx > LOC:Dot_Empty_Lines    ! Only do for last few lines (was 45)
        L_IQ:Text   = '.'
     .

     CurrentLine    = SUB(L_IQ:Text, 1, PRI:PageColumns)

     PRINT(RPT:Detail)

     ProgressValue   += 1
     DISPLAY(?ProgressValue)
     IF ~(ProgressValue%YieldGrain) THEN YIELD().
  .

  !CurrentLine    = '.'
  PRINT(RPT:Detail)

  ENDPAGE(Report)
  !Report{PROP:FlushPreview}             = CHOOSE(SELF.PrintPreview=False,True,Previewer.Display())
  CLOSE(Report)

  CLOSE(Window)

  !IF SELF.PrintPreview
  !  Previewer.Kill
  !  DISPOSE(Previewer)
  !.
  RETURN


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Prompt1, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Prompt1

!!! <summary>
!!! Generated from procedure template - Window
!!! Invoices / Credit Notes / Delivery Notes / Statements
!!! </summary>
Print_Cont_April_07 PROCEDURE (p:ID, p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow, <*Q_Type_UL_S500 p_Print_Q>)

LOC:Fields_Q         QUEUE,PRE(L_FQ)                       !
FieldName            STRING(50)                            !Field Name
XPos                 SHORT                                 !X Position
YPos                 SHORT                                 !Y Position
Length               SHORT                                 !
Alignment            BYTE                                  !Alignment
PLID                 ULONG                                 !
PFID                 ULONG                                 !
                     END                                   !
LOC:Invoice_Q        QUEUE,PRE(L_IQ)                       !
Line                 ULONG                                 !
Text                 STRING(500)                           !
                     END                                   !
LOC:Locals           GROUP,PRE(LOC)                        !
Dot_Empty_Lines      USHORT                                !Print dot on empty lines from this line (0 is off)
Print_Error_Msg      STRING(200)                           !
Page_No              LONG(1)                               !
Print_without_Preview BYTE                                 !
Result               LONG                                  !
Invoice_Type         STRING(20)                            !
Field                STRING(250)                           !
Cancel               BYTE                                  !
Items_Started        BYTE                                  !
Items_XPos           SHORT                                 !
Items_Length         SHORT                                 !
Items_Alignment      BYTE                                  !
Items_Ended          BYTE                                  !
Items_Complete       BYTE                                  !
Idx                  ULONG                                 !
Port                 STRING(255)                           !LPT1, LPT2, etc.
IID                  ULONG                                 !Invoice Number
Complete             LONG                                  !
LineResult           LONG                                  !
RetryTimes           LONG                                  !
RetryDelay           LONG                                  !
                     END                                   !
LOC:Statement_Group  GROUP,PRE(L_STG)                      !Group of statement specific vars
Date_XPos            ULONG                                 !
Date_Length          ULONG                                 !
Date_Align           BYTE                                  !
InvNo_XPos           ULONG                                 !
InvNo_Length         ULONG                                 !
InvNo_Align          BYTE                                  !
DI_XPos              ULONG                                 !
DI_Length            ULONG                                 !
DI_Align             BYTE                                  !
Debit_XPos           ULONG                                 !
Debit_Length         ULONG                                 !
Debit_Align          BYTE                                  !
Credit_XPos          ULONG                                 !
Credit_Length        ULONG                                 !
Credit_Align         BYTE                                  !
InvNo2_XPos          ULONG                                 !
InvNo2_Length        ULONG                                 !
InvNo2_Align         BYTE                                  !
Amount_XPos          ULONG                                 !
Amount_Length        ULONG                                 !
Amount_Align         BYTE                                  !
                     END                                   !
L_Return_Group       GROUP,PRE(L_RG)                       !
Suburb               STRING(50)                            !Suburb
PostalCode           STRING(10)                            !
Country              STRING(50)                            !Country
Province             STRING(35)                            !Province
City                 STRING(35)                            !City
Found                BYTE                                  !
                     END                                   !
LOC:Printer_Props    GROUP,PRE(L_PP)                       !
PageLines            SHORT(48)                             !Lines on this page
PageColumns          SHORT(80)                             !Columns on a page
PID                  ULONG                                 !Print Type ID
PrintType            BYTE                                  !
                     END                                   !
LOC:Omitted_6        LONG                                  !
QuickWindow          WINDOW('Print Continuous'),AT(,,420,238),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MAX,MDI,HLP('Print_Invoices_Cont'),SYSTEM,TIMER(100)
                       SHEET,AT(3,3,414,214),USE(?Sheet1)
                         TAB('Print '),USE(?Tab1)
                           LIST,AT(6,20,406,194),USE(?List1),FONT('Courier New',8,,FONT:regular,CHARSET:ANSI),VSCROLL, |
  FORMAT('14R(2)|M~Line~L@n13@1020L(2)|M~Text~@s255@'),FROM(LOC:Invoice_Q)
                         END
                         TAB('Fields'),USE(?Tab2)
                           LIST,AT(6,20,405,182),USE(?List2),VSCROLL,FORMAT('200L(2)|M~Field Name~@s50@20R(2)|M~X ' & |
  'Pos~L@n_5@20R(2)|M~Y Pos~L@n_5@20R(2)|M~Length~L@n_5@12R(2)|M~Alignment~L@n3@'),FROM(LOC:Fields_Q)
                           PROMPT(''),AT(7,205,287,10),USE(?Prompt1),LEFT
                         END
                       END
                       BUTTON('Print 3'),AT(130,222,45,14),USE(?Button_Print_Line:2),HIDE
                       BUTTON('Print 2'),AT(175,222,45,14),USE(?Button_Print_Line),HIDE
                       BUTTON('&Re-Print'),AT(242,222,59,14),USE(?Button_RePrint),LEFT,ICON(ICON:Print),FLAT,HIDE, |
  TIP('Print with option to re-print')
                       CHECK(' &Print subsequent without Preview'),AT(3,226),USE(LOC:Print_without_Preview),HIDE, |
  TIP('Print all subsequent prints without preview')
                       BUTTON('&Print'),AT(308,222,,14),USE(?Button_Print),LEFT,ICON(ICON:Print1),FLAT,TIP('Print and close')
                       BUTTON('&Cancel'),AT(368,222,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

    ! How it all works................
    !   Yeah right.. like I know.  Ref: Celine Dion (of all people), Because you l.......

PL_View         VIEW(PrintLayout)
!    PROJECT(PRIL:PLID, PRIL:PID, PRIL:PFID, PRIL:XPos, PRIL:YPos)
       JOIN(PRIF:PKey_PFID, PRIL:PFID)
       PROJECT(PRIF:FieldName)
    .  .



!MAN_View        VIEW(ManifestLoadDeliveries)
!    PROJECT(MALD:MLID, MALD:DIID)
!       JOIN(MAL:PKey_MLID, MALD:MLID)
!       PROJECT(MAL:MID)
!    .  .
View_Statement      VIEW(_StatementItems)
    .


Statement_View      ViewManager
    MAP
Print_Lines             PROCEDURE
    .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:Result)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_Print                   ROUTINE
    LOC:Cancel    = TRUE

    CLEAR(PRI:Record)
    SET(PRI:PKey_PID)
    LOOP
       NEXT(Prints)
       IF ERRORCODE()
          BREAK
       .

       IF p:PrintType = PRI:PrintType
          L_PP:PageLines    = PRI:PageLines
          L_PP:PageColumns  = PRI:PageColumns
          L_PP:PID          = PRI:PID
          L_PP:PrintType    = PRI:PrintType

          LOC:Cancel        = FALSE
          BREAK
    .  .
    EXIT
Load_Field_Q              ROUTINE
    FREE(LOC:Fields_Q)

    PUSHBIND
    BIND('PRIL:PID', PRIL:PID)

    OPEN(PL_View)
    PL_View{PROP:Filter}    = 'PRIL:PID = ' & L_PP:PID
    SET(PL_View)

    LOOP
       NEXT(PL_View)
       IF ERRORCODE()
          BREAK
       .

       L_FQ:FieldName   = PRIF:FieldName
       L_FQ:XPos        = PRIL:XPos
       L_FQ:YPos        = PRIL:YPos
       L_FQ:Length      = PRIL:Length
       L_FQ:Alignment   = PRIL:Alignment

       L_FQ:PLID        = PRIL:PLID
       L_FQ:PFID        = PRIL:PFID
       ADD(LOC:Fields_Q)
    .
    CLOSE(PL_View)
    UNBIND('PRIL:PID')
    POPBIND

    SORT(LOC:Fields_Q, +L_FQ:FieldName)
    ?Prompt1{PROP:Text} = 'Fileds: ' & RECORDS(LOC:Fields_Q)
    EXIT
Check_Fields               ROUTINE
    ! Scan all Field Q items for items that must print on this line.
    ! Load L_IQ:Text with them.

    LOC:Idx     = 0
    LOOP
       LOC:Idx += 1
       GET(LOC:Fields_Q, LOC:Idx)
       IF ERRORCODE()
          BREAK
       .
       IF L_FQ:YPos ~= L_IQ:Line
          CYCLE
       .

       ! Print this on this line then - we dont check for overlap
       CLEAR(LOC:Field)

       EXECUTE L_PP:PrintType             ! Invoices, Credit Notes, Delivery Notes, Statements
          DO Add_Credit
          DO Add_Delivery_Fields
          DO Add_Statement_Fields
       ELSE
          DO Add_Invoice_Fields
       .

       ! Exclude empty fields and 0 line fields
       IF CLIP(LOC:Field) ~= '' AND L_IQ:Line > 0
   db.debugout('[Print_Cont]  Fields: ' & CLIP(L_FQ:FieldName))
          IF L_FQ:XPos <= 0 OR (L_FQ:XPos + L_FQ:Length - 1) < L_FQ:XPos OR (L_FQ:XPos + L_FQ:Length - 1) > SIZE(L_IQ:Text)
             MESSAGE('There is a problem with the field: ' & CLIP(L_FQ:FieldName) & '||XPos: ' & L_FQ:XPos & '|YPos: ' & L_FQ:YPos & '|Length: ' & L_FQ:Length, 'Print_Cont', ICON:Hand)
             IF L_FQ:XPos <= 0
                L_FQ:XPos    = 1
             .
             IF (L_FQ:XPos + L_FQ:Length - 1) < L_FQ:XPos
                L_FQ:Length  = 30
          .  .

          CASE L_FQ:Alignment
          OF 0
             L_IQ:Text[L_FQ:XPos : L_FQ:XPos + L_FQ:Length - 1]  = LEFT(LOC:Field, L_FQ:Length)
          OF 1
             L_IQ:Text[L_FQ:XPos : L_FQ:XPos + L_FQ:Length - 1]  = CENTER(LOC:Field, L_FQ:Length)
          OF 2
             L_IQ:Text[L_FQ:XPos : L_FQ:XPos + L_FQ:Length - 1]  = RIGHT(LOC:Field, L_FQ:Length)
    .  .  .

    EXIT
Populate                     ROUTINE
    ! Populate
    SORT(LOC:Fields_Q, L_FQ:YPos, L_FQ:XPos)

    FREE(LOC:Invoice_Q)
    L_IQ:Line       = 0

    ! Load any zero fields - Note the Items for Statements are loaded here.
    DO Check_Fields

    LOOP            !L_PP:PageLines TIMES
       L_IQ:Line           += 1
       IF L_PP:PageLines < L_IQ:Line             ! We have page break condition
          IF LOC:Items_Complete = TRUE
             BREAK
          .
          LOC:Page_No      += 1

          L_IQ:Line         = 1                 ! Start at line 1 again
          LOC:Items_Started = FALSE
       .

       CLEAR(L_IQ:Text)
       CLEAR(LOC:Field)

       ! For every line, check through the Fields on this line in X Pos order an populate...
       DO Check_Fields

       IF LOC:Items_Started = TRUE
          ! Every line print for Items
          IF LOC:Items_Ended = TRUE             ! On last Item line
             LOC:Items_Started = FALSE
          .

    db.debugout('[Print_Cont - Populate]  Type: ' & L_PP:PrintType & ',   Items')

          IF L_PP:PrintType = 3
             DO Add_Statement_Items
          ELSE
             DO Add_Items
          .

          IF CLIP(LOC:Field) ~= ''
             IF LOC:Items_XPos <= 0 OR (LOC:Items_XPos + LOC:Items_Length - 1) < L_FQ:XPos
                MESSAGE('There is a problem with the Items: ' & CLIP(L_FQ:FieldName) & '||XPos: ' & LOC:Items_XPos & '|YPos: ' & L_FQ:YPos & '|Length: ' & LOC:Items_Length, 'Print_Cont', ICON:Hand)
                IF L_FQ:XPos <= 0
                   L_FQ:XPos    = 1
                .
                IF (L_FQ:XPos + L_FQ:Length - 1) < L_FQ:XPos
                   L_FQ:Length  = 30
             .  .
             CASE LOC:Items_Alignment
             OF 0
                L_IQ:Text[LOC:Items_XPos : LOC:Items_XPos + LOC:Items_Length - 1]  = LEFT(LOC:Field, LOC:Items_Length)
             OF 1
                L_IQ:Text[LOC:Items_XPos : LOC:Items_XPos + LOC:Items_Length - 1]  = CENTER(LOC:Field, LOC:Items_Length)
             OF 2
                L_IQ:Text[LOC:Items_XPos : LOC:Items_XPos + LOC:Items_Length - 1]  = RIGHT(LOC:Field, LOC:Items_Length)
       .  .  .

       IF CLIP(L_IQ:Text) ~= ''
          L_IQ:Text  = SUB(L_IQ:Text, 1, L_PP:PageColumns)            ! Chop off anything beyond page length
       .
       ADD(LOC:Invoice_Q)
    .

    EXIT
! --------------------------------------------------------------------------------------------
Load_Invoice              ROUTINE
    CLEAR(LOC:IID)

    ! Load the Invoice
    IF p:ID_Options = 1         ! We have been passed the DID - we need to get the invoice for it.
       INV:DID          = p:ID
       IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
          LOC:IID       = INV:IID
       ELSE
          ! We cant find the invoice
          MESSAGE('The Invoice cannot be found using the Delivery ID given - DID: ' & p:ID, 'Print_Cont', ICON:Hand)
          LOC:Cancel    = TRUE
          POST(EVENT:CloseWindow)
          p:ID          = 0
       .
    ELSE
       LOC:IID          = p:ID
    .

    IF LOC:Cancel = FALSE
       INV:IID          = LOC:IID
       IF Access:_Invoice.TryFetch(INV:PKey_IID) ~= LEVEL:Benign
          IF p:ID_Options = 1         ! We have been passed the DID
             MESSAGE('The Invoice cannot be found - IID: ' & LOC:IID & '||DID is: ' & p:ID, 'Print_Cont', ICON:Hand)
          ELSE
             MESSAGE('The Invoice cannot be found - IID: ' & LOC:IID, 'Print_Cont', ICON:Hand)
          .
          LOC:Cancel    = TRUE
          POST(EVENT:CloseWindow)
       ELSE
          CLEAR(INI:Record,-1)
          INI:IID       = LOC:IID
          SET(INI:FKey_IID, INI:FKey_IID)


          ! Set Invoice / Copy Invoice
          IF INV:Printed = TRUE
             LOC:Invoice_Type = 'Copy Tax Invoice'
          ELSE
             !LOC:Invoice_Type = 'Tax Invoice'             ! We only print this string if Copy

             INV:Printed      = TRUE
             IF Access:_Invoice.TryUpdate() = LEVEL:Benign
    .  .  .  .
    EXIT

Add_Invoice_Fields    ROUTINE
    CASE CLIP(L_FQ:FieldName)
    OF 'Invoice No.'
       ! Add the Invoice Number
       LOC:Field            = INV:IID
    OF 'Branch'
       LOC:Field            = INV:BranchName                
    OF 'Date'
       LOC:Field            = FORMAT(INV:InvoiceDate,@d5)
    OF 'For Account'
       LOC:Field            = INV:ClientNo & ' ' & INV:ClientName
    OF 'DI No.'
       LOC:Field            = INV:DINo
    OF 'Manifest No.'         
       LOC:Field            = INV:MIDs
    OF 'Shipper'
       LOC:Field            = INV:ShipperName
    OF 'Ship Line 1'
       LOC:Field            = INV:ShipperLine1
    OF 'Ship Line 2'
       IF CLIP(INV:ShipperLine2) = ''
          LOC:Field         = INV:ShipperSuburb
       ELSE
          LOC:Field         = INV:ShipperLine2
       .
    OF 'Ship Suburb'
       IF CLIP(INV:ShipperLine2) = ''
          LOC:Field         = INV:ShipperPostalCode
       ELSE
          IF CLIP(INV:ShipperSuburb) = ''
             LOC:Field      = INV:ShipperPostalCode
          ELSE
             LOC:Field      = INV:ShipperSuburb
       .  .
    OF 'Ship Post'
       IF CLIP(INV:ShipperLine2) = '' OR CLIP(INV:ShipperSuburb) = ''
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ShipperPostalCode
       .
    OF 'Consignee'
       LOC:Field            = INV:ConsigneeName
    OF 'Con Line 1'
       LOC:Field            = INV:ConsigneeLine1
    OF 'Con Line 2'
       IF CLIP(INV:ConsigneeLine2) = ''
          LOC:Field         = INV:ConsigneeSuburb
       ELSE
          LOC:Field         = INV:ConsigneeLine2
       .
    OF 'Con Suburb'
       IF CLIP(INV:ConsigneeLine2) = ''
          LOC:Field         = INV:ConsigneePostalCode
       ELSE
          IF CLIP(INV:ConsigneeSuburb) = ''
             LOC:Field      = INV:ConsigneePostalCode
          ELSE
             LOC:Field      = INV:ConsigneeSuburb
       .  .
    OF 'Con Post'
       IF CLIP(INV:ConsigneeLine2) = '' OR CLIP(INV:ConsigneeSuburb) = ''
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ConsigneePostalCode
       .
    OF 'Items Start'
       LOC:Items_Started    = TRUE
       LOC:Items_Ended      = FALSE

       LOC:Items_XPos       = L_FQ:XPos
       LOC:Items_Length     = L_FQ:Length
       LOC:Items_Alignment  = L_FQ:Alignment
    OF 'Items End'
       LOC:Items_Ended      = TRUE

    OF 'Vol Weight'
       LOC:Field            = 'Vol Wt : ' & FORMAT(INV:VolumetricWeight, @n9.2)
    OF 'Act Weight'
       LOC:Field            = 'Act Wt : ' & FORMAT(INV:Weight, @n9.2)

    OF 'Debt Name'
       LOC:Field            = INV:ClientName            ! Repeated
    OF 'Debt Line 1'
       LOC:Field            = INV:ClientLine1
    OF 'Debt Line 2'
       IF CLIP(INV:ClientLine2) = ''
          LOC:Field         = INV:ClientSuburb
       ELSE
          LOC:Field         = INV:ClientLine2
       .
    OF 'Debt Suburb'
       IF CLIP(INV:ClientLine2) = ''                    ! Suburb already done
          LOC:Field         = INV:ClientPostalCode      ! Do postcode here
       ELSE
          IF CLIP(INV:ClientSuburb) = ''
             LOC:Field      = INV:ClientPostalCode
          ELSE
             LOC:Field      = INV:ClientSuburb
       .  .
    OF 'Debt Post'
       IF CLIP(INV:ClientSuburb) = '' OR CLIP(INV:ClientLine2) = ''     ! Postcode already done
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ClientPostalCode
       .
    OF 'Debt Vat No.'
       LOC:Field            = 'VAT No. ' & INV:VATNo
    OF 'Debt Ref.'
       LOC:Field            = 'Ref. ' & INV:ClientReference
    OF 'Charge Fuel'
       LOC:Field            = FORMAT(INV:FuelSurcharge, @n-12.2)
    OF 'Charge Insurance'
       LOC:Field            = FORMAT(INV:Insurance, @n-12.2)
    OF 'Charge Docs'
       LOC:Field            = FORMAT(INV:Documentation, @n-12.2)
    OF 'Charge Freight'
       LOC:Field            = FORMAT(INV:FreightCharge, @n-12.2)
    OF 'Charge VAT'
       LOC:Field            = FORMAT(INV:VAT, @n-12.2)
    OF 'Charge Total'
       LOC:Field            = FORMAT(INV:Total, @n-12.2)
    OF 'Copy Invoice'
       LOC:Field            = LOC:Invoice_Type          ! Non invoice field
    OF 'Fuel Surcharge'
       LOC:Field            = 'Fuel Surcharge'
    OF 'Invoice Message'
       LOC:Field            = INV:InvoiceMessage
    .

    EXIT
! --------------------------------------------------------------------------------------------
Add_Items                   ROUTINE
    NEXT(_InvoiceItems)
    IF ERRORCODE() OR INI:IID ~= INV:IID
       LOC:Items_Complete   = TRUE
    ELSE
       LOC:Field            = INI:Units & ' ' & CLIP(INI:Description) & ' of ' & CLIP(INI:Commodity)

        !                L_IQ:Text[LOC:Items_XPos : LOC:Items_XPos + LOC:Items_Length - 1]  = LEFT(LOC:Field, LOC:Items_Length)

       IF CLIP(INI:ContainerDescription) ~= ''
          Len_CD_#          = LEN(CLIP(INI:ContainerDescription))
          Len_F_#           = LEN(CLIP(LOC:Field))

          IF LOC:Items_Length / 2 < Len_CD_#  AND  Len_F_# + Len_CD_# + 2 < LOC:Items_Length
             ! Then right align
             LOC:Field[LOC:Items_Length - Len_CD_# : LOC:Items_Length] = INI:ContainerDescription
          ELSE
             IF Len_F_# > LOC:Items_Length / 2 OR Len_CD_# > LOC:Items_Length / 2        ! Then just add after
                LOC:Field      = CLIP(LOC:Field) & '  ' & INI:ContainerDescription
             ELSE
                LOC:Field[LOC:Items_Length / 2 : LOC:Items_Length] = INI:ContainerDescription
    .  .  .  .
    EXIT
! --------------------------------------------------------------------------------------------
Load_Delivery             ROUTINE
    ! Load the Delivey Note
    DEL:DID         = p:ID
    IF Access:Deliveries.TryFetch(DEL:PKey_DID) ~= LEVEL:Benign
       MESSAGE('The DI cannot be found - ID: ' & p:ID, 'Print_Cont', ICON:Hand)
       LOC:Cancel   = TRUE
       POST(EVENT:CloseWindow)
    ELSE
!       CLEAR(DELI:Record,-1)
!       DELI:DID     = p:ID
!       SET(DELI:FKey_DID_ItemNo, DELI:FKey_DID_ItemNo)
       ! Note: We are going to use the Invoice and Invoice Items for Delivery Note printing

       ! We need the IID - so we set the items file after getting that only...
    .

    IF LOC:Cancel = FALSE
       ! Load the associated invoice
       INV:DID      = DEL:DID
       IF Access:_Invoice.TryFetch(INV:FKey_DID) ~= LEVEL:Benign
          MESSAGE('The DI Invoice cannot be found - DID: ' & p:ID & '||Delivery Note will not be printed.', 'Print_Cont', ICON:Hand)
          LOC:Cancel = TRUE
          CLEAR(INV:Record)
       ELSE
          CLEAR(INI:Record,-1)
          INI:IID    = INV:IID
          SET(INI:FKey_IID, INI:FKey_IID)
    .  .
    EXIT

Add_Delivery_Fields    ROUTINE
    CASE CLIP(L_FQ:FieldName)
    OF 'Invoice No.'
       ! Add the Invoice Number
       IF INV:POD_IID = 0                       ! Old Invoices will be missing PODs - Feb 21 05
          LOC:Field         = INV:IID
       ELSE
          LOC:Field         = INV:POD_IID
       .
    OF 'Branch'
       LOC:Field            = INV:BranchName                
    OF 'Date'
       LOC:Field            = FORMAT(INV:InvoiceDate,@d5)
    OF 'For Account'
       LOC:Field            = INV:ClientNo              ! INV:ClientName
    OF 'DI No.'
       LOC:Field            = INV:DINo
    OF 'Manifest No.'         
       LOC:Field            = INV:MIDs
    OF 'Shipper'
       LOC:Field            = INV:ShipperName
    OF 'Ship Line 1'
       LOC:Field            = INV:ShipperLine1
    OF 'Ship Line 2'
       IF CLIP(INV:ShipperLine2) = ''
          LOC:Field         = INV:ShipperSuburb
       ELSE
          LOC:Field         = INV:ShipperLine2
       .
    OF 'Ship Suburb'
       IF CLIP(INV:ShipperLine2) = ''
          LOC:Field         = INV:ShipperPostalCode
       ELSE
          IF CLIP(INV:ShipperSuburb) = ''
             LOC:Field      = INV:ShipperPostalCode
          ELSE
             LOC:Field      = INV:ShipperSuburb
       .  .
    OF 'Ship Post'
       IF CLIP(INV:ShipperLine2) = '' OR CLIP(INV:ShipperSuburb) = ''
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ShipperPostalCode
       .
    OF 'Consignee'
       LOC:Field            = INV:ConsigneeName
    OF 'Con Line 1'
       LOC:Field            = INV:ConsigneeLine1
    OF 'Con Line 2'
       IF CLIP(INV:ConsigneeLine2) = ''
          LOC:Field         = INV:ConsigneeSuburb
       ELSE
          LOC:Field         = INV:ConsigneeLine2
       .
    OF 'Con Suburb'
       IF CLIP(INV:ConsigneeLine2) = ''
          LOC:Field         = INV:ConsigneePostalCode
       ELSE
          IF CLIP(INV:ConsigneeSuburb) = ''
             LOC:Field      = INV:ConsigneePostalCode
          ELSE
             LOC:Field      = INV:ConsigneeSuburb
       .  .
    OF 'Con Post'
       IF CLIP(INV:ConsigneeLine2) = '' OR CLIP(INV:ConsigneeSuburb) = ''
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ConsigneePostalCode
       .

    OF 'Items Start'
       LOC:Items_Started    = TRUE
       LOC:Items_Ended      = FALSE

       LOC:Items_XPos       = L_FQ:XPos
       LOC:Items_Length     = L_FQ:Length
       LOC:Items_Alignment  = L_FQ:Alignment
    OF 'Items End'
       LOC:Items_Ended      = TRUE
    OF 'Vol Weight'
       IF INV:VolumetricWeight ~= 0.0
          LOC:Field         = 'Vol Wt : ' & FORMAT(INV:VolumetricWeight, @n9.2)
       .
    OF 'Act Weight'
       LOC:Field            = 'Act Wt : ' & FORMAT(INV:Weight, @n9.2)
    OF 'Spec. Inst. 1'
       LOC:Field            = SUB(DEL:SpecialDeliveryInstructions, 1, L_FQ:Length)
    OF 'Spec. Inst. 2'
       LOC:Field            = SUB(DEL:SpecialDeliveryInstructions, L_FQ:Length + 1, L_FQ:Length)
    OF 'Spec. Inst. 3'
       LOC:Field            = SUB(DEL:SpecialDeliveryInstructions, 2 * L_FQ:Length + 1, L_FQ:Length)
    OF 'Debt Ref.'
       LOC:Field            = 'Ref. ' & INV:ClientReference
    .

    EXIT
! --------------------------------------------------------------------------------------------
Load_Statements             ROUTINE
    ! Load the Statement
    STA:STID        = p:ID
    IF Access:_Statements.TryFetch(STA:PKey_STID) ~= LEVEL:Benign
       MESSAGE('The Statement cannot be found - STID: ' & p:ID, 'Print_Cont', ICON:Hand)
       LOC:Cancel   = TRUE
       POST(EVENT:CloseWindow)
    ELSE
       ! Load the Client Record
       CLI:CID  = STA:CID
       IF Access:Clients.TryFetch(CLI:PKey_CID) ~= LEVEL:Benign
          MESSAGE('The Statement client cannot be found - CID: ' & STA:CID & '||Statement will not be printed.', 'Print_Cont', ICON:Hand)
          LOC:Cancel   = TRUE
          POST(EVENT:CloseWindow)
          CLEAR(CLI:Record)
       ELSE
          ! Load Clients address
          ADD:AID       = CLI:AID
          IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
             ! Load Suburb details
             L_Return_Group = Get_Suburb_Details(ADD:SUID)
       .  .

       Statement_View.Init(View_Statement, Relate:_StatementItems)
       Statement_View.AddSortOrder( STAI:FKey_STID )
       Statement_View.AppendOrder( 'STAI:STIID' )
       Statement_View.AddRange( STAI:STID, p:ID )
       !Statement_View.SetFilter( '' )

       Statement_View.Reset(1)

!       CLEAR(STAI:Record,-1)
!       STAI:STID    = p:ID
!       SET(STAI:FKey_STID, STAI:FKey_STID)
    .
    EXIT

Add_Statement_Fields     ROUTINE
    CASE CLIP(L_FQ:FieldName)
    OF 'Statement No.'
       LOC:Field            = STA:STID
    OF 'FBN Name'               ! F.B.N. Transport'
       LOC:Field            = 'F.B.N. Transport'
    OF 'FBN Line 1'             ! P.O.Box 1405'
       LOC:Field            = 'P.O.Box 1405'
    OF 'FBN Line 2'             ! Suburb
       LOC:Field            = 'Hillcrest'
    OF 'FBN Line 3'             ! Postal Code
       LOC:Field            = '3650'
    OF 'VAT No.'
       LOC:Field            = '4360117727'
    OF 'Page No.'
       LOC:Field            = 'Page No.'
    OF 'Page No. Val'
       LOC:Field            = LOC:Page_No
    OF 'FBN Phone'              ! : 031 205 1705'
       LOC:Field            = '+27 (0)31 205 1705'
    OF 'FBN Fax'                ! Fax: 031 205 2098'
       LOC:Field            = '+27 (0)31 205 2098'
    OF 'Client Name Left'
       LOC:Field            = CLI:ClientName
    OF 'Client Line 1'
       LOC:Field            = ADD:Line1
    OF 'Client Line 2'
       LOC:Field            = ADD:Line2
    OF 'Suburb'
       LOC:Field            = L_RG:Suburb
    OF 'Postal'
       LOC:Field            = L_RG:PostalCode
    OF 'Client VAT No.'
       LOC:Field            = CLI:VATNo
    OF 'Client Name Right'
       LOC:Field            = CLI:ClientName
    OF 'Client No.'
       LOC:Field            = CLI:ClientNo
    OF 'Please Post To'
       LOC:Field            = 'Please Post To'
    OF 'FBN 2 Name'               ! F.B.N. Transport'
       LOC:Field            = 'F.B.N. Transport'
    OF 'FBN 2 Line 1'             ! P.O.Box 1405'
       LOC:Field            = 'P.O.Box 1405'
    OF 'FBN 2 Line 2'             ! Suburb
       LOC:Field            = 'Hillcrest'
    OF 'FBN 2 Line 3'             ! Postal Code
       LOC:Field            = '3650'
    OF 'FBN Line 4'             ! Croft & Johnstone Rd.'
       LOC:Field            = 'Croft & Johnstone Rd.'
    OF 'FBN Line 5'             ! 4057'
       LOC:Field            = '4057'
    OF 'Statement for Period Ending:'
       LOC:Field            = 'Statement for Period Ending:'
    OF 'Statement Date'
       LOC:Field            = FORMAT(STA:StatementDate, @d5)
    OF 'Print Date'
       LOC:Field            = 'Date: ' & FORMAT(TODAY(), @d5)
    OF '90 Days'
       LOC:Field            = '90 Days'
    OF '60 Days'
       LOC:Field            = '60 Days'
    OF '30 Days'
       LOC:Field            = '30 Days'
    OF 'Current'
       LOC:Field            = 'Current'
    OF 'Total'
       LOC:Field            = 'Total'
    OF '90 Days Val.'
       LOC:Field            = FORMAT(STA:Days90, @n-11.2)
    OF '60 Days Val.'
       LOC:Field            = FORMAT(STA:Days60, @n-11.2)
    OF '30 Days Val.'
       LOC:Field            = FORMAT(STA:Days30, @n-11.2)
    OF 'Current Val.'
       LOC:Field            = FORMAT(STA:Current, @n-11.2)
    OF 'Total Val.'
       LOC:Field            = FORMAT(STA:Total, @n-11.2)
    OF 'Total Due'
       LOC:Field            = 'Total Due'
    OF 'Total Due Val'
       LOC:Field            = FORMAT(STA:Total, @n-11.2)
    OF 'Last Period Payments'
       LOC:Field            = 'Last Period Payments'
    OF 'Last Period Payments Val'
       LOC:Field            = FORMAT(STA:Paid, @n-11.2)
    OF 'Amount Paid'
       LOC:Field            = 'Amount Paid'
    OF 'Amount Paid Val'      
       LOC:Field            = ''                    ! Customer fills in on remitance
    OF 'Comment Line'
       LOC:Field            = '**ToDo**'            ! Where from?  Provide fields on Statement run???
    OF 'Comment Line 2'
       LOC:Field            = '**ToDo**'


    ! Note: For the statement items we just record the details in the local vars.  These will be used
    !       once we reach the Start Items tag, THIS MEANS THAT THE Ypos for these entries must be
    !       above the Start Items YPos!!!
    OF 'Invoice Date'
       L_STG:Date_XPos      = L_FQ:XPos
       L_STG:Date_Length    = L_FQ:Length
       L_STG:Date_Align     = L_FQ:Alignment
    OF 'Invoice No.'
       L_STG:InvNo_XPos     = L_FQ:XPos
       L_STG:InvNo_Length   = L_FQ:Length
       L_STG:InvNo_Align    = L_FQ:Alignment
    OF 'DI No'
       L_STG:DI_XPos        = L_FQ:XPos
       L_STG:DI_Length      = L_FQ:Length
       L_STG:DI_Align       = L_FQ:Alignment
    OF 'Debit'
       L_STG:Debit_XPos     = L_FQ:XPos
       L_STG:Debit_Length   = L_FQ:Length
       L_STG:Debit_Align    = L_FQ:Alignment
    OF 'Credit'
       L_STG:Credit_XPos    = L_FQ:XPos
       L_STG:Credit_Length  = L_FQ:Length
       L_STG:Credit_Align   = L_FQ:Alignment
    OF 'Invoice No. 2'          ! Required????
       L_STG:InvNo2_XPos    = L_FQ:XPos
       L_STG:InvNo2_Length  = L_FQ:Length
       L_STG:InvNo2_Align   = L_FQ:Alignment
    OF 'Amount'
       L_STG:Amount_XPos    = L_FQ:XPos
       L_STG:Amount_Length  = L_FQ:Length
       L_STG:Amount_Align   = L_FQ:Alignment

    OF 'Items Start'
       LOC:Items_Started    = TRUE
       LOC:Items_Ended      = FALSE

       LOC:Items_XPos       = L_FQ:XPos
       LOC:Items_Length     = L_FQ:Length
       LOC:Items_Alignment  = L_FQ:Alignment

    OF 'Inv Date Label'
       LOC:Field            = 'Date'
    OF 'Inv No. Label'
       LOC:Field            = 'Inv. No.'
    OF 'DI No Label'
       LOC:Field            = 'DI No.'
    OF 'Debit Label'
       LOC:Field            = 'Debit'
    OF 'Credit Label'
       LOC:Field            = 'Credit'
    OF 'Inv No. 2 Label'
       LOC:Field            = 'Inv. No.'
    OF 'Amount Label'
       LOC:Field            = 'Amount'

    OF 'Items End'
       LOC:Items_Ended      = TRUE
    .

    EXIT
Add_Statement_Items      ROUTINE
    DATA
R:Item      LONG
R:Xpos      LONG
R:Align     LONG
R:Length    LONG
R:Field     LIKE(LOC:Field)

    CODE
    IF Statement_View.Next() ~= LEVEL:Benign
       LOC:Items_Complete   = TRUE
    ELSE
       ! Place the fields according to the layout
       R:Item               = 0
       LOOP
          R:Item           += 1
          CASE R:Item
          OF 1
             R:Field          = FORMAT(STAI:InvoiceDate,@d5)
             R:XPos           = L_STG:Date_XPos
             R:Length         = L_STG:Date_Length
             R:Align          = L_STG:Date_Align
          OF 2
             R:Field          = STAI:IID
             R:XPos           = L_STG:InvNo_XPos
             R:Length         = L_STG:InvNo_Length
             R:Align          = L_STG:InvNo_Align
          OF 3
             R:Field          = STAI:DINo
             R:XPos           = L_STG:DI_XPos
             R:Length         = L_STG:DI_Length
             R:Align          = L_STG:DI_Align
          OF 4
             R:Field          = FORMAT(STAI:Debit,@n-12.2)
             R:XPos           = L_STG:Debit_XPos
             R:Length         = L_STG:Debit_Length
             R:Align          = L_STG:Debit_Align
          OF 5
             R:Field          = FORMAT(STAI:Credit,@n-12.2)
             R:XPos           = L_STG:Credit_XPos
             R:Length         = L_STG:Credit_Length
             R:Align          = L_STG:Credit_Align
          OF 6
             R:Field          = STAI:IID
             R:XPos           = L_STG:InvNo2_XPos
             R:Length         = L_STG:InvNo2_Length
             R:Align          = L_STG:InvNo2_Align
          OF 7
             R:Field          = FORMAT(STAI:Amount,@n-12.2)
             R:XPos           = L_STG:Amount_XPos
             R:Length         = L_STG:Amount_Length
             R:Align          = L_STG:Amount_Align
          ELSE
             BREAK
          .

          IF R:Xpos <= 0 OR (R:Xpos + LOC:Items_Length - 1) < R:Xpos
             MESSAGE('There is a problem with the Items (' & R:Item & '): ' & CLIP(L_FQ:FieldName) & '||XPos: ' & R:Xpos & '|Length: ' & R:Length, 'Print_Cont', ICON:Hand)
             IF R:XPos <= 0
                R:XPos    = 1
             .
             IF (R:XPos + R:Length - 1) < R:XPos
                R:Length  = 30
          .  .
          CASE R:Align
          OF 0
             L_IQ:Text[R:Xpos : R:Xpos + R:Length - 1]  = LEFT(R:Field, R:Length)
          OF 1
             L_IQ:Text[R:Xpos : R:Xpos + R:Length - 1]  = CENTER(R:Field, R:Length)
          OF 2
             L_IQ:Text[R:Xpos : R:Xpos + R:Length - 1]  = RIGHT(R:Field, R:Length)
    .  .  .
    EXIT
! --------------------------------------------------------------------------------------------
Load_Credit                     ROUTINE
    LOC:Items_Complete  = TRUE                          ! No items

    ! Load the Invoice
    IF LOC:Cancel = FALSE
       INV:IID          = p:ID
       IF Access:_Invoice.TryFetch(INV:PKey_IID) ~= LEVEL:Benign
          MESSAGE('The Credit Note cannot be found - IID: ' & p:ID, 'Print_Cont', ICON:Hand)
          LOC:Cancel    = TRUE
          POST(EVENT:CloseWindow)
       ELSE
          ! Alls well
    .  . 
    EXIT


Add_Credit                  ROUTINE                 ! No code yet
    CASE CLIP(L_FQ:FieldName)
    OF 'Credit Note Text'
       IF INV:Printed = TRUE
          LOC:Field         = 'Copy Credit Note'
       ELSE
          LOC:Field         = 'Credit Note'

          INV:Printed       = TRUE
          IF Access:_Invoice.TryUpdate() = LEVEL:Benign
       .  .
    OF 'Delete Text'
       LOC:Field            = 'XXXXXXXXXXXXXXXXX'


    OF 'Credit Note No.'
       ! Add the Invoice Number
       LOC:Field            = INV:IID
    OF 'Branch'
       LOC:Field            = INV:BranchName                
    OF 'Date'
       LOC:Field            = FORMAT(INV:InvoiceDate,@d5)
    OF 'For Account'
       LOC:Field            = INV:ClientName
    !OF 'DI No.'
    !   LOC:Field            = INV:DINo

    OF 'Debt Credit'
       LOC:Field            = 'Debtor Credited'

    OF 'Debt Name'
       LOC:Field            = INV:ClientName            ! Repeated
    OF 'Debt Line 1'
       LOC:Field            = INV:ClientLine1
    OF 'Debt Line 2'
       LOC:Field            = INV:ClientLine2
    OF 'Debt Suburb'
       LOC:Field            = INV:ClientSuburb
    OF 'Debt Post'
       LOC:Field            = INV:ClientPostalCode
    OF 'Debt Vat No.'
       LOC:Field            = 'VAT No. ' & INV:VATNo

    OF 'Reason'
       LOC:Field            = 'Reason for Credit Note'
    OF 'Invoice Details'
       LOC:Field            = 'Cr Inv ' & INV:CR_IID

    OF 'Reason Line 1'
       LOC:Field            = INV:InvoiceMessage

    OF 'Reason Line 3'
       !LOC:Field            = 'Total cost of Original Invoice: ' & INV:ShipperName

    OF 'Original Invoice'
       LOC:Field            = 'Total cost of Original Invoice: ' & INV:ShipperLine1

    OF 'Credit Amt.'
       LOC:Field            = FORMAT(INV:FreightCharge, @n12.2)
    OF 'Credit VAT'
       LOC:Field            = FORMAT(INV:VAT, @n12.2)
    OF 'Credit Total'
       LOC:Field            = FORMAT(INV:Total, @n12.2)
    .
    EXIT
! --------------------------------------------------------------------------------------------
Load_Q                           ROUTINE
    ! (p:ID , p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow, <Q_Type_UL_S500 p_Print_Q>)
    ! Q_Type_UL_S500      QUEUE,TYPE
    ! UL1_        ULONG
    ! S500        STRING(500)
    !                     .

    ! Expect 1st record of passed Q to have max columns... ??
    GET(p_Print_Q, 1)
    IF ERRORCODE()
    .
    L_PP:PageLines      = RECORDS(p_Print_Q)
    L_PP:PageColumns    = LEN(CLIP(p_Print_Q.S500))

    L_PP:PrintType      = p:PrintType

    FREE(LOC:Invoice_Q)
    L_IQ:Line           = 0

    LOOP
       L_IQ:Line       += 1
       !IF L_PP:PageLines < L_IQ:Line             ! We have page break condition
       !   LOC:Page_No  += 1
       !   L_IQ:Line     = 1                      ! Start at line 1 again
       !.
       GET(p_Print_Q, L_IQ:Line)
       IF ERRORCODE()
          BREAK
       .

       L_IQ:Text        = CLIP(p_Print_Q.S500)

       ADD(LOC:Invoice_Q)
    .
    EXIT
! --------------------------------------------------------------------------------------------
Print_Now                   ROUTINE
    DATA
R:Opt       BYTE
R:Ini       CSTRING(35)

    CODE
    ! We only have a device option for the Invoices at this stage
    R:Opt               = GETINI('Printer_Devices', 'Invoice_Opt', '', GLO:Local_INI)
!    IF L_PP:PrintType = 0 OR PrinterType                ! Only for Invoices
       IF R:Opt = 0
          R:Ini         = 'Printer_Ports'
       ELSE
          R:Ini         = 'Printer_Devices'
       .
!    ELSE
!       R:Ini            = 'Printer_Ports'
!    .


    EXECUTE L_PP:PrintType + 1           ! Invoices, Credit Notes, Delivery Notes, Statements
       LOC:Port         = GETINI(R:Ini, 'Invoice', 'LPT1', GLO:Local_INI)
       LOC:Port         = GETINI(R:Ini, 'Credit_Note', 'LPT1', GLO:Local_INI)
       LOC:Port         = GETINI(R:Ini, 'Delivery_Note', 'LPT1', GLO:Local_INI)
       LOC:Port         = GETINI(R:Ini, 'Statement', 'LPT1', GLO:Local_INI)
    ELSE
       LOC:Port         = GETINI(R:Ini, 'Invoice', 'LPT1', GLO:Local_INI)   ! Arbitrarily use this type if not set
    .


    IF R:Opt = 0                            ! Line print
       LOC:Idx     = 0
       QuickWindow{PROP:Timer}      = 10
    ELSE                                    ! Device print
       PRINTER{PROPPRINT:Device}    = CLIP(LOC:Port)
       Print_Lines()
       DO Print_Now_Done
    .
    EXIT
Print_Now_Done        ROUTINE
    IF LOC:Print_without_Preview = 1
       LOC:Result   = 2
    .
    IF p:Preview_Option = 2
       PUTINI('Print_Cont', 'Print_without_Preview', LOC:Print_without_Preview, GLO:Local_INI)
    .

    POST(EVENT:CloseWindow)
    EXIT
! --------------------------------------------------------------------------------------------
Check_Omitted           ROUTINE
    LOC:Omitted_6   = OMITTED(6)
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Cont_April_07')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
      DO Check_Omitted
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Relate:PrintFields.SetOpenRelated()
  Relate:PrintFields.Open                                  ! File PrintFields used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Prints.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PrintLayout.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoadDeliveries.UseFile                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoad.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Statements.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_StatementItems.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
      IF p:HideWindow = 1
         QuickWindow{PROP:Hide}   = TRUE
      .
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface)                        ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Print_Cont_April_07',QuickWindow)          ! Restore window settings from non-volatile store
      LOC:Dot_Empty_Lines     = GETINI('Print_Cont', 'Dot_Empty_Lines', 45, GLO:Local_INI)
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
      QuickWindow{PROP:Timer}     = 0
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
    Relate:PrintFields.Close
      Statement_View.Kill()
  END
  IF SELF.Opened
    INIMgr.Update('Print_Cont_April_07',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Print_Line
          ! Save temp file for printing....
          Over_#  = TRUE
      
          SETCURSOR(CURSOR:WAIT)
          LOC:Idx = 0
          LOOP
             LOC:Idx  += 1
             GET(LOC:Invoice_Q, LOC:Idx)
             IF ERRORCODE()
                BREAK
             .
      
             
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             ! p:Name_Opt
             !   0.  - None
             !   1.  - Set static name to p:Name
             !   2.  - Use static name
      
             Add_Log(CLIP(L_IQ:Text), '', 'Temp_Rpt.txt', Over_#, 1, 0, 0)
             Over_#   = FALSE
          .
          SETCURSOR()
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Print_Line:2
      ThisWindow.Update()
          Print_Lines()
    OF ?Button_Print_Line
      ThisWindow.Update()
      Line_Print('Temp_Rpt.txt')
      ThisWindow.Reset
    OF ?Button_Print
      ThisWindow.Update()
          DO Print_Now
      
      
    OF ?Cancel
      ThisWindow.Update()
          LOC:Result      = -1
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          ! (p:ID , p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow, <Q_Type_UL_S500 p_Print_Q>)
          ! (ULONG, BYTE       , BYTE=0      , BYTE=1          , BYTE=0      , <Q_Type_UL_S500 p_Print_Q>), LONG, PROC
          !   1           2           3               4               5                   6
          ! p:PrintType         -     0 = Invoices
          !                           1 = Credit Notes
          !                           2 = Delivery Notes
          !                           3 = Statements
          !                           42 = Passed Q
          !
          ! p:ID_Options        -     used for Invoices, if 1 then DID is passed not IID
          !
          ! p:Preview_Option    -     0 = none
          !                           1 = preview (standard)
          !                           2 = ask preview all - when this mode return value is next preview response from user
          !                               This only occurs on first call - although callers responsibility
          !
          ! p:HideWindow              Use in conjunction with p:Preview_Option = 0
          !
          ! LOC:Result
          !
          ! Q_Type_UL_S500      QUEUE,TYPE
          ! UL1_        ULONG
          ! S500        STRING(500)
      
          IF p:PrintType = 42 AND LOC:Omitted_6
             LOC:Cancel   = TRUE
             MESSAGE('Print type is 42 but no print Q has been passed.', 'Print_Cont', ICON:Hand)
          .
      
          IF LOC:Cancel = FALSE AND p:PrintType ~= 42
             DO Load_Print
          .
      
          IF LOC:Cancel = TRUE
             MESSAGE('The requested print type was not found.', 'Print Cont.', ICON:Exclamation)
          ELSE
             EXECUTE p:PrintType + 1         ! Invoices, Credit Notes, Delivery Notes, Statements
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Invoice'
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Credit Note'
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Delivery Note'
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Statement'
             ELSE
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - ' & p:PrintType & ' <Unknown>'
          .  .
      !    db.debugout('Print_Cont - p:Type: ' & p:PrintType & ',  Loaded Type: ' & p:PrintType)
      
          IF LOC:Cancel = FALSE
             EXECUTE p:PrintType + 1         ! Invoices, Credit Notes, Delivery Notes, Statements
                DO Load_Invoice
                DO Load_Credit
                DO Load_Delivery
                DO Load_Statements
             ELSE
                DO Load_Q
          .  .
      
          IF LOC:Cancel = FALSE AND p:PrintType ~= 42
             DO Load_Field_Q
      
             DO Populate
          .
      
          IF LOC:Cancel = FALSE
             IF p:Preview_Option <= 0
                POST(EVENT:Accepted, ?Button_Print)
             ELSIF p:Preview_Option = 2
                UNHIDE(?LOC:Print_without_Preview)
                LOC:Print_without_Preview        = GETINI('Print_Cont', 'Print_without_Preview', '', GLO:Local_INI)
             .
          ELSE
             POST(EVENT:CloseWindow)
          .
    OF EVENT:Timer
          QuickWindow{PROP:Timer} = 0
      
          IF LOC:RetryDelay > 0
             IF ABS(CLOCK() - LOC:RetryDelay) > 150
                LOC:RetryDelay    = 0
             .
          ELSE
             LOC:Idx += 1
             GET(LOC:Invoice_Q, LOC:Idx)
             IF ERRORCODE()
                LOC:Complete = TRUE
          .  .
      
          IF LOC:Complete = FALSE
             IF LOC:RetryDelay <= 0
                !  0    Function Succeeded
                !  1    Device or File Open Error
                !  2    Device or File Write Error
                !  3    Device or file Close Error
                !  4    File Seek Error
      
                LOC:LineResult   = LinePrint(CLIP(L_IQ:Text), SUB(LOC:Port,1,4))
      
                IF LOC:LineResult = 0
                   LOC:RetryTimes    = 3
                   LOC:RetryDelay    = 0
                ELSE
                   IF LOC:RetryTimes > 0
                      LOC:RetryTimes -= 1
                      LOC:RetryDelay  = CLOCK()
                   ELSE
                      CASE LOC:LineResult
                      OF 1
                         LOC:Print_Error_Msg  = 'Device / File Open Error'
                      OF 2
                         LOC:Print_Error_Msg  = 'Device / File Write Error'
                      OF 3
                         LOC:Print_Error_Msg  = 'Device / File Close Error'
                      OF 4
                         LOC:Print_Error_Msg  = 'File Seek Error'
                      .
                      CASE MESSAGE('There was a problem printing a line.||Error: ' & CLIP(LOC:Print_Error_Msg) & '||Cancel all printing?', 'Print_Cont', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
                      OF BUTTON:Yes
                         LOC:Result = -1
                      .
      
                      LOC:Complete  = TRUE
          .  .  .  .
      
          IF LOC:Complete = FALSE
             QuickWindow{PROP:Timer} = 10
          ELSE
             DO Print_Now_Done
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Print_Lines                 PROCEDURE()

YieldGrain      EQUATE(10)

CurrentLine     STRING(255),AUTO
ProgressValue   LONG
LineCount       LONG
!PrevQ           PreviewQueue
!Previewer       &PrintPreviewClass

window WINDOW('Printing'),AT(,,219,33),FONT('MS Sans Serif',8,,FONT:regular),CENTER,GRAY,DOUBLE
       PROGRESS,USE(ProgressValue),AT(4,17,212,14),RANGE(0,100)
       STRING('100%'),AT(200,4),USE(?String3),RIGHT
       STRING('0%'),AT(4,4),USE(?String2),LEFT
       STRING('Page Complete'),AT(85,4),USE(?String1),CENTER
     END

Report REPORT,AT(0,0,8000,8698),PRE(RPT),FONT('Arial',10,,),THOUS
Detail DETAIL,AT(,,8000,177)
         STRING(@s100),AT(0,0,8000,208),USE(CurrentLine)
       END
     END

  CODE
  OPEN(Window)
  ?ProgressValue{PROP:RangeHigh}        = RECORDS(LOC:Invoice_Q)
  OPEN(Report)
  !Report{PROP:Preview}                  = PrevQ.Filename
  !IF SELF.PrintPreview
  !  Previewer &= NEW PrintPreviewClass
  !  Previewer.Init(PrevQ)
  !END
  LOC:Idx     = 0
  LOOP
     LOC:Idx += 1
     GET(LOC:Invoice_Q, LOC:Idx)
     IF ERRORCODE()
        BREAK
     .

     IF CLIP(L_IQ:Text) = '' AND LOC:Idx > LOC:Dot_Empty_Lines    ! Only do for last few lines (was 45)
        L_IQ:Text   = '.'
     .

     CurrentLine    = SUB(L_IQ:Text, 1, L_PP:PageColumns)

     PRINT(RPT:Detail)

     ProgressValue   += 1
     DISPLAY(?ProgressValue)
     IF ~(ProgressValue%YieldGrain) THEN YIELD().
  .

  !CurrentLine    = '.'
  PRINT(RPT:Detail)

  ENDPAGE(Report)
  !Report{PROP:FlushPreview}             = CHOOSE(SELF.PrintPreview=False,True,Previewer.Display())
  CLOSE(Report)

  CLOSE(Window)

  !IF SELF.PrintPreview
  !  Previewer.Kill
  !  DISPOSE(Previewer)
  !.
  RETURN


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Prompt1, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Prompt1

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_DeliveryItems_Containers PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Options_G        GROUP,PRE(L_OG)                       !
Date_Options         GROUP,PRE()                           !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
BranchName           STRING(35)                            !Branch Name
BID                  ULONG                                 !Branch ID
BID_FirstTime        BYTE                                  !
                     END                                   !
Process:View         VIEW(TripSheetDeliveries)
                       PROJECT(TRDI:DeliveredDate)
                       PROJECT(TRDI:DeliveredTime)
                       PROJECT(TRDI:DIID)
                       JOIN(DELI:PKey_DIID,TRDI:DIID)
                         PROJECT(DELI:ContainerNo)
                         PROJECT(DELI:ItemNo)
                         PROJECT(DELI:DID)
                         JOIN(DEL:PKey_DID,DELI:DID)
                           PROJECT(DEL:BID)
                           PROJECT(DEL:DINo)
                           PROJECT(DEL:CID)
                           JOIN(CLI:PKey_CID,DEL:CID)
                             PROJECT(CLI:ClientName)
                             PROJECT(CLI:ClientNo)
                           END
                         END
                       END
                     END
FDB7::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop_Branch QUEUE                           !Queue declaration for browse/combo box using ?L_OG:BranchName
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
ProgressWindow       WINDOW('Report Delivery Items - Containers'),AT(,,281,213),FONT('MS Sans Serif',8,,FONT:regular), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       SHEET,AT(4,4,273,186),USE(?Sheet1)
                         TAB('Options'),USE(?Tab_Options)
                           PROMPT('From Date:'),AT(33,45),USE(?L_OG:From_Date:Prompt)
                           SPIN(@d6b),AT(85,45,60,10),USE(L_OG:From_Date)
                           BUTTON('...'),AT(152,45,12,10),USE(?Calendar)
                           PROMPT('To Date:'),AT(33,65),USE(?L_OG:To_Date:Prompt)
                           SPIN(@d6b),AT(85,65,60,10),USE(L_OG:To_Date)
                           BUTTON('...'),AT(152,65,12,10),USE(?Calendar:2)
                           PROMPT('Branch:'),AT(33,90),USE(?L_OG:To_Date:Prompt:2)
                           LIST,AT(85,90,105,10),USE(L_OG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop_Branch)
                         END
                         TAB('Run'),USE(?Tab_Run)
                           STRING(''),AT(70,89,141,10),USE(?Progress:UserString),CENTER
                           PROGRESS,AT(84,99,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(70,115,141,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Pause'),AT(170,194,,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                       BUTTON('Cancel'),AT(228,194,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('DeliveryItems Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Containers'),AT(0,20,7750,220),USE(?ReportTitle),FONT('MS Sans Serif',14,,FONT:regular), |
  CENTER
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(1302,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(771,354,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(3490,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(4271,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(6200,350,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         STRING('Item No.'),AT(823,385,,170),USE(?HeaderTitle:1),TRN
                         STRING('DI No.'),AT(63,385,688,167),USE(?HeaderTitle:2),TRN
                         STRING('Client No.'),AT(3531,385,688,167),USE(?HeaderTitle:3),TRN
                         STRING('Container No.'),AT(4700,390,1450,170),USE(?HeaderTitle:4),TRN
                         STRING('Delivered Date / Time'),AT(6250,390,1450,170),USE(?HeaderTitle:5),TRN
                         STRING('Client'),AT(1406,385,1450,170),USE(?HeaderTitle:6),TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(771,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         STRING(@n_10),AT(52,52),USE(DEL:DINo),RIGHT(1)
                         LINE,AT(3490,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(4271,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(6198,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         STRING(@n_5),AT(823,52,417,167),USE(DELI:ItemNo),RIGHT
                         STRING(@s100),AT(1375,52),USE(CLI:ClientName)
                         STRING(@s35),AT(4698,52,1042,156),USE(DELI:ContainerNo),LEFT
                         STRING(@n_10b),AT(3531,52),USE(CLI:ClientNo),RIGHT(1)
                         STRING(@d6b),AT(6313,52),USE(TRDI:DeliveredDate),RIGHT(1)
                         STRING(@t7b),AT(7146,52),USE(TRDI:DeliveredTime)
                         LINE,AT(1302,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass
FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop_Branch         !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Delivery Item ID' & |
      '|' & 'By Delivery ID && Item No.' & |
      '|' & 'By Commodity' & |
      '|' & 'By Container Operator' & |
      '|' & 'By Container Type' & |
      '|' & 'By Container Return Address' & |
      '|' & 'By Packaging Type ID' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_DeliveryItems_Containers')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?L_OG:From_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:BID',L_OG:BID)                                ! Added by: Report
  BIND('L_OG:From_Date',L_OG:From_Date)                    ! Added by: Report
  BIND('L_OG:To_Date',L_OG:To_Date)                        ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
      L_OG:From_Date  = GETINI('Print_DeliveryItems_Containers', 'From_Date', , GLO:Local_INI)
      L_OG:To_Date    = GETINI('Print_DeliveryItems_Containers', 'To_Date', , GLO:Local_INI)
  INIMgr.Fetch('Print_DeliveryItems_Containers',ProgressWindow) ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:TripSheetDeliveries, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Delivery Item ID')) THEN
     ThisReport.AppendOrder('+DELI:DIID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Delivery ID && Item No.')) THEN
     ThisReport.AppendOrder('+DELI:DID,+DELI:ItemNo')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Commodity')) THEN
     ThisReport.AppendOrder('+DELI:CMID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Container Operator')) THEN
     ThisReport.AppendOrder('+DELI:COID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Container Type')) THEN
     ThisReport.AppendOrder('+DELI:CTID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Container Return Address')) THEN
     ThisReport.AppendOrder('+DELI:ContainerReturnAID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Packaging Type ID')) THEN
     ThisReport.AppendOrder('+DELI:PTID')
  END
  ThisReport.SetFilter('DELI:Type = 0')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:TripSheetDeliveries.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  FDB7.Init(?L_OG:BranchName,Queue:FileDrop_Branch.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop_Branch,Relate:Branches,ThisWindow)
  FDB7.Q &= Queue:FileDrop_Branch
  FDB7.AddSortOrder(BRA:Key_BranchName)
  FDB7.AddField(BRA:BranchName,FDB7.Q.BRA:BranchName) !List box control field - type derived from field
  FDB7.AddField(BRA:BID,FDB7.Q.BRA:BID) !Primary key field - type derived from field
  FDB7.AddUpdateField(BRA:BID,L_OG:BID)
  ThisWindow.AddItem(FDB7.WindowComponent)
  FDB7.DefaultFill = 0
  !Container Listing per client and date    Report must be run by Branch/All must be Date driven. Report must have the delivered date and time. 
  !
  !
  !
  !
  !Container Listing per client and date
  !
  !
  !Date   Client     Container number     Delivery Date      Delivery tim
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_DeliveryItems_Containers',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
          PUTINI('Print_DeliveryItems_Containers', 'From_Date', L_OG:From_Date, GLO:Local_INI)
          PUTINI('Print_DeliveryItems_Containers', 'To_Date', L_OG:To_Date, GLO:Local_INI)
          ThisReport.SetFilter('')
      
          ThisReport.SetFilter('DELI:Type = 0','1')
      
          ThisReport.SetFilter('L_OG:BID = 0 OR L_OG:BID = DEL:BID', '2')
      
          IF L_OG:From_Date > 0
             ThisReport.SetFilter('TRDI:DeliveredDate > L_OG:From_Date', '3')
          .
          IF L_OG:To_Date > 0
             ThisReport.SetFilter('TRDI:DeliveredDate << L_OG:To_Date + 1', '4')
          .
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',L_OG:From_Date)
      IF Calendar5.Response = RequestCompleted THEN
      L_OG:From_Date=Calendar5.SelectedDate
      DISPLAY(?L_OG:From_Date)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',L_OG:To_Date)
      IF Calendar6.Response = RequestCompleted THEN
      L_OG:To_Date=Calendar6.SelectedDate
      DISPLAY(?L_OG:To_Date)
      END
      ThisWindow.Reset(True)
    OF ?Pause
      ThisWindow.Update()
          DISABLE(?Tab_Options)
          SELECT(?Tab_Run)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_DeliveryItems_Containers','Print_DeliveryItems_Containers','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True


FDB7.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
        Queue:FileDrop_Branch.BRA:BranchName       = 'All'
        GET(Queue:FileDrop_Branch, Queue:FileDrop_Branch.BRA:BranchName)
        IF ERRORCODE()
           CLEAR(Queue:FileDrop_Branch)
           Queue:FileDrop_Branch.BRA:BranchName    = 'All'
           Queue:FileDrop_Branch.BRA:BID           = 0
           ADD(Queue:FileDrop_Branch)
        .
    
    
        IF L_OG:BID_FirstTime = TRUE
           SELECT(?L_OG:BranchName, RECORDS(Queue:FileDrop_Branch))
    
           L_OG:BID         = 0
           L_OG:BranchName  = 'All'
        .
    
        L_OG:BID_FirstTime     = FALSE
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Debtor_Inactive PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Print            BYTE                                  !
LOC:Options          GROUP,PRE(L_OG)                       !
Before_Date          DATE                                  !
Exclude_Never_Used   BYTE                                  !
Exclude_Status1      BYTE                                  !Normal, On Hold, Closed, Dormant
Exclude_Status2      BYTE                                  !Normal, On Hold, Closed, Dormant
Exclude_Status3      BYTE                                  !Normal, On Hold, Closed, Dormant
Show                 BYTE                                  !
Used_After_Date      LONG                                  !Used after this date
                     END                                   !
LOC:Report_Fields    GROUP,PRE(L_RF)                       !
Last_Date            DATE                                  !
Show_Txt             STRING(30)                            !
                     END                                   !
LOC:Statement_Info   GROUP,PRE(L_SI)                       !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
                     END                                   !
Process:View         VIEW(Clients)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:DateOpened)
                       PROJECT(CLI:Status)
                     END
ProgressWindow       WINDOW('Report Inactive Clients'),AT(,,261,229),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       SHEET,AT(4,4,251,202),USE(?Sheet1)
                         TAB('Progress'),USE(?Tab_Progress),DISABLE
                           PROGRESS,AT(75,109,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(60,97,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(60,123,141,10),USE(?Progress:PctText),CENTER
                         END
                         TAB('Options'),USE(?Tab_Options)
                           PROMPT('Used After Date:'),AT(14,27),USE(?L_OG:Used_After_Date:Prompt:2)
                           SPIN(@d6b),AT(108,28,60,10),USE(L_OG:Used_After_Date,,?L_OG:Used_After_Date:2),RIGHT(1),MSG('Used after' & |
  ' this date'),TIP('Used after this date')
                           BUTTON('...'),AT(176,27,12,10),USE(?Calendar:2)
                           PROMPT('But not Used Since Date:'),AT(14,47),USE(?Before_Date:Prompt)
                           SPIN(@d6b),AT(108,47,60,10),USE(L_OG:Before_Date),RIGHT(1),TIP('No Deliveries since this date')
                           BUTTON('...'),AT(176,47,12,10),USE(?Calendar)
                           BUTTON('&Today'),AT(194,47,45,10),USE(?Button_Today)
                           CHECK(' &Exclude Never Used'),AT(107,84),USE(L_OG:Exclude_Never_Used),TRN
                           PROMPT('Exclude Status 1:'),AT(14,106),USE(?Exclude_Status1:Prompt)
                           LIST,AT(108,106,81,10),USE(L_OG:Exclude_Status1),DROP(5),FROM('Normal|#0|On Hold|#1|Clo' & |
  'sed|#2|Dormant|#3|None|#255'),MSG('Normal, On Hold, Closed, Dormant'),TIP('Normal, On' & |
  ' Hold, Closed, Dormant')
                           PROMPT('Exclude Status 2:'),AT(14,124),USE(?Exclude_Status2:Prompt)
                           LIST,AT(108,125,81,10),USE(L_OG:Exclude_Status2),DROP(5),FROM('Normal|#0|On Hold|#1|Clo' & |
  'sed|#2|Dormant|#3|None|#255'),MSG('Normal, On Hold, Closed, Dormant'),TIP('Normal, On' & |
  ' Hold, Closed, Dormant')
                           PROMPT('Exclude Status 3:'),AT(14,142),USE(?Exclude_Status3:Prompt)
                           LIST,AT(108,143,81,10),USE(L_OG:Exclude_Status3),DROP(5),FROM('Normal|#0|On Hold|#1|Clo' & |
  'sed|#2|Dormant|#3|None|#255'),MSG('Normal, On Hold, Closed, Dormant'),TIP('Normal, On' & |
  ' Hold, Closed, Dormant')
                           PROMPT('Show:'),AT(14,180),USE(?Show:Prompt)
                           LIST,AT(108,180,81,10),USE(L_OG:Show),DROP(5),FROM('Invoice History|#0|Account Balances|#1')
                         END
                       END
                       BUTTON('Cancel'),AT(206,210,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                       BUTTON('Pause'),AT(150,210,,15),USE(?Pause),LEFT,ICON('VCRDOWN.ICO'),FLAT
                     END

Report               REPORT('Clients Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Inactive Clients'),AT(2708,52,2344,260),USE(?ReportTitle),FONT('Arial',14,,FONT:regular), |
  CENTER
                         STRING('Client with no Deliveries since:'),AT(63,156),USE(?String25),TRN
                         STRING(@s30),AT(5771,156),USE(L_RF:Show_Txt),RIGHT(1)
                         STRING(@d6b),AT(1594,156),USE(L_OG:Before_Date)
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(594,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2594,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(3219,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(3875,350,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(4843,350,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(5812,350,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(6781,350,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         STRING('Client No.'),AT(50,390,,170),USE(?HeaderTitle:1),TRN
                         STRING('Client Name'),AT(656,385,868,170),USE(?HeaderTitle:2),TRN
                         STRING('Opened'),AT(2615,385,583,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('Last Used'),AT(3240,385,583,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Last Month'),AT(3925,390,868,170),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('1 Month Ago'),AT(4893,390,868,170),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('2 Months Ago'),AT(5862,390,868,170),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('3 Months or More'),AT(6831,390,868,170),USE(?HeaderTitle:8),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(594,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(2594,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(3219,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(3875,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(4843,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(5812,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(6781,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                         STRING(@n_10b),AT(52,52,479,167),USE(CLI:ClientNo),RIGHT
                         STRING(@s100),AT(677,52,1875,156),USE(CLI:ClientName),LEFT
                         STRING(@d5b),AT(2615,52,,170),USE(CLI:DateOpened),RIGHT(1)
                         STRING(@d5b),AT(3240,52,,170),USE(L_RF:Last_Date),RIGHT(1)
                         STRING(@n-17.2),AT(3925,50,868,170),USE(L_SI:Current),RIGHT(1)
                         STRING(@n-17.2),AT(4893,50,868,170),USE(L_SI:Days30),RIGHT(1)
                         STRING(@n-17.2),AT(5862,50,868,170),USE(L_SI:Days60),RIGHT(1)
                         STRING(@n-17.2),AT(6831,50,868,170),USE(L_SI:Days90),RIGHT(1)
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Client Name' & |
      '|' & 'By Client No.' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Debtor_Inactive')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
      L_OG:Before_Date        = GETINI('Print_Debtor_Inactive', 'Before_Date', , GLO:Local_INI)
      L_OG:Exclude_Never_Used = GETINI('Print_Debtor_Inactive', 'Exclude_Never_Used', 1, GLO:Local_INI)
  
      L_OG:Exclude_Status1    = GETINI('Print_Debtor_Inactive', 'Exclude_Status1', 255, GLO:Local_INI)
      L_OG:Exclude_Status2    = GETINI('Print_Debtor_Inactive', 'Exclude_Status2', 255, GLO:Local_INI)
      L_OG:Exclude_Status3    = GETINI('Print_Debtor_Inactive', 'Exclude_Status3', 255, GLO:Local_INI)
  
  
      L_OG:Used_After_Date    = GETINI('Print_Debtor_Inactive', 'Used_After_Date', , GLO:Local_INI)
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Clients, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client Name')) THEN
     ThisReport.AppendOrder('+CLI:ClientName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientNo')
  END
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Clients.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
    Relate:_SQLTemp.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
        IF L_OG:Used_After_Date <= 0 
          SELECT(?L_OG:Used_After_Date:2)
          CYCLE
        .
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',L_OG:Used_After_Date)
      IF Calendar6.Response = RequestCompleted THEN
      L_OG:Used_After_Date=Calendar6.SelectedDate
      DISPLAY(?L_OG:Used_After_Date:2)
      END
      ThisWindow.Reset(True)
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',L_OG:Before_Date)
      IF Calendar5.Response = RequestCompleted THEN
      L_OG:Before_Date=Calendar5.SelectedDate
      DISPLAY(?L_OG:Before_Date)
      END
      ThisWindow.Reset(True)
    OF ?Button_Today
      ThisWindow.Update()
          L_OG:Before_Date    = TODAY()
          DISPLAY(?L_OG:Before_Date)
    OF ?Pause
      ThisWindow.Update()
          ENABLE(?Tab_Progress)
          DISABLE(?Tab_Options)
          PUTINI('Print_Debtor_Inactive', 'Before_Date', L_OG:Before_Date, GLO:Local_INI)
          PUTINI('Print_Debtor_Inactive', 'Exclude_Never_Used', L_OG:Exclude_Never_Used, GLO:Local_INI)
      
          PUTINI('Print_Debtor_Inactive', 'Exclude_Status1', L_OG:Exclude_Status1, GLO:Local_INI)
          PUTINI('Print_Debtor_Inactive', 'Exclude_Status2', L_OG:Exclude_Status2, GLO:Local_INI)
          PUTINI('Print_Debtor_Inactive', 'Exclude_Status3', L_OG:Exclude_Status3, GLO:Local_INI)
      
          PUTINI('Print_Debtor_Inactive', 'Used_After_Date', L_OG:Used_After_Date, GLO:Local_INI)
          EXECUTE L_OG:Show + 1
             L_RF:Show_Txt    = 'Invoice History'
             L_RF:Show_Txt    = 'Account Balances'
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      LOC:Print   = TRUE
  
      CLEAR(LOC:Statement_Info)
      CLEAR(L_RF:Last_Date)
  
      _SQLTemp{PROP:SQL}  = 'SELECT TOP 1 DID, DINo, DIDateAndTime FROM Deliveries WHERE CID = ' & CLI:CID & | 
                            ' AND DIDateAndTime >= ' & SQL_Get_DateT_G(L_OG:Used_After_Date) & |
                            ' ORDER BY DIDateAndTime DESC'
      IF ERRORCODE()
         MESSAGE('SQL Problem||Error: ' & ERROR() & '||F Err: ' & FILEERROR(), 'Print_Debtor_Inactive', ICON:Hand)
      ELSE
         NEXT(_SQLTemp)
         IF ERRORCODE()
            ! No DI?
            IF L_OG:Exclude_Never_Used = TRUE
               LOC:Print   = FALSE
            .
         ELSE
            IF DEFORMAT(_SQ:S1) = 0
               ! No DI?
               IF L_OG:Exclude_Never_Used = TRUE
                  LOC:Print   = FALSE
               .
            ELSE
               L_RF:Last_Date = _SQ:SDate
  
               IF L_RF:Last_Date < L_OG:Before_Date
                  ! Inactive
               ELSE
                  LOC:Print   = FALSE
      .  .  .  .
  
  
      IF LOC:Print = TRUE
         IF L_OG:Exclude_Status1 ~= 255 AND L_OG:Exclude_Status1 = CLI:Status
            LOC:Print   = FALSE
         .
         IF L_OG:Exclude_Status2 ~= 255 AND L_OG:Exclude_Status2 = CLI:Status
            LOC:Print   = FALSE
         .
         IF L_OG:Exclude_Status3 ~= 255 AND L_OG:Exclude_Status3 = CLI:Status
            LOC:Print   = FALSE
         .
      .
    IF LOC:Print = TRUE
       EXECUTE L_OG:Show + 1
          Get_Client_Invoice_History(CLI:CID, TODAY(), , LOC:Statement_Info)
          Gen_Statement(0, CLI:CID, TODAY(), CLOCK(),, TRUE, LOC:Statement_Info)
    .  .

!    Get_Client_Invoice_History
!    (ULONG, LONG=0, BYTE=0, *STRING),LONG,PROC
!    (p:CID, p:Date, p:Option, p_Bal_Group)
    !   p:Option
    !       0   - Total
    !       1   - VAT
    !       2   - Freight
    !       3   - Additional
    !       4   - Fuel
    !       5   - Doc
    !       6   - Insurance

!   Gen_Statement
!   (ULONG, ULONG, LONG, LONG, <*DECIMAL>, BYTE=0, <*STRING>, BYTE=0, BYTE=0, BYTE=0),LONG,PROC
!   (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
  IF LOC:Print
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Debtor_Inactive','Print_Debtor_Inactive','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

