

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('abbreak.inc'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS005.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_ClientsRateTypes PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Locals           GROUP,PRE(LO)                         ! 
Type                 STRING(30)                            ! 
Special              STRING(30)                            ! 
                     END                                   ! 
Process:View         VIEW(ClientsRateTypes)
                       PROJECT(CRT:ClientRateType)
                       PROJECT(CRT:LTID)
                       JOIN(LOAD2:PKey_LTID,CRT:LTID)
                       END
                     END
ProgressWindow       WINDOW('Report Clients Rate Types'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('LoadTypes Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Load Types Listing'),AT(0,20,7750),USE(?ReportTitle),FONT('Arial',14,,FONT:bold),CENTER
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(3708,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(6250,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         STRING('Load Type'),AT(50,390,1837,170),USE(?HeaderTitle:1),TRN
                         STRING('Container Option'),AT(3823,385,1302,167),USE(?HeaderTitle:2),TRN
                         STRING('Full Load / Container Park'),AT(6365,385,,170),USE(?HeaderTitle:4),TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(3708,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(6250,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         STRING(@s30),AT(6406,52,1260,167),USE(LO:Special)
                         STRING(@s30),AT(3771,52,2260,167),USE(LO:Type)
                         STRING(@s80),AT(52,52,3552,167),USE(CRT:ClientRateType),LEFT
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Load Type' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_ClientsRateTypes')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:ClientsRateTypes.Open                             ! File ClientsRateTypes used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_ClientsRateTypes',ProgressWindow)    ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:ClientsRateTypes, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Load Type')) THEN
     ThisReport.AppendOrder('+CRT:ClientRateType')
  END
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:ClientsRateTypes.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ClientsRateTypes.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_ClientsRateTypes',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
    ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery

    EXECUTE LOAD2:LoadOption + 1
       LO:Type          = 'Consolidated'
       LO:Type          = 'Container Park'
       LO:Type          = 'Container'
       LO:Type          = 'Full Load'
       LO:Type          = 'Empty Container'
       LO:Type          = 'Local Deliveries'
    .


    CLEAR(LO:Special)
    IF LOAD2:ContainerParkStandard = TRUE
       LO:Special       = 'CP Std'
    .
    IF LOAD2:Hazchem = TRUE
       IF CLIP(LO:Special) = ''
          LO:Special    = 'Hazchem'
       ELSE
          LO:Special    = CLIP(LO:Special) & ' / Hazchem'
    .  .

  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_ClientsRateTypes','Print_ClientsRateTypes','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Whats_Where PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Manifested       STRING(30)                            ! Manifested status - this has maintenance function - Not Manifested, Partially Manifested, Partially Manifested Multiple, Fully Manifested, Fully Manifested Multiple
LOC:Delivered        STRING(20)                            ! Delivered Status - this has maintenance function - Not Delivered, Partially Delivered, Delivered
Process:View         VIEW(Deliveries)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:Delivered)
                       PROJECT(DEL:FID)
                       PROJECT(DEL:Manifested)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:SID)
                       PROJECT(DEL:CRTID)
                       PROJECT(DEL:BID)
                       PROJECT(DEL:CID)
                       JOIN(DELI:FKey_DID_ItemNo,DEL:DID)
                         PROJECT(DELI:ItemNo)
                         PROJECT(DELI:Units)
                         PROJECT(DELI:Volume)
                         PROJECT(DELI:Weight)
                         PROJECT(DELI:PTID)
                         PROJECT(DELI:CMID)
                         JOIN(PACK:PKey_PTID,DELI:PTID)
                         END
                         JOIN(COM:PKey_CMID,DELI:CMID)
                           PROJECT(COM:Commodity)
                         END
                       END
                       JOIN(SERI:PKey_SID,DEL:SID)
                         PROJECT(SERI:ServiceRequirement)
                       END
                       JOIN(CRT:PKey_CRTID,DEL:CRTID)
                         PROJECT(CRT:ClientRateType)
                       END
                       JOIN(BRA:PKey_BID,DEL:BID)
                       END
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientName)
                       END
                       JOIN(FLO:PKey_FID,DEL:FID)
                         PROJECT(FLO:FBNFloor)
                         PROJECT(FLO:Floor)
                       END
                     END
ProgressWindow       WINDOW('Report Deliveries'),AT(,,216,144),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(53,42,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(37,32,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(37,58,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Pause'),AT(110,126,49,15),USE(?Pause),LEFT,ICON('VCRDOWN.ICO'),FLAT
                       BUTTON('Cancel'),AT(162,126,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Deliveries Report'),AT(250,854,8000,9646),PRE(RPT),PAPER(PAPER:LETTER),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,8000,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('What''s Where'),AT(0,52,8000),USE(?ReportTitle),FONT(,14,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER
                         STRING('Service Requirements'),AT(4781,396,1083,167),USE(?String26),TRN
                         STRING('DI No.'),AT(63,396,521,167),USE(?HeaderTitle:1),TRN
                         STRING('DI Date'),AT(698,396,729,167),USE(?HeaderTitle:2),TRN
                         STRING('Manifested'),AT(6146,396,,170),USE(?HeaderTitle:3),TRN
                         STRING('Delivered'),AT(7125,396,542,167),USE(?HeaderTitle:4),TRN
                         STRING('Load Type'),AT(3271,396),USE(?String26:2),TRN
                         STRING('Client'),AT(1490,396),USE(?String26:3),TRN
                         BOX,AT(0,350,8000,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(635,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(1438,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(3208,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(4698,354,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(6083,354,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(7094,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                       END
break_floor            BREAK(DEL:FID)
                         HEADER,AT(0,0,,385),PAGEBEFORE(1)
                           STRING(@s35),AT(1156,42,4229,281),USE(FLO:Floor),FONT(,14,COLOR:Black,FONT:bold,CHARSET:ANSI)
                           CHECK('FBN Floor'),AT(5823,42,1333,281),USE(FLO:FBNFloor)
                           STRING('Floor:'),AT(542,42,510,281),USE(?String16),FONT(,14,COLOR:Black,,CHARSET:ANSI),TRN
                         END
break_delivery           BREAK(DELI:DID)
                           HEADER,AT(0,0,,260)
                             LINE,AT(0,0,8000,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                             LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                             LINE,AT(635,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                             LINE,AT(1438,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                             LINE,AT(3208,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                             LINE,AT(4698,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                             LINE,AT(6083,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                             LINE,AT(7094,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                             LINE,AT(8000,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                             LINE,AT(0,250,8000,0),USE(?DetailEndLine2),COLOR(COLOR:Black)
                             STRING(@s20),AT(7125,42,844,167),USE(LOC:Delivered)
                             STRING(@s35),AT(1490,42,1667,167),USE(CLI:ClientName)
                             STRING(@s35),AT(3271,42,1385,167),USE(CRT:ClientRateType)
                             STRING(@s35),AT(4781,42,1083,167),USE(SERI:ServiceRequirement)
                             STRING(@s30),AT(6146,42,917,167),USE(LOC:Manifested)
                             STRING(@n_10),AT(-104,42,,167),USE(DEL:DINo),RIGHT(1),TRN
                             STRING(@d6),AT(698,42,,167),USE(DEL:DIDate),LEFT
                           END
Detail                     DETAIL,AT(,,8000,250),USE(?Detail)
                             STRING(@s35),AT(1667,21,1240,167),USE(COM:Commodity)
                             STRING('Item No.:'),AT(94,21),USE(?String22),TRN
                             STRING('Weight:'),AT(6885,21),USE(?String20),TRN
                             STRING(@n6),AT(531,21),USE(DELI:ItemNo)
                             STRING('Commodity:'),AT(1021,21),USE(?String22:2),TRN
                             STRING(@n-11.2),AT(7271,21),USE(DELI:Weight),RIGHT(1)
                             STRING('Items:'),AT(4833,21),USE(?String20:3),TRN
                             STRING(@n6),AT(5167,21),USE(DELI:Units)
                             STRING('Volume:'),AT(5708,21),USE(?String20:2),TRN
                             STRING(@n-11.2),AT(6104,21),USE(DELI:Volume),RIGHT(1)
                           END
                           FOOTER,AT(0,0,,104)
                           END
                         END
                       END
                       FOOTER,AT(250,10500,8000,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,8000,10500),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,8000,10500),USE(?FormImage),TILED
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Whats_Where')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:Deliveries, ?Progress:PctText, Progress:Thermometer, ProgressMgr, DEL:FID)
  ThisReport.AddSortOrder(DEL:FKey_FID)
  ThisReport.AppendOrder('+CLI:ClientName,+DEL:DINo,+DELI:ItemNo')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Deliveries.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      ! Not Manifested|Partially Manifested|Partially Manifested Multiple|Fully Manifested|Fully Manifested Multiple
      EXECUTE DEL:Manifested
         LOC:Manifested   = 'Partially Manifested'
         LOC:Manifested   = 'Partially Manifested Multiple'
         LOC:Manifested   = 'Fully Manifested'
         LOC:Manifested   = 'Fully Manifested Multiple'
      ELSE
         LOC:Manifested   = 'Not Manifested'
      .
  
      ! Not Delivered|Partially Delivered|Delivered
      EXECUTE DEL:Delivered
         LOC:Delivered    = 'Partially Delivered'
         LOC:Delivered    = 'Delivered'
      ELSE
         LOC:Delivered    = 'Not Delivered'
      .
  
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Whats_Where','Print_Whats_Where','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Creditor_Manifests PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Report_Group     GROUP,PRE(L_RG)                       ! 
Turnover             DECIMAL(12,2)                         ! 
Extra_Leg            DECIMAL(12,2)                         ! 
VAT                  DECIMAL(12,2)                         ! 
                     END                                   ! 
LOC:Options          GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
Process:View         VIEW(Manifest)
                       PROJECT(MAN:Broking)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:CreatedDate)
                       PROJECT(MAN:MID)
                       PROJECT(MAN:TID)
                       PROJECT(MAN:VATRate)
                     END
ProgressWindow       WINDOW('Report Manifest'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Manifest Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         LINE,AT(0,0,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         STRING('Manifests'),AT(0,20,4844),USE(?ReportTitle),FONT(,14,,FONT:regular),CENTER
                         STRING(@d5b),AT(5417,104),USE(LO:From_Date),RIGHT(1)
                         STRING('-    '),AT(6198,104,208,156),USE(?String24),TRN
                         STRING(@d5b),AT(6615,104),USE(LO:To_Date),RIGHT(1)
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(833,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1719,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(2802,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(4677,365,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(5740,365,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(6833,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(3656,365,0,250),USE(?HeaderLine:8),COLOR(COLOR:Black)
                         STRING('Cost'),AT(3760,396,833,167),USE(?HeaderTitle:8),CENTER,TRN
                         STRING('Transporter ID'),AT(2896,396,688,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Extra Legs'),AT(5844,396,896,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Created Date'),AT(52,396,729,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('MID'),AT(958,396,688,167),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Turnover'),AT(1823,396,896,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('VAT'),AT(4771,396,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Broking'),AT(6979,396,656,167),USE(?HeaderTitle:5),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(833,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(1719,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(2802,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(4677,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(5740,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         STRING(@d6),AT(50,50,,170),USE(MAN:CreatedDate),LEFT
                         STRING(@n_10),AT(958,52,,170),USE(MAN:MID),RIGHT
                         STRING(@n-15.2),AT(1823,52),USE(L_RG:Turnover),RIGHT(1)
                         STRING(@n_10),AT(2896,52,,170),USE(MAN:TID),RIGHT
                         STRING(@n-14.2),AT(3760,52,,170),USE(MAN:Cost),RIGHT(1),TRN
                         STRING(@n-15.2),AT(4771,52,,170),USE(L_RG:VAT),RIGHT(1)
                         STRING(@n-15.2),AT(5844,52),USE(L_RG:Extra_Leg),RIGHT(1)
                         LINE,AT(6833,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                         CHECK(' Broking'),AT(6979,52,,170),USE(MAN:Broking)
                         LINE,AT(3656,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
totals                 DETAIL,AT(,,,635),USE(?totals)
                         LINE,AT(208,135,6719,0),USE(?Line19),COLOR(COLOR:Black)
                         STRING('Totals:'),AT(313,219),USE(?String29),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@n-15.2),AT(5844,219),USE(L_RG:Extra_Leg,,?L_RG:Extra_Leg:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         LINE,AT(208,448,6719,0),USE(?Line19:2),COLOR(COLOR:Black)
                         STRING(@n-17.2),AT(1729,219),USE(L_RG:Turnover,,?L_RG:Turnover:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(3760,219,,170),USE(MAN:Cost,,?MAN:Cost:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-15.2),AT(4771,219,,170),USE(L_RG:VAT,,?L_RG:VAT:2),RIGHT(1),SUM,TALLY(Detail),TRN
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Created Date' & |
      '|' & 'By Manifest ID' & |
      '|' & 'By Branch & Date' & |
      '|' & 'By Transporter & Date' & |
      '|' & 'By Vehicle Composition & Date' & |
      '|' & 'By Journey & Date' & |
      '|' & 'By Driver' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
        PRINT(RPT:totals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Creditor_Manifests')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Manifest.Open                                     ! File Manifest used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Creditor_Manifests',ProgressWindow)  ! Restore window settings from non-volatile store
      LO:From_Date    = GETINI('Print_Creditor_Manifests', 'From_Date', '', GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Creditor_Manifests', 'To_Date', '', GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      IF LO:To_Date > 0
         PUTINI('Print_Creditor_Manifests', 'From_Date', LO:From_Date, GLO:Local_INI)
         PUTINI('Print_Creditor_Manifests', 'To_Date', LO:To_Date, GLO:Local_INI)
      ELSE
         ReturnValue  = LEVEL:Fatal
      .
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Manifest, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Created Date')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Manifest ID')) THEN
     ThisReport.AppendOrder('+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch & Date')) THEN
     ThisReport.AppendOrder('+MAN:BID,+MAN:CreatedDate,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter & Date')) THEN
     ThisReport.AppendOrder('+MAN:TID,+MAN:CreatedDate,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Vehicle Composition & Date')) THEN
     ThisReport.AppendOrder('+MAN:VCID,+MAN:CreatedDate,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Journey & Date')) THEN
     ThisReport.AppendOrder('+MAN:JID,+MAN:CreatedDate,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Driver')) THEN
     ThisReport.AppendOrder('+MAN:DRID,+MAN:CreatedDate,+MAN:MID')
  END
  ThisReport.SetFilter('MAN:CreatedDate >= LO:From_Date AND MAN:CreatedDate << (LO:To_Date + 1)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Manifest.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Manifest.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Creditor_Manifests',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      L_RG:VAT                    = MAN:Cost * (MAN:VATRate / 100)
  
      L_RG:Turnover               = Get_Manifest_Info(MAN:MID, 4)                         ! 4 is VAT incl
      L_RG:Extra_Leg              = Get_Manifest_Info(MAN:MID, 6)                         ! VAT is included (extra legs)
  
  
  !    L_MT:Legs_Cost_VAT          = L_MT:Legs_Cost - Get_Manifest_Info(MAL:MID, 3)        ! excluded (3), so minus ex to get VAT
  !    L_MT:Delivery_Charges_Ex    = Get_Manifest_Info(MAL:MID, 0)                         ! 4 is VAT EXCL
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Creditor_Manifests','Print_Creditor_Manifests','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!!  
!!! </summary>
Print_Creditor_Invoices PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Options          GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
LOC:Report_Group     GROUP,PRE(L_RG)                       ! 
Total                DECIMAL(12,2)                         ! 
Owing                DECIMAL(11,2)                         ! Owing amount
ReportComplete       BYTE                                  ! 
                     END                                   ! 
LOC:Options_Extra    GROUP,PRE(L_OE)                       ! 
Run_Type             BYTE                                  ! 
Payment              BYTE(255)                             ! Payment options
BranchName           STRING(35)                            ! Branch Name
BID                  ULONG                                 ! Branch ID
BID_FirstTime        BYTE(1)                               ! 
                     END                                   ! 
LOC:Break_Totals     GROUP,PRE(L_BT)                       ! 
Count                ULONG                                 ! 
Cost                 DECIMAL(12,2)                         ! 
VAT                  DECIMAL(12,2)                         ! VAT amount
Total                DECIMAL(12,2)                         ! 
                     END                                   ! 
Process:View         VIEW(_InvoiceTransporter)
                       PROJECT(INT:BID)
                       PROJECT(INT:Broking)
                       PROJECT(INT:CR_TIN)
                       PROJECT(INT:Cost)
                       PROJECT(INT:InvoiceDate)
                       PROJECT(INT:MID)
                       PROJECT(INT:Status)
                       PROJECT(INT:TID)
                       PROJECT(INT:TIN)
                       PROJECT(INT:VAT)
                       JOIN(TRA:PKey_TID,INT:TID)
                         PROJECT(TRA:TransporterName)
                       END
                       JOIN(BRA:PKey_BID,INT:BID)
                         PROJECT(BRA:BranchName)
                       END
                     END
FDB5::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop_Branch QUEUE                           !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
ProgressWindow       WINDOW('Report Invoice Transporter'),AT(,,210,139),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,MDI,TIMER(1)
                       SHEET,AT(4,4,203,114),USE(?Sheet1)
                         TAB('Process'),USE(?Tab1),HIDE
                           PROGRESS,AT(51,57,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(35,43,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(35,74,141,10),USE(?Progress:PctText),CENTER
                         END
                         TAB('Options'),USE(?Tab2)
                           PROMPT('Run Type:'),AT(13,26),USE(?L_OE:Run_Type:Prompt)
                           LIST,AT(77,26,122,10),USE(L_OE:Run_Type),VSCROLL,DROP(15),FROM('All|#0|Invoices  (MID)|' & |
  '#1|Additionals  (MID & Adjustment Invoice No.)|#2|Office Gen.  (MID)|#3|Credit Notes' & |
  '|#4|Extra Invoices (flag)|#5')
                           GROUP,AT(11,53,188,10),USE(?Group1)
                             PROMPT('Payment:'),AT(13,53),USE(?L_OE:Payment:Prompt)
                             LIST,AT(77,53,122,10),USE(L_OE:Payment),VSCROLL,DROP(15),FROM('All|#255|No Payments|#0|' & |
  'Partially Paid|#1|Amount Owing|#250|Fully Paid|#3'),MSG('Payment options'),TIP('Payment options')
                           END
                           PROMPT('Branch Name:'),AT(13,82),USE(?BranchName:Prompt:2)
                           LIST,AT(77,82,122,10),USE(L_OE:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop_Branch)
                         END
                       END
                       BUTTON('Cancel'),AT(158,122,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                       BUTTON('Pause'),AT(102,122,,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                     END

Report               REPORT('_InvoiceTransporter Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Transporter Invoices'),AT(0,52,4844,260),USE(?ReportTitle),FONT(,14,,FONT:regular), |
  CENTER
                         STRING(@d5b),AT(5469,104),USE(LO:From_Date),RIGHT(1)
                         STRING(' - '),AT(6177,104,156,156),USE(?String24),TRN
                         STRING(@d5b),AT(6458,104),USE(LO:To_Date),RIGHT(1)
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(711,350,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1354,365,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(2948,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(3688,350,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(4521,350,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(5417,350,0,250),USE(?HeaderLine:8),COLOR(COLOR:Black)
                         LINE,AT(6344,350,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         LINE,AT(7000,350,0,250),USE(?HeaderLine:71),COLOR(COLOR:Black)
                         STRING('Inv. Date'),AT(52,406,583,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Total'),AT(5385,406,896,167),USE(?HeaderTitle:8),CENTER,TRN
                         STRING('Branch'),AT(6365,406,552,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Transporter'),AT(1406,406,1510,156),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('MID'),AT(2958,406,688,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('Cost'),AT(3646,406,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('VAT'),AT(4531,406,833,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('Broking'),AT(7031,406,656,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('TIN'),AT(688,406,688,167),USE(?HeaderTitle:10),CENTER,TRN
                       END
Breaktotals            DETAIL,AT(,,,396),USE(?breaktotals)
                         LINE,AT(625,52,5781,0),USE(?Line18:3),COLOR(COLOR:Black)
                         STRING('Sub Total:'),AT(52,104),USE(?String29),TRN
                         STRING(@n13),AT(646,104,688,167),USE(L_BT:Count),RIGHT,TRN
                         STRING(@n-17.2),AT(3521,104,958,167),USE(L_BT:Cost),RIGHT,TRN
                         STRING(@n-17.2),AT(4406,104,958,167),USE(L_BT:VAT),RIGHT(1),TRN
                         STRING(@n-17.2),AT(5292,104),USE(L_BT:Total),RIGHT(1),TRN
                         LINE,AT(625,313,5781,0),USE(?Line18:2),COLOR(COLOR:Black)
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(711,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(1354,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(2948,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(3688,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(4521,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                         LINE,AT(5417,0,0,250),USE(?DetailLine:9),COLOR(COLOR:Black)
                         LINE,AT(6344,0,0,250),USE(?DetailLine:10),COLOR(COLOR:Black)
                         LINE,AT(7000,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         STRING(@d5b),AT(52,52,,167),USE(INT:InvoiceDate),RIGHT(2)
                         STRING(@n_10),AT(646,52),USE(INT:TIN),RIGHT(1),TRN
                         STRING(@s35),AT(1406,52,1510,156),USE(TRA:TransporterName),LEFT(1),TRN
                         STRING(@n_10),AT(2958,52,688,167),USE(INT:MID),RIGHT,TRN
                         STRING(@n-14.2),AT(3646,52,833,167),USE(INT:Cost),RIGHT,TRN
                         STRING(@n-14.2),AT(4531,52,833,167),USE(INT:VAT),RIGHT(1),TRN
                         STRING(@n-15.2),AT(5385,52),USE(L_RG:Total),RIGHT(1),TRN
                         STRING(@s8),AT(6406,52,,167),USE(BRA:BranchName)
                         CHECK('Broking'),AT(7083,52,,167),USE(INT:Broking)
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
totals                 DETAIL,AT(,,,396),USE(?totals),FONT(,,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(625,52,5781,0),USE(?Line18),COLOR(COLOR:Black)
                         STRING('Totals:'),AT(52,104),USE(?String29),TRN
                         STRING(@n_10),AT(646,104,688,167),USE(INT:TID,,?INT:TID:2),RIGHT,CNT,TALLY(Detail),TRN
                         STRING(@n-16.2),AT(3521,104,958,167),USE(INT:Cost,,?INT:Cost:2),RIGHT,SUM,TALLY(Detail),TRN
                         STRING(@n-16.2),AT(4406,104,958,167),USE(INT:VAT,,?INT:VAT:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-16.2),AT(5323,104),USE(L_RG:Total,,?L_RG:Total:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         LINE,AT(625,313,5781,0),USE(?Line18:2),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6948,52,698,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
BreakMgr             CLASS(BreakManagerClass)              ! Break Manager
TakeEnd                PROCEDURE(SHORT BreakId,SHORT LevelId),DERIVED
                     END

TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

FDB5                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop_Branch         !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Invoice Date' & |
      '|' & 'By Transporter Invoice No.' & |
      '|' & 'By Transporter & Date' & |
      '|' & 'By Branch & Date' & |
      '|' & 'By Manifest & Date' & |
      '|' & 'By User & Date' & |
      '|' & 'By Delivery & Date (extra legs)' & |
      '|' & 'By Credited Invoice & Date' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
  !    PRINT(RPT:totals)
  
      L_RG:ReportComplete = 1
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Creditor_Invoices')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  BIND('L_OE:BID',L_OE:BID)                                ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BreakMgr.Init()
  BreakMgr.AddBreak()
  BreakMgr.AddLevel() !TransporterFooter
  BreakMgr.AddResetField(INT:TID)
  BreakMgr.AddTotal(L_BT:Count,1)
  BreakMgr.AddTotal(L_BT:Cost,INT:Cost,eBreakTotalSum,1)
  BreakMgr.AddTotal(L_BT:VAT,INT:VAT,eBreakTotalSum,1)
  BreakMgr.AddTotal(L_BT:Total,L_RG:Total,eBreakTotalSum,1)
  BreakMgr.AddBreak()
  BreakMgr.AddLevel() !ReportComplete
  BreakMgr.AddResetField(L_RG:ReportComplete)
  BreakMgr.AddHotField(INT:Cost)
  BreakMgr.AddHotField(INT:TID)
  BreakMgr.AddHotField(INT:VAT)
  BreakMgr.AddHotField(L_RG:Total)
  SELF.AddItem(BreakMgr)
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Creditor_Invoices',ProgressWindow)   ! Restore window settings from non-volatile store
      LO:From_Date    = GETINI('Print_Creditor_Invoices', 'From_Date', '', GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Creditor_Invoices', 'To_Date', '', GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      IF LO:To_Date > 0
         PUTINI('Print_Creditor_Invoices', 'From_Date', LO:From_Date, GLO:Local_INI)
         PUTINI('Print_Creditor_Invoices', 'To_Date', LO:To_Date, GLO:Local_INI)
      ELSE
         ReturnValue  = LEVEL:Fatal
      .
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:_InvoiceTransporter, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice Date')) THEN
     ThisReport.AppendOrder('+INT:InvoiceDate')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter Invoice No.')) THEN
     ThisReport.AppendOrder('+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter & Date')) THEN
     ThisReport.AppendOrder('+INT:TID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch & Date')) THEN
     ThisReport.AppendOrder('+INT:BID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Manifest & Date')) THEN
     ThisReport.AppendOrder('+INT:MID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By User & Date')) THEN
     ThisReport.AppendOrder('+INT:UID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Delivery & Date (extra legs)')) THEN
     ThisReport.AppendOrder('+INT:DID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Credited Invoice & Date')) THEN
     ThisReport.AppendOrder('+INT:CR_TIN,+INT:InvoiceDate,+INT:TIN')
  END
  ThisReport.SetFilter('INT:InvoiceDate >= LO:From_Date AND INT:InvoiceDate << (LO:To_Date + 1) AND (L_OE:BID = 0 OR L_OE:BID = INT:BID)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_InvoiceTransporter.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  FDB5.Init(?L_OE:BranchName,Queue:FileDrop_Branch.ViewPosition,FDB5::View:FileDrop,Queue:FileDrop_Branch,Relate:Branches,ThisWindow)
  FDB5.Q &= Queue:FileDrop_Branch
  FDB5.AddSortOrder(BRA:Key_BranchName)
  FDB5.AddField(BRA:BranchName,FDB5.Q.BRA:BranchName) !List box control field - type derived from field
  FDB5.AddField(BRA:BID,FDB5.Q.BRA:BID) !Primary key field - type derived from field
  FDB5.AddUpdateField(BRA:BID,L_OE:BID)
  ThisWindow.AddItem(FDB5.WindowComponent)
  FDB5.DefaultFill = 0
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Creditor_Invoices',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_OE:Run_Type
          CASE L_OE:Run_Type + 1
          OF 1            ! All
             ENABLE(?Group1)
          OF 2            ! Invoices
             ENABLE(?Group1)
          OF 3            ! Additionals
             ENABLE(?Group1)
          OF 4            ! Office Gen.
             ENABLE(?Group1)
          OF 5            ! Credit Notes
             DISABLE(?Group1)
          OF 6            ! Extra Invoices (flag)
             ENABLE(?Group1)
          ELSE
             DISABLE(?Group1)
          .
    OF ?Pause
      ThisWindow.Update()
          UNHIDE(?Tab1)
          HIDE(?Tab2)
      
      
          ! 'All|#0|Invoices|#1|Additionals|#2|Office Gen.|#3|Credit Notes|#4'
      
          CASE L_OE:Run_Type + 1
          OF 1            ! All
          OF 2            ! Invoices
             ThisReport.SetFilter('INT:Cost >= 0.0 AND INT:MID <> 0 AND (INT:CR_TIN = 0 OR SQL(CR_TIN IS NULL))', 'ikb')
          OF 3            ! Additionals
             ThisReport.SetFilter('INT:MID <> 0 AND INT:CR_TIN <> 0', 'ikb')
          OF 4            ! Office Gen.
             ThisReport.SetFilter('INT:MID = 0', 'ikb')
          OF 5            ! Credit Notes
             ThisReport.SetFilter('INT:Cost < 0.0', 'ikb')
          OF 6            ! Extra Invoices
             ThisReport.SetFilter('INT:ExtraInv = 1', 'ikb')
          .
      
      
          ! All|No Payments|Partially Paid|Amount Owing|Fully Paid
          ! 255|0|1|250|3
      
          CASE L_OE:Payment
          OF 255
          OF 250
             ThisReport.SetFilter('INT:Status <= 1', 'ikb2')
          OF 0
             ThisReport.SetFilter('INT:Status = 0', 'ikb2')
          OF 1
             ThisReport.SetFilter('INT:Status = 1', 'ikb2')
          OF 3
             ThisReport.SetFilter('INT:Status = 3', 'ikb2')
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      L_RG:Total      = INT:Cost + INT:VAT
  
  
      L_RG:Owing      = Get_InvTransporter_Outstanding(INT:TIN)
  ReturnValue = PARENT.TakeRecord()
    IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter Invoice No.')) OR |
            (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter & Date'))
  !     PRINT(RPT:totals)
    .
  IF 0
    PRINT(RPT:Breaktotals)
  END
  IF 0
    PRINT(RPT:totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


BreakMgr.TakeEnd PROCEDURE(SHORT BreakId,SHORT LevelId)

  CODE
     CASE BreakId
     OF 1
        CASE LevelId
        OF 1 !TransporterFooter
           L_BT:Total   = L_BT:Cost + L_BT:VAT
           
           IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter Invoice No.')) OR |
                   (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter & Date'))
           
           PRINT(RPT:Breaktotals)
           END
        END
     OF 2
        CASE LevelId
        OF 1 !ReportComplete
           PRINT(RPT:totals)
        END
     END
  PARENT.TakeEnd(BreakId,LevelId)


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Creditor_Invoices','Print_Creditor_Invoices','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True


FDB5.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
        Queue:FileDrop_Branch.BRA:BranchName       = 'All'
        GET(Queue:FileDrop_Branch, Queue:FileDrop_Branch.BRA:BranchName)
        IF ERRORCODE()
           CLEAR(Queue:FileDrop_Branch)
           Queue:FileDrop_Branch.BRA:BranchName    = 'All'
           Queue:FileDrop_Branch.BRA:BID           = 0
           ADD(Queue:FileDrop_Branch)
        .
    
    
        IF L_OE:BID_FirstTime = TRUE
           SELECT(?L_OE:BranchName, RECORDS(Queue:FileDrop_Branch))
    
           L_OE:BID         = 0
           L_OE:BranchName  = 'All'
        .
    
        L_OE:BID_FirstTime     = FALSE
  RETURN ReturnValue

