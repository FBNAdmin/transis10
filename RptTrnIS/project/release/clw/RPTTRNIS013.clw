

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS013.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Print_Designer PROCEDURE (p:PID)

LOC:Invoice_Q        QUEUE,PRE(L_IQ)                       ! 
Line                 ULONG                                 ! 
Text                 STRING(500)                           ! 
                     END                                   ! 
LOC:Print_Group      GROUP,PRE(L_PG)                       ! 
Idx                  ULONG                                 ! 
Idx2                 ULONG                                 ! 
                     END                                   ! 
State_Group          GROUP,PRE(L_SG)                       ! 
Layout_Changed       BYTE                                  ! 
PrintName            STRING(50)                            ! Name of this Print
PrintTypeStr         STRING(20)                            ! 
PrintType            BYTE                                  ! 
PageLines            SHORT(48)                             ! Lines on this page
Auto_Update          BYTE                                  ! Automatically update changed settings
PageColumns          SHORT(80)                             ! Columns on a page
                     END                                   ! 
LOC:Fields_Q         QUEUE,PRE(L_FQ)                       ! 
FieldName            STRING(50)                            ! Field Name
XPos                 SHORT                                 ! X Position
YPos                 SHORT                                 ! Y Position
Length               SHORT                                 ! 
PLID                 ULONG                                 ! 
PFID                 ULONG                                 ! 
                     END                                   ! 
LOC:Field_Loaded_Group GROUP,PRE(L_FL)                     ! 
PLID                 ULONG                                 ! 
XPos                 SHORT                                 ! X Position
YPos                 SHORT                                 ! Y Position
Length               SHORT                                 ! 
Orig_XPos            SHORT                                 ! 
Orig_YPos            SHORT                                 ! 
Orig_Length          SHORT                                 ! 
FieldName            STRING(50)                            ! Field Name
                     END                                   ! 
LOC:PageLines        SHORT(48)                             ! 
LOC:Copies           USHORT(1)                             ! Copies to print
LOC:Dot_Empty_Lines  USHORT                                ! Print dot on empty lines from this line (0 is off)
LOC:UpDownFromLine   USHORT                                ! 
Test_Options         GROUP,PRE(L_TO)                       ! 
PrintType            BYTE                                  ! 
PrintOption          BYTE                                  ! 
PaperWidth           LONG(216)                             ! 
PaperHeight          LONG(203)                             ! 
FontName             STRING(200)                           ! 
EmptyLineChar        STRING('. {19}')                      ! 
                     END                                   ! 
Window               WINDOW('Invoice Print'),AT(,,527,307),FONT('Tahoma',8,,FONT:regular),DOUBLE,GRAY,MDI
                       SHEET,AT(4,2,519,287),USE(?Sheet1)
                         TAB('Page'),USE(?Tab1)
                           LIST,AT(9,20,510,260),USE(?List1),FONT('Courier New',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  HVSCROLL,FORMAT('20R(2)|M~Line~L@n13@1020L(2)|M~Text~@s255@'),FROM(LOC:Invoice_Q)
                         END
                         TAB('Design'),USE(?Tab2)
                           LIST,AT(10,20,240,262),USE(?List_Fields),HVSCROLL,FORMAT('100L(2)|M~Field Name~@s50@40R' & |
  '(2)|M~X Pos~L@n_5@40R(2)|M~Y Pos~L@n_5@20R(2)|M~Length~L@n_5@'),FROM(LOC:Fields_Q)
                           BUTTON('Sort by Field Name'),AT(262,20,77,14),USE(?Button_SortFieldName)
                           BUTTON('Load'),AT(469,36,45,14),USE(?Button_Load),HIDE
                           BUTTON('Sort by YPos && XPos'),AT(262,36,,14),USE(?Button_Sort_YPos_XPos)
                           BUTTON('Save'),AT(469,20,45,14),USE(?Button_Save),TIP('Save any changes to the layout now')
                           BUTTON('Populate'),AT(469,60,45,14),USE(?Button_Populate),TIP('Populate the page with the fields')
                           PROMPT('X Pos:'),AT(334,76),USE(?XPos:Prompt),TRN
                           SPIN(@n_5),AT(366,76,60,10),USE(L_FL:XPos),RIGHT(1),MSG('X Position'),TIP('X Position')
                           PROMPT('Page Lines:'),AT(473,78,37,10),USE(?PageLines:Prompt),TRN
                           PROMPT('Y Pos:'),AT(334,94),USE(?YPos:Prompt),TRN
                           SPIN(@n_5),AT(366,94,60,10),USE(L_FL:YPos),RIGHT(1),MSG('Y Position'),TIP('Y Position')
                           PROMPT('Length:'),AT(334,113),USE(?Length:Prompt),TRN
                           SPIN(@n_5),AT(366,113,60,10),USE(L_FL:Length),RIGHT(1)
                           BUTTON('&Update'),AT(366,132,60,14),USE(?Button_Update)
                           CHECK(' Auto Update'),AT(366,153),USE(L_SG:Auto_Update),MSG('Automatically update chang' & |
  'ed settings'),TIP('Automatically update changed settings'),TRN
                           PROMPT('Print Type:'),AT(264,179),USE(?L_TO:PrintType:Prompt)
                           LIST,AT(323,180,60,10),USE(L_TO:PrintType),DROP(5),FROM('Invoices|#0|Credit Notes|#1|De' & |
  'livery Notes|#2|Statements|#3')
                           PROMPT('Print Option:'),AT(264,195),USE(?L_TO:PrintOption:Prompt)
                           LIST,AT(323,194,60,10),USE(L_TO:PrintOption),DROP(8,100),FROM('Original|#0|Type 1|#1|Ty' & |
  'pe 2|#2|Type 3|#3|Type 4|#4')
                           LINE,AT(262,170,250,0),USE(?LINE1),COLOR(COLOR:Black)
                           PROMPT('Paper Width:'),AT(264,210),USE(?L_TO:PaperWidth:Prompt)
                           ENTRY(@n10),AT(323,209,60,10),USE(L_TO:PaperWidth),RIGHT(1)
                           PROMPT('Paper Height:'),AT(264,222),USE(?L_TO:PaperHeight:Prompt)
                           ENTRY(@n10),AT(323,221,60,10),USE(L_TO:PaperHeight),RIGHT(1)
                           PROMPT('Font Name:'),AT(264,237),USE(?L_TO:FontName:Prompt)
                           ENTRY(@s200),AT(323,236,130,10),USE(L_TO:FontName)
                           BUTTON('...'),AT(458,236,14,11),USE(?BUTTON1)
                           SPIN(@n_5),AT(323,268,46,10),USE(LOC:PageLines),RIGHT(1),TIP('Lines to populate on the test page')
                           PROMPT('Test Page Lines:'),AT(264,268),USE(?LOC:PageLines:Prompt),TRN
                           SPIN(@n6),AT(323,250,46,10),USE(LOC:Dot_Empty_Lines),RIGHT(1),MSG('Print dot on empty l' & |
  'ines from this line (0 is off)'),TIP('Print dot on empty lines from this line (0 is ' & |
  'off)<0DH,0AH>Default is 45<0DH,0AH>Note: this setting is set in My Settings, changes' & |
  ' here are not saved')
                           PROMPT('Dot Empty Lines:'),AT(264,250),USE(?LOC:Dot_Empty_Lines:Prompt),TRN
                           BUTTON('Test Page'),AT(469,268,45,14),USE(?Button_TestPage),TIP('Populate the rows and ' & |
  'columns with test page numbers')
                           BUTTON('Advance'),AT(469,179,45,14),USE(?Button_Advance),SKIP,TIP('Advance the printer ' & |
  'page 1 line')
                           ENTRY(@n_5),AT(474,92,36,9),USE(L_SG:PageLines,,?L_SG:PageLines:3),RIGHT(1),COLOR(00E3E3E3h), |
  MSG('Lines on this page'),READONLY,SKIP,TIP('Lines on this page')
                           PROMPT('Empty Line Char:'),AT(415,250),USE(?L_TO:EmptyLineChar:Prompt)
                           ENTRY(@s20),AT(474,250,36,10),USE(L_TO:EmptyLineChar)
                         END
                       END
                       BUTTON('&Print'),AT(408,290,,14),USE(?OkButton),LEFT,ICON(ICON:Print1),DEFAULT,FLAT
                       BUTTON('&Close'),AT(470,290,,14),USE(?CancelButton),LEFT,ICON('wacancel.ico'),FLAT
                       PROMPT('Copies:'),AT(336,294),USE(?LOC:Copies:Prompt)
                       SPIN(@n6),AT(364,294,35,10),USE(LOC:Copies),RIGHT(1),MSG('Copies to print'),TIP('Copies to print')
                       BUTTON('Up'),AT(5,290),USE(?BUTTON_all_up)
                       BUTTON('Down'),AT(41,290,32,14),USE(?BUTTON_down_up)
                       PROMPT('From Line:'),AT(77,294),USE(?LOC:UpDownFromLine:Prompt)
                       SPIN(@n6),AT(115,294,40,10),USE(LOC:UpDownFromLine),RIGHT(1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
PL_View         VIEW(PrintLayout)
       JOIN(PRIF:PKey_PFID, PRIL:PFID)
       PROJECT(PRIF:FieldName)
    .  .
                     MAP
Move_UpDown             PROCEDURE(LONG pUpDown, LONG pMaxLines)
                     .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_Field_Q              ROUTINE
    FREE(LOC:Fields_Q)

    PUSHBIND
    BIND('PRIL:PID', PRIL:PID)

    OPEN(PL_View)
    PL_View{PROP:Filter}    = 'PRIL:PID = ' & p:PID
    SET(PL_View)

    LOOP
       NEXT(PL_View)
       IF ERRORCODE()
          BREAK
       .

       L_FQ:FieldName   = PRIF:FieldName
       L_FQ:XPos        = PRIL:XPos
       L_FQ:YPos        = PRIL:YPos
       L_FQ:Length      = PRIL:Length

       L_FQ:PLID        = PRIL:PLID
       L_FQ:PFID        = PRIL:PFID
       ADD(LOC:Fields_Q)
    .
    CLOSE(PL_View)
    POPBIND

    SORT(LOC:Fields_Q, L_FQ:FieldName)
    EXIT
Save_Field_Q              ROUTINE
    L_PG:Idx    = 0
    LOOP
       L_PG:Idx    += 1
       GET(LOC:Fields_Q, L_PG:Idx)
       IF ERRORCODE()
          BREAK
       .

       PRIL:PLID        = L_FQ:PLID
       IF Access:PrintLayout.TryFetch(PRIL:PKey_PLID) = LEVEL:Benign
          PRIL:XPos     = L_FQ:XPos
          PRIL:YPos     = L_FQ:YPos
          PRIL:Length   = L_FQ:Length
          IF Access:PrintLayout.TryUpdate() = LEVEL:Benign
    .  .  .

    
    L_SG:Layout_Changed = FALSE
    EXIT
! -------------------------------------------------------------------------------------------------------
Load_Test_Page        ROUTINE
    FREE(LOC:Invoice_Q)
    CLEAR(LOC:Invoice_Q)

    LOOP LOC:PageLines TIMES
       L_IQ:Line  += 1
       L_IQ:Text   = FORMAT(L_IQ:Line,@n02) & ' - 6789x123456789x123456789x123456789x123456789x123456789x123456789x123456789x'

       ADD(LOC:Invoice_Q)
    .
    EXIT
! -------------------------------------------------------------------------------------------------------
Populate                     ROUTINE
DATA
FieldToPlace CSTRING(255)
ToFill LONG
   
CODE
    SORT(LOC:Fields_Q, L_FQ:YPos, L_FQ:XPos)

    FREE(LOC:Invoice_Q)
    L_IQ:Line       = 0

    LOOP L_SG:PageLines TIMES
       L_IQ:Line   += 1

       CLEAR(L_IQ:Text)

       ! For every line, check through the Fields on this line in X Pos order an populate...
       L_PG:Idx     = 0
       LOOP
          L_PG:Idx += 1
          GET(LOC:Fields_Q, L_PG:Idx)
          IF ERRORCODE()
             BREAK
          .

          IF L_FQ:YPos ~= L_IQ:Line
             CYCLE
          .

 db.debugout('[Print_Designer] L_FQ:YPos: ' & L_FQ:YPos & ',  XPos: ' & L_FQ:XPos & ', Length: ' & L_FQ:Length & ' - Line: ' & CLIP(L_IQ:Text))
! db.debugout('[Print_Designer] before splice')

          IF L_FQ:XPos <= 0
             L_FQ:XPos      = 1
             L_FQ:Length    = L_FQ:Length + (L_FQ:XPos - 1)
          .

          IF L_FQ:Length > 0
             ! Print this on this line then - we dont check for overlap
             FieldToPlace = '-' & CLIP(L_FQ:FieldName) & '-'
             ToFill = L_FQ:Length - LEN(CLIP(FieldToPlace))             
             IF ToFill > 0
                 LOOP ToFill TIMES
                     FieldToPlace = FieldToPlace & '*'
                 .
                 db.Debugout('field padded: ' & L_FQ:FieldName & '  len now: ' & LEN(CLIP(FieldToPlace)) & '   spec len: ' & L_FQ:Length)
             .
             L_IQ:Text[L_FQ:XPos : L_FQ:XPos + L_FQ:Length - 1] = CLIP(FieldToPlace)
          ELSE
             L_IQ:Text[L_FQ:XPos : L_FQ:XPos + 1]               = '-' & CLIP(L_FQ:FieldName) & '-'
          .

! db.debugout('[Print_Designer] after splice')
       .

!       L_IQ:Text   = FORMAT(L_IQ:Line,@n02) & ' - 6789x123456789x123456789x123456789x123456789x123456789x123456789x123456789x'

       ADD(LOC:Invoice_Q)
    .

    SORT(LOC:Fields_Q, L_FQ:FieldName)
    EXIT
! -------------------------------------------------------------------------------------------------------
Print_Now                   ROUTINE
    ! (p:ID , p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow, <Q_Type_UL_S500 p_Print_Q>)
    ! (ULONG, BYTE       , BYTE=0      , BYTE=1          , BYTE=0      , <Q_Type_UL_S500 p_Print_Q>), LONG, PROC
    !   1           2           3               4               5                   6
    ! p:PrintType         -     0 = Invoices
    !                           1 = Credit Notes
    !                           2 = Delivery Notes
    !                           3 = Statements
    !                           42 = Passed Q
    !
    ! p:ID_Options        -     used for Invoices, if 1 then DID is passed not IID
    !
    ! p:Preview_Option    -     0 = none
    !                           1 = preview (standard)
    !                           2 = ask preview all - when this mode return value is next preview response from user
    !                               This only occurs on first call - although callers responsibility
    !
    ! p:HideWindow              Use in conjunction with p:Preview_Option = 0
    !
    ! LOC:Result
    !
    ! Q_Type_UL_S500      QUEUE,TYPE
    ! UL1_        ULONG
    ! S500        STRING(500)

    !L_SG:PrintType

    LOOP LOC:Copies TIMES
       Print_Cont(0, 42,, 0,, LOC:Invoice_Q, Test_Options)
    .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Designer')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:PrintFields.SetOpenRelated()
  Relate:PrintFields.Open                                  ! File PrintFields used by this procedure, so make sure it's RelationManager is open
  Access:PrintLayout.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Prints.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Designer',Window)                    ! Restore window settings from non-volatile store
      LOC:Dot_Empty_Lines     = GETINI('Print_Cont', 'Dot_Empty_Lines', 45, GLO:Local_INI)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PrintFields.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Designer',Window)                 ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Update
          L_FQ:PLID   = L_FL:PLID
          GET(LOC:Fields_Q, L_FQ:PLID)
          IF ERRORCODE()
             MESSAGE('Error on getting field from queue to save.||Error: ' & ERROR(), 'Print Designer', ICON:Hand)
          ELSE
             IF L_FQ:XPos ~= L_FL:XPos OR L_FQ:YPos ~= L_FL:YPos OR L_FQ:Length ~= L_FL:Length
                L_SG:Layout_Changed   = TRUE
             .
      
             L_FQ:XPos        = L_FL:XPos
             L_FQ:YPos        = L_FL:YPos
             L_FQ:Length      = L_FL:Length
      
             L_FL:Orig_Length = L_FL:Length
             L_FL:Orig_XPos   = L_FL:XPos
             L_FL:Orig_YPos   = L_FL:YPos
      
             PUT(LOC:Fields_Q)
             IF ERRORCODE()
                MESSAGE('ERROR on save field to queue: ' & ERROR())
             .
          .
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_SortFieldName
      ThisWindow.Update()
          SORT(LOC:Fields_Q, L_FQ:FieldName)
    OF ?Button_Sort_YPos_XPos
      ThisWindow.Update()
          SORT(LOC:Fields_Q, L_FQ:YPos, L_FQ:XPos)
    OF ?Button_Save
      ThisWindow.Update()
          DO Save_Field_Q
    OF ?Button_Populate
      ThisWindow.Update()
          DO Populate
    OF ?BUTTON1
      ThisWindow.Update()
         FONTDIALOG('Select Font', L_TO:FontName)
         DISPLAY()
    OF ?Button_TestPage
      ThisWindow.Update()
          DO Load_Test_Page
    OF ?Button_Advance
      ThisWindow.Update()
          LinePrint('')
    OF ?OkButton
      ThisWindow.Update()
          DO Print_Now
    OF ?CancelButton
      ThisWindow.Update()
          IF L_SG:Layout_Changed = TRUE
             CASE MESSAGE('The Layout has been changed.||Would you like to save it now?', 'Layout Changed', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
             OF BUTTON:Yes
                DO Save_Field_Q
          .  .
       POST(EVENT:CloseWindow)
    OF ?BUTTON_all_up
      ThisWindow.Update()
         Move_UpDown(-1, L_SG:PageLines)
    OF ?BUTTON_down_up
      ThisWindow.Update()
      !SORT(LOC:Fields_Q, L_FQ:YPos)
      !
      !GET(LOC:Fields_Q, 1)
      !IF L_FQ:YPos = L_SG:PageLines
      !   MESSAGE('Printing to last Line ' & L_SG:PageLines & ', cant move it down')
      !ELSE
      !   idx_# = 0
      !   LOOP
      !      idx_# += 1
      !      GET(LOC:Fields_Q, idx_#)
      !      IF ERRORCODE() 
      !         BREAK
      !      .
      !      
      !      db.Debugout('L_FQ:FieldName: ' & L_FQ:FieldName & '      ypos: ' & L_FQ:YPos)
      !      
      !      L_FQ:YPos += 1
      !      PUT(LOC:Fields_Q)
      !   .
      !.
         Move_UpDown(1, L_SG:PageLines)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List1
      GET(LOC:Invoice_Q, CHOICE(?List1))
      IF ~ERRORCODE() 
         LOC:UpDownFromLine = CHOICE(?List1)
         DISPLAY(?LOC:UpDownFromLine)
      .
    OF ?List_Fields
          GET(LOC:Fields_Q, CHOICE(?List_Fields))
          IF ~ERRORCODE()
             IF L_FL:PLID ~= 0 AND L_FL:PLID ~= L_FQ:PLID
                IF L_FL:Orig_XPos ~= L_FL:XPos OR L_FL:Orig_YPos ~= L_FL:YPos OR L_FL:Length ~= L_FL:Orig_Length
                   IF L_SG:Auto_Update = TRUE
                      L_FQ:PLID   = L_FL:PLID
                      GET(LOC:Fields_Q, L_FQ:PLID)
                      IF ~ERRORCODE()
                         L_SG:Layout_Changed  = TRUE
                         L_FQ:XPos            = L_FL:XPos
                         L_FQ:YPos            = L_FL:YPos
                         L_FQ:Length          = L_FL:Length
                         PUT(LOC:Fields_Q)
                      .
                   ELSE
                      CASE MESSAGE('The previously selected field (' & CLIP(L_FL:FieldName) & ') has modified X / Y positions.||Would you like to update this fields positions now?', 'Field Changed', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                      OF BUTTON:Yes
                         L_FQ:PLID   = L_FL:PLID
                         GET(LOC:Fields_Q, L_FQ:PLID)
                         IF ~ERRORCODE()
                            L_SG:Layout_Changed  = TRUE
                            L_FQ:XPos            = L_FL:XPos
                            L_FQ:YPos            = L_FL:YPos
                            L_FQ:Length          = L_FL:Length
                            PUT(LOC:Fields_Q)
             .  .  .  .  .
      
             GET(LOC:Fields_Q, CHOICE(?List_Fields))
             IF ~ERRORCODE()
                L_FL:PLID         = L_FQ:PLID
                L_FL:XPos         = L_FQ:XPos
                L_FL:YPos         = L_FQ:YPos
                L_FL:Length       = L_FQ:Length
      
                L_FL:Orig_XPos    = L_FQ:XPos
                L_FL:Orig_YPos    = L_FQ:YPos
                L_FL:Orig_Length  = L_FQ:Length
      
                L_FL:FieldName    = L_FQ:FieldName
             ELSE
                CLEAR(LOC:Field_Loaded_Group)
             .
             DISPLAY
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          PRI:PID     = p:PID
          IF Access:Prints.TryFetch(PRI:PKey_PID) = LEVEL:Benign
             L_SG:PrintName       = PRI:PrintName
             L_SG:PageLines       = PRI:PageLines
             L_SG:PrintType       = PRI:PrintType
      
             L_SG:PageColumns     = PRI:PageColumns
      
             LOC:PageLines        = PRI:PageLines
      
             ! Invoices|Credit Notes|Delivery Notes
             EXECUTE PRI:PrintType
                L_SG:PrintTypeStr    = 'Credit Notes'
                L_SG:PrintTypeStr    = 'Delivery Notes'
                L_SG:PrintTypeStr    = 'Statements'
             ELSE
                L_SG:PrintTypeStr    = 'Invoices'
             .
          ELSE
             MESSAGE('Print not found for PID: ' & p:PID, 'Print Designer', ICON:Hand)
             POST(EVENT:CloseWindow)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          DO Load_Field_Q
      
          DO Populate
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Move_UpDown          PROCEDURE(LONG pUpDown, LONG pMaxLines)
   !  pUpDown:  1 for down -1 for up
FromLine                USHORT      ! Line above / below that we would need to use
CantDo                  BYTE(0)

CODE
   SORT(LOC:Fields_Q, L_FQ:YPos)

   FromLine = 1
   IF LOC:UpDownFromLine > 1
      FromLine = LOC:UpDownFromLine
   .

   ! Check we have a free line to move into first   
   ! If going UP check we have room above the fromLine to go into
   ! If going DOWN check we have room at the bottom of the report to go into   
   idx_# = 0
   LOOP
      idx_# += 1
      GET(LOC:Fields_Q, idx_#)
      IF ERRORCODE() 
         BREAK
      .
      
      IF pUpDown < 0          ! up
         IF L_FQ:YPos = FromLine
            CantDo = TRUE   
            BREAK
         .         
      ELSE
         IF L_FQ:YPos = pMaxLines
            CantDo = TRUE   
            BREAK
         .         
      .            
   .
   
   ! Move if all OK
   IF CantDo = TRUE
      MESSAGE('Printing on Line ' & FromLine & ', cant move it up/down')
   ELSE
!      IF LOC:UpDownFromLine > 1
!         LOC:UpDownFromLine += pUpDown
!      .
      
      idx_# = 0
      LOOP
         idx_# += 1
         GET(LOC:Fields_Q, idx_#)
         IF ERRORCODE() 
            BREAK
         .
         
         ! If we are above fromLine skip
         IF L_FQ:YPos < FromLine
            CYCLE
         .
         
         db.Debugout('L_FQ:FieldName: ' & L_FQ:FieldName & '      ypos: ' & L_FQ:YPos & '    pUpDown: ' & pUpDown)
         
         L_FQ:YPos = L_FQ:YPos + pUpDown
         PUT(LOC:Fields_Q)
      .
      DO Populate
   .     
   
   
   RETURN
   
!!! <summary>
!!! Generated from procedure template - Process
!!! Validate rates - see comments
!!! </summary>
Process_Rates_Checker PROCEDURE 

Progress:Thermometer BYTE                                  ! 
L_CG:Current_Group   GROUP,PRE(L_CG)                       ! 
RID                  ULONG                                 ! Rate ID
CID                  ULONG                                 ! Client ID
JID                  ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
LTID                 ULONG                                 ! Load Type ID
CTID                 ULONG                                 ! Container Type ID
CRTID                ULONG                                 ! Client Rate Type ID
Effective_Date       DATE                                  ! Effective from date
ToMass               DECIMAL(9)                            ! Up to this mass in Kgs
RatePerKg            DECIMAL(10,4)                         ! Rate per Kg
MinimiumCharge       DECIMAL(10,2)                         ! 
Prev_ToMass          DECIMAL(9)                            ! Up to this mass in Kgs
                     END                                   ! 
LOC:Rates_Q          QUEUE,PRE(L_RQ)                       ! 
CID                  ULONG                                 ! Client ID
RID                  ULONG                                 ! Rate ID
Previous_RID         ULONG                                 ! Rate ID
Problem_Type         BYTE                                  ! Problem Type - rate is 0, 1 is 0 to Mass
ClientNo             ULONG                                 ! Client No.
                     END                                   ! 
LOC:Out_Put_Section  BYTE                                  ! 
Process:View         VIEW(Clients)
                       PROJECT(CLI:CID)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:ClientNo)
                       JOIN(RAT:FKey_CID,CLI:CID)
                         PROJECT(RAT:CID)
                         PROJECT(RAT:CTID)
                         PROJECT(RAT:Effective_Date)
                         PROJECT(RAT:JID)
                         PROJECT(RAT:LTID)
                         PROJECT(RAT:MinimiumCharge)
                         PROJECT(RAT:RID)
                         PROJECT(RAT:RatePerKg)
                         PROJECT(RAT:ToMass)
                       END
                     END
ProgressWindow       WINDOW('Process Rates'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
OutFile         FILE,DRIVER('ASCII'),PRE(O_F),CREATE
Rec         RECORD
Line            STRING(2000)
            .   .
View_Rate           VIEW(__Rates)
       JOIN(CLI:PKey_CID, RAT:CID)
       .

       JOIN(LOAD2:PKey_LTID, RAT:LTID)
       .
       JOIN(JOU:PKey_JID, RAT:JID)
       .
       JOIN(CTYP:PKey_CTID, RAT:CTID)
    .  .


Rate_View           ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_Rates_Checker')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ContainerTypes.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients_ContainerParkDiscounts.UseFile            ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LoadTypes2.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Process_Rates_Checker',ProgressWindow)     ! Restore window settings from non-volatile store
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:Clients, ?Progress:PctText, Progress:Thermometer, ProgressMgr, CLI:ClientNo)
  ThisProcess.AddSortOrder(CLI:Key_ClientNo)
  ThisProcess.AppendOrder('+RAT:LTID,+RAT:CRTID,+RAT:JID,+RAT:CTID,-RAT:Effective_Date,+RAT:ToMass')
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='Checking Rates'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(Clients,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Process_Rates_Checker',ProgressWindow)  ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
    IF RECORDS(LOC:Rates_Q) > 0
       ?Progress:UserString{PROP:Text}  = 'Exporting - please wait...'
       ?Progress:PctText{PROP:Text}     = ''
       DISPLAY

       SETCURSOR(CURSOR:Wait)
       CLEAR(L_CG:CID)

       SORT(LOC:Rates_Q, L_RQ:Problem_Type, L_RQ:CID, L_RQ:RID)

       Rate_View.Init(View_Rate, Relate:__Rates)
       Rate_View.AddSortOrder(RAT:PKey_CRID)
       !Rate_View.AppendOrder()
       Rate_View.AddRange(RAT:RID, L_CG:RID)
       !Rate_View.SetFilter()

       OutFile{PROP:Name}  = 'Rate_Chk.TXT'
       CREATE(OutFile)
       IF ERRORCODE()
          MESSAGE('Could not Create Out File.||Error: ' & ERROR() & '||File: ' & PATH() & '\' & OutFile{PROP:Name})
       .

       OPEN(OutFile)
       IF ERRORCODE()
          MESSAGE('Found ' & RECORDS(LOC:Rates_Q) & ' problems out of ' & RECORDS(__Rates) & ' rate records.||Output file could not be created.||File: ' & PATH() & '\Rate_Chk.TXT||Error: ' & ERROR(),'Rate Check',ICON:Hand)
       ELSE
          IF RECORDS(LOC:Rates_Q) > 0
             O_F:Line     = '*********************************************************'
             ADD(OutFile)
             O_F:Line     = '*****************  Inconsistent Rates  ******************'
             ADD(OutFile)
             O_F:Line     = '*********************************************************'
             ADD(OutFile)
          .

          LOOP I_# = 1 TO RECORDS(LOC:Rates_Q)
             GET(LOC:Rates_Q, I_#)
             IF ERRORCODE()
                BREAK
             .
             db.debugout('[Process_Rates_Checker]   L_RQ:CID: ' & L_RQ:CID & ', L_RQ:RID: ' & L_RQ:RID & ',  L_RQ:Previous_RID: ' & L_RQ:Previous_RID & ',  Client No.: ' & L_RQ:ClientNo)

             IF L_RQ:Problem_Type = 4           ! Min charge is redundant
                IF LOC:Out_Put_Section ~= 4
                   O_F:Line     = ''
                   ADD(OutFile)
                   O_F:Line     = '*********************************************************'
                   ADD(OutFile)
                   O_F:Line     = '******  Min Charge not needed here                 ******'
                   ADD(OutFile)
                   O_F:Line     = '*********************************************************'
                   ADD(OutFile)
                .
                LOC:Out_Put_Section = 4
             .
             IF L_RQ:Problem_Type = 3           ! Min amt greater than higher rate * min mass amt
                IF LOC:Out_Put_Section ~= 3
                   O_F:Line     = ''
                   ADD(OutFile)
                   O_F:Line     = '*********************************************************'
                   ADD(OutFile)
                   O_F:Line     = '******  Min Charge > following rate * mass charge  ******'
                   ADD(OutFile)
                   O_F:Line     = '*********************************************************'
                   ADD(OutFile)
                .
                LOC:Out_Put_Section = 3
             .
             IF L_RQ:Problem_Type = 2           ! Client has no rates
                IF LOC:Out_Put_Section ~= 2
                   O_F:Line     = ''
                   ADD(OutFile)
                   O_F:Line     = '*********************************************************'
                   ADD(OutFile)
                   O_F:Line     = '***************  Clients with No Rates  *****************'
                   ADD(OutFile)
                   O_F:Line     = '*********************************************************'
                   ADD(OutFile)
                .
                LOC:Out_Put_Section = 2

                CLEAR(O_F:Rec)
                CLI:CID     = L_RQ:CID
                IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
                   L_CG:CID     = CLI:CID
                   O_F:Line     = 'Client: ' & CLIP(CLI:ClientName) & '  (' & CLI:ClientNo & ')'
                   ADD(OutFile)
                .

                CYCLE
             .
             IF L_RQ:Problem_Type = 1           ! No Mass Rates
                IF LOC:Out_Put_Section ~= 1
                   O_F:Line     = ''
                   ADD(OutFile)
                   O_F:Line     = '*********************************************************'
                   ADD(OutFile)
                   O_F:Line     = '****************  Rates with No Mass  *******************'
                   ADD(OutFile)
                   O_F:Line     = '*********************************************************'
                   ADD(OutFile)
                .
                LOC:Out_Put_Section = 1
             .

             L_CG:RID       = L_RQ:RID
             Rate_View.ApplyRange()
             Rate_View.Reset()

             ! Client heading
             IF Rate_View.Next() ~= LEVEL:Benign
                O_F:Line        = '----------------------------------------'
                ADD(OutFile)
                O_F:Line        = 'Rate record missing - RID: ' & L_RQ:RID
                ADD(OutFile)
                O_F:Line        = '----------------------------------------'
                ADD(OutFile)
                CYCLE
             .

             CLEAR(O_F:Rec)
             IF CLI:CID = 0
                O_F:Line     = 'Client: <missing ' & CLI:CID & '>'
             ELSIF L_CG:CID ~= CLI:CID
                ADD(OutFile)

                L_CG:CID     = CLI:CID
                O_F:Line     = 'Client: ' & CLIP(CLI:ClientName) & '  (' & CLI:ClientNo & ')'
             .
             IF CLIP(O_F:Line) ~= ''
                ADD(OutFile)
             .

             O_F:Line       = 'Rate - Load Type: ' & CLIP(LOAD2:LoadType) & ',  Journey: ' & CLIP(JOU:Journey)
             IF CTYP:CTID ~= 0
                O_F:Line    = CLIP(O_F:Line) & ',  Container Type: ' & CTYP:ContainerType
             .

             IF L_RQ:Problem_Type = 0
                O_F:Line    = CLIP(O_F:Line) & ',  Rate: ' & RAT:RatePerKg & '  (ToMass: ' & RAT:ToMass & ', Min Charge: ' & RAT:MinimiumCharge & ')'
             ELSE
                O_F:Line    = CLIP(O_F:Line) & ',  ToMass: ' & RAT:ToMass & ',  Rate: ' & RAT:RatePerKg & '  (Min Charge: ' & RAT:MinimiumCharge & ')'
             .
             ADD(OutFile)


             IF L_RQ:Problem_Type = 0
                L_CG:RID       = L_RQ:Previous_RID
                IF Rate_View.ApplyRange() = 0
                   MESSAGE('No range change')
                .
                Rate_View.Reset()

                IF Rate_View.Next() ~= LEVEL:Benign
                   O_F:Line        = '----------------------------------------'
                   ADD(OutFile)
                   O_F:Line        = 'Rate 2 record missing - RID: ' & L_RQ:Previous_RID
                   ADD(OutFile)
                   O_F:Line        = '----------------------------------------'
                   ADD(OutFile)
                   CYCLE
                ELSE
                   CLEAR(O_F:Rec)
                   IF CLI:CID = 0
                      O_F:Line     = 'Client: <missing ' & CLI:CID & '>'
                   ELSIF L_CG:CID ~= CLI:CID
                      O_F:Line     = '<Client not same!!> Client: ' & CLIP(CLI:ClientName)
                   .
                   IF CLIP(O_F:Line) ~= ''
                      ADD(OutFile)
                   .

                   O_F:Line       = 'Prev - Load Type: ' & CLIP(LOAD2:LoadType) & ',  Journey: ' & CLIP(JOU:Journey)
                   IF CTYP:CTID ~= 0
                      O_F:Line    = CLIP(O_F:Line) & ',  Container Type: ' & CTYP:ContainerType
                   .

                   O_F:Line       = CLIP(O_F:Line) & ',  Rate: ' & RAT:RatePerKg & '  (ToMass: ' & RAT:ToMass & ', Min Charge: ' & RAT:MinimiumCharge & ')'
                   ADD(OutFile)
          .  .  .

          CLOSE(OutFile)

          ! (whandle, URL, p:Params, p:LauchDir)
          ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Rate_Chk.TXT')

          MESSAGE('Found ' & RECORDS(LOC:Rates_Q) & ' problems out of ' & RECORDS(__Rates) & ' rate records.||Output file created: ' & PATH() & '\Rate_Chk.TXT', 'Rate Check', ICON:Exclamation)
       .

       Rate_View.Kill()
       SETCURSOR
    ELSE
       MESSAGE('No problems found in ' & RECORDS(__Rates) & ' rate records.', 'Rate Check', ICON:Asterisk)
    .

    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      ! Notes:
      !   Check through rates looking for inconsistancies such as an up to tonnage having a greater cost than
      !   last cost when the tonnage is also greater.
  
      ! We will have all the rates come through here.
      ! A rate group is defined by - RAT:CID, RAT:JID, RAT:LTID, RAT:CTID
      ! When any of these change we are onto another group and must re-set our checking vars
  
      ! Not all Clients have rates
  
      ! Problem_Type
      !   0 - Rate inconsistant
      !   1 - No Mass
      !   2 - No Rates
  
      IF RAT:RID = 0                      ! No Rates
         CLI_CP:CID   = CLI:CID           ! Check Container Park Discounts
         IF Access:Clients_ContainerParkDiscounts.TryFetch(CLI_CP:FKey_CID) = LEVEL:Benign
            ! They have them...
         ELSE
            CLEAR(LOC:Rates_Q)
            L_RQ:CID              = CLI:CID
            L_RQ:Problem_Type     = 2
            L_RQ:ClientNo         = CLI:ClientNo
            ADD(LOC:Rates_Q)
         .
      ELSE
         IF L_CG:CID ~= RAT:CID OR L_CG:JID ~= RAT:JID OR L_CG:LTID ~= RAT:LTID OR L_CG:CTID ~= RAT:CTID |
                 OR L_CG:Effective_Date ~= RAT:Effective_Date OR L_CG:CRTID ~= RAT:CRTID
            ! We have a new rate group
            CLEAR(L_CG:Current_Group)
            ?Progress:UserString{PROP:Text}  = CLIP(CLI:ClientName) & '  (' & CLI:ClientNo & ')'
         .
  
  
         IF L_CG:CID = 0
            L_CG:Current_Group  :=: RAT:Record
  
            IF RAT:ToMass = 0.0                               
               CLEAR(LOC:Rates_Q)
               L_RQ:Problem_Type     = 1
               L_RQ:CID              = RAT:CID
               L_RQ:RID              = RAT:RID
               L_RQ:ClientNo         = CLI:ClientNo
               ADD(LOC:Rates_Q)
            .
         ELSE
            ! We are in a group, check the stuff
            ! IF L_CG:ToMass > RAT:ToMass                - will always be the case
  
            ! Mass is increasing, so next rate should be smaller than last one
  
            CLEAR(LOC:Rates_Q)
            L_RQ:CID             = RAT:CID
            L_RQ:RID             = RAT:RID
            L_RQ:ClientNo        = CLI:ClientNo
  
            ! If this rate is greater than previous one (for higher mass) then problem, unless Min Charge for previous..
            IF RAT:RatePerKg > L_CG:RatePerKg
  !             db.debugout('Rate is greater than last one - ' & RAT:RatePerKg & ' > ' & L_CG:RatePerKg & ',   ' & RAT:RID & ' > ' & L_CG:RID & '  - Client: ' & CLI:ClientNo)
               IF L_CG:RatePerKg = 0.0 AND L_CG:MinimiumCharge ~= 0.0
               ELSE
                  L_RQ:Previous_RID = L_CG:RID
                  ADD(LOC:Rates_Q)
               .
            ELSIF RAT:ToMass = 0.0
               L_RQ:Problem_Type = 1
               ADD(LOC:Rates_Q)
            .
  
            ! Check Min charge
            IF RAT:MinimiumCharge = 0.0                   ! No min on this item
               IF L_CG:MinimiumCharge ~= 0
                  ! Check that Rate * Min Weight is greater than the MinimiumCharge on this section
                  IF RAT:RatePerKg * (L_CG:Prev_ToMass + 1) < L_CG:MinimiumCharge
                     L_RQ:Problem_Type    = 3
                     ADD(LOC:Rates_Q)
               .  .
            ELSE
               IF RAT:RatePerKg * (L_CG:Prev_ToMass + 1) >= RAT:MinimiumCharge
                  L_RQ:Problem_Type       = 4
                  ADD(LOC:Rates_Q)
         .  .  .
  
         L_CG:RID                = RAT:RID
         L_CG:RatePerKg          = RAT:RatePerKg
  
         L_CG:Prev_ToMass        = RAT:ToMass
      .
  
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Process
!!! </summary>
Process_TripSheetDeliveries PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Delete           BYTE                                  ! 
LOC:Count            ULONG                                 ! 
Process:View         VIEW(TripSheetDeliveries)
                       PROJECT(TRDI:DIID)
                       PROJECT(TRDI:TRID)
                       JOIN(DELI:PKey_DIID,TRDI:DIID)
                         PROJECT(DELI:DIID)
                       END
                       JOIN(TRI:PKey_TID,TRDI:TRID)
                         PROJECT(TRI:TRID)
                       END
                     END
ProgressWindow       WINDOW('Process Tripsheet Deliveries'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
Kill                   PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepClass                             ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_TripSheetDeliveries')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:TripSheetDeliveries.Open                          ! File TripSheetDeliveries used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Process_TripSheetDeliveries',ProgressWindow) ! Restore window settings from non-volatile store
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ThisProcess.Init(Process:View, Relate:TripSheetDeliveries, ?Progress:PctText, Progress:Thermometer)
  ThisProcess.AddSortOrder()
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(TripSheetDeliveries,'QUICKSCAN=on')
  SEND(DeliveryItems,'QUICKSCAN=on')
  SEND(TripSheets,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TripSheetDeliveries.Close
  END
  IF SELF.Opened
    INIMgr.Update('Process_TripSheetDeliveries',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisProcess.Kill PROCEDURE

  CODE
  PARENT.Kill
      IF LOC:Count > 0
         MESSAGE(LOC:Count & ' orphaned TripSheetDeliveries records were removed.  Please saee the log file for details: "Orphaned Manifest Load Deliveries.log"', 'Process TripSheetDeliveries', ICON:Exclamation)
      ELSE
         MESSAGE('No orphaned TripSheetDeliveries records were found.', 'Process TripSheetDeliveries', ICON:Exclamation)
      .


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      LOC:Delete  = FALSE
  
      IF TRI:TRID = 0
         ! Delete this Item
         LOC:Delete = TRUE
      .
  
      IF DELI:DIID = 0
         ! Delete this Item
         LOC:Delete = 2
      .
  
      IF LOC:Delete > 0
         IF LOC:Delete = TRUE
            Add_Log('TripSheet Missing - TDID: ' & TRDI:TDID & ',  TRID: '  & TRDI:TRID & ',  DIID: ' & TRDI:DIID & ',  UnitsLoaded: ' & TRDI:UnitsLoaded & ' ,DeliverdDate: ' & TRDI:DeliveredDate & ',  DeliverdTime: ' & TRDI:DeliveredTime & ',  UnitsDelivered: ' & TRDI:UnitsDelivered & ',  UnitsNotAccepted: ' & TRDI:UnitsNotAccepted |
                  , 'Item', 'Orphaned Trip Sheet Deliveries')
         ELSE
            Add_Log('Delivery Item Missing - TDID: ' & TRDI:TDID & ',  TRID: '  & TRDI:TRID & ',  DIID: ' & TRDI:DIID & ',  UnitsLoaded: ' & TRDI:UnitsLoaded & ' ,DeliverdDate: ' & TRDI:DeliveredDate & ',  DeliverdTime: ' & TRDI:DeliveredTime & ',  UnitsDelivered: ' & TRDI:UnitsDelivered & ',  UnitsNotAccepted: ' & TRDI:UnitsNotAccepted |
                  , 'Item - Reason: ' & LOC:Delete, 'Orphaned Trip Sheet Deliveries')
         .
  
         IF Relate:TripSheetDeliveries.Delete(0) = LEVEL:Benign
            LOC:Count     += 1
      .  .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Process
!!! </summary>
Process_Manifests PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Result           LONG                                  ! 
LOC:Option           BYTE                                  ! 
LOC:Results          GROUP,PRE(L_RG)                       ! 
Created              LONG                                  ! 
Exist                LONG                                  ! 
Errors               LONG                                  ! 
Created_MIDs         STRING(2000)                          ! List of MID's for which invoices were created
                     END                                   ! 
LOC:User_Access      LONG                                  ! 
Process:View         VIEW(Manifest)
                       PROJECT(MAN:CreatedDate)
                       PROJECT(MAN:MID)
                       PROJECT(MAN:State)
                     END
ProgressWindow       WINDOW('Process Manifest'),AT(,,179,107),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  TIMER(1)
                       SHEET,AT(4,4,172,82),USE(?Sheet1)
                         TAB('Option'),USE(?Tab1)
                           PROMPT('This process will create or re-create Manifest Transporter Invoices for all Man' & |
  'ifests that are in a Loaded or later state.'),AT(14,22,150,31),USE(?Prompt2),FONT(,,,FONT:regular, |
  CHARSET:ANSI),TRN
                           PROMPT('Invoice Option:'),AT(14,62),USE(?LOC:Option:Prompt),TRN
                           LIST,AT(74,62,90,10),USE(LOC:Option),DROP(5),FROM('Ask if Exists|#0|Don''t re-create if' & |
  ' Exists|#1|Re-create if Exists|#2')
                         END
                         TAB('Progress'),USE(?Tab_Progress)
                           PROGRESS,AT(34,42,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(18,30,141,10),USE(?Progress:UserString),CENTER,TRN
                           STRING(''),AT(18,58,141,10),USE(?Progress:PctText),CENTER,TRN
                         END
                       END
                       BUTTON('Cancel'),AT(126,90,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                       BUTTON('&Pause'),AT(74,90,49,15),USE(?Pause),LEFT,ICON(ICON:Tick),FLAT
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_Manifests')
      LOC:User_Access   = Get_User_Access(GLO:UID, 'Batch Add Trans. Invoices', 'Process_Manifests',, 1)
      ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:Default_Action)
  
      ! Returns
      !   0   - Allow (default)
      !   1   - Disable
      !   2   - Hide
      !   100 - Allow
      !   101 - Disallow
  
      IF LOC:User_Access = 0 OR LOC:User_Access = 100
         ! Ok
      ELSE
         MESSAGE('Access denied for this area.||(Process_Manifests)', 'Access', ICON:Hand)
         RETURN(LEVEL:Fatal)
      .
  
  
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Manifest.Open                                     ! File Manifest used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Process_Manifests',ProgressWindow)         ! Restore window settings from non-volatile store
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:Manifest, ?Progress:PctText, Progress:Thermometer, ProgressMgr, MAN:MID)
  ThisProcess.AddSortOrder(MAN:PKey_MID)
  ThisProcess.SetFilter('MAN:State > 3')
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(Manifest,'QUICKSCAN=on')
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Manifest.Close
  END
  IF SELF.Opened
    INIMgr.Update('Process_Manifests',ProgressWindow)      ! Save window data to non-volatile store
  END
      IF L_RG:Created > 0
         CASE MESSAGE('Invoices created: ' & L_RG:Created & '|Invoices existing: ' & L_RG:Exist & |
                  '|Errors: ' & L_RG:Errors & '||Would you like to see a list of MID<39>s of created Transporter Invoices?', 'Process Complete', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ! (whandle, URL, p:Params, p:LauchDir)
            ISExecute(0{PROP:Handle}, 'TransporterInvoices_Batch.txt')
         .
      ELSIF GlobalResponse ~= RequestCancelled
         MESSAGE('None created.||Invoices existing: ' & L_RG:Exist & |
                  '|Errors: ' & L_RG:Errors, 'Process Complete', ICON:Asterisk)
      .
      
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
          SELECT(?Tab_Progress)
          HIDE(?Tab1)
          DISPLAY()
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      ! For all Manifests that are in state later that Loading
  
      ! p:Option
      !   0   - Ask if exists
      !   1   - Dont ask - Dont re-create
      !   2   - Dont ask - Re-create
  
      LOC:Result  = Gen_Transporter_Invoice(MAN:MID, LOC:Option, MAN:CreatedDate)
      IF LOC:Result > 0
         ! Done
         L_RG:Created += 1
  
         ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)  -  Global
         ! (STRING, STRING, <STRING>, BYTE=0, BYTE=0, BYTE=1, BYTE=1)
         Add_Log('Created Transporter invoice for MID: ' & MAN:MID, 'Process_Manifests', 'TransporterInvoices_Batch.txt')
      ELSIF LOC:Result < 0
         ! Error
         L_RG:Errors  += 1
      ELSE
         ! Nothing done (exists?)
         L_RG:Exist   += 1
      .
  
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Print_Creditor_Age_Analysis_Window PROCEDURE 

LOC:Options          GROUP,PRE(L_OG)                       ! 
Run_Date             DATE                                  ! Run with effective date of
MonthEndDay          BYTE(25)                              ! Calculate the aging based on this month day
Ignore_Zero_Balance  BYTE(1)                               ! Ignore debtors / clients that have a zero balance
Output_Option        BYTE                                  ! 
OverrideMonthEndDay  BYTE                                  ! Override Settings month end day
IncludeArchived      BYTE                                  ! 
Orientation          BYTE(1)                               ! 
                     END                                   ! 
Window               WINDOW('Creditor Age Analysis'),AT(,,297,222),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,MDI
                       SHEET,AT(3,4,289,199),USE(?Sheet1)
                         TAB('Options'),USE(?Tab1)
                           GROUP,AT(10,22,195,60),USE(?Group_Date),TRN
                             PROMPT('Run Date:'),AT(10,22),USE(?Run_Date:Prompt),TRN
                             SPIN(@d6b),AT(79,22,68,10),USE(L_OG:Run_Date),RIGHT(1),MSG('Run with effective date of'),TIP('Run with e' & |
  'ffective date of')
                             BUTTON('...'),AT(151,22,12,10),USE(?Calendar)
                             CHECK(' Override Month End Day'),AT(78,42),USE(L_OG:OverrideMonthEndDay),MSG('Override S' & |
  'ettings month end day'),TIP('Override Settings month end day'),TRN
                             GROUP,AT(10,54,137,10),USE(?Group_Day),DISABLE
                               PROMPT('Month End Day:'),AT(10,54),USE(?MonthEndDay:Prompt),TRN
                               SPIN(@n3),AT(79,54,68,10),USE(L_OG:MonthEndDay),RIGHT(1),MSG('Calculate the aging based' & |
  ' on this month day'),TIP('Calculate the aging based on this month day')
                             END
                           END
                           CHECK(' &Ignore Zero Balance'),AT(78,96),USE(L_OG:Ignore_Zero_Balance),MSG('Ignore debt' & |
  'ors / clients that have a zero balance'),TIP('Ignore debtors / clients that have a z' & |
  'ero balance'),TRN
                           PROMPT('Output Option:'),AT(10,120),USE(?L_OG:Output_Option:Prompt),TRN
                           LIST,AT(79,120,83,10),USE(L_OG:Output_Option),VSCROLL,DROP(15),FROM('None|#0|All|#1|Cur' & |
  'rent|#2|30 Days|#3|60 Days|#4|90 Days|#5'),TIP('Output details for the following balances')
                           CHECK(' Include Archived'),AT(78,141),USE(L_OG:IncludeArchived),TRN
                           OPTION('Orientation:'),AT(78,162,152,33),USE(L_OG:Orientation),BOXED
                             RADIO(' Portrait'),AT(90,175),USE(?L_OG:Orientation:Radio1),VALUE('0')
                             RADIO(' Landscape'),AT(157,175),USE(?L_OG:Orientation:Radio2),VALUE('1')
                           END
                         END
                       END
                       BUTTON('&OK'),AT(176,205,55,14),USE(?OkButton),LEFT,ICON('waok.ico'),DEFAULT,FLAT
                       BUTTON('&Cancel'),AT(235,205,,14),USE(?CancelButton),LEFT,ICON('wacancel.ico'),FLAT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Calendar3            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Creditor_Age_Analysis_Window')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Run_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:_Statement_Runs.SetOpenRelated()
  Relate:_Statement_Runs.Open                              ! File _Statement_Runs used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Creditor_Age_Analysis_Window',Window) ! Restore window settings from non-volatile store
  IF ?L_OG:OverrideMonthEndDay{Prop:Checked}
    ENABLE(?Group_Day)
  END
  IF NOT ?L_OG:OverrideMonthEndDay{PROP:Checked}
    DISABLE(?Group_Day)
  END
      L_OG:Run_Date       = TODAY()
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Statement_Runs.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Creditor_Age_Analysis_Window',Window) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar3.SelectOnClose = True
      Calendar3.Ask('Select a Date',L_OG:Run_Date)
      IF Calendar3.Response = RequestCompleted THEN
      L_OG:Run_Date=Calendar3.SelectedDate
      DISPLAY(?L_OG:Run_Date)
      END
      ThisWindow.Reset(True)
    OF ?L_OG:OverrideMonthEndDay
      IF ?L_OG:OverrideMonthEndDay{PROP:Checked}
        ENABLE(?Group_Day)
      END
      IF NOT ?L_OG:OverrideMonthEndDay{PROP:Checked}
        DISABLE(?Group_Day)
      END
      ThisWindow.Reset()
    OF ?OkButton
      ThisWindow.Update()
          ThisWindow.Update
      
          IF L_OG:OverrideMonthEndDay = TRUE
             EXECUTE L_OG:Orientation + 1
               Print_Creditor_Age_Analysis(L_OG:Run_Date, L_OG:MonthEndDay, L_OG:Ignore_Zero_Balance, L_OG:Output_Option, L_OG:IncludeArchived)
               Print_Creditor_Age_Analysis_landscape(L_OG:Run_Date, L_OG:MonthEndDay, L_OG:Ignore_Zero_Balance, L_OG:Output_Option, L_OG:IncludeArchived)
             .
          ELSE
             EXECUTE L_OG:Orientation + 1
               Print_Creditor_Age_Analysis(L_OG:Run_Date,, L_OG:Ignore_Zero_Balance, L_OG:Output_Option, L_OG:IncludeArchived)
               Print_Creditor_Age_Analysis_landscape(L_OG:Run_Date,, L_OG:Ignore_Zero_Balance, L_OG:Output_Option, L_OG:IncludeArchived)
             .
          .
      
          ThisWindow.Reset
      
       POST(EVENT:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

