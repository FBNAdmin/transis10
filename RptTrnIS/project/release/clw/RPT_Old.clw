

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('abbreak.inc'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPT_OLD.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Type is for Continuous  or Laser,  Option is for COD
!!! </summary>
Print_Cont_Invs_old  PROCEDURE  (p:Type, p:Preview, p:From, p:To, p:Un_Printed) ! Declare Procedure
LOC:DIDs_Q           QUEUE,PRE(L_DQ)                       ! 
DID                  ULONG                                 ! Delivery ID
                     END                                   ! 
LOC:Result           LONG                                  ! 
LOC:Idx              LONG                                  ! 
LOC:Count_Invoiced   LONG                                  ! 
LOC:Inv_Result       LONG                                  ! 
LOC:Errors           LONG                                  ! 
LOC:Not_Created      LONG                                  ! 
LOC:Q_Recs           ULONG                                 ! 
LOC:Preview_Option   BYTE                                  ! 
LOC:Print_Result     LONG                                  ! 
Tek_Failed_File     STRING(100)

View_Inv            VIEW(_Invoice)
    PROJECT(INV:IID)
    .


Inv_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
     Access:_Invoice.UseFile()
    ! (p:Type, p:Preview, p:From, p:To, p:Un_Printed)
    ! (BYTE=0, BYTE=1, LONG=0, LONG=0),LONG,PROC
    !   p:Type
    !       0 - Continuous
    !       1 - Page printer
    !   p:Un_Printed
    !       0 - All
    !       1 - Un-Printed only


    ! Print_Man_Invs(MAN:MID, 0,, 0)       ! No preview on auto print

    PUSHBIND
    BIND('INV:InvoiceDate', INV:InvoiceDate)
    BIND('INV:Printed', INV:Printed)

    Inv_View.Init(View_Inv, Relate:_Invoice)
    Inv_View.AddSortOrder()
    Inv_View.AppendOrder('INV:InvoiceDate')
    !Inv_View.AddRange()
    Inv_View.SetFilter('INV:InvoiceDate >= ' & p:From & ' AND INV:InvoiceDate <= ' & p:To,'1')
    IF p:Un_Printed = TRUE
       Inv_View.SetFilter('INV:Printed <> 1','2')
    .

    Inv_View.Reset()
    LOOP
       IF Inv_View.Next() ~= LEVEL:Benign
          BREAK
       .

       IF p:Type = 0
          ! (p:ID, p:PrintType, p:ID_Options, p:Preview_Option)
          ! (ULONG, BYTE, BYTE=0, BYTE=1), LONG, PROC
          Print_Cont(INV:IID, 0,, p:Preview)
       ELSE
          Print_Invoices(, INV:IID)
    .  .

    Inv_View.Kill()
    POPBIND
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:_Invoice.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Print_DN_POD_Cont_old PROCEDURE  (p:MID)                   ! Declare Procedure
LOC:DID_Q            QUEUE,PRE(L_DQ)                       ! 
DID                  ULONG                                 ! Delivery ID
                     END                                   ! 
LOC:Idx              ULONG                                 ! 
LOC:Preview_Option   BYTE                                  ! 
LOC:Print_Result     LONG                                  ! 
Tek_Failed_File     STRING(100)

MAN_Del         VIEW(ManifestLoadAlias)
    PROJECT(A_MAL:MLID, A_MAL:MID, A_MAL:TTID)
       JOIN(A_MALD:FSKey_MLID_DIID, A_MAL:MLID)                                 ! Manifest Load Deliveries
       PROJECT(A_MALD:MLID, A_MALD:DIID, A_MALD:UnitsLoaded)
          JOIN(A_DELI:PKey_DIID, A_MALD:DIID)                                   ! Delivery Items
          PROJECT(A_DELI:DIID, A_DELI:DID, A_DELI:Weight, A_DELI:Units)
    .  .  .


Man_View        ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
     Access:ManifestLoadAlias.UseFile()
     Access:ManifestLoadDeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
    ! (p:MID)
    Man_View.Init(MAN_Del, Relate:ManifestLoadAlias)
    Man_View.AddSortOrder( A_MAL:FKey_MID )
    !Man_View.AppendOrder( '' )
    Man_View.AddRange(A_MAL:MID, p:MID)
    !Man_View.SetFilter( '' )

    Man_View.Reset(1)

    ! Collect a list of unquie DIDs for this MID
    LOOP
       IF Man_View.Next() ~= LEVEL:Benign
          BREAK
       .

       L_DQ:DID     = A_DELI:DID
       GET(LOC:DID_Q, L_DQ:DID)
       IF ERRORCODE()
          L_DQ:DID     = A_DELI:DID
          ADD(LOC:DID_Q)
    .  .

    LOC:Preview_Option  = 2

    LOC:Idx             = 0
    LOOP
       LOC:Idx  += 1
       GET(LOC:DID_Q, LOC:Idx)
       IF ERRORCODE()
          BREAK
       .

       ! Print_Cont(L_DQ:DID, 2, 1)
       IF RECORDS(LOC:DID_Q) > 1
                  ! (p:ID, p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow)
                  ! (ULONG, BYTE      , BYTE=0      , BYTE=1          , BYTE=0), LONG, PROC
          LOC:Print_Result        = Print_Cont(L_DQ:DID, 2, 1, LOC:Preview_Option)
          IF LOC:Print_Result < 0
             BREAK
          ELSIF LOC:Print_Result = 2
             LOC:Preview_Option   = 0       ! No preview
          .
       ELSE
          Print_Cont(L_DQ:DID, 2, 1)
    .  .

    Man_View.Kill()
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:ManifestLoadAlias.Close()
    Relate:ManifestLoadDeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! ---------------------  uses SQL Query class
!!! </summary>
Print_ManagementProfit_Summary_Calc___NOT_USED PROCEDURE  (p_Val_Group) ! Declare Procedure
LOC:Transaction_Log_Name STRING(255)                       ! 
LOC:Settings         GROUP,PRE(L_SG)                       ! 
ExcludeBadDebts      BYTE(1)                               ! Exclude Bad Debt Credit Notes
Output               BYTE                                  ! Output trail of items
Branch1              ULONG                                 ! Branch ID
BranchName1          STRING(35)                            ! Branch Name
Branch2              ULONG                                 ! Branch ID
BranchName2          STRING(35)                            ! Branch Name
                     END                                   ! 
LOC:Options          GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
LOC:Summary_Group    GROUP,PRE(L_SG)                       ! 
Turnover_DBN         DECIMAL(10,2)                         ! 
Turnover_JHB         DECIMAL(10,2)                         ! 
Turnover_Total       DECIMAL(10,2)                         ! 
Credits_DBN          DECIMAL(10,2)                         ! 
Credits_JHB          DECIMAL(10,2)                         ! 
Credits_Total        DECIMAL(10,2)                         ! 
Turnover_less_Credits_DBN DECIMAL(10,2)                    ! 
Turnover_less_Credits_JHB DECIMAL(10,2)                    ! 
Work_Days            LONG                                  ! 
Broking_Kg_DBN       DECIMAL(8)                            ! 
Broking_Kg_JHB       DECIMAL(8)                            ! 
Broking_Kg_Total     DECIMAL(8)                            ! 
Overnight_Kg_DBN     DECIMAL(8)                            ! 
Overnight_Kg_JHB     DECIMAL(8)                            ! 
Overnight_Kg_Total   DECIMAL(8)                            ! 
Kg_Total             DECIMAL(8)                            ! 
Kg_Per_Day           DECIMAL(8)                            ! 
Kg_Per_Day_DBN       DECIMAL(8)                            ! 
Kg_Per_Day_JHB       DECIMAL(8)                            ! 
Average_Cost_Per_Kg  DECIMAL(7,2)                          ! 
Gross_GP_DBN         DECIMAL(10,2)                         ! 
Gross_GP_JHB         DECIMAL(10,2)                         ! 
Gross_GP_Total       DECIMAL(10,2)                         ! 
Gross_GP_Per_Day     DECIMAL(10,2)                         ! 
Loadmaster_DBN       DECIMAL(10,2)                         ! 
Loadmaster_JHB       DECIMAL(10,2)                         ! 
Loadmaster_Total     DECIMAL(10,2)                         ! 
Loadmaster_Per_Day   DECIMAL(10,2)                         ! 
Loadmaster_GP_Total  DECIMAL(10,2)                         ! 
Loadmaster_GP_Per_Day DECIMAL(10,2)                        ! 
Turnover_Per_Day     DECIMAL(10,2)                         ! 
Turnover_Per_Day_DBN DECIMAL(10,2)                         ! 
Turnover_Per_Day_JHB DECIMAL(10,2)                         ! 
Sales_Broking_DBN    DECIMAL(10,2)                         ! 
Sales_Broking_JHB    DECIMAL(10,2)                         ! 
Sales_Overnight_DBN  DECIMAL(10,2)                         ! 
Sales_Overnight_JHB  DECIMAL(10,2)                         ! 
Costs_Broking_DBN    DECIMAL(10,2)                         ! 
Costs_Broking_JHB    DECIMAL(10,2)                         ! 
GP_Broking_DBN       DECIMAL(10,2)                         ! 
GP_Broking_JHB       DECIMAL(10,2)                         ! 
GP_Per_Broking_DBN   DECIMAL(6,2)                          ! 
GP_Per_Broking_JHB   DECIMAL(6,2)                          ! 
GP_Overnight_DBN     DECIMAL(10,2)                         ! 
GP_Overnight_JHB     DECIMAL(10,2)                         ! 
GP_Per_Overnight_DBN DECIMAL(6,2)                          ! 
GP_Per_Overnight_JHB DECIMAL(6,2)                          ! 
Extra_Invoices_DBN   DECIMAL(10,2)                         ! Transporter
Extra_Invoices_JHB   DECIMAL(10,2)                         ! Transporter
Transporter_Credits_DBN DECIMAL(10,2)                      ! 
Transporter_Credits_JHB DECIMAL(10,2)                      ! 
Transporter_Debits_DBN DECIMAL(10,2)                       ! 
Transporter_Debits_JHB DECIMAL(10,2)                       ! 
Sales_Broking_DBN_Total DECIMAL(10,2)                      ! 
Sales_Broking_JHB_Total DECIMAL(10,2)                      ! 
Extra_Leg_DBN        DECIMAL(10,2)                         ! 
Extra_Leg_Broking_DBN DECIMAL(10,2)                        ! 
Extra_Leg_ON_DBN     DECIMAL(10,2)                         ! 
Extra_Leg_JHB        DECIMAL(10,2)                         ! 
Extra_Leg_Broking_JHB DECIMAL(10,2)                        ! 
Extra_Leg_ON_JHB     DECIMAL(10,2)                         ! 
Transporter_Costs_DBN DECIMAL(10,2)                        ! 
Transporter_Costs_JHB DECIMAL(10,2)                        ! 
                     END                                   ! 
LOC:Values_Manifest  GROUP,PRE(L_VG)                       ! 
Kgs_Broking          DECIMAL(7)                            ! 
Kgs_O_Night          DECIMAL(7)                            ! 
T_O_Broking          DECIMAL(10,2)                         ! 
T_O_O_Night          DECIMAL(10,2)                         ! 
C_Note               DECIMAL(10,2)                         ! 
Total_T_O            DECIMAL(10,2)                         ! 
Loadmaster           DECIMAL(10,2)                         ! 
Trans_Broking        DECIMAL(10,2)                         ! 
Extra_Inv            DECIMAL(10,2)                         ! 
Trans_Credit         DECIMAL(10,2)                         ! 
Total_Cost           DECIMAL(10,2)                         ! 
Trans_Debit          DECIMAL(10,2)                         ! 
Total_Cost_Less_Deb  DECIMAL(10,2)                         ! 
GP                   DECIMAL(10)                           ! 
GP_Per               DECIMAL(10,2)                         ! 
MID                  ULONG                                 ! Manifest ID
CreatedDate          DATE                                  ! Created Date
Ext_Invocie_TO       DECIMAL(12,2)                         ! Turn over extra invoice
                     END                                   ! 
!Turnover_Summary        CLASS,TYPE
!
!Get_Inv_Tots        PROCEDURE
!    .
!
!
!
!Calc        Turnover_Summary
!
!
!sql_        SQLQueryClass
!sql_2       SQLQueryClass

  CODE
!    CLEAR(BRA:Record, -1)
!    SET(BRA:PKey_BID)
!    NEXT(Branches)
!    L_SG:Branch1        = BRA:BID
!    L_SG:BranchName1    = BRA:BranchName
!
!    NEXT(Branches)
!    L_SG:Branch2        = BRA:BID
!    L_SG:BranchName2    = BRA:BranchName
!    Calc.Get_Inv_Tots()
!
!
!    p_Val_Group     = p_Val_Group
!
!
!    IF L_VG:T_O_Broking ~= L_SG:Sales_Broking_DBN_Total OR |
!            L_VG:T_O_O_Night = L_SG:Sales_Overnight_DBN
!
!
!    .
!Turnover_Summary.Get_Inv_Tots        PROCEDURE
!R:Type      BYTE
!
!    CODE
!    ! Loop through the Invoice file and gather all information
!    ! Flaw in this code is that it can only deal with 2 branches.
!
!    ! Get_Invoices
!    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info  , p:LogName)
!    ! (LONG  , BYTE    , BYTE=0, BYTE=0        , ULONG=0, <LONG>  , BYTE=0  , BYTE=0    , BYTE=0           , BYTE=0  , <STRING>, <STRING> ),STRING
!    ! p:Option
!    !   0.  - Total Charges inc.
!    !   1.  - Excl VAT
!    !   2.  - VAT
!    !   3.  - Kgs
!    ! p:Type
!    !   0.  - All
!    !   1.  - Invoices                      Total >= 0.0
!    !   2.  - Credit Notes                  Total < 0.0
!    !   3.  - Credit Notes excl Bad Debts   Total < 0.0
!    ! p:Limit_On
!    !   0.  - Date [& Branch]
!    !   1.  - MID
!    ! p:Manifest_Option
!    !   0.  - None
!    !   1.  - Overnight
!    !   2.  - Broking
!
!    R:Type      = 2
!    IF L_SG:ExcludeBadDebts = TRUE
!       R:Type   = 3
!    .
!
!    ! Total inc., Invoices, filter on dates, overnight
!    L_SG:Sales_Overnight_DBN    = Get_Invoices(LO:From_Date, 0, 1,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 1, 2, 'ON', LOC:Transaction_Log_Name)       ! Overnight
!    L_SG:Sales_Overnight_JHB    = Get_Invoices(LO:From_Date, 0, 1,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 1, 2, 'ON', LOC:Transaction_Log_Name)       ! Overnight
!
!    L_SG:Sales_Broking_DBN      = Get_Invoices(LO:From_Date, 0, 1,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 2, 2, 'BR', LOC:Transaction_Log_Name)       ! Broking
!    L_SG:Sales_Broking_JHB      = Get_Invoices(LO:From_Date, 0, 1,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 2, 2, 'BR', LOC:Transaction_Log_Name)       ! Broking
!                                                                          
!
!    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type, p:Output, p:Source)
!    !   1           2       3         4               5           6        7         8          9           10              11     , 12
!
!    L_SG:Extra_Invoices_DBN      = Get_InvoicesTransporter(LO:From_Date, 0, 1, FALSE,,, L_SG:Branch1, LO:To_Date,TRUE,, L_SG:Output,2,, LOC:Transaction_Log_Name)    ! Credits for day
!    L_SG:Extra_Invoices_JHB      = Get_InvoicesTransporter(LO:From_Date, 0, 1, FALSE,,, L_SG:Branch2, LO:To_Date,TRUE,, L_SG:Output,2,, LOC:Transaction_Log_Name)    ! Credits for day
!
!    L_SG:Transporter_Debits_DBN  = Get_InvoicesTransporter(LO:From_Date, 0, 1, TRUE,,, L_SG:Branch1, LO:To_Date,,, L_SG:Output,2,, LOC:Transaction_Log_Name)         ! Credits for day - Transporter not MID
!    L_SG:Transporter_Debits_JHB  = Get_InvoicesTransporter(LO:From_Date, 0, 1, TRUE,,, L_SG:Branch2, LO:To_Date,,, L_SG:Output,2,, LOC:Transaction_Log_Name)         ! Credits for day - Transporter not MID
!
!    L_SG:Transporter_Credits_DBN = -Get_InvoicesTransporter(LO:From_Date, 0, 2,,,, L_SG:Branch1, LO:To_Date,,, L_SG:Output,2,, LOC:Transaction_Log_Name)
!    L_SG:Transporter_Credits_JHB = -Get_InvoicesTransporter(LO:From_Date, 0, 2,,,, L_SG:Branch2, LO:To_Date,,, L_SG:Output,2,, LOC:Transaction_Log_Name)
!
!    ! All extra legs are broking but some are from broking manifests and some are from loadmaster manifests
!    L_SG:Extra_Leg_ON_DBN          = Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2, 'EL', LOC:Transaction_Log_Name, 1)
!    L_SG:Extra_Leg_Broking_DBN     = Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2, 'EL', LOC:Transaction_Log_Name, 2)
!    L_SG:Extra_Leg_ON_JHB          = Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2, 'EL', LOC:Transaction_Log_Name, 1)
!    L_SG:Extra_Leg_Broking_JHB     = Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2, 'EL', LOC:Transaction_Log_Name, 2)
!
!    L_SG:Extra_Leg_DBN             = L_SG:Extra_Leg_ON_DBN + L_SG:Extra_Leg_Broking_DBN
!    L_SG:Extra_Leg_JHB             = L_SG:Extra_Leg_ON_JHB + L_SG:Extra_Leg_Broking_JHB
!
!    L_SG:Sales_Broking_DBN        -= L_SG:Extra_Leg_Broking_DBN     ! 22 Aug - take off broking extra legs, ON are not included
!    L_SG:Sales_Broking_JHB        -= L_SG:Extra_Leg_Broking_JHB
!
!    L_SG:Sales_Broking_DBN_Total   = L_SG:Extra_Leg_DBN + L_SG:Sales_Broking_DBN
!    L_SG:Sales_Broking_JHB_Total   = L_SG:Extra_Leg_JHB + L_SG:Sales_Broking_JHB
!
!    L_SG:Sales_Overnight_DBN      -= L_SG:Extra_Leg_ON_DBN        ! Only remove the overnight extra legs
!    L_SG:Sales_Overnight_JHB      -= L_SG:Extra_Leg_ON_JHB
!
!!    L_SG:Sales_Broking_DBN_Total     += L_SG:Extra_Invoices_DBN + L_SG:Transporter_Credits_DBN - L_SG:Transporter_Debits_DBN
!!    L_SG:Sales_Broking_JHB_Total     += L_SG:Extra_Invoices_JHB + L_SG:Transporter_Credits_JHB - L_SG:Transporter_Debits_JHB
!!    L_SG:Sales_Broking_DBN_Total     += L_SG:Transporter_Credits_DBN - L_SG:Transporter_Debits_DBN
!!    L_SG:Sales_Broking_JHB_Total     += L_SG:Transporter_Credits_JHB - L_SG:Transporter_Debits_JHB
!
!    L_SG:Credits_DBN            = Get_Invoices(LO:From_Date, 0, R:Type,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 0, 2,, LOC:Transaction_Log_Name)    ! total
!    L_SG:Credits_JHB            = Get_Invoices(LO:From_Date, 0, R:Type,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 0, 2,, LOC:Transaction_Log_Name)    ! total
!
!    L_SG:Sales_Overnight_DBN   += L_SG:Credits_DBN       ! neg - excluding credits
!    L_SG:Sales_Overnight_JHB   += L_SG:Credits_JHB       ! neg - excluding credits
!
!    L_SG:Credits_Total          = L_SG:Credits_DBN + L_SG:Credits_JHB
!
!    L_SG:Turnover_DBN           = L_SG:Sales_Broking_DBN_Total + L_SG:Sales_Overnight_DBN - L_SG:Credits_DBN
!    L_SG:Turnover_JHB           = L_SG:Sales_Broking_JHB_Total + L_SG:Sales_Overnight_JHB - L_SG:Credits_JHB
!
!    L_SG:Turnover_less_Credits_DBN = L_SG:Turnover_DBN + L_SG:Credits_DBN
!    L_SG:Turnover_less_Credits_JHB = L_SG:Turnover_JHB + L_SG:Credits_JHB
!
!    L_SG:Turnover_Total         = L_SG:Turnover_less_Credits_DBN + L_SG:Turnover_less_Credits_JHB
!
!    ! Not of invoiced days (working days) - of course will be wrong if people invoice in weekend or public holiday...
!    sql_.PropSQL('SELECT COUNT(DISTINCT LTRIM(DATEPART([year], INVOICEDATEANDTIME)) + <39>/<39> + LTRIM(DATEPART([month], INVOICEDATEANDTIME)) + ' & |
!                 '<39>/<39> + LTRIM(DATEPART([day], INVOICEDATEANDTIME))) ' & |
!                 'FROM _Invoice WHERE InvoiceDateAndTime >= ' & |
!                SQL_Get_DateT_G(LO:From_Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(LO:To_Date + 1,,1))
!    IF sql_.Next_Q() > 0
!       L_SG:Work_Days           = sql_.Data_G.F1
!    .
!!    DO Audit_Check              ! Checks and outputs DIs on WEnd or Public holidays
!
!
!    L_SG:Broking_Kg_DBN         = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 2, 2,, LOC:Transaction_Log_Name)       ! Broking
!    L_SG:Broking_Kg_JHB         = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 2, 2,, LOC:Transaction_Log_Name)       ! Broking
!    L_SG:Broking_Kg_Total       = L_SG:Broking_Kg_DBN + L_SG:Broking_Kg_JHB
!
!    L_SG:Overnight_Kg_DBN       = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 1, 2,, LOC:Transaction_Log_Name)       ! Broking
!    L_SG:Overnight_Kg_JHB       = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 1, 2,, LOC:Transaction_Log_Name)       ! Broking
!    L_SG:Overnight_Kg_Total     = L_SG:Overnight_Kg_DBN + L_SG:Overnight_Kg_JHB
!
!    L_SG:Kg_Total               = L_SG:Broking_Kg_Total + L_SG:Overnight_Kg_Total
!
!    L_SG:Kg_Per_Day             = L_SG:Kg_Total / L_SG:Work_Days
!    L_SG:Kg_Per_Day_DBN         = (L_SG:Broking_Kg_DBN + L_SG:Overnight_Kg_DBN) / L_SG:Work_Days
!    L_SG:Kg_Per_Day_JHB         = (L_SG:Broking_Kg_JHB + L_SG:Overnight_Kg_JHB) / L_SG:Work_Days
!
!
!    ! Get_InvoicesTransporter
!    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type, p:Output, p:Source, p:Info  , p:LogName, p:Manifest_Type)
!    ! (LONG  , BYTE    , BYTE=0, BYTE=0        , BYTE=0        , <ULONG>, ULONG=0, <LONG>  , BYTE=0     , BYTE=0        , BYTE=0  , BYTE=0  , <STRING>, <STRING> , BYTE=0 ),STRING
!    !   1           2       3         4               5           6        7         8          9           10              11        12        13        14         15
!    ! p:Option
!    !   0.  - Total Charges inc.
!    !   1.  - Excl VAT
!    !   2.  - VAT
!    ! p:Type
!    !   0.  - All
!    !   1.  - Invoices                      Total >= 0.0
!    !   2.  - Credit Notes                  Total < 0.0
!    ! p:DeliveryLegs
!    !   0.  - None
!    !   1.  - Del Legs only
!    !   2.  - Both
!    ! p:MID
!    !   0   = Not for a MID
!    !   x   = for a MID
!    ! p:Invoice_Type (was Manifest_Type)                        10
!    !   0   - All
!    !   1   - Overnight
!    !   2   - Broking
!    ! p:Manifest_Type                                           15
!    !   0   - All
!    !   1   - Overnight
!    !   2   - Broking
!    ! p:Extra_Inv
!    !   Only
!    ! p:Source
!    !   1   - Management Profit
!    !   2   - Management Profit Summary
!
!    ! Delivery legs (1), Broking Manifest type
!!   L_SG:Sales_Broking_DBN     += Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2)
!!   L_SG:Sales_Broking_JHB     += Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2)
!
!    ! Delivery legs & ??? (2), Broking Manifest type
!    L_SG:Costs_Broking_DBN      = Get_InvoicesTransporter(LO:From_Date, 0, 0,,2,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2,, LOC:Transaction_Log_Name)
!    L_SG:Costs_Broking_JHB      = Get_InvoicesTransporter(LO:From_Date, 0, 0,,2,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2,, LOC:Transaction_Log_Name)
!
!    L_SG:Costs_Broking_DBN     += L_SG:Extra_Leg_DBN
!    L_SG:Costs_Broking_JHB     += L_SG:Extra_Leg_JHB
!
!    L_SG:Transporter_Costs_DBN  = L_SG:Costs_Broking_DBN + L_SG:Extra_Invoices_DBN + L_SG:Transporter_Debits_DBN - L_SG:Transporter_Credits_DBN
!    L_SG:Transporter_Costs_JHB  = L_SG:Costs_Broking_JHB + L_SG:Extra_Invoices_JHB + L_SG:Transporter_Debits_JHB - L_SG:Transporter_Credits_JHB
!
!    ! Overnight Manifest type
!    L_SG:Loadmaster_DBN         = Get_InvoicesTransporter(LO:From_Date, 0, 1,,,, L_SG:Branch1, LO:To_Date,, 1, L_SG:Output, 2,, LOC:Transaction_Log_Name)
!    L_SG:Loadmaster_JHB         = Get_InvoicesTransporter(LO:From_Date, 0, 1,,,, L_SG:Branch2, LO:To_Date,, 1, L_SG:Output, 2,, LOC:Transaction_Log_Name)
!
!
!
!    L_SG:Average_Cost_Per_Kg    = L_SG:Turnover_Total / L_SG:Kg_Total
!            !(L_SG:Costs_Broking_DBN + L_SG:Costs_Broking_JHB + L_SG:Loadmaster_DBN + L_SG:Loadmaster_JHB) / L_SG:Kg_Total
!
!    L_SG:Turnover_Per_Day       = L_SG:Turnover_Total / L_SG:Work_Days
!    L_SG:Turnover_Per_Day_DBN   = L_SG:Turnover_DBN / L_SG:Work_Days
!    L_SG:Turnover_Per_Day_JHB   = L_SG:Turnover_JHB / L_SG:Work_Days
!
!    L_SG:GP_Broking_DBN         = L_SG:Sales_Broking_DBN_Total - L_SG:Transporter_Costs_DBN
!    L_SG:GP_Broking_JHB         = L_SG:Sales_Broking_JHB_Total - L_SG:Transporter_Costs_JHB
!
!    L_SG:GP_Per_Broking_DBN     = (L_SG:GP_Broking_DBN / L_SG:Sales_Broking_DBN_Total) * 100
!    L_SG:GP_Per_Broking_JHB     = (L_SG:GP_Broking_JHB / L_SG:Sales_Broking_JHB_Total) * 100
!
!    L_SG:GP_Overnight_DBN       = L_SG:Sales_Overnight_DBN - L_SG:Loadmaster_DBN
!    !L_SG:GP_Overnight_DBN      += L_SG:Extra_Invoices_DBN + L_SG:Transporter_Credits_DBN - L_SG:Transporter_Debits_DBN
!    L_SG:GP_Overnight_JHB       = L_SG:Sales_Overnight_JHB - L_SG:Loadmaster_JHB
!    !L_SG:GP_Overnight_JHB      += L_SG:Extra_Invoices_JHB + L_SG:Transporter_Credits_JHB - L_SG:Transporter_Debits_JHB
!
!    L_SG:GP_Per_Overnight_DBN   = (L_SG:GP_Overnight_DBN / L_SG:Sales_Overnight_DBN) * 100
!    L_SG:GP_Per_Overnight_JHB   = (L_SG:GP_Overnight_JHB / L_SG:Sales_Overnight_JHB) * 100
!
!
!!    L_SG:Gross_GP_DBN           = L_SG:Turnover_less_Credits_DBN - (L_SG:Costs_Broking_DBN + L_SG:Loadmaster_DBN)
!!    L_SG:Gross_GP_JHB           = L_SG:Turnover_less_Credits_JHB - (L_SG:Costs_Broking_JHB + L_SG:Loadmaster_JHB)
!
!    L_SG:Gross_GP_DBN           = L_SG:GP_Overnight_DBN + L_SG:GP_Broking_DBN
!    L_SG:Gross_GP_JHB           = L_SG:GP_Overnight_JHB + L_SG:GP_Broking_JHB
!
!    L_SG:Gross_GP_Total         = L_SG:Gross_GP_DBN + L_SG:Gross_GP_JHB
!    L_SG:Gross_GP_Per_Day       = L_SG:Gross_GP_Total / L_SG:Work_Days
!
!
!    L_SG:Loadmaster_Total       = L_SG:Loadmaster_DBN + L_SG:Loadmaster_JHB
!    L_SG:Loadmaster_Per_Day     = L_SG:Loadmaster_Total / L_SG:Work_Days
!
!    L_SG:Loadmaster_GP_Total    = L_SG:Loadmaster_Total + L_SG:Gross_GP_Total   ! Note loadmaster is not seen as a cost here but an income
!    L_SG:Loadmaster_GP_Per_Day  = L_SG:Loadmaster_GP_Total / L_SG:Work_Days
!    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! Type is for Continuous or Laser,  Option is for COD
!!! </summary>
Print_Man_Invs_old   PROCEDURE  (p:MID, p:Type, p:Option, p:Preview) ! Declare Procedure
LOC:DIDs_Q           QUEUE,PRE(L_DQ)                       ! 
DID                  ULONG                                 ! Delivery ID
                     END                                   ! 
LOC:Result           LONG                                  ! 
LOC:Idx              LONG                                  ! 
LOC:Count_Invoiced   LONG                                  ! 
LOC:Inv_Result       LONG                                  ! 
LOC:Errors           LONG                                  ! 
LOC:Not_Created      LONG                                  ! 
LOC:Q_Recs           ULONG                                 ! 
LOC:Preview_Option   BYTE                                  ! 
LOC:Print_Result     LONG                                  ! 
Tek_Failed_File     STRING(100)

MAN_Del         VIEW(ManifestLoadAlias)
    PROJECT(A_MAL:MLID, A_MAL:MID)
       JOIN(A_MALD:FSKey_MLID_DIID, A_MAL:MLID)
       PROJECT(A_MALD:MLID, A_MALD:DIID)
          JOIN(A_DELI:PKey_DIID, A_MALD:DIID)
          PROJECT(A_DELI:DIID, A_DELI:DID)
             JOIN(A_DEL:PKey_DID, A_DELI:DID)
             PROJECT(A_DEL:DID, A_DEL:DINo, A_DEL:DIDate, A_DEL:Terms)
    .  .  .  .


Del_View        ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
     Access:ManifestLoadAlias.UseFile()
     Access:ManifestLoadDeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
     Access:DeliveriesAlias.UseFile()
    ! (p:MID, p:Type, p:Option, p:Preview)
    ! (ULONG, BYTE, BYTE=0, BYTE=1),LONG,PROC
    !   p:Option
    !       0 - COD invoices
    !       1 - All
    !   p:Type
    !       0 - Continuous
    !       1 - Page printer


    ! Print_Man_Invs(MAN:MID, 0,, 0)       ! No preview on auto print

    !       Print_Man_Invs(MAN:MID, 0, 1)



    PUSHBIND
    BIND('A_DEL:Terms', A_DEL:Terms)

    Del_View.Init(MAN_Del, Relate:ManifestLoadAlias)
    Del_View.AddSortOrder(A_MAL:FKey_MID)
    Del_View.AppendOrder('A_DEL:DID,A_DELI:DIID')
    Del_View.AddRange(A_MAL:MID, p:MID)

    IF p:Option = 0
       ! Pre Paid|COD|Account
       Del_View.SetFilter('A_DEL:Terms = 1')
    .


    !MAN_Del{PROP:Filter}    = 'A_MAL:MID = ' & p:MID
    !MAN_Del{PROP:Order}     = 'A_DEL:DID,A_DELI:DIID'
    !OPEN(MAN_Del)
    !SET(MAN_Del)
    Del_View.Reset()
    LOOP
       IF Del_View.Next() ~= LEVEL:Benign
          BREAK
       .

       ! We will have every Item entry listed when we really just want the DIDs.
       ! DID will not be unique
       L_DQ:DID     = A_DEL:DID
       GET(LOC:DIDs_Q, L_DQ:DID)
       IF ERRORCODE()
          L_DQ:DID  = A_DEL:DID
          ADD(LOC:DIDs_Q)
    .  .

    LOC:Preview_Option  = 2
    LOC:Q_Recs          = RECORDS(LOC:DIDs_Q)

    IF LOC:Q_Recs > 0
       LOC:Idx      = 0
       LOOP
          LOC:Idx  += 1
          GET(LOC:DIDs_Q, LOC:Idx)
          IF ERRORCODE()
             BREAK
          .

          CASE p:Type
          OF 0
             IF LOC:Q_Recs > 1
                        ! (p:ID, p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow)
                        ! (ULONG, BYTE      , BYTE=0      , BYTE=1          , BYTE=0), LONG, PROC
                LOC:Print_Result        = Print_Cont(L_DQ:DID, 0, 1, LOC:Preview_Option)
                IF LOC:Print_Result < 0
                   BREAK
                ELSIF LOC:Print_Result = 2
                   LOC:Preview_Option   = 0       ! No preview
                .
             ELSE
                Print_Cont(L_DQ:DID, 0, 1)
             .
          OF 1
             Print_Invoices(L_DQ:DID)
          .

          LOC:Count_Invoiced += 1
       .
    ELSE
    .

    Del_View.Kill()
    POPBIND
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:ManifestLoadAlias.Close()
    Relate:ManifestLoadDeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Relate:DeliveriesAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Report
!!! ---------------------  uses SQL Query class
!!! </summary>
Print_ManagementProfit_Summary_old PROCEDURE 

!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
TintRptViewer  TinReportViewer
!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
Progress:Thermometer BYTE                                  ! 
LOC:Errors           ULONG                                 ! 
LOC:Transaction_Log_Name STRING(255)                       ! 
LOC:Settings         GROUP,PRE(L_SG)                       ! 
ExcludeBadDebts      BYTE(1)                               ! Exclude Bad Debt Credit Notes
Output               BYTE                                  ! Output trail of items
Branch1              ULONG                                 ! Branch ID
BranchName1          STRING(35)                            ! Branch Name
Branch2              ULONG                                 ! Branch ID
BranchName2          STRING(35)                            ! Branch Name
                     END                                   ! 
LOC:Options          GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
LOC:Summary_Group    GROUP,PRE(L_SG)                       ! 
Turnover_DBN         DECIMAL(10,2)                         ! 
Turnover_JHB         DECIMAL(10,2)                         ! 
Turnover_Total       DECIMAL(10,2)                         ! 
Credits_DBN          DECIMAL(10,2)                         ! 
Credits_JHB          DECIMAL(10,2)                         ! 
Credits_Total        DECIMAL(10,2)                         ! 
Turnover_less_Credits_DBN DECIMAL(10,2)                    ! 
Turnover_less_Credits_JHB DECIMAL(10,2)                    ! 
Work_Days            LONG                                  ! 
Broking_Kg_DBN       DECIMAL(8)                            ! 
Broking_Kg_JHB       DECIMAL(8)                            ! 
Broking_Kg_Total     DECIMAL(8)                            ! 
Overnight_Kg_DBN     DECIMAL(8)                            ! 
Overnight_Kg_JHB     DECIMAL(8)                            ! 
Overnight_Kg_Total   DECIMAL(8)                            ! 
Kg_Total             DECIMAL(8)                            ! 
Kg_Per_Day           DECIMAL(8)                            ! 
Kg_Per_Day_DBN       DECIMAL(8)                            ! 
Kg_Per_Day_JHB       DECIMAL(8)                            ! 
Average_Cost_Per_Kg  DECIMAL(7,2)                          ! 
Gross_GP_DBN         DECIMAL(10,2)                         ! 
Gross_GP_JHB         DECIMAL(10,2)                         ! 
Gross_GP_Total       DECIMAL(10,2)                         ! 
Gross_GP_Per_Day     DECIMAL(10,2)                         ! 
Loadmaster_DBN       DECIMAL(10,2)                         ! 
Loadmaster_JHB       DECIMAL(10,2)                         ! 
Loadmaster_Total     DECIMAL(10,2)                         ! 
Loadmaster_Per_Day   DECIMAL(10,2)                         ! 
Loadmaster_GP_Total  DECIMAL(10,2)                         ! 
Loadmaster_GP_Per_Day DECIMAL(10,2)                        ! 
Turnover_Per_Day     DECIMAL(10,2)                         ! 
Turnover_Per_Day_DBN DECIMAL(10,2)                         ! 
Turnover_Per_Day_JHB DECIMAL(10,2)                         ! 
Sales_Broking_DBN    DECIMAL(10,2)                         ! 
Sales_Broking_JHB    DECIMAL(10,2)                         ! 
Sales_Overnight_DBN  DECIMAL(10,2)                         ! 
Sales_Overnight_JHB  DECIMAL(10,2)                         ! 
Costs_Broking_DBN    DECIMAL(10,2)                         ! 
Costs_Broking_JHB    DECIMAL(10,2)                         ! 
GP_Broking_DBN       DECIMAL(10,2)                         ! 
GP_Broking_JHB       DECIMAL(10,2)                         ! 
GP_Per_Broking_DBN   DECIMAL(6,2)                          ! 
GP_Per_Broking_JHB   DECIMAL(6,2)                          ! 
GP_Overnight_DBN     DECIMAL(10,2)                         ! 
GP_Overnight_JHB     DECIMAL(10,2)                         ! 
GP_Per_Overnight_DBN DECIMAL(6,2)                          ! 
GP_Per_Overnight_JHB DECIMAL(6,2)                          ! 
Extra_Invoices_DBN   DECIMAL(10,2)                         ! Transporter
Extra_Invoices_JHB   DECIMAL(10,2)                         ! Transporter
Transporter_Credits_DBN DECIMAL(10,2)                      ! 
Transporter_Credits_JHB DECIMAL(10,2)                      ! 
Transporter_Debits_DBN DECIMAL(10,2)                       ! 
Transporter_Debits_JHB DECIMAL(10,2)                       ! 
Sales_Broking_DBN_Total DECIMAL(10,2)                      ! 
Sales_Broking_JHB_Total DECIMAL(10,2)                      ! 
Extra_Leg_DBN        DECIMAL(10,2)                         ! 
Extra_Leg_Broking_DBN DECIMAL(10,2)                        ! 
Extra_Leg_ON_DBN     DECIMAL(10,2)                         ! 
Extra_Leg_JHB        DECIMAL(10,2)                         ! 
Extra_Leg_Broking_JHB DECIMAL(10,2)                        ! 
Extra_Leg_ON_JHB     DECIMAL(10,2)                         ! 
Transporter_Costs_DBN DECIMAL(10,2)                        ! 
Transporter_Costs_JHB DECIMAL(10,2)                        ! 
                     END                                   ! 
Process:View         VIEW(Setup)
                     END
FDB4::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
FDB7::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
ProgressWindow       WINDOW('Report - Management Profit Summary'),AT(,,195,126),FONT('MS Sans Serif',8,,FONT:regular), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       SHEET,AT(4,2,188,102),USE(?Sheet1)
                         TAB('Settings'),USE(?Tab2)
                           CHECK(' Exclude Bad Debts'),AT(78,24),USE(L_SG:ExcludeBadDebts),MSG('Exclude Bad Debt C' & |
  'redit Notes'),TIP('Exclude Bad Debt Credit Notes')
                           PROMPT('Audit Output:'),AT(10,42),USE(?L_SG:Output_Trail:Prompt)
                           LIST,AT(78,42,60,10),USE(L_SG:Output),DROP(5),FROM('None|#0|All|#1'),MSG('Output trail of items'), |
  TIP('Output trail of items')
                           PROMPT('Branch Name 1:'),AT(10,65),USE(?L_SG:BranchName1:Prompt:2)
                           LIST,AT(78,66,109,10),USE(L_SG:BranchName1),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                           PROMPT('Branch Name 2:'),AT(10,81),USE(?BranchName2:Prompt:2)
                           LIST,AT(78,81,109,10),USE(BRA:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop:1),MSG('Branch Name')
                         END
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(22,34,151,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(15,22,165,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(15,50,165,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Cancel'),AT(142,108,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                       BUTTON('Pause'),AT(88,108,49,15),USE(?Pause),LEFT,ICON('Blue_R.ico'),FLAT
                     END

Report               REPORT('Manifest Report'),AT(250,833,9302,6979),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,9302,583),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Management Profit Summary'),AT(0,21,9300),USE(?ReportTitle),FONT(,12,,FONT:bold),CENTER
                         STRING('To:'),AT(4979,313),USE(?String19:49),FONT(,10,,,CHARSET:ANSI),TRN
                         STRING('From:'),AT(3469,313),USE(?String19:48),FONT(,10,,,CHARSET:ANSI),TRN
                         STRING(@d5b),AT(3854,313),USE(LO:From_Date),FONT(,10,,,CHARSET:ANSI),RIGHT(1)
                         STRING(@d5b),AT(5240,313),USE(LO:To_Date),FONT(,10,,,CHARSET:ANSI),RIGHT(1)
                       END
Detail                 DETAIL,AT(,,10604,6792),USE(?Detail)
                         BOX,AT(6458,104,2604,3229),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(2)
                         STRING('Durban'),AT(6563,10,1094,208),USE(?String19:58),CENTER
                         STRING('Turnover DBN:'),AT(198,219),USE(?String19),TRN
                         STRING(@n-14.2),AT(1417,219),USE(L_SG:Turnover_DBN),RIGHT(1)
                         STRING(@n-14.2),AT(8031,4281,833,167),USE(L_SG:Costs_Broking_JHB),RIGHT(1)
                         STRING('Costs Broking (incl. EL):'),AT(6625,4281),USE(?String19:39),TRN
                         STRING(@n-14.2),AT(4115,5760),USE(L_SG:Loadmaster_GP_Per_Day),RIGHT(1)
                         STRING(@n-14.2),AT(8031,1573,833,167),USE(L_SG:Transporter_Credits_DBN),FONT(,,COLOR:Red,, |
  CHARSET:ANSI),RIGHT(1)
                         STRING('Transporter Credits:'),AT(6625,1365,1219,167),USE(?String19:51),TRN
                         STRING('Transporter Debits:'),AT(6625,1573,958,167),USE(?String19:52),TRN
                         STRING('Turnover JHB:'),AT(198,656),USE(?String19:2),TRN
                         STRING(@n-14.2),AT(1417,656),USE(L_SG:Turnover_JHB),RIGHT(1)
                         STRING(@n-14.2),AT(8031,2115,833,167),USE(L_SG:GP_Broking_DBN),RIGHT(1)
                         STRING(@n-14.2),AT(1417,6198),USE(L_SG:Turnover_Per_Day),RIGHT(1)
                         STRING('Turnover Per Day:'),AT(198,6198),USE(?String19:31),TRN
                         STRING(@n-14.2),AT(8031,5458,833,167),USE(L_SG:GP_Broking_JHB),RIGHT(1)
                         STRING('Turnover Total:'),AT(2656,1135),USE(?String19:3),TRN
                         STRING('Turnover Per Day JHB:'),AT(2688,6510),USE(?String19:33),TRN
                         STRING('Gross GP DBN:'),AT(198,4031),USE(?String19:21),TRN
                         STRING('Sales Broking:'),AT(6625,219),USE(?String19:34),TRN
                         STRING(@n-14.2),AT(4115,1135),USE(L_SG:Turnover_Total),RIGHT(1)
                         STRING('Sales Broking Total:'),AT(6625,677,990,167),USE(?String19:56),TRN
                         STRING(@n-14.2),AT(8031,677,833,167),USE(L_SG:Sales_Broking_DBN_Total),RIGHT(1)
                         STRING(@n-14.2),AT(4115,6198),USE(L_SG:Turnover_Per_Day_DBN),RIGHT(1)
                         STRING(@N-9.2~%~),AT(8031,2344,833,167),USE(L_SG:GP_Per_Broking_DBN),RIGHT(1)
                         STRING('GP % Broking:'),AT(6625,2365),USE(?String19:42),TRN
                         STRING('Credits DBN:'),AT(198,438),USE(?String19:4),TRN
                         STRING(@n-14.2),AT(1417,438),USE(L_SG:Credits_DBN),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1)
                         STRING(@n-14.2),AT(4115,6510),USE(L_SG:Turnover_Per_Day_JHB),RIGHT(1)
                         STRING('Gross GP JHB:'),AT(198,4271),USE(?String19:22),TRN
                         STRING(@N-9.2~%~),AT(8031,5698,833,167),USE(L_SG:GP_Per_Broking_JHB),RIGHT(1)
                         STRING('GP % Broking:'),AT(6625,5698),USE(?String19:43),TRN
                         STRING('Credits JHB:'),AT(198,885),USE(?String19:5),TRN
                         STRING(@n-14.2),AT(1417,885),USE(L_SG:Credits_JHB),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1)
                         STRING(@n-14.2),AT(8031,219),USE(L_SG:Sales_Broking_DBN),RIGHT(1)
                         STRING('Extra Legs:'),AT(6625,438,552,167),USE(?String19:59),TRN
                         STRING('Gross GP Total:'),AT(198,4594),USE(?String19:23),TRN
                         STRING('Credits Total:'),AT(198,1188),USE(?String19:6),TRN
                         STRING(@n-14.2),AT(8031,1365,833,167),USE(L_SG:Transporter_Debits_DBN),RIGHT(1)
                         STRING(@n-14.2),AT(1417,1188),USE(L_SG:Credits_Total),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1)
                         STRING('Sales Overnight:'),AT(6625,2677),USE(?String19:36),TRN
                         STRING(@n-14.2),AT(8031,3594,833,167),USE(L_SG:Sales_Broking_JHB),RIGHT(1)
                         STRING(@n-14.2),AT(8031,2875,833,167),USE(L_SG:GP_Overnight_DBN),RIGHT(1)
                         STRING('Extra Transporter Invoices:'),AT(6625,4500),USE(?String19:54),TRN
                         STRING(@n-14.2),AT(8031,4708,833,167),USE(L_SG:Transporter_Debits_JHB),RIGHT(1)
                         STRING('GP Overnight:'),AT(6625,2875),USE(?String19:44),TRN
                         STRING('Turnover less Credits DBN:'),AT(2656,438),USE(?String19:7),TRN
                         STRING(@n-14.2),AT(4115,438),USE(L_SG:Turnover_less_Credits_DBN),RIGHT(1)
                         STRING(@n-14.2),AT(8031,438,833,167),USE(L_SG:Extra_Leg_DBN),RIGHT(1)
                         STRING('Loadmaster DBN:'),AT(198,4917),USE(?String19:25),TRN
                         STRING(@n-14.2),AT(8031,2677,833,167),USE(L_SG:Sales_Overnight_DBN),RIGHT(1)
                         STRING(@n-14.2),AT(8031,6177,833,167),USE(L_SG:GP_Overnight_JHB),RIGHT(1)
                         STRING(@N-9.2~%~),AT(8031,3104,833,167),USE(L_SG:GP_Per_Overnight_DBN),RIGHT(1)
                         STRING('Johannesburg'),AT(6563,3375,1094,208),USE(?String19:61),CENTER
                         STRING('Turnover less Credits JHB:'),AT(2656,885),USE(?String19:8),TRN
                         STRING(@n-14.2),AT(4115,885),USE(L_SG:Turnover_less_Credits_JHB),RIGHT(1)
                         STRING(@n-14.2),AT(8031,5969,833,167),USE(L_SG:Sales_Overnight_JHB),RIGHT(1)
                         STRING(@n-14.2),AT(8031,4906,833,167),USE(L_SG:Transporter_Credits_JHB),FONT(,,COLOR:Red,, |
  CHARSET:ANSI),RIGHT(1)
                         STRING('Sales Overnight:'),AT(6625,5969),USE(?String19:37),TRN
                         STRING(@n-14.2),AT(8031,958,833,167),USE(L_SG:Costs_Broking_DBN),RIGHT(1)
                         STRING(@n-14),AT(4135,1542,813,167),USE(L_SG:Work_Days),RIGHT(1),TRN
                         STRING('GP Broking:'),AT(6625,2115),USE(?String19:40),TRN
                         STRING('Work Days:'),AT(2656,1542),USE(?String19:9),TRN
                         STRING('Loadmaster Total:'),AT(198,5479),USE(?String19:27),TRN
                         STRING(@N-9.2~%~),AT(8031,6417,833,167),USE(L_SG:GP_Per_Overnight_JHB),RIGHT(1)
                         STRING('Broking Kg DBN:'),AT(198,1542),USE(?String19:10),TRN
                         STRING(@n-11.0),AT(1594,1542),USE(L_SG:Broking_Kg_DBN),RIGHT(1)
                         STRING('GP Broking:'),AT(6625,5458),USE(?String19:41),TRN
                         STRING(@n-14.2),AT(1417,4021),USE(L_SG:Gross_GP_DBN),RIGHT(1)
                         STRING('Broking Kg JHB:'),AT(198,1802),USE(?String19:11),TRN
                         STRING(@n-11.0),AT(1594,1802),USE(L_SG:Broking_Kg_JHB),RIGHT(1)
                         STRING('Total Cost Broking:'),AT(6625,1833),USE(?String19:62),TRN
                         STRING('Loadmaster + GP Total:'),AT(198,5760),USE(?String19:29),TRN
                         STRING(@n-14.2),AT(1417,4271),USE(L_SG:Gross_GP_JHB),RIGHT(1)
                         STRING('Sales Broking:'),AT(6625,3594),USE(?String19:35),TRN
                         STRING('Broking Kg Total:'),AT(198,2094),USE(?String19:12),TRN
                         STRING('Loadmaster GP Per Day:'),AT(2708,5760),USE(?String19:30),TRN
                         STRING(@n-11.0),AT(1594,2094),USE(L_SG:Broking_Kg_Total),RIGHT(1)
                         STRING(@n-14.2),AT(1417,4594),USE(L_SG:Gross_GP_Total),RIGHT(1)
                         STRING('Overnight Kg DBN:'),AT(198,2438),USE(?String19:13),TRN
                         STRING(@n-11.0),AT(1594,2417),USE(L_SG:Overnight_Kg_DBN),RIGHT(1)
                         STRING(@n-14.2),AT(4115,4594),USE(L_SG:Gross_GP_Per_Day),RIGHT(1)
                         STRING(@n-14.2),AT(8031,4031),USE(L_SG:Sales_Broking_JHB_Total),RIGHT(1)
                         STRING('Sales Broking Total:'),AT(6625,4031),USE(?String19:57)
                         STRING(@n-14.2),AT(8031,4500,833,167),USE(L_SG:Extra_Invoices_JHB),RIGHT(1)
                         STRING('Gross GP Per Day:'),AT(2688,4594),USE(?String19:24),TRN
                         STRING(@n-11.0),AT(1594,2729),USE(L_SG:Overnight_Kg_JHB),RIGHT(1)
                         STRING('GP Overnight:'),AT(6625,6198),USE(?String19:45),TRN
                         STRING(@n-14.2),AT(1417,4917),USE(L_SG:Loadmaster_DBN),RIGHT(1)
                         STRING('Total Cost Broking:'),AT(6625,5146),USE(?String19:63),TRN
                         STRING(@n-14.2),AT(8031,5146,833,167),USE(L_SG:Transporter_Costs_JHB),RIGHT(1),TRN
                         STRING('Costs Broking (incl. EL):'),AT(6625,958),USE(?String19:38),TRN
                         STRING('Kg Per Day DBN:'),AT(2656,2417),USE(?String19:18),TRN
                         STRING('Overnight Kg JHB:'),AT(198,2729),USE(?String19:14),TRN
                         STRING(@n-11.0),AT(4292,2417),USE(L_SG:Kg_Per_Day_DBN),RIGHT(1)
                         STRING(@n-11.0),AT(1594,3042),USE(L_SG:Overnight_Kg_Total),RIGHT(1)
                         STRING('GP % Overnight:'),AT(6625,3104),USE(?String19:46),TRN
                         STRING(@n-14.2),AT(1417,5167),USE(L_SG:Loadmaster_JHB),RIGHT(1)
                         STRING('Loadmaster JHB:'),AT(198,5167),USE(?String19:26),TRN
                         STRING('Overnight Kg Total:'),AT(198,3042),USE(?String19:15),TRN
                         STRING(@n-11.0),AT(4292,2729),USE(L_SG:Kg_Per_Day_JHB),RIGHT(1)
                         STRING(@n-11.0),AT(1594,3406),USE(L_SG:Kg_Total),RIGHT(1)
                         BOX,AT(6458,3448,2604,3229),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(2)
                         GROUP('Group - Hide'),AT(3490,3542,1927,990),USE(?Group1),BOXED,HIDE
                           STRING(@n-14.2),AT(4375,3979),USE(L_SG:Extra_Leg_ON_JHB),RIGHT(1)
                           STRING('EL ON:'),AT(3646,3990),USE(?String19:65),TRN
                           STRING('EL:'),AT(3646,4240),USE(?String19:66),TRN
                           STRING(@n-14.2),AT(4375,4240),USE(L_SG:Extra_Leg_JHB,,?L_SG:Extra_Leg_JHB:2),RIGHT(1)
                           STRING('EL Broking:'),AT(3646,3719),USE(?String19:64),TRN
                           STRING(@n-14.2),AT(4375,3719),USE(L_SG:Extra_Leg_Broking_JHB),RIGHT(1)
                         END
                         STRING('Transporter Debits:'),AT(6625,4906),USE(?String19:53),TRN
                         STRING('GP % Overnight:'),AT(6625,6417),USE(?String19:47),TRN
                         STRING('Kg Per Day JHB:'),AT(2656,2729),USE(?String19:19),TRN
                         STRING('Turnover Per Day DBN:'),AT(2688,6198),USE(?String19:32),TRN
                         STRING('Kg Total:'),AT(198,3406),USE(?String19:16),TRN
                         STRING(@n-14.2),AT(1417,5479),USE(L_SG:Loadmaster_Total),RIGHT(1)
                         STRING(@n-10.2),AT(1635,3635),USE(L_SG:Average_Cost_Per_Kg),RIGHT(1)
                         STRING('Extra Legs:'),AT(6625,3813),USE(?String19:60)
                         STRING(@n-14.2),AT(8031,3813,833,167),USE(L_SG:Extra_Leg_JHB),RIGHT(1)
                         STRING(@n-11.0),AT(4292,3042),USE(L_SG:Kg_Per_Day),RIGHT(1)
                         STRING('Transporter Credits:'),AT(6625,4708),USE(?String19:55),TRN
                         STRING('Kg Per Day Total:'),AT(2667,3042),USE(?String19:17),TRN
                         STRING(@n-14.2),AT(4115,5479),USE(L_SG:Loadmaster_Per_Day),RIGHT(1)
                         STRING(@n-14.2),AT(8031,1156,833,167),USE(L_SG:Extra_Invoices_DBN),RIGHT(1)
                         STRING(@n-14.2),AT(8031,1833,833,167),USE(L_SG:Transporter_Costs_DBN),RIGHT(1)
                         STRING('Extra Transporter Invoices:'),AT(6625,1156),USE(?String19:50),TRN
                         STRING('Loadmaster Per Day:'),AT(2708,5479),USE(?String19:28),TRN
                         STRING('Avg. Cost Per Kg:'),AT(198,3635),USE(?String19:20),TRN
                         STRING(@n-14.2),AT(1417,5760),USE(L_SG:Loadmaster_GP_Total),RIGHT(1)
                       END
                       FOOTER,AT(250,7813,9302,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(8365,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ReportMemoryRecords     BYTE(0)                            ! Used to do the first Next call
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Next                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END

TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

Turnover_Summary        CLASS,TYPE

Get_Inv_Tots        PROCEDURE
    .



Calc        Turnover_Summary


sql_        SQLQueryClass
sql_2       SQLQueryClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Audit_Check             ROUTINE
    DATA

R:Date      DATE
R:Log       BYTE
R:Count     LONG
R:Dates     STRING(255)

    CODE
    R:Log   = L_SG:Output
    LOOP 2 TIMES
       R:Count  = 0
       R:Dates  = ''

       IF R:Log = 1
          ! Audit is for all - run query to show all DIs being included in the above date range that fall on a weekend or public holiday
          Add_Log('[DIs on Weekends and Public Holidays]', 'Print_Turnover_Summary', 'Print Turnover Summary - Holiday DIs - ' & FORMAT(TODAY(), @d7) & '.csv',, 1)
       .

       sql_.PropSQL('SELECT DISTINCT LTRIM(DATEPART([year], INVOICEDATEANDTIME)) + <39>/<39> + LTRIM(DATEPART([month], INVOICEDATEANDTIME)) + ' & |
                    '<39>/<39> + LTRIM(DATEPART([day], INVOICEDATEANDTIME)) ' & |
                    'FROM _Invoice WHERE InvoiceDateAndTime >= ' & |
                   SQL_Get_DateT_G(LO:From_Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(LO:To_Date + 1,,1))
       LOOP
          IF sql_.Next_Q() <= 0
             BREAK
          .

          R:Date        = DEFORMAT(sql_.Data_G.F1, @d10)

          ! Weekends and public holidays
          IF Check_PublicHoliday(R:Date) > 0 OR R:Date % 7 = 0 OR R:Date % 7 = 6
             R:Dates    = CLIP(R:Dates) & ', ' & FORMAT(R:Date,@d6)

             ! Output all these DI No.
             sql_2.PropSQL('SELECT DINo, Total FROM _Invoice WHERE InvoiceDateAndTime >= ' & |
                         SQL_Get_DateT_G(R:Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(R:Date + 1,,1))

             LOOP
                IF sql_2.Next_Q() <= 0
                   BREAK
                .

                R:Count += 1

                IF R:Log = 1
                   ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
                   Add_Log('Public Holiday / WEnd - DI No.: ' & CLIP(sql_2.Data_G.F1) & ', Date: ' & FORMAT(R:Date, @d6) & ', Amt: ' & CLIP(sql_2.Data_G.F2), |
                           'Print_Turnover_Summary', 'Print Turnover Summary - Holiday DIs - ' & FORMAT(TODAY(), @d7) & '.csv',, 1)
       .  .  .  .

       IF R:Count > 0 AND R:Log = 0
          CASE MESSAGE('There are ' & R:Count & ' DIs with a Public Holiday or Weekend Date.||Dates: ' & CLIP(R:Dates) & '||Would you like to output these to a log file?', 'Print Turnover Summary - DI Date Check', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
          OF BUTTON:Yes
             ! Ok
          OF BUTTON:No
             BREAK
          .
       ELSIF R:Log = 1 AND R:Count > 0
          CASE MESSAGE('There are ' & R:Count & ' DIs with a Public Holiday or Weekend Date.||Dates: ' & CLIP(R:Dates) & '||A log of these was created: ' & 'Print Turnover Summary - Holiday DIs - ' & FORMAT(TODAY(), @d7) & '.csv||Would you like to view this now?', 'Print Turnover Summary - DI Date Check', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
          OF BUTTON:Yes
             ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Print Turnover Summary - Holiday DIs - ' & FORMAT(TODAY(), @d7) & '.csv')
          .
          BREAK
    .  .
    EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      IF L_SG:Output > 0
         CASE MESSAGE('Would you like to load the accumulation details now?|Note that if the file does not load automatically you can find it at the location shown below.||File: ' & PATH() & '\Print Turnover Summary - ' & FORMAT(TODAY(), @d7) & '.csv', 'Print Turnover Summary', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Print Turnover Summary - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
    ENDPAGE(SELF.Report)
    IF SELF.SkipPreview
      TintRptViewer.CheckForPageNum(Self.PreviewQueue)
      TintRptViewer.SetupCopiesCollate(Self.PreviewQueue)
      if (TinTRptViewer.GetEjectDir())
        TinTRptViewer.ReversePreviewQueue(Self.PreviewQueue)
      end
      SELF.Report{PROP:FlushPreview} = True
      GlobalResponse = RequestCompleted
    ELSE
      GlobalResponse = TintRptViewer.Display(Report,Self.PreviewQueue,Report{Prop:Text},'Page Width',0)
      SELF.SkipPreview = TRUE
    END
    FREE(SELF.PreviewQueue)
   END
  ! You can place code in this embed point to test GlobalResponse to see if report printed or not.
  ! CASE GlobalResponse
  ! OF RequestCompleted     ! Report Was Printed
  !   ! Do Something
  ! OF RequestCancelled     ! Report Was NOT Printed
  !   ! Do Something
  ! END
   OMIT('***TINTOOLS***')
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  PARENT.AskPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  ***TINTOOLS***
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
      ! Doesn't work, using Tin Tools
  
  !  LOOP
  !     CASE MESSAGE('Print another copy?', 'Print', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
  !     OF BUTTON:Yes
  !        SELF.PrintReport()
  !     ELSE
  !        BREAK
  !  .  .
  !


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_ManagementProfit_Summary_old')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?L_SG:ExcludeBadDebts
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:Setup.Open                                        ! File Setup used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceTransporter.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF NOT TargetSelector.ASK(1) THEN
     SELF.Kill()
     RETURN Level:Fatal
  END
  IF NOT TargetSelector.GetPrintSelected() THEN
     SELF.SetReportTarget(TargetSelector.GetReportSelected())
  END
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Setup, ?Progress:PctText, Progress:Thermometer, 0)
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  FDB4.Init(?L_SG:BranchName1,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(BRA:Key_BranchName)
  FDB4.AddField(BRA:BranchName,FDB4.Q.BRA:BranchName) !List box control field - type derived from field
  FDB4.AddField(BRA:BID,FDB4.Q.BRA:BID) !Primary key field - type derived from field
  FDB4.AddUpdateField(BRA:BID,L_SG:Branch1)
  ThisWindow.AddItem(FDB4.WindowComponent)
  FDB4.DefaultFill = 0
  FDB7.Init(?BRA:BranchName,Queue:FileDrop:1.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop:1,Relate:Branches,ThisWindow)
  FDB7.Q &= Queue:FileDrop:1
  FDB7.AddSortOrder(BRA:Key_BranchName)
  FDB7.AddField(BRA:BranchName,FDB7.Q.BRA:BranchName) !List box control field - type derived from field
  FDB7.AddField(BRA:BID,FDB7.Q.BRA:BID) !Primary key field - type derived from field
  FDB7.AddUpdateField(BRA:BID,L_SG:Branch2)
  ThisWindow.AddItem(FDB7.WindowComponent)
  FDB7.DefaultFill = 0
      LO:From_Date    = GETINI('Print_Turnover', 'From_Date', , GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Turnover', 'To_Date', , GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      PUTINI('Print_Turnover', 'From_Date', LO:From_Date, GLO:Local_INI)
      PUTINI('Print_Turnover', 'To_Date', LO:To_Date, GLO:Local_INI)
  
      
      sql_.Init(GLO:DBOwner, '_SQLTemp2')
      sql_2.Init(GLO:DBOwner, '_SQLTemp2')
      
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
    ! set printer eject style
    TinTRptViewer.SetEjectDir(TinEject:PageFaceUp)
    IF TintRptViewer.PrintViewerSetup() THEN
      Self:Response = RequestCancelled       ! Changed by LH 4/19/98
      POST(EVENT:CloseDown)                  ! Changed by LH 4/19/98
      RETURN Level:NOTIFY                    ! Changed by LH 4/19/98
    END
    SELF.SkipPreview = TintRptViewer.SkipPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:Setup.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Next PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    IF ReportMemoryRecords=0 THEN
       ReportMemoryRecords+=1
       RETURN Level:Benign
    ELSE
       SELF.Response = RequestCompleted
       POST(EVENT:CloseWindow)
       RETURN Level:Notify
    END
  ReturnValue = PARENT.Next()
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
          SELECT(?Tab1)
          DISABLE(?Pause)
          DISPLAY
          IF L_SG:Output > 0
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             LOC:Transaction_Log_Name   = 'Print Turnover Summary - ' & FORMAT(TODAY(), @d7) & '.csv'
             Add_Log('Start', 'Print_Turnover_Summary', LOC:Transaction_Log_Name, TRUE, 1)
      
      
      
             CASE MESSAGE('Would you like to empty the Audit Management Profit table?','Print Summary', ICON:Question, 'All|Summary|None', 1)
             OF 1
                _SQLTemp{PROP:SQL}    = 'DELETE FROM Audit_ManagementProfit'
             OF 2
                _SQLTemp{PROP:SQL}    = 'DELETE FROM Audit_ManagementProfit WHERE source = 2'
          .  .
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      Calc.Get_Inv_Tots()
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          CLEAR(BRA:Record, -1)
          SET(BRA:PKey_BID)
          NEXT(Branches)
          L_SG:Branch1        = BRA:BID
          L_SG:BranchName1    = BRA:BranchName
      
          NEXT(Branches)
          L_SG:Branch2        = BRA:BID
          L_SG:BranchName2    = BRA:BranchName
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Turnover_Summary.Get_Inv_Tots        PROCEDURE
R:Type      BYTE

    CODE
    ! Loop through the Invoice file and gather all information
    ! Flaw in this code is that it can only deal with 2 branches.

    ! Get_Invoices
    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info  , p:LogName)
    !   1           2       3           4          5       6          7        8            9                 10        11          12
    ! (LONG  , BYTE    , BYTE=0, BYTE=0        , ULONG=0, <LONG>  , BYTE=0  , BYTE=0    , BYTE=0           , BYTE=0  , <STRING>, <STRING>),STRING
    ! p:Option
    !   0.  - Total Charges inc.
    !   1.  - Excl VAT
    !   2.  - VAT
    !   3.  - Kgs
    ! p:Type
    !   0.  - All
    !   1.  - Invoices                      Total >= 0.0
    !   2.  - Credit Notes                  Total < 0.0
    !   3.  - Credit Notes excl Bad Debts   Total < 0.0
    ! p:Limit_On
    !   0.  - Date [& Branch]
    !   1.  - MID
    ! p:Manifest_Option
    !   0.  - None
    !   1.  - Overnight
    !   2.  - Broking
    ! p:Not_Manifest
    !   0.  - All
    !   1.  - Not Manifest
    !   2.  - Manifest

    R:Type      = 2
    IF L_SG:ExcludeBadDebts = TRUE
       R:Type   = 3
    .

    ! Total inc., Invoices, filter on dates, overnight
    L_SG:Sales_Overnight_DBN    = Get_Invoices(LO:From_Date, 0, 1, 2, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 1, 2, 'I-ON', LOC:Transaction_Log_Name)       ! Overnight
    L_SG:Sales_Overnight_DBN   += Get_Invoices(LO:From_Date, 0, 1, 1, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 1, 2, 'I-EI-O', LOC:Transaction_Log_Name)       ! Overnight
    L_SG:Sales_Overnight_JHB    = Get_Invoices(LO:From_Date, 0, 1, 2, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 1, 2, 'I-ON', LOC:Transaction_Log_Name)       ! Overnight
    L_SG:Sales_Overnight_JHB   += Get_Invoices(LO:From_Date, 0, 1, 1, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 1, 2, 'I-EI-O', LOC:Transaction_Log_Name)       ! Overnight

    L_SG:Sales_Broking_DBN      = Get_Invoices(LO:From_Date, 0, 1, 2, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 2, 2, 'I-BR', LOC:Transaction_Log_Name)       ! Broking
    L_SG:Sales_Broking_DBN     += Get_Invoices(LO:From_Date, 0, 1, 1, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 2, 2, 'I-EI-B', LOC:Transaction_Log_Name)       ! Broking
    L_SG:Sales_Broking_JHB      = Get_Invoices(LO:From_Date, 0, 1, 2, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 2, 2, 'I-BR', LOC:Transaction_Log_Name)       ! Broking
    L_SG:Sales_Broking_JHB     += Get_Invoices(LO:From_Date, 0, 1, 1, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 2, 2, 'I-EI-B', LOC:Transaction_Log_Name)       ! Broking
                                                                          

    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type, p:Output, p:Source)
    !   1           2       3         4               5           6        7         8          9           10              11     , 12

    L_SG:Extra_Invoices_DBN      = Get_InvoicesTransporter(LO:From_Date, 0, 1, FALSE,,, L_SG:Branch1, LO:To_Date,TRUE,, L_SG:Output,2,'T-EI', LOC:Transaction_Log_Name)    ! Credits for day
    L_SG:Extra_Invoices_JHB      = Get_InvoicesTransporter(LO:From_Date, 0, 1, FALSE,,, L_SG:Branch2, LO:To_Date,TRUE,, L_SG:Output,2,'T-EI', LOC:Transaction_Log_Name)    ! Credits for day

    L_SG:Transporter_Debits_DBN  = Get_InvoicesTransporter(LO:From_Date, 0, 1, TRUE,,, L_SG:Branch1, LO:To_Date,,, L_SG:Output,2,'T-TD', LOC:Transaction_Log_Name)         ! Credits for day - Transporter not MID
    L_SG:Transporter_Debits_JHB  = Get_InvoicesTransporter(LO:From_Date, 0, 1, TRUE,,, L_SG:Branch2, LO:To_Date,,, L_SG:Output,2,'T-TD', LOC:Transaction_Log_Name)         ! Credits for day - Transporter not MID

    L_SG:Transporter_Credits_DBN = -Get_InvoicesTransporter(LO:From_Date, 0, 2,,,, L_SG:Branch1, LO:To_Date,,, L_SG:Output,2,'T-TC', LOC:Transaction_Log_Name)
    L_SG:Transporter_Credits_JHB = -Get_InvoicesTransporter(LO:From_Date, 0, 2,,,, L_SG:Branch2, LO:To_Date,,, L_SG:Output,2,'T-TC', LOC:Transaction_Log_Name)

    ! All extra legs are broking but some are from broking manifests and some are from loadmaster manifests
    L_SG:Extra_Leg_ON_DBN          = Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2, 'T-EL', LOC:Transaction_Log_Name, 1)
    ! Turnover - Invoices (p3 = 1), Del Leg Only (p5 = 1), Broking (p10 = 2)
    ! Invoices & CNs (p3 = 0), Del Leg Only (p5 = 1), Broking Invoice (p10 = 2), Broking Manifest (p15 = 2)
    L_SG:Extra_Leg_Broking_DBN     = Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2, 'T-EL', LOC:Transaction_Log_Name, 2)
    L_SG:Extra_Leg_ON_JHB          = Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2, 'T-EL', LOC:Transaction_Log_Name, 1)
    L_SG:Extra_Leg_Broking_JHB     = Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2, 'T-EL', LOC:Transaction_Log_Name, 2)

    L_SG:Extra_Leg_DBN             = L_SG:Extra_Leg_ON_DBN + L_SG:Extra_Leg_Broking_DBN
    L_SG:Extra_Leg_JHB             = L_SG:Extra_Leg_ON_JHB + L_SG:Extra_Leg_Broking_JHB

    L_SG:Sales_Broking_DBN        -= L_SG:Extra_Leg_Broking_DBN     ! 22 Aug - take off broking extra legs, ON are not included
    L_SG:Sales_Broking_JHB        -= L_SG:Extra_Leg_Broking_JHB

    L_SG:Sales_Broking_DBN_Total   = L_SG:Extra_Leg_DBN + L_SG:Sales_Broking_DBN
    L_SG:Sales_Broking_JHB_Total   = L_SG:Extra_Leg_JHB + L_SG:Sales_Broking_JHB

    L_SG:Sales_Overnight_DBN      -= L_SG:Extra_Leg_ON_DBN        ! Only remove the overnight extra legs
    L_SG:Sales_Overnight_JHB      -= L_SG:Extra_Leg_ON_JHB

!    L_SG:Sales_Broking_DBN_Total     += L_SG:Extra_Invoices_DBN + L_SG:Transporter_Credits_DBN - L_SG:Transporter_Debits_DBN
!    L_SG:Sales_Broking_JHB_Total     += L_SG:Extra_Invoices_JHB + L_SG:Transporter_Credits_JHB - L_SG:Transporter_Debits_JHB
!    L_SG:Sales_Broking_DBN_Total     += L_SG:Transporter_Credits_DBN - L_SG:Transporter_Debits_DBN
!    L_SG:Sales_Broking_JHB_Total     += L_SG:Transporter_Credits_JHB - L_SG:Transporter_Debits_JHB

    L_SG:Credits_DBN            = Get_Invoices(LO:From_Date, 0, R:Type,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 0, 2,'I-CR', LOC:Transaction_Log_Name)    ! total
    L_SG:Credits_JHB            = Get_Invoices(LO:From_Date, 0, R:Type,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 0, 2,'I-CR', LOC:Transaction_Log_Name)    ! total

    L_SG:Sales_Overnight_DBN   += L_SG:Credits_DBN       ! neg - excluding credits
    L_SG:Sales_Overnight_JHB   += L_SG:Credits_JHB       ! neg - excluding credits

    L_SG:Credits_Total          = L_SG:Credits_DBN + L_SG:Credits_JHB

    L_SG:Turnover_DBN           = L_SG:Sales_Broking_DBN_Total + L_SG:Sales_Overnight_DBN  - L_SG:Credits_DBN
    L_SG:Turnover_JHB           = L_SG:Sales_Broking_JHB_Total + L_SG:Sales_Overnight_JHB  - L_SG:Credits_JHB

    L_SG:Turnover_less_Credits_DBN = L_SG:Turnover_DBN + L_SG:Credits_DBN
    L_SG:Turnover_less_Credits_JHB = L_SG:Turnover_JHB + L_SG:Credits_JHB

    L_SG:Turnover_Total         = L_SG:Turnover_less_Credits_DBN + L_SG:Turnover_less_Credits_JHB

    ! Not of invoiced days (working days) - of course will be wrong if people invoice in weekend or public holiday...
    sql_.PropSQL('SELECT COUNT(DISTINCT LTRIM(DATEPART([year], INVOICEDATEANDTIME)) + <39>/<39> + LTRIM(DATEPART([month], INVOICEDATEANDTIME)) + ' & |
                 '<39>/<39> + LTRIM(DATEPART([day], INVOICEDATEANDTIME))) ' & |
                 'FROM _Invoice WHERE InvoiceDateAndTime >= ' & |
                SQL_Get_DateT_G(LO:From_Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(LO:To_Date + 1,,1))
    IF sql_.Next_Q() > 0
       L_SG:Work_Days           = sql_.Data_G.F1
    .
    DO Audit_Check              ! Checks and outputs DIs on WEnd or Public holidays


    L_SG:Broking_Kg_DBN         = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 2, 2,, LOC:Transaction_Log_Name)       ! Broking
    L_SG:Broking_Kg_JHB         = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 2, 2,, LOC:Transaction_Log_Name)       ! Broking
    L_SG:Broking_Kg_Total       = L_SG:Broking_Kg_DBN + L_SG:Broking_Kg_JHB

    L_SG:Overnight_Kg_DBN       = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 1, 2,, LOC:Transaction_Log_Name)       ! Broking
    L_SG:Overnight_Kg_JHB       = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 1, 2,, LOC:Transaction_Log_Name)       ! Broking
    L_SG:Overnight_Kg_Total     = L_SG:Overnight_Kg_DBN + L_SG:Overnight_Kg_JHB

    L_SG:Kg_Total               = L_SG:Broking_Kg_Total + L_SG:Overnight_Kg_Total

    L_SG:Kg_Per_Day             = L_SG:Kg_Total / L_SG:Work_Days
    L_SG:Kg_Per_Day_DBN         = (L_SG:Broking_Kg_DBN + L_SG:Overnight_Kg_DBN) / L_SG:Work_Days
    L_SG:Kg_Per_Day_JHB         = (L_SG:Broking_Kg_JHB + L_SG:Overnight_Kg_JHB) / L_SG:Work_Days


    ! Get_InvoicesTransporter
    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type,
    ! (LONG  , BYTE    , BYTE=0, BYTE=0        , BYTE=0        , <ULONG>, ULONG=0, <LONG>  , BYTE=0     , BYTE=0        ,
    !   1           2       3         4               5           6        7         8          9           10
    !  p:Output, p:Source, p:Info  , p:LogName, p:Manifest_Type)
    !  BYTE=0  , BYTE=0  , <STRING>, <STRING> , BYTE=0 ),STRING
    !    11        12        13        14         15
    ! p:Option                                                  2
    !   0.  - Total Charges inc.
    !   1.  - Excl VAT
    !   2.  - VAT
    ! p:Type                                                    3
    !   0.  - All
    !   1.  - Invoices                      Total >= 0.0
    !   2.  - Credit Notes                  Total < 0.0
    ! p:DeliveryLegs                                            5
    !   0.  - Both
    !   1.  - Del Legs only
    !   2.  - No Del Legs
    ! p:MID                                                     6
    !   0   = Not for a MID
    !   x   = for a MID
    ! p:Invoice_Type (was Manifest_Type)                        10
    !   0   - All
    !   1   - Overnight
    !   2   - Broking
    ! p:Manifest_Type                                           15
    !   0   - All
    !   1   - Overnight
    !   2   - Broking
    ! p:Extra_Inv                                               9
    !   Only
    ! p:Source                                                  12
    !   1   - Management Profit
    !   2   - Management Profit Summary

    ! Delivery legs (1), Broking Manifest type
!   L_SG:Sales_Broking_DBN     += Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2)
!   L_SG:Sales_Broking_JHB     += Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2)

    ! Turnover - Invoices (p3 = 1), Broking (p10 = 2), Legs & Other (p5 = 0)

    ! Invoices & Credits (p3 = 0), No Del Legs (p5 = 2), Broking (p10 = 2)
    L_SG:Costs_Broking_DBN      = Get_InvoicesTransporter(LO:From_Date, 0, 0,,2,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2,'T-CB', LOC:Transaction_Log_Name)
!    L_SG:Costs_Broking_DBN     += Get_InvoicesTransporter(LO:From_Date, 0, 0,,2,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2,'T-CB', LOC:Transaction_Log_Name)
    L_SG:Costs_Broking_JHB      = Get_InvoicesTransporter(LO:From_Date, 0, 0,,2,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2,'T-CB', LOC:Transaction_Log_Name)
!    L_SG:Costs_Broking_JHB     += Get_InvoicesTransporter(LO:From_Date, 0, 0,,2,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2,'T-CB', LOC:Transaction_Log_Name)

    L_SG:Costs_Broking_DBN     += L_SG:Extra_Leg_DBN
    L_SG:Costs_Broking_JHB     += L_SG:Extra_Leg_JHB

    L_SG:Transporter_Costs_DBN  = L_SG:Costs_Broking_DBN + L_SG:Extra_Invoices_DBN + L_SG:Transporter_Debits_DBN - L_SG:Transporter_Credits_DBN
    L_SG:Transporter_Costs_JHB  = L_SG:Costs_Broking_JHB + L_SG:Extra_Invoices_JHB + L_SG:Transporter_Debits_JHB - L_SG:Transporter_Credits_JHB

    ! Overnight Manifest type
    L_SG:Loadmaster_DBN         = Get_InvoicesTransporter(LO:From_Date, 0, 1,,,, L_SG:Branch1, LO:To_Date,, 1, L_SG:Output, 2,'T-CO', LOC:Transaction_Log_Name)
    L_SG:Loadmaster_JHB         = Get_InvoicesTransporter(LO:From_Date, 0, 1,,,, L_SG:Branch2, LO:To_Date,, 1, L_SG:Output, 2,'T-CO', LOC:Transaction_Log_Name)



    L_SG:Average_Cost_Per_Kg    = L_SG:Turnover_Total / L_SG:Kg_Total
            !(L_SG:Costs_Broking_DBN + L_SG:Costs_Broking_JHB + L_SG:Loadmaster_DBN + L_SG:Loadmaster_JHB) / L_SG:Kg_Total

    !L_SG:Turnover_Per_Day       = L_SG:Turnover_Total / L_SG:Work_Days
    L_SG:Turnover_Per_Day       = (L_SG:Turnover_less_Credits_DBN + L_SG:Turnover_less_Credits_JHB) / L_SG:Work_Days
    L_SG:Turnover_Per_Day_DBN   = L_SG:Turnover_less_Credits_DBN / L_SG:Work_Days
    L_SG:Turnover_Per_Day_JHB   = L_SG:Turnover_less_Credits_JHB / L_SG:Work_Days

    L_SG:GP_Broking_DBN         = L_SG:Sales_Broking_DBN_Total - L_SG:Transporter_Costs_DBN
    L_SG:GP_Broking_JHB         = L_SG:Sales_Broking_JHB_Total - L_SG:Transporter_Costs_JHB

    L_SG:GP_Per_Broking_DBN     = (L_SG:GP_Broking_DBN / L_SG:Sales_Broking_DBN_Total) * 100
    L_SG:GP_Per_Broking_JHB     = (L_SG:GP_Broking_JHB / L_SG:Sales_Broking_JHB_Total) * 100

    L_SG:GP_Overnight_DBN       = L_SG:Sales_Overnight_DBN - L_SG:Loadmaster_DBN
    !L_SG:GP_Overnight_DBN      += L_SG:Extra_Invoices_DBN + L_SG:Transporter_Credits_DBN - L_SG:Transporter_Debits_DBN
    L_SG:GP_Overnight_JHB       = L_SG:Sales_Overnight_JHB - L_SG:Loadmaster_JHB
    !L_SG:GP_Overnight_JHB      += L_SG:Extra_Invoices_JHB + L_SG:Transporter_Credits_JHB - L_SG:Transporter_Debits_JHB

    L_SG:GP_Per_Overnight_DBN   = (L_SG:GP_Overnight_DBN / L_SG:Sales_Overnight_DBN) * 100
    L_SG:GP_Per_Overnight_JHB   = (L_SG:GP_Overnight_JHB / L_SG:Sales_Overnight_JHB) * 100


!    L_SG:Gross_GP_DBN           = L_SG:Turnover_less_Credits_DBN - (L_SG:Costs_Broking_DBN + L_SG:Loadmaster_DBN)
!    L_SG:Gross_GP_JHB           = L_SG:Turnover_less_Credits_JHB - (L_SG:Costs_Broking_JHB + L_SG:Loadmaster_JHB)

    L_SG:Gross_GP_DBN           = L_SG:GP_Overnight_DBN + L_SG:GP_Broking_DBN
    L_SG:Gross_GP_JHB           = L_SG:GP_Overnight_JHB + L_SG:GP_Broking_JHB

    L_SG:Gross_GP_Total         = L_SG:Gross_GP_DBN + L_SG:Gross_GP_JHB
    L_SG:Gross_GP_Per_Day       = L_SG:Gross_GP_Total / L_SG:Work_Days


    L_SG:Loadmaster_Total       = L_SG:Loadmaster_DBN + L_SG:Loadmaster_JHB
    L_SG:Loadmaster_Per_Day     = L_SG:Loadmaster_Total / L_SG:Work_Days

    L_SG:Loadmaster_GP_Total    = L_SG:Loadmaster_Total + L_SG:Gross_GP_Total   ! Note loadmaster is not seen as a cost here but an income
    L_SG:Loadmaster_GP_Per_Day  = L_SG:Loadmaster_GP_Total / L_SG:Work_Days
    RETURN




ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
      IF LOC:Errors > 0
         MESSAGE('Completed with Errors: ' & LOC:Errors & '||Please review the System Log.', 'Print Turnover', ICON:Exclamation)
      .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_ManagementProfit_Summary','Print_ManagementProfit_Summary','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! remove column Owing out of this one for new one
!!! </summary>
Print_Creditor_Invoices_1_Dec_06 PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Options          GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
LOC:Report_Group     GROUP,PRE(L_RG)                       ! 
Total                DECIMAL(12,2)                         ! 
Owing                DECIMAL(11,2)                         ! Owing amount
                     END                                   ! 
LOC:Options_Extra    GROUP,PRE(L_OE)                       ! 
Run_Type             BYTE                                  ! 
Payment              BYTE(255)                             ! Payment options
                     END                                   ! 
Process:View         VIEW(_InvoiceTransporter)
                       PROJECT(INT:Broking)
                       PROJECT(INT:CR_TIN)
                       PROJECT(INT:Cost)
                       PROJECT(INT:InvoiceDate)
                       PROJECT(INT:MID)
                       PROJECT(INT:Status)
                       PROJECT(INT:TID)
                       PROJECT(INT:TIN)
                       PROJECT(INT:VAT)
                       PROJECT(INT:BID)
                       JOIN(TRA:PKey_TID,INT:TID)
                         PROJECT(TRA:TransporterName)
                       END
                       JOIN(BRA:PKey_BID,INT:BID)
                         PROJECT(BRA:BranchName)
                       END
                     END
ProgressWindow       WINDOW('Report Invoice Transporter'),AT(,,210,139),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,MDI,TIMER(1)
                       SHEET,AT(4,4,203,114),USE(?Sheet1)
                         TAB('Process'),USE(?Tab1),HIDE
                           PROGRESS,AT(51,57,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(35,43,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(35,74,141,10),USE(?Progress:PctText),CENTER
                         END
                         TAB('Options'),USE(?Tab2)
                           PROMPT('Run Type:'),AT(13,26),USE(?L_OE:Run_Type:Prompt)
                           LIST,AT(77,26,122,10),USE(L_OE:Run_Type),VSCROLL,DROP(15),FROM('All|#0|Invoices  (MID)|' & |
  '#1|Additionals  (MID & Adjustment Invoice No.)|#2|Office Gen.  (MID)|#3|Credit Notes' & |
  '|#4|Extra Invoices (flag)|#5')
                           GROUP,AT(11,53,188,10),USE(?Group1)
                             PROMPT('Payment:'),AT(13,53),USE(?L_OE:Payment:Prompt)
                             LIST,AT(77,53,122,10),USE(L_OE:Payment),VSCROLL,DROP(15),FROM('All|#255|No Payments|#0|' & |
  'Partially Paid|#1|Amount Owing|#250|Fully Paid|#3'),MSG('Payment options'),TIP('Payment options')
                           END
                         END
                       END
                       BUTTON('Cancel'),AT(100,122,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                       BUTTON('Pause'),AT(154,122,,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                     END

Report               REPORT('_InvoiceTransporter Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Transporter Invoices'),AT(0,52,4844,260),USE(?ReportTitle),FONT(,14,,FONT:regular), |
  CENTER
                         STRING(@d5b),AT(5469,104),USE(LO:From_Date),RIGHT(1)
                         STRING(' - '),AT(6177,104,156,156),USE(?String24),TRN
                         STRING(@d5b),AT(6458,104),USE(LO:To_Date),RIGHT(1)
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(711,350,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(1354,365,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(2146,350,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(2844,350,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(3688,350,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(4521,350,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(5417,350,0,250),USE(?HeaderLine:8),COLOR(COLOR:Black)
                         LINE,AT(6344,350,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         LINE,AT(7000,350,0,250),USE(?HeaderLine:71),COLOR(COLOR:Black)
                         STRING('Inv. Date'),AT(52,406,583,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Owing'),AT(5396,406,896,167),USE(?HeaderTitle:9),CENTER,TRN
                         STRING('Total'),AT(4469,406,896,167),USE(?HeaderTitle:8),CENTER,TRN
                         STRING('Branch'),AT(6365,406,552,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Transporter'),AT(1406,406,688,167),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('MID'),AT(2115,406,688,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('Cost'),AT(2802,406,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('VAT'),AT(3635,406,833,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('Broking'),AT(7031,406,656,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('TIN'),AT(688,406,688,167),USE(?HeaderTitle:10),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(711,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(1354,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(2146,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(2844,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(3688,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(4521,0,0,250),USE(?DetailLine:8),COLOR(COLOR:Black)
                         LINE,AT(5417,0,0,250),USE(?DetailLine:9),COLOR(COLOR:Black)
                         LINE,AT(6344,0,0,250),USE(?DetailLine:10),COLOR(COLOR:Black)
                         LINE,AT(7000,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                         STRING(@d5b),AT(52,52,,167),USE(INT:InvoiceDate),RIGHT(2)
                         STRING(@n_10),AT(646,52),USE(INT:TIN),RIGHT(1),TRN
                         STRING(@s35),AT(1406,52,833,156),USE(TRA:TransporterName),LEFT(1),TRN
                         STRING(@n_10),AT(2115,52,688,167),USE(INT:MID),RIGHT,TRN
                         STRING(@n-14.2),AT(2802,52,833,167),USE(INT:Cost),RIGHT,TRN
                         STRING(@n-14.2),AT(3635,52,833,167),USE(INT:VAT),RIGHT(1),TRN
                         STRING(@n-15.2),AT(4469,52),USE(L_RG:Total),RIGHT(1),TRN
                         STRING(@n-15.2),AT(5396,52),USE(L_RG:Owing),RIGHT(1),TRN
                         STRING(@s8),AT(6406,52,,167),USE(BRA:BranchName)
                         CHECK('Broking'),AT(7083,52,,167),USE(INT:Broking)
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
totals                 DETAIL,AT(,,,396),USE(?totals)
                         LINE,AT(625,52,5781,0),USE(?Line18),COLOR(COLOR:Black)
                         STRING('Totals:'),AT(52,104),USE(?String29),TRN
                         STRING(@n_10),AT(740,104,688,167),USE(INT:TID,,?INT:TID:2),RIGHT,CNT,TALLY(Detail),TRN
                         STRING(@n-16.2),AT(2677,104,958,167),USE(INT:Cost,,?INT:Cost:2),RIGHT,SUM,TALLY(Detail),TRN
                         STRING(@n-16.2),AT(3510,104,958,167),USE(INT:VAT,,?INT:VAT:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-16.2),AT(4406,104),USE(L_RG:Total,,?L_RG:Total:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-15.2),AT(5396,104),USE(L_RG:Owing,,?L_RG:Owing:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         LINE,AT(625,313,5781,0),USE(?Line18:2),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Invoice Date' & |
      '|' & 'By Transporter Invoice No.' & |
      '|' & 'By Transporter & Date' & |
      '|' & 'By Branch & Date' & |
      '|' & 'By Manifest & Date' & |
      '|' & 'By User & Date' & |
      '|' & 'By Delivery & Date (extra legs)' & |
      '|' & 'By Credited Invoice & Date' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:totals)
  
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Creditor_Invoices_1_Dec_06')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:_InvoiceTransporter.SetOpenRelated()
  Relate:_InvoiceTransporter.Open                          ! File _InvoiceTransporter used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Creditor_Invoices_1_Dec_06',ProgressWindow) ! Restore window settings from non-volatile store
      LO:From_Date    = GETINI('Print_Creditor_Invoices', 'From_Date', '', GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Creditor_Invoices', 'To_Date', '', GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      IF LO:To_Date > 0
         PUTINI('Print_Creditor_Invoices', 'From_Date', LO:From_Date, GLO:Local_INI)
         PUTINI('Print_Creditor_Invoices', 'To_Date', LO:To_Date, GLO:Local_INI)
      ELSE
         ReturnValue  = LEVEL:Fatal
      .
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:_InvoiceTransporter, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Invoice Date')) THEN
     ThisReport.AppendOrder('+INT:InvoiceDate')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter Invoice No.')) THEN
     ThisReport.AppendOrder('+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter & Date')) THEN
     ThisReport.AppendOrder('+INT:TID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch & Date')) THEN
     ThisReport.AppendOrder('+INT:BID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Manifest & Date')) THEN
     ThisReport.AppendOrder('+INT:MID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By User & Date')) THEN
     ThisReport.AppendOrder('+INT:UID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Delivery & Date (extra legs)')) THEN
     ThisReport.AppendOrder('+INT:DID,+INT:InvoiceDate,+INT:TIN')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Credited Invoice & Date')) THEN
     ThisReport.AppendOrder('+INT:CR_TIN,+INT:InvoiceDate,+INT:TIN')
  END
  ThisReport.SetFilter('INT:InvoiceDate >= LO:From_Date AND INT:InvoiceDate << (LO:To_Date + 1)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:_InvoiceTransporter.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_InvoiceTransporter.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Creditor_Invoices_1_Dec_06',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_OE:Run_Type
          CASE L_OE:Run_Type + 1
          OF 1            ! All
             ENABLE(?Group1)
          OF 2            ! Invoices
             ENABLE(?Group1)
          OF 3            ! Additionals
             ENABLE(?Group1)
          OF 4            ! Office Gen.
             ENABLE(?Group1)
          OF 5            ! Credit Notes
             DISABLE(?Group1)
          OF 6            ! Extra Invoices (flag)
             ENABLE(?Group1)
          ELSE
             DISABLE(?Group1)
          .
    OF ?Pause
      ThisWindow.Update()
          UNHIDE(?Tab1)
          HIDE(?Tab2)
      
      
          ! 'All|#0|Invoices|#1|Additionals|#2|Office Gen.|#3|Credit Notes|#4'
      
          CASE L_OE:Run_Type + 1
          OF 1            ! All
          OF 2            ! Invoices
             ThisReport.SetFilter('INT:Cost >= 0.0 AND INT:MID <> 0 AND (INT:CR_TIN = 0 OR SQL(CR_TIN IS NULL))', 'ikb')
          OF 3            ! Additionals
             ThisReport.SetFilter('INT:MID <> 0 AND INT:CR_TIN <> 0', 'ikb')
          OF 4            ! Office Gen.
             ThisReport.SetFilter('INT:MID = 0', 'ikb')
          OF 5            ! Credit Notes
             ThisReport.SetFilter('INT:Cost < 0.0', 'ikb')
          OF 6            ! Extra Invoices
             ThisReport.SetFilter('INT:ExtraInv = 1', 'ikb')
          .
      
      
          ! All|No Payments|Partially Paid|Amount Owing|Fully Paid
          ! 255|0|1|250|3
      
          CASE L_OE:Payment
          OF 255
          OF 250
             ThisReport.SetFilter('INT:Status <= 1', 'ikb2')
          OF 0
             ThisReport.SetFilter('INT:Status = 0', 'ikb2')
          OF 1
             ThisReport.SetFilter('INT:Status = 1', 'ikb2')
          OF 3
             ThisReport.SetFilter('INT:Status = 3', 'ikb2')
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      L_RG:Total      = INT:Cost + INT:VAT
  
  
      L_RG:Owing      = Get_InvTransporter_Outstanding(INT:TIN)
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Creditor_Invoices','Print_Creditor_Invoices','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! (Manifest based)
!!! </summary>
Print_Turnover_June19 PROCEDURE 

!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
TintRptViewer  TinReportViewer
!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
Progress:Thermometer BYTE                                  ! 
LOC:Errors           ULONG                                 ! 
LOC:Settings         GROUP,PRE(L_SG)                       ! 
VAT                  BYTE                                  ! 
BID                  ULONG                                 ! Branch ID
BranchName           STRING(35)                            ! Branch Name
ExcludeBadDebts      BYTE(1)                               ! Exclude Bad Debt Credit Notes
Output               BYTE                                  ! Output trail of items
                     END                                   ! 
LOC:Values           GROUP,PRE(L_VG)                       ! 
Kgs_Broking          DECIMAL(7)                            ! 
Kgs_O_Night          DECIMAL(7)                            ! 
T_O_Broking          DECIMAL(10,2)                         ! 
T_O_O_Night          DECIMAL(10,2)                         ! 
C_Note               DECIMAL(10,2)                         ! 
Total_T_O            DECIMAL(10,2)                         ! 
Loadmaster           DECIMAL(10,2)                         ! 
Trans_Broking        DECIMAL(10,2)                         ! 
Extra_Inv            DECIMAL(10,2)                         ! 
Trans_Credit         DECIMAL(10,2)                         ! 
Total_Cost           DECIMAL(10,2)                         ! 
Trans_Debit          DECIMAL(10,2)                         ! 
Total_Cost_Less_Deb  DECIMAL(10,2)                         ! 
GP                   DECIMAL(10)                           ! 
GP_Per               DECIMAL(10,2)                         ! 
MID                  ULONG                                 ! Manifest ID
CreatedDate          DATE                                  ! Created Date
Ext_Invocie_TO       DECIMAL(12,2)                         ! Turn over extra invoice
                     END                                   ! 
Totals_Group         GROUP,PRE(L_TG)                       ! 
GP                   DECIMAL(12,2)                         ! 
GP_Per               DECIMAL(7,2)                          ! 
Total_TO             DECIMAL(12,2)                         ! 
Total_Cost           DECIMAL(12,2)                         ! 
                     END                                   ! 
LOC:Dates_Done       QUEUE,PRE(L_DQ)                       ! 
Date                 DATE                                  ! 
                     END                                   ! 
LOC:Options          GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
LOC:Cur_Date         DATE                                  ! 
LOC:Report_Values    GROUP,PRE(L_RG)                       ! 
VAT_Option           STRING(20)                            ! 
                     END                                   ! 
Process:View         VIEW(Manifest)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:Broking)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:CreatedDate)
                       PROJECT(MAN:State)
                       PROJECT(MAN:VATRate)
                     END
FDB4::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
ProgressWindow       WINDOW('Report Turnover (Manifest)'),AT(,,177,126),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       SHEET,AT(4,2,169,102),USE(?Sheet1)
                         TAB('Settings'),USE(?Tab2)
                           PROMPT('Branch:'),AT(24,42),USE(?Prompt2)
                           PROMPT('VAT:'),AT(24,22),USE(?L_SG:VAT:Prompt)
                           LIST,AT(55,22,60,10),USE(L_SG:VAT),DROP(5),FROM('Excluding|#0|Including|#1')
                           LIST,AT(55,42,105,10),USE(L_SG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                           CHECK(' Exclude Bad Debts'),AT(55,60),USE(L_SG:ExcludeBadDebts),MSG('Exclude Bad Debt C' & |
  'redit Notes'),TIP('Exclude Bad Debt Credit Notes')
                           PROMPT('Output:'),AT(26,84),USE(?L_SG:Output_Trail:Prompt)
                           LIST,AT(55,84,60,10),USE(L_SG:Output),VSCROLL,DROP(5),FROM('None|#0|All|#1'),MSG('Output tra' & |
  'il of items'),TIP('Output trail of items')
                         END
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(36,34,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(20,22,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(20,50,141,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Cancel'),AT(124,108,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                       BUTTON('Pause'),AT(70,108,49,15),USE(?Pause),LEFT,ICON('Blue_R.ico'),FLAT
                     END

Report               REPORT('Manifest Report'),AT(250,958,10604,6854),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,10604,698),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Management Profit Report'),AT(4198,10),USE(?ReportTitle),FONT(,12,,FONT:bold),CENTER
                         STRING('Branch:'),AT(7927,42),USE(?String57),TRN
                         STRING(@s35),AT(8396,42),USE(L_SG:BranchName)
                         STRING(@s20),AT(104,42),USE(L_RG:VAT_Option)
                         BOX,AT(0,469,10573,260),USE(?HeaderBox),COLOR(COLOR:Black)
                         BOX,AT(0,260,10573,219),USE(?HeaderBox:2),COLOR(COLOR:Black)
                         STRING('Total'),AT(8750,292,833,167),USE(?HeaderTitle:15),CENTER,TRN
                         STRING('GP %'),AT(9635,500,833,167),USE(?HeaderTitle:17),CENTER,TRN
                         STRING('Trans Dr.'),AT(7969,292,833,167),USE(?HeaderTitle:14),CENTER,TRN
                         STRING('GP'),AT(9635,292,833,167),USE(?HeaderTitle:16),CENTER,TRN
                         STRING('Trans Cr.'),AT(6333,292,833,167),USE(?HeaderTitle:12),CENTER,TRN
                         STRING('Total'),AT(7156,292,833,167),USE(?HeaderTitle:13),CENTER,TRN
                         STRING('T/O O/Night'),AT(2042,521,833,167),USE(?HeaderTitle:8),CENTER,TRN
                         STRING('Trans Brok'),AT(4594,292,833,167),USE(?HeaderTitle:10),CENTER,TRN
                         STRING('Loadmaster'),AT(4594,521,833,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Ext. Inv.'),AT(5490,292,833,167),USE(?HeaderTitle:11),CENTER,TRN
                         STRING('C/Note'),AT(2927,292,833,167),USE(?HeaderTitle:9),CENTER,TRN
                         STRING('MID'),AT(646,292,688,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Kgs Brok'),AT(1396,292,594,167),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Kgs O/N'),AT(1396,521,594,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Date'),AT(52,292,583,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('T/O Brok'),AT(2042,292,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Total T/O'),AT(3760,292,833,167),USE(?HeaderTitle:5),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,10604,396),USE(?Detail)
                         LINE,AT(0,406,10333,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         STRING(@n_10),AT(646,31,,170),USE(L_VG:MID),RIGHT(1),TRN
                         STRING(@n-10.0),AT(1396,31),USE(L_VG:Kgs_Broking),RIGHT(1),TRN
                         STRING(@n-10.0),AT(1396,208),USE(L_VG:Kgs_O_Night),RIGHT(1),TRN
                         STRING(@n-14.2),AT(2042,31),USE(L_VG:T_O_Broking),RIGHT(1),TRN
                         STRING(@n-14.2),AT(2042,208),USE(L_VG:T_O_O_Night),RIGHT(1),TRN
                         STRING(@n-14.2),AT(2927,31),USE(L_VG:C_Note),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1),TRN
                         STRING(@n-14.2),AT(3760,31),USE(L_VG:Total_T_O),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4594,208),USE(L_VG:Loadmaster),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4594,31),USE(L_VG:Trans_Broking),RIGHT(1),TRN
                         STRING(@n-14.2),AT(5490,31),USE(L_VG:Extra_Inv),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6333,31),USE(L_VG:Trans_Credit),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1), |
  TRN
                         STRING(@d5),AT(52,31,,170),USE(L_VG:CreatedDate),RIGHT(1),TRN
                         STRING(@n-14.2),AT(7156,31),USE(L_VG:Total_Cost),RIGHT(1),TRN
                         STRING(@n-14.2),AT(7969,31),USE(L_VG:Trans_Debit),RIGHT(1),TRN
                         STRING(@n-14.2),AT(8750,31),USE(L_VG:Total_Cost_Less_Deb),RIGHT(1),TRN
                         STRING(@n-14.0),AT(9375,31,1094,156),USE(L_VG:GP),RIGHT(1),TRN
                         STRING(@n-14.2),AT(9375,208,1094,167),USE(L_VG:GP_Per),RIGHT(1),TRN
                       END
totals                 DETAIL,AT(,,,542),USE(?totals)
                         STRING('Totals'),AT(104,73,885),USE(?String56),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@n-10.0),AT(1396,73),USE(L_VG:Kgs_Broking,,?L_VG:Kgs_Broking:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-10.0),AT(1396,260),USE(L_VG:Kgs_O_Night,,?L_VG:Kgs_O_Night:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(2042,73),USE(L_VG:T_O_Broking,,?L_VG:T_O_Broking:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(2042,260),USE(L_VG:T_O_O_Night,,?L_VG:T_O_O_Night:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(2927,73),USE(L_VG:C_Note,,?L_VG:C_Note:2),FONT(,,COLOR:Red,,CHARSET:ANSI), |
  RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(3760,73),USE(L_VG:Total_T_O,,?L_VG:Total_T_O:2),RIGHT(1),SUM(L_TG:Total_TO), |
  TALLY(Detail),TRN
                         STRING(@n-14.2),AT(4594,260),USE(L_VG:Loadmaster,,?L_VG:Loadmaster:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(4594,73),USE(L_VG:Trans_Broking,,?L_VG:Trans_Broking:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(5490,73),USE(L_VG:Extra_Inv,,?L_VG:Extra_Inv:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(6333,73),USE(L_VG:Trans_Credit,,?L_VG:Trans_Credit:2),FONT(,,COLOR:Red, |
  ,CHARSET:ANSI),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(7156,73),USE(L_VG:Total_Cost,,?L_VG:Total_Cost:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(7969,73),USE(L_VG:Trans_Debit,,?L_VG:Trans_Debit:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(8750,73),USE(L_VG:Total_Cost_Less_Deb,,?L_VG:Total_Cost_Less_Deb:2),RIGHT(1), |
  SUM(L_TG:Total_Cost),TALLY(Detail),TRN
                         STRING(@n-14.2),AT(9375,73,1094,167),USE(L_TG:GP),RIGHT(1),TRN
                         STRING(@n-10.2),AT(9375,260,1094,167),USE(L_TG:GP_Per),RIGHT(1),TRN
                       END
                       FOOTER,AT(250,7813,10604,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(9521,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END

TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Missing_Invs                          ROUTINE
    CLEAR(LOC:Dates_Done)
    GET(LOC:Dates_Done, RECORDS(LOC:Dates_Done))
    ! The last date done - and we are not on 1st date of range
    IF MAN:CreatedDate > L_DQ:Date + 1 AND MAN:CreatedDate ~= LO:From_Date
       ! Check for invoices not associated with a Manifest in this period
       IF L_DQ:Date = 0
          LOC:Cur_Date             = LO:From_Date
       ELSE
          LOC:Cur_Date             = L_DQ:Date + 1
       .

       LOOP
          CLEAR(LOC:Values)

          Cred_Type_#      = 2
          IF L_SG:ExcludeBadDebts = TRUE
             Cred_Type_#   = 3
          .
          IF L_SG:VAT = TRUE
             ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID, p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source)
             L_VG:C_Note           = -Get_Invoices(LOC:Cur_Date, 0, Cred_Type_#,, L_SG:BID,,,,,1)          ! Any passed on this day
          ELSE
             L_VG:C_Note           = -Get_Invoices(LOC:Cur_Date, 1, Cred_Type_#,, L_SG:BID,,,,,1)          ! Any passed on this day
          .

          L_VG:Total_T_O           = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note

!          IF L_SG:VAT = TRUE
!             L_VG:Extra_Inv        = Get_Invoices(LOC:Cur_Date, 0, 0, TRUE, L_SG:BID)                ! Invoice for day not related to MID
!             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 0, 2, TRUE,,, L_SG:BID)  ! Credits for day - Transporter not MID
!          ELSE
!             L_VG:Extra_Inv        = Get_Invoices(LOC:Cur_Date, 1, 0, TRUE, L_SG:BID)                ! Invoice for day not related to MID
!             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 1, 2, TRUE,,, L_SG:BID)  ! Credits for day - Transporter not MID
!          .

          IF L_SG:VAT = TRUE
             L_VG:Ext_Invocie_TO   = Get_Invoices(LOC:Cur_Date, 0, 1, TRUE, L_SG:BID,, L_SG:Output,,,1)   ! Invoice for day not related to MID

             ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID, p:BID, p:ToDate, p:Extra_Inv, p:Manifest_Type, p:Output, p:Source)

             L_VG:Extra_Inv        = Get_InvoicesTransporter(LOC:Cur_Date, 0, 1, TRUE,,, L_SG:BID,,TRUE,,,1)      ! Credits for day - Transporter not MID
             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 0, 2, TRUE,,, L_SG:BID,,,,,1)     ! Credits for day - Transporter not MID

             L_VG:Trans_Debit      = Get_InvoicesTransporter(LOC:Cur_Date, 0, 2,,,, L_SG:BID,,,,,1)
          ELSE
             L_VG:Ext_Invocie_TO   = Get_Invoices(LOC:Cur_Date, 1, 1, TRUE, L_SG:BID,, L_SG:Output,,,1)   ! Invoice for day not related to MID

             L_VG:Extra_Inv        = Get_InvoicesTransporter(LOC:Cur_Date, 1, 1, TRUE,,, L_SG:BID,,TRUE,,,1)      ! Credits for day - Transporter not MID
             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 1, 2, TRUE,,, L_SG:BID,,,,,1)     ! Credits for day - Transporter not MID

             L_VG:Trans_Debit      = Get_InvoicesTransporter(LOC:Cur_Date, 1, 2,,,, L_SG:BID,,,,,1)
          .

          L_VG:Total_Cost             = L_VG:Loadmaster + L_VG:Trans_Broking + L_VG:Extra_Inv - L_VG:Trans_Credit

          L_VG:Total_Cost_Less_Deb    = L_VG:Total_Cost - L_VG:Trans_Debit

          L_VG:T_O_Broking           += L_VG:Ext_Invocie_TO                                                   ! Add extras to BROKING ALWAYS

          L_VG:Total_T_O              = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note

          L_VG:GP                     = L_VG:Total_T_O - L_VG:Total_Cost_Less_Deb
          L_VG:GP_Per                 = (L_VG:GP / L_VG:Total_T_O) * 100


          L_DQ:Date                = LOC:Cur_Date
          ADD(LOC:Dates_Done, L_DQ:Date)

          L_VG:MID                 = 0
          L_VG:CreatedDate         = LOC:Cur_Date

          IF L_VG:Total_Cost ~= 0.0 OR L_VG:Total_T_O ~= 0.0 OR L_VG:Trans_Debit ~= 0.0 OR L_VG:Extra_Inv ~= 0.0 OR |
                L_VG:Trans_Credit ~= 0.0 OR L_VG:C_Note ~= 0.0 OR L_VG:Ext_Invocie_TO ~= 0.0

             db.debugout('[--------]  Total_Cost: ' & L_VG:Total_Cost & ',  Total_T_O: ' & L_VG:Total_T_O & ',  Trans_Debit: ' & L_VG:Trans_Debit & |
                         ',  Extra_Inv: ' & L_VG:Extra_Inv & ',  Trans_Credit: ' & L_VG:Trans_Credit & '  C_Note: ' & L_VG:C_Note)

             PRINT(RPT:Detail)
          .

          LOC:Cur_Date            += 1
          IF LOC:Cur_Date >= MAN:CreatedDate
             BREAK
    .  .  .


    EXIT
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Manifest ID' & |
      '|' & 'By Branch' & |
      '|' & 'By Transporter' & |
      '|' & 'By Vehicle Composition' & |
      '|' & 'By Journey' & |
      '|' & 'By Driver' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      ! Print Totals here
      db.debugout('[Print_Turnover]  Total GP = L_TG:Total_TO (' & L_TG:Total_TO & ') - L_TG:Total_Cost (' & L_TG:Total_Cost & ')')
      L_TG:GP       = L_TG:Total_TO - L_TG:Total_Cost
      L_TG:GP_Per   = (L_TG:GP / L_TG:Total_TO) * 100
  
      PRINT(RPT:totals)
  
      IF L_SG:Output > 0
         CASE MESSAGE('Would you like to load the accumulation details now?|Note that if the file does not load automatically you can find it at the location shown below.||File: ' & PATH() & '\Print Turnover - ' & FORMAT(TODAY(), @d7) & '.csv', 'Print Turnover', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Print Turnover - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
    ENDPAGE(SELF.Report)
    IF SELF.SkipPreview
      TintRptViewer.CheckForPageNum(Self.PreviewQueue)
      TintRptViewer.SetupCopiesCollate(Self.PreviewQueue)
      if (TinTRptViewer.GetEjectDir())
        TinTRptViewer.ReversePreviewQueue(Self.PreviewQueue)
      end
      SELF.Report{PROP:FlushPreview} = True
      GlobalResponse = RequestCompleted
    ELSE
      GlobalResponse = TintRptViewer.Display(Report,Self.PreviewQueue,Report{Prop:Text},'Page Width',0)
      SELF.SkipPreview = TRUE
    END
    FREE(SELF.PreviewQueue)
   END
  ! You can place code in this embed point to test GlobalResponse to see if report printed or not.
  ! CASE GlobalResponse
  ! OF RequestCompleted     ! Report Was Printed
  !   ! Do Something
  ! OF RequestCancelled     ! Report Was NOT Printed
  !   ! Do Something
  ! END
   OMIT('***TINTOOLS***')
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  PARENT.AskPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  ***TINTOOLS***
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
      ! Doesn't work, using Tin Tools
  
  !  LOOP
  !     CASE MESSAGE('Print another copy?', 'Print', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
  !     OF BUTTON:Yes
  !        SELF.PrintReport()
  !     ELSE
  !        BREAK
  !  .  .
  !


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Turnover_June19')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_SG:BID',L_SG:BID)                                ! Added by: Report
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Manifest, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Manifest ID')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:BID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:TID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Vehicle Composition')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:VCID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Journey')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:JID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Driver')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:DRID,+MAN:MID')
  END
  ThisReport.SetFilter('MAN:State = 3 AND ((L_SG:BID = 0 OR L_SG:BID = MAN:BID) AND MAN:CreatedDate >= LO:From_Date AND MAN:CreatedDate << LO:To_Date + 1)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Manifest.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  FDB4.Init(?L_SG:BranchName,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(BRA:Key_BranchName)
  FDB4.AddField(BRA:BranchName,FDB4.Q.BRA:BranchName) !List box control field - type derived from field
  FDB4.AddField(BRA:BID,FDB4.Q.BRA:BID) !Primary key field - type derived from field
  FDB4.AddUpdateField(BRA:BID,L_SG:BID)
  ThisWindow.AddItem(FDB4.WindowComponent)
      LO:From_Date    = GETINI('Print_Turnover', 'From_Date', , GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Turnover', 'To_Date', , GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      PUTINI('Print_Turnover', 'From_Date', LO:From_Date, GLO:Local_INI)
      PUTINI('Print_Turnover', 'To_Date', LO:To_Date, GLO:Local_INI)
  
      
      L_SG:VAT    = GETINI('Print_Turnover', 'VAT', , GLO:Local_INI)
      L_SG:BID    = GETINI('Print_Turnover', 'BID', , GLO:Local_INI)
  
      IF L_SG:BID ~= 0
         BRA:BID  = L_SG:BID
         IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
            L_SG:BranchName   = BRA:BranchName
         .
      ELSE
         L_SG:BranchName   = 'All'
      .
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
    ! set printer eject style
    TinTRptViewer.SetEjectDir(TinEject:PageFaceUp)
    IF TintRptViewer.PrintViewerSetup() THEN
      Self:Response = RequestCancelled       ! Changed by LH 4/19/98
      POST(EVENT:CloseDown)                  ! Changed by LH 4/19/98
      RETURN Level:NOTIFY                    ! Changed by LH 4/19/98
    END
    SELF.SkipPreview = TintRptViewer.SkipPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:_SQLTemp.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
          ! ****---*** ???    Note: there is something wrong here - the L_SG:BID var is not being set by ABC templates, will use cludge below for now
          !                   Could it be Tintools?  or what?
      
          IF L_SG:BranchName ~= 'All'
             BRA:BranchName   = L_SG:BranchName
             IF Access:Branches.Fetch(BRA:Key_BranchName) = LEVEL:Benign
                L_SG:BID      = BRA:BID
          .  .
      
      
             
          SELECT(?Tab1)
          DISABLE(?Pause)
          DISPLAY
          PUTINI('Print_Turnover', 'VAT', L_SG:VAT, GLO:Local_INI)
          PUTINI('Print_Turnover', 'BID', L_SG:BID, GLO:Local_INI)
      
      
          EXECUTE L_SG:VAT + 1
             L_RG:VAT_Option  = 'Excluding VAT'
             L_RG:VAT_Option  = 'Including VAT'
          .
          IF L_SG:Output > 0
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             Add_Log('Start', 'Print_Turnover', 'Print Turnover - ' & FORMAT(TODAY(), @d7) & '.csv', TRUE, 1)
      
             CASE MESSAGE('Would you like to empty the Audit Summary table?','Audit Summary', ICON:Question, BUTTON:Yes+BUTTON:No,|
                      BUTTON:No)
             OF BUTTON:Yes
                _SQLTemp{PROP:SQL}    = 'DELETE FROM Audit_ManagementProfit'
          .  .
      
      
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_SG:BranchName
          MESSAGE('L_SG:BID: ' & L_SG:BID)
          
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
              ! need to check for un-manifested invoices
              ! need to add in dates from too check to make sure all dates are included
  
      ! We have a Q of done dates, we need to check if the current date is the date after the last date
      ! or the date after the start date
      ! This will work as all orders are by date
  
      ! Check if the current record date is the next record
      DO Check_Missing_Invs
  
  
  
  
  
      ! ==============   normal processing   ==============
      CLEAR(LOC:Values)
  
      L_VG:MID                    = MAN:MID
      L_VG:CreatedDate            = MAN:CreatedDate
  
  
      ! Get_Manifest_Info     
      !   p:Option
      !       0.  Charges total
      !       1.  Insurance total
      !       2.  Weight total, for this Manifest!    Uses Units_Loaded
      !       3.  Extra Legs cost
      !       4.  Charges total incl VAT
      !       5.  No. DI's
      !   p:DI_Type
      !       0.  All             (default)
      !       1.  Non-Broking
      !       2.  Broking
  
  !    IF MAN:Broking = TRUE
         L_VG:Kgs_Broking         = Get_Manifest_Info(MAN:MID, 2, 2,, L_SG:Output)
  !    ELSE
         L_VG:Kgs_O_Night         = Get_Manifest_Info(MAN:MID, 2, 1,, L_SG:Output)
  !    .
  
      ! Below is no longer possible, the Broking status is set on the Manifest
  
  !    IF L_VG:Kgs_Broking ~= 0.0 AND L_VG:Kgs_O_Night ~= 0.0
  !       ! Error - some DIs are overnight some are broking...
  !       
  !       ! (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel)       (BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0)
  !       Add_SystemLog(100, 'Print_Turnover', 'MID: ' & MAN:MID & ', has both Broking and O/Night DIs attached to it!', 'Please correct.')
  !       LOC:Errors              += 1
  !    .
  
      IF MAN:Broking = FALSE                !L_VG:Kgs_Broking = 0.0
         IF L_SG:VAT = TRUE
            L_VG:T_O_O_Night      = Get_Manifest_Info(MAN:MID, 4,,1, L_SG:Output)             ! All charges - from invoice    inc
            !L_VG:T_O_O_Night      = Get_Invoices(0, 0, 1, FALSE, MAN:MID,, L_SG:Output, 1)   - this would also work
  
            L_VG:T_O_Broking      = Get_InvoicesTransporter(0, 0, 1, 0, 1, MAN:MID,,,, 2, L_SG:Output,1)           ! Del legs, broking - inc vat
  
            ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
            IF L_VG:T_O_Broking > 0.0
               Add_Log('MID: ' & MAN:MID & ',  L_VG:T_O_Broking: ' & L_VG:T_O_Broking & ',  MAN:CreatedDate: ' & FORMAT(MAN:CreatedDate,@d6), 'Get_Manifest_Info-Extra', 'Extra.log',,, 0)
            .
         ELSE
            L_VG:T_O_O_Night      = Get_Manifest_Info(MAN:MID, 0,,1, L_SG:Output)             ! All charges - from invoice
            L_VG:T_O_Broking      = Get_InvoicesTransporter(0, 1, 1, 0, 1, MAN:MID,,,, 2, L_SG:Output,1)           ! Del legs, broking - ex vat
         .
  
     db.debugout('[Print_Turnover]  MAN:MID: ' & MAN:MID & ', T_O_O_Night: ' & L_VG:T_O_O_Night & ',  L_VG:T_O_Broking: ' & L_VG:T_O_Broking)
  
         L_VG:T_O_O_Night        -= L_VG:T_O_Broking                                          ! Legs cost off total DI value (included)
      ELSE
         L_VG:T_O_O_Night         = 0.0
         IF L_SG:VAT = TRUE
            L_VG:T_O_Broking      = Get_Manifest_Info(MAN:MID, 4,,1, L_SG:Output)             ! All charges - from invoice    inc
  
            IF L_VG:T_O_Broking > 0.0
               Add_Log('MID: ' & MAN:MID & ',  L_VG:T_O_Broking: ' & L_VG:T_O_Broking  & ',  MAN:CreatedDate: ' & FORMAT(MAN:CreatedDate,@d6), 'Get_Manifest_Info-Extra', 'Extra.log',,, 0)
            .
         ELSE
            L_VG:T_O_Broking      = Get_Manifest_Info(MAN:MID, 0,,1, L_SG:Output)             ! All charges - from invoice
      .  .
  
      
      ! Get_Invoices          (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID, p:ToDate, p:Output, p:Limit_On)
      ! p:Option
      !   0.  - Total Charges inc.
      !   1.  - Excl VAT
      !   2.  - VAT
      ! p:Type
      !   0.  - All
      !   1.  - Invoices                      Total >= 0.0
      !   2.  - Credit Notes                  Total < 0.0
      !   3.  - Credit Notes excl Bad Debts   Total < 0.0
      ! p:Limit_On
      !   0.  Date & Branch
      !   1.  MID
  
      L_DQ:Date                   = MAN:CreatedDate
      GET(LOC:Dates_Done, L_DQ:Date)
      IF ERRORCODE()
         Cred_Type_#      = 2
         IF L_SG:ExcludeBadDebts = TRUE
            Cred_Type_#   = 3
         .
  
         IF L_SG:VAT = TRUE
            ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID, p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source)
            L_VG:C_Note           = -Get_Invoices(MAN:CreatedDate, 0, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1)    ! Any passed on this day
         ELSE
            L_VG:C_Note           = -Get_Invoices(MAN:CreatedDate, 1, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1)    ! Any passed on this day
      .  .
  
      IF L_SG:VAT = TRUE
         ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type, p:Output, p:Source)
         ! (LONG  , BYTE    , BYTE=0, BYTE=0        , BYTE=0        , <ULONG>, ULONG=0, <LONG>  , BYTE=0     , BYTE=0         , BYTE=0  , BYTE=0),STRING
         !   1           2       3         4               5           6        7         8          9           10              11     , 12    
         ! p:Option                                                2
         !   0.  - Total Charges inc.
         !   1.  - Excl VAT
         !   2.  - VAT
         ! p:Type                                                  3
         !   0.  - All
         !   1.  - Invoices                      Total >= 0.0
         !   2.  - Credit Notes                  Total < 0.0
         ! p:DeliveryLegs                                          5
         !   0.  - None
         !   1.  - Del Legs only
         !   2.  - Both
         ! p:MID                                                   6
         !   0   = Not for a MID
         !   x   = for a MID
         ! p:Extra_Inv                                             9
         !   Only
         ! p:Invoice_Type (was Manifest_Type)                      10
         !   0   - All
         !   1   - Overnight
         !   2   - Broking
         ! p:Source                                                12
         !   1   - Management Profit
         !   2   - Management Profit Summary
         L_VG:Loadmaster       = Get_InvoicesTransporter(0, 0, 1, 0, 0, MAN:MID,,,, 1, L_SG:Output,1)
         L_VG:Trans_Broking    = Get_InvoicesTransporter(0, 0, 1, 0, 0, MAN:MID,,,, 2, L_SG:Output,1)
      ELSE
         L_VG:Loadmaster       = Get_InvoicesTransporter(0, 1, 1, 0, 0, MAN:MID,,,, 1, L_SG:Output,1)
         L_VG:Trans_Broking    = Get_InvoicesTransporter(0, 1, 1, 0, 0, MAN:MID,,,, 2, L_SG:Output,1)
      .
  
  
      ! Get all Extra Invoices not associated to a MID
      GET(LOC:Dates_Done, L_DQ:Date)
      IF ERRORCODE()
         IF L_SG:VAT = TRUE
            L_VG:Ext_Invocie_TO   = Get_Invoices(MAN:CreatedDate, 0, 1, TRUE, L_SG:BID,, L_SG:Output,,,1)   ! Invoice for day not related to MID
  
            IF L_VG:Ext_Invocie_TO > 0.0
               Add_Log('MID: ' & MAN:MID & ',  L_VG:Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',   MAN:CreatedDate: ' & FORMAT(MAN:CreatedDate,@d6), 'Get_Manifest_Info-Extra', 'Extra.log',,, 0)
            .
  
            ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID, p:BID, p:ToDate, p:Extra_Inv, p:Manifest_Type, p:Output)
            L_VG:Extra_Inv        = Get_InvoicesTransporter(MAN:CreatedDate, 0, 1, FALSE,,, L_SG:BID,,TRUE,, L_SG:Output,1)    ! Credits for day - Transporter not MID
            L_VG:Trans_Credit     = -Get_InvoicesTransporter(MAN:CreatedDate, 0, 2, TRUE,,, L_SG:BID,,,, L_SG:Output,1)         ! Credits for day - Transporter not MID
  
            L_VG:Trans_Debit      = Get_InvoicesTransporter(MAN:CreatedDate, 0, 2,,,, L_SG:BID,,,, L_SG:Output,1)
  
     db.debugout('[Print_Turnover]  Extra invoices - Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',  Extra_Inv: ' & L_VG:Extra_Inv & ',  Trans_Credit: ' & L_VG:Trans_Credit & ',  Trans_Debit: ' & L_VG:Trans_Debit)
  
         ELSE
            L_VG:Ext_Invocie_TO   = Get_Invoices(MAN:CreatedDate, 1, 1, TRUE, L_SG:BID,, L_SG:Output,,,1)   ! Invoice for day not related to MID
  
            L_VG:Extra_Inv        = Get_InvoicesTransporter(MAN:CreatedDate, 1, 1, FALSE,,, L_SG:BID,,TRUE,, L_SG:Output,1)      ! Credits for day - Transporter not MID
            L_VG:Trans_Credit     = -Get_InvoicesTransporter(MAN:CreatedDate, 1, 2, TRUE,,, L_SG:BID,,,, L_SG:Output,1)     ! Credits for day - Transporter not MID
  
            L_VG:Trans_Debit      = Get_InvoicesTransporter(MAN:CreatedDate, 1, 2,,,, L_SG:BID,,,, L_SG:Output,1)
      .  .
  
      L_VG:Total_Cost             = L_VG:Loadmaster + L_VG:Trans_Broking + L_VG:Extra_Inv - L_VG:Trans_Credit
  
      L_VG:Total_Cost_Less_Deb    = L_VG:Total_Cost - L_VG:Trans_Debit
  
     db.debugout('[Print_Turnover]  Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',  T_O_Broking: ' & L_VG:T_O_Broking & ',  C_Note: ' & L_VG:C_Note)
  
      L_VG:T_O_Broking           += L_VG:Ext_Invocie_TO                                                   ! Add extras to BROKING ALWAYS
  
      L_VG:Total_T_O              = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note
  
  
      L_VG:GP                     = L_VG:Total_T_O - L_VG:Total_Cost_Less_Deb
      L_VG:GP_Per                 = (L_VG:GP / L_VG:Total_T_O) * 100
  
  
  
      L_DQ:Date                   = MAN:CreatedDate
      GET(LOC:Dates_Done, L_DQ:Date)
      IF ERRORCODE()
         L_DQ:Date                = MAN:CreatedDate
         ADD(LOC:Dates_Done, L_DQ:Date)
      .
  IF 0
    PRINT(RPT:totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
      IF LOC:Errors > 0
         MESSAGE('Completed with Errors: ' & LOC:Errors & '||Please review the System Log.', 'Print Turnover', ICON:Exclamation)
      .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Turnover_June19','Print_Turnover_June19','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True


FDB4.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      ! Add an All
      Queue:FileDrop.BRA:BranchName           = 'All'
      GET(Queue:FileDrop, Queue:FileDrop.BRA:BranchName)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop)
         Queue:FileDrop.BRA:BranchName        = 'All'
         Queue:FileDrop.BRA:BID               = 0
         ADD(Queue:FileDrop)
  
         IF L_SG:BID = 0
            L_SG:BranchName   = 'All'
      .  .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_TripSheet_Collections_old PROCEDURE (p:TRID)

Progress:Thermometer BYTE                                  ! 
LOC:Rep_Fields       GROUP,PRE(L_RF)                       ! 
Weight               DECIMAL(10,2)                         ! In kg's
VolumetricWeight     DECIMAL(10,2)                         ! Weight based on Volumetric calculation (in kgs)
Charge               DECIMAL(10,2)                         ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
State                STRING(20)                            ! State of this manifest
Driver               STRING(50)                            ! First Name
Driver_Assistant     STRING(35)                            ! 
Reg_of_Horse         STRING(20)                            ! 
Reg_of_Trailer       STRING(20)                            ! 
COD_Comment          STRING(20)                            ! 
TransporterName      STRING(55)                            ! Transporters Name
Suburb               STRING(50)                            ! Suburb
                     END                                   ! 
LOC:Delivery_Totals  GROUP,PRE(L_DT)                       ! 
DID                  ULONG                                 ! Delivery ID
InsuranceCharge      DECIMAL(9,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Insurance            DECIMAL(9,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Docs                 DECIMAL(9,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Fuel                 DECIMAL(9,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Charge               DECIMAL(9,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
                     END                                   ! 
LOC:Manifest_Totals  GROUP,PRE(L_MT)                       ! 
Cost                 DECIMAL(10,2)                         ! 
TRID                 ULONG                                 ! Tripsheet ID
Delivery_Charges     DECIMAL(11,2)                         ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Total_Weight         DECIMAL(10,2)                         ! In kg's
Gross_Profit         DECIMAL(10,2)                         ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Gross_Profit_Percent DECIMAL(6,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Average_C_Per_Kg     DECIMAL(7,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Legs_Cost            DECIMAL(10,2)                         ! 
Total_Cost           DECIMAL(10,2)                         ! Includes extra legs cost
                     END                                   ! 
Process:View         VIEW(TripSheets)
                       PROJECT(TRI:DepartDate)
                       PROJECT(TRI:DepartTime)
                       PROJECT(TRI:TRID)
                       PROJECT(TRI:VCID)
                       PROJECT(TRI:BID)
                       JOIN(VCO:PKey_VCID,TRI:VCID)
                         PROJECT(VCO:CompositionName)
                         PROJECT(VCO:TID)
                       END
                       JOIN(BRA:PKey_BID,TRI:BID)
                         PROJECT(BRA:BranchName)
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(615,2417,10396,5302),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('Arial',10,,FONT:regular, |
  CHARSET:ANSI),THOUS
                       HEADER,AT(615,1000,10396,1417),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Trip Sheet No.:'),AT(7615,63),USE(?String31),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@N_10),AT(9135,63,1021,167),USE(TRI:TRID),FONT(,,,FONT:bold,CHARSET:ANSI),RIGHT(1), |
  TRN
                         STRING('Trip Sheet'),AT(458,52,9479,417),USE(?String55),FONT(,24,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         IMAGE('fbn_logo_small.jpg'),AT(52,52,2302,458),USE(?Image1)
                         STRING('Branch:'),AT(63,563,1083,167),USE(?String51),TRN
                         STRING(@s35),AT(1063,563),USE(BRA:BranchName)
                         STRING('Arrival Time Depot: _{25}'),AT(7615,323),USE(?String52:3),TRN
                         STRING('Driver:'),AT(63,771,1083,167),USE(?String69:2),TRN
                         STRING(@s35),AT(1063,771,1583,167),USE(L_RF:Driver)
                         STRING('Assistant:'),AT(5073,625),USE(?String52:7),TRN
                         STRING(@s35),AT(5719,625,1823,156),USE(L_RF:Driver_Assistant),TRN
                         STRING('Supervisor Signature: _{23}'),AT(7615,625),USE(?String52:4),TRN
                         STRING('Date Despatched:'),AT(63,979,1083,167),USE(?String3:11),LEFT,TRN
                         STRING('Time Out:'),AT(63,1198,1083),USE(?String3:10),LEFT,TRN
                         STRING(@d6),AT(1063,979,,167),USE(TRI:DepartDate),RIGHT(1)
                         STRING(@t7),AT(1063,1198,729,167),USE(TRI:DepartTime),RIGHT(1)
                         STRING('Drivers Signature: _{26}'),AT(7615,927),USE(?String52:5),TRN
                         STRING('Starting Kms: _{23}'),AT(5073,927),USE(?String52),TRN
                         STRING('Manager Signature: _{25}'),AT(7615,1240),USE(?String52:6),TRN
                         STRING('Closing Kms: _{23}'),AT(5073,1240),USE(?String52:2),TRN
                       END
break_tripsheet        BREAK(TRDI:TRID)
                         HEADER,AT(0,0,,563),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI),PAGEBEFORE(-1)
                           STRING('Transporter:'),AT(63,52,1083),USE(?String3:2),LEFT,TRN
                           STRING(@s55),AT(1063,52,3073,156),USE(L_RF:TransporterName),LEFT
                           LINE,AT(52,240,10350,0),USE(?Line8:4),COLOR(COLOR:Black)
                           STRING(@s35),AT(938,292,1583,167),USE(VCO:CompositionName),TRN
                           LINE,AT(52,510,10350,0),USE(?Line8:6),COLOR(COLOR:Black)
                           STRING('Reg. Horse:'),AT(2969,292),USE(?String69:3),TRN
                           STRING(@s20),AT(3646,292,979,167),USE(L_RF:Reg_of_Horse)
                           STRING('Trailer:'),AT(5188,292),USE(?String69:4),TRN
                           STRING(@s20),AT(5625,292),USE(L_RF:Reg_of_Trailer)
                           STRING('Vehicle Comp.:'),AT(125,292),USE(?String69),TRN
                           STRING('State:'),AT(8563,63),USE(?String54),FONT(,,,FONT:regular,CHARSET:ANSI),TRN
                           STRING(@s20),AT(8917,63,,167),USE(L_RF:State),FONT(,,,FONT:regular,CHARSET:ANSI),RIGHT(1), |
  TRN
                           STRING(@s50),AT(8135,292),USE(L_RF:Suburb)
                           STRING('Suburb:'),AT(7698,292),USE(?String54:2),FONT(,,,FONT:regular,CHARSET:ANSI),TRN
                         END
break_delivery           BREAK(DEL:DID)
Detail                     DETAIL,AT(,,,0),USE(?Detail),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                             GROUP,AT(31,-21,9650,281),USE(?Group1),BOXED,HIDE,TRN
                               STRING('Not printing detail lines at all'),AT(52,73,885,167),USE(?String1),TRN
                               STRING(@n-11.2),AT(4594,73,677,167),USE(L_RF:VolumetricWeight),RIGHT(1),HIDE
                               STRING(@n~CW~-13.2),AT(2469,73,875,167),USE(L_RF:Weight),RIGHT(1)
                             END
                           END
                         END
                       END
Collections            DETAIL,AT(,,,4667),USE(?collections),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI),TOGETHER
                         STRING('Collections'),AT(52,0,10313,260),USE(?String66),FONT(,12,,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         STRING('No.'),AT(63,252),USE(?String58),TRN
                         STRING('DI Number'),AT(363,252),USE(?String58:9),TRN
                         STRING('From'),AT(1292,260,1563,156),USE(?String58:2),CENTER,TRN
                         STRING('To'),AT(3021,260,1823,156),USE(?String58:3),CENTER,TRN
                         STRING('Weight (kg)'),AT(4979,250,938,167),USE(?String58:4),CENTER,TRN
                         STRING('No. of Packages'),AT(6094,250,938,167),USE(?String58:5),CENTER,TRN
                         STRING('Time In'),AT(7063,250,938,167),USE(?String58:6),CENTER,TRN
                         STRING('Time Out'),AT(8000,250,938,167),USE(?String58:7),CENTER,TRN
                         STRING('Signature'),AT(9281,260,729,156),USE(?String58:8),CENTER,TRN
                         LINE,AT(300,458,0,4200),USE(?Line14),COLOR(COLOR:Black)
                         LINE,AT(1165,458,0,4200),USE(?Line14:1),COLOR(COLOR:Black)
                         LINE,AT(2948,458,0,4200),USE(?Line14:3),COLOR(COLOR:Black)
                         LINE,AT(4865,458,0,4200),USE(?Line14:7),COLOR(COLOR:Black)
                         LINE,AT(6063,458,0,4200),USE(?Line14:6),COLOR(COLOR:Black)
                         LINE,AT(7073,458,0,4200),USE(?Line14:5),COLOR(COLOR:Black)
                         LINE,AT(8000,458,0,4200),USE(?Line14:4),COLOR(COLOR:Black)
                         LINE,AT(8946,458,0,4200),USE(?Line14:2),COLOR(COLOR:Black)
                         STRING('1'),AT(63,479,208,156),USE(?String58:10),TRN
                         STRING('2'),AT(63,677,208,156),USE(?String58:11),TRN
                         STRING('3'),AT(63,885,208,156),USE(?String58:12),TRN
                         STRING('4'),AT(63,1083,208,156),USE(?String58:13),TRN
                         STRING('5'),AT(63,1281,208,156),USE(?String58:14),TRN
                         STRING('6'),AT(63,1479,208,156),USE(?String58:15),TRN
                         STRING('7'),AT(63,1677,208,156),USE(?String58:16),TRN
                         STRING('8'),AT(63,1885,208,156),USE(?String58:17),TRN
                         STRING('9'),AT(63,2085,208,156),USE(?String58:171),TRN
                         STRING('10'),AT(63,2285,208,156),USE(?String58:172),TRN
                         STRING('11'),AT(63,2485,208,156),USE(?String58:173),TRN
                         STRING('12'),AT(63,2685,208,156),USE(?String58:174),TRN
                         STRING('13'),AT(63,2885,208,156),USE(?String58:175),TRN
                         STRING('14'),AT(63,3085,208,156),USE(?String58:176),TRN
                         STRING('15'),AT(63,3285,208,156),USE(?String58:177),TRN
                         STRING('16'),AT(63,3485,208,156),USE(?String58:178),TRN
                         STRING('17'),AT(63,3685,208,156),USE(?String58:179),TRN
                         STRING('18'),AT(63,3885,208,156),USE(?String58:1781),TRN
                         STRING('19'),AT(63,4085,208,156),USE(?String58:1782),TRN
                         STRING('20'),AT(63,4285,208,156),USE(?String58:1783),TRN
                         STRING('21'),AT(63,4485,208,156),USE(?String58:1784),TRN
                         LINE,AT(42,450,10350,0),USE(?Line13),COLOR(COLOR:Black)
                         LINE,AT(42,650,10350,0),USE(?Line13:2),COLOR(COLOR:Black)
                         LINE,AT(42,850,10350,0),USE(?Line13:3),COLOR(COLOR:Black)
                         LINE,AT(42,1050,10350,0),USE(?Line13:4),COLOR(COLOR:Black)
                         LINE,AT(42,1250,10350,0),USE(?Line13:41),COLOR(COLOR:Black)
                         LINE,AT(42,1450,10350,0),USE(?Line13:51),COLOR(COLOR:Black)
                         LINE,AT(42,1650,10350,0),USE(?Line13:61),COLOR(COLOR:Black)
                         LINE,AT(42,1850,10350,0),USE(?Line13:71),COLOR(COLOR:Black)
                         LINE,AT(42,2050,10350,0),USE(?Line13:81),COLOR(COLOR:Black)
                         LINE,AT(42,2250,10350,0),USE(?Line13:811),COLOR(COLOR:Black)
                         LINE,AT(42,2450,10350,0),USE(?Line13:822),COLOR(COLOR:Black)
                         LINE,AT(42,2650,10350,0),USE(?Line13:832),COLOR(COLOR:Black)
                         LINE,AT(42,2850,10350,0),USE(?Line13:842),COLOR(COLOR:Black)
                         LINE,AT(42,3050,10350,0),USE(?Line13:852),COLOR(COLOR:Black)
                         LINE,AT(42,3250,10350,0),USE(?Line13:862),COLOR(COLOR:Black)
                         LINE,AT(42,3450,10350,0),USE(?Line13:872),COLOR(COLOR:Black)
                         LINE,AT(42,3650,10350,0),USE(?Line13:882),COLOR(COLOR:Black)
                         LINE,AT(42,3850,10350,0),USE(?Line13:892),COLOR(COLOR:Black)
                         LINE,AT(42,4050,10350,0),USE(?Line13:8212),COLOR(COLOR:Black)
                         LINE,AT(42,4250,10350,0),USE(?Line13:8222),COLOR(COLOR:Black)
                         LINE,AT(42,4450,10350,0),USE(?Line13:8232),COLOR(COLOR:Black)
                         LINE,AT(42,4650,10350,0),USE(?Line13:8242),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(615,7740,10396,240),USE(?Footer),FONT('MS Sans Serif',,COLOR:Black,,CHARSET:ANSI)
                         STRING('Report Date:'),AT(73,31),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(875,31),USE(?ReportDateStamp),TRN
                         STRING('Report Time:'),AT(2250,31),USE(?ReportTimePrompt),TRN
                         STRING('<<-- Time Stamp -->'),AT(3052,31),USE(?ReportTimeStamp),TRN
                         STRING(@N3),AT(10031,31),USE(ReportPageNumber)
                         STRING('Page:'),AT(9615,31),USE(?String61),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.AskPreview PROCEDURE

  CODE
          PRINT(RPT:Collections)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_TripSheet_Collections_old')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Add_Suburbs.SetOpenRelated()
  Relate:Add_Suburbs.Open                                  ! File Add_Suburbs used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterAlias.Open                             ! File TransporterAlias used by this procedure, so make sure it's RelationManager is open
  Access:TripSheetDeliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailerAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_TripSheet_Collections_old',ProgressWindow) ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:TripSheets, ?Progress:PctText, Progress:Thermometer, ProgressMgr, TRI:TRID)
  ThisReport.AddSortOrder(TRI:PKey_TID)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:TripSheets.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      IF p:TRID ~= 0
         ThisReport.AddRange(TRI:TRID, p:TRID)
      .
  
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Suburbs.Close
    Relate:TransporterAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_TripSheet_Collections_old',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      A_TRU:TTID              = VCO:TTID0
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Horse    = A_TRU:Registration
      .
      A_TRU:TTID              = VCO:TTID1
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Trailer  = A_TRU:Registration
      .
  
      DRI:DRID                = TRI:DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RF:Driver          = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
      DRI:DRID                    = TRI:Assistant_DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RF:Driver_Assistant    = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
  
  
      A_TRA:TID            = VCO:TID
      IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) = LEVEL:Benign
         L_RF:TransporterName = A_TRA:TransporterName
      .
  
      SUBU:SUID           = TRI:SUID
      IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) = LEVEL:Benign
         L_RF:Suburb      = SUBU:Suburb
      .
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:Collections)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF L_MT:TRID ~= TRI:TRID
         !L_MT:MID                     = MAL:MID
         L_MT:TRID                    = TRI:TRID
  
         EXECUTE TRI:State
            L_RF:State  = 'Loaded'
            L_RF:State  = 'On Route'
            L_RF:State  = 'Transferred'
         ELSE
            L_RF:State  = 'Loading'
      .  .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_TripSheet_Collections_old','Print_TripSheet_Collections_old','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! (Manifest based)
!!! </summary>
Print_Turnover_aug28 PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Errors           ULONG                                 ! 
LOC:Settings         GROUP,PRE(L_SG)                       ! 
VAT                  BYTE                                  ! 
BID                  ULONG                                 ! Branch ID
BranchName           STRING(35)                            ! Branch Name
ExcludeBadDebts      BYTE(1)                               ! Exclude Bad Debt Credit Notes
Output               BYTE                                  ! Output trail of items
                     END                                   ! 
LOC:Values           GROUP,PRE(L_VG)                       ! 
Kgs_Broking          DECIMAL(7)                            ! 
Kgs_O_Night          DECIMAL(7)                            ! 
T_O_Broking          DECIMAL(10,2)                         ! 
T_O_O_Night          DECIMAL(10,2)                         ! 
C_Note               DECIMAL(10,2)                         ! 
Total_T_O            DECIMAL(10,2)                         ! 
Loadmaster           DECIMAL(10,2)                         ! 
Trans_Broking        DECIMAL(10,2)                         ! 
Extra_Inv            DECIMAL(10,2)                         ! 
Trans_Credit         DECIMAL(10,2)                         ! 
Total_Cost           DECIMAL(10,2)                         ! 
Trans_Debit          DECIMAL(10,2)                         ! 
Total_Cost_Less_Deb  DECIMAL(10,2)                         ! 
GP                   DECIMAL(10)                           ! 
GP_Per               DECIMAL(10,2)                         ! 
MID                  ULONG                                 ! Manifest ID
CreatedDate          DATE                                  ! Created Date
Ext_Invocie_TO       DECIMAL(12,2)                         ! Turn over extra invoice
                     END                                   ! 
Totals_Group         GROUP,PRE(L_TG)                       ! 
GP                   DECIMAL(12,2)                         ! 
GP_Per               DECIMAL(7,2)                          ! 
Total_TO             DECIMAL(12,2)                         ! 
Total_Cost           DECIMAL(12,2)                         ! 
                     END                                   ! 
LOC:Dates_Done       QUEUE,PRE(L_DQ)                       ! 
Date                 DATE                                  ! 
                     END                                   ! 
LOC:Options          GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
LOC:Cur_Date         DATE                                  ! 
Process:View         VIEW(Manifest)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:Broking)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:CreatedDate)
                       PROJECT(MAN:State)
                       PROJECT(MAN:VATRate)
                     END
FDB4::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
ProgressWindow       WINDOW('Report Turnover (Manifest)'),AT(,,177,126),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       SHEET,AT(4,2,169,102),USE(?Sheet1)
                         TAB('Settings'),USE(?Tab2)
                           PROMPT('VAT:'),AT(32,22),USE(?VAT:Prompt)
                           LIST,AT(55,22,60,10),USE(L_SG:VAT),DROP(5),FROM('Excluding|#0|Including|#1')
                           PROMPT('Branch:'),AT(24,42),USE(?Prompt2)
                           LIST,AT(55,42,105,10),USE(L_SG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                           CHECK(' Exclude Bad Debts'),AT(55,60),USE(L_SG:ExcludeBadDebts),MSG('Exclude Bad Debt C' & |
  'redit Notes'),TIP('Exclude Bad Debt Credit Notes')
                           PROMPT('Output:'),AT(26,84),USE(?L_SG:Output_Trail:Prompt)
                           LIST,AT(55,84,60,10),USE(L_SG:Output),DROP(5),FROM('None|#0|All|#1'),MSG('Output trail of items'), |
  TIP('Output trail of items')
                         END
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(36,34,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(20,22,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(20,50,141,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Cancel'),AT(124,108,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                       BUTTON('Pause'),AT(70,108,49,15),USE(?Pause),LEFT,ICON('Blue_R.ico'),FLAT
                     END

Report               REPORT('Manifest Report'),AT(250,958,11000,6854),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,11000,698),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Management Profit Report'),AT(0,21,10573),USE(?ReportTitle),FONT(,12,,FONT:bold),CENTER
                         BOX,AT(0,469,10573,260),USE(?HeaderBox),COLOR(COLOR:Black)
                         BOX,AT(0,260,10573,219),USE(?HeaderBox:2),COLOR(COLOR:Black)
                         STRING('Total'),AT(8750,292,833,167),USE(?HeaderTitle:15),CENTER,TRN
                         STRING('GP %'),AT(9635,500,833,167),USE(?HeaderTitle:17),CENTER,TRN
                         STRING('Trans Dr'),AT(7969,292,833,167),USE(?HeaderTitle:14),CENTER,TRN
                         STRING('GP'),AT(9635,292,833,167),USE(?HeaderTitle:16),CENTER,TRN
                         STRING('Trans Cr.'),AT(6333,292,833,167),USE(?HeaderTitle:12),CENTER,TRN
                         STRING('Total'),AT(7156,292,833,167),USE(?HeaderTitle:13),CENTER,TRN
                         STRING('T/O O/Night'),AT(2042,521,833,167),USE(?HeaderTitle:8),CENTER,TRN
                         STRING('Trans Brok'),AT(4594,292,833,167),USE(?HeaderTitle:10),CENTER,TRN
                         STRING('Loadmaster'),AT(4594,521,833,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Ext. Inv.'),AT(5490,292,833,167),USE(?HeaderTitle:11),CENTER,TRN
                         STRING('C/Note'),AT(2927,292,833,167),USE(?HeaderTitle:9),CENTER,TRN
                         STRING('MID'),AT(646,292,688,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Kgs Brok'),AT(1396,292,594,167),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Kgs O/N'),AT(1396,521,594,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Date'),AT(52,292,583,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('T/O Brok'),AT(2042,292,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Total T/O'),AT(3760,292,833,167),USE(?HeaderTitle:5),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,11000,396),USE(?Detail)
                         LINE,AT(0,406,10333,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         STRING(@n_10),AT(646,31,,170),USE(L_VG:MID),RIGHT(1),TRN
                         STRING(@n-10.0),AT(1396,31),USE(L_VG:Kgs_Broking),RIGHT(1),TRN
                         STRING(@n-10.0),AT(1396,208),USE(L_VG:Kgs_O_Night),RIGHT(1),TRN
                         STRING(@n-14.2),AT(2042,31),USE(L_VG:T_O_Broking),RIGHT(1),TRN
                         STRING(@n-14.2),AT(2042,208),USE(L_VG:T_O_O_Night),RIGHT(1),TRN
                         STRING(@n-14.2),AT(2927,31),USE(L_VG:C_Note),RIGHT(1),TRN
                         STRING(@n-14.2),AT(3760,31),USE(L_VG:Total_T_O),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4594,208),USE(L_VG:Loadmaster),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4594,31),USE(L_VG:Trans_Broking),RIGHT(1),TRN
                         STRING(@n-14.2),AT(5490,31),USE(L_VG:Extra_Inv),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6333,31),USE(L_VG:Trans_Credit),RIGHT(1),TRN
                         STRING(@d5),AT(52,31,,170),USE(L_VG:CreatedDate),RIGHT(1),TRN
                         STRING(@n-14.2),AT(7156,31),USE(L_VG:Total_Cost),RIGHT(1),TRN
                         STRING(@n-14.2),AT(7969,31),USE(L_VG:Trans_Debit),RIGHT(1),TRN
                         STRING(@n-14.2),AT(8750,31),USE(L_VG:Total_Cost_Less_Deb),RIGHT(1),TRN
                         STRING(@n-14.0),AT(9656,31),USE(L_VG:GP),RIGHT(1),TRN
                         STRING(@n-14.2),AT(9635,208),USE(L_VG:GP_Per),RIGHT(1),TRN
                       END
totals                 DETAIL,AT(,,,542),USE(?totals)
                         STRING('Totals'),AT(104,73,885),USE(?String56),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@n-10.0),AT(1396,73),USE(L_VG:Kgs_Broking,,?L_VG:Kgs_Broking:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-10.0),AT(1396,260),USE(L_VG:Kgs_O_Night,,?L_VG:Kgs_O_Night:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(2042,73),USE(L_VG:T_O_Broking,,?L_VG:T_O_Broking:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(2042,260),USE(L_VG:T_O_O_Night,,?L_VG:T_O_O_Night:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(2927,73),USE(L_VG:C_Note,,?L_VG:C_Note:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(3760,73),USE(L_VG:Total_T_O,,?L_VG:Total_T_O:2),RIGHT(1),SUM(L_TG:Total_TO), |
  TALLY(Detail),TRN
                         STRING(@n-14.2),AT(4594,73),USE(L_VG:Loadmaster,,?L_VG:Loadmaster:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(4594,260),USE(L_VG:Trans_Broking,,?L_VG:Trans_Broking:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(5490,73),USE(L_VG:Extra_Inv,,?L_VG:Extra_Inv:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(6333,73),USE(L_VG:Trans_Credit,,?L_VG:Trans_Credit:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(7156,73),USE(L_VG:Total_Cost,,?L_VG:Total_Cost:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(7969,73),USE(L_VG:Trans_Debit,,?L_VG:Trans_Debit:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(8750,73),USE(L_VG:Total_Cost_Less_Deb,,?L_VG:Total_Cost_Less_Deb:2),RIGHT(1), |
  SUM(L_TG:Total_Cost),TALLY(Detail),TRN
                         STRING(@n-14.2),AT(9688,73),USE(L_TG:GP),RIGHT(1),TRN
                         STRING(@n-10.2),AT(9906,260),USE(L_TG:GP_Per),RIGHT(1),TRN
                       END
                       FOOTER,AT(250,7813,11000,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(9521,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,10333,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,10333,11188),USE(?FormImage),TILED
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END

TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Missing_Invs                          ROUTINE
    CLEAR(LOC:Dates_Done)
    GET(LOC:Dates_Done, RECORDS(LOC:Dates_Done))
    ! The last date done - and we are not on 1st date of range
    IF MAN:CreatedDate > L_DQ:Date + 1 AND MAN:CreatedDate ~= LO:From_Date
       ! Check for invoices not associated with a Manifest in this period
       IF L_DQ:Date = 0
          LOC:Cur_Date             = LO:From_Date
       ELSE
          LOC:Cur_Date             = L_DQ:Date + 1
       .

       LOOP
          CLEAR(LOC:Values)

          Cred_Type_#      = 2
          IF L_SG:ExcludeBadDebts = TRUE
             Cred_Type_#   = 3
          .
          IF L_SG:VAT = TRUE
             L_VG:C_Note           = -Get_Invoices(LOC:Cur_Date, 0, Cred_Type_#,, L_SG:BID)          ! Any passed on this day
          ELSE
             L_VG:C_Note           = -Get_Invoices(LOC:Cur_Date, 1, Cred_Type_#,, L_SG:BID)          ! Any passed on this day
          .

          L_VG:Total_T_O           = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note

          IF L_SG:VAT = TRUE
             L_VG:Extra_Inv        = Get_Invoices(LOC:Cur_Date, 0, 0, TRUE, L_SG:BID)                ! Invoice for day not related to MID
             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 0, 2, TRUE,,, L_SG:BID)  ! Credits for day - Transporter not MID
          ELSE
             L_VG:Extra_Inv        = Get_Invoices(LOC:Cur_Date, 1, 0, TRUE, L_SG:BID)                ! Invoice for day not related to MID
             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 1, 2, TRUE,,, L_SG:BID)  ! Credits for day - Transporter not MID
          .

          L_VG:Total_Cost          = L_VG:Loadmaster + L_VG:Trans_Broking + L_VG:Extra_Inv - L_VG:Trans_Credit

          L_VG:Trans_Debit         = Get_InvoicesTransporter(LOC:Cur_Date, 0, 2,,,, L_SG:BID)

          L_VG:Total_Cost_Less_Deb = L_VG:Total_Cost - L_VG:Trans_Debit



          L_VG:GP                  = L_VG:Total_T_O - L_VG:Total_Cost_Less_Deb
          L_VG:GP_Per              = (L_VG:GP / L_VG:Total_T_O) * 100

          L_DQ:Date                = LOC:Cur_Date
          ADD(LOC:Dates_Done, L_DQ:Date)

          L_VG:MID                 = 0
          L_VG:CreatedDate         = LOC:Cur_Date

          IF L_VG:Total_Cost ~= 0.0 OR L_VG:Total_T_O ~= 0.0 OR L_VG:Trans_Debit ~= 0.0 OR L_VG:Extra_Inv ~= 0.0 OR L_VG:Trans_Credit ~= 0.0 OR |
                L_VG:C_Note ~= 0.0

             db.debugout('[--------]  Total_Cost: ' & L_VG:Total_Cost & ',  Total_T_O: ' & L_VG:Total_T_O & ',  Trans_Debit: ' & L_VG:Trans_Debit & |
                         ',  Extra_Inv: ' & L_VG:Extra_Inv & ',  Trans_Credit: ' & L_VG:Trans_Credit & '  C_Note: ' & L_VG:C_Note)

             PRINT(RPT:Detail)
          .

          LOC:Cur_Date            += 1
          IF LOC:Cur_Date >= MAN:CreatedDate
             BREAK
    .  .  .


    EXIT
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Manifest ID' & |
      '|' & 'By Branch' & |
      '|' & 'By Transporter' & |
      '|' & 'By Vehicle Composition' & |
      '|' & 'By Journey' & |
      '|' & 'By Driver' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      ! Print Totals here
      db.debugout('[Print_Turnover]  Total GP = L_TG:Total_TO (' & L_TG:Total_TO & ') - L_TG:Total_Cost (' & L_TG:Total_Cost & ')')
      L_TG:GP       = L_TG:Total_TO - L_TG:Total_Cost
      L_TG:GP_Per   = (L_TG:GP / L_TG:Total_TO) * 100
  
      PRINT(RPT:totals)
  
      IF L_SG:Output > 0
         CASE MESSAGE('Would you like to load the accumulation details now?|Note that if the file does not load automatically you can find it at the location shown below.||File: ' & PATH() & '\Print Turnover - ' & FORMAT(TODAY(), @d7) & '.csv', 'Print Turnover', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Print Turnover - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Turnover_aug28')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?VAT:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_SG:BID',L_SG:BID)                                ! Added by: Report
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Manifest, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Manifest ID')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:BID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:TID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Vehicle Composition')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:VCID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Journey')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:JID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Driver')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:DRID,+MAN:MID')
  END
  ThisReport.SetFilter('MAN:State = 3 AND ((L_SG:BID = 0 OR L_SG:BID = MAN:BID) AND MAN:CreatedDate >= LO:From_Date AND MAN:CreatedDate << LO:To_Date + 1)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Manifest.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  FDB4.Init(?L_SG:BranchName,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(BRA:Key_BranchName)
  FDB4.AddField(BRA:BranchName,FDB4.Q.BRA:BranchName) !List box control field - type derived from field
  FDB4.AddField(BRA:BID,FDB4.Q.BRA:BID) !Primary key field - type derived from field
  FDB4.AddUpdateField(BRA:BID,L_SG:BID)
  ThisWindow.AddItem(FDB4.WindowComponent)
      LO:From_Date    = GETINI('Print_Turnover', 'From_Date', , GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Turnover', 'To_Date', , GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      PUTINI('Print_Turnover', 'From_Date', LO:From_Date, GLO:Local_INI)
      PUTINI('Print_Turnover', 'To_Date', LO:To_Date, GLO:Local_INI)
  
      
      L_SG:VAT    = GETINI('Print_Turnover', 'VAT', , GLO:Local_INI)
      L_SG:BID    = GETINI('Print_Turnover', 'BID', , GLO:Local_INI)
  
      IF L_SG:BID ~= 0
         BRA:BID  = L_SG:BID
         IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
            L_SG:BranchName   = BRA:BranchName
         .
      ELSE
         L_SG:BranchName   = 'All'
      .
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
          SELECT(?Tab1)
          DISABLE(?Pause)
          DISPLAY
          PUTINI('Print_Turnover', 'VAT', L_SG:VAT, GLO:Local_INI)
          PUTINI('Print_Turnover', 'BID', L_SG:BID, GLO:Local_INI)
          IF L_SG:Output > 0
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             Add_Log('Start', 'Print_Turnover', 'Print Turnover - ' & FORMAT(TODAY(), @d7) & '.csv', TRUE, 1)
          .
      
      
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
              ! need to check for un-manifested invoices
              ! need to add in dates from too check to make sure all dates are included
  
      ! We have a Q of done dates, we need to check if the current date is the date after the last date
      ! or the date after the start date
      ! This will work as all orders are by date
  
      ! Check if the current record date is the next record
      DO Check_Missing_Invs
  
  
  
  
  
      ! ==============   normal processing   ==============
      CLEAR(LOC:Values)
  
      L_VG:MID                    = MAN:MID
      L_VG:CreatedDate            = MAN:CreatedDate
  
  
      ! Get_Manifest_Info     
      !   p:Option
      !       0.  Charges total
      !       1.  Insurance total
      !       2.  Weight total, for this Manifest!    Uses Units_Loaded
      !       3.  Extra Legs cost
      !       4.  Charges total incl VAT
      !       5.  No. DI's
      !   p:DI_Type
      !       0.  All             (default)
      !       1.  Non-Broking
      !       2.  Broking
  
  !    IF MAN:Broking = TRUE
         L_VG:Kgs_Broking         = Get_Manifest_Info(MAN:MID, 2, 2,, L_SG:Output)
  !    ELSE
         L_VG:Kgs_O_Night         = Get_Manifest_Info(MAN:MID, 2, 1,, L_SG:Output)
  !    .
  
      ! Below is no longer possible, the Broking status is set on the Manifest
  
  !    IF L_VG:Kgs_Broking ~= 0.0 AND L_VG:Kgs_O_Night ~= 0.0
  !       ! Error - some DIs are overnight some are broking...
  !       
  !       ! (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel)       (BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0)
  !       Add_SystemLog(100, 'Print_Turnover', 'MID: ' & MAN:MID & ', has both Broking and O/Night DIs attached to it!', 'Please correct.')
  !       LOC:Errors              += 1
  !    .
  
      IF MAN:Broking = FALSE                !L_VG:Kgs_Broking = 0.0
         IF L_SG:VAT = TRUE
            L_VG:T_O_O_Night      = Get_Manifest_Info(MAN:MID, 4,,1, L_SG:Output)             ! All charges - from invoice    inc
            !L_VG:T_O_O_Night      = Get_Invoices(0, 0, 1, FALSE, MAN:MID,, L_SG:Output, 1)   - this would also work
  
            L_VG:T_O_Broking      = Get_InvoicesTransporter(0, 0, 1, 0, 1, MAN:MID)           ! Del legs - inc vat
         ELSE
            L_VG:T_O_O_Night      = Get_Manifest_Info(MAN:MID, 0,,1, L_SG:Output)             ! All charges - from invoice
            L_VG:T_O_Broking      = Get_InvoicesTransporter(0, 1, 1, 0, 1, MAN:MID)           ! Del legs - ex vat
         .
  
     db.debugout('[Print_Turnover]  T_O_O_Night ' & L_VG:T_O_O_Night & ',  T_O_Broking: ' & L_VG:T_O_Broking)
  
         L_VG:T_O_O_Night        -= L_VG:T_O_Broking                                          ! Legs cost off total DI value (included)
      ELSE
         L_VG:T_O_O_Night         = 0.0
         IF L_SG:VAT = TRUE
            L_VG:T_O_Broking      = Get_Manifest_Info(MAN:MID, 4,,1, L_SG:Output)             ! All charges - from invoice    inc
         ELSE
            L_VG:T_O_Broking      = Get_Manifest_Info(MAN:MID, 0,,1, L_SG:Output)             ! All charges - from invoice
      .  .
  
      
      ! Get_Invoices          (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID, p:ToDate, p:Output, p:Limit_On)
      ! p:Option
      !   0.  - Total Charges inc.
      !   1.  - Excl VAT
      !   2.  - VAT
      ! p:Type
      !   0.  - All
      !   1.  - Invoices                      Total >= 0.0
      !   2.  - Credit Notes                  Total < 0.0
      !   3.  - Credit Notes excl Bad Debts   Total < 0.0
      ! p:Limit_On
      !   0.  Date & Branch
      !   1.  MID
      L_DQ:Date                   = MAN:CreatedDate
      GET(LOC:Dates_Done, L_DQ:Date)
      IF ERRORCODE()
         Cred_Type_#      = 2
         IF L_SG:ExcludeBadDebts = TRUE
            Cred_Type_#   = 3
         .
  
         IF L_SG:VAT = TRUE
            L_VG:C_Note           = -Get_Invoices(MAN:CreatedDate, 0, Cred_Type_#,, L_SG:BID,, L_SG:Output)    ! Any passed on this day
         ELSE
            L_VG:C_Note           = -Get_Invoices(MAN:CreatedDate, 1, Cred_Type_#,, L_SG:BID,, L_SG:Output)    ! Any passed on this day
      .  .
  
      IF MAN:Broking = FALSE
         IF L_SG:VAT = TRUE
            ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID, p:BID)
            !     1       2           3           4               5       6       7
            ! p:Option
            !   0.  - Total Charges inc.
            !   1.  - Excl VAT
            !   2.  - VAT
            ! p:Type
            !   0.  - All
            !   1.  - Invoices                      Total >= 0.0
            !   2.  - Credit Notes                  Total < 0.0
            ! p:DeliveryLegs
            !   0.  - None
            !   1.  - Del Legs only
            !   2.  - Both
            ! p:MID
            !   0   = Not for a MID
            !   x   = for a MID
            L_VG:Loadmaster       = Get_InvoicesTransporter(0, 0, 1, 0, 0, MAN:MID)
         ELSE
            L_VG:Loadmaster       = Get_InvoicesTransporter(0, 1, 1, 0, 0, MAN:MID)
         .
         L_VG:Trans_Broking       = L_VG:T_O_Broking              ! Legs cost is broking cost in this case
      ELSE
         IF L_SG:VAT = TRUE
            L_VG:Trans_Broking    = Get_InvoicesTransporter(0, 0, 1, 0, 0, MAN:MID)
            L_VG:Trans_Broking   += Get_InvoicesTransporter(0, 0, 1, 0, 1, MAN:MID)   ! Del legs - inc vat
         ELSE
            L_VG:Trans_Broking    = Get_InvoicesTransporter(0, 1, 1, 0, 0, MAN:MID)
            !L_VG:T_O_Broking      = Get_InvoicesTransporter(0, 1, 1, 0, 1, MAN:MID)   ! Del legs - ex vat        ???
            L_VG:Trans_Broking   += Get_InvoicesTransporter(0, 1, 1, 0, 1, MAN:MID)   ! Del legs - ex vat
      .  .
  
      GET(LOC:Dates_Done, L_DQ:Date)
      IF ERRORCODE()
         IF L_SG:VAT = TRUE
            L_VG:Ext_Invocie_TO   = Get_Invoices(MAN:CreatedDate, 0, 1, TRUE, L_SG:BID,, L_SG:Output)   ! Invoice for day not related to MID
  
            L_VG:Extra_Inv        = Get_InvoicesTransporter(MAN:CreatedDate, 0, 1, TRUE,,, L_SG:BID)      ! Credits for day - Transporter not MID
            L_VG:Trans_Credit     = -Get_InvoicesTransporter(MAN:CreatedDate, 0, 2, TRUE,,, L_SG:BID)     ! Credits for day - Transporter not MID
  
            L_VG:Trans_Debit      = Get_InvoicesTransporter(MAN:CreatedDate, 0, 2,,,, L_SG:BID)
  
     db.debugout('[Print_Turnover]  Extra invoices - Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',  Extra_Inv: ' & L_VG:Extra_Inv & ',  Trans_Credit: ' & L_VG:Trans_Credit & ',  Trans_Debit: ' & L_VG:Trans_Debit)
  
         ELSE
            L_VG:Ext_Invocie_TO   = Get_Invoices(MAN:CreatedDate, 1, 1, TRUE, L_SG:BID,, L_SG:Output)   ! Invoice for day not related to MID
  
            L_VG:Extra_Inv        = Get_InvoicesTransporter(MAN:CreatedDate, 1, 1, TRUE,,, L_SG:BID)      ! Credits for day - Transporter not MID
            L_VG:Trans_Credit     = -Get_InvoicesTransporter(MAN:CreatedDate, 1, 2, TRUE,,, L_SG:BID)     ! Credits for day - Transporter not MID
  
            L_VG:Trans_Debit      = Get_InvoicesTransporter(MAN:CreatedDate, 1, 2,,,, L_SG:BID)
      .  .
  
      L_VG:Total_Cost             = L_VG:Loadmaster + L_VG:Trans_Broking + L_VG:Extra_Inv - L_VG:Trans_Credit
  
      L_VG:Total_Cost_Less_Deb    = L_VG:Total_Cost - L_VG:Trans_Debit
  
     db.debugout('[Print_Turnover]  Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',  T_O_Broking: ' & L_VG:T_O_Broking)
  
      L_VG:T_O_Broking           += L_VG:Ext_Invocie_TO                                                   ! Add extras to BROKING ALWAYS
  
      L_VG:Total_T_O              = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note
  
  
      L_VG:GP                     = L_VG:Total_T_O - L_VG:Total_Cost_Less_Deb
      L_VG:GP_Per                 = (L_VG:GP / L_VG:Total_T_O) * 100
  
  
  
      L_DQ:Date                   = MAN:CreatedDate
      GET(LOC:Dates_Done, L_DQ:Date)
      IF ERRORCODE()
         L_DQ:Date                = MAN:CreatedDate
         ADD(LOC:Dates_Done, L_DQ:Date)
      .
  IF 0
    PRINT(RPT:totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
      IF LOC:Errors > 0
         MESSAGE('Completed with Errors: ' & LOC:Errors & '||Please review the System Log.', 'Print Turnover', ICON:Exclamation)
      .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Turnover_aug28','Print_Turnover_aug28','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True


FDB4.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      ! Add an All
      Queue:FileDrop.BRA:BranchName           = 'All'
      GET(Queue:FileDrop, Queue:FileDrop.BRA:BranchName)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop)
         Queue:FileDrop.BRA:BranchName        = 'All'
         Queue:FileDrop.BRA:BID               = 0
         ADD(Queue:FileDrop)
  
         IF L_SG:BID = 0
            L_SG:BranchName   = 'All'
      .  .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Report
!!! (Manifest based)
!!! </summary>
Print_Turnover_june15 PROCEDURE 

!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
TintRptViewer  TinReportViewer
!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
Progress:Thermometer BYTE                                  ! 
LOC:Errors           ULONG                                 ! 
LOC:Settings         GROUP,PRE(L_SG)                       ! 
VAT                  BYTE                                  ! 
BID                  ULONG                                 ! Branch ID
BranchName           STRING(35)                            ! Branch Name
ExcludeBadDebts      BYTE(1)                               ! Exclude Bad Debt Credit Notes
Output               BYTE                                  ! Output trail of items
                     END                                   ! 
LOC:Values           GROUP,PRE(L_VG)                       ! 
Kgs_Broking          DECIMAL(7)                            ! 
Kgs_O_Night          DECIMAL(7)                            ! 
T_O_Broking          DECIMAL(10,2)                         ! 
T_O_O_Night          DECIMAL(10,2)                         ! 
C_Note               DECIMAL(10,2)                         ! 
Total_T_O            DECIMAL(10,2)                         ! 
Loadmaster           DECIMAL(10,2)                         ! 
Trans_Broking        DECIMAL(10,2)                         ! 
Extra_Inv            DECIMAL(10,2)                         ! 
Trans_Credit         DECIMAL(10,2)                         ! 
Total_Cost           DECIMAL(10,2)                         ! 
Trans_Debit          DECIMAL(10,2)                         ! 
Total_Cost_Less_Deb  DECIMAL(10,2)                         ! 
GP                   DECIMAL(10)                           ! 
GP_Per               DECIMAL(10,2)                         ! 
MID                  ULONG                                 ! Manifest ID
CreatedDate          DATE                                  ! Created Date
Ext_Invocie_TO       DECIMAL(12,2)                         ! Turn over extra invoice
                     END                                   ! 
Totals_Group         GROUP,PRE(L_TG)                       ! 
GP                   DECIMAL(12,2)                         ! 
GP_Per               DECIMAL(7,2)                          ! 
Total_TO             DECIMAL(12,2)                         ! 
Total_Cost           DECIMAL(12,2)                         ! 
                     END                                   ! 
LOC:Dates_Done       QUEUE,PRE(L_DQ)                       ! 
Date                 DATE                                  ! 
                     END                                   ! 
LOC:Options          GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
LOC:Cur_Date         DATE                                  ! 
LOC:Report_Values    GROUP,PRE(L_RG)                       ! 
VAT_Option           STRING(20)                            ! 
                     END                                   ! 
Process:View         VIEW(Manifest)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:Broking)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:CreatedDate)
                       PROJECT(MAN:State)
                       PROJECT(MAN:VATRate)
                     END
FDB4::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
ProgressWindow       WINDOW('Report Turnover (Manifest)'),AT(,,177,126),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       SHEET,AT(4,2,169,102),USE(?Sheet1)
                         TAB('Settings'),USE(?Tab2)
                           PROMPT('Branch:'),AT(24,42),USE(?Prompt2)
                           PROMPT('VAT:'),AT(24,22),USE(?L_SG:VAT:Prompt)
                           LIST,AT(55,22,60,10),USE(L_SG:VAT),DROP(5),FROM('Excluding|#0|Including|#1')
                           LIST,AT(55,42,105,10),USE(L_SG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                           CHECK(' Exclude Bad Debts'),AT(55,60),USE(L_SG:ExcludeBadDebts),MSG('Exclude Bad Debt C' & |
  'redit Notes'),TIP('Exclude Bad Debt Credit Notes')
                           PROMPT('Output:'),AT(26,84),USE(?L_SG:Output_Trail:Prompt)
                           LIST,AT(55,84,60,10),USE(L_SG:Output),VSCROLL,DROP(5),FROM('None|#0|All|#1'),MSG('Output tra' & |
  'il of items'),TIP('Output trail of items')
                         END
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(36,34,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(20,22,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(20,50,141,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Cancel'),AT(124,108,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                       BUTTON('Pause'),AT(70,108,49,15),USE(?Pause),LEFT,ICON('Blue_R.ico'),FLAT
                     END

Report               REPORT('Manifest Report'),AT(250,958,10604,6854),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,10604,698),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Management Profit Report'),AT(4198,10),USE(?ReportTitle),FONT(,12,,FONT:bold),CENTER
                         STRING('Branch:'),AT(7927,42),USE(?String57),TRN
                         STRING(@s35),AT(8396,42),USE(L_SG:BranchName)
                         STRING(@s20),AT(104,42),USE(L_RG:VAT_Option)
                         BOX,AT(0,469,10573,260),USE(?HeaderBox),COLOR(COLOR:Black)
                         BOX,AT(0,260,10573,219),USE(?HeaderBox:2),COLOR(COLOR:Black)
                         STRING('Total'),AT(8750,292,833,167),USE(?HeaderTitle:15),CENTER,TRN
                         STRING('GP %'),AT(9635,500,833,167),USE(?HeaderTitle:17),CENTER,TRN
                         STRING('Trans Dr'),AT(7969,292,833,167),USE(?HeaderTitle:14),CENTER,TRN
                         STRING('GP'),AT(9635,292,833,167),USE(?HeaderTitle:16),CENTER,TRN
                         STRING('Trans Cr.'),AT(6333,292,833,167),USE(?HeaderTitle:12),CENTER,TRN
                         STRING('Total'),AT(7156,292,833,167),USE(?HeaderTitle:13),CENTER,TRN
                         STRING('T/O O/Night'),AT(2042,521,833,167),USE(?HeaderTitle:8),CENTER,TRN
                         STRING('Trans Brok'),AT(4594,292,833,167),USE(?HeaderTitle:10),CENTER,TRN
                         STRING('Loadmaster'),AT(4594,521,833,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Ext. Inv.'),AT(5490,292,833,167),USE(?HeaderTitle:11),CENTER,TRN
                         STRING('C/Note'),AT(2927,292,833,167),USE(?HeaderTitle:9),CENTER,TRN
                         STRING('MID'),AT(646,292,688,167),USE(?HeaderTitle:1),CENTER,TRN
                         STRING('Kgs Brok'),AT(1396,292,594,167),USE(?HeaderTitle:2),CENTER,TRN
                         STRING('Kgs O/N'),AT(1396,521,594,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Date'),AT(52,292,583,167),USE(?HeaderTitle:3),CENTER,TRN
                         STRING('T/O Brok'),AT(2042,292,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('Total T/O'),AT(3760,292,833,167),USE(?HeaderTitle:5),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,10604,396),USE(?Detail)
                         LINE,AT(0,406,10333,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         STRING(@n_10),AT(646,31,,170),USE(L_VG:MID),RIGHT(1),TRN
                         STRING(@n-10.0),AT(1396,31),USE(L_VG:Kgs_Broking),RIGHT(1),TRN
                         STRING(@n-10.0),AT(1396,208),USE(L_VG:Kgs_O_Night),RIGHT(1),TRN
                         STRING(@n-14.2),AT(2042,31),USE(L_VG:T_O_Broking),RIGHT(1),TRN
                         STRING(@n-14.2),AT(2042,208),USE(L_VG:T_O_O_Night),RIGHT(1),TRN
                         STRING(@n-14.2),AT(2927,31),USE(L_VG:C_Note),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1),TRN
                         STRING(@n-14.2),AT(3760,31),USE(L_VG:Total_T_O),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4594,208),USE(L_VG:Loadmaster),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4594,31),USE(L_VG:Trans_Broking),RIGHT(1),TRN
                         STRING(@n-14.2),AT(5490,31),USE(L_VG:Extra_Inv),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6333,31),USE(L_VG:Trans_Credit),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1), |
  TRN
                         STRING(@d5),AT(52,31,,170),USE(L_VG:CreatedDate),RIGHT(1),TRN
                         STRING(@n-14.2),AT(7156,31),USE(L_VG:Total_Cost),RIGHT(1),TRN
                         STRING(@n-14.2),AT(7969,31),USE(L_VG:Trans_Debit),RIGHT(1),TRN
                         STRING(@n-14.2),AT(8750,31),USE(L_VG:Total_Cost_Less_Deb),RIGHT(1),TRN
                         STRING(@n-14.0),AT(9375,31,1094,156),USE(L_VG:GP),RIGHT(1),TRN
                         STRING(@n-14.2),AT(9375,208,1094,167),USE(L_VG:GP_Per),RIGHT(1),TRN
                       END
totals                 DETAIL,AT(,,,542),USE(?totals)
                         STRING('Totals'),AT(104,73,885),USE(?String56),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@n-10.0),AT(1396,73),USE(L_VG:Kgs_Broking,,?L_VG:Kgs_Broking:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-10.0),AT(1396,260),USE(L_VG:Kgs_O_Night,,?L_VG:Kgs_O_Night:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(2042,73),USE(L_VG:T_O_Broking,,?L_VG:T_O_Broking:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(2042,260),USE(L_VG:T_O_O_Night,,?L_VG:T_O_O_Night:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(2927,73),USE(L_VG:C_Note,,?L_VG:C_Note:2),FONT(,,COLOR:Red,,CHARSET:ANSI), |
  RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(3760,73),USE(L_VG:Total_T_O,,?L_VG:Total_T_O:2),RIGHT(1),SUM(L_TG:Total_TO), |
  TALLY(Detail),TRN
                         STRING(@n-14.2),AT(4594,260),USE(L_VG:Loadmaster,,?L_VG:Loadmaster:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(4594,73),USE(L_VG:Trans_Broking,,?L_VG:Trans_Broking:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(5490,73),USE(L_VG:Extra_Inv,,?L_VG:Extra_Inv:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(6333,73),USE(L_VG:Trans_Credit,,?L_VG:Trans_Credit:2),FONT(,,COLOR:Red, |
  ,CHARSET:ANSI),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-14.2),AT(7156,73),USE(L_VG:Total_Cost,,?L_VG:Total_Cost:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(7969,73),USE(L_VG:Trans_Debit,,?L_VG:Trans_Debit:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-14.2),AT(8750,73),USE(L_VG:Total_Cost_Less_Deb,,?L_VG:Total_Cost_Less_Deb:2),RIGHT(1), |
  SUM(L_TG:Total_Cost),TALLY(Detail),TRN
                         STRING(@n-14.2),AT(9375,73,1094,167),USE(L_TG:GP),RIGHT(1),TRN
                         STRING(@n-10.2),AT(9375,260,1094,167),USE(L_TG:GP_Per),RIGHT(1),TRN
                       END
                       FOOTER,AT(250,7813,10604,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(9521,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END

TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Missing_Invs                          ROUTINE
    CLEAR(LOC:Dates_Done)
    GET(LOC:Dates_Done, RECORDS(LOC:Dates_Done))
    ! The last date done - and we are not on 1st date of range
    IF MAN:CreatedDate > L_DQ:Date + 1 AND MAN:CreatedDate ~= LO:From_Date
       ! Check for invoices not associated with a Manifest in this period
       IF L_DQ:Date = 0
          LOC:Cur_Date             = LO:From_Date
       ELSE
          LOC:Cur_Date             = L_DQ:Date + 1
       .

       LOOP
          CLEAR(LOC:Values)

          Cred_Type_#      = 2
          IF L_SG:ExcludeBadDebts = TRUE
             Cred_Type_#   = 3
          .
          IF L_SG:VAT = TRUE
             ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID, p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source)
             L_VG:C_Note           = -Get_Invoices(LOC:Cur_Date, 0, Cred_Type_#,, L_SG:BID,,,,,1)          ! Any passed on this day
          ELSE
             L_VG:C_Note           = -Get_Invoices(LOC:Cur_Date, 1, Cred_Type_#,, L_SG:BID,,,,,1)          ! Any passed on this day
          .

          L_VG:Total_T_O           = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note

!          IF L_SG:VAT = TRUE
!             L_VG:Extra_Inv        = Get_Invoices(LOC:Cur_Date, 0, 0, TRUE, L_SG:BID)                ! Invoice for day not related to MID
!             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 0, 2, TRUE,,, L_SG:BID)  ! Credits for day - Transporter not MID
!          ELSE
!             L_VG:Extra_Inv        = Get_Invoices(LOC:Cur_Date, 1, 0, TRUE, L_SG:BID)                ! Invoice for day not related to MID
!             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 1, 2, TRUE,,, L_SG:BID)  ! Credits for day - Transporter not MID
!          .

          IF L_SG:VAT = TRUE
             L_VG:Ext_Invocie_TO   = Get_Invoices(LOC:Cur_Date, 0, 1, TRUE, L_SG:BID,, L_SG:Output,,,1)   ! Invoice for day not related to MID

             ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID, p:BID, p:ToDate, p:Extra_Inv, p:Manifest_Type, p:Output, p:Source)

             L_VG:Extra_Inv        = Get_InvoicesTransporter(LOC:Cur_Date, 0, 1, TRUE,,, L_SG:BID,,TRUE,,,1)      ! Credits for day - Transporter not MID
             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 0, 2, TRUE,,, L_SG:BID,,,,,1)     ! Credits for day - Transporter not MID

             L_VG:Trans_Debit      = Get_InvoicesTransporter(LOC:Cur_Date, 0, 2,,,, L_SG:BID,,,,,1)
          ELSE
             L_VG:Ext_Invocie_TO   = Get_Invoices(LOC:Cur_Date, 1, 1, TRUE, L_SG:BID,, L_SG:Output,,,1)   ! Invoice for day not related to MID

             L_VG:Extra_Inv        = Get_InvoicesTransporter(LOC:Cur_Date, 1, 1, TRUE,,, L_SG:BID,,TRUE,,,1)      ! Credits for day - Transporter not MID
             L_VG:Trans_Credit     = -Get_InvoicesTransporter(LOC:Cur_Date, 1, 2, TRUE,,, L_SG:BID,,,,,1)     ! Credits for day - Transporter not MID

             L_VG:Trans_Debit      = Get_InvoicesTransporter(LOC:Cur_Date, 1, 2,,,, L_SG:BID,,,,,1)
          .

          L_VG:Total_Cost             = L_VG:Loadmaster + L_VG:Trans_Broking + L_VG:Extra_Inv - L_VG:Trans_Credit

          L_VG:Total_Cost_Less_Deb    = L_VG:Total_Cost - L_VG:Trans_Debit

          L_VG:T_O_Broking           += L_VG:Ext_Invocie_TO                                                   ! Add extras to BROKING ALWAYS

          L_VG:Total_T_O              = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note

          L_VG:GP                     = L_VG:Total_T_O - L_VG:Total_Cost_Less_Deb
          L_VG:GP_Per                 = (L_VG:GP / L_VG:Total_T_O) * 100


          L_DQ:Date                = LOC:Cur_Date
          ADD(LOC:Dates_Done, L_DQ:Date)

          L_VG:MID                 = 0
          L_VG:CreatedDate         = LOC:Cur_Date

          IF L_VG:Total_Cost ~= 0.0 OR L_VG:Total_T_O ~= 0.0 OR L_VG:Trans_Debit ~= 0.0 OR L_VG:Extra_Inv ~= 0.0 OR |
                L_VG:Trans_Credit ~= 0.0 OR L_VG:C_Note ~= 0.0 OR L_VG:Ext_Invocie_TO ~= 0.0

             db.debugout('[--------]  Total_Cost: ' & L_VG:Total_Cost & ',  Total_T_O: ' & L_VG:Total_T_O & ',  Trans_Debit: ' & L_VG:Trans_Debit & |
                         ',  Extra_Inv: ' & L_VG:Extra_Inv & ',  Trans_Credit: ' & L_VG:Trans_Credit & '  C_Note: ' & L_VG:C_Note)

             PRINT(RPT:Detail)
          .

          LOC:Cur_Date            += 1
          IF LOC:Cur_Date >= MAN:CreatedDate
             BREAK
    .  .  .


    EXIT
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Manifest ID' & |
      '|' & 'By Branch' & |
      '|' & 'By Transporter' & |
      '|' & 'By Vehicle Composition' & |
      '|' & 'By Journey' & |
      '|' & 'By Driver' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      ! Print Totals here
      db.debugout('[Print_Turnover]  Total GP = L_TG:Total_TO (' & L_TG:Total_TO & ') - L_TG:Total_Cost (' & L_TG:Total_Cost & ')')
      L_TG:GP       = L_TG:Total_TO - L_TG:Total_Cost
      L_TG:GP_Per   = (L_TG:GP / L_TG:Total_TO) * 100
  
      PRINT(RPT:totals)
  
      IF L_SG:Output > 0
         CASE MESSAGE('Would you like to load the accumulation details now?|Note that if the file does not load automatically you can find it at the location shown below.||File: ' & PATH() & '\Print Turnover - ' & FORMAT(TODAY(), @d7) & '.csv', 'Print Turnover', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Print Turnover - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
    ENDPAGE(SELF.Report)
    IF SELF.SkipPreview
      TintRptViewer.CheckForPageNum(Self.PreviewQueue)
      TintRptViewer.SetupCopiesCollate(Self.PreviewQueue)
      if (TinTRptViewer.GetEjectDir())
        TinTRptViewer.ReversePreviewQueue(Self.PreviewQueue)
      end
      SELF.Report{PROP:FlushPreview} = True
      GlobalResponse = RequestCompleted
    ELSE
      GlobalResponse = TintRptViewer.Display(Report,Self.PreviewQueue,Report{Prop:Text},'Page Width',0)
      SELF.SkipPreview = TRUE
    END
    FREE(SELF.PreviewQueue)
   END
  ! You can place code in this embed point to test GlobalResponse to see if report printed or not.
  ! CASE GlobalResponse
  ! OF RequestCompleted     ! Report Was Printed
  !   ! Do Something
  ! OF RequestCancelled     ! Report Was NOT Printed
  !   ! Do Something
  ! END
   OMIT('***TINTOOLS***')
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  PARENT.AskPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  ***TINTOOLS***
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
      ! Doesn't work, using Tin Tools
  
  !  LOOP
  !     CASE MESSAGE('Print another copy?', 'Print', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
  !     OF BUTTON:Yes
  !        SELF.PrintReport()
  !     ELSE
  !        BREAK
  !  .  .
  !


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Turnover_june15')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_SG:BID',L_SG:BID)                                ! Added by: Report
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Manifest, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Manifest ID')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:BID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:TID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Vehicle Composition')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:VCID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Journey')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:JID,+MAN:MID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Driver')) THEN
     ThisReport.AppendOrder('+MAN:CreatedDate,+MAN:DRID,+MAN:MID')
  END
  ThisReport.SetFilter('MAN:State = 3 AND ((L_SG:BID = 0 OR L_SG:BID = MAN:BID) AND MAN:CreatedDate >= LO:From_Date AND MAN:CreatedDate << LO:To_Date + 1)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Manifest.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  FDB4.Init(?L_SG:BranchName,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(BRA:Key_BranchName)
  FDB4.AddField(BRA:BranchName,FDB4.Q.BRA:BranchName) !List box control field - type derived from field
  FDB4.AddField(BRA:BID,FDB4.Q.BRA:BID) !Primary key field - type derived from field
  FDB4.AddUpdateField(BRA:BID,L_SG:BID)
  ThisWindow.AddItem(FDB4.WindowComponent)
      LO:From_Date    = GETINI('Print_Turnover', 'From_Date', , GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Turnover', 'To_Date', , GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      PUTINI('Print_Turnover', 'From_Date', LO:From_Date, GLO:Local_INI)
      PUTINI('Print_Turnover', 'To_Date', LO:To_Date, GLO:Local_INI)
  
      
      L_SG:VAT    = GETINI('Print_Turnover', 'VAT', , GLO:Local_INI)
      L_SG:BID    = GETINI('Print_Turnover', 'BID', , GLO:Local_INI)
  
      IF L_SG:BID ~= 0
         BRA:BID  = L_SG:BID
         IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
            L_SG:BranchName   = BRA:BranchName
         .
      ELSE
         L_SG:BranchName   = 'All'
      .
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
    ! set printer eject style
    TinTRptViewer.SetEjectDir(TinEject:PageFaceUp)
    IF TintRptViewer.PrintViewerSetup() THEN
      Self:Response = RequestCancelled       ! Changed by LH 4/19/98
      POST(EVENT:CloseDown)                  ! Changed by LH 4/19/98
      RETURN Level:NOTIFY                    ! Changed by LH 4/19/98
    END
    SELF.SkipPreview = TintRptViewer.SkipPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:_SQLTemp.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
          ! ****---*** ???    Note: there is something wrong here - the L_SG:BID var is not being set by ABC templates, will use cludge below for now
          !                   Could it be Tintools?  or what?
      
          IF L_SG:BranchName ~= 'All'
             BRA:BranchName   = L_SG:BranchName
             IF Access:Branches.Fetch(BRA:Key_BranchName) = LEVEL:Benign
                L_SG:BID      = BRA:BID
          .  .
      
      
             
          SELECT(?Tab1)
          DISABLE(?Pause)
          DISPLAY
          PUTINI('Print_Turnover', 'VAT', L_SG:VAT, GLO:Local_INI)
          PUTINI('Print_Turnover', 'BID', L_SG:BID, GLO:Local_INI)
      
      
          EXECUTE L_SG:VAT + 1
             L_RG:VAT_Option  = 'Excluding VAT'
             L_RG:VAT_Option  = 'Including VAT'
          .
          IF L_SG:Output > 0
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             Add_Log('Start', 'Print_Turnover', 'Print Turnover - ' & FORMAT(TODAY(), @d7) & '.csv', TRUE, 1)
      
             CASE MESSAGE('Would you like to empty the Audit Summary table?','Audit Summary', ICON:Question, BUTTON:Yes+BUTTON:No,|
                      BUTTON:No)
             OF BUTTON:Yes
                _SQLTemp{PROP:SQL}    = 'DELETE FROM Audit_ManagementProfit'
          .  .
      
      
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_SG:BranchName
          MESSAGE('L_SG:BID: ' & L_SG:BID)
          
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
              ! need to check for un-manifested invoices
              ! need to add in dates from too check to make sure all dates are included
  
      ! We have a Q of done dates, we need to check if the current date is the date after the last date
      ! or the date after the start date
      ! This will work as all orders are by date
  
      ! Check if the current record date is the next record
      DO Check_Missing_Invs
  
  
  
  
  
      ! ==============   normal processing   ==============
      CLEAR(LOC:Values)
  
      L_VG:MID                    = MAN:MID
      L_VG:CreatedDate            = MAN:CreatedDate
  
  
      ! Get_Manifest_Info     
      !   p:Option
      !       0.  Charges total
      !       1.  Insurance total
      !       2.  Weight total, for this Manifest!    Uses Units_Loaded
      !       3.  Extra Legs cost
      !       4.  Charges total incl VAT
      !       5.  No. DI's
      !   p:DI_Type
      !       0.  All             (default)
      !       1.  Non-Broking
      !       2.  Broking
  
  !    IF MAN:Broking = TRUE
         L_VG:Kgs_Broking         = Get_Manifest_Info(MAN:MID, 2, 2,, L_SG:Output)
  !    ELSE
         L_VG:Kgs_O_Night         = Get_Manifest_Info(MAN:MID, 2, 1,, L_SG:Output)
  !    .
  
      ! Below is no longer possible, the Broking status is set on the Manifest
  
  !    IF L_VG:Kgs_Broking ~= 0.0 AND L_VG:Kgs_O_Night ~= 0.0
  !       ! Error - some DIs are overnight some are broking...
  !       
  !       ! (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel)       (BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0)
  !       Add_SystemLog(100, 'Print_Turnover', 'MID: ' & MAN:MID & ', has both Broking and O/Night DIs attached to it!', 'Please correct.')
  !       LOC:Errors              += 1
  !    .
  
      IF MAN:Broking = FALSE                !L_VG:Kgs_Broking = 0.0
         IF L_SG:VAT = TRUE
            L_VG:T_O_O_Night      = Get_Manifest_Info(MAN:MID, 4,,1, L_SG:Output)             ! All charges - from invoice    inc
            !L_VG:T_O_O_Night      = Get_Invoices(0, 0, 1, FALSE, MAN:MID,, L_SG:Output, 1)   - this would also work
  
            L_VG:T_O_Broking      = Get_InvoicesTransporter(0, 0, 1, 0, 1, MAN:MID,,,,, L_SG:Output,1)           ! Del legs - inc vat
  
            ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
            IF L_VG:T_O_Broking > 0.0
               Add_Log('MID: ' & MAN:MID & ',  L_VG:T_O_Broking: ' & L_VG:T_O_Broking & ',  MAN:CreatedDate: ' & FORMAT(MAN:CreatedDate,@d6), 'Get_Manifest_Info-Extra', 'Extra.log',,, 0)
            .
         ELSE
            L_VG:T_O_O_Night      = Get_Manifest_Info(MAN:MID, 0,,1, L_SG:Output)             ! All charges - from invoice
            L_VG:T_O_Broking      = Get_InvoicesTransporter(0, 1, 1, 0, 1, MAN:MID,,,,, L_SG:Output,1)           ! Del legs - ex vat
         .
                                               
     db.debugout('[Print_Turnover]  MAN:MID: ' & MAN:MID & ', T_O_O_Night: ' & L_VG:T_O_O_Night & ',  T_O_Broking: ' & L_VG:T_O_Broking)
  
         L_VG:T_O_O_Night        -= L_VG:T_O_Broking                                          ! Legs cost off total DI value (included)
      ELSE
         L_VG:T_O_O_Night         = 0.0
         IF L_SG:VAT = TRUE
            L_VG:T_O_Broking      = Get_Manifest_Info(MAN:MID, 4,,1, L_SG:Output)             ! All charges - from invoice    inc
  
            IF L_VG:T_O_Broking > 0.0
               Add_Log('MID: ' & MAN:MID & ',  L_VG:T_O_Broking: ' & L_VG:T_O_Broking  & ',  MAN:CreatedDate: ' & FORMAT(MAN:CreatedDate,@d6), 'Get_Manifest_Info-Extra', 'Extra.log',,, 0)
            .
         ELSE
            L_VG:T_O_Broking      = Get_Manifest_Info(MAN:MID, 0,,1, L_SG:Output)             ! All charges - from invoice
      .  .
  
      
      ! Get_Invoices          (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID, p:ToDate, p:Output, p:Limit_On)
      ! p:Option
      !   0.  - Total Charges inc.
      !   1.  - Excl VAT
      !   2.  - VAT
      ! p:Type
      !   0.  - All
      !   1.  - Invoices                      Total >= 0.0
      !   2.  - Credit Notes                  Total < 0.0
      !   3.  - Credit Notes excl Bad Debts   Total < 0.0
      ! p:Limit_On
      !   0.  Date & Branch
      !   1.  MID
  
      L_DQ:Date                   = MAN:CreatedDate
      GET(LOC:Dates_Done, L_DQ:Date)
      IF ERRORCODE()
         Cred_Type_#      = 2
         IF L_SG:ExcludeBadDebts = TRUE
            Cred_Type_#   = 3
         .
  
         IF L_SG:VAT = TRUE
            ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID, p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source)
            L_VG:C_Note           = -Get_Invoices(MAN:CreatedDate, 0, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1)    ! Any passed on this day
         ELSE
            L_VG:C_Note           = -Get_Invoices(MAN:CreatedDate, 1, Cred_Type_#,, L_SG:BID,, L_SG:Output,,,1)    ! Any passed on this day
      .  .
  
      IF MAN:Broking = FALSE
         IF L_SG:VAT = TRUE
            ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type, p:Output, p:Source)
            ! (LONG  , BYTE    , BYTE=0, BYTE=0        , BYTE=0        , <ULONG>, ULONG=0, <LONG>  , BYTE=0     , BYTE=0         , BYTE=0  , BYTE=0),STRING
            !   1           2       3         4               5           6        7         8          9           10              11     , 12    
            ! p:Option                                                2
            !   0.  - Total Charges inc.
            !   1.  - Excl VAT
            !   2.  - VAT
            ! p:Type                                                  3
            !   0.  - All
            !   1.  - Invoices                      Total >= 0.0
            !   2.  - Credit Notes                  Total < 0.0
            ! p:DeliveryLegs                                          5
            !   0.  - None
            !   1.  - Del Legs only
            !   2.  - Both
            ! p:MID                                                   6
            !   0   = Not for a MID
            !   x   = for a MID
            ! p:Extra_Inv                                             9
            !   Only
            ! p:Invoice_Type (was Manifest_Type)                      10
            !   0   - All
            !   1   - Overnight
            !   2   - Broking
            ! p:Source                                                12
            !   1   - Management Profit
            !   2   - Management Profit Summary
            L_VG:Loadmaster       = Get_InvoicesTransporter(0, 0, 1, 0, 0, MAN:MID,,,,, L_SG:Output,1)
         ELSE
            L_VG:Loadmaster       = Get_InvoicesTransporter(0, 1, 1, 0, 0, MAN:MID,,,,, L_SG:Output,1)
         .
         L_VG:Trans_Broking       = L_VG:T_O_Broking              ! Legs cost is broking cost in this case
      ELSE
         IF L_SG:VAT = TRUE
            L_VG:Trans_Broking    = Get_InvoicesTransporter(0, 0, 1, 0, 0, MAN:MID,,,,, L_SG:Output,1)
            L_VG:Trans_Broking   += Get_InvoicesTransporter(0, 0, 1, 0, 1, MAN:MID,,,,, L_SG:Output,1)   ! Del legs - inc vat
         ELSE
            L_VG:Trans_Broking    = Get_InvoicesTransporter(0, 1, 1, 0, 0, MAN:MID,,,,, L_SG:Output,1)
            !L_VG:T_O_Broking      = Get_InvoicesTransporter(0, 1, 1, 0, 1, MAN:MID)   ! Del legs - ex vat        ???
            L_VG:Trans_Broking   += Get_InvoicesTransporter(0, 1, 1, 0, 1, MAN:MID,,,,, L_SG:Output,1)   ! Del legs - ex vat
      .  .
  
  
      ! Get all Extra Invoices not associated to a MID
      GET(LOC:Dates_Done, L_DQ:Date)
      IF ERRORCODE()
         IF L_SG:VAT = TRUE
            L_VG:Ext_Invocie_TO   = Get_Invoices(MAN:CreatedDate, 0, 1, TRUE, L_SG:BID,, L_SG:Output,,,1)   ! Invoice for day not related to MID
  
            IF L_VG:Ext_Invocie_TO > 0.0
               Add_Log('MID: ' & MAN:MID & ',  L_VG:Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',   MAN:CreatedDate: ' & FORMAT(MAN:CreatedDate,@d6), 'Get_Manifest_Info-Extra', 'Extra.log',,, 0)
            .
  
            ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID, p:BID, p:ToDate, p:Extra_Inv, p:Manifest_Type, p:Output)
            L_VG:Extra_Inv        = Get_InvoicesTransporter(MAN:CreatedDate, 0, 1, FALSE,,, L_SG:BID,,TRUE,, L_SG:Output,1)    ! Credits for day - Transporter not MID
            L_VG:Trans_Credit     = -Get_InvoicesTransporter(MAN:CreatedDate, 0, 2, TRUE,,, L_SG:BID,,,, L_SG:Output,1)         ! Credits for day - Transporter not MID
  
            L_VG:Trans_Debit      = Get_InvoicesTransporter(MAN:CreatedDate, 0, 2,,,, L_SG:BID,,,, L_SG:Output,1)
  
     db.debugout('[Print_Turnover]  Extra invoices - Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',  Extra_Inv: ' & L_VG:Extra_Inv & ',  Trans_Credit: ' & L_VG:Trans_Credit & ',  Trans_Debit: ' & L_VG:Trans_Debit)
  
         ELSE
            L_VG:Ext_Invocie_TO   = Get_Invoices(MAN:CreatedDate, 1, 1, TRUE, L_SG:BID,, L_SG:Output,,,1)   ! Invoice for day not related to MID
  
            L_VG:Extra_Inv        = Get_InvoicesTransporter(MAN:CreatedDate, 1, 1, FALSE,,, L_SG:BID,,TRUE,, L_SG:Output,1)      ! Credits for day - Transporter not MID
            L_VG:Trans_Credit     = -Get_InvoicesTransporter(MAN:CreatedDate, 1, 2, TRUE,,, L_SG:BID,,,, L_SG:Output,1)     ! Credits for day - Transporter not MID
  
            L_VG:Trans_Debit      = Get_InvoicesTransporter(MAN:CreatedDate, 1, 2,,,, L_SG:BID,,,, L_SG:Output,1)
      .  .
  
      L_VG:Total_Cost             = L_VG:Loadmaster + L_VG:Trans_Broking + L_VG:Extra_Inv - L_VG:Trans_Credit
  
      L_VG:Total_Cost_Less_Deb    = L_VG:Total_Cost - L_VG:Trans_Debit
  
     db.debugout('[Print_Turnover]  Ext_Invocie_TO: ' & L_VG:Ext_Invocie_TO & ',  T_O_Broking: ' & L_VG:T_O_Broking & ',  C_Note: ' & L_VG:C_Note)
  
      L_VG:T_O_Broking           += L_VG:Ext_Invocie_TO                                                   ! Add extras to BROKING ALWAYS
  
      L_VG:Total_T_O              = L_VG:T_O_Broking + L_VG:T_O_O_Night - L_VG:C_Note
  
  
      L_VG:GP                     = L_VG:Total_T_O - L_VG:Total_Cost_Less_Deb
      L_VG:GP_Per                 = (L_VG:GP / L_VG:Total_T_O) * 100
  
  
  
      L_DQ:Date                   = MAN:CreatedDate
      GET(LOC:Dates_Done, L_DQ:Date)
      IF ERRORCODE()
         L_DQ:Date                = MAN:CreatedDate
         ADD(LOC:Dates_Done, L_DQ:Date)
      .
  IF 0
    PRINT(RPT:totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
      IF LOC:Errors > 0
         MESSAGE('Completed with Errors: ' & LOC:Errors & '||Please review the System Log.', 'Print Turnover', ICON:Exclamation)
      .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Turnover_june15','Print_Turnover_june15','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True


FDB4.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      ! Add an All
      Queue:FileDrop.BRA:BranchName           = 'All'
      GET(Queue:FileDrop, Queue:FileDrop.BRA:BranchName)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop)
         Queue:FileDrop.BRA:BranchName        = 'All'
         Queue:FileDrop.BRA:BID               = 0
         ADD(Queue:FileDrop)
  
         IF L_SG:BID = 0
            L_SG:BranchName   = 'All'
      .  .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Report
!!! ***  filter class  ***
!!! </summary>
Print_Manifests_old PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Data             GROUP,PRE()                           ! 
HorseReg             STRING(20)                            ! 
Tonnage              DECIMAL(12,2)                         ! 
Cost                 DECIMAL(13,2)                         ! 
Extra_Legs_Cost      DECIMAL(12,2)                         ! 
Legs_Cost_VAT        DECIMAL(13,2)                         ! 
GP                   DECIMAL(13,2)                         ! 
GP_Per               DECIMAL(5,1)                          ! 
Delivery_Charges_Ex  DECIMAL(13,2)                         ! 
                     END                                   ! 
LOC:Data_Totals      GROUP,PRE()                           ! 
Tot_GP               DECIMAL(15,2)                         ! 
Tot_Delivery_Charges_Ex DECIMAL(15,2)                      ! 
Total_GP_Per         DECIMAL(7,2)                          ! 
VCID                 ULONG                                 ! Vehicle Composition ID
Tot_GP_v             DECIMAL(15,2)                         ! 
Tot_Delivery_Charges_Ex_v DECIMAL(15,2)                    ! 
Total_GP_Per_v       DECIMAL(7,2)                          ! 
                     END                                   ! 
LOC:Options          GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
Included_TIDs        CSTRING('0<0>{503}')                  ! 
Branch               STRING(35)                            ! Branch Name
BID                  ULONG                                 ! Branch ID
BID_FirstTime        BYTE(1)                               ! 
Vehicles_for_Transporters BYTE                             ! 
Included_VCIDs       CSTRING('0<0>{499}')                  ! 
CompositionName      STRING(35)                            ! 
VCID                 ULONG                                 ! Vehicle Composition ID
Filter_By_Transporters BYTE                                ! 
Filter_By_Vehicles   BYTE                                  ! 
Summary              BYTE                                  ! 
                     END                                   ! 
LOC:Q                QUEUE,PRE(L_Q)                        ! 
Str                  STRING(20)                            ! 
                     END                                   ! 
Process:View         VIEW(Manifest)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:DepartDateAndTime)
                       PROJECT(MAN:MID)
                       PROJECT(MAN:Rate)
                       PROJECT(MAN:TID)
                       PROJECT(MAN:VCID)
                       JOIN(TRA:PKey_TID,MAN:TID)
                         PROJECT(TRA:TransporterName)
                       END
                       JOIN(VCO:PKey_VCID,MAN:VCID)
                       END
                     END
BRW7::View:Browse    VIEW(Transporter)
                       PROJECT(TRA:TransporterName)
                       PROJECT(TRA:TID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB9::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
FDB12::View:FileDrop VIEW(VehicleCompositionAlias)
                       PROJECT(A_VCO:CompositionName)
                       PROJECT(A_VCO:VCID)
                     END
Queue:FileDrop_Branch QUEUE                           !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop       QUEUE                            !
A_VCO:CompositionName  LIKE(A_VCO:CompositionName)    !List box control field - type derived from field
A_VCO:VCID             LIKE(A_VCO:VCID)               !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
ProgressWindow       WINDOW('Report Manifest'),AT(,,472,326),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,MDI,TIMER(1)
                       SHEET,AT(4,4,465,302),USE(?Sheet1)
                         TAB('Options'),USE(?Tab_Options)
                           PROMPT('From Date:'),AT(14,22),USE(?LO:From_Date:Prompt)
                           SPIN(@d5b),AT(66,22,60,10),USE(LO:From_Date),RIGHT(1)
                           BUTTON('...'),AT(130,22,12,10),USE(?Calendar)
                           PROMPT(''),AT(150,22,143,10),USE(?Prompt_DayFrom)
                           PROMPT('To Date:'),AT(14,38),USE(?LO:To_Date:Prompt)
                           SPIN(@d5b),AT(66,38,60,10),USE(LO:To_Date),RIGHT(1)
                           BUTTON('...'),AT(130,38,12,10),USE(?Calendar:2)
                           PROMPT(''),AT(150,38,143,10),USE(?Prompt_DayTo),HIDE
                           PROMPT('Branch:'),AT(14,58),USE(?Prompt5)
                           LIST,AT(66,58,111,10),USE(LO:Branch),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop_Branch)
                           CHECK(' &Summary'),AT(408,22),USE(LO:Summary)
                           CHECK(' Filter by &Transporters'),AT(66,72),USE(LO:Filter_By_Transporters)
                           GROUP,AT(36,88,18,116),USE(?Group_FT)
                             LIST,AT(66,96,177,108),USE(?List_Inc),VSCROLL,FORMAT('80L(2)|M~Transporters~@s50@'),FROM(LOC:Q)
                             LIST,AT(278,96,170,108),USE(?List),VSCROLL,FORMAT('140L(2)|M~Transporters~@s35@'),FROM(Queue:Browse), |
  IMM,MSG('Browsing Records')
                             GROUP('Transporters to Include'),AT(62,84,185,124),USE(?Group1),BOXED
                             END
                             GROUP('All Transporters'),AT(270,84,185,124),USE(?Group1:2),BOXED
                             END
                             BUTTON('<<-'),AT(254,126,12,24),USE(?Button_Add)
                             BUTTON('->'),AT(254,170,12,24),USE(?Button_Remove)
                             CHECK(' Filter by &Vehicles'),AT(66,212),USE(LO:Filter_By_Vehicles)
                             GROUP,AT(32,220,23,80),USE(?Group_FV)
                               CHECK(' &Vehicles for Selected Transporters'),AT(278,238),USE(LO:Vehicles_for_Transporters)
                               GROUP('All Vehicle Compositions'),AT(270,224,185,79),USE(?Group3),BOXED
                                 LIST,AT(278,252,170,10),USE(LO:CompositionName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Comp' & |
  'osition Name~@s255@'),FROM(Queue:FileDrop)
                               END
                               BUTTON('<<-'),AT(254,238,12,24),USE(?Button_Add_VC)
                               GROUP('Vehicle Compositions to Include'),AT(62,224,185,79),USE(?Group3:2),BOXED
                               END
                               BUTTON('->'),AT(254,274,12,24),USE(?Button_Remove_VC)
                               LIST,AT(66,236,177,60),USE(?List_Veh),HVSCROLL,FORMAT('255L(2)|M~Composition Name~@s255@'), |
  FROM(LOC:Q)
                             END
                           END
                         END
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(41,116,389,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(85,104,302,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(85,132,302,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Pause'),AT(416,308,,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                       BUTTON('Cancel'),AT(362,308,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Manifest Report'),AT(250,719,7750,10469),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,469),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Manifests'),AT(2677,10,2396,313),USE(?ReportTitle),FONT('Arial',18,,FONT:regular), |
  CENTER
                         STRING('From:'),AT(5708,146),USE(?String27:2),TRN
                         STRING(@d5b),AT(6021,146),USE(LO:From_Date),RIGHT(1)
                         STRING(@d5b),AT(7063,146),USE(LO:To_Date),RIGHT(1)
                         LINE,AT(0,470,7750,0),USE(?Line22),COLOR(COLOR:Black)
                         STRING('To:'),AT(6833,146),USE(?String27),TRN
                       END
break_transporter      BREAK(MAN:TID)
                         HEADER,AT(0,0,,583)
                           STRING('Transporter:'),AT(521,52),USE(?String35),FONT(,12,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@s35),AT(1594,52),USE(TRA:TransporterName),FONT(,12,,FONT:bold,CHARSET:ANSI),TRN
                           BOX,AT(0,354,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                           LINE,AT(781,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                           LINE,AT(2219,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                           LINE,AT(2792,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                           LINE,AT(3833,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                           LINE,AT(4875,354,0,250),USE(?HeaderLine:8),COLOR(COLOR:Black)
                           LINE,AT(7240,354,0,250),USE(?HeaderLine:9),COLOR(COLOR:Black)
                           LINE,AT(5365,354,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                           LINE,AT(6250,354,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                           STRING('MID'),AT(52,385,688,167),USE(?HeaderTitle:1),CENTER,TRN
                           STRING('Cost'),AT(3969,385,833,167),USE(?HeaderTitle:4),CENTER,TRN
                           STRING('Rate'),AT(4927,385,365,167),USE(?HeaderTitle:5),CENTER,TRN
                           STRING('Horse Reg.'),AT(833,385,1302,167),USE(?HeaderTitle:2),CENTER,TRN
                           STRING('Tonnage'),AT(2292,385,,167),USE(?HeaderTitle:3),TRN
                           STRING('Extra Legs'),AT(5469,385,677,167),USE(?HeaderTitle:6),CENTER,TRN
                           STRING('Turnover'),AT(2885,385,833,167),USE(?HeaderTitle:9),CENTER,TRN
                           STRING('GP'),AT(6375,385,677,167),USE(?HeaderTitle:7),CENTER,TRN
                           STRING('GP %'),AT(7313,385,354,167),USE(?HeaderTitle:8),TRN
                         END
break_vehicle            BREAK(MAN:VCID),USE(?break_vehicle)
Detail                     DETAIL,AT(,,7750,250),USE(?Detail)
                             LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                             LINE,AT(781,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                             LINE,AT(2219,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                             LINE,AT(2792,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                             LINE,AT(3833,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                             LINE,AT(4875,0,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                             LINE,AT(5365,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                             LINE,AT(6250,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                             LINE,AT(7240,0,0,250),USE(?HeaderLine:10),COLOR(COLOR:Black)
                             LINE,AT(7750,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                             STRING(@n_10),AT(52,52,,167),USE(MAN:MID),RIGHT
                             STRING(@s20),AT(833,52),USE(HorseReg)
                             STRING(@n6.1),AT(2292,52,448,167),USE(Tonnage),RIGHT(1)
                             STRING(@n-14.2),AT(2885,52),USE(Delivery_Charges_Ex),RIGHT(1)
                             STRING(@n-14.2),AT(3979,52,,167),USE(MAN:Cost),RIGHT(1)
                             STRING(@n5.2),AT(4927,52,,167),USE(MAN:Rate),RIGHT
                             STRING(@n-11.2b),AT(5469,52),USE(Extra_Legs_Cost),RIGHT(1)
                             STRING(@n-12.2),AT(6375,52),USE(GP),RIGHT(1)
                             STRING(@n5.1),AT(7313,52),USE(GP_Per),RIGHT(1)
                             LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                           END
                           FOOTER,AT(0,0,,417)
                             LINE,AT(167,52,7500,0),USE(?Line21:42),COLOR(COLOR:Black)
                             STRING(@n-17.2),AT(1750,94,990,156),USE(Tonnage,,?Tonnage:3),RIGHT(1),SUM,TALLY(Detail),RESET(break_vehicle), |
  TRN
                             STRING(@n-18.2),AT(2667,94),USE(Delivery_Charges_Ex,,?Delivery_Charges_Ex:3),RIGHT(1),SUM, |
  TALLY(Detail),RESET(break_vehicle),TRN
                             STRING(@n-14.2),AT(3979,94,,170),USE(MAN:Cost,,?MAN:Cost:3),RIGHT(1),SUM,TALLY(Detail),RESET(break_vehicle), |
  TRN
                             STRING(@n-17.2),AT(5156,94),USE(Extra_Legs_Cost,,?Extra_Legs_Cost:3),RIGHT(1),SUM,TALLY(Detail), |
  RESET(break_vehicle),TRN
                             STRING(@n-18.2),AT(6063,94),USE(Tot_GP_v),RIGHT(1),TRN
                             STRING(@n-7.2),AT(7208,94),USE(Total_GP_Per_v),RIGHT(1),TRN
                             STRING(@s20),AT(167,94),USE(HorseReg,,?HorseReg:2),TRN
                           END
                         END
                       END
grandtotals            DETAIL,AT(,,,354),USE(?grandtotals)
                         LINE,AT(167,52,7500,0),USE(?Line20),COLOR(COLOR:Black)
                         STRING(@n-17.2),AT(1750,94,990,156),USE(Tonnage,,?Tonnage:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-18.2),AT(2667,94),USE(Delivery_Charges_Ex,,?Delivery_Charges_Ex:2),RIGHT(1),SUM, |
  TALLY(Detail),TRN
                         STRING(@n-14.2),AT(3979,94,,170),USE(MAN:Cost,,?MAN:Cost:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-17.2),AT(5156,94),USE(Extra_Legs_Cost,,?Extra_Legs_Cost:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-18.2),AT(6063,94),USE(Tot_GP),RIGHT(1),TRN
                         STRING(@n-7.2),AT(7208,94),USE(Total_GP_Per),RIGHT(1),TRN
                         LINE,AT(167,271,7500,0),USE(?Line20:2),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass
BRW7                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyFilter            PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                      ! Default Locator
FDB9                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop_Branch         !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END

FDB12                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ApplyFilter            PROCEDURE(),DERIVED
                     END

A_Filter_Q       QUEUE,TYPE
Filter_Str          CSTRING(101)
Filter_Value        CSTRING(255)
                .

A_Filter_     CLASS,TYPE

A_FQ            &A_Filter_Q
Name_           STRING(100)                  ! This is used in A_Filter_My

Init            PROCEDURE(STRING p_Name, LONG p_ListCtrl)

Add_            PROCEDURE(STRING p_Str, STRING p_Value, BYTE p_PermitDup=0),VIRTUAL
Get_            PROCEDURE(),STRING

Del_            PROCEDURE(LONG p_Pos),LONG,PROC
Get_Item        PROCEDURE(LONG p_Pos, BYTE p_Type),STRING


Save_           PROCEDURE(STRING p_Name),LONG,PROC
Load_           PROCEDURE(STRING p_Name),LONG,PROC

Construct       PROCEDURE()
Destruct        PROCEDURE()
            .

! This class is where any specific work for this procedure or instance of the object can be implemented
A_Filter_My  CLASS(A_Filter_),TYPE
Add_            PROCEDURE(STRING p_Str, STRING p_Value, BYTE p_PermitDup=0),VIRTUAL
            .
            


Filters_Q       QUEUE,TYPE
Name_               STRING(101)
FO                  &A_Filter_My
                .

Filters_     CLASS,TYPE

FQ              &Filters_Q

Get_FO          PROCEDURE(STRING p_Name),LONG

Init            PROCEDURE(STRING p_Name, LONG p_ListCtrl)

Add_            PROCEDURE(STRING p_Name, STRING p_Str, STRING p_Value, BYTE p_PermitDup=0),VIRTUAL
Get_            PROCEDURE(STRING p_Name),STRING

Del_            PROCEDURE(STRING p_Name, LONG p_Pos),LONG,PROC
Get_Item        PROCEDURE(STRING p_Name, LONG p_Pos, BYTE p_Type),STRING


Save_           PROCEDURE(STRING p_Name, STRING p_SaveName),LONG,PROC
Load_           PROCEDURE(STRING p_Name, STRING p_SaveName),LONG,PROC

Construct       PROCEDURE()
Destruct        PROCEDURE()
            .






Fil         Filters_

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Get_Values              ROUTINE
    Cost                         = MAN:Cost                                              ! VAT is excluded

!    L_MT:Delivery_Charges        = Get_Manifest_Info(MAN:MID, 4)    !,,, p:Output)             ! 4 is VAT incl


    Extra_Legs_Cost              = Get_Manifest_Info(MAN:MID, 6)                         ! VAT is included
    Legs_Cost_VAT                = Extra_Legs_Cost - Get_Manifest_Info(MAN:MID, 3)       !,,, p:Output)   ! excluded (3), so minus ex to get VAT
    
!    L_MT:Out_VAT                 = L_MT:Cost * (MAN:VATRate / 100)
!    L_MT:Total_Cost              = L_MT:Cost + L_MT:Out_VAT                              ! Add VAT

    Delivery_Charges_Ex          = Get_Manifest_Info(MAN:MID, 0)    !,,, p:Output)             ! 4 is VAT EXCL
    Tot_Delivery_Charges_Ex     += Delivery_Charges_Ex

    GP                           = Delivery_Charges_Ex - Cost - (Extra_Legs_Cost - Legs_Cost_VAT)
    Tot_GP                      += GP

    GP_Per                       = (GP / Delivery_Charges_Ex) * 100  ! %


    Tonnage                      = Get_Manifest_Info(MAN:MID, 2) / 1000   !,,, p:Output)

    IF VCID ~= MAN:VCID
       Tot_GP_v                     = 0.0
       Tot_Delivery_Charges_Ex_v    = 0.0
       Total_GP_Per_v               = 0.0
    .

    Tot_GP_v                       += GP
    Tot_Delivery_Charges_Ex_v      += Delivery_Charges_Ex
    Total_GP_Per_v                 += (GP / Delivery_Charges_Ex) * 100

!    L_MT:Average_C_Per_Kg        = (L_MT:Delivery_Charges_Ex / L_MT:Total_Weight) * 100     ! in cents (not rands)

!    EXECUTE MAN:State + 1
!       L_RF:State  = 'Loading'
!       L_RF:State  = 'Loaded'
!       L_RF:State  = 'On Route'
!       L_RF:State  = 'Transferred'
!    .
    EXIT
Get_Horse               ROUTINE
!    A_TRU:TTID              = VCO:TTID0
!    IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!       L_RF:Reg_of_Hours    = A_TRU:Registration
!    .

!    DRI:DRID                = MAN:DRID
!    IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
!       L_RF:Driver          = CLIP(DRI:FirstName) & ' ' & DRI:Surname
!    .


    ! (p:VCID, p:Option, p:Truck, p:Additional)
    ! (ULONG, BYTE, BYTE=0, BYTE=0),STRING
    HorseReg    = Get_VehComp_Info(MAN:VCID, 2, 100)
    EXIT
! ------------------------------------------------------------------
Set_Days                ROUTINE
    ! (LONG),STRING
    ?Prompt_DayFrom{PROP:Text}      = Week_Day(LO:From_Date)

    ?Prompt_DayTo{PROP:Text}        = Week_Day(LO:To_Date)

    DISPLAY
    EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      Total_GP_Per    = (Tot_GP / Tot_Delivery_Charges_Ex) * 100  ! %
      PRINT(RPT:grandtotals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Manifests_old')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LO:From_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  BIND('LO:Included_TIDs',LO:Included_TIDs)                ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:Filters.Open                                      ! File Filters used by this procedure, so make sure it's RelationManager is open
  Relate:VehicleCompositionAlias.Open                      ! File VehicleCompositionAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:Transporter,SELF) ! Initialize the browse manager
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,TRA:Key_TransporterName)              ! Add the sort order for TRA:Key_TransporterName for sort order 1
  BRW7.AddLocator(BRW7::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW7::Sort0:Locator.Init(,TRA:TransporterName,1,BRW7)    ! Initialize the browse locator using  using key: TRA:Key_TransporterName , TRA:TransporterName
  BRW7.SetFilter('(LO:Included_TIDs)')                     ! Apply filter expression to browse
  BRW7.AddField(TRA:TransporterName,BRW7.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW7.AddField(TRA:TID,BRW7.Q.TRA:TID)                    ! Field TRA:TID is a hot field or requires assignment from browse
  INIMgr.Fetch('Print_Manifests_old',ProgressWindow)       ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Manifest, ?Progress:PctText, Progress:Thermometer, 200)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder('+TRA:TransporterName,+VCO:CompositionName,+MAN:MID')
  ThisReport.SetFilter('MAN:DepartDate >= LO:From_Date AND MAN:DepartDate << LO:To_Date + 1')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Manifest.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  IF ?LO:Filter_By_Transporters{Prop:Checked}
    ENABLE(?Group_FT)
  END
  IF NOT ?LO:Filter_By_Transporters{PROP:Checked}
    DISABLE(?Group_FT)
  END
  IF ?LO:Filter_By_Vehicles{Prop:Checked}
    ENABLE(?Group_FV)
  END
  IF NOT ?LO:Filter_By_Vehicles{PROP:Checked}
    DISABLE(?Group_FV)
  END
  FDB9.Init(?LO:Branch,Queue:FileDrop_Branch.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop_Branch,Relate:Branches,ThisWindow)
  FDB9.Q &= Queue:FileDrop_Branch
  FDB9.AddSortOrder(BRA:Key_BranchName)
  FDB9.AddField(BRA:BranchName,FDB9.Q.BRA:BranchName) !List box control field - type derived from field
  FDB9.AddField(BRA:BID,FDB9.Q.BRA:BID) !Primary key field - type derived from field
  FDB9.AddUpdateField(BRA:BID,LO:BID)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
  FDB12.Init(?LO:CompositionName,Queue:FileDrop.ViewPosition,FDB12::View:FileDrop,Queue:FileDrop,Relate:VehicleCompositionAlias,ThisWindow)
  FDB12.Q &= Queue:FileDrop
  FDB12.AddSortOrder(A_VCO:Key_Name)
  FDB12.AddField(A_VCO:CompositionName,FDB12.Q.A_VCO:CompositionName) !List box control field - type derived from field
  FDB12.AddField(A_VCO:VCID,FDB12.Q.A_VCO:VCID) !Primary key field - type derived from field
  FDB12.AddUpdateField(A_VCO:VCID,LO:VCID)
  ThisWindow.AddItem(FDB12.WindowComponent)
  FDB12.DefaultFill = 0
      Fil.Init('Transporters', ?List_Inc)
      Fil.Init('Vehicles', ?List_Veh)
      Fil.Load_('Transporters', 'Print_Manifest-Transporters')
      Fil.Load_('Vehicles', 'Print_Manifest-Vehicles')
  
  
      LO:Included_TIDs        = Fil.Get_('Transporters')
      LO:Included_VCIDs       = Fil.Get_('Vehicles')
  
      IF CLIP(LO:Included_TIDs) = ''
         LO:Included_TIDs = '0'
      .
  
      IF CLIP(LO:Included_VCIDs) = ''
         LO:Included_VCIDs = '0'
      .
  
      BRW7.SetFilter('SQL(TID NOT IN (' & LO:Included_TIDs & '))')
      FDB12.SetFilter('SQL(VCID NOT IN (' & LO:Included_VCIDs & '))')
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:Filters.Close
    Relate:VehicleCompositionAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Manifests_old',ProgressWindow)    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LO:From_Date
          DO Set_Days
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',LO:From_Date)
      IF Calendar5.Response = RequestCompleted THEN
      LO:From_Date=Calendar5.SelectedDate
      DISPLAY(?LO:From_Date)
      END
      ThisWindow.Reset(True)
          DO Set_Days
      
    OF ?LO:To_Date
          DO Set_Days
      
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',LO:To_Date)
      IF Calendar6.Response = RequestCompleted THEN
      LO:To_Date=Calendar6.SelectedDate
      DISPLAY(?LO:To_Date)
      END
      ThisWindow.Reset(True)
          DO Set_Days
      
    OF ?LO:Filter_By_Transporters
      IF ?LO:Filter_By_Transporters{PROP:Checked}
        ENABLE(?Group_FT)
      END
      IF NOT ?LO:Filter_By_Transporters{PROP:Checked}
        DISABLE(?Group_FT)
      END
      ThisWindow.Reset()
    OF ?Button_Add
      ThisWindow.Update()
      BRW7.UpdateViewRecord()
          Fil.Add_('Transporters', TRA:TransporterName, TRA:TID)
      
          LO:Included_TIDs    = Fil.Get_('Transporters')
      
      !    message('tids:||' & LO:Included_TIDs)
      
          IF CLIP(LO:Included_TIDs) = ''
             LO:Included_TIDs = '0'
          .
          !BRW7.Updatebuffer()
          BRW7.Reset(1)
          BRW7.ResetQueue(Reset:Done)
          !BRW7.UpdateWindow()
    OF ?Button_Remove
      ThisWindow.Update()
          Fil.Del_('Transporters', CHOICE(?List_Inc))
      
          LO:Included_TIDs    = Fil.Get_('Transporters')
          IF CLIP(LO:Included_TIDs) = ''
             LO:Included_TIDs = '0'
          .
          !BRW7.Updatebuffer()
          BRW7.Reset(1)
          BRW7.ResetQueue(Reset:Done)
          !BRW7.UpdateWindow()
    OF ?LO:Filter_By_Vehicles
      IF ?LO:Filter_By_Vehicles{PROP:Checked}
        ENABLE(?Group_FV)
      END
      IF NOT ?LO:Filter_By_Vehicles{PROP:Checked}
        DISABLE(?Group_FV)
      END
      ThisWindow.Reset()
    OF ?LO:Vehicles_for_Transporters
          FDB12.Reset(1)
          FDB12.ResetQueue(Reset:Done)
          
      
    OF ?Button_Add_VC
      ThisWindow.Update()
          Fil.Add_('Vehicles', LO:CompositionName, LO:VCID)
      
          LO:Included_VCIDs    = Fil.Get_('Vehicles')
      
      !    message('tids:||' & LO:Included_TIDs)
      
          IF CLIP(LO:Included_VCIDs) = ''
             LO:Included_VCIDs = '0'
          .
          CLEAR(LO:CompositionName)
      
          FDB12.Reset(1)
          FDB12.ResetQueue(Reset:Done)
      
          GET(Queue:FileDrop, 1)
          ?LO:CompositionName{PROP:Selected}  = 1
          POST(EVENT:Accepted, ?LO:CompositionName)
      
          DISPLAY
    OF ?Button_Remove_VC
      ThisWindow.Update()
          Fil.Del_('Vehicles', CHOICE(?List_Veh))
      
          LO:Included_VCIDs    = Fil.Get_('Vehicles')
          IF CLIP(LO:Included_VCIDs) = ''
             LO:Included_VCIDs = '0'
          .
          FDB12.Reset(1)
          FDB12.ResetQueue(Reset:Done)
      
    OF ?Pause
      ThisWindow.Update()
          Fil.Save_('Transporters', 'Print_Manifest-Transporters')
          Fil.Save_('Vehicles', 'Print_Manifest-Vehicles')
          DISABLE(?Tab_Options)
      
      
          
          ThisReport.AddSortOrder()
      
          !ThisReport.SetFilter('')
      
          IF LO:BID ~= 0
             ThisReport.SetFilter('MAN:BID = ' & LO:BID, '0')
          .
      
          IF LO:From_Date > 0
             ThisReport.SetFilter('MAN:DepartDate >= LO:From_Date', '1')
          .
      
          IF LO:To_Date > 0
             ThisReport.SetFilter('MAN:DepartDate << LO:To_Date + 1', '2')
          .
      
          IF LO:Filter_By_Transporters = TRUE
             LO:Included_TIDs        = Fil.Get_('Transporters')
             IF CLIP(LO:Included_TIDs) ~= '' AND CLIP(LO:Included_TIDs) ~= '0'
                ThisReport.SetFilter('SQL(A.TID IN (' & CLIP(LO:Included_TIDs) & '))', '3')
             .
      
             IF LO:Filter_By_Vehicles = TRUE
                LO:Included_VCIDs      = Fil.Get_('Vehicles')
                IF CLIP(LO:Included_VCIDs) ~= '' AND CLIP(LO:Included_VCIDs) ~= '0'
                   ThisReport.SetFilter('SQL(A.VCID IN (' & CLIP(LO:Included_VCIDs) & '))', '4')
          .  .  .
      
          ThisReport.ApplyFilter()
      
      
      
          IF LO:Summary = TRUE
             SETTARGET(Report)
             ?Detail{PROP:Height}   = 0
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LO:From_Date
          DO Set_Days
    OF ?LO:To_Date
          DO Set_Days
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

A_Filter_.Add_            PROCEDURE(STRING p_Str, STRING p_Value, BYTE p_PermitDup=0)
A_Ok    BYTE(1)
    CODE
    SELF.A_FQ.Filter_Value    = p_Value

    IF p_PermitDup = 0
       GET(SELF.A_FQ, SELF.A_FQ.Filter_Value)
       IF ~ERRORCODE()
          A_Ok  = FALSE
    .  .

    IF A_Ok = TRUE
       SELF.A_FQ.Filter_Str      = p_Str
       SELF.A_FQ.Filter_Value    = p_Value
       ADD(SELF.A_FQ)
    .
    RETURN


A_Filter_.Del_            PROCEDURE(LONG p_Pos)       !,LONG
Ret     LONG
    CODE
    Ret     = 0
    GET(SELF.A_FQ, p_Pos)
    IF ERRORCODE()
       Ret  = -1
    ELSE
       DELETE(SELF.A_FQ)
    .
    RETURN(Ret)


A_Filter_.Get_Item        PROCEDURE(LONG p_Pos, BYTE p_Type)  !,STRING
Fltr        CSTRING(500)
    CODE
    GET(SELF.A_FQ, p_Pos)
    IF ~ERRORCODE()
       EXECUTE p_Type
          Fltr  = SELF.A_FQ.Filter_Str
          Fltr  = SELF.A_FQ.Filter_Value
    .  .
    RETURN(Fltr)


A_Filter_.Get_            PROCEDURE()     !,STRING
Fltr        CSTRING(500)
Idx         LONG
    CODE
    Fltr    = ''
    Idx     = 0

    LOOP
       Idx  += 1
       GET(SELF.A_FQ, Idx)
       IF ERRORCODE()
          BREAK
       .

       !SELF.A_FQ.Filter_Str
       !

       Fltr     = Fltr & ',' & SELF.A_FQ.Filter_Value
    .
    Fltr        = SUB(Fltr, 2, LEN(CLIP(Fltr)))
    RETURN(Fltr)








A_Filter_.Init            PROCEDURE(STRING p_Name, LONG p_ListCtrl)
    CODE
    SELF.Name_              = p_Name

    p_ListCtrl{PROP:From}   = SELF.A_FQ
    RETURN

A_Filter_.Construct       PROCEDURE()
    CODE
    SELF.A_FQ                 &= NEW(A_Filter_Q)
    RETURN

A_Filter_.Destruct        PROCEDURE()
    CODE
    DISPOSE(SELF.A_FQ)
    RETURN
A_Filter_.Save_           PROCEDURE(STRING p_Name)    !,LONG
Ret     LONG
    CODE
    Access:Filters.Open()
    Access:Filters.UseFile()

    CLEAR(_F:Record)
    _F:Name      = p_Name
    IF Access:Filters.TryFetch(_F:Key_Name) ~= LEVEL:Benign
       Access:Filters.PrimeRecord()
       _F:Name   = p_Name
       IF Access:Filters.TryInsert() ~= LEVEL:Benign
          Access:Filters.CancelAutoInc()
          Ret    = -1
    .  .

    _F:Value     = SELF.Get_()

    IF Access:Filters.TryUpdate() ~= LEVEL:Benign
       Ret   = -2
    .

    Access:Filters.Close()
    RETURN(Ret)


A_Filter_.Load_           PROCEDURE(STRING p_Name)    !,LONG
Ret     LONG
Str     STRING(500)
    CODE
    Access:Filters.Open()
    Access:Filters.UseFile()

    CLEAR(_F:Record)
    _F:Name  = p_Name
    IF Access:Filters.TryFetch(_F:Key_Name) ~= LEVEL:Benign
       Ret   = -1
    ELSE
       Str   = _F:Value
       LOOP
          IF CLIP(Str) = ''
             BREAK
          .
          ! (*STRING, STRING, BYTE = 0, BYTE = 0),STRING
          SELF.Add_('', Get_1st_Element_From_Delim_Str(Str, ',', TRUE))
       .

       ! No strings to go with the values have been loaded... the filter class wont know about the files that
       ! the strings would come from (like it doesnt know what the values mean).  We could store them along with vlaues
       ! but if the strings (in this case Transporters names) changed then this would be bad.
       ! We need to load the strings though, so another process must load them, we could create a virtual method
       ! and the individual implementation would have to provide this...
       ! Made tha Add_ virtual....
    .

    Access:Filters.Close()
    RETURN(Ret)



! _Filters        FILE,DRIVER('MSSQL'),CREATE,PRE(_F),THREAD,BINDABLE,NAME('_Filters')
! PKey_FID        KEY(_F:FID),NOCASE,OPT,PRIMARY
! Key_Name        KEY(_F:Name),NOCASE,OPT
! Record            RECORD,PRE()
! FID                 ULONG
! Name                CSTRING(101)
! Values              CSTRING(1001)
!                 . .
! ------------------------------------------------------------------
Filters_.Add_            PROCEDURE(STRING p_Name, STRING p_Str, STRING p_Value, BYTE p_PermitDup=0)
    CODE
    IF SELF.Get_FO(p_Name) = 0
       SELF.FQ.FO.Add_(p_Str, p_Value, p_PermitDup)
    .
    RETURN

Filters_.Del_            PROCEDURE(STRING p_Name, LONG p_Pos)       !,LONG
Ret     LONG
    CODE
    Ret     = SELF.Get_FO(p_Name)
    IF Ret = 0
       RETURN( SELF.FQ.FO.Del_(p_Pos) )
    .
    RETURN(Ret)


Filters_.Get_Item        PROCEDURE(STRING p_Name, LONG p_Pos, BYTE p_Type)  !,STRING
Fltr        CSTRING(501)
    CODE
    IF SELF.Get_FO(p_Name) = 0
        Fltr    = SELF.FQ.FO.Get_Item(p_Pos, p_Type)
    .
    RETURN(Fltr)


Filters_.Get_            PROCEDURE(STRING p_Name)     !,STRING
Fltr        CSTRING(501)
    CODE
    IF SELF.Get_FO(p_Name) = 0
        Fltr    = SELF.FQ.FO.Get_()
    .
    RETURN(Fltr)




Filters_.Init            PROCEDURE(STRING p_Name, LONG p_ListCtrl)
Res     LONG
    CODE
    Res                 = SELF.Get_FO(p_Name)

    CASE Res
    OF 0        ! Exist and got
       SELF.FQ.FO.Init(p_Name, p_ListCtrl)

       SELF.FQ.Name_    = p_Name
       PUT(SELF.FQ)
    ELSE        ! Doesnt exit
       ! Add an object to the Queue
       SELF.FQ.Name_    = p_Name
       SELF.FQ.FO      &= NEW(A_Filter_My)
       ADD(SELF.FQ)

       SELF.FQ.FO.Init(p_Name, p_ListCtrl)
    .
    RETURN


Filters_.Construct       PROCEDURE()
    CODE
    SELF.FQ                 &= NEW(Filters_Q)
    RETURN

Filters_.Destruct        PROCEDURE()
    CODE
    LOOP
       GET(SELF.FQ,1)
       IF ERRORCODE()
          BREAK
       .
       DISPOSE(SELF.FQ.FO)
       DELETE(SELF.FQ)
    .

    DISPOSE(SELF.FQ)
    RETURN



Filters_.Get_FO         PROCEDURE(STRING p_Name)        !,LONG
Ret     LONG
    CODE
    SELF.FQ.Name_   = p_Name
    GET(SELF.FQ, SELF.FQ.Name_)
    IF ERRORCODE()
       Ret  = -1
    .
    RETURN(Ret)
Filters_.Save_           PROCEDURE(STRING p_Name, STRING p_SaveName)    !,LONG
Ret     LONG
    CODE
    Ret     = SELF.Get_FO(p_Name)
    IF Ret = 0
       Ret  = SELF.FQ.FO.Save_(p_SaveName)
    .
    RETURN(Ret)


Filters_.Load_           PROCEDURE(STRING p_Name, STRING p_SaveName)    !,LONG
Ret     LONG
    CODE
    Ret     = SELF.Get_FO(p_Name)
    IF Ret = 0
       Ret  = SELF.FQ.FO.Load_(p_SaveName)
    .
    RETURN(Ret)

!    Fil.Save_('Transporters', 'Print_Manifest-Transporters')
!    Fil.Load_('Transporters', 'Print_Manifest-Transporters')

!    Fil.Save_('Vehicles', 'Print_Manifest-Transporters')
!    Fil.Load_('Vehicles', 'Print_Manifest-Transporters')

! ------------------------------------------------------------------
A_Filter_My.Add_         PROCEDURE(STRING p_Str, STRING p_Value, BYTE p_PermitDup=0)       !,VIRTUAL
l_Str       STRING(1001)
A_Ok        BYTE(1)
    CODE
    ! I know about my tables in this procedure...
    l_Str                       = p_Str

!    Fil.Load_('Transporters', 'Print_Manifest-Transporters')
!    Fil.Load_('Vehicles', 'Print_Manifest-Transporters')

    IF CLIP(l_Str) = ''
       CASE SELF.Name_
       OF 'Transporters'
          ! Load it from table based on p_Value
          TRA:TID               = p_Value
          IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
             l_Str              = TRA:TransporterName
          .
       OF 'Vehicles'
          ! Load it from table based on p_Value
          A_VCO:VCID            = p_Value
          IF Access:VehicleCompositionAlias.TryFetch(A_VCO:PKey_VCID) = LEVEL:Benign
             l_Str              = A_VCO:CompositionName
    .  .  .


    !PARENT.Add_(p_Str, p_Value)
    ! **** ?????
    ! The above GPF's but it shouldnt?
    

    SELF.A_FQ.Filter_Value      = p_Value

    IF p_PermitDup = 0
       GET(SELF.A_FQ, SELF.A_FQ.Filter_Value)
       IF ~ERRORCODE()
          A_Ok  = FALSE
    .  .

    IF A_Ok = TRUE
       SELF.A_FQ.Filter_Str     = l_Str
       SELF.A_FQ.Filter_Value   = p_Value
       ADD(SELF.A_FQ)
    .

!    message('p_Str: ' & p_Str & '|p_Value: ' & p_Value)
    RETURN

ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      DO Get_Values
      DO Get_Horse
      IF LO:Summary = TRUE
         SETTARGET(Report)
         ?Detail{PROP:Height}   = 0
      .
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:grandtotals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Manifests','Print_Manifests','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True


BRW7.ApplyFilter PROCEDURE

  CODE
  !    LO:Included_TIDs    = Fil.Get_('Transporters')     - loaded already
  
      BRW7.SetFilter('SQL(TID NOT IN (' & LO:Included_TIDs & '))')
  
  PARENT.ApplyFilter


FDB9.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
        Queue:FileDrop_Branch.BRA:BranchName       = 'All'
        GET(Queue:FileDrop_Branch, Queue:FileDrop_Branch.BRA:BranchName)
        IF ERRORCODE()
           CLEAR(Queue:FileDrop_Branch)
           Queue:FileDrop_Branch.BRA:BranchName    = 'All'
           Queue:FileDrop_Branch.BRA:BID           = 0
           ADD(Queue:FileDrop_Branch)
        .
    
    
        IF LO:BID_FirstTime = TRUE
           !SELECT(?LO:Branch, RECORDS(Queue:FileDrop_Branch))
    
           LO:BID         = 0
           LO:Branch      = 'All'
        .
    
        LO:BID_FirstTime     = FALSE
  RETURN ReturnValue


FDB12.ApplyFilter PROCEDURE

  CODE
      !LO:Included_VCIDs      = Fil.Get_('Vehicles')      - loaded already
  
      FDB12.SetFilter('SQL(VCID NOT IN (' & LO:Included_VCIDs & '))')
  
      IF LO:Vehicles_for_Transporters = TRUE
         LO:Included_TIDs    = Fil.Get_('Transporters')
         IF CLIP(LO:Included_TIDs) ~= ''
            FDB12.SetFilter('SQL(TID IN (' & LO:Included_TIDs & '))','2')
         ELSE
            FDB12.SetFilter('','2')
         .
      ELSE
         FDB12.SetFilter('','2')
      .
  PARENT.ApplyFilter

!!! <summary>
!!! Generated from procedure template - Process
!!! Process the Clients File
!!! </summary>
Process_Client_Balances_old PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
LOC:Per_Timer        LONG                                  ! 
Process:View         VIEW(Clients)
                     END
ProgressWindow       WINDOW('Process Clients'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_Client_Balances_old')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  ProgressWindow{Prop:MDI} = True                          ! Make progress window an MDI child window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 100                         ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:Clients, ?Progress:PctText, Progress:Thermometer, ProgressMgr, CLI:CID)
  ThisProcess.AddSortOrder(CLI:PKey_CID)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='Updating Static Balances...'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(Clients,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
          db.debugout('done per timer:  ' & LOC:Per_Timer)
      
      
      
          LOC:Per_Timer    = 0
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
      Gen_Statement(0, CLI:CID, TODAY(), CLOCK(),, TRUE, LOC:Statement_Info)
  
  
      LOC:Per_Timer   += 1
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Report
!!! ------------------- complete Fuel Surcharge code - done report fields.....
!!! </summary>
Print_Rates_2_July_07 PROCEDURE (p:CID)

Progress:Thermometer BYTE                                  ! 
LOC:Group            GROUP,PRE(LOC)                        ! 
CID                  ULONG                                 ! Client ID
FID                  ULONG                                 ! Floor ID
Printed_CP_Head      BYTE                                  ! 
Printed_AD_Head      BYTE                                  ! 
Printed_FS_Head      BYTE                                  ! 
Terms                STRING(20)                            ! Terms - Pre Paid, COD, Account, On Statement
InsuranceRequired    STRING(20)                            ! Insurance Required
ACCID                ULONG                                 ! Additional Charge Category ID
Terms_Bold           BYTE                                  ! 
Journey_Type         BYTE                                  ! 0 = Journey, 1 = Desc.
Journey              STRING(100)                           ! Description
VolumetricRatio      DECIMAL(8,2)                          ! x square cube weighs this amount
ClientNo             ULONG                                 ! Client No.
CID_VR               ULONG                                 ! Client ID
Discounted_Rate_Per_Kg DECIMAL(10,4)                       ! Used on the Container Park Rates Detail
Client_Status        STRING(20)                            ! Normal, On Hold, Closed, Dormant
                     END                                   ! 
LOC:Print_Rates      BYTE                                  ! 
L_OG:Options         GROUP,PRE(L_OG)                       ! 
EffectiveRatesOnly   BYTE                                  ! Only print effective rates
EffectiveDateAtDate  DATE                                  ! Print Rates that were in effect at this date
EffectiveDateAtDate_Rpt DATE                               ! 
EffectiveRatesOnly_Rpt BYTE                                ! 
CreateLog            BYTE                                  ! 
                     END                                   ! 
L_EQ:Effective_Q     QUEUE,PRE(L_EQ)                       ! 
LTID                 ULONG                                 ! Load Type ID
CRTID                ULONG                                 ! Client Rate Type ID
JID                  ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
CTID                 ULONG(0)                              ! Container Type ID
ToMass               DECIMAL(9)                            ! Up to this mass in Kgs
                     END                                   ! 
L_CPQ:Container_Parks_Discount_Q QUEUE,PRE(L_CPQ)          ! 
FID                  ULONG                                 ! Floor ID
ContainerParkRateDiscount DECIMAL(5,2)                     ! Discount % that is applied to the container park rates for this client
                     END                                   ! 
Process:View         VIEW(Clients)
                       PROJECT(CLI:AccountLimit)
                       PROJECT(CLI:CID)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:DateOpened)
                       PROJECT(CLI:DocumentCharge)
                       PROJECT(CLI:InsuranceRequired)
                       PROJECT(CLI:Terms)
                       PROJECT(CLI:VolumetricRatio)
                       PROJECT(CLI:BID)
                       JOIN(BRA:PKey_BID,CLI:BID)
                         PROJECT(BRA:BranchName)
                       END
                     END
ProgressWindow       WINDOW('Report Rates'),AT(,,191,140),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  TIMER(1)
                       BUTTON('Pause'),AT(84,122,49,15),USE(?Pause),LEFT,ICON('VCRDOWN.ICO'),FLAT
                       SHEET,AT(4,3,184,116),USE(?Sheet1)
                         TAB('Process'),USE(?Tab1)
                           PROGRESS,AT(40,44,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(26,32,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(26,60,141,10),USE(?Progress:PctText),CENTER
                         END
                         TAB('Options'),USE(?Tab_Options)
                           CHECK(' Effective Rates Only'),AT(88,20),USE(L_OG:EffectiveRatesOnly),MSG('Only print e' & |
  'ffective rates'),TIP('Only print effective rates')
                           PROMPT('Effective Rate at Date:'),AT(10,36),USE(?L_OG:EffectiveDateAtDate:Prompt)
                           SPIN(@d5b),AT(88,36,65,10),USE(L_OG:EffectiveDateAtDate),RIGHT(1),MSG('Print Rates that' & |
  ' were in effect at this date'),TIP('Print Rates that were in effect at this date')
                           BUTTON('...'),AT(156,36,12,10),USE(?Calendar)
                           PROMPT('To always use the current day as the Effective Date clear the Effective Rate at Date box'), |
  AT(9,52,173,20),USE(?Prompt2)
                           CHECK(' Create &Log'),AT(88,81),USE(L_OG:CreateLog)
                           BUTTON('&Save'),AT(88,100,49,15),USE(?Button_Save),TIP('Save options now.<0DH,0AH>Optio' & |
  'ns will also be saved if you print rates but not if you cancel.')
                         END
                       END
                       BUTTON('Cancel'),AT(139,122,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Rates Report'),AT(198,1115,8000,9302),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(198,250,,844),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Rates'),AT(3573,52),USE(?ReportTitle),FONT('Arial',28,,FONT:bold,CHARSET:ANSI),CENTER
                         IMAGE('fbn_logo_small.jpg'),AT(52,52,2300,460),USE(?Image1)
                         STRING(@s100),AT(2396,615,2917,208),USE(CLI:ClientName,,?CLI:ClientName:2),FONT(,10,,FONT:regular, |
  CHARSET:ANSI),TRN
                         STRING('Client:'),AT(1906,615),USE(?String20:2),FONT(,10,,FONT:regular,CHARSET:ANSI),TRN
                         STRING(@n_10b),AT(5323,615,,208),USE(CLI:ClientNo,,?CLI:ClientNo:2),FONT(,10,,FONT:regular, |
  CHARSET:ANSI),RIGHT(4),TRN
                         STRING(@d6b),AT(896,615),USE(L_OG:EffectiveDateAtDate_Rpt),RIGHT(1)
                         STRING('Effective Date:'),AT(104,615),USE(?String62),TRN
                         CHECK(' Effective Rates Only'),AT(6375,615),USE(L_OG:EffectiveRatesOnly_Rpt)
                       END
break_client           BREAK(CLI:CID)
                         HEADER,AT(0,0,,823),PAGEBEFORE(-1),WITHNEXT(1)
                           STRING(@s20),AT(6583,573),USE(LOC:InsuranceRequired)
                           STRING(@d5b),AT(1063,313,677,167),USE(CLI:DateOpened),RIGHT(1)
                           STRING('Insurance:'),AT(6010,573),USE(?String54),TRN
                           STRING(@s20),AT(4250,573,1354,156),USE(LOC:Terms),RIGHT(1),TRN
                           STRING(@n-17.2),AT(4615,313),USE(CLI:AccountLimit),RIGHT(1),TRN
                           STRING('Status:'),AT(6010,313),USE(?String28:2),TRN
                           STRING(@s20),AT(6583,313),USE(LOC:Client_Status)
                           STRING(@n-11.2),AT(1063,573),USE(LOC:VolumetricRatio),RIGHT(1)
                           STRING('Terms:'),AT(4021,573),USE(?String51),TRN
                           STRING(@n-11.2),AT(2896,313),USE(CLI:DocumentCharge),RIGHT(1)
                           STRING('Acc. Limit:'),AT(4021,313),USE(?String41:5),TRN
                           STRING('Branch:'),AT(6010,31),USE(?String28),TRN
                           STRING('Client:'),AT(365,31),USE(?String20),FONT(,10,,FONT:regular,CHARSET:ANSI),TRN
                           STRING(@s100),AT(938,31,4010,208),USE(CLI:ClientName),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@s35),AT(6583,31,1354,167),USE(BRA:BranchName)
                           STRING('Date Opened:'),AT(365,313),USE(?String41),TRN
                           STRING('Docs. Charge:'),AT(2115,313),USE(?String41:3),TRN
                           STRING('Vol. Ratio:'),AT(365,573),USE(?String41:2),TRN
                           STRING(@n_10b),AT(4656,31,1094,208),USE(CLI:ClientNo),FONT(,10,,FONT:bold,CHARSET:ANSI),RIGHT(4), |
  TRN
                         END
detail_cp_head           DETAIL,AT(,,,479),USE(?detail_cp_head)
                           STRING('Container Park Discounts'),AT(365,52,5469,156),USE(?String24),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           STRING('Effective Date'),AT(83,292,1250,156),USE(?HeaderTitle:5),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Floor'),AT(1646,292,1500,170),USE(?HeaderTitle:8),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Discount %'),AT(3844,292,1500,170),USE(?HeaderTitle:10),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Minimium Charge'),AT(5448,292,1500,170),USE(?HeaderTitle:11),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           BOX,AT(0,260,7600,208),USE(?HeaderBox:2),COLOR(COLOR:Black)
                           LINE,AT(0,260,0,220),USE(?DetailcpLine:0),COLOR(COLOR:Black)
                           LINE,AT(1600,260,0,220),USE(?DetailcpLine:1),COLOR(COLOR:Black)
                           LINE,AT(3760,260,0,220),USE(?DetailcpLine:2),COLOR(COLOR:Black)
                           LINE,AT(5385,260,0,220),USE(?DetailcpLine:6),COLOR(COLOR:Black)
                           LINE,AT(7050,260,0,220),USE(?DetailcpLine:4),COLOR(COLOR:Black)
                           LINE,AT(8000,260,0,220),USE(?DetailcpLine:5),COLOR(COLOR:Black)
                         END
detail_cp                DETAIL,AT(,,7979,188),USE(?detail_container_parks)
                           LINE,AT(0,0,0,188),USE(?DetailcpLine:01),COLOR(COLOR:Black)
                           LINE,AT(1600,0,0,188),USE(?DetailcpLine:11),COLOR(COLOR:Black)
                           LINE,AT(3760,0,0,188),USE(?DetailcpLine:21),COLOR(COLOR:Black)
                           LINE,AT(5385,0,0,188),USE(?DetailcpLine:61),COLOR(COLOR:Black)
                           LINE,AT(7050,0,0,188),USE(?DetailcpLine:41),COLOR(COLOR:Black)
                           LINE,AT(8000,0,0,188),USE(?DetailcpLine:51),COLOR(COLOR:Black)
                           STRING(@d5),AT(83,21,1500,167),USE(CLI_CP:Effective_Date),LEFT
                           STRING(@s35),AT(1646,21),USE(FLO:Floor),TRN
                           STRING(@n7.2),AT(3844,21,1500,167),USE(CLI_CP:ContainerParkRateDiscount),RIGHT(1)
                           STRING(@n-13.2),AT(5448,21,1500,167),USE(CLI_CP:ContainerParkMinimium),RIGHT(1)
                           LINE,AT(0,188,8000,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                         END
detail_cp_rates_hdr      DETAIL,AT(,,,490),USE(?detail_cp_rates_hdr)
                           STRING(@s35),AT(2083,52),USE(FLO:Floor,,?FLO:Floor:2),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING('Container Park Rates:'),AT(365,52,1354,156),USE(?String69),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           BOX,AT(0,260,7600,208),USE(?HeaderBox2:2),COLOR(COLOR:Black)
                           LINE,AT(1604,250,0,219),USE(?HeaderLine:12),COLOR(COLOR:Black)
                           LINE,AT(3198,250,0,219),USE(?HeaderLine:22),COLOR(COLOR:Black)
                           LINE,AT(4802,250,0,219),USE(?HeaderLine:32),COLOR(COLOR:Black)
                           LINE,AT(6396,250,0,219),USE(?HeaderLine:42),COLOR(COLOR:Black)
                           STRING('Discounted Rate Per Kg'),AT(4854,271,1500,170),USE(?HeaderTitle:13),FONT(,,,FONT:bold, |
  CHARSET:ANSI),LEFT(1),TRN
                           STRING('Effective Date'),AT(83,271,1250,156),USE(?HeaderTitle:123),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('To Mass'),AT(1646,271,1500,170),USE(?HeaderTitle:23),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Rate Per Kg'),AT(3250,271,1500,170),USE(?HeaderTitle:14),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                         END
detail_cp_rates          DETAIL,AT(,,,177),USE(?detail_cp_rates)
                           LINE,AT(0,0,0,188),USE(?DetailLine:0),COLOR(COLOR:Black)
                           LINE,AT(1600,0,0,188),USE(?DetailLine:1),COLOR(COLOR:Black)
                           LINE,AT(3200,0,0,188),USE(?DetailLine:2),COLOR(COLOR:Black)
                           LINE,AT(4800,0,0,188),USE(?DetailLine:3),COLOR(COLOR:Black)
                           LINE,AT(6400,0,0,188),USE(?DetailLine:4),COLOR(COLOR:Black)
                           LINE,AT(8000,0,0,188),USE(?DetailLine:5),COLOR(COLOR:Black)
                           STRING(@n-12.0),AT(1646,21,1500,170),USE(CPRA:ToMass),RIGHT
                           STRING(@n-13.2),AT(3250,21,1500,170),USE(CPRA:RatePerKg),RIGHT(1)
                           STRING(@n-13.2),AT(4854,21,1500,167),USE(LOC:Discounted_Rate_Per_Kg),RIGHT(1)
                           STRING(@d5),AT(83,21,1250,156),USE(CPRA:Effective_Date),LEFT
                           LINE,AT(0,188,8000,0),USE(?DetailEndLine3),COLOR(COLOR:Black)
                         END
detail_ad_head           DETAIL,AT(,,,479),USE(?detail_ad_head)
                           STRING('Additional Charges'),AT(365,52,5469,156),USE(?String24:2),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           STRING('Description'),AT(1646,292,1500,170),USE(?HeaderTitle:7),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Charge'),AT(5448,292,1500,170),USE(?HeaderTitle:9),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Effective Date'),AT(83,292,1250,156),USE(?HeaderTitle:6),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           BOX,AT(0,260,7600,220),USE(?HeaderBox:3),COLOR(COLOR:Black)
                           LINE,AT(0,260,0,220),USE(?DetailcpLine:02),COLOR(COLOR:Black)
                           LINE,AT(1600,260,0,220),USE(?DetailcpLine:12),COLOR(COLOR:Black)
                           LINE,AT(3760,260,0,220),USE(?DetailcpLine:22),COLOR(COLOR:Black)
                           LINE,AT(5385,260,0,220),USE(?DetailcpLine:62),COLOR(COLOR:Black)
                           LINE,AT(7050,260,0,220),USE(?DetailcpLine:42),COLOR(COLOR:Black)
                           LINE,AT(8000,260,0,220),USE(?DetailcpLine:52),COLOR(COLOR:Black)
                         END
detail_ad                DETAIL,AT(,,,188),USE(?detail_additional)
                           LINE,AT(0,0,0,188),USE(?DetailcpLine:03),COLOR(COLOR:Black)
                           LINE,AT(1600,0,0,188),USE(?DetailcpLine:13),COLOR(COLOR:Black)
                           LINE,AT(3760,0,0,188),USE(?DetailcpLine:23),COLOR(COLOR:Black)
                           LINE,AT(5385,0,0,188),USE(?DetailcpLine:63),COLOR(COLOR:Black)
                           LINE,AT(7050,0,0,188),USE(?DetailcpLine:43),COLOR(COLOR:Black)
                           LINE,AT(8000,0,0,188),USE(?DetailcpLine:53),COLOR(COLOR:Black)
                           STRING(@d5),AT(83,21,1500,167),USE(CARA:Effective_Date),LEFT
                           STRING(@n-15.2),AT(5448,21,1500,167),USE(CARA:Charge),RIGHT(1)
                           STRING(@s35),AT(1646,21),USE(ACCA:Description),TRN
                           LINE,AT(0,188,8000,0),USE(?DetailEndLine2),COLOR(COLOR:Black)
                         END
detail_fs_head           DETAIL,AT(,,,479),USE(?detail_fs_head)
                           STRING('Fuel Surcharge'),AT(365,52,5469,156),USE(?String24:23),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                           STRING('Description'),AT(1646,292,1500,170),USE(?HeaderTitle:73),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Charge'),AT(5448,292,1500,170),USE(?HeaderTitle:93),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Effective Date'),AT(83,292,1250,156),USE(?HeaderTitle:63),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           BOX,AT(0,260,7600,220),USE(?HeaderBox:4),COLOR(COLOR:Black)
                           LINE,AT(0,260,0,220),USE(?DetailcpLine:023),COLOR(COLOR:Black)
                           LINE,AT(1600,260,0,220),USE(?DetailcpLine:123),COLOR(COLOR:Black)
                           LINE,AT(3760,260,0,220),USE(?DetailcpLine:223),COLOR(COLOR:Black)
                           LINE,AT(5385,260,0,220),USE(?DetailcpLine:623),COLOR(COLOR:Black)
                           LINE,AT(7050,260,0,220),USE(?DetailcpLine:423),COLOR(COLOR:Black)
                           LINE,AT(8000,260,0,220),USE(?DetailcpLine:523),COLOR(COLOR:Black)
                         END
detail_fs                DETAIL,AT(,,,188),USE(?detail_fuelsurcharge)
                           LINE,AT(0,0,0,188),USE(?DetailcpLine:033),COLOR(COLOR:Black)
                           LINE,AT(1600,0,0,188),USE(?DetailcpLine:133),COLOR(COLOR:Black)
                           LINE,AT(3760,0,0,188),USE(?DetailcpLine:233),COLOR(COLOR:Black)
                           LINE,AT(5385,0,0,188),USE(?DetailcpLine:633),COLOR(COLOR:Black)
                           LINE,AT(7050,0,0,188),USE(?DetailcpLine:433),COLOR(COLOR:Black)
                           LINE,AT(8000,0,0,188),USE(?DetailcpLine:533),COLOR(COLOR:Black)
                           STRING(@d5),AT(83,21,1500,167),USE(FSRA:Effective_Date),LEFT
                           STRING(@N~% ~-10.2),AT(5448,21,1500,167),USE(FSRA:FuelSurcharge),RIGHT(1)
                           LINE,AT(0,188,8000,0),USE(?DetailEndLine23),COLOR(COLOR:Black)
                         END
detail_genhdr            DETAIL,AT(,,,333),USE(?detail_genhdr)
                           STRING('General Rates'),AT(365,83,5469,156),USE(?String24:3),FONT(,,,FONT:bold+FONT:underline, |
  CHARSET:ANSI),TRN
                         END
detail_genhdrcol         DETAIL,AT(,,,219),USE(?detail_genhdrcol)
                           BOX,AT(0,0,7600,219),USE(?HeaderBox),COLOR(COLOR:Black)
                           LINE,AT(1600,0,0,219),USE(?HeaderLine:1),COLOR(COLOR:Black)
                           LINE,AT(3200,0,0,219),USE(?HeaderLine:2),COLOR(COLOR:Black)
                           LINE,AT(4800,0,0,219),USE(?HeaderLine:3),COLOR(COLOR:Black)
                           LINE,AT(6400,0,0,219),USE(?HeaderLine:4),COLOR(COLOR:Black)
                           STRING('Effective Date'),AT(83,35,1250,156),USE(?HeaderTitle:12),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('To Mass'),AT(1650,35,1500,170),USE(?HeaderTitle:2),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Rate Per Kg'),AT(3250,35,1500,170),USE(?HeaderTitle:3),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Minimium Charge'),AT(4850,35,1500,170),USE(?HeaderTitle:4),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                         END
break_journey            BREAK(RAT:JID)
                           HEADER,AT(0,0,,219),WITHNEXT(1)
                             STRING('Journey'),AT(417,21),USE(?String17),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                             STRING(@s100),AT(1094,21,6771,167),USE(LOC:Journey),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           END
break_clientratetype       BREAK(RAT:CRTID)
                             HEADER,AT(0,0,,698),WITHNEXT(1)
                               STRING('Client Rate Type'),AT(698,42),USE(?String19),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                               STRING(@s80),AT(1813,42,6198,167),USE(CRT:ClientRateType),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                               STRING(@s100),AT(1813,240,5823,167),USE(LOAD2:LoadType),FONT(,,,FONT:bold,CHARSET:ANSI)
                               BOX,AT(0,458,7600,219),USE(?HeaderBox2),COLOR(COLOR:Black)
                               LINE,AT(1600,458,0,219),USE(?HeaderLine:123),COLOR(COLOR:Black)
                               LINE,AT(3200,458,0,219),USE(?HeaderLine:223),COLOR(COLOR:Black)
                               LINE,AT(4800,458,0,219),USE(?HeaderLine:323),COLOR(COLOR:Black)
                               LINE,AT(6400,458,0,219),USE(?HeaderLine:423),COLOR(COLOR:Black)
                               STRING('Load Type'),AT(698,250),USE(?String19:2),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                               STRING('Effective Date'),AT(83,500,1250,156),USE(?HeaderTitle:1234),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                               STRING('To Mass'),AT(1646,500,1500,170),USE(?HeaderTitle:234),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                               STRING('Rate Per Kg'),AT(3250,500,1500,170),USE(?HeaderTitle:33),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                               STRING('Minimium Charge'),AT(4854,500,1500,170),USE(?HeaderTitle:43),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                             END
Detail                       DETAIL,AT(,,,188),USE(?Detail)
                               LINE,AT(0,0,0,188),USE(?DetailLine:03),COLOR(COLOR:Black)
                               LINE,AT(1600,0,0,188),USE(?DetailLine:13),COLOR(COLOR:Black)
                               LINE,AT(3200,0,0,188),USE(?DetailLine:23),COLOR(COLOR:Black)
                               LINE,AT(4800,0,0,188),USE(?DetailLine:33),COLOR(COLOR:Black)
                               LINE,AT(6400,0,0,188),USE(?DetailLine:43),COLOR(COLOR:Black)
                               LINE,AT(8000,0,0,188),USE(?DetailLine:53),COLOR(COLOR:Black)
                               STRING(@n-12.0),AT(1646,21,1500,170),USE(RAT:ToMass),RIGHT
                               STRING(@n-13.2),AT(3250,21,1500,170),USE(RAT:RatePerKg),RIGHT(1)
                               STRING(@n-14.2b),AT(4854,21,1500,170),USE(RAT:MinimiumCharge),RIGHT
                               CHECK(' Ad Hoc'),AT(6979,21),USE(RAT:AdHoc)
                               STRING(@d5),AT(83,21,1250,156),USE(RAT:Effective_Date),LEFT
                               LINE,AT(0,188,8000,0),USE(?DetailEndLine34),COLOR(COLOR:Black)
                             END
                           END
                         END
                         FOOTER,AT(0,0,,208)
                           LINE,AT(875,63,5833,0),USE(?Line39),COLOR(COLOR:Black),LINEWIDTH(15)
                         END
                       END
                       FOOTER,AT(198,10500,,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
BreakMgr             CLASS(BreakManagerClass)              ! Break Manager
TakeEnd                PROCEDURE(SHORT BreakId,SHORT LevelId),DERIVED
                     END

TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar4            CalendarClass
Clients_View            VIEW(Clients_ContainerParkDiscounts)
       JOIN(FLO:PKey_FID, CLI_CP:FID)
       !PROJECT()
    .  .

View_CPs            ViewManager




Add_View            VIEW(__RatesAdditionalCharges)
        JOIN(ACCA:PKey_ACCID, CARA:ACCID)
    .   .

View_Add            ViewManager


FS_View             VIEW(__RatesFuelSurcharge)
    .

View_FS             ViewManager



Rates_View              VIEW(__Rates)
    PROJECT(RAT:AdHoc)
    PROJECT(RAT:CRTID)
    PROJECT(RAT:CTID)
    PROJECT(RAT:Effective_Date)
    PROJECT(RAT:JID)
    PROJECT(RAT:LTID)
    PROJECT(RAT:MinimiumCharge)
    PROJECT(RAT:RatePerKg)
    PROJECT(RAT:ToMass)
       JOIN(CRT:PKey_CRTID,RAT:CRTID)
          PROJECT(CRT:ClientRateType)
          PROJECT(CRT:LTID)
          JOIN(LOAD2:PKey_LTID,CRT:LTID)
            PROJECT(LOAD2:LoadType)
       .  .
       JOIN(JOU:PKey_JID,RAT:JID)
          PROJECT(JOU:Description)
          PROJECT(JOU:Journey)
    .  .

View_Rates              ViewManager
View_Rates_CP       VIEW(__RatesContainerPark)
       JOIN(FLO:PKey_FID, CPRA:FID)
          PROJECT(FLO:Print_Rates)
    .  .

Rates_CP_View       ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CP_Rates                        ROUTINE
    LOC:Printed_CP_Head = FALSE
    CLEAR(LOC:FID)

    View_CPs.Init(Clients_View, Relate:Clients_ContainerParkDiscounts)
    View_CPs.AddSortOrder(CLI_CP:FKey_CID)
    View_CPs.AppendOrder('CLI_CP:FID,-CLI_CP:Effective_Date')
    !View_CPs.AddRange(CLI_CP:CID, LOC:CID)
    View_CPs.SetFilter('CLI_CP:CID = ' & LOC:CID)

    IF L_OG:EffectiveRatesOnly = TRUE
       View_CPs.SetFilter('CLI_CP:Effective_Date < ' & L_OG:EffectiveDateAtDate + 1,'2')
    .

    View_CPs.Reset()
    LOOP
       IF View_CPs.Next() ~= LEVEL:Benign
          BREAK
       .

       ! Only want latest Rate from each Floor
       ! ***************   should implement a queue check like on Validate Record for this and Additionals ??????


       IF L_OG:EffectiveRatesOnly = TRUE
          IF LOC:FID = CLI_CP:FID
             CYCLE
          .
          LOC:FID   = CLI_CP:FID
       .

       ! We want this one
       IF LOC:Printed_CP_Head = FALSE
          PRINT(RPT:detail_cp_head)
          LOC:Printed_CP_Head   = TRUE
       .
       PRINT(RPT:detail_cp)

       L_CPQ:FID                        = CLI_CP:FID
       L_CPQ:ContainerParkRateDiscount  = CLI_CP:ContainerParkRateDiscount
       ADD(L_CPQ:Container_Parks_Discount_Q)

       db.debugout('[Print_Rates]   CP_Rates - Print - RPT:detail_cp')
    .

    View_CPs.Kill()
    EXIT
AD_Rates                        ROUTINE
    LOC:Printed_AD_Head = FALSE
    CLEAR(LOC:ACCID)

    View_Add.Init(Add_View, Relate:__RatesAdditionalCharges)
    ! C6.1 bug???  the following produces a string order by clause for the date!  Check if fixed in 6.2....?????
    !View_Add.AddSortOrder(CARA:CKey_CID_EffDate)
    !View_Add.AppendOrder('CLI:FKey_BID')
    !View_Add.AddRange(CARA:CID, LOC:CID)
    !View_Add.SetFilter('')

    View_Add.AddSortOrder(CARA:FKey_CID)
    View_Add.AppendOrder('ACCA:Description,-CARA:Effective_Date')

    View_Add.SetFilter('CARA:CID = ' & LOC:CID)

    IF L_OG:EffectiveRatesOnly = TRUE
       View_Add.SetFilter('CARA:Effective_Date < ' & L_OG:EffectiveDateAtDate + 1,'2')
    .

    View_Add.Reset()
    LOOP
       IF View_Add.Next() ~= LEVEL:Benign
          BREAK
       .

       ! Only want latest effective additional charge
       IF L_OG:EffectiveRatesOnly = TRUE
          IF CARA:ACCID = LOC:ACCID
             CYCLE
          .
          LOC:ACCID  = CARA:ACCID
       .

       ! We want this one
       IF LOC:Printed_AD_Head = FALSE
          PRINT(RPT:detail_ad_head)
          LOC:Printed_AD_Head = TRUE
       .
       PRINT(RPT:detail_ad)
    .

    View_Add.Kill()
    EXIT
FS_Rates                        ROUTINE
    LOC:Printed_FS_Head = FALSE
    CLEAR(LOC:ACCID)

    View_FS.Init(FS_View, Relate:__RatesFuelSurcharge)

    View_FS.AddSortOrder(FSRA:FKey_CID)
    View_FS.AppendOrder('-FSRA:Effective_Date')

    View_FS.SetFilter('FSRA:CID = ' & LOC:CID)

    IF L_OG:EffectiveRatesOnly = TRUE
       View_FS.SetFilter('FSRA:Effective_Date < ' & L_OG:EffectiveDateAtDate + 1,'2')
    .

    View_FS.Reset()
    LOOP
       IF View_FS.Next() ~= LEVEL:Benign
          BREAK
       .

       ! We want this one
       IF LOC:Printed_FS_Head = FALSE
          PRINT(RPT:detail_fs_head)
          LOC:Printed_FS_Head = TRUE
       .
       PRINT(RPT:detail_fs)

       ! Only want latest effective additional charge
       BREAK
    .

    View_FS.Kill()
    EXIT
Print_Others                     ROUTINE
    IF LOC:CID ~= CLI:CID
       !CLEAR(LOC:Print_Rates)
       !IF RAT:CID = CLI:CID
          !LOC:Print_Rates   = TRUE
       !.

       LOC:CID      = CLI:CID

       LOC:VolumetricRatio  = (1000 / CLI:VolumetricRatio)

       EXECUTE CLI:InsuranceRequired + 1        ! None|Included in Rate|Per DI
          LOC:InsuranceRequired     = 'Excluding GIT'
          LOC:InsuranceRequired     = 'Including GIT'
          LOC:InsuranceRequired     = 'Per DI'
       .

       EXECUTE CLI:Terms + 1                    ! Pre Paid|COD|Account
          LOC:Terms                 = 'Pre Paid'
          LOC:Terms                 = 'COD'
          LOC:Terms                 = 'Account ' & CLI:PaymentPeriod & ' days'
          LOC:Terms                 = 'On Statement'
       .

       IF CLI:Terms = 2 OR CLI:Terms = 3
          IF CLI:Terms = 2 AND CLI:PaymentPeriod = 0
             LOC:Terms              = 'Account 30 days'
          .

          IF LOC:Terms_Bold         = TRUE
             SETTARGET(REPORT)
             ?LOC:Terms{PROP:Font,4} = FONT:Regular
             LOC:Terms_Bold         = FALSE
             SETTARGET()
          .
       ELSE
          SETTARGET(REPORT)
          ?LOC:Terms{PROP:Font,4}   = FONT:Bold
          LOC:Terms_Bold            = TRUE
          SETTARGET()
       .

       DO AD_Rates

       DO FS_Rates

       DO CP_Rates


       IF LOC:Printed_CP_Head = TRUE OR LOC:Printed_AD_Head = TRUE OR LOC:Printed_FS_Head = TRUE        ! Then print the Heading
          PRINT(RPT:detail_genhdr)
       .

       !PRINT(RPT:detail_genhdrcol)
    .
    EXIT
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Client Name' & |
      '|' & 'By Client No.' & |
      '|' & 'By Branch & Client Name' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Rates_2_July_07')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
      L_OG:EffectiveRatesOnly     = GETINI('Print_Rates', 'EffectiveRatesOnly', 1, GLO:Local_INI)
      L_OG:EffectiveDateAtDate    = GETINI('Print_Rates', 'EffectiveDateAtDate', TODAY(), GLO:Local_INI)
  
      L_OG:CreateLog              = GETINI('Print_Rates', 'CreateLog', 0, GLO:Local_INI)
  
  
      IF L_OG:EffectiveDateAtDate <= 1
         L_OG:EffectiveDateAtDate = TODAY()
      .
  
  
  
      L_OG:EffectiveDateAtDate_Rpt    = L_OG:EffectiveDateAtDate
      L_OG:EffectiveRatesOnly_Rpt     = L_OG:EffectiveRatesOnly
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Pause
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:EffectiveDateAtDate',L_OG:EffectiveDateAtDate) ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
      ! When single client, ask about effective date if not today
      IF DEFORMAT(p:CID) ~= 0
         IF L_OG:EffectiveRatesOnly = TRUE AND L_OG:EffectiveDateAtDate ~= TODAY()
            CASE MESSAGE('Only printing effective rates but the Effective date to Print As Of is not today.||' & |
                  'Effective Date for print: ' & FORMAT(L_OG:EffectiveDateAtDate, @d6) & '||' & |
                  'Would you like to use this date or today?||Note you can go and change the effective date for the future' & |
                  ' by going Browse -> Debtors -> Reports -> Rates Report where you will be given an option.', 'Rates Print', ICON:Question, |
                  FORMAT(L_OG:EffectiveDateAtDate,@d6) & '|Today', 2)
            OF 2
               L_OG:EffectiveDateAtDate   = TODAY()
      .  .  .
      ! Ask about Journey or Journey Description
      CASE MESSAGE('Would you like to see the Journey or the Journey Description?', 'Print Rates', ICON:Question,'Journey|Description', 1)
      OF 2
         LOC:Journey_Type = 1
      .
    IF p:CID = 0
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
    .
  Relate:AdditionalCharges.SetOpenRelated()
  Relate:AdditionalCharges.Open                            ! File AdditionalCharges used by this procedure, so make sure it's RelationManager is open
  Access:Clients_ContainerParkDiscounts.UseFile            ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesContainerPark.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesAdditionalCharges.UseFile                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Floors.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__Rates.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ClientsRateTypes.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LoadTypes2.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesFuelSurcharge.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BreakMgr.Init()
  BreakMgr.AddBreak()
  BreakMgr.AddLevel() !ContainerPark_Discounts
  BreakMgr.AddResetField(CLI:CID)
  SELF.AddItem(BreakMgr)
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Rates_2_July_07',ProgressWindow)     ! Restore window settings from non-volatile store
      IF DEFORMAT(p:CID) ~= 0
         CLI:CID            = DEFORMAT(p:CID)
         IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
            ProcessSortSelectionVariable    = 'By Client No.'
      .  .
  
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Clients, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client Name')) THEN
     ThisReport.AppendOrder('+CLI:ClientName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientNo')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch & Client Name')) THEN
     ThisReport.AppendOrder('+BRA:BranchName,+CLI:ClientName')
  END
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = 'Printing Rates...'
  Relate:Clients.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      IF DEFORMAT(p:CID) ~= 0
         !Cid_#  = DEFORMAT(p:CID)
         ThisReport.SetFilter('CLI:CID=' & DEFORMAT(p:CID),'ikb')
      .
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
    IF p:CID = 0
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
    .
  
      IF L_OG:CreateLog = TRUE
         Add_Log('EffectiveRatesOnly: ' & L_OG:EffectiveRatesOnly & ',  Effective Date: ' & FORMAT(L_OG:EffectiveDateAtDate,@d6) & ',  CID: ' & p:CID, 'Rates', 'Print_Rates - ' & FORMAT(TODAY(), @d5-), 1)
         Add_Log('RAT:RID,RAT:CID,RAT:JID,RAT:LTID,RAT:CTID,RAT:CRTID,RAT:ToMass,RAT:RUBID,RAT:Effective_Date,RAT:RatePerKg,RAT:MinimiumCharge', 'Rates', 'Print_Rates - ' & FORMAT(TODAY(), @d5-), 0)
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AdditionalCharges.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Rates_2_July_07',ProgressWindow)  ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Pause
      ThisWindow.Update()
          PUTINI('Print_Rates', 'EffectiveRatesOnly', L_OG:EffectiveRatesOnly, GLO:Local_INI)
          PUTINI('Print_Rates', 'EffectiveDateAtDate', L_OG:EffectiveDateAtDate, GLO:Local_INI)
      
          L_OG:EffectiveDateAtDate_Rpt    = L_OG:EffectiveDateAtDate
          L_OG:EffectiveRatesOnly_Rpt     = L_OG:EffectiveRatesOnly
      
          SELECT(?Tab1)
      
          DISABLE(?Pause)
    OF ?Calendar
      ThisWindow.Update()
      Calendar4.SelectOnClose = True
      Calendar4.Ask('Select a Date',L_OG:EffectiveDateAtDate)
      IF Calendar4.Response = RequestCompleted THEN
      L_OG:EffectiveDateAtDate=Calendar4.SelectedDate
      DISPLAY(?L_OG:EffectiveDateAtDate)
      END
      ThisWindow.Reset(True)
    OF ?Button_Save
      ThisWindow.Update()
          PUTINI('Print_Rates', 'EffectiveRatesOnly', L_OG:EffectiveRatesOnly, GLO:Local_INI)
          PUTINI('Print_Rates', 'EffectiveDateAtDate', L_OG:EffectiveDateAtDate, GLO:Local_INI)
          
          PUTINI('Print_Rates', 'CreateLog', L_OG:CreateLog, GLO:Local_INI)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF p:CID = 0
             SELECT(?Tab_Options)
          .
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      !IF LOC:Print_Rates
      IF LOC:CID_VR ~= CLI:CID
         LOC:CID_VR           = CLI:CID
         LOC:VolumetricRatio  = (1000 / CLI:VolumetricRatio)
  
         EXECUTE CLI:Status + 1
            LOC:Client_Status     = 'Normal'
            LOC:Client_Status     = 'On Hold'
            LOC:Client_Status     = 'Closed'
            LOC:Client_Status     = 'Dormant'
         ELSE
            LOC:Client_Status     = '<Unknown>'
      .  .
  
  
      DO Print_Others
  
      FREE(L_EQ:Effective_Q)
  
      View_Rates.Init(Rates_View, Relate:__Rates)
      View_Rates.AddSortOrder()
      View_Rates.AppendOrder('+RAT:LTID,+RAT:CRTID,+RAT:JID,+RAT:CTID,-RAT:Effective_Date,+RAT:ToMass')
      View_Rates.SetFilter('RAT:CID = CLI:CID AND (0 = L_OG:EffectiveDateAtDate OR RAT:Effective_Date << (L_OG:EffectiveDateAtDate  + 1))')
      View_Rates.Reset()
      LOOP
         IF View_Rates.Next() ~= LEVEL:Benign
            BREAK
         .
  
         ! Check that we have only the Effective rates...
         IF L_OG:EffectiveRatesOnly = TRUE
            L_EQ:LTID        = RAT:LTID
            L_EQ:CRTID       = RAT:CRTID
            L_EQ:JID         = RAT:JID
            L_EQ:CTID        = RAT:CTID
            L_EQ:ToMass      = RAT:ToMass
  
            GET(L_EQ:Effective_Q, L_EQ:LTID, L_EQ:CRTID, L_EQ:JID, L_EQ:CTID, L_EQ:ToMass)
            IF ERRORCODE()
               L_EQ:LTID     = RAT:LTID
               L_EQ:CRTID    = RAT:CRTID
               L_EQ:JID      = RAT:JID
               L_EQ:CTID     = RAT:CTID
               L_EQ:ToMass   = RAT:ToMass
               ADD(L_EQ:Effective_Q, +L_EQ:LTID, +L_EQ:CRTID, +L_EQ:JID, +L_EQ:CTID, +L_EQ:ToMass)
  
               ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt) - global
               IF L_OG:CreateLog = TRUE
                  Add_Log(RAT:RID & ',' & RAT:CID & ',' & RAT:JID & ',' & RAT:LTID & ',' & RAT:CTID & ',' & RAT:CRTID & ',' & RAT:ToMass & ',' & RAT:RUBID & ',' & FORMAT(RAT:Effective_Date,@d6) & ',' & RAT:RatePerKg & ',' & RAT:MinimiumCharge, 'Rates', 'Print_Rates - ' & FORMAT(TODAY(), @d5-), 0)
               .
            ELSE
               CYCLE
         .  .
  
         EXECUTE LOC:Journey_Type + 1
            LOC:Journey     = JOU:Journey
            LOC:Journey     = JOU:Description
         .
  
         PRINT(RPT:Detail)
      .
      View_Rates.Kill()
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:detail_cp_head)
  END
  IF 0
    PRINT(RPT:detail_cp)
  END
  IF 0
    PRINT(RPT:detail_cp_rates_hdr)
  END
  IF 0
    PRINT(RPT:detail_cp_rates)
  END
  IF 0
    PRINT(RPT:detail_ad_head)
  END
  IF 0
    PRINT(RPT:detail_ad)
  END
  IF 0
    PRINT(RPT:detail_fs_head)
  END
  IF 0
    PRINT(RPT:detail_fs)
  END
  IF 0
    PRINT(RPT:detail_genhdr)
  END
  IF 0
    PRINT(RPT:detail_genhdrcol)
  END
  IF LOC:Print_Rates
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


BreakMgr.TakeEnd PROCEDURE(SHORT BreakId,SHORT LevelId)

L:FID_Last          LIKE(FLO:FID)

L:Last_Eff_Date     LIKE(CPRA:Effective_Date)
  CODE
  PARENT.TakeEnd(BreakId,LevelId)
      IF LOC:Printed_CP_Head = TRUE           ! ie if they have container park rates, print the list also
         ! Print Container Park Rates now
         ! Loop through all Floors with discounts
  
         Rates_CP_View.Init(View_Rates_CP, Relate:__RatesContainerPark)
  
         Rates_CP_View.AddSortOrder()    !CPRA:CKey_FID_JID_EffDate_ToMass)
         Rates_CP_View.AppendOrder('CPRA:FID,CPRA:JID,-CPRA:Effective_Date,CPRA:ToMass')
         !Rates_CP_View.AddRange()           - we want all floors one by one
         Rates_CP_View.SetFilter('FLO:Print_Rates = 1 AND (0 = ' & L_OG:EffectiveDateAtDate & ' OR CPRA:Effective_Date << ' & L_OG:EffectiveDateAtDate  + 1 & ')')
                                                                                                   
     !    IF L_OG:EffectiveRatesOnly = TRUE
     !       View_Add.SetFilter('CARA:Effective_Date < ' & L_OG:EffectiveDateAtDate + 1,'2')
     !    .
  
  
  
     !    View_Rates.AppendOrder('+RAT:LTID,+RAT:CRTID,+RAT:JID,+RAT:CTID,-RAT:Effective_Date,+RAT:ToMass')
     !    View_Rates.SetFilter('RAT:CID = CLI:CID AND (0 = L_OG:EffectiveDateAtDate OR RAT:Effective_Date << (L_OG:EffectiveDateAtDate  + 1))')
  
         Rates_CP_View.Reset()
         LOOP
            IF Rates_CP_View.Next() ~= LEVEL:Benign
               BREAK
            .
  
            IF L_OG:EffectiveRatesOnly = TRUE
               ! Check that either our floor has changed or the effective date is the same
               IF L:Last_Eff_Date ~= CPRA:Effective_Date
                  IF L:FID_Last = CPRA:FID
                     CYCLE
            .  .  .
  
            IF L:FID_Last ~= CPRA:FID
               L:FID_Last                = CPRA:FID
               L:Last_Eff_Date           = CPRA:Effective_Date
  
               L_CPQ:FID                 = CLI_CP:FID
               GET(L_CPQ:Container_Parks_Discount_Q, L_CPQ:FID)
  
               ! Heading for next Floor
               PRINT(RPT:detail_cp_rates_hdr)
            .
  
            LOC:Discounted_Rate_Per_Kg   = CPRA:RatePerKg - (CPRA:RatePerKg * (L_CPQ:ContainerParkRateDiscount / 100))
  
            PRINT(RPT:detail_cp_rates)
         .
  
         Rates_CP_View.Kill()
      .


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Rates','Print_Rates','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Manifest_08Jan08 PROCEDURE (p:MID, p:Output, p:SkipPreview)

Progress:Thermometer BYTE                                  ! 
LOC:Contol_Vals      GROUP,PRE(L_CG)                       ! 
DID                  ULONG                                 ! Delivery ID
SkipPreview          BYTE                                  ! 
                     END                                   ! 
LOC:Rep_Fields       GROUP,PRE(L_RF)                       ! 
Weight               DECIMAL(15,2)                         ! In kg's
VolumetricWeight     DECIMAL(15,2)                         ! Weight based on Volumetric calculation (in kgs)
Charge               DECIMAL(17,4)                         ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
State                STRING(20)                            ! State of this manifest
Driver               STRING(50)                            ! First Name
Reg_of_Hours         STRING(20)                            ! 
Leg_Cost             DECIMAL(12,2)                         ! 
                     END                                   ! 
LOC:Delivery_Totals  GROUP,PRE(L_DT)                       ! 
DID                  ULONG                                 ! Delivery ID
DIID                 ULONG                                 ! Delivery Item ID
MLID                 ULONG                                 ! Manifest Load ID
InsuranceCharge      DECIMAL(9,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Insurance            DECIMAL(9,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Docs                 DECIMAL(9,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Fuel                 DECIMAL(9,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Charge               DECIMAL(9,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
                     END                                   ! 
LOC:Manifest_Totals  GROUP,PRE(L_MT)                       ! 
Cost                 DECIMAL(10,2)                         ! 
MID                  ULONG                                 ! Manifest ID - last used
Delivery_Charges     DECIMAL(11,2)                         ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Total_Weight         DECIMAL(10,2)                         ! In kg's
Gross_Profit         DECIMAL(10,2)                         ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Gross_Profit_Percent DECIMAL(6,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Average_C_Per_Kg     DECIMAL(7,2)                          ! Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Legs_Cost            DECIMAL(10,2)                         ! 
Total_Cost           DECIMAL(10,2)                         ! Includes extra legs cost
Out_VAT              DECIMAL(10,2)                         ! 
Legs_Cost_VAT        DECIMAL(10,2)                         ! 
Delivery_Charges_Ex  DECIMAL(11,2)                         ! Charge for the DI- Excludes VAT, Docs, Fuel, Insurance and Additional Charges charges
                     END                                   ! 
Process:View         VIEW(ManifestLoad)
                       PROJECT(MAL:MID)
                       PROJECT(MAL:MLID)
                       PROJECT(MAL:TTID)
                       JOIN(MALD:FSKey_MLID_DIID,MAL:MLID)
                         PROJECT(MALD:UnitsLoaded)
                         PROJECT(MALD:DIID)
                         JOIN(DELI:PKey_DIID,MALD:DIID)
                           PROJECT(DELI:DID)
                           PROJECT(DELI:DIID)
                           PROJECT(DELI:Units)
                           PROJECT(DELI:VolumetricWeight)
                           PROJECT(DELI:Weight)
                           JOIN(DEL:PKey_DID,DELI:DID)
                             PROJECT(DEL:AdditionalCharge)
                             PROJECT(DEL:Charge)
                             PROJECT(DEL:DID)
                             PROJECT(DEL:DINo)
                             PROJECT(DEL:DocumentCharge)
                             PROJECT(DEL:FuelSurcharge)
                             PROJECT(DEL:InsuranceRate)
                             PROJECT(DEL:Insure)
                             PROJECT(DEL:TotalConsignmentValue)
                             PROJECT(DEL:VATRate)
                             PROJECT(DEL:DeliveryAID)
                             PROJECT(DEL:CollectionAID)
                             JOIN(A_ADD:PKey_AID,DEL:DeliveryAID)
                               PROJECT(A_ADD:AddressName)
                               PROJECT(A_ADD:SUID)
                               JOIN(A_SUBU:PKey_SUID,A_ADD:SUID)
                                 PROJECT(A_SUBU:Suburb)
                               END
                             END
                             JOIN(ADD:PKey_AID,DEL:CollectionAID)
                               PROJECT(ADD:AddressName)
                             END
                           END
                         END
                       END
                       JOIN(MAN:PKey_MID,MAL:MID)
                         PROJECT(MAN:Cost)
                         PROJECT(MAN:CreatedDate)
                         PROJECT(MAN:VATRate)
                         PROJECT(MAN:BID)
                         PROJECT(MAN:VCID)
                         PROJECT(MAN:TID)
                         PROJECT(MAN:JID)
                         JOIN(BRA:PKey_BID,MAN:BID)
                           PROJECT(BRA:BranchName)
                         END
                         JOIN(VCO:PKey_VCID,MAN:VCID)
                           PROJECT(VCO:CompositionName)
                         END
                         JOIN(TRA:PKey_TID,MAN:TID)
                           PROJECT(TRA:TransporterName)
                         END
                         JOIN(JOU:PKey_JID,MAN:JID)
                           PROJECT(JOU:Journey)
                         END
                       END
                       JOIN(TRU:PKey_TTID,MAL:TTID)
                         PROJECT(TRU:Registration)
                         PROJECT(TRU:VMMID)
                         JOIN(VMM:PKey_VMMID,TRU:VMMID)
                           PROJECT(VMM:MakeModel)
                         END
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(615,1354,7052,8333),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular,CHARSET:ANSI), |
  THOUS
                       HEADER,AT(615,1000,7052,0),USE(?Header)
                       END
break_manifest         BREAK(MAL:MID)
                         HEADER,AT(0,0,,1990),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI),PAGEBEFORE(-1)
                           STRING('Branch:'),AT(4156,31,552,198),USE(?String31:2),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                           STRING('State:'),AT(4354,490),USE(?String54),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@s20),AT(5396,490,1542,167),USE(L_RF:State),FONT(,,,FONT:bold,CHARSET:ANSI),RIGHT(1)
                           STRING(@s35),AT(4792,31,1302,198),USE(BRA:BranchName),FONT(,10,,,CHARSET:ANSI),LEFT(1)
                           STRING('Journey:'),AT(927,271),USE(?String40:5),FONT(,,,FONT:underline,CHARSET:ANSI),TRN
                           STRING(@s70),AT(1615,260,5260,208),USE(JOU:Journey),FONT(,,,FONT:bold+FONT:underline,CHARSET:ANSI), |
  LEFT(1),TRN
                           STRING(@n-14.2),AT(5917,698,1021,167),USE(L_MT:Legs_Cost),RIGHT(1),TRN
                           STRING(@n-14.2),AT(2094,1094,1021,167),USE(L_MT:Out_VAT),RIGHT(1)
                           STRING('Legs Cost (incl.):'),AT(4365,698),USE(?String40:7),TRN
                           STRING('Total Cost:'),AT(104,896),USE(?String40:6),TRN
                           STRING('Output VAT:'),AT(104,1094),USE(?String81),TRN
                           STRING('Manifest No.:'),AT(1125,31),USE(?String31),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@n_10),AT(2073,31,1021,198),USE(MAL:MID),FONT(,10,,FONT:bold,CHARSET:ANSI),RIGHT(1), |
  TRN
                           STRING('Transporter:'),AT(104,490),USE(?String40),TRN
                           STRING(@s35),AT(1104,490,2010,167),USE(TRA:TransporterName),FONT(,,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),TRN
                           STRING('Gross Profit (GP - excl.):'),AT(4354,1125),USE(?String50),TRN
                           STRING(@n-14.2),AT(2094,896,1021,167),USE(L_MT:Total_Cost),RIGHT(1),TRN
                           STRING('Legs Cost - Out VAT:'),AT(4365,896),USE(?String40:8),TRN
                           STRING(@n-14.2),AT(5917,896,1021,167),USE(L_MT:Legs_Cost_VAT),RIGHT(1),TRN
                           STRING('Cost:'),AT(104,698),USE(?String40:2),TRN
                           STRING(@n-14.2),AT(2094,698,1021,167),USE(L_MT:Cost),RIGHT(1)
                           STRING(@n-14.2),AT(2094,1510,1021,167),USE(L_MT:Total_Weight),RIGHT(1)
                           STRING('Average c./kg:'),AT(4354,1510),USE(?String50:3),TRN
                           STRING(@n-10.2),AT(5917,1510,1021,167),USE(L_MT:Average_C_Per_Kg),RIGHT(1)
                           STRING('Deliveries Total Freight Charge:'),AT(104,1292),USE(?String40:3),TRN
                           STRING(@n-15.2),AT(2094,1292,1021,167),USE(L_MT:Delivery_Charges),RIGHT(1)
                           STRING(@n-14.2),AT(5917,1125,1021,167),USE(L_MT:Gross_Profit),RIGHT(1)
                           STRING('GP %:'),AT(4354,1313),USE(?String50:2),TRN
                           STRING('Total Weight:'),AT(104,1510),USE(?String40:4),TRN
                           STRING(@n-9.2),AT(5917,1313,1021,167),USE(L_MT:Gross_Profit_Percent),RIGHT(1)
                           LINE,AT(21,1688,7000,0),USE(?Line8:4),COLOR(COLOR:Black)
                           STRING(@s35),AT(938,1740,1583,167),USE(VCO:CompositionName),TRN
                           STRING('Driver:'),AT(4917,1740),USE(?String69:2),TRN
                           STRING(@s35),AT(5323,1740,1583,167),USE(L_RF:Driver)
                           LINE,AT(21,1948,7000,0),USE(?Line8:6),COLOR(COLOR:Black)
                           STRING('Reg.:'),AT(3292,1740),USE(?String69:3),TRN
                           STRING(@s20),AT(3646,1740,979,167),USE(L_RF:Reg_of_Hours)
                           STRING('Vehicle Comp.:'),AT(125,1740),USE(?String69),TRN
                         END
break_trans              BREAK(MAL:TTID)
                           HEADER,AT(0,0,,542)
                             STRING('Vehicle Reg.:'),AT(167,63),USE(?String33:2),TRN
                             STRING('Model:'),AT(3042,63),TRN
                             STRING(@s35),AT(3552,63,2500,208),USE(VMM:MakeModel),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                             STRING(@s20),AT(1063,63,1708,208),USE(TRU:Registration),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI)
                             STRING('Act Kg'),AT(2938,313,542,167),USE(?String3:5),CENTER,TRN
                             STRING('Freight'),AT(4104,313,615,167),USE(?String3:7),CENTER,TRN
                             STRING('Suburb'),AT(4844,313,813,167),USE(?String3:10),CENTER,TRN
                             STRING('2nd Transporter'),AT(5677,313,1323,167),USE(?String3:8),CENTER,TRN
                             STRING('Items'),AT(2531,313,406,167),USE(?String3:4),TRN
                             STRING('Vol Kg'),AT(3500,313,542,167),USE(?String3:6),CENTER,TRN
                             STRING('Sender'),AT(427,313,1000,167),USE(?String3:2),CENTER,TRN
                             STRING('Deliver To'),AT(1479,313,1000,167),USE(?String3:3),CENTER,TRN
                             STRING('DI No.'),AT(52,313,323,167),USE(?String3),TRN
                             LINE,AT(396,271,0,229),USE(?Line1),COLOR(COLOR:Black)
                             LINE,AT(1448,271,0,229),USE(?Line1:2),COLOR(COLOR:Black)
                             LINE,AT(2510,271,0,229),USE(?Line1:3),COLOR(COLOR:Black)
                             LINE,AT(2948,271,0,229),USE(?Line1:4),COLOR(COLOR:Black)
                             LINE,AT(3510,271,0,229),USE(?Line1:5),COLOR(COLOR:Black)
                             LINE,AT(4073,271,0,229),USE(?Line1:6),COLOR(COLOR:Black)
                             LINE,AT(4792,271,0,229),USE(?Line1:7),COLOR(COLOR:Black)
                             LINE,AT(5677,271,0,229),USE(?Line1:8),COLOR(COLOR:Black)
                             LINE,AT(21,21,7000,0),USE(?Line8),COLOR(COLOR:Black)
                             LINE,AT(21,271,7000,0),USE(?Line8:2),COLOR(COLOR:Black)
                             LINE,AT(21,500,7000,0),USE(?Line8:22),COLOR(COLOR:Black)
                           END
break_delivery             BREAK(DEL:DINo)
Detail                       DETAIL,AT(,,,0),USE(?Detail),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                               GROUP,AT(31,-52,6948,271),USE(?Group1),BOXED,HIDE,TRN
                                 STRING(@n~L ~6),AT(5167,42,406,167),USE(MALD:UnitsLoaded)
                                 STRING('Not printing detail lines at all'),AT(52,42,885,167),USE(?String1),TRN
                                 STRING(@n~AW~-13.2),AT(1552,42,875,167),USE(DELI:Weight),RIGHT(1)
                                 STRING(@n-11.2),AT(3781,42,677,167),USE(L_RF:VolumetricWeight),RIGHT(1),HIDE
                                 STRING(@n~U ~6),AT(5813,42),USE(DELI:Units)
                                 STRING(@n_10),AT(1042,42,458,167),USE(DELI:DID),RIGHT(1),TRN
                                 STRING(@n~CW~-13.2),AT(2469,42,875,167),USE(L_RF:Weight),RIGHT(1)
                                 STRING(@n10.2),AT(4490,21,646,167),USE(L_RF:Charge),RIGHT(1)
                                 STRING(@n10.2),AT(4490,21,646,167),USE(L_RF:Leg_Cost,,?L_RF:Leg_Cost:3),RIGHT(1)
                               END
                             END
                             FOOTER,AT(0,0,,198),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                               STRING(@s50),AT(4844,10,813,167),USE(A_SUBU:Suburb)
                               STRING(@s35),AT(6271,10,750,167),USE(A_TRA:TransporterName,,?A_TRA:TransporterName:2),FONT(, |
  ,,FONT:regular,CHARSET:ANSI),TRN
                               STRING(@n-13.2),AT(4125,10,646,167),USE(L_RF:Charge,,?L_RF:Charge:3),RIGHT(1),SUM,TALLY(Detail), |
  RESET(break_delivery),TRN
                               STRING(@n13.2),AT(5469,10,771,167),USE(L_RF:Leg_Cost,,?L_RF:Leg_Cost:2),FONT(,,,FONT:regular, |
  CHARSET:ANSI),RIGHT(1),TALLY(Detail),TRN
                               STRING(@s35),AT(427,10,1000,167),USE(ADD:AddressName),TRN
                               STRING(@s35),AT(1479,10,1000,167),USE(A_ADD:AddressName),TRN
                               STRING(@n-11),AT(2938,10,542,167),USE(L_RF:Weight,,?L_RF:Weight:2),RIGHT(1),SUM,TALLY(Detail), |
  RESET(break_delivery),TRN
                               STRING(@n6),AT(2490,10),USE(MALD:UnitsLoaded,,?MALD:UnitsLoaded:2),RIGHT(1),SUM,TALLY(Detail), |
  RESET(break_delivery),TRN
                               STRING(@n-11),AT(3500,10,542,167),USE(L_RF:VolumetricWeight,,?L_RF:VolumetricWeight:2),RIGHT(1), |
  SUM,TALLY(Detail),RESET(break_delivery),TRN
                               STRING(@n_10),AT(-115,10,479,167),USE(DEL:DINo),RIGHT(1)
                               LINE,AT(396,0,0,198),USE(?Line12),COLOR(COLOR:Black)
                               LINE,AT(1448,0,0,198),USE(?Line1:22),COLOR(COLOR:Black)
                               LINE,AT(2510,0,0,198),USE(?Line1:32),COLOR(COLOR:Black)
                               LINE,AT(2948,0,0,198),USE(?Line1:42),COLOR(COLOR:Black)
                               LINE,AT(3510,0,0,198),USE(?Line1:52),COLOR(COLOR:Black)
                               LINE,AT(4073,0,0,198),USE(?Line1:62),COLOR(COLOR:Black)
                               LINE,AT(4792,0,0,198),USE(?Line1:72),COLOR(COLOR:Black)
                               LINE,AT(5677,0,0,198),USE(?Line1:82),COLOR(COLOR:Black)
                             END
detail_Trans                 DETAIL,AT(,,,271),USE(?detail_Trans),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                               LINE,AT(1583,42,4458,0),USE(?Line8:5),COLOR(COLOR:Black)
                               STRING('Leg:'),AT(1635,73,219,167),USE(?String39),TRN
                               STRING(@n3),AT(1938,73,302,167),USE(DELL:Leg),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                               STRING('2nd Transporter:'),AT(2323,73,813,167),USE(?String35),TRN
                               STRING(@s35),AT(3208,73,1573,167),USE(A_TRA:TransporterName),FONT(,,,FONT:regular,CHARSET:ANSI), |
  TRN
                               STRING('Cost:'),AT(4906,73,323,167),USE(?String35:2),TRN
                               STRING(@n13.2),AT(5208,73,771,167),USE(L_RF:Leg_Cost),FONT(,,,FONT:regular,CHARSET:ANSI),RIGHT(1), |
  TRN
                             END
                           END
                         END
                         FOOTER,AT(0,0,,396)
                           LINE,AT(21,115,7000,0),USE(?Line8:3),COLOR(COLOR:Black)
                           STRING('Total Weight:'),AT(2094,156),USE(?String65:2),TRN
                           STRING(@n-11.2),AT(2979,156),USE(L_RF:Weight,,?L_RF:Weight:3),RIGHT(1),SUM,TALLY(Detail),RESET(break_manifest), |
  TRN
                           STRING(@n6),AT(1240,156),USE(MALD:UnitsLoaded,,?MALD:UnitsLoaded:3),RIGHT(1),SUM,TALLY(Detail), |
  RESET(break_manifest),TRN
                           STRING('Total Freight:'),AT(4167,156),USE(?String65:3),TRN
                           STRING(@n-13.2),AT(5063,156),USE(L_RF:Charge,,?L_RF:Charge:2),RIGHT(1),SUM,TALLY(Detail),RESET(break_manifest), |
  TRN
                           STRING('Units:'),AT(844,156),USE(?String65),TRN
                         END
                       END
                       FOOTER,AT(615,9688,7052,302),USE(?Footer),FONT('MS Sans Serif',,COLOR:Black,,CHARSET:ANSI)
                         STRING('Report Date:'),AT(2188,31),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(2990,31),USE(?ReportDateStamp),TRN
                         STRING('Report Time:'),AT(4156,31),USE(?ReportTimePrompt),TRN
                         STRING('<<-- Time Stamp -->'),AT(4958,31),USE(?ReportTimeStamp),TRN
                         STRING(@N3),AT(6729,31),USE(ReportPageNumber)
                         STRING('Created Date:'),AT(156,31),USE(?ReportDatePrompt:2),TRN
                         STRING(@d6),AT(938,31),USE(MAN:CreatedDate)
                         STRING('Page:'),AT(6313,31),USE(?String61),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Manifest_08Jan08')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DeliveryLegs.SetOpenRelated()
  Relate:DeliveryLegs.Open                                 ! File DeliveryLegs used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterAlias.Open                             ! File TransporterAlias used by this procedure, so make sure it's RelationManager is open
  Access:TruckTrailerAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  IF L_CG:SkipPreview = FALSE THEN
     TargetSelector.AddItem(PDFReporter.IReportGenerator)
  END
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:ManifestLoad, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder('+MAL:MID,+MAL:TTID,+DEL:DINo,+DELI:ItemNo')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:ManifestLoad.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      IF p:MID ~= 0
         ThisReport.SetFilter('MAL:MID=' & p:MID, 'ikb')
      .
  
      ! (p:MID, p:Output, p:SkipPreview)
      ! (ULONG=0, BYTE=0, BYTE=0)
  
  
      L_CG:SkipPreview    = p:SkipPreview
  SELF.SkipPreview = L_CG:SkipPreview
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryLegs.Close
    Relate:TransporterAlias.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      IF L_CG:DID ~= DELI:DID
         L_CG:DID    = DELI:DID
  
         CLEAR(DELL:Record, -1)
         DELL:DID    = L_CG:DID         !DELI:DID
         SET(DELL:FKey_DID_Leg, DELL:FKey_DID_Leg)
         LOOP
            IF Access:DeliveryLegs.TryNext() ~= LEVEL:Benign
               BREAK
            .
            IF DELL:DID ~= L_CG:DID     !DELI:DID
               BREAK
            .
     
     !       message('DELL:DID ~= DELI:DID||' & DELL:DID & ' ~= ' & DELI:DID)
            ! First leg is included in line 1
            IF DELL:Leg <= 1
               CYCLE
            .
     
            CLEAR(A_TRA:Record)
            A_TRA:TID    = DELL:TID
            IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) ~= LEVEL:Benign
               CLEAR(A_TRA:Record)
            .
     
            L_RF:Leg_Cost = DELL:Cost * (1 + (DELL:VATRate / 100))
     
            PRINT(RPT:detail_Trans)
      .  .
  
      CLEAR(L_DT:InsuranceCharge)
      IF DEL:Insure = TRUE
         L_DT:InsuranceCharge    = (DEL:InsuranceRate / 100) * DEL:TotalConsignmentValue
      .
  
      CLEAR(L_RF:Leg_Cost)
  
      ! Load the Delivery Legs....
      CLEAR(A_TRA:Record)
      CLEAR(DELL:Record, -1)
      DELL:DID    = DELI:DID
      SET(DELL:FKey_DID_Leg, DELL:FKey_DID_Leg)
      LOOP
         IF Access:DeliveryLegs.TryNext() ~= LEVEL:Benign
            CLEAR(DELL:Record)
            BREAK
         .
         IF DELL:DID ~= DELI:DID
            CLEAR(DELL:Record)
            BREAK
         .
  
  !       message('DELL:DID ~= DELI:DID||' & DELL:DID & ' ~= ' & DELI:DID)
         ! First leg is included in line 1
         IF DELL:Leg > 1
            CLEAR(DELL:Record)
            BREAK
         .
  
         CLEAR(A_TRA:Record)
         A_TRA:TID    = DELL:TID
         IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) ~= LEVEL:Benign
            CLEAR(A_TRA:Record)
         .
  
         L_RF:Leg_Cost = DELL:Cost * (1 + (DELL:VATRate / 100))
  
         BREAK
      .
  
  
  
      A_TRU:TTID              = VCO:TTID0
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Hours    = A_TRU:Registration
      .
  
      DRI:DRID                = MAN:DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RF:Driver          = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
  
  L_RF:Weight = DELI:Weight * (MALD:UnitsLoaded / DELI:Units)
  L_RF:VolumetricWeight = DELI:VolumetricWeight * (MALD:UnitsLoaded / DELI:Units)
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:detail_Trans)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF L_DT:DIID ~= DELI:DIID OR L_DT:MLID ~= MAL:MLID      ! L_DT:DID ~= DEL:DID OR
         L_DT:DID      = DEL:DID
         L_DT:DIID     = DELI:DIID
         L_DT:MLID     = MAL:MLID
  
!         ! Total loaded items from DID on MLID   /   Total Items on DID
!         L_RF:Charge   = (DEL:DocumentCharge + DEL:FuelSurcharge + DEL:Charge + DEL:AdditionalCharge) * |
!                          (Get_ManLoadItems_Info(MAL:MLID, 1, DEL:DID, DELI:DIID) / Get_DelItem_s_Totals(DEL:DID, 3))

         ! Total loaded items from DID on MLID   /   Total Items on DID
         L_RF:Charge   = (DEL:DocumentCharge + DEL:FuelSurcharge + DEL:Charge + DEL:AdditionalCharge) * |
                          (Get_ManLoadItems_Info(MAL:MLID, 1, DEL:DID, DELI:DIID) / Get_DelItem_s_Totals(DEL:DID, 3))

         IF GLO:Testing_Mode = TRUE
       db.debugout('[Print_Manifest]  Docs, fuel, charge, additional: ' & DEL:DocumentCharge & ', ' & DEL:FuelSurcharge & ', ' & DEL:Charge & ', ' & DEL:AdditionalCharge)
       db.debugout('[Print_Manifest]  Man load info: ' & CLIP(Get_ManLoadItems_Info(MAL:MLID, 1, DEL:DID, DELI:DIID)) & ',   Del Items: ' & Get_DelItem_s_Totals(DEL:DID, 3))
       db.debugout('[Print_Manifest]  Total: ' & L_RF:Charge & ' + VAT = ' & L_RF:Charge * (1 + (DEL:VATRate / 100)))
         .

         ! Note: The source for these values is the DI and not the Invoice which can cause discrepancy between reports
         L_RF:Charge   = ROUND(L_RF:Charge, 0.01) + ROUND(L_RF:Charge * (DEL:VATRate / 100), 0.01)                    ! Add VAT

    db.debugout('[Print_Manifest]  DID: ' & DEL:DID & ',  DIID: ' & DELI:DIID & ',  DI No: ' & DEL:DINo & ',   Charge: ' & L_RF:Charge)
      .
  
  
  
      IF L_MT:MID ~= MAL:MID                                                                  ! Once per Manifest
         L_MT:MID                     = MAL:MID

         ! Get_Manifest_Info
         ! (p:MID, p:Option, p:DI_Type, p:Inv_Totals, p:Output, p:Info  )
         ! (ULONG, BYTE    , BYTE=0   , BYTE=0      , BYTE=0  , <STRING>),STRING
         !   1       2           3           4           5           6
         !   p:Option
         !       0.  Charges total
         !       1.  Insurance total
         !       2.  Weight total, for this Manifest!    Uses Units_Loaded
         !       3.  Extra Legs cost
         !       4.  Charges total incl VAT
         !       5.  No. DI's
         !       6.  Extra Legs cost - inc VAT
         !   p:DI_Type
         !       0.  All             (default)
         !       1.  Non-Broking
         !       2.  Broking
         !   p:Inv_Totals
         !       0.  Collect from Delivery info.
         !       1.  Collect from Invoiced info.     (only Manifest invoices)

         L_MT:Delivery_Charges        = Get_Manifest_Info(MAL:MID, 4,,, p:Output)             ! 4 is VAT incl
  
         L_MT:Cost                    = MAN:Cost                                              ! VAT is excluded
  
         L_MT:Legs_Cost               = Get_Manifest_Info(MAL:MID, 6)                         ! VAT is included
         L_MT:Legs_Cost_VAT           = L_MT:Legs_Cost - Get_Manifest_Info(MAL:MID, 3,,, p:Output)   ! excluded (3), so minus ex to get VAT
         
         ! L_MT:Legs_Cost  - Not added on example Manifest D22858
  
         L_MT:Out_VAT                 = L_MT:Cost * (MAN:VATRate / 100)
         L_MT:Total_Cost              = L_MT:Cost + L_MT:Out_VAT                              ! Add VAT
  
         L_MT:Delivery_Charges_Ex     = Get_Manifest_Info(MAL:MID, 0,,, p:Output)             ! 4 is VAT EXCL
  
         L_MT:Gross_Profit            = L_MT:Delivery_Charges_Ex - L_MT:Cost - (L_MT:Legs_Cost - L_MT:Legs_Cost_VAT)
  
         L_MT:Gross_Profit_Percent    = (L_MT:Gross_Profit / L_MT:Delivery_Charges_Ex) * 100  ! %
  
  
         L_MT:Total_Weight            = Get_Manifest_Info(MAL:MID, 2,,, p:Output)
  
         L_MT:Average_C_Per_Kg        = (L_MT:Delivery_Charges_Ex / L_MT:Total_Weight) * 100     ! in cents (not rands)
  
         EXECUTE MAN:State
            L_RF:State  = 'Loaded'
            L_RF:State  = 'On Route'
            L_RF:State  = 'Transferred'
         ELSE
            L_RF:State  = 'Loading'
      .  .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('Manifest','TransIS','Print Manifest','Print Manifest','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Debtor_Age_Analysis_PDF_old PROCEDURE (p:SRID, p:Option, p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:CID, p:Credit_Limit_Option, p:ClientStatus, p:BID)

Progress:Thermometer BYTE                                  ! 
LOC:Locals           GROUP,PRE(LOC)                        ! 
Date                 DATE                                  ! 
Time                 TIME                                  ! 
                     END                                   ! 
LOC:STRID            ULONG                                 ! Statement Run ID
LOC:Statement_Q      QUEUE,PRE(L_SQ)                       ! Statement ID
STID                 ULONG                                 ! Statement ID
CID                  ULONG                                 ! Client ID
                     END                                   ! 
LOC:Info             STRING(35)                            ! 
LOC:Progress_Group   GROUP,PRE(L_PG)                       ! 
Clients              LONG                                  ! 
No_Done              LONG                                  ! 
Win_Txt              CSTRING(50)                           ! 
TimeIn               LONG                                  ! 
                     END                                   ! 
LOC:Grand_Totals     GROUP,PRE(L_GT)                       ! 
Total                DECIMAL(10,2)                         ! Total
Current              DECIMAL(10,2)                         ! Current
Days30               DECIMAL(10,2)                         ! 30 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days90               DECIMAL(10,2)                         ! 90 Days
Number               ULONG                                 ! Number of entries
Current_PerTot       DECIMAL(6,1)                          ! 
Days30_PerTot        DECIMAL(6,1)                          ! Current
Days60_PerTot        DECIMAL(6,1)                          ! Current
Days90_PerTot        DECIMAL(6,1)                          ! Current
                     END                                   ! 
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
LOC:Options_Group    GROUP,PRE(L_OG)                       ! 
Status               BYTE(255)                             ! Normal, On Hold, Closed, Dormant
Report_Status        STRING(20)                            ! 
                     END                                   ! 
Process:View         VIEW(Clients)
                       PROJECT(CLI:AID)
                       PROJECT(CLI:AccountLimit)
                       PROJECT(CLI:CID)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:Status)
                     END
ProgressWindow       WINDOW('Clients Age Analysis'),AT(,,255,156),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       SHEET,AT(4,2,248,135),USE(?Sheet1)
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(24,71,205,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(14,59,227,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(14,87,227,10),USE(?Progress:PctText),CENTER
                         END
                         TAB('Options'),USE(?Tab2),HIDE
                           PROMPT('Client Status:'),AT(32,36),USE(?L_OG:Status:Prompt)
                           LIST,AT(82,36,60,10),USE(L_OG:Status),DROP(15),FROM('All|#255|Normal|#0|On Hold|#1|Clos' & |
  'ed|#2|Dormant|#3'),MSG('Normal, On Hold, Closed, Dormant'),TIP('Normal, On Hold, Clo' & |
  'sed, Dormant')
                         END
                       END
                       BUTTON('Pause'),AT(199,138,,15),USE(?Pause),LEFT,ICON('waok.ico'),DISABLE,FLAT
                       BUTTON('Cancel'),AT(146,138,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Clients Report'),AT(250,844,7823,9656),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7823,563),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Client Age Analysis'),AT(2740,0,,365),USE(?ReportTitle),FONT('Arial',18,,FONT:bold), |
  CENTER
                         STRING('Client Status:'),AT(5417,104),USE(?String36),FONT('Arial',12,,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s20),AT(6396,104),USE(L_OG:Report_Status),FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                         BOX,AT(0,350,7813,208),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(417,350,0,200),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2604,354,0,200),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(3563,350,0,200),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(4427,350,0,200),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(5281,350,0,200),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(6115,350,0,200),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(6969,350,0,200),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         STRING('No.'),AT(156,385,,170),USE(?HeaderTitle:1),TRN
                         STRING('Debtors Name'),AT(479,385,900,170),USE(?HeaderTitle:2),TRN
                         STRING('Limit'),AT(2677,385,833,167),USE(?HeaderTitle:9),CENTER,TRN
                         STRING('90 Days'),AT(3563,385,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('60 Days'),AT(4396,385,833,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('30 Days'),AT(5240,385,833,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Current'),AT(6094,385,833,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Total'),AT(6958,385,833,167),USE(?HeaderTitle:8),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,7823,479),USE(?Detail)
                         LINE,AT(0,0,0,479),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(417,0,0,240),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(2604,0,0,240),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(3563,0,0,240),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(4427,0,0,240),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(5281,0,0,240),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(6115,0,0,240),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(6969,0,0,240),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(7813,0,0,479),USE(?DetailLine:8),COLOR(COLOR:Black)
                         STRING(@s35),AT(469,281,2813,167),USE(LOC:Info),TRN
                         STRING(@n-17),AT(2646,31,885,167),USE(CLI:AccountLimit),RIGHT(1),TRN
                         STRING(@n-14.2),AT(3563,31),USE(L_SI:Days90),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4396,31),USE(L_SI:Days60),RIGHT(1),TRN
                         STRING(@n-14.2),AT(5240,31),USE(L_SI:Days30),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6094,31),USE(L_SI:Current),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6958,31),USE(L_SI:Total),RIGHT(1),TRN
                         STRING(@n_10b),AT(-313,31,,170),USE(CLI:ClientNo),RIGHT,TRN
                         STRING(@s35),AT(469,31,2135,167),USE(CLI:ClientName),LEFT
                         LINE,AT(417,240,7400,0),USE(?DetailEndLine:2),COLOR(COLOR:Black)
                         LINE,AT(0,479,7813,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
detail_totals          DETAIL,AT(,,,625),USE(?detail_totals)
                         STRING(@n-14.2),AT(5240,146,833,167),USE(L_GT:Days30),RIGHT(1)
                         STRING(@n-14.2),AT(3563,146),USE(L_GT:Days90),RIGHT(1)
                         STRING(@n-14.2),AT(4396,146,833,167),USE(L_GT:Days60),RIGHT(1)
                         STRING(@n13),AT(1979,146),USE(L_GT:Number),RIGHT(1)
                         STRING(@n-14.2),AT(6094,146,833,167),USE(L_GT:Current),RIGHT(1)
                         STRING(@n-14.2),AT(6958,146,833,167),USE(L_GT:Total),RIGHT(1)
                         STRING('% of Total:'),AT(1188,375),USE(?String29:2),TRN
                         STRING(@n-9.1),AT(5240,375,833,167),USE(L_GT:Days30_PerTot),RIGHT(1)
                         STRING(@n-9.1),AT(4396,375,833,167),USE(L_GT:Days60_PerTot),RIGHT(1)
                         STRING(@n-9.1),AT(3563,375,833,167),USE(L_GT:Days90_PerTot),RIGHT(1)
                         STRING(@n-9.1),AT(6094,375,833,167),USE(L_GT:Current_PerTot),RIGHT(4)
                         STRING('Debtors Listed:'),AT(1188,146),USE(?String29),TRN
                       END
                       FOOTER,AT(250,10500,7823,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(7021,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

View_State      VIEW(_Statements)
    PROJECT(STA:STID, STA:STRID, STA:CID)
    .

State_View      ViewManager





View_Addr       VIEW(AddressContacts)
    !PROJECT()
    .


Addr_View       ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_Statement_Run              ROUTINE
    SETCURSOR(CURSOR:Wait)
    FREE(LOC:Statement_Q)

    State_View.Init(View_State, Relate:_Statements)
    State_View.AddSortOrder(STA:FKey_STRID)
    !State_View.AppendOrder()
    State_View.AddRange(STA:STRID, LOC:STRID)
    !State_View.SetFilter()

    State_View.Reset()
    LOOP
       IF State_View.Next() ~= LEVEL:Benign
          BREAK
       .

       L_SQ:STID    = STA:STID
       L_SQ:CID     = STA:CID
       ADD(LOC:Statement_Q)
    .
    State_View.Kill()
    SETCURSOR()

    ?Progress:UserString{PROP:TExt} = 'Statements: ' & RECORDS(LOC:Statement_Q)
    EXIT
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Client No.' & |
      '|' & 'By Client Name' & |
      '|' & 'By Branch ID' & |
      '|' & 'By Client ID' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Debtor_Age_Analysis_PDF_old')
      ! (p:SRID, p:Option, p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:CID, p:Credit_Limit_Option, p:ClientStatus)
      ! (ULONG, BYTE, LONG=0, BYTE=0, BYTE=0, BYTE=0, ULONG=0, BYTE=0, BYTE=255)
  
      L_OG:Status     = p:ClientStatus
      EXECUTE L_OG:Status + 1
         L_OG:Report_Status  = 'Normal'
         L_OG:Report_Status  = 'On Hold'
         L_OG:Report_Status  = 'Closed'
         L_OG:Report_Status  = 'Dormant'
      ELSE
         L_OG:Report_Status  = 'All'
      .
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:Status',L_OG:Status)                          ! Added by: Report
      BIND('p:CID', p:CID)
         Addr_View.Init(View_Addr, Relate:AddressContacts)
         Addr_View.AddSortOrder(ADDC:FKey_AID)
         Addr_View.AppendOrder('ADDC:ContactName')
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
    IF p:CID = 0
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
    ELSE
       ! No need for selection when we have only 1 client
       ProcessSortSelectionVariable     = 'By Client No.'
    .
  Relate:AddressContacts.SetOpenRelated()
  Relate:AddressContacts.Open                              ! File AddressContacts used by this procedure, so make sure it's RelationManager is open
  Access:_Statements.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Statement_Runs.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Clients, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientNo')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client Name')) THEN
     ThisReport.AppendOrder('+CLI:ClientName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch ID')) THEN
     ThisReport.AppendOrder('+CLI:BID,+CLI:ClientName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client ID')) THEN
     ThisReport.AppendOrder('+CLI:CID')
  END
  ThisReport.SetFilter('(p:CID = 0 OR p:CID = CLI:CID) AND (L_OG:Status = 255 OR L_OG:Status = CLI:Status)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = 'Processing Age Analysis'
  Relate:Clients.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      ! Show statement info for which the age analysis will be used - offer to run new one
      LOC:Date        = TODAY()
      LOC:Time        = CLOCK()
  
  
  
      IF p:SRID ~= 0
         LOC:STRID    = p:SRID
         DO Load_Statement_Run
      ELSE
         LOC:Date     = p:Date
         LOC:Time     = DEFORMAT('12:00:00',@t4)
      .
  
  
      ! Both of these set Invoice UpToDate status to 0 if there is a new Credit Note or Client Payment that has not
      ! had its UpToDate status set yet.   At the same time that the related Invoies are set to 0 the processed
      ! Invoice or Client Payment is set to UpToDate.  In otherwords this should be quick.
  
      Process_Invoice_StatusUpToDate('0')
      Process_ClientPaymentsAllocation_StatusUpToDate('0')
      
      IF p:OutPut > 0       ! (p:Text, p:Heading, p:Name)
         Add_Log(',Client No.,Invoice No.,Invoice Date,Invoice Total,Credits,Payments', 'Headings', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv', TRUE)
      .
  
  
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
      IF p:CID = 0
         L_PG:Clients     = RECORDS(Clients)
         L_PG:Win_Txt     = ProgressWindow{PROP:Text}
      .
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      UNBIND('p:CID')
         Addr_View.Kill()
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressContacts.Close
  END
      IF p:OutPut > 0
         CASE MESSAGE('Would you like to load the accumulation details now?|Note that if the file does not load automatically you can find it at the location shown below.||File: ' & PATH() & '\Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv', 'Age Analysis', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Pause
      ThisWindow.Update()
          EXECUTE L_OG:Status + 1
             L_OG:Report_Status  = 'Normal'
             L_OG:Report_Status  = 'On Hold'
             L_OG:Report_Status  = 'Closed'
             L_OG:Report_Status  = 'Dormant'
          ELSE
             L_OG:Report_Status  = 'All'
          .
          SELECT(?Tab1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      ! Also see Validation - Section moved there, as needed to filter for new Credit Limit option
  
      IF CLIP(LOC:Info) ~= '<no statement>'
         ! Put contact details in LOC:Info
         Addr_View.AddRange(ADDC:AID, CLI:AID)
         Addr_View.Reset()
         IF Addr_View.Next() = LEVEL:Benign
            LOC:Info      = ADDC:ContactName
      .  .
  
  
      ADD:AID             = CLI:AID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
         IF CLIP(LOC:Info) = ''
            LOC:Info      = CLIP(ADD:PhoneNo)
         ELSE
            LOC:Info      = CLIP(ADD:PhoneNo) & ', ' & LOC:Info
      .  .
  
  
      L_GT:Current_PerTot = L_GT:Current / L_GT:Total * 100
      L_GT:Days30_PerTot  = L_GT:Days30  / L_GT:Total * 100
      L_GT:Days60_PerTot  = L_GT:Days60  / L_GT:Total * 100
      L_GT:Days90_PerTot  = L_GT:Days90  / L_GT:Total * 100
        IF p:Ignore_Zero_Bal = TRUE
           IF (L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current + L_SI:Total) = 0.0
  
              !L_GT:Total = 0.0 AND L_GT:Current = 0.0 AND L_GT:Days30 = 0.0 AND L_GT:Days60 = 0.0 AND L_GT:Days90 = 0.0
              !db.debugout('[Print_Debtors_Age_Analysis]  0.0 on all values')
              
  
              SkipDetails  = TRUE
        .  .
  IF 0
    PRINT(RPT:detail_totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      ! All|Last Statement|None
      IF p:Option = 1
         L_SQ:CID  = CLI:CID
         GET(LOC:Statement_Q, L_SQ:CID)
         IF ERRORCODE()
            ReturnValue       = Record:Filtered
      .  .
  
      IF p:Option < 2
         IF LOC:STRID = 0
            ReturnValue       = LEVEL:Fatal
      .  .
  
  
  
  
      ! This Section is from TakeRecord - needed here for additional option - p:Credit_Limit_Option
      CLEAR(LOC:Info)
  
      ! p:SRID = 0    - not from statement
  
      IF p:SRID = 0
         ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
         L_PG:No_Done    += 1
  
         ProgressWindow{PROP:Text}        = L_PG:Win_Txt & ' - ' & L_PG:No_Done & '/' & L_PG:Clients
         ?Progress:UserString{Prop:Text}  = CLIP(CLI:ClientName)
  
         L_PG:TimeIn                      = CLOCK()
         Junk_#           = Gen_Statement(0, CLI:CID, LOC:Date, LOC:Time,, TRUE, LOC:Statement_Info,, p:MonthEndDay, p:OutPut)
         IF CLOCK() - L_PG:TimeIn > 100       ! 1 second
            db.debugout('[Print_Debtor_Age_Analysis]  CLI:ClientNo: ' & CLI:ClientNo & ',  Gen_Statement > 1 sec: ' & CLOCK() - L_PG:TimeIn)
         .
      ELSE
         ! Get the suitable Statement
         CLEAR(LOC:Info)
         CLEAR(STA:Record)
  
         L_SQ:CID  = CLI:CID
         GET(LOC:Statement_Q, L_SQ:CID)
         IF ~ERRORCODE()
            STA:STID      = L_SQ:STID
            IF Access:_Statements.TryFetch(STA:PKey_STID) = LEVEL:Benign
            ELSE
               LOC:Info   = '<no statement>'
            .
         ELSE
            LOC:Info      = '<no statement>'
         .
  
  
         L_SI:Days90      = STA:Days90
         L_SI:Days60      = STA:Days60
         L_SI:Days30      = STA:Days30
         L_SI:Current     = STA:Current
         L_SI:Total       = STA:Total
      .
  
  
      IF p:Credit_Limit_Option = TRUE
         IF L_SI:Total <= CLI:AccountLimit
            ReturnValue   = Record:Filtered
      .  .
  
  
      IF ReturnValue = Record:OK
         L_GT:Total      += L_SI:Total
         L_GT:Current    += L_SI:Current
         L_GT:Days30     += L_SI:Days30
         L_GT:Days60     += L_SI:Days60
         L_GT:Days90     += L_SI:Days90
         L_GT:Number     += 1
      .
  !    old
  !       only changed this section on the 1st of Dec 06
  !
  !
  !       IF p:SRID = 0                    ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay)
  !          L_GT:Total      += L_SI:Total
  !          L_GT:Current    += L_SI:Current
  !          L_GT:Days30     += L_SI:Days30
  !          L_GT:Days60     += L_SI:Days60
  !          L_GT:Days90     += L_SI:Days90
  !          L_GT:Number     += 1
  !       ELSE
  !          L_GT:Total      += L_SI:Total
  !          L_GT:Current    += L_SI:Current
  !          L_GT:Days30     += L_SI:Days30
  !          L_GT:Days60     += L_SI:Days60
  !          L_GT:Days90     += L_SI:Days90
  !          L_GT:Number     += 1
  !    .  .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Debtor_Age_Analysis','Print_Debtor_Age_Analysis','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

