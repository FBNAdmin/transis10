

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS010.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Process
!!! </summary>
Process_Deliveries PROCEDURE (p:DIDs)

Progress:Thermometer BYTE                                  ! 
LOC:DID              ULONG                                 ! Delivery ID
LOC:Dates            GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
LOC:Manifested       BYTE                                  ! Manifested status - this has maintenance function - Not Manifested, Partially Manifested, Partially Manifested Multiple, Fully Manifested, Fully Manifested Multiple
LOC:Delivered        BYTE                                  ! Delivered Status - this has maintenance function - Not Delivered, Partially Delivered, Delivered, Delivered (manual)
LOC:Updated_M        LONG                                  ! 
LOC:Updated_D        LONG                                  ! 
LOC:Count            LONG                                  ! 
LOC:Total            LONG                                  ! 
Process:View         VIEW(Deliveries)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:Delivered)
                       PROJECT(DEL:Manifested)
                     END
ProgressWindow       WINDOW('Process Deliveries'),AT(,,231,69),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  TIMER(1)
                       PROGRESS,AT(16,15,199,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,231,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,231,10),USE(?Progress:PctText),CENTER
                       PROMPT(''),AT(1,42,230,10),USE(?Prompt_Msg2),CENTER
                       BUTTON('Cancel'),AT(91,52,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_Deliveries')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  ProgressWindow{Prop:MDI} = True                          ! Make progress window an MDI child window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:Deliveries, ?Progress:PctText, Progress:Thermometer, 521)
  ThisProcess.AddSortOrder(DEL:PKey_DID)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
      IF CLIP(p:DIDs) ~= ''
         IF INSTRING(p:DIDs, ',', 1) > 0
            ! ??? this doesnt look right below, no DID field specified to be IN list
            ThisProcess.SetFilter('SQL( IN(' & CLIP(p:DIDs) & ') )', 'ikb')
  
            LOC:Total  = Count_Recs(Deliveries{PROP:Name}, 'DID IN(' & CLIP(p:DIDs) & ')')
         ELSE
            LOC:DID    = DEFORMAT(p:DIDs)
            ThisProcess.AddRange(DEL:DID, LOC:DID)
         .
  
         db.debugout('[Process_Deliveries]  DID: ' & LOC:DID)
      ELSE
         LOOP
            ! (p:From, p:To, p:Option, p:Heading)
            LOC:Dates    = Ask_Date_Range(TODAY()-30,,1,'Select DI Date to Process From')
            IF LO:From_Date = 0
               CASE MESSAGE('You have selected no From Date, this procedure will run through every single DI and ' & |
                      'take a long time if you do not choose a From Date.||Choose a From Date now?',|
                      'Select DI Date to Process From',ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
               OF BUTTON:Yes
                  CYCLE
               OF BUTTON:No
                  BREAK
               OF BUTTON:Cancel
                  ReturnValue = Level:Fatal
                  BREAK
  
               .
               LOC:Total  = Count_Recs(Deliveries{PROP:Name})
            ELSE
               ThisProcess.SetFilter('DEL:DIDate >= ' & LO:From_Date)
  
               ! Returns SQL Date / Time string - (p:Date, p:Time, p:Encompass)
               LOC:Total  = Count_Recs(Deliveries{PROP:Name}, 'DIDateAndTime >= ' & SQL_Get_DateT_G(LO:From_Date,,1))
  
               BREAK
      .  .  .
  
  
      ThisProcess.RecordsToProcess  = LOC:Total
  SELF.DeferWindow = 1
  SELF.WaitCursor = 1
  SEND(Deliveries,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
    Relate:_SQLTemp.Close
      MESSAGE('Complete.||Updated Manifests: ' & LOC:Updated_M & '|Updated Deliveries: ' & LOC:Updated_D, 'Process Deliveries', ICON:Asterisk)
      
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      LOC:Count          += 1
      LOC:Manifested      = DEL:Manifested
      LOC:Delivered       = DEL:Delivered
  
      IF Upd_Delivery_Man_Status(DEL:DID) ~= DEL:Manifested
         LOC:Updated_M   += 1
      .
  
      IF Upd_Delivery_Del_Status(DEL:DID) ~= DEL:Delivered
         LOC:Updated_D   += 1
      .
  
      ?Progress:UserString{PROP:Text} = 'Updated - Man/Del: ' & LOC:Updated_M & '/' & LOC:Updated_D & '      DI: ' & DEL:DINo
      ?Prompt_Msg2{PROP:Text}         = LOC:Count & ' / ' & LOC:Total
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Window
!!! Invoices / Credit Notes / Delivery Notes / Statements
!!! </summary>
Print_Cont PROCEDURE (ULONG p:ID,BYTE p:PrintType,BYTE p:ID_Options,BYTE p:Preview_Option,BYTE p:HideWindow,<*Q_Type_UL_S500 p_Print_Q>,<STRING pOptions>)

LOC:Fields_Q         QUEUE,PRE(L_FQ)                       ! 
FieldName            STRING(50)                            ! Field Name
XPos                 SHORT                                 ! X Position
YPos                 SHORT                                 ! Y Position
Length               SHORT                                 ! 
Alignment            BYTE                                  ! Alignment
PLID                 ULONG                                 ! 
PFID                 ULONG                                 ! 
                     END                                   ! 
LOC:Invoice_Q        QUEUE,PRE(L_IQ)                       ! 
Line                 ULONG                                 ! 
Text                 STRING(500)                           ! 
                     END                                   ! 
LOC:Locals           GROUP,PRE(LOC)                        ! 
EmptyLineChar        STRING('. {19}')                      ! 
Dot_Empty_Lines      USHORT                                ! Print dot on empty lines from this line (0 is off)
Print_Error_Msg      STRING(200)                           ! 
Page_No              LONG(1)                               ! 
Print_without_Preview BYTE                                 ! 
Result               LONG                                  ! 
Invoice_Type         STRING(20)                            ! 
Field                STRING(250)                           ! 
Cancel               BYTE                                  ! 
Items_Started        BYTE                                  ! 
Items_XPos           SHORT                                 ! 
Items_Length         SHORT                                 ! 
Items_Alignment      BYTE                                  ! 
Items_Ended          BYTE                                  ! 
Items_Complete       BYTE                                  ! 
Idx                  ULONG                                 ! 
Port                 STRING(255)                           ! LPT1, LPT2, etc.
IID                  ULONG                                 ! Invoice Number
Complete             LONG                                  ! 
LineResult           LONG                                  ! 
RetryTimes           LONG                                  ! 
RetryDelay           LONG                                  ! 
Report_Option        BYTE(0)                               ! 
PrintType2           BYTE(1)                               ! 
PaperWidth           LONG(216)                             ! 
PaperHeight          LONG(203)                             ! 
FontName             STRING(200)                           ! 
SpecialInstructions  GROUP,PRE()                           ! 
copy                 STRING(256)                           ! 
Line1                STRING(100)                           ! 
Line2                STRING(100)                           ! 
Line3                STRING(100)                           ! 
Line4                STRING(100)                           ! 
                     END                                   ! 
                     END                                   ! 
LOC:Statement_Group  GROUP,PRE(L_STG)                      ! Group of statement specific vars
Date_XPos            ULONG                                 ! 
Date_Length          ULONG                                 ! 
Date_Align           BYTE                                  ! 
InvNo_XPos           ULONG                                 ! 
InvNo_Length         ULONG                                 ! 
InvNo_Align          BYTE                                  ! 
DI_XPos              ULONG                                 ! 
DI_Length            ULONG                                 ! 
DI_Align             BYTE                                  ! 
Debit_XPos           ULONG                                 ! 
Debit_Length         ULONG                                 ! 
Debit_Align          BYTE                                  ! 
Credit_XPos          ULONG                                 ! 
Credit_Length        ULONG                                 ! 
Credit_Align         BYTE                                  ! 
InvNo2_XPos          ULONG                                 ! 
InvNo2_Length        ULONG                                 ! 
InvNo2_Align         BYTE                                  ! 
Amount_XPos          ULONG                                 ! 
Amount_Length        ULONG                                 ! 
Amount_Align         BYTE                                  ! 
                     END                                   ! 
L_Return_Group       GROUP,PRE(L_RG)                       ! 
Suburb               STRING(50)                            ! Suburb
PostalCode           STRING(10)                            ! 
Country              STRING(50)                            ! Country
Province             STRING(35)                            ! Province
City                 STRING(35)                            ! City
Found                BYTE                                  ! 
                     END                                   ! 
LOC:Printer_Props    GROUP,PRE(L_PP)                       ! 
PageLines            SHORT(48)                             ! Lines on this page
PageColumns          SHORT(80)                             ! Columns on a page
PID                  ULONG                                 ! Print Type ID
PrintType            BYTE                                  ! 
                     END                                   ! 
LOC:Omitted_6        LONG                                  ! 
QuickWindow          WINDOW('Print Continuous'),AT(,,420,238),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MAX,MDI,HLP('Print_Invoices_Cont'),SYSTEM,TIMER(100)
                       SHEET,AT(3,3,414,214),USE(?Sheet1)
                         TAB('Print '),USE(?Tab1)
                           LIST,AT(7,19,406,194),USE(?List1),FONT('Courier New',8,,FONT:regular,CHARSET:ANSI),VSCROLL, |
  FORMAT('14R(2)|M~Line~L@n13@1020L(2)|M~Text~@s255@'),FROM(LOC:Invoice_Q)
                         END
                         TAB('Fields'),USE(?Tab2)
                           LIST,AT(6,20,405,182),USE(?List2),VSCROLL,FORMAT('200L(2)|M~Field Name~@s50@20R(2)|M~X ' & |
  'Pos~L@n_5@20R(2)|M~Y Pos~L@n_5@20R(2)|M~Length~L@n_5@12R(2)|M~Alignment~L@n3@'),FROM(LOC:Fields_Q)
                           PROMPT(''),AT(7,205,287,10),USE(?Prompt1),LEFT
                         END
                       END
                       BUTTON('Print 3'),AT(130,222,45,14),USE(?Button_Print_Line:2),HIDE
                       BUTTON('Print 2'),AT(175,222,45,14),USE(?Button_Print_Line),HIDE
                       BUTTON('&Re-Print'),AT(242,222,59,14),USE(?Button_RePrint),LEFT,ICON(ICON:Print),FLAT,HIDE, |
  TIP('Print with option to re-print')
                       CHECK(' &Print subsequent without Preview'),AT(3,226),USE(LOC:Print_without_Preview),HIDE, |
  TIP('Print all subsequent prints without preview')
                       BUTTON('&Print'),AT(308,222,,14),USE(?Button_Print),LEFT,ICON(ICON:Print1),FLAT,TIP('Print and close')
                       BUTTON('&Cancel'),AT(368,222,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

    ! How it all works................
    !   Yeah right.. like I know.  Ref: Celine Dion (of all people), Because you l.......

PL_View         VIEW(PrintLayout)
!    PROJECT(PRIL:PLID, PRIL:PID, PRIL:PFID, PRIL:XPos, PRIL:YPos)
       JOIN(PRIF:PKey_PFID, PRIL:PFID)
       PROJECT(PRIF:FieldName)
    .  .



!MAN_View        VIEW(ManifestLoadDeliveries)
!    PROJECT(MALD:MLID, MALD:DIID)
!       JOIN(MAL:PKey_MLID, MALD:MLID)
!       PROJECT(MAL:MID)
!    .  .
View_Statement      VIEW(_StatementItems)
    .


Statement_View      ViewManager
    MAP
Print_Lines    PROCEDURE
Print_Lines_New             PROCEDURE
    .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:Result)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_Print                   ROUTINE
    LOC:Cancel    = TRUE

    CLEAR(PRI:Record)
    SET(PRI:PKey_PID)
    LOOP
       NEXT(Prints)
       IF ERRORCODE()
          BREAK
       .

       IF p:PrintType = PRI:PrintType
          L_PP:PageLines    = PRI:PageLines
          L_PP:PageColumns  = PRI:PageColumns
          L_PP:PID          = PRI:PID
          L_PP:PrintType    = PRI:PrintType

          LOC:Cancel        = FALSE
          BREAK
    .  .
    EXIT
Load_Field_Q              ROUTINE
    FREE(LOC:Fields_Q)

    PUSHBIND
    BIND('PRIL:PID', PRIL:PID)

    OPEN(PL_View)
    PL_View{PROP:Filter}    = 'PRIL:PID = ' & L_PP:PID
    SET(PL_View)

    LOOP
       NEXT(PL_View)
       IF ERRORCODE()
          BREAK
       .

       L_FQ:FieldName   = PRIF:FieldName
       L_FQ:XPos        = PRIL:XPos
       L_FQ:YPos        = PRIL:YPos
       L_FQ:Length      = PRIL:Length
       L_FQ:Alignment   = PRIL:Alignment

       L_FQ:PLID        = PRIL:PLID
       L_FQ:PFID        = PRIL:PFID
       ADD(LOC:Fields_Q)
    .
    CLOSE(PL_View)
    UNBIND('PRIL:PID')
    POPBIND

    SORT(LOC:Fields_Q, +L_FQ:FieldName)
    ?Prompt1{PROP:Text} = 'Fileds: ' & RECORDS(LOC:Fields_Q)
    EXIT
Check_Fields               ROUTINE
    ! Scan all Field Q items for items that must print on this line.
    ! Load L_IQ:Text with them.

    LOC:Idx     = 0
    LOOP
       LOC:Idx += 1
       GET(LOC:Fields_Q, LOC:Idx)
       IF ERRORCODE()
          BREAK
       .
       IF L_FQ:YPos ~= L_IQ:Line
          CYCLE
       .

       ! Print this on this line then - we dont check for overlap
       CLEAR(LOC:Field)

       EXECUTE L_PP:PrintType             ! Invoices, Credit Notes, Delivery Notes, Statements
          DO Add_Credit
          DO Add_Delivery_Fields
          DO Add_Statement_Fields
       ELSE
          DO Add_Invoice_Fields
       .

       ! Exclude empty fields and 0 line fields
       IF CLIP(LOC:Field) ~= '' AND L_IQ:Line > 0
   db.debugout('[Print_Cont]  Fields: ' & CLIP(L_FQ:FieldName))
          IF L_FQ:XPos <= 0 OR (L_FQ:XPos + L_FQ:Length - 1) < L_FQ:XPos OR (L_FQ:XPos + L_FQ:Length - 1) > SIZE(L_IQ:Text)
             MESSAGE('There is a problem with the field: ' & CLIP(L_FQ:FieldName) & '||XPos: ' & L_FQ:XPos & '|YPos: ' & L_FQ:YPos & '|Length: ' & L_FQ:Length, 'Print_Cont', ICON:Hand)
             IF L_FQ:XPos <= 0
                L_FQ:XPos    = 1
             .
             IF (L_FQ:XPos + L_FQ:Length - 1) < L_FQ:XPos
                L_FQ:Length  = 30
          .  .

          CASE L_FQ:Alignment
          OF 0
             L_IQ:Text[L_FQ:XPos : L_FQ:XPos + L_FQ:Length - 1]  = LEFT(LOC:Field, L_FQ:Length)
          OF 1
             L_IQ:Text[L_FQ:XPos : L_FQ:XPos + L_FQ:Length - 1]  = CENTER(LOC:Field, L_FQ:Length)
          OF 2
             L_IQ:Text[L_FQ:XPos : L_FQ:XPos + L_FQ:Length - 1]  = RIGHT(LOC:Field, L_FQ:Length)
    .  .  .

    EXIT
Populate                     ROUTINE
    ! Populate
    SORT(LOC:Fields_Q, L_FQ:YPos, L_FQ:XPos)

    FREE(LOC:Invoice_Q)
    L_IQ:Line       = 0

    ! Load any zero fields - Note the Items for Statements are loaded here.
    DO Check_Fields

    LOOP            !L_PP:PageLines TIMES
       L_IQ:Line           += 1
       IF L_PP:PageLines < L_IQ:Line             ! We have page break condition
          IF LOC:Items_Complete = TRUE
             BREAK
          .
          LOC:Page_No      += 1

          L_IQ:Line         = 1                 ! Start at line 1 again
          LOC:Items_Started = FALSE
       .

       CLEAR(L_IQ:Text)
       CLEAR(LOC:Field)

       ! For every line, check through the Fields on this line in X Pos order an populate...
       DO Check_Fields

       IF LOC:Items_Started = TRUE
          ! Every line print for Items
          IF LOC:Items_Ended = TRUE             ! On last Item line
             LOC:Items_Started = FALSE
          .

    db.debugout('[Print_Cont - Populate]  Type: ' & L_PP:PrintType & ',   Items')

          IF L_PP:PrintType = 3
             DO Add_Statement_Items
          ELSE
             DO Add_Items
          .

          IF CLIP(LOC:Field) ~= ''
             IF LOC:Items_XPos <= 0 OR (LOC:Items_XPos + LOC:Items_Length - 1) < L_FQ:XPos
                MESSAGE('There is a problem with the Items: ' & CLIP(L_FQ:FieldName) & '||XPos: ' & LOC:Items_XPos & '|YPos: ' & L_FQ:YPos & '|Length: ' & LOC:Items_Length, 'Print_Cont', ICON:Hand)
                IF L_FQ:XPos <= 0
                   L_FQ:XPos    = 1
                .
                IF (L_FQ:XPos + L_FQ:Length - 1) < L_FQ:XPos
                   L_FQ:Length  = 30
             .  .
             CASE LOC:Items_Alignment
             OF 0
                L_IQ:Text[LOC:Items_XPos : LOC:Items_XPos + LOC:Items_Length - 1]  = LEFT(LOC:Field, LOC:Items_Length)
             OF 1
                L_IQ:Text[LOC:Items_XPos : LOC:Items_XPos + LOC:Items_Length - 1]  = CENTER(LOC:Field, LOC:Items_Length)
             OF 2
                L_IQ:Text[LOC:Items_XPos : LOC:Items_XPos + LOC:Items_Length - 1]  = RIGHT(LOC:Field, LOC:Items_Length)
       .  .  .

       IF CLIP(L_IQ:Text) ~= ''
          L_IQ:Text  = SUB(L_IQ:Text, 1, L_PP:PageColumns)            ! Chop off anything beyond page length
       .
       ADD(LOC:Invoice_Q)
    .

    EXIT
! --------------------------------------------------------------------------------------------
Load_Invoice              ROUTINE
    CLEAR(LOC:IID)

    ! Load the Invoice
    IF p:ID_Options = 1         ! We have been passed the DID - we need to get the invoice for it.
       INV:DID          = p:ID
       IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
          LOC:IID       = INV:IID
       ELSE
          ! We cant find the invoice
          MESSAGE('The Invoice cannot be found using the Delivery ID given - DID: ' & p:ID, 'Print_Cont', ICON:Hand)
          LOC:Cancel    = TRUE
          POST(EVENT:CloseWindow)
          p:ID          = 0
       .
    ELSE
       LOC:IID          = p:ID
    .

    IF LOC:Cancel = FALSE
       INV:IID          = LOC:IID
       IF Access:_Invoice.TryFetch(INV:PKey_IID) ~= LEVEL:Benign
          IF p:ID_Options = 1         ! We have been passed the DID
             MESSAGE('The Invoice cannot be found - IID: ' & LOC:IID & '||DID is: ' & p:ID, 'Print_Cont', ICON:Hand)
          ELSE
             MESSAGE('The Invoice cannot be found - IID: ' & LOC:IID, 'Print_Cont', ICON:Hand)
          .
          LOC:Cancel    = TRUE
          POST(EVENT:CloseWindow)
       ELSE
          CLEAR(INI:Record,-1)
          INI:IID       = LOC:IID
          SET(INI:FKey_IID, INI:FKey_IID)


          ! Set Invoice / Copy Invoice
          IF INV:Printed = TRUE
             LOC:Invoice_Type = 'Copy Tax Invoice'
          ELSE
             !LOC:Invoice_Type = 'Tax Invoice'             ! We only print this string if Copy

             INV:Printed      = TRUE
             IF Access:_Invoice.TryUpdate() = LEVEL:Benign
    .  .  .  .
    EXIT

Add_Invoice_Fields    ROUTINE
    CASE CLIP(L_FQ:FieldName)
    OF 'Invoice No.'
       ! Add the Invoice Number
       LOC:Field            = INV:IID
    OF 'Branch'
       LOC:Field            = INV:BranchName                
    OF 'Date'
       LOC:Field            = FORMAT(INV:InvoiceDate,@d5)
    OF 'For Account'
       LOC:Field            = INV:ClientNo & ' ' & INV:ClientName
    OF 'DI No.'
       LOC:Field            = LEFT(FORMAT(INV:DINo, @n_7b))
    OF 'Manifest No.'         
       LOC:Field            = INV:MIDs
    OF 'Shipper'
       LOC:Field            = INV:ShipperName
    OF 'Ship Line 1'
       LOC:Field            = INV:ShipperLine1
    OF 'Ship Line 2'
       IF CLIP(INV:ShipperLine2) = ''
          LOC:Field         = INV:ShipperSuburb
       ELSE
          LOC:Field         = INV:ShipperLine2
       .
    OF 'Ship Suburb'
       IF CLIP(INV:ShipperLine2) = ''
          LOC:Field         = INV:ShipperPostalCode
       ELSE
          IF CLIP(INV:ShipperSuburb) = ''
             LOC:Field      = INV:ShipperPostalCode
          ELSE
             LOC:Field      = INV:ShipperSuburb
       .  .
    OF 'Ship Post'
       IF CLIP(INV:ShipperLine2) = '' OR CLIP(INV:ShipperSuburb) = ''
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ShipperPostalCode
       .
    OF 'Consignee'
       LOC:Field            = INV:ConsigneeName
    OF 'Con Line 1'
       LOC:Field            = INV:ConsigneeLine1
    OF 'Con Line 2'
       IF CLIP(INV:ConsigneeLine2) = ''
          LOC:Field         = INV:ConsigneeSuburb
       ELSE
          LOC:Field         = INV:ConsigneeLine2
       .
    OF 'Con Suburb'
       IF CLIP(INV:ConsigneeLine2) = ''
          LOC:Field         = INV:ConsigneePostalCode
       ELSE
          IF CLIP(INV:ConsigneeSuburb) = ''
             LOC:Field      = INV:ConsigneePostalCode
          ELSE
             LOC:Field      = INV:ConsigneeSuburb
       .  .
    OF 'Con Post'
       IF CLIP(INV:ConsigneeLine2) = '' OR CLIP(INV:ConsigneeSuburb) = ''
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ConsigneePostalCode
       .
    OF 'Items Start'
       LOC:Items_Started    = TRUE
       LOC:Items_Ended      = FALSE

       LOC:Items_XPos       = L_FQ:XPos
       LOC:Items_Length     = L_FQ:Length
       LOC:Items_Alignment  = L_FQ:Alignment
    OF 'Items End'
       LOC:Items_Ended      = TRUE

    OF 'Vol Weight'
       LOC:Field            = 'Vol Wt : ' & FORMAT(INV:VolumetricWeight, @n9.2)
    OF 'Act Weight'
       LOC:Field            = 'Act Wt : ' & FORMAT(INV:Weight, @n9.2)

    OF 'Debt Name'
       LOC:Field            = INV:ClientName            ! Repeated
    OF 'Debt Line 1'
       LOC:Field            = INV:ClientLine1
    OF 'Debt Line 2'
       IF CLIP(INV:ClientLine2) = ''
          LOC:Field         = INV:ClientSuburb
       ELSE
          LOC:Field         = INV:ClientLine2
       .
    OF 'Debt Suburb'
       IF CLIP(INV:ClientLine2) = ''                    ! Suburb already done
          LOC:Field         = INV:ClientPostalCode      ! Do postcode here
       ELSE
          IF CLIP(INV:ClientSuburb) = ''
             LOC:Field      = INV:ClientPostalCode
          ELSE
             LOC:Field      = INV:ClientSuburb
       .  .
    OF 'Debt Post'
       IF CLIP(INV:ClientSuburb) = '' OR CLIP(INV:ClientLine2) = ''     ! Postcode already done
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ClientPostalCode
       .
    OF 'Debt Vat No.'
       LOC:Field            = 'VAT No. ' & INV:VATNo
    OF 'Debt Ref.'
       LOC:Field            = 'Ref. ' & INV:ClientReference
    OF 'Charge Fuel'
       LOC:Field            = FORMAT(INV:FuelSurcharge, @n-12.2)
    OF 'Charge Toll'
       LOC:Field            = FORMAT(INV:TollCharge, @n-12.2)
    OF 'Charge Insurance'
       LOC:Field            = FORMAT(INV:Insurance, @n-12.2)
    OF 'Charge Docs'
       LOC:Field            = FORMAT(INV:Documentation, @n-12.2)
    OF 'Charge Freight'
       LOC:Field            = FORMAT(INV:FreightCharge + INV:AdditionalCharge, @n-12.2)   ! Added 1 Jun 10
    OF 'Charge VAT'
       LOC:Field            = FORMAT(INV:VAT, @n-12.2)
    OF 'Charge Total'
       LOC:Field            = FORMAT(INV:Total, @n-12.2)
    OF 'Copy Invoice'
       LOC:Field            = LOC:Invoice_Type          ! Non invoice field
    OF 'Fuel Surcharge'
       LOC:Field            = 'Fuel Surcharge'
    OF 'XXXXX Tolls'
       LOC:Field            = 'XXXXXXX  E-Toll'
    OF 'Invoice Message'
       LOC:Field            = INV:InvoiceMessage
    .

    EXIT
! --------------------------------------------------------------------------------------------
Add_Items                   ROUTINE
    NEXT(_InvoiceItems)
    IF ERRORCODE() OR INI:IID ~= INV:IID
       LOC:Items_Complete   = TRUE
    ELSE
       IF INI:Type = 2
          LOC:Field         = CLIP(INI:Description)
       ELSE
          LOC:Field         = INI:Units & ' ' & CLIP(INI:Description) & ' of ' & CLIP(INI:Commodity)
       .

        !                L_IQ:Text[LOC:Items_XPos : LOC:Items_XPos + LOC:Items_Length - 1]  = LEFT(LOC:Field, LOC:Items_Length)

       IF CLIP(INI:ContainerDescription) ~= ''
          Len_CD_#          = LEN(CLIP(INI:ContainerDescription))
          Len_F_#           = LEN(CLIP(LOC:Field))

          IF LOC:Items_Length / 2 < Len_CD_#  AND  Len_F_# + Len_CD_# + 2 < LOC:Items_Length
             ! Then right align
             LOC:Field[LOC:Items_Length - Len_CD_# : LOC:Items_Length] = INI:ContainerDescription
          ELSE
             IF Len_F_# > LOC:Items_Length / 2 OR Len_CD_# > LOC:Items_Length / 2        ! Then just add after
                LOC:Field      = CLIP(LOC:Field) & '  ' & INI:ContainerDescription
             ELSE
                LOC:Field[LOC:Items_Length / 2 : LOC:Items_Length] = INI:ContainerDescription
    .  .  .  .
    EXIT
! --------------------------------------------------------------------------------------------
Load_Delivery             ROUTINE
    ! Load the Delivey Note
    DEL:DID         = p:ID
    IF Access:Deliveries.TryFetch(DEL:PKey_DID) ~= LEVEL:Benign
       MESSAGE('The DI cannot be found - ID: ' & p:ID, 'Print_Cont', ICON:Hand)
       LOC:Cancel   = TRUE
       POST(EVENT:CloseWindow)
    ELSE
!       CLEAR(DELI:Record,-1)
!       DELI:DID     = p:ID
!       SET(DELI:FKey_DID_ItemNo, DELI:FKey_DID_ItemNo)
       ! Note: We are going to use the Invoice and Invoice Items for Delivery Note printing

       ! We need the IID - so we set the items file after getting that only...
    .

    IF LOC:Cancel = FALSE
       ! Load the associated invoice
       INV:DID      = DEL:DID
       IF Access:_Invoice.TryFetch(INV:FKey_DID) ~= LEVEL:Benign
          MESSAGE('The DI Invoice cannot be found - DID: ' & p:ID & '||Delivery Note will not be printed.', 'Print_Cont', ICON:Hand)
          LOC:Cancel = TRUE
          CLEAR(INV:Record)
       ELSE
          CLEAR(INI:Record,-1)
          INI:IID    = INV:IID
          SET(INI:FKey_IID, INI:FKey_IID)
    .  .
    EXIT

Add_Delivery_Fields    ROUTINE
    CASE CLIP(L_FQ:FieldName)
    OF 'Invoice No.'
       ! Add the Invoice Number
       IF INV:POD_IID = 0                       ! Old Invoices will be missing PODs - Feb 21 05
          LOC:Field         = INV:IID
       ELSE
          LOC:Field         = INV:POD_IID
       .
    OF 'Branch'
       LOC:Field            = INV:BranchName                
    OF 'Date'
       LOC:Field            = FORMAT(INV:InvoiceDate,@d5)
    OF 'For Account'
       LOC:Field            = INV:ClientNo              ! INV:ClientName
    OF 'DI No.'
       LOC:Field            = INV:DINo
    OF 'Manifest No.'         
       LOC:Field            = INV:MIDs
    OF 'Shipper'
       LOC:Field            = INV:ShipperName
    OF 'Ship Line 1'
       LOC:Field            = INV:ShipperLine1
    OF 'Ship Line 2'
       IF CLIP(INV:ShipperLine2) = ''
          LOC:Field         = INV:ShipperSuburb
       ELSE
          LOC:Field         = INV:ShipperLine2
       .
    OF 'Ship Suburb'
       IF CLIP(INV:ShipperLine2) = ''
          LOC:Field         = INV:ShipperPostalCode
       ELSE
          IF CLIP(INV:ShipperSuburb) = ''
             LOC:Field      = INV:ShipperPostalCode
          ELSE
             LOC:Field      = INV:ShipperSuburb
       .  .
    OF 'Ship Post'
       IF CLIP(INV:ShipperLine2) = '' OR CLIP(INV:ShipperSuburb) = ''
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ShipperPostalCode
       .
    OF 'Consignee'
       LOC:Field            = INV:ConsigneeName
    OF 'Con Line 1'
       LOC:Field            = INV:ConsigneeLine1
    OF 'Con Line 2'
       IF CLIP(INV:ConsigneeLine2) = ''
          LOC:Field         = INV:ConsigneeSuburb
       ELSE
          LOC:Field         = INV:ConsigneeLine2
       .
    OF 'Con Suburb'
       IF CLIP(INV:ConsigneeLine2) = ''
          LOC:Field         = INV:ConsigneePostalCode
       ELSE
          IF CLIP(INV:ConsigneeSuburb) = ''
             LOC:Field      = INV:ConsigneePostalCode
          ELSE
             LOC:Field      = INV:ConsigneeSuburb
       .  .
    OF 'Con Post'
       IF CLIP(INV:ConsigneeLine2) = '' OR CLIP(INV:ConsigneeSuburb) = ''
          LOC:Field         = ''
       ELSE
          LOC:Field         = INV:ConsigneePostalCode
       .

    OF 'Items Start'
       LOC:Items_Started    = TRUE
       LOC:Items_Ended      = FALSE

       LOC:Items_XPos       = L_FQ:XPos
       LOC:Items_Length     = L_FQ:Length
       LOC:Items_Alignment  = L_FQ:Alignment
    OF 'Items End'
       LOC:Items_Ended      = TRUE
    OF 'Vol Weight'
       IF INV:VolumetricWeight ~= 0.0
          LOC:Field         = 'Vol Wt : ' & FORMAT(INV:VolumetricWeight, @n9.2)
       .
    OF 'Act Weight'
       LOC:Field            = 'Act Wt : ' & FORMAT(INV:Weight, @n9.2)
    OF 'Spec. Inst. 1'
       ! 4 lines potentially or always?  assuming always
       ! Text may have carriage returns in it, we need to put those on separate lines if so
       ! Unless the lines are too long, in which case revert to system of just putting them in the space provided
       
       ! First, split by CR/LF
       !    if any one line is longer than L_FQ:Length 1 then remove CRLFs, add space and split on position
       !    else use lines from 1 to 4
       LOC:SpecialInstructions.copy = DEL:SpecialDeliveryInstructions
       
       LOC:SpecialInstructions.Line1 = Get_1st_Element_From_Delim_Str(LOC:SpecialInstructions.copy, '<13,10>',1)
       LOC:SpecialInstructions.Line2 = Get_1st_Element_From_Delim_Str(LOC:SpecialInstructions.copy, '<13,10>',1)
       LOC:SpecialInstructions.Line3 = Get_1st_Element_From_Delim_Str(LOC:SpecialInstructions.copy, '<13,10>',1)
       LOC:SpecialInstructions.Line4 = LOC:SpecialInstructions.copy
       
       IF LEN(CLIP(LOC:SpecialInstructions.Line1)) > 36 OR |
                  LEN(CLIP(LOC:SpecialInstructions.Line2)) > 36 OR |
                  LEN(CLIP(LOC:SpecialInstructions.Line3)) > 36 OR |
                  LEN(CLIP(LOC:SpecialInstructions.Line4)) > 36 
                  
          LOC:SpecialInstructions.copy = DEL:SpecialDeliveryInstructions
          Replace_Strings(LOC:SpecialInstructions.copy, '<13,10>', ' ')
          
          LOC:SpecialInstructions.Line1 = SUB(LOC:SpecialInstructions.copy, 1, 36)
          LOC:SpecialInstructions.Line2 = SUB(LOC:SpecialInstructions.copy, 36+1, 36)
          LOC:SpecialInstructions.Line3 = SUB(LOC:SpecialInstructions.copy, 2*36+1, 36)
          LOC:SpecialInstructions.Line4 = SUB(LOC:SpecialInstructions.copy, 3*36+1, 36)
       .
    
       LOC:Field            = LOC:SpecialInstructions.Line1
    OF 'Spec. Inst. 2'
       LOC:Field            = LOC:SpecialInstructions.Line2
    OF 'Spec. Inst. 3'
       LOC:Field            = LOC:SpecialInstructions.Line3
    OF 'Spec. Inst. 4'
       LOC:Field            = LOC:SpecialInstructions.Line4
    OF 'Debt Ref.'
       LOC:Field            = 'Ref. ' & INV:ClientReference
    .

    EXIT
! --------------------------------------------------------------------------------------------
Load_Statements             ROUTINE
    ! Load the Statement
    STA:STID        = p:ID
    IF Access:_Statements.TryFetch(STA:PKey_STID) ~= LEVEL:Benign
       MESSAGE('The Statement cannot be found - STID: ' & p:ID, 'Print_Cont', ICON:Hand)
       LOC:Cancel   = TRUE
       POST(EVENT:CloseWindow)
    ELSE
       ! Load the Client Record
       CLI:CID  = STA:CID
       IF Access:Clients.TryFetch(CLI:PKey_CID) ~= LEVEL:Benign
          MESSAGE('The Statement client cannot be found - CID: ' & STA:CID & '||Statement will not be printed.', 'Print_Cont', ICON:Hand)
          LOC:Cancel   = TRUE
          POST(EVENT:CloseWindow)
          CLEAR(CLI:Record)
       ELSE
          ! Load Clients address
          ADD:AID       = CLI:AID
          IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
             ! Load Suburb details
             L_Return_Group = Get_Suburb_Details(ADD:SUID)
       .  .

       Statement_View.Init(View_Statement, Relate:_StatementItems)
       Statement_View.AddSortOrder( STAI:FKey_STID )
       Statement_View.AppendOrder( 'STAI:STIID' )
       Statement_View.AddRange( STAI:STID, p:ID )
       !Statement_View.SetFilter( '' )

       !Statement_View.Reset(1)
       Statement_View.Reset()

!       CLEAR(STAI:Record,-1)
!       STAI:STID    = p:ID
!       SET(STAI:FKey_STID, STAI:FKey_STID)
    .
    EXIT

Add_Statement_Fields     ROUTINE
    CASE CLIP(L_FQ:FieldName)
    OF 'Statement No.'
       LOC:Field            = STA:STID
    OF 'FBN Name'               ! F.B.N. Transport'
       LOC:Field            = 'F.B.N. Transport'
    OF 'FBN Line 1'             ! P.O.Box 1405'
       LOC:Field            = 'P.O.Box 1405'
    OF 'FBN Line 2'             ! Suburb
       LOC:Field            = 'Hillcrest'
    OF 'FBN Line 3'             ! Postal Code
       LOC:Field            = '3650'
    OF 'VAT No.'
       LOC:Field            = '4360117727'
    OF 'Page No.'
       LOC:Field            = 'Page No.'
    OF 'Page No. Val'
       LOC:Field            = LOC:Page_No
    OF 'FBN Phone'              ! : 031 205 1705'
       LOC:Field            = '+27 (0)31 205 1705'
    OF 'FBN Fax'                ! Fax: 031 205 2098'
       LOC:Field            = '+27 (0)31 205 2098'
    OF 'Client Name Left'
       LOC:Field            = CLI:ClientName
    OF 'Client Line 1'
       LOC:Field            = ADD:Line1
    OF 'Client Line 2'
       LOC:Field            = ADD:Line2
    OF 'Suburb'
       LOC:Field            = L_RG:Suburb
    OF 'Postal'
       LOC:Field            = L_RG:PostalCode
    OF 'Client VAT No.'
       LOC:Field            = CLI:VATNo
    OF 'Client Name Right'
       LOC:Field            = CLI:ClientName
    OF 'Client No.'
       LOC:Field            = CLI:ClientNo
    OF 'Please Post To'
       LOC:Field            = 'Please Post To'
    OF 'FBN 2 Name'               ! F.B.N. Transport'
       LOC:Field            = 'F.B.N. Transport'
    OF 'FBN 2 Line 1'             ! P.O.Box 1405'
       LOC:Field            = 'P.O.Box 1405'
    OF 'FBN 2 Line 2'             ! Suburb
       LOC:Field            = 'Hillcrest'
    OF 'FBN 2 Line 3'             ! Postal Code
       LOC:Field            = '3650'
    OF 'FBN Line 4'             ! Croft & Johnstone Rd.'
       LOC:Field            = 'Croft & Johnstone Rd.'
    OF 'FBN Line 5'             ! 4057'
       LOC:Field            = '4057'
    OF 'Statement for Period Ending:'
       LOC:Field            = 'Statement for Period Ending:'
    OF 'Statement Date'
       LOC:Field            = FORMAT(STA:StatementDate, @d5)
    OF 'Print Date'
       LOC:Field            = 'Date: ' & FORMAT(TODAY(), @d5)
    OF '90 Days'
       LOC:Field            = '90 Days'
    OF '60 Days'
       LOC:Field            = '60 Days'
    OF '30 Days'
       LOC:Field            = '30 Days'
    OF 'Current'
       LOC:Field            = 'Current'
    OF 'Total'
       LOC:Field            = 'Total'
    OF '90 Days Val.'
       LOC:Field            = FORMAT(STA:Days90, @n-11.2)
    OF '60 Days Val.'
       LOC:Field            = FORMAT(STA:Days60, @n-11.2)
    OF '30 Days Val.'
       LOC:Field            = FORMAT(STA:Days30, @n-11.2)
    OF 'Current Val.'
       LOC:Field            = FORMAT(STA:Current, @n-11.2)
    OF 'Total Val.'
       LOC:Field            = FORMAT(STA:Total, @n-11.2)
    OF 'Total Due'
       LOC:Field            = 'Total Due'
    OF 'Total Due Val'
       LOC:Field            = FORMAT(STA:Total, @n-11.2)
    OF 'Last Period Payments'
       LOC:Field            = 'Last Period Payments'
    OF 'Last Period Payments Val'
       LOC:Field            = FORMAT(STA:Paid, @n-11.2)
    OF 'Amount Paid'
       LOC:Field            = 'Amount Paid'
    OF 'Amount Paid Val'      
       LOC:Field            = ''                    ! Customer fills in on remitance
    OF 'Comment Line'
       LOC:Field            = '**ToDo**'            ! Where from?  Provide fields on Statement run???
    OF 'Comment Line 2'
       LOC:Field            = '**ToDo**'


    ! Note: For the statement items we just record the details in the local vars.  These will be used
    !       once we reach the Start Items tag, THIS MEANS THAT THE Ypos for these entries must be
    !       above the Start Items YPos!!!
    OF 'Invoice Date'
       L_STG:Date_XPos      = L_FQ:XPos
       L_STG:Date_Length    = L_FQ:Length
       L_STG:Date_Align     = L_FQ:Alignment
    OF 'Invoice No.'
       L_STG:InvNo_XPos     = L_FQ:XPos
       L_STG:InvNo_Length   = L_FQ:Length
       L_STG:InvNo_Align    = L_FQ:Alignment
    OF 'DI No'
       L_STG:DI_XPos        = L_FQ:XPos
       L_STG:DI_Length      = L_FQ:Length
       L_STG:DI_Align       = L_FQ:Alignment
    OF 'Debit'
       L_STG:Debit_XPos     = L_FQ:XPos
       L_STG:Debit_Length   = L_FQ:Length
       L_STG:Debit_Align    = L_FQ:Alignment
    OF 'Credit'
       L_STG:Credit_XPos    = L_FQ:XPos
       L_STG:Credit_Length  = L_FQ:Length
       L_STG:Credit_Align   = L_FQ:Alignment
    OF 'Invoice No. 2'          ! Required????
       L_STG:InvNo2_XPos    = L_FQ:XPos
       L_STG:InvNo2_Length  = L_FQ:Length
       L_STG:InvNo2_Align   = L_FQ:Alignment
    OF 'Amount'
       L_STG:Amount_XPos    = L_FQ:XPos
       L_STG:Amount_Length  = L_FQ:Length
       L_STG:Amount_Align   = L_FQ:Alignment

    OF 'Items Start'
       LOC:Items_Started    = TRUE
       LOC:Items_Ended      = FALSE

       LOC:Items_XPos       = L_FQ:XPos
       LOC:Items_Length     = L_FQ:Length
       LOC:Items_Alignment  = L_FQ:Alignment

    OF 'Inv Date Label'
       LOC:Field            = 'Date'
    OF 'Inv No. Label'
       LOC:Field            = 'Inv. No.'
    OF 'DI No Label'
       LOC:Field            = 'DI No.'
    OF 'Debit Label'
       LOC:Field            = 'Debit'
    OF 'Credit Label'
       LOC:Field            = 'Credit'
    OF 'Inv No. 2 Label'
       LOC:Field            = 'Inv. No.'
    OF 'Amount Label'
       LOC:Field            = 'Amount'

    OF 'Items End'
       LOC:Items_Ended      = TRUE
    .

    EXIT
Add_Statement_Items      ROUTINE
    DATA
R:Item      LONG
R:Xpos      LONG
R:Align     LONG
R:Length    LONG
R:Field     LIKE(LOC:Field)

    CODE

    IF Statement_View.Next() ~= LEVEL:Benign
       LOC:Items_Complete   = TRUE
    ELSE
       ! Place the fields according to the layout

       R:Item               = 0
       LOOP
          R:Item           += 1
          CASE R:Item
          OF 1
             R:Field          = FORMAT(STAI:InvoiceDate,@d5)
             R:XPos           = L_STG:Date_XPos
             R:Length         = L_STG:Date_Length
             R:Align          = L_STG:Date_Align
          OF 2
             R:Field          = STAI:IID
             R:XPos           = L_STG:InvNo_XPos
             R:Length         = L_STG:InvNo_Length
             R:Align          = L_STG:InvNo_Align
          OF 3
             R:Field          = STAI:DINo
             R:XPos           = L_STG:DI_XPos
             R:Length         = L_STG:DI_Length
             R:Align          = L_STG:DI_Align
          OF 4
             R:Field          = FORMAT(STAI:Debit,@n-12.2)
             R:XPos           = L_STG:Debit_XPos
             R:Length         = L_STG:Debit_Length
             R:Align          = L_STG:Debit_Align
          OF 5
             R:Field          = FORMAT(STAI:Credit,@n-12.2)
             R:XPos           = L_STG:Credit_XPos
             R:Length         = L_STG:Credit_Length
             R:Align          = L_STG:Credit_Align
          OF 6
             R:Field          = STAI:IID
             R:XPos           = L_STG:InvNo2_XPos
             R:Length         = L_STG:InvNo2_Length
             R:Align          = L_STG:InvNo2_Align
          OF 7
             R:Field          = FORMAT(STAI:Amount,@n-12.2)
             R:XPos           = L_STG:Amount_XPos
             R:Length         = L_STG:Amount_Length
             R:Align          = L_STG:Amount_Align
          ELSE
             BREAK
          .

          IF R:Xpos <= 0 OR (R:Xpos + LOC:Items_Length - 1) < R:Xpos
             MESSAGE('There is a problem with the Items (' & R:Item & '): ' & CLIP(L_FQ:FieldName) & '||XPos: ' & R:Xpos & '|Length: ' & R:Length, 'Print_Cont', ICON:Hand)
             IF R:XPos <= 0
                R:XPos    = 1
             .
             IF (R:XPos + R:Length - 1) < R:XPos
                R:Length  = 30
          .  .
          CASE R:Align
          OF 0
             L_IQ:Text[R:Xpos : R:Xpos + R:Length - 1]  = LEFT(R:Field, R:Length)
          OF 1
             L_IQ:Text[R:Xpos : R:Xpos + R:Length - 1]  = CENTER(R:Field, R:Length)
          OF 2
             L_IQ:Text[R:Xpos : R:Xpos + R:Length - 1]  = RIGHT(R:Field, R:Length)
    .  .  .
    EXIT
! --------------------------------------------------------------------------------------------
Load_Credit                     ROUTINE
    LOC:Items_Complete  = TRUE                          ! No items

    ! Load the Invoice
    IF LOC:Cancel = FALSE
       INV:IID          = p:ID
       IF Access:_Invoice.TryFetch(INV:PKey_IID) ~= LEVEL:Benign
          MESSAGE('The Credit Note cannot be found - IID: ' & p:ID, 'Print_Cont', ICON:Hand)
          LOC:Cancel    = TRUE
          POST(EVENT:CloseWindow)
       ELSE
          ! Alls well
    .  . 
    EXIT


Add_Credit                  ROUTINE                 ! No code yet
    CASE CLIP(L_FQ:FieldName)
    OF 'Credit Note Text'
       IF INV:Printed = TRUE
          LOC:Field         = 'Copy Credit Note'
       ELSE
          LOC:Field         = 'Credit Note'

          INV:Printed       = TRUE
          IF Access:_Invoice.TryUpdate() = LEVEL:Benign
       .  .
    OF 'Delete Text'
       LOC:Field            = 'XXXXXXXXXXXXXXXXX'


    OF 'Credit Note No.'
       ! Add the Invoice Number
       LOC:Field            = INV:IID
    OF 'Branch'
       LOC:Field            = INV:BranchName                
    OF 'Date'
       LOC:Field            = FORMAT(INV:InvoiceDate,@d5)
    OF 'For Account'
       LOC:Field            = INV:ClientName
    !OF 'DI No.'
    !   LOC:Field            = INV:DINo

    OF 'Debt Credit'
       LOC:Field            = 'Debtor Credited'

    OF 'Debt Name'
       LOC:Field            = INV:ClientName            ! Repeated
    OF 'Debt Line 1'
       LOC:Field            = INV:ClientLine1
    OF 'Debt Line 2'
       LOC:Field            = INV:ClientLine2
    OF 'Debt Suburb'
       LOC:Field            = INV:ClientSuburb
    OF 'Debt Post'
       LOC:Field            = INV:ClientPostalCode
    OF 'Debt Vat No.'
       LOC:Field            = 'VAT No. ' & INV:VATNo

    OF 'Reason'
       LOC:Field            = 'Reason for Credit Note'
    OF 'Invoice Details'
       LOC:Field            = 'Cr Inv ' & INV:CR_IID

    OF 'Reason Line 1'
       LOC:Field            = INV:InvoiceMessage

    OF 'Reason Line 3'
       !LOC:Field            = 'Total cost of Original Invoice: ' & INV:ShipperName

    OF 'Original Invoice'
       LOC:Field            = 'Total cost of Original Invoice: ' & INV:ShipperLine1

    OF 'Credit Amt.'
       LOC:Field            = FORMAT(INV:FreightCharge, @n12.2)
    OF 'Credit VAT'
       LOC:Field            = FORMAT(INV:VAT, @n12.2)
    OF 'Credit Total'
       LOC:Field            = FORMAT(INV:Total, @n12.2)
    .
    EXIT
! --------------------------------------------------------------------------------------------
Load_Q                           ROUTINE
    ! (p:ID , p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow, <Q_Type_UL_S500 p_Print_Q>)
    ! Q_Type_UL_S500      QUEUE,TYPE
    ! UL1_        ULONG
    ! S500        STRING(500)
    !                     .

    ! Expect 1st record of passed Q to have max columns... ??
    GET(p_Print_Q, 1)
    IF ERRORCODE()
    .
    L_PP:PageLines      = RECORDS(p_Print_Q)
    L_PP:PageColumns    = LEN(CLIP(p_Print_Q.S500))

    L_PP:PrintType      = p:PrintType

    FREE(LOC:Invoice_Q)
    L_IQ:Line           = 0

    LOOP
       L_IQ:Line       += 1
       !IF L_PP:PageLines < L_IQ:Line             ! We have page break condition
       !   LOC:Page_No  += 1
       !   L_IQ:Line     = 1                      ! Start at line 1 again
       !.
       GET(p_Print_Q, L_IQ:Line)
       IF ERRORCODE()
          BREAK
       .

       L_IQ:Text        = CLIP(p_Print_Q.S500)

       ADD(LOC:Invoice_Q)
    .
    EXIT
! --------------------------------------------------------------------------------------------
Print_Now                   ROUTINE
    DATA
R:Opt       BYTE
R:Ini       CSTRING(35)

    CODE
    ! We only have a device option for the Invoices at this stage
    R:Opt               = GETINI('Printer_Devices', 'Invoice_Opt', '', GLO:Local_INI)
!    IF L_PP:PrintType = 0 OR PrinterType                ! Only for Invoices
       IF R:Opt = 0
          R:Ini         = 'Printer_Ports'
       ELSE
          R:Ini         = 'Printer_Devices'
       .
!    ELSE
!       R:Ini            = 'Printer_Ports'
!    .


    EXECUTE L_PP:PrintType + 1           ! Invoices, Credit Notes, Delivery Notes, Statements
       LOC:Port         = GETINI(R:Ini, 'Invoice', 'LPT1', GLO:Local_INI)
       LOC:Port         = GETINI(R:Ini, 'Credit_Note', 'LPT1', GLO:Local_INI)
       LOC:Port         = GETINI(R:Ini, 'Delivery_Note', 'LPT1', GLO:Local_INI)
       LOC:Port         = GETINI(R:Ini, 'Statement', 'LPT1', GLO:Local_INI)
    ELSE
       LOC:Port         = GETINI(R:Ini, 'Invoice', 'LPT1', GLO:Local_INI)   ! Arbitrarily use this type if not set
    .


    IF R:Opt = 0                            ! Line print
       LOC:Idx     = 0
       QuickWindow{PROP:Timer}      = 10
    ELSE                                    ! Device print
       db.Debugout('Printing - here - to device: ' & CLIP(LOC:Port))   
       PRINTER{PROPPRINT:Device}    = CLIP(LOC:Port)
         
       IF LOC:PrintType2 = 0
            Print_Lines_New()
       ELSE 
            Print_Lines()
       .
         
       DO Print_Now_Done
    .
    EXIT
Print_Now_Done        ROUTINE
    IF LOC:Print_without_Preview = 1
       LOC:Result   = 2
    .
    IF p:Preview_Option = 2
       PUTINI('Print_Cont', 'Print_without_Preview', LOC:Print_without_Preview, GLO:Local_INI)
    .

    POST(EVENT:CloseWindow)
    EXIT
! --------------------------------------------------------------------------------------------
Check_Omitted        ROUTINE
   DATA
Test_Options    GROUP,PRE(L_TO)
PrintType         BYTE
PrintOption       BYTE
PaperWidth        LONG(216)
PaperHeight       LONG(203)
FontName             STRING(200)
EmptyLineChar     STRING(20)
                  END

R:Ini       CSTRING(100)
R:Opt       BYTE

   CODE   
   ! (ULONG p:ID,BYTE p:PrintType,BYTE p:ID_Options,BYTE p:Preview_Option,BYTE p:HideWindow,<*Q_Type_UL_S500 p_Print_Q>,<STRING pOptions>)
   !     1        2                 3                 4                       5                       6                       7
   
   LOC:Omitted_6   = OMITTED(6)
      
   IF OMITTED(7) = FALSE
      Test_Options = pOptions
      
      LOC:Report_Option = Test_Options.PrintOption
      LOC:PrintType2    = Test_Options.PrintType
      LOC:FontName      = Test_Options.FontName
      LOC:PaperHeight   = Test_Options.PaperHeight
      LOC:PaperWidth    = Test_Options.PaperWidth
      LOC:EmptyLineChar = Test_Options.EmptyLineChar
   ELSE
      R:Opt               = GETINI('Printer_Devices', 'Invoice_Opt', '', GLO:Local_INI)
      IF R:Opt = 0
         R:Ini         = 'Printer_Ports'
      ELSE
         R:Ini         = 'Printer_Devices'
      .
      
      LOC:PrintType2  = GETINI(R:Ini, 'PrintType2', '1', GLO:Local_INI)  
      LOC:Report_Option = GETINI(R:Ini, 'Report_Option', '2', GLO:Local_INI)  
      LOC:EmptyLineChar = GETINI(R:Ini, 'EmptyLineChar', '.', GLO:Local_INI)  
      
      ! Set the default saved to ini if not there already
      IF (GETINI(R:Ini, 'PrintType2', '-', GLO:Local_INI)  = '-') 
         PUTINI(R:Ini, 'PrintType2', '1', GLO:Local_INI)  
      .
      IF (GETINI(R:Ini, 'Report_Option', '-', GLO:Local_INI) = '-')
         PUTINI(R:Ini, 'Report_Option', '2', GLO:Local_INI)  
      .      
      IF (GETINI(R:Ini, 'EmptyLineChar', '.', GLO:Local_INI)  = '.') 
         PUTINI(R:Ini, 'EmptyLineChar', '.', GLO:Local_INI)  
      .      
   .
   EXIT
   

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Cont')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
      DO Check_Omitted
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  Relate:PrintFields.SetOpenRelated()
  Relate:PrintFields.Open                                  ! File PrintFields used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Prints.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PrintLayout.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoadDeliveries.UseFile                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoad.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Statements.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_StatementItems.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
      IF p:HideWindow = 1
         QuickWindow{PROP:Hide}   = TRUE
      .
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface)                        ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Print_Cont',QuickWindow)                   ! Restore window settings from non-volatile store
      LOC:Dot_Empty_Lines     = GETINI('Print_Cont', 'Dot_Empty_Lines', 45, GLO:Local_INI)
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
      QuickWindow{PROP:Timer}     = 0
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
    Relate:PrintFields.Close
      Statement_View.Kill()
  END
  IF SELF.Opened
    INIMgr.Update('Print_Cont',QuickWindow)                ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button_Print_Line
          ! Save temp file for printing....
          Over_#  = TRUE
      
          SETCURSOR(CURSOR:WAIT)
          LOC:Idx = 0
          LOOP
             LOC:Idx  += 1
             GET(LOC:Invoice_Q, LOC:Idx)
             IF ERRORCODE()
                BREAK
             .
      
             
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             ! p:Name_Opt
             !   0.  - None
             !   1.  - Set static name to p:Name
             !   2.  - Use static name
      
             Add_Log(CLIP(L_IQ:Text), '', 'Temp_Rpt.txt', Over_#, 1, 0, 0)
             Over_#   = FALSE
          .
          SETCURSOR()
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Print_Line:2
      ThisWindow.Update()
          Print_Lines()
    OF ?Button_Print_Line
      ThisWindow.Update()
      Line_Print('Temp_Rpt.txt')
      ThisWindow.Reset
    OF ?Button_Print
      ThisWindow.Update()
          DO Print_Now
      
      
    OF ?Cancel
      ThisWindow.Update()
          LOC:Result      = -1
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          ! (p:ID , p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow, <Q_Type_UL_S500 p_Print_Q>)
          ! (ULONG, BYTE       , BYTE=0      , BYTE=1          , BYTE=0      , <Q_Type_UL_S500 p_Print_Q>), LONG, PROC
          !   1           2           3               4               5                   6
          ! p:PrintType         -     0 = Invoices
          !                           1 = Credit Notes
          !                           2 = Delivery Notes
          !                           3 = Statements
          !                           42 = Passed Q
          !
          ! p:ID_Options        -     used for Invoices, if 1 then DID is passed not IID
          !
          ! p:Preview_Option    -     0 = none
          !                           1 = preview (standard)
          !                           2 = ask preview all - when this mode return value is next preview response from user
          !                               This only occurs on first call - although callers responsibility
          !
          ! p:HideWindow              Use in conjunction with p:Preview_Option = 0
          !
          ! LOC:Result
          !
          ! Q_Type_UL_S500      QUEUE,TYPE
          ! UL1_        ULONG
          ! S500        STRING(500)
      
          IF p:PrintType = 42 AND LOC:Omitted_6
             LOC:Cancel   = TRUE
             MESSAGE('Print type is 42 but no print Q has been passed.', 'Print_Cont', ICON:Hand)
          .
      
          IF LOC:Cancel = FALSE AND p:PrintType ~= 42
             DO Load_Print
          .
      
          IF LOC:Cancel = TRUE
             MESSAGE('The requested print type was not found.', 'Print Cont.', ICON:Exclamation)
          ELSE
             EXECUTE p:PrintType + 1         ! Invoices, Credit Notes, Delivery Notes, Statements
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Invoice'
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Credit Note'
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Delivery Note'
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - Statement'
             ELSE
                QuickWindow{PROP:Text}   = CLIP(QuickWindow{PROP:Text}) & ' - ' & p:PrintType & ' <Unknown>'
          .  .
      !    db.debugout('Print_Cont - p:Type: ' & p:PrintType & ',  Loaded Type: ' & p:PrintType)
      
          IF LOC:Cancel = FALSE
             EXECUTE p:PrintType + 1         ! Invoices, Credit Notes, Delivery Notes, Statements
                DO Load_Invoice
                DO Load_Credit
                DO Load_Delivery
                DO Load_Statements
             ELSE
                DO Load_Q
          .  .
      
          IF LOC:Cancel = FALSE AND p:PrintType ~= 42
             DO Load_Field_Q
      
             DO Populate
          .
      
          IF LOC:Cancel = FALSE
             IF p:Preview_Option <= 0
                POST(EVENT:Accepted, ?Button_Print)
             ELSIF p:Preview_Option = 2
                UNHIDE(?LOC:Print_without_Preview)
                LOC:Print_without_Preview        = GETINI('Print_Cont', 'Print_without_Preview', '', GLO:Local_INI)
             .
          ELSE
             POST(EVENT:CloseWindow)
          .
    OF EVENT:Timer
          QuickWindow{PROP:Timer} = 0
      
          IF LOC:RetryDelay > 0
             IF ABS(CLOCK() - LOC:RetryDelay) > 150
                LOC:RetryDelay    = 0
             .
          ELSE
             LOC:Idx += 1
             GET(LOC:Invoice_Q, LOC:Idx)
             IF ERRORCODE()
                LOC:Complete = TRUE
          .  .
      
          IF LOC:Complete = FALSE
             IF LOC:RetryDelay <= 0
                !  0    Function Succeeded
                !  1    Device or File Open Error
                !  2    Device or File Write Error
                !  3    Device or file Close Error
                !  4    File Seek Error
      
                LOC:LineResult   = LinePrint(CLIP(L_IQ:Text), SUB(LOC:Port,1,4))
      
                IF LOC:LineResult = 0
                   LOC:RetryTimes    = 3
                   LOC:RetryDelay    = 0
                ELSE
                   IF LOC:RetryTimes > 0
                      LOC:RetryTimes -= 1
                      LOC:RetryDelay  = CLOCK()
                   ELSE
                      CASE LOC:LineResult
                      OF 1
                         LOC:Print_Error_Msg  = 'Device / File Open Error'
                      OF 2
                         LOC:Print_Error_Msg  = 'Device / File Write Error'
                      OF 3
                         LOC:Print_Error_Msg  = 'Device / File Close Error'
                      OF 4
                         LOC:Print_Error_Msg  = 'File Seek Error'
                      .
                      CASE MESSAGE('There was a problem printing a line.||Error: ' & CLIP(LOC:Print_Error_Msg) & '||Cancel all printing?', 'Print_Cont', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
                      OF BUTTON:Yes
                         LOC:Result = -1
                      .
      
                      LOC:Complete  = TRUE
          .  .  .  .
      
          IF LOC:Complete = FALSE
             QuickWindow{PROP:Timer} = 10
          ELSE
             DO Print_Now_Done
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Print_Lines                 PROCEDURE()

YieldGrain      EQUATE(10)

CurrentLine     STRING(255),AUTO
ProgressValue   LONG
LineCount       LONG
!PrevQ           PreviewQueue
!Previewer       &PrintPreviewClass

window WINDOW('Printing'),AT(,,219,33),FONT('MS Sans Serif',8,,FONT:regular),CENTER,GRAY,DOUBLE
       PROGRESS,USE(ProgressValue),AT(4,17,212,14),RANGE(0,100)
       STRING('100%'),AT(200,4),USE(?String3),RIGHT
       STRING('0%'),AT(4,4),USE(?String2),LEFT
       STRING('Page Complete'),AT(85,4),USE(?String1),CENTER
     END

Report REPORT,AT(0,0,8000,8698),PRE(RPT),FONT('Arial',10,,),THOUS
Detail DETAIL,AT(,,8000,177)
         STRING(@s100),AT(0,0,8000,208),USE(CurrentLine)
       END
     END

   CODE
      db.Debugout('--- using Print_Lines ORIGINAL')
      
  OPEN(Window)
  ?ProgressValue{PROP:RangeHigh}        = RECORDS(LOC:Invoice_Q)
  OPEN(Report)
  !Report{PROP:Preview}                  = PrevQ.Filename
  !IF SELF.PrintPreview
  !  Previewer &= NEW PrintPreviewClass
  !  Previewer.Init(PrevQ)
  !END
  LOC:Idx     = 0
  LOOP
     LOC:Idx += 1
     GET(LOC:Invoice_Q, LOC:Idx)
     IF ERRORCODE()
        BREAK
     .

     IF CLIP(L_IQ:Text) = '' AND LOC:Idx > LOC:Dot_Empty_Lines    ! Only do for last few lines (was 45)
        L_IQ:Text   = '.'
     .

     CurrentLine    = SUB(L_IQ:Text, 1, L_PP:PageColumns)

     PRINT(RPT:Detail)

     ProgressValue   += 1
     DISPLAY(?ProgressValue)
     IF ~(ProgressValue%YieldGrain) THEN YIELD().
  .

  !CurrentLine    = '.'
  PRINT(RPT:Detail)

  ENDPAGE(Report)
  !Report{PROP:FlushPreview}             = CHOOSE(SELF.PrintPreview=False,True,Previewer.Display())
  CLOSE(Report)

  CLOSE(Window)

  !IF SELF.PrintPreview
  !  Previewer.Kill
  !  DISPOSE(Previewer)
  !.
  RETURN

Print_Lines_New                 PROCEDURE()

YieldGrain      EQUATE(10)

CurrentLine     STRING(255),AUTO
ProgressValue   LONG
LineCount       LONG

ThisPageLine  LONG

window WINDOW('Printing'),AT(,,219,33),FONT('MS Sans Serif',8,,FONT:regular),CENTER,GRAY,DOUBLE
       PROGRESS,USE(ProgressValue),AT(4,17,212,14),RANGE(0,100)
       STRING('100%'),AT(200,4),USE(?String3),RIGHT
       STRING('0%'),AT(4,4),USE(?String2),LEFT
       STRING('Page Complete'),AT(85,4),USE(?String1),CENTER
     END

! This size below is based on _Invoice_ measurements given:  width 216 (240 with perf), length 203
Report    REPORT,AT(0,0, 216, 203),PRE(RPT),PAPER(PAPER:USER, 216, 203),MM
Detail    DETAIL,AT(0,0,200,4),USE(?DETAIL1)
                  STRING(@s100),AT(0,0,200,4),USE(CurrentLine)
               END
            END

Report2 REPORT,AT(0,0),PRE(RPT2), MM
Detail DETAIL,AT(0,0,200,4),USE(?DETAIL1)
   STRING(@s80),AT(0,0,200,4),USE(CurrentLine)
   END
DetailPage                 DETAIL,AT(0,0,210,3),USE(?DETAILPAGE),PAGEAFTER(1)
                                       END
         END

Report3 REPORT,AT(0,0),PRE(RPT3), MM
Detail DETAIL,AT(0,0,200,4),USE(?DETAIL1)
   STRING(@s80),AT(0,0,200,4),USE(CurrentLine)
   END
         END

Report4                             REPORT,AT(0,0),PRE(RPT4),THOUS
Detail                                 DETAIL,AT(0,0,8000,200),USE(?DETAIL1)
                                          STRING(@s80),AT(0,0,7990,187),USE(CurrentLine)
                                       END
                                    END

CODE
   ! LOC:PaperWidth is in mm, to get to 1000/inch 
   ! 1 inch = 25.4mm, 1000 / 25.4 = 1mm = 39.37 1000ths
   LOC:PaperWidth = LOC:PaperWidth * 39.37 
   LOC:PaperHeight = LOC:PaperHeight * 39.37
   
   db.Debugout('--- using Print_Lines_New    Report_Option: ' & LOC:Report_Option & '    Lines: ' & RECORDS(LOC:Invoice_Q))
   
  OPEN(Window)
  ?ProgressValue{PROP:RangeHigh}        = RECORDS(LOC:Invoice_Q)
   
   EXECUTE LOC:Report_Option
      OPEN(Report)
      OPEN(Report2)
      BEGIN         
         db.Debugout('USER PAPER - width: ' & LOC:PaperWidth & '   Height: ' & LOC:PaperHeight & '    LOC:FontName: ' & CLIP(LOC:FontName))
         
         SYSTEM{PROP:AutoPaper} = '' 
         OPEN(Report3)
         SETTARGET(Report3)
         Report3{PROPPRINT:PAPER} = PAPER:USER
         Report3{PROPPRINT:PAPERWIDTH} = LOC:PaperWidth
         Report3{PROPPRINT:PAPERHEIGHT} = LOC:PaperHeight
         IF CLIP(LOC:FontName) <> ''
            Report3{PROP:FontName} = LOC:FontName
         .         
         SETTARGET()
      END      
      BEGIN         
         db.Debugout('USER PAPER 4 - width: ' & LOC:PaperWidth & '   Height: ' & LOC:PaperHeight & '    LOC:FontName: "' & CLIP(LOC:FontName) & '"')
         IF LOC:PaperWidth + LOC:PaperHeight > 0
            SYSTEM{PROP:AutoPaper} = '' 
         .
         
         OPEN(Report4)
         SETTARGET(Report4)
         IF LOC:PaperWidth + LOC:PaperHeight > 0
            Report4{PROPPRINT:PAPER} = PAPER:USER
            Report4{PROPPRINT:PAPERWIDTH} = LOC:PaperWidth
            Report4{PROPPRINT:PAPERHEIGHT} = LOC:PaperHeight
         .         
         IF CLIP(LOC:FontName) <> ''
            Report4{PROP:FontName} = LOC:FontName
         .         
         SETTARGET()
      END      
   .
   
   db.Debugout('---     SYSTEM PROP:AutoPaper:  "' & SYSTEM{PROP:AutoPaper} & '"      L_PP:PageLines: ' & L_PP:PageLines)

   LOC:Idx     = 0
   ThisPageLine = 0
  LOOP
     LOC:Idx += 1
     ThisPageLine += 1
     GET(LOC:Invoice_Q, LOC:Idx)
     IF ERRORCODE()
        BREAK
     .

     IF CLIP(L_IQ:Text) = '' AND LOC:Idx >= LOC:Dot_Empty_Lines    ! Only do for last few lines (was 45)
        L_IQ:Text   = CLIP(LOC:EmptyLineChar)
     .

     CurrentLine    = SUB(L_IQ:Text, 1, L_PP:PageColumns)

      EXECUTE LOC:Report_Option
        PRINT(RPT:Detail)
        PRINT(RPT2:Detail)
        PRINT(RPT3:Detail)
        PRINT(RPT4:Detail)
      .
      
      db.Debugout('Printed Line ' & LOC:Idx & ': ' & CurrentLine)
      
      IF LOC:Report_Option = 2 AND L_PP:PageLines <= ThisPageLine
         ! End of this page
         PRINT(RPT2:DetailPage)
         
         db.Debugout('     Page break printed @ ' & ThisPageLine & '  of ' & L_PP:PageLines)
         
         ThisPageLine = 0
      .      

     ProgressValue   += 1
     DISPLAY(?ProgressValue)
     IF ~(ProgressValue%YieldGrain) THEN YIELD().
  .

  !CurrentLine    = '.'
  !PRINT(RPT:Detail) - 26.01.15

  EXECUTE LOC:Report_Option
     CLOSE(Report)
     CLOSE(Report2)
     CLOSE(Report3)
     CLOSE(Report4)
  .
   
  CLOSE(Window)
  RETURN

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Prompt1, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Prompt1

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Print_DN_POD_Cont PROCEDURE (p:MID)

LOC:Complete         BYTE                                  ! 
LOC:Pause            BYTE                                  ! 
LOC:State            STRING(100)                           ! 
LOC:Info             STRING(255)                           ! 
LOC:ChangeTime       LONG                                  ! 
LOC:No_Printed       ULONG                                 ! 
LOC:Result           LONG                                  ! 
LOC:SQL_Str          STRING(2000)                          ! 
LOC:Total_Invoices   LONG                                  ! 
LOC:Inv_No           LONG                                  ! 
LOC:Page_Print_Time  SHORT                                 ! In seconds
LOC:Print_Now        BYTE                                  ! 
LOC:TimeOut          LONG                                  ! 
LOC:Last_Print_Time  LONG                                  ! 
LOC:DID_Q            QUEUE,PRE(L_DQ)                       ! 
DID                  ULONG                                 ! Delivery ID
                     END                                   ! 
LOC:Idx              ULONG                                 ! 
LOC:Preview_Option   BYTE                                  ! 
LOC:Print_Result     LONG                                  ! 
QuickWindow          WINDOW('Print Delivery Notes'),AT(,,225,152),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,IMM,MDI,HLP('Print_DN_POD_Cont'),SYSTEM,TIMER(50)
                       SHEET,AT(4,4,217,130),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           STRING(@s100),AT(18,22,189,10),USE(LOC:State),FONT(,,,FONT:bold,CHARSET:ANSI),CENTER,TRN
                           PROMPT(''),AT(18,36,189,56),USE(?Prompt1),LEFT,TRN
                           LINE,AT(10,97,205,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Page Print Time (seconds):'),AT(18,103),USE(?LOC:Page_Print_Time:Prompt),TRN
                           SPIN(@n5),AT(114,103,48,9),USE(LOC:Page_Print_Time),RIGHT(1),MSG('In seconds'),SKIP,TIP('In seconds')
                           PROMPT(''),AT(18,120,189,9),USE(?Prompt_Time),CENTER,TRN
                         END
                       END
                       BUTTON('&Help'),AT(4,136,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       CHECK('Pause'),AT(118,136,49,14),USE(LOC:Pause),LEFT,ICON('VCR_Ps.ico'),FLAT
                       BUTTON('&Cancel'),AT(172,136,49,14),USE(?Cancel:2),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

MAN_Del         VIEW(ManifestLoadAlias)
    PROJECT(A_MAL:MLID, A_MAL:MID, A_MAL:TTID)
       JOIN(A_MALD:FSKey_MLID_DIID, A_MAL:MLID)                                 ! Manifest Load Deliveries
       PROJECT(A_MALD:MLID, A_MALD:DIID, A_MALD:UnitsLoaded)
          JOIN(A_DELI:PKey_DIID, A_MALD:DIID)                                   ! Delivery Items
          PROJECT(A_DELI:DIID, A_DELI:DID, A_DELI:Weight, A_DELI:Units)
    .  .  .


Man_View        ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_DN_POD_Cont')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:State
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DeliveryItemAlias.SetOpenRelated()
  Relate:DeliveryItemAlias.Open                            ! File DeliveryItemAlias used by this procedure, so make sure it's RelationManager is open
  Relate:ManifestLoadAlias.Open                            ! File ManifestLoadAlias used by this procedure, so make sure it's RelationManager is open
  Relate:ManifestLoadDeliveriesAlias.Open                  ! File ManifestLoadDeliveriesAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Print_DN_POD_Cont',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryItemAlias.Close
    Relate:ManifestLoadAlias.Close
    Relate:ManifestLoadDeliveriesAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_DN_POD_Cont',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Cancel:2
      ThisWindow.Update()
          QuickWindow{PROP:Timer} = 0
      
          CASE MESSAGE('Cancel the Printing now?', 'Confirm Cancel', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
          OF BUTTON:No
             QuickWindow{PROP:Timer} = 50
          OF BUTTON:Yes
             POST(EVENT:CloseWindow)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          LOC:Page_Print_Time = GETINI('Print_DN_POD_Cont', 'Page_Print_Time', 8, GLO:Local_INI)
          IF LOC:Page_Print_Time < 2
             LOC:Page_Print_Time  = 2
          .
      
      
          ! Set it up to always wait before printing if Last Print Time was recent...
          LOC:Last_Print_Time = GETINI('Print_DN_POD_Cont', 'Last_Cont_Print_Time', 0, GLO:Local_INI)
      
      
          LOC:Print_Now       = FALSE
          ! (p:MID)
          Man_View.Init(MAN_Del, Relate:ManifestLoadAlias)
          Man_View.AddSortOrder( A_MAL:FKey_MID )
          Man_View.AddRange(A_MAL:MID, p:MID)
          Man_View.AppendOrder( 'A_MALD:MLDID' )
          !Man_View.SetFilter( '' )
      
          Man_View.Reset(1)
          ! Collect a list of unquie DIDs for this MID
          LOOP
             IF Man_View.Next() ~= LEVEL:Benign
                BREAK
             .
      
             L_DQ:DID     = A_DELI:DID
             GET(LOC:DID_Q, L_DQ:DID)
             IF ERRORCODE()
                L_DQ:DID     = A_DELI:DID
                ADD(LOC:DID_Q)
          .  .
          Man_View.Kill()
      
      
      
          LOC:Total_Invoices  = RECORDS(LOC:DID_Q)
      
          LOC:Preview_Option  = 2
      
          LOC:Idx             = 0
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
          PUTINI('Print_DN_POD_Cont', 'Page_Print_Time', LOC:Page_Print_Time, GLO:Local_INI)
          PUTINI('Print_DN_POD_Cont', 'Last_Cont_Print_Time', CLOCK(), GLO:Local_INI)
    OF EVENT:Timer
          IF LOC:Print_Now = FALSE
             LOC:TimeOut  = LOC:Page_Print_Time - ((CLOCK() - LOC:Last_Print_Time) / 100)
             ?Prompt_Time{PROP:Text}  = 'Printing in ' & LOC:TimeOut & ' seconds'
      
             IF LOC:Page_Print_Time <= ABS((CLOCK() - LOC:Last_Print_Time) / 100)
                LOC:Print_Now = TRUE
          .  .
      
      
          
          IF LOC:Print_Now = TRUE
             ?Prompt_Time{PROP:Text}  = 'Printing...'
             QuickWindow{PROP:Timer}  = 0
      
             IF LOC:Pause = TRUE
                IF CLOCK() - LOC:ChangeTime > 50
                   LOC:ChangeTime    = CLOCK()
                   IF LOC:State = 'Paused'
                      LOC:State  = ''
                   ELSE
                      LOC:State  = 'Paused'
                .  .
             ELSE
                LOC:Idx  += 1
                GET(LOC:DID_Q, LOC:Idx)
                IF ERRORCODE()
                   LOC:Complete   = TRUE
                .
      
                IF LOC:Complete ~= TRUE
                   LOC:Inv_No   += 1
      
                   LOC:State     = 'Printing Delivery Note ' & LOC:Inv_No & ' of ' & LOC:Total_Invoices
      
                   ?Prompt1{PROP:Text}   = 'DID: ' & L_DQ:DID
      
                   LOC:No_Printed   += 1
      
                           ! (p:ID, p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow)
                           ! (ULONG, BYTE      , BYTE=0      , BYTE=1          , BYTE=0), LONG, PROC
                   LOC:Print_Result        = Print_Cont(L_DQ:DID, 2, 1, LOC:Preview_Option)
                   IF LOC:Print_Result < 0
                      LOC:Complete  = TRUE
                   ELSIF LOC:Print_Result = 2
                      LOC:Preview_Option   = 0       ! No preview
             .  .  .
      
             LOC:Last_Print_Time      = CLOCK()
      
             IF LOC:Idx >= LOC:Total_Invoices
                LOC:Complete = TRUE
             .
      
             IF LOC:Complete = FALSE
                QuickWindow{PROP:Timer}  = 50
             ELSE
                IF LOC:No_Printed <= 0 AND LOC:Result >= 0                           ! And not cancelled
                   MESSAGE('No Delivery Notes were found to print.', 'Print Delivery Notes', ICON:Exclamation)
                .
      
                POST(EVENT:CloseWindow)
          .  .
      
          LOC:Print_Now   = FALSE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

