

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('abbreak.inc'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS020.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! (p:CID, p:Payment_Status, p:Total, p:Cur, p:Other)
!!! </summary>
Get_Client_Advance_Payments PROCEDURE (p:CID, p:Payment_Status, p:Total, p:Cur, p:Other)

LOC:Time             LONG                                  ! 
LOC:Times            LONG(10)                              ! 
LOC:Timer            LONG                                  ! 
LOC:Locals           GROUP,PRE()                           ! 
LOC:Date_Month_End   LONG                                  ! 
L_RG:Greater_Current DECIMAL(12,2)                         ! 
L_RG:Current         DECIMAL(12,2)                         ! 
LOC:Allocated        DECIMAL(12,2)                         ! 
                     END                                   ! 
LOC:Returned         LONG                                  ! 
LOC:Progress         LONG                                  ! 
QuickWindow          WINDOW('Client Pre-Payments'),AT(,,142,48),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  IMM,MDI,HLP('Get_Client_Advance_Payments'),TIMER(11)
                       PROGRESS,AT(32,16,77,8),USE(?Progress1),RANGE(0,100)
                       BUTTON('&Cancel'),AT(92,32,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
View_CP         VIEW(ClientsPayments)
    PROJECT(CLIP:CID, CLIP:CPID, CLIP:DateMade, CLIP:Amount, CLIP:Status)
    .

CP_View         ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:Returned)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Set_Return_Parameters           ROUTINE
    ! (p:CID, p:Payment_Status, p:Total, p:Cur, p:Other)
    ! (ULONG, LONG=2, *DECIMAL, <*DECIMAL>, <*DECIMAL>),LONG

    p:Total     = L_RG:Current + L_RG:Greater_Current

    IF ~OMITTED(4)
       p:Cur       = L_RG:Current
    .
    IF ~OMITTED(5)
       p:Other     = L_RG:Greater_Current
    .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Get_Client_Advance_Payments')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
        BIND('CLIP:Status', CLIP:Status)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ClientsPayments.Open                              ! File ClientsPayments used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
    QuickWindow{PROP:Hide}  = TRUE
  Do DefineListboxStyle
  INIMgr.Fetch('Get_Client_Advance_Payments',QuickWindow)  ! Restore window settings from non-volatile store
        IF DAY(TODAY()) > 25
           LOC:Date_Month_End       = DATE(MONTH(TODAY()), 25, YEAR(TODAY()))
        ELSE
           IF MONTH(TODAY()) = 1
              LOC:Date_Month_End    = DATE(12, 25, YEAR(TODAY()) - 1)
           ELSE
              LOC:Date_Month_End    = DATE(MONTH(TODAY()) - 1, 25, YEAR(TODAY()))
        .  .
    
  
  !      MESSAGE('The current period will be taken to be ' & FORMAT(LOC:Date_Month_End, @d6b), 'Print Client Advance Payments', ICON:Asterisk)
      ! Check and update statuses on Client Payments - we would need to loop through them 1st....
      ! So we will instead only update the ones that show as being un-allocated...
  
      CP_View.Init(View_CP, Relate:ClientsPayments)
      CP_View.AddSortOrder(CLIP:FKey_CID)
      CP_View.AppendOrder('CLIP:CPID')
      CP_View.AddRange(CLIP:CID, p:CID)
  
      ! Not Allocated | Partial Allocation | Fully Allocated
      IF p:Payment_Status > 0
         CP_View.SetFilter('CLIP:Status < ' & p:Payment_Status)            ! Not fully allocated
      .
  
      CP_View.Reset()
  SELF.SetAlerts()
      SETCURSOR(CURSOR:Wait)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
          UNBIND('CLIP:Status')
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
      CP_View.Kill()
  
  
  !    IF L_RG:Current ~= 0.0 OR L_RG:Greater_Current ~= 0.0
  !       db.debugout('[Get_Client_Advnace_Payments]  L_RG:Current: ' & L_RG:Current & ',   L_RG:Greater_Current: ' & L_RG:Greater_Current)
  !    .
  
  
      DO Set_Return_Parameters
    Relate:ClientsPayments.Close
  END
  IF SELF.Opened
    INIMgr.Update('Get_Client_Advance_Payments',QuickWindow) ! Save window data to non-volatile store
  END
      SETCURSOR()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
          LOC:Timer   = QuickWindow{PROP:Timer}
      
          LOC:Time    = CLOCK()
          LOOP LOC:Times TIMES
             IF CP_View.Next() ~= LEVEL:Benign
                LOC:Timer     = 0
                POST(EVENT:CloseWindow)
                BREAK
             .
      
             LOC:Returned += 1                ! Count of number of payments found (of specified status - where passed)
      
             LOC:Progress += 1
             ?Progress1{PROP:Progress}    = LOC:Progress
             IF LOC:Progress > 100
                LOC:Progress = 0
             .
      
             ! (p:CPID, p:Amt_Allocated)
             IF Upd_ClientPayments_Status(CLIP:CPID, LOC:Allocated) >= 2
                CYCLE
             .
      
             IF CLIP:DateMade <= LOC:Date_Month_End       ! Prior to current
                L_RG:Greater_Current  += (CLIP:Amount - LOC:Allocated)
             ELSE
                L_RG:Current          += (CLIP:Amount - LOC:Allocated)
                .
      !       db.debugout('L_RG:Greater_Current: ' & L_RG:Greater_Current & ',  L_RG:Current: ' & L_RG:Current)
          .  !.
      
      
          IF CLOCK() - LOC:Time > 80
             LOC:Times     -= 1
          .
          IF CLOCK() - LOC:Time < 5
             LOC:Times     += 3
          .
          IF LOC:Times <= 0
             LOC:Times = 1
          .
      
      
          QuickWindow{PROP:Timer} = LOC:Timer
      
          IF LOC:Returned > 20
             IF QuickWindow{PROP:Hide} = TRUE
                QuickWindow{PROP:Hide}  = FALSE
                SETCURSOR()
          .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Debtor_Age_Analysis_PDF PROCEDURE (p:SRID, p:Option, p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:CID, p:Credit_Limit_Option, p:ClientStatus, p:BID)

Progress:Thermometer BYTE                                  ! 
LOC:Locals           GROUP,PRE(LOC)                        ! 
Date                 DATE                                  ! 
Time                 TIME                                  ! 
                     END                                   ! 
LOC:STRID            ULONG                                 ! Statement Run ID
LOC:Statement_Q      QUEUE,PRE(L_SQ)                       ! Statement ID
STID                 ULONG                                 ! Statement ID
CID                  ULONG                                 ! Client ID
                     END                                   ! 
LOC:Info             STRING(35)                            ! 
LOC:Progress_Group   GROUP,PRE(L_PG)                       ! 
Clients              LONG                                  ! 
No_Done              LONG                                  ! 
Win_Txt              CSTRING(50)                           ! 
TimeIn               LONG                                  ! 
                     END                                   ! 
LOC:Grand_Totals     GROUP,PRE(L_GT)                       ! 
Total                DECIMAL(10,2)                         ! Total
Current              DECIMAL(10,2)                         ! Current
Days30               DECIMAL(10,2)                         ! 30 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days90               DECIMAL(10,2)                         ! 90 Days
Number               ULONG                                 ! Number of entries
Current_PerTot       DECIMAL(6,1)                          ! 
Days30_PerTot        DECIMAL(6,1)                          ! Current
Days60_PerTot        DECIMAL(6,1)                          ! Current
Days90_PerTot        DECIMAL(6,1)                          ! Current
                     END                                   ! 
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
LOC:Options_Group    GROUP,PRE(L_OG)                       ! 
Status               BYTE(255)                             ! Normal, On Hold, Closed, Dormant
Report_Status        STRING(50)                            ! can be more than one
BranchName           STRING(35)                            ! Branch Name
BID                  ULONG                                 ! Branch ID
BID_FirstTime        BYTE(1)                               ! 
Client_Normal        BYTE                                  ! 
Client_OnHold        BYTE                                  ! 
Client_Closed        BYTE                                  ! 
Client_Dormant       BYTE                                  ! 
                     END                                   ! 
Process:View         VIEW(Clients)
                       PROJECT(CLI:AID)
                       PROJECT(CLI:AccountLimit)
                       PROJECT(CLI:BID)
                       PROJECT(CLI:CID)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:Status)
                     END
ProgressWindow       WINDOW('Clients Age Analysis - PDF'),AT(,,255,156),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       SHEET,AT(4,2,248,135),USE(?Sheet1)
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(24,71,205,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(14,59,227,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(14,87,227,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Pause'),AT(199,138,,15),USE(?Pause),LEFT,ICON('waok.ico'),DISABLE,FLAT
                       BUTTON('Cancel'),AT(146,138,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Clients Report'),AT(250,1156,7823,9344),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7823,917),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Client Age Analysis'),AT(2740,0,,365),USE(?ReportTitle),FONT('Arial',18,,FONT:bold), |
  CENTER
                         STRING('Branch:'),AT(52,396,469,198),USE(?String36:2),FONT('Arial',10,,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('Client Status:'),AT(4583,396),USE(?String36),FONT('Arial',10,,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s50),AT(5521,396,2292,198),USE(L_OG:Report_Status),FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         STRING(@s35),AT(573,396,1823,198),USE(L_OG:BranchName),FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         BOX,AT(0,698,7813,208),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(417,708,0,200),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2604,708,0,200),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(3563,708,0,200),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(4427,708,0,200),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(5281,708,0,200),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(6115,708,0,200),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(6969,708,0,200),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         STRING('No.'),AT(156,719,,170),USE(?HeaderTitle:1),TRN
                         STRING('Debtors Name'),AT(479,719,900,170),USE(?HeaderTitle:2),TRN
                         STRING('Limit'),AT(2677,719,833,167),USE(?HeaderTitle:9),CENTER,TRN
                         STRING('90 Days'),AT(3563,719,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('60 Days'),AT(4396,719,833,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('30 Days'),AT(5240,719,833,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Current'),AT(6094,719,833,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Total'),AT(6958,719,833,167),USE(?HeaderTitle:8),CENTER,TRN
                       END
Detail                 DETAIL,AT(,,7823,479),USE(?Detail)
                         LINE,AT(0,0,0,479),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(417,0,0,240),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(2604,0,0,240),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(3563,0,0,240),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(4427,0,0,240),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(5281,0,0,240),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(6115,0,0,240),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(6969,0,0,240),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(7813,0,0,479),USE(?DetailLine:8),COLOR(COLOR:Black)
                         STRING(@s35),AT(469,281,2813,167),USE(LOC:Info),TRN
                         STRING(@n-17),AT(2646,31,885,167),USE(CLI:AccountLimit),RIGHT(1),TRN
                         STRING(@n-14.2),AT(3563,31),USE(L_SI:Days90),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4396,31),USE(L_SI:Days60),RIGHT(1),TRN
                         STRING(@n-14.2),AT(5240,31),USE(L_SI:Days30),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6094,31),USE(L_SI:Current),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6958,31),USE(L_SI:Total),RIGHT(1),TRN
                         STRING(@n_10b),AT(-313,31,,170),USE(CLI:ClientNo),RIGHT,TRN
                         STRING(@s35),AT(469,31,2100,167),USE(CLI:ClientName),LEFT
                         LINE,AT(417,240,7400,0),USE(?DetailEndLine:2),COLOR(COLOR:Black)
                         LINE,AT(0,479,7813,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
detail_totals          DETAIL,AT(,,,625),USE(?detail_totals)
                         STRING(@n-14.2),AT(5240,146,833,167),USE(L_GT:Days30),RIGHT(1)
                         STRING(@n-14.2),AT(3563,146),USE(L_GT:Days90),RIGHT(1)
                         STRING(@n-14.2),AT(4396,146,833,167),USE(L_GT:Days60),RIGHT(1)
                         STRING(@n13),AT(1979,146),USE(L_GT:Number),RIGHT(1)
                         STRING(@n-14.2),AT(6094,146,833,167),USE(L_GT:Current),RIGHT(1)
                         STRING(@n-14.2),AT(6958,146,833,167),USE(L_GT:Total),RIGHT(1)
                         STRING('% of Total:'),AT(1188,375),USE(?String29:2),TRN
                         STRING(@n-9.1),AT(5240,375,833,167),USE(L_GT:Days30_PerTot),RIGHT(1)
                         STRING(@n-9.1),AT(4396,375,833,167),USE(L_GT:Days60_PerTot),RIGHT(1)
                         STRING(@n-9.1),AT(3563,375,833,167),USE(L_GT:Days90_PerTot),RIGHT(1)
                         STRING(@n-9.1),AT(6094,375,833,167),USE(L_GT:Current_PerTot),RIGHT(4)
                         STRING('Debtors Listed:'),AT(1188,146),USE(?String29),TRN
                       END
                       FOOTER,AT(250,10500,7823,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(7021,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

View_State      VIEW(_Statements)
    PROJECT(STA:STID, STA:STRID, STA:CID)
    .

State_View      ViewManager





View_Addr       VIEW(AddressContacts)
    !PROJECT()
    .


Addr_View       ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_Statement_Run              ROUTINE
    SETCURSOR(CURSOR:Wait)
    FREE(LOC:Statement_Q)

    State_View.Init(View_State, Relate:_Statements)
    State_View.AddSortOrder(STA:FKey_STRID)
    !State_View.AppendOrder()
    State_View.AddRange(STA:STRID, LOC:STRID)
    !State_View.SetFilter()

    State_View.Reset()
    LOOP
       IF State_View.Next() ~= LEVEL:Benign
          BREAK
       .

       L_SQ:STID    = STA:STID
       L_SQ:CID     = STA:CID
       ADD(LOC:Statement_Q)
    .
    State_View.Kill()
    SETCURSOR()

    ?Progress:UserString{PROP:TExt} = 'Statements: ' & RECORDS(LOC:Statement_Q)
    EXIT
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Client No.' & |
      '|' & 'By Client Name' & |
      '|' & 'By Branch ID' & |
      '|' & 'By Client ID' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Debtor_Age_Analysis_PDF')
      ! (p:SRID, p:Option, p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:CID, p:Credit_Limit_Option, p:ClientStatus, p:BID)
      ! (ULONG, BYTE, LONG=0, BYTE=0, BYTE=0, BYTE=0, ULONG=0, BYTE=0, BYTE=255, ULONG=0)
  
  
  
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_OG:BID',L_OG:BID)                                ! Added by: Report
  BIND('L_OG:Status',L_OG:Status)                          ! Added by: Report
  BIND('L_OG:Client_Normal',L_OG:Client_Normal)            ! Added by: Report
  BIND('L_OG:Client_OnHold',L_OG:Client_OnHold)            ! Added by: Report
  BIND('L_OG:Client_Closed',L_OG:Client_Closed)            ! Added by: Report
  BIND('L_OG:Client_Dormant',L_OG:Client_Dormant)          ! Added by: Report
      BIND('p:CID', p:CID)
         Addr_View.Init(View_Addr, Relate:AddressContacts)
         Addr_View.AddSortOrder(ADDC:FKey_AID)
         Addr_View.AppendOrder('ADDC:ContactName')
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
    IF p:CID = 0
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
    ELSE
       ! No need for selection when we have only 1 client
       ProcessSortSelectionVariable     = 'By Client No.'
    .
  Relate:AddressContacts.SetOpenRelated()
  Relate:AddressContacts.Open                              ! File AddressContacts used by this procedure, so make sure it's RelationManager is open
  Access:_Statements.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Statement_Runs.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
        L_OG:BID    = p:BID
  
        IF L_OG:BID = 0
           L_OG:BranchName    = 'All'
        ELSE
           BRA:BID    = L_OG:BID
           IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
              L_OG:BranchName    = CLIP(BRA:BranchName)
        .  .
      L_OG:Status         = 254       ! none
  
      L_OG:Client_Normal  = 255
      L_OG:Client_OnHold  = 255
      L_OG:Client_Closed  = 255
      L_OG:Client_Dormant = 255
  
  
      IF BAND(p:ClientStatus, 0001b) > 0
         L_OG:Client_Normal   = 0
         L_OG:Report_Status   = 'Normal'
      .
  
      IF BAND(p:ClientStatus, 0010b) > 0
         L_OG:Client_OnHold   = 1
         ! (p:Add, p:List, p:Delim, p:Option, p:Prefix)
         Add_to_List('On Hold', L_OG:Report_Status, ', ')
      .
  
      IF BAND(p:ClientStatus, 0100b) > 0
         L_OG:Client_Closed   = 2
         Add_to_List('Closed', L_OG:Report_Status, ', ')
      .
  
      IF BAND(p:ClientStatus, 1000b) > 0
         L_OG:Client_Dormant  = 3
         Add_to_List('Dormant', L_OG:Report_Status, ', ')
      .
  
      IF p:ClientStatus = 0
         L_OG:Report_Status   = 'All'
         L_OG:Status          = 255
      .
  ThisReport.Init(Process:View, Relate:Clients, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientNo')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client Name')) THEN
     ThisReport.AppendOrder('+CLI:ClientName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch ID')) THEN
     ThisReport.AppendOrder('+CLI:BID,+CLI:ClientName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client ID')) THEN
     ThisReport.AppendOrder('+CLI:CID')
  END
  ThisReport.SetFilter('(p:CID = 0 OR p:CID = CLI:CID) AND (L_OG:BID = 0 OR L_OG:BID = CLI:BID) AND (L_OG:Status = 255 OR L_OG:Client_Normal = CLI:Status OR L_OG:Client_OnHold = CLI:Status OR L_OG:Client_Closed = CLI:Status OR L_OG:Client_Dormant = CLI:Status)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report)
  ?Progress:UserString{PROP:Text} = 'Processing Age Analysis'
  Relate:Clients.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      ! Show statement info for which the age analysis will be used - offer to run new one
      LOC:Date        = TODAY()
      LOC:Time        = CLOCK()
  
  
  
      IF p:SRID ~= 0
         LOC:STRID    = p:SRID
         DO Load_Statement_Run
      ELSE
         LOC:Date     = p:Date
         LOC:Time     = DEFORMAT('12:00:00',@t4)
      .
  
  
      ! Both of these set Invoice UpToDate status to 0 if there is a new Credit Note or Client Payment that has not
      ! had its UpToDate status set yet.   At the same time that the related Invoies are set to 0 the processed
      ! Invoice or Client Payment is set to UpToDate.  In otherwords this should be quick.
  
      Process_Invoice_StatusUpToDate('0')
      Process_ClientPaymentsAllocation_StatusUpToDate('0')
      
      IF p:OutPut > 0       ! (p:Text, p:Heading, p:Name)
         Add_Log(',Client No.,Invoice No.,Invoice Date,Invoice Total,Credits,Payments', 'Headings', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv', TRUE)
      .
  
  
  SELF.SetAlerts()
      IF p:CID = 0
         L_PG:Clients     = RECORDS(Clients)
         L_PG:Win_Txt     = ProgressWindow{PROP:Text}
      .
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      UNBIND('p:CID')
         Addr_View.Kill()
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressContacts.Close
  END
      IF p:OutPut > 0
         CASE MESSAGE('Would you like to load the accumulation details now?|Note that if the file does not load automatically you can find it at the location shown below.||File: ' & PATH() & '\Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv', 'Age Analysis', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Pause
      ThisWindow.Update()
          EXECUTE L_OG:Status + 1
             L_OG:Report_Status  = 'Normal'
             L_OG:Report_Status  = 'On Hold'
             L_OG:Report_Status  = 'Closed'
             L_OG:Report_Status  = 'Dormant'
          ELSE
             L_OG:Report_Status  = 'All'
          .
          SELECT(?Tab1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      ! Also see Validation - Section moved there, as needed to filter for new Credit Limit option
  
      IF CLIP(LOC:Info) ~= '<no statement>'
         ! Put contact details in LOC:Info
         Addr_View.AddRange(ADDC:AID, CLI:AID)
         Addr_View.Reset()
         IF Addr_View.Next() = LEVEL:Benign
            LOC:Info      = ADDC:ContactName
      .  .
  
  
      ADD:AID             = CLI:AID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
         IF CLIP(LOC:Info) = ''
            LOC:Info      = CLIP(ADD:PhoneNo)
         ELSE
            LOC:Info      = CLIP(ADD:PhoneNo) & ', ' & LOC:Info
      .  .
  
  
      L_GT:Current_PerTot = L_GT:Current / L_GT:Total * 100
      L_GT:Days30_PerTot  = L_GT:Days30  / L_GT:Total * 100
      L_GT:Days60_PerTot  = L_GT:Days60  / L_GT:Total * 100
      L_GT:Days90_PerTot  = L_GT:Days90  / L_GT:Total * 100
        IF p:Ignore_Zero_Bal = TRUE
           IF (L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current + L_SI:Total) = 0.0
  
              !L_GT:Total = 0.0 AND L_GT:Current = 0.0 AND L_GT:Days30 = 0.0 AND L_GT:Days60 = 0.0 AND L_GT:Days90 = 0.0
              !db.debugout('[Print_Debtors_Age_Analysis]  0.0 on all values')
              
  
              SkipDetails  = TRUE
        .  .
  IF 0
    PRINT(RPT:detail_totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      ! All|Last Statement|None
      IF p:Option = 1
         L_SQ:CID  = CLI:CID
         GET(LOC:Statement_Q, L_SQ:CID)
         IF ERRORCODE()
            ReturnValue       = Record:Filtered
      .  .
  
      IF p:Option < 2
         IF LOC:STRID = 0
            ReturnValue       = LEVEL:Fatal
      .  .
  
  
  
  
      ! This Section is from TakeRecord - needed here for additional option - p:Credit_Limit_Option
      CLEAR(LOC:Info)
  
      ! p:SRID = 0    - not from statement
  
      IF p:SRID = 0
         ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
         L_PG:No_Done    += 1
  
         ProgressWindow{PROP:Text}        = L_PG:Win_Txt & ' - ' & L_PG:No_Done & '/' & L_PG:Clients
         ?Progress:UserString{Prop:Text}  = CLIP(CLI:ClientName)
  
         L_PG:TimeIn                      = CLOCK()
         Junk_#           = Gen_Statement(0, CLI:CID, LOC:Date, LOC:Time,, TRUE, LOC:Statement_Info,, p:MonthEndDay, p:OutPut)
         IF CLOCK() - L_PG:TimeIn > 100       ! 1 second
            db.debugout('[Print_Debtor_Age_Analysis]  CLI:ClientNo: ' & CLI:ClientNo & ',  Gen_Statement > 1 sec: ' & CLOCK() - L_PG:TimeIn)
         .
      ELSE
         ! Get the suitable Statement
         CLEAR(LOC:Info)
         CLEAR(STA:Record)
  
         L_SQ:CID  = CLI:CID
         GET(LOC:Statement_Q, L_SQ:CID)
         IF ~ERRORCODE()
            STA:STID      = L_SQ:STID
            IF Access:_Statements.TryFetch(STA:PKey_STID) = LEVEL:Benign
            ELSE
               LOC:Info   = '<no statement>'
            .
         ELSE
            LOC:Info      = '<no statement>'
         .
  
  
         L_SI:Days90      = STA:Days90
         L_SI:Days60      = STA:Days60
         L_SI:Days30      = STA:Days30
         L_SI:Current     = STA:Current
         L_SI:Total       = STA:Total
      .
  
  
      IF p:Credit_Limit_Option = TRUE
         IF L_SI:Total <= CLI:AccountLimit
            ReturnValue   = Record:Filtered
      .  .
  
  
      IF ReturnValue = Record:OK
         L_GT:Total      += L_SI:Total
         L_GT:Current    += L_SI:Current
         L_GT:Days30     += L_SI:Days30
         L_GT:Days60     += L_SI:Days60
         L_GT:Days90     += L_SI:Days90
         L_GT:Number     += 1
      .
  !    old
  !       only changed this section on the 1st of Dec 06
  !
  !
  !       IF p:SRID = 0                    ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay)
  !          L_GT:Total      += L_SI:Total
  !          L_GT:Current    += L_SI:Current
  !          L_GT:Days30     += L_SI:Days30
  !          L_GT:Days60     += L_SI:Days60
  !          L_GT:Days90     += L_SI:Days90
  !          L_GT:Number     += 1
  !       ELSE
  !          L_GT:Total      += L_SI:Total
  !          L_GT:Current    += L_SI:Current
  !          L_GT:Days30     += L_SI:Days30
  !          L_GT:Days60     += L_SI:Days60
  !          L_GT:Days90     += L_SI:Days90
  !          L_GT:Number     += 1
  !    .  .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Window
!!! FuelSurchargeReportWindow
!!! </summary>
Start_FuelSurchargeReport PROCEDURE 

ClientNo             ULONG                                 ! Client No.
AllClients           BYTE                                  ! 
ClientString         CSTRING(101)                          ! 
CurrentCID           ULONG                                 ! Client ID
DateFrom             DATE                                  ! 
DateTo               DATE                                  ! 
QBranches            QUEUE,PRE(F)                          ! 
BranchName           STRING(35)                            ! Branch Name
BID                  ULONG                                 ! Branch ID
                     END                                   ! 
QRecords             QUEUE,PRE()                           ! 
DisplayName          CSTRING(128)                          ! 
DisplayName_icon     LONG                                  ! 
DisplayName_level    LONG                                  ! 
InvoiceDate          DATE                                  ! Invoice Date
Total                DECIMAL(14,2)                         ! (exlcuding VAT in this case)
FreightCharge        DECIMAL(14,2)                         ! 
FuelSurcharge        DECIMAL(14,2)                         ! 
FuelSurchargePercent DECIMAL(7,2)                          ! FuelSurchargePercent of cargo charge (i.e. Total)
BID                  ULONG                                 ! Branch ID
CID                  ULONG                                 ! Client ID
BranchName           CSTRING(36)                           ! Branch Name
ClientName           CSTRING(101)                          ! 
Year                 SHORT                                 ! 
MonthNo              BYTE                                  ! 
Month                CSTRING(11)                           ! 
InvDate              DATE                                  ! Invoice Date
InvTime              TIME                                  ! Invoice Time - generated time
IID                  ULONG                                 ! Invoice Number
TotalExVAT           DECIMAL(14,2)                         ! 
                     END                                   ! 
LevelName            &CSTRING                              ! 
LevelNum             SHORT                                 ! 
InclVAT              BYTE                                  ! 
Check1               BYTE                                  ! 
Check2               BYTE                                  ! 
Check3               BYTE                                  ! 
Check4               BYTE                                  ! 
ProgressPercent      BYTE                                  ! 
ShowFreightCharge    BYTE                                  ! 
FreightFieldWidth    SHORT                                 ! 
MaxLevel             Byte
ReportMode           Byte
lClientName          Like(CLI:ClientName)
QuickWindow          WINDOW('Fuel Surcharge Report'),AT(,,533,237),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT), |
  RESIZE,CENTER,GRAY,IMM,MAX,MDI,HLP('FuelSurchargeReport'),SYSTEM
                       PROMPT('Period:'),AT(9,7,33),USE(?PeriodPrompt)
                       LIST,AT(49,5,82,11),USE(?PeriodList),DROP(10),FORMAT('16L(2)|M@s255@')
                       PROMPT('Date from:'),AT(141,5),USE(?DateFrom:Prompt),RIGHT,TRN
                       ENTRY(@D17B),AT(182,5,60,11),USE(DateFrom),RIGHT(1)
                       BUTTON('...'),AT(247,4,12,12),USE(?Calendar),SKIP
                       PROMPT('Date to:'),AT(268,5),USE(?DateTo:Prompt),RIGHT,TRN
                       ENTRY(@D17B),AT(299,5,60,11),USE(DateTo),RIGHT(1)
                       BUTTON('...'),AT(364,4,12,12),USE(?Calendar:2),SKIP
                       GROUP('Criteria'),AT(2,18,529,93),USE(?CriteriaGroup),BOXED,TRN
                         CHECK(' Show Branches'),AT(9,31,66),USE(Check1),TRN
                         LIST,AT(78,30,82,11),USE(?BranchList),DROP(5),FORMAT('140L(2)|M@s35@'),FROM(QBranches)
                         CHECK(' Show Clients'),AT(9,47,66,10),USE(Check2),TRN
                         CHECK(' All Clients'),AT(77,46),USE(AllClients),TRN
                         BUTTON('...'),AT(77,60,12,12),USE(?CallClientLookup),DISABLE
                         ENTRY(@s100),AT(94,60,296,11),USE(ClientString),DISABLE,READONLY,REQ,SKIP,TIP('Client name')
                         PROMPT('Client No.:'),AT(314,47),USE(?ClientNo:Prompt),RIGHT,HIDE,TRN
                         STRING(@n_10b),AT(353,47,38,10),USE(ClientNo),RIGHT(1),HIDE,TRN
                         CHECK(' Show Months'),AT(9,78,66,10),USE(Check3),TRN
                         CHECK(' Show Invoices'),AT(9,95,66,10),USE(Check4),TRN
                         CHECK(' Show Fright Charge'),AT(77,95),USE(ShowFreightCharge)
                         CHECK(' Including VAT'),AT(405,95),USE(InclVAT),HIDE
                         BUTTON('&Refresh'),AT(475,90,49,14),USE(?Refresh),LEFT,ICON('RefreshI.ico'),FLAT,MSG('List Records'), |
  TIP('List Records')
                       END
                       LIST,AT(3,115,527,102),USE(?InvoiceList),VSCROLL,FORMAT('218L(2)|MJT(LR)~Item Name~@s12' & |
  '7@56R(2)|M~Invoice Date~C(1)@D17B@75R(2)|M~Total~C(1)@n-15.2@0R(2)|M~Freight Charge~' & |
  'C(1)@n-14.2@80R(2)|M~Fuel Surcharge~C(1)@n-14.2@64R(2)|M~Fuel S.%~C(1)@n-7.2@'),FROM(QRecords)
                       BUTTON('Print Report'),AT(2,221),USE(?ReportBtn),DISABLE
                       PROGRESS,AT(59,226,371,6),USE(ProgressPercent,,?Progress1),HIDE,RANGE(0,100)
                       STRING(@n4~%~),AT(449,223,27),USE(ProgressPercent),RIGHT,HIDE
                       BUTTON('&Close'),AT(481,221,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(344,95,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       BUTTON('Previous'),AT(381,4,73,13),USE(?PrevPeriod),LEFT,ICON('movedown.ico'),FLAT,SKIP
                       BUTTON('Next'),AT(457,4,73,13),USE(?NextPeriod),LEFT,ICON('moveup.ico'),FLAT,SKIP
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
FillQRecords           PROCEDURE()                         ! New method added to this class instance
SetBranchName          PROCEDURE(ULong pBID),String        ! New method added to this class instance
SetClientName          PROCEDURE(ULong pCID),String        ! New method added to this class instance
CalcInvRecord          PROCEDURE(TFuelSurchargeRecord Q)   ! New method added to this class instance
ShowClient             PROCEDURE(String pTitle)            ! New method added to this class instance
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar6            CalendarClass
Calendar7            CalendarClass
AllPeriods           ULONG

Period:ThisYear      EQUATE(Period:Current9)
Period:LastYear      EQUATE(Period:Past9)

DatePeriodFilter     CLASS(DatePeriodClass)
FillQPeriod            PROCEDURE(ULONG pPeriodEquates),VIRTUAL
GetPeriodName          PROCEDURE(ULONG pPeriodEquate),STRING,VIRTUAL
                     END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
RefreshOnShowBranches ROUTINE
  Update(?Check1)
  If Check1 THEN
     UnHide(?BranchList)
     Do GetBranchSelection
  Else
     Clear(QBranches)
     ?BranchList{Prop:Selected} = 1
     Hide(?BranchList)
  End
  
GetBranchSelection Routine
  Get(QBranches,Choice(?BranchList))
  If ErrorCode() THEN
     Clear(QBranches)
     ?BranchList{Prop:Selected} = 1
  End

RefreshOnShowClients Routine
  Update(?Check2)
  If Check2 THEN
     Enable(?AllClients)
  Else
     AllClients = True
     AllClients{PROP:Checked} = True
     Disable(?AllClients)
  End
  Do RefreshOnAllClients

RefreshOnAllClients Routine
  If AllClients Then    
    Disable(?ClientString)
    Disable(?CallClientLookup)
    ClientString = 'All'       
    Hide(?ClientNo:Prompt)
    Hide(?ClientNo)
  Else    
    Enable(?ClientString)
    Enable(?CallClientLookup)
    ClientString = ''
    ClientNo = ''
    UnHide(?ClientNo:Prompt)
    UnHide(?ClientNo)
  End
  CurrentCID = 0

ClearResultSet Routine
  Free(QRecords)  
  Disable(?ReportBtn)
  
!ClearQRecords Routine
!  QRecords.DisplayName          = ''
!  QRecords.DisplayName_icon     = 0
!  QRecords.DisplayName_level    = 0
!  QRecords.InvoiceDate          = 0
!  QRecords.Total                = 0
!  QRecords.FuelSurcharge        = 0
!  QRecords.FuelSurchargePercent = 0
!  QRecords.BID                  = 0
!  QRecords.CID                  = 0
!  QRecords.BranchName           = ''
!  QRecords.ClientName           = 0
!  QRecords.Year                 = 0
!  QRecords.MonthNo              = 0
!  QRecords.Month                = ''
!  QRecords.InvDate              = 0
!  QRecords.InvTime              = 0
!  QRecords.IID                  = 0
!  QRecords.TotalExVAT           = 0

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

DatPar     GROUP(tgDatePeriodsParams).
  CODE
  GlobalErrors.SetProcedureName('Start_FuelSurchargeReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  !Message('Not implemented yet.','TransIS',ICON:Asterisk)
  !ReturnValue = Level:Notify
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PeriodPrompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  InclVAT = 0
  AllClients = True
  FreightFieldWidth = 80
  ?InvoiceList{PROPLIST:Width,4} = 0
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Start_FuelSurchargeReport',QuickWindow)    ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  ?InvoiceList{PROP:Alrt} = MouseRight
  
  AllPeriods=Period:All+Period:Today+Period:Yesterday+Period:ThisWeek+Period:LastWeek+Period:ThisMonth+Period:LastMonth+Period:DateRange + Period:ThisYear + Period:LastYear
  
  DatPar.PeriodEquates        = AllPeriods
  DatPar.DefaultPeriod        = Period:ThisMonth
  DatPar.ListControl          = ?PeriodList
  DatPar.DateFromControl      = ?DateFrom
  DatPar.DateToControl        = ?DateTo
  DatPar.CurDateFrom          &= DateFrom
  DatPar.CurDateTo            &= DateTo
  DatPar.PrevControl          = ?PrevPeriod
  DatPar.NextControl          = ?NextPeriod
  
  DatePeriodFilter.Init(DatPar)
  
  !Branches
  Free(QBranches)
  If RunSQLRequest('Select * From Branches Order By BranchName',Branches)
     Loop
          If Access:Branches.TryNext() ~= Level:benign Then Break .
          Clear(QBranches)
          QBranches.BranchName = BRA:BranchName
          QBranches.BID        = BRA:BID
          Add(QBranches)
     End
  End
  Clear(QBranches)
  QBranches.BranchName = 'All'
  QBranches.BID        = 0
  Add(QBranches,1)
  !Check boxes
  Check1 = False
  Do RefreshOnShowBranches
  Check2 = False
  Do RefreshOnShowClients
  Check3 = False
  Check4 = True  
  ?InvoiceList{PROP:IconList,1} = '~office-building.ico'
  ?InvoiceList{PROP:IconList,2} = '~contact16.ico'
  ?InvoiceList{PROP:IconList,3} = '~calendar16.ico'
  ?InvoiceList{PROP:IconList,4} = '~Object.ico'
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  DatePeriodFilter.Kill
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
  END
  IF SELF.Opened
    INIMgr.Update('Start_FuelSurchargeReport',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Select_Clients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Calendar
      POST(Event:Accepted, ?DateFrom)
    OF ?Calendar:2
      POST(Event:Accepted, ?DateTo)
    OF ?Check1
      Do RefreshOnShowBranches
    OF ?Check2
      Do RefreshOnShowClients
      Display()
    OF ?AllClients
      Do RefreshOnAllClients
      Display()
    OF ?ShowFreightCharge
      Update(?ShowFreightCharge)
      If ShowFreightCharge Then
         If ~FreightFieldWidth Then
            FreightFieldWidth = 80
         End
         ?InvoiceList{PROPLIST:Width,4} = FreightFieldWidth
      Else
         If ?InvoiceList{PROPLIST:Width,4} ~= 0 Then
            FreightFieldWidth = ?InvoiceList{PROPLIST:Width,4}
         End
         ?InvoiceList{PROPLIST:Width,4} = 0
      End
    OF ?ReportBtn
      Update()
      If ~Check1 And ~Check2 And ~Check3 And ~Check4 Then
        Message('At least one section (branches, clients, months, invoices) should be selected.','TransIS - Required field',ICON:Asterisk)
        Cycle
      End
      If ReportMode ~= BShift(BShift(BShift(Check1,1)+Check2,1)+Check3,1)+Check4 Then
         Self.FillQRecords()
      End
      If ~Records(QRecords)
         Message('No data.','TransIS - Information',ICON:Asterisk)
         Cycle
      Else
         If ShowFreightCharge Then
            Print_FuelSurchargeReportWithFreightCharge(QRecords,BShift(BShift(BShift(Check1,1)+Check2,1)+Check3,1)+Check4,DateFrom,DateTo)
         Else
            Print_FuelSurchargeReport(QRecords,BShift(BShift(BShift(Check1,1)+Check2,1)+Check3,1)+Check4,DateFrom,DateTo)
         End
      End
      !Message('Not Implemented yet.','TransIS',ICON:Asterisk)      
    OF ?Cancel
      Post(EVENT:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',DateFrom)
      IF Calendar6.Response = RequestCompleted THEN
      DateFrom=Calendar6.SelectedDate
      DISPLAY(?DateFrom)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar7.SelectOnClose = True
      Calendar7.Ask('Select a Date',DateTo)
      IF Calendar7.Response = RequestCompleted THEN
      DateTo=Calendar7.SelectedDate
      DISPLAY(?DateTo)
      END
      ThisWindow.Reset(True)
    OF ?CallClientLookup
      ThisWindow.Update()
      CLI:ClientName = ClientString
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        ClientString = CLI:ClientName
        CurrentCID = CLI:CID
        ClientNo = CLI:ClientNo
      END
      ThisWindow.Reset(1)
    OF ?ClientString
      If 0
      IF NOT QuickWindow{PROP:AcceptAll}
        IF ClientString OR ?ClientString{PROP:Req}
          CLI:ClientName = ClientString
          IF Access:Clients.TryFetch(CLI:Key_ClientName)
            IF SELF.Run(1,SelectRecord) = RequestCompleted
              ClientString = CLI:ClientName
              CurrentCID = CLI:CID
              ClientNo = CLI:ClientNo
            ELSE
              CLEAR(CurrentCID)
              CLEAR(ClientNo)
              SELECT(?ClientString)
              CYCLE
            END
          ELSE
            CurrentCID = CLI:CID
            ClientNo = CLI:ClientNo
          END
        END
      END
      ThisWindow.Reset()
      End
    OF ?Refresh
      ThisWindow.Update()
      If Check2 = True And AllClients = False And CurrentCID = 0 Then
         Message('Please choose Client.','TransIS - Required Fields',ICON:Asterisk)
         Cycle
      End
      Self.FillQRecords
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    DatePeriodFilter.TakeEvent
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?InvoiceList
    CASE EVENT()
    OF EVENT:AlertKey
       If Records(QRecords)
          If KeyCode() = MouseRight Then            
             Case Popup('Contract All|Expand All')
             Of 1                
                Loop i# = 1 To Records(QRecords)
                     Get(QRecords,i#)                     
                     If Abs(QRecords.DisplayName_level) < MaxLevel Then
                        QRecords.DisplayName_level = - Abs(QRecords.DisplayName_level)
                        Put(QRecords)
                     End
                End
                Display(?InvoiceList)
             Of 2
                Loop i# = 1 To Records(QRecords)
                     Get(QRecords,i#)
                     QRecords.DisplayName_level = Abs(QRecords.DisplayName_level)
                     Put(QRecords)
                End
                Display(?InvoiceList)
             End
          End
          SetKeyCode(0)
       End
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

ThisWindow.FillQRecords PROCEDURE()

FilterString CString(1024)
OrderString  CString(1024)
!SearchStringClient CString(1024)
!SearchStringMonth  CString(1024)
Detailed     Byte
lvl          Long
l            Long
recordCount  Ulong
!BranchName   Like(BRA:BranchName)
GrandTotal   Like(TFuelSurchargeRecord)

  CODE
  ! Set filter and order
  FilterString = ''
  OrderString = ''
  !SearchStringClient = ''
  !SearchStringMonth  = ''
  Detailed = False
  MaxLevel = 0
  If Check1 Then !Branches
     MaxLevel += 1
     If Not InString('BranchName',OrderString,1,1) Then
        OrderString = ConString(OrderString,'BranchName',',')
        !SearchStringClient = ConString(SearchStringClient,'BID',',')
        !SearchStringMonth  = ConString(SearchStringMonth,'BID',',')
     End
     Get(QBranches,Choice(?BranchList))
     If ~ErrorCode() Then
        If QBranches.BID Then
           FilterString = ConString(FilterString,'(BID='&QBranches.BID&')',' AND ')
        End
     End
  End
  If Check2 Then !Client
     MaxLevel += 1
     If Not InString('ClientName',OrderString,1,1) Then
        OrderString = ConString(OrderString,'ClientName',',')
     End
     If AllClients = False Then
        If CurrentCID Then
           FilterString = ConString(FilterString,'(CID='&CurrentCID&')',' AND ')
        End
     End
  End
  If Check3 Then ! Month
     MaxLevel += 1
     If Not InString('Year,MonthNo',OrderString,1,1) Then
        OrderString = ConString(OrderString,'Year,MonthNo',',')
        !SearchStringMonth = ConString(SearchStringMonth,'Year,MonthNo',',')
     End
  End        
  If Check4 Then ! Invoices
     MaxLevel += 1
     Detailed = True
  End
  If DateFrom Then
     FilterString = ConString(FilterString,'InvoiceDateAndTime >= CONVERT(DATETIME,''' & Format(DateFrom,@D10-) & ''',102)',' AND ')     
  End
  If DateTo Then
     FilterString = ConString(FilterString,'InvoiceDateAndTime < CONVERT(DATETIME,''' & Format(DateTo+1,@D10-) & ''',102)',' AND ')     
  End
  OrderString = ConString(OrderString,'InvDate,InvTime',',')
  !STOP('OrderSting="'&OrderString&'"<13,10>FilterString="'&FilterString&'"')
  !Count records
  recordCount = GetSQLValue('Select Count(*) From _Invoice'&Choose(FilterString='','',' Where '&FilterString))
  !Message('recordCount = '&recordCount&'<13,10>Select Count(*) From _Invoice'&Choose(FilterString='','',' Where '&FilterString),,,,2)  
  ! Requesting data
  Clear(GrandTotal)
  Do ClearResultSet
  If recordCount = 0 Then
     Message('No data.','TransIS - Information',ICON:Asterisk)
     Return
  End
  i# = 0
  ProgressPercent = 0
  UnHide(?Progress1)
  UnHide(?ProgressPercent)
  Display(?InvoiceList)
  !Message('SELECT IID,POD_IID,CR_IID,BID,BRANCHNAME,CID,CLIENTNO,CLIENTNAME,CLIENTLINE1,CLIENTLINE2,CLIENTSUBURB,CLIENTPOSTALCODE,VATNO,INVOICEMESSAGE,DID,DINO,CLIENTREFERENCE,MIDS,TERMS,ICID,SHIPPERNAME,SHIPPERLINE1,SHIPPERLINE2,SHIPPERSUBURB,SHIPPERPOSTALCODE,CONSIGNEENAME,CONSIGNEELINE1,CONSIGNEELINE2,CONSIGNEESUBURB,CONSIGNEEPOSTALCODE,INVOICEDATEANDTIME,PRINTED,INSURANCE,DOCUMENTATION,FUELSURCHARGE,ADDITIONALCHARGE,FREIGHTCHARGE,VAT,TOTAL,VATRATE,WEIGHT,VOLUMETRICWEIGHT,VOLUME,BADDEBT,STATUS,STATUSUPTODATE,MID,UID,DC_ID,IJID,CREATED_DATETIME FROM dbo._Invoice'&Choose(FilterString='','',' Where '&FilterString),'Debug message',,,,2)
  If RunSQLRequest('SELECT IID,POD_IID,CR_IID,BID,BRANCHNAME,CID,CLIENTNO,CLIENTNAME,CLIENTLINE1,CLIENTLINE2,CLIENTSUBURB,CLIENTPOSTALCODE,VATNO,INVOICEMESSAGE,DID,DINO,CLIENTREFERENCE,MIDS,TERMS,ICID,SHIPPERNAME,SHIPPERLINE1,SHIPPERLINE2,SHIPPERSUBURB,SHIPPERPOSTALCODE,CONSIGNEENAME,CONSIGNEELINE1,CONSIGNEELINE2,CONSIGNEESUBURB,CONSIGNEEPOSTALCODE,INVOICEDATEANDTIME,PRINTED,INSURANCE,DOCUMENTATION,FUELSURCHARGE,ADDITIONALCHARGE,FREIGHTCHARGE,VAT,TOTAL,VATRATE,WEIGHT,VOLUMETRICWEIGHT,VOLUME,BADDEBT,STATUS,STATUSUPTODATE,MID,UID,DC_ID,IJID,CREATED_DATETIME FROM dbo._Invoice'&Choose(FilterString='','',' Where '&FilterString),_Invoice) Then
     Loop
          If Access:_Invoice.TryNext() ~= Level:Benign Then Break .
          ! Add a record to the Queue for each concerned level
          l = 0 ! indicates current tree level
          BranchName = ''
          lClientName = ''
          Self.CalcInvRecord(GrandTotal)
          If Check1 Then ! Branch
             l += 1
             Clear(QRecords)
             QRecords.BID = INV:BID             
             ! seach on specific BID and all other criteria = 0
             Get(QRecords,QRecords.BID,QRecords.CID,QRecords.Year,QRecords.MonthNo,QRecords.IID)             
             If ErrorCode() Then
                Clear(QRecords)
                QRecords.BID = INV:BID
                BranchName = Self.SetBranchName(INV:BID)
                QRecords.DisplayName = BranchName
                Self.CalcInvRecord(QRecords)
                QRecords.DisplayName_icon  = 1
                QRecords.DisplayName_level = l
                Add(QRecords)
             Else
                Self.CalcInvRecord(QRecords)
                Put(QRecords)
             End
          End          
          If Check2 Then ! Client
             l += 1
             Clear(QRecords)
             If InString('BranchName',OrderString,1,1) Then !set BID only if branch is taken into account
                QRecords.BID = INV:BID
             End
             QRecords.CID = INV:CID
             ! seach on specific CID (and probably BID) and all other criteria = 0
             Get(QRecords,QRecords.BID,QRecords.CID,QRecords.Year,QRecords.MonthNo,QRecords.IID)
             If ErrorCode() Then
                Clear(QRecords)                     
                QRecords.CID = INV:CID
                lClientName = Self.SetClientName(INV:CID)
                QRecords.DisplayName = lClientName
                If InString('BranchName',OrderString,1,1) Then QRecords.BID = INV:BID .
                BranchName = Self.SetBranchName(INV:BID)
                Self.CalcInvRecord(QRecords)
                QRecords.DisplayName_icon  = 2
                QRecords.DisplayName_level = l
                Add(QRecords)
             Else   
                Self.CalcInvRecord(QRecords)
                Put(QRecords)
             End
          End          
          If Check3 Then ! Month
             l += 1
             Clear(QRecords)
             If InString('BranchName',OrderString,1,1) Then !set BID only if Branch is taken into account
                QRecords.BID = INV:BID
             End
             If InString('ClientName',OrderString,1,1) Then !set CID only if Client is taken into account
                QRecords.CID = INV:CID
             End                  
             QRecords.Year  = Year(INV:InvoiceDate)
             QRecords.MonthNo = Month(INV:InvoiceDate)
             ! seach on specific Year and MonthNo (and probably BID an CID) with all other criteria = 0
             Get(QRecords,QRecords.BID,QRecords.CID,QRecords.Year,QRecords.MonthNo,QRecords.IID)
             If ErrorCode() Then
                Clear(QRecords)
                QRecords.Month = Upper(GetMonthName(Month(INV:InvoiceDate)))
                QRecords.DisplayName = QRecords.Month
                If InString('ClientName',OrderString,1,1) Then QRecords.CID = INV:CID .
                lClientName = Self.SetClientName(INV:CID)
                If InString('BranchName',OrderString,1,1) Then QRecords.BID = INV:BID .
                BranchName = Self.SetBranchName(INV:BID)
                QRecords.Year    = Year(INV:InvoiceDate)
                QRecords.MonthNo = Month(INV:InvoiceDate)
                Self.CalcInvRecord(QRecords)
                QRecords.DisplayName_icon  = 3
                QRecords.DisplayName_level = l
                Add(QRecords)
             Else
                Self.CalcInvRecord(QRecords)
                Put(QRecords)
             End
          End
          If Check4 Then ! Invoice
             l += 1
             Clear(QRecords)
             QRecords.IID  = INV:IID
             QRecords.DisplayName = 'Invoice No.: ' & INV:IID
             QRecords.InvoiceDate = INV:InvoiceDate
             QRecords.CID = INV:CID
             lClientName = Self.SetClientName(INV:CID)
             QRecords.BID = INV:BID
             BranchName = Self.SetBranchName(INV:BID)
             QRecords.Year  = Year(INV:InvoiceDate)
             QRecords.MonthNo = Month(INV:InvoiceDate)
             QRecords.InvDate = INV:InvoiceDate
             QRecords.InvTime = INV:InvoiceTime
             Self.CalcInvRecord(QRecords)
             QRecords.DisplayName_icon  = 4
             QRecords.DisplayName_level = l
             Add(QRecords)
          End
          i# += 1
          ProgressPercent = Int(i#*100/recordCount)
          Display(?Progress1)
          Display(?ProgressPercent)
          Yield
     End
     Sort(QRecords,OrderString)
     Clear(QRecords)
     QRecords :=: GrandTotal
     QRecords.DisplayName = 'Grand Total:'
     !QRecords.DisplayName_icon  = 0
     !QRecords.DisplayName_level = 0
     Add(QRecords)
  End
  Hide(?Progress1)
  Hide(?ProgressPercent)
  If Records(QRecords) THEN
     Enable(?ReportBtn)
     ?InvoiceList{PROP:Selected} = 1
  End
  ReportMode = BShift(BShift(BShift(Check1,1)+Check2,1)+Check3,1)+Check4  


ThisWindow.SetBranchName PROCEDURE(ULong pBID)

s CString(36)

  CODE
  s = BranchName
  If s = '' THEN
     If pBID = 0 THEN
        s = 'NO BRANCH selected'
     Else
        s = GetSQLVAlue('Select BranchName From Branches Where BID = '&pBID)
        If Clip(s) = '' THEN
           s = 'Unknown BRANCH (ID: '&pBID&')'
        End
     End
  End
  QRecords.BranchName    = Upper(Clip(s))
  Return s


ThisWindow.SetClientName PROCEDURE(ULong pCID)

s CString(101)

  CODE
  s = lClientName
  If s = '' THEN
     If pCID = 0 THEN
        s = 'NO Client specified'
     Else
        s = GetSQLVAlue('Select ClientName From Clients Where CID = '&pCID)
        If Clip(s) = '' THEN
           s = 'Unknown Client (ID: '&pCID&')'
        !Else
        !   s = Clip(s) & ' (No.:'&Clip(GetSQLVAlue('Select ClientNo From Clients Where CID = '&pCID))&')'
        End
     End
  End
  QRecords.ClientName    = Upper(Clip(s))
  Return s


ThisWindow.CalcInvRecord PROCEDURE(TFuelSurchargeRecord Q)

  CODE
  Q.Total                += INV:Total - Choose(InclVAT=1,0.00,INV:VAT)
  Q.TotalExVAT           += INV:Total - INV:VAT
  Q.FreightCharge        += INV:FreightCharge
  Q.FuelSurcharge        += INV:FuelSurcharge
  If Q.TotalExVAT = 0 THEN
     Q.FuelSurchargePercent = 0.00
  Else
     Q.FuelSurchargePercent = Q.FuelSurcharge * 100.00 / Q.TotalExVAT
  End


ThisWindow.ShowClient PROCEDURE(String pTitle)

  CODE
  !Stop(pTitle&|
  !  '<13,10>Client No.:'&ClientNo&|
  !  '<13,10>ClientName:'&ClientString&|
  !  '<13,10>CurrentCID:'&CurrentCID)



Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?PeriodPrompt, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?PeriodPrompt
  SELF.SetStrategy(?PeriodList, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?PeriodList
  SELF.SetStrategy(?DateFrom:Prompt, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?DateFrom:Prompt
  SELF.SetStrategy(?DateFrom, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?DateFrom
  SELF.SetStrategy(?Calendar, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?Calendar
  SELF.SetStrategy(?DateTo:Prompt, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?DateTo:Prompt
  SELF.SetStrategy(?DateTo, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?DateTo
  SELF.SetStrategy(?DateTo, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?DateTo
  SELF.SetStrategy(?Calendar:2, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?Calendar:2
  SELF.SetStrategy(?CriteriaGroup, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?CriteriaGroup
  SELF.SetStrategy(?BranchList, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?BranchList
  SELF.SetStrategy(?CHECK1, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?CHECK1
  SELF.SetStrategy(?CHECK2, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?CHECK2
  SELF.SetStrategy(?CHECK3, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?CHECK3
  SELF.SetStrategy(?Refresh, Resize:LockXPos+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Refresh
  SELF.SetStrategy(?InvoiceList, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom) ! Override strategy for ?InvoiceList
  SELF.SetStrategy(?ReportBtn, Resize:LockXPos+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?ReportBtn
  SELF.SetStrategy(?Progress1, Resize:LockXPos+Resize:FixBottom, Resize:ConstantRight+Resize:LockHeight) ! Override strategy for ?Progress1
  SELF.SetStrategy(?ProgressPercent, Resize:FixRight+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?ProgressPercent
  SELF.SetStrategy(?ClientNo:Prompt, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?ClientNo:Prompt
  SELF.SetStrategy(?ClientNo, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?ClientNo
  SELF.SetStrategy(?InclVAT, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?InclVAT
  SELF.SetStrategy(?ShowFreightCharge, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?ShowFreightCharge

DatePeriodFilter.FillQPeriod      PROCEDURE(ULONG pPeriodEquates)
 CODE
  Parent.FillQPeriod(pPeriodEquates)

  SELF.QPeriod.PeriodID=Period:ThisYear
  GET(SELF.QPeriod,SELF.QPeriod.PeriodID)
  IF ~ErrorCode()
     SELF.QPeriod.DateFrom=DATE(1,1,YEAR(TODAY()))
     SELF.QPeriod.DateTo=DATE(12,31,YEAR(TODAY()))
     PUT(SELF.QPeriod)
  END

  SELF.QPeriod.PeriodID=Period:LastYear
  GET(SELF.QPeriod,SELF.QPeriod.PeriodID)
  IF ~ErrorCode()
     SELF.QPeriod.DateFrom=DATE(1,1,YEAR(TODAY())-1)
     SELF.QPeriod.DateTo=DATE(12,31,YEAR(TODAY())-1)
     PUT(SELF.QPeriod)
  END


DatePeriodFilter.GetPeriodName    PROCEDURE(ULONG pPeriodEquate)
retStr    String(50)
 CODE
  CASE pPeriodEquate
  OF Period:ThisYear
    retStr='This Year'
  OF Period:LastYear
    retStr='Last Year'
  ELSE
    retStr=PARENT.GetPeriodName(pPeriodEquate)
  END
  RETURN retStr
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
GetMonthName         PROCEDURE  (pMonthNo)                 ! Declare Procedure
RetVal CString(10)

  CODE
  !stop(pMonthNo)
  If pMonthNo < 1 Then Return RetVal .
  Case (pMonthNo%12)
  Of 1
     RetVal = 'January'
  Of 2
     RetVal = 'February'
  Of 3
     RetVal = 'March'
  Of 4
     RetVal = 'April'
  Of 5
     RetVal = 'May'
  Of 6
     RetVal = 'June'
  Of 7
     RetVal = 'July'
  Of 8
     RetVal = 'August'
  Of 9
     RetVal = 'September'
  Of 10
     RetVal = 'October'
  Of 11
     RetVal = 'November'
  Of 0
     RetVal = 'December'
  End
  Return RetVal
!!! <summary>
!!! Generated from procedure template - Report
!!! Report the _Invoice File
!!! </summary>
Print_FuelSurchargeReport PROCEDURE (TFuelSurchargeQueue Q, Byte pMode, Long pDateFrom, Long pDateTo)

Progress:Thermometer BYTE                                  ! 
YearMonth            CSTRING(16)                           ! 
MonthYear            STRING(18)                            ! 
BranchTotal   Group(TFuelSurchargeRecord),PRE(B)
Printed         Byte
              End
ClientTotal   Group(TFuelSurchargeRecord),PRE(C)
Printed         Byte
              End              
MonthTotal    Group(TFuelSurchargeRecord),PRE(M)
Printed         Byte
              End
InvoiceRecord Group(TFuelSurchargeRecord),PRE(I)
Printed         Byte
              End
GrandTotal    Group(TFuelSurchargeRecord),PRE(G)
Printed         Byte
              End
UpperLevelChanged Byte
Process:View         VIEW(_Invoice)
                     END
ProgressWindow       WINDOW('Report _Invoice'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('_Invoice Report'),AT(250,1100,7750,10083),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular,CHARSET:DEFAULT),THOUS
                       HEADER,AT(250,250,7750,850),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT)
                         STRING('Fuel Surcharge Report'),AT(0,20,7750,302),USE(?ReportTitle),FONT('MS Sans Serif',18, |
  ,FONT:bold,CHARSET:DEFAULT),CENTER
                         BOX,AT(0,600,7750,250),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(1552,604,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(3104,604,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(4646,604,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(6198,604,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         STRING('Invoice Date'),AT(50,640,1450,170),USE(?HeaderTitle:1),TRN
                         STRING('Invoice Number'),AT(1600,640,1450,170),USE(?HeaderTitle:2),TRN
                         STRING('Total'),AT(3150,640,1450,170),USE(?HeaderTitle:3),TRN
                         STRING('Fuel Surcharge'),AT(4700,640,1450,170),USE(?HeaderTitle:4),TRN
                         STRING('Fuel Surcharge %'),AT(6250,640,1450,170),USE(?HeaderTitle:5),TRN
                         STRING(''),AT(3927,406,3771,167),USE(?DateRangeString),RIGHT(50)
                       END
LevelSeparator         DETAIL,AT(0,0,7750,250),USE(?LevelSeparator)
                         BOX,AT(0,0,7750,250),USE(?HeaderBox:1),COLOR(COLOR:White),LINEWIDTH(1)
                       END
Break_Branch           BREAK(B:BID),USE(?BREAK1)
                         HEADER,AT(0,0,7750,0),USE(?Hdr_Branch)
                           STRING('Branch:'),AT(52,31),USE(?BranchNameTitle)
                           STRING(@s35),AT(635,31,4906,167),USE(B:BranchName)
                           LINE,AT(0,250,7750,0),USE(?BranchEndLine),COLOR(COLOR:Green)
                         END
Break_Client             BREAK(C:CID),USE(?BREAK2)
                           HEADER,AT(0,0,7750,0),USE(?Hdr_Client)
                             STRING('Client:'),AT(52,31),USE(?ClientNameTitle)
                             STRING(@s100),AT(635,31,4906,167),USE(C:ClientName)
                             LINE,AT(0,250,7750,0),USE(?ClientEndLine),COLOR(COLOR:Black)
                           END
Break_Month                BREAK(YearMonth),USE(?BREAK3)
                             HEADER,AT(0,0,7750,0),USE(?Hdr_Month)
                               STRING(@s19),AT(52,31,1448,167),USE(MonthYear)
                               LINE,AT(0,250,7750,0),USE(?MonthEndLine),COLOR(COLOR:Black)
                             END
BranchData                   DETAIL,AT(0,0,7750,250),USE(?BranchData)
                               LINE,AT(0,0,0,250),USE(?BranchLine:0),COLOR(COLOR:Black)
                               LINE,AT(3104,0,0,250),USE(?BranchLine:2),COLOR(COLOR:Black)
                               LINE,AT(4646,0,0,250),USE(?BranchLine:3),COLOR(COLOR:Black)
                               LINE,AT(6198,0,0,250),USE(?BranchLine:4),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?BranchLine:5),COLOR(COLOR:Black)
                               STRING(@n-15.2),AT(3150,50,1450,170),USE(B:Total),RIGHT(50)
                               STRING(@n-14.2),AT(4700,50,1450,170),USE(B:FuelSurcharge),RIGHT(50)
                               STRING(@n-7.2),AT(6250,50,1450,170),USE(B:FuelSurchargePercent),RIGHT(50)
                               LINE,AT(0,250,7750,0),USE(?BranchEndLine:2),COLOR(COLOR:Black)
                               STRING(@s36),AT(52,52,2385,167),USE(B:BranchName,,?B:BranchName:2)
                             END
ClientData                   DETAIL,AT(0,0,7750,250),USE(?ClientData)
                               LINE,AT(0,0,0,250),USE(?ClientLine:0),COLOR(COLOR:Black)
                               LINE,AT(3104,0,0,250),USE(?ClientLine:2),COLOR(COLOR:Black)
                               LINE,AT(4646,0,0,250),USE(?ClientLine:3),COLOR(COLOR:Black)
                               LINE,AT(6198,0,0,250),USE(?ClientLine:4),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?ClientLine:5),COLOR(COLOR:Black)
                               STRING(@n-15.2),AT(3150,50,1450,170),USE(C:Total),RIGHT(50)
                               STRING(@n-14.2),AT(4700,50,1450,170),USE(C:FuelSurcharge),RIGHT(50)
                               STRING(@n-7.2),AT(6250,50,1450,170),USE(C:FuelSurchargePercent),RIGHT(50)
                               LINE,AT(0,250,7750,0),USE(?ClientEndLine:2),COLOR(COLOR:Black)
                               STRING(@s100),AT(52,42,3000,167),USE(C:ClientName,,?C:ClientName:2)
                             END
MonthData                    DETAIL,AT(0,0,7750,250),USE(?MonthData)
                               LINE,AT(0,0,0,250),USE(?MonthLine:0),COLOR(COLOR:Black)
                               LINE,AT(3104,0,0,250),USE(?MonthLine:2),COLOR(COLOR:Black)
                               LINE,AT(4646,0,0,250),USE(?MonthLine:3),COLOR(COLOR:Black)
                               LINE,AT(6198,0,0,250),USE(?MonthLine:4),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?MonthLine:5),COLOR(COLOR:Black)
                               STRING(@n-15.2),AT(3150,50,1450,170),USE(M:Total),RIGHT(50)
                               STRING(@n-14.2),AT(4700,50,1450,170),USE(M:FuelSurcharge),RIGHT(50)
                               STRING(@n-7.2),AT(6250,50,1450,170),USE(M:FuelSurchargePercent),RIGHT(50)
                               STRING(@s19),AT(52,31,2385,167),USE(MonthYear,,?MonthYear:2)
                               LINE,AT(0,250,7750,0),USE(?MonthEndLine:2),COLOR(COLOR:Black)
                             END
Detail                       DETAIL,AT(0,0,7750,250),USE(?Detail)
                               LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                               LINE,AT(1552,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                               LINE,AT(3104,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                               LINE,AT(4646,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                               LINE,AT(6198,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                               STRING(@d6),AT(50,50,1450,170),USE(I:InvoiceDate)
                               STRING(@n_10),AT(1600,50,1450,170),USE(I:IID),RIGHT(50)
                               STRING(@n-15.2),AT(3150,50,1450,170),USE(I:Total),RIGHT(50)
                               STRING(@n-14.2),AT(4700,50,1450,170),USE(I:FuelSurcharge),RIGHT(50)
                               STRING(@n-7.2),AT(6250,50,1450,170),USE(I:FuelSurchargePercent),RIGHT(50)
                               LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                             END
NoDetails                    DETAIL,AT(0,0,7750,0),USE(?NoDetails)
                             END
                             FOOTER,AT(0,0,7750,0),USE(?Ftr_Month)
                               LINE,AT(0,0,0,250),USE(?MonthLine:0:2),COLOR(COLOR:Black)
                               STRING(@s16),AT(312,31,1187,167),USE(M:Month,,?MonthName:2)
                               LINE,AT(3104,0,0,250),USE(?MonthLine:6),COLOR(COLOR:Black)
                               LINE,AT(4646,0,0,250),USE(?MonthLine:7),COLOR(COLOR:Black)
                               LINE,AT(6198,0,0,250),USE(?MonthLine:8),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?MonthLine:9),COLOR(COLOR:Black)
                               STRING(@n-15.2),AT(3150,50,1450,170),USE(M:Total,,?INV:Total:2),RIGHT(50)
                               STRING(@n-14.2),AT(4700,50,1450,170),USE(M:FuelSurcharge,,?INV:FuelSurcharge:2),RIGHT(50)
                               STRING(@n-7.2),AT(6250,50,1450,170),USE(M:FuelSurchargePercent,,?INV:VATRate:2),RIGHT(50)
                               LINE,AT(0,250,7750,0),USE(?MonthEndLine:3),COLOR(COLOR:Black)
                             END
                           END
                           FOOTER,AT(0,0,7750,0),USE(?Ftr_Client)
                             LINE,AT(52,52,7676,0),USE(?ClientLine:6),COLOR(COLOR:Black)
                             STRING('Client Total'),AT(313,104),USE(?ClientTotalString),TRN
                             STRING(@n-15.2),AT(3150,104,1450,167),USE(C:Total,,?C:Total:2),RIGHT(50)
                             STRING(@n-14.2),AT(4700,104,1450,167),USE(C:FuelSurcharge,,?INV:FuelSurcharge:3),RIGHT(50)
                             STRING(@n-15.2),AT(6250,104,1450,167),USE(C:FuelSurchargePercent,,?INV:VATRate:3),RIGHT(50)
                             LINE,AT(52,312,7676,0),USE(?ClientLine:7),COLOR(COLOR:Black)
                           END
                         END
                         FOOTER,AT(0,0,7750,0),USE(?Ftr_Branch)
                           LINE,AT(52,52,7676,0),USE(?BranchLine:6),COLOR(COLOR:Black)
                           STRING('Branch Total'),AT(313,104),USE(?BranchTotalString),TRN
                           STRING(@n-15.2),AT(3150,104,1450,167),USE(B:Total,,?INV:Total:3),RIGHT(50)
                           STRING(@n-14.2),AT(4700,104,1450,167),USE(B:FuelSurcharge,,?INV:FuelSurcharge:4),RIGHT(50)
                           STRING(@n-15.2),AT(6250,104,1450,167),USE(B:FuelSurchargePercent,,?INV:VATRate:4),RIGHT(50)
                           LINE,AT(52,312,7676,0),USE(?Line19:4),COLOR(COLOR:Black)
                         END
                       END
grandtotals            DETAIL,AT(0,0,,396),USE(?grandtotals)
                         LINE,AT(52,52,7676,0),USE(?Line19),COLOR(COLOR:Black)
                         STRING('Grand Total'),AT(313,104),USE(?String27:2),TRN
                         LINE,AT(52,312,7676,0),USE(?Line19:2),COLOR(COLOR:Black)
                         STRING(@n-15.2),AT(3150,104,1450,167),USE(G:Total),RIGHT(50)
                         STRING(@n-14.2),AT(4700,104,1450,167),USE(G:FuelSurcharge,,?INV:FuelSurcharge),RIGHT(50)
                         STRING(@n-15.2),AT(6250,104,1450,167),USE(G:FuelSurchargePercent,,?INV:VATRate),RIGHT(50)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt:2),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp:2),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt:2),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp:2),FONT('Arial',8,, |
  FONT:regular),TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount:2),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Next                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
BreakMgr             BreakManagerClass                     ! Break Manager
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_FuelSurchargeReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:_Invoice.SetOpenRelated()
  Relate:_Invoice.Open                                     ! File _Invoice used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  If False !do not use INV:...
  BreakMgr.Init()
  BreakMgr.AddBreak()
  BreakMgr.AddLevel() !Break_Branch
  BreakMgr.AddResetField(INV:BID)
  BreakMgr.AddLevel() !Break_Client
  BreakMgr.AddResetField(INV:CID)
  BreakMgr.AddLevel() !Break_Month
  BreakMgr.AddResetField(YearMonth)
  SELF.AddItem(BreakMgr)
  End !do not use INV:...
  BreakMgr.Init()
  BreakMgr.AddBreak()
  BreakMgr.AddLevel() !Break_Branch
  BreakMgr.AddResetField(B:BID)
  BreakMgr.AddLevel() !Break_Client
  BreakMgr.AddResetField(C:CID)
  BreakMgr.AddLevel() !Break_Month
  BreakMgr.AddResetField(YearMonth)
  SELF.AddItem(BreakMgr)  
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_FuelSurchargeReport',ProgressWindow) ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF NOT TargetSelector.ASK(1) THEN
     SELF.Kill()
     RETURN Level:Fatal
  END
  IF NOT TargetSelector.GetPrintSelected() THEN
     SELF.SetReportTarget(TargetSelector.GetReportSelected())
  END
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:_Invoice, ?Progress:PctText, Progress:Thermometer, RECORDS(Q))
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Invoice.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_FuelSurchargeReport',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Next PROCEDURE

ReturnValue          BYTE,AUTO

Progress BYTE,AUTO
  CODE
      ThisReport.RecordsProcessed+=1
      GET(Q,ThisReport.RecordsProcessed)
      IF ERRORCODE() THEN
         ReturnValue = Level:Notify
      ELSE
         ReturnValue = Level:Benign
      END
      IF ReturnValue = Level:Notify
          IF ThisReport.RecordsProcessed>RECORDS(Q)
             SELF.Response = RequestCompleted
             POST(EVENT:CloseWindow)
             RETURN Level:Notify
          ELSE
             SELF.Response = RequestCancelled
             POST(EVENT:CloseWindow)
             RETURN Level:Fatal
          END
      ELSE
         Progress = ThisReport.RecordsProcessed / ThisReport.RecordsToProcess*100
         IF Progress > 100 THEN Progress = 100.
         IF Progress <> Progress:Thermometer
           Progress:Thermometer = Progress
           DISPLAY()
         END
      END
      RETURN Level:Benign
  ReturnValue = PARENT.Next()
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  If ReturnValue = Level:Benign Then
     SetTarget(Self.Report)
     If BAnd(pMode,01111b) = 8 Or BAnd(pMode,01000b) = 0 Then
        Hide(?Hdr_Branch,?BranchEndLine)
        Hide(?Ftr_Branch,?Line19:4)
     Else
        UnHide(?Hdr_Branch,?BranchEndLine)
        If BAnd(pMode,01111b) > 8 And BAnd(pMode,00111b) > 4
           Hide(?BranchEndLine)
        End
        UnHide(?Ftr_Branch,?Line19:4)
     End
     If BAnd(pMode,00111b) = 4 Or BAnd(pMode,00100b) = 0 Then
        Hide(?Hdr_Client,?ClientEndLine)
        Hide(?Ftr_Client,?ClientLine:7)
     Else
        UnHide(?Hdr_Client,?ClientEndLine)
        If BAnd(pMode,00111b) > 4 And BAnd(pMode,00011b) > 2
           Hide(?ClientEndLine)
        End
        UnHide(?Ftr_Client,?ClientLine:7)
     End
     If BAnd(pMode,00011b) = 2 or BAnd(pMode,00010b) = 0 Then
        Hide(?Hdr_Month,?MonthEndLine)
        Hide(?Ftr_Month,?MonthEndLine:3)
     Else
        UnHide(?Hdr_Month,?MonthEndLine)
        UnHide(?Ftr_Month,?MonthEndLine:3)
     End
     If pDateFrom Or pDateTo Then
        ?DateRangeString{PROP:Text} = Choose(pDateFrom=0,'...',Format(pDateFrom,@D6B)) & ' - ' & Choose(pDateTo=0,'...',Format(pDateTo,@D6B))
        UnHide(?DateRangeString)
     Else
        Hide(?DateRangeString)
     End
     SetTarget()
  End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp:2{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp:2{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  UpperLevelChanged = False
  If pMode > 8
     If B:BID And Q.BID And Q.BID ~= B:BID Then
        UpperLevelChanged = True
        !stop('pMode='&pMode&' Branch changed. BID='&B:BID&' Q.BID='&Q.BID)
     End
  ElsIf pMode > 4
     If C:CID And Q.CID And Q.CID ~= C:CID THEN
        UpperLevelChanged = True
        !stop('pMode='&pMode&' Client changed. CID='&C:CID&' Q.CID='&Q.CID)
     End
  ElsIf pMode > 2
     If M:Year And M:MonthNo And Q.MonthNo And (Q.Year ~= M:Year Or Q.MonthNo ~= M:MonthNo) THEN
        UpperLevelChanged = True
        !stop('pMode='&pMode&' Month changed. M:Year='&M:Year&' M:MonthNo='&M:MonthNo&' Q.Year='&Q.Year&' Q.MonthNo='&Q.MonthNo)
     End
  End
  !stop('Pointer:'&Pointer(Q)& '. UpperLevelChanged = '&UpperLevelChanged)
  If Q.IID Then ! Invoice
     InvoiceRecord :=: Q
     I:Printed = 0
  ElsIf Q.MonthNo Then ! Month
     MonthTotal :=: Q
     M:Printed = 0
     YearMonth = M:Year & '-' & M:Month
     If Year(pDateFrom) ~= Year(pDateTo) THEN
        MonthYear = M:Month &' ('&M:Year&')'
     Else
        MonthYear = M:Month
     End
     Clear(InvoiceRecord)
     !stop('Month record. YearMonth: '&YearMonth)
  ElsIf Q.CID
     Clear(InvoiceRecord)
     If C:CID ~= Q.CID Then ! only if changed
        If BAnd(pMode,00011b) = 2 Then ! always clear if the last level
           Clear(MonthTotal)
        End
     End
     ClientTotal :=: Q
     C:Printed = 0
     !stop('Client record. '&Choose(C:CID ~= Q.CID,'Changed.','The same.')& 'C:CID: '&C:CID)
  ElsIf Q.BID
     Clear(InvoiceRecord)
     If B:CID ~= Q.BID Then ! only if changed
        If BAnd(pMode,00111b) = 4 Then ! always clear if the last level
           Clear(ClientTotal)
        End
        If BAnd(pMode,00011b) = 2 Then ! always clear if the last level
           Clear(MonthTotal)
        End        
     End
     BranchTotal :=: Q
     B:Printed = 0
     !stop('Branch record. '&Choose(B:CID ~= Q.BID,'Changed.','The same.')& 'B:BID: '&B:BID)
  ELSE
     GrandTotal :=: Q
     If BAnd(pMode,01111b) = 8 Then ! always clear if the last level
        Clear(BranchTotal)
     End
     If BAnd(pMode,00111b) = 4 Then ! always clear if the last level
        Clear(ClientTotal)
     End
     If BAnd(pMode,00011b) = 2 Then ! always clear if the last level
        Clear(MonthTotal)
     End        
     !stop('Branch: '&Choose(B:CID ~= Q.BID,'Changed.','The same.')& 'B:BID: '&B:BID&|
     !     'Client: '&Choose(C:CID ~= Q.CID,'Changed.','The same.')& 'C:CID: '&C:CID&|
     !     'YearMonth: '&YearMonth |
     !)
  End
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  If 0 ! for debug
  IF BAND(pMode,01111)=8 AND B:BID~=0
    PRINT(RPT:BranchData)
  END
  IF BAND(pMode,00111)=4 AND C:CID~=0
    PRINT(RPT:ClientData)
  END
  IF BAND(pMode,00011)=2 AND M:MonthNo~=0
    PRINT(RPT:MonthData)
  END
  IF BAND(pMode,00001)=1 AND I:IID~=0
    PRINT(RPT:Detail)
  END
  IF False !BAND(pMode,00001)=0
    PRINT(RPT:NoDetails)
  END
  IF G:Total Or G:FuelSurcharge Or G:FuelSurchargePercent
    PRINT(RPT:grandtotals)
  END
  IF ~SkipDetails
    PRINT(RPT:LevelSeparator)
  END
  End !if 0
  IF UpperLevelChanged
    PRINT(RPT:LevelSeparator)
    !stop('PRINT(RPT:LevelSeparator)')
  END
  !Stop(pMode&', B:BID='&B:BID&', C:CID='&C:CID&', M:MonthNo='&M:MonthNo)
  IF BAND(pMode,01111b)=8 AND B:BID~=0 And B:Printed = 0
    PRINT(RPT:BranchData)
    !stop('PRINT(RPT:BranchData)')
    B:Printed = 1
  END
  IF BAND(pMode,00111b)=4 AND C:CID~=0  And C:Printed = 0
    PRINT(RPT:ClientData)
    !stop('PRINT(RPT:ClientData)')
    C:Printed = 1
  END  
  IF BAND(pMode,00011b)=2 AND M:MonthNo~=0 And M:Printed = 0
    PRINT(RPT:MonthData)
    !stop('PRINT(RPT:MonthData)')
    M:Printed = 1
  END
  IF BAND(pMode,00001b)=1 AND I:IID~=0 And I:Printed = 0
    PRINT(RPT:Detail)
    !stop('PRINT(RPT:Detail)')
    I:Printed = 1
  END
  IF False !BAND(pMode,00001b)=0
    PRINT(RPT:NoDetails)
    !stop('PRINT(RPT:NoDetails)')
  END
  IF G:Total Or G:FuelSurcharge Or G:FuelSurchargePercent
    PRINT(RPT:grandtotals)
    !stop('PRINT(RPT:grandtotals)')
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','FuelSurchargeReport','FuelSurchargeReport','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! Report the _Invoice File
!!! </summary>
Print_FuelSurchargeReportWithFreightCharge PROCEDURE (TFuelSurchargeQueue Q, Byte pMode, Long pDateFrom, Long pDateTo)

Progress:Thermometer BYTE                                  ! 
YearMonth            CSTRING(16)                           ! 
MonthYear            STRING(18)                            ! 
BranchTotal   Group(TFuelSurchargeRecord),PRE(B)
Printed         Byte
              End
ClientTotal   Group(TFuelSurchargeRecord),PRE(C)
Printed         Byte
              End              
MonthTotal    Group(TFuelSurchargeRecord),PRE(M)
Printed         Byte
              End
InvoiceRecord Group(TFuelSurchargeRecord),PRE(I)
Printed         Byte
              End
GrandTotal    Group(TFuelSurchargeRecord),PRE(G)
Printed         Byte
              End
UpperLevelChanged Byte
Process:View         VIEW(_Invoice)
                     END
ProgressWindow       WINDOW('Report _Invoice'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('_Invoice Report'),AT(250,1100,7750,10083),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular,CHARSET:DEFAULT),THOUS
                       HEADER,AT(250,250,7750,850),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT)
                         STRING('Fuel Surcharge Report'),AT(0,20,7750,302),USE(?ReportTitle),FONT('MS Sans Serif',18, |
  ,FONT:bold,CHARSET:DEFAULT),CENTER
                         BOX,AT(0,600,7750,250),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(1552,604,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(3104,604,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(5531,604,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(6594,604,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         STRING('Invoice Date'),AT(50,640,1450,170),USE(?HeaderTitle:1),TRN
                         STRING('Invoice Number'),AT(1600,640,1450,170),USE(?HeaderTitle:2),TRN
                         STRING('Total'),AT(3150,640,1450,170),USE(?HeaderTitle:3),TRN
                         STRING('Fuel Surcharge'),AT(5583,635,969,170),USE(?HeaderTitle:4),TRN
                         STRING('Fuel Surcharge %'),AT(6667,635,1031,170),USE(?HeaderTitle:5),TRN
                         STRING(''),AT(3927,406,3771,167),USE(?DateRangeString),RIGHT(50)
                         LINE,AT(4208,604,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         STRING('Freight Charge'),AT(4281,635,969,167),USE(?HeaderTitle:6),TRN
                       END
LevelSeparator         DETAIL,AT(0,0,7750,250),USE(?LevelSeparator)
                         BOX,AT(0,0,7750,250),USE(?HeaderBox:2),COLOR(COLOR:White),LINEWIDTH(1)
                       END
Break_Branch           BREAK(B:BID),USE(?BREAK1)
                         HEADER,AT(0,0,7750,0),USE(?Hdr_Branch)
                           STRING('Branch:'),AT(52,31),USE(?BranchNameTitle)
                           STRING(@s35),AT(635,31,4906,167),USE(B:BranchName)
                           LINE,AT(0,250,7750,0),USE(?BranchEndLine),COLOR(COLOR:Green)
                         END
Break_Client             BREAK(C:CID),USE(?BREAK2)
                           HEADER,AT(0,0,7750,0),USE(?Hdr_Client)
                             STRING('Client:'),AT(52,31),USE(?ClientNameTitle)
                             STRING(@s100),AT(635,31,4906,167),USE(C:ClientName)
                             LINE,AT(0,250,7750,0),USE(?ClientEndLine),COLOR(COLOR:Black)
                           END
Break_Month                BREAK(YearMonth),USE(?BREAK3)
                             HEADER,AT(0,0,7750,0),USE(?Hdr_Month)
                               STRING(@s19),AT(52,31,1448,167),USE(MonthYear)
                               LINE,AT(0,250,7750,0),USE(?MonthEndLine),COLOR(COLOR:Black)
                             END
BranchData                   DETAIL,AT(0,0,7750,250),USE(?BranchData)
                               LINE,AT(0,0,0,250),USE(?BranchLine:0),COLOR(COLOR:Black)
                               LINE,AT(3104,0,0,250),USE(?BranchLine:2),COLOR(COLOR:Black)
                               LINE,AT(4208,0,0,250),USE(?BranchLine:3),COLOR(COLOR:Black)
                               LINE,AT(6594,0,0,250),USE(?BranchLine:4),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?BranchLine:5),COLOR(COLOR:Black)
                               STRING(@n-15.2),AT(3167,52,990,170),USE(B:Total),RIGHT(50)
                               STRING(@n-14.2),AT(5583,52,969,170),USE(B:FuelSurcharge),RIGHT(50)
                               STRING(@n-7.2),AT(6667,52,1031,170),USE(B:FuelSurchargePercent),RIGHT(50)
                               LINE,AT(0,250,7750,0),USE(?BranchEndLine:2),COLOR(COLOR:Black)
                               STRING(@s36),AT(52,52,2385,167),USE(B:BranchName,,?B:BranchName:2)
                               LINE,AT(5531,0,0,250),USE(?BranchLine),COLOR(COLOR:Black)
                               STRING(@n-14.2),AT(4521,42,969,167),USE(B:FreightCharge),RIGHT(50)
                             END
ClientData                   DETAIL,AT(0,0,7750,250),USE(?ClientData)
                               LINE,AT(0,0,0,250),USE(?ClientLine:0),COLOR(COLOR:Black)
                               LINE,AT(3104,0,0,250),USE(?ClientLine:2),COLOR(COLOR:Black)
                               LINE,AT(4208,0,0,250),USE(?ClientLine:3),COLOR(COLOR:Black)
                               LINE,AT(6594,0,0,250),USE(?ClientLine:4),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?ClientLine:5),COLOR(COLOR:Black)
                               STRING(@n-15.2),AT(3167,52,990,170),USE(C:Total),RIGHT(50)
                               STRING(@n-14.2),AT(5583,42,969,170),USE(C:FuelSurcharge),RIGHT(50)
                               STRING(@n-7.2),AT(6667,52,1031,170),USE(C:FuelSurchargePercent),RIGHT(50)
                               LINE,AT(0,250,7750,0),USE(?ClientEndLine:2),COLOR(COLOR:Black)
                               STRING(@s100),AT(52,42,3000,167),USE(C:ClientName,,?C:ClientName:2)
                               LINE,AT(5531,0,0,250),USE(?ClientLine),COLOR(COLOR:Black)
                               STRING(@n-14.2),AT(4521,42,969,167),USE(C:FreightCharge),RIGHT(50)
                             END
MonthData                    DETAIL,AT(0,0,7750,250),USE(?MonthData)
                               LINE,AT(0,0,0,250),USE(?MonthLine:0),COLOR(COLOR:Black)
                               LINE,AT(3104,0,0,250),USE(?MonthLine:2),COLOR(COLOR:Black)
                               LINE,AT(4208,0,0,250),USE(?MonthLine:3),COLOR(COLOR:Black)
                               LINE,AT(6594,0,0,250),USE(?MonthLine:4),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?MonthLine:5),COLOR(COLOR:Black)
                               STRING(@n-15.2),AT(3167,52,990,170),USE(M:Total),RIGHT(50)
                               STRING(@n-14.2),AT(5583,52,969,170),USE(M:FuelSurcharge),RIGHT(50)
                               STRING(@n-7.2),AT(6667,52,1031,170),USE(M:FuelSurchargePercent),RIGHT(50)
                               STRING(@s19),AT(52,31,2385,167),USE(MonthYear,,?MonthYear:2)
                               LINE,AT(0,250,7750,0),USE(?MonthEndLine:2),COLOR(COLOR:Black)
                               LINE,AT(5531,0,0,250),USE(?MonthLine),COLOR(COLOR:Black)
                               STRING(@n-14.2),AT(4521,52,969,167),USE(M:FreightCharge),RIGHT(50)
                             END
Detail                       DETAIL,AT(0,0,7750,250),USE(?Detail)
                               LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                               LINE,AT(1552,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                               LINE,AT(3104,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                               LINE,AT(4208,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                               LINE,AT(6594,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                               STRING(@d6),AT(50,50,1450,170),USE(I:InvoiceDate)
                               STRING(@n_10),AT(1600,50,1450,170),USE(I:IID),RIGHT(50)
                               STRING(@n-15.2),AT(3167,52,990,170),USE(I:Total),RIGHT(50)
                               STRING(@n-14.2),AT(5583,52,969,170),USE(I:FuelSurcharge),RIGHT(50)
                               STRING(@n-7.2),AT(6667,52,1031,170),USE(I:FuelSurchargePercent),RIGHT(50)
                               LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                               LINE,AT(5531,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                               STRING(@n-14.2),AT(4521,52,969,167),USE(I:FreightCharge),RIGHT(50)
                             END
NoDetails                    DETAIL,AT(0,0,7750,0),USE(?NoDetails)
                             END
                             FOOTER,AT(0,0,7750,0),USE(?Ftr_Month)
                               LINE,AT(0,0,0,250),USE(?MonthLine:0:2),COLOR(COLOR:Black)
                               STRING(@s16),AT(312,31,1187,167),USE(M:Month,,?MonthName:2)
                               LINE,AT(3104,0,0,250),USE(?MonthLine:6),COLOR(COLOR:Black)
                               LINE,AT(4208,0,0,250),USE(?MonthLine:7),COLOR(COLOR:Black)
                               LINE,AT(6594,0,0,250),USE(?MonthLine:8),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?MonthLine:9),COLOR(COLOR:Black)
                               STRING(@n-15.2),AT(3167,52,990,170),USE(M:Total,,?INV:Total:2),RIGHT(50)
                               STRING(@n-14.2),AT(5583,52,969,170),USE(M:FuelSurcharge,,?INV:FuelSurcharge:2),RIGHT(50)
                               STRING(@n-14.2),AT(4521,52,969,167),USE(M:FreightCharge,,?I:FreightCharge:2),RIGHT(50)
                               STRING(@n-7.2),AT(6667,52,1031,170),USE(M:FuelSurchargePercent,,?INV:VATRate:2),RIGHT(50)
                               LINE,AT(5531,0,0,250),USE(?MonthLine:10),COLOR(COLOR:Black)
                               LINE,AT(0,250,7750,0),USE(?MonthEndLine:3),COLOR(COLOR:Black)
                             END
                           END
                           FOOTER,AT(0,0,7750,0),USE(?Ftr_Client)
                             LINE,AT(52,52,7676,0),USE(?ClientLine:6),COLOR(COLOR:Black)
                             STRING('Client Total'),AT(313,104),USE(?ClientTotalString),TRN
                             STRING(@n-15.2),AT(3146,104,1010,167),USE(C:Total,,?C:Total:2),RIGHT(50)
                             STRING(@n-14.2),AT(4521,104,969,167),USE(C:FreightCharge,,?C:FreightCharge:2),RIGHT(50)
                             STRING(@n-14.2),AT(5583,104,969,167),USE(C:FuelSurcharge,,?INV:FuelSurcharge:3),RIGHT(50)
                             STRING(@n-15.2),AT(6667,104,1031,167),USE(C:FuelSurchargePercent,,?INV:VATRate:3),RIGHT(50)
                             LINE,AT(52,312,7676,0),USE(?ClientLine:7),COLOR(COLOR:Black)
                           END
                         END
                         FOOTER,AT(0,0,7750,0),USE(?Ftr_Branch)
                           LINE,AT(52,52,7676,0),USE(?BranchLine:6),COLOR(COLOR:Black)
                           STRING('Branch Total'),AT(313,104),USE(?BranchTotalString),TRN
                           STRING(@n-15.2),AT(3146,104,1010,167),USE(B:Total,,?INV:Total:3),RIGHT(50)
                           STRING(@n-14.2),AT(4521,104,969,167),USE(B:FreightCharge,,?B:FreightCharge:2),RIGHT(50)
                           STRING(@n-14.2),AT(5583,104,969,167),USE(B:FuelSurcharge,,?INV:FuelSurcharge:4),RIGHT(50)
                           STRING(@n-15.2),AT(6667,104,1031,167),USE(B:FuelSurchargePercent,,?INV:VATRate:4),RIGHT(50)
                           LINE,AT(52,312,7676,0),USE(?Line19:4),COLOR(COLOR:Black)
                         END
                       END
grandtotals            DETAIL,AT(0,0,,396),USE(?grandtotals)
                         LINE,AT(52,52,7676,0),USE(?Line19),COLOR(COLOR:Black)
                         STRING('Grand Total'),AT(313,104),USE(?String27:2),TRN
                         LINE,AT(52,312,7676,0),USE(?Line19:2),COLOR(COLOR:Black)
                         STRING(@n-15.2),AT(3146,94,1010,167),USE(G:Total),RIGHT(50)
                         STRING(@n-14.2),AT(5583,104,969,167),USE(G:FuelSurcharge,,?INV:FuelSurcharge),RIGHT(50)
                         STRING(@n-15.2),AT(6667,104,1031,167),USE(G:FuelSurchargePercent,,?INV:VATRate),RIGHT(50)
                         STRING(@n-14.2),AT(4521,104,969,167),USE(G:FreightCharge,,?I:FreightCharge:3),RIGHT(50)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt:2),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp:2),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt:2),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp:2),FONT('Arial',8,, |
  FONT:regular),TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount:2),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Next                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
BreakMgr             BreakManagerClass                     ! Break Manager
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_FuelSurchargeReportWithFreightCharge')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:_Invoice.SetOpenRelated()
  Relate:_Invoice.Open                                     ! File _Invoice used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  If False !do not use INV:...
  BreakMgr.Init()
  BreakMgr.AddBreak()
  BreakMgr.AddLevel() !Break_Branch
  BreakMgr.AddResetField(INV:BID)
  BreakMgr.AddLevel() !Break_Client
  BreakMgr.AddResetField(INV:CID)
  BreakMgr.AddLevel() !Break_Month
  BreakMgr.AddResetField(YearMonth)
  SELF.AddItem(BreakMgr)
  End !do not use INV:...
  BreakMgr.Init()
  BreakMgr.AddBreak()
  BreakMgr.AddLevel() !Break_Branch
  BreakMgr.AddResetField(B:BID)
  BreakMgr.AddLevel() !Break_Client
  BreakMgr.AddResetField(C:CID)
  BreakMgr.AddLevel() !Break_Month
  BreakMgr.AddResetField(YearMonth)
  SELF.AddItem(BreakMgr)  
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_FuelSurchargeReportWithFreightCharge',ProgressWindow) ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF NOT TargetSelector.ASK(1) THEN
     SELF.Kill()
     RETURN Level:Fatal
  END
  IF NOT TargetSelector.GetPrintSelected() THEN
     SELF.SetReportTarget(TargetSelector.GetReportSelected())
  END
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:_Invoice, ?Progress:PctText, Progress:Thermometer, RECORDS(Q))
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Invoice.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_FuelSurchargeReportWithFreightCharge',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Next PROCEDURE

ReturnValue          BYTE,AUTO

Progress BYTE,AUTO
  CODE
      ThisReport.RecordsProcessed+=1
      GET(Q,ThisReport.RecordsProcessed)
      IF ERRORCODE() THEN
         ReturnValue = Level:Notify
      ELSE
         ReturnValue = Level:Benign
      END
      IF ReturnValue = Level:Notify
          IF ThisReport.RecordsProcessed>RECORDS(Q)
             SELF.Response = RequestCompleted
             POST(EVENT:CloseWindow)
             RETURN Level:Notify
          ELSE
             SELF.Response = RequestCancelled
             POST(EVENT:CloseWindow)
             RETURN Level:Fatal
          END
      ELSE
         Progress = ThisReport.RecordsProcessed / ThisReport.RecordsToProcess*100
         IF Progress > 100 THEN Progress = 100.
         IF Progress <> Progress:Thermometer
           Progress:Thermometer = Progress
           DISPLAY()
         END
      END
      RETURN Level:Benign
  ReturnValue = PARENT.Next()
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  If ReturnValue = Level:Benign Then
     SetTarget(Self.Report)
     If BAnd(pMode,01111b) = 8 Or BAnd(pMode,01000b) = 0 Then
        Hide(?Hdr_Branch,?BranchEndLine)
        Hide(?Ftr_Branch,?Line19:4)
     Else
        UnHide(?Hdr_Branch,?BranchEndLine)
        If BAnd(pMode,01111b) > 8 And BAnd(pMode,00111b) > 4
           Hide(?BranchEndLine)
        End
        UnHide(?Ftr_Branch,?Line19:4)
     End
     If BAnd(pMode,00111b) = 4 Or BAnd(pMode,00100b) = 0 Then
        Hide(?Hdr_Client,?ClientEndLine)
        Hide(?Ftr_Client,?ClientLine:7)
     Else
        UnHide(?Hdr_Client,?ClientEndLine)
        If BAnd(pMode,00111b) > 4 And BAnd(pMode,00011b) > 2
           Hide(?ClientEndLine)
        End
        UnHide(?Ftr_Client,?ClientLine:7)
     End
     If BAnd(pMode,00011b) = 2 or BAnd(pMode,00010b) = 0 Then
        Hide(?Hdr_Month,?MonthEndLine)
        Hide(?Ftr_Month,?MonthEndLine:3)
     Else
        UnHide(?Hdr_Month,?MonthEndLine)
        UnHide(?Ftr_Month,?MonthEndLine:3)
     End
     If pDateFrom Or pDateTo Then
        ?DateRangeString{PROP:Text} = Choose(pDateFrom=0,'...',Format(pDateFrom,@D6B)) & ' - ' & Choose(pDateTo=0,'...',Format(pDateTo,@D6B))
        UnHide(?DateRangeString)
     Else
        Hide(?DateRangeString)
     End
     SetTarget()
  End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp:2{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp:2{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  UpperLevelChanged = False
  If pMode > 8
     If B:BID And Q.BID And Q.BID ~= B:BID Then
        UpperLevelChanged = True
        !stop('pMode='&pMode&' Branch changed. BID='&B:BID&' Q.BID='&Q.BID)
     End
  ElsIf pMode > 4
     If C:CID And Q.CID And Q.CID ~= C:CID THEN
        UpperLevelChanged = True
        !stop('pMode='&pMode&' Client changed. CID='&C:CID&' Q.CID='&Q.CID)
     End
  ElsIf pMode > 2
     If M:Year And M:MonthNo And Q.MonthNo And (Q.Year ~= M:Year Or Q.MonthNo ~= M:MonthNo) THEN
        UpperLevelChanged = True
        !stop('pMode='&pMode&' Month changed. M:Year='&M:Year&' M:MonthNo='&M:MonthNo&' Q.Year='&Q.Year&' Q.MonthNo='&Q.MonthNo)
     End
  End
  !stop('Pointer:'&Pointer(Q)& '. UpperLevelChanged = '&UpperLevelChanged)
  If Q.IID Then ! Invoice
     InvoiceRecord :=: Q
     I:Printed = 0
  ElsIf Q.MonthNo Then ! Month
     MonthTotal :=: Q
     M:Printed = 0
     YearMonth = M:Year & '-' & M:Month
     If Year(pDateFrom) ~= Year(pDateTo) THEN
        MonthYear = M:Month &' ('&M:Year&')'
     Else
        MonthYear = M:Month
     End
     Clear(InvoiceRecord)
     !stop('Month record. YearMonth: '&YearMonth)
  ElsIf Q.CID
     Clear(InvoiceRecord)
     If C:CID ~= Q.CID Then ! only if changed
        If BAnd(pMode,00011b) = 2 Then ! always clear if the last level
           Clear(MonthTotal)
        End
     End
     ClientTotal :=: Q
     C:Printed = 0
     !stop('Client record. '&Choose(C:CID ~= Q.CID,'Changed.','The same.')& 'C:CID: '&C:CID)
  ElsIf Q.BID
     Clear(InvoiceRecord)
     If B:CID ~= Q.BID Then ! only if changed
        If BAnd(pMode,00111b) = 4 Then ! always clear if the last level
           Clear(ClientTotal)
        End
        If BAnd(pMode,00011b) = 2 Then ! always clear if the last level
           Clear(MonthTotal)
        End        
     End
     BranchTotal :=: Q
     B:Printed = 0
     !stop('Branch record. '&Choose(B:CID ~= Q.BID,'Changed.','The same.')& 'B:BID: '&B:BID)
  ELSE
     GrandTotal :=: Q
     If BAnd(pMode,01111b) = 8 Then ! always clear if the last level
        Clear(BranchTotal)
     End
     If BAnd(pMode,00111b) = 4 Then ! always clear if the last level
        Clear(ClientTotal)
     End
     If BAnd(pMode,00011b) = 2 Then ! always clear if the last level
        Clear(MonthTotal)
     End        
     !stop('Branch: '&Choose(B:CID ~= Q.BID,'Changed.','The same.')& 'B:BID: '&B:BID&|
     !     'Client: '&Choose(C:CID ~= Q.CID,'Changed.','The same.')& 'C:CID: '&C:CID&|
     !     'YearMonth: '&YearMonth |
     !)
  End
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  If 0 ! for debug
  IF BAND(pMode,01111)=8 AND B:BID~=0
    PRINT(RPT:BranchData)
  END
  IF BAND(pMode,00111)=4 AND C:CID~=0
    PRINT(RPT:ClientData)
  END
  IF BAND(pMode,00011)=2 AND M:MonthNo~=0
    PRINT(RPT:MonthData)
  END
  IF BAND(pMode,00001)=1 AND I:IID~=0
    PRINT(RPT:Detail)
  END
  IF False !BAND(pMode,00001)=0
    PRINT(RPT:NoDetails)
  END
  IF G:Total Or G:FuelSurcharge Or G:FuelSurchargePercent
    PRINT(RPT:grandtotals)
  END
  IF ~SkipDetails
    PRINT(RPT:LevelSeparator)
  END
  End !if 0
  IF UpperLevelChanged
    PRINT(RPT:LevelSeparator)
    !stop('PRINT(RPT:LevelSeparator)')
  END
  !Stop(pMode&', B:BID='&B:BID&', C:CID='&C:CID&', M:MonthNo='&M:MonthNo)
  IF BAND(pMode,01111b)=8 AND B:BID~=0 And B:Printed = 0
    PRINT(RPT:BranchData)
    !stop('PRINT(RPT:BranchData)')
    B:Printed = 1
  END
  IF BAND(pMode,00111b)=4 AND C:CID~=0  And C:Printed = 0
    PRINT(RPT:ClientData)
    !stop('PRINT(RPT:ClientData)')
    C:Printed = 1
  END  
  IF BAND(pMode,00011b)=2 AND M:MonthNo~=0 And M:Printed = 0
    PRINT(RPT:MonthData)
    !stop('PRINT(RPT:MonthData)')
    M:Printed = 1
  END
  IF BAND(pMode,00001b)=1 AND I:IID~=0 And I:Printed = 0
    PRINT(RPT:Detail)
    !stop('PRINT(RPT:Detail)')
    I:Printed = 1
  END
  IF False !BAND(pMode,00001b)=0
    PRINT(RPT:NoDetails)
    !stop('PRINT(RPT:NoDetails)')
  END
  IF G:Total Or G:FuelSurcharge Or G:FuelSurchargePercent
    PRINT(RPT:grandtotals)
    !stop('PRINT(RPT:grandtotals)')
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','FuelSurchargeReport','FuelSurchargeReport','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! ---------------------  uses SQL Query class
!!! </summary>
Print_ManagementProfit_Summary_copy PROCEDURE 

!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
TintRptViewer  TinReportViewer
!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
Progress:Thermometer BYTE                                  ! 
LOC:Errors           ULONG                                 ! 
LOC:Transaction_Log_Name STRING(255)                       ! 
LOC:Settings         GROUP,PRE(L_SG)                       ! 
ExcludeBadDebts      BYTE(1)                               ! Exclude Bad Debt Credit Notes
Output               BYTE                                  ! Output trail of items
Branch1              ULONG                                 ! Branch ID
BranchName1          STRING(35)                            ! Branch Name
Branch2              ULONG                                 ! Branch ID
BranchName2          STRING(35)                            ! Branch Name
                     END                                   ! 
LOC:Options          GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
LOC:Summary_Group    GROUP,PRE(L_SG)                       ! 
Turnover_DBN         DECIMAL(10,2)                         ! 
Turnover_JHB         DECIMAL(10,2)                         ! 
Turnover_Total       DECIMAL(10,2)                         ! 
Credits_DBN          DECIMAL(10,2)                         ! 
Credits_JHB          DECIMAL(10,2)                         ! 
Credits_Total        DECIMAL(10,2)                         ! 
Turnover_less_Credits_DBN DECIMAL(10,2)                    ! 
Turnover_less_Credits_JHB DECIMAL(10,2)                    ! 
Work_Days            LONG                                  ! 
Broking_Kg_DBN       DECIMAL(8)                            ! 
Broking_Kg_JHB       DECIMAL(8)                            ! 
Broking_Kg_Total     DECIMAL(8)                            ! 
Overnight_Kg_DBN     DECIMAL(8)                            ! 
Overnight_Kg_JHB     DECIMAL(8)                            ! 
Overnight_Kg_Total   DECIMAL(8)                            ! 
Kg_Total             DECIMAL(8)                            ! 
Kg_Per_Day           DECIMAL(8)                            ! 
Kg_Per_Day_DBN       DECIMAL(8)                            ! 
Kg_Per_Day_JHB       DECIMAL(8)                            ! 
Average_Cost_Per_Kg  DECIMAL(7,2)                          ! 
Gross_GP_DBN         DECIMAL(10,2)                         ! 
Gross_GP_JHB         DECIMAL(10,2)                         ! 
Gross_GP_Total       DECIMAL(10,2)                         ! 
Gross_GP_Per_Day     DECIMAL(10,2)                         ! 
Loadmaster_DBN       DECIMAL(10,2)                         ! 
Loadmaster_JHB       DECIMAL(10,2)                         ! 
Loadmaster_Total     DECIMAL(10,2)                         ! 
Loadmaster_Per_Day   DECIMAL(10,2)                         ! 
Loadmaster_GP_Total  DECIMAL(10,2)                         ! 
Loadmaster_GP_Per_Day DECIMAL(10,2)                        ! 
Turnover_Per_Day     DECIMAL(10,2)                         ! 
Turnover_Per_Day_DBN DECIMAL(10,2)                         ! 
Turnover_Per_Day_JHB DECIMAL(10,2)                         ! 
Sales_Broking_DBN    DECIMAL(10,2)                         ! 
Sales_Broking_JHB    DECIMAL(10,2)                         ! 
Sales_Overnight_DBN  DECIMAL(10,2)                         ! 
Sales_Overnight_JHB  DECIMAL(10,2)                         ! 
Costs_Broking_DBN    DECIMAL(10,2)                         ! 
Costs_Broking_JHB    DECIMAL(10,2)                         ! 
GP_Broking_DBN       DECIMAL(10,2)                         ! 
GP_Broking_JHB       DECIMAL(10,2)                         ! 
GP_Per_Broking_DBN   DECIMAL(6,2)                          ! 
GP_Per_Broking_JHB   DECIMAL(6,2)                          ! 
GP_Overnight_DBN     DECIMAL(10,2)                         ! 
GP_Overnight_JHB     DECIMAL(10,2)                         ! 
GP_Per_Overnight_DBN DECIMAL(6,2)                          ! 
GP_Per_Overnight_JHB DECIMAL(6,2)                          ! 
Extra_Invoices_DBN   DECIMAL(10,2)                         ! Transporter
Extra_Invoices_JHB   DECIMAL(10,2)                         ! Transporter
Transporter_Credits_DBN DECIMAL(10,2)                      ! 
Transporter_Credits_JHB DECIMAL(10,2)                      ! 
Transporter_Debits_DBN DECIMAL(10,2)                       ! 
Transporter_Debits_JHB DECIMAL(10,2)                       ! 
Sales_Broking_DBN_Total DECIMAL(10,2)                      ! 
Sales_Broking_JHB_Total DECIMAL(10,2)                      ! 
Extra_Leg_DBN        DECIMAL(10,2)                         ! 
Extra_Leg_Broking_DBN DECIMAL(10,2)                        ! 
Extra_Leg_ON_DBN     DECIMAL(10,2)                         ! 
Extra_Leg_JHB        DECIMAL(10,2)                         ! 
Extra_Leg_Broking_JHB DECIMAL(10,2)                        ! 
Extra_Leg_ON_JHB     DECIMAL(10,2)                         ! 
Transporter_Costs_DBN DECIMAL(10,2)                        ! 
Transporter_Costs_JHB DECIMAL(10,2)                        ! 
                     END                                   ! 
Process:View         VIEW(Setup)
                     END
FDB4::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
FDB7::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
ProgressWindow       WINDOW('Report - Management Profit Summary'),AT(,,195,126),FONT('MS Sans Serif',8,,FONT:regular), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       SHEET,AT(4,2,188,102),USE(?Sheet1)
                         TAB('Settings'),USE(?Tab2)
                           CHECK(' Exclude Bad Debts'),AT(78,24),USE(L_SG:ExcludeBadDebts),MSG('Exclude Bad Debt C' & |
  'redit Notes'),TIP('Exclude Bad Debt Credit Notes')
                           PROMPT('Audit Output:'),AT(10,42),USE(?L_SG:Output_Trail:Prompt)
                           LIST,AT(78,42,60,10),USE(L_SG:Output),DROP(5),FROM('None|#0|All|#1'),MSG('Output trail of items'), |
  TIP('Output trail of items')
                           PROMPT('Branch Name 1:'),AT(10,65),USE(?L_SG:BranchName1:Prompt:2)
                           LIST,AT(78,66,109,10),USE(L_SG:BranchName1),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                           PROMPT('Branch Name 2:'),AT(10,81),USE(?BranchName2:Prompt:2)
                           LIST,AT(78,81,109,10),USE(L_SG:BranchName2),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop:1)
                         END
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(22,34,151,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(15,22,165,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(15,50,165,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Cancel'),AT(142,108,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                       BUTTON('Pause'),AT(88,108,49,15),USE(?Pause),LEFT,ICON('Blue_R.ico'),FLAT
                     END

Report               REPORT('Manifest Report'),AT(250,833,9302,6979),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,9302,583),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Management Profit Summary'),AT(0,21,9300),USE(?ReportTitle),FONT(,12,,FONT:bold),CENTER
                         STRING('To:'),AT(4979,313),USE(?String19:49),FONT(,10,,,CHARSET:ANSI),TRN
                         STRING('From:'),AT(3469,313),USE(?String19:48),FONT(,10,,,CHARSET:ANSI),TRN
                         STRING(@d5b),AT(3854,313),USE(LO:From_Date),FONT(,10,,,CHARSET:ANSI),RIGHT(1)
                         STRING(@d5b),AT(5240,313),USE(LO:To_Date),FONT(,10,,,CHARSET:ANSI),RIGHT(1)
                       END
Detail                 DETAIL,AT(10,10,10604,6833),USE(?Detail)
                         STRING('Johannesburg'),AT(6563,3375,1094,208),USE(?String19:61),CENTER
                         BOX,AT(6458,104,2500,2604),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(2)
                         STRING('Durban'),AT(6563,10,1094,208),USE(?String19:58),CENTER
                         STRING('Turnover DBN:'),AT(135,167),USE(?String19),TRN
                         STRING(@n-14.2),AT(1406,167),USE(L_SG:Turnover_DBN),RIGHT(1)
                         STRING('Broking Kg:'),AT(6563,302),USE(?String19:67),TRN
                         STRING(@n-11.0),AT(7927,302,885,167),USE(L_SG:Broking_Kg_DBN,,?L_SG:Broking_Kg_DBN:2),RIGHT(1), |
  TRN
                         STRING(@n-14.2),AT(3927,5792),USE(L_SG:Loadmaster_GP_Per_Day),RIGHT(1)
                         BOX,AT(52,1458,4844,833),USE(?Box5),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(6521,1490,2400,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Overnight Kg:'),AT(6563,1552),USE(?String19:68),TRN
                         STRING(@n-11.0),AT(7927,1552,885),USE(L_SG:Overnight_Kg_DBN,,?L_SG:Overnight_Kg_DBN:2),RIGHT(1), |
  TRN
                         BOX,AT(4896,833,885,573),USE(?Box4),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Turnover JHB:'),AT(135,646),USE(?String19:2),TRN
                         STRING(@n-14.2),AT(1406,646),USE(L_SG:Turnover_JHB),RIGHT(1)
                         STRING(@n-14.2),AT(7927,1021,885,167),USE(L_SG:GP_Broking_DBN),RIGHT(1)
                         BOX,AT(52,2344,2344,833),USE(?Box5:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@n-14.2),AT(1417,6177),USE(L_SG:Turnover_Per_Day),RIGHT(1)
                         STRING('Turnover Per Day:'),AT(135,6177),USE(?String19:31),TRN
                         STRING(@n-14.2),AT(7927,4323,885,167),USE(L_SG:GP_Broking_JHB),RIGHT(1)
                         STRING('Turnover Total:'),AT(2521,1135),USE(?String19:3),TRN
                         STRING('Turnover Per Day JHB:'),AT(2667,6448),USE(?String19:33),TRN
                         STRING('Gross GP DBN:'),AT(135,4042),USE(?String19:21),TRN
                         STRING(@n-14.2),AT(3927,1135),USE(L_SG:Turnover_Total),RIGHT(1)
                         STRING('Sales Broking Total:'),AT(6563,542,990,167),USE(?String19:56),TRN
                         STRING(@n-14.2),AT(7927,542,885,167),USE(L_SG:Sales_Broking_DBN_Total),RIGHT(1)
                         STRING(@n-14.2),AT(3927,6198),USE(L_SG:Turnover_Per_Day_DBN),RIGHT(1)
                         STRING(@N-9.2~%~),AT(7927,1260,885,167),USE(L_SG:GP_Per_Broking_DBN),RIGHT(1)
                         STRING('GP % Broking:'),AT(6563,1260),USE(?String19:42),TRN
                         BOX,AT(2552,2344,2344,833),USE(?Box5:6),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Credits DBN:'),AT(135,406),USE(?String19:4),TRN
                         STRING(@n-14.2),AT(1406,406),USE(L_SG:Credits_DBN),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1)
                         STRING(@n-14.2),AT(3927,6448),USE(L_SG:Turnover_Per_Day_JHB),RIGHT(1)
                         STRING('Gross GP JHB:'),AT(135,4281),USE(?String19:22),TRN
                         STRING(@N-9.2~%~),AT(7927,4552,885,167),USE(L_SG:GP_Per_Broking_JHB),RIGHT(1)
                         LINE,AT(6510,4771,2400,0),USE(?Line2),COLOR(COLOR:Black)
                         BOX,AT(52,5729,2344,302),USE(?Box5:10),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(2552,5729,2344,302),USE(?Box5:11),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('GP % Broking:'),AT(6563,4552),USE(?String19:43),TRN
                         STRING('Credits JHB:'),AT(135,885),USE(?String19:5),TRN
                         STRING(@n-14.2),AT(1406,885),USE(L_SG:Credits_JHB),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1)
                         STRING('Gross GP Total:'),AT(135,4531),USE(?String19:23),TRN
                         STRING('Credits Total:'),AT(135,1135),USE(?String19:6),TRN
                         BOX,AT(52,52,4844,1354),USE(?Box3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@n-14.2),AT(1406,1135),USE(L_SG:Credits_Total),FONT(,,COLOR:Red,,CHARSET:ANSI),RIGHT(1)
                         STRING('Sales Overnight:'),AT(6563,1781),USE(?String19:36),TRN
                         STRING(@n-14.2),AT(7927,2240,885,167),USE(L_SG:GP_Overnight_DBN),RIGHT(1)
                         BOX,AT(52,4844,2344,833),USE(?Box5:8),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(2552,4844,2344,833),USE(?Box5:9),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('GP Overnight:'),AT(6563,2240),USE(?String19:44),TRN
                         STRING('Turnover less Credits DBN:'),AT(2521,406),USE(?String19:7),TRN
                         STRING(@n-14.2),AT(3927,406),USE(L_SG:Turnover_less_Credits_DBN),RIGHT(1)
                         STRING('Loadmaster DBN:'),AT(135,4917),USE(?String19:25),TRN
                         STRING(@n-14.2),AT(7927,1781,885,167),USE(L_SG:Sales_Overnight_DBN),RIGHT(1)
                         STRING(@n-14.2),AT(7927,5531,885,167),USE(L_SG:GP_Overnight_JHB),RIGHT(1)
                         STRING(@N-9.2~%~),AT(7927,2458,885,167),USE(L_SG:GP_Per_Overnight_DBN),RIGHT(1)
                         STRING('Turnover less Credits JHB:'),AT(2521,885),USE(?String19:8),TRN
                         STRING(@n-14.2),AT(3927,885),USE(L_SG:Turnover_less_Credits_JHB),RIGHT(1)
                         STRING(@n-14.2),AT(7927,5062,885,167),USE(L_SG:Sales_Overnight_JHB),RIGHT(1)
                         STRING('Loadmaster:'),AT(6563,5271),USE(?String19:72),TRN
                         STRING(@n-14.2),AT(7927,5271),USE(L_SG:Loadmaster_JHB,,?L_SG:Loadmaster_JHB:2),RIGHT(1),TRN
                         BOX,AT(52,6094,2344,302),USE(?Box5:12),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(2552,6094,2344,573),USE(?Box5:13),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Sales Overnight:'),AT(6563,5063),USE(?String19:37),TRN
                         STRING(@n-14),AT(5031,1156,583,167),USE(L_SG:Work_Days),CENTER,TRN
                         STRING('GP Broking:'),AT(6563,1021),USE(?String19:40),TRN
                         STRING('Work Days:'),AT(5031,917),USE(?String19:9),TRN
                         STRING('Loadmaster Total:'),AT(135,5406),USE(?String19:27),TRN
                         STRING(@N-9.2~%~),AT(7927,5781,885,167),USE(L_SG:GP_Per_Overnight_JHB),RIGHT(1)
                         STRING('Broking Kg DBN:'),AT(135,1542),USE(?String19:10),TRN
                         STRING(@n-11.0),AT(1594,1542),USE(L_SG:Broking_Kg_DBN),RIGHT(1)
                         STRING('GP Broking:'),AT(6563,4323),USE(?String19:41),TRN
                         STRING(@n-14.2),AT(1417,4042),USE(L_SG:Gross_GP_DBN),RIGHT(1)
                         STRING('Broking Kg JHB:'),AT(156,1771),USE(?String19:11),TRN
                         STRING(@n-11.0),AT(1594,1792),USE(L_SG:Broking_Kg_JHB),RIGHT(1)
                         STRING('Loadmaster:'),AT(6563,2000),USE(?String19:69),TRN
                         STRING(@n-14.2),AT(7927,2000),USE(L_SG:Loadmaster_DBN,,?L_SG:Loadmaster_DBN:2),RIGHT(1),TRN
                         STRING('Total Cost Broking:'),AT(6563,781),USE(?String19:62),TRN
                         STRING('Loadmaster + GP Total:'),AT(135,5792),USE(?String19:29),TRN
                         STRING(@n-14.2),AT(1417,4281),USE(L_SG:Gross_GP_JHB),RIGHT(1)
                         STRING('Broking Kg Total:'),AT(135,2052),USE(?String19:12),TRN
                         STRING('Loadmaster GP Per Day:'),AT(2667,5792),USE(?String19:30),TRN
                         STRING(@n-11.0),AT(1594,2052),USE(L_SG:Broking_Kg_Total),RIGHT(1)
                         STRING(@n-14.2),AT(1417,4531),USE(L_SG:Gross_GP_Total),RIGHT(1)
                         STRING('Overnight Kg DBN:'),AT(135,2438),USE(?String19:13),TRN
                         STRING(@n-11.0),AT(1594,2438),USE(L_SG:Overnight_Kg_DBN),RIGHT(1)
                         STRING(@n-14.2),AT(3927,4531),USE(L_SG:Gross_GP_Per_Day),RIGHT(1)
                         STRING(@n-14.2),AT(7927,3865),USE(L_SG:Sales_Broking_JHB_Total),RIGHT(1)
                         STRING('Sales Broking Total:'),AT(6563,3865),USE(?String19:57)
                         STRING('Gross GP Per Day:'),AT(2667,4531),USE(?String19:24),TRN
                         STRING(@n-11.0),AT(1594,2677),USE(L_SG:Overnight_Kg_JHB),RIGHT(1)
                         STRING('GP Overnight:'),AT(6563,5531),USE(?String19:45),TRN
                         STRING(@n-14.2),AT(1417,4917),USE(L_SG:Loadmaster_DBN),RIGHT(1)
                         STRING('Total Cost Broking:'),AT(6563,4094),USE(?String19:63),TRN
                         STRING(@n-14.2),AT(7927,4094,885,167),USE(L_SG:Transporter_Costs_JHB),RIGHT(1),TRN
                         STRING('Kg Per Day DBN:'),AT(2656,2438),USE(?String19:18),TRN
                         STRING('Overnight Kg JHB:'),AT(135,2677),USE(?String19:14),TRN
                         STRING(@n-11.0),AT(4104,2438),USE(L_SG:Kg_Per_Day_DBN),RIGHT(1)
                         STRING(@n-11.0),AT(1594,2917),USE(L_SG:Overnight_Kg_Total),RIGHT(1)
                         STRING('GP % Overnight:'),AT(6563,2458),USE(?String19:46),TRN
                         STRING(@n-14.2),AT(1417,5156),USE(L_SG:Loadmaster_JHB),RIGHT(1)
                         STRING('Loadmaster JHB:'),AT(135,5156),USE(?String19:26),TRN
                         STRING('Overnight Kg Total:'),AT(135,2917),USE(?String19:15),TRN
                         STRING(@n-11.0),AT(4104,2677),USE(L_SG:Kg_Per_Day_JHB),RIGHT(1)
                         STRING(@n-11.0),AT(1583,3312),USE(L_SG:Kg_Total),RIGHT(1)
                         BOX,AT(6458,3448,2500,2604),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(2)
                         GROUP('Group - Hide'),AT(104,6667,4792,2083),USE(?Group1),BOXED,HIDE
                           STRING(@n-14.2),AT(885,7063),USE(L_SG:Extra_Leg_ON_JHB),RIGHT(1)
                           STRING('Sales Broking:'),AT(2542,7438),USE(?String19:35),TRN
                           STRING(@n-14.2),AT(3948,7438,833,167),USE(L_SG:Sales_Broking_JHB),RIGHT(1)
                           STRING('EL ON:'),AT(156,7063),USE(?String19:65),TRN
                           STRING('EL:'),AT(156,7271),USE(?String19:66),TRN
                           STRING(@n-14.2),AT(885,7271),USE(L_SG:Extra_Leg_JHB,,?L_SG:Extra_Leg_JHB:2),RIGHT(1)
                           STRING('Extra Legs:'),AT(2542,7656),USE(?String19:60)
                           STRING(@n-14.2),AT(3948,7656,833,167),USE(L_SG:Extra_Leg_JHB),RIGHT(1)
                           STRING('Sales Broking:'),AT(156,7490),USE(?String19:34),TRN
                           STRING(@n-14.2),AT(1563,7490),USE(L_SG:Sales_Broking_DBN),RIGHT(1)
                           STRING('Extra Legs:'),AT(156,7708,552,167),USE(?String19:59),TRN
                           STRING(@n-14.2),AT(1552,7708,833,167),USE(L_SG:Extra_Leg_DBN),RIGHT(1)
                           STRING('Costs Broking (incl. EL):'),AT(2542,7896),USE(?String19:39),TRN
                           STRING(@n-14.2),AT(3948,7896,833,167),USE(L_SG:Costs_Broking_JHB),RIGHT(1)
                           STRING('Costs Broking (incl. EL):'),AT(156,7917),USE(?String19:38),TRN
                           STRING(@n-14.2),AT(1552,7917,833,167),USE(L_SG:Costs_Broking_DBN),RIGHT(1)
                           STRING('Extra Transporter Invoices:'),AT(2542,8115),USE(?String19:54),TRN
                           STRING(@n-14.2),AT(3948,8115,833,167),USE(L_SG:Extra_Invoices_JHB),RIGHT(1)
                           STRING('Extra Transporter Invoices:'),AT(156,8115),USE(?String19:50),TRN
                           STRING(@n-14.2),AT(1552,8115,833,167),USE(L_SG:Extra_Invoices_DBN),RIGHT(1)
                           STRING('Transporter Credits:'),AT(2542,8323),USE(?String19:55),TRN
                           STRING(@n-14.2),AT(3948,8323,833,167),USE(L_SG:Transporter_Debits_JHB),RIGHT(1)
                           STRING('Transporter Credits:'),AT(156,8323,1219,167),USE(?String19:51),TRN
                           STRING(@n-14.2),AT(1563,8323,833,167),USE(L_SG:Transporter_Debits_DBN),RIGHT(1)
                           STRING('Transporter Debits:'),AT(2542,8521),USE(?String19:53),TRN
                           STRING(@n-14.2),AT(3948,8521,833,167),USE(L_SG:Transporter_Credits_JHB),FONT(,,COLOR:Red,, |
  CHARSET:ANSI),RIGHT(1)
                           STRING('Transporter Debits:'),AT(156,8531,958,167),USE(?String19:52),TRN
                           STRING(@n-14.2),AT(1563,8531,833,167),USE(L_SG:Transporter_Credits_DBN),FONT(,,COLOR:Red,, |
  CHARSET:ANSI),RIGHT(1)
                           STRING('EL Broking:'),AT(156,6844),USE(?String19:64),TRN
                           STRING(@n-14.2),AT(885,6844),USE(L_SG:Extra_Leg_Broking_JHB),RIGHT(1)
                         END
                         BOX,AT(52,3229,4844,300),USE(?Box5:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('GP % Overnight:'),AT(6563,5781),USE(?String19:47),TRN
                         STRING('Kg Per Day JHB:'),AT(2656,2677),USE(?String19:19),TRN
                         STRING('Turnover Per Day DBN:'),AT(2667,6198),USE(?String19:32),TRN
                         STRING('Kg Total:'),AT(135,3313),USE(?String19:16),TRN
                         STRING(@n-14.2),AT(1417,5406),USE(L_SG:Loadmaster_Total),RIGHT(1)
                         STRING(@n-10.2),AT(1656,3656),USE(L_SG:Average_Cost_Per_Kg),RIGHT(1)
                         STRING('Broking Kg:'),AT(6563,3635),USE(?String19:70),TRN
                         STRING(@n-11.0),AT(7927,3635,885),USE(L_SG:Broking_Kg_JHB,,?L_SG:Broking_Kg_JHB:2),RIGHT(1), |
  TRN
                         BOX,AT(52,3594,4844,302),USE(?Box5:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,3958,2344,833),USE(?Box5:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(2552,3958,2344,833),USE(?Box5:7),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@n-11.0),AT(4104,2917),USE(L_SG:Kg_Per_Day),RIGHT(1)
                         STRING('Kg Per Day Total:'),AT(2667,2917),USE(?String19:17),TRN
                         STRING(@n-14.2),AT(3927,5406),USE(L_SG:Loadmaster_Per_Day),RIGHT(1)
                         STRING('Overnight Kg:'),AT(6563,4833),USE(?String19:71),TRN
                         STRING(@n-11.0),AT(7927,4833,885),USE(L_SG:Overnight_Kg_JHB,,?L_SG:Overnight_Kg_JHB:2),RIGHT(1), |
  TRN
                         STRING(@n-14.2),AT(7927,781,885,167),USE(L_SG:Transporter_Costs_DBN),RIGHT(1)
                         STRING('Loadmaster Per Day:'),AT(2667,5406),USE(?String19:28),TRN
                         STRING('Avg. Cent Cost Per Kg:'),AT(135,3656),USE(?String19:20),TRN
                         STRING(@n-14.2),AT(1417,5792),USE(L_SG:Loadmaster_GP_Total),RIGHT(1)
                       END
                       FOOTER,AT(250,7813,9302,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(8365,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ReportMemoryRecords     BYTE(0)                            ! Used to do the first Next call
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Next                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END

TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

FDB4                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB7                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

Turnover_Summary        CLASS,TYPE

Get_Inv_Tots        PROCEDURE
    .



Calc        Turnover_Summary


sql_        SQLQueryClass
sql_2       SQLQueryClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Audit_Check             ROUTINE
    DATA

R:Date      DATE
R:Log       BYTE
R:Count     LONG
R:Dates     STRING(255)

    CODE
    R:Log   = L_SG:Output
    LOOP 2 TIMES
       R:Count  = 0
       R:Dates  = ''

       IF R:Log = 1
          ! Audit is for all - run query to show all DIs being included in the above date range that fall on a weekend or public holiday
          Add_Log('[DIs on Weekends and Public Holidays]', 'Print_Turnover_Summary', 'Print Turnover Summary - Holiday DIs - ' & FORMAT(TODAY(), @d7) & '.csv',, 1)
       .

       sql_.PropSQL('SELECT DISTINCT LTRIM(DATEPART([year], INVOICEDATEANDTIME)) + <39>/<39> + LTRIM(DATEPART([month], INVOICEDATEANDTIME)) + ' & |
                    '<39>/<39> + LTRIM(DATEPART([day], INVOICEDATEANDTIME)) ' & |
                    'FROM _Invoice WHERE InvoiceDateAndTime >= ' & |
                   SQL_Get_DateT_G(LO:From_Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(LO:To_Date + 1,,1))
       LOOP
          IF sql_.Next_Q() <= 0
             BREAK
          .

          R:Date        = DEFORMAT(sql_.Data_G.F1, @d10)

          ! Weekends and public holidays
          IF Check_PublicHoliday(R:Date) > 0 OR R:Date % 7 = 0 OR R:Date % 7 = 6
             R:Dates    = CLIP(R:Dates) & ', ' & FORMAT(R:Date,@d6)

             ! Output all these DI No.
             sql_2.PropSQL('SELECT DINo, Total FROM _Invoice WHERE InvoiceDateAndTime >= ' & |
                         SQL_Get_DateT_G(R:Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(R:Date + 1,,1))

             LOOP
                IF sql_2.Next_Q() <= 0
                   BREAK
                .

                R:Count += 1

                IF R:Log = 1
                   ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
                   Add_Log('Public Holiday / WEnd - DI No.: ' & CLIP(sql_2.Data_G.F1) & ', Date: ' & FORMAT(R:Date, @d6) & ', Amt: ' & CLIP(sql_2.Data_G.F2), |
                           'Print_Turnover_Summary', 'Print Turnover Summary - Holiday DIs - ' & FORMAT(TODAY(), @d7) & '.csv',, 1)
       .  .  .  .

       IF R:Count > 0 AND R:Log = 0
          CASE MESSAGE('There are ' & R:Count & ' DIs with a Public Holiday or Weekend Date.||Dates: ' & CLIP(R:Dates) & '||Would you like to output these to a log file?', 'Print Turnover Summary - DI Date Check', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
          OF BUTTON:Yes
             ! Ok
          OF BUTTON:No
             BREAK
          .
       ELSIF R:Log = 1 AND R:Count > 0
          CASE MESSAGE('There are ' & R:Count & ' DIs with a Public Holiday or Weekend Date.||Dates: ' & CLIP(R:Dates) & '||A log of these was created: ' & 'Print Turnover Summary - Holiday DIs - ' & FORMAT(TODAY(), @d7) & '.csv||Would you like to view this now?', 'Print Turnover Summary - DI Date Check', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
          OF BUTTON:Yes
             ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Print Turnover Summary - Holiday DIs - ' & FORMAT(TODAY(), @d7) & '.csv')
          .
          BREAK
    .  .
    EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      IF L_SG:Output > 0
         CASE MESSAGE('Would you like to load the accumulation details now?|Note that if the file does not load automatically you can find it at the location shown below.||File: ' & PATH() & '\Print Turnover Summary - ' & FORMAT(TODAY(), @d7) & '.csv', 'Print Turnover Summary', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\Print Turnover Summary - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
    ENDPAGE(SELF.Report)
    IF SELF.SkipPreview
      TintRptViewer.CheckForPageNum(Self.PreviewQueue)
      TintRptViewer.SetupCopiesCollate(Self.PreviewQueue)
      if (TinTRptViewer.GetEjectDir())
        TinTRptViewer.ReversePreviewQueue(Self.PreviewQueue)
      end
      SELF.Report{PROP:FlushPreview} = True
      GlobalResponse = RequestCompleted
    ELSE
      GlobalResponse = TintRptViewer.Display(Report,Self.PreviewQueue,Report{Prop:Text},'Page Width',0)
      SELF.SkipPreview = TRUE
    END
    FREE(SELF.PreviewQueue)
   END
  ! You can place code in this embed point to test GlobalResponse to see if report printed or not.
  ! CASE GlobalResponse
  ! OF RequestCompleted     ! Report Was Printed
  !   ! Do Something
  ! OF RequestCancelled     ! Report Was NOT Printed
  !   ! Do Something
  ! END
   OMIT('***TINTOOLS***')
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  PARENT.AskPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  ***TINTOOLS***
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
      ! Doesn't work, using Tin Tools
  
  !  LOOP
  !     CASE MESSAGE('Print another copy?', 'Print', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
  !     OF BUTTON:Yes
  !        SELF.PrintReport()
  !     ELSE
  !        BREAK
  !  .  .
  !


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_ManagementProfit_Summary_copy')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?L_SG:ExcludeBadDebts
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:Setup.Open                                        ! File Setup used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceTransporter.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF NOT TargetSelector.ASK(1) THEN
     SELF.Kill()
     RETURN Level:Fatal
  END
  IF NOT TargetSelector.GetPrintSelected() THEN
     SELF.SetReportTarget(TargetSelector.GetReportSelected())
  END
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Setup, ?Progress:PctText, Progress:Thermometer, 0)
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  FDB4.Init(?L_SG:BranchName1,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(BRA:Key_BranchName)
  FDB4.AddField(BRA:BranchName,FDB4.Q.BRA:BranchName) !List box control field - type derived from field
  FDB4.AddField(BRA:BID,FDB4.Q.BRA:BID) !Primary key field - type derived from field
  FDB4.AddUpdateField(BRA:BID,L_SG:Branch1)
  ThisWindow.AddItem(FDB4.WindowComponent)
  FDB4.DefaultFill = 0
  FDB7.Init(?L_SG:BranchName2,Queue:FileDrop:1.ViewPosition,FDB7::View:FileDrop,Queue:FileDrop:1,Relate:Branches,ThisWindow)
  FDB7.Q &= Queue:FileDrop:1
  FDB7.AddSortOrder(BRA:Key_BranchName)
  FDB7.AddField(BRA:BranchName,FDB7.Q.BRA:BranchName) !List box control field - type derived from field
  FDB7.AddField(BRA:BID,FDB7.Q.BRA:BID) !Primary key field - type derived from field
  FDB7.AddUpdateField(BRA:BID,L_SG:Branch2)
  ThisWindow.AddItem(FDB7.WindowComponent)
  FDB7.DefaultFill = 0
      LO:From_Date    = GETINI('Print_Turnover', 'From_Date', , GLO:Local_INI)
      LO:To_Date      = GETINI('Print_Turnover', 'To_Date', , GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      PUTINI('Print_Turnover', 'From_Date', LO:From_Date, GLO:Local_INI)
      PUTINI('Print_Turnover', 'To_Date', LO:To_Date, GLO:Local_INI)
  
      
      sql_.Init(GLO:DBOwner, '_SQLTemp2')
      sql_2.Init(GLO:DBOwner, '_SQLTemp2')
      
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
    ! set printer eject style
    TinTRptViewer.SetEjectDir(TinEject:PageFaceUp)
    IF TintRptViewer.PrintViewerSetup() THEN
      Self:Response = RequestCancelled       ! Changed by LH 4/19/98
      POST(EVENT:CloseDown)                  ! Changed by LH 4/19/98
      RETURN Level:NOTIFY                    ! Changed by LH 4/19/98
    END
    SELF.SkipPreview = TintRptViewer.SkipPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:Setup.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Next PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    IF ReportMemoryRecords=0 THEN
       ReportMemoryRecords+=1
       RETURN Level:Benign
    ELSE
       SELF.Response = RequestCompleted
       POST(EVENT:CloseWindow)
       RETURN Level:Notify
    END
  ReturnValue = PARENT.Next()
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
          SELECT(?Tab1)
          DISABLE(?Pause)
          DISPLAY
          IF L_SG:Output > 0
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             LOC:Transaction_Log_Name   = 'Print Turnover Summary - ' & FORMAT(TODAY(), @d7) & '.csv'
             Add_Log('Start', 'Print_Turnover_Summary', LOC:Transaction_Log_Name, TRUE, 1)
      
      
      
             CASE MESSAGE('Would you like to empty the Audit Management Profit table?','Print Summary', ICON:Question, 'All|Summary|None', 1)
             OF 1
                _SQLTemp{PROP:SQL}    = 'DELETE FROM Audit_ManagementProfit'
             OF 2
                _SQLTemp{PROP:SQL}    = 'DELETE FROM Audit_ManagementProfit WHERE source = 2'
          .  .
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      Calc.Get_Inv_Tots()
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          CLEAR(BRA:Record, -1)
          SET(BRA:PKey_BID)
          NEXT(Branches)
          L_SG:Branch1        = BRA:BID
          L_SG:BranchName1    = BRA:BranchName
      
          NEXT(Branches)
          L_SG:Branch2        = BRA:BID
          L_SG:BranchName2    = BRA:BranchName
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Turnover_Summary.Get_Inv_Tots        PROCEDURE
R:Type      BYTE

    CODE
    ! Loop through the Invoice file and gather all information
    ! Flaw in this code is that it can only deal with 2 branches.

    ! Get_Invoices
    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info  , p:LogName)
    !   1           2       3           4          5       6          7        8            9                 10        11          12
    ! (LONG  , BYTE    , BYTE=0, BYTE=0        , ULONG=0, <LONG>  , BYTE=0  , BYTE=0    , BYTE=0           , BYTE=0  , <STRING>, <STRING>),STRING
    ! p:Option
    !   0.  - Total Charges inc.
    !   1.  - Excl VAT
    !   2.  - VAT
    !   3.  - Kgs
    ! p:Type
    !   0.  - All
    !   1.  - Invoices                                  Total >= 0.0
    !   2.  - Credit Notes                              Total < 0.0    Instead of <, should be invoices with A_INV:CR_IID
    !   3.  - Credit Notes excl Bad Debts               Total < 0.0
    !   4.  - Credit Notes excl Journals                Total < 0.0
    !   5.  - Credit Notes excl Bad Debts & Journals    Total < 0.0
    ! p:Limit_On    &    p:ID
    !   0.  - Date [& Branch]
    !   1.  - MID
    ! p:Manifest_Option
    !   0.  - None
    !   1.  - Overnight
    !   2.  - Broking
    ! p:Not_Manifest
    !   0.  - All
    !   1.  - Not Manifest
    !   2.  - Manifest

    R:Type      = 2
    IF L_SG:ExcludeBadDebts = TRUE
       R:Type   = 3
    .

    ! Total inc., Invoices, filter on dates, overnight
    L_SG:Sales_Overnight_DBN    = Get_Invoices(LO:From_Date, 0, 1, 2, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 1, 2, 'I-ON', LOC:Transaction_Log_Name)       ! Overnight
    L_SG:Sales_Overnight_DBN   += Get_Invoices(LO:From_Date, 0, 1, 1, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 1, 2, 'I-EI-O', LOC:Transaction_Log_Name)       ! Overnight
    L_SG:Sales_Overnight_JHB    = Get_Invoices(LO:From_Date, 0, 1, 2, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 1, 2, 'I-ON', LOC:Transaction_Log_Name)       ! Overnight
    L_SG:Sales_Overnight_JHB   += Get_Invoices(LO:From_Date, 0, 1, 1, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 1, 2, 'I-EI-O', LOC:Transaction_Log_Name)       ! Overnight

    L_SG:Sales_Broking_DBN      = Get_Invoices(LO:From_Date, 0, 1, 2, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 2, 2, 'I-BR', LOC:Transaction_Log_Name)       ! Broking
    L_SG:Sales_Broking_DBN     += Get_Invoices(LO:From_Date, 0, 1, 1, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 2, 2, 'I-EI-B', LOC:Transaction_Log_Name)       ! Broking
    L_SG:Sales_Broking_JHB      = Get_Invoices(LO:From_Date, 0, 1, 2, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 2, 2, 'I-BR', LOC:Transaction_Log_Name)       ! Broking
    L_SG:Sales_Broking_JHB     += Get_Invoices(LO:From_Date, 0, 1, 1, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 2, 2, 'I-EI-B', LOC:Transaction_Log_Name)       ! Broking
                                                                          

    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type, p:Output, p:Source)
    !   1           2       3         4               5           6        7         8          9           10              11     , 12

    L_SG:Extra_Invoices_DBN      = Get_InvoicesTransporter(LO:From_Date, 0, 1, FALSE,,, L_SG:Branch1, LO:To_Date,TRUE,, L_SG:Output,2,'T-EI', LOC:Transaction_Log_Name)    ! Credits for day
    L_SG:Extra_Invoices_JHB      = Get_InvoicesTransporter(LO:From_Date, 0, 1, FALSE,,, L_SG:Branch2, LO:To_Date,TRUE,, L_SG:Output,2,'T-EI', LOC:Transaction_Log_Name)    ! Credits for day

    L_SG:Transporter_Debits_DBN  = Get_InvoicesTransporter(LO:From_Date, 0, 1, TRUE,,, L_SG:Branch1, LO:To_Date,,, L_SG:Output,2,'T-TD', LOC:Transaction_Log_Name)         ! Credits for day - Transporter not MID
    L_SG:Transporter_Debits_JHB  = Get_InvoicesTransporter(LO:From_Date, 0, 1, TRUE,,, L_SG:Branch2, LO:To_Date,,, L_SG:Output,2,'T-TD', LOC:Transaction_Log_Name)         ! Credits for day - Transporter not MID

    L_SG:Transporter_Credits_DBN = -Get_InvoicesTransporter(LO:From_Date, 0, 2,,,, L_SG:Branch1, LO:To_Date,,, L_SG:Output,2,'T-TC', LOC:Transaction_Log_Name)
    L_SG:Transporter_Credits_JHB = -Get_InvoicesTransporter(LO:From_Date, 0, 2,,,, L_SG:Branch2, LO:To_Date,,, L_SG:Output,2,'T-TC', LOC:Transaction_Log_Name)

    ! All extra legs are broking but some are from broking manifests and some are from loadmaster manifests
    L_SG:Extra_Leg_ON_DBN          = Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2, 'T-EL', LOC:Transaction_Log_Name, 1)
    ! Turnover - Invoices (p3 = 1), Del Leg Only (p5 = 1), Broking (p10 = 2)
    ! Invoices & CNs (p3 = 0), Del Leg Only (p5 = 1), Broking Invoice (p10 = 2), Broking Manifest (p15 = 2)
    L_SG:Extra_Leg_Broking_DBN     = Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2, 'T-EL', LOC:Transaction_Log_Name, 2)
    L_SG:Extra_Leg_ON_JHB          = Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2, 'T-EL', LOC:Transaction_Log_Name, 1)
    L_SG:Extra_Leg_Broking_JHB     = Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2, 'T-EL', LOC:Transaction_Log_Name, 2)

    L_SG:Extra_Leg_DBN             = L_SG:Extra_Leg_ON_DBN + L_SG:Extra_Leg_Broking_DBN
    L_SG:Extra_Leg_JHB             = L_SG:Extra_Leg_ON_JHB + L_SG:Extra_Leg_Broking_JHB

    L_SG:Sales_Broking_DBN        -= L_SG:Extra_Leg_Broking_DBN     ! 22 Aug - take off broking extra legs, ON are not included
    L_SG:Sales_Broking_JHB        -= L_SG:Extra_Leg_Broking_JHB

    L_SG:Sales_Broking_DBN_Total   = L_SG:Extra_Leg_DBN + L_SG:Sales_Broking_DBN
    L_SG:Sales_Broking_JHB_Total   = L_SG:Extra_Leg_JHB + L_SG:Sales_Broking_JHB

    L_SG:Sales_Overnight_DBN      -= L_SG:Extra_Leg_ON_DBN        ! Only remove the overnight extra legs
    L_SG:Sales_Overnight_JHB      -= L_SG:Extra_Leg_ON_JHB

!    L_SG:Sales_Broking_DBN_Total     += L_SG:Extra_Invoices_DBN + L_SG:Transporter_Credits_DBN - L_SG:Transporter_Debits_DBN
!    L_SG:Sales_Broking_JHB_Total     += L_SG:Extra_Invoices_JHB + L_SG:Transporter_Credits_JHB - L_SG:Transporter_Debits_JHB
!    L_SG:Sales_Broking_DBN_Total     += L_SG:Transporter_Credits_DBN - L_SG:Transporter_Debits_DBN
!    L_SG:Sales_Broking_JHB_Total     += L_SG:Transporter_Credits_JHB - L_SG:Transporter_Debits_JHB

    L_SG:Credits_DBN            = Get_Invoices(LO:From_Date, 0, R:Type,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 0, 2,'I-CR', LOC:Transaction_Log_Name)    ! total
    L_SG:Credits_JHB            = Get_Invoices(LO:From_Date, 0, R:Type,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 0, 2,'I-CR', LOC:Transaction_Log_Name)    ! total

    L_SG:Sales_Overnight_DBN   += L_SG:Credits_DBN       ! neg - excluding credits
    L_SG:Sales_Overnight_JHB   += L_SG:Credits_JHB       ! neg - excluding credits

    L_SG:Credits_Total          = L_SG:Credits_DBN + L_SG:Credits_JHB

    L_SG:Turnover_DBN           = L_SG:Sales_Broking_DBN_Total + L_SG:Sales_Overnight_DBN  - L_SG:Credits_DBN
    L_SG:Turnover_JHB           = L_SG:Sales_Broking_JHB_Total + L_SG:Sales_Overnight_JHB  - L_SG:Credits_JHB

    L_SG:Turnover_less_Credits_DBN = L_SG:Turnover_DBN + L_SG:Credits_DBN
    L_SG:Turnover_less_Credits_JHB = L_SG:Turnover_JHB + L_SG:Credits_JHB

    L_SG:Turnover_Total         = L_SG:Turnover_less_Credits_DBN + L_SG:Turnover_less_Credits_JHB

    ! Not of invoiced days (working days) - of course will be wrong if people invoice in weekend or public holiday...
    sql_.PropSQL('SELECT COUNT(DISTINCT LTRIM(DATEPART([year], INVOICEDATEANDTIME)) + <39>/<39> + LTRIM(DATEPART([month], INVOICEDATEANDTIME)) + ' & |
                 '<39>/<39> + LTRIM(DATEPART([day], INVOICEDATEANDTIME))) ' & |
                 'FROM _Invoice WHERE InvoiceDateAndTime >= ' & |
                SQL_Get_DateT_G(LO:From_Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(LO:To_Date + 1,,1))
    IF sql_.Next_Q() > 0
       L_SG:Work_Days           = sql_.Data_G.F1
    .
    DO Audit_Check              ! Checks and outputs DIs on WEnd or Public holidays


    L_SG:Broking_Kg_DBN         = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 2, 2,, LOC:Transaction_Log_Name)       ! Broking
    L_SG:Broking_Kg_JHB         = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 2, 2,, LOC:Transaction_Log_Name)       ! Broking
    L_SG:Broking_Kg_Total       = L_SG:Broking_Kg_DBN + L_SG:Broking_Kg_JHB

    L_SG:Overnight_Kg_DBN       = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch1, LO:To_Date, L_SG:Output, 0, 1, 2,, LOC:Transaction_Log_Name)       ! Broking
    L_SG:Overnight_Kg_JHB       = Get_Invoices(LO:From_Date, 3, 1,, L_SG:Branch2, LO:To_Date, L_SG:Output, 0, 1, 2,, LOC:Transaction_Log_Name)       ! Broking
    L_SG:Overnight_Kg_Total     = L_SG:Overnight_Kg_DBN + L_SG:Overnight_Kg_JHB

    L_SG:Kg_Total               = L_SG:Broking_Kg_Total + L_SG:Overnight_Kg_Total

    L_SG:Kg_Per_Day             = L_SG:Kg_Total / L_SG:Work_Days
    L_SG:Kg_Per_Day_DBN         = (L_SG:Broking_Kg_DBN + L_SG:Overnight_Kg_DBN) / L_SG:Work_Days
    L_SG:Kg_Per_Day_JHB         = (L_SG:Broking_Kg_JHB + L_SG:Overnight_Kg_JHB) / L_SG:Work_Days


    ! Get_InvoicesTransporter
    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type,
    ! (LONG  , BYTE    , BYTE=0, BYTE=0        , BYTE=0        , <ULONG>, ULONG=0, <LONG>  , BYTE=0     , BYTE=0        ,
    !   1           2       3         4               5           6        7         8          9           10
    !  p:Output, p:Source, p:Info  , p:LogName, p:Manifest_Type)
    !  BYTE=0  , BYTE=0  , <STRING>, <STRING> , BYTE=0 ),STRING
    !    11        12        13        14         15
    ! p:Option                                                  2
    !   0.  - Total Charges inc.
    !   1.  - Excl VAT
    !   2.  - VAT
    ! p:Type                                                    3
    !   0.  - All
    !   1.  - Invoices                      Total >= 0.0
    !   2.  - Credit Notes                  Total < 0.0
    ! p:DeliveryLegs                                            5
    !   0.  - Both
    !   1.  - Del Legs only
    !   2.  - No Del Legs
    ! p:MID                                                     6
    !   0   = Not for a MID
    !   x   = for a MID
    ! p:Invoice_Type (was Manifest_Type)                        10
    !   0   - All
    !   1   - Overnight
    !   2   - Broking
    ! p:Manifest_Type                                           15
    !   0   - All
    !   1   - Overnight
    !   2   - Broking
    ! p:Extra_Inv                                               9
    !   Only
    ! p:Source                                                  12
    !   1   - Management Profit
    !   2   - Management Profit Summary

    ! Delivery legs (1), Broking Manifest type
!   L_SG:Sales_Broking_DBN     += Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2)
!   L_SG:Sales_Broking_JHB     += Get_InvoicesTransporter(LO:From_Date, 0, 0,,1,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2)

    ! Turnover - Invoices (p3 = 1), Broking (p10 = 2), Legs & Other (p5 = 0)

    ! Invoices & Credits (p3 = 0), No Del Legs (p5 = 2), Broking (p10 = 2)
    L_SG:Costs_Broking_DBN      = Get_InvoicesTransporter(LO:From_Date, 0, 0,,2,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2,'T-CB', LOC:Transaction_Log_Name)
!    L_SG:Costs_Broking_DBN     += Get_InvoicesTransporter(LO:From_Date, 0, 0,,2,, L_SG:Branch1, LO:To_Date,, 2, L_SG:Output, 2,'T-CB', LOC:Transaction_Log_Name)
    L_SG:Costs_Broking_JHB      = Get_InvoicesTransporter(LO:From_Date, 0, 0,,2,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2,'T-CB', LOC:Transaction_Log_Name)
!    L_SG:Costs_Broking_JHB     += Get_InvoicesTransporter(LO:From_Date, 0, 0,,2,, L_SG:Branch2, LO:To_Date,, 2, L_SG:Output, 2,'T-CB', LOC:Transaction_Log_Name)

    L_SG:Costs_Broking_DBN     += L_SG:Extra_Leg_DBN
    L_SG:Costs_Broking_JHB     += L_SG:Extra_Leg_JHB

    ! 4 July 12 - Yvonne noticed double credit applied here, above we are getting Costs_Broking including ALL, so we dont need to aagin deduct the credit here
    !            in theory anyway.  So next 2 lines changed
!    L_SG:Transporter_Costs_DBN  = L_SG:Costs_Broking_DBN + L_SG:Extra_Invoices_DBN + L_SG:Transporter_Debits_DBN - L_SG:Transporter_Credits_DBN
!    L_SG:Transporter_Costs_JHB  = L_SG:Costs_Broking_JHB + L_SG:Extra_Invoices_JHB + L_SG:Transporter_Debits_JHB - L_SG:Transporter_Credits_JHB
    L_SG:Transporter_Costs_DBN  = L_SG:Costs_Broking_DBN + L_SG:Extra_Invoices_DBN + L_SG:Transporter_Debits_DBN 
    L_SG:Transporter_Costs_JHB  = L_SG:Costs_Broking_JHB + L_SG:Extra_Invoices_JHB + L_SG:Transporter_Debits_JHB 

    ! Overnight Manifest type
    L_SG:Loadmaster_DBN         = Get_InvoicesTransporter(LO:From_Date, 0, 1,,,, L_SG:Branch1, LO:To_Date,, 1, L_SG:Output, 2,'T-CO', LOC:Transaction_Log_Name)
    L_SG:Loadmaster_JHB         = Get_InvoicesTransporter(LO:From_Date, 0, 1,,,, L_SG:Branch2, LO:To_Date,, 1, L_SG:Output, 2,'T-CO', LOC:Transaction_Log_Name)



    L_SG:Average_Cost_Per_Kg    = L_SG:Turnover_Total / L_SG:Kg_Total
            !(L_SG:Costs_Broking_DBN + L_SG:Costs_Broking_JHB + L_SG:Loadmaster_DBN + L_SG:Loadmaster_JHB) / L_SG:Kg_Total

    !L_SG:Turnover_Per_Day       = L_SG:Turnover_Total / L_SG:Work_Days
    L_SG:Turnover_Per_Day       = (L_SG:Turnover_less_Credits_DBN + L_SG:Turnover_less_Credits_JHB) / L_SG:Work_Days
    L_SG:Turnover_Per_Day_DBN   = L_SG:Turnover_less_Credits_DBN / L_SG:Work_Days
    L_SG:Turnover_Per_Day_JHB   = L_SG:Turnover_less_Credits_JHB / L_SG:Work_Days

    L_SG:GP_Broking_DBN         = L_SG:Sales_Broking_DBN_Total - L_SG:Transporter_Costs_DBN
    L_SG:GP_Broking_JHB         = L_SG:Sales_Broking_JHB_Total - L_SG:Transporter_Costs_JHB

    L_SG:GP_Per_Broking_DBN     = (L_SG:GP_Broking_DBN / L_SG:Sales_Broking_DBN_Total) * 100
    L_SG:GP_Per_Broking_JHB     = (L_SG:GP_Broking_JHB / L_SG:Sales_Broking_JHB_Total) * 100

    L_SG:GP_Overnight_DBN       = L_SG:Sales_Overnight_DBN - L_SG:Loadmaster_DBN
    !L_SG:GP_Overnight_DBN      += L_SG:Extra_Invoices_DBN + L_SG:Transporter_Credits_DBN - L_SG:Transporter_Debits_DBN
    L_SG:GP_Overnight_JHB       = L_SG:Sales_Overnight_JHB - L_SG:Loadmaster_JHB
    !L_SG:GP_Overnight_JHB      += L_SG:Extra_Invoices_JHB + L_SG:Transporter_Credits_JHB - L_SG:Transporter_Debits_JHB

    L_SG:GP_Per_Overnight_DBN   = (L_SG:GP_Overnight_DBN / L_SG:Sales_Overnight_DBN) * 100
    L_SG:GP_Per_Overnight_JHB   = (L_SG:GP_Overnight_JHB / L_SG:Sales_Overnight_JHB) * 100


!    L_SG:Gross_GP_DBN           = L_SG:Turnover_less_Credits_DBN - (L_SG:Costs_Broking_DBN + L_SG:Loadmaster_DBN)
!    L_SG:Gross_GP_JHB           = L_SG:Turnover_less_Credits_JHB - (L_SG:Costs_Broking_JHB + L_SG:Loadmaster_JHB)

    L_SG:Gross_GP_DBN           = L_SG:GP_Overnight_DBN + L_SG:GP_Broking_DBN
    L_SG:Gross_GP_JHB           = L_SG:GP_Overnight_JHB + L_SG:GP_Broking_JHB

    L_SG:Gross_GP_Total         = L_SG:Gross_GP_DBN + L_SG:Gross_GP_JHB
    L_SG:Gross_GP_Per_Day       = L_SG:Gross_GP_Total / L_SG:Work_Days


    L_SG:Loadmaster_Total       = L_SG:Loadmaster_DBN + L_SG:Loadmaster_JHB
    L_SG:Loadmaster_Per_Day     = L_SG:Loadmaster_Total / L_SG:Work_Days

    L_SG:Loadmaster_GP_Total    = L_SG:Loadmaster_Total + L_SG:Gross_GP_Total   ! Note loadmaster is not seen as a cost here but an income
    L_SG:Loadmaster_GP_Per_Day  = L_SG:Loadmaster_GP_Total / L_SG:Work_Days
    RETURN




ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
      IF LOC:Errors > 0
         MESSAGE('Completed with Errors: ' & LOC:Errors & '||Please review the System Log.', 'Print Turnover', ICON:Exclamation)
      .
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_ManagementProfit_Summary','Print_ManagementProfit_Summary','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Debtor_Listing_OLD PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:DefaultEmail     STRING(255)                           ! 
LOC:Open             BYTE                                  ! 
LOC:OnHold           BYTE                                  ! 
LOC:Closed           BYTE                                  ! 
LOC:Dormant          BYTE                                  ! 
LOC:ExportToCSV      BYTE                                  ! Export the data to a CSV file as well (can use in spreadsheets etc)
Process:View         VIEW(Clients)
                       PROJECT(CLI:ClientName)
                       PROJECT(CLI:ClientNo)
                       PROJECT(CLI:Status)
                       PROJECT(CLI:VATNo)
                       PROJECT(CLI:AID)
                       JOIN(ADD:PKey_AID,CLI:AID)
                         PROJECT(ADD:Fax)
                         PROJECT(ADD:Line1)
                         PROJECT(ADD:PhoneNo)
                         PROJECT(ADD:SUID)
                         JOIN(SUBU:PKey_SUID,ADD:SUID)
                           PROJECT(SUBU:PostalCode)
                           PROJECT(SUBU:Suburb)
                         END
                       END
                     END
ProgressWindow       WINDOW('Report Clients'),AT(,,229,174),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       SHEET,AT(2,2,225,151),USE(?Sheet1)
                         TAB('Options'),USE(?Tab_Options)
                           GROUP('Account Statuses to Include'),AT(31,25,167,73),USE(?Group1),BOXED
                             CHECK(' Open'),AT(46,39),USE(LOC:Open)
                             CHECK(' On Hold'),AT(46,54),USE(LOC:OnHold)
                             CHECK(' Closed'),AT(46,68),USE(LOC:Closed)
                             CHECK(' Dormant'),AT(46,82),USE(LOC:Dormant)
                           END
                           CHECK(' Export To CSV'),AT(31,119),USE(LOC:ExportToCSV),MSG('Export the data to a CSV f' & |
  'ile as well (can use in spreadsheets etc)'),TIP('Export the data to a CSV file as we' & |
  'll (can use in spreadsheets etc)')
                         END
                         TAB('Run'),USE(?Tab_Run),HIDE
                           PROGRESS,AT(59,56,111,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(43,44,141,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(43,70,141,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Pause'),AT(126,156,49,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                       BUTTON('Cancel'),AT(178,156,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Clients Report'),AT(250,854,7656,9646),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7656,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Client Listing'),AT(-21,21,7700),USE(?ReportTitle),FONT('Arial',18,,FONT:regular), |
  CENTER
                         BOX,AT(0,350,8000,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(604,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2927,350,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(6458,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         STRING('Client No.'),AT(73,385,,167),USE(?HeaderTitle:1),TRN
                         STRING('Client Name'),AT(667,385,1500,170),USE(?HeaderTitle:2),TRN
                         STRING('VAT No.'),AT(6563,385,990,156),USE(?HeaderTitle:6),TRN
                         STRING('Address'),AT(2979,385,1500,170),USE(?HeaderTitle:3),TRN
                       END
Detail                 DETAIL,AT(,,7656,417),USE(?Detail)
                         LINE,AT(0,0,0,427),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(604,-10,0,427),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(2927,0,0,218),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(6458,0,0,208),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(8000,0,0,427),USE(?DetailLine:5),COLOR(COLOR:Black)
                         STRING(@s20),AT(6563,31,990,156),USE(CLI:VATNo),LEFT
                         LINE,AT(3948,208,0,219),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(604,208,7656,0),USE(?Line10),COLOR(COLOR:Black)
                         STRING(@s10),AT(5729,31,625,156),USE(SUBU:PostalCode)
                         STRING(@n_10b),AT(-135,31,,170),USE(CLI:ClientNo),RIGHT,TRN
                         STRING(@s35),AT(667,31,2219,167),USE(CLI:ClientName),LEFT
                         STRING(@s35),AT(2969,31,1510,156),USE(ADD:Line1),LEFT
                         STRING(@s50),AT(4583,31,1042,156),USE(SUBU:Suburb),LEFT
                         STRING('Tel.:'),AT(677,240),USE(?String19),TRN
                         STRING(@s20),AT(927,240),USE(ADD:PhoneNo),TRN
                         STRING('Fax:'),AT(2344,240),USE(?String20),TRN
                         STRING(@s20),AT(2604,240),USE(ADD:Fax),TRN
                         STRING('Email:'),AT(4010,240),USE(?String22),TRN
                         STRING(@s255),AT(4375,240,3177,156),USE(LOC:DefaultEmail)
                         LINE,AT(0,427,8000,0),USE(?DetailEndLine:3),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,10500,7656,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Client Name' & |
      '|' & 'By Client No.' & |
      '|' & 'By Branch' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Debtor_Listing_OLD')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Open
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Open',LOC:Open)                                ! Added by: Report
  BIND('LOC:OnHold',LOC:OnHold)                            ! Added by: Report
  BIND('LOC:Closed',LOC:Closed)                            ! Added by: Report
  BIND('LOC:Dormant',LOC:Dormant)                          ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:EmailAddresses.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
      LOC:Open    = GETINI('Print_Debtor_Listing', 'Open', 1, GLO:Local_INI)
      LOC:OnHold  = GETINI('Print_Debtor_Listing', 'OnHold', 1, GLO:Local_INI)
      LOC:Closed  = GETINI('Print_Debtor_Listing', 'Closed', , GLO:Local_INI)
      LOC:Dormant = GETINI('Print_Debtor_Listing', 'Dormant', , GLO:Local_INI)
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF NOT TargetSelector.ASK(1) THEN
     SELF.Kill()
     RETURN Level:Fatal
  END
  IF NOT TargetSelector.GetPrintSelected() THEN
     SELF.SetReportTarget(TargetSelector.GetReportSelected())
  END
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Clients, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client Name')) THEN
     ThisReport.AppendOrder('+CLI:ClientName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Client No.')) THEN
     ThisReport.AppendOrder('+CLI:ClientNo')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch')) THEN
     ThisReport.AppendOrder('+CLI:BID')
  END
  ThisReport.SetFilter('(LOC:Open = 1 AND CLI:Status = 0) OR (LOC:OnHold = 1 AND CLI:Status = 1) OR (LOC:Closed = 1 AND CLI:Status = 2) OR(LOC:Dormant = 1 AND CLI:Status = 3)')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Clients.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Pause
      ThisWindow.Update()
          UNHIDE(?Tab_Run)
          HIDE(?Tab_Options)
      
      
          PUTINI('Print_Debtor_Listing', 'Open', LOC:Open, GLO:Local_INI)
          PUTINI('Print_Debtor_Listing', 'OnHold', LOC:OnHold, GLO:Local_INI)
          PUTINI('Print_Debtor_Listing', 'Closed', LOC:Closed, GLO:Local_INI)
          PUTINI('Print_Debtor_Listing', 'Dormant', LOC:Dormant, GLO:Local_INI)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      CLEAR(LOC:DefaultEmail)
  
      CLEAR(EMAI:Record, -1)
      EMAI:CID    = CLI:CID
      SET(EMAI:FKey_CID, EMAI:FKey_CID)
      LOOP
         NEXT(EmailAddresses)
         IF ERRORCODE()
            BREAK
         .
         IF EMAI:CID ~= CLI:CID
            BREAK
         .
  
         IF EMAI:RateLetter = TRUE
            LOC:DefaultEmail  = EMAI:EmailAddress
         .
  
         IF EMAI:DefaultAddress = TRUE
            LOC:DefaultEmail  = EMAI:EmailAddress
            BREAK
      .  .
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Debtor_Listing','Print_Debtor_Listing','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Creditor_Age_Analysis_Landscape PROCEDURE (p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:IncludeArchived)

!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
TintRptViewer  TinReportViewer
!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
Progress:Thermometer BYTE                                  ! 
LOC:Locals           GROUP,PRE(LOC)                        ! 
Date                 DATE                                  ! 
Time                 TIME                                  ! 
                     END                                   ! 
LOC:IncludeArchived  BYTE                                  ! 
LOC:STRID            ULONG                                 ! Statement Run ID
LOC:Statement_Q      QUEUE,PRE(L_SQ)                       ! Statement ID
STID                 ULONG                                 ! Statement ID
CID                  ULONG                                 ! Client ID
                     END                                   ! 
LOC:Info             STRING(35)                            ! 
LOC:Grand_Totals     GROUP,PRE(L_GT)                       ! 
Total                DECIMAL(10,2)                         ! Total
Current              DECIMAL(10,2)                         ! Current
Days30               DECIMAL(10,2)                         ! 30 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days90               DECIMAL(10,2)                         ! 90 Days
Number               ULONG                                 ! Number of entries
Current_PerTot       DECIMAL(6,1)                          ! 
Days30_PerTot        DECIMAL(6,1)                          ! Current
Days60_PerTot        DECIMAL(6,1)                          ! Current
Days90_PerTot        DECIMAL(6,1)                          ! Current
                     END                                   ! 
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
Process:View         VIEW(Transporter)
                       PROJECT(TRA:AID)
                       PROJECT(TRA:Archived)
                       PROJECT(TRA:Comments)
                       PROJECT(TRA:TID)
                       PROJECT(TRA:TransporterName)
                       PROJECT(TRA:BID)
                       JOIN(BRA:PKey_BID,TRA:BID)
                       END
                     END
ProgressWindow       WINDOW('Transporter Age Analysis'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Clients Report'),AT(250,844,10935,6500),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,10792,573),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Transporter Age Analysis'),AT(427,10,9927,333),USE(?ReportTitle),FONT('Arial',18,, |
  FONT:bold),CENTER
                         LINE,AT(3177,365,0,200),USE(?HeaderLine:8),COLOR(COLOR:Black)
                         BOX,AT(0,354,10781,208),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(719,365,0,200),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2021,365,0,200),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(6510,365,0,200),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(7344,365,0,200),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(8187,365,0,200),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(9042,365,0,200),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(9906,365,0,200),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         STRING('ID'),AT(156,385,,170),USE(?HeaderTitle:1),TRN
                         STRING('Creditor Name'),AT(760,385,900,170),USE(?HeaderTitle:2),TRN
                         STRING('Details'),AT(2052,385,969,167),USE(?HeaderTitle:3),TRN
                         STRING('Comments'),AT(3208,385,625,167),USE(?HeaderTitle:9),CENTER,TRN
                         STRING('90 Days'),AT(6508,385,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('60 Days'),AT(7341,385,833,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('30 Days'),AT(8185,385,833,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Current'),AT(9039,385,833,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Total'),AT(9904,385,833,167),USE(?HeaderTitle:8),CENTER,TRN
                       END
Detail                 DETAIL,AT(0,0,10792,240),USE(?Detail)
                         LINE,AT(0,0,0,238),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(719,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(2021,0,0,238),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(3177,0,0,238),USE(?HeaderLine:9),COLOR(COLOR:Black)
                         LINE,AT(6510,0,0,238),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(7344,0,0,238),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(8187,0,0,238),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(9042,0,0,238),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(9906,0,0,238),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(10771,0,0,238),USE(?DetailLine:8),COLOR(COLOR:Black)
                         STRING(@n_10),AT(31,31,667,170),USE(TRA:TID),RIGHT,TRN
                         STRING(@s35),AT(760,31,1219,167),USE(TRA:TransporterName),LEFT
                         STRING(@s35),AT(2052,31,1094,167),USE(LOC:Info),TRN
                         STRING(@s255),AT(3208,31,3229),USE(TRA:Comments),FONT('Microsoft Sans Serif',,,FONT:regular)
                         STRING(@n-14.2),AT(6458,31),USE(L_SI:Days90),RIGHT(1),TRN
                         STRING(@n-14.2),AT(7292,31),USE(L_SI:Days60),RIGHT(1),TRN
                         STRING(@n-14.2),AT(8135,31),USE(L_SI:Days30),RIGHT(1),TRN
                         STRING(@n-14.2),AT(8990,31),USE(L_SI:Current),RIGHT(1),TRN
                         STRING(@n-14.2),AT(9854,31),USE(L_SI:Total),RIGHT(1),TRN
                         LINE,AT(0,240,10792,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
detail_totals          DETAIL,AT(0,0,10802,771),USE(?detail_totals)
                         LINE,AT(1094,167,6771,0),USE(?Line20),COLOR(COLOR:Black),LINEWIDTH(10)
                         STRING(@n-14.2),AT(5250,246,833,167),USE(L_GT:Days30),RIGHT(1),TRN
                         STRING(@n-14.2),AT(3573,246),USE(L_GT:Days90),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4406,246,833,167),USE(L_GT:Days60),RIGHT(1),TRN
                         STRING(@n13),AT(1979,246),USE(L_GT:Number),RIGHT(1)
                         STRING(@n-14.2),AT(6104,246,833,167),USE(L_GT:Current),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6969,246,833,167),USE(L_GT:Total),RIGHT(1),TRN
                         STRING('% of Total:'),AT(1115,469),USE(?String29:2),TRN
                         STRING(@n-9.1),AT(5250,469,833,167),USE(L_GT:Days30_PerTot),RIGHT(1),TRN
                         STRING(@n-9.1),AT(4406,469,833,167),USE(L_GT:Days60_PerTot),RIGHT(1),TRN
                         STRING(@n-9.1),AT(3573,469,833,167),USE(L_GT:Days90_PerTot),RIGHT(1),TRN
                         STRING(@n-9.1),AT(6104,469,833,167),USE(L_GT:Current_PerTot),RIGHT(1),TRN
                         LINE,AT(1094,677,6771,0),USE(?Line20:2),COLOR(COLOR:Black),LINEWIDTH(10)
                         STRING('Creditors Listed:'),AT(1115,246),USE(?String29),TRN
                       END
                       FOOTER,AT(250,7400,10802,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(9906,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

View_Addr       VIEW(AddressContacts)
    !PROJECT()
    .


Addr_View       ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Transporter Name' & |
      '|' & 'By Transporter ID' & |
      '|' & 'By Branch' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:detail_totals)
  
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
    ENDPAGE(SELF.Report)
    IF SELF.SkipPreview
      TintRptViewer.CheckForPageNum(Self.PreviewQueue)
      TintRptViewer.SetupCopiesCollate(Self.PreviewQueue)
      if (TinTRptViewer.GetEjectDir())
        TinTRptViewer.ReversePreviewQueue(Self.PreviewQueue)
      end
      SELF.Report{PROP:FlushPreview} = True
      GlobalResponse = RequestCompleted
    ELSE
      GlobalResponse = TintRptViewer.Display(Report,Self.PreviewQueue,Report{Prop:Text},'Page Width',0)
      SELF.SkipPreview = TRUE
    END
    FREE(SELF.PreviewQueue)
   END
  ! You can place code in this embed point to test GlobalResponse to see if report printed or not.
  ! CASE GlobalResponse
  ! OF RequestCompleted     ! Report Was Printed
  !   ! Do Something
  ! OF RequestCancelled     ! Report Was NOT Printed
  !   ! Do Something
  ! END
   OMIT('***TINTOOLS***')
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  PARENT.AskPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  ***TINTOOLS***
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Creditor_Age_Analysis_Landscape')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:IncludeArchived',LOC:IncludeArchived)          ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:AddressContacts.SetOpenRelated()
  Relate:AddressContacts.Open                              ! File AddressContacts used by this procedure, so make sure it's RelationManager is open
  Access:_Statements.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Statement_Runs.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Transporter, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter Name')) THEN
     ThisReport.AppendOrder('+TRA:TransporterName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter ID')) THEN
     ThisReport.AppendOrder('+TRA:TID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch')) THEN
     ThisReport.AppendOrder('+BRA:BranchName,+TRA:TransporterName')
  END
  ThisReport.SetFilter('LOC:IncludeArchived = 1 OR TRA:Archived = 0')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Transporter.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 5                           ! Assign timer interval
      LOC:Date        = p:Date
      LOC:Time        = DEFORMAT('12:00:00',@t4)
  
  
  
  
     ! (p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:IncludeArchived)
     
     LOC:IncludeArchived  = p:IncludeArchived
      Process_InvoiceTransporter_StatusUpToDate('0')
      Process_TransporterPayments_StatusUpToDate('0')
      IF p:OutPut > 0       ! (p:Text, p:Heading, p:Name)
         Add_Log(',Transporter ID,Invoice No.,Invoice Date,Invoice Cost, Invoice VAT,Credits,Payments', 'Headings', 'Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv', TRUE)
      .
  
  
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
    ! set printer eject style
    TinTRptViewer.SetEjectDir(TinEject:PageFaceUp)
    IF TintRptViewer.PrintViewerSetup() THEN
      Self:Response = RequestCancelled       ! Changed by LH 4/19/98
      POST(EVENT:CloseDown)                  ! Changed by LH 4/19/98
      RETURN Level:NOTIFY                    ! Changed by LH 4/19/98
    END
    SELF.SkipPreview = TintRptViewer.SkipPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressContacts.Close
  END
      IF p:OutPut > 0
         CASE MESSAGE('Would you like to load the accumulation details now?||File: ' & PATH() & '\Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv', 'Age Analysis', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(0{PROP:Handle}, PATH() & '\Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      ! Get the suitable Statement
      CLEAR(LOC:Info)
                                               ! (p:TID, p:Date, p:Owing, p:Statement_Info, p:MonthEndDay, p:OutPut)
      Junk_#          = Gen_Aging_Transporter(TRA:TID, LOC:Date, , LOC:Statement_Info, p:MonthEndDay, p:OutPut)
  
      ! Totals
      L_GT:Total     += L_SI:Total
      L_GT:Current   += L_SI:Current
      L_GT:Days30    += L_SI:Days30
      L_GT:Days60    += L_SI:Days60
      L_GT:Days90    += L_SI:Days90
      L_GT:Number    += 1
  
  
  
      ! Put contact details in LOC:Info
      Addr_View.Init(View_Addr, Relate:AddressContacts)
      Addr_View.AddSortOrder(ADDC:FKey_AID)
      Addr_View.AppendOrder('ADDC:ContactName')
      Addr_View.AddRange(ADDC:AID, TRA:AID)
  
      Addr_View.Reset()
  
      IF Addr_View.Next() = LEVEL:Benign
         LOC:Info     = ADDC:ContactName
      .
  
      Addr_View.Kill()
  
  
      ADD:AID = TRA:AID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
         LOC:Info             = CLIP(ADD:PhoneNo) & ', ' & LOC:Info
      .
  
  
      L_GT:Current_PerTot     = L_GT:Current / L_GT:Total * 100
      L_GT:Days30_PerTot      = L_GT:Days30  / L_GT:Total * 100
      L_GT:Days60_PerTot      = L_GT:Days60  / L_GT:Total * 100
      L_GT:Days90_PerTot      = L_GT:Days90  / L_GT:Total * 100
  ReturnValue = PARENT.TakeRecord()
        IF p:Ignore_Zero_Bal = TRUE
           IF (L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current + L_SI:Total) = 0.0
              L_GT:Number -= 1
  
              !L_GT:Total = 0.0 AND L_GT:Current = 0.0 AND L_GT:Days30 = 0.0 AND L_GT:Days60 = 0.0 AND L_GT:Days90 = 0.0
              !db.debugout('[Print_Debtors_Age_Analysis]  0.0 on all values')
              
  
              SkipDetails  = TRUE
        .  .
  IF 0
    PRINT(RPT:detail_totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Creditor_Age_Analysis','Print_Creditor_Age_Analysis','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Creditor_Age_Analysis_Landscape_backup PROCEDURE (p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:IncludeArchived)

!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
TintRptViewer  TinReportViewer
!--------------------------------------------------------------------------
! Tinman Report Viewer
!--------------------------------------------------------------------------
Progress:Thermometer BYTE                                  ! 
LOC:Locals           GROUP,PRE(LOC)                        ! 
Date                 DATE                                  ! 
Time                 TIME                                  ! 
                     END                                   ! 
LOC:IncludeArchived  BYTE                                  ! 
LOC:STRID            ULONG                                 ! Statement Run ID
LOC:Statement_Q      QUEUE,PRE(L_SQ)                       ! Statement ID
STID                 ULONG                                 ! Statement ID
CID                  ULONG                                 ! Client ID
                     END                                   ! 
LOC:Info             STRING(35)                            ! 
LOC:Grand_Totals     GROUP,PRE(L_GT)                       ! 
Total                DECIMAL(10,2)                         ! Total
Current              DECIMAL(10,2)                         ! Current
Days30               DECIMAL(10,2)                         ! 30 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days90               DECIMAL(10,2)                         ! 90 Days
Number               ULONG                                 ! Number of entries
Current_PerTot       DECIMAL(6,1)                          ! 
Days30_PerTot        DECIMAL(6,1)                          ! Current
Days60_PerTot        DECIMAL(6,1)                          ! Current
Days90_PerTot        DECIMAL(6,1)                          ! Current
                     END                                   ! 
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
Process:View         VIEW(Transporter)
                       PROJECT(TRA:AID)
                       PROJECT(TRA:Archived)
                       PROJECT(TRA:TID)
                       PROJECT(TRA:TransporterName)
                       PROJECT(TRA:BID)
                       JOIN(BRA:PKey_BID,TRA:BID)
                       END
                     END
ProgressWindow       WINDOW('Transporter Age Analysis'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Clients Report'),AT(250,844,10935,9656),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,10792,573),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Transporter Age Analysis'),AT(427,10,9927,333),USE(?ReportTitle),FONT('Arial',18,, |
  FONT:bold),CENTER
                         LINE,AT(3177,365,0,200),USE(?HeaderLine:8),COLOR(COLOR:Black)
                         BOX,AT(0,354,10781,208),USE(?HeaderBox),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(719,365,0,200),USE(?HeaderLine:1),COLOR(COLOR:Black)
                         LINE,AT(2021,365,0,200),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(6510,365,0,200),USE(?HeaderLine:3),COLOR(COLOR:Black)
                         LINE,AT(7344,365,0,200),USE(?HeaderLine:4),COLOR(COLOR:Black)
                         LINE,AT(8187,365,0,200),USE(?HeaderLine:5),COLOR(COLOR:Black)
                         LINE,AT(9042,365,0,200),USE(?HeaderLine:6),COLOR(COLOR:Black)
                         LINE,AT(9906,365,0,200),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         STRING('ID'),AT(156,385,,170),USE(?HeaderTitle:1),TRN
                         STRING('Creditor Name'),AT(760,385,900,170),USE(?HeaderTitle:2),TRN
                         STRING('Details'),AT(2052,385,969,167),USE(?HeaderTitle:3),TRN
                         STRING('Comments'),AT(3208,385,625,167),USE(?HeaderTitle:9),CENTER,TRN
                         STRING('90 Days'),AT(6508,385,833,167),USE(?HeaderTitle:4),CENTER,TRN
                         STRING('60 Days'),AT(7341,385,833,167),USE(?HeaderTitle:5),CENTER,TRN
                         STRING('30 Days'),AT(8185,385,833,167),USE(?HeaderTitle:6),CENTER,TRN
                         STRING('Current'),AT(9039,385,833,167),USE(?HeaderTitle:7),CENTER,TRN
                         STRING('Total'),AT(9904,385,833,167),USE(?HeaderTitle:8),CENTER,TRN
                       END
Detail                 DETAIL,AT(0,0,10792,240),USE(?Detail)
                         LINE,AT(0,0,0,238),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(719,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(2021,0,0,238),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(3177,0,0,238),USE(?HeaderLine:9),COLOR(COLOR:Black)
                         LINE,AT(6510,0,0,238),USE(?DetailLine:3),COLOR(COLOR:Black)
                         LINE,AT(7344,0,0,238),USE(?DetailLine:4),COLOR(COLOR:Black)
                         LINE,AT(8187,0,0,238),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(9042,0,0,238),USE(?DetailLine:6),COLOR(COLOR:Black)
                         LINE,AT(9906,0,0,238),USE(?DetailLine:7),COLOR(COLOR:Black)
                         LINE,AT(10771,0,0,238),USE(?DetailLine:8),COLOR(COLOR:Black)
                         STRING(@n_10),AT(31,31,667,170),USE(TRA:TID),RIGHT,TRN
                         STRING(@s35),AT(760,31,1219,167),USE(TRA:TransporterName),LEFT
                         STRING(@s35),AT(2052,31,1094,167),USE(LOC:Info),TRN
                         STRING(@s255),AT(3208,31,3229),USE(?TRA:Comments),FONT('Microsoft Sans Serif',,,FONT:regular)
                         STRING(@n-14.2),AT(6458,31),USE(L_SI:Days90),RIGHT(1),TRN
                         STRING(@n-14.2),AT(7292,31),USE(L_SI:Days60),RIGHT(1),TRN
                         STRING(@n-14.2),AT(8135,31),USE(L_SI:Days30),RIGHT(1),TRN
                         STRING(@n-14.2),AT(8990,31),USE(L_SI:Current),RIGHT(1),TRN
                         STRING(@n-14.2),AT(9854,31),USE(L_SI:Total),RIGHT(1),TRN
                         LINE,AT(0,240,10792,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
detail_totals          DETAIL,AT(0,0,10802,771),USE(?detail_totals)
                         LINE,AT(1094,167,6771,0),USE(?Line20),COLOR(COLOR:Black),LINEWIDTH(10)
                         STRING(@n-14.2),AT(5250,246,833,167),USE(L_GT:Days30),RIGHT(1),TRN
                         STRING(@n-14.2),AT(3573,246),USE(L_GT:Days90),RIGHT(1),TRN
                         STRING(@n-14.2),AT(4406,246,833,167),USE(L_GT:Days60),RIGHT(1),TRN
                         STRING(@n13),AT(1979,246),USE(L_GT:Number),RIGHT(1)
                         STRING(@n-14.2),AT(6104,246,833,167),USE(L_GT:Current),RIGHT(1),TRN
                         STRING(@n-14.2),AT(6969,246,833,167),USE(L_GT:Total),RIGHT(1),TRN
                         STRING('% of Total:'),AT(1115,469),USE(?String29:2),TRN
                         STRING(@n-9.1),AT(5250,469,833,167),USE(L_GT:Days30_PerTot),RIGHT(1),TRN
                         STRING(@n-9.1),AT(4406,469,833,167),USE(L_GT:Days60_PerTot),RIGHT(1),TRN
                         STRING(@n-9.1),AT(3573,469,833,167),USE(L_GT:Days90_PerTot),RIGHT(1),TRN
                         STRING(@n-9.1),AT(6104,469,833,167),USE(L_GT:Current_PerTot),RIGHT(1),TRN
                         LINE,AT(1094,677,6771,0),USE(?Line20:2),COLOR(COLOR:Black),LINEWIDTH(10)
                         STRING('Creditors Listed:'),AT(1115,246),USE(?String29),TRN
                       END
                       FOOTER,AT(250,10500,10802,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(9906,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ProcessSortSelectionVariable         STRING(100)           ! Used in the sort order selection
ProcessSortSelectionCanceled         BYTE                  ! Used in the sort order selection to know if it was canceled
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

View_Addr       VIEW(AddressContacts)
    !PROJECT()
    .


Addr_View       ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ProcessSortSelectionWindow    ROUTINE
 DATA
SortSelectionWindow WINDOW('Select the Order'),AT(,,165,92),FONT('Microsoft Sans Serif',8,,),CENTER,GRAY,DOUBLE
       PROMPT('Select the order to process the data.'),AT(5,4,156,18),FONT(,,,FONT:bold),USE(?SortMessage:Prompt)
       LIST,AT(5,26,155,42),FONT('Microsoft Sans Serif',8,,FONT:bold),USE(ProcessSortSelectionVariable,,?SortSelectionList),VSCROLL,FORMAT('100L@s100@'),FROM('')
       BUTTON('&OK'),AT(51,74,52,14),ICON('SOK.ICO'),MSG('Accept data and close the window'),TIP('Accept data and close the window'),USE(?SButtonOk),LEFT,FLAT
       BUTTON('&Cancel'),AT(107,74,52,14),ICON('SCANCEL.ICO'),MSG('Cancel operation'),TIP('Cancel operation'),USE(?SButtonCancel),LEFT,FLAT
     END
 CODE
      ProcessSortSelectionCanceled=1
      ProcessSortSelectionVariable=''
      OPEN(SortSelectionWindow)
      ?SortSelectionList{PROP:FROM}=''&|
      'By Transporter Name' & |
      '|' & 'By Transporter ID' & |
      '|' & 'By Branch' & |
      ''
      ?SortSelectionList{PROP:Selected}=1
      ?SortSelectionList{Prop:Alrt,252} = MouseLeft2

      ACCEPT
        CASE EVENT()
        OF Event:OpenWindow
            CYCLE
        OF Event:Timer
            CYCLE
        END
        CASE FIELD()
        OF ?SortSelectionList
          IF KEYCODE() = MouseLeft2
              ProcessSortSelectionCanceled=0
              POST(Event:CloseWindow)
          END
        END
        CASE ACCEPTED()
        OF ?SButtonCancel
            ProcessSortSelectionVariable=''
            ProcessSortSelectionCanceled=1
            POST(Event:CloseWindow)
        OF ?SButtonOk
            ProcessSortSelectionCanceled=0
            POST(Event:CloseWindow)
        END
      END
      CLOSE(SortSelectionWindow)
 IF ProcessSortSelectionCanceled THEN
    ProcessSortSelectionVariable=''
 END
 EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      PRINT(RPT:detail_totals)
  
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
    ENDPAGE(SELF.Report)
    IF SELF.SkipPreview
      TintRptViewer.CheckForPageNum(Self.PreviewQueue)
      TintRptViewer.SetupCopiesCollate(Self.PreviewQueue)
      if (TinTRptViewer.GetEjectDir())
        TinTRptViewer.ReversePreviewQueue(Self.PreviewQueue)
      end
      SELF.Report{PROP:FlushPreview} = True
      GlobalResponse = RequestCompleted
    ELSE
      GlobalResponse = TintRptViewer.Display(Report,Self.PreviewQueue,Report{Prop:Text},'Page Width',0)
      SELF.SkipPreview = TRUE
    END
    FREE(SELF.PreviewQueue)
   END
  ! You can place code in this embed point to test GlobalResponse to see if report printed or not.
  ! CASE GlobalResponse
  ! OF RequestCompleted     ! Report Was Printed
  !   ! Do Something
  ! OF RequestCancelled     ! Report Was NOT Printed
  !   ! Do Something
  ! END
   OMIT('***TINTOOLS***')
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  PARENT.AskPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  ***TINTOOLS***
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Creditor_Age_Analysis_Landscape_backup')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:IncludeArchived',LOC:IncludeArchived)          ! Added by: Report
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Do ProcessSortSelectionWindow
  IF ProcessSortSelectionCanceled THEN
     RETURN LEvel:Fatal
  END
  Relate:AddressContacts.SetOpenRelated()
  Relate:AddressContacts.Open                              ! File AddressContacts used by this procedure, so make sure it's RelationManager is open
  Access:_Statements.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Statement_Runs.UseFile                           ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Creditor_Age_Analysis_Landscape_backup',ProgressWindow) ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Transporter, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  IF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter Name')) THEN
     ThisReport.AppendOrder('+TRA:TransporterName')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Transporter ID')) THEN
     ThisReport.AppendOrder('+TRA:TID')
  ELSIF (UPPER(CLIP(ProcessSortSelectionVariable))=UPPER('By Branch')) THEN
     ThisReport.AppendOrder('+BRA:BranchName,+TRA:TransporterName')
  END
  ThisReport.SetFilter('LOC:IncludeArchived = 1 OR TRA:Archived = 0')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Transporter.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 5                           ! Assign timer interval
      LOC:Date        = p:Date
      LOC:Time        = DEFORMAT('12:00:00',@t4)
  
  
  
  
     ! (p:Date, p:MonthEndDay, p:Ignore_Zero_Bal, p:OutPut, p:IncludeArchived)
     
     LOC:IncludeArchived  = p:IncludeArchived
  !    Process_InvoiceTransporter_StatusUpToDate('0')
  !    Process_TransporterPayments_StatusUpToDate('0')
      IF p:OutPut > 0       ! (p:Text, p:Heading, p:Name)
         Add_Log(',Transporter ID,Invoice No.,Invoice Date,Invoice Cost, Invoice VAT,Credits,Payments', 'Headings', 'Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv', TRUE)
      .
  
  
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
    ! set printer eject style
    TinTRptViewer.SetEjectDir(TinEject:PageFaceUp)
    IF TintRptViewer.PrintViewerSetup() THEN
      Self:Response = RequestCancelled       ! Changed by LH 4/19/98
      POST(EVENT:CloseDown)                  ! Changed by LH 4/19/98
      RETURN Level:NOTIFY                    ! Changed by LH 4/19/98
    END
    SELF.SkipPreview = TintRptViewer.SkipPreview
  !--------------------------------------------------------------------------
  ! Tinman Report Viewer
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressContacts.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Creditor_Age_Analysis_Landscape_backup',ProgressWindow) ! Save window data to non-volatile store
  END
      IF p:OutPut > 0
         CASE MESSAGE('Would you like to load the accumulation details now?||File: ' & PATH() & '\Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv', 'Age Analysis', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
         OF BUTTON:Yes
            ISExecute(0{PROP:Handle}, PATH() & '\Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv')
      .  .
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      ! Get the suitable Statement
      CLEAR(LOC:Info)
                                               ! (p:TID, p:Date, p:Owing, p:Statement_Info, p:MonthEndDay, p:OutPut)
      Junk_#          = Gen_Aging_Transporter(TRA:TID, LOC:Date, , LOC:Statement_Info, p:MonthEndDay, p:OutPut)
                        
      ! Totals
      L_GT:Total     += L_SI:Total
      L_GT:Current   += L_SI:Current
      L_GT:Days30    += L_SI:Days30
      L_GT:Days60    += L_SI:Days60
      L_GT:Days90    += L_SI:Days90
      L_GT:Number    += 1
  
  
  
      ! Put contact details in LOC:Info
      Addr_View.Init(View_Addr, Relate:AddressContacts)
      Addr_View.AddSortOrder(ADDC:FKey_AID)
      Addr_View.AppendOrder('ADDC:ContactName')
      Addr_View.AddRange(ADDC:AID, TRA:AID)
  
      Addr_View.Reset()
  
      IF Addr_View.Next() = LEVEL:Benign
         LOC:Info     = ADDC:ContactName
      .
  
      Addr_View.Kill()
  
  
      ADD:AID = TRA:AID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
         LOC:Info             = CLIP(ADD:PhoneNo) & ', ' & LOC:Info
      .
  
  
      L_GT:Current_PerTot     = L_GT:Current / L_GT:Total * 100
      L_GT:Days30_PerTot      = L_GT:Days30  / L_GT:Total * 100
      L_GT:Days60_PerTot      = L_GT:Days60  / L_GT:Total * 100
      L_GT:Days90_PerTot      = L_GT:Days90  / L_GT:Total * 100
  ReturnValue = PARENT.TakeRecord()
        IF p:Ignore_Zero_Bal = TRUE
           IF (L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current + L_SI:Total) = 0.0
              L_GT:Number -= 1
  
              !L_GT:Total = 0.0 AND L_GT:Current = 0.0 AND L_GT:Days30 = 0.0 AND L_GT:Days60 = 0.0 AND L_GT:Days90 = 0.0
              !db.debugout('[Print_Debtors_Age_Analysis]  0.0 on all values')
              
  
              SkipDetails  = TRUE
        .  .
  IF 0
    PRINT(RPT:detail_totals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Creditor_Age_Analysis','Print_Creditor_Age_Analysis','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

