

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS014.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Print_Statements PROCEDURE (p:STRID)

Locals               GROUP,PRE(LO)                         ! 
Print_Type           BYTE                                  ! 
Complete             BYTE                                  ! 
Time                 LONG                                  ! 
Last_Time            LONG                                  ! 
Loops                LONG(10)                              ! 
Page_Print_Time      LONG(4)                               ! 
Print_Now            LONG                                  ! 
Last_Print_Time      LONG                                  ! 
TimeOut              LONG                                  ! 
Print_Result         LONG                                  ! 
Preview_Option       LONG(2)                               ! 
Statement_Order      BYTE                                  ! 
                     END                                   ! 
QuickWindow          WINDOW('Printing Statements...'),AT(,,179,110),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,MDI,HLP('Print_Statements'),SYSTEM,TIMER(10)
                       GROUP,AT(19,74,96,10),USE(?Group1)
                         PROMPT('Page Print Time:'),AT(31,74),USE(?LO:Page_Print_Time:Prompt)
                         SPIN(@n-14),AT(90,74,37,10),USE(LO:Page_Print_Time),RIGHT(1),RANGE(2,100)
                       END
                       BUTTON('&Cancel'),AT(126,92,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(156,0,17,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       PROMPT('Statement ID:'),AT(31,14),USE(?STA:STID:Prompt)
                       STRING(@n_10),AT(83,14),USE(STA:STID),RIGHT(1)
                       PROMPT(''),AT(20,34,139,10),USE(?Prompt_Note),FONT(,,,FONT:bold,CHARSET:ANSI),CENTER
                       PROMPT(''),AT(20,54,139,10),USE(?Prompt_Time),FONT(,,,FONT:bold,CHARSET:ANSI),CENTER
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

View_Statements     VIEW(_Statements)
    PROJECT(STA:STID, STA:CID)
        JOIN(CLI:PKey_CID, STA:CID)
        PROJECT(CLI:ClientName)
    .   .


State_View          ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Statements')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LO:Page_Print_Time:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:_Statements.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Print_Statements',QuickWindow)             ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      State_View.Kill()
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Statements',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          QuickWindow{PROP:Timer}  = 0
      
          CASE MESSAGE('Would you like to Print Statements Continuous or Print Statements Laser?','Print Statements', ICON:Question, 'Continuous|Laser', 1)
          OF 1
             LO:Print_Type        = 0
          OF 2
             LO:Print_Type        = 1
          .
      
          LO:Statement_Order      = 1
          CASE MESSAGE('Would you like to print statements in ID order or in Client Name order?','Print Statements', ICON:Question, 'Statement ID|Client', 2)
          OF 1
             LO:Statement_Order   = 0
          OF 2
             LO:Statement_Order   = 1
          .
      
      
      
          State_View.Init(View_Statements, Relate:_Statements)
          State_View.AddSortOrder()
      
      !    State_View.AddSortOrder(STA:FKey_STRID)
      
          IF LO:Statement_Order = 0
             State_View.AppendOrder('STA:STID')
          ELSE
             State_View.AppendOrder('CLI:ClientName')
          .
      
          !State_View.AddRange(STA:STRID, p:STRID)
          State_View.SetFilter('STA:STRID = ' & p:STRID)
      
          State_View.Reset()
      
          QuickWindow{PROP:Timer}  = 10
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
          IF LO:Complete = TRUE
             LO:Time  = CLOCK()
      
             IF ABS(LO:Last_Time - LO:Time) > 50
                LO:Last_Time  = LO:Time
                IF ?Prompt_Note{PROP:Text} = 'Complete'
                   ?Prompt_Note{PROP:Text} = ''
                ELSE
                   ?Prompt_Note{PROP:Text} = 'Complete'
             .  .
             QuickWindow{PROP:Timer}  = 50
          .
          IF LO:Print_Now = FALSE AND LO:Complete = FALSE
             LO:TimeOut  = LO:Page_Print_Time - ((CLOCK() - LO:Last_Print_Time) / 100)
             ?Prompt_Time{PROP:Text}  = 'Printing in ' & LO:TimeOut & ' seconds'
      
             IF LO:Page_Print_Time <= ABS((CLOCK() - LO:Last_Print_Time) / 100)
                LO:Print_Now = TRUE
          .  .
          IF LO:Print_Now = TRUE AND LO:Complete = FALSE
             QuickWindow{PROP:Timer}  = 0
             LO:Time = CLOCK()
      
             IF State_View.Next() ~= LEVEL:Benign
                LO:Complete   = TRUE
                BREAK
             .
      
             DISPLAY(?STA:STID)
      
             CASE LO:Print_Type
             OF 0
                        ! (p:ID, p:PrintType, p:ID_Options, p:Preview_Option, p:HideWindow)
                        ! (ULONG, BYTE      , BYTE=0      , BYTE=1          , BYTE=0), LONG, PROC
                LO:Print_Result        = Print_Cont(STA:STID, 3,, LO:Preview_Option)
                IF LO:Print_Result < 0
                   LO:Complete  = TRUE
                ELSIF LO:Print_Result = 2
                   LO:Preview_Option   = 0       ! No preview
                .
             OF 1
                Print_Statement(, STA:STID)
             .
      
      
             IF LO:Complete = TRUE
                HIDE(?Prompt_Time)
                HIDE(?Group1)
      
                ?Cancel{PROP:Text}       = '&Close'
                ?Prompt_Note{PROP:Text}  = 'Complete'
                QuickWindow{PROP:Timer}  = 50
             ELSE
                QuickWindow{PROP:Timer}  = 10
             .
      
             LO:Last_Print_Time      = CLOCK()
          .
      !               old
      !    IF LO:Print_Now = TRUE
      !       QuickWindow{PROP:Timer}  = 0
      !       LO:Time = CLOCK()
      !
      !       IF LO:Complete = TRUE
      !          IF ABS(LO:Last_Time - LO:Time) > 50
      !             LO:Last_Time  = LO:Time
      !             IF ?Prompt_Note{PROP:Text} = 'Complete'
      !                ?Prompt_Note{PROP:Text} = ''
      !             ELSE
      !                ?Prompt_Note{PROP:Text} = 'Complete'
      !          .  .
      !          QuickWindow{PROP:Timer}  = 50
      !       ELSE
      !          LOOP LO:Loops TIMES
      !             IF LO:Loops % 10 = 0                      ! Every 10 loops check the time
      !                IF CLOCK() - LO:Time > 80
      !                   LO:Loops  -= LO:Loops / 10
      !                   BREAK
      !             .  .
      !
      !             IF State_View.Next() ~= LEVEL:Benign
      !                State_View.Kill()
      !                LO:Complete   = TRUE
      !                BREAK
      !             .
      !
      !             DISPLAY(?STA:STID)
      !
      !             EXECUTE LO:Print_Type + 1
      !                Print_Cont(STA:STID, 3)
      !                Print_Statement(, STA:STID)
      !          .  .
      !
      !
      !
      !          IF CLOCK() - LO:Time > 80
      !             LO:Loops    -= LO:Loops / 5
      !          .
      !
      !          IF LO:Loops <= 0
      !             LO:Loops     = 1
      !          .
      !
      !          IF LO:Complete = TRUE
      !             ?Cancel{PROP:Text}       = '&Close'
      !             ?Prompt_Note{PROP:Text}  = 'Complete'
      !             QuickWindow{PROP:Timer}  = 50
      !          ELSE
      !             QuickWindow{PROP:Timer}  = 10
      !       .  .
      !
      !
      !       LO:Last_Print_Time      = CLOCK()
      !    .
          LO:Print_Now = FALSE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Print_Remittances PROCEDURE (p:RERID)

Locals               GROUP,PRE(LO)                         ! 
Complete             BYTE                                  ! 
Time                 LONG                                  ! 
Loops                LONG                                  ! 
                     END                                   ! 
QuickWindow          WINDOW('Printing Remittances...'),AT(,,179,110),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  GRAY,IMM,HLP('Print_Statements'),SYSTEM,TIMER(10)
                       BUTTON('&Cancel'),AT(124,92,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(124,0,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       PROMPT('Remittance ID:'),AT(31,30),USE(?STA:STID:Prompt)
                       STRING(@n_10),AT(83,30),USE(REMI:REMID),RIGHT(1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

View_Statements     VIEW(_Remittance)
    PROJECT(REMI:REMID)
    .


State_View          ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Remittances')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Cancel
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:_Remittance.SetOpenRelated()
  Relate:_Remittance.Open                                  ! File _Remittance used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Print_Remittances',QuickWindow)            ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_Remittance.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Remittances',QuickWindow)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          State_View.Init(View_Statements, Relate:_Remittance)
          State_View.AddSortOrder(REMI:FKey_RERID)
          State_View.AppendOrder(REMI:REMID)
          State_View.AddRange(REMI:RERID, p:RERID)
          !State_View.SetFilter()
      
          State_View.Reset()
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
          QuickWindow{PROP:Timer}  = 0
          LO:Time = CLOCK()
      
      
          LOOP LO:Loops TIMES
             IF LO:Loops % 10 = 0
                IF CLOCK() - LO:Time > 80
                   LO:Loops  -= LO:Loops / 10
                   BREAK
             .  .
      
             IF State_View.Next() ~= LEVEL:Benign
                State_View.Kill()
                LO:Complete   = TRUE
                BREAK
             .
      
             DISPLAY(?REMI:REMID)
      
             Print_RemittanceAdvice(, REMI:REMID)
          .
      
      
      
          IF CLOCK() - LO:Time > 80
             LO:Loops        -= LO:Loops / 5
          .
      
          IF LO:Complete ~= TRUE
             QuickWindow{PROP:Timer}  = 10
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Print_Manifests_Tagged PROCEDURE  (shpTagClass p_Client_Tags, p:Type) ! Declare Procedure
L:Idx                ULONG                                 ! 

  CODE
    L:Idx   = 0
    LOOP
       L:Idx    += 1
       GET(p_Client_Tags.TagQueue, L:Idx)
       IF ERRORCODE()
          BREAK
       .

       ! (p:MID, p:Output, p:SkipPreview)

       EXECUTE p:Type
          Print_Manifest(p_Client_Tags.TagQueue.Ptr,, TRUE)
          Print_Manifest_Loading(p_Client_Tags.TagQueue.Ptr)
    .  .

    RETURN
!!! <summary>
!!! Generated from procedure template - Process
!!! </summary>
Process_TransporterInvoices PROCEDURE 

Progress:Thermometer BYTE                                  ! 
Process:View         VIEW(_InvoiceTransporter)
                       PROJECT(INT:TIN)
                     END
ProgressWindow       WINDOW('Process Transporter Invoices'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_TransporterInvoices')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:_InvoiceTransporter.SetOpenRelated()
  Relate:_InvoiceTransporter.Open                          ! File _InvoiceTransporter used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Process_TransporterInvoices',ProgressWindow) ! Restore window settings from non-volatile store
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:_InvoiceTransporter, ?Progress:PctText, Progress:Thermometer, ProgressMgr, INT:TIN)
  ThisProcess.AddSortOrder(INT:PKey_TIN)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(_InvoiceTransporter,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_InvoiceTransporter.Close
  END
  IF SELF.Opened
    INIMgr.Update('Process_TransporterInvoices',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      Upd_InvoiceTransporter_Paid_Status(INT:TIN)
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Process
!!! Delete Bad DIs, Check DIs Invoiced
!!! </summary>
Process_DIs PROCEDURE (p_Option)

Progress:Thermometer BYTE                                  ! 
LOC:Count            LONG                                  ! 
LOC:Dates            GROUP,PRE(L_D)                        ! 
FromDate             DATE                                  ! 
ToDate               DATE                                  ! 
                     END                                   ! 
Process:View         VIEW(Deliveries)
                       PROJECT(DEL:DID)
                     END
ProgressWindow       WINDOW('Process Deliveries'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepClass                             ! Progress Manager
Q_Type      QUEUE,TYPE
DID         LIKE(DEL:DID)
DINo        LIKE(DEL:DINo)
MID         LIKE(MAN:MID)
State       STRING(30)
        .

Man_Check   CLASS

No_InvoiceQ  &Q_Type


Construct       PROCEDURE()
Destruct        PROCEDURE()

Check_Manifest  PROCEDURE()
Show_User       PROCEDURE()
    .


Man_    Man_Check

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_DIs')
      p_Option    = UPPER(p_Option)
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
      CASE p_Option
      OF 'D'                          ! Delete
         BIND('DEL:CID', DEL:CID)
         BIND('DEL:BID', DEL:BID)
         BIND('DEL:JID', DEL:JID)
         BIND('DEL:Charge', DEL:Charge)
      OF 'I'                          ! Invoiced
         BIND('DEL:DIDate',DEL:DIDate)
      ELSE
      .
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Access:DeliveriesAdditionalCharges.UseFile               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryLegs.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryProgress.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TripSheetDeliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItems_Components.UseFile                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoadDeliveries.UseFile                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceComposition.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceTransporter.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Process_DIs',ProgressWindow)               ! Restore window settings from non-volatile store
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ThisProcess.Init(Process:View, Relate:Deliveries, ?Progress:PctText, Progress:Thermometer)
  ThisProcess.AddSortOrder()
  ThisProcess.AppendOrder('-DEL:DID')
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
      CASE p_Option
      OF 'D'
         ! Delete Bad DIs - No, Client, branch, Journey or Charge
         ThisProcess.SetFilter('DEL:CID = 0 AND DEL:JID = 0 AND DEL:Charge = 0.0 AND (DEL:DIDate = 0 OR SQL(DIDateAndTime IS NULL))')
      OF 'I'                          ! Invoiced
         ! (p:From, p:To, p:Option, p:Heading)
         LOC:Dates    = Ask_Date_Range()
         IF L_D:FromDate > 0
            ThisProcess.SetFilter('DEL:DIDate >= ' & L_D:FromDate, 'ikb1')
         .
         IF L_D:ToDate > 0
            ThisProcess.SetFilter('DEL:DIDate <= ' & L_D:ToDate, 'ikb2')
         .
      ELSE
      .
  SEND(Deliveries,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      CASE p_Option
      OF 'D'                          ! Delete
         UNBIND('DEL:CID')
         UNBIND('DEL:BID')
         UNBIND('DEL:JID')
         UNBIND('DEL:Charge')
      OF 'I'                          ! Invoiced
         UNBIND('DEL:DIDate')
      ELSE
      .
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
  END
  IF SELF.Opened
    INIMgr.Update('Process_DIs',ProgressWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
          CASE p_Option
          OF 'D'
             MESSAGE('Removed ' & LOC:Count & ' bad DI<39>s.', 'Process DI Delete', ICON:Asterisk)
          OF 'I'                          ! Invoiced
             Man_.Show_User()
          ELSE
          .
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Man_Check.Construct       PROCEDURE()
    CODE
    SELF.No_InvoiceQ     &= NEW(Q_Type)
    RETURN

Man_Check.Destruct        PROCEDURE()
    CODE
    DISPOSE(SELF.No_InvoiceQ)
    RETURN


Man_Check.Check_Manifest  PROCEDURE()
    CODE
    CLEAR(SELF.No_InvoiceQ)

    INV:DID  = DEL:DID
    IF Access:_Invoice.TryFetch(INV:FKey_DID) ~= LEVEL:Benign
       ! Check it is on a Manifest
                         
       Get_Delivery_ManIDs(DEL:DID, 0, MIDs_", 1)

!       db.debugout('DEL:DID: ' & DEL:DID & ',  MIDs: ' & CLIP(MIDs_") )

       IF CLIP(MIDs_") = ''
          ! Not Manifested - make list
          SELF.No_InvoiceQ.DID  = DEL:DID
          SELF.No_InvoiceQ.DINo = DEL:DINo
          !SELF.No_InvoiceQ.MID
          ADD(SELF.No_InvoiceQ)
       ELSE
          ! Check state of these Manifests, if they are > Loading (0) then should be Invoiced
          LOOP
             IF CLIP(MIDs_") = ''
                BREAK
             .
             MID_"   = Get_1st_Element_From_Delim_Str(MIDs_", ',', TRUE)
             MAN:MID = MID_"
             IF Access:Manifest.TryFetch(MAN:PKey_MID) = LEVEL:Benign
                CLEAR(SELF.No_InvoiceQ)
                SELF.No_InvoiceQ.DID     = DEL:DID
                SELF.No_InvoiceQ.DINo    = DEL:DINo
                SELF.No_InvoiceQ.MID     = MAN:MID

                IF MAN:State = 0
                   SELF.No_InvoiceQ.State   = 'Loading'
                ELSE
                   ! Should be invoiced, but isnt!
                   SELF.No_InvoiceQ.State   = '> Loading'
                .
                ADD(SELF.No_InvoiceQ)
    .  .  .  .
    RETURN

Man_Check.Show_User     PROCEDURE()
Idx     LONG
Heading STRING(100)
    CODE
    IF RECORDS(SELF.No_InvoiceQ) = 0
       MESSAGE('All DI<39>s have Invoices.','Deliveries Check', ICON:Asterisk)
    ELSE
       SORT(SELF.No_InvoiceQ, -SELF.No_InvoiceQ.State, SELF.No_InvoiceQ.MID, SELF.No_InvoiceQ.DID)

       Add_Log('DI No., Manifest ID (MID), Loading State', 'Headings', 'DIs without Invoices - ' & FORMAT(TODAY(), @d8) & '.txt', TRUE)
       LOOP
          Idx  += 1
          GET(SELF.No_InvoiceQ, Idx)
          IF ERRORCODE()
             BREAK
          .

          !SELF.No_InvoiceQ.State
          !SELF.No_InvoiceQ.MID
          !SELF.No_InvoiceQ.DID

          IF SELF.No_InvoiceQ.State = '> Loading'
             Heading   = 'DIs on Manifests which SHOULD have Invoices'
          ELSIF SELF.No_InvoiceQ.State = 'Loading'
             Heading   = 'DIs on Manifests still in Loading'
          ELSE
             Heading   = 'DIs not on a Manifest'
          .

          ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt)
          Add_Log(SELF.No_InvoiceQ.DINo & ', ' & SELF.No_InvoiceQ.MID & ', ' & SELF.No_InvoiceQ.State, Heading, 'DIs without Invoices - ' & FORMAT(TODAY(), @d8) & '.txt', FALSE)
       .

       LOOP
          CASE MESSAGE('Some DI<39>s do not have Invoices.', 'Deliveries Check', ICON:Question, 'Show|Exit', 1)
          OF 1
             ISExecute(ProgressWindow{PROP:Handle}, PATH() & '\DIs without Invoices - ' & FORMAT(TODAY(), @d8) & '.txt')
          ELSE
             BREAK
    .  .  .
    RETURN

ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      CASE p_Option
      OF 'D'
         !db.debugout('LOC:Count: ' & LOC:Count &',  DEL:DID: ' & DEL:DID & ',  DEL:DINo: ' & DEL:DINo)
  
         IF Access:Deliveries.Fetch(DEL:PKey_DID) ~= LEVEL:Benign
            !
         ELSE
            IF DEL:DID = 0            ! Delete regardless of referential restraints
               LOC:Count             += 1
  
               ! Check for children items
               CLEAR(DELI:Record,-1)
               DELI:DID   = 0                 ! see if...
               SET(DELI:FKey_DID_ItemNo, DELI:FKey_DID_ItemNo)
               LOOP
                  IF Access:DeliveryItems.TryNext() ~= LEVEL:Benign
                     BREAK
                  .
                  IF DELI:DID ~= 0
                     BREAK
                  .
  
                  IF Relate:DeliveryItems.Delete(0) ~= LEVEL:Benign
                     ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt)
                     Add_Log('DI Delete Count: ' & LOC:Count & ', DELI:DIID: ' & DELI:DIID & ', ItemNo: ' & DELI:ItemNo & ',  DEL:DID: ' & DEL:DID & ',  DEL:DINo: ' & DEL:DINo, 'Bad DI Items', 'Bad_DI_Items_NOT_Deleted.log', FALSE)
                  ELSE
                     Add_Log('DI Delete Count: ' & LOC:Count & ', DELI:DIID: ' & DELI:DIID & ', ItemNo: ' & DELI:ItemNo & ',  DEL:DID: ' & DEL:DID & ',  DEL:DINo: ' & DEL:DINo, 'Bad DI Items', 'Bad_DI_Items_Deleted.log', FALSE)
               .  .
  
               IF Access:Deliveries.DeleteRecord(0) = LEVEL:Benign
                  Add_Log('Count: ' & LOC:Count &',  DEL:DID: ' & DEL:DID & ',  DEL:DINo: ' & DEL:DINo, 'Bad DIs', 'Bad_DIs_Deleted.log', FALSE)
               ELSE
                  LOC:Count             -= 1
               .
            ELSE
               IF Relate:Deliveries.Delete(0) ~= LEVEL:Benign           ! This is a delete process, so delete from RelationManager
                  Add_Log('File Access Error: ' & Access:Deliveries.GetError() & ', Count: ' & LOC:Count &',  DEL:DID: ' & DEL:DID & ',  DEL:DINo: ' & DEL:DINo, 'Bad DIs', 'Bad_DIs_NOT_Deleted.log', FALSE)
  
                  !ThisWindow.Response    = RequestCompleted
                  !ReturnValue            = Level:Fatal
               ELSE
                  LOC:Count             += 1
  
                  Add_Log('Count: ' & LOC:Count &',  DEL:DID: ' & DEL:DID & ',  DEL:DINo: ' & DEL:DINo, 'Bad DIs', 'Bad_DIs_Deleted.log', FALSE)
         .  .  .
      OF 'I'                          ! Invoiced
         Man_.Check_Manifest()
         LOC:Count   += 1
      ELSE
         LOC:Count   += 1
      .
  RETURN ReturnValue

