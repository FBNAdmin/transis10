

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS009.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Process
!!! Also Invoice Alias
!!! </summary>
Process_ClientPaymentsAllocation_StatusUpToDate PROCEDURE (p:StatusUpToDate)

Progress:Thermometer BYTE                                  ! 
Process:View         VIEW(ClientsPaymentsAllocation)
                       PROJECT(CLIPA:StatusUpToDate)
                     END
ProgressWindow       WINDOW('Process Clients Payments'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_ClientPaymentsAllocation_StatusUpToDate')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:ClientsPaymentsAllocation.Open                    ! File ClientsPaymentsAllocation used by this procedure, so make sure it's RelationManager is open
  Relate:InvoiceAlias.Open                                 ! File InvoiceAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Process_ClientPaymentsAllocation_StatusUpToDate',ProgressWindow) ! Restore window settings from non-volatile store
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:ClientsPaymentsAllocation, ?Progress:PctText, Progress:Thermometer, ProgressMgr, CLIPA:CPAID)
  ThisProcess.AddSortOrder(CLIPA:PKey_CPAID)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='Processing Client Payments'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
      IF DEFORMAT(p:StatusUpToDate) = 0
         ThisProcess.SetFilter('CLIPA:StatusUpToDate = 0 OR SQL(StatusUpToDate IS NULL)')
      ELSE
         ThisProcess.SetFilter('CLIPA:StatusUpToDate = 1')
      .
  
  SELF.DeferWindow = 2
  SEND(ClientsPaymentsAllocation,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ClientsPaymentsAllocation.Close
    Relate:InvoiceAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Process_ClientPaymentsAllocation_StatusUpToDate',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
  ! SELF.OpenFailed=1
    RETURN                                                               
  PARENT.TakeNoRecords


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      IF p:StatusUpToDate = '1'
         IF Access:ClientsPaymentsAllocation.TryFetch(CLIPA:PKey_CPAID) = LEVEL:Benign
            CLIPA:StatusUpToDate     = FALSE
            IF Access:ClientsPaymentsAllocation.TryUpdate() ~= LEVEL:Benign
               ! Problem
         .  .
      ELSE
         A_INV:IID                   = CLIPA:IID
         IF Access:InvoiceAlias.TryFetch(A_INV:PKey_IID) = LEVEL:Benign
            A_INV:StatusUpToDate     = FALSE                             ! Set StatusUpToDate to FALSE
            IF Access:InvoiceAlias.TryUpdate() ~= LEVEL:Benign
               ! Problem
         .  .
  
  
         IF Access:ClientsPaymentsAllocation.TryFetch(CLIPA:PKey_CPAID) = LEVEL:Benign
            CLIPA:StatusUpToDate     = TRUE
            IF Access:ClientsPaymentsAllocation.TryUpdate() ~= LEVEL:Benign
               ! Problem
      .  .  .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Process
!!! </summary>
Process_ClientPayments_StatusUpToDate PROCEDURE 

Progress:Thermometer BYTE                                  ! 
LOC:Options          GROUP,PRE(LO)                         ! 
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
Process:View         VIEW(ClientsPayments)
                     END
ProgressWindow       WINDOW('Process Clients Payments'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_ClientPayments_StatusUpToDate')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:ClientsPayments.Open                              ! File ClientsPayments used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Process_ClientPayments_StatusUpToDate',ProgressWindow) ! Restore window settings from non-volatile store
      LO:From_Date    = GETINI('Process_ClientPayments_StatusUpToDate', 'From_Date', '', GLO:Local_INI)
      LO:To_Date      = GETINI('Process_ClientPayments_StatusUpToDate', 'To_Date', '', GLO:Local_INI)
  
      LOC:Options     = Ask_Date_Range(LO:From_Date, LO:To_Date)
  
      IF LO:To_Date > 0
         PUTINI('Process_ClientPayments_StatusUpToDate', 'From_Date', LO:From_Date, GLO:Local_INI)
         PUTINI('Process_ClientPayments_StatusUpToDate', 'To_Date', LO:To_Date, GLO:Local_INI)
      ELSE
         ReturnValue  = LEVEL:Fatal
      .
  
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:ClientsPayments, ?Progress:PctText, Progress:Thermometer, ProgressMgr, CLIP:CPID)
  ThisProcess.AddSortOrder(CLIP:PKey_CPID)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(ClientsPayments,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ClientsPayments.Close
  END
  IF SELF.Opened
    INIMgr.Update('Process_ClientPayments_StatusUpToDate',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
  ! SELF.OpenFailed=1
    RETURN                                                               
  PARENT.TakeNoRecords


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      Upd_ClientPayments_Status(CLIP:CPID)
      
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Process
!!! really only updates Invoices related to Credit Notes
!!! </summary>
Process_Invoice_StatusUpToDate PROCEDURE (p:StatusUpToDate)

Progress:Thermometer BYTE                                  ! 
Process:View         VIEW(_Invoice)
                       PROJECT(INV:Status)
                       PROJECT(INV:StatusUpToDate)
                     END
ProgressWindow       WINDOW('Process Invoices'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_Invoice_StatusUpToDate')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:InvoiceAlias.Open                                 ! File InvoiceAlias used by this procedure, so make sure it's RelationManager is open
  Relate:_Invoice.SetOpenRelated()
  Relate:_Invoice.Open                                     ! File _Invoice used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:_Invoice, ?Progress:PctText, Progress:Thermometer, ProgressMgr, INV:IID)
  ThisProcess.AddSortOrder(INV:PKey_IID)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='Processing Invoices'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
      IF DEFORMAT(p:StatusUpToDate) = 0            ! Limit to Status Not Up to Date
         ThisProcess.SetFilter('INV:StatusUpToDate = 0 OR SQL(StatusUpToDate IS NULL)', '1')
  
         ! At the moment the process record code only deals with Status 2 and 5 invoices so filter here on those then!
         ThisProcess.SetFilter('INV:Status = 2 OR INV:Status = 5', '2')           ! Credit notes
      ELSE
         ThisProcess.SetFilter('INV:StatusUpToDate = 1')
      .
  
  SELF.DeferWindow = 2
  SEND(_Invoice,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:InvoiceAlias.Close
    Relate:_Invoice.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
  ! SELF.OpenFailed=1
    RETURN                                                               
  PARENT.TakeNoRecords


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      ! p:StatusUpToDate
      !   0   - Not up to date, then check associated Invoices
      !   1   - Up To Date, then reset these to Not up to date
  
      ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
      !       0           1               2                3               4               5
  
      ! We want to update the Invoice when related (credit notes) have been changed / added
  
      IF p:StatusUpToDate = '1'
         IF Access:_Invoice.TryFetch(INV:PKey_IID) = LEVEL:Benign
            INV:StatusUpToDate    = FALSE
            IF Access:_Invoice.TryUpdate() ~= LEVEL:Benign
               ! Problem
         .  .
      ELSE
         CASE INV:Status
         OF 2 OROF 5                                  ! If this is a Crdit Note
  
            CLEAR(A_INV:Record, -1)
            A_INV:IID        = INV:CR_IID             ! The credited Invoice
            IF Access:InvoiceAlias.TryFetch(A_INV:PKey_IID) = LEVEL:Benign
               A_INV:StatusUpToDate  = FALSE          ! Set StatusUpToDate to FALSE
               IF Access:InvoiceAlias.TryUpdate() ~= LEVEL:Benign
                  ! Problem
            .  .
  
            IF Access:_Invoice.TryFetch(INV:PKey_IID) = LEVEL:Benign
               INV:StatusUpToDate    = TRUE
               IF Access:_Invoice.TryUpdate() ~= LEVEL:Benign
                  ! Problem
            .  .
         ELSE                                         ! This is an invoice... update related credit notes?
      .  .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Process
!!! really only updates Invoices related to Credit Notes
!!! </summary>
Process_InvoiceTransporter_StatusUpToDate PROCEDURE (p:StatusUpToDate)

Progress:Thermometer BYTE                                  ! 
Process:View         VIEW(_InvoiceTransporter)
                       PROJECT(INT:StatusUpToDate)
                     END
ProgressWindow       WINDOW('Process Transporter Invoices'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_InvoiceTransporter_StatusUpToDate')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:InvoiceTransporterAlias.Open                      ! File InvoiceTransporterAlias used by this procedure, so make sure it's RelationManager is open
  Relate:_InvoiceTransporter.SetOpenRelated()
  Relate:_InvoiceTransporter.Open                          ! File _InvoiceTransporter used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Process_InvoiceTransporter_StatusUpToDate',ProgressWindow) ! Restore window settings from non-volatile store
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:_InvoiceTransporter, ?Progress:PctText, Progress:Thermometer, ProgressMgr, INT:TIN)
  ThisProcess.AddSortOrder(INT:PKey_TIN)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
    IF p:StatusUpToDate = '0'            ! Limit to Status Not Up to Date
       ThisProcess.SetFilter('INT:StatusUpToDate = 0 OR SQL(StatusUpToDate IS NULL)')
    ELSE
       ThisProcess.SetFilter('INT:StatusUpToDate = 1')
    .
  
  SELF.DeferWindow = 2
  SELF.WaitCursor = 1
  SEND(_InvoiceTransporter,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:InvoiceTransporterAlias.Close
    Relate:_InvoiceTransporter.Close
  END
  IF SELF.Opened
    INIMgr.Update('Process_InvoiceTransporter_StatusUpToDate',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
  ! SELF.OpenFailed=1
    RETURN                                                               
  PARENT.TakeNoRecords


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      ! p:StatusUpToDate
      !   0   - Not up to date, then check associated Invoices
      !   1   - Up To Date, then reset these to Not up to date
  
      ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
      !       0           1               2                3               4               5
  
  
      ! We want to update the Invoice when related (credit notes) have been changed / added
      IF p:StatusUpToDate = '1'
         IF Access:_InvoiceTransporter.TryFetch(INT:PKey_TIN) ~= LEVEL:Benign
            INT:StatusUpToDate    = FALSE
            IF Access:_InvoiceTransporter.TryUpdate() ~= LEVEL:Benign
               ! Problem
         .  .
      ELSE
         CASE INT:Status
         OF 2                                        ! If this is a Crdit Note
  
            CLEAR(A_INT:Record, -1)
            A_INT:TIN        = INT:CR_TIN            ! The credited Invoice
            IF Access:InvoiceTransporterAlias.TryFetch(A_INT:PKey_TIN) ~= LEVEL:Benign
               A_INT:StatusUpToDate   = FALSE                         ! Set StatusUpToDate to FALSE
               IF Access:InvoiceTransporterAlias.TryUpdate() ~= LEVEL:Benign
                  ! Problem
            .  .
  
            IF Access:_InvoiceTransporter.TryFetch(INT:PKey_TIN) ~= LEVEL:Benign
               INT:StatusUpToDate     = TRUE
               IF Access:_InvoiceTransporter.TryUpdate() ~= LEVEL:Benign
                  ! Problem
            .  .
         ELSE                                    ! This is an invoice... update related credit notes?
      .  .
  
  
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Process
!!! </summary>
Process_TransporterPayments_StatusUpToDate PROCEDURE (p:StatusUpToDate)

Progress:Thermometer BYTE                                  ! 
Process:View         VIEW(TransporterPaymentsAllocations)
                       PROJECT(TRAPA:StatusUpToDate)
                     END
ProgressWindow       WINDOW('Process Clients Payments'),AT(,,245,59),FONT('Tahoma',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(14,15,216,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(97,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Process'), |
  TIP('Cancel Process')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepClass                             ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_TransporterPayments_StatusUpToDate')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:InvoiceTransporterAlias.Open                      ! File InvoiceTransporterAlias used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterPaymentsAllocations.SetOpenRelated()
  Relate:TransporterPaymentsAllocations.Open               ! File TransporterPaymentsAllocations used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ThisProcess.Init(Process:View, Relate:TransporterPaymentsAllocations, ?Progress:PctText, Progress:Thermometer)
  ThisProcess.AddSortOrder()
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
      IF p:StatusUpToDate = '0'
         ThisProcess.SetFilter('TRAPA:StatusUpToDate = 0 OR SQL(StatusUpToDate IS NULL)')
      ELSE
         ThisProcess.SetFilter('TRAPA:StatusUpToDate = 1')
      .
  
  
  SELF.DeferWindow = 2
  SEND(TransporterPaymentsAllocations,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:InvoiceTransporterAlias.Close
    Relate:TransporterPaymentsAllocations.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
      ! We want to update the Invoice when related (credit notes) have been changed / added
      ! p:StatusUpToDate
      !   0   - Not up to date, then check associated Invoices
      !   1   - Up To Date, then reset these to Not up to date
  
      IF p:StatusUpToDate = '1'
         IF Access:TransporterPaymentsAllocations.TryFetch(TRAPA:PKey_TRPAID) ~= LEVEL:Benign
            TRAPA:StatusUpToDate     = FALSE
            IF Access:TransporterPaymentsAllocations.TryUpdate() ~= LEVEL:Benign
               ! Problem
         .  .
      ELSE
         A_INT:TIN                   = TRAPA:TIN
         IF Access:InvoiceTransporterAlias.TryFetch(A_INT:PKey_TIN) ~= LEVEL:Benign
            A_INT:StatusUpToDate     = FALSE                             ! Set StatusUpToDate to FALSE
            IF Access:InvoiceTransporterAlias.TryUpdate() ~= LEVEL:Benign
               ! Problem
         .  .
  
  
         IF Access:TransporterPaymentsAllocations.TryFetch(TRAPA:PKey_TRPAID) ~= LEVEL:Benign
            TRAPA:StatusUpToDate     = TRUE
            IF Access:TransporterPaymentsAllocations.TryUpdate() ~= LEVEL:Benign
               ! Problem
      .  .  .
  RETURN ReturnValue

