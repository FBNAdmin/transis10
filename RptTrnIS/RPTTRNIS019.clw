

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('abrppsel.inc'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS019.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! ***  filter class  ***
!!! </summary>
Print_Manifests PROCEDURE 

Progress:Thermometer BYTE                                  !
LOC:Data             GROUP,PRE()                           !
HorseReg             STRING(20)                            !
Tonnage              DECIMAL(12,2)                         !
Cost                 DECIMAL(13,2)                         !
Extra_Legs_Cost      DECIMAL(12,2)                         !
Legs_Cost_VAT        DECIMAL(13,2)                         !
GP                   DECIMAL(13,2)                         !
GP_Per               DECIMAL(5,1)                          !
Delivery_Charges_Ex  DECIMAL(13,2)                         !
Summary_Text         STRING(20)                            !
                     END                                   !
LOC:Data_Totals      GROUP,PRE()                           !
Tot_GP               DECIMAL(15,2)                         !
Tot_Delivery_Charges_Ex DECIMAL(15,2)                      !
Total_GP_Per         DECIMAL(7,2)                          !
VCID                 ULONG                                 !Vehicle Composition ID
Tot_GP_v             DECIMAL(15,2)                         !
Tot_Delivery_Charges_Ex_v DECIMAL(15,2)                    !
Total_GP_Per_v       DECIMAL(7,2)                          !
                     END                                   !
LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
Included_TIDs        CSTRING('0<0>{503}')                  !
Branch               STRING(35)                            !Branch Name
BID                  ULONG                                 !Branch ID
BID_FirstTime        BYTE(1)                               !
Vehicles_for_Transporters BYTE                             !
Included_VCIDs       CSTRING('0<0>{499}')                  !
CompositionName      STRING(35)                            !
VCID                 ULONG                                 !Vehicle Composition ID
Filter_By_Transporters BYTE                                !
Filter_By_Vehicles   BYTE                                  !
Summary              BYTE                                  !
                     END                                   !
LOC:Q                QUEUE,PRE(L_Q)                        !
Str                  STRING(20)                            !
                     END                                   !
Process:View         VIEW(Manifest)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:DepartDateAndTime)
                       PROJECT(MAN:MID)
                       PROJECT(MAN:Rate)
                       PROJECT(MAN:TID)
                       PROJECT(MAN:VCID)
                       JOIN(TRA:PKey_TID,MAN:TID)
                         PROJECT(TRA:TransporterName)
                       END
                       JOIN(VCO:PKey_VCID,MAN:VCID)
                         PROJECT(VCO:CompositionName)
                       END
                     END
BRW7::View:Browse    VIEW(Transporter)
                       PROJECT(TRA:TransporterName)
                       PROJECT(TRA:TID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB9::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
FDB12::View:FileDrop VIEW(VehicleCompositionAlias)
                       PROJECT(A_VCO:CompositionName)
                     END
Queue:FileDrop_Branch QUEUE                           !Queue declaration for browse/combo box using ?LO:Branch
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?LO:CompositionName
A_VCO:CompositionName  LIKE(A_VCO:CompositionName)    !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
ProgressWindow       WINDOW('Report Manifest'),AT(,,472,326),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,MDI,TIMER(1)
                       SHEET,AT(4,4,465,302),USE(?Sheet1)
                         TAB('Options'),USE(?Tab_Options)
                           PROMPT('From Date:'),AT(14,22),USE(?LO:From_Date:Prompt)
                           SPIN(@d5b),AT(66,22,60,10),USE(LO:From_Date),RIGHT(1)
                           BUTTON('...'),AT(130,22,12,10),USE(?Calendar)
                           PROMPT(''),AT(150,22,143,10),USE(?Prompt_DayFrom)
                           PROMPT('To Date:'),AT(14,38),USE(?LO:To_Date:Prompt)
                           SPIN(@d5b),AT(66,38,60,10),USE(LO:To_Date),RIGHT(1)
                           BUTTON('...'),AT(130,38,12,10),USE(?Calendar:2)
                           PROMPT(''),AT(150,38,143,10),USE(?Prompt_DayTo),HIDE
                           PROMPT('Branch:'),AT(14,58),USE(?Prompt5)
                           LIST,AT(66,58,111,10),USE(LO:Branch),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop_Branch)
                           CHECK(' &Summary'),AT(408,22),USE(LO:Summary)
                           CHECK(' Filter by &Transporters'),AT(66,72),USE(LO:Filter_By_Transporters)
                           GROUP,AT(36,88,18,116),USE(?Group_FT)
                             LIST,AT(66,96,177,108),USE(?List_Inc),VSCROLL,FORMAT('80L(2)|M~Transporters~@s50@'),FROM(LOC:Q)
                             LIST,AT(278,96,170,108),USE(?List),VSCROLL,FORMAT('140L(2)|M~Transporters~@s35@'),FROM(Queue:Browse), |
  IMM,MSG('Browsing Records')
                             GROUP('Transporters to Include'),AT(62,84,185,124),USE(?Group1),BOXED
                             END
                             GROUP('All Transporters'),AT(270,84,185,124),USE(?Group1:2),BOXED
                             END
                             BUTTON('<<-'),AT(254,126,12,24),USE(?Button_Add)
                             BUTTON('->'),AT(254,170,12,24),USE(?Button_Remove)
                             CHECK(' Filter by &Vehicles'),AT(66,212),USE(LO:Filter_By_Vehicles)
                             GROUP,AT(32,220,23,80),USE(?Group_FV)
                               CHECK(' &Vehicles for Selected Transporters'),AT(278,238),USE(LO:Vehicles_for_Transporters)
                               GROUP('All Vehicle Compositions'),AT(270,224,185,79),USE(?Group3),BOXED
                                 LIST,AT(278,252,170,10),USE(LO:CompositionName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Comp' & |
  'osition Name~@s255@'),FROM(Queue:FileDrop)
                               END
                               BUTTON('<<-'),AT(254,238,12,24),USE(?Button_Add_VC)
                               GROUP('Vehicle Compositions to Include'),AT(62,224,185,79),USE(?Group3:2),BOXED
                               END
                               BUTTON('->'),AT(254,274,12,24),USE(?Button_Remove_VC)
                               LIST,AT(66,236,177,60),USE(?List_Veh),HVSCROLL,FORMAT('255L(2)|M~Composition Name~@s255@'), |
  FROM(LOC:Q)
                             END
                           END
                         END
                         TAB('Progress'),USE(?Tab1)
                           PROGRESS,AT(41,116,389,12),USE(Progress:Thermometer),RANGE(0,100)
                           STRING(''),AT(85,104,302,10),USE(?Progress:UserString),CENTER
                           STRING(''),AT(85,132,302,10),USE(?Progress:PctText),CENTER
                         END
                       END
                       BUTTON('Pause'),AT(416,308,,15),USE(?Pause),LEFT,ICON('waok.ico'),FLAT
                       BUTTON('Cancel'),AT(362,308,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Manifest Report'),AT(250,719,7750,10469),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,469),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Manifests'),AT(2677,10,2396,313),USE(?ReportTitle),FONT('Arial',18,,FONT:regular), |
  CENTER
                         STRING('From:'),AT(5708,146),USE(?String27:2),TRN
                         STRING(@d5b),AT(6021,146),USE(LO:From_Date),RIGHT(1)
                         STRING(@d5b),AT(7063,146),USE(LO:To_Date),RIGHT(1)
                         STRING(@s20),AT(115,146),USE(Summary_Text),TRN
                         LINE,AT(0,470,7750,0),USE(?Line22),COLOR(COLOR:Black)
                         STRING('To:'),AT(6833,146),USE(?String27),TRN
                       END
break_transporter      BREAK(MAN:TID)
                         HEADER,AT(0,0,,583)
                           STRING('Transporter :'),AT(490,52),USE(?String35),FONT(,12,,FONT:bold,CHARSET:ANSI),TRN
                           STRING(@s35),AT(1594,52),USE(TRA:TransporterName),FONT(,12,,FONT:bold,CHARSET:ANSI),TRN
                           BOX,AT(0,354,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                           LINE,AT(781,354,0,250),USE(?HeaderLine:1),COLOR(COLOR:Black)
                           LINE,AT(2219,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                           LINE,AT(2792,354,0,250),USE(?HeaderLine:3),COLOR(COLOR:Black)
                           LINE,AT(3833,354,0,250),USE(?HeaderLine:4),COLOR(COLOR:Black)
                           LINE,AT(4875,354,0,250),USE(?HeaderLine:8),COLOR(COLOR:Black)
                           LINE,AT(7240,354,0,250),USE(?HeaderLine:9),COLOR(COLOR:Black)
                           LINE,AT(5365,354,0,250),USE(?HeaderLine:5),COLOR(COLOR:Black)
                           LINE,AT(6250,354,0,250),USE(?HeaderLine:6),COLOR(COLOR:Black)
                           STRING('MID'),AT(52,385,688,167),USE(?HeaderTitle:1),CENTER,TRN
                           STRING('Cost'),AT(3969,385,833,167),USE(?HeaderTitle:4),CENTER,TRN
                           STRING('Rate'),AT(4927,385,365,167),USE(?HeaderTitle:5),CENTER,TRN
                           STRING('Horse Reg.'),AT(833,385,1302,167),USE(?HeaderTitle:2),CENTER,TRN
                           STRING('Tonnage'),AT(2292,385,,167),USE(?HeaderTitle:3),TRN
                           STRING('Extra Legs'),AT(5469,385,677,167),USE(?HeaderTitle:6),CENTER,TRN
                           STRING('Turnover'),AT(2885,385,833,167),USE(?HeaderTitle:9),CENTER,TRN
                           STRING('GP'),AT(6375,385,677,167),USE(?HeaderTitle:7),CENTER,TRN
                           STRING('GP %'),AT(7313,385,354,167),USE(?HeaderTitle:8),TRN
                         END
break_vehicle            BREAK(MAN:VCID),USE(?break_vehicle)
Detail                     DETAIL,AT(,,7750,260),USE(?Detail)
                             GROUP,AT(1667,52,5000,156),USE(?Group_All)
                               LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                               LINE,AT(781,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                               LINE,AT(2219,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                               LINE,AT(2792,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                               LINE,AT(3833,0,0,250),USE(?DetailLine:4),COLOR(COLOR:Black)
                               LINE,AT(4875,0,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                               LINE,AT(5365,0,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                               LINE,AT(6250,0,0,250),USE(?DetailLine:6),COLOR(COLOR:Black)
                               LINE,AT(7240,0,0,250),USE(?HeaderLine:10),COLOR(COLOR:Black)
                               LINE,AT(7750,0,0,250),USE(?DetailLine:7),COLOR(COLOR:Black)
                               STRING(@n_10),AT(52,52,,167),USE(MAN:MID),RIGHT
                               STRING(@s20),AT(833,52),USE(HorseReg)
                               STRING(@n6.1),AT(2292,52,448,167),USE(Tonnage),RIGHT(1)
                               STRING(@n-14.2),AT(2885,52),USE(Delivery_Charges_Ex),RIGHT(1)
                               STRING(@n-14.2),AT(3979,52,,167),USE(MAN:Cost),RIGHT(1)
                               STRING(@n5.2),AT(4927,52,,167),USE(MAN:Rate),RIGHT
                               STRING(@n-11.2b),AT(5469,52),USE(Extra_Legs_Cost),RIGHT(1)
                               STRING(@n-12.2),AT(6375,52),USE(GP),RIGHT(1)
                               STRING(@n5.1),AT(7313,52),USE(GP_Per),RIGHT(1)
                               LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                             END
                           END
                           FOOTER,AT(0,0,,0)
                             GROUP,AT(1510,52,5000,156),USE(?Group_footer),HIDE
                               STRING(@s20),AT(167,52),USE(HorseReg,,?HorseReg:2),TRN
                               STRING(@n-17.2),AT(1750,52,990,156),USE(Tonnage,,?Tonnage:3),RIGHT(1),SUM,RESET(break_vehicle), |
  TRN
                               STRING(@n-18.2),AT(2667,52),USE(Delivery_Charges_Ex,,?Delivery_Charges_Ex:3),RIGHT(1),SUM, |
  RESET(break_vehicle),TRN
                               STRING(@n-14.2),AT(3979,52,,170),USE(MAN:Cost,,?MAN:Cost:3),RIGHT(1),SUM,RESET(break_vehicle), |
  TRN
                               STRING(@n-17.2),AT(5156,52),USE(Extra_Legs_Cost,,?Extra_Legs_Cost:3),RIGHT(1),SUM,RESET(break_vehicle), |
  TRN
                               STRING(@n-18.2),AT(6063,52),USE(Tot_GP_v),RIGHT(1),TRN
                               STRING(@n-7.2),AT(7208,52),USE(Total_GP_Per_v),RIGHT(1),TRN
                               LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                             END
                           END
                         END
                       END
grandtotals            DETAIL,AT(,,,354),USE(?grandtotals)
                         LINE,AT(167,52,7500,0),USE(?Line20),COLOR(COLOR:Black)
                         STRING(@n-17.2),AT(1750,94,990,156),USE(Tonnage,,?Tonnage:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-18.2),AT(2667,94),USE(Delivery_Charges_Ex,,?Delivery_Charges_Ex:2),RIGHT(1),SUM, |
  TALLY(Detail),TRN
                         STRING(@n-14.2),AT(3979,94,,170),USE(MAN:Cost,,?MAN:Cost:2),RIGHT(1),SUM,TALLY(Detail),TRN
                         STRING(@n-17.2),AT(5156,94),USE(Extra_Legs_Cost,,?Extra_Legs_Cost:2),RIGHT(1),SUM,TALLY(Detail), |
  TRN
                         STRING(@n-18.2),AT(6063,94),USE(Tot_GP),RIGHT(1),TRN
                         STRING(@n-7.2),AT(7208,94),USE(Total_GP_Per),RIGHT(1),TRN
                         LINE,AT(167,271,7500,0),USE(?Line20:2),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass
BRW7                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyFilter            PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                      ! Default Locator
FDB9                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop_Branch         !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END

FDB12                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ApplyFilter            PROCEDURE(),DERIVED
                     END

A_Filter_Q       QUEUE,TYPE
Filter_Str          CSTRING(101)
Filter_Value        CSTRING(255)
                .

A_Filter_     CLASS,TYPE

A_FQ            &A_Filter_Q
Name_           STRING(100)                  ! This is used in A_Filter_My

Init            PROCEDURE(STRING p_Name, LONG p_ListCtrl)

Add_            PROCEDURE(STRING p_Str, STRING p_Value, BYTE p_PermitDup=0),VIRTUAL
Get_            PROCEDURE(),STRING

Del_            PROCEDURE(LONG p_Pos),LONG,PROC
Get_Item        PROCEDURE(LONG p_Pos, BYTE p_Type),STRING


Save_           PROCEDURE(STRING p_Name),LONG,PROC
Load_           PROCEDURE(STRING p_Name),LONG,PROC

Construct       PROCEDURE()
Destruct        PROCEDURE()
            .

! This class is where any specific work for this procedure or instance of the object can be implemented
A_Filter_My  CLASS(A_Filter_),TYPE
Add_            PROCEDURE(STRING p_Str, STRING p_Value, BYTE p_PermitDup=0),VIRTUAL
            .
            


Filters_Q       QUEUE,TYPE
Name_               STRING(101)
FO                  &A_Filter_My
                .

Filters_     CLASS,TYPE

FQ              &Filters_Q

Get_FO          PROCEDURE(STRING p_Name),LONG

Init            PROCEDURE(STRING p_Name, LONG p_ListCtrl)

Add_            PROCEDURE(STRING p_Name, STRING p_Str, STRING p_Value, BYTE p_PermitDup=0),VIRTUAL
Get_            PROCEDURE(STRING p_Name),STRING

Del_            PROCEDURE(STRING p_Name, LONG p_Pos),LONG,PROC
Get_Item        PROCEDURE(STRING p_Name, LONG p_Pos, BYTE p_Type),STRING


Save_           PROCEDURE(STRING p_Name, STRING p_SaveName),LONG,PROC
Load_           PROCEDURE(STRING p_Name, STRING p_SaveName),LONG,PROC

Construct       PROCEDURE()
Destruct        PROCEDURE()
            .






Fil         Filters_

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Get_Values              ROUTINE
    Cost                         = MAN:Cost                                              ! VAT is excluded

!    L_MT:Delivery_Charges        = Get_Manifest_Info(MAN:MID, 4)    !,,, p:Output)             ! 4 is VAT incl


    Extra_Legs_Cost              = Get_Manifest_Info(MAN:MID, 6)                         ! VAT is included
    Legs_Cost_VAT                = Extra_Legs_Cost - Get_Manifest_Info(MAN:MID, 3)       !,,, p:Output)   ! excluded (3), so minus ex to get VAT
    
!    L_MT:Out_VAT                 = L_MT:Cost * (MAN:VATRate / 100)
!    L_MT:Total_Cost              = L_MT:Cost + L_MT:Out_VAT                              ! Add VAT

    Delivery_Charges_Ex          = Get_Manifest_Info(MAN:MID, 0)    !,,, p:Output)             ! 4 is VAT EXCL
    Tot_Delivery_Charges_Ex     += Delivery_Charges_Ex

    GP                           = Delivery_Charges_Ex - Cost - (Extra_Legs_Cost - Legs_Cost_VAT)
    Tot_GP                      += GP

    GP_Per                       = (GP / Delivery_Charges_Ex) * 100  ! %


    Tonnage                      = Get_Manifest_Info(MAN:MID, 2) / 1000   !,,, p:Output)


    IF VCID ~= MAN:VCID
       VCID                         = MAN:VCID
       Tot_GP_v                     = 0.0
       Tot_Delivery_Charges_Ex_v    = 0.0
       Total_GP_Per_v               = 0.0
    .

    Tot_GP_v                       += GP
    Tot_Delivery_Charges_Ex_v      += Delivery_Charges_Ex
    Total_GP_Per_v                 += (Tot_GP_v / Tot_Delivery_Charges_Ex_v) * 100

!    L_MT:Average_C_Per_Kg        = (L_MT:Delivery_Charges_Ex / L_MT:Total_Weight) * 100     ! in cents (not rands)

!    EXECUTE MAN:State + 1
!       L_RF:State  = 'Loading'
!       L_RF:State  = 'Loaded'
!       L_RF:State  = 'On Route'
!       L_RF:State  = 'Transferred'
!    .
    EXIT
Get_Horse               ROUTINE
!    A_TRU:TTID              = VCO:TTID0
!    IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!       L_RF:Reg_of_Hours    = A_TRU:Registration
!    .

!    DRI:DRID                = MAN:DRID
!    IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
!       L_RF:Driver          = CLIP(DRI:FirstName) & ' ' & DRI:Surname
!    .


    ! (p:VCID, p:Option, p:Truck, p:Additional)
    ! (ULONG, BYTE, BYTE=0, BYTE=0),STRING
    HorseReg    = Get_VehComp_Info(MAN:VCID, 2, 100)
    EXIT
! ------------------------------------------------------------------
Set_Days                ROUTINE
    ! (LONG),STRING
    ?Prompt_DayFrom{PROP:Text}      = Week_Day(LO:From_Date)

    ?Prompt_DayTo{PROP:Text}        = Week_Day(LO:To_Date)

    DISPLAY
    EXIT

ThisWindow.AskPreview PROCEDURE

  CODE
      Total_GP_Per    = (Tot_GP / Tot_Delivery_Charges_Ex) * 100  ! %
      PRINT(RPT:grandtotals)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Manifests')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LO:From_Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LO:From_Date',LO:From_Date)                        ! Added by: Report
  BIND('LO:To_Date',LO:To_Date)                            ! Added by: Report
  BIND('LO:Included_TIDs',LO:Included_TIDs)                ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                                     ! File Branches used by this procedure, so make sure it's RelationManager is open
  Relate:Filters.Open                                      ! File Filters used by this procedure, so make sure it's RelationManager is open
  Relate:VehicleCompositionAlias.Open                      ! File VehicleCompositionAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:Transporter,SELF) ! Initialize the browse manager
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,TRA:Key_TransporterName)              ! Add the sort order for TRA:Key_TransporterName for sort order 1
  BRW7.AddLocator(BRW7::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW7::Sort0:Locator.Init(,TRA:TransporterName,1,BRW7)    ! Initialize the browse locator using  using key: TRA:Key_TransporterName , TRA:TransporterName
  BRW7.SetFilter('(LO:Included_TIDs)')                     ! Apply filter expression to browse
  BRW7.AddField(TRA:TransporterName,BRW7.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW7.AddField(TRA:TID,BRW7.Q.TRA:TID)                    ! Field TRA:TID is a hot field or requires assignment from browse
  INIMgr.Fetch('Print_Manifests',ProgressWindow)           ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Manifest, ?Progress:PctText, Progress:Thermometer, 200)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder('+TRA:TransporterName,+VCO:CompositionName,+MAN:MID')
  ThisReport.SetFilter('MAN:DepartDate >= LO:From_Date AND MAN:DepartDate << LO:To_Date + 1')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Manifest.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  IF ?LO:Filter_By_Transporters{Prop:Checked}
    ENABLE(?Group_FT)
  END
  IF NOT ?LO:Filter_By_Transporters{PROP:Checked}
    DISABLE(?Group_FT)
  END
  IF ?LO:Filter_By_Vehicles{Prop:Checked}
    ENABLE(?Group_FV)
  END
  IF NOT ?LO:Filter_By_Vehicles{PROP:Checked}
    DISABLE(?Group_FV)
  END
  FDB9.Init(?LO:Branch,Queue:FileDrop_Branch.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop_Branch,Relate:Branches,ThisWindow)
  FDB9.Q &= Queue:FileDrop_Branch
  FDB9.AddSortOrder(BRA:Key_BranchName)
  FDB9.AddField(BRA:BranchName,FDB9.Q.BRA:BranchName) !List box control field - type derived from field
  FDB9.AddField(BRA:BID,FDB9.Q.BRA:BID) !Primary key field - type derived from field
  FDB9.AddUpdateField(BRA:BID,LO:BID)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
  FDB12.Init(?LO:CompositionName,Queue:FileDrop.ViewPosition,FDB12::View:FileDrop,Queue:FileDrop,Relate:VehicleCompositionAlias,ThisWindow)
  FDB12.Q &= Queue:FileDrop
  FDB12.AddSortOrder(A_VCO:Key_Name)
  FDB12.AddField(A_VCO:CompositionName,FDB12.Q.A_VCO:CompositionName) !List box control field - type derived from field
  FDB12.AddUpdateField(A_VCO:VCID,LO:VCID)
  ThisWindow.AddItem(FDB12.WindowComponent)
  FDB12.DefaultFill = 0
      Fil.Init('Transporters', ?List_Inc)
      Fil.Init('Vehicles', ?List_Veh)
      Fil.Load_('Transporters', 'Print_Manifest-Transporters')
      Fil.Load_('Vehicles', 'Print_Manifest-Vehicles')
  
  
      LO:Included_TIDs        = Fil.Get_('Transporters')
      LO:Included_VCIDs       = Fil.Get_('Vehicles')
  
      IF CLIP(LO:Included_TIDs) = ''
         LO:Included_TIDs = '0'
      .
  
      IF CLIP(LO:Included_VCIDs) = ''
         LO:Included_VCIDs = '0'
      .
  
      BRW7.SetFilter('SQL(TID NOT IN (' & LO:Included_TIDs & '))')
      FDB12.SetFilter('SQL(VCID NOT IN (' & LO:Included_VCIDs & '))')
      LO:From_Date                = GETINI('Print_Manifests', 'From_Date', , GLO:Local_INI)
      LO:To_Date                  = GETINI('Print_Manifests', 'To_Date', , GLO:Local_INI)
  
      LO:BID                      = GETINI('Print_Manifests', 'BID', , GLO:Local_INI)
  
      LO:Vehicles_for_Transporters = GETINI('Print_Manifests', 'Vehicles_for_Transporters', , GLO:Local_INI)
  
      LO:Filter_By_Transporters   = GETINI('Print_Manifests', 'Filter_By_Transporters', , GLO:Local_INI)
      LO:Filter_By_Vehicles       = GETINI('Print_Manifests', 'Filter_By_Vehicles', , GLO:Local_INI)
  
      LO:Summary                  = GETINI('Print_Manifests', 'Summary', , GLO:Local_INI)
  
  
  
      BRA:BID                     = LO:BID
      IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
         LO:Branch                = BRA:BranchName
      .
  
  
      IF ?LO:Filter_By_Transporters{Prop:Checked} = True
        ENABLE(?Group_FT)
      END
      IF ?LO:Filter_By_Transporters{Prop:Checked} = False
        DISABLE(?Group_FT)
      END
  
  
      IF ?LO:Filter_By_Vehicles{Prop:Checked} = True
        ENABLE(?Group_FV)
      END
      IF ?LO:Filter_By_Vehicles{Prop:Checked} = False
        DISABLE(?Group_FV)
      END
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Go'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Branches.Close
    Relate:Filters.Close
    Relate:VehicleCompositionAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Manifests',ProgressWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?LO:To_Date
          
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Restart'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LO:From_Date
          DO Set_Days
    OF ?Calendar
      ThisWindow.Update()
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',LO:From_Date)
      IF Calendar5.Response = RequestCompleted THEN
      LO:From_Date=Calendar5.SelectedDate
      DISPLAY(?LO:From_Date)
      END
      ThisWindow.Reset(True)
          DO Set_Days
      
    OF ?LO:To_Date
          DO Set_Days
      
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',LO:To_Date)
      IF Calendar6.Response = RequestCompleted THEN
      LO:To_Date=Calendar6.SelectedDate
      DISPLAY(?LO:To_Date)
      END
      ThisWindow.Reset(True)
          DO Set_Days
      
    OF ?LO:Filter_By_Transporters
      IF ?LO:Filter_By_Transporters{PROP:Checked}
        ENABLE(?Group_FT)
      END
      IF NOT ?LO:Filter_By_Transporters{PROP:Checked}
        DISABLE(?Group_FT)
      END
      ThisWindow.Reset()
    OF ?Button_Add
      ThisWindow.Update()
      BRW7.UpdateViewRecord()
          Fil.Add_('Transporters', TRA:TransporterName, TRA:TID)
      
          LO:Included_TIDs    = Fil.Get_('Transporters')
      
      !    message('tids:||' & LO:Included_TIDs)
      
          IF CLIP(LO:Included_TIDs) = ''
             LO:Included_TIDs = '0'
          .
          !BRW7.Updatebuffer()
          BRW7.Reset(1)
          BRW7.ResetQueue(Reset:Done)
          !BRW7.UpdateWindow()
    OF ?Button_Remove
      ThisWindow.Update()
          Fil.Del_('Transporters', CHOICE(?List_Inc))
      
          LO:Included_TIDs    = Fil.Get_('Transporters')
          IF CLIP(LO:Included_TIDs) = ''
             LO:Included_TIDs = '0'
          .
          !BRW7.Updatebuffer()
          BRW7.Reset(1)
          BRW7.ResetQueue(Reset:Done)
          !BRW7.UpdateWindow()
    OF ?LO:Filter_By_Vehicles
      IF ?LO:Filter_By_Vehicles{PROP:Checked}
        ENABLE(?Group_FV)
      END
      IF NOT ?LO:Filter_By_Vehicles{PROP:Checked}
        DISABLE(?Group_FV)
      END
      ThisWindow.Reset()
    OF ?LO:Vehicles_for_Transporters
          FDB12.Reset(1)
          FDB12.ResetQueue(Reset:Done)
          
      
    OF ?Button_Add_VC
      ThisWindow.Update()
          Fil.Add_('Vehicles', LO:CompositionName, LO:VCID)
      
          LO:Included_VCIDs    = Fil.Get_('Vehicles')
      
      !    message('tids:||' & LO:Included_TIDs)
      
          IF CLIP(LO:Included_VCIDs) = ''
             LO:Included_VCIDs = '0'
          .
          CLEAR(LO:CompositionName)
      
          FDB12.Reset(1)
          FDB12.ResetQueue(Reset:Done)
      
          GET(Queue:FileDrop, 1)
          ?LO:CompositionName{PROP:Selected}  = 1
          POST(EVENT:Accepted, ?LO:CompositionName)
      
          DISPLAY
    OF ?Button_Remove_VC
      ThisWindow.Update()
          Fil.Del_('Vehicles', CHOICE(?List_Veh))
      
          LO:Included_VCIDs    = Fil.Get_('Vehicles')
          IF CLIP(LO:Included_VCIDs) = ''
             LO:Included_VCIDs = '0'
          .
          FDB12.Reset(1)
          FDB12.ResetQueue(Reset:Done)
      
    OF ?Pause
      ThisWindow.Update()
          PUTINI('Print_Manifests', 'From_Date', LO:From_Date, GLO:Local_INI)
          PUTINI('Print_Manifests', 'To_Date', LO:To_Date, GLO:Local_INI)
      
          PUTINI('Print_Manifests', 'BID', LO:BID, GLO:Local_INI)
      
          PUTINI('Print_Manifests', 'Vehicles_for_Transporters', LO:Vehicles_for_Transporters, GLO:Local_INI)
      
          PUTINI('Print_Manifests', 'Filter_By_Transporters', LO:Filter_By_Transporters, GLO:Local_INI)
          PUTINI('Print_Manifests', 'Filter_By_Vehicles', LO:Filter_By_Vehicles, GLO:Local_INI)
      
          PUTINI('Print_Manifests', 'Summary', LO:Summary, GLO:Local_INI)
      
      
          Summary_Text            = ''
          IF LO:Summary = TRUE
             Summary_Text         = 'Summary'
          .
          Fil.Save_('Transporters', 'Print_Manifest-Transporters')
          Fil.Save_('Vehicles', 'Print_Manifest-Vehicles')
          DISABLE(?Tab_Options)
      
      
          
          ThisReport.AddSortOrder()
          ThisReport.AppendOrder('+TRA:TransporterName,+VCO:CompositionName')
      
          !ThisReport.SetFilter('')
      
          IF LO:BID ~= 0
             ThisReport.SetFilter('MAN:BID = ' & LO:BID, '0')
          .
      
          IF LO:From_Date > 0
             ThisReport.SetFilter('MAN:DepartDate >= LO:From_Date', '1')
          .
      
          IF LO:To_Date > 0
             ThisReport.SetFilter('MAN:DepartDate << LO:To_Date + 1', '2')
          .
      
          IF LO:Filter_By_Transporters = TRUE
             LO:Included_TIDs        = Fil.Get_('Transporters')
             IF CLIP(LO:Included_TIDs) ~= '' AND CLIP(LO:Included_TIDs) ~= '0'
                ThisReport.SetFilter('SQL(A.TID IN (' & CLIP(LO:Included_TIDs) & '))', '3')
             .
      
             IF LO:Filter_By_Vehicles = TRUE
                LO:Included_VCIDs      = Fil.Get_('Vehicles')
                IF CLIP(LO:Included_VCIDs) ~= '' AND CLIP(LO:Included_VCIDs) ~= '0'
                   ThisReport.SetFilter('SQL(A.VCID IN (' & CLIP(LO:Included_VCIDs) & '))', '4')
          .  .  .
      
          ThisReport.ApplyFilter()
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LO:From_Date
          DO Set_Days
    OF ?LO:To_Date
          DO Set_Days
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

A_Filter_.Add_            PROCEDURE(STRING p_Str, STRING p_Value, BYTE p_PermitDup=0)
A_Ok    BYTE(1)
    CODE
    SELF.A_FQ.Filter_Value    = p_Value

    IF p_PermitDup = 0
       GET(SELF.A_FQ, SELF.A_FQ.Filter_Value)
       IF ~ERRORCODE()
          A_Ok  = FALSE
    .  .

    IF A_Ok = TRUE
       SELF.A_FQ.Filter_Str      = p_Str
       SELF.A_FQ.Filter_Value    = p_Value
       ADD(SELF.A_FQ)
    .
    RETURN


A_Filter_.Del_            PROCEDURE(LONG p_Pos)       !,LONG
Ret     LONG
    CODE
    Ret     = 0
    GET(SELF.A_FQ, p_Pos)
    IF ERRORCODE()
       Ret  = -1
    ELSE
       DELETE(SELF.A_FQ)
    .
    RETURN(Ret)


A_Filter_.Get_Item        PROCEDURE(LONG p_Pos, BYTE p_Type)  !,STRING
Fltr        CSTRING(500)
    CODE
    GET(SELF.A_FQ, p_Pos)
    IF ~ERRORCODE()
       EXECUTE p_Type
          Fltr  = SELF.A_FQ.Filter_Str
          Fltr  = SELF.A_FQ.Filter_Value
    .  .
    RETURN(Fltr)


A_Filter_.Get_            PROCEDURE()     !,STRING
Fltr        CSTRING(500)
Idx         LONG
    CODE
    Fltr    = ''
    Idx     = 0

    LOOP
       Idx  += 1
       GET(SELF.A_FQ, Idx)
       IF ERRORCODE()
          BREAK
       .

       !SELF.A_FQ.Filter_Str
       !

       Fltr     = Fltr & ',' & SELF.A_FQ.Filter_Value
    .
    Fltr        = SUB(Fltr, 2, LEN(CLIP(Fltr)))
    RETURN(Fltr)








A_Filter_.Init            PROCEDURE(STRING p_Name, LONG p_ListCtrl)
    CODE
    SELF.Name_              = p_Name

    p_ListCtrl{PROP:From}   = SELF.A_FQ
    RETURN

A_Filter_.Construct       PROCEDURE()
    CODE
    SELF.A_FQ                 &= NEW(A_Filter_Q)
    RETURN

A_Filter_.Destruct        PROCEDURE()
    CODE
    DISPOSE(SELF.A_FQ)
    RETURN
A_Filter_.Save_           PROCEDURE(STRING p_Name)    !,LONG
Ret     LONG
    CODE
    Access:Filters.Open()
    Access:Filters.UseFile()

    CLEAR(_F:Record)
    _F:Name      = p_Name
    IF Access:Filters.TryFetch(_F:Key_Name) ~= LEVEL:Benign
       Access:Filters.PrimeRecord()
       _F:Name   = p_Name
       IF Access:Filters.TryInsert() ~= LEVEL:Benign
          Access:Filters.CancelAutoInc()
          Ret    = -1
    .  .

    _F:Value     = SELF.Get_()

    IF Access:Filters.TryUpdate() ~= LEVEL:Benign
       Ret   = -2
    .

    Access:Filters.Close()
    RETURN(Ret)


A_Filter_.Load_           PROCEDURE(STRING p_Name)    !,LONG
Ret     LONG
Str     STRING(500)
    CODE
    Access:Filters.Open()
    Access:Filters.UseFile()

    CLEAR(_F:Record)
    _F:Name  = p_Name
    IF Access:Filters.TryFetch(_F:Key_Name) ~= LEVEL:Benign
       Ret   = -1
    ELSE
       Str   = _F:Value
       LOOP
          IF CLIP(Str) = ''
             BREAK
          .
          ! (*STRING, STRING, BYTE = 0, BYTE = 0),STRING
          SELF.Add_('', Get_1st_Element_From_Delim_Str(Str, ',', TRUE))
       .

       ! No strings to go with the values have been loaded... the filter class wont know about the files that
       ! the strings would come from (like it doesnt know what the values mean).  We could store them along with vlaues
       ! but if the strings (in this case Transporters names) changed then this would be bad.
       ! We need to load the strings though, so another process must load them, we could create a virtual method
       ! and the individual implementation would have to provide this...
       ! Made tha Add_ virtual....
    .

    Access:Filters.Close()
    RETURN(Ret)



! _Filters        FILE,DRIVER('MSSQL'),CREATE,PRE(_F),THREAD,BINDABLE,NAME('_Filters')
! PKey_FID        KEY(_F:FID),NOCASE,OPT,PRIMARY
! Key_Name        KEY(_F:Name),NOCASE,OPT
! Record            RECORD,PRE()
! FID                 ULONG
! Name                CSTRING(101)
! Values              CSTRING(1001)
!                 . .
! ------------------------------------------------------------------
Filters_.Add_            PROCEDURE(STRING p_Name, STRING p_Str, STRING p_Value, BYTE p_PermitDup=0)
    CODE
    IF SELF.Get_FO(p_Name) = 0
       SELF.FQ.FO.Add_(p_Str, p_Value, p_PermitDup)
    .
    RETURN

Filters_.Del_            PROCEDURE(STRING p_Name, LONG p_Pos)       !,LONG
Ret     LONG
    CODE
    Ret     = SELF.Get_FO(p_Name)
    IF Ret = 0
       RETURN( SELF.FQ.FO.Del_(p_Pos) )
    .
    RETURN(Ret)


Filters_.Get_Item        PROCEDURE(STRING p_Name, LONG p_Pos, BYTE p_Type)  !,STRING
Fltr        CSTRING(501)
    CODE
    IF SELF.Get_FO(p_Name) = 0
        Fltr    = SELF.FQ.FO.Get_Item(p_Pos, p_Type)
    .
    RETURN(Fltr)


Filters_.Get_            PROCEDURE(STRING p_Name)     !,STRING
Fltr        CSTRING(501)
    CODE
    IF SELF.Get_FO(p_Name) = 0
        Fltr    = SELF.FQ.FO.Get_()
    .
    RETURN(Fltr)




Filters_.Init            PROCEDURE(STRING p_Name, LONG p_ListCtrl)
Res     LONG
    CODE
    Res                 = SELF.Get_FO(p_Name)

    CASE Res
    OF 0        ! Exist and got
       SELF.FQ.FO.Init(p_Name, p_ListCtrl)

       SELF.FQ.Name_    = p_Name
       PUT(SELF.FQ)
    ELSE        ! Doesnt exit
       ! Add an object to the Queue
       SELF.FQ.Name_    = p_Name
       SELF.FQ.FO      &= NEW(A_Filter_My)
       ADD(SELF.FQ)

       SELF.FQ.FO.Init(p_Name, p_ListCtrl)
    .
    RETURN


Filters_.Construct       PROCEDURE()
    CODE
    SELF.FQ                 &= NEW(Filters_Q)
    RETURN

Filters_.Destruct        PROCEDURE()
    CODE
    LOOP
       GET(SELF.FQ,1)
       IF ERRORCODE()
          BREAK
       .
       DISPOSE(SELF.FQ.FO)
       DELETE(SELF.FQ)
    .

    DISPOSE(SELF.FQ)
    RETURN



Filters_.Get_FO         PROCEDURE(STRING p_Name)        !,LONG
Ret     LONG
    CODE
    SELF.FQ.Name_   = p_Name
    GET(SELF.FQ, SELF.FQ.Name_)
    IF ERRORCODE()
       Ret  = -1
    .
    RETURN(Ret)
Filters_.Save_           PROCEDURE(STRING p_Name, STRING p_SaveName)    !,LONG
Ret     LONG
    CODE
    Ret     = SELF.Get_FO(p_Name)
    IF Ret = 0
       Ret  = SELF.FQ.FO.Save_(p_SaveName)
    .
    RETURN(Ret)


Filters_.Load_           PROCEDURE(STRING p_Name, STRING p_SaveName)    !,LONG
Ret     LONG
    CODE
    Ret     = SELF.Get_FO(p_Name)
    IF Ret = 0
       Ret  = SELF.FQ.FO.Load_(p_SaveName)
    .
    RETURN(Ret)

!    Fil.Save_('Transporters', 'Print_Manifest-Transporters')
!    Fil.Load_('Transporters', 'Print_Manifest-Transporters')

!    Fil.Save_('Vehicles', 'Print_Manifest-Transporters')
!    Fil.Load_('Vehicles', 'Print_Manifest-Transporters')

! ------------------------------------------------------------------
A_Filter_My.Add_         PROCEDURE(STRING p_Str, STRING p_Value, BYTE p_PermitDup=0)       !,VIRTUAL
l_Str       STRING(1001)
A_Ok        BYTE(1)
    CODE
    ! I know about my tables in this procedure...
    l_Str                       = p_Str

!    Fil.Load_('Transporters', 'Print_Manifest-Transporters')
!    Fil.Load_('Vehicles', 'Print_Manifest-Transporters')

    IF CLIP(l_Str) = ''
       CASE SELF.Name_
       OF 'Transporters'
          ! Load it from table based on p_Value
          TRA:TID               = p_Value
          IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
             l_Str              = TRA:TransporterName
          .
       OF 'Vehicles'
          ! Load it from table based on p_Value
          A_VCO:VCID            = p_Value
          IF Access:VehicleCompositionAlias.TryFetch(A_VCO:PKey_VCID) = LEVEL:Benign
             l_Str              = A_VCO:CompositionName
    .  .  .


    !PARENT.Add_(p_Str, p_Value)
    ! **** ?????
    ! The above GPF's but it shouldnt?
    

    SELF.A_FQ.Filter_Value      = p_Value

    IF p_PermitDup = 0
       GET(SELF.A_FQ, SELF.A_FQ.Filter_Value)
       IF ~ERRORCODE()
          A_Ok  = FALSE
    .  .

    IF A_Ok = TRUE
       SELF.A_FQ.Filter_Str     = l_Str
       SELF.A_FQ.Filter_Value   = p_Value
       ADD(SELF.A_FQ)
    .

!    message('p_Str: ' & p_Str & '|p_Value: ' & p_Value)
    RETURN

ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      DO Get_Values
      DO Get_Horse
  ReturnValue = PARENT.TakeRecord()
    IF LO:Summary = TRUE
       SETTARGET(Report)
       ?Detail{PROP:Height}         = 0
       ?Group_All{PROP:Hide}        = TRUE
  
       ?Group_footer{PROP:Hide}     = FALSE
  !     ?Group_footer{PROP:Height}   = 250
       SETTARGET()
    .
  IF 0
    PRINT(RPT:grandtotals)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Manifests','Print_Manifests','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True


BRW7.ApplyFilter PROCEDURE

  CODE
  !    LO:Included_TIDs    = Fil.Get_('Transporters')     - loaded already
  
      BRW7.SetFilter('SQL(TID NOT IN (' & LO:Included_TIDs & '))')
  
  PARENT.ApplyFilter


FDB9.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
        Queue:FileDrop_Branch.BRA:BranchName       = 'All'
        GET(Queue:FileDrop_Branch, Queue:FileDrop_Branch.BRA:BranchName)
        IF ERRORCODE()
           CLEAR(Queue:FileDrop_Branch)
           Queue:FileDrop_Branch.BRA:BranchName    = 'All'
           Queue:FileDrop_Branch.BRA:BID           = 0
           ADD(Queue:FileDrop_Branch)
        .
    
    
        IF LO:BID_FirstTime = TRUE
           !SELECT(?LO:Branch, RECORDS(Queue:FileDrop_Branch))
    
           LO:BID         = 0
           LO:Branch      = 'All'
        .
    
        LO:BID_FirstTime     = FALSE
  RETURN ReturnValue


FDB12.ApplyFilter PROCEDURE

  CODE
      !LO:Included_VCIDs      = Fil.Get_('Vehicles')      - loaded already
  
      FDB12.SetFilter('SQL(VCID NOT IN (' & LO:Included_VCIDs & '))')
  
      IF LO:Vehicles_for_Transporters = TRUE
         LO:Included_TIDs    = Fil.Get_('Transporters')
         IF CLIP(LO:Included_TIDs) ~= ''
            FDB12.SetFilter('SQL(TID IN (' & LO:Included_TIDs & '))','2')
         ELSE
            FDB12.SetFilter('','2')
         .
      ELSE
         FDB12.SetFilter('','2')
      .
  PARENT.ApplyFilter

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_Transporters_old PROCEDURE 

Progress:Thermometer BYTE                                  !
Address              STRING(350)                           !
Telephone            STRING(14)                            !
Fax                  STRING(14)                            !
Mobile               STRING(14)                            !
Process:View         VIEW(Transporter)
                       PROJECT(TRA:TransporterName)
                     END
ProgressWindow       WINDOW('Report Transporter'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Transporter Report'),AT(250,850,7750,10333),PRE(RPT),PAPER(PAPER:A4),FONT('MS Sans Serif', |
  8,,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,604),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular)
                         STRING('Transporter Details'),AT(0,20,7750),USE(?ReportTitle),FONT('Arial',18,,FONT:regular), |
  CENTER
                         BOX,AT(0,350,7750,250),USE(?HeaderBox),COLOR(COLOR:Black)
                         LINE,AT(1667,354,0,250),USE(?DetailLine:5),COLOR(COLOR:Black)
                         LINE,AT(4792,354,0,250),USE(?HeaderLine:2),COLOR(COLOR:Black)
                         LINE,AT(5792,354,0,250),USE(?HeaderLine:9),COLOR(COLOR:Black)
                         LINE,AT(6771,354,0,250),USE(?HeaderLine:10),COLOR(COLOR:Black)
                         STRING('Fax'),AT(5844,385,,170),USE(?HeaderTitle:5),TRN
                         STRING('Cell / Mobile'),AT(6792,385,,170),USE(?HeaderTitle:6),TRN
                         STRING('Address'),AT(1719,417,1198,156),USE(?HeaderTitle:7),TRN
                         STRING('Transporter Name'),AT(83,385,1563,156),USE(?HeaderTitle:2),TRN
                         STRING('Phone'),AT(4865,385,,170),USE(?HeaderTitle:4),TRN
                       END
Detail                 DETAIL,AT(,,7750,250),USE(?Detail)
                         LINE,AT(0,0,0,250),USE(?DetailLine:0),COLOR(COLOR:Black)
                         LINE,AT(1667,0,0,250),USE(?DetailLine:1),COLOR(COLOR:Black)
                         LINE,AT(4792,0,0,250),USE(?DetailLine:2),COLOR(COLOR:Black)
                         LINE,AT(5792,0,0,250),USE(?HeaderLine:7),COLOR(COLOR:Black)
                         LINE,AT(7750,0,0,250),USE(?DetailLine:3),COLOR(COLOR:Black)
                         STRING(@s35),AT(83,52,1563,156),USE(TRA:TransporterName),LEFT
                         STRING(@s255),AT(1719,52,3021,156),USE(Address),TRN
                         STRING(@s14),AT(4865,52),USE(Telephone),TRN
                         STRING(@s14),AT(5844,52),USE(Fax),TRN
                         LINE,AT(6771,0,0,250),USE(?HeaderLine:8),COLOR(COLOR:Black)
                         STRING(@s14),AT(6792,52),USE(Mobile),TRN
                         LINE,AT(0,250,7750,0),USE(?DetailEndLine),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                         STRING('Date:'),AT(115,52,344,135),USE(?ReportDatePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Date Stamp -->'),AT(490,52,927,135),USE(?ReportDateStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING('Time:'),AT(1625,52,271,135),USE(?ReportTimePrompt),FONT('Arial',8,,FONT:regular),TRN
                         STRING('<<-- Time Stamp -->'),AT(1927,52,927,135),USE(?ReportTimeStamp),FONT('Arial',8,,FONT:regular), |
  TRN
                         STRING(@pPage <<#p),AT(6950,52,700,135),USE(?PageCount),FONT('Arial',8,,FONT:regular),PAGENO
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('MS Sans Serif',8,,FONT:regular)
                         IMAGE,AT(0,0,7750,11188),USE(?FormImage),TILED
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Transporters_old')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Addresses.SetOpenRelated()
  Relate:Addresses.Open                                    ! File Addresses used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Print_Transporters_old',ProgressWindow)    ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ThisReport.Init(Process:View, Relate:Transporter, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  ThisReport.AppendOrder('+TRA:TransporterName')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:Transporter.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  SELF.Zoom = PageWidth
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Addresses.Close
  END
  IF SELF.Opened
    INIMgr.Update('Print_Transporters_old',ProgressWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      ! (p:AID, p:Type)
      ! (ULONG,BYTE=0),STRING
      ! p:Type
      !   0 - Comma delimited string
      !   1 - Block
      !   2 - Comma delimited string - including empty Line 1 & 2 where they are empty
      !
      ! Returns
      !   Address Name, Line1 (if), Line2 (if), Post Code, City, Province
      !   or
      !   Address Name, Line1 (if), Line2 (if), <missing info string>
  
      Address     = Get_Address(TRA:AID)
  
  
      ADD:AID     = TRA:AID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) ~= LEVEL:Benign
         CLEAR(ADD:Record)
      .
  
      Telephone   = ADD:PhoneNo
  
      Fax         = ADD:Fax
  
      Mobile      = ADD:PhoneNo2
  
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','RPTTRNIS','Print_Transporters','Print_Transporters','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.SetScanCopyMode(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Print_TripSheet_no_Preview PROCEDURE (p:TRID)

Progress:Thermometer BYTE                                  !
LOC:Rep_Fields       GROUP,PRE(L_RF)                       !
Weight               DECIMAL(10,2)                         !In kg's
VolumetricWeight     DECIMAL(10,2)                         !Weight based on Volumetric calculation (in kgs)
Charge               DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
State                STRING(20)                            !State of this manifest
Driver               STRING(50)                            !First Name
Driver_Assistant     STRING(35)                            !
Reg_of_Horse         STRING(20)                            !
Reg_of_Trailer       STRING(20)                            !
COD_Comment          STRING(20)                            !
TransporterName      STRING(55)                            !Transporters Name
Suburb               STRING(50)                            !Suburb
                     END                                   !
LOC:Delivery_Totals  GROUP,PRE(L_DT)                       !
DID                  ULONG                                 !Delivery ID
InsuranceCharge      DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Insurance            DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Docs                 DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Fuel                 DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Charge               DECIMAL(9,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
                     END                                   !
LOC:Manifest_Totals  GROUP,PRE(L_MT)                       !
Cost                 DECIMAL(10,2)                         !
TRID                 ULONG                                 !Tripsheet ID
Delivery_Charges     DECIMAL(11,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Total_Weight         DECIMAL(10,2)                         !In kg's
Gross_Profit         DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Gross_Profit_Percent DECIMAL(6,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Average_C_Per_Kg     DECIMAL(7,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Legs_Cost            DECIMAL(10,2)                         !
Total_Cost           DECIMAL(10,2)                         !Includes extra legs cost
                     END                                   !
Process:View         VIEW(TripSheetDeliveries)
                       PROJECT(TRDI:TRID)
                       PROJECT(TRDI:UnitsLoaded)
                       PROJECT(TRDI:DIID)
                       JOIN(DELI:PKey_DIID,TRDI:DIID)
                         PROJECT(DELI:DID)
                         PROJECT(DELI:Units)
                         PROJECT(DELI:VolumetricWeight)
                         PROJECT(DELI:Weight)
                         PROJECT(DELI:DIID)
                         JOIN(MALD:FKey_DIID,DELI:DIID)
                           PROJECT(MALD:MLID)
                           JOIN(MAL:PKey_MLID,MALD:MLID)
                             PROJECT(MAL:MID)
                             JOIN(MAN:PKey_MID,MAL:MID)
                             END
                           END
                         END
                         JOIN(DEL:PKey_DID,DELI:DID)
                           PROJECT(DEL:DID)
                           PROJECT(DEL:DINo)
                           PROJECT(DEL:InsuranceRate)
                           PROJECT(DEL:Insure)
                           PROJECT(DEL:TotalConsignmentValue)
                           PROJECT(DEL:CID)
                           PROJECT(DEL:DeliveryAID)
                           PROJECT(DEL:CollectionAID)
                           JOIN(CLI:PKey_CID,DEL:CID)
                           END
                           JOIN(A_ADD:PKey_AID,DEL:DeliveryAID)
                             PROJECT(A_ADD:AddressName)
                           END
                           JOIN(ADD:PKey_AID,DEL:CollectionAID)
                             PROJECT(ADD:AddressName)
                           END
                         END
                       END
                       JOIN(TRI:PKey_TID,TRDI:TRID)
                         PROJECT(TRI:DepartDate)
                         PROJECT(TRI:DepartTime)
                         PROJECT(TRI:TRID)
                         PROJECT(TRI:BID)
                         PROJECT(TRI:VCID)
                         JOIN(BRA:PKey_BID,TRI:BID)
                           PROJECT(BRA:BranchName)
                         END
                         JOIN(VCO:PKey_VCID,TRI:VCID)
                           PROJECT(VCO:CompositionName)
                           PROJECT(VCO:TID)
                         END
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(615,2417,10396,5302),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('Arial',10,,FONT:regular, |
  CHARSET:ANSI),THOUS
                       HEADER,AT(615,1000,10396,1417),USE(?Header),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Trip Sheet No.:'),AT(7615,63),USE(?String31),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@N_10),AT(9135,63,1021,167),USE(TRI:TRID),FONT(,,,FONT:bold,CHARSET:ANSI),RIGHT(1), |
  TRN
                         STRING('Trip Sheet'),AT(458,52,9479,417),USE(?String55),FONT(,24,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         IMAGE('fbn_logo_small.jpg'),AT(52,52,2302,458),USE(?Image1)
                         STRING('Branch:'),AT(63,563,1083,167),USE(?String51),TRN
                         STRING(@s35),AT(1063,563),USE(BRA:BranchName)
                         STRING('Arrival Time Depot: _{25}'),AT(7615,323),USE(?String52:3),TRN
                         STRING('Driver:'),AT(63,771,1083,167),USE(?String69:2),TRN
                         STRING(@s35),AT(1063,771,1583,167),USE(L_RF:Driver)
                         STRING('Assistant:'),AT(5073,625),USE(?String52:7),TRN
                         STRING(@s35),AT(5719,625,1823,156),USE(L_RF:Driver_Assistant),TRN
                         STRING('Supervisor Signature: _{23}'),AT(7615,625),USE(?String52:4),TRN
                         STRING('Date Despatched:'),AT(63,979,1083,167),USE(?String3:11),LEFT,TRN
                         STRING('Time Out:'),AT(63,1198,1083),USE(?String3:10),LEFT,TRN
                         STRING(@d6),AT(1063,979,,167),USE(TRI:DepartDate),RIGHT(1)
                         STRING(@t7),AT(1063,1198,729,167),USE(TRI:DepartTime),RIGHT(1)
                         STRING('Drivers Signature: _{26}'),AT(7615,927),USE(?String52:5),TRN
                         STRING('Starting Kms: _{23}'),AT(5073,927),USE(?String52),TRN
                         STRING('Manager Signature: _{25}'),AT(7615,1240),USE(?String52:6),TRN
                         STRING('Closing Kms: _{23}'),AT(5073,1240),USE(?String52:2),TRN
                       END
break_tripsheet        BREAK(TRDI:TRID)
                         HEADER,AT(0,0,,740),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI),PAGEBEFORE(-1)
                           STRING('Transporter:'),AT(63,52,1083),USE(?String3:12),LEFT,TRN
                           STRING(@s55),AT(1063,52,3073,156),USE(L_RF:TransporterName),LEFT
                           LINE,AT(42,250,10350,0),USE(?Line8:4),COLOR(COLOR:Black)
                           STRING(@s35),AT(938,302,1583,167),USE(VCO:CompositionName),TRN
                           LINE,AT(42,500,10350,0),USE(?Line8:6),COLOR(COLOR:Black)
                           STRING('Comments'),AT(7865,531,646,167),USE(?String3:8),CENTER,TRN
                           LINE,AT(42,719,10350,0),USE(?Line8:2),COLOR(COLOR:Black)
                           STRING('Signature'),AT(9281,531,781,167),USE(?String3:9),CENTER,TRN
                           STRING('Time Out'),AT(6813,531,542,167),USE(?String3:7),CENTER,TRN
                           STRING('Reg. Horse:'),AT(2969,302),USE(?String69:3),TRN
                           STRING(@s20),AT(3646,302,979,167),USE(L_RF:Reg_of_Horse)
                           STRING('Trailer:'),AT(5188,302),USE(?String69:4),TRN
                           STRING(@s20),AT(5625,302),USE(L_RF:Reg_of_Trailer)
                           STRING('Vehicle Comp.:'),AT(125,302),USE(?String69),TRN
                           STRING('Act Kg'),AT(5219,531,542,167),USE(?String3:5),CENTER,TRN
                           STRING('Time In'),AT(5927,531,542,167),USE(?String3:6),CENTER,TRN
                           STRING('No. of Items'),AT(4573,531,594,167),USE(?String3:4),TRN
                           STRING('Sender'),AT(1073,531,1000,167),USE(?String3:2),CENTER,TRN
                           STRING('Deliver To'),AT(3042,531,1000,167),USE(?String3:3),CENTER,TRN
                           STRING('DN No.'),AT(42,531,,167),USE(?String3),TRN
                           STRING('State:'),AT(8563,63),USE(?String54),FONT(,,,FONT:regular,CHARSET:ANSI),TRN
                           STRING(@s20),AT(8917,63,,167),USE(L_RF:State),FONT(,,,FONT:regular,CHARSET:ANSI),RIGHT(1), |
  TRN
                           STRING(@s50),AT(8135,292),USE(L_RF:Suburb)
                           STRING('Suburb:'),AT(7698,292),USE(?String54:2),FONT(,,,FONT:regular,CHARSET:ANSI),TRN
                         END
break_delivery           BREAK(DEL:DID)
Detail                     DETAIL,AT(,,,0),USE(?Detail),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                             GROUP,AT(31,-21,9650,281),USE(?Group1),BOXED,HIDE,TRN
                               STRING(@n6),AT(5344,73,406,167),USE(TRDI:UnitsLoaded)
                               STRING('Not printing detail lines at all'),AT(52,73,885,167),USE(?String1),TRN
                               STRING(@n~AW~-13.2),AT(1552,73,875,167),USE(DELI:Weight),RIGHT(1)
                               STRING(@n-11.2),AT(4594,73,677,167),USE(L_RF:VolumetricWeight),RIGHT(1),HIDE
                               STRING(@n6),AT(5813,73),USE(DELI:Units),HIDE
                               STRING(@n_10),AT(1042,73,458,167),USE(DELI:DID),RIGHT(1),TRN
                               STRING(@n~CW~-13.2),AT(2469,73,875,167),USE(L_RF:Weight),RIGHT(1)
                             END
                           END
                           FOOTER,AT(0,0,,188),FONT('MS Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                             STRING(@n-11),AT(5219,10,542,167),USE(L_RF:Weight,,?L_RF:Weight:2),RIGHT(1),SUM,RESET(break_delivery), |
  TRN
                             STRING(@s20),AT(7604,10,1302,208),USE(L_RF:COD_Comment)
                             STRING(@n6),AT(4760,10),USE(TRDI:UnitsLoaded,,?TRDI:UnitsLoaded:2),RIGHT(1),SUM,RESET(break_delivery), |
  TRN
                             STRING(@s35),AT(2646,10,2031,167),USE(A_ADD:AddressName)
                             STRING(@n_10),AT(-63,10,479,167),USE(DEL:DINo),RIGHT(1)
                             STRING(@s35),AT(521,10,2031,208),USE(ADD:AddressName)
                             LINE,AT(448,0,0,198),USE(?Line1:9),COLOR(COLOR:Black)
                             LINE,AT(2583,0,0,198),USE(?Line1:2),COLOR(COLOR:Black)
                             LINE,AT(4750,0,0,198),USE(?Line1:3),COLOR(COLOR:Black)
                             LINE,AT(5208,0,0,198),USE(?Line1:6),COLOR(COLOR:Black)
                             LINE,AT(5813,0,0,198),USE(?Line1:4),COLOR(COLOR:Black)
                             LINE,AT(6656,0,0,198),USE(?Line1:7),COLOR(COLOR:Black)
                             LINE,AT(7542,0,0,198),USE(?Line1:8),COLOR(COLOR:Black)
                             LINE,AT(8946,0,0,198),USE(?Line1:5),COLOR(COLOR:Black)
                             LINE,AT(42,188,10350,0),USE(?Line12),COLOR(COLOR:Black),LINEWIDTH(3)
                           END
                         END
                         FOOTER,AT(0,0,,365)
                           LINE,AT(42,52,10350,0),USE(?Line8:3),COLOR(COLOR:Black)
                           STRING('Total Weight:'),AT(2094,104),USE(?String65:2),TRN
                           STRING(@n-11.2),AT(2979,104),USE(L_RF:Weight,,?L_RF:Weight:3),RIGHT(1),SUM,RESET(break_tripsheet), |
  TRN
                           STRING(@n6),AT(1240,104),USE(TRDI:UnitsLoaded,,?TRDI:UnitsLoaded:3),RIGHT(1),SUM,RESET(break_tripsheet), |
  TRN
                           STRING('Units:'),AT(844,104),USE(?String65),TRN
                         END
                       END
Collections            DETAIL,AT(,,,2125),USE(?collections),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI),TOGETHER
                         STRING('Collections'),AT(52,0,9531,260),USE(?String66),FONT(,12,,FONT:bold,CHARSET:ANSI),CENTER, |
  TRN
                         STRING('No.'),AT(63,252),USE(?String58),TRN
                         STRING('DI Number'),AT(363,252),USE(?String58:9),TRN
                         STRING('From'),AT(1292,260,1563,156),USE(?String58:2),CENTER,TRN
                         STRING('To'),AT(3021,260,1823,156),USE(?String58:3),CENTER,TRN
                         STRING('Weight (kg)'),AT(4979,250,938,167),USE(?String58:4),CENTER,TRN
                         STRING('No. of Packages'),AT(6094,250,938,167),USE(?String58:5),CENTER,TRN
                         STRING('Time In'),AT(7063,250,938,167),USE(?String58:6),CENTER,TRN
                         STRING('Time Out'),AT(8000,250,938,167),USE(?String58:7),CENTER,TRN
                         STRING('Signature'),AT(9281,260,729,156),USE(?String58:8),CENTER,TRN
                         LINE,AT(300,458,0,1590),USE(?Line14),COLOR(COLOR:Black)
                         LINE,AT(1165,458,0,1590),USE(?Line141),COLOR(COLOR:Black)
                         LINE,AT(2948,458,0,1590),USE(?Line14:3),COLOR(COLOR:Black)
                         LINE,AT(4865,458,0,1590),USE(?Line14:7),COLOR(COLOR:Black)
                         LINE,AT(6063,458,0,1590),USE(?Line14:6),COLOR(COLOR:Black)
                         LINE,AT(7073,458,0,1590),USE(?Line14:5),COLOR(COLOR:Black)
                         LINE,AT(8000,458,0,1590),USE(?Line14:4),COLOR(COLOR:Black)
                         LINE,AT(8946,458,0,1590),USE(?Line14:2),COLOR(COLOR:Black)
                         STRING('1'),AT(63,479,208,156),USE(?String58:10),TRN
                         STRING('2'),AT(63,677,208,156),USE(?String58:11),TRN
                         STRING('3'),AT(63,885,208,156),USE(?String58:12),TRN
                         STRING('4'),AT(63,1083,208,156),USE(?String58:13),TRN
                         STRING('5'),AT(63,1281,208,156),USE(?String58:14),TRN
                         STRING('6'),AT(63,1479,208,156),USE(?String58:15),TRN
                         STRING('7'),AT(63,1677,208,156),USE(?String58:16),TRN
                         STRING('8'),AT(63,1885,208,156),USE(?String58:17),TRN
                         LINE,AT(42,450,10350,0),USE(?Line13),COLOR(COLOR:Black)
                         LINE,AT(42,650,10350,0),USE(?Line13:2),COLOR(COLOR:Black)
                         LINE,AT(42,850,10350,0),USE(?Line13:3),COLOR(COLOR:Black)
                         LINE,AT(42,1050,10350,0),USE(?Line13:4),COLOR(COLOR:Black)
                         LINE,AT(42,1250,10350,0),USE(?Line13:41),COLOR(COLOR:Black)
                         LINE,AT(42,1450,10350,0),USE(?Line13:5),COLOR(COLOR:Black)
                         LINE,AT(42,1650,10350,0),USE(?Line13:6),COLOR(COLOR:Black)
                         LINE,AT(42,1850,10350,0),USE(?Line13:7),COLOR(COLOR:Black)
                         LINE,AT(42,2050,10350,0),USE(?Line13:8),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(615,7740,10396,240),USE(?Footer),FONT('MS Sans Serif',,COLOR:Black,,CHARSET:ANSI)
                         STRING('Report Date:'),AT(73,31),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(875,31),USE(?ReportDateStamp),TRN
                         STRING('Report Time:'),AT(2250,31),USE(?ReportTimePrompt),TRN
                         STRING('<<-- Time Stamp -->'),AT(3052,31),USE(?ReportTimeStamp),TRN
                         STRING(@N3),AT(10031,31),USE(ReportPageNumber)
                         STRING('Page:'),AT(9615,31),USE(?String61),TRN
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.AskPreview PROCEDURE

  CODE
          PRINT(RPT:Collections)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_TripSheet_no_Preview')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Add_Suburbs.SetOpenRelated()
  Relate:Add_Suburbs.Open                                  ! File Add_Suburbs used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterAlias.Open                             ! File TransporterAlias used by this procedure, so make sure it's RelationManager is open
  Access:DeliveryLegs.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailerAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoad.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoadDeliveries.UseFile                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:TripSheetDeliveries, ?Progress:PctText, Progress:Thermometer, ProgressMgr, TRDI:TRID)
  ThisReport.AddSortOrder(TRDI:FKey_TRID)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report)
  ?Progress:UserString{PROP:Text} = ''
  Relate:TripSheetDeliveries.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
      IF p:TRID ~= 0
         ThisReport.AddRange(TRDI:TRID, p:TRID)
      .
  
      PRINTER{PROPPRINT:Copies} = 2
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Add_Suburbs.Close
    Relate:TransporterAlias.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      CLEAR(L_DT:InsuranceCharge)
      IF DEL:Insure = TRUE
         L_DT:InsuranceCharge    = (DEL:InsuranceRate / 100) * DEL:TotalConsignmentValue
      .
  
  !       message('DELI:DID||' & DELI:DID)
  
      A_TRU:TTID              = VCO:TTID0
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Horse    = A_TRU:Registration
      .
      A_TRU:TTID              = VCO:TTID1
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
         L_RF:Reg_of_Trailer  = A_TRU:Registration
      .
  
      DRI:DRID                = TRI:DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RF:Driver          = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
      DRI:DRID                    = TRI:Assistant_DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
         L_RF:Driver_Assistant    = CLIP(DRI:FirstName) & ' ' & DRI:Surname
      .
  
  
      !A_TRA:TID            = VCO:TID
      A_TRA:TID            = TRI:TID
      IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) = LEVEL:Benign
         L_RF:TransporterName = A_TRA:TransporterName
      .
  
      SUBU:SUID           = TRI:SUID
      IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) = LEVEL:Benign
         L_RF:Suburb      = SUBU:Suburb
      .
  
  
      CLEAR(L_RF:COD_Comment)
      CASE DEL:Terms              ! Pre Paid|COD|Account
      OF 0
      OF 1
         L_RF:COD_Comment     = 'COD'
      OF 2
      .
      ! Get Invoice
      str_"       = Get_Invoices(0, 4, 1,, DEL:DID,,, 2)
      INV:IID     = Get_1st_Element_From_Delim_Str(str_", ',', TRUE)
      IF Access:_Invoice.TryFetch(INV:PKey_IID) ~= LEVEL:Benign
         CLEAR(INV:Record)
      .
      
  
      ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info  , p:LogName)
      !   1           2       3           4          5       6          7        8            9                 10        11          12
      ! (LONG  , BYTE    , BYTE=0, BYTE=0        , ULONG=0, <LONG>  , BYTE=0  , BYTE=0    , BYTE=0           , BYTE=0  , <STRING>, <STRING>),STRING
      ! p:Option
      !   0.  - Total Charges inc.    (default)
      !   1.  - Excl VAT
      !   2.  - VAT
      !   3.  - Kgs
      !   4.  - IIDs
      ! p:Type
      !   0.  - All                   (default)
      !   1.  - Invoices                                  Total >= 0.0
      !   2.  - Credit Notes                              Total < 0.0    Instead of <, should be invoices with A_INV:CR_IID
      !   3.  - Credit Notes excl Bad Debts               Total < 0.0
      !   4.  - Credit Notes excl Journals                Total < 0.0
      !   5.  - Credit Notes excl Bad Debts & Journals    Total < 0.0
      ! p:Limit_On    &    p:ID
      !   0.  - Date [& Branch]       (default)
      !   1.  - MID
      !   2.  - DID
      ! p:Manifest_Option
      !   0.  - None                  (default)
      !   1.  - Overnight
      !   2.  - Broking
      ! p:Not_Manifest
      !   0.  - All                   (default)
      !   1.  - Not Manifest
      !   2.  - Manifest
  L_RF:Weight = DELI:Weight * (TRDI:UnitsLoaded / DELI:Units)
  L_RF:VolumetricWeight = DELI:VolumetricWeight * (TRDI:UnitsLoaded / DELI:Units)
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:Collections)
  END
  IF ~SkipDetails
    PRINT(RPT:Detail)
  END
  RETURN ReturnValue


ThisReport.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF L_DT:DID ~= DEL:DID
         L_DT:DID      = DEL:DID
         ! Total loaded items from DID on MLID   /   Total Items on DID
         L_RF:Charge   = (DEL:DocumentCharge + DEL:FuelSurcharge + DEL:Charge) * |
                         Get_ManLoadItems_Info(MAL:MLID, 1, DEL:DID) / Get_DelItem_s_Totals(DEL:DID, 3)
  
         L_RF:Charge   = L_RF:Charge * (DEL:VATRate / 100)
      .
  
  
  
      IF L_MT:TRID ~= TRDI:TRID
         !L_MT:MID                     = MAL:MID
         L_MT:TRID                    = TRDI:TRID
  
         L_MT:Delivery_Charges        = Get_Manifest_Info(MAL:MID, 4)
  
         L_MT:Cost                    = MAN:Cost * (MAN:VATRate / 100)
         L_MT:Legs_Cost               = Get_Manifest_Info(MAL:MID, 3)                         ! Legs cost
  
         L_MT:Total_Cost              = (L_MT:Legs_Cost + MAN:Cost) * (MAN:VATRate / 100)
  
         L_MT:Gross_Profit            = L_MT:Delivery_Charges - L_MT:Total_Cost
  
         L_MT:Gross_Profit_Percent    = (L_MT:Gross_Profit / L_MT:Delivery_Charges) * 100     ! %
  
  !       MESSAGE('MAL:MID: ' & MAL:MID)
  
         L_MT:Total_Weight            = Get_Manifest_Info(MAL:MID, 2)
  
         L_MT:Average_C_Per_Kg        = (L_MT:Delivery_Charges / L_MT:Total_Weight) * 100     ! in cents (not rands)
  
         EXECUTE TRI:State
            L_RF:State  = 'Loaded'
            L_RF:State  = 'On Route'
            L_RF:State  = 'Transferred'
         ELSE
            L_RF:State  = 'Loading'
      .  .
  RETURN ReturnValue

