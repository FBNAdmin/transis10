

   MEMBER('RPTTRNIS.clw')                                  ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('RPTTRNIS015.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Process the Clients File  (runs on startup)
!!! </summary>
Process_Client_Balances PROCEDURE 

LOC:Progress         LONG                                  !
LOC:Statement_Info   GROUP,PRE(L_SI)                       !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
                     END                                   !
Full_Speed           BYTE(1)                               !
LOC:Timer            LONG                                  !
LOC:ExitCode         LONG                                  !
LOC:SetTimer         LONG                                  !User changes timer
LOC:StartStopTime    LONG                                  !
LOC:StopStartTotal   DECIMAL(10,2,0.0)                     !
LOC:StartedAt        LONG                                  !
QuickWindow          WINDOW('Process Clients'),AT(,,225,78),FONT('Tahoma',8,,FONT:regular),DOUBLE,ALRT(EscKey), |
  CENTER,GRAY,MDI,HLP('Process_Client_Balances'),TIMER(150)
                       STRING(''),AT(10,7,205,10),USE(?String1),LEFT(1)
                       PROGRESS,AT(32,23,162,8),USE(?Progress1),RANGE(0,100)
                       PROMPT('You can continue working while this runs in the background.'),AT(16,40,199,10),USE(?Prompt1), |
  CENTER,TRN
                       BUTTON('&Cancel'),AT(172,60,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),DISABLE,FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       CHECK(' &Full Speed'),AT(87,62),USE(Full_Speed),SKIP,TIP('Clicking this will make the p' & |
  'rocess go faster but will also limit your ability to work until the process completes.')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

View_Clients        VIEW(Clients)
    PROJECT(CLI:CID, CLI:ClientName)
    .


Client_View     ViewManager
Measures             IKB_Stats_class

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:ExitCode)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_Client_Balances')
     LOC:StartedAt = CLOCK()
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Process_Client_Balances',QuickWindow)      ! Restore window settings from non-volatile store
      Client_View.Init(View_Clients, Relate:Clients)
      Client_View.AddSortOrder(CLI:PKey_CID)
  
  
      Client_View.Reset()
      ?Progress1{PROP:RangeHigh}  = RECORDS(Clients)
      
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      EXECUTE Full_Speed + 1
         LOC:SetTimer     = 50
         LOC:SetTimer     = 1       ! changed from 10 to 1 on 27 june 12
      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  Add_Log( 'Total time: ' & LEFT(FORMAT((CLOCK() - LOC:StartedAt) / 100, @n_12.2)) & |    
        '  (between time: ' & LEFT(FORMAT(LOC:StopStartTotal / 100, @n_12.2)) & ')', 'Process Stats - DONE @ ' & FORMAT(CLOCK(), @t4), 'Stats - Gen_Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
     BEEP(BEEP:SystemDefault)
     BEEP(BEEP:SystemDefault)
      Client_View.Kill()
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Process_Client_Balances',QuickWindow)   ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Full_Speed
          EXECUTE Full_Speed + 1
             LOC:SetTimer     = 50
             LOC:SetTimer     = 1       ! changed from 10 to 1 on 27 june 12
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
         IF LOC:StartStopTime > 0
            LOC:StopStartTotal += CLOCK() - LOC:StartStopTime
         .
      
         db.Debugout('start - ' & (CLOCK() - LOC:StartStopTime) / 100 & ' / ' & LEFT(FORMAT(LOC:StopStartTotal / 100, @n12.2)))
         Measures._Start('Process_Balances')
      
          LOC:Timer                       = QuickWindow{PROP:Timer}
          QuickWindow{PROP:Timer}         = 0
      
          IF Client_View.Next() ~= LEVEL:Benign
             LOC:ExitCode                 = 1
      
             LOC:Timer                    = 0
             POST(EVENT:CloseWindow)
          ELSE
             ?String1{PROP:Text}          = 'Client: ' & CLI:ClientName
      
             ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
             Gen_Statement(0, CLI:CID, TODAY(), CLOCK(),, TRUE, LOC:Statement_Info)
      
             LOC:Progress                += 1
             ?Progress1{PROP:Progress}    = LOC:Progress
          .
      
          IF LOC:SetTimer ~= 0 AND LOC:SetTimer ~= LOC:Timer
             LOC:Timer    = LOC:SetTimer
          .
          QuickWindow{PROP:Timer}         = LOC:Timer
      
      
         Measures._Stop('Process_Balances')
      
         db.Debugout('')
         db.Debugout('Process STATS: ' & Measures._Stats())
         Add_Log(Measures._Stats(), 'Process Stats @ ' & FORMAT(CLOCK(), @t4), 'Stats - Gen_Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
         Measures._Clear()
      
         db.Debugout('stop')
         db.Debugout('')
         LOC:StartStopTime = CLOCK()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

