TManifestTotals      GROUP,Type
Cost                   DECIMAL(10,2)                         !
MID                    ULONG                                 !Manifest ID - last used
Delivery_Charges       DECIMAL(11,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Total_Weight           DECIMAL(10,2)                         !In kg's
Gross_Profit           DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Gross_Profit_Percent   DECIMAL(6,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Average_C_Per_Kg       DECIMAL(7,2)                          !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Legs_Cost              DECIMAL(10,2)                         !
Total_Cost             DECIMAL(10,2)                         !Includes extra legs cost
Out_VAT                DECIMAL(10,2)                         !
Legs_Cost_VAT          DECIMAL(10,2)                         !
Delivery_Charges_Ex    DECIMAL(11,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel, Insurance and Additional Charges charges
                     END
