

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS004.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! String returned  - can be limited to 1 DID
!!! </summary>
Get_ManLoad_Info     PROCEDURE  (p:MID, p:Option, p:DID)   ! Declare Procedure
LOC:Info             STRING(100)                           !
Tek_Failed_File     STRING(100)

View_ML             VIEW(ManifestLoadAlias)
    .

ML_View             ViewManager


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadAlias.Open()
     .
     Access:ManifestLoadAlias.UseFile()
    ! (p:MID, p:Option, p:DID)
    !   p:Option
    !       0. Total Weight on Manifest
    !       1. Total Units on Manifest
    !       2. Total Deliveries on Manifest
    !       3. Total Deliveries Charges on Manifest

    ML_View.Init(View_ML, Relate:ManifestLoadAlias)
    ML_View.AddSortOrder(A_MAL:FKey_MID)
    !ML_View.AppendOrder()
    ML_View.AddRange(A_MAL:MID, p:MID)
    !ML_View.SetFilter()

    ML_View.Reset()

    LOOP
       IF ML_View.Next() ~= LEVEL:Benign
          BREAK
       .

       IF OMITTED(3)
          LOC:Info    += Get_ManLoadItems_Info(A_MAL:MLID, p:Option)
       ELSE
          LOC:Info    += Get_ManLoadItems_Info(A_MAL:MLID, p:Option, p:DID)
    .  .

    ML_View.Kill()
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Info)

    Exit

CloseFiles     Routine
    Relate:ManifestLoadAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! Generate an invoice or multiple invoices (DID list passed)
!!! </summary>
Gen_Invoice_s        PROCEDURE  (p:DIDs, p:BID, p:Report_Basic_Errors, p:MID, p:Inv_Date, p:Invoices_Exist) ! Declare Procedure
LOC:Idx              LONG                                  !
LOC:Log_File         STRING('Gen_Invoice {9}')             !
LOC:DIs_Already_Invoiced STRING(500)                       !
LOC:Extraction_Groyp GROUP,PRE()                           !
LOC:Orig_DIDs        STRING(1000)                          !
LOC:DID              ULONG                                 !Delivery ID
LOC:DID_No           LONG                                  !
LOC:Fatal_Errors     LONG                                  !
LOC:Item_Errors      LONG                                  !
LOC:Other_Errors     LONG                                  !
LOC:Invoice_Exists   LONG                                  !
LOC:Committed        BYTE                                  !was the tran commited
                     END                                   !
LOC:Invoice_DIDs_Q   QUEUE,PRE(L_IQ)                       !
DID                  ULONG                                 !Delivery ID
DINo                 ULONG                                 !Delivery Instruction Number
                     END                                   !
LOC:InvoiceItems_Group GROUP,PRE(L_IG)                     !
Weight               DECIMAL(10,2)                         !In kg's
VolumetricWeight     DECIMAL(10,2)                         !Weight based on Volumetric calculation (in kgs)
Volume               DECIMAL(10,2)                         !Volume for manual entry (metres cubed)
No_Items             ULONG                                 !
Units                ULONG                                 !
                     END                                   !
LOC:Overall_Group    GROUP,PRE(L_OG)                       !
DocumentCharge       DECIMAL(10,2)                         !Document Charge applied to this DI
FuelSurcharge        DECIMAL(10,2)                         !Fuel Surcharge applied to this DI
TollCharge           DECIMAL(11,2)                         !
Charge               DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Insurance            DECIMAL(10,2)                         !Charge for the DI- Excludes VAT, Docs, Fuel and Insurance charges
Deliveries           ULONG                                 !
Composition_Primed   BYTE                                  !
                     END                                   !
LOC:MIDs             STRING(100)                           !List of Manifest IDs that the delivery is currently manifested on
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_InvoiceItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_InvoiceComposition.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:AddressAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Add_Suburbs.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Commodities.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Delivery_CODAddresses.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Clients.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Branches.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:PackagingTypes.Open()
     .
     Access:_Invoice.UseFile()
     Access:_InvoiceItems.UseFile()
     Access:_InvoiceComposition.UseFile()
     Access:DeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
     Access:AddressAlias.UseFile()
     Access:Add_Suburbs.UseFile()
     Access:Commodities.UseFile()
     Access:Delivery_CODAddresses.UseFile()
     Access:Clients.UseFile()
     Access:Branches.UseFile()
     Access:PackagingTypes.UseFile()
    ! Notes:
    !   Errors are +
    !   Nothing done is -1
    !
    !   Previous invoies for the Delivery notes are checked for

    !   Added Audit entries for management / admin to check, there are also log entries but these
    !   are not integrated into the app. so I decided to add Audit entries.  Will keep log entries also.

    !   Note - audits are only done on Invoice errors, not where an error such as no DIDs to invoice occurs
    ! ----------------------------   Load a Q of all the DIDs to Invoice
    ! (p:DIDs, p:BID, p:Report_Basic_Errors, p:MID, p:Inv_Date)
    ! (STRING, ULONG=0, BYTE=1, ULONG=0, LONG=0),LONG,PROC
    !
    ! Generate an invoice from the p:DIDs

    IF p:Inv_Date = 0
       p:Inv_Date       = TODAY()
    .

    IF p:BID = 0
       p:BID            = GLO:BranchID
    .

    LOC:Orig_DIDs       = CLIP(p:DIDs)
    LOOP
       IF CLIP(p:DIDs) = ''
          BREAK
       .

       LOC:DID_No      += 1

       LOC:DID          = Get_1st_Element_From_Delim_Str(p:DIDs, ',', TRUE)
       IF LOC:DID <= 0
          LOC:Fatal_Errors   += 1
          Add_Log('DID is zero?  Entry: ' & LOC:DID_No & ', List: ' & CLIP(LOC:Orig_DIDs), 'DID', CLIP(LOC:Log_File))
          MESSAGE('DID is zero.||Aborting Invoice generation.', 'Invoice Generation', ICON:Hand)
          BREAK
       .


       A_DEL:DID        = LOC:DID
       IF Access:DeliveriesAlias.TryFetch(A_DEL:PKey_DID) ~= LEVEL:Benign
          LOC:Fatal_Errors   += 1
          Add_Log('DID not found!  DID: ' & LOC:DID & ',  Entry: ' & LOC:DID_No & ', List: ' & CLIP(LOC:Orig_DIDs), 'DID', CLIP(LOC:Log_File))

          ! (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel)
          Add_Audit(1, 'Generating Invoices (Gen_Invoice_s)', 'DID not found!  DID: ' & LOC:DID & ',  Entry: ' & LOC:DID_No, 'DID List: ' & CLIP(LOC:Orig_DIDs))

          MESSAGE('DID was not found - DID: ' & LOC:DID & '||Aborting Invoice generation.', 'Invoice Generation', ICON:Hand)
          BREAK
       .

       ! We have the Delivery Alias
       ! Check that we can Invoice it?  What could we check?  They are allowed to invoice on DI creation.

       L_IQ:DID         = LOC:DID
       L_IQ:DINo        = A_DEL:DINo
       ADD(LOC:Invoice_DIDs_Q)
    .
    ! ----------------------------   Loop through Q checking for existing Invoices for the Deliveries
    IF LOC:Fatal_Errors = 0
       SORT(LOC:Invoice_DIDs_Q, L_IQ:DID)

       LOC:Idx           = 0
       LOOP
          LOC:Idx       += 1
          GET(LOC:Invoice_DIDs_Q, LOC:Idx)
          IF ERRORCODE()
             BREAK
          .

          INV:DID        = L_IQ:DID
          IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
             IF L_IQ:DINo ~= INV:DINo
                LOC:Fatal_Errors   += 1

                Add_Log('DI No. ' & INV:DINo & ' on existing Invoice ' & INV:IID & ' for DID ' & L_IQ:DID & ' differs to Delivery Ins. DI No. ' & L_IQ:DINo, 'Gen_Invocies_s')
                Add_Audit(1, 'Generating Invoices (Gen_Invoice_s)', 'Existing Invoice - DI No. ' & INV:DINo & ' on existing Invoice ' & INV:IID & ' for DID ' & L_IQ:DID, 'Differs to Delivery Ins. DI No. ' & L_IQ:DINo)

                MESSAGE('Existing Invoice for DI No. ' & L_IQ:DINo & ' has a different DI No. reflected!||Please write down this DI No. and the Invocie No. ' & INV:IID & ' and report this to support.', 'Generate Invocies', ICON:Hand)
             ELSE
                Add_Audit(2, 'Generating Invoices (Gen_Invoice_s)', 'Existing Invoice for DI No. ' & INV:DINo & ', existing Invoice ' & INV:IID & ', DID ' & L_IQ:DID, '')
             .

             IF L_IQ:DINo = INV:DINo AND p:MID ~= 0               ! Check if we have it in a list
                ! (p:Delim_List, p:Delim, p:Element, p:Chop_Out, p:Spaced)
                LOC:MIDs    = INV:MIDs

                IF Match_Element_In_Delim_Str(LOC:MIDs, ',', p:MID) > 0
                ELSE
                   Add_Log('DI No. ' & INV:DINo & ' on existing Invoice ' & INV:IID & ' for DID ' & L_IQ:DID & ' does not reflect this MID, Inv. MIDs ' & CLIP(INV:MIDs) & ',  generating MID ' & p:MID, 'Gen_Invocies_s')
                   Add_Audit(5, 'Generating Invoices (Gen_Invoice_s)', 'Existing Invoice - DI No. ' & INV:DINo & ' on existing Invoice ' & INV:IID & ' for DID ' & L_IQ:DID & ' does not reflect this MID', 'Inv. MIDs ' & CLIP(INV:MIDs) & ',  generating MID ' & p:MID)

                   MESSAGE('Existing Invoice for DI No. ' & L_IQ:DINo & ' does not reflect this MID.|Inv. MIDs ' & CLIP(INV:MIDs) & ',  generating MID ' & p:MID &'||This is a warning only, if you have a DI which is NOT multiply manifested you may want to fix this.', 'Generate Invocies', ICON:Asterisk)
             .  .

             LOC:Invoice_Exists += 1

             ! We have invoiced this Delivery already - make a list
             IF CLIP(LOC:DIs_Already_Invoiced) = ''
                LOC:DIs_Already_Invoiced    = 'DI No. ' & L_IQ:DINo & ' on invoice ' & INV:IID
             ELSE
                LOC:DIs_Already_Invoiced    = CLIP(LOC:DIs_Already_Invoiced) & '|DI No. ' & L_IQ:DINo & ' on invoice ' & INV:IID
             .

             DELETE(LOC:Invoice_DIDs_Q)
       .  .
    
       IF p:Report_Basic_Errors = TRUE AND CLIP(LOC:DIs_Already_Invoiced) ~= ''
          Add_Log(CLIP(LOC:DIs_Already_Invoiced), 'DIDs Already Invoiced', LOC:Log_File)
          MESSAGE('The following Deliveries already have Invoices:||' & CLIP(LOC:DIs_Already_Invoiced), 'Invoice Generation', ICON:Exclamation)
    .  .
    ! ----------------------------   Loop through Q totalling and creating Invoices, Invoice Items and Conpositions
    IF LOC:Fatal_Errors = 0
       IF RECORDS(LOC:Invoice_DIDs_Q) <= 0
          LOC:Fatal_Errors         = -1
          IF p:Report_Basic_Errors = TRUE
             MESSAGE('There were no DIs to Invoice.', 'Gen_Invoice', ICON:Hand)
          .
       ELSE
          LOGOUT(1, _InvoiceComposition, _Invoice, _InvoiceItems)
          IF ERRORCODE()
             LOC:Fatal_Errors     += 1

             Add_Audit(1, 'Generating Invoices (Gen_Invoice_s)', 'Failed to logout tables for Invoice addition! Nothing Invoiced.', 'DID list: ' & CLIP(LOC:Orig_DIDs))

             MESSAGE('Failed to logout tables for Invoice addition!|Nothing will be Invoiced.||Error: ' & ERROR() & '||File Error: ' & FILEERROR(), 'Gen_Invoice - Failed Logout', ICON:Hand)
          ELSE
             SORT(LOC:Invoice_DIDs_Q, L_IQ:DID)

             ! +++++++++++++ Create the Invoice Composition - loop through the Invoices created above
             CLEAR(INCO:Record)
             IF Access:_InvoiceComposition.TryPrimeAutoInc() ~= LEVEL:Benign
                LOC:Fatal_Errors += 1

                Add_Audit(1, 'Generating Invoices (Gen_Invoice_s)', 'Failed to Prime Invoice Composition! Nothing Invoiced.', 'DID list: ' & CLIP(LOC:Orig_DIDs))

                MESSAGE('Failed to Prime Invoice Composition record!||Note invoices will not be added.||G Err: ' & Access:_InvoiceComposition.GetError(), 'Gen_Invoice', ICON:Hand)
             ELSE
                LOC:Idx  = 0
                LOOP
                   LOC:Idx      += 1
                   GET(LOC:Invoice_DIDs_Q, LOC:Idx)
                   IF ERRORCODE()
                      BREAK
                   .

                   CLEAR(LOC:InvoiceItems_Group)

                   A_DEL:DID     = L_IQ:DID
                   IF Access:DeliveriesAlias.TryFetch(A_DEL:PKey_DID) ~= LEVEL:Benign
                      LOC:Fatal_Errors += 1

                      Add_Audit(1, 'Generating Invoices (Gen_Invoice_s)', 'Failed to get DID record! DID ' & L_IQ:DID & ' will not be invoiced.', 'DID list: ' & CLIP(LOC:Orig_DIDs))

                      MESSAGE('Failed to get DID record (2)!||DID: ' & L_IQ:DID, 'Gen_Invoice', ICON:Hand)
                      BREAK
                   .

                   ! +++++++++++++ Create the Invoice
                   DO Create_Invoice

                   ! Total the Items on a Delivery
                   CLEAR(A_DELI:Record, -1)
                   A_DELI:DID    = L_IQ:DID
                   SET(A_DELI:FKey_DID_ItemNo, A_DELI:FKey_DID_ItemNo)
                   LOOP
                      IF Access:DeliveryItemAlias.TryNext() ~= LEVEL:Benign
                         BREAK
                      .
                      IF A_DELI:DID ~= L_IQ:DID
                         BREAK
                      .

                      ! +++++++++++++ Create an Invoice Item
                      CLEAR(INI:Record)
                      IF Access:_InvoiceItems.TryPrimeAutoInc() ~= LEVEL:Benign
                         LOC:Item_Errors += 1
                         MESSAGE('Failed to prime Invoice Item!||G Err: ' & Access:_InvoiceItems.GetError(), 'Gen_Invoice', ICON:Hand)
                         BREAK
                      ELSE
                         !INI:ITID
                         INI:IID            = INV:IID
                         INI:DIID           = A_DELI:DIID
                         INI:Type           = A_DELI:Type
                         INI:CMID           = A_DELI:CMID
                         INI:ItemNo         = A_DELI:ItemNo

                         COM:CMID           = A_DELI:CMID
                         IF Access:Commodities.TryFetch(COM:PKey_CMID) = LEVEL:Benign
                            INI:Commodity   = COM:Commodity
                         .

                         PACK:PTID          = A_DELI:PTID
                         IF Access:PackagingTypes.TryFetch(PACK:PKey_PTID) = LEVEL:Benign
                            INI:Description = PACK:Packaging
                         .

                         ! Get Item info - Packaging / Container no.
                         !IF A_DELI:Type = 0 OR CLIP(A_DELI:ContainerNo) ~= '' OR CLIP(A_DELI:ContainerVessel) ~= '' OR |
                         !                   CLIP(A_DELI:SealNo) ~= ''
                         ! New feature A_DELI:ShowOnInvoice
                         IF A_DELI:ShowOnInvoice = 1 AND (CLIP(A_DELI:ContainerNo) ~= '' OR CLIP(A_DELI:SealNo) ~= '' OR CLIP(A_DELI:ContainerVessel) ~= '')
                            IF LEN(CLIP(A_DELI:ContainerNo) & ', S ' & CLIP(A_DELI:SealNo) & ', ' & CLIP(A_DELI:ContainerVessel)) > 40
                               INI:ContainerDescription = 'Cntr ' & CLIP(A_DELI:ContainerNo) & ', S ' & CLIP(A_DELI:SealNo)  & |
                                                          ', ' & CLIP(A_DELI:ContainerVessel)
                            ELSE
                               INI:ContainerDescription = 'Container ' & CLIP(A_DELI:ContainerNo) & ', S ' & CLIP(A_DELI:SealNo)  & |
                                                          ', ' & CLIP(A_DELI:ContainerVessel)
                         .  .

                         INI:Units          = A_DELI:Units

                         ! +++++++++++++ Insert the Invoice Item
                         IF Access:_InvoiceItems.TryInsert() ~= LEVEL:Benign
                            LOC:Item_Errors    += 1
                            MESSAGE('Failed to insert Invoice Item!||G Err: ' & Access:_InvoiceItems.GetError(), 'Gen_Invoice', ICON:Hand)
                            BREAK
                      .  .

                      L_IG:Weight           += A_DELI:Weight
                      L_IG:VolumetricWeight += A_DELI:VolumetricWeight
                      L_IG:Volume           += A_DELI:Volume
                      L_IG:Units            += A_DELI:Units
                      L_IG:No_Items         += 1
                   .
                   IF LOC:Fatal_Errors > 0
                      BREAK
                   .


                   ! +++++++++++++ Insert the Invoice
                   INV:Weight                = L_IG:Weight
                   INV:VolumetricWeight      = L_IG:VolumetricWeight
                   INV:Volume                = L_IG:Volume
                   IF Access:_Invoice.TryInsert() ~= LEVEL:Benign
                      LOC:Fatal_Errors += 1

                      Add_Audit(1, 'Generating Invoices (Gen_Invoice_s)', 'Failed to Insert Invoice! DID ' & L_IQ:DID & ' will not be invoiced.', 'DID list: ' & CLIP(LOC:Orig_DIDs))

                      MESSAGE('Failed to Insert the Invoice!||G Err: ' & Access:_Invoice.GetError(), 'Gen_Invoice', ICON:Hand)
                      BREAK
                   .


                   ! Total the Composition - although we don't use these yet.... ??
                   L_OG:DocumentCharge      += A_DEL:DocumentCharge
                   L_OG:FuelSurcharge       += A_DEL:FuelSurcharge
                   L_OG:TollCharge          += A_DEL:TollCharge
                   L_OG:Charge              += A_DEL:Charge
                   L_OG:Insurance           += INV:Insurance
                   L_OG:Deliveries          += 1
                .

                IF LOC:Fatal_Errors = 0
                   ! +++++++++++++ Insert the Invoice Composition
                   IF Access:_InvoiceComposition.TryInsert() ~= LEVEL:Benign
                      LOC:Other_Errors += 1
                      MESSAGE('Failed to Insert the Invoice Composition!||G Err: ' & Access:_InvoiceComposition.GetError(), 'Gen_Invoice', ICON:Hand)
                   .
                ELSE
                   ! Errors.......??  Reported already
             .  .

             IF (LOC:Fatal_Errors + LOC:Item_Errors + LOC:Other_Errors) > 0
                ROLLBACK
                IF ERRORCODE()
                   Add_Audit(1, 'Generating Invoices (Gen_Invoice_s)', 'Rollback failed!', 'DID list: ' & CLIP(LOC:Orig_DIDs))

                   Add_Log('Rollback failed!  Error: ' & CLIP(ERROR()) & ' - ' & CLIP(FILEERROR()), 'Rollback', CLIP(LOC:Log_File))
                   MESSAGE('Serious Error.  Rollback Failed.||Error: ' & ERROR() & '|File Error: ' & FILEERROR(), 'Gen_Invoice', ICON:Hand)
                ELSE
                   Add_Audit(1, 'Generating Invoices (Gen_Invoice_s)', 'Invoice addition Rolledback!  Nothing added.', 'DID list: ' & CLIP(LOC:Orig_DIDs))
                .
             ELSE
                COMMIT
                IF ~ERRORCODE()
                   LOC:Committed    = TRUE
                ELSE
                   ROLLBACK
                   IF ERRORCODE()
                      Add_Audit(1, 'Generating Invoices (Gen_Invoice_s)', 'Commit failed - Rollback failed!', 'DID list: ' & CLIP(LOC:Orig_DIDs))
                      Add_Log('Commit - Rollback failed!  Error: ' & CLIP(ERROR()) & ' - ' & CLIP(FILEERROR()), 'Rollback', CLIP(LOC:Log_File))
                      MESSAGE('Serious Error.  Commit - Rollback Failed.||Error: ' & ERROR() & '|File Error: ' & FILEERROR(), 'Gen_Invoice', ICON:Hand)
                   ELSE
                      Add_Audit(1, 'Generating Invoices (Gen_Invoice_s)', 'Commit failed - Invoice addition Rolledback!  Nothing added.', 'DID list: ' & CLIP(LOC:Orig_DIDs))
    .  .  .  .  .  .
    ! Return code
    ! LOC:Fatal_Errors
    !   > 0         - nothing done
    !   -1          - nothing to do, no DIs

    IF ~OMITTED(6)
       p:Invoices_Exist = LOC:Invoice_Exists
    .

    IF LOC:Committed = TRUE
       IF LOC:Fatal_Errors <> 0
          ! We committed and have fatal errors??
          Add_Log('Fatal Errors but a committed transaction.  DID List: ' & CLIP(LOC:Orig_DIDs), 'Gen_Invocies_s')

          Add_Audit(1, 'Generating Invoices (Gen_Invoice_s)', 'Fatal Errors but a committed transaction.', 'DID List: ' & CLIP(LOC:Orig_DIDs))

          MESSAGE('Fatal Errors but a committed transaction!||Please report this to support.', 'Generate Invocies', ICON:Hand)
       .

       LOC:Fatal_Errors = 0
    ELSE
       IF LOC:Fatal_Errors = 0
          LOC:Fatal_Errors  = LOC:Item_Errors + LOC:Other_Errors
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Fatal_Errors)

    Exit

CloseFiles     Routine
    Relate:_Invoice.Close()
    Relate:_InvoiceItems.Close()
    Relate:_InvoiceComposition.Close()
    Relate:DeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Relate:AddressAlias.Close()
    Relate:Add_Suburbs.Close()
    Relate:Commodities.Close()
    Relate:Delivery_CODAddresses.Close()
    Relate:Clients.Close()
    Relate:Branches.Close()
    Relate:PackagingTypes.Close()
    Exit
Create_Invoice                                  ROUTINE
    ! We have the Delivery loaded
    CLEAR(INV:Record)
    IF Access:_Invoice.PrimeRecord() ~= LEVEL:Benign            !PrimeAutoInc() ~= LEVEL:Benign
       Access:_Invoice.CancelAutoInc()
       LOC:Fatal_Errors += 1
       Add_Audit(1, 'Generating Invoices (Gen_Invoice_s)', 'Failed to Prime Invoice!  DID ' & A_DEL:DID & ' will not be Invoiced' , 'DID list: ' & CLIP(LOC:Orig_DIDs))
       MESSAGE('Failed to create an Invocie entry!||G Err: ' & Access:_Invoice.GetError(), 'Gen_Invoice_s - Failed 2', ICON:Hand)
    ELSE
       !INV:IID                                         - primed
       INV:POD_IID              = INV:IID                           ! POD is same as Invoice no.

       INV:BID                  = p:BID
       BRA:BID                  = p:BID
       IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
          INV:BranchName        = BRA:BranchName
       .

       INV:CID                  = A_DEL:CID
       INV:DID                  = A_DEL:DID
       INV:DINo                 = A_DEL:DINo
       INV:Terms                = A_DEL:Terms
       INV:ICID                 = INCO:ICID

       INV:UID                  = GLO:UID

       !LOC:MIDs                 = INV:MIDs                         ! why was this here?  6/2/10
       Junk_#                   = Get_Delivery_ManIDs(A_DEL:DID, 0, LOC:MIDs, 1)
       INV:MIDs                 = LOC:MIDs                          ! should have been this way around and here instead?   6/2/10

       INV:MID                  = p:MID                             ! Generating MID - if there is one

       INV:ClientReference      = A_DEL:ClientReference

       ! Client Address
       IF A_DEL:Terms < 2 AND A_DEL:DC_ID ~= 0
          DO Client_Address_From_COD
       ELSE
          DO Client_Address_From_General
       .

       ! Col - Del - Addresses
       DO Addresses_From_General

       INV:InvoiceDate          = p:Inv_Date
       INV:InvoiceTime          = CLOCK()

       IF A_DEL:Insure = TRUE
          INV:Insurance         = A_DEL:TotalConsignmentValue * (A_DEL:InsuranceRate / 100)
       .
       INV:Documentation        = A_DEL:DocumentCharge
       INV:FuelSurcharge        = A_DEL:FuelSurcharge
       INV:TollCharge           = A_DEL:TollCharge
       INV:FreightCharge        = A_DEL:Charge
       INV:AdditionalCharge     = A_DEL:AdditionalCharge            ! New


       ! VAT rate from DI, not current rate!
       INV:VATRate              = A_DEL:VATRate

      ! Current VAT rate at time of invoicing
       !INV:VATRate              = Get_Setup_Info(4)
 
       INV:VAT                  = (INV:Insurance + INV:Documentation + INV:FuelSurcharge + INV:TollCharge + INV:FreightCharge + INV:AdditionalCharge) * (INV:VATRate / 100)

       INV:Total                = INV:VAT + (INV:Insurance + INV:Documentation + INV:FuelSurcharge + INV:TollCharge + INV:FreightCharge + INV:AdditionalCharge)

       IF INV:Total = 0.0
          MESSAGE('The invoice no. ' & INV:IID & ' has zero amount!', 'Invoice Generation', ICON:Exclamation)
    .  .

    EXIT
Addresses_From_General                  ROUTINE
    A_ADD:AID                = A_DEL:CollectionAID
    IF Access:AddressAlias.TryFetch(A_ADD:PKey_AID) = LEVEL:Benign
       INV:ShipperName       = A_ADD:AddressName
       INV:ShipperLine1      = A_ADD:Line1
       INV:ShipperLine2      = A_ADD:Line2

       SUBU:SUID             = A_ADD:SUID
       IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) = LEVEL:Benign
          INV:ShipperSuburb     = SUBU:Suburb
          INV:ShipperPostalCode = SUBU:PostalCode
    .  .

    A_ADD:AID                = A_DEL:DeliveryAID
    IF Access:AddressAlias.TryFetch(A_ADD:PKey_AID) = LEVEL:Benign
       INV:ConsigneeName     = A_ADD:AddressName
       INV:ConsigneeLine1    = A_ADD:Line1
       INV:ConsigneeLine2    = A_ADD:Line2
       SUBU:SUID             = A_ADD:SUID
       IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) = LEVEL:Benign
          INV:ConsigneeSuburb     = SUBU:Suburb
          INV:ConsigneePostalCode = SUBU:PostalCode
    .  .
    EXIT
Client_Address_From_General           ROUTINE
    CLI:CID                 = INV:CID
    IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
       INV:ClientName       = CLI:ClientName
       INV:ClientNo         = CLI:ClientNo
       INV:VATNo            = CLI:VATNo
                         
       INV:InvoiceMessage   = CLI:InvoiceMessage

       A_ADD:AID            = CLI:AID

       IF Access:AddressAlias.TryFetch(A_ADD:PKey_AID) = LEVEL:Benign
          INV:ClientLine1   = A_ADD:Line1
          INV:ClientLine2   = A_ADD:Line2

          SUBU:SUID         = A_ADD:SUID
          IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) = LEVEL:Benign
             INV:ClientSuburb     = SUBU:Suburb
             INV:ClientPostalCode = SUBU:PostalCode
    .  .  .
    EXIT
Client_Address_From_COD               ROUTINE
!    message('using DC ID: ' & A_DEL:DC_ID & '||from DID: ' & A_DEL:DID)
    CLI:CID                 = INV:CID
    IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
       INV:ClientNo         = CLI:ClientNo                  ! COD or Pre-Paid account?
    .

    INV:DC_ID               = A_DEL:DC_ID

    DCADD:DC_ID             = A_DEL:DC_ID
    IF Access:Delivery_CODAddresses.TryFetch(DCADD:PKey_DC_ID) = LEVEL:Benign
       INV:ClientName       = DCADD:AddressName
       INV:VATNo            = DCADD:VATNo

       INV:ClientLine1      = DCADD:Line1
       INV:ClientLine2      = DCADD:Line2
       INV:ClientSuburb     = DCADD:Line3
       INV:ClientPostalCode = DCADD:Line4
    ELSE
       MESSAGE('Delivery COD Address is missing - DC_ID: ' & A_DEL:DC_ID, 'Gen_Invoice_s', ICON:Hand)
    .
    EXIT
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Setup_Info       PROCEDURE  (p:Option)                 ! Declare Procedure
LOC:Returned         STRING(100)                           !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Setup.Open()
     .
     Access:Setup.UseFile()
    ! (p:Option)
    SET(SET:PKey_SID)
    IF Access:Setup.TryNext() = LEVEL:Benign
       EXECUTE p:Option
          LOC:Returned     = '<invalid>'
          LOC:Returned     = '<invalid>'
          LOC:Returned     = '<invalid>'            !SET:GeneralRatesClientID
          LOC:Returned     = SET:VATRate
          LOC:Returned     = '<invalid>'            !SET:GeneralRatesTransporterID          5
          LOC:Returned     = SET:TestDatabase
       ELSE
          MESSAGE('Option not known.||Option: ' & p:Option, 'Get_Setup_Info', ICON:Hand)
       .
    ELSE
       MESSAGE('No Setup record available.||Please configure one.||Option: ' & p:Option, 'Get_Setup_Info', ICON:Hand)
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Returned)

    Exit

CloseFiles     Routine
    Relate:Setup.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Gen_Man_Invs         PROCEDURE  (p:MID, p:Inv_Date)        ! Declare Procedure
LOC:DIDs_Q           QUEUE,PRE(L_DQ)                       !
DID                  ULONG                                 !Delivery ID
                     END                                   !
LOC:Result           LONG                                  !
LOC:Idx              LONG                                  !
LOC:Count_Invoiced   LONG                                  !
LOC:Inv_Result       LONG                                  !
LOC:Errors           LONG                                  !
LOC:Not_Created      LONG                                  !
LOC:Invoices_Exist   LONG                                  !
Tek_Failed_File     STRING(100)

MAN_Del     VIEW(ManifestLoadAlias)
    PROJECT(A_MAL:MLID, A_MAL:MID)
       JOIN(A_MALD:FSKey_MLID_DIID, A_MAL:MLID)
       PROJECT(A_MALD:MLID, A_MALD:DIID)
          JOIN(A_DELI:PKey_DIID, A_MALD:DIID)
          PROJECT(A_DELI:DIID, A_DELI:DID)
             JOIN(A_DEL:PKey_DID, A_DELI:DID)
             PROJECT(A_DEL:DID, A_DEL:DINo, A_DEL:DIDate)
    .  .  .  .



Del_Man     ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
     Access:ManifestLoadAlias.UseFile()
     Access:ManifestLoadDeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
     Access:DeliveriesAlias.UseFile()
    ! (p:MID, p:Inv_Date)
    !BIND('A_MAL:MID', A_MAL:MID)

    Del_Man.Init(MAN_Del, Relate:ManifestLoadAlias)
    Del_Man.AddSortOrder(A_MAL:FKey_MID)
    Del_Man.AppendOrder('A_DEL:DID,A_DELI:DIID')
    Del_Man.AddRange(A_MAL:MID, p:MID)
    !Del_Man.SetFilter()

    Del_Man.Reset()
    LOOP
       IF Del_Man.Next() ~= LEVEL:Benign
          BREAK
       .

       ! We will have every Item entry listed when we really just want the DIDs.
       ! DID will not be unique
       L_DQ:DID     = A_DEL:DID
       GET(LOC:DIDs_Q, L_DQ:DID)
       IF ERRORCODE()
          L_DQ:DID  = A_DEL:DID
          ADD(LOC:DIDs_Q)
    .  .
    Del_Man.Kill()


    IF RECORDS(LOC:DIDs_Q) > 0
       LOC:Idx      = 0
       LOOP
          LOC:Idx  += 1
          GET(LOC:DIDs_Q, LOC:Idx)
          IF ERRORCODE()
             BREAK
          .

          LOC:Invoices_Exist    = 0

          ! (p:DIDs, p:BID, p:Report_Basic_Errors, p:MID, p:Inv_Date)
          LOC:Inv_Result         = Gen_Invoice_s(L_DQ:DID,, 0, p:MID, p:Inv_Date, LOC:Invoices_Exist)
          LOC:Not_Created       += LOC:Invoices_Exist

          IF LOC:Inv_Result ~= 0            ! Don't report basic errors
             IF LOC:Inv_Result > 0
                LOC:Errors      += 1
                LOC:Result      -= 1
             ELSIF LOC:Inv_Result < 0
                ! Nothing to invoice, existing for example, or blank DID passed
             .

             ! Then there was an error - which has been reported to the user
          ELSE
             LOC:Count_Invoiced += 1
             ! Alls fine
       .  .

       IF LOC:Errors > 0
          MESSAGE(LOC:Not_Created & ' Deliveries were not invoiced.|' & LOC:Count_Invoiced & ' were invoiced.||' & LOC:Not_Created & ' had existing invoices.|' & LOC:Errors & ' had errors.', 'Manifest Invoicing', ICON:Exclamation)
       ELSE
          IF LOC:Not_Created > 0
             MESSAGE(LOC:Not_Created & ' Deliveries were not invoiced, they have existing invoices.|' & LOC:Count_Invoiced & ' were invoiced.', 'Manifest Invoicing', ICON:Exclamation)
          ELSE
             MESSAGE(LOC:Count_Invoiced & ' were invoiced.', 'Manifest Invoicing', ICON:Asterisk)
       .  .
    ELSE
       LOC:Result   -= 1
       MESSAGE('There were no Deliveries for the specified Manifest - MID: ' & p:MID, 'Gen_Man_Invs_and_DNs', ICON:Hand)
    .


    IF LOC:Count_Invoiced <= 0
            ! This must generate regardless!
       !CASE MESSAGE('There were no Debtor Invoices generated for this Manifest.||Would you like to generate a Transporter Invoice now?', 'Generate Manifest Invoices', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
       !OF BUTTON:Yes
       !   Gen_Transporter_Invoice(p:MID)
       !.
    ELSE
       !Gen_Transporter_Invoice(p:MID)
    .


    ! Note we no longer allow Manifests to change status when there are invoicing errors
    IF LOC:Result >= 0
       Gen_Transporter_Invoice(p:MID,, p:Inv_Date)
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:ManifestLoadAlias.Close()
    Relate:ManifestLoadDeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Relate:DeliveriesAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! from Manifest  (and extra leg transporter invoices)
!!! </summary>
Gen_Transporter_Invoice PROCEDURE  (p:MID, p:Option, p:Inv_Date) ! Declare Procedure
LOC:Result           LONG                                  !
LOC:DIs_Q            QUEUE,PRE(L_DQ)                       !Q of already created Invoices for these DI Legs
DID                  ULONG                                 !Delivery ID
                     END                                   !
LOC:DIs              STRING(1000)                          !
LOC:DID              ULONG                                 !Delivery ID
LOC:Replace          BYTE                                  !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_InvoiceTransporter.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryLegs.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
     Access:_InvoiceTransporter.UseFile()
     Access:ManifestAlias.UseFile()
     Access:DeliveryLegs.UseFile()
     Access:DeliveriesAlias.UseFile()
    ! (p:MID, p:Option, p:Inv_Date)
    ! (ULONG, BYTE=0, LONG),LONG,PROC
    ! p:Option
    !   0   - Ask if exists
    !   1   - Dont ask - Dont re-create
    !   2   - Dont ask - Re-create

    ! As called from Gen_Man_Inv:  Gen_Transporter_Invoice(p:MID,, p:Inv_Date)
    
    A_MAN:MID   = p:MID
    IF Access:ManifestAlias.TryFetch(A_MAN:PKey_MID) = LEVEL:Benign
       LOC:Replace              = TRUE

       CLEAR(INT:Record,-1)
       INT:MID                  = p:MID
       INT:Manifest             = 1
       IF Access:_InvoiceTransporter.TryFetch(INT:SKey_MID_Manifest) = LEVEL:Benign
          LOC:Replace           = FALSE
          CASE p:Option
          OF 1
             LOC:Replace        = 0
          OF 2
             LOC:Replace        = 2
          ELSE      !IF p:Option = 0
             CASE MESSAGE('There is already a Transporter Invoice for this Manifest - MID ' & p:MID & ' and TID ' & INT:TIN & '.||Would you like to replace this Invcoice?', 'Generate Transporter Invoice', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
             OF BUTTON:Yes
                LOC:Replace     = 2
                Add_Log('User: ' & GLO:Login & '  chose to replace Transporter invoice: ' & INT:TIN & '  when prompted that one existed already.','Gen_Transporter_Invoice', 'Audit_Transporter_Invoice.log')
             ELSE
                Add_Log('User: ' & GLO:Login & '  chose NOT to replace Transporter invoice: ' & INT:TIN & '  when prompted that one existed already.','Gen_Transporter_Invoice', 'Audit_Transporter_Invoice.log')
       .  .  .

       IF LOC:Replace = 2
          db.debugout('[Gen_Tran._Inv]  Deleting Inv Trans - INT:TIN: ' & INT:TIN)
          IF Relate:_InvoiceTransporter.Delete(0) ~= LEVEL:Benign                   ! Problem ???
             MESSAGE('Could not delete the existing Manifest Transporter Invoice!||MID ' & p:MID & ' and TID ' & INT:TIN, 'Gen_Trans._Invoice', ICON:Hand)
             Add_Log('User: ' & GLO:Login & '  replacement of Transporter invoice: ' & INT:TIN & '  FAILED due to delete error.','Gen_Transporter_Invoice', 'Audit_Transporter_Invoice.log')
       .  .

       IF LOC:Replace > 0
          ! Generate a Transporter Invoice - if one does not exist for this MID
          INT:MID                  = p:MID
          INT:Manifest             = 1
          IF Access:_InvoiceTransporter.TryFetch(INT:SKey_MID_Manifest) ~= LEVEL:Benign

!          INT:MID                  = p:MID
!          INT:DID                  = 0
!          IF Access:_InvoiceTransporter.TryFetch(INT:SKey_MID_DID) ~= LEVEL:Benign
             CLEAR(INT:Record)

             IF Access:_InvoiceTransporter.TryPrimeAutoInc() = LEVEL:Benign
                !INT:TIN
                INT:TID            = A_MAN:TID
                INT:BID            = A_MAN:BID
                INT:MID            = A_MAN:MID
                INT:JID            = A_MAN:JID

                INT:Cost           = A_MAN:Cost
                INT:VAT            = A_MAN:Cost * (A_MAN:VATRate / 100)

                INT:Rate           = A_MAN:Rate
                INT:VATRate        = A_MAN:VATRate

                INT:CreatedDate    = TODAY()                ! Changed 12/02/14  from A_MAN:CreatedDate
                INT:CreatedTime    = CLOCK()                ! A_MAN:CreatedTime - agreed on call with Y

                INT:UID            = GLO:UID

                INT:Broking        = A_MAN:Broking

                INT:InvoiceDate    = p:Inv_Date             ! Generate the invoice with Manifest date not - TODAY()
                INT:InvoiceTime    = CLOCK()

                INT:Manifest       = 1                      ! This is the Manifest Invoice

                IF Access:_InvoiceTransporter.TryInsert() ~= LEVEL:Benign
                   Access:_InvoiceTransporter.CancelAutoInc()
                   LOC:Result      = -4
                ELSE
                   ! Created
                .
             ELSE
                LOC:Result         = -3
             .
          ELSE
             ! Found already!
             LOC:Result            = -2
             MESSAGE('There is an existing Manifest Transporter Invoice!||MID ' & p:MID & ' and TID ' & INT:TIN, 'Gen_Trans._Invoice', ICON:Hand)
       .  .

       IF LOC:Result >= 0            ! If we have updated the Manifest Transporter Invoice
          DO Leg_Invoices
       .
    ELSE
       LOC:Result               = -1
       MESSAGE('The Manifest could not be found - MID: ' & p:MID, 'Generate Transporter Invoice', ICON:Hand)
    .

    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:_InvoiceTransporter.Close()
    Relate:ManifestAlias.Close()
    Relate:DeliveryLegs.Close()
    Relate:DeliveriesAlias.Close()
    Exit
Leg_Invoices            ROUTINE
     !db.debugout('[Gen_Trn._Invoice]  In Leg')

    ! Generate Transporter Leg invoices
    ! Loop through the DIs getting any Leg charges

    IF Get_Manifest_DIDs(p:MID, LOC:DIs) > 0
       !db.debugout('[Gen_Trn._Invoice]  Leg DIs: ' & CLIP(LOC:DIs))

       IF LEN(CLIP(LOC:DIs)) > 995
          MESSAGE('There may be DIs that have legs that do not get Transporter Invoices!|MID ' & p:MID & '||Report this whole error message to support!','Gen_Transporter_Invoice',ICON:Hand)
       .

       LOOP
          IF CLIP(LOC:DIs) = ''
             BREAK
          .

          LOC:DID  = Get_1st_Element_From_Delim_Str(LOC:DIs, ',', TRUE)

          IF LOC:DID ~= 0
             ! Look for Legs on this DI
             CLEAR(DELL:Record,-1)
             DELL:DID   = LOC:DID
             SET(DELL:FKey_DID_Leg, DELL:FKey_DID_Leg)
             LOOP
                IF Access:DeliveryLegs.TryNext() ~= LEVEL:Benign
                   BREAK
                .

                IF DELL:DID ~= LOC:DID
                   BREAK
                .

                ! Check if we have it, delete it if we have been asked to
                INT:DLID           = DELL:DLID
                IF Access:_InvoiceTransporter.TryFetch(INT:FKey_DLID) = LEVEL:Benign
                   IF LOC:Replace = 2
                      db.debugout('[Gen_Tran._Inv]  Leg_Invoices - Deleting Inv Trans - MID: ' & p:MID & ', INT:TIN: ' & INT:TIN & ',  DELL:DLID: ' & DELL:DLID)

                      IF Relate:_InvoiceTransporter.Delete(0) ~= LEVEL:Benign
                         ! Problem ???
                .  .  .

                INT:DLID           = DELL:DLID
                IF Access:_InvoiceTransporter.TryFetch(INT:FKey_DLID) = LEVEL:Benign
                   ! We have an invoice for this leg...
                ELSE
                   ! We have at least 1 leg, create an Invoice for it
                   CLEAR(INT:Record)
                   IF Access:_InvoiceTransporter.TryPrimeAutoInc() = LEVEL:Benign
                      !INT:TIN
                      INT:TID            = DELL:TID

                      INT:BID            = A_MAN:BID
                      INT:MID            = A_MAN:MID

                      INT:CreatedDate    = A_MAN:CreatedDate
                      INT:CreatedTime    = A_MAN:CreatedTime

                      INT:UID            = GLO:UID

                      ! Delivery Leg...
                      INT:JID            = DELL:JID
                      INT:DID            = DELL:DID

                      A_DEL:DID          = DELL:DID
                      IF Access:DeliveriesAlias.TryFetch(A_DEL:PKey_DID) = LEVEL:Benign
                         INT:DINo        = A_DEL:DINo               !               - we dont have this on Legs
                      .

                      INT:DLID           = DELL:DLID
                      INT:Leg            = DELL:Leg
                      INT:CollectionAID  = DELL:CollectionAID
                      INT:DeliveryAID    = DELL:DeliveryAID

                      INT:Cost           = DELL:Cost
                      INT:VAT            = DELL:Cost * (DELL:VATRate / 100)

                      INT:VATRate        = DELL:VATRate

                      INT:Broking        = TRUE                     ! Note - All legs are considered Broking

                      INT:Status         = 0
                      INT:StatusUpToDate = FALSE

                      INT:InvoiceDate    = p:Inv_Date               ! Generate the invoice with Manifest date not - TODAY()
                      INT:InvoiceTime    = CLOCK()

                      IF Access:_InvoiceTransporter.TryInsert() ~= LEVEL:Benign
                         Access:_InvoiceTransporter.CancelAutoInc()
                         LOC:Result      = -40
                      ELSE
                         ! Created
                      .
                   ELSE
                      LOC:Result         = -30
                   .

                   IF LOC:Result < 0
                      BREAK
    .  .  .  .  .  .

    EXIT
