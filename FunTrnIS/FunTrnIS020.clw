

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS020.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Gen_Statement_old_May08 PROCEDURE  (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output) ! Declare Procedure
LOC:Result           LONG                                  !
LOC:MonthEndDate     LONG                                  !
LOC:Run_Date_Mth_End BYTE                                  !
LOC:Next_Mth_Mth_End BYTE                                  !
LOC:Statement_Info   GROUP,PRE(L_SI)                       !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
                     END                                   !
LOC:Credited         DECIMAL(11,2)                         !
LOC:Amount           DECIMAL(10,2)                         !
LOC:CreditNotes_Added_Q QUEUE,PRE(L_CQ)                    !
IID                  ULONG                                 !Invoice Number
                     END                                   !
LOC:Stats_Group      GROUP,PRE(L_SG)                       !
No_Invs              LONG                                  !
TimeIn               LONG                                  !
                     END                                   !
Tek_Failed_File     STRING(100)

Inv_View            VIEW(_Invoice)
    PROJECT(INV:IID, INV:BID, INV:CID, INV:DINo, INV:InvoiceDate, INV:InvoiceTime, INV:Total, INV:DID, INV:Status, INV:StatusUpToDate, INV:ClientNo)
    .

View_Inv            ViewManager



InvA_View            VIEW(InvoiceAlias)
    PROJECT(A_INV:IID, A_INV:BID, A_INV:CID, A_INV:DINo, A_INV:InvoiceDate, A_INV:InvoiceTime, A_INV:Total, A_INV:DID, A_INV:Status)
    .

View_InvA            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAllocation.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Statements.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_StatementItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceAlias.Open()
     .
     Access:_Invoice.UseFile()
     Access:ClientsPaymentsAllocation.UseFile()
     Access:_Statements.UseFile()
     Access:_StatementItems.UseFile()
     Access:InvoiceAlias.UseFile()
    PUSHBIND
    BIND('INV:InvoiceDate', INV:InvoiceDate)
    BIND('INV:InvoiceTime', INV:InvoiceTime)
    BIND('INV:InvoiceDateAndTime', INV:InvoiceDateAndTime)
    BIND('INV:Status', INV:Status)
    BIND('A_INV:InvoiceDate', A_INV:InvoiceDate)
    BIND('A_INV:InvoiceTime', A_INV:InvoiceTime)
    BIND('A_INV:InvoiceDateAndTime', A_INV:InvoiceDateAndTime)
    BIND('A_INV:Status', A_INV:Status)
    ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:OutPut)
    !     1       2      3        4       5          6            7               8            9             10
    ! (ULONG, ULONG, LONG, LONG, <*DECIMAL>, BYTE=0, <*STRING>, BYTE=0, BYTE=0, BYTE=0),LONG
    !   p:Date  - Run / Effective date
    !   p:Run_Type
    !       0   -               ! Client & Statement Gen.
    !       1   - 
    !       2   -
    !
    !   p:OutPut
    !       0   - none
    !       1   - All
    !       2   - Current
    !       3   - 30 Days
    !       4   - 60 Days
    !       5   - 90 Days
    !
    ! We should run the invoice maintenance function before running the statements... maybe not though, see code

    L_SG:TimeIn                 = CLOCK()

    ! If p:MonthEndDay is provided it is taken to be the forthcoming month end otherwise settings are used
    ! Get_Month_End_Date
    ! (p:Date, p:MonthEndDay)
    ! (LONG, BYTE=0),DATE
    LOC:MonthEndDate            = Get_Month_End_Date(p:Date, p:MonthEndDay)

!    db.debugout('[Gen_Statement]   p:MonthEndDay: ' & p:MonthEndDay & ',   Date: ' & FORMAT(LOC:MonthEndDate,@d6))
!    db.debugout('[Gen_Statement]  In - CID: ' & p:CID & ',  Date: ' & FORMAT(p:Date,@d5))
    IF p:Dont_Add = TRUE
       DO Get_Details
       IF ~OMITTED(7)
          p:Statement_Info      = LOC:Statement_Info
       .
    ELSE
       ! Create Statement
       CLEAR(STA:Record)
       IF Access:_Statements.TryPrimeAutoInc() = LEVEL:Benign
          !STA:STID            auto inc
          STA:STRID             = p:STRID
          STA:StatementDate     = p:Date
          STA:StatementTime     = p:Time

          STA:CID               = p:CID
          STA:BID               = GLO:BranchID

          DO Get_Details
          STA:Record           :=: LOC:Statement_Info

          IF STA:Total = 0.0
             ! No statement when nothing to state.
             IF Access:_Statements.CancelAutoInc() ~= LEVEL:Benign
             .
          ELSE
             IF Access:_Statements.TryInsert() ~= LEVEL:Benign
                ! hmm ??
             ELSE
                LOC:Result      = TRUE
          .  .
       ELSE
          ! hmmm?
    .  .

    IF ~OMITTED(5)
       p:Owing                  = L_SI:Total
    .

    ! Update Client - updates Client Record with these balances
    Upd_Client_Balances(p:CID, p:Date, LOC:Statement_Info)


    IF CLOCK() - L_SG:TimeIn > 100
       db.debugout('[Gen_Statement]  No. Invs: ' & L_SG:No_Invs & ',   CID: ' & p:CID        & |
                    ',  Time > 1 sec: ' & CLOCK() - L_SG:TimeIn)
    .
    UNBIND('INV:InvoiceDate')
    UNBIND('INV:InvoiceTime')
    UNBIND('INV:InvoiceDateAndTime')
    UNBIND('INV:Status')
    UNBIND('A_INV:InvoiceDate')
    UNBIND('A_INV:InvoiceTime')
    UNBIND('A_INV:InvoiceDateAndTime')
    UNBIND('A_INV:Status')
    POPBIND
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:_Invoice.Close()
    Relate:ClientsPaymentsAllocation.Close()
    Relate:_Statements.Close()
    Relate:_StatementItems.Close()
    Relate:InvoiceAlias.Close()
    Exit
Get_Details                           ROUTINE
!    My_SQL.Init(GLO:DBOwner,'_TempSQL')
!
!    IF My_SQL.PropSQL('SELECT IID, BID, CID, DINo, InvoiceDate, InvoiceTime, Total, DID, Status, StatusUpToDate, ClientNo' & |
!                    ' FROM _Invoice WHERE CID = ' & p:CID & ' AND InvoiceDate < ' & SQL_Get_DateT_G(p:Date + 1) & |
!                    ' AND AND Status < 4 ORDER BY InvoiceDate') < 0
!       MESSAGE('Error on SQL.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Gen_Statement', ICON:Hand)
!    .
!

    L_SG:No_Invs        = 0

    View_Inv.Init(Inv_View, Relate:_Invoice)
    View_Inv.AddSortOrder(INV:FKey_CID)
    View_Inv.AddRange(INV:CID, p:CID)

    View_Inv.AppendOrder('INV:InvoiceDate')

    ! Assume they only want current, 2 is fully paid
    ! Note: use of tomorrow as date, with no time specified.

    !       0           1               2              3                 4               5
    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
    ! Fully paid invoices are excluded from the set because they cannot impact on the balance owing

    View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND INV:Status < 4')
    !IF p:Dont_Add = TRUE
    !   Add a filter for Credit notes (2) as they cannot impact on an Age analysis run
    !.

    View_Inv.Reset()
    IF ERRORCODE()
       MESSAGE('Error on View.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Gen_Statement', ICON:Hand)
    .
    LOOP
       IF View_Inv.Next() ~= LEVEL:Benign
          BREAK
       .
       L_SG:No_Invs += 1

       ! Here we only have the invoices less than the date specified.
       ! and not fully paid.
       ! For credit notes, if they are appearing on their 1st statement, then set them to having been shown
       IF p:Run_Type < 2 AND p:Dont_Add = FALSE          ! Client & Statement Gen.
          Upd_Invoice_Paid_Status(INV:IID, 1)            ! Update Fully Paid - Credit to Fully Paid and Credit Note to Credit Note - Shown
       ELSE
          IF INV:StatusUpToDate = FALSE                  ! We know we need to check the Invoice Status
             Upd_Invoice_Paid_Status(INV:IID)
       .  .

       IF p:Dont_Add = FALSE            ! Add to Statement
          L_CQ:IID       = INV:IID
          GET(LOC:CreditNotes_Added_Q, L_CQ:IID)
          IF ~ERRORCODE()               ! Already have shown Credit note with respective invoice
             CYCLE
          .

          DO Add_to_Statement
       ELSE
          IF INV:Status = 2 OR INV:Status = 5                             ! If this is a Credit Note, then no amount
             STAI:Amount    = 0.0
          ELSE
             STAI:Debit     = INV:Total                                   ! Invoice total

             ! Check that Invoice has no Credit Notes for it
             LOC:Credited   = Get_Inv_Credited( INV:IID, p:Date )         ! Less any Credit Notes

             STAI:Debit    += LOC:Credited

             STAI:Credit    = Get_ClientsPay_Alloc_Amt( INV:IID, 0, p:Date )      ! Payment total for this Invoice

             STAI:Amount    = STAI:Debit - STAI:Credit
          .

          LOC:Amount        = STAI:Amount
       .

!    db.debugout('[Gen_Statement]  CID: ' & p:CID & ',  INV:IID: ' & INV:IID & ',  INV:CID: ' & INV:CID & ',  INV:DID: ' & INV:DID & ',  INV:Total: ' & INV:Total)
       CASE Months_Between(LOC:MonthEndDate, INV:InvoiceDate)
       OF 0
          L_SI:Current   += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 2                       ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             Add_Log(',' & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, 'Current', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       OF 1
          L_SI:Days30    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 3
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '30 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       OF 2
          L_SI:Days60    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 4
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '60 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       ELSE
          L_SI:Days90    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 5
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '90 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
    .  .

    View_Inv.Kill()

    L_SI:Total            = L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current
    EXIT
Add_to_Statement                ROUTINE
    IF Access:_StatementItems.TryPrimeAutoInc() = LEVEL:Benign
       ! STAI:STIID
       STAI:STID            = STA:STID

       STAI:InvoiceDate     = INV:InvoiceDate
       STAI:IID             = INV:IID
       STAI:DID             = INV:DID
       STAI:DINo            = INV:DINo

       ! p:Option
       !   0.  Invoice Paid
       !   1.  Clients Payments Allocation total
       STAI:Debit           = INV:Total

       ! When Credit
       LOC:Credited         = Get_Inv_Credited( INV:IID )
       STAI:Debit          += LOC:Credited               ! Less any Credit

       STAI:Credit          = Get_ClientsPay_Alloc_Amt( INV:IID, 0 )

       !    0               1               2           3                   4               5
       ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
       IF INV:Status = 2 OR INV:Status = 5          ! Credit notes
          STAI:Amount       = 0.0

          ! FBN wants the "Credit" amount to be in the Credit column and a + amount
          STAI:Credit       = INV:Total
          STAI:Debit       -= INV:Total
       ELSE
          STAI:Amount       = STAI:Debit - STAI:Credit
       .

       LOC:Amount           = STAI:Amount

       IF Access:_StatementItems.TryInsert() = LEVEL:Benign
          IF LOC:Credited ~= 0.0
             ! Add the credit note in here and add it to the Q so that it is not added here again
             L_CQ:IID       = INV:IID

             DO Credit_Notes_for_IID

             ADD(LOC:CreditNotes_Added_Q)
    .  .  .
    EXIT
Credit_Notes_for_IID            ROUTINE
    View_InvA.Init(InvA_View, Relate:InvoiceAlias)
    View_InvA.AddSortOrder(A_INV:Key_CR_IID)
    View_InvA.AppendOrder('A_INV:InvoiceDate')
    View_InvA.AddRange(A_INV:CR_IID, L_CQ:IID)

    !       0           1               2              3                 4               5
    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
    View_InvA.SetFilter('A_INV:InvoiceDate < ' & p:Date + 1 & ' AND A_INV:Status < 4')

    View_InvA.Reset()
    LOOP
       IF View_InvA.Next() ~= LEVEL:Benign
          BREAK
       .

       ! Show it then after the invoice
       IF Access:_StatementItems.TryPrimeAutoInc() = LEVEL:Benign
          ! STAI:STIID
          STAI:STID            = STA:STID

          STAI:InvoiceDate     = A_INV:InvoiceDate
          STAI:IID             = A_INV:IID
          STAI:DID             = A_INV:DID
          STAI:DINo            = A_INV:DINo

          ! p:Option
          !   0.  Invoice Paid
          !   1.  Clients Payments Allocation total
          STAI:Debit           = A_INV:Total

          ! When Credit
          LOC:Credited         = Get_Inv_Credited( A_INV:IID )                      ! --------- should not be for credit notes
          STAI:Debit          += LOC:Credited               ! Less any Credit

          STAI:Credit          = Get_ClientsPay_Alloc_Amt( A_INV:IID, 0 )           ! --------- should not be for credit notes

          !    0               1               2           3                   4               5
          ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
          IF A_INV:Status = 2                                                       ! --------- should only be
             STAI:Amount       = 0.0

             ! FBN wants the "Credit" amount to be in the Credit column
             STAI:Credit      -= A_INV:Total
             STAI:Debit       -= A_INV:Total
          .

    db.debugout('[Gen_Statement]  Adding Credit note - STAI:STIID: ' & STAI:STIID)

          IF Access:_StatementItems.TryInsert() = LEVEL:Benign
    .  .  .

    View_InvA.Kill()
    EXIT
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_FuelSurcharge_11May PROCEDURE  (p:CID, p:Date)         ! Declare Procedure
LOC:Surcharge        STRING(35)                            !
Tek_Failed_File     STRING(100)

View_FSC            VIEW(__RatesFuelSurcharge)
    .


FSC_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__RatesFuelSurcharge.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsAlias.Open()
     .
     Access:__RatesFuelSurcharge.UseFile()
     Access:ClientsAlias.UseFile()
    ! (p:CID, p:Date)

    CLI:CID     = p:CID
    IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) ~= LEVEL:Benign
       CLEAR(CLI:Record)
    .

    IF A_CLI:FuelSurchargeActive = TRUE
       IF p:Date = 0
          p:Date   = TODAY()
       .

       BIND('FSRA:Effective_Date',FSRA:Effective_Date)

       FSC_View.Init(View_FSC, Relate:__RatesFuelSurcharge)
       FSC_View.AddSortOrder(FSRA:CKey_CID_EffDate)
       !FSC_View.AppendOrder('FSRA:Effective_Date')
       FSC_View.AddRange(FSRA:CID, p:CID)
       FSC_View.SetFilter('FSRA:Effective_Date <= ' & p:Date)

       FSC_View.Reset()

       LOOP
          IF FSC_View.Next() ~= LEVEL:Benign
             BREAK
          .

          LOC:Surcharge    = FSRA:FuelSurcharge

          BREAK
       .

       FSC_View.Kill()

       UNBIND('FSRA:Effective_Date')
    .
           Relate:__RatesFuelSurcharge.Close()
           Relate:ClientsAlias.Close()


       Return(LOC:Surcharge)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_FuelCost         PROCEDURE  (p:Date, p:Eff_Date, p:BaseRate) ! Declare Procedure
LOC:FuelCost         DECIMAL(10,3)                         !Cost per litre
LOC:Static_Group     GROUP,PRE(L_SG),STATIC                !
UseDate              LONG                                  !
UseTime              LONG                                  !
FuelCost             DECIMAL(10,3)                         !Cost per litre
EffectiveDate        DATE                                  !
FuelBaseRate         DECIMAL(10,4)                         !% cost of total cost
As_At_Date           LONG                                  !
                     END                                   !
LOC:Buffer           USHORT                                !
Tek_Failed_File     STRING(100)

View_FC            VIEW(_FuelCostsAlias)
    .


FC_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_FuelCostsAlias.Open()
     .
     Access:_FuelCostsAlias.UseFile()
    ! (p:Date, p:Eff_Date, p:BaseRate)
    ! (LONG, <*LONG>, <*DECIMAL>)
    IF p:Date = 0
       p:Date   = TODAY()
    .

    IF p:Date = L_SG:As_At_Date AND L_SG:UseDate = TODAY() AND ABS(CLOCK() - L_SG:UseTime) < (100 * 30)      ! 30 secs
       LOC:FuelCost         = L_SG:FuelCost
       IF ~OMITTED(2)
          p:Eff_Date        = L_SG:EffectiveDate
       .
       IF ~OMITTED(3)
          p:BaseRate        = L_SG:FuelBaseRate
       .
    ELSE
       !LOC:Buffer           = Access:_FuelCost.SaveBuffer()

       BIND('A_FUE:EffectiveDate', A_FUE:EffectiveDate)

       CLEAR(A_FUE:Record)
       FC_View.Init(View_FC, Relate:_FuelCostsAlias)
       FC_View.AddSortOrder()
       FC_View.AppendOrder('-A_FUE:EffectiveDate')
       FC_View.SetFilter('A_FUE:EffectiveDate < ' & p:Date + 1)

       FC_View.Reset()

       LOOP
          IF FC_View.Next() ~= LEVEL:Benign
             BREAK
          .

          LOC:FuelCost      = A_FUE:FuelCost

          BREAK
       .

       FC_View.Kill()

       UNBIND('A_FUE:EffectiveDate')


       L_SG:UseDate         = TODAY()
       L_SG:UseTime         = CLOCK()
       L_SG:FuelCost        = A_FUE:FuelCost
       L_SG:EffectiveDate   = A_FUE:EffectiveDate
       L_SG:FuelBaseRate    = A_FUE:FuelBaseRate
       L_SG:As_At_Date      = p:Date

       IF ~OMITTED(2)
          p:Eff_Date        = L_SG:EffectiveDate
       .
       IF ~OMITTED(3)
          p:BaseRate        = L_SG:FuelBaseRate
       .

     !  Access:_FuelCost.RestoreBuffer(LOC:Buffer)
    .
           Relate:_FuelCostsAlias.Close()


       Return(LOC:FuelCost)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_FuelBaseRate     PROCEDURE  (p:Date, p:EffDate)        ! Declare Procedure
LOC:FuelBaseRate     STRING(35)                            !
LOC:Static_Group     GROUP,PRE(L_SG),STATIC                !
UseDate              LONG                                  !
UseTime              LONG                                  !
FuelBaseRate         DECIMAL(10,4)                         !
EffectiveDate        DATE                                  !
                     END                                   !
Tek_Failed_File     STRING(100)

View_FC            VIEW(_FuelBaseRate)
    .


FC_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_FuelBaseRate.Open()
     .
     Access:_FuelBaseRate.UseFile()
    ! (p:Date, p:EffDate)
    ! (LONG=0, <*LONG>),STRING

    IF p:Date = 0
       p:Date   = TODAY()
    .

    IF p:Date = L_SG:EffectiveDate AND L_SG:UseDate = TODAY() AND ABS(CLOCK() - L_SG:UseTime) < (100 * 30)      ! 30 secs
       LOC:FuelBaseRate         = L_SG:FuelBaseRate
       IF ~OMITTED(2)
          p:EffDate             = L_SG:EffectiveDate
       .
    ELSE
       BIND('FUB:EffectiveDate', FUB:EffectiveDate)

       CLEAR(FUB:Record)
       FC_View.Init(View_FC, Relate:_FuelBaseRate)
       FC_View.AddSortOrder()
       FC_View.AppendOrder('-FUB:EffectiveDate')
       FC_View.SetFilter('FUB:EffectiveDate < ' & p:Date + 1)

       FC_View.Reset()

       LOOP
          IF FC_View.Next() ~= LEVEL:Benign
             BREAK
          .

          LOC:FuelBaseRate      = FUB:FuelBaseRate

          BREAK
       .

       FC_View.Kill()

       UNBIND('FUB:EffectiveDate')


       L_SG:UseDate         = TODAY()
       L_SG:UseTime         = CLOCK()
       L_SG:FuelBaseRate    = FUB:FuelBaseRate
       L_SG:EffectiveDate   = FUB:EffectiveDate

       IF ~OMITTED(2)
          p:EffDate         = L_SG:EffectiveDate
    .  .
           Relate:_FuelBaseRate.Close()


       Return(LOC:FuelBaseRate)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
SetCtrlReadOnly      PROCEDURE  (Byte pStatus,LONG pCtrl,<TIDQueue pCtrls>) ! Declare Procedure
ReadOnlyColor   EQUATE(COLOR:BTNFACE)
CurColor        LONG
curCtrl         LONG

  CODE
    IF pStatus
        CurColor = ReadOnlyColor
    ELSE
        CurColor = COLOR:NONE
    END
    IF Omitted(pCtrls)
        curCtrl = pCtrl
        Do SetCurrent
    ELSE
        LOOP ct#=1 TO RECORDS(pCtrls)
            GET(pCtrls,ct#)
            IF ErrorCode() THEN cycle.
            curCtrl = pCtrls.ID
            Do SetCurrent
        END
    END

SetCurrent      Routine
    Case curCtrl{Prop:Type}
    of CREATE:String
        curCtrl{Prop:FontColor} = curCtrl{Prop:BackGround}
    of CREATE:Image
        curCtrl{Prop:Hide} = True
    of  CREATE:Entry
        curCtrl{Prop:BackGround} = CurColor
        curCtrl{Prop:ReadOnly}   = pStatus
    of CREATE:DropList orof CREATE:Combo orof CREATE:DropCombo
        curCtrl{Prop:Disable}    = pStatus
    of CREATE:Text
        curCtrl{Prop:BackGround} = CurColor
        curCtrl{Prop:ReadOnly}   = pStatus
    of CREATE:Check orof CREATE:Radio
        curCtrl{Prop:BackGround} = CurColor
        curCtrl{Prop:Disable}   = pStatus
    of CREATE:spin
        curCtrl{Prop:BackGround} = CurColor
        curCtrl{Prop:ReadOnly}   = pStatus
        curCtrl{Prop:Disable}    = pStatus
    of CREATE:button
        curCtrl{Prop:Disable}    = pStatus
    of CREATE:List
        curCtrl{Prop:BackGround} = CurColor
        if curCtrl{Prop:Drop} <> 0
            curCtrl{Prop:Disable} = pStatus
        .
    of CREATE:Tab 
        curCtrl{Prop:Disable} = pStatus
    of CREATE:Item orof CREATE:Menu
        curCtrl{Prop:Disable} = pStatus
    End

