

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABASCII.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('FUNTRNIS006.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Line_Print PROCEDURE (p:File_Name)

ViewerActive1        BYTE(False)
AsciiFilename1       STRING(FILE:MaxFilePath),AUTO,STATIC,THREAD
AsciiFile1           FILE,DRIVER('ASCII'),NAME(AsciiFilename1),PRE(A1),THREAD
RECORD                RECORD,PRE()
TextLine                STRING(255)
                      END
                     END
ViewWindow           WINDOW('View an ASCII File'),AT(,,296,136),FONT('Tahoma',8),CENTER,GRAY,SYSTEM
                       LIST,AT(5,5,285,110),USE(?AsciiBox),FONT('Courier New',8,,FONT:regular,CHARSET:ANSI),HVSCROLL, |
  FROM(''),IMM
                       BUTTON('&Close'),AT(255,120,35,10),USE(?Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Viewer1              CLASS(AsciiViewerClass)
GetDOSFilename         PROCEDURE(*STRING Dest),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_File              ROUTINE
    IF ~OMITTED(1) = TRUE
       AsciiFilename1   = p:File_Name
    .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Line_Print')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?AsciiBox
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  SELF.Open(ViewWindow)                                    ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Line_Print',ViewWindow)                    ! Restore window settings from non-volatile store
  CLEAR(AsciiFilename1)
  ViewerActive1=Viewer1.Init(AsciiFile1,A1:Textline,AsciiFilename1,?AsciiBox,GlobalErrors,EnableSearch+EnablePrint)
  IF ~ViewerActive1 THEN RETURN Level:Fatal.
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('Line_Print',ViewWindow)                 ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF ViewWindow{Prop:AcceptAll} THEN RETURN.
  IF ViewerActive1 THEN Viewer1.TakeEvent(EVENT()).
  PARENT.Reset(Force)


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?AsciiBox
    IF ViewerActive1
      IF Viewer1.TakeEvent(EVENT())=Level:Notify THEN CYCLE.
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
      IF ViewerActive1
        Viewer1.Kill
        ViewerActive1=False
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Viewer1.GetDOSFilename PROCEDURE(*STRING Dest)

ReturnValue          BYTE,AUTO


  CODE
    DO Check_File
    IF CLIP(AsciiFilename1) = ''
  ReturnValue = PARENT.GetDOSFilename(Dest)
    .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Manifest_Info    PROCEDURE  (p:MID, p:Option, p:DI_Type, p:Inv_Totals, p:Output, p:Info) ! Declare Procedure
LOC:DIDs_Q           QUEUE,PRE(L_DQ)                       !
DID                  ULONG                                 !Delivery ID
DIID                 ULONG                                 !Delivery Item ID
MLDID                ULONG                                 !Manifest Load Delivery Items ID
UnitsLoaded          USHORT                                !Number of units
TotalUnitsLoaded     LONG                                  !Total for this DID range
Broking              BYTE                                  !
                     END                                   !
LOC:Result           STRING(50)                            !
LOC:Idx              LONG                                  !
LOC:Count_Invoiced   LONG                                  !
LOC:Inv_Result       LONG                                  !
LOC:Errors           LONG                                  !
LOC:Not_Created      LONG                                  !
LOC:Load             BYTE                                  !
LOC:Accum_Amt        DECIMAL(17,5)                         !
LOC:DID_Last         ULONG                                 !Delivery ID
LOC:DI_Units_Tot     LONG                                  !
LOC:Temp_Amt         DECIMAL(20,5)                         !
LOC:Info             STRING(200)                           !
Tek_Failed_File     STRING(100)

MAN_Del         VIEW(ManifestLoadAlias)
    PROJECT(A_MAL:MLID, A_MAL:MID, A_MAL:TTID)
       JOIN(A_MALD:FSKey_MLID_DIID, A_MAL:MLID)                                 ! Manifest Load Deliveries
       PROJECT(A_MALD:MLID, A_MALD:MLDID, A_MALD:DIID, A_MALD:UnitsLoaded)
          JOIN(A_DELI:PKey_DIID, A_MALD:DIID)                                   ! Delivery Items
          PROJECT(A_DELI:DIID, A_DELI:DID, A_DELI:Weight, A_DELI:Units)
             JOIN(A_DEL:PKey_DID, A_DELI:DID)                                   ! Deliveries
             PROJECT(A_DEL:DID, A_DEL:DINo, A_DEL:DIDate, A_DEL:DocumentCharge, A_DEL:FuelSurcharge, A_DEL:TollCharge, A_DEL:Charge, |
                        A_DEL:Insure, A_DEL:TotalConsignmentValue, A_DEL:InsuranceRate, A_DEL:VATRate, |
                        A_DEL:AdditionalCharge)
                JOIN(SERI:PKey_SID, A_DEL:SID)
                PROJECT(SERI:Broking)
    .  .  .  .  .

Man_View        ViewManager




Del_Legs         VIEW(DeliveryLegsAlias)
    PROJECT(A_DELL:DID, A_DELL:Leg, A_DELL:Cost, A_DELL:VATRate)
    .


Del_View        ViewManager




Inv_            VIEW(InvoiceAlias)
    .

Inv_View        ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryLegsAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ServiceRequirements.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceAlias.Open()
     .
     Access:ManifestLoadAlias.UseFile()
     Access:ManifestLoadDeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
     Access:DeliveriesAlias.UseFile()
     Access:DeliveryLegsAlias.UseFile()
     Access:ServiceRequirements.UseFile()
     Access:InvoiceAlias.UseFile()
    ! (p:MID, p:Option, p:DI_Type, p:Inv_Totals, p:Output, p:Info  )
    ! (ULONG, BYTE    , BYTE=0   , BYTE=0      , BYTE=0  , <STRING>),STRING
    !   1       2           3           4           5           6
    !   p:Option
    !       0.  Charges total
    !       1.  Insurance total
    !       2.  Weight total, for this Manifest!    Uses Units_Loaded
    !       3.  Extra Legs cost
    !       4.  Charges total incl VAT
    !       5.  No. DI's
    !       6.  Extra Legs cost - inc VAT
    !   p:DI_Type
    !       0.  All             (default)
    !       1.  Non-Broking
    !       2.  Broking
    !   p:Inv_Totals
    !       0.  Collect from Delivery info.
    !       1.  Collect from Invoiced info.     (only Manifest invoices)

    ! *** nothing changed in here since 2007!  although date now updated

!    db.debugout('here - Option: ' & p:Option & ',  MID: ' & p:MID)
    PUSHBIND()
    BIND('SERI:Broking',SERI:Broking)

    Man_View.Init(MAN_Del, Relate:ManifestLoadAlias)
    Man_View.AddSortOrder(A_MAL:FKey_MID)
    Man_View.AppendOrder('A_DEL:DID,A_DELI:DIID')
    Man_View.AddRange(A_MAL:MID, p:MID)

    CASE p:DI_Type
    OF 1
       Man_View.SetFilter('SERI:Broking=0 OR SQL(Broking IS NULL)')         ! SQL !!!???
    OF 2
       Man_View.SetFilter('SERI:Broking=1')
    .

    Man_View.Reset()
    LOOP
       IF Man_View.Next() ~= LEVEL:Benign
          BREAK
       .

       CLEAR(LOC:Load)

       CASE p:Option
       OF 0 OROF 4
          ! DIID can be loaded on more than 1 vehicle...
          ! Need unique DID, Item and Truck
          L_DQ:DID          = A_DEL:DID
          !L_DQ:DIID         = A_DELI:DIID          - we don't want items, we want deliveries
          L_DQ:MLDID        = A_MALD:MLDID       !A_MAL:MLID

          GET(LOC:DIDs_Q, L_DQ:DID, L_DQ:MLDID)
          IF ERRORCODE()
             LOC:Load       = TRUE
          .
       OF 1 OROF 3 OROF 5 OROF 6
          ! Then we only need unique DIDs
          L_DQ:DID          = A_DEL:DID
          GET(LOC:DIDs_Q, L_DQ:DID)
          IF ERRORCODE()
             LOC:Load       = TRUE
          .
       OF 2
          ! DID will not be unique - required when checking the DID data
          ! Need unique DID and Manifest Load
          L_DQ:DID          = A_DEL:DID
          L_DQ:MLDID        = A_MALD:MLDID
          GET(LOC:DIDs_Q, L_DQ:DID, L_DQ:MLDID)
          IF ERRORCODE()
             LOC:Load       = TRUE
          .
       ELSE
          MESSAGE('Option does not exist!  p:Option: ' & p:Option, 'Get_Manifest_Info', ICON:Hand)
       .


       IF LOC:Load = TRUE
!    db.debugout('load')
          !A_DEL:DID
          !A_DEL:DINo
          !A_DEL:DIDate

          CASE p:Option
          OF 0 OROF 4
             IF p:Inv_Totals = FALSE
                !LOC:Result    += ((A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge) * (A_MALD:UnitsLoaded / A_DELI:Units)) * 100
                ! For every Delivery, sum charges and divide by the ratio of units on the Manifest Load to units on the Manifest
                LOC:DI_Units_Tot  = Get_DelItem_s_Totals(A_DEL:DID, 3)

                IF p:Option = 0
                   LOC:Temp_Amt   = (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:TollCharge + A_DEL:Charge + A_DEL:AdditionalCharge) * |
                                     A_MALD:UnitsLoaded / LOC:DI_Units_Tot
                ELSE
                   LOC:Temp_Amt   = ( (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:TollCharge + A_DEL:Charge + A_DEL:AdditionalCharge) * |
                                     (1 + (A_DEL:VATRate / 100)) ) * |
                                     A_MALD:UnitsLoaded / LOC:DI_Units_Tot

           !db.debugout('Del - ' & A_DEL:DID & ' - charge: ' & ( (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge) * (1 + (A_DEL:VATRate / 100)) ))
           !db.debugout('[Get_Manifest_Info]  Charges: ' & (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge + A_DEL:AdditionalCharge))
           !db.debugout('[Get_Manifest_Info]  Man Items Info: ' & Get_ManLoadItems_Info(A_MALD:MLDID, 1, A_DEL:DID))
           !db.debugout('[Get_Manifest_Info]  Items Totals: ' & LOC:DI_Units_Tot & ',   A_MALD:UnitsLoaded: ' & A_MALD:UnitsLoaded)
           !db.debugout('[Get_Manifest_Info]  DID: ' & A_DEL:DID & ',  Accum_Amt: ' & LOC:Accum_Amt & ',  + Temp_Amt: ' & LOC:Temp_Amt)
           !db.debugout('Ratio: ' & Get_ManLoadItems_Info(A_MALD:MLDID, 1, A_DEL:DID) / LOC:DI_Units_Tot)
                .

                LOC:Accum_Amt += LOC:Temp_Amt

                IF p:Output > 0
                   ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
                   Add_Log('MID: ' & p:MID & ', DINo.: ' & A_DEL:DINo & ', DID: ' & A_DEL:DID & ', MLDID: ' & A_MALD:MLDID & ', Amt: ' & LOC:Temp_Amt & '  (tot: ' & LOC:Accum_Amt & ')' & |
                           ', Units Loaded: ' & A_MALD:UnitsLoaded & ', Units Tot DI: ' & LOC:DI_Units_Tot & ', Option: ' & p:Option & ', Broking: ' & SERI:Broking, 'Get_Manifest_Info',,, 2, 0)
             .  .

!    db.debugout('Del charge: ' & (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge) & |
!                ',  Man Items: ' & Get_ManLoadItems_Info(A_MALD:MLDID, 1, A_DEL:DID) & ',  Del Items: ' & Get_DelItem_s_Totals(A_DEL:DID, 3))
          OF 1
             IF A_DEL:Insure = TRUE
                LOC:Accum_Amt    += A_DEL:TotalConsignmentValue * (A_DEL:InsuranceRate / 100)
             .
          OF 2
             IF p:Inv_Totals = FALSE
                LOC:Accum_Amt    += A_DELI:Weight * (A_MALD:UnitsLoaded / A_DELI:Units)

                IF p:Output > 0
                   Add_Log('MID: ' & p:MID & ', DINo.: ' & A_DEL:DINo & ', DID: ' & A_DEL:DID & ', MLDID: ' & A_MALD:MLDID & ', Item Weight: ' & A_DELI:Weight & |
                           ', Units Loaded: ' & A_MALD:UnitsLoaded & ', Item Units: ' & A_DELI:Units & ', Option: ' & p:Option & ', Broking: ' & SERI:Broking, 'Get_Manifest_Info',,, 2, 0)
             .  .
!             MESSAGE('Weight: ' & A_DELI:Weight & ',  A_MALD:UnitsLoaded: ' & A_MALD:UnitsLoaded & ',  A_DELI:Units: ' & A_DELI:Units & '||Add Weight: ' & (A_DELI:Weight * (A_MALD:UnitsLoaded / A_DELI:Units)) & '||LOC:Result: ' & LOC:Result / 100, 'A_DEL:DID: ' & A_DEL:DID)
          OF 3 OROF 6
             ! Accumulate Leg info for this delivery
             ! Not worried about the Filter being slow - this will be SQL.  How would this work on TPS? Use Key if available?


             Del_View.Init(Del_Legs, Relate:DeliveryLegsAlias)
             Del_View.AddSortOrder(A_DELL:FKey_DID_Leg)
             !Del_View.AppendOrder()
             Del_View.AddRange(A_DELL:DID, A_DEL:DID)
             !Del_View.SetFilter()

             Del_View.Reset()
             LOOP
                IF Del_View.Next() ~= LEVEL:Benign
                   BREAK
                .
                ! Accumulate Legs cost total
                CASE p:Option
                OF 3
                   LOC:Accum_Amt   += A_DELL:Cost
                ELSE
                   LOC:Accum_Amt   += A_DELL:Cost * (1 + (A_DELL:VATRate / 100))
                .

                IF p:Output > 0
                   Add_Log('MID: ' & p:MID & ', DINo.: ' & A_DEL:DINo & ', DID: ' & A_DEL:DID  & ', Leg Cost: ' & A_DELL:Cost & |
                           ', Leg VAT Rate: ' & A_DELL:VATRate & ', Option: ' & p:Option & ', Broking: ' & SERI:Broking, 'Get_Manifest_Info',,, 2, 0)
             .  .

             Del_View.Kill()
          OF 5
             LOC:Accum_Amt  += 1
          .

          L_DQ:DID          = A_DEL:DID
          L_DQ:DIID         = A_DELI:DIID
          L_DQ:MLDID        = A_MALD:MLDID
          L_DQ:UnitsLoaded  = A_MALD:UnitsLoaded        ! Only used for option 0 or 4
          L_DQ:Broking      = SERI:Broking
          ADD(LOC:DIDs_Q)
    .  .

    Man_View.Kill()


    IF p:Inv_Totals = TRUE AND (p:Option = 0 OR p:Option = 4 OR p:Option = 2)
       ! Accumulate totals from Invoice
       SORT(LOC:DIDs_Q, L_DQ:DID)

       DO Pre_Process_Q

       !BIND('L_DQ:DID',L_DQ:DID)
       !BIND('A_INV:DID',A_INV:DID)
       Inv_View.Init(Inv_, Relate:InvoiceAlias)
       Inv_View.AddSortOrder(A_INV:FKey_DID)
       Inv_View.AddRange(A_INV:DID, L_DQ:DID)
       !Inv_View.SetFilter('A_INV:DID = ' & L_DQ:DID)

       LOC:Accum_Amt    = 0.0
       LOC:DID_Last     = 0

       LOC:Idx          = 0
       LOOP
          LOC:Idx += 1
          GET(LOC:DIDs_Q, LOC:Idx)
          IF ERRORCODE()
             BREAK
          .
          IF LOC:DID_Last = L_DQ:DID
             CYCLE
          .
          LOC:DID_Last  = L_DQ:DID

      ! ???  12 Feb 07 - Potential problems with this code below
      ! This will go through all invoices with this DID, this will therefore include Credits and Journal Invoices
      ! so although the total amounts arrived at would be accurate they would have gone through portion
      ! calcualtions relating to the loading of the DI on this Manifest.

          !A_INV:DID     = L_DQ:DID
          Inv_View.ApplyRange()
          Inv_View.Reset()
          LOOP
             IF Inv_View.Next() ~= LEVEL:Benign
                BREAK
             .
             IF A_INV:MID = 0                   ! Only Manifest created Invoices (includes Credits and Journals)
                CYCLE
             .
             IF A_INV:POD_IID ~= A_INV:IID      ! 12 Feb 07 - exclude non original invoices from this?
                CYCLE
             .

             LOC:DI_Units_Tot   = Get_DelItem_s_Totals(L_DQ:DID, 3)

            !   p:Option
            !       0.  Charges total
            !       2.  Weight total, for this Manifest!    Uses Units_Loaded
            !       4.  Charges total incl VAT

            LOC:Temp_Amt   = 0
             CASE p:Option
             OF 0
                !A_INV:Insurance
                !LOC:Temp_Amt    = A_INV:Documentation + A_INV:FuelSurcharge + A_INV:AdditionalCharge + A_INV:FreightCharge

                LOC:Temp_Amt    = (A_INV:Documentation + A_INV:AdditionalCharge + A_INV:FreightCharge + A_INV:AdditionalCharge |
                                    + A_INV:FuelSurcharge + A_INV:TollCharge)
                                                !  * L_DQ:TotalUnitsLoaded / LOC:DI_Units_Tot
             OF 4
                !LOC:Temp_Amt    = A_INV:Total
                LOC:Temp_Amt    = A_INV:Total   !  *   L_DQ:TotalUnitsLoaded / LOC:DI_Units_Tot
             OF 2
                LOC:Temp_Amt    = A_INV:Weight  !  *   L_DQ:TotalUnitsLoaded / LOC:DI_Units_Tot
             .

!    IF A_INV:MID = 3378
!       db.debugout('[Get_Manifest_Info]   DID: ' & L_DQ:DID & ',  Total: ' & L_DQ:TotalUnitsLoaded & ',  DI_Units_Tot: ' & LOC:DI_Units_Tot & ',   Temp_Amt: ' & LOC:Temp_Amt)
!    .

             LOC:Accum_Amt  += LOC:Temp_Amt     *   L_DQ:TotalUnitsLoaded / LOC:DI_Units_Tot

             IF p:Output > 0
                IF ~OMITTED(6)
                   LOC:Info = p:Info
                .

                ! (p:IID, p:DID, p:DINo, p:MID, p:TIN, p:Tran_Date, p:Amount, p:Option)                                       ! Service type broking! May not be on broking Manifest
                Audit_ManageProfit(A_INV:IID, A_INV:DID, A_INV:DINo, A_INV:MID,, A_INV:InvoiceDate, A_INV:Total, 1, A_INV:BID, L_DQ:Broking, LOC:Info)

                Add_Log('MID: ' & p:MID & ', IID (no): ' & A_INV:IID & ', DID: ' & L_DQ:DID & ', MLDID: ' & L_DQ:MLDID & ', Amt: ' & LOC:Temp_Amt & '  (tot: ' & LOC:Accum_Amt & '), p:Option: ' & p:Option & ', p:Inv_Totals: ' & p:Inv_Totals, 'Get_Manifest_Info',,, 2, 0)
       .  .  .
       Inv_View.Kill()
    .

    UNBIND('SERI:Broking')
    POPBIND()

    LOC:Result    = LOC:Accum_Amt               !* 100


!    db.debugout('[Get_Manifest_Info]   LOC:Accum_Amt: ' & LOC:Accum_Amt & ',  p:MID: ' & p:MID & ',  p:Option: ' & p:Option)
! -----------------------------------------------------------------------------
!           old

!                IF p:Option = 0
!                   LOC:Accum_Amt += (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge + A_DEL:AdditionalCharge) * |
!                                     Get_ManLoadItems_Info(A_MALD:MLDID, 1, A_DEL:DID) / Get_DelItem_s_Totals(A_DEL:DID, 3)
!                ELSE
!                   LOC:Accum_Amt += ( (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge + A_DEL:AdditionalCharge) * |
!                                     (1 + (A_DEL:VATRate / 100)) ) * |
!                                     Get_ManLoadItems_Info(A_MALD:MLDID, 1, A_DEL:DID) / Get_DelItem_s_Totals(A_DEL:DID, 3)



    ! Code from copied procedure......

!    IF RECORDS(LOC:DIDs_Q) > 0
!       LOC:Idx      = 0
!       LOOP
!          LOC:Idx  += 1
!          GET(LOC:DIDs_Q, LOC:Idx)
!          IF ERRORCODE()
!             BREAK
!          .
!
!          LOC:Inv_Result         = Gen_Invoice_s(L_DQ:DID,, 0)
!          IF LOC:Inv_Result ~= 0            ! Don't report basic errors
!             IF LOC:Inv_Result > 0          ! -1 = nothing to invoice - already invoiced in this case
!                LOC:Errors      += 1
!             ELSIF LOC:Inv_Result < 0
!                LOC:Not_Created += 1
!             .
!             LOC:Result         -= 1
!             ! Then there was an error - which has been reported to the user
!          ELSE
!             LOC:Count_Invoiced += 1
!             ! Alls fine
!       .  .
!
!       IF LOC:Result < 0
!          IF LOC:Errors > 0
!             MESSAGE(LOC:Not_Created + LOC:Errors & ' Deliveries were not invoiced.|' & LOC:Count_Invoiced & ' were invoiced.||' & LOC:Not_Created & ' had existing invoices.|' & LOC:Errors & ' had errors.', 'Manifest Invoicing', ICON:Asterisk)
!          ELSE
!             MESSAGE(LOC:Not_Created & ' Deliveries were not invoiced, they have existing invoices.|' & LOC:Count_Invoiced & ' were invoiced.', 'Manifest Invoicing', ICON:Asterisk)
!       .  .
!    ELSE
!       LOC:Result   -= 1
!       MESSAGE('There were no Deliveries for the specified Manifest - MID: ' & p:MID, 'Gen_Man_Invs_and_DNs', ICON:Hand)
!    .









!           old
!    ! (p:MID, p:Option)
!    !   p:Option
!    !       0.  Charges total
!    !       1.  Insurance total
!    !       2.  Weight total, for this Manifest!    Uses Units_Loaded
!    !       3.  Extra Legs
!    !       4.  Charges total incl VAT
!
!    PUSHBIND
!    BIND('A_MAL:MID', A_MAL:MID)
!    BIND('A_DELL:DID', A_DELL:DID)
!
!!    db.debugout('here - Option: ' & p:Option & ',  MID: ' & p:MID)
!
!
!    Man_View.Init(MAN_Del, Relate:ManifestLoadAlias)
!
!
!    OPEN(MAN_Del)
!    MAN_Del{PROP:Filter}    = 'A_MAL:MID = ' & p:MID
!    MAN_Del{PROP:Order}     = 'A_DEL:DID,A_DELI:DIID'
!    SET(MAN_Del)
!    LOOP
!!       db.debugout('next')
!
!       NEXT(MAN_Del)
!       IF ERRORCODE()
!          BREAK
!       .
!
!       CLEAR(LOC:Load)
!
!       CASE p:Option
!       OF 0 OROF 4
!          ! DIID can be loaded on more than 1 vehicle...
!          ! Need unique DID, Item and Truck
!          L_DQ:DID          = A_DEL:DID
!          !L_DQ:DIID         = A_DELI:DIID          - we don't want items, we want deliveries
!          L_DQ:MLID         = A_MAL:MLID
!
!          GET(LOC:DIDs_Q, L_DQ:DID, L_DQ:MLID)
!          IF ERRORCODE()
!             LOC:Load       = TRUE
!          .
!       OF 1 OROF 3
!          ! Then we only need unique DIDs
!          L_DQ:DID          = A_DEL:DID
!          GET(LOC:DIDs_Q, L_DQ:DID)
!          IF ERRORCODE()
!             LOC:Load       = TRUE
!          .
!       OF 2
!          ! DID will not be unique - required when checking the DID data
!          ! Need unique DID and Manifest Load
!          L_DQ:DID          = A_DEL:DID
!          L_DQ:MLID         = A_MAL:MLID
!          GET(LOC:DIDs_Q, L_DQ:DID, L_DQ:MLID)
!          IF ERRORCODE()
!             LOC:Load       = TRUE
!          .
!       ELSE
!          MESSAGE('Option does not exist!  p:Option: ' & p:Option, 'Get_Manifest_Info', ICON:Hand)
!       .
!
!       IF LOC:Load = TRUE
!!    db.debugout('load')
!          !A_DEL:DID
!          !A_DEL:DINo
!          !A_DEL:DIDate
!
!          CASE p:Option
!          OF 0 OROF 4
!             !LOC:Result    += ((A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge) * (A_MALD:UnitsLoaded / A_DELI:Units)) * 100
!             ! For every Delivery, sum charges and divide by the ratio of units on the Manifest Load to units on the Manifest
!             IF p:Option = 0
!                LOC:Accum_Amt += (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge) * |
!                                  Get_ManLoadItems_Info(A_MAL:MLID, 1, A_DEL:DID) / Get_DelItem_s_Totals(A_DEL:DID, 3)
!             ELSE
!                LOC:Accum_Amt += ( (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge) * (1 + (A_DEL:VATRate / 100)) ) * |
!                                  Get_ManLoadItems_Info(A_MAL:MLID, 1, A_DEL:DID) / Get_DelItem_s_Totals(A_DEL:DID, 3)
!
!!        db.debugout('Del - ' & A_DEL:DID & ' - charge: ' & ( (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge) * (1 + (A_DEL:VATRate / 100)) ))
!!        db.debugout('Man Items Info: ' & Get_ManLoadItems_Info(A_MAL:MLID, 1, A_DEL:DID))
!!        db.debugout('Items Totals: ' & Get_DelItem_s_Totals(A_DEL:DID, 3))
!!        db.debugout('Ratio: ' & Get_ManLoadItems_Info(A_MAL:MLID, 1, A_DEL:DID) / Get_DelItem_s_Totals(A_DEL:DID, 3))
!             .
!
!!    db.debugout('Del charge: ' & (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge) & |
!!                ',  Man Items: ' & Get_ManLoadItems_Info(A_MAL:MLID, 1, A_DEL:DID) & ',  Del Items: ' & Get_DelItem_s_Totals(A_DEL:DID, 3))
!          OF 1
!             IF A_DEL:Insure = TRUE
!                LOC:Accum_Amt += A_DEL:TotalConsignmentValue * (A_DEL:InsuranceRate / 100)
!             .
!          OF 2
!             LOC:Accum_Amt    += A_DELI:Weight * (A_MALD:UnitsLoaded / A_DELI:Units)
!
!!             MESSAGE('Weight: ' & A_DELI:Weight & ',  A_MALD:UnitsLoaded: ' & A_MALD:UnitsLoaded & ',  A_DELI:Units: ' & A_DELI:Units & '||Add Weight: ' & (A_DELI:Weight * (A_MALD:UnitsLoaded / A_DELI:Units)) & '||LOC:Result: ' & LOC:Result / 100, 'A_DEL:DID: ' & A_DEL:DID)
!          OF 3
!             ! Accumulate Leg info for this delivery
!             ! Not worried about the Filter being slow - this will be SQL.  How would this work on TPS? Use Key if available?
!
!             OPEN(Del_Legs)
!             Del_Legs{PROP:Filter}  = 'A_DELL:DID = ' & A_DEL:DID
!             Del_Legs{PROP:Order}   = 'A_DELL:DID, A_DELL:Leg'
!             SET(Del_Legs)
!             LOOP
!                NEXT(Del_Legs)
!                IF ERRORCODE()
!                   BREAK
!                .
!                ! Accumulate Legs cost total
!                LOC:Accum_Amt   += A_DELL:Cost
!             .
!             CLOSE(Del_Legs)
!          .
!
!          L_DQ:DID          = A_DEL:DID
!          L_DQ:DIID         = A_DELI:DIID
!          L_DQ:MLID         = A_MAL:MLID
!          ADD(LOC:DIDs_Q)
!    .  .
!
!    CLOSE(MAN_Del)
!    POPBIND
!
!!    db.debugout('out')
!
!
!
!    LOC:Result    = LOC:Accum_Amt * 100
!
!!    CASE p:Option
!!    OF 0 OROF 4
!!       ! For every Delivery, sum charges and divide by the ratio of units on the Manifest Load to units on the Manifest
!!       LOC:Result    = LOC:Accum_Amt * 100
!!    OF 1
!!    OF 2
!!    OF 3
!!    .




!           old 2
!    ! (p:MID, p:Option, p:DI_Type, p:Inv_Totals, p:Output)
!    ! (ULONG, BYTE    , BYTE=0   , BYTE=0      , BYTE=0  ),STRING
!    !   p:Option
!    !       0.  Charges total
!    !       1.  Insurance total
!    !       2.  Weight total, for this Manifest!    Uses Units_Loaded
!    !       3.  Extra Legs cost
!    !       4.  Charges total incl VAT
!    !       5.  No. DI's
!    !       6.  Extra Legs cost - inc VAT
!    !   p:DI_Type
!    !       0.  All             (default)
!    !       1.  Non-Broking
!    !       2.  Broking
!    !   p:Inv_Totals
!    !       0.  Collect from Delivery info.
!    !       1.  Collect from Invoiced info.
!
!!    db.debugout('here - Option: ' & p:Option & ',  MID: ' & p:MID)
!    PUSHBIND()
!    BIND('SERI:Broking',SERI:Broking)
!
!    Man_View.Init(MAN_Del, Relate:ManifestLoadAlias)
!    Man_View.AddSortOrder(A_MAL:FKey_MID)
!    Man_View.AppendOrder('A_DEL:DID,A_DELI:DIID')
!    Man_View.AddRange(A_MAL:MID, p:MID)
!
!    CASE p:DI_Type
!    OF 1
!       Man_View.SetFilter('SERI:Broking=0 OR SQL(Broking IS NULL)')         ! SQL !!!???
!    OF 2
!       Man_View.SetFilter('SERI:Broking=1')
!    .
!
!    Man_View.Reset()
!    LOOP
!       IF Man_View.Next() ~= LEVEL:Benign
!          BREAK
!       .
!
!       CLEAR(LOC:Load)
!
!       CASE p:Option
!       OF 0 OROF 4
!          ! DIID can be loaded on more than 1 vehicle...
!          ! Need unique DID, Item and Truck
!          L_DQ:DID          = A_DEL:DID
!          !L_DQ:DIID         = A_DELI:DIID          - we don't want items, we want deliveries
!          L_DQ:MLDID        = A_MALD:MLDID       !A_MAL:MLID
!
!          GET(LOC:DIDs_Q, L_DQ:DID, L_DQ:MLDID)
!          IF ERRORCODE()
!             LOC:Load       = TRUE
!          .
!       OF 1 OROF 3 OROF 5 OROF 6
!          ! Then we only need unique DIDs
!          L_DQ:DID          = A_DEL:DID
!          GET(LOC:DIDs_Q, L_DQ:DID)
!          IF ERRORCODE()
!             LOC:Load       = TRUE
!          .
!       OF 2
!          ! DID will not be unique - required when checking the DID data
!          ! Need unique DID and Manifest Load
!          L_DQ:DID          = A_DEL:DID
!          L_DQ:MLDID        = A_MALD:MLDID
!          GET(LOC:DIDs_Q, L_DQ:DID, L_DQ:MLDID)
!          IF ERRORCODE()
!             LOC:Load       = TRUE
!          .
!       ELSE
!          MESSAGE('Option does not exist!  p:Option: ' & p:Option, 'Get_Manifest_Info', ICON:Hand)
!       .
!
!
!       IF LOC:Load = TRUE
!!    db.debugout('load')
!          !A_DEL:DID
!          !A_DEL:DINo
!          !A_DEL:DIDate
!
!          CASE p:Option
!          OF 0 OROF 4
!             IF p:Inv_Totals = FALSE
!                !LOC:Result    += ((A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge) * (A_MALD:UnitsLoaded / A_DELI:Units)) * 100
!                ! For every Delivery, sum charges and divide by the ratio of units on the Manifest Load to units on the Manifest
!                LOC:DI_Units_Tot  = Get_DelItem_s_Totals(A_DEL:DID, 3)
!
!                IF p:Option = 0
!                   LOC:Temp_Amt   = (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge + A_DEL:AdditionalCharge) * |
!                                     A_MALD:UnitsLoaded / LOC:DI_Units_Tot
!                ELSE
!                   LOC:Temp_Amt   = ( (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge + A_DEL:AdditionalCharge) * |
!                                     (1 + (A_DEL:VATRate / 100)) ) * |
!                                     A_MALD:UnitsLoaded / LOC:DI_Units_Tot
!
!   !        db.debugout('Del - ' & A_DEL:DID & ' - charge: ' & ( (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge) * (1 + (A_DEL:VATRate / 100)) ))
!   !        db.debugout('[Get_Manifest_Info]  Charges: ' & (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge + A_DEL:AdditionalCharge))
!   !        db.debugout('[Get_Manifest_Info]  Man Items Info: ' & Get_ManLoadItems_Info(A_MALD:MLDID, 1, A_DEL:DID))
!   !        db.debugout('[Get_Manifest_Info]  Items Totals: ' & Get_DelItem_s_Totals(A_DEL:DID, 3))
!   !        db.debugout('[Get_Manifest_Info]  DID: ' & A_DEL:DID & ',  Accum_Amt: ' & LOC:Accum_Amt)
!   !        db.debugout('Ratio: ' & Get_ManLoadItems_Info(A_MALD:MLDID, 1, A_DEL:DID) / Get_DelItem_s_Totals(A_DEL:DID, 3))
!                .
!
!                LOC:Accum_Amt += LOC:Temp_Amt
!
!                IF p:Output > 0
!                   ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
!                   Add_Log('MID: ' & p:MID & ', DINo.: ' & A_DEL:DINo & ', DID: ' & A_DEL:DID & ', MLDID: ' & A_MALD:MLDID & ', Amt: ' & LOC:Temp_Amt & '  (tot: ' & LOC:Accum_Amt & ')' & |
!                           ', Units Loaded: ' & A_MALD:UnitsLoaded & ', Units Tot DI: ' & LOC:DI_Units_Tot & ', Option: ' & p:Option & ', Broking: ' & SERI:Broking, 'Get_Manifest_Info',,, 2, 0)
!             .  .
!
!!    db.debugout('Del charge: ' & (A_DEL:DocumentCharge + A_DEL:FuelSurcharge + A_DEL:Charge) & |
!!                ',  Man Items: ' & Get_ManLoadItems_Info(A_MALD:MLDID, 1, A_DEL:DID) & ',  Del Items: ' & Get_DelItem_s_Totals(A_DEL:DID, 3))
!          OF 1
!             IF A_DEL:Insure = TRUE
!                LOC:Accum_Amt    += A_DEL:TotalConsignmentValue * (A_DEL:InsuranceRate / 100)
!             .
!          OF 2
!             IF p:Inv_Totals = FALSE
!                LOC:Accum_Amt    += A_DELI:Weight * (A_MALD:UnitsLoaded / A_DELI:Units)
!
!                IF p:Output > 0
!                   Add_Log('MID: ' & p:MID & ', DINo.: ' & A_DEL:DINo & ', DID: ' & A_DEL:DID & ', MLDID: ' & A_MALD:MLDID & ', Item Weight: ' & A_DELI:Weight & |
!                           ', Units Loaded: ' & A_MALD:UnitsLoaded & ', Item Units: ' & A_DELI:Units & ', Option: ' & p:Option & ', Broking: ' & SERI:Broking, 'Get_Manifest_Info',,, 2, 0)
!             .  .
!!             MESSAGE('Weight: ' & A_DELI:Weight & ',  A_MALD:UnitsLoaded: ' & A_MALD:UnitsLoaded & ',  A_DELI:Units: ' & A_DELI:Units & '||Add Weight: ' & (A_DELI:Weight * (A_MALD:UnitsLoaded / A_DELI:Units)) & '||LOC:Result: ' & LOC:Result / 100, 'A_DEL:DID: ' & A_DEL:DID)
!          OF 3 OROF 6
!             ! Accumulate Leg info for this delivery
!             ! Not worried about the Filter being slow - this will be SQL.  How would this work on TPS? Use Key if available?
!
!
!             Del_View.Init(Del_Legs, Relate:DeliveryLegsAlias)
!             Del_View.AddSortOrder(A_DELL:FKey_DID_Leg)
!             !Del_View.AppendOrder()
!             Del_View.AddRange(A_DELL:DID, A_DEL:DID)
!             !Del_View.SetFilter()
!
!             Del_View.Reset()
!             LOOP
!                IF Del_View.Next() ~= LEVEL:Benign
!                   BREAK
!                .
!                ! Accumulate Legs cost total
!                CASE p:Option
!                OF 3
!                   LOC:Accum_Amt   += A_DELL:Cost
!                ELSE
!                   LOC:Accum_Amt   += A_DELL:Cost * (1 + (A_DELL:VATRate / 100))
!                .
!
!                IF p:Output > 0
!                   Add_Log('MID: ' & p:MID & ', DINo.: ' & A_DEL:DINo & ', DID: ' & A_DEL:DID  & ', Leg Cost: ' & A_DELL:Cost & |
!                           ', Leg VAT Rate: ' & A_DELL:VATRate & ', Option: ' & p:Option & ', Broking: ' & SERI:Broking, 'Get_Manifest_Info',,, 2, 0)
!             .  .
!
!             Del_View.Kill()
!          OF 5
!             LOC:Accum_Amt  += 1
!          .
!
!          L_DQ:DID          = A_DEL:DID
!          L_DQ:DIID         = A_DELI:DIID
!          L_DQ:MLDID        = A_MALD:MLDID
!          ADD(LOC:DIDs_Q)
!    .  .
!
!    Man_View.Kill()
!
!
!    IF p:Inv_Totals = TRUE AND (p:Option = 0 OR p:Option = 4 OR p:Option = 2)
!       ! Accumulate totals from Invoice
!       LOC:Accum_Amt    = 0.0
!
!       SORT(LOC:DIDs_Q, L_DQ:DID)
!
!       Inv_View.Init(Inv_, Relate:InvoiceAlias)
!       Inv_View.AddSortOrder(A_INV:FKey_DID)
!       Inv_View.AddRange(A_INV:DID, L_DQ:DID)
!
!       LOC:Idx  = 0
!       LOOP
!          LOC:Idx += 1
!          GET(LOC:DIDs_Q, LOC:Idx)
!          IF ERRORCODE()
!             BREAK
!          .
!          IF LOC:DID_Last = L_DQ:DID
!             CYCLE
!          .
!          LOC:DID_Last  = L_DQ:DID
!
!          A_INV:DID     = L_DQ:DID
!          IF Access:InvoiceAlias.TryFetch(A_INV:FKey_DID) = LEVEL:Benign
!             CASE p:Option
!             OF 0
!                !A_INV:Insurance
!                LOC:Temp_Amt    = A_INV:Documentation + A_INV:FuelSurcharge + A_INV:AdditionalCharge + A_INV:FreightCharge
!             OF 4
!                LOC:Temp_Amt    = A_INV:Total
!             OF 2
!                LOC:Temp_Amt    = A_INV:Weight
!             .
!
!             LOC:Accum_Amt  += LOC:Temp_Amt
!
!             IF p:Output > 0
!                ! (p:IID, p:DID, p:DINo, p:MID, p:TIN, p:Tran_Date, p:Amount, p:Option)
!                Audit_ManageProfit(A_INV:IID, A_INV:DID, A_INV:DINo, A_INV:MID,, A_INV:InvoiceDate, A_INV:Total, 1)
!
!                Add_Log('MID: ' & p:MID & ', IID (no): ' & A_INV:IID & ', DID: ' & L_DQ:DID & ', MLDID: ' & L_DQ:MLDID & ', Amt: ' & LOC:Temp_Amt & '  (tot: ' & LOC:Accum_Amt & '), Option: ' & p:Option & ', Broking: ' & SERI:Broking, 'Get_Manifest_Info',,, 2, 0)
!    .  .  .  .
!
!    POPBIND()
!
!    LOC:Result    = LOC:Accum_Amt               !* 100
!
!
!!    db.debugout('[Get_Manifest_Info]   LOC:Accum_Amt: ' & LOC:Accum_Amt & ',  p:MID: ' & p:MID & ',  p:Option: ' & p:Option)
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:ManifestLoadAlias.Close()
    Relate:ManifestLoadDeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Relate:DeliveriesAlias.Close()
    Relate:DeliveryLegsAlias.Close()
    Relate:ServiceRequirements.Close()
    Relate:InvoiceAlias.Close()
    Exit
Pre_Process_Q               ROUTINE
    LOC:DID_Last        = 0
    LOC:DI_Units_Tot    = 0
    LOC:Idx             = RECORDS(LOC:DIDs_Q) + 1
    LOOP
       LOC:Idx -= 1
       GET(LOC:DIDs_Q, LOC:Idx)
       IF ERRORCODE()
          BREAK
       .
       IF LOC:DID_Last ~= L_DQ:DID
          IF LOC:DID_Last ~= 0
             GET(LOC:DIDs_Q, LOC:Idx + 1)
             IF ~ERRORCODE()
                L_DQ:TotalUnitsLoaded  = LOC:DI_Units_Tot
                PUT(LOC:DIDs_Q)
!              db.debugout('[Get_Manifest_Info]   DID: ' & L_DQ:DID & ',  Total: ' & L_DQ:TotalUnitsLoaded)
          .  .

          GET(LOC:DIDs_Q, LOC:Idx)
          LOC:DI_Units_Tot  = 0
          LOC:DID_Last      = L_DQ:DID
       .

       LOC:DI_Units_Tot += L_DQ:UnitsLoaded
    .

    GET(LOC:DIDs_Q, LOC:Idx + 1)
    IF ~ERRORCODE()
       L_DQ:TotalUnitsLoaded  = LOC:DI_Units_Tot
       PUT(LOC:DIDs_Q)
!              db.debugout('[Get_Manifest_Info]   DID: ' & L_DQ:DID & ',  Total: ' & L_DQ:TotalUnitsLoaded)
    .
    EXIT
!!! <summary>
!!! Generated from procedure template - Source
!!! Returns 0 or invoice ID (IID)
!!! </summary>
Check_Delivery_Invoiced PROCEDURE  (p:DID, p:Option)       ! Declare Procedure
LOC:IID              ULONG                                 !Invoice Number
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
     Access:_Invoice.UseFile()
    ! (p:DID, p:Option)
    !   p:Option
    !       - 0 - return invoice no
    !       - 1 - return invoice no if printed

    INV:DID     = p:DID
    IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
       LOC:IID  = INV:IID

       IF p:Option = 1
          IF INV:Printed = FALSE
             CLEAR(LOC:IID)
    .  .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:IID)

    Exit

CloseFiles     Routine
    Relate:_Invoice.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Delivery_Rates PROCEDURE (p:DID, p:LTID, p:CID, p:JID, p:DIDate, p:FIDRate, p:SelectedRate, p:Mass, p:MinimiumChargeRate, p:To_Weight, p:Effective_Date)

LOC:Rates_Q          QUEUE,PRE(L_RQ)                       !
Client               STRING(35)                            !
ClientRateType       STRING(100)                           !Load Type
Journey              STRING(70)                            !Description
ToMass               DECIMAL(9)                            !Up to this mass in Kgs
RatePerKg            DECIMAL(10,3)                         !Rate per Kg
MinimiumCharge       DECIMAL(10,2)                         !
Effective_Date       DATE                                  !Effective from date
CID                  ULONG                                 !Client ID
JID                  ULONG                                 !Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
CRTID                ULONG                                 !Client Rate Type ID
Rate_ID              ULONG                                 !Rate Table ID
                     END                                   !
LOC:Screen_Vars      GROUP,PRE(L_SV)                       !
Load_Type            STRING(35)                            !Load Type
Delivery_Journey     STRING(70)                            !Description
Client               STRING(35)                            !
Show_for_Mass        BYTE(1)                               !Show only the applicable rate for the mass
Show_All_Clients     BYTE                                  !
                     END                                   !
LOC:Load_Params      GROUP,PRE(L_LP)                       !
FID                  ULONG                                 !Floor ID
CID                  ULONG                                 !Client ID
JID                  ULONG                                 !Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
CTID                 ULONG                                 !Container Type ID
Mass                 DECIMAL(11,2)                         !
Eff_Date             LONG                                  !
                     END                                   !
LOC:Use_Eff_Date     LONG                                  !
LOC:Client           BYTE(1)                               !Add to Q for this client
LOC:CID              ULONG                                 !Client ID
LOC:ReturnID         ULONG                                 !
LOC:Found_JID        ULONG                                 !Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
LOC:Found_LTID       ULONG                                 !Load Type ID
QuickWindow          WINDOW('Delivery Rates'),AT(,,413,256),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('Delivery_Rates'),SYSTEM
                       PROMPT('Client Rate Type:'),AT(150,2),USE(?LOAD:LoadType:Prompt)
                       ENTRY(@s35),AT(210,2,198,10),USE(CRT:ClientRateType),COLOR(00E9E9E9h),MSG('Load Type'),READONLY, |
  SKIP,TIP('Load Type')
                       SHEET,AT(4,2,405,236),USE(?Sheet1)
                         TAB('Rates'),USE(?Tab1)
                           LIST,AT(9,18,397,215),USE(?List1),VSCROLL,ALRT(MouseLeft2),FORMAT('70L(2)|M~Client Name' & |
  '~@s35@100L(2)|M~Client Rate Type~@s100@40L(2)|M~Journey~@s70@40R(2)|M~To Mass~L@n-12' & |
  '.0@46R(2)|M~Rate Per Kg~L@n-11.3@44R(2)|M~Mini. Charge~L@n-14.2b@44R(2)|M~Effective ' & |
  'Date~L@d5@'),FROM(LOC:Rates_Q)
                         END
                       END
                       BUTTON('&Select'),AT(4,240,,14),USE(?Button_Select),LEFT,ICON('waselect.ico'),FLAT,TIP('Select this rate')
                       CHECK(' Show Rates for all Delivery &Masses'),AT(64,244),USE(L_SV:Show_for_Mass),MSG('Show only ' & |
  'the applicable rate for the mass'),TIP('Show only the applicable rate for the mass<0DH>' & |
  '<0AH>Check this off to see all the Clients rates')
                       CHECK(' Show &All Clients'),AT(208,244),USE(L_SV:Show_All_Clients)
                       BUTTON('&OK'),AT(308,240,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,HIDE,MSG('Accept operation'), |
  TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(360,240,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Consol_View         VIEW(__RatesConsolidated)
!       JOIN(CLI:PKey_CID, CSRA:CID)
!       PROJECT(CLI:ClientName)
    .!  .


Container_View      VIEW(__RatesContainer)
    .

BreakBulk_View      VIEW(__RatesBreakbulk)
    .


Rate_View           VIEW(__Rates)
    .

View_Rates          ViewManager


Client_ContPark_View    VIEW(Clients_ContainerParkDiscounts)
       JOIN(CLI:PKey_CID, CLI_CP:CID)
       PROJECT(CLI:CID, CLI:ClientName)
    .  .

ContainerPark_View      VIEW(__RatesContainerPark)
    .



View_Man            ViewManager

View_Man_CP         ViewManager

    



  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:ReturnID)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Load_Rates_Q                       ROUTINE
    db.debugout('[Delivery_Rates]  Load_Rates_Q (Date: ' & FORMAT(p:DIDate,@d5) & ', Mass: ' & p:Mass & ') - start')

    SETCURSOR(CURSOR:Wait)
    ! (ULONG, ULONG, ULONG, ULONG, LONG, ULONG, *DECIMAL, *DECIMAL, *DECIMAL, *DECIMAL, *LONG),ULONG
    ! (p:DID, p:LTID, p:CID, p:JID, p:DIDate, p:FIDRate, p:SelectedRate, p:Mass, p:MinimiumChargeRate, p:To_Weight, p:Effective_Date)

    QuicKWindow{PROP:Text}  = CLIP(QuicKWindow{PROP:Text}) & ' - ' & CLIP(LOAD2:LoadType)

    ! Based on Load Type
    FREE(LOC:Rates_Q)
    !L_LP:Mass               = p:Mass
    L_LP:Eff_Date           = p:DIDate
    L_LP:FID                = p:FIDRate           ! Need to use rate floor not - DEL:FID
    L_LP:CID                = p:CID
    L_LP:JID                = p:JID

    LOC:Client              = TRUE

    LOOP 2 TIMES
       CASE LOAD2:LoadOption                ! Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
       OF 1                                 ! Container Park
          IF LOAD2:ContainerParkStandard = TRUE
             L_LP:FID                   = LOAD2:FID
             DO Get_CP_Rate
             BREAK
          ELSE
             L_LP:CID                   = p:CID
             DO Get_Container_Park_Rate
          .
       OF 2 OROF 4                          ! Container or Empty Container - both need container type
          L_LP:CTID                     = Get_DelItems_ContainerType(p:DID)
          IF L_LP:CTID ~= 0
             DO Get_Rate
          .
       ELSE                                 ! Consolidated, Full Load, Local Delivery
          DO Get_Rate
       .

       LOC:Client           = FALSE
       IF L_SV:Show_All_Clients = FALSE
          BREAK
    .  .
    SETCURSOR()

    db.debugout('[Delivery_Rates]  Load_Rates_Q - end')
    EXIT
!               old
!Load_Rates_Q                       ROUTINE
!    db.debugout('Delivery_Rates - Load_Rates_Q - start')
!
!    SETCURSOR(CURSOR:Wait)
!    ! (ULONG, *DECIMAL, *DECIMAL, *DECIMAL, *LONG),ULONG
!    ! (p:DID, p:Mass, p:MinimiumChargeRate, p:To_Weight, p:Effective_Date)
!
!    QuicKWindow{PROP:Text}  = CLIP(QuicKWindow{PROP:Text}) & ' - ' & CLIP(LOAD2:LoadType)
!
!    ! Based on Load Type
!    FREE(LOC:Rates_Q)
!    !L_LP:Mass               = p:Mass
!    L_LP:Eff_Date           = DEL:DIDate
!    L_LP:FID                = DEL:FIDRate           ! Need to use rate floor not - DEL:FID
!    L_LP:CID                = DEL:CID
!    L_LP:JID                = DEL:JID
!
!    LOC:Client              = TRUE
!
!    LOOP 2 TIMES
!       ! Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
!
!       CASE LOAD:FullLoad_ContainerPark
!       OF 0                                    ! None
!          CASE LOAD:ContainerOption            ! None, Container, Containerised
!          OF 0 OROF 2                          ! Consolidated, Containerised (Consolidated)
!             DO Get_Rate
!          OF 1                                 ! Container
!             L_LP:CTID      = Get_DelItems_ContainerType(p:DID)
!             IF L_LP:CTID ~= 0
!                DO Get_Rate
!          .  .
!       OF 1                                    ! Full Load
!          DO Get_Rate
!       OF 2                                    ! Container Park
!          DO Get_Container_Park_Rate
!       OF 3                                    ! Container Park Special
!          L_LP:FID          = LOAD:FID
!          DO Get_CP_Rate                       
!          BREAK                                ! NO Client free option
!       .
!
!       LOC:Client           = FALSE
!    .
!    SETCURSOR()
!
!    db.debugout('Delivery_Rates - Load_Rates_Q - end')
!    EXIT
! ---------------------------------------------------------------------------------------------
Get_Rate                               ROUTINE
    CLEAR(LOC:Use_Eff_Date)
    CLEAR(LOC:Found_JID)
    CLEAR(LOC:Found_LTID)
    PUSHBIND

    BIND('RAT:CID', RAT:CID)
    BIND('RAT:LTID', RAT:LTID)
    BIND('RAT:JID', RAT:JID)
    BIND('RAT:Effective_Date', RAT:Effective_Date)
    BIND('RAT:CTID', RAT:CTID)

    View_Rates.Init(Rate_View, Relate:__Rates)
!    View_Rates.AddSortOrder(RAT:CKey_CID_LTID_JID_CTID_EffDate_ToMass)

    db.debugout('[Delivery_Rate]  CID: ' & L_LP:CID & ',  JID: ' & L_LP:JID & ',  Date: ' & FORMAT(L_LP:Eff_Date,@d5))
                        
    View_Rates.AddSortOrder()
    IF LOC:Client = TRUE                                ! Adding for Client
       !View_Rates.AddSortOrder(RAT:FKey_CID)
       !View_Rates.AddSortOrder(RAT:FKey_CID)
       View_Rates.AppendOrder('RAT:LTID, RAT:JID, RAT:CTID, -RAT:Effective_Date, RAT:ToMass')
       !View_Rates.AddRange(RAT:CID, L_LP:CID)
       View_Rates.SetFilter('RAT:CID = ' & L_LP:CID, '0')
    ELSE
       CLEAR(LOC:Rates_Q)
       ADD(LOC:Rates_Q)

       !View_Rates.AddSortOrder(RAT:FKey_JID)
       View_Rates.AppendOrder('RAT:CID, RAT:LTID, RAT:CTID, -RAT:Effective_Date, RAT:ToMass')
       !View_Rates.AddRange(RAT:JID, L_LP:JID)
       View_Rates.SetFilter( 'RAT:JID = ' & L_LP:JID, '0' )
       View_Rates.SetFilter( 'RAT:CID ~= ' & L_LP:CID, '1' )
    .

    View_Rates.SetFilter('RAT:LTID = ' & LOAD2:LTID, '2')
    View_Rates.SetFilter('RAT:Effective_Date < ' & L_LP:Eff_Date + 1, '4')

    IF LOAD2:LoadOption = 2            ! Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
       ! Container Type rate applies
       View_Rates.SetFilter('RAT:CTID=' & L_LP:CTID, '5')
    .

    View_Rates.Reset()
    IF ERRORCODE()
       MESSAGE('Error on Rate file - .RESET||Get_Rate', 'Delivery Rates', ICON:Hand)
    .
    LOOP
       IF View_Rates.Next() ~= LEVEL:Benign
          CLEAR(RAT:Record)
          BREAK
       .

       IF LOC:Client = FALSE                           ! Adding for ALL Clients
          ! If Client changes then reset effective date
          IF LOC:CID ~= RAT:CID
             LOC:CID        = RAT:CID
             CLEAR(LOC:Use_Eff_Date)
             CLEAR(LOC:Found_JID)
             CLEAR(LOC:Found_LTID)
       .  .

       IF LOC:Use_Eff_Date = 0
          LOC:Use_Eff_Date  = RAT:Effective_Date       ! 1st date less then our effective date, i.e. our effective date
       .
       IF RAT:Effective_Date ~= LOC:Use_Eff_Date       ! Outside our effective date now
          !CLEAR(RAT:Record)

          IF LOC:Client = FALSE                        ! Adding for ALL Clients
             CYCLE
          ELSE
             ! We want to add all Journeys rates for this client
             ! If our last saved Journey is not equal to this one then - process it otherwise cycle
             ! Only want last effective date
             IF L_RQ:JID = RAT:JID
                CYCLE
       .  .  .

       IF L_SV:Show_for_Mass = TRUE AND LOC:Client = FALSE
    db.debugout('[Delivery_Rate]  CID: ' & RAT:CID & ',  JID: ' & RAT:JID & ',  LTID: ' & RAT:LTID & ',   F JID: ' & LOC:Found_JID & ', F LTID: ' & RAT:LTID & ', Mass: ' & RAT:ToMass)

          ! We already have a rate for this Journey and Load Type
          IF LOC:Found_JID = RAT:JID AND LOC:Found_LTID = RAT:LTID
             CYCLE
       .  .

       IF L_SV:Show_for_Mass = TRUE AND LOC:Client = FALSE
          IF RAT:ToMass < p:Mass
             CYCLE
       .  .

       LOC:Found_JID        = RAT:JID
       LOC:Found_LTID       = RAT:LTID

!    db.debugout('Delivery_Rates - Adding Rate - CID: ' & RAT:CID & ',  JID: ' & RAT:JID & ',  Date: ' & FORMAT(RAT:Effective_Date,@d5) & ', Mass: ' & RAT:ToMass)

       CLEAR(LOC:Rates_Q)
       L_RQ:ToMass          = RAT:ToMass
       L_RQ:RatePerKg       = RAT:RatePerKg
       L_RQ:MinimiumCharge  = RAT:MinimiumCharge
       !L_RG:Effective_Date  = CPRA:Effective_Date

       L_RQ:CID             = RAT:CID
       L_RQ:JID             = RAT:JID
       L_RQ:CRTID           = RAT:CRTID
       L_RQ:Rate_ID         = RAT:RID
       L_RQ:Effective_Date  = RAT:Effective_Date
       DO Client_and_Journey

       ADD(LOC:Rates_Q)
    .

    View_Rates.Kill()
    POPBIND


    ! Check that our last entry is not the blank separator entry - i.e. we had some other
    ! clients with this rate.
    GET(LOC:Rates_Q, RECORDS(LOC:Rates_Q))
    IF ~ERRORCODE()
       IF L_RQ:Rate_ID = 0
          DELETE(LOC:Rates_Q)
    .  .
    EXIT
Get_Container_Park_Rate     ROUTINE
    DO Get_CP_Rate

    IF CPRA:CPID = 0
       ! No applicable rate was found
       IF LOC:Client = TRUE                                ! Adding for Client
          MESSAGE('No suitable Container Park Rate was found.||See Floors (FID: ' & L_LP:FID & ').', 'Delivery Rates - Container Park Rates', ICON:Exclamation)
       .
    ELSE
       CLEAR(LOC:Use_Eff_Date)
       CLEAR(LOC:Found_JID)
       CLEAR(LOC:Found_LTID)

       PUSHBIND
       BIND('CLI_CP:CID', CLI_CP:CID)
       BIND('CLI_CP:FID', CLI_CP:FID)
       BIND('CLI_CP:Effective_Date', CLI_CP:Effective_Date)

       View_Man.Init(Client_ContPark_View, Relate:Clients_ContainerParkDiscounts)

       ! Notes:  Once we find the effective Discount we need to find the effective Container Park Rate.
       !         The Park rate is found using the Journey, Park Rate Effective Date and ToMass.

       ! First Find the Discount
       View_Man.AddSortOrder()
       IF LOC:Client = TRUE                                ! Adding for Client
          !View_Man.AddSortOrder(CLI_CP:FKey_CID)
          View_Man.AppendOrder( '-CLI_CP:Effective_Date' )
                                  
          !View_Man.AddRange(CLI_CP:CID, L_LP:CID)
          View_Man.SetFilter( 'CLI_CP:CID = ' & L_LP:CID, '0')
          View_Man.SetFilter( 'CLI_CP:FID = ' & L_LP:FID & ' AND CLI_CP:Effective_Date < ' & L_LP:Eff_Date + 1)
       ELSE
          !View_Man.AddSortOrder(CLI_CP:FKey_FID)
          View_Man.AppendOrder( 'CLI:ClientName, -CLI_CP:Effective_Date' )
          !View_Man.AddRange(CLI_CP:FID, L_LP:FID)
          View_Man.SetFilter( 'CLI_CP:FID = ' & L_LP:FID, '0')
          View_Man.SetFilter( 'CLI_CP:CID ~= ' & L_LP:CID & ' AND CLI_CP:FID = ' & L_LP:FID & ' AND CLI_CP:Effective_Date < ' & L_LP:Eff_Date + 1)  ! Was no + 1!!!
       .

       View_Man.Reset()

       LOOP
          IF View_Man.Next() ~= LEVEL:Benign
             CLEAR(CLI_CP:Record)
             BREAK
          .

          IF LOC:Client = FALSE                              ! Adding for ALL Clients
             ! If Client changes then reset effective date
             IF LOC:CID ~= CLI_CP:CID
                LOC:CID        = CLI_CP:CID
                CLEAR(LOC:Use_Eff_Date)
                CLEAR(LOC:Found_JID)
                CLEAR(LOC:Found_LTID)
          .  .

          IF LOC:Use_Eff_Date = 0
             LOC:Use_Eff_Date  = CLI_CP:Effective_Date       ! 1st date less then our effective date, i.e. our effective date
          .
          IF CLI_CP:Effective_Date ~= LOC:Use_Eff_Date       ! Outside our effective date now
             !CLEAR(CLI_CP:Record)

             IF LOC:Client = FALSE                           ! Adding for ALL Clients
                CYCLE
             ELSE
                ! We want to add all Journeys rates for this client
                ! If our last saved Journey is not equal to this one then - process it otherwise cycle
                ! Only want last effective date
                IF L_RQ:JID = CPRA:JID
                   CYCLE
   !                BREAK
          .  .  .

          IF L_SV:Show_for_Mass = TRUE
!       db.debugout('Delivery_Rates - CID: ' & RAT:CID & ',  JID: ' & RAT:JID & ',  LTID: ' & RAT:LTID & ',   F JUD: ' & LOC:Found_JID & ', F LTID: ' & RAT:LTID)

             ! We already have a rate for this Journey and Load Type
             IF LOC:Found_JID = CPRA:JID
                CYCLE
          .  .

          LOC:Found_JID        = CPRA:JID

          ! We have FID, with suitable Effective date
          ! Should only be 1 entry
    db.debugout('[Delivery_Rates]  Get_CP. - CLI:ClientName: ' & CLIP(CLI:ClientName) & ',  CLI_CP:FID: ' & CLI_CP:FID & ',  CLI_CP:CPDID: ' & CLI_CP:CPDID)

          CLEAR(LOC:Rates_Q)
          L_RQ:ToMass          = CPRA:ToMass
          L_RQ:RatePerKg       = CPRA:RatePerKg - (CPRA:RatePerKg * CLI_CP:ContainerParkRateDiscount / 100)
          L_RQ:MinimiumCharge  = CLI_CP:ContainerParkMinimium
          L_RQ:Effective_Date  = CLI_CP:Effective_Date

          L_RQ:CID             = CLI_CP:CID
          L_RQ:JID             = CPRA:JID
          L_RQ:Rate_ID         = CPRA:CPID

          DO Client_and_Journey
          ADD(LOC:Rates_Q)
       .

       View_Man.Kill()

       POPBIND


       SORT(LOC:Rates_Q, L_RQ:Client, L_RQ:Journey, L_RQ:ToMass)
       ! Now move our client to the top if hes here
       I_#      = 0
       Pos_#    = 1
       LOOP
          I_#  += 1
          GET(LOC:Rates_Q, I_#)
          IF ERRORCODE()
             BREAK
          .

          IF I_# <= Pos_#
             CYCLE
          .

          IF L_RQ:CID = L_LP:CID
             DELETE(LOC:Rates_Q)

             ADD(LOC:Rates_Q, Pos_#)

             Pos_#  += 1
    .  .  .
    EXIT
Get_CP_Rate                         ROUTINE
    ! Now Find the Rate to apply the discount to
    CLEAR(LOC:Use_Eff_Date)
    CLEAR(LOC:Found_JID)

    BIND('CPRA:FID', CPRA:FID)
    BIND('CPRA:Effective_Date', CPRA:Effective_Date)

    View_Man_CP.Init(ContainerPark_View, Relate:__RatesContainerPark)

    View_Man_CP.AddSortOrder()          ! CPRA:FKey_FID
    View_Man_CP.AppendOrder( 'CPRA:JID, -CPRA:Effective_Date, CPRA:ToMass' )
    !View_Man_CP.AddRange(CPRA:FID, L_LP:FID)
    View_Man_CP.SetFilter( 'CPRA:FID = ' & L_LP:FID, '0')
    View_Man_CP.SetFilter( 'CPRA:JID = ' & L_LP:JID & ' AND CPRA:Effective_Date < ' & L_LP:Eff_Date + 1)

    View_Man_CP.Reset()
    IF ERRORCODE()
       MESSAGE('Error on CP Rate file - .RESET||Get_CP_Rate', 'Delivery Rates', ICON:Hand)
    .
    LOOP
       IF View_Man_CP.Next() ~= LEVEL:Benign
          CLEAR(CPRA:Record)
          BREAK
       .
   db.debugout('[Delivery_Rate]  CPRA:CPID: ' & CPRA:CPID & ', CPRA:ToMass: ' & CPRA:ToMass & ',  CPRA:Effective_Date: ' & FORMAT(CPRA:Effective_Date,@d5))

       IF LOC:Use_Eff_Date = 0
          LOC:Use_Eff_Date  = CPRA:Effective_Date       ! 1st date less then our effective date, i.e. our effective date
       .

       IF CPRA:Effective_Date ~= LOC:Use_Eff_Date       ! Outside our effective date now
          IF LOAD2:ContainerParkStandard = TRUE
             ! We want to add all Journeys rates for this client
             ! If our last saved Journey is not equal to this one then - process it otherwise cycle
             ! Only want last effective date
             IF L_RQ:JID = CPRA:JID
                CYCLE
             .

             IF L_SV:Show_for_Mass = TRUE
                ! We already have a rate for this Journey and Load Type
                IF LOC:Found_JID = CPRA:JID
                   CYCLE
             .  .

             LOC:Found_JID     = CPRA:JID
          ELSE
             BREAK
       .  .

   db.debugout('[Delivery_Rate]  CPRA:ToMass (' & CPRA:ToMass & ') < p:Mass (' & p:Mass & ')  - Show_for_Mass: ' & L_SV:Show_for_Mass & ', Client: ' & LOC:Client & ', C.P.Std: ' & LOAD2:ContainerParkStandard)

       IF L_SV:Show_for_Mass = TRUE !AND LOC:Client = FALSE
          IF p:Mass ~= 0.0
             IF CPRA:ToMass < p:Mass
                CYCLE
       .  .  .

       IF LOAD2:ContainerParkStandard = TRUE
          CLEAR(LOC:Rates_Q)
          L_RQ:ToMass          = CPRA:ToMass
          L_RQ:RatePerKg       = CPRA:RatePerKg
          L_RQ:MinimiumCharge  = 0.0
          L_RQ:Effective_Date  = CPRA:Effective_Date

          !L_RQ:CID             = CLI_CP:CID
          L_RQ:JID             = CPRA:JID
          L_RQ:Rate_ID         = CPRA:CPID

          DO Client_and_Journey

          L_RQ:Client          = '<No Client - General CP Rate>'
          ADD(LOC:Rates_Q)
       ELSE
          BREAK                             ! We have one
    .  .

    View_Man_CP.Kill()
    EXIT
Client_and_Journey                ROUTINE
    CLI:CID                 = L_RQ:CID
    IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
       L_RQ:Client          = CLI:ClientName
    .

    JOU:JID                 = L_RQ:JID
    IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
       L_RQ:Journey         = JOU:Journey
    .

    CRT:CRTID               = L_RQ:CRTID
    IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
       L_RQ:ClientRateType  = CRT:ClientRateType
    .
    EXIT
! ---------------------------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Delivery_Rates')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOAD:LoadType:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
      PUSHBIND()
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesConsolidated.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesContainer.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesContainerPark.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesBreakbulk.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ClientsRateTypes.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients_ContainerParkDiscounts.UseFile            ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__Rates.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LoadTypes2.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Delivery_Rates',QuickWindow)               ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
    ! (p:DID, p:LTID, p:CID, p:JID, p:DIDate, p:FIDRate, p:SelectedRate, p:Mass, p:MinimiumChargeRate, p:To_Weight, p:Effective_Date)
    ! (ULONG, ULONG, ULONG, ULONG, LONG, ULONG, *DECIMAL, *DECIMAL, *DECIMAL, *DECIMAL, *LONG),ULONG

    LOAD2:LTID                  = p:LTID
    IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
       L_SV:Load_Type           = LOAD2:LoadType
    ELSE
       MESSAGE('Load Type needed.', 'Delivery Rates', ICON:Hand)
       ReturnValue              = LEVEL:Fatal
       POST(EVENT:CloseWindow)
    .


    CLI:CID                     = p:CID
    IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
       L_SV:Client              = CLI:ClientName
    .


    JOU:JID                     = p:JID
    IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
       L_SV:Delivery_Journey    = JOU:Journey
    .

  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
      POPBIND()
  END
  IF SELF.Opened
    INIMgr.Update('Delivery_Rates',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
          CLEAR(p:SelectedRate)
          CLEAR(LOC:ReturnID)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Select
      ThisWindow.Update()
          GET(LOC:Rates_Q, CHOICE(?List1))
          IF ~ERRORCODE() AND L_RQ:Rate_ID ~= 0
             !LOC:Rate_Return      = L_RQ:RatePerKg * 100
             !L_RQ:MinimiumCharge
      
             p:MinimiumChargeRate = L_RQ:MinimiumCharge
             p:To_Weight          = L_RQ:ToMass
             p:Effective_Date     = RAT:Effective_Date
      
             p:SelectedRate       = L_RQ:RatePerKg
             LOC:ReturnID         = L_RQ:Rate_ID
          .
       POST(EVENT:CloseWindow)
    OF ?L_SV:Show_for_Mass
          DO Load_Rates_Q
    OF ?L_SV:Show_All_Clients
          DO Load_Rates_Q
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List1
    CASE EVENT()
    OF EVENT:AlertKey
        POST(EVENT:Accepted, ?Button_Select)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          DO Load_Rates_Q
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!!     ! (p:BID, p:Option)
!!!     ! (p:BID, p:Option)
!!!     ! p:Option
!!!     !   1 - Branch name
!!!     !   2 - AID
!!!     !   3 - FID
!!!     !   4 - GeneralRatesClientID
!!!     !   5 - GeneralRatesTransporterID
!!!     !   6 - GeneralFuelSurchargeClientID
!!! </summary>
Get_Branch_Info      PROCEDURE  (p:BID, p:Option)          ! Declare Procedure
LOC:Returned         STRING(100)                           !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Branches.Open()
     .
     Access:Branches.UseFile()
    ! (p:BID, p:Option)
    ! p:Option
    !   1 - Branch name
    !   2 - AID
    !   3 - FID
    !   4 - GeneralRatesClientID
    !   5 - GeneralRatesTransporterID
    !   6 - GeneralFuelSurchargeClientID
    !   7 - BRA:GeneralTollChargeClientID
    
    
    IF p:BID = 0
       p:BID    = GLO:BranchID
    .

    BRA:BID     = p:BID
    IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
       EXECUTE p:Option
          LOC:Returned     = BRA:BranchName
          LOC:Returned     = BRA:AID
          LOC:Returned     = BRA:FID
          LOC:Returned     = BRA:GeneralRatesClientID
          LOC:Returned     = BRA:GeneralRatesTransporterID          ! 5
          LOC:Returned     = BRA:GeneralFuelSurchargeClientID       ! 6
          LOC:Returned     = BRA:GeneralTollChargeClientID          ! 7
       ELSE
          MESSAGE('Option not known.||Option: ' & p:Option, 'Get_Branch_Info', ICON:Hand)
       .
    ELSE
       MESSAGE('No Branch record available for BID: ' & p:BID & '.||Option: ' & p:Option, 'Get_Branch_Info', ICON:Hand)
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Returned)

    Exit

CloseFiles     Routine
    Relate:Branches.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
GetManifestTotals    PROCEDURE  (ULong pMID, *Decimal pCost, *Decimal pVatRate, *TManifestTotals MT, Byte pOutput = 0) ! Declare Procedure

  CODE
         MT.MID                     = pMID
         !L_MT:MID                     = MAL:MID

         ! Get_Manifest_Info
         ! (p:MID, p:Option, p:DI_Type, p:Inv_Totals, p:Output, p:Info  )
         ! (ULONG, BYTE    , BYTE=0   , BYTE=0      , BYTE=0  , <STRING>),STRING
         !   1       2           3           4           5           6
         !   p:Option
         !       0.  Charges total
         !       1.  Insurance total
         !       2.  Weight total, for this Manifest!    Uses Units_Loaded
         !       3.  Extra Legs cost
         !       4.  Charges total incl VAT
         !       5.  No. DI's
         !       6.  Extra Legs cost - inc VAT
         !   p:DI_Type
         !       0.  All             (default)
         !       1.  Non-Broking
         !       2.  Broking
         !   p:Inv_Totals
         !       0.  Collect from Delivery info.
         !       1.  Collect from Invoiced info.     (only Manifest invoices)

         MT.Delivery_Charges        = Get_Manifest_Info(pMID, 4,,, pOutput)             ! 4 is VAT incl
  
         MT.Cost                    = pCost !MAN:Cost                                              ! VAT is excluded
  
         MT.Legs_Cost               = Get_Manifest_Info(pMID, 6)                         ! VAT is included
         MT.Legs_Cost_VAT           = MT.Legs_Cost - Get_Manifest_Info(pMID, 3,,, pOutput)   ! excluded (3), so minus ex to get VAT
         
         ! p:MT.Legs_Cost  - Not added on example Manifest D22858
  
         MT.Out_VAT                 = MT.Cost * (pVatRate / 100) !MT.Cost * (MAN:VATRate / 100)
         MT.Total_Cost              = MT.Cost + MT.Out_VAT                              ! Add VAT
  
         MT.Delivery_Charges_Ex     = Get_Manifest_Info(pMID, 0,,, pOutput)             ! 4 is VAT EXCL
  
         MT.Gross_Profit            = MT.Delivery_Charges_Ex - MT.Cost - (MT.Legs_Cost - MT.Legs_Cost_VAT)
  
         MT.Gross_Profit_Percent    = (MT.Gross_Profit / MT.Delivery_Charges_Ex) * 100  ! %
  
  
         MT.Total_Weight            = Get_Manifest_Info(pMID, 2,,, pOutput)
  
         MT.Average_C_Per_Kg        = (MT.Delivery_Charges_Ex / MT.Total_Weight) * 100     ! in cents (not rands)
