

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS021.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Client_FuelSurcharge PROCEDURE  (p:CID, p:Date, p:FuelBaseRate, p:CID_Used, p:No_Negatives) ! Declare Procedure
LOC:Buffer           USHORT                                !
LOC:Surcharge        STRING(35)                            !
LOC:FuelBaseRate     DECIMAL(10,4)                         !% cost of total cost
LOC:Count            LONG                                  !
LOC:Default          BYTE                                  !
Tek_Failed_File     STRING(100)

View_FC            VIEW(__RatesFuelCost)
    .


FC_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__RatesFuelCost.Open()
     .
     Access:ClientsAlias.UseFile()
     Access:__RatesFuelCost.UseFile()
    ! Get_Client_FuelSurcharge
    ! (p:CID, p:Date, p:FuelBaseRate, p:CID_Used, p:No_Negatives=1)
    ! (ULONG, LONG=0, <*DECIMAL>, <*ULONG>, BYTE=1),STRING
    !
    ! p:Date        - effective date, if -1 then this is not used in get and last effective charge will be got
    ! p:CID_Used    - this could be different from the p:CID when a client has none of their own rates
    !
    ! Notes:    Buffer is saved and restored for RatesFuelCost

    
    A_CLI:CID   = p:CID
    IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) ~= LEVEL:Benign
       CLEAR(A_CLI:Record)
    .
    IF A_CLI:FuelSurchargeActive = TRUE
       LOC:Buffer   = Access:__RatesFuelCost.SaveBuffer()

       IF p:Date = -1
          ! Date will not be used in the query
       ELSIF p:Date = 0
          p:Date    = TODAY() + 1                               ! Tomorrow or, before tomorrow as it will be in query
       .

       IF p:Date ~= -1
          BIND('FCRA:Effective_Date',FCRA:Effective_Date)
       .

       FC_View.Init(View_FC, Relate:__RatesFuelCost)
       FC_View.AddSortOrder(FCRA:CKey_CID_EffDateDesc)          ! Decending date
       !FC_View.AppendOrder('FCRA:Effective_Date')
       FC_View.AddRange(FCRA:CID, p:CID)

       LOOP
          IF p:Date ~= -1
             FC_View.SetFilter('FCRA:Effective_Date < ' & p:Date + 1)
          .

          FC_View.Reset()
          LOOP
             IF FC_View.Next() ~= LEVEL:Benign
                BREAK
             .

             LOC:Count            += 1

             LOC:Surcharge         = FCRA:FuelSurcharge
             !LOC:FuelBaseRate      = FCRA:FuelBaseRate ???  not used yet/ever?

             BREAK
          .

          IF LOC:Default = FALSE AND LOC:Count <= 0
             ! This client has none defined... use default for Branch
             p:CID          = Get_Branch_Info(, 6)

             FC_View.AddRange(FCRA:CID, p:CID)

             LOC:Default    = TRUE
             CYCLE
          .
          BREAK
       .
       FC_View.Kill()

       UNBIND('FCRA:Effective_Date')

       Access:__RatesFuelCost.RestoreBuffer(LOC:Buffer)

       ! FCRA:FuelSurcharge record is cleared somehow by here???
       IF p:No_Negatives = TRUE AND DEFORMAT(LOC:Surcharge) < 0.0
          LOC:Surcharge     = '0.0'
       .

       IF ~OMITTED(4)
          p:CID_Used  = p:CID           ! Because p:CID is set to the default above if the Clients one is missing 
    .  .
           Relate:ClientsAlias.Close()
           Relate:__RatesFuelCost.Close()

    Return(Round(Deformat(LOC:Surcharge),.01)) ! 2 August 2011 by Victor

       Return(LOC:Surcharge)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Client_FuelBaseRate_old PROCEDURE  (p:FuelCost, p:As_At_Date, p:Clients_FuelBaseRate, p:Base_Date, p:CostAtBase, p:FuelCost_Diff, p:CPL_Change_Per, p:FuelBaseRate, p:N, p:O) ! Declare Procedure
LOC:FuelCostAtBase   DECIMAL(10,3)                         !
LOC:FuelBaseRate     DECIMAL(10,4)                         !
LOC:FuelCost_Diff    DECIMAL(10,3)                         !
LOC:CPL_Change_Per   DECIMAL(12,5)                         !
LOC:O                DECIMAL(12,5)                         !
LOC:N                DECIMAL(12,5)                         !
LOC:Returned_Effective_Date LONG                           !
Tek_Failed_File     STRING(100)

View_FC            VIEW(__RatesFuelCost)
    .


FC_View            ViewManager

  CODE

    ! (p:FuelCost, p:As_At_Date, p:Clients_FuelBaseRate, p:Base_Date, p:CostAtBase, p:FuelCost_Diff, p:CPL_Change_Per,
    !               p:FuelBaseRate, p:N, p:O)
    ! (*DECIMAL, LONG, *DECIMAL, <*LONG>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>)

    ! Revised Base:
    ! S = O / R
    ! = (M + N) / (O + P)
    ! = (M + (M * Q)) / ((M + N) + (100% - M))
    ! = (M + (M * Q)) / ((M + (M * Q)) + (100% - M))
    ! = (M + (M * (Z / X))) / ((M + (M * (Z / X))) + (100% - M))
    ! = (M + (M * ((Y - X) / X))) / ((M + (M * ((Y - X) / X))) + (100% - M))
    !
    ! M = Last System Fuel Base Rate (before Clients effective date)
    ! Y = Clients Fuel Cost
    ! X = Cost of Fuel at last System Base Rate


    !p:As_At_Date            = FCRA:Effective_Date

    ! Y
    !p:FuelCost              = FCRA:FuelCost

    db.debugout('p:FuelCost: ' & p:FuelCost)

    ! Get the Fuel Base Rate in effect As At Date   - M
    LOC:FuelBaseRate          = Get_FuelBaseRate(p:As_At_Date, LOC:Returned_Effective_Date)

    db.debugout('LOC:FuelBaseRate: ' & LOC:FuelBaseRate & '  - Eff Date: ' & FORMAT(LOC:Returned_Effective_Date, @d5))

    ! Get the Fuel Cost in effect as at the Fuel Base Rate's effective date     - X
    LOC:FuelCostAtBase        = Get_FuelCost(LOC:Returned_Effective_Date)

    db.debugout('LOC:FuelCostAtBase: ' & LOC:FuelCostAtBase)

    ! Difference in Cost between passed and the cost at Base Rate time          - Z
    LOC:FuelCost_Diff         = (p:FuelCost - LOC:FuelCostAtBase)

    db.debugout('LOC:FuelCost_Diff: ' & LOC:FuelCost_Diff)

    ! Cost per litre change %                                                   - Q
    LOC:CPL_Change_Per        = LOC:FuelCost_Diff / LOC:FuelCostAtBase

    db.debugout('LOC:CPL_Change_Per: ' & LOC:CPL_Change_Per)

    LOC:N                     = LOC:FuelBaseRate * LOC:CPL_Change_Per         ! - N

    ! O = M + (M * Q) = M + N
    LOC:O                     = LOC:FuelBaseRate + LOC:N                      ! - O

    db.debugout('LOC:O: ' & LOC:O)

    ! S = O / R
    p:Clients_FuelBaseRate    = (LOC:O / (LOC:O + (100 - LOC:FuelBaseRate)))    * 100       ! - S


    ! Return parameters

    !       1           2                   3                   4           5           6                   7
    ! (p:FuelCost, p:As_At_Date, p:Clients_FuelBaseRate, p:Base_Date, p:FuelCost, p:FuelCost_Diff, p:CPL_Change_Per,
    !                   8           9      10
    !           p:FuelBaseRate, LOC:N, LOC:O)
    !
    !       1       2       3       4       5               6           7           8       9           10
    ! (*DECIMAL, LONG, *DECIMAL, <*LONG>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>)

    IF ~OMITTED(4)
       p:Base_Date      = LOC:Returned_Effective_Date
    .
    IF ~OMITTED(5)
       p:CostAtBase     = LOC:FuelCostAtBase
    .
    IF ~OMITTED(6)
       p:FuelCost_Diff  = LOC:FuelCost_Diff
    .
    IF ~OMITTED(7)
       p:CPL_Change_Per = LOC:CPL_Change_Per
    .
    IF ~OMITTED(8)
       p:FuelBaseRate   = LOC:FuelBaseRate
    .
    IF ~OMITTED(9)
       p:N              = LOC:N
    .
    IF ~OMITTED(10)
       p:O              = LOC:O
    .


       Return

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_FuelSurcharge_old PROCEDURE  (p:CID, p:Date, p:RateDate) ! Declare Procedure
LOC:Surcharge        STRING(35)                            !
LOC:FuelCost         DECIMAL(12,4)                         !
LOC:FuelBaseRate     DECIMAL(12,5)                         !
LOC:Buffer           USHORT                                !
LOC:PerOnBase        DECIMAL(12,6)                         !
LOC:Clients_FuelBaseRate DECIMAL(12,5)                     !
Tek_Failed_File     STRING(100)

View_FC            VIEW(__RatesFuelCost)
    .


FC_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__RatesFuelCost.Open()
     .
     Access:ClientsAlias.UseFile()
     Access:__RatesFuelCost.UseFile()
    ! (p:CID, p:Date, p:RateDate)
    ! (ULONG, LONG=0, LONG=0),STRING
    !
    ! p:Date      - effective date
    ! p:RateDate  - purpose of this is to allow to use a specific date for the FuelCost to the RateFuelCost, used on Client form

    A_CLI:CID   = p:CID
    IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) ~= LEVEL:Benign
       CLEAR(A_CLI:Record)
    .

    IF A_CLI:FuelSurchargeActive = TRUE
       LOC:Buffer   = Access:__RatesFuelCost.SaveBuffer()

       IF p:Date = 0
          p:Date    = TODAY()
       .

       BIND('FCRA:Effective_Date',FCRA:Effective_Date)

       FC_View.Init(View_FC, Relate:__RatesFuelCost)
       FC_View.AddSortOrder(FCRA:CKey_CID_EffDateDesc)          ! Decending date
       !FC_View.AppendOrder('FCRA:Effective_Date')
       FC_View.AddRange(FCRA:CID, p:CID)
       FC_View.SetFilter('FCRA:Effective_Date < ' & p:Date + 1)

       FC_View.Reset()

       LOOP
          IF FC_View.Next() ~= LEVEL:Benign
             BREAK
          .

          ! The current cost to FBN
          IF p:RateDate = 0
             LOC:FuelCost       = Get_FuelCost(p:Date)
          ELSE
             LOC:FuelCost       = Get_FuelCost(p:RateDate)
          .

          ! LOC:FuelCost        - system current fuel cost
          ! LOC:FuelBaseRate    - system current effective fuel base rate

          ! % increase on Base is  Cost diff / Client Cost
          ! The clients last base cost = FCRA:FuelCost
          ! eg. (9.000 - 8.000) / 8.000 = .125 = 12.5%
          LOC:PerOnBase         = ((LOC:FuelCost - FCRA:FuelCost) / FCRA:FuelCost)      ! * 100

   ! (p:FuelCost, p:FuelCost_Date, p:As_At_Date, p:Clients_FuelBaseRate, p:Cost_As_At, p:FuelCost_Diff, p:CPL_Change_Per,
   !               p:FuelBaseRate, p:N, p:O)
   ! (*DECIMAL, LONG, LONG, *DECIMAL, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>)

!          Get_Client_FuelBaseRate(FCRA:FuelCost, FCRA:Effective_Date, p:Date, LOC:Clients_FuelBaseRate)
! ************* broken now as called has changed....

          ! % on Base * Base
          ! orig LOC:Surcharge         = LOC:PerOnBase * LOC:FuelBaseRate
          LOC:Surcharge         = LOC:PerOnBase * LOC:Clients_FuelBaseRate

          BREAK
       .

       FC_View.Kill()

       UNBIND('FCRA:Effective_Date')

       Access:__RatesFuelCost.RestoreBuffer(LOC:Buffer)
    .
           Relate:ClientsAlias.Close()
           Relate:__RatesFuelCost.Close()


       Return(LOC:Surcharge)

!!! <summary>
!!! Generated from procedure template - Source
!!!    returns N - increase on base
!!! </summary>
Calc_FuelBaseRate    PROCEDURE  (p:X_Fuel_Cost, p:Y_Fuel_Cost, p:M_Fuel_Base_Rate, p:Z_CPL_Change, p:Q_CPL_Change_Per, p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per) ! Declare Procedure
LOC:X_Fuel_Cost      DECIMAL(20,6)                         !
LOC:Y_Fuel_Cost      DECIMAL(20,6)                         !
LOC:M_Fuel_Base_Rate DECIMAL(20,6)                         !
LOC:Z_CPL_Change     DECIMAL(20,6)                         !
LOC:Q_CPL_Change_Per DECIMAL(20,6)                         !
LOC:N_Increase_On_Base DECIMAL(20,6)                       !
LOC:O_Change_FuelC_Per DECIMAL(20,6)                       !
LOC:S_Revised_FuelBase_Per DECIMAL(20,6)                   !
LOC:T_Revised_OtherBase_Per DECIMAL(20,6)                  !
Tek_Failed_File     STRING(100)


  CODE

    ! (p:X_Fuel_Cost, p:Y_Fuel_Cost, p:M_Fuel_Base_Rate,
    !       1               2               3
    !  p:Z_CPL_Change, p:Q_CPL_Change_Per,  p:O_Change_FuelC_Per,
    !       4               5               6
    !  p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per
    !       7                               8

    ! (*DECIMAL, *DECIMAL, *DECIMAL, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), *DECIMAL  (N)
    !       1       2          3            4           5           6           7           8

    ! Revised Base:
    ! S = O / R
    ! = (M + N) / (O + P)
    ! = (M + (M * Q)) / ((M + N) + (100% - M))
    ! = (M + (M * Q)) / ((M + (M * Q)) + (100% - M))
    ! = (M + (M * (Z / X))) / ((M + (M * (Z / X))) + (100% - M))
    ! = (M + (M * ((Y - X) / X))) / ((M + (M * ((Y - X) / X))) + (100% - M))

    ! Difference in Cost                                                                  - Z  (Y - X)
    LOC:Z_CPL_Change            = p:Y_Fuel_Cost - p:X_Fuel_Cost

    ! Cost per litre change %                                                             - Q
    LOC:Q_CPL_Change_Per        = (LOC:Z_CPL_Change / p:X_Fuel_Cost) * 100

    ! ** Result **                                                                        - N
    LOC:N_Increase_On_Base      = p:M_Fuel_Base_Rate * (LOC:Q_CPL_Change_Per / 100)

    ! O = M + (M * Q) = M + N                                                             - O
    LOC:O_Change_FuelC_Per      = p:M_Fuel_Base_Rate + LOC:N_Increase_On_Base

    ! S = O / R       = O / (O + P)    = O / (O + (100 - M))                              - S
    LOC:S_Revised_FuelBase_Per  = (LOC:O_Change_FuelC_Per / (LOC:O_Change_FuelC_Per + (100 - p:M_Fuel_Base_Rate))) * 100

    ! T = P / R
    LOC:T_Revised_OtherBase_Per = (100 - p:M_Fuel_Base_Rate) / (LOC:O_Change_FuelC_Per + (100 - p:M_Fuel_Base_Rate))
    ! Return parameters

    ! (p:X_Fuel_Cost, p:Y_Fuel_Cost, p:M_Fuel_Base_Rate,
    !       1               2               3
    !  p:Z_CPL_Change, p:Q_CPL_Change_Per, p:O_Change_FuelC_Per,
    !       4               5               6
    !  p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per
    !       7                               8

    ! (*DECIMAL, *DECIMAL, *DECIMAL, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), *DECIMAL  (N)
    !       1       2          3            4           5           6           7           8

    IF ~OMITTED(4)
       p:Z_CPL_Change               = LOC:Z_CPL_Change
    .
    IF ~OMITTED(5)
       p:Q_CPL_Change_Per           = LOC:Q_CPL_Change_Per
    .
    IF ~OMITTED(6)
       p:O_Change_FuelC_Per         = LOC:O_Change_FuelC_Per
    .
    IF ~OMITTED(7)
       p:S_Revised_FuelBase_Per     = LOC:S_Revised_FuelBase_Per
    .
    IF ~OMITTED(8)
       p:T_Revised_OtherBase_Per    = LOC:T_Revised_OtherBase_Per
    .


       Return(LOC:N_Increase_On_Base)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Client_FuelBaseRate_old2 PROCEDURE  (p:FuelCost, p:FuelCost_Date, p:Increase_OnBase, p:As_At_Date, p:Clients_FuelBaseRate, p:Cost_As_At, p:FuelCost_Diff, p:CPL_Change_Per, p:FuelBaseRate, p:O) ! Declare Procedure
LOC:FuelCost_As_At   DECIMAL(15,5)                         !
LOC:FuelBaseRate     DECIMAL(15,5)                         !
LOC:FuelCost_Diff    DECIMAL(15,5)                         !
LOC:CPL_Change_Per   DECIMAL(17,7)                         !
LOC:O                DECIMAL(15,5)                         !
LOC:Clients_FuelBaseRate DECIMAL(15,5)                     !
LOC:Returned_Effective_Date LONG                           !
Tek_Failed_File     STRING(100)


  CODE

    ! (p:FuelCost, p:FuelCost_Date, p:Increase_OnBase, p:As_At_Date, p:Clients_FuelBaseRate, p:Cost_As_At, p:FuelCost_Diff
    !       1             2                 3                4                 5                  6                 7
    !              p:CPL_Change_Per, p:FuelBaseRate, p:O)
    !                     8                9          10
    ! (*DECIMAL, LONG, *DECIMAL, LONG=0, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>)
    !       1      2       3       4         5            6           7           8           9          10

    ! Revised Base:
    ! S = O / R
    ! = (M + N) / (O + P)
    ! = (M + (M * Q)) / ((M + N) + (100% - M))
    ! = (M + (M * Q)) / ((M + (M * Q)) + (100% - M))
    ! = (M + (M * (Z / X))) / ((M + (M * (Z / X))) + (100% - M))
    ! = (M + (M * ((Y - X) / X))) / ((M + (M * ((Y - X) / X))) + (100% - M))
    !
    ! M = Last System Fuel Base Rate (before Clients effective date)
    ! X = Clients Fuel Cost
    ! Y = Cost of Fuel at As_At_Date

    IF p:As_At_Date = 0
       p:As_At_Date           = TODAY()
    .

    ! X - passed p:FuelCost
    db.debugout('p:FuelCost: ' & p:FuelCost)

    ! Get the Fuel Base Rate in effect As At Date   - M
    LOC:FuelBaseRate          = Get_FuelBaseRate(p:FuelCost_Date)

    db.debugout('LOC:FuelBaseRate: ' & LOC:FuelBaseRate & '  - Eff Date: ' & FORMAT(p:FuelCost_Date, @d5))

    ! Get the Fuel Cost in effect on As At Date                                 - Y
    LOC:FuelCost_As_At        = Get_FuelCost(p:As_At_Date)

    db.debugout('LOC:FuelCost_As_At: ' & LOC:FuelCost_As_At)

    ! Difference in Cost between passed and the cost at Base Rate time          - Z  (Y - X)
    LOC:FuelCost_Diff         = (LOC:FuelCost_As_At - p:FuelCost)

    db.debugout('LOC:FuelCost_Diff: ' & LOC:FuelCost_Diff)

    ! Cost per litre change %                                                   - Q
    LOC:CPL_Change_Per        = LOC:FuelCost_Diff / p:FuelCost

    db.debugout('LOC:CPL_Change_Per: ' & LOC:CPL_Change_Per)

    !LOC:N
    p:Increase_OnBase         = LOC:FuelBaseRate * LOC:CPL_Change_Per         ! - N

    ! O = M + (M * Q) = M + N
    LOC:O                     = LOC:FuelBaseRate + p:Increase_OnBase          ! - O

    db.debugout('LOC:O: ' & LOC:O)

    ! S = O / R
    LOC:Clients_FuelBaseRate  = (LOC:O / (LOC:O + (100 - LOC:FuelBaseRate)))    * 100       ! - S


    ! Return parameters

    ! (p:FuelCost, p:FuelCost_Date, p:Increase_OnBase, p:As_At_Date, p:Clients_FuelBaseRate, p:Cost_As_At, p:FuelCost_Diff
    !       1             2                 3                4                 5                  6                 7
    !              p:CPL_Change_Per, p:FuelBaseRate, p:O)
    !                     8                9          10
    ! (*DECIMAL, LONG, *DECIMAL, LONG=0, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>)
    !       1      2       3       4         5            6           7           8           9          10

    IF ~OMITTED(5)
       p:Clients_FuelBaseRate = LOC:Clients_FuelBaseRate      ! S
    .
    IF ~OMITTED(6)
       p:Cost_As_At         = LOC:FuelCost_As_At            ! Y
    .
    IF ~OMITTED(7)
       p:FuelCost_Diff      = LOC:FuelCost_Diff             ! Z
    .
    IF ~OMITTED(8)
       p:CPL_Change_Per     = LOC:CPL_Change_Per            ! Q
    .
    IF ~OMITTED(9)
       p:FuelBaseRate       = LOC:FuelBaseRate              ! M
    .
    IF ~OMITTED(10)
       p:O                  = LOC:O
    .


       Return

