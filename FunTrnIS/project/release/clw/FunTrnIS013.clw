

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS013.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID, p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source)
!!! </summary>
Get_Invoices         PROCEDURE  (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID, p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info, p:LogName) ! Declare Procedure
LOC:Return           STRING(2100)                          ! 
LOC:Total            DECIMAL(12,2)                         ! 
LOC:ToDate           DATE                                  ! 
LOC:Temp_Val         DECIMAL(20,5)                         ! 
LOC:Info             STRING(200)                           ! 
LOC:Omitted          GROUP,PRE(LOC)                        ! 
LogName              STRING(255)                           ! 
                     END                                   ! 
LOC:Log_Name_Opt     BYTE                                  ! 
LOC:String_List      STRING(2100)                          ! 
Tek_Failed_File     STRING(100)

View_Inv            VIEW(InvoiceAlias)
    PROJECT(A_INV:MIDs, A_INV:Total, A_INV:BadDebt, A_INV:IID, A_INV:Documentation, A_INV:FuelSurcharge, A_INV:TollCharge, A_INV:AdditionalCharge, A_INV:FreightCharge, |
            A_INV:VAT, A_INV:Weight, A_INV:InvoiceDate)
    .

View_Inv_Man         VIEW(InvoiceAlias)
    PROJECT(A_INV:MIDs, A_INV:Total, A_INV:BadDebt, A_INV:IID, A_INV:Documentation, A_INV:FuelSurcharge, A_INV:TollCharge, A_INV:AdditionalCharge, A_INV:FreightCharge, |
            A_INV:VAT, A_INV:Weight, A_INV:InvoiceDate)
       JOIN(A_MAN:PKey_MID, A_INV:MID)
       PROJECT(A_MAN:Broking)
    .  .


Inv_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestAlias.Open()
     .
     Access:InvoiceAlias.UseFile()
     Access:ManifestAlias.UseFile()
    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:ID   , p:ToDate, p:Output, p:Limit_On, p:Manifest_Option, p:Source, p:Info  , p:LogName)
    !   1           2       3           4          5       6          7        8            9                 10        11          12
    ! (LONG  , BYTE    , BYTE=0, BYTE=0        , ULONG=0, <LONG>  , BYTE=0  , BYTE=0    , BYTE=0           , BYTE=0  , <STRING>, <STRING>),STRING
    ! p:Option
    !   0.  - Total Charges inc.    (default)
    !   1.  - Excl VAT
    !   2.  - VAT
    !   3.  - Kgs
    !   4.  - IIDs
    !   5.  - CR IIDs
    !   6.  - CR IIDs with IIDs that they belong to, "CRIID->IID" (which is IID of CN then IID of Inv)
    ! p:Type
    !   0.  - All                   (default)
    !   1.  - Invoices                                  Total >= 0.0
    !   2.  - Credit Notes                              Total < 0.0    Instead of <, should be invoices with A_INV:CR_IID
    !   3.  - Credit Notes excl Bad Debts               Total < 0.0
    !   4.  - Credit Notes excl Journals                Total < 0.0
    !   5.  - Credit Notes excl Bad Debts & Journals    Total < 0.0
    ! p:Limit_On    &    p:ID
    !   0.  - Date [& Branch]       (default)
    !   1.  - MID
    !   2.  - DID
    ! p:Manifest_Option
    !   0.  - None                  (default)
    !   1.  - Overnight
    !   2.  - Broking
    ! p:Not_Manifest
    !   0.  - All                   (default)
    !   1.  - Not Manifest
    !   2.  - Manifest

    IF OMITTED(6)                         
       LOC:ToDate     = p:Date              ! We have only 1 date, then this is the From and To
    ELSE
       LOC:ToDate     = p:ToDate
    .

    PUSHBIND()
    BIND('A_INV:InvoiceDate',A_INV:InvoiceDate)
    BIND('A_INV:BID', A_INV:BID)
    BIND('A_INV:BadDebt', A_INV:BadDebt)
    BIND('A_INV:IJID', A_INV:IJID)
    BIND('A_MAN:Broking', A_MAN:Broking)
    BIND('A_INV:Total', A_INV:Total)
    BIND('A_INV:MID', A_INV:MID)

    IF p:Manifest_Option > 0
       Inv_View.Init(View_Inv_Man, Relate:InvoiceAlias)
    ELSE
       Inv_View.Init(View_Inv, Relate:InvoiceAlias)
    .

    CASE p:Limit_On
    OF 0
       Inv_View.AddSortOrder()
       !Inv_View.AppendOrder()
       !Inv_View.AddRange()
       Inv_View.SetFilter('A_INV:InvoiceDate < ' & LOC:ToDate + 1 & ' AND A_INV:InvoiceDate >= ' & p:Date, '1')
       IF p:ID ~= 0
          Inv_View.SetFilter('A_INV:BID = ' & p:ID, '2')
       .
    OF 1
       Inv_View.AddSortOrder(A_INV:FKey_MID)
       Inv_View.AddRange(A_INV:MID, p:ID)
    OF 2
       Inv_View.AddSortOrder(A_INV:FKey_DID)
       Inv_View.AddRange(A_INV:DID, p:ID)
    .

    EXECUTE p:Manifest_Option
       Inv_View.SetFilter('A_MAN:Broking = 0', 'MAN1')    
       Inv_View.SetFilter('A_MAN:Broking = 1 OR SQL(B.Broking IS NULL)', 'MAN1')    ! account for the case where inner join missing Manifests
    .

    ! p:Type
    !   0.  - All
    !   1.  - Invoices                                  Total >= 0.0
    !   2.  - Credit Notes                              Total < 0.0    Instead of <, should be invoices with A_INV:CR_IID
    !   3.  - Credit Notes excl Bad Debts               Total < 0.0
    !   4.  - Credit Notes excl Journals                Total < 0.0
    !   5.  - Credit Notes excl Bad Debts & Journals    Total < 0.0

    CASE p:Type
    OF 3 OROF 5
       Inv_View.SetFilter('A_INV:BadDebt <> 1', 'MAN2')
    .
    CASE p:Type
    OF 4 OROF 5
       Inv_View.SetFilter('A_INV:IJID = 0', 'MAN3')
    .

    CASE p:Type
    OF 1
       Inv_View.SetFilter('A_INV:Total > 0.0', 'MAN4')
    OF 2 TO 5
       Inv_View.SetFilter('A_INV:Total < 0.0', 'MAN4')
    .


    CASE p:Not_Manifest
    OF 1                    ! Only show invoices not Manifest generated
       Inv_View.SetFilter('A_INV:MID = 0 OR SQL(A.MID IS NULL)', 'MAN5')
    OF 2
       Inv_View.SetFilter('A_INV:MID <> 0 AND SQL(A.MID IS NOT NULL)', 'MAN5')
    .


    Inv_View.Reset()
    LOOP
       IF Inv_View.Next() ~= LEVEL:Benign
          BREAK
       .

!       CASE p:Not_Manifest
!       OF 1                    ! Only show invoices not Manifest generated
!          !IF CLIP(A_INV:MIDs) ~= ''
!          IF A_INV:MID ~= 0
!             CYCLE
!          .
!       OF 2
!          IF A_INV:MID = 0
!             CYCLE
!       .  .

!       CASE p:Type
!       OF 1
!          IF A_INV:Total < 0.0
!             CYCLE
!          .
!       OF 2 OROF 3
!          IF A_INV:Total >= 0.0
!             CYCLE
!          .
!
!!          IF p:Type = 3
!!             IF A_INV:BadDebt = TRUE
!!                CYCLE
!!          .  .
!
!    db.debugout('[Get_Invoices]  Credit Note Type - IID: ' & A_INV:IID & ',   Opt: ' & p:Option & ',   A_INV:Total: ' & A_INV:Total & ',     Clock: ' & CLOCK())
!
!       ELSE
!          ! All
!       .

       CASE p:Option
       OF 1
          LOC:Temp_Val  = A_INV:Documentation + A_INV:FuelSurcharge + A_INV:TollCharge + A_INV:AdditionalCharge + A_INV:FreightCharge
       OF 2
          LOC:Temp_Val  = A_INV:VAT
       OF 3
          LOC:Temp_Val  = A_INV:Weight
       OF 4
          !LOC:Temp_Val  = A_INV:IID        ! We want only the string
       OF 5 OROF 6
          !A_INV:CR_IID
       ELSE
          LOC:Temp_Val  = A_INV:Total
       .

       CASE p:Option
       OF 4
          ! String list
          !  (p:Add, p:List, p:Delim, p:Option, p:Prefix)
          Add_to_List(A_INV:IID, LOC:String_List, ',')
          ! Note: LOC:String_List not added to audit or logs, see below
       OF 5
          !Add_to_List(A_INV:IID & '-' & A_INV:CR_IID, LOC:String_List, ',')
          Add_to_List(A_INV:CR_IID, LOC:String_List, ',')
       OF 6          
          ! So the output will be invoice (credit note, and the invoice it is for)
          Add_to_List(A_INV:IID & '->' & A_INV:CR_IID, LOC:String_List, ',')          
       ELSE
          LOC:Total        += LOC:Temp_Val
       .

       IF p:Output = TRUE
          IF p:Option ~= 3
             IF ~OMITTED(11)
                LOC:Info    = p:Info
             .
             IF ~OMITTED(12)
                LOC:LogName         = p:LogName
             ELSE
                ! p:Name_Opt
                !   0.  - None
                !   1.  - Set static name to p:Name
                !   2.  - Use static name
                LOC:Log_Name_Opt    = 2
             .

             !  (p:IID, p:DID, p:DINo, p:MID, p:TIN, p:Tran_Date, p:Amount)
             IF p:Manifest_Option > 0
                Audit_ManageProfit(A_INV:IID, A_INV:DID, A_INV:DINo, A_INV:MID,, A_INV:InvoiceDate, LOC:Temp_Val, p:Source, A_INV:BID, A_MAN:Broking, LOC:Info)
             ELSE
                Audit_ManageProfit(A_INV:IID, A_INV:DID, A_INV:DINo, A_INV:MID,, A_INV:InvoiceDate, LOC:Temp_Val, p:Source, A_INV:BID, 0, LOC:Info)
          .  .

          IF p:Limit_On = 0
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             ! (p:Text, p:Heading, p:Name  , p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt)
             ! (STRING, STRING   , <STRING>, BYTE=0     , BYTE=0    , BYTE=1    , BYTE=1         )
             ! p:Rollover
             !   0.  - No rollover of file allowed
             !   1.  - Rollover allowed (default)
             ! p:Name_Opt
             !   0.  - None
             !   1.  - Set static name to p:Name
             !   2.  - Use static name
             ! p:Date_Time_Opt
             !   0.  - Off
             !   1.  - On (default)
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)

             Add_Log('IID: ' & A_INV:IID & ', InvoiceDate: ' & FORMAT(A_INV:InvoiceDate,@d6) & ', Opt: ' & p:Option & ', Type: ' & p:Type & ', BID: ' & p:ID & ', Not Man.: ' & p:Not_Manifest & |
                   ', Val: ' & LOC:Temp_Val & ', Tot: ' & LOC:Total & ', Broking: ' & A_MAN:Broking, 'Get_Invoices', LOC:LogName,, LOC:Log_Name_Opt, 0)
          ELSE
             Add_Log('IID: ' & A_INV:IID & ', InvoiceDate: ' & FORMAT(A_INV:InvoiceDate,@d6) & ', Opt: ' & p:Option & ', Type: ' & p:Type & ', MID: ' & p:ID & ', Not Man.: ' & p:Not_Manifest & |
                   ', Val: ' & LOC:Temp_Val & ', Tot: ' & LOC:Total & ', Broking: ' & A_MAN:Broking, 'Get_Invoices', LOC:LogName,, LOC:Log_Name_Opt, 0)
    .  .  .

    Inv_View.Kill()
    UNBIND('A_INV:InvoiceDate')
    UNBIND('A_INV:BID')
    UNBIND('A_INV:BadDebt')
    UNBIND('A_INV:IJID')
    UNBIND('A_MAN:Broking')
    UNBIND('A_INV:Total')
    UNBIND('A_INV:MID')
    POPBIND()

    ! p:Type
    !   0.  - All
    !   1.  - Invoices                                  Total >= 0.0
    !   2.  - Credit Notes                              Total < 0.0    Instead of <, should be invoices with A_INV:CR_IID
    !   3.  - Credit Notes excl Bad Debts               Total < 0.0
    !   4.  - Credit Notes excl Journals                Total < 0.0
    !   5.  - Credit Notes excl Bad Debts & Journals    Total < 0.0


    EXECUTE p:Type + 1
       db.debugout('[Get_Invoices]  All Type - Opt: ' & p:Option & ',   Total: ' & LOC:Total)
       db.debugout('[Get_Invoices]  Invoice Type - Opt: ' & p:Option & ',   Total: ' & LOC:Total)
       db.debugout('[Get_Invoices]  Credit Note Type - Opt: ' & p:Option & ',   Total: ' & LOC:Total)
       db.debugout('[Get_Invoices]  Credit Note excl Bad Type - Opt: ' & p:Option & ',   Total: ' & LOC:Total)
       db.debugout('[Get_Invoices]  Credit Note excl Journal Type - Opt: ' & p:Option & ',   Total: ' & LOC:Total)
       db.debugout('[Get_Invoices]  Credit Note excl Bad & Journal Type - Opt: ' & p:Option & ',   Total: ' & LOC:Total)
    ELSE
       db.debugout('[Get_Invoices]  Type <unknown ' & p:Type & '> - Opt: ' & p:Option & ',   Total: ' & LOC:Total)
    .




    CASE p:Option
    OF 4 OROF 5 OROF 6
       LOC:Return  = LOC:String_List
    ELSE
       LOC:Return  = LOC:Total                 ! String
    .

    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:InvoiceAlias.Close()
    Relate:ManifestAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_InvoicesTransporter PROCEDURE  (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID, p:BID, p:ToDate, p:Extra_Inv, p:Invoice_Type, p:Output, p:Source, p:Info, p:LogName, p:Manifest_Type) ! Declare Procedure
LOC:Total            DECIMAL(20,4)                         ! 
LOC:Return           STRING(100)                           ! 
LOC:Bind             BYTE                                  ! 
LOC:Temp_Val         DECIMAL(20,5)                         ! 
LOC:Ommitted_Group   GROUP,PRE(LOC)                        ! 
ToDate               DATE                                  ! 
MID                  ULONG                                 ! Manifest ID
Info                 STRING(200)                           ! 
LogName              STRING(255)                           ! 
                     END                                   ! 
Tek_Failed_File     STRING(100)

View_Inv            VIEW(_InvoiceTransporter)
    PROJECT(INT:DLID, INT:MID, INT:ExtraInv, INT:Cost, INT:TIN, INT:VAT, INT:InvoiceDate, INT:BID, INT:Status, INT:Broking)
    .

View_Inv_Man        VIEW(_InvoiceTransporter)
    PROJECT(INT:DLID, INT:MID, INT:ExtraInv, INT:Cost, INT:TIN, INT:VAT, INT:InvoiceDate, INT:BID, INT:Status, INT:Broking)
       JOIN(A_MAN:PKey_MID, INT:MID)
       PROJECT(A_MAN:Broking)
    .  .

Inv_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_InvoiceTransporter.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestAlias.Open()
     .
     Access:_InvoiceTransporter.UseFile()
     Access:ManifestAlias.UseFile()
    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type, p:Output, p:Source, p:Info, p:LogName, p:Manifest_Type)
    ! (p:Date, p:Option, p:Type, p:Not_Manifest, p:DeliveryLegs, p:MID  , p:BID  , p:ToDate, p:Extra_Inv, p:Invoice_Type,
    ! (LONG  , BYTE    , BYTE=0, BYTE=0        , BYTE=0        , <ULONG>, ULONG=0, <LONG>  , BYTE=0     , BYTE=0        ,
    !   1           2       3         4               5           6        7         8          9           10              11         12       13        14         15
    !  p:Output, p:Source, p:Info  , p:LogName, p:Manifest_Type)
    !  BYTE=0  , BYTE=0  , <STRING>, <STRING> , BYTE=0         ),STRING
    !     11         12       13        14         15
    ! p:Option
    !   0.  - Total Charges inc.
    !   1.  - Excl VAT
    !   2.  - VAT
    ! p:Type
    !   0.  - All
    !   1.  - Invoices                      Total >= 0.0
    !   2.  - Credit Notes                  Total < 0.0
    ! p:DeliveryLegs
    !   0.  - Both
    !   1.  - Del Legs only
    !   2.  - No Del Legs
    ! p:MID
    !   0   = Not for a MID
    !   x   = for a MID
    ! p:Invoice_Type (was Manifest_Type)
    !   0   - All
    !   1   - Overnight
    !   2   - Broking
    ! p:Manifest_Type
    !   0   - All
    !   1   - Overnight
    !   2   - Broking
    ! p:Extra_Inv
    !   Only
    ! p:Source
    !   1   - Management Profit
    !   2   - Management Profit Summary

    !Get_InvoicesTransporter(0, 0, 1, 0, 0, MAN:MID, , , , 2, L_SG:Output,1)
    ! Invoices, Broking      1  2  3  4  5     6    7 8 9  10     11      12
    ! Leg, Broking           1  2  3  4  5     6    7 8 9  10     11      12

    ! (LO:From_Date, 0, 0, , , , L_SG:Branch1, LO:To_Date, , 1, L_SG:Output, 3)
    !           1    2  3 4 5 6   7                8      9  10   11        12

    IF OMITTED(8)
       LOC:ToDate       = p:Date
    ELSE
       LOC:ToDate       = p:ToDate
    .

    IF OMITTED(6)
       LOC:MID          = 0
    ELSE
       LOC:MID          = p:MID
    .

    IF p:Output = TRUE
       IF ~OMITTED(13)
          LOC:Info       = p:Info
       .
       IF ~OMITTED(14)
          LOC:LogName    = p:LogName
       .

       IF CLIP(LOC:LogName) = ''
          ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt)
          ! p:Name_Opt
          !   0.  - None
          !   1.  - Set static name to p:Name
          !   2.  - Use static name
          Add_Log('pDate: ' & FORMAT(LOC:ToDate,@d5) & ', pOption: ' & p:Option & ', pType: ' & p:Type & ', pNot_Manifest: ' & p:Not_Manifest & |
                    ', pDeliveryLegs: ' & p:DeliveryLegs & ', pMID: ' & LOC:MID & ', pBID: ' & p:BID & ', pToDate: ' & FORMAT(p:ToDate,@d5b) & |
                    ', pExtra_Inv: ' & p:Extra_Inv & ', pInvoice_Type: ' & p:Invoice_Type & ', pOutput: ' & p:Output & ', pSource: ' & p:Source & |
                    ', pInfo: ' & CLIP(LOC:Info) & ', pLogName: ' & CLIP(LOC:LogName) & ', pManifest_Type: ' & p:Manifest_Type, 'Get_InvoicesTransporter',,, 2, 0)
       ELSE
          Add_Log('pDate: ' & FORMAT(LOC:ToDate,@d5) & ', pOption: ' & p:Option & ', pType: ' & p:Type & ', pNot_Manifest: ' & p:Not_Manifest & |
                    ', pDeliveryLegs: ' & p:DeliveryLegs & ', pMID: ' & LOC:MID & ', pBID: ' & p:BID & ', pToDate: ' & FORMAT(p:ToDate,@d5b) & |
                    ', pExtra_Inv: ' & p:Extra_Inv & ', pInvoice_Type: ' & p:Invoice_Type & ', pOutput: ' & p:Output & ', pSource: ' & p:Source & |
                    ', pInfo: ' & CLIP(LOC:Info) & ', pLogName: ' & CLIP(LOC:LogName) & ', pManifest_Type: ' & p:Manifest_Type, 'Get_InvoicesTransporter', LOC:LogName,,, 0)
    .  .



    IF p:Date ~= 0 OR LOC:ToDate ~= 0 OR p:BID ~= 0     OR LOC:MID = 0
       LOC:Bind         = TRUE
       PUSHBIND()
       BIND('INT:InvoiceDate',INT:InvoiceDate)
       BIND('INT:BID',INT:BID)
       BIND('INT:Broking',INT:Broking)
       BIND('A_MAN:Broking',A_MAN:Broking)
    .

    IF p:Manifest_Type = 0
       Inv_View.Init(View_Inv, Relate:_InvoiceTransporter)
    ELSE
       Inv_View.Init(View_Inv_Man, Relate:_InvoiceTransporter)
    .
    IF OMITTED(6)
       Inv_View.AddSortOrder()
    ELSE
       Inv_View.AddSortOrder(INT:Fkey_MID)
       Inv_View.AddRange(INT:MID, LOC:MID)
    .

    !Inv_View.AppendOrder('')
    IF p:Date ~= 0              !AND p:MID = 0
       Inv_View.SetFilter('INT:InvoiceDate >= ' & p:Date, '1')
    .
    IF LOC:ToDate ~= 0          !AND p:MID = 0
       Inv_View.SetFilter('INT:InvoiceDate < ' & LOC:ToDate + 1, '2')
    .
    IF p:BID ~= 0
       Inv_View.SetFilter('INT:BID = ' & p:BID,'3')
    .
    EXECUTE p:Invoice_Type
       Inv_View.SetFilter('INT:Broking = 0','4')      
       Inv_View.SetFilter('INT:Broking = 1 OR SQL(A.Broking IS NULL)','4')                    ! NULL!!!
    .
    EXECUTE p:Manifest_Type
       Inv_View.SetFilter('A_MAN:Broking = 0','5')
       Inv_View.SetFilter('A_MAN:Broking = 1','5')
    .

    Inv_View.Reset()
    LOOP
       IF Inv_View.Next() ~= LEVEL:Benign
          BREAK
       .

    !  Taking this out, Broking flag should be used to establish broking now - 15 / 06 / 2006
    ! ? put back in 25 July 06

       CASE p:DeliveryLegs
       OF 2
          IF INT:DLID ~= 0
             CYCLE
          .
       OF 1
          IF INT:DLID = 0
             CYCLE
       .  .

       IF p:Not_Manifest = TRUE             ! Only show invoices not Manifest generated
          IF INT:MID ~= 0
             CYCLE
       .  .

!    db.debugout('[Get_InvoicesTransporter]   1  -  INT:ExtraInv: ' & INT:ExtraInv & ',    p:Extra_Inv: ' & p:Extra_Inv)

       IF p:Extra_Inv ~= INT:ExtraInv
          CYCLE
       .

!    db.debugout('[Get_InvoicesTransporter]   2 Type: ' & p:Type & ',  INT:TIN: ' & INT:TIN & ',  INT:Cost: ' & INT:Cost & ', INT:VAT: ' & INT:VAT & ',  INT:MID: ' & INT:MID & ',  INT:InvoiceDate: ' & FORMAT(INT:InvoiceDate,@d5) & ',  p:Not_Manifest: ' & p:Not_Manifest & ', p:Extra_Inv: ' & p:Extra_Inv)

       CASE p:Type
       OF 1
          IF INT:Cost < 0.0
             CYCLE
          .
       OF 2
          IF INT:Cost >= 0.0
             CYCLE
          .
       ELSE
          ! All
       .

    db.debugout('[Get_InvoicesTransporter]   Type: ' & p:Type & ',  INT:TIN: ' & INT:TIN & ',  INT:Cost: ' & INT:Cost & ', INT:VAT: ' & INT:VAT & |
                ',  INT:MID: ' & INT:MID & ',  INT:InvoiceDate: ' & FORMAT(INT:InvoiceDate,@d5) & ',  p:Not_Manifest: ' & p:Not_Manifest & ', p:Extra_Inv: ' & p:Extra_Inv & ', p:DeliveryLegs: ' & p:DeliveryLegs & ', p:Invoice_Type: ' & p:Invoice_Type)

       CASE p:Option
       OF 1
          LOC:Temp_Val  = INT:Cost
       OF 2
          LOC:Temp_Val  = INT:VAT
       ELSE
          LOC:Temp_Val  = INT:Cost + INT:VAT
       .

       LOC:Total       += LOC:Temp_Val

       IF p:Output = TRUE
          IF ~OMITTED(13)
             LOC:Info       = p:Info
          .
          IF ~OMITTED(14)
             LOC:LogName    = p:LogName
          .

          IF CLIP(LOC:LogName) = ''
             ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt)
             Add_Log('TIN: ' & INT:TIN & ', InvoiceDate: ' & FORMAT(INT:InvoiceDate,@d6) & ', MID: ' & INT:MID & ', Opt: ' & p:Option & ', Type: ' & p:Type & ', Not Man.: ' & p:Not_Manifest & |
                   ', Invoice_Type: ' & p:Invoice_Type & ', Val: ' & LOC:Temp_Val & ', Tot: ' & LOC:Total & ', BID: ' & INT:BID & ', Status: ' & INT:Status & ', DLID: ' & INT:DLID & ', Broking: ' & INT:Broking & ', pSource: ' & p:Source & ', pInfo: ' & CLIP(LOC:Info), 'Get_InvoicesTransporter',,, 2, 0)
          ELSE
             Add_Log('TIN: ' & INT:TIN & ', InvoiceDate: ' & FORMAT(INT:InvoiceDate,@d6) & ', MID: ' & INT:MID & ', Opt: ' & p:Option & ', Type: ' & p:Type & ', Not Man.: ' & p:Not_Manifest & |
                   ', Invoice_Type: ' & p:Invoice_Type & ', Val: ' & LOC:Temp_Val & ', Tot: ' & LOC:Total & ', BID: ' & INT:BID & ', Status: ' & INT:Status & ', DLID: ' & INT:DLID & ', Broking: ' & INT:Broking & ', pSource: ' & p:Source & ', pInfo: ' & CLIP(LOC:Info), 'Get_InvoicesTransporter', LOC:LogName,,, 0)
          .

          ! (ULONG=0, ULONG=0, ULONG=0, ULONG=0, ULONG=0, LONG, *DECIMAL, BYTE, LONG, LONG, <STRING>)
          ! (p:IID, p:DID, p:DINo, p:MID, p:TIN, p:Tran_Date, p:Amount, p:Option, p:BID, p:Broking, p:Info)
          Audit_ManageProfit(,,, INT:MID, INT:TIN, INT:InvoiceDate, LOC:Temp_Val, p:Source, INT:BID, INT:Broking, LOC:Info)
    .  .

    Inv_View.Kill()

    IF LOC:Bind = TRUE
       UNBIND('INT:InvoiceDate')
       UNBIND('INT:BID')
       UNBIND('INT:Broking')
       UNBIND('A_MAN:Broking')
       POPBIND()
    .

    LOC:Return  = LOC:Total
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:_InvoiceTransporter.Close()
    Relate:ManifestAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! ** global **
!!! </summary>
Add_SystemLog        PROCEDURE  (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel) ! Declare Procedure
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:SystemLog.Open()
     .
     Access:SystemLog.UseFile()
    ! (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel)
    ! (BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0)

    IF Access:SystemLog.PrimeRecord() = LEVEL:Benign
       SLO:Severity         = p:Severity

       SLO:AppSection       = '[' & GlobalErrors.GetProcedureName() & ']'       ! Set it to last Window

       IF ~OMITTED(2)
          SLO:AppSection    = CLIP(SLO:AppSection) & '  ' & p:AppSection
       .
       IF ~OMITTED(3)
          SLO:Data1         = p:Data1
       .
       IF ~OMITTED(4)
          SLO:Data2         = p:Data2
       .

       SLO:AccessLevel      = p:AccessLevel

       SLO:EntryDate        = TODAY()
       SLO:EntryTime        = CLOCK()

       SLO:UID              = GLO:UID

       IF Access:SystemLog.TryInsert() = LEVEL:Benign
          ! Added
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:SystemLog.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! alias buffer returned
!!! </summary>
Get_InvTransporter_Outstanding PROCEDURE  (p:TIN)          ! Declare Procedure
LOC:Total_Paid       DECIMAL(15,2)                         ! 
LOC:Total_Credit     DECIMAL(15,2)                         ! 
LOC:Total_Outstanding DECIMAL(15,2)                        ! 
LOC:Total_Outstanding_str STRING(40)                       ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceTransporterAlias.Open()
     .
     Access:InvoiceTransporterAlias.UseFile()
    ! (p:TIN)

    A_INT:TIN                 = p:TIN
    IF Access:InvoiceTransporterAlias.TryFetch(A_INT:PKey_TIN) = LEVEL:Benign
       IF A_INT:Cost < 0.0                                                ! Credit notes are less
          ! Nothing to update
       ELSE                                                             ! Invoice
          LOC:Total_Paid    = Get_TransportersPay_Alloc_Amt( p:TIN, 0 )

          ! Check that Invoice has no Credit Notes for it
          ! p:Options
          !   0.  All
          !   1.  Credits
          !   2.  Debits
          LOC:Total_Credit  = Get_InvTransporter_Credited( p:TIN, 1 )                 ! Reduce owing by Credit Notes

          LOC:Total_Paid   += - (LOC:Total_Credit)


          LOC:Total_Outstanding = (A_INT:Cost + A_INT:VAT) - LOC:Total_Paid


          !     0               1           2               3   
          ! No Payments, Partially Paid, Credit Note, Fully Paid
          IF (A_INT:Cost + A_INT:VAT) <= LOC:Total_Paid
             A_INT:Status     = 3
          ELSIF LOC:Total_Paid ~= 0.0
             A_INT:Status     = 1
          ELSE
             A_INT:Status     = 0
       .  .

       A_INT:StatusUpToDate   = TRUE

       IF Access:InvoiceTransporterAlias.TryUpdate() = LEVEL:Benign
    .  .

    LOC:Total_Outstanding_str   = LOC:Total_Outstanding
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Total_Outstanding_str)

    Exit

CloseFiles     Routine
    Relate:InvoiceTransporterAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Gen_Aging_Transporter PROCEDURE  (p:TID, p:Date, p:Owing, p:Statement_Info, p:MonthEndDay, p:OutPut) ! Declare Procedure
LOC:Result           LONG                                  ! 
LOC:MonthEndDate     LONG                                  ! 
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
LOC:Accum_Group      GROUP,PRE(L_AG)                       ! 
Amount               DECIMAL(10,2)                         ! 
Debit                DECIMAL(10,2)                         ! 
Credit               DECIMAL(10,2)                         ! 
                     END                                   ! 
LOC:Credited         DECIMAL(10,2)                         ! 
LOC:Buffer_ID        USHORT                                ! 
LOC:TIN              ULONG                                 ! Transporter Invoice No.
LOC:Status           BYTE                                  ! used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid
Tek_Failed_File     STRING(100)

Inv_View            VIEW(_InvoiceTransporter)
    PROJECT(INT:TIN, INT:BID, INT:MID, INT:InvoiceDate, INT:InvoiceTime, INT:Cost, INT:VAT, INT:TID, INT:Status, INT:StatusUpToDate)
    .

View_Inv            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_InvoiceTransporter.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TransporterPaymentsAllocations.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceTransporterAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Remittance.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_RemittanceItems.Open()
     .
     Access:_InvoiceTransporter.UseFile()
     Access:TransporterPaymentsAllocations.UseFile()
     Access:InvoiceTransporterAlias.UseFile()
     Access:_Remittance.UseFile()
     Access:_RemittanceItems.UseFile()
    ! (p:TID, p:Date, p:Owing, p:Statement_Info, p:MonthEndDay, p:OutPut)
    ! (ULONG, LONG, <*DECIMAL>, <*STRING>, BYTE=0, BYTE=0),LONG
    !   1       2       3          4        5      6
    !
    ! We should run the invoice maintenance function before running the statements... maybe not though, see code
    !
    ! p:Run_Type
    !   0   - No record generation
    !   1   - Generate record

    PUSHBIND
    BIND('INT:InvoiceDate', INT:InvoiceDate)
    BIND('INT:InvoiceTime', INT:InvoiceTime)
    BIND('INT:InvoiceDateTime', INT:InvoiceDateTime)
    BIND('INT:Status',INT:Status)


    ! If p:MonthEndDay is provided it is taken to be the forthcoming month end otherwise settings are used
    ! Get_Month_End_Date
    ! (p:Date, p:MonthEndDay)
    ! (LONG, BYTE=0),DATE
    LOC:MonthEndDate            = Get_Month_End_Date(p:Date, p:MonthEndDay)

!    db.debugout('[Gen_Statement]   p:MonthEndDay: ' & p:MonthEndDay & ',   LOC:MonthEndDate: ' & FORMAT(LOC:MonthEndDate,@d5))
!    db.debugout('[Gen_Statement]  In - CID: ' & p:CID & ',  Date: ' & FORMAT(p:Date,@d5))

    DO Get_Details


    IF ~OMITTED(4)
       p:Statement_Info         = LOC:Statement_Info
    .

    IF ~OMITTED(3)
       p:Owing                  = L_SI:Total
    .

    UNBIND('INT:InvoiceDate')
    UNBIND('INT:InvoiceTime')
    UNBIND('INT:InvoiceDateTime')
    UNBIND('INT:Status')
    POPBIND
    !LOC:Result ????????????????
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:_InvoiceTransporter.Close()
    Relate:TransporterPaymentsAllocations.Close()
    Relate:InvoiceTransporterAlias.Close()
    Relate:_Remittance.Close()
    Relate:_RemittanceItems.Close()
    Exit
Get_Details                           ROUTINE
    View_Inv.Init(Inv_View, Relate:_InvoiceTransporter)
    View_Inv.AddSortOrder(INT:FKey_TID)
    View_Inv.AppendOrder('INT:InvoiceDate')
    View_Inv.AddRange(INT:TID, p:TID)

    ! Assume they only want current, 2 is fully paid
    ! Note: use of tomorrow as date, with no time specified.

    !       0           1               2              3  
    ! No Payments, Partially Paid, Credit Note, Fully Paid
    ! Fully paid invoices are excluded from the set because they cannot impact on the balance owing

    View_Inv.SetFilter('INT:InvoiceDate < ' & p:Date + 1 & ' AND INT:Status < 3')

    !IF p:Dont_Add = TRUE
    !   Add a filter for Credit notes (2) as they cannot impact on a Age analysis run
    !.

    View_Inv.Reset()
    IF ERRORCODE()
       MESSAGE('[Gen_Statement_Trans.]  Error on view - Error: ' & ERROR())
    .
    LOOP
       IF View_Inv.Next() ~= LEVEL:Benign
          BREAK
       .
       LOC:TIN  = INT:TIN

       ! Here we only have the invoices less than the date specified and not fully paid.
       IF INT:StatusUpToDate = FALSE                                    ! We know we need to check the Invoice Status
          LOC:Buffer_ID = Access:_InvoiceTransporter.SaveBuffer()
          Upd_InvoiceTransporter_Paid_Status(LOC:TIN)       !, 1)  ! Update Fully Paid - NO OPTION IS AVAILABLE
          Access:_InvoiceTransporter.RestoreBuffer(LOC:Buffer_ID)
       .

       IF INT:Status = 2                                                ! If this is a Credit Note, then no amount
          IF INT:CR_TIN = 0
             L_AG:Amount  = INT:Cost + INT:VAT                          ! Credit note with no associated Invoice wont reflect in any other balance
          ELSE
             L_AG:Amount  = 0.0
          .
       ELSE
          L_AG:Debit   = INT:Cost + INT:VAT                             ! Invoice total
          ! Check that Invoice has no Credit Notes for it

          ! (p:TIN, p:Option, p:TIN_to_Ignore, p:Type, p:ToDate)
          LOC:Credited = Get_InvTransporter_Credited( LOC:TIN, 1,,, p:Date )        ! Less any Credit Notes

          L_AG:Debit  += LOC:Credited

          ! (p:ID, p:Option, p:ToDate)
          L_AG:Credit  = Get_TransportersPay_Alloc_Amt( LOC:TIN, 0, p:Date )        ! Payment total for this Invoice

          L_AG:Amount  = L_AG:Debit - L_AG:Credit
       .

!    db.debugout('[Gen_Statement_Trans.]  TID: ' & p:TID & ',  INT:TIN: ' & INT:TIN & ',  INT:Cost: ' & INT:Cost & ',  INT:VAT: ' & INT:VAT)

       CASE Months_Between(LOC:MonthEndDate, INT:InvoiceDate)
       OF 0
          L_SI:Current   += L_AG:Amount

          IF p:OutPut = 1 OR p:OutPut = 2
             ! (p:Text, p:Heading, p:Name)
             Add_Log(',' & INT:TID & ',' & INT:TIN & ',' & FORMAT(INT:InvoiceDate,@d18) & ',' & INT:Cost & ',' & INT:VAT & ',' & LOC:Credited & ',' & L_AG:Credit, 'Current', 'Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv')
          .
       OF 1
          L_SI:Days30    += L_AG:Amount

          IF p:OutPut = 1 OR p:OutPut = 2
             ! (p:Text, p:Heading, p:Name)
             Add_Log(',' & INT:TID & ',' & INT:TIN & ',' & FORMAT(INT:InvoiceDate,@d18) & ',' & INT:Cost & ',' & INT:VAT & ',' & LOC:Credited & ',' & L_AG:Credit, '30 Days', 'Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv')
          .
       OF 2
          L_SI:Days60    += L_AG:Amount

          IF p:OutPut = 1 OR p:OutPut = 2
             ! (p:Text, p:Heading, p:Name)
             Add_Log(',' & INT:TID & ',' & INT:TIN & ',' & FORMAT(INT:InvoiceDate,@d18) & ',' & INT:Cost & ',' & INT:VAT & ',' & LOC:Credited & ',' & L_AG:Credit, '60 Days', 'Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv')
          .
       ELSE
          L_SI:Days90    += L_AG:Amount

          IF p:OutPut = 1 OR p:OutPut = 2
             ! (p:Text, p:Heading, p:Name)
             Add_Log(',' & INT:TID & ',' & INT:TIN & ',' & FORMAT(INT:InvoiceDate,@d18) & ',' & INT:Cost & ',' & INT:VAT & ',' & LOC:Credited & ',' & L_AG:Credit, '90 Days', 'Gen Statement Trans - ' & FORMAT(TODAY(), @d7) & '.csv')
          .
    .  .

    View_Inv.Kill()

    L_SI:Total            = L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current
    EXIT
