

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS010.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Remove_Browse_Layout PROCEDURE                             ! Declare Procedure

  CODE
    REMOVE(LFM_CFile)
    IF ERRORCODE()
       MESSAGE('Error removing file.||Error: ' & CLIP(ERROR()) & '||DB Error: ' & FILEERROR(), 'Remove Browse Layouts', ICON:Hand)
    .
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! note: Ulong returned - divide by 100!
!!! </summary>
Get_ManTruck_Weight  PROCEDURE  (p:MID, p:TTID)            ! Declare Procedure
LOC:Weight_As_Ulong  ULONG                                 ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoad.Open()
     .
     Access:ManifestLoad.UseFile()
    ! (p:MID, p:TTID)

    ! Check through all ManifestLoads for this Truck
    ! The Truck can only be on one ManifestLoad per Manifest

    CLEAR(MAL:Record,-1)
    MAL:MID         = p:MID
    SET(MAL:FKey_TTID, MAL:FKey_TTID)
    LOOP
       IF Access:ManifestLoad.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF MAL:MID ~= p:MID
          BREAK
       .

       IF MAL:TTID ~= p:TTID
          CYCLE
       .

       LOC:Weight_As_Ulong  += Get_ManLoadItems_Info(MAL:MLID,0) * 100

       ! The Truck can only appear on 1 ManifestLoad per Manifest
       BREAK                            
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Weight_As_Ulong)

    Exit

CloseFiles     Routine
    Relate:ManifestLoad.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! note: Ulong returned - divide by 100!
!!! </summary>
Get_DelItemsUnMan_Units PROCEDURE  (p:DID)                 ! Declare Procedure
LOC:Weight_As_Ulong  ULONG                                 ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItems.Open()
     .
     Access:DeliveryItems.UseFile()
    ! (p:DID)
    DELI:DID    = p:DID
    SET(DELI:FKey_DID_ItemNo, DELI:FKey_DID_ItemNo)
    LOOP
       IF Access:DeliveryItems.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF DELI:DID ~= p:DID
          BREAK
       .

       IF DELI:Weight < DELI:VolumetricWeight
          LOC:Weight_As_Ulong   += DELI:VolumetricWeight * 100
       ELSE
          LOC:Weight_As_Ulong   += DELI:Weight           * 100
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Weight_As_Ulong)

    Exit

CloseFiles     Routine
    Relate:DeliveryItems.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! Can be called as procedure - has own error messages
!!! </summary>
Set_DeliveryMID      PROCEDURE  (p:DID, p:MID, p:Reset)    ! Declare Procedure
LOC:Result           LONG                                  ! 
LOC:MID              ULONG,DIM(2)                          ! Manifest ID
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Deliveries.Open()
     .
     Access:Deliveries.UseFile()
!    ! (p:DID, p:MID, p:Reset)
!    DEL:DID         = p:DID
!    IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
!       IF p:Reset = TRUE
!          ! Then we are re-setting the MID to zero - happens if all items removed from Manifest
!          ! Also could be used to bypass tests below and set any MID
!       ELSE
!          IF DEL:MID ~= 0 AND DEL:MID ~= p:MID
!             ! Check that this is really the case by scanning the LoadItems
!             LOC:MID[1]    = Get_Delivery_ManIDs(DEL:DID, 1)
!             LOC:MID[2]    = Get_Delivery_ManIDs(DEL:DID, 2)
!
!             IF LOC:MID[1] = 0
!                ! We have no Manifested items - report this error to Log but allow setting MID now
!                     ! (p:Text, p:Heading, p:Name)
!                Add_Log('There are no Manifested Items but Delivery has MID set (MID: ' & DEL:MID & '), we are going to set new MID: ' & p:MID, 'Set_DeliveryMID 1')
!             ELSE
!                IF LOC:MID[2] = 0 AND LOC:MID[1] = p:MID
!                   ! If MID 2 is zero AND MID 1 is same as our set MID then allow to continue and report ?????
!                   Add_Log('The MID on the Items is same as one being set now (MID: ' & p:MID & '), but Delivery has MID: ' & DEL:MID & ' set.  We are going to set new Delivery MID: ' & p:MID, 'Set_DeliveryMID 1')
!                ELSE
!                   ! Our MID 2 is not zero OR MID 1 is not the same as pass MID - we have 2 MIDs for this Delivery!
!                   ! Report and fail! ?????
!                   Add_Log('The MID on the Items is NOT SAME as the one to set (set MID: ' & p:MID & ')! Delivery has MID: ' & DEL:MID & ' set.  Item 1 MID: ' & LOC:MID[1] & ',  Item 2 MID: ' & LOC:MID[2], 'Set_DeliveryMID 1')
!                   LOC:Result  = -1
!       .  .  .  .
!
!       IF LOC:Result < 0
!          MESSAGE('Unable to update the Delivery (DID: ' & p:DID & ') with this Manifest (MID: ' & p:MID & ')||A different Manifest has been assigned! (MID: ' & DEL:MID & ')||Item 1 MID: ' & LOC:MID[1] & '|Item 2 MID: ' & LOC:MID[2] & '||Please write down this error and the MIDs and tell support.', 'Delivery Update - Manifest ID', ICON:Hand)
!       ELSE
!          DEL:MID       = p:MID
!          IF Access:Deliveries.TryUpdate() ~= LEVEL:Benign
!             LOC:Result = -2
!             MESSAGE('Unable to update the Delivery (DID: ' & p:DID & ') with this Manifest (MID: ' & p:MID & ')||Update failed: ' & Access:Deliveries.GetError() & '||Please write down this error and tell support.', 'Delivery Update - Manifest ID', ICON:Hand)
!       .  .
!    ELSE
!       LOC:Result       = -3
!       MESSAGE('Unable to update the Delivery (DID: ' & p:DID & ') with this Manifest (MID: ' & p:MID & ')||Cannot find Delivery record.||Please write down this error and the MIDs and tell support.', 'Delivery Update - Manifest ID', ICON:Hand)
!    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:Deliveries.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! ** global **
!!! </summary>
Add_Audit            PROCEDURE  (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel) ! Declare Procedure
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Audit.Open()
     .
     Access:Audit.UseFile()
    ! (p:Severity, p:AppSection, p:Data1, p:Data2, p:AccessLevel)
    ! (BYTE=0, <STRING>, <STRING>, <STRING>, BYTE=0)

    IF Access:Audit.PrimeRecord() = LEVEL:Benign
       AUD:Severity         = p:Severity

       IF ~OMITTED(2)
          AUD:AppSection    = p:AppSection & '  [' & GlobalErrors.GetProcedureName() & ']'
       ELSE
          AUD:AppSection    = GlobalErrors.GetProcedureName()        ! Set it to last Window
       .
       IF ~OMITTED(3)
          AUD:Data1         = p:Data1
       .
       IF ~OMITTED(4)
          AUD:Data2         = p:Data2
       .

       AUD:AccessLevel      = p:AccessLevel

       IF Access:Audit.TryInsert() = LEVEL:Benign
          ! Added
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:Audit.Close()
    Exit
