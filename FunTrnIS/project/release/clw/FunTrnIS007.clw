

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS007.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! divide by 100
!!! </summary>
Get_DeliveryAdditionalCharges PROCEDURE  (p:DID)           ! Declare Procedure
LOC:Total            ULONG                                 ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAdditionalCharges.Open()
     .
     Access:DeliveriesAdditionalCharges.UseFile()
    ! (p:DID)
    CLEAR(DELA:RECORD, -1)
    DELA:DID    = p:DID
    SET(DELA:FKey_DID, DELA:FKey_DID)
    LOOP
       IF Access:DeliveriesAdditionalCharges.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF DELA:DID ~= p:DID
          BREAK
       .

       LOC:Total    += DELA:Charge * 100
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Total)

    Exit

CloseFiles     Routine
    Relate:DeliveriesAdditionalCharges.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! return the totals for DI's on the passed combination
!!! </summary>
Get_Delivery_Info    PROCEDURE  (p:DELCID, p:Option)       ! Declare Procedure
LOC:DeliveryComposition_Group GROUP,PRE(L_DC)              ! Totals so far
Items                ULONG                                 ! Total no. of items
Weight               DECIMAL(15,2)                         ! Total weight
Volume               DECIMAL(12,3)                         ! Total volume
Deliveries           ULONG                                 ! Total deliveries
First_DID            ULONG                                 ! Delivery ID
                     END                                   ! 
LOC:DID              ULONG                                 ! Delivery ID
Tek_Failed_File     STRING(100)

Del_View            VIEW(DeliveriesAlias)
    PROJECT(A_DEL:DID)
    .

View_Del            ViewManager


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
     Access:DeliveriesAlias.UseFile()
    ! (ULONG, BYTE=0),STRING
    ! (p:DELCID, p:Option)
    ! p:Option
    !   0 - Return Items, Weight and no of Deliveries
    !   ? - ?
    !
    ! Used
    !   Update_Deliveries, Update_DeliveryCompositions, Browse_DeliveryComposition

    CLEAR(LOC:DeliveryComposition_Group)

    IF p:Option ~= 0
       MESSAGE('The option does not exists - Option: ' & p:Option, 'Get Delivery Info.', ICON:Exclamation)
    ELSE
       View_Del.Init(Del_View, Relate:DeliveriesAlias)
       View_Del.AddSortOrder(A_DEL:FKey_DELCID)             !DELC:PKey_DELCID)
       View_Del.AppendOrder('A_DEL:DID')
       View_Del.AddRange(A_DEL:DELCID, p:DELCID)            !DELC:DELCID, p:DELCID)
       !View_Del.SetFilter()

       View_Del.Reset()
       LOOP
          IF View_Del.Next() ~= LEVEL:Benign
             BREAK
          .

          LOC:DID   = A_DEL:DID
          IF L_DC:First_DID = 0
             L_DC:First_DID = A_DEL:DID
          .

          ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache)
          ! (ULONG, BYTE, <ULONG>, <*STRING>, BYTE=0),ULONG
          !
          ! p:Option
          !   0. Weight of all DID items - Volumetric considered
          !   1. Total Items not loaded on the DID    - Manifest
          !   2. Total Items loaded on the DID        - Manifest
          !   3. Total Items on the DID
          !   4. Total Items not loaded on the DID    - Tripsheet
          !   5. Total Items loaded on the DID        - Tripsheet
          !   6. Weight of all DID items - real weight
          !   7. Total Items not delivered on the DID - Tripsheet     (similar to 4)
          !
          ! p:DIID is passed to check a single DIID
          ! p:Items_DIID_List is passed to return a list of DIIDs for the given Option

          IF p:Option = 0
             L_DC:Items       += Get_DelItem_s_Totals(LOC:DID, 3)
             L_DC:Weight      += Get_DelItem_s_Totals(LOC:DID, 6) / 100
             !L_DC:Volume       = Get_DelItem_s_Totals(LOC:DID, 0) / 100
             L_DC:Deliveries  += 1
       .  .

       View_Del.Kill()
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:DeliveryComposition_Group)

    Exit

CloseFiles     Routine
    Relate:DeliveriesAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! (p:DID, p:CID, p:JID, p:CRTID, p:FID, p:Mass, p:Eff_Date, <*p:Mincharge>, <p:Setup>, <p:To_Weight>, <p:Effective_Date>, <p:RateID>, p:Quiet)
!!! </summary>
Get_Delivery_Rate    PROCEDURE  (p:DID, p:CID, p:JID, p:CRTID, p:FID, p:Mass, p:Eff_Date, <*p:Mincharge>, <p:Setup>, <p:To_Weight>, <p:Effective_Date>, <p:RateID>, p:Quiet) ! Declare Procedure
LOC:RG               GROUP,PRE(L_RG)                       ! 
RatePerKg            LIKE(RAT:RatePerKg)                   ! Rate per Kg
ClientRateType       STRING(100)                           ! Load Type
                     END                                   ! 
LOC:CTID             ULONG                                 ! Container Type ID
LOC:FID              ULONG                                 ! used for CP rates
LOC:CID              ULONG                                 ! Client ID
LOC:MinimiumCharge   DECIMAL(10,2)                         ! 
LOC:MinimiumCharge_SmallestWeight DECIMAL(10,2)            ! 
LOC:Try_Setup        BYTE                                  ! 
LOC:Use_Eff_Date     LONG                                  ! 
LOC:Static_Group     GROUP,PRE(L_SG),STATIC                ! 
GeneralRatesClientID ULONG                                 ! Client ID
Setup_Loaded_ID      ULONG                                 ! this is changed whenever the setup is changed and will cause the statics to reload
                     END                                   ! 
LOC:Ret_Group        GROUP,PRE(L_RG)                       ! 
Setup                BYTE                                  ! 
To_Weight            DECIMAL(9)                            ! Up to this mass in Kgs
Effective_Date       DATE                                  ! Effective from this date
                     END                                   ! 
Tek_Failed_File     STRING(100)

Rate_View               VIEW(__Rates)
    .
View_Rates              ViewManager




Client_ContPark_View    VIEW(Clients_ContainerParkDiscounts)
    .
View_Man                ViewManager



ContainerPark_View      VIEW(__RatesContainerPark)
    .
View_Man_CP             ViewManager


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsRateTypes.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Setup.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__RatesContainerPark.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__Rates.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Clients_ContainerParkDiscounts.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:LoadTypes2.Open()
     .
     Access:ClientsRateTypes.UseFile()
     Access:Setup.UseFile()
     Access:__RatesContainerPark.UseFile()
     Access:__Rates.UseFile()
     Access:Clients_ContainerParkDiscounts.UseFile()
     Access:LoadTypes2.UseFile()
    ! (p:DID, p:CID, p:JID, p:CRTID, p:FID, p:Mass, p:Eff_Date, <*p:Mincharge>, <p:Setup>,
    ! (ULONG, ULONG, ULONG, ULONG, ULONG, *DECIMAL, LONG=0, <*DECIMAL>, <*BYTE>,
    !   1       2       3       4    5       6          7       8           9
    ! <p:To_Weight>, <p:Effective_Date>, <p:RateID>, p:Quite)
    ! <*DECIMAL>, <*LONG>, <*ULONG>, BYTE=0),ULONG
    !    10         11       12       13

    CLEAR(LOAD2:Record)
    CLEAR(CLI_CP:Record)
    CLEAR(CPRA:Record)
    CLEAR(RAT:Record)

    IF p:Eff_Date = 0
       p:Eff_Date   = TODAY()
    .

    CRT:CRTID       = p:CRTID
    IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) ~= LEVEL:Benign
       db.debugout('[Get_Delivery_Rate]  Failed to get Clients Rate Type - ID: ' & p:CRTID)
    ELSE
       L_RG:ClientRateType = CRT:ClientRateType   ! 4 June 2012
      
       PUSHBIND
       BIND('RAT:CID', RAT:CID)
       BIND('RAT:CRTID', RAT:CRTID)
       BIND('RAT:JID', RAT:JID)
       BIND('RAT:CTID', RAT:CTID)
       BIND('RAT:Effective_Date', RAT:Effective_Date)

       DO Setup_Check

       LOAD2:LTID   = CRT:LTID
       IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) ~= LEVEL:Benign
         db.debugout('[Get_Delivery_Rate]  Load Type NOT loaded: ' & CRT:LTID)
          CLEAR(LOAD2:Record)
       .


     db.debugout('[Get_Delivery_Rate]  Load Option: ' & LOAD2:LoadOption & ',  LTID: ' & LOAD2:LTID)

       CASE LOAD2:LoadOption                ! Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
       OF 1                                 ! Container Park
          IF LOAD2:ContainerParkStandard = TRUE
             LOC:FID                    = LOAD2:FID
             DO Get_CP_Rate

             IF CPRA:CPID ~= 0
                L_RG:RatePerKg          = CPRA:RatePerKg
                L_RG:To_Weight          = CPRA:ToMass
                L_RG:Effective_Date     = CPRA:Effective_Date
                LOC:MinimiumCharge      = 0.0
             .
          ELSE
             LOC:CID                    = p:CID
             DO Get_Container_Park_Rate

             IF CLI_CP:CPDID = 0
                LOC:CID                 = L_SG:GeneralRatesClientID
                DO Get_Container_Park_Rate
                IF CLI_CP:CPDID ~= 0
                   L_RG:Setup           = TRUE
             .  .

             IF CLI_CP:CPDID = 0
                IF p:Quiet ~= 1
                   MESSAGE('No rate found for the Floor - Container Park Rates (see Floor ID: ' & p:FID & ').||Standard Consolidated rates will be used.', 'Get Delivery Rate - Setup Data', ICON:Hand)
                .                
                DO Get_Rate
          .  .
       OF 2 OROF 4                          ! Container or Empty Container - both need container type
          LOC:CTID                      = Get_DelItems_ContainerType(p:DID)
          db.debugout('[Get_Del._Rate]  LOC:CTID: ' & LOC:CTID & ',  p:DID: ' & p:DID)
          IF LOC:CTID ~= 0
             DO Get_Rate
          .
          ! Otherwise Use General Container rates... which are?  Container rates for the setup CID
       ELSE                                 ! Consolidated, Full Load, Local Delivery
          DO Get_Rate

          IF RAT:RID = 0
             LOC:Try_Setup  = TRUE
       .  .


       IF LOC:Try_Setup = TRUE
          p:CID       = L_SG:GeneralRatesClientID
          DO Get_Rate
          IF RAT:RID = 0
             !MESSAGE('No rate found for the Client or Setup Container Type (see Setup - Client ID:' & p:CID & ').', 'Get Delivery Rate - Setup Data', ICON:Hand)
          ELSE
             L_RG:Setup     = TRUE
       .  .


       ! (p:DID, p:CID, p:JID, p:LTID, p:FID, p:Mass, p:Eff_Date, <*p:Mincharge>, <p:Setup>, <p:To_Weight>, <p:Effective_Date>, <p:RateID>)
       ! (ULONG, ULONG, ULONG, ULONG, ULONG, *DECIMAL, LONG=0, <*DECIMAL>, <BYTE>, <*DECIMAL>, <*LONG>, <*ULONG>),ULONG
       !   1       2       3       4    5       6          7       8           9       10         11       12
       IF ~OMITTED(8)
          IF LOC:MinimiumCharge = 0.0
             LOC:MinimiumCharge = LOC:MinimiumCharge_SmallestWeight
          .
          p:Mincharge       = LOC:MinimiumCharge
       .
       IF ~OMITTED(9)
          p:Setup           = L_RG:Setup
       .
       IF ~OMITTED(10)
          p:To_Weight       = L_RG:To_Weight
       .
       IF ~OMITTED(11)
          p:Effective_Date  = L_RG:Effective_Date
       .

       IF ~OMITTED(12)
          CASE LOAD2:LoadOption             ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
          OF 1
             IF LOAD2:ContainerParkStandard = TRUE
                p:RateID   = CPRA:CPID
             ELSE
                p:RateID   = CLI_CP:CPDID
             .
          ELSE
             p:RateID      = RAT:RID
       .  .

       UNBIND('RAT:CID')
       UNBIND('RAT:CRTID')
       UNBIND('RAT:JID')
       UNBIND('RAT:CTID')
       UNBIND('RAT:Effective_Date')
       POPBIND
    .
!                   old
!    ! (p:DID, p:CID, p:JID, p:CRTID, p:FID, p:Mass, p:Eff_Date, <*p:Mincharge>, <p:Setup>,
!    ! (ULONG, ULONG, ULONG, ULONG, ULONG, *DECIMAL, LONG=0, <*DECIMAL>, <*BYTE>,
!    !   1       2       3       4    5       6          7       8           9
!    ! <p:To_Weight>, <p:Effective_Date>, <p:RateID>)
!    ! <*DECIMAL>, <*LONG>, <*ULONG>),ULONG
!    !    10         11       12
!
!    PUSHBIND
!
!    CLEAR(LOAD2:Record)
!    CLEAR(CLI_CP:Record)
!    CLEAR(CPRA:Record)
!    CLEAR(RAT:Record)
!
!    BIND('RAT:LTID', RAT:LTID)
!    BIND('RAT:JID', RAT:JID)
!    BIND('RAT:CTID', RAT:CTID)
!    BIND('RAT:Effective_Date', RAT:Effective_Date)
!
!    IF p:Eff_Date = 0
!       p:Eff_Date   = TODAY()
!    .
!
!    CRT:CRTID       = p:CRTID
!    IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
!       LOAD2:LTID   = CRT:LTID
!       IF Access:LoadTypes2.TryFetch(LOAD2:PKey_LTID) = LEVEL:Benign
!          DO Setup_Check
!
!          CASE LOAD2:LoadOption                         ! Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
!          OF 0                                      
!             IF LOAD:ContainerOption = 1                ! None, Container, Containerised
!                ! Container Type rate applies
!
!                LOC:CTID       = Get_DelItems_ContainerType(p:DID)
!                IF LOC:CTID ~= 0
!                   DO Get_Rate
!                .          ! Otherwise Use General Container rates... which are?  Container rates for the setup CID
!             ELSE
!                DO Get_Rate
!             .             ! Otherwise Use General rates.
!
!             IF RAT:RID = 0
!                LOC:Try_Setup  = TRUE                   ! See setup
!             .
!          OF 1
!             ! Full Load rate applies
!             DO Get_Rate
!                       
!             IF RAT:RID = 0                             ! Use General rates.
!                LOC:Try_Setup  = TRUE                   ! See setup
!             .
!          OF 2
!             ! Container Park Rates apply - we need a floor with rates - if no rate found revert to consolidated!
!             LOC:CID   = p:CID
!             DO Get_Container_Park_Rate
!
!             IF CLI_CP:CPDID = 0
!                LOC:CID   = L_SG:GeneralRatesClientID
!                DO Get_Container_Park_Rate
!                IF CLI_CP:CPDID ~= 0
!                   L_RG:Setup   = TRUE
!             .  .
!
!             IF CLI_CP:CPDID = 0
!                MESSAGE('No rate found for the Floor - Container Park Rates (see Floor ID: ' & p:FID & ').||Standard Consolidated rates will be used.', 'Get Delivery Rate - Setup Data', ICON:Hand)
!                DO Get_Rate
!             .
!          OF 3
!             ! Container Park Special Rates - rates are applied without discount
!             LOC:FID                   = LOAD:FID
!             DO Get_CP_Rate
!
!             IF CPRA:CPID ~= 0
!                L_RG:RatePerKg         = CPRA:RatePerKg
!                L_RG:To_Weight         = CPRA:ToMass
!                L_RG:Effective_Date    = CPRA:Effective_Date
!                LOC:MinimiumCharge     = 0.0
!       .  .  .
!
!       IF LOC:Try_Setup = TRUE
!          p:CID       = L_SG:GeneralRatesClientID
!          DO Get_Rate
!          IF RAT:RID = 0
!             !MESSAGE('No rate found for the Client or Setup Container Type (see Setup - Client ID:' & p:CID & ').', 'Get Delivery Rate - Setup Data', ICON:Hand)
!          ELSE
!             L_RG:Setup   = TRUE
!    .  .  .
!
!
!    ! (p:DID, p:CID, p:JID, p:LTID, p:FID, p:Mass, p:Eff_Date, <*p:Mincharge>, <p:Setup>, <p:To_Weight>, <p:Effective_Date>, <p:RateID>)
!    ! (ULONG, ULONG, ULONG, ULONG, ULONG, *DECIMAL, LONG=0, <*DECIMAL>, <BYTE>, <*DECIMAL>, <*LONG>, <*ULONG>),ULONG
!    !   1       2       3       4    5       6          7       8           9       10         11       12
!    IF ~OMITTED(8)
!       p:Mincharge      = LOC:MinimiumCharge
!    .
!
!    IF ~OMITTED(9)
!       p:Setup          = L_RG:Setup
!    .
!    IF ~OMITTED(10)
!       p:To_Weight      = L_RG:To_Weight
!    .
!    IF ~OMITTED(11)
!       p:Effective_Date = L_RG:Effective_Date
!    .
!
!    IF ~OMITTED(12)
!       ! Option - Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
!       ikb
!       CASE LOAD:FullLoad_ContainerPark
!       OF 2
!          p:RateID      = CLI_CP:CPDID
!       OF 3
!          p:RateID      = CPRA:CPID
!       ELSE
!          p:RateID      = RAT:RID
!    .  .
!
!    POPBIND
!
!!    RETURN( LOC:RatePerKg )        - done in Template
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:RG)

    Exit

CloseFiles     Routine
    Relate:ClientsRateTypes.Close()
    Relate:Setup.Close()
    Relate:__RatesContainerPark.Close()
    Relate:__Rates.Close()
    Relate:Clients_ContainerParkDiscounts.Close()
    Relate:LoadTypes2.Close()
    Exit
Get_Rate                               ROUTINE
    CLEAR(LOC:Use_Eff_Date)
    CLEAR(LOC:MinimiumCharge_SmallestWeight)

    View_Rates.Init(Rate_View, Relate:__Rates)
    View_Rates.AddSortOrder()
    View_Rates.AppendOrder('RAT:CRTID, RAT:JID, RAT:CTID, -RAT:Effective_Date, RAT:ToMass')

   db.debugout('[Get_Rate]  CID: ' & p:CID & ',  CRTID: ' & p:CRTID & ',  JID: ' & p:JID & ',  Date: ' & FORMAT(p:Eff_Date,@d5) & ',  LoadOption: ' & LOAD2:LoadOption)
                        
    !View_Rates.AddRange(RAT:CID, p:CID)

    View_Rates.SetFilter('RAT:CID=' & p:CID, '0')
    View_Rates.SetFilter('RAT:CRTID=' & p:CRTID, '1')
    View_Rates.SetFilter('RAT:JID=' & p:JID, '2')
    View_Rates.SetFilter('RAT:Effective_Date<' & p:Eff_Date+1, '3')

    IF LOAD2:LoadOption = 2            ! Consolidated, Container Park, Container, Full Load, Empty Container, Local Delivery
       ! Container Type rate applies
       View_Rates.SetFilter('RAT:CTID=' & LOC:CTID, '4')
    .

    View_Rates.Reset()
    LOOP
       IF View_Rates.Next() ~= LEVEL:Benign
          CLEAR(RAT:Record)
          BREAK
       .

       IF LOC:MinimiumCharge_SmallestWeight = 0.0
          LOC:MinimiumCharge_SmallestWeight = RAT:MinimiumCharge
       .

    db.debugout('[Get_Rate]  Rate Loop - CID: ' & RAT:CID & ',  JID: ' & RAT:JID & ',  Date: ' & FORMAT(RAT:Effective_Date,@d5) & ',  ToMass: ' & RAT:ToMass & ',  RatePerKg: ' & RAT:RatePerKg & ',  RID: ' & RAT:RID)

       IF LOC:Use_Eff_Date = 0
          LOC:Use_Eff_Date  = RAT:Effective_Date       ! 1st date less then our effective date, i.e. our effective date
       .
       IF RAT:Effective_Date ~= LOC:Use_Eff_Date       ! Outside our effective date now
          CLEAR(RAT:Record)
          BREAK
       .

       IF RAT:ToMass < p:Mass
          CYCLE
       .

    db.debugout('[Get_Rate]  Rate - CID: ' & RAT:CID & ',  JID: ' & RAT:JID & ',  Date: ' & FORMAT(RAT:Effective_Date,@d5) & ',  ToMass: ' & RAT:ToMass & ',  RatePerKg: ' & RAT:RatePerKg)

       L_RG:RatePerKg       = RAT:RatePerKg
       L_RG:To_Weight       = RAT:ToMass
       L_RG:Effective_Date  = RAT:Effective_Date
       LOC:MinimiumCharge   = RAT:MinimiumCharge

       BREAK
    .

    View_Rates.Kill()
    EXIT
! ------------------------------------------------------------------------------------------------------------
Get_Container_Park_Rate     ROUTINE
    LOC:FID     = p:FID
    DO Get_CP_Rate

    IF CPRA:CPID = 0
       ! No applicable rate was found
       !MESSAGE('No suitable Container Park Rate was found.||See Floors (FID: ' & L_LP:FID & ').', 'Delivery Rates - Container Park Rates', ICON:Exclamation)
    ELSE
       CLEAR(LOC:Use_Eff_Date)

       PUSHBIND
       BIND('CLI_CP:CID', CLI_CP:CID)
       BIND('CLI_CP:FID', CLI_CP:FID)
       BIND('CLI_CP:Effective_Date', CLI_CP:Effective_Date)

       View_Man.Init(Client_ContPark_View, Relate:Clients_ContainerParkDiscounts)

       ! Notes:  Once we find the effective Discount we need to find the effective Container Park Rate.
       !         The Park rate is found using the Journey, Park Rate Effective Date and ToMass.

       ! First Find the Discount
       View_Man.AddSortOrder()          ! CLI_CP:FKey_CID
       View_Man.AppendOrder( '-CLI_CP:Effective_Date' )
       !View_Man.AddRange(CLI_CP:CID, LOC:CID)
       View_Man.SetFilter('CLI_CP:CID = ' & LOC:CID, '0')
       View_Man.SetFilter('CLI_CP:FID = ' & p:FID & ' AND CLI_CP:Effective_Date < ' & p:Eff_Date + 1,'1')

       View_Man.Reset()

       LOOP
          IF View_Man.Next() ~= LEVEL:Benign
             CLEAR(CLI_CP:Record)
             BREAK
          .

          IF LOC:Use_Eff_Date = 0
             LOC:Use_Eff_Date  = CLI_CP:Effective_Date       ! 1st date less then our effective date, i.e. our effective date
          .
          IF CLI_CP:Effective_Date ~= LOC:Use_Eff_Date       ! Outside our effective date now
             CLEAR(CLI_CP:Record)

             BREAK
          .

          ! We have FID, with suitable Effective date
          ! Should only be 1 entry
          L_RG:RatePerKg       = (CPRA:RatePerKg - (CPRA:RatePerKg * CLI_CP:ContainerParkRateDiscount / 100))
          L_RG:To_Weight       = CPRA:ToMass
          L_RG:Effective_Date  = CLI_CP:Effective_Date
          LOC:MinimiumCharge   = CLI_CP:ContainerParkMinimium

    db.debugout('[Get_Delivery_Rate - Get_Container_Park_Rate]  LOC:CID: ' & LOC:CID & ',  CLI_CP:CID: ' & CLIP(CLI_CP:CID) & ',  CLI_CP:FID: ' & CLI_CP:FID & |
            ', Discount: ' & CLI_CP:ContainerParkRateDiscount & ', CPRA:RatePerKg: ' & CPRA:RatePerKg & |
            ',   Result RatePerKg: ' & L_RG:RatePerKg & ',  MinimiumCharge: ' & LOC:MinimiumCharge)
          BREAK
       .

       View_Man.Kill()
       POPBIND
    .
    EXIT
Get_CP_Rate                         ROUTINE
    ! Now Find the Rate to apply the discount to
    CLEAR(LOC:Use_Eff_Date)
    PUSHBIND

    BIND('CPRA:FID', CPRA:FID)
    BIND('CPRA:JID', CPRA:JID)
    BIND('CPRA:Effective_Date', CPRA:Effective_Date)

    View_Man_CP.Init(ContainerPark_View, Relate:__RatesContainerPark)

    View_Man_CP.AddSortOrder()          ! CPRA:FKey_FID
    View_Man_CP.AppendOrder( '-CPRA:Effective_Date, CPRA:ToMass' )
    !View_Man_CP.AddRange(CPRA:FID, LOC:FID)
    View_Man_CP.SetFilter( 'CPRA:FID = ' & LOC:FID,'0')
    View_Man_CP.SetFilter( 'CPRA:JID = ' & p:JID & ' AND CPRA:Effective_Date < ' & p:Eff_Date + 1,'1')

    View_Man_CP.Reset()

    LOOP
       IF View_Man_CP.Next() ~= LEVEL:Benign
          CLEAR(CPRA:Record)
          BREAK
       .

       IF LOC:Use_Eff_Date = 0
          LOC:Use_Eff_Date  = CPRA:Effective_Date       ! 1st date less then our effective date, i.e. our effective date
       .
       IF CPRA:Effective_Date ~= LOC:Use_Eff_Date       ! Outside our effective date now
          CLEAR(CPRA:Record)
          BREAK
       .

       IF p:Mass ~= 0.0
          IF CPRA:ToMass < p:Mass
             CYCLE
       .  .

!    db.debugout('[Get_Delivery_Rate]  CPRA:CPID: ' & CPRA:CPID)

       BREAK                                ! We have one
    .

    View_Man_CP.Kill()
    POPBIND
    EXIT
! ------------------------------------------------------------------------------------------------------------
Setup_Check                         ROUTINE
    IF L_SG:Setup_Loaded_ID ~= GLO:Setup_Loaded_ID
       SET(Setup)
       NEXT(Setup)
       IF ~ERRORCODE()
          L_SG:Setup_Loaded_ID      = GLO:Setup_Loaded_ID

          L_SG:GeneralRatesClientID = SET:GeneralRatesClientID
    .  .
    EXIT
! ------------------------------------------------------------------------------------------------------------
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_DelLegs_Info     PROCEDURE  (p:DID, p:Option)          ! Declare Procedure
LOC:Cost_No          DECIMAL(15,2)                         ! 
LOC:Return_Str       STRING(35)                            ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryLegsAlias.Open()
     .
     Access:DeliveryLegsAlias.UseFile()
    ! (p:DID, p:Option)
    ! p:Option          0. = Number
    !                   1. = Total Cost
    !                   2. = Total Cost inc.
    !                   3. = TIDs

    CLEAR(LOC:Cost_No)

    CLEAR(A_DELL:Record, -1)
    A_DELL:DID    = p:DID
    SET(A_DELL:FKey_DID_Leg, A_DELL:FKey_DID_Leg)
    LOOP
       IF Access:DeliveryLegsAlias.TryNext() ~= LEVEL:Benign
          BREAK
       .

       IF A_DELL:DID ~= p:DID
          BREAK
       .

       EXECUTE p:Option + 1
          LOC:Cost_No      += 1
          LOC:Cost_No      += A_DELL:Cost
          LOC:Cost_No      += A_DELL:Cost * (1 + (A_DELL:VATRate / 100))
          LOC:Return_Str    = CLIP(LOC:Return_Str) & ',' & A_DELL:TID
    .  .

    IF p:Option ~= 3
       LOC:Return_Str       = LOC:Cost_No
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return_Str)

    Exit

CloseFiles     Routine
    Relate:DeliveryLegsAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! (p:FID, p:JID, p:Eff_Date, p:Mass, p:Rate, p:Cache_Type)
!!! </summary>
Get_Floor_Rate       PROCEDURE  (p:FID, p:JID, p:Eff_Date, p:Mass, p:Rate, p:Cache_Type) ! Declare Procedure
LOC:Use_Eff_Date     LONG                                  ! 
LOC:CPID             ULONG                                 ! Container Park Rate ID
LOC:Cache_Time       LONG,STATIC                           ! statis
LOC:FID              ULONG,STATIC                          ! static
LOC:Cache_Q          QUEUE,PRE(L_CQ),STATIC                ! static
CPID                 ULONG                                 ! Container Park Rate ID
JID                  ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
RatePerKg            DECIMAL(10,4)                         ! Rate per Kg
FID                  ULONG                                 ! Floor ID
                     END                                   ! 
LOC:Cache_Valid      BYTE                                  ! 
Tek_Failed_File     STRING(100)

ContainerPark_View      VIEW(__RatesContainerPark)
    .


View_Man_CP         ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__RatesContainerPark.Open()
     .
     Access:__RatesContainerPark.UseFile()
    ! (p:FID, p:JID, p:Eff_Date, p:Mass, p:Rate, p:Cache_Type)
    ! (ULONG, ULONG, LONG, <*DECIMAL>, <*DECIMAL>, BYTE=0), ULONG
    ! p:Cache_Type
    !       x - 
    !       2 - When caching, Mass is ignored.  Only use for checks not for getting rate

    IF p:Cache_Type = 2
       IF RECORDS(LOC:Cache_Q) > 250 OR ABS(CLOCK() - LOC:Cache_Time) > 6000
          db.debugout('[Get_Floor_Rate]  Cache freed.  Recs.: ' & RECORDS(LOC:Cache_Q))
          FREE(LOC:Cache_Q)
       ELSE
          L_CQ:FID              = p:FID                     ! Check if we have any of these
          GET(LOC:Cache_Q, L_CQ:FID)
          IF ERRORCODE() AND LOC:FID ~= p:FID               ! Extra check that we didnt check this FID last check and find nothing...
          ELSE
             LOC:Cache_Valid    = TRUE

             ! If we dont have this item in the cache - then we dont have it... unless we freed the Q above
             L_CQ:FID           = p:FID
             L_CQ:JID           = p:JID
             GET(LOC:Cache_Q, L_CQ:FID, L_CQ:JID)
             IF ERRORCODE()
                ! Not in Cache
             ELSE
                LOC:CPID        = L_CQ:CPID                 ! This is any value
    .  .  .  .


    IF LOC:Cache_Valid = FALSE
       db.debugout('[Get_Floor_Rate]  Searching... Clock: ' & CLOCK() & ' , Cache_Type: ' & p:Cache_Type & ',  LOC:FID: ' & LOC:FID & ',  p:FID: ' & p:FID)
       LOC:FID      = p:FID

       PUSHBIND
       CLEAR(LOC:Use_Eff_Date)

       BIND('CPRA:JID', CPRA:JID)
       BIND('CPRA:Effective_Date', CPRA:Effective_Date)

       View_Man_CP.Init(ContainerPark_View, Relate:__RatesContainerPark)

       View_Man_CP.AddSortOrder(CPRA:FKey_FID)
       View_Man_CP.AppendOrder( 'CPRA:FKey_JID, -CPRA:Effective_Date, CPRA:ToMass' )
       View_Man_CP.AddRange(CPRA:FID, p:FID)

       IF p:Cache_Type = 2
          View_Man_CP.SetFilter( 'CPRA:Effective_Date <= ' & p:Eff_Date)
       ELSE
          View_Man_CP.SetFilter( 'CPRA:JID = ' & p:JID & ' AND CPRA:Effective_Date <= ' & p:Eff_Date)
       .

       View_Man_CP.Reset()

       LOOP
          IF View_Man_CP.Next() ~= LEVEL:Benign
             CLEAR(CPRA:Record)
             BREAK
          .

          IF LOC:Use_Eff_Date = 0
             LOC:Use_Eff_Date  = CPRA:Effective_Date       ! 1st date less then our effective date, i.e. our effective date
          .
          IF CPRA:Effective_Date ~= LOC:Use_Eff_Date       ! Outside our effective date now
             CLEAR(CPRA:Record)
             BREAK
          .

          IF ~OMITTED(4)
             IF p:Mass ~= 0.0
                IF CPRA:ToMass < p:Mass
                   CYCLE
          .  .  .

          IF p:Cache_Type = 2
             ! Caching all then
             L_CQ:FID   = p:FID
             L_CQ:JID   = CPRA:JID
             GET(LOC:Cache_Q, L_CQ:JID, L_CQ:FID)
             IF ERRORCODE()
                L_CQ:FID        = p:FID
                L_CQ:JID        = CPRA:JID
                L_CQ:CPID       = CPRA:CPID
                L_CQ:RatePerKg  = CPRA:RatePerKg

             db.debugout('[Get_Floor_Rate]  Caching - p:JID: ' & p:JID & ',  p:FID: ' & p:FID)
                ADD(LOC:Cache_Q)

                LOC:Cache_Time  = CLOCK()
             .
          ELSE
            BREAK                                ! We have one
       .  .
       View_Man_CP.Kill()

       POPBIND

       IF p:Cache_Type = 2
          L_CQ:FID          = p:FID
          L_CQ:JID          = p:JID
          GET(LOC:Cache_Q, L_CQ:FID, L_CQ:JID)
          IF ~ERRORCODE()
             CPRA:CPID      = L_CQ:CPID
       .  .

       IF CPRA:CPID ~= 0
          IF ~OMITTED(5)
             p:Rate         = CPRA:RatePerKg
          .
          LOC:CPID          = CPRA:CPID
    .  .

    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:CPID)

    Exit

CloseFiles     Routine
    Relate:__RatesContainerPark.Close()
    Exit
