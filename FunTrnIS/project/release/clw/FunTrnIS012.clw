

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS012.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! currently checks CRTID
!!! </summary>
Check_Rates          PROCEDURE  (p:CRTID)                  ! Declare Procedure
LOC:Return           ULONG                                 ! 
LOC:Where            CSTRING(255)                          ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__Rates.Open()
     .
     Access:__Rates.UseFile()
    ! p:CRTID

    LOC:Where   = 'CRTID = ' & p:CRTID

    __Rates{PROP:SQL}   = 'SELECT COUNT(*) FROM __Rates WHERE ' & LOC:Where
    IF ERRORCODE()
       MESSAGE('SQL Failed.', 'Check_Rates', ICON:Hand)
    ELSE
       NEXT(__Rates)
       IF ERRORCODE()

       ELSE
          LOC:Return    = RAT:RID
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:__Rates.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! TripSheetDeliveries & DeliveryItems & Deliveries
!!! </summary>
Get_Info_SQL         PROCEDURE  (p:Option, p:ID, p:ResultString, p:SQL, p:Items) ! Declare Procedure
LOC:Records          LONG                                  ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_SQLTemp.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetDeliveries.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Commodities.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:PackagingTypes.Open()
     .
     Access:_SQLTemp.UseFile()
     Access:TripSheetDeliveries.UseFile()
     Access:DeliveryItems.UseFile()
     Access:Commodities.UseFile()
     Access:PackagingTypes.UseFile()
    ! (p:Option, p:ID, p:ResultString, p:SQL, p:Items)
    ! (BYTE=0, ULONG, <*STRING>, <STRING>, LONG=1), LONG, PROC
    !   p:Option
    !       0 - Unique set of Delivery Dates returned in a passed string
    !       1 - Unique set of Delivery Commodities returned in a passed string
    !       2 - Unique set of Delivery Packagings returned in a passed string
    !       3 - Unique set of Delivery Dates & Times returned in a passed string - eg. Date, Time, Date, Time
    !       4 - Count of DIs for CID
    !               p:Items, here means ??
    !               p:SQL, passed in SQL executed
    !       5 - Unique set of Delivery Date/Times returned in a passed string - eg Date Time, Date Time (different output to 3)
    !       6 - Unique set of Container Nos. returned in a passed string

    CASE p:Option
    OF 0
       _SQLTemp{PROP:SQL}  = 'SELECT DISTINCT <39>1<39>, <39>1<39>, ' & |
             'CAST(CAST(DATEPART(yy, DeliveredDateAndTime) AS varchar) + <39>-<39> + ' & |
                  'CAST(DATEPART(mm, DeliveredDateAndTime) AS varchar) + <39>-<39> + ' & |
                  'CAST(DATEPART(dd, DeliveredDateAndTime) AS varchar) ' & |
             ' AS DATETIME) ' & |
                                 ' FROM TripSheetDeliveries ' & |
                                 'WHERE DIID IN ' & |
                                 '(SELECT DIID FROM DeliveryItems WHERE DID = ' & p:ID & ')'
    OF 1
       _SQLTemp{PROP:SQL}  = 'SELECT DISTINCT CMID FROM DeliveryItems WHERE DID = ' & p:ID
    OF 2
       _SQLTemp{PROP:SQL}  = 'SELECT DISTINCT PTID FROM DeliveryItems WHERE DID = ' & p:ID
    OF 3
       _SQLTemp{PROP:SQL}  = 'SELECT DISTINCT <39>1<39>, <39>1<39>, ' & |
                             'DeliveredDateAndTime ' & |
                                 ' FROM TripSheetDeliveries ' & |
                                 'WHERE DIID IN ' & |
                                 '(SELECT DIID FROM DeliveryItems WHERE DID = ' & p:ID & ')'
    OF 4
       IF OMITTED(4)
          _SQLTemp{PROP:SQL}  = 'SELECT COUNT(*) FROM Deliveries WHERE CID = ' & p:ID
!          db.debugout('[Get_Info_SQL]  Omit SQL: ' & _SQLTemp{PROP:SQL})
       ELSE
          _SQLTemp{PROP:SQL}  = CLIP(p:SQL)         ! 'SELECT COUNT(*) FROM Deliveries WHERE CID = ' & p:ID
!          db.debugout('[Get_Info_SQL]  SQL: ' & _SQLTemp{PROP:SQL})
       .
    OF 5
       _SQLTemp{PROP:SQL}  = 'SELECT DISTINCT <39>1<39>, <39>1<39>, ' & |
                             'DeliveredDateAndTime ' & |
                                 ' FROM TripSheetDeliveries ' & |
                                 'WHERE DIID IN ' & |
                                 '(SELECT DIID FROM DeliveryItems WHERE DID = ' & p:ID & ')'
    OF 6
       _SQLTemp{PROP:SQL}  = 'SELECT DISTINCT ContainerNo FROM DeliveryItems WHERE DID = ' & p:ID
    .


    IF ERRORCODE()
       MESSAGE('Option ' & p:Option & ' SQL statement error: ' & CLIP(ERROR()) & '|F Err: ' & CLIP(FILEERROR()) & '||SQL: ' & CLIP(_SQLTemp{PROP:SQL}), 'Get_Info_SQL', ICON:Hand)
    ELSE
       LOOP
          NEXT(_SQLTemp)
          IF ERRORCODE()
             BREAK
          .

          LOC:Records   += 1

          IF ~OMITTED(3)
             CASE p:Option
             OF 0
                Add_to_List(FORMAT(_SQ:SDate,@d5), p:ResultString, ',')
             OF 1
                COM:CMID        = _SQ:S1
                IF Access:Commodities.TryFetch(COM:PKey_CMID) = LEVEL:Benign
                   Add_to_List(COM:Commodity, p:ResultString, ',')
                .
             OF 2
                PACK:PTID       = _SQ:S1
                IF Access:PackagingTypes.TryFetch(PACK:PKey_PTID) = LEVEL:Benign
                   Add_to_List(PACK:Packaging, p:ResultString, ',')
                .
             OF 3
                Add_to_List(FORMAT(_SQ:SDate,@d5), p:ResultString, ',')
                Add_to_List(FORMAT(_SQ:STime,@t4), p:ResultString, ',')
             OF 4
                CASE p:Items
                OF 1
                   p:ResultString  = _SQ:S1
                OF 2
                   p:ResultString  = CLIP(_SQ:S1) & ',' & CLIP(_SQ:S2)
                ELSE
                   p:ResultString  = CLIP(_SQ:S1) & ',' & CLIP(_SQ:S2)
                   ! p:ResultString  =
                .

                BREAK
             OF 5
                Add_to_List(FORMAT(_SQ:SDate,@d5) & ' ' & FORMAT(_SQ:STime,@t1), p:ResultString, ',')
             OF 6                    
                Add_to_List(_SQ:S1, p:ResultString, ',')
    .  .  .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Records)

    Exit

CloseFiles     Routine
    Relate:_SQLTemp.Close()
    Relate:TripSheetDeliveries.Close()
    Relate:DeliveryItems.Close()
    Relate:Commodities.Close()
    Relate:PackagingTypes.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! - duplicated!!! remove when older than 2 months
!!! </summary>
Get_TranspPay_Alloc_Amt PROCEDURE  (p:ID, p:Option)        ! Declare Procedure
LOC:Paid_Dec         STRING(50)                            ! 
Tek_Failed_File     STRING(100)

TransPay_View           VIEW(TransporterPaymentsAllocationsAlias)
    PROJECT(A_TRAPA:Amount)
    .



TP_View                 ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TransporterPaymentsAllocationsAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TransporterPaymentsAlias.Open()
     .
     Access:TransporterPaymentsAllocationsAlias.UseFile()
     Access:TransporterPaymentsAlias.UseFile()
    ! p:Option
    !   0.  Transporter Invoice Paid
    !   1.  Transporter Payments Allocation total

    IF p:Option > 1
       MESSAGE('The option does not exist: ' & p:Option, 'Get_TranspPay_Alloc_Amt', ICON:Hand)
    ELSE
       TP_View.Init(TransPay_View, Relate:TransporterPaymentsAllocationsAlias)

       CASE p:Option
       OF 0
          TP_View.AddSortOrder(A_TRAPA:FKey_TIN)
          TP_View.AppendOrder('A_TRAPA:TPID,A_TRAPA:AllocationNo')
          TP_View.AddRange(A_TRAPA:TIN, p:ID)
       OF 1                    
          TP_View.AddSortOrder(A_TRAPA:FKey_TPID_AllocationNo)
          TP_View.AddRange(A_TRAPA:TPID, p:ID)
       .

       TP_View.Reset()
       LOOP
          IF TP_View.Next() ~= LEVEL:Benign
             BREAK
          .

          LOC:Paid_Dec     += A_TRAPA:Amount
       .


       ! We need to check if these payments have been credited...
       IF p:Option = 1
          A_TRAP:TPID_Reversal  = p:ID
          IF Access:TransporterPaymentsAlias.TryFetch(A_TRAP:SKey_TPID_Reversal) = LEVEL:Benign
             ! This TPID has been reversed.. add these up too
             TP_View.AddSortOrder(A_TRAPA:FKey_TPID_AllocationNo)
             TP_View.AppendOrder('A_TRAPA:AllocationNo')
             TP_View.AddRange(A_TRAPA:TPID, A_TRAP:TPID)

             TP_View.SetSort(2)

             TP_View.Reset()
             LOOP
                IF TP_View.Next() ~= LEVEL:Benign
                   BREAK
                .

                LOC:Paid_Dec     += A_TRAPA:Amount
       .  .  .

       TP_View.Kill()
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Paid_Dec)

    Exit

CloseFiles     Routine
    Relate:TransporterPaymentsAllocationsAlias.Close()
    Relate:TransporterPaymentsAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Inv_Outstanding  PROCEDURE  (p:IID)                    ! Declare Procedure
L_BI:Browse_Items    GROUP,PRE()                           ! 
L_BI:Outstanding     DECIMAL(15,2)                         ! Total Outstanding
L_BI:Credited        DECIMAL(15,2)                         ! 
L_BI:Payments        DECIMAL(15,2)                         ! 
                     END                                   ! 
LOC:Return           STRING(30)                            ! 

  CODE
    Relate:_Invoice.Open()
    Access:_Invoice.UseFile()

    ! Invoices may be credited or have payments
    INV:IID                 = p:IID
    IF Access:_Invoice.TryFetch(INV:PKey_IID) = LEVEL:Benign
       L_BI:Payments        = Get_ClientsPay_Alloc_Amt(INV:IID)
       L_BI:Credited        = Get_Inv_Credited(INV:IID)

       ! Credit notes may have associated invoices or not??
       IF INV:CR_IID ~= 0                                                  ! Should only be for credit notes!
          IF INV:Status = 2 OR INV:Status = 5
          ELSE
             ! ??? problem, have credit note ID but not credit note!
          .

          L_BI:Credited    -= Get_Inv_Credited(INV:CR_IID)
       .

       L_BI:Outstanding     = INV:Total - L_BI:Payments + L_BI:Credited     ! L_BI:Credited is negative
    .

    Relate:_Invoice.Close()

    LOC:Return              = L_BI:Outstanding

!    db.debugout('[Get_Inv_Outstanding]  LOC:Return: ' & CLIP(LOC:Return))

    RETURN(LOC:Return)
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Desktop_Size         PROCEDURE                             ! Declare Procedure
LOC:Size_Group       GROUP,PRE(L_SG)                       ! 
Height               LONG                                  ! 
Width                LONG                                  ! 
                     END                                   ! 
Window WINDOW,MAXIMIZE
     END


  CODE
    OPEN( Window )

    Window{ PROP:Pixels }   = TRUE
    L_SG:Height             = Window{ PROP:Height }
    L_SG:Width              = Window{ PROP:Width }

    CLOSE( Window )

    RETURN( LOC:Size_Group )
