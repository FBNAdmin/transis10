

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS002.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! 0 - Journeys, 1 - Load Types
!!! </summary>
Get_Clients_Related  PROCEDURE  (p:CID, p:ID, p:Option, p:Mode) ! Declare Procedure
LOC:Result           LONG                                  ! 
LOC:Use_Eff_Date     LONG                                  ! 
LOC:Idx              ULONG                                 ! 
LOC:JID              ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
LOC:LTID             ULONG                                 ! Load Type ID
LOC:Rates_Q          QUEUE,PRE(L_RQ),STATIC                ! static
CID                  ULONG                                 ! Client ID
RID                  ULONG                                 ! Rate ID
JID                  ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
LTID                 ULONG                                 ! Load Type ID
                     END                                   ! 
LOC:Cache_Stats      GROUP,PRE(L_CS),STATIC                ! static
Last_Cache_Out       LONG                                  ! 
Caching_No           ULONG                                 ! changed when potential changes to cache results
Hits                 ULONG                                 ! 
Requests             ULONG                                 ! 
Clients              ULONG                                 ! 
Freed                ULONG                                 ! Q freed times
                     END                                   ! 
Tek_Failed_File     STRING(100)

Rate_View       VIEW(__Rates)
    PROJECT(RAT:RID,RAT:JID,RAT:LTID)
    .

View_Rates      ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__Rates.Open()
     .
     Access:__Rates.UseFile()
    ! (p:CID, p:ID, p:Option, p:Mode)
    ! (ULONG, ULONG, BYTE, BYTE=0),LONG
    !   p:Option
    !       0 - based on JID
    !       1 - based on LTID
    !   p:Mode
    !       0 - Dont use cache, just check 1st
    !       1 - Cache return true if there was 1 match
    !       2 - Count recs. that match for this client and ID type
    !
    ! Notes:  Flaw in caching is that if clients without journeys are loaded often they will be slow
    !         as there will be no cache for them.
    !         Another flaw is that the global is only local, if another user changes the rates then the global
    !         will not be changed on their machine - need a file with this no so all users can see the changed no.


    IF p:Option > 2
       MESSAGE('Option does not exist: ' & p:Option, 'Get_Clients_Related', ICON:Hand)
    ELSE
       IF p:Mode = 0
          DO Load_Q
       ELSE
          L_CS:Requests    += 1

          ! We are going to use a queue - which we will keep up to 100 records
          IF RECORDS(LOC:Rates_Q) > 100 OR L_CS:Caching_No ~= GLO:Rates_Caching_No
             FREE(LOC:Rates_Q)              ! Reload the Q
             L_CS:Freed    += 1

             IF L_CS:Caching_No ~= GLO:Rates_Caching_No
                CLEAR(LOC:Cache_Stats)
                L_CS:Caching_No     = GLO:Rates_Caching_No
          .  .

          L_RQ:CID          = p:CID         ! Flawed - see notes
          GET(LOC:Rates_Q, L_RQ:CID)
          IF ERRORCODE()
             DO Load_Q
             L_CS:Clients  += 1
          ELSE
             L_CS:Hits     += 1
          .

          L_RQ:CID          = p:CID

          IF p:Mode = 2
             ! Count all records for this Client, for this ID type
             GET(LOC:Rates_Q, L_RQ:CID)
             IF ~ERRORCODE()
                CASE p:Option
                OF 0                             ! Journey
                   SORT(LOC:Rates_Q, L_RQ:CID, L_RQ:JID)
                OF 1                             ! Load Type
                   SORT(LOC:Rates_Q, L_RQ:CID, L_RQ:LTID)
                .

                ! Start at this client
                GET(LOC:Rates_Q, L_RQ:CID)
                LOC:Idx         = POINTER(LOC:Rates_Q) - 1
                LOOP
                   LOC:Idx     += 1
                   GET(LOC:Rates_Q, LOC:Idx)
                   IF ERRORCODE() OR L_RQ:CID ~= p:CID
                      BREAK
                   .
                   CASE p:Option
                   OF 0                             ! Journey
                      IF L_RQ:JID = LOC:JID
                         CYCLE
                      .
                   OF 1                             ! Load Type
                      IF L_RQ:LTID = LOC:LTID
                         CYCLE
                   .  .
                   LOC:JID      = L_RQ:JID
                   LOC:LTID     = L_RQ:LTID
                   LOC:Result  += 1
             .  .
          ELSE
             ! Now try get a record
             CASE p:Option
             OF 0                                   ! Journey
                L_RQ:JID        = p:ID
                GET(LOC:Rates_Q, L_RQ:CID, L_RQ:JID)
                IF ~ERRORCODE()
                   LOC:Result   = TRUE
                .
             OF 1                              ! Load Type
                L_RQ:LTID       = p:ID
                GET(LOC:Rates_Q, L_RQ:CID, L_RQ:LTID)
                IF ~ERRORCODE()
                   LOC:Result   = TRUE
          .  .  .

          IF (L_CS:Requests % 100) = 0 AND ABS(CLOCK() - L_CS:Last_Cache_Out) > 200     ! 2 seconds
             ! Every 100 requests output the cache info
             ! Output Cache info   - (p:Text, p:Heading, p:Name)
             START(Add_Log_h,, 'Cached info - Requests: ' & L_CS:Requests & ',  Hits: ' & L_CS:Hits & ',  Clients: ' & L_CS:Clients & ',  Q Freed: ' & L_CS:Freed & ',  Cur recs: ' & RECORDS(LOC:Rates_Q), 'Get_Clients_Related' ,'Cache')
             L_CS:Last_Cache_Out    = CLOCK()
    .  .  .
!               old 2
!    ! (p:CID, p:ID, p:Option, p:Mode)
!    !   p:Option
!    !       0 - based on JID
!    !       1 - based on LTID
!
!!    p:Mode  = 0
!
!    IF p:Option > 1
!       MESSAGE('Option does not exist: ' & p:Option, 'Get_Clients_Related', ICON:Hand)
!    ELSE
!       IF p:Mode = 0
!          DO Load_Q
!       ELSE
!          IF L_SG:CID ~= p:CID
!             FREE(LOC:Rates_Q)
!             DO Load_Q
!             L_SG:CID       = p:CID
!          .
!
!          ! Now try get a record
!          CASE p:Option
!          OF 0                        ! Journey
!             L_RQ:JID       = p:ID
!             GET(LOC:Rates_Q, L_RQ:JID)
!             IF ~ERRORCODE()
!                LOC:Result  = TRUE
!             .
!          OF 1                        ! Load Type
!             L_RQ:LTID      = p:ID
!             GET(LOC:Rates_Q, L_RQ:LTID)
!             IF ~ERRORCODE()
!                LOC:Result  = TRUE
!       .  .  .
!
!       IF (L_CS:Requests % 20) = 0                      ! Every 20 requests output the cache info
!          ! Output Cache info   - (p:Text, p:Heading, p:Name)
!          START(Add_Log_h,, 'Cached info - Requests: ' & L_CS:Requests & ',  Hits: ' & L_CS:Hits & ',  Clients: ' & L_CS:Clients & ',  Q Freed: ' & L_CS:Freed & ',  Cur recs: ' & RECORDS(LOC:CT_Rates_Q), 'Get_Clients_Related' ,'Cache')
!    .  .
!    
!
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:__Rates.Close()
    Exit
Load_Q              ROUTINE
    ! (p:CID, p:ID, p:Option)
    !   p:Option
    !       0 - based on JID
    !       1 - based on LTID

    PUSHBIND
    View_Rates.Init(Rate_View, Relate:__Rates)
    View_Rates.AddSortOrder( RAT:FKey_CID )
    View_Rates.AppendOrder( 'RAT:LTID, RAT:JID, RAT:Effective_Date' )
    View_Rates.AddRange( RAT:CID, p:CID )

    IF p:Mode = 0
       CASE p:Option
       OF 0                        ! Journey
          BIND( 'RAT:JID', RAT:JID)
          View_Rates.SetFilter( 'RAT:JID = ' & p:ID, '1')
       OF 1                        ! Load Type
          BIND( 'RAT:LTID', RAT:LTID)
          View_Rates.SetFilter( 'RAT:LTID = ' & p:ID, '1')
       .
    ELSE
       ! Find 1st of all client rates
    .

    View_Rates.Reset()

    LOOP
       IF View_Rates.Next() ~= LEVEL:Benign
          BREAK
       .

!    db.debugout('Found - LTID: ' & RAT:LTID & ', RAT:RID: ' & RAT:RID)

       IF p:Mode = 0
          ! We have at least 1 record for this Client for this type
          LOC:Result   = 1
          BREAK
       ELSE
          IF RAT:JID ~= LOC:JID OR RAT:LTID ~= LOC:LTID
             CLEAR(LOC:Use_Eff_Date)
          .

          IF LOC:Use_Eff_Date = 0
             LOC:Use_Eff_Date  = RAT:Effective_Date       ! 1st date less then our effective date, i.e. our effective date
          .
          IF RAT:Effective_Date ~= LOC:Use_Eff_Date       ! Outside our effective date now
             CYCLE
          .

          LOC:JID       = RAT:JID
          LOC:LTID      = RAT:LTID

          L_RQ:RID      = RAT:RID
          GET(LOC:Rates_Q, L_RQ:RID)
          IF ERRORCODE()
             L_RQ:CID   = p:CID
             L_RQ:RID   = RAT:RID
             L_RQ:JID   = RAT:JID
             L_RQ:LTID  = RAT:LTID
             ADD(LOC:Rates_Q, L_RQ:CID)
    .  .  .

    View_Rates.Kill()
    POPBIND
    EXIT
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Changed_Rates        PROCEDURE                             ! Declare Procedure

  CODE
    GLO:Rates_Caching_No    += 1

    db.debugout('GLO:Rates_Caching_No: ' & GLO:Rates_Caching_No)

    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_ContainerType_Info PROCEDURE  (p:CTID, p:Option)       ! Declare Procedure
LOC:Info             STRING(100)                           ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ContainerTypes.Open()
     .
     Access:ContainerTypes.UseFile()
    ! (p:CTID, p:Option)
    ! p:Option
    !   0. Dimensions are irregular

    CTYP:CTID           = p:CTID
    IF Access:ContainerTypes.TryFetch(CTYP:PKey_CTID) = LEVEL:Benign
!       message('CTYP:OpenTop OR CTYP:OverHeight OR CTYP:FlatRack||' & CTYP:OpenTop & '  OR  ' & CTYP:OverHeight & ' OR ' & CTYP:FlatRack, 'Option: ' & p:Option)

       CASE p:Option
       OF 0
          IF CTYP:OpenTop OR CTYP:OverHeight OR CTYP:FlatRack
             LOC:Info   = 1
          .
       ELSE
          MESSAGE('Option not implemented - Option: ' & p:Option, 'Get Container Type Info.', ICON:Exclamation)
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Info)

    Exit

CloseFiles     Routine
    Relate:ContainerTypes.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! uses DeliveryItemAlias
!!! </summary>
Get_DelItems_ContainerType PROCEDURE  (p:DID, p:DIItemNo)  ! Declare Procedure
LOC:CTID             ULONG                                 ! Container Type ID
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
     Access:DeliveryItemAlias.UseFile()
    ! (p:DID, p:DIItemNo)
    ! Get the 1st item in the Delivery's container type ID - CTID

    A_DELI:DID      = p:DID
    SET(A_DELI:FKey_DID_ItemNo, A_DELI:FKey_DID_ItemNo)
    LOOP
       IF Access:DeliveryItemAlias.TryNext() ~= LEVEL:Benign
          BREAK
       .

       IF A_DELI:DID ~= p:DID
          BREAK
       .

       LOC:CTID         = A_DELI:CTID
       IF ~OMITTED(2)
          p:DIItemNo    = A_DELI:ItemNo
       .
       BREAK
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:CTID)

    Exit

CloseFiles     Routine
    Relate:DeliveryItemAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_VehComp_Info     PROCEDURE  (p:VCID, p:Option, p:Truck, p:Additional) ! Declare Procedure
LOC:Returned         STRING(100)                           ! 
LOC:Ret_2            STRING(100)                           ! 
LOC:Idx              LONG                                  ! 
LOC:Cache            GROUP,PRE(L_CG),STATIC                ! static
VCID                 ULONG                                 ! Vehicle Composition ID
Time                 LONG                                  ! 
Add_Option           BYTE                                  ! 
Ret_Next             STRING(100)                           ! 
                     END                                   ! 
LOC:Cache2           GROUP,PRE(L_C2),STATIC                ! 
VCID                 ULONG                                 ! Vehicle Composition ID
Time                 LONG                                  ! 
Capacity             DECIMAL(12)                           ! In Kgs
Registrations        STRING(250)                           ! 
MakeModels           STRING(250)                           ! 
Licensing            STRING(250)                           ! 
Horse_Reg            STRING(20)                            ! 
                     END                                   ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:VehicleMakeModel.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TruckTrailerAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:VehicleCompositionAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Drivers.Open()
     .
     Access:VehicleMakeModel.UseFile()
     Access:TruckTrailerAlias.UseFile()
     Access:VehicleCompositionAlias.UseFile()
     Access:Drivers.UseFile()
    ! (p:VCID, p:Option, p:Truck, p:Additional)
    ! (ULONG, BYTE, BYTE=0, BYTE=0),STRING
    ! p:Option  1.  Capacity                            of all
    !           2.  Registrations                       of all OR p:Truck = 100 gives horse reg only
    !           3.  Makes & Models                      of all
    !           4.  Description of Truck / Trailer      requires p:Truck  0 to 3
    !           5.  Truck / Trailer Capacity            requires p:Truck  0 to 3
    !           6.  Truck / Trailer ID (TTID)           requires p:Truck  0 to 3
    !           7.  Count of vehicles in composition
    !           8.  Compositions Capacity
    !           9.  Driver of Horse
    !           10. Transporters ID for this VCID
    !           11. Licensing dates                     of all
    !           12. Truck / Trailer Licensing dates     requires p:Truck  0 to 3
    !
    ! p:Truck
    !           - Truck no. in combination, 1 to 4
    !
    ! p:Additional
    !           - Specifies next option to be called, for cache
    !
    ! Cache works by caching 1 item that the caller specifies will be called next, if it expires or is not
    ! correct then not used.

    IF p:Option < 1 OR p:Option > 11
       MESSAGE('Unknown Option specified for VCID: ' & p:VCID & '||Option: ' & p:Option, 'Get_VehComp_Info', ICON:Hand)
    ELSE
       IF L_CG:VCID = p:VCID AND L_CG:Add_Option = p:Option AND (CLOCK() - L_CG:Time) < 100
          LOC:Returned             = L_CG:Ret_Next
       ELSE
          ! Check cache 2 for same composition
          IF (p:Option > 3 AND p:Option < 11) OR L_C2:VCID ~= p:VCID OR (CLOCK() - L_C2:Time) > 1000
             A_VCO:VCID            = p:VCID
             IF Access:VehicleCompositionAlias.TryFetch(A_VCO:PKey_VCID) = LEVEL:Benign
                LOC:Idx            = p:Option
                DO Option_Sel
                LOC:Returned       = LOC:Ret_2

                IF p:Option < 4
                   L_C2:VCID       = p:VCID
                   L_C2:Time       = CLOCK()
                .

                IF p:Additional ~= 0
                   LOC:Idx         = p:Additional
                   DO Option_Sel
                   L_CG:Ret_Next   = LOC:Ret_2

                   L_CG:Time       = CLOCK()
                   L_CG:Add_Option = p:Additional
                   L_CG:VCID       = p:VCID
             .  .

             IF p:Option = 2 AND p:Truck = 100
                LOC:Returned       = L_C2:Horse_Reg
             .
          ELSE
             IF p:Option = 11
                LOC:Returned       = L_C2:Licensing
             ELSE
                EXECUTE p:Option
                   LOC:Returned    = L_C2:Capacity
                   LOC:Returned    = L_C2:Registrations
                   LOC:Returned    = L_C2:MakeModels
                .

                IF p:Option = 2 AND p:Truck = 100
                   LOC:Returned    = L_C2:Horse_Reg
    .  .  .  .  .

!    db.debugout('[Get_VehComp_Info]  p:VCID: ' & p:VCID & ', p:Option: ' & p:Option & ', p:Truck: ' & p:Truck & ', LOC:Returned: ' & LOC:Returned)
!       cache maybe

!    ! (p:VCID, p:Option, p:Truck)
!    ! p:Option  1. Capacity of all
!    !           2. Registrations of all
!    !           3. Makes & Models of all
!    !           4. Description of Truck / Trailer
!    !           5. Truck / Trailer Capacity
!    !           6. Truck / Trailer ID - TTID
!    !           7. Count of vehicles in composition
!    !           8. Compositions Capacity
!    !           9. Driver of Horse
!
!    IF p:Option < 1 OR p:Option > 9
!       MESSAGE('Unknown Option specified for VCID: ' & p:VCID & '||Option: ' & p:Option, 'Get_VehComp_Info', ICON:Hand)
!    ELSE
!       IF ABS(CLOCK() - L_CG:Time) > 100 OR L_CG:VCID ~= p:VCID
!          IF p:Option ~= 3
!          LOOP LOC:Idx = 1 TO 9
!             A_VCO:VCID        = p:VCID
!             IF Access:VehicleCompositionAlias.TryFetch(A_VCO:PKey_VCID) = LEVEL:Benign
!                CASE LOC:Idx
!                OF 1 TO 3         ! < 4
!                   A_TRU:TTID     = A_VCO:TTID0
!                   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!                      EXECUTE LOC:Idx
!                         LOC:Returned   += A_TRU:Capacity
!                         LOC:Returned    = A_TRU:Registration
!                         BEGIN
!                            VMM:VMMID    = A_TRU:VMMID
!                            IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
!                               LOC:Returned  = VMM:MakeModel
!                   .  .  .  .
!
!                   A_TRU:TTID             = A_VCO:TTID1
!                   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!                      EXECUTE LOC:Idx
!                         LOC:Returned    += A_TRU:Capacity
!                         LOC:Returned     = CLIP(LOC:Returned) & ', ' & A_TRU:Registration
!                         BEGIN
!                            VMM:VMMID     = A_TRU:VMMID
!                            IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
!                               LOC:Returned  = CLIP(LOC:Returned) & ', ' & VMM:MakeModel
!                   .  .  .  .
!
!                   A_TRU:TTID             = A_VCO:TTID2
!                   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!                      EXECUTE LOC:Idx
!                         LOC:Returned    += A_TRU:Capacity
!                         LOC:Returned     = CLIP(LOC:Returned) & ', ' & A_TRU:Registration
!                         BEGIN
!                            VMM:VMMID     = A_TRU:VMMID
!                            IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
!                               LOC:Returned  = CLIP(LOC:Returned) & ', ' & VMM:MakeModel
!                   .  .  .  .
!
!                   A_TRU:TTID             = A_VCO:TTID3
!                   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!                      EXECUTE LOC:Idx
!                         LOC:Returned    += A_TRU:Capacity
!                         LOC:Returned     = CLIP(LOC:Returned) & ', ' & A_TRU:Registration
!                         BEGIN
!                            VMM:VMMID     = A_TRU:VMMID
!                            IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
!                               LOC:Returned  = CLIP(LOC:Returned) & ', ' & VMM:MakeModel
!                   .  .  .  .
!                OF 4 OROF 5           ! ELSIF LOC:Idx < 6
!                   !           4. Description of Truck / Trailer
!                   !           5. Truck / Trailer Capacity
!                   EXECUTE p:Truck
!                      A_TRU:TTID          = A_VCO:TTID1
!                      A_TRU:TTID          = A_VCO:TTID2
!                      A_TRU:TTID          = A_VCO:TTID3
!                   ELSE
!                      A_TRU:TTID          = A_VCO:TTID0
!                   .
!
!                   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!                      CASE LOC:Idx
!                      OF 4
!                         VMM:VMMID        = A_TRU:VMMID
!                         IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
!                            LOC:Returned  = CLIP(VMM:MakeModel) & ' - ' & A_TRU:Registration
!                         .
!                      OF 5
!                         LOC:Returned     = A_TRU:Capacity
!                   .  .
!                OF 6              !ELSIF LOC:Idx = 6
!                   !           6. Truck / Trailer ID - TTID
!                   EXECUTE p:Truck
!                      LOC:Returned        = A_VCO:TTID1
!                      LOC:Returned        = A_VCO:TTID2
!                      LOC:Returned        = A_VCO:TTID3
!                   ELSE
!                      LOC:Returned        = A_VCO:TTID0
!                   .
!                OF 7
!                   IF A_VCO:TTID0 ~= 0
!                      LOC:Returned    += 1
!                   .
!                   IF A_VCO:TTID1 ~= 0
!                      LOC:Returned    += 1
!                   .
!                   IF A_VCO:TTID2 ~= 0
!                      LOC:Returned    += 1
!                   .
!                   IF A_VCO:TTID3 ~= 0
!                      LOC:Returned    += 1
!                   .
!                OF 8
!                   LOC:Returned   = A_VCO:Capacity
!                OF 9
!                   A_TRU:TTID     = A_VCO:TTID0
!                   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!                      DRI:DRID    = A_TRU:DRID
!                      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
!                         LOC:Returned     = CLIP(DRI:FirstName) & ' ' & DRI:Surname
!                   .  .
!
!
!
!
!             ELSE
!                MESSAGE('The option does not exists - ' & p:Option & '||Please report this error.', 'Get_VehComp_Info', ICON:Hand)
!    .  .  .  .
!
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Returned)

    Exit

CloseFiles     Routine
    Relate:VehicleMakeModel.Close()
    Relate:TruckTrailerAlias.Close()
    Relate:VehicleCompositionAlias.Close()
    Relate:Drivers.Close()
    Exit
Option_Sel                  ROUTINE
    CLEAR(LOC:Ret_2)

    CASE LOC:Idx
    OF 1 TO 3 OROF 11        ! < 4
       CLEAR(LOC:Cache2)

       A_TRU:TTID               = A_VCO:TTID0
       IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
          L_C2:Capacity        += A_TRU:Capacity
          L_C2:Registrations    = A_TRU:Registration
          L_C2:Horse_Reg        = A_TRU:Registration

          IF A_TRU:LicenseExpiryDate = 0
             L_C2:Licensing     = '<not available>'
          ELSE
             L_C2:Licensing     = FORMAT(A_TRU:LicenseExpiryDate, @d5b)
          .

          VMM:VMMID             = A_TRU:VMMID
          IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
             L_C2:MakeModels    = VMM:MakeModel
       .  .

       A_TRU:TTID               = A_VCO:TTID1
       IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
          L_C2:Capacity        += A_TRU:Capacity
          L_C2:Registrations    = CLIP(L_C2:Registrations) & ', ' & A_TRU:Registration

          IF A_TRU:LicenseExpiryDate = 0
             L_C2:Licensing     = CLIP(L_C2:Licensing) & ', <not available>'
          ELSE
             L_C2:Licensing     = CLIP(L_C2:Licensing) & ', ' & FORMAT(A_TRU:LicenseExpiryDate, @d5b)
          .

          VMM:VMMID             = A_TRU:VMMID
          IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
             L_C2:MakeModels    = CLIP(L_C2:MakeModels) & ', ' & VMM:MakeModel
       .  .

       A_TRU:TTID               = A_VCO:TTID2
       IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
          L_C2:Capacity        += A_TRU:Capacity
          L_C2:Registrations    = CLIP(L_C2:Registrations) & ', ' & A_TRU:Registration

          IF A_TRU:LicenseExpiryDate = 0
             L_C2:Licensing     = CLIP(L_C2:Licensing) & ', <not available>'
          ELSE
             L_C2:Licensing     = CLIP(L_C2:Licensing) & ', ' & FORMAT(A_TRU:LicenseExpiryDate, @d5b)
          .

          VMM:VMMID             = A_TRU:VMMID
          IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
             L_C2:MakeModels    = CLIP(L_C2:MakeModels) & ', ' & VMM:MakeModel
       .  .

       A_TRU:TTID               = A_VCO:TTID3
       IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
          L_C2:Capacity        += A_TRU:Capacity
          L_C2:Registrations    = CLIP(L_C2:Registrations) & ', ' & A_TRU:Registration

          IF A_TRU:LicenseExpiryDate = 0
             L_C2:Licensing     = CLIP(L_C2:Licensing) & ', <not available>'
          ELSE
             L_C2:Licensing     = CLIP(L_C2:Licensing) & ', ' & FORMAT(A_TRU:LicenseExpiryDate, @d5b)
          .

          VMM:VMMID             = A_TRU:VMMID
          IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
             L_C2:MakeModels    = CLIP(L_C2:MakeModels) & ', ' & VMM:MakeModel
       .  .

       IF LOC:Idx = 11
          LOC:Ret_2             = L_C2:Licensing
       ELSE
          EXECUTE LOC:Idx
             LOC:Ret_2          = L_C2:Capacity
             LOC:Ret_2          = L_C2:Registrations
             LOC:Ret_2          = L_C2:MakeModels
       .  .
    OF 4 OROF 5           ! ELSIF LOC:Idx < 6
       !           4. Description of Truck / Trailer
       !           5. Truck / Trailer Capacity
       EXECUTE p:Truck
          A_TRU:TTID          = A_VCO:TTID1
          A_TRU:TTID          = A_VCO:TTID2
          A_TRU:TTID          = A_VCO:TTID3
       ELSE
          A_TRU:TTID          = A_VCO:TTID0
       .

       IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
          CASE LOC:Idx
          OF 4
             VMM:VMMID        = A_TRU:VMMID
             IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
                LOC:Ret_2  = CLIP(VMM:MakeModel) & ' - ' & A_TRU:Registration
             .
          OF 5
             LOC:Ret_2     = A_TRU:Capacity
          OF 12
             LOC:Ret_2     = FORMAT(A_TRU:LicenseExpiryDate, @d5b)
       .  .
    OF 6              !ELSIF LOC:Idx = 6
       !           6. Truck / Trailer ID - TTID
       EXECUTE p:Truck
          LOC:Ret_2        = A_VCO:TTID1
          LOC:Ret_2        = A_VCO:TTID2
          LOC:Ret_2        = A_VCO:TTID3
       ELSE
          LOC:Ret_2        = A_VCO:TTID0
       .
    OF 7
       IF A_VCO:TTID0 ~= 0
          LOC:Ret_2    += 1
       .
       IF A_VCO:TTID1 ~= 0
          LOC:Ret_2    += 1
       .
       IF A_VCO:TTID2 ~= 0
          LOC:Ret_2    += 1
       .
       IF A_VCO:TTID3 ~= 0
          LOC:Ret_2    += 1
       .
    OF 8
       LOC:Ret_2        = A_VCO:Capacity
    OF 9
       A_TRU:TTID       = A_VCO:TTID0
       IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
          DRI:DRID      = A_TRU:DRID
          IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
             LOC:Ret_2  = CLIP(DRI:FirstName) & ' ' & DRI:Surname
       .  .
    OF 10
       LOC:Ret_2        = A_VCO:TID
    ELSE
       MESSAGE('The option does not exists - ' & LOC:Idx & '||Please report this error.', 'Get_VehComp_Info', ICON:Hand)
    .
    EXIT
!               old
!Option_Sel                  ROUTINE
!    CASE LOC:Idx
!    OF 1 TO 3         ! < 4
!       A_TRU:TTID     = A_VCO:TTID0
!       IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!          L_C2:Capacity        += A_TRU:Capacity
!          L_C2:Registrations    = A_TRU:Registration
!          L_C2:MakeModels       =
!
!
!          EXECUTE LOC:Idx
!             LOC:Ret_2   += A_TRU:Capacity
!             LOC:Ret_2    = A_TRU:Registration
!             BEGIN
!                VMM:VMMID    = A_TRU:VMMID
!                IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
!                   LOC:Ret_2  = VMM:MakeModel
!       .  .  .  .
!
!       A_TRU:TTID             = A_VCO:TTID1
!       IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!          EXECUTE LOC:Idx
!             LOC:Ret_2    += A_TRU:Capacity
!             LOC:Ret_2     = CLIP(LOC:Ret_2) & ', ' & A_TRU:Registration
!             BEGIN
!                VMM:VMMID     = A_TRU:VMMID
!                IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
!                   LOC:Ret_2  = CLIP(LOC:Ret_2) & ', ' & VMM:MakeModel
!       .  .  .  .
!
!       A_TRU:TTID             = A_VCO:TTID2
!       IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!          EXECUTE LOC:Idx
!             LOC:Ret_2    += A_TRU:Capacity
!             LOC:Ret_2     = CLIP(LOC:Ret_2) & ', ' & A_TRU:Registration
!             BEGIN
!                VMM:VMMID     = A_TRU:VMMID
!                IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
!                   LOC:Ret_2  = CLIP(LOC:Ret_2) & ', ' & VMM:MakeModel
!       .  .  .  .
!
!       A_TRU:TTID             = A_VCO:TTID3
!       IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!          EXECUTE LOC:Idx
!             LOC:Ret_2    += A_TRU:Capacity
!             LOC:Ret_2     = CLIP(LOC:Ret_2) & ', ' & A_TRU:Registration
!             BEGIN
!                VMM:VMMID     = A_TRU:VMMID
!                IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
!                   LOC:Ret_2  = CLIP(LOC:Ret_2) & ', ' & VMM:MakeModel
!       .  .  .  .
!    OF 4 OROF 5           ! ELSIF LOC:Idx < 6
!       !           4. Description of Truck / Trailer
!       !           5. Truck / Trailer Capacity
!       EXECUTE p:Truck
!          A_TRU:TTID          = A_VCO:TTID1
!          A_TRU:TTID          = A_VCO:TTID2
!          A_TRU:TTID          = A_VCO:TTID3
!       ELSE
!          A_TRU:TTID          = A_VCO:TTID0
!       .
!
!       IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!          CASE LOC:Idx
!          OF 4
!             VMM:VMMID        = A_TRU:VMMID
!             IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
!                LOC:Ret_2  = CLIP(VMM:MakeModel) & ' - ' & A_TRU:Registration
!             .
!          OF 5
!             LOC:Ret_2     = A_TRU:Capacity
!       .  .
!    OF 6              !ELSIF LOC:Idx = 6
!       !           6. Truck / Trailer ID - TTID
!       EXECUTE p:Truck
!          LOC:Ret_2        = A_VCO:TTID1
!          LOC:Ret_2        = A_VCO:TTID2
!          LOC:Ret_2        = A_VCO:TTID3
!       ELSE
!          LOC:Ret_2        = A_VCO:TTID0
!       .
!    OF 7
!       IF A_VCO:TTID0 ~= 0
!          LOC:Ret_2    += 1
!       .
!       IF A_VCO:TTID1 ~= 0
!          LOC:Ret_2    += 1
!       .
!       IF A_VCO:TTID2 ~= 0
!          LOC:Ret_2    += 1
!       .
!       IF A_VCO:TTID3 ~= 0
!          LOC:Ret_2    += 1
!       .
!    OF 8
!       LOC:Ret_2   = A_VCO:Capacity
!    OF 9
!       A_TRU:TTID     = A_VCO:TTID0
!       IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!          DRI:DRID    = A_TRU:DRID
!          IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
!             LOC:Ret_2     = CLIP(DRI:FirstName) & ' ' & DRI:Surname
!       .  .
!    ELSE
!       MESSAGE('The option does not exists - ' & LOC:Idx & '||Please report this error.', 'Get_VehComp_Info', ICON:Hand)
!    .
!    EXIT
