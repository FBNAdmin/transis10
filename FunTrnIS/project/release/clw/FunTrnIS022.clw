

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS022.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Client_FuelCosts PROCEDURE  (p:CID, p:Date, p:Return_Eff_Date) ! Declare Procedure
LOC:Buffer           USHORT                                ! 
LOC:FuelCost         DECIMAL(10,3)                         ! Fuel Cost
Tek_Failed_File     STRING(100)

View_FC            VIEW(__RatesFuelCostAlias)
                   .


FC_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__RatesFuelCostAlias.Open()
     .
     Access:ClientsAlias.UseFile()
     Access:__RatesFuelCostAlias.UseFile()
    ! (p:CID, p:Date, p:Return_Eff_Date)
    ! (ULONG, LONG=0, *LONG), STRING        !*DECIMAL
    !
    ! p:Date                - as of date
    ! p:Return_Eff_Date     - effective date of the returned record
    !
    ! Note:   RatesFuelCost buffer is saved and loaded here - but alias is used... maybe this was done as a work around for a problem?

    A_CLI:CID   = p:CID
    IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) ~= LEVEL:Benign
       CLEAR(A_CLI:Record)
    .

    IF A_CLI:FuelSurchargeActive = TRUE
       LOC:Buffer   = Access:__RatesFuelCost.SaveBuffer()

       IF p:Date = 0
          p:Date    = TODAY()
       .

       BIND('A_FCRA:Effective_Date',A_FCRA:Effective_Date)

       FC_View.Init(View_FC, Relate:__RatesFuelCostAlias)
       FC_View.AddSortOrder(A_FCRA:CKey_CID_EffDateDesc)          ! Decending date
       !FC_View.AppendOrder('A_FCRA:Effective_Date')
       FC_View.AddRange(A_FCRA:CID, p:CID)
       FC_View.SetFilter('A_FCRA:Effective_Date < ' & p:Date + 1)

       FC_View.Reset()
       LOOP
          IF FC_View.Next() ~= LEVEL:Benign
             BREAK
          .

          LOC:FuelCost          = A_FCRA:FuelCost

          IF ~OMITTED(3)
             p:Return_Eff_Date  = A_FCRA:Effective_Date
          .

          BREAK
       .
       FC_View.Kill()

       UNBIND('A_FCRA:Effective_Date')

       !db.debugout('before: A_FCRA:CID: ' & A_FCRA:CID & '      p:CID: ' & p:CID)
       Access:__RatesFuelCost.RestoreBuffer(LOC:Buffer)
    .
           Relate:ClientsAlias.Close()
           Relate:__RatesFuelCostAlias.Close()


       Return(LOC:FuelCost)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Client_FuelSurcharge_Calcs PROCEDURE  (p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Cur_Eff_Date, p:X_Fuel_Cost, p:X_Eff_Date, p:M_Fuel_Base_Rate, p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base, p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per) ! Declare Procedure
LOC:X_Eff_Date       LONG                                  ! 
LOC:X_Fuel_Cost      DECIMAL(10,3)                         ! Fuel Cost
LOC:M_Fuel_Base_Rate DECIMAL(10,4)                         ! % cost of total cost
LOC:N_Increase_On_Base DECIMAL(20,6)                       ! 
LOC:N_Increase_On_Base_Str STRING(20)                      ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_FuelCost.Open()
     .
     Access:_FuelCost.UseFile()
    ! This procedure is to help display the values on the calc. sheet
    ! (p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Cur_Eff_Date,
    !     1         2               3           4
    !
    !  p:X_Fuel_Cost, p:X_Eff_Date, p:M_Fuel_Base_Rate
    !     5               6               7
    !
    !  p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base, p:O_Change_FuelC_Per,
    !     8               9                   10                       11
    !
    !  p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per
    !     12                                  13

    ! (ULONG, ULONG, *DECIMAL, *LONG,
    !    1     2        3        4
    !
    ! <*DECIMAL>, <*LONG>, <*DECIMAL>,
    !    5           6           7
    !
    ! <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING  (N)
    !    8           9           10          11          12          13

!    ! We will allow 3 calling states
!    IF OMITTED(5) AND OMITTED(6) AND OMITTED(7) AND OMITTED(8) AND OMITTED(9) AND OMITTED(10)
!       ! all
!    ELSIF OMITTED(5) AND OMITTED(6) AND OMITTED(7) AND OMITTED(8) AND             OMITTED(10)
!       ! all except S - Revised Base
!    ELSIF ~OMITTED(5) AND ~OMITTED(6) AND ~OMITTED(7) AND ~OMITTED(8) AND ~OMITTED(9) AND ~OMITTED(10)
!       ! none
!    ELSE
!       MESSAGE('Cannot call this function with these parameters.||Or can you?')
!    .
!
    ! (p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Cur_Eff_Date,
    !    1          2               3           4

    ! Get X values
    ! (p:CID, p:Date, p:Return_Eff_Date)
    ! (ULONG, LONG=0, *LONG), STRING
    LOC:X_Fuel_Cost         = Get_Client_FuelCosts(p:CID, p:Cur_Eff_Date - 1, LOC:X_Eff_Date)

    ! If no previous client cost then use base system fuel cost???  Ok we will use the selected Base Rate record instead!!
    !IF LOC:X_Fuel_Cost = 0.0

    !.

    ! Get M
    FUE:FCID                = p:Base_Rate_ID
    IF Access:_FuelCost.TryFetch(FUE:PKey_FCID) = LEVEL:Benign
       LOC:M_Fuel_Base_Rate = FUE:FuelBaseRate

       IF LOC:X_Fuel_Cost = 0.0
          LOC:X_Eff_Date    = FUE:EffectiveDate
          LOC:X_Fuel_Cost   = FUE:FuelCost
    .  .



    ! (p:X_Fuel_Cost, p:Y_Fuel_Cost, p:M_Fuel_Base_Rate,
    !       1               2               3
    !  p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base, p:O_Change_FuelC_Per,
    !       4               5               6                       7
    !  p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per
    !       8                               9

!    db.debugout('LOC:X_Fuel_Cost: ' & LOC:X_Fuel_Cost & ',  LOC:X_Eff_Date: ' & FORMAT(LOC:X_Eff_Date, @d5) & |
!                ',  p:Cur_Eff_Date: ' & FORMAT(p:Cur_Eff_Date,@d5) & ',  p:Base_Rate_ID: ' & p:Base_Rate_ID & |
!                ',  LOC:M_Fuel_Base_Rate: ' & LOC:M_Fuel_Base_Rate & ',  p:Cur_Cost: ' & p:Cur_Cost)


    LOC:N_Increase_On_Base  = Calc_FuelBaseRate(LOC:X_Fuel_Cost, p:Cur_Cost, LOC:M_Fuel_Base_Rate,          |
                            p:Z_CPL_Change, p:Q_CPL_Change_Per,                       |
                            p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per)

    IF LOC:N_Increase_On_Base < 0
       LOC:N_Increase_On_Base   = 0.0
    .

!    db.debugout('p:N_Increase_On_Base: ' & p:N_Increase_On_Base & ',     LOC:N_Increase_On_Base: ' & LOC:N_Increase_On_Base)
    ! This procedure is to help display the values on the calc. sheet
    ! (p:CID, p:Base_Rate_ID, p:Cur_Cost, p:Cur_Eff_Date,
    !     1         2               3           4
    !
    !  p:X_Fuel_Cost, p:X_Eff_Date, p:M_Fuel_Base_Rate
    !     5               6               7
    !
    !  p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base, p:O_Change_FuelC_Per,
    !     8               9                   10                       11
    !
    !  p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per
    !     12                                  13

    ! (ULONG, ULONG, *DECIMAL, *LONG,
    !    1     2        3        4
    !
    ! <*DECIMAL>, <*LONG>, <*DECIMAL>,
    !    5           6           7
    !
    ! <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), STRING  (N)
    !    8           9           10          11          12          13



    IF ~OMITTED(5)
       p:X_Fuel_Cost            = LOC:X_Fuel_Cost
    .
    IF ~OMITTED(6)
       p:X_Eff_Date             = LOC:X_Eff_Date
    .
    IF ~OMITTED(7)
       p:M_Fuel_Base_Rate       = LOC:M_Fuel_Base_Rate
    .


    LOC:N_Increase_On_Base_Str  = LOC:N_Increase_On_Base
           Relate:_FuelCost.Close()


       Return(LOC:N_Increase_On_Base_Str)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Calc_FuelBaseRate_old PROCEDURE  (p:X_Fuel_Cost, p:Y_Fuel_Cost, p:M_Fuel_Base_Rate, p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base, p:O_Change_FuelC_Per, p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per) ! Declare Procedure
LOC:X_Fuel_Cost      DECIMAL(20,6)                         ! 
LOC:Y_Fuel_Cost      DECIMAL(20,6)                         ! 
LOC:M_Fuel_Base_Rate DECIMAL(20,6)                         ! 
LOC:Z_CPL_Change     DECIMAL(20,6)                         ! 
LOC:Q_CPL_Change_Per DECIMAL(20,6)                         ! 
LOC:N_Increase_On_Base DECIMAL(20,6)                       ! 
LOC:O_Change_FuelC_Per DECIMAL(20,6)                       ! 
LOC:S_Revised_FuelBase_Per DECIMAL(20,6)                   ! 
LOC:T_Revised_OtherBase_Per DECIMAL(20,6)                  ! 
Tek_Failed_File     STRING(100)


  CODE

    ! (p:X_Fuel_Cost, p:Y_Fuel_Cost, p:M_Fuel_Base_Rate,
    !       1               2               3
    !  p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base, p:O_Change_FuelC_Per,
    !       4               5               6                       7
    !  p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per
    !       8                               9

    ! (*DECIMAL, *DECIMAL, *DECIMAL, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), *DECIMAL  (N)
    !       1       2          3            4           5           6           7           8         9        

    ! Revised Base:
    ! S = O / R
    ! = (M + N) / (O + P)
    ! = (M + (M * Q)) / ((M + N) + (100% - M))
    ! = (M + (M * Q)) / ((M + (M * Q)) + (100% - M))
    ! = (M + (M * (Z / X))) / ((M + (M * (Z / X))) + (100% - M))
    ! = (M + (M * ((Y - X) / X))) / ((M + (M * ((Y - X) / X))) + (100% - M))

    ! Difference in Cost                                                        - Z  (Y - X)
    LOC:Z_CPL_Change            = p:Y_Fuel_Cost - p:X_Fuel_Cost

    ! Cost per litre change %                                                   - Q
    LOC:Q_CPL_Change_Per        = (LOC:Z_CPL_Change / p:X_Fuel_Cost) * 100

    ! p:N_Increase_On_Base                                                      - N
    LOC:N_Increase_On_Base      = p:M_Fuel_Base_Rate * (LOC:Q_CPL_Change_Per / 100)

    ! O = M + (M * Q) = M + N                                                   - O
    LOC:O_Change_FuelC_Per      = p:M_Fuel_Base_Rate + LOC:N_Increase_On_Base

    ! S = O / R       = O / (O + P)    = O / (O + (100 - M))                    - S
    LOC:S_Revised_FuelBase_Per  = (LOC:O_Change_FuelC_Per / (LOC:O_Change_FuelC_Per + (100 - p:M_Fuel_Base_Rate))) * 100

    ! T = P / R
    LOC:T_Revised_OtherBase_Per = (100 - p:M_Fuel_Base_Rate) / (LOC:O_Change_FuelC_Per + (100 - p:M_Fuel_Base_Rate))
    ! Return parameters

    ! (p:X_Fuel_Cost, p:Y_Fuel_Cost, p:M_Fuel_Base_Rate,
    !       1               2               3
    !  p:Z_CPL_Change, p:Q_CPL_Change_Per, p:N_Increase_On_Base, p:O_Change_FuelC_Per,
    !       4               5               6                       7
    !  p:S_Revised_FuelBase_Per, p:T_Revised_OtherBase_Per
    !       8                               9

    ! (*DECIMAL, *DECIMAL, *DECIMAL, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>), *DECIMAL  (N)
    !       1       2          3            4           5           6           7           8         9

    IF ~OMITTED(4)
       p:Z_CPL_Change               = LOC:Z_CPL_Change
    .
    IF ~OMITTED(5)
       p:Q_CPL_Change_Per           = LOC:Q_CPL_Change_Per
    .
    IF ~OMITTED(6)
       p:N_Increase_On_Base         = LOC:N_Increase_On_Base
    .
    IF ~OMITTED(7)
       p:O_Change_FuelC_Per         = LOC:O_Change_FuelC_Per
    .
    IF ~OMITTED(8)
       p:S_Revised_FuelBase_Per     = LOC:S_Revised_FuelBase_Per
    .
    IF ~OMITTED(9)
       p:T_Revised_OtherBase_Per    = LOC:T_Revised_OtherBase_Per
    .


       Return(LOC:N_Increase_On_Base)

