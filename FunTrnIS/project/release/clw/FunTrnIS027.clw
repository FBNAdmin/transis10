

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS027.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Client_ETolls    PROCEDURE  (ULONG p:CID,LONG p:Date,<*ULONG p:CID_Used>) ! Declare Procedure
LOC:Buffer           USHORT                                ! 
LOC:TollRate         STRING(35)                            ! 
LOC:Count            LONG                                  ! 
LOC:Default          BYTE                                  ! 
Tek_Failed_File     STRING(100)

View_ET            VIEW(__RatesToll)
    .


ET_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__RatesToll.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsAlias.Open()
     .
     Access:__RatesToll.UseFile()
     Access:ClientsAlias.UseFile()
    ! Get_Client_TollCharge
    ! (p:CID, p:Date, p:CID_Used)
    ! (ULONG, LONG=0, <*ULONG>),STRING
    !
    ! p:Date        - effective date, if -1 then this is not used in get and last effective charge will be got
    ! p:CID_Used    - this could be different from the p:CID when a client has none of their own rates
    !
    ! Notes:    Buffer is saved and restored for RatesFuelCost

    A_CLI:CID   = p:CID
    IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) ~= LEVEL:Benign
       CLEAR(A_CLI:Record)
    .
    IF A_CLI:TollChargeActive = TRUE
       LOC:Buffer   = Access:__RatesToll.SaveBuffer()

       IF p:Date = -1
          ! Date will not be used in the query
       ELSIF p:Date = 0
          p:Date    = TODAY() + 1                               ! Tomorrow or, before tomorrow as it will be in query
       .

       IF p:Date ~= -1
          BIND('TOL:Effective_Date',TOL:Effective_Date)
       .

       ET_View.Init(View_ET, Relate:__RatesToll)
       ET_View.AddSortOrder(TOL:CKey_CID_EffDate)          ! Decending date
       !FC_View.AppendOrder('FCRA:Effective_Date')
       ET_View.AddRange(TOL:CID, p:CID)

       LOOP
          IF p:Date ~= -1
             ET_View.SetFilter('TOL:Effective_Date < ' & p:Date + 1)
          .

          ET_View.Reset()
          LOOP
             IF ET_View.Next() ~= LEVEL:Benign
                BREAK
             .

             LOC:Count            += 1

             LOC:TollRate          = TOL:TollRate
             !LOC:FuelBaseRate      = FCRA:FuelBaseRate ???  not used yet/ever?

             BREAK
          .

          IF LOC:Default = FALSE AND LOC:Count <= 0                ! We aint got a rate yet... so client doesnt have, get default
             ! This client has none defined... use default for Branch
             p:CID          = Get_Branch_Info(, 7)                 ! Gets Branch specified default rate client.......

             ET_View.AddRange(TOL:CID, p:CID)

             LOC:Default    = TRUE
             CYCLE
          .
          BREAK
       .
       ET_View.Kill()

       UNBIND('TOL:Effective_Date')

       Access:__RatesToll.RestoreBuffer(LOC:Buffer)

       IF ~OMITTED(3)
          p:CID_Used  = p:CID           ! Because p:CID is set to the default above if the Clients one is missing 
    .  .
           Relate:__RatesToll.Close()
           Relate:ClientsAlias.Close()

    Return(Round(Deformat(LOC:TollRate),.01)) ! 2 August 2011 by Victor

       Return(LOC:TollRate)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Add_Vehicle_Composition PROCEDURE  (p_TID, p_CompositionName, p_FreightLinerTTID, p_TrailerTTID, p_SuperLinkTTID, p_Capacity) ! Declare Procedure
LOC:Return           LONG                                  ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:VehicleCompositionAlias.Open()
     .
     Access:VehicleCompositionAlias.UseFile()
   ! (p_TID, p_CompositionName, p_FreightLinerTTID, p_TrailerTTID, p_SuperLinkTTID, p_Capacity)

   CLEAR(A_VCO:Record)
   IF Access:VehicleCompositionAlias.TryPrimeAutoInc() = Level:Benign
      A_VCO:TID                     = p_TID
      
      A_VCO:CompositionName         = p_CompositionName
      A_VCO:TTID0                   = p_FreightLinerTTID
      A_VCO:TTID1                   = p_TrailerTTID
      A_VCO:TTID2                   = p_SuperLinkTTID
      
      A_VCO:Capacity                = p_Capacity
      
      A_VCO:ShowForAllTransporters  = FALSE
      
      IF Access:VehicleCompositionAlias.TryInsert() = LEVEL:Benign
         LOC:Return                 = A_VCO:VCID
      ELSE
         MESSAGE('Error on TryInsert, File error: ' & Access:VehicleCompositionAlias.GetError() & '|||Please take a screen shot of this error and send to support.', 'Add_Vehicle_Composition', ICON:Hand)
            ! debug code
         !IF Access:VehicleCompositionAlias.Insert() = LEVEL:Benign
         ! .
         

         Access:VehicleCompositionAlias.CancelAutoInc()
      .
   ELSE
      MESSAGE('Error on TryPrimeAutoInc, File error: ' & Access:VehicleCompositionAlias.GetError() & '|||Please take a screen shot of this error and send to support.', 'Add_Vehicle_Composition', ICON:Hand)
   .
           Relate:VehicleCompositionAlias.Close()


       Return(LOC:Return)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Validate_Email_Addresses PROCEDURE  (STRING p_EmailAddresses) ! Declare Procedure
LOC:Email_Address    STRING(255)                           ! 
LOC:Result           BYTE                                  ! 
LOC:Results          STRING(255)                           ! 

  CODE
   ! Take each address and check it, set result in comma del return list
   LOOP
      IF CLIP(p_EmailAddresses) = ''
         BREAK
      .
         
      LOC:Email_Address    = LEFT(Get_1st_Element_From_Delim_Str(p_EmailAddresses, ',', 1))
      
      LOC:Result           =  MATCH( UPPER( CLIP(LOC:Email_Address) ) , '^[-A-Z0-9._]+@{{[-A-Z0-9._]+\.}+[A-Z][A-Z][A-Z]?[A-Z]?$', Match:Regular)
      
      !db.Debugout('++++++++++++  LOC:Email_Address: ' & CLIP(LOC:Email_Address) & '        LOC:Result: ' & LOC:Result)
      
      Add_to_List(LOC:Result, LOC:Results, ',')
   .

   RETURN(LOC:Results)
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Save_Email_Addresses PROCEDURE  (STRING p_EmailAddresses,ULONG p_CID) ! Declare Procedure
LOC:Email_Address    STRING(255)                           ! 
LOC:Result           BYTE                                  ! 
LOC:Results          STRING(255)                           ! 
LOC:Return           LONG                                  ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:EmailAddresses.Open()
     .
     Access:EmailAddresses.UseFile()
   ! Take each address and check it, set result in comma del return list
   LOOP
      IF CLIP(p_EmailAddresses) = ''
         BREAK
      .
         
      LOC:Email_Address    = LEFT(Get_1st_Element_From_Delim_Str(p_EmailAddresses, ',', 1))
      
      ! check if we have it already
      EMAI:CID             = p_CID
      EMAI:EmailAddress    = LOC:Email_Address
      IF Access:EmailAddresses.TryFetch(EMAI:SKey_EmailAddress_CID) = Level:Benign
         ! We have this address already for this client, dont add again
      ELSE
         LOC:Result           =  MATCH( UPPER( CLIP(LOC:Email_Address) ) , '^[-A-Z0-9._]+@{{[-A-Z0-9._]+\.}+[A-Z][A-Z][A-Z]?[A-Z]?$', Match:Regular)
         
         !db.Debugout('++++++++++++  LOC:Email_Address: ' & CLIP(LOC:Email_Address) & '        LOC:Result: ' & LOC:Result)
         
         IF LOC:Result = 1
            Access:EmailAddresses.PrimeAutoInc()
            EMAI:EmailName       = LOC:Email_Address
            EMAI:EmailAddress    = LOC:Email_Address
            EMAI:CID             = p_CID
            EMAI:Operations      = TRUE
            IF Access:EmailAddresses.TryInsert() ~= Level:Benign
               Access:EmailAddresses.CancelAutoInc()
            .
         .
      .
   .
           Relate:EmailAddresses.Close()


       Return(LOC:Return)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Load_Email_Addresses PROCEDURE  (ULONG p_CID)              ! Declare Procedure
LOC:Result           STRING(1000)                          ! 
Tek_Failed_File     STRING(100)

View_       VIEW(EmailAddresses)
   PROJECT(EMAI:EmailName, EMAI:EmailAddress)
.



ViewMan_             ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:EmailAddresses.Open()
     .
     Access:EmailAddresses.UseFile()
   PUSHBIND()
   BIND('EMAI:DefaultOnDI', EMAI:DefaultOnDI)

   ViewMan_.Init(View_, Relate:EmailAddresses)
   ViewMan_.AddSortOrder(EMAI:FKey_CID)
   ViewMan_.AppendOrder('EMAI:EmailAddress')
   ViewMan_.AddRange(EMAI:CID, p_CID)
   ViewMan_.SetFilter('EMAI:DefaultOnDI = 1')

   ViewMan_.Reset()
   LOOP
      IF ViewMan_.Next() ~= Level:Benign
         BREAK
      .
      IF CLIP(LOC:Result) = ''
         LOC:Result  = CLIP(EMAI:EmailAddress)
      ELSE
         LOC:Result  = CLIP(LOC:Result) & ', ' & CLIP(EMAI:EmailAddress)
      .
   .
   ViewMan_.Kill()

   UNBIND('EMAI:DefaultOnDI')
   POPBIND()
           Relate:EmailAddresses.Close()


       Return(LOC:Result)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Floor_Info       PROCEDURE  (ULONG p:FID, BYTE p:Option) ! Declare Procedure
Ret                  STRING(50)                            ! 

  CODE
   ! option: 101, 1st branch floor belongs to
   Ret = ''

   A_FLO:FID = p:FID
   IF Access:FloorsAlias.TryFetch(A_FLO:PKey_FID) = Level:Benign
      EXECUTE p:Option
         Ret = A_FLO:Floor
         Ret = A_FLO:FBNFloor         
      ELSE
         IF p:Option = 101 OR p:Option = 102
            BRA:FID = p:FID
            IF Access:Branches.TryFetch(BRA:FKey_FID) = Level:Benign
               CASE p:Option
               OF 101
                  Ret = BRA:BID
               OF 102
                  Ret = BRA:BranchName
               .                  
            .
         .
      .
   .

   RETURN Ret
