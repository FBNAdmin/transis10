

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('FUNTRNIS016.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! does this delivery require releasing, if so is it?
!!! </summary>
Check_Delivery_Release PROCEDURE  (p_Statement_Info, p:DID, p_Release_Status_Str) ! Declare Procedure
LOC:Release_Req_Reason LONG                                ! 
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Deliveries.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Clients.Open()
     .
     Access:Deliveries.UseFile()
     Access:Clients.UseFile()
    ! (p_Statement_Info, p:DID, p_Release_Status_Str)
    ! (<*STRING>, ULONG=0, <*STRING>),LONG
    ! p:DID
    !   0   - Delivery and Client records loaded
    !           Client fields   - CLI:Terms, CLI:UpdatedDate, CLI:BalanceCurrent, CLI:Balance30Days, CLI:Balance60Days
    !                             CLI:Balance90Days, CLI:CID, CLI:Status, CLI:Limit
    !           Delivery fields - DEL:ReleasedUID
    !
    !   Returns
    !   0   - None of the Below
    !   1   - Released
    !   2   - Acc. Pre-Paid
    !   3   - 60 Day+ Balance
    !   4   - Account Limit
    !   5   - Acc. On Hold
    !   6   - Acc. Closed
    !   7   - Acc. Dormant
    !
    !   8   - (Payment) Period exceeded - New 1/02/09 - 8   	- taken out 7/04/11 (uses days since invoice and not statement)
    !
    !   p_Statement_Info - only populated in some circumstances!   Shouldn't be a return option???  27/04/08


    CLEAR(LOC:Statement_Info)

    IF p:DID ~= 0
       CLEAR(DEL:Record)
       DEL:DID      = p:DID
       IF Access:Deliveries.TryFetch(DEL:PKey_DID) ~= LEVEL:Benign
       ELSE
          CLEAR(CLI:Record)
          CLI:CID   = DEL:CID
          IF Access:Clients.TryFetch(CLI:PKey_CID) ~= LEVEL:Benign
          ELSE
    .  .  .


    LOC:Release_Req_Reason				= 0			! No release required

    IF DEL:ReleasedUID ~= 0
       LOC:Release_Req_Reason           = 1         ! Definately released
    ELSE
       ! CLI:Terms  - Pre Paid|COD|Account|On Statement
       ! All Pre-Paid needs to be Released
       ! Client Status comes first!

       IF CLI:Status > 0
          EXECUTE CLI:Status
             LOC:Release_Req_Reason     = 5         ! 'Acc. On Hold'
             LOC:Release_Req_Reason     = 6         ! 'Acc. Closed'
             LOC:Release_Req_Reason     = 7         ! 'Acc. Dormant'
       .  .

       IF LOC:Release_Req_Reason <= 0
          CASE CLI:Terms
          OF 0                                      ! Pre Paid  - all need release
             LOC:Release_Req_Reason     = 2         ! 'Acc. Pre-Paid'
             ! If pre-paid then we dont need to check the balances, they dont apply, neither does the limit
          OF 1                                      ! COD       - never need release
             ! Except where account on hold...
          ELSE
             ! (p:CID, p:Option, p_Bal_Group)
             ! This function will also update the balances to be at least todays
             IF CLI:UpdatedDate = TODAY()
                L_SI:Current            = CLI:BalanceCurrent
                L_SI:Days30             = CLI:Balance30Days
                L_SI:Days60             = CLI:Balance60Days
                L_SI:Days90             = CLI:Balance90Days
             ELSE
                Get_Client_Balances(CLI:CID, 1, LOC:Statement_Info)
             .

             ! Release reasons are hierarchal - 22 Feb 07
             ! So set the highest status where applicable, not the 1st status...

             IF L_SI:Days90 + L_SI:Days60 > 0.0
                LOC:Release_Req_Reason  = 3         ! '60 Day+ Balance'
             .

             IF L_SI:Total >= CLI:AccountLimit
                LOC:Release_Req_Reason  = 4         ! 'Account Limit'
             .

             !       0           1               2              3                 4               5              6         7
             ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown, Bad Debt, Over Paid
				
			 ! Taken out - 7/04/11
             	! (p:Table, p:Where)
             	! (STRING, <STRING>),LONG
             	!IF Count_Recs('_Invoice', 'CID = ' & INV:CID & ' AND InvoiceDateAndTime < '  & SQL_Get_DateT_G(TODAY() - CLI:PaymentPeriod,,1) & ' AND Status < 2') > 0
             	!   LOC:Release_Req_Reason  = 8         ! '(Payment) Period exceeded'
    .  .  .  	!.


    IF ~OMITTED(1)
       p_Statement_Info    = LOC:Statement_Info
    .



    ! ----------------------   Populate   ----------------------
    IF ~OMITTED(3)
       p_Release_Status_Str         = ''

       EXECUTE LOC:Release_Req_Reason + 1
          p_Release_Status_Str      = ''
          p_Release_Status_Str      = 'Released'
          p_Release_Status_Str      = 'Acc. Pre-Paid'
          p_Release_Status_Str      = '60 Day+ Balance'
          p_Release_Status_Str      = 'Account Limit'
          p_Release_Status_Str      = 'Acc. On Hold'
          p_Release_Status_Str      = 'Acc. Closed'
          p_Release_Status_Str      = 'Acc. Dormant'
          p_Release_Status_Str      = 'Period Exceeded (' & CLI:PaymentPeriod & ')'         ! New 1/02/09 - 8
       ELSE
          p_Release_Status_Str      = '<unknown release-' & LOC:Release_Req_Reason & '>'
    .  .
!                   old =======================================
!    ! (p_Statement_Info, p:DID)
!    ! (<*STRING>, ULONG=0),LONG
!    ! p:DID
!    !   0   - Delivery and Client records loaded
!    !           Client fields   - CLI:Terms, CLI:UpdatedDate, CLI:BalanceCurrent, CLI:Balance30Days, CLI:Balance60Days
!    !                             CLI:Balance90Days, CLI:CID, CLI:Status, CLI:Limit
!    !           Delivery fields - DEL:ReleasedUID
!    !
!    !   Returns
!    !   0   - None of the Below
!    !   1   - Released
!    !   2   - Acc. Pre-Paid
!    !   3   - 60 Day+ Balance
!    !   4   - Account Limit
!    !   5   - Acc. On Hold
!    !   6   - Acc. Closed
!    !   7   - Acc. Dormant
!
!
!    CLEAR(LOC:Statement_Info)
!
!    IF p:DID ~= 0
!       CLEAR(DEL:Record)
!       DEL:DID      = p:DID
!       IF Access:Deliveries.TryFetch(DEL:PKey_DID) ~= LEVEL:Benign
!       ELSE
!          CLEAR(CLI:Record)
!          CLI:CID   = DEL:CID
!          IF Access:Clients.TryFetch(CLI:PKey_CID) ~= LEVEL:Benign
!          ELSE
!    .  .  .
!
!    IF DEL:ReleasedUID ~= 0
!       LOC:Release_Req_Reason           = 1         ! Definately released
!    ELSE
!       ! CLI:Terms  - Pre Paid|COD|Account|On Statement
!       ! All Pre-Paid needs to be Released
!       IF CLI:Terms = 0                             ! Pre Paid  - all need release
!          LOC:Release_Req_Reason        = 2         ! 'Acc. Pre-Paid'
!       ELSIF CLI:Terms = 1                          ! COD       - never need release
!
!       ELSE
!          ! (p:CID, p:Option, p_Bal_Group)
!          ! This function will also update the balances to be at least todays
!          IF CLI:UpdatedDate = TODAY()
!             L_SI:Current               = CLI:BalanceCurrent
!             L_SI:Days30                = CLI:Balance30Days
!             L_SI:Days60                = CLI:Balance60Days
!             L_SI:Days90                = CLI:Balance90Days
!          ELSE
!             Get_Client_Balances(CLI:CID, 1, LOC:Statement_Info)
!          .
!
!          ! Release reasons are hierarchal - 22 Fen 07
!          ! So set the highest status where applicable, not the 1st status...
!
!          IF L_SI:Days90 + L_SI:Days60 > 0.0
!             LOC:Release_Req_Reason     = 3         ! '60 Day+ Balance'
!          .
!
!          IF L_SI:Total >= CLI:AccountLimit
!             LOC:Release_Req_Reason     = 4         ! 'Account Limit'
!          .
!
!          IF CLI:Status > 0
!             EXECUTE CLI:Status
!                LOC:Release_Req_Reason  = 5         ! 'Acc. On Hold'
!                LOC:Release_Req_Reason  = 6         ! 'Acc. Closed'
!                LOC:Release_Req_Reason  = 7         ! 'Acc. Dormant'
!    .  .  .  .
!
!
!    IF ~OMITTED(1)
!       p_Statement_Info    = LOC:Statement_Info
!    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Release_Req_Reason)

    Exit

CloseFiles     Routine
    Relate:Deliveries.Close()
    Relate:Clients.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_User_Info        PROCEDURE  (p:Info, p:Value, p:UID, p:Login) ! Declare Procedure
LOC:Result           LONG                                  ! 
LOC:Got              BYTE                                  ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Users.Open()
     .
     Access:Users.UseFile()
    ! (p:Info, p:Value, p:UID, p:Login)
    ! (BYTE, *STRING, <ULONG>, <STRING>),LONG
    !   p:Info  1   - Login
    !           2   - Password
    !           3   - User Name + Surname
    !           4   - Name
    !           5   - Surname
    !           6   - Access Level
    !           7   - UID


    IF OMITTED(3) AND OMITTED(4)
       USE:UID      = GLO:UID
       IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
          LOC:Got   = TRUE
       .
    ELSIF OMITTED(4)
       USE:UID      = p:UID
       IF Access:Users.TryFetch(USE:PKey_UID) = LEVEL:Benign
          LOC:Got   = TRUE
       .
    ELSE
       USE:Login    = p:Login
       IF Access:Users.TryFetch(USE:Key_Login) = LEVEL:Benign
          LOC:Got   = TRUE
    .  .


    IF LOC:Got = TRUE
       EXECUTE p:Info
          p:Value   = USE:Login                             ! 1
          p:Value   = USE:Password                          ! 2
          p:Value   = CLIP(USE:Name) & ' ' & USE:Surname    ! 3
          p:Value   = USE:Name                              ! 4
          p:Value   = USE:Surname                           ! 5
          p:Value   = USE:AccessLevel                       ! 6
          p:Value   = USE:UID                               ! 7
       ELSE
          MESSAGE('Option does not exist.||Option (Info): ' & p:Info, 'Get_User_Info', ICON:Hand)
       .
    ELSE
       LOC:Result   = -1
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:Users.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Check_PublicHoliday  PROCEDURE  (p:Date)                   ! Declare Procedure
LOC:Result           LONG                                  ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:PublicHolidays.Open()
     .
     Access:PublicHolidays.UseFile()
    ! p:Date
    CLEAR(PUB:Record)
    PUB:PublicHoliday   = p:Date
    IF Access:PublicHolidays.TryFetch(PUB:Key_PublicHoliday) = LEVEL:Benign
       LOC:Result       = TRUE
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:PublicHolidays.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
SQL_Format_Date_Query PROCEDURE  (p:Col, p:Pic)            ! Declare Procedure
LOC:Return           STRING(250)                           ! 

  CODE
    ! (p:Col, p:Pic)
    IF p:Pic ~= 0
       MESSAGE('Picture not supported.  Only dd/mm/yyyy supported.', 'SQL_Format_Date_Query', ICON:Exclamation)
    .

    LOC:Return  = 'LTRIM(DATEPART(dd, '   & CLIP(p:Col) & ')) + <39>/<39> + ' & |
                    'LTRIM(DATEPART(mm, '   & CLIP(p:Col) & ')) + <39>/<39> + ' & |
                    'LTRIM(DATEPART(yyyy, ' & CLIP(p:Col) & '))'

    RETURN(CLIP(LOC:Return))
!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
Win_Move_DI_Floors PROCEDURE 

LOC:Screen           GROUP,PRE(L_SG)                       ! 
From_Floor           STRING(35)                            ! Floor Name
To_Floor             STRING(35)                            ! Floor Name
From_FID             ULONG                                 ! Floor ID
To_FID               ULONG                                 ! Floor ID
From_Date            DATE                                  ! 
To_Date              DATE                                  ! 
                     END                                   ! 
FDB5::View:FileDrop  VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FID)
                     END
FDB6::View:FileDrop  VIEW(FloorsAlias)
                       PROJECT(A_FLO:FID)
                     END
BRW7::View:Browse    VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:FID)
                       PROJECT(DEL:CID)
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientNo)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse:DIs     QUEUE                            !Queue declaration for browse/combo box using ?List:DIs
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:DIDate             LIKE(DEL:DIDate)               !List box control field - type derived from field
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
CLI:ClientNo           LIKE(CLI:ClientNo)             !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
L_SG:From_Date         STRING(1)                      !Browse hot field - unable to determine correct data type
L_SG:To_Date           STRING(1)                      !Browse hot field - unable to determine correct data type
DEL:DID                LIKE(DEL:DID)                  !Primary key field - type derived from field
DEL:FID                LIKE(DEL:FID)                  !Browse key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:From  QUEUE                            !
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:FID                LIKE(FLO:FID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:To    QUEUE                            !
L_SG:To_Floor          STRING(1)                      !List box control field - unable to determine correct data type
A_FLO:FID              LIKE(A_FLO:FID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW7::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW7::PopupChoice    SIGNED                       ! Popup current choice
BRW7::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW7::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Move DI Floors'),AT(,,314,225),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  HLP('Win_Move_DI_Floors'),SYSTEM
                       SHEET,AT(3,4,307,201),USE(?Sheet1)
                         TAB('&1) General'),USE(?Tab1)
                           PROMPT('From Floor:'),AT(19,28),USE(?Prompt1),TRN
                           LIST,AT(95,28,108,10),USE(L_SG:From_Floor),VSCROLL,DROP(15),FORMAT('140L(2)|M~Floor~@s35@'), |
  FROM(Queue:FileDrop:From)
                           PROMPT('From DI Date:'),AT(19,47),USE(?From_Date:Prompt),TRN
                           SPIN(@d5b),AT(95,47,60,10),USE(L_SG:From_Date)
                           BUTTON('...'),AT(159,47,12,10),USE(?Calendar)
                           PROMPT('To DI Date:'),AT(19,61),USE(?To_Date:Prompt),TRN
                           SPIN(@d5b),AT(95,61,60,10),USE(L_SG:To_Date)
                           BUTTON('...'),AT(159,61,12,10),USE(?Calendar:2)
                           PANEL,AT(7,85,297,4),USE(?Panel1)
                           PROMPT('To Floor:'),AT(19,104),USE(?Prompt1:2),TRN
                           LIST,AT(95,104,108,10),USE(A_FLO:Floor),VSCROLL,DROP(15),FORMAT('140L(2)|M~To Floor~@s35@'), |
  FROM(Queue:FileDrop:To),MSG('Floor Name')
                           PROMPT('This will'),AT(19,139,275,24),USE(?Prompt_Info),FONT('Arial',12,,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                           BUTTON('&Go'),AT(255,186,49,14),USE(?Ok:2),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                         END
                         TAB('&2) From Floor DI''s'),USE(?Tab2)
                           BUTTON,AT(7,19,27,14),USE(?Button_Refresh),ICON('RefreshI.ico'),FLAT,TIP('Refresh')
                           LIST,AT(7,34,297,167),USE(?List:DIs),HVSCROLL,FORMAT('40R(1)|M~DI No.~L(2)@n_10@36R(1)|' & |
  'M~DI Date~L(2)@d5b@60L(1)|M~Client Reference~L(2)@s60@36R(1)|M~Client No.~L(2)@n_10b' & |
  '@90L(1)|M~Client Name~L(2)@s100@'),FROM(Queue:Browse:DIs),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('&Close'),AT(262,208,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(3,208,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW7::LastSortOrder       BYTE
BRW7::SortHeader  CLASS(SortHeaderClassType) !Declare SortHeader Class
QueueResorted          PROCEDURE(STRING pString),VIRTUAL
                  END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB5                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:From           !Reference to display queue
                     END

FDB6                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:To             !Reference to display queue
                     END

BRW:DIs              CLASS(BrowseClass)                    ! Browse using ?List:DIs
Q                      &Queue:Browse:DIs              !Reference to browse queue
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                      ! Default Locator
Calendar8            CalendarClass
Calendar9            CalendarClass
View_DI         VIEW(Deliveries)
    PROJECT(DEL:DID)
    .


DI_View         ViewManager



sql_            SQLQueryClass

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Move_DIs            ROUTINE
    sql_.PropSQL('UPDATE Deliveries SET FID = ' & L_SG:To_FID & |
            ' WHERE FID = ' & L_SG:From_FID & ' AND DIDateAndTime >= ' & SQL_Get_DateT_G(L_SG:From_Date) & |
            ' AND DIDateAndTime < ' & SQL_Get_DateT_G(L_SG:To_Date + 1))
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Win_Move_DI_Floors')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_SG:From_Date',L_SG:From_Date)           ! Added by: BrowseBox(ABC)
  BIND('L_SG:To_Date',L_SG:To_Date)               ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddItem(?Cancel,RequestCancelled)          ! Add the cancel control to the window manager
  Relate:Deliveries.Open                          ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Relate:DeliveriesAlias.Open                     ! File DeliveriesAlias used by this procedure, so make sure it's RelationManager is open
  Relate:FloorsAlias.Open                         ! File FloorsAlias used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW:DIs.Init(?List:DIs,Queue:Browse:DIs.ViewPosition,BRW7::View:Browse,Queue:Browse:DIs,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  BRW:DIs.Q &= Queue:Browse:DIs
  BRW:DIs.AddSortOrder(,DEL:FKey_FID)             ! Add the sort order for DEL:FKey_FID for sort order 1
  BRW:DIs.AddRange(DEL:FID,L_SG:From_Floor)       ! Add single value range limit for sort order 1
  BRW:DIs.AddLocator(BRW7::Sort0:Locator)         ! Browse has a locator for sort order 1
  BRW7::Sort0:Locator.Init(,DEL:FID,1,BRW:DIs)    ! Initialize the browse locator using  using key: DEL:FKey_FID , DEL:FID
  BRW:DIs.AppendOrder('+DEL:DINo')                ! Append an additional sort order
  BRW:DIs.SetFilter('(DEL:DIDate >= L_SG:From_Date AND DEL:DIDate << L_SG:To_Date + 1)') ! Apply filter expression to browse
  BRW:DIs.AddField(DEL:DINo,BRW:DIs.Q.DEL:DINo)   ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW:DIs.AddField(DEL:DIDate,BRW:DIs.Q.DEL:DIDate) ! Field DEL:DIDate is a hot field or requires assignment from browse
  BRW:DIs.AddField(DEL:ClientReference,BRW:DIs.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW:DIs.AddField(CLI:ClientNo,BRW:DIs.Q.CLI:ClientNo) ! Field CLI:ClientNo is a hot field or requires assignment from browse
  BRW:DIs.AddField(CLI:ClientName,BRW:DIs.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW:DIs.AddField(L_SG:From_Date,BRW:DIs.Q.L_SG:From_Date) ! Field L_SG:From_Date is a hot field or requires assignment from browse
  BRW:DIs.AddField(L_SG:To_Date,BRW:DIs.Q.L_SG:To_Date) ! Field L_SG:To_Date is a hot field or requires assignment from browse
  BRW:DIs.AddField(DEL:DID,BRW:DIs.Q.DEL:DID)     ! Field DEL:DID is a hot field or requires assignment from browse
  BRW:DIs.AddField(DEL:FID,BRW:DIs.Q.DEL:FID)     ! Field DEL:FID is a hot field or requires assignment from browse
  BRW:DIs.AddField(CLI:CID,BRW:DIs.Q.CLI:CID)     ! Field CLI:CID is a hot field or requires assignment from browse
  INIMgr.Fetch('Win_Move_DI_Floors',QuickWindow)  ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
  FDB5.Init(?L_SG:From_Floor,Queue:FileDrop:From.ViewPosition,FDB5::View:FileDrop,Queue:FileDrop:From,Relate:Floors,ThisWindow)
  FDB5.Q &= Queue:FileDrop:From
  FDB5.AddSortOrder(FLO:Key_Floor)
  FDB5.AddField(FLO:Floor,FDB5.Q.FLO:Floor) !List box control field - type derived from field
  FDB5.AddField(FLO:FID,FDB5.Q.FLO:FID) !Primary key field - type derived from field
  FDB5.AddUpdateField(FLO:FID,L_SG:From_FID)
  ThisWindow.AddItem(FDB5.WindowComponent)
  FDB5.DefaultFill = 0
  FDB6.Init(?A_FLO:Floor,Queue:FileDrop:To.ViewPosition,FDB6::View:FileDrop,Queue:FileDrop:To,Relate:FloorsAlias,ThisWindow)
  FDB6.Q &= Queue:FileDrop:To
  FDB6.AddSortOrder(A_FLO:Key_Floor)
  FDB6.AddField(L_SG:To_Floor,FDB6.Q.L_SG:To_Floor) !List box control field - unable to determine correct data type
  FDB6.AddField(A_FLO:FID,FDB6.Q.A_FLO:FID) !Primary key field - type derived from field
  FDB6.AddUpdateField(A_FLO:FID,L_SG:To_FID)
  ThisWindow.AddItem(FDB6.WindowComponent)
  FDB6.DefaultFill = 0
  BRW:DIs.AddToolbarTarget(Toolbar)               ! Browse accepts toolbar control
  BRW:DIs.ToolbarItem.HelpButton = ?Help
  BRW7::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW7::FormatManager.Init('FunTrnIS','Win_Move_DI_Floors',1,?List:DIs,7,BRW7::PopupTextExt,Queue:Browse:DIs,5,LFM_CFile,LFM_CFile.Record)
  BRW7::FormatManager.BindInterface(,,,'.\FunTrnIS.INI')
  SELF.SetAlerts()
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW7::SortHeader.Init(Queue:Browse:DIs,?List:DIs,'','',BRW7::View:Browse,DEL:PKey_DID)
  BRW7::SortHeader.UseSortColors = False
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
    Relate:DeliveriesAlias.Close
    Relate:FloorsAlias.Close
  !Kill the Sort Header
  BRW7::SortHeader.Kill()
  END
  ! List Format Manager destructor
  BRW7::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Win_Move_DI_Floors',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW7::SortHeader.SetAlerts()


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update()
      Calendar8.SelectOnClose = True
      Calendar8.Ask('Select a Date',L_SG:From_Date)
      IF Calendar8.Response = RequestCompleted THEN
      L_SG:From_Date=Calendar8.SelectedDate
      DISPLAY(?L_SG:From_Date)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update()
      Calendar9.SelectOnClose = True
      Calendar9.Ask('Select a Date',L_SG:To_Date)
      IF Calendar9.Response = RequestCompleted THEN
      L_SG:To_Date=Calendar9.SelectedDate
      DISPLAY(?L_SG:To_Date)
      END
      ThisWindow.Reset(True)
    OF ?Ok:2
      ThisWindow.Update()
          DO Move_DIs
    OF ?Button_Refresh
      ThisWindow.Update()
      BRW:DIs.ResetFromFile()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Take Sort Headers Events
  IF BRW7::SortHeader.TakeEvents()
     RETURN Level:Notify
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          sql_.Init(GLO:DBOwner, '_SQLTemp2')
          ?Prompt_Info{PROP:Text} = ''
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW:DIs.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW7::LastSortOrder<>NewOrder THEN
     BRW7::SortHeader.ClearSort()
  END
  IF BRW7::LastSortOrder <> NewOrder THEN
     BRW7::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW7::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW:DIs.TakeNewSelection PROCEDURE

  CODE
  IF BRW7::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW7::PopupTextExt = ''
        BRW7::PopupChoiceExec = True
        BRW7::FormatManager.MakePopup(BRW7::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW7::PopupTextExt = '|-|' & CLIP(BRW7::PopupTextExt)
        END
        BRW7::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW7::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW7::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW7::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW7::PopupChoiceOn AND BRW7::PopupChoiceExec THEN
     BRW7::PopupChoiceExec = False
     BRW7::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW7::PopupTextExt)
     BRW7::SortHeader.RestoreHeaderText()
     BRW:DIs.RestoreSort()
     IF BRW7::FormatManager.DispatchChoice(BRW7::PopupChoice)
        BRW7::SortHeader.ResetSort()
     ELSE
        BRW7::SortHeader.SortQueue()
     END
  END

BRW7::SortHeader.QueueResorted       PROCEDURE(STRING pString)
  CODE
    IF pString = ''
       BRW:DIs.RestoreSort()
       BRW:DIs.ResetSort(True)
    ELSE
       BRW:DIs.ReplaceSort(pString,BRW7::Sort0:Locator)
       BRW:DIs.SetLocatorFromSort()
    END
