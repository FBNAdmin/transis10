

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS005.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! List of DIDs on MID
!!! </summary>
Get_Manifest_DIDs    PROCEDURE  (p:MID, p:DIDs)            ! Declare Procedure
LOC:DID_Q            QUEUE,PRE(L_DQ)                       ! 
DID                  ULONG                                 ! Delivery ID
                     END                                   ! 
LOC:Return           LONG                                  ! 
Tek_Failed_File     STRING(100)

ML:View         ViewManager



ML_View       VIEW(ManifestLoadAlias)
    PROJECT(A_MAL:MLID, A_MAL:MID)
       JOIN(A_MALD:FSKey_MLID_DIID, A_MAL:MLID)
       PROJECT(A_MALD:MLID, A_MALD:DIID, A_MALD:UnitsLoaded)
          JOIN(A_DELI:PKey_DIID, A_MALD:DIID)
          PROJECT(A_DELI:DIID, A_DELI:DID, A_DELI:Units)
    .  .  .

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
     Access:ManifestLoadAlias.UseFile()
     Access:ManifestLoadDeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
    ! p:MID, p:DIDs
    ML:View.Init(ML_View, Relate:ManifestLoadAlias)
    ML:View.AddSortOrder(A_MAL:FKey_MID)
    ML:View.AppendOrder('A_DELI:DID,A_DELI:DIID')
    ML:View.AddRange(A_MAL:MID, p:MID)
    !ML:View.SetFilter()

    ML:View.Reset()
    LOOP
       IF ML:View.Next() ~= LEVEL:Benign
          BREAK
       .

       ! Our distinct
       L_DQ:DID = A_DELI:DID
       GET(LOC:DID_Q, L_DQ:DID)
       IF ~ERRORCODE()
          CYCLE
       .
       L_DQ:DID = A_DELI:DID
       ADD(LOC:DID_Q, L_DQ:DID)



       IF CLIP(p:DIDs) = ''
          p:DIDs    = A_DELI:DID
       ELSE
          p:DIDs    = CLIP(p:DIDs) & ',' & A_DELI:DID
    .  .

    ML:View.Kill()

    LOC:Return  = RECORDS(LOC:DID_Q)
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:ManifestLoadAlias.Close()
    Relate:ManifestLoadDeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! Note: Multi DIDs are not counted as unloaded!
!!! </summary>
Get_ManDelItems_UnLoaded PROCEDURE  (p:MID, p:Items_DIID_List, p:Multi_DIDs) ! Declare Procedure
LOC:Not_Loaded       ULONG                                 ! 
Tek_Failed_File     STRING(100)

ML_View       VIEW(ManifestLoadAlias)
    PROJECT(A_MAL:MLID, A_MAL:MID)

       JOIN(A_MALD:FSKey_MLID_DIID, A_MAL:MLID)
       PROJECT(A_MALD:MLID, A_MALD:DIID, A_MALD:UnitsLoaded)

          JOIN(A_DELI:PKey_DIID, A_MALD:DIID)
          PROJECT(A_DELI:DIID, A_DELI:DID, A_DELI:Units)

             JOIN(A_DEL:PKey_DID, A_DELI:DID)
             PROJECT(A_DEL:DID, A_DEL:DINo, A_DEL:MultipleManifestsAllowed, A_DEL:Manifested)
    .  .  .  .

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
     Access:ManifestLoadAlias.UseFile()
     Access:ManifestLoadDeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
     Access:DeliveriesAlias.UseFile()
    ! (p:MID, p:Items_DIID_List, p:Multi_DIDs)
    
    ! We want to Check all DIDs that are loaded / partially loaded on this Manifest are allowed to be.
    ! If partially loaded they must have the flag set.
    !
    ! To do this we need to itterate through the ManifestLoads, checking and then the ManifestLoadDeliveries
    ! to get the Deliveries.
    ! For each Delivery check flag, if partial allowed then ignore if not report.

    SET(A_MAL:FKey_MID)
    BIND('A_MAL:MID',A_MAL:MID)
    ML_View{PROP:Filter}    = 'A_MAL:MID = ' & p:MID
    ML_View{PROP:Order}     = 'A_MAL:MID, A_DEL:DID, A_DELI:DIID'
    OPEN(ML_View)
    SET(ML_View)
    IF ERRORCODE()
       MESSAGE('Error on Open View - ML_View.||Error: ' & ERROR(), 'Get_ManDelItems_UnLoaded', ICON:Hand)
    ELSE
       LOOP
          NEXT(ML_View)
          IF ERRORCODE()
             BREAK
          .

!    db.debugout('A_MAL:MID: ' & A_MAL:MID & '|A_DEL:DID: ' & A_DEL:DID & '|A_DELI:DIID: ' & A_DELI:DIID & |
!           '|A_DELI:ItemNo: ' & A_DELI:ItemNo & '|A_MALD:MLDID: ' & A_MALD:MLDID)

          ! A_DEL:DID, A_DEL:DINo, A_DEL:MultipleManifestsAllowed, A_DEL:Manifested
          IF A_DEL:MultipleManifestsAllowed = TRUE
             ! We are allowed multiple manifests for this DID
             IF ~OMITTED(3)
                IF CLIP(p:Multi_DIDs) = ''
                   p:Multi_DIDs   = A_DEL:DID
                ELSE
                   p:Multi_DIDs   = CLIP(p:Multi_DIDs) & ',' & A_DEL:DID
             .  .
          ELSE
             IF OMITTED(2)                            ! (p:DID, p:Option, p:DIID, p:Items_DIID_List)  Option 1 = Total Items not loaded on this DI
                LOC:Not_Loaded   += Get_DelItem_s_Totals(A_DEL:DID, 1)
             ELSE
                LOC:Not_Loaded   += Get_DelItem_s_Totals(A_DEL:DID, 1,, p:Items_DIID_List)
    .  .  .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Not_Loaded)

    Exit

CloseFiles     Routine
    Relate:ManifestLoadAlias.Close()
    Relate:ManifestLoadDeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Relate:DeliveriesAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Manifest_Del_MIDs PROCEDURE  (p:MID, p:Multi_MIDs)     ! Declare Procedure
LOC:Not_Loaded       ULONG                                 ! 
LOC:DID_Q            QUEUE,PRE(L_DQ)                       ! 
DID                  ULONG                                 ! Delivery ID
                     END                                   ! 
LOC:MID_Q            QUEUE,PRE(L_MQ)                       ! 
MID                  ULONG                                 ! Manifest ID
DIID                 ULONG                                 ! Delivery Item ID
CreatedDate          DATE                                  ! Created Date
                     END                                   ! 
LOC:Idx              LONG                                  ! 
Tek_Failed_File     STRING(100)

ML:View         ViewManager
Del:View        ViewManager



ML_View       VIEW(ManifestLoadAlias)
    PROJECT(A_MAL:MLID, A_MAL:MID)
       JOIN(A_MALD:FSKey_MLID_DIID, A_MAL:MLID)
       PROJECT(A_MALD:MLID, A_MALD:DIID, A_MALD:UnitsLoaded)
          JOIN(A_DELI:PKey_DIID, A_MALD:DIID)
          PROJECT(A_DELI:DIID, A_DELI:DID, A_DELI:Units)
!                   JOIN(A_DEL:PKey_DID, A_DELI:DID)
!                      PROJECT(A_DEL:DID, A_DEL:DINo, A_DEL:MultipleManifestsAllowed, A_DEL:Manifested)
    .  .  .     !.




Del_View       VIEW(DeliveryItemAlias)
    PROJECT(A_DELI:DIID, A_DELI:DID, A_DELI:ItemNo)
       JOIN(A_MALD:FKey_DIID, A_DELI:DIID)
       PROJECT(A_MALD:MLID, A_MALD:DIID, A_MALD:UnitsLoaded)
          JOIN(A_MAL:PKey_MLID, A_MALD:MLID)
          PROJECT(A_MAL:MLID, A_MAL:MID)
             JOIN(A_MAN:PKey_MID, A_MAL:MID)
             PROJECT(A_MAN:MID, A_MAN:CreatedDate)
    .  .  .  .

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestAlias.Open()
     .
     Access:ManifestLoadAlias.UseFile()
     Access:ManifestLoadDeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
     Access:DeliveriesAlias.UseFile()
     Access:ManifestAlias.UseFile()
    ! (p:MID, p:Multi_MIDs)
    ! (ULONG, <*STRING>),ULONG
    
    ! We want to Check all DIDs that are loaded / partially loaded on this Manifest are allowed to be.
    ! If partially loaded they must have the flag set.
    !
    ! To do this we need to itterate through the ManifestLoads, checking and then the ManifestLoadDeliveries
    ! to get the Deliveries.
    ! For each Delivery check flag, if partial allowed then ignore if not report.


    ! Loop through all Deliveries for this Manifest and add them to a Q
    PUSHBIND()
    BIND('A_MAL:MID',A_MAL:MID)

    ML:View.Init(ML_View, Relate:ManifestLoadAlias)
    ML:View.AddSortOrder(A_MAL:FKey_MID)
    ML:View.AppendOrder('A_MAL:MLID')
    ML:View.AddRange(A_MAL:MID, p:MID)
    !ML:View.SetFilter()

    ML:View.Reset(1)

    !SET(A_MAL:FKey_MID)
    !ML_View{PROP:Filter}    = 'A_MAL:MID = ' & p:MID
    !ML_View{PROP:Order}     = ''
    !OPEN(ML_View)
    !SET(ML_View)
    IF ERRORCODE()
       MESSAGE('Error on Open View - ML_View.||Error: ' & ERROR() & '|F Error: ' & FILEERROR(), 'Get_Manifest_Del_MIDs', ICON:Hand)
    ELSE
       LOOP
          !NEXT(ML_View)
          IF ML:View.Next() ~= LEVEL:Benign
             BREAK
          .

          L_DQ:DID      = A_DELI:DID
          GET(LOC:DID_Q, A_DELI:DID)
          IF ERRORCODE()
             L_DQ:DID   = A_DELI:DID
             ADD(LOC:DID_Q)
    .  .  .
    ML:View.Kill()



    ! Loop through Delivery Items in Q and their Manifests and count ones not like passed
    BIND('A_DELI:DID',A_DELI:DID)

    Del:View.Init(Del_View, Relate:DeliveryItemAlias)
    Del:View.AddSortOrder(A_DELI:FKey_DID_ItemNo)
    !Del:View.AppendOrder()
    Del:View.AddRange(A_DELI:DID, L_DQ:DID)
    !Del:View.SetFilter()

    LOC:Idx = 0
    LOOP
       LOC:Idx  += 1
       GET(LOC:DID_Q, LOC:Idx)
       IF ERRORCODE()
          BREAK
       .

       !SET(A_DELI:FKey_DID_ItemNo)
       Del:View.ApplyRange()                ! When L_DQ:DID changes
       Del:View.Reset(1)
       !Del_View{PROP:Filter}   = 'A_DELI:DID = ' & L_DQ:DID
       !Del_View{PROP:Order}    = ''
       !OPEN(Del_View)
       !SET(Del_View)
       IF ERRORCODE()
          MESSAGE('Error on Open View - Del_View.||Error: ' & ERROR() & '|FError: ' & ERROR(), 'Get_Manifest_Del_MIDs', ICON:Hand)
       ELSE
          LOOP
             IF Del:View.Next() ~= LEVEL:Benign
                BREAK
             .

             IF A_MAN:MID ~= p:MID
                L_MQ:MID              = A_MAN:MID
                GET(LOC:MID_Q, L_MQ:MID)
                IF ERRORCODE()
                   L_MQ:MID           = A_MAN:MID
                   L_MQ:DIID          = A_DELI:DIID
                   L_MQ:CreatedDate   = A_MAN:CreatedDate
                   ADD(LOC:MID_Q, L_MQ:MID)
    .  .  .  .  .
    Del:View.Kill()


    IF RECORDS(LOC:MID_Q) > 0
       LOC:Idx      = 0
       LOOP
          LOC:Idx  += 1
          GET(LOC:MID_Q, LOC:Idx)
          IF ERRORCODE()
             BREAK
          .

          IF CLIP(p:Multi_MIDs) = ''
             p:Multi_MIDs   = L_MQ:MID & ',' & L_MQ:DIID
          ELSE
             p:Multi_MIDs   = CLIP(p:Multi_MIDs) & ',' & L_MQ:MID & ',' & L_MQ:DIID
       .  .

       LOC:Not_Loaded       = RECORDS(LOC:MID_Q)
    .

    POPBIND()
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Not_Loaded)

    Exit

CloseFiles     Routine
    Relate:ManifestLoadAlias.Close()
    Relate:ManifestLoadDeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Relate:DeliveriesAlias.Close()
    Relate:ManifestAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Transporter_Rate PROCEDURE  (p:TID, p:JID, p:VCID, p:Setup, p:Setup_VCID, p:MinimiumLoad, p:BaseCharge, p:TRID, p:Local_Rate, p:Eff_Date) ! Declare Procedure
LOC:Setup_Group      GROUP,PRE(L_SG)                       ! 
GeneralRatesTransportID ULONG                              ! Transporter ID
Setup_Loaded_ID      ULONG(1)                              ! this is changed whenever the setup is changed and will cause the statics to reload
                     END                                   ! 
R_Rate_G             GROUP,PRE(LOC)                        ! 
Rate                 LIKE(MAN:Rate)                        ! 
                     END                                   ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__RatesTransporter.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Setup.Open()
     .
     Access:__RatesTransporter.UseFile()
     Access:Setup.UseFile()
    ! (p:TID, p:JID, p:VCID, p:Setup, p:Setup_VCID, p:MinimiumLoad, p:BaseCharge, p:TRID, p:Local_Rate, p:Eff_Date)
    !   1       2       3       4       5               6               7           8           9           10
    ! (ULONG, ULONG, ULONG=0, *BYTE, <*ULONG>, <*DECIMAL>, <*DECIMAL>, <*ULONG>, BYTE=0, LONG=0), ULONG
    !   1       2       3       4       5           6           7       8           9       10

    IF p:Eff_Date = 0
       p:Eff_Date   = TODAY()
    .

    ! We search for the Transporter and Journey requested - optionally also Vehicle Composition
    ! In the case of a Setup rate being requested we may not have a Vehicle Composition
    ! We clear buffer and return 0.0 for rate when nothing found.
    ! We return the Setup flag if we got it from setup and optionally the VCID
    ! We return the MinimiumLoad and BaseCharge optionally

    CLEAR(TRRA:Record)
    TRRA:TID                = p:TID

    IF p:JID = 0 AND p:Local_Rate = FALSE
       MESSAGE('No Journey has been specified and Local Rate option is not set!||Please write this message down and contact support.', 'Get_Transporter_Rate', ICON:Hand)
    .

!    db.debugout('[Get_Transporter_Rate]  Should be not zero - p:VCID: ' & p:VCID & ',   p:TID: ' & p:TID & ',  p:JID: ' & p:JID & ',  p:Eff_Date: ' & FORMAT(p:Eff_Date,@d6))

    TRRA:JID                = p:JID
    IF p:VCID ~= 0
       TRRA:VCID            = p:VCID
       TRRA:Effective_Date  = p:Eff_Date
       SET(TRRA:CKey_TID_JID_VCID_EffDate, TRRA:CKey_TID_JID_VCID_EffDate)
       LOOP
          IF Access:__RatesTransporter.TryNext() ~= LEVEL:Benign
             CLEAR(TRRA:Record)
             BREAK
          .
          IF TRRA:TID ~= p:TID OR TRRA:JID ~= p:JID
             CLEAR(TRRA:Record)
          .
          BREAK
       .

       IF TRRA:TRID = 0
!    db.debugout('[Get_Transporter_Rate]  Setup rate will be used')
          DO Setup_Rate
       .
    ELSE
       ! Get a Setup rate
       DO Setup_Rate
    .

    IF TRRA:TRID ~= 0
       IF ~OMITTED(6)
          p:MinimiumLoad    = TRRA:MinimiumLoad
       .
       IF ~OMITTED(7)
          p:BaseCharge      = TRRA:BaseCharge
       .
       IF ~OMITTED(8)
          p:TRID            = TRRA:TRID
    .  .


    LOC:Rate    = TRRA:PerRate
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(R_Rate_G)

    Exit

CloseFiles     Routine
    Relate:__RatesTransporter.Close()
    Relate:Setup.Close()
    Exit
Setup_Rate                            ROUTINE
    DO Load_Setup

    CLEAR(TRRA:Record,-1)
    TRRA:TID            = p:TID
    TRRA:JID            = p:JID
    TRRA:Effective_Date = p:Eff_Date

    SET(TRRA:CKey_TID_JID_VCID_EffDate, TRRA:CKey_TID_JID_VCID_EffDate)
    LOOP
       IF Access:__RatesTransporter.TryNext() ~= LEVEL:Benign
          CLEAR(TRRA:Record)
          BREAK
       .

       IF TRRA:TID ~= p:TID OR TRRA:JID ~= p:JID
          CLEAR(TRRA:Record)
          BREAK
       .

       ! (p:TID, p:JID, p:VCID, p:Setup, p:Setup_VCID, p:MinimiumLoad, p:BaseCharge, p:TRID)
       !   1       2       3       4       5               6               7           8
       ! (ULONG, ULONG, ULONG=0, *BYTE, <ULONG>, <*DECIMAL>, <*DECIMAL>, <ULONG>), ULONG
       !   1       2       3       4       5           6           7       8
       p:Setup  = TRUE

       IF ~OMITTED(5)
          p:Setup_VCID    = TRRA:VCID
       .

       BREAK
    .
    EXIT

Load_Setup                           ROUTINE
    IF L_SG:Setup_Loaded_ID ~= GLO:Setup_Loaded_ID
       SET(Setup)
       NEXT(Setup)
       IF ~ERRORCODE()
          L_SG:Setup_Loaded_ID          = GLO:Setup_Loaded_ID

          L_SG:GeneralRatesTransportID  = SET:GeneralRatesTransporterID
    .  .
    EXIT
!!! <summary>
!!! Generated from procedure template - Source
!!! Change the Floors
!!! </summary>
Move_ManDeliveries   PROCEDURE  (p:MID, p:FID)             ! Declare Procedure
LOC:Done_DIDs_Q      QUEUE,PRE(L_DQ)                       ! 
DID                  ULONG                                 ! Delivery ID
                     END                                   ! 
LOC:Result           LONG                                  ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoad.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Deliveries.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
     Access:ManifestLoad.UseFile()
     Access:DeliveryItemAlias.UseFile()
     Access:Deliveries.UseFile()
     Access:ManifestLoadDeliveriesAlias.UseFile()
    ! (p:MID, p:FID)
    ! (ULONG, ULONG),LONG

    CLEAR(MAL:Record, -1)
    MAL:MID     = p:MID
    SET(MAL:FKey_MID, MAL:FKey_MID)
    LOOP
       IF Access:ManifestLoad.TryNext() ~= LEVEL:Benign
          BREAK
       .

       IF MAL:MID ~= p:MID
          BREAK
       .

       CLEAR(A_MALD:Record, -1)
       A_MALD:MLID  = MAL:MLID
       SET(A_MALD:FSKey_MLID_DIID, A_MALD:FSKey_MLID_DIID)
       LOOP
          IF Access:ManifestLoadDeliveriesAlias.TryNext() ~= LEVEL:Benign
             BREAK
          .
          IF A_MALD:MLID ~= MAL:MLID
             BREAK
          .

          ! Now get the Item record to get the DID
          A_DELI:DIID               = A_MALD:DIID
          IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) = LEVEL:Benign
             ! Check if we have updated this DID or not
             L_DQ:DID               = A_DELI:DID
             GET(LOC:Done_DIDs_Q, L_DQ:DID)
             IF ERRORCODE()
                ! We have no record of it - update it now

                ! New condition - all items of this DI must be on Manifests in state 3 (transferred) or higher
                ! before we will set the floor.
                !  (p:DID, p:MID, p:DIIDs, p:Option)
                !  (ULONG, <*STRING>, <*STRING>, BYTE=0),BYTE
                IF Get_DelMan_State(A_DELI:DID,,, 1) < 3            ! Not complete on all items
                   L_DQ:DID      = A_DELI:DID
                   ADD(LOC:Done_DIDs_Q)
                ELSE
                   DEL:DID             = A_DELI:DID
                   IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
                      DEL:FID          = p:FID
                      IF Access:Deliveries.TryUpdate() = LEVEL:Benign
                         L_DQ:DID      = A_DELI:DID
                         ADD(LOC:Done_DIDs_Q)
                      ELSE
                         LOC:Result    = -1
                         MESSAGE('Failed to Update the Delivery (DID: ' & A_DELI:DID & ') with the Floor ID: ' & p:FID & '||G Error: ' & Access:Deliveries.GetError(), 'Delivery Update', ICON:Hand)
                      .
                   ELSE
                      LOC:Result       = -2
                      MESSAGE('Failed to Fetch the Delivery (DID: ' & A_DELI:DID & ') to update with the Floor ID: ' & p:FID & '||G Error: ' & Access:Deliveries.GetError(), 'Delivery Fetch', ICON:Hand)
    .  .  .  .  .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:ManifestLoad.Close()
    Relate:DeliveryItemAlias.Close()
    Relate:Deliveries.Close()
    Relate:ManifestLoadDeliveriesAlias.Close()
    Exit
