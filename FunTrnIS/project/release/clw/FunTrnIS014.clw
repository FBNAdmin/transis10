

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('FUNTRNIS014.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! from Payments Allocations
!!! </summary>
Gen_Remittance_Paid  PROCEDURE  (p:REMRID, p:TID, p:RunDate, p:RunTime, p:FromDate, p:ToDate, p:Run_Type, p:OutPut, p:Paid_Total) ! Declare Procedure
LOC:Result           LONG                                  ! 
LOC:MonthEndDate     LONG                                  ! 
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
LOC:Accum_Group      GROUP,PRE(L_AG)                       ! 
Amount               DECIMAL(10,2)                         ! 
Debit                DECIMAL(10,2)                         ! 
Credit               DECIMAL(10,2)                         ! 
                     END                                   ! 
LOC:Credited         DECIMAL(10,2)                         ! 
Tek_Failed_File     STRING(100)

Inv_View            VIEW(TransporterPaymentsAllocations)
    !PROJECT(TRAPA:TRPAID,TRAPA:TPID,TRAPA:AllocationNo,TRAPA:AllocationDate,TRAPA:AllocationTime,TRAPA:Amount,TRAPA:Comment,)
       JOIN(TRAP:PKey_TPID, TRAPA:TPID)
       !PROJECT()
    .  .

View_Inv            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_InvoiceTransporter.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TransporterPaymentsAllocations.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceTransporterAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Remittance.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_RemittanceItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TransporterPayments.Open()
     .
     Access:_InvoiceTransporter.UseFile()
     Access:TransporterPaymentsAllocations.UseFile()
     Access:InvoiceTransporterAlias.UseFile()
     Access:_Remittance.UseFile()
     Access:_RemittanceItems.UseFile()
     Access:TransporterPayments.UseFile()
    ! (p:REMRID, p:TID, p:RunDate, p:RunTime, p:FromDate, p:ToDate, p:Run_Type, p:OutPut, p:Paid_Total)
    ! (ULONG,   ULONG,  LONG,       LONG,       LONG,       LONG,   BYTE=1,       BYTE=0, <*DECIMAL>)
    !   1           2        3          4           5       6           7           8          9
    !
    ! We should run the invoice maintenance function before running the statements... maybe not though, see code
    !
    ! p:Run_Type
    !   0   - No record generation
    !   1   - Generate record

    PUSHBIND
    BIND('TRAPA:AllocationDate', TRAPA:AllocationDate)

    ! Month end date is - using the run date and the MonthEndDay, eg. 12 May run date = 25 May month end date
!    LOC:MonthEndDate            = DATE(MONTH(p:Date), p:MonthEndDay, YEAR(p:Date))

    IF p:Run_Type = 1
       CLEAR(REMI:Record)
       IF Access:_Remittance.TryPrimeAutoInc() ~= LEVEL:Benign
          LOC:Result            = -1
       ELSE
          DO Get_Details
       .
    ELSE
       DO Get_Details
    .

    IF p:Run_Type = 1 AND LOC:Result >= 0
       IF L_SI:Total = 0.0
          Access:_Remittance.CancelAutoInc()
       ELSE
          ! REMI:REMID
          REMI:RERID            = p:REMRID
          REMI:RemittanceDate   = p:RunDate
          REMI:RemittanceTime   = p:RunTime
          REMI:TID              = p:TID
          REMI:BID              = GLO:BranchID
          !REMI:Days90           = L_SI:Days90
          !REMI:Days60           = L_SI:Days60
          !REMI:Days30           = L_SI:Days30
          !REMI:Current          = L_SI:Current
          REMI:Total            = L_SI:Total

          IF Access:_Remittance.TryInsert() = LEVEL:Benign
             LOC:Result         = REMI:REMID
          ELSE
             Access:_Remittance.CancelAutoInc()
             LOC:Result         = -2
    .  .  .

    POPBIND


    IF ~OMITTED(9)
       p:Paid_Total     = L_SI:Total
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:_InvoiceTransporter.Close()
    Relate:TransporterPaymentsAllocations.Close()
    Relate:InvoiceTransporterAlias.Close()
    Relate:_Remittance.Close()
    Relate:_RemittanceItems.Close()
    Relate:TransporterPayments.Close()
    Exit
Get_Details                           ROUTINE
    View_Inv.Init(Inv_View, Relate:TransporterPaymentsAllocations)
    View_Inv.AddSortOrder(TRAP:FKey_TID)
    View_Inv.AppendOrder('TRAPA:AllocationDate, TRAPA:TRPAID')
    View_Inv.AddRange(TRAP:TID, p:TID)

    ! Note: use of tomorrow as date, with no time specified.

    IF p:FromDate > 0
       View_Inv.SetFilter('TRAPA:AllocationDate >= ' & p:FromDate, 'ikb1')
    .
    IF p:ToDate > 0
       View_Inv.SetFilter('TRAPA:AllocationDate < ' & p:ToDate + 1, 'ikb2')
    .

    View_Inv.Reset()
    IF ERRORCODE()
       db.debugout('[Gen_Remittance_Paid]  Error on view - Error: ' & ERROR())
    .
    LOOP
       IF View_Inv.Next() ~= LEVEL:Benign
          BREAK
       .

       ! Add this payment in
       CLEAR(REMIT:Record)
       IF Access:_RemittanceItems.TryPrimeAutoInc() ~= LEVEL:Benign
       ELSE
          !REMIT:REMIID
          REMIT:REMID        = REMI:REMID
          REMIT:InvoiceDate  = TRAPA:AllocationDate
          REMIT:InvoiceTime  = TRAPA:AllocationTime
          REMIT:TIN          = TRAPA:TIN
          REMIT:MID          = TRAPA:MID
          REMIT:AmountPaid   = TRAPA:Amount

          IF Access:_RemittanceItems.TryInsert() = LEVEL:Benign
             L_SI:Total     += TRAPA:Amount
             
          ELSE
             Access:_RemittanceItems.CancelAutoInc()
    .  .  .

    View_Inv.Kill()
    EXIT
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_DelItem_Client_Info PROCEDURE  (p:CID, p:Option, p:DateFrom, p:DateTo) ! Declare Procedure
LOC:Total            DECIMAL(15,2)                         ! 
LOC:Return           STRING(50)                            ! 
Tek_Failed_File     STRING(100)

View_Dels_Items         VIEW(DeliveriesAlias)
    PROJECT(A_DEL:CID, A_DEL:DID)
       JOIN(A_DELI:FKey_DID_ItemNo, A_DEL:DID)
       PROJECT(A_DELI:DID, A_DELI:DIID, A_DELI:Weight, A_DELI:VolumetricWeight, A_DELI:Units)
    .  .


!View_Dels_Items         VIEW(DeliveryItemAlias)
!    PROJECT(A_DELI:DID, A_DELI:DIID, A_DELI:Weight, A_DELI:VolumetricWeight, A_DELI:Units)
!       JOIN(A_DEL:PKey_DID, A_DELI:DID)
!       PROJECT(A_DEL:CID)
!    .  .

DelItems_View           ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
     Access:DeliveryItemAlias.UseFile()
     Access:DeliveriesAlias.UseFile()
    ! (p:CID, p:Option, p:DateFrom, p:DateTo)
    ! (ULONG, BYTE, LONG=0, LONG=0)                 , STRING
    !
    ! p:Option
    !   0. Weight of all CID items - Volumetric considered
    !   1. Total Items for CID
    !   2. Weight of all CID items - real weight

    PUSHBIND()
    BIND('A_DEL:DIDate', A_DEL:DIDate)
    BIND('A_DEL:CID', A_DEL:CID)

    DelItems_View.Init(View_Dels_Items, Relate:DeliveryItemAlias)        ! Different View
    DelItems_View.AddSortOrder()
!    DelItems_View.AddSortOrder(A_DEL:FKey_CID)         - weird SQL generated with PTID ! ???
    !DelItems_View.AppendOrder()
!    DelItems_View.AddRange(A_DEL:CID, p:CID)

    DelItems_View.SetFilter('A_DEL:CID = ' & p:CID  , 'A')

    IF p:DateFrom > 0
       DelItems_View.SetFilter('A_DEL:DIDate >= ' & p:DateFrom  , '1')
    .
    IF p:DateTo > 0
       DelItems_View.SetFilter('A_DEL:DIDate < ' & p:DateTo + 1, '2')
    .

    DelItems_View.Reset()
!    IF ERRORCODE()
!       db.debugout('[Get_DelItem_Client_Info]   Error on view - Error: ' & ERROR())
!    ELSE
!       db.debugout('[Get_DelItem_Client_Info]   p:DateFrom: ' & FORMAT(p:DateFrom, @d6) & ', p:Option: ' & p:Option & ', p:CID: ' & p:CID)
!    .
    LOOP
       IF DelItems_View.Next() ~= LEVEL:Benign
          BREAK
       .

!      db.debugout('[Get_DelItem_Client_Info]   Next DID - ' & A_DELI:DID)

       CASE p:Option
       OF 0
          ! Volumetric Weight is used for rates
          IF A_DELI:Weight < A_DELI:VolumetricWeight
             LOC:Total       += A_DELI:VolumetricWeight
          ELSE
             LOC:Total       += A_DELI:Weight
          .
       OF 1
          LOC:Total          += A_DELI:Units
       OF 2
          LOC:Total          += A_DELI:Weight
    .  .
    DelItems_View.Kill()

    UNBIND('A_DEL:DIDate')
    UNBIND('A_DEL:CID')
    POPBIND()


    LOC:Return      = LOC:Total
! Weird SQL code generated!

!A0F44H(2) 10:49:57.421 Preparing Statement 0FCA238H : SELECT  A.DID, A.DINO, A.BID, A.CID, A.CLIENTREFERENCE, A.JID, A.COLLECTIONAID, A.DELIVERYAID, A.CRTID, A.LTID, A.SID, A.TRIPSHEETINTID, A.TRIPSHEETOUTTID, A.FID, A.DC_ID, A.DELCID, A.COLLECTEDBYDRID, A.UID, B.DIID, B.DID, B.ITEMNO, B.CMID, B.COID, B.CTID, B.CONTAINERRETURNAID, B.UNITS, B.PTID, B.WEIGHT, B.VOLUMETRICWEIGHT FROM  {oj dbo.Deliveries A LEFT OUTER JOIN dbo.DeliveryItems B ON  A.DID= B.DID }  WHERE (  B.PTID = 0 AND  B.PTID = 351 )  ORDER BY  B.PTID Time Taken:0.00 secs
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Total)

    Exit

CloseFiles     Routine
    Relate:DeliveryItemAlias.Close()
    Relate:DeliveriesAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
Date_Test PROCEDURE 

Dates_Group          GROUP,PRE(L_DG)                       ! 
Date                 LONG                                  ! 
Date_Long            LONG                                  ! 
                     END                                   ! 
QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM,HLP('Date_Test'), |
  SYSTEM
                       PROMPT('Date:'),AT(45,23),USE(?L_DG:Date:Prompt)
                       SPIN(@d6b),AT(95,23,60,10),USE(L_DG:Date),RIGHT(1)
                       PROMPT('Date Long:'),AT(45,44),USE(?Date_Long:Prompt)
                       ENTRY(@n-14),AT(95,44,60,10),USE(L_DG:Date_Long),RIGHT(1),READONLY,SKIP
                       BUTTON('&OK'),AT(154,142,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(208,142,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(45,142,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Date_Test')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?L_DG:Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Date_Test',QuickWindow)                    ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('Date_Test',QuickWindow)                 ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_DG:Date
          L_DG:Date_Long  = L_DG:Date
          DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_DG:Date
          L_DG:Date_Long  = L_DG:Date
          DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_FuelSurcharge_old2 PROCEDURE  (p:CID, p:Date, p:RateDate) ! Declare Procedure
LOC:Surcharge        STRING(35)                            ! 
LOC:Buffer           USHORT                                ! 
LOC:Clients_Increase_OnBase DECIMAL(12,5)                  ! 
Tek_Failed_File     STRING(100)

View_FC            VIEW(__RatesFuelCost)
    .


FC_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__RatesFuelCost.Open()
     .
     Access:ClientsAlias.UseFile()
     Access:__RatesFuelCost.UseFile()
    ! (p:CID, p:Date, p:RateDate)
    ! (ULONG, LONG=0, LONG=0),STRING
    !
    ! p:Date      - effective date
    ! p:RateDate  - purpose of this is to allow to use a specific date for the FuelCost to the RateFuelCost, used on Client form

    A_CLI:CID   = p:CID
    IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) ~= LEVEL:Benign
       CLEAR(A_CLI:Record)
    .

    IF A_CLI:FuelSurchargeActive = TRUE
       LOC:Buffer   = Access:__RatesFuelCost.SaveBuffer()

       IF p:Date = 0
          p:Date    = TODAY()
       .

       BIND('FCRA:Effective_Date',FCRA:Effective_Date)

       FC_View.Init(View_FC, Relate:__RatesFuelCost)
       FC_View.AddSortOrder(FCRA:CKey_CID_EffDateDesc)          ! Decending date
       !FC_View.AppendOrder('FCRA:Effective_Date')
       FC_View.AddRange(FCRA:CID, p:CID)
       FC_View.SetFilter('FCRA:Effective_Date < ' & p:Date + 1)

       FC_View.Reset()

       LOOP
          IF FC_View.Next() ~= LEVEL:Benign
             BREAK
          .

    ! (p:FuelCost, p:FuelCost_Date, p:Increase_OnBase, p:As_At_Date, p:Clients_FuelBaseRate, p:Cost_As_At, p:FuelCost_Diff
    !       1             2                 3                4                 5                  6                 7
    !              p:CPL_Change_Per, p:FuelBaseRate, p:O)
    !                     8                9          10
    ! (*DECIMAL, LONG, *DECIMAL, LONG=0, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>, <*DECIMAL>)
    !       1      2       3       4         5            6           7           8           9          10

          !Get_Client_FuelSurcharge(FCRA:FuelCost, FCRA:Effective_Date, LOC:Clients_Increase_OnBase, p:Date)

          LOC:Surcharge         = LOC:Clients_Increase_OnBase

          BREAK
       .

       FC_View.Kill()

       UNBIND('FCRA:Effective_Date')

       Access:__RatesFuelCost.RestoreBuffer(LOC:Buffer)
    .
           Relate:ClientsAlias.Close()
           Relate:__RatesFuelCost.Close()


       Return(LOC:Surcharge)

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Special_Set_Invoiced_Dates PROCEDURE 

QuickWindow          WINDOW('Special Functions'),AT(,,260,160),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,HLP('Special_Set_Invoiced_Date_to_DI'),SYSTEM
                       SHEET,AT(4,4,252,135),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           BUTTON('Debtors Invoices - Set Invoiced Date to Manifest Date'),AT(37,46,185,14),USE(?Button_Do), |
  TIP('This will set the Invoiced Date on all Invoices to the date of the Manifest they' & |
  ' are attached to.<0DH,0AH>Cedit notes are excluded.')
                           BUTTON('Creditors Invoices - Set Invoiced Date to Manifest Date'),AT(37,76,,14),USE(?Button_Do:2), |
  TIP('This will set the Invoiced Date on all Invoices to the DI date')
                         END
                       END
                       BUTTON('&Close'),AT(207,142,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(4,142,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Special_Set_Invoiced_Dates')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button_Do
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Access:_Invoice.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:_InvoiceTransporter.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Special_Set_Invoiced_Dates',QuickWindow)   ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('Special_Set_Invoiced_Dates',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Do
      ThisWindow.Update()
          CASE MESSAGE('Are you sure you want to set ALL Invoiced dates to their corresponding Manifest date?|(excluding Credit Notes)','Confirm',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No)
          OF BUTTON:Yes
             _SQLTemp{PROP:SQL}  = 'UPDATE _Invoice ' & |
                                     'SET InvoiceDateAndTime = (' & |
                                     'SELECT CreatedDateAndTime FROM Manifest ' & |
                                     'WHERE _Invoice.MID = Manifest.MID) ' & |
                                     'WHERE _Invoice.STATUS <> 2 AND _Invoice.STATUS <> 3 AND _Invoice.STATUS <> 5 ' & |
                                     'AND _Invoice.MID IN (SELECT Manifest.MID FROM Manifest)'
             IF ERRORCODE()
                CASE MESSAGE('Error: ' & CLIP(ERROR()) & '||F Err: ' & FILEERROR() & '||SQL: ' & CLIP(_SQLTemp{PROP:SQL}) & '||Would you like to copy the SQL string to the clipboard?', 'Invoice Date Reset', ICON:Hand, BUTTON:Yes+BUTTON:No,BUTTON:YEs)
                OF BUTTON:Yes
                   SETCLIPBOARD(CLIP(_SQLTemp{PROP:SQL}))
                .
             ELSE
                MESSAGE('Done.||All Invoices now have the Manifest Date as the Invoiced Date.', 'Invoice Date Reset', ICON:Asterisk)
          .  .
      
      
      
      
      !       _SQLTemp{PROP:SQL}  = 'UPDATE _Invoice ' & |
      !                               'SET _Invoice.InvoiceDateAndTime = Deliveries.DIDateandTime ' & |
      !                               'FROM Deliveries ' & |
      !                               'WHERE _Invoice.DID = Deliveries.DID'
    OF ?Button_Do:2
      ThisWindow.Update()
          CASE MESSAGE('Are you sure you want to set ALL Transporter Invoiced dates to their corresponding Manifest Created date?','Confirm',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No)
          OF BUTTON:Yes
             _SQLTemp{PROP:SQL}  = 'UPDATE _InvoiceTransporter ' & |
                                     'SET _InvoiceTransporter.InvoiceDateTime = Manifest.CreatedDateAndTime ' & |
                                     'FROM Manifest ' & |
                                     'WHERE _InvoiceTransporter.MID = Manifest.MID'
             IF ERRORCODE()
                MESSAGE('Error: ' & CLIP(ERROR()) & '||F Err: ' & FILEERROR(), 'Invoice Date Reset', ICON:Hand)
             ELSE
                MESSAGE('Done.||All Transporter Invoices now have the Manifest Created Date as the Invoiced Date.', 'Invoice Date Reset', ICON:Asterisk)
          .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

