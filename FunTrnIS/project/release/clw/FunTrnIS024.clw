

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS024.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
RunSQLRequest        PROCEDURE  (String pRequest,<*File pFile>) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
  if Omitted(pFile) then
     Do OpenFiles
     _SQLTemp{Prop:SQL} = pRequest
     if SQLError(_SQLTemp) then
        Do CloseFiles
        Return False
     else
        Do CloseFiles
        Return True
     End
  else
     pFile{Prop:SQL} = pRequest
     if SQLError(pFile) then
        Return False
     else
        Return True
     End
  End

!--------------------------------------
OpenFiles  ROUTINE
  Relate:_SQLTemp.Open()                                   ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Relate:_SQLTemp.Close
     FilesOpened = False
  END
!!! <summary>
!!! Generated from procedure template - Source
!!! Concat 2 strings - Elmis added
!!! </summary>
ConString            PROCEDURE  (String A,String B,String Sep) ! Declare Procedure

  CODE
  if Clip(A) = '' then
     Return Clip(B)
  End
  if Clip(B) = '' then
     Return Clip(A)
  End
  Return Clip(A) & Sep & Clip(B)
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
SQLError             PROCEDURE  (File pFile)               ! Declare Procedure

  CODE
  Case Clip(FileErrorCode())
  of ''
     Return False
  else
     SetClipboard(pFile{Prop:SQL})
     Message('Error occured during executing of SQL request:||'&pFile{Prop:SQL}&'||File Error Code: '&FileErrorCode()&'||Error description: '&FileError()&'||SQL request has been copied into clipboard!','Error',Icon:Hand)
     Return True
  End
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
GetTempFile          PROCEDURE  (String pPrefix,String pExtension) ! Declare Procedure
TempFileName CString(256)

  CODE
  TempFileName = ''
  Loop i# = 1 to 999
       TempFileName = clip(GETTEMPPATH())&'\'&pPrefix&Format(i#,@N03)&'.'&pExtension
       if ~Exists(TempFileName) then Break.
       if i# = 999 then
          Message('Cannot create temp file!',,Icon:Hand)
          Return ''
       End
  End
  Return TempFileName
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ShellExecute         PROCEDURE  (String pStartLine)        ! Declare Procedure
ShExOperation        CSTRING(20)
ShExFileName         &CSTRING
ShExParams           CSTRING(20)
ShExDefaultDir       CSTRING(256)
Result               UNSIGNED

  CODE
  ShExOperation = 'open'
  ShExFileName &= new CString(LEN(pStartLine)+1)
  ShExFileName = pStartLine
  Result = ShellExecute(Target{prop:handle},ShExOperation,ShExFileName,ShExParams,ShExDefaultDir,1)
  IF Result < 33
     MESSAGE('Unable to view this document.|'&pStartLine,'Shell Execute',ICON:Exclamation)
  END
  IF ~(ShExFileName &= Null) THEN DISPOSE(ShExFileName).
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
FixFileName          PROCEDURE  (String pDirName)          ! Declare Procedure
RetStr   CString(256)

  CODE
   RetStr = pDirName
   ReplaceText(RetStr,':','_')
   ReplaceText(RetStr,'*',' _Asterisk_ ')
   ReplaceText(RetStr,'?',' _Question_ ')
   ReplaceText(RetStr,'"',' '' ')
   ReplaceText(RetStr,'<',' _ ')
   ReplaceText(RetStr,'>',' _ ')
   ReplaceText(RetStr,'|',' _OR_ ')
   ReplaceText(RetStr,'/',' _')
   ReplaceText(RetStr,'\',' _')
   RETURN RetStr
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ReplaceText          PROCEDURE  (*CString aText,String SubTextA,String SubTextB) ! Declare Procedure
SubTextPos Long
SubTextLen Long
TextLen    Long
StartPos   Long
Count      Long(0)

  CODE
  TextLen    = Len(aText)
  SubTextLen = Len(SubTextA)
  StartPos   = 1
  Count      = 0
  Loop
       SubTextPos = Instring(SubTextA,aText,1,StartPos)
       if ~SubTextPos then Break.
       TextLen    = Len(Clip(aText))
       !Message('['&aText[1 : SubTextPos-1] & ']|||[' & SubTextB & ']|||[' & aText[SubTextPos+SubTextLen : TextLen]&']','Replace')
       aText = aText[1 : SubTextPos-1] & SubTextB & aText[SubTextPos+SubTextLen : TextLen]
       StartPos = SubTextPos + Len(SubTextB)
       Count += 1
  End
  
  Return Count
