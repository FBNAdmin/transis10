

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('FUNTRNIS015.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
DI_Items_Action PROCEDURE 

L_LM:DI_Items_Action BYTE                                  ! 
DI_Item_Action       BYTE                                  ! 
QuickWindow          WINDOW('DI Item Action'),AT(,,260,143),FONT('Tahoma',8,,FONT:regular),RESIZE,ALRT(EscKey), |
  CENTER,GRAY,IMM,MDI,HLP('DI_Items_Action')
                       SHEET,AT(3,4,253,135),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           PROMPT('Go to:'),AT(14,36),USE(?Prompt3),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           PROMPT('&DI Items Action (default):'),AT(14,66),USE(?L_LM:DI_Items_Action:Prompt),TRN
                           LIST,AT(98,66,88,10),USE(L_LM:DI_Items_Action),DROP(5),FROM('Ask Always|#0|General Tab|' & |
  '#1|Charges Tab|#2')
                           BUTTON('&Charges Tab'),AT(135,34,,14),USE(?Button_CgargesTab)
                           BUTTON('&General Tab'),AT(74,34,,14),USE(?Button_gentab)
                           GROUP,AT(14,80,235,54),USE(?Group1),BOXED,TRN
                             PROMPT('Selecting any other default option than Ask Always will mean this screen is no ' & |
  'longer shown.  You can still change this option later though by going to the menu Se' & |
  'tup and Choosing My Settings.'),AT(19,93,223,35),USE(?Prompt2),TRN
                           END
                         END
                       END
                       BUTTON('&Help'),AT(206,8,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(DI_Item_Action)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DI_Items_Action')
      L_LM:DI_Items_Action        = GETINI('Setup', 'DI_Items_Action', , GLO:Local_INI)
  
      IF L_LM:DI_Items_Action = 0
      ELSE
         DI_Item_Action   = L_LM:DI_Items_Action
  
         RETURN(LEVEL:Notify)
      .
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('DI_Items_Action',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('DI_Items_Action',QuickWindow)           ! Save window data to non-volatile store
  END
      PUTINI('Setup', 'DI_Items_Action', L_LM:DI_Items_Action, GLO:Local_INI)
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_CgargesTab
      ThisWindow.Update()
          DI_Item_Action  = 2
          POST(EVENT:CloseWindow)
    OF ?Button_gentab
      ThisWindow.Update()
          DI_Item_Action  = 1
          POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
          CYCLE
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! (p:TIN, p:CR_TIN, p:Status, p_b:Cost_Total, pb:Payments, pb:Credits, pb:Outstanding)
!!! </summary>
Get_Transporter_Totals PROCEDURE  (p:TIN, p:CR_TIN, p:Status, p_b:Cost_Total, pb:Payments, pb:Credits, pb:Outstanding) ! Declare Procedure

  CODE
    ! (ULONG, ULONG, BYTE, *DECIMAL, *DECIMAL, *DECIMAL, *DECIMAL)
    ! (p:TIN, p:CR_TIN, p:Status, p_b:Cost_Total, pb:Payments, pb:Credits, pb:Outstanding)
    ! Key   p   - passed in only
    !       pb  - pass back only
    !       p_b - passed in and adjusted

    ! Outstanding returned in case of Invocie (or adjustment) is just outstanding on that item
    !                      in case of Credit Note, if has CR_TIN related invoice, than zero, otherwise total


    ! Invoices may be credited or have payments
    pb:Payments         = Get_TransportersPay_Alloc_Amt(p:TIN)

    ! p:Options
    !   0.  All
    !   1.  Credits
    !   2.  Debits
    pb:Credits          = -Get_InvTransporter_Credited(p:TIN, 1)        ! Credited is negative

    ! Credit notes may have associated invoices or not??
    ! CR_TIN can also be for an additional / extra invoice
    IF p:CR_TIN ~= 0 AND p_b:Cost_Total < 0.0                           ! Should only be for credit notes!
       !    Get Payments & Credits for parent
       pb:Payments     += Get_TransportersPay_Alloc_Amt(p:CR_TIN)
       pb:Credits      += -Get_InvTransporter_Credited(p:CR_TIN, 1, p:TIN)

       ! Get original entry (of which this is a sub entry)
       A_INT:TIN         = p:CR_TIN
       IF Access:InvoiceTransporterAlias.TryFetch(A_INT:PKey_TIN) ~= LEVEL:Benign
          CLEAR(A_INT:Record)
       .


       ! No Payments|Partially Paid|Credit Note|Fully Paid
       IF p:Status = 2
          pb:Outstanding    = 0.0
       ELSE
          ! We have an extra / additional / adjustment invoice - show total cost for both
          ! Have cost for it in total, add on parent related costs
! Show just this invoices amount
          !pb:Outstanding    = (A_INT:Cost + A_INT:VAT + p_b:Cost_Total) + LOC:Adjustments - (pb:Payments + pb:Credits)
          pb:Outstanding    = p_b:Cost_Total - (pb:Payments + pb:Credits)
       .

       ! This is then the Outstanding amt for the parent inv considerring all
    ELSE
       pb:Outstanding       = p_b:Cost_Total - (pb:Payments + pb:Credits)
    .
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Upd_Client_Balances  PROCEDURE  (p:CID, p:Date, p_Bal_Group) ! Declare Procedure
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
LOC:Return           LONG                                  ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsAlias.Open()
     .
     Access:ClientsAlias.UseFile()
    ! (p:CID, p:Date, p_Bal_Group)
    LOC:Statement_Info      = p_Bal_Group

    A_CLI:CID               = p:CID
    IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) ~= LEVEL:Benign
       LOC:Return           = -1
    ELSE
       ! Not sure what the historical use might be for this.  But have now added a check for Today, 
       !       if today is less than the currently stored day, then update it (may be future date stored in error)
       IF A_CLI:UpdatedDate < p:Date OR TODAY() < A_CLI:UpdatedDate
          A_CLI:UpdatedDate    = p:Date            ! Because this can be used historically
          A_CLI:UpdatedTime    = CLOCK()

          A_CLI:BalanceCurrent = L_SI:Current
          A_CLI:Balance30Days  = L_SI:Days30
          A_CLI:Balance60Days  = L_SI:Days60
          A_CLI:Balance90Days  = L_SI:Days90

          IF Access:ClientsAlias.TryUpdate() = LEVEL:Benign
             !
    .  .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:ClientsAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Client_Balances  PROCEDURE  (p:CID, p:Option, p_Bal_Group) ! Declare Procedure
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
LOC:Return           LONG                                  ! 
LOC:Month_End_Day    BYTE                                  ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsAlias.Open()
     .
     Access:ClientsAlias.UseFile()
    ! (p:CID, p:Option, p_Bal_Group)
    ! (ULONG, BYTE=0, *STRING),LONG,PROC
    !
    !   The Option is specified to allow the historical balances to be used and not recalculated
    !
    !   p:Option        
    !       0   - Get whatever is there (no recalculate)
    !       1   - As long as its todays
    !       2   - Must be in last hour
    !       3   - Recalculate always

    A_CLI:CID               = p:CID
    IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) ~= LEVEL:Benign
       LOC:Return           = -1
    ELSE
       CASE p:Option
       OF 0
       OF 1
          IF A_CLI:UpdatedDate ~= TODAY()
             ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
             Gen_Statement(0, A_CLI:CID, TODAY(), CLOCK(),, TRUE, LOC:Statement_Info)
          .
       OF 2
          IF A_CLI:UpdatedDate ~= TODAY() OR ABS(CLOCK() - A_CLI:UpdatedTime) > (100 * 60 * 60)
             Gen_Statement(0, A_CLI:CID, TODAY(), CLOCK(),, TRUE, LOC:Statement_Info)
          .
       OF 3
          Gen_Statement(0, A_CLI:CID, TODAY(), CLOCK(),, TRUE, LOC:Statement_Info)
       .

       L_SI:Days90          = A_CLI:Balance90Days
       L_SI:Days60          = A_CLI:Balance60Days
       L_SI:Days30          = A_CLI:Balance30Days
       L_SI:Current         = A_CLI:BalanceCurrent

       L_SI:Total           = L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current

       p_Bal_Group          = LOC:Statement_Info
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:ClientsAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Gen_Statement_old    PROCEDURE  (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output) ! Declare Procedure
LOC:Result           LONG                                  ! 
LOC:MonthEndDate     LONG                                  ! 
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
LOC:Credited         DECIMAL(11,2)                         ! 
LOC:CreditNotes_Added_Q QUEUE,PRE(L_CQ)                    ! 
IID                  ULONG                                 ! Invoice Number
                     END                                   ! 
Tek_Failed_File     STRING(100)

Inv_View            VIEW(_Invoice)
    PROJECT(INV:IID, INV:BID, INV:CID, INV:DINo, INV:InvoiceDate, INV:InvoiceTime, INV:Total, INV:DID, INV:Status, INV:StatusUpToDate, INV:ClientNo)
    .

View_Inv            ViewManager



InvA_View            VIEW(InvoiceAlias)
    PROJECT(A_INV:IID, A_INV:BID, A_INV:CID, A_INV:DINo, A_INV:InvoiceDate, A_INV:InvoiceTime, A_INV:Total, A_INV:DID, A_INV:Status)
    .

View_InvA            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAllocation.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Statements.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_StatementItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceAlias.Open()
     .
     Access:_Invoice.UseFile()
     Access:ClientsPaymentsAllocation.UseFile()
     Access:_Statements.UseFile()
     Access:_StatementItems.UseFile()
     Access:InvoiceAlias.UseFile()
    ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:OutPut)
    ! (ULONG, ULONG, LONG, LONG, <*DECIMAL>, BYTE=0, <*STRING>, BYTE=0, BYTE, BYTE=0),LONG
    !   p:Run_Type
    !       0   -               ! Client & Statement Gen.
    !       1   - 
    !       2   -
    !
    !   p:OutPut
    !       0   - none
    !       1   - All
    !       2   - Current
    !       3   - 30 Days
    !       4   - 60 Days
    !       5   - 90 Days
    !
    ! We should run the invoice maintenance function before running the statements... maybe not though, see code

    PUSHBIND
    BIND('INV:InvoiceDate', INV:InvoiceDate)
    BIND('INV:InvoiceTime', INV:InvoiceTime)
    BIND('INV:InvoiceDateAndTime', INV:InvoiceDateAndTime)
    BIND('INV:Status', INV:Status)
    BIND('A_INV:InvoiceDate', A_INV:InvoiceDate)
    BIND('A_INV:InvoiceTime', A_INV:InvoiceTime)
    BIND('A_INV:InvoiceDateAndTime', A_INV:InvoiceDateAndTime)
    BIND('A_INV:Status', A_INV:Status)

    ! Month end date is - using the run date and the MonthEndDay, eg. 12 May run date = 25 May month end date
    IF p:MonthEndDay > DAY(p:Date)          ! This day is still coming this month
       LOC:MonthEndDate         = DATE(MONTH(p:Date), p:MonthEndDay, YEAR(p:Date))
    ELSE
       LOC:MonthEndDate         = DATE(MONTH(p:Date) + 1, p:MonthEndDay, YEAR(p:Date))
    .

!    db.debugout('[Gen_Statement]   p:MonthEndDay: ' & p:MonthEndDay & ',   Date: ' & FORMAT(LOC:MonthEndDate,@d6))
!    db.debugout('[Gen_Statement]  In - CID: ' & p:CID & ',  Date: ' & FORMAT(p:Date,@d5))
    IF p:Dont_Add = TRUE
       DO Get_Details
       IF ~OMITTED(7)
          p:Statement_Info      = LOC:Statement_Info
       .
    ELSE
       ! Create Statement
       CLEAR(STA:Record)
       IF Access:_Statements.TryPrimeAutoInc() = LEVEL:Benign
          !STA:STID            auto inc
          STA:STRID             = p:STRID
          STA:StatementDate     = p:Date
          STA:StatementTime     = p:Time

          STA:CID               = p:CID
          STA:BID               = GLO:BranchID

          DO Get_Details
          STA:Record           :=: LOC:Statement_Info

          IF STA:Total = 0.0
             ! No statement when nothing to state.
             IF Access:_Statements.CancelAutoInc() ~= LEVEL:Benign
             .
          ELSE
             IF Access:_Statements.TryInsert() ~= LEVEL:Benign
                ! hmm ??
             ELSE
                LOC:Result      = TRUE
          .  .
       ELSE
          ! hmmm?
    .  .

    IF ~OMITTED(5)
       p:Owing                  = L_SI:Total
    .

    POPBIND


    ! Update Client - updates Client Record with these balances
    Upd_Client_Balances(p:CID, p:Date, LOC:Statement_Info)
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:_Invoice.Close()
    Relate:ClientsPaymentsAllocation.Close()
    Relate:_Statements.Close()
    Relate:_StatementItems.Close()
    Relate:InvoiceAlias.Close()
    Exit
Get_Details                           ROUTINE
!    My_SQL.Init(GLO:DBOwner,'_TempSQL')
!
!    IF My_SQL.PropSQL('SELECT IID, BID, CID, DINo, InvoiceDate, InvoiceTime, Total, DID, Status, StatusUpToDate, ClientNo' & |
!                    ' FROM _Invoice WHERE CID = ' & p:CID & ' AND InvoiceDate < ' & SQL_Get_DateT_G(p:Date + 1) & |
!                    ' AND AND Status < 4 ORDER BY InvoiceDate') < 0
!       MESSAGE('Error on SQL.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Gen_Statement', ICON:Hand)
!    .
!

    View_Inv.Init(Inv_View, Relate:_Invoice)
    View_Inv.AddSortOrder(INV:FKey_CID)
    View_Inv.AddRange(INV:CID, p:CID)

    View_Inv.AppendOrder('INV:InvoiceDate')

    ! Assume they only want current, 2 is fully paid
    ! Note: use of tomorrow as date, with no time specified.

    !       0           1               2              3                 4               5
    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
    ! Fully paid invoices are excluded from the set because they cannot impact on the balance owing

    View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND INV:Status < 4')
    !IF p:Dont_Add = TRUE
    !   Add a filter for Credit notes (2) as they cannot impact on a Age analysis run
    !.

    View_Inv.Reset()
    IF ERRORCODE()
       MESSAGE('Error on View.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Gen_Statement', ICON:Hand)
    .
    LOOP
       IF View_Inv.Next() ~= LEVEL:Benign
          BREAK
       .

       ! Here we only have the invoices less than the date specified.
       ! and not fully paid.
       ! For credit notes, if they are appearing on their 1st statement, then set them to having been shown
       IF p:Run_Type < 2 AND p:Dont_Add = FALSE          ! Client & Statement Gen.
          Upd_Invoice_Paid_Status(INV:IID, 1)            ! Update Fully Paid - Credit to Fully Paid and Credit Note to Credit Note - Shown
       ELSE
          IF INV:StatusUpToDate = FALSE                  ! We know we need to check the Invoice Status
             Upd_Invoice_Paid_Status(INV:IID)
       .  .

       IF p:Dont_Add = FALSE            ! Add to Statement
          L_CQ:IID       = INV:IID
          GET(LOC:CreditNotes_Added_Q, L_CQ:IID)
          IF ~ERRORCODE()               ! Already have shown Credit note with respective invoice
             CYCLE
          .

          DO Add_to_Statement
       ELSE
          IF INV:Status = 2 OR INV:Status = 5                           ! If this is a Credit Note, then no amount
             STAI:Amount  = 0.0
          ELSE
             STAI:Debit     = INV:Total                                   ! Invoice total

             ! Check that Invoice has no Credit Notes for it
             LOC:Credited   = Get_Inv_Credited( INV:IID )                 ! Less any Credit Notes

             STAI:Debit    += LOC:Credited

             STAI:Credit    = Get_ClientsPay_Alloc_Amt( INV:IID, 0 )      ! Payment total for this Invoice

             STAI:Amount    = STAI:Debit - STAI:Credit
       .  .

!    db.debugout('[Gen_Statement]  CID: ' & p:CID & ',  INV:IID: ' & INV:IID & ',  INV:CID: ' & INV:CID & ',  INV:DID: ' & INV:DID & ',  INV:Total: ' & INV:Total)
       CASE Months_Between(LOC:MonthEndDate, INV:InvoiceDate)
       OF 0
          L_SI:Current   += STAI:Amount

          IF p:OutPut = 1 OR p:OutPut = 2                       ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             Add_Log(',' & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, 'Current', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       OF 1
          L_SI:Days30    += STAI:Amount

          IF p:OutPut = 1 OR p:OutPut = 3
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '30 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       OF 2
          L_SI:Days60    += STAI:Amount

          IF p:OutPut = 1 OR p:OutPut = 4
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '60 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       ELSE
          L_SI:Days90    += STAI:Amount

          IF p:OutPut = 1 OR p:OutPut = 5
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '90 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
    .  .

    View_Inv.Kill()

    L_SI:Total            = L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current
    EXIT
Add_to_Statement                ROUTINE
    IF Access:_StatementItems.TryPrimeAutoInc() = LEVEL:Benign
       ! STAI:STIID
       STAI:STID            = STA:STID

       STAI:InvoiceDate     = INV:InvoiceDate
       STAI:IID             = INV:IID
       STAI:DID             = INV:DID
       STAI:DINo            = INV:DINo

       ! p:Option
       !   0.  Invoice Paid
       !   1.  Clients Payments Allocation total
       STAI:Debit           = INV:Total

       ! When Credit
       LOC:Credited         = Get_Inv_Credited( INV:IID )
       STAI:Debit          += LOC:Credited               ! Less any Credit

       STAI:Credit          = Get_ClientsPay_Alloc_Amt( INV:IID, 0 )

       !    0               1               2           3                   4               5
       ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
       IF INV:Status = 2 OR INV:Status = 5          ! Credit notes
          STAI:Amount       = 0.0

          ! FBN wants the "Credit" amount to be in the Credit column and a + amount
          STAI:Credit       = INV:Total
          STAI:Debit       -= INV:Total
       ELSE
          STAI:Amount       = STAI:Debit - STAI:Credit
       .

       IF Access:_StatementItems.TryInsert() = LEVEL:Benign
          IF LOC:Credited ~= 0.0
             ! Add the credit note in here and add it to the Q so that it is not added here again
             L_CQ:IID       = INV:IID

             DO Credit_Notes_for_IID

             ADD(LOC:CreditNotes_Added_Q)
    .  .  .
    EXIT
Credit_Notes_for_IID            ROUTINE
    View_InvA.Init(InvA_View, Relate:InvoiceAlias)
    View_InvA.AddSortOrder(A_INV:Key_CR_IID)
    View_InvA.AppendOrder('A_INV:InvoiceDate')
    View_InvA.AddRange(A_INV:CR_IID, L_CQ:IID)

    !       0           1               2              3                 4               5
    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
    View_InvA.SetFilter('A_INV:InvoiceDate < ' & p:Date + 1 & ' AND A_INV:Status < 4')

    View_InvA.Reset()
    LOOP
       IF View_InvA.Next() ~= LEVEL:Benign
          BREAK
       .

       ! Show it then after the invoice
       IF Access:_StatementItems.TryPrimeAutoInc() = LEVEL:Benign
          ! STAI:STIID
          STAI:STID            = STA:STID

          STAI:InvoiceDate     = A_INV:InvoiceDate
          STAI:IID             = A_INV:IID
          STAI:DID             = A_INV:DID
          STAI:DINo            = A_INV:DINo

          ! p:Option
          !   0.  Invoice Paid
          !   1.  Clients Payments Allocation total
          STAI:Debit           = A_INV:Total

          ! When Credit
          LOC:Credited         = Get_Inv_Credited( A_INV:IID )                      ! --------- sould not be for credit notes
          STAI:Debit          += LOC:Credited               ! Less any Credit

          STAI:Credit          = Get_ClientsPay_Alloc_Amt( A_INV:IID, 0 )           ! --------- sould not be for credit notes

          !    0               1               2           3                   4               5
          ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
          IF A_INV:Status = 2                                                       ! --------- sould only be
             STAI:Amount       = 0.0

             ! FBN wants the "Credit" amount to be in the Credit column
             STAI:Credit      -= A_INV:Total
             STAI:Debit       -= A_INV:Total
          .

    db.debugout('[Gen_Statement]  Adding Credit note - STAI:STIID: ' & STAI:STIID)

          IF Access:_StatementItems.TryInsert() = LEVEL:Benign
    .  .  .

    View_InvA.Kill()
    EXIT
