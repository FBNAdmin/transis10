

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('FUNTRNIS011.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Floor_DIs        PROCEDURE  (p:FID)                    ! Declare Procedure
LOC:Return           LONG                                  ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_SQLTemp.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Deliveries.Open()
     .
     Access:_SQLTemp.UseFile()
     Access:Deliveries.UseFile()
    ! (p:FID)
    CLEAR(_SQ:Record)
    _SQLTemp{PROP:SQL}  = 'SELECT Count(*) FROM Deliveries WHERE FID = ' & p:FID
    IF ERRORCODE()
       MESSAGE('SQL error: ' & CLIP(ERROR()) & '|File Err: ' & CLIP(FILEERROR()), 'Get Floor DIs', ICON:Hand)
    .

    NEXT(_SQLTemp)
    IF ~ERRORCODE()
       LOC:Return   = _SQ:S1
    .


    
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:_SQLTemp.Close()
    Relate:Deliveries.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Statement_Run_Info PROCEDURE  (p:STRID)                ! Declare Procedure
LOC:Return           LONG                                  ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_SQLTemp.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Statements.Open()
     .
     Access:_SQLTemp.UseFile()
     Access:_Statements.UseFile()
    ! (p:STRID)
    CLEAR(_SQ:Record)
    _SQLTemp{PROP:SQL}  = 'SELECT Count(*) FROM _Statements WHERE STRID = ' & p:STRID
    IF ERRORCODE()
       MESSAGE('SQL error: ' & CLIP(ERROR()) & '|File Err: ' & CLIP(FILEERROR()), 'Get Statement Run Info.', ICON:Hand)
    .

    NEXT(_SQLTemp)
    IF ~ERRORCODE()
       LOC:Return   = _SQ:S1
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:_SQLTemp.Close()
    Relate:_Statements.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! if 1 JID, returns that, if multiple, checks that against passed
!!! </summary>
Check_ClientRates_for_JID PROCEDURE  (p:CID, p:JID)        ! Declare Procedure
LOC:JID_Q            QUEUE,PRE(L_JQ)                       ! 
JID                  ULONG                                 ! Journey ID
                     END                                   ! 
Tek_Failed_File     STRING(100)

View_LoadT          VIEW(LoadTypes2Alias)
       JOIN(CPRA:FKey_FID, LOAD2:FID)
    .  .

LoadT_View          ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_View_Rates_Client_Journeys.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__RatesContainerPark.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:LoadTypes2Alias.Open()
     .
     Access:_View_Rates_Client_Journeys.UseFile()
     Access:__RatesContainerPark.UseFile()
     Access:LoadTypes2Alias.UseFile()
    ! p:CID, p:JID
    ! Load a Queue with all the Clients Rates
    !                 + all the Clients Container Park Rates
    !                 + all Special CP Rates that aren't in the Queue yet
    ! Check the Q for the Setup default rate, return it if found.
    ! If 1 Journey found only, return this

    CLEAR(LOC:JID_Q)

    ! Check Rates & Container Park Rates (this is a Union View)
    CLEAR(V_RCJ:Record,-1)
    V_RCJ:CID                   = p:CID
    SET(V_RCJ:CKey_CID_JID, V_RCJ:CKey_CID_JID)
    LOOP
       IF Access:_View_Rates_Client_Journeys.TryNext() ~= LEVEL:Benign
          BREAK
       .

       IF V_RCJ:CID ~= p:CID
          BREAK
       .

       L_JQ:JID                  = V_RCJ:JID
       ADD(LOC:JID_Q)
!   db.debugout('[Check_ClientRates_for_JID]   V_RCJ:JID: ' & V_RCJ:JID)
    .



    ! Check Container Park Special Rates.........  everyone has these.....  except those with CP discounts for the Special CPs.
    ! Check through Special Load Types for Floors with Special access, for all of these check through the Journeys
    ! if we have them in our Q then they must not be included in the list, otherwise add them.....
    PUSHBIND()
    BIND('A_LOAD2:LoadOption', A_LOAD2:LoadOption)
    BIND('A_LOAD2:FID', A_LOAD2:FID)

    LoadT_View.Init(View_LoadT, Relate:LoadTypes2Alias)
    LoadT_View.AddSortOrder(A_LOAD2:PKey_LTID)

    !LoadT_View.AppendOrder()
    !LoadT_View.AddRange()
    LoadT_View.SetFilter('A_LOAD2:LoadOption = 1 AND A_LOAD2:FID <> 0')
    LoadT_View.Reset()
    LOOP
       IF LoadT_View.Next() ~= LEVEL:Benign
          BREAK
       .

   db.debugout('[Check_ClientRates_for_JID]   CPRA:JID: ' & CPRA:JID)

       L_JQ:JID         = CPRA:JID
       GET(LOC:JID_Q, L_JQ:JID)
       IF ERRORCODE()
          ! We have no entry for this Journey
          L_JQ:JID      = CPRA:JID
          ADD(LOC:JID_Q)
    .  .
    LoadT_View.Kill

    UNBIND('A_LOAD2:LoadOption')
    UNBIND('A_LOAD2:FID')
    POPBIND()



    CLEAR(LOC:JID_Q)
    IF RECORDS(LOC:JID_Q) > 0
       IF RECORDS(LOC:JID_Q) > 1
          ! If not, use setup default JID
          ! Check that this client has this Journey in their rates somewhere
          L_JQ:JID     = p:JID
          GET(LOC:JID_Q, L_JQ:JID)
          IF ERRORCODE()
             CLEAR(LOC:JID_Q)
          .
       ELSE
          L_JQ:JID     = p:JID
    .  .


    db.debugout('[Check_ClientRates_for_JID]   Recs: ' & RECORDS(LOC:JID_Q) & ',  p:JID: ' & p:JID & ',  L_JQ:JID: ' & L_JQ:JID)

    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(L_JQ:JID)

    Exit

CloseFiles     Routine
    Relate:_View_Rates_Client_Journeys.Close()
    Relate:__RatesContainerPark.Close()
    Relate:LoadTypes2Alias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
Rates_Modify PROCEDURE (shpTagClass p_Client_Tags)

LOC:Group            GROUP,PRE(LO)                         ! 
Rates_Option         BYTE(1)                               ! 
Rate_Change          BYTE                                  ! Change the Rate Up / Down
Set_Percent          BYTE                                  ! Set the percentage rate to the specified percent
Percentage           DECIMAL(6,2)                          ! Percentage change
Estimate_Based_On    BYTE                                  ! 
No_Clients           ULONG                                 ! 
Minimium_Charges     BYTE(0)                               ! 
Ad_Hoc               BYTE                                  ! 
Round_To_Cents       BYTE(1)                               ! Round to Cents
Round_To_Rands       BYTE(4)                               ! 
Eff_DateTime_Str     STRING(8)                             ! 
Eff_DateTime         GROUP,PRE(),OVER(Eff_DateTime_Str)    ! 
Effective_Date       DATE                                  ! 
Effective_Time       TIME                                  ! 
                     END                                   ! 
                     END                                   ! 
LOC:Run_Group        GROUP,PRE(L_RG)                       ! 
Timer                LONG(10)                              ! 
Time                 LONG                                  ! 
Loops                LONG                                  ! 
Progress             LONG                                  ! 
Complete             BYTE                                  ! 
Idx                  LONG                                  ! 
CID                  ULONG                                 ! Client ID
Count                LONG                                  ! 
Count_Clients        LONG                                  ! 
Count_Skipped        LONG                                  ! 
RUBID                ULONG                                 ! Rate Update Batch ID
                     END                                   ! 
LOC:Entry_Group      GROUP,PRE(L_EG)                       ! 
CRTID                ULONG                                 ! Client Rate Type ID
RID                  ULONG                                 ! Rate ID
LTID                 ULONG                                 ! Load Type ID
JID                  ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
CTID                 ULONG(0)                              ! Container Type ID
Effective_Date       DATE                                  ! Effective from date
ToMass               DECIMAL(9)                            ! Up to this mass in Kgs
RatePerKg            DECIMAL(10,4)                         ! Rate per Kg
MinimiumCharge       DECIMAL(10,2)                         ! 
AdHoc                BYTE                                  ! Ad Hoc rate (not in rate letter)
                     END                                   ! 
QuickWindow          WINDOW('Modify Rates'),AT(,,395,258),FONT('Tahoma',8,,FONT:regular),DOUBLE,ALRT(CtrlF4),ALRT(EscKey), |
  CENTER,GRAY,IMM,MDI,HLP('Rates_Modify'),TIMER(100)
                       SHEET,AT(3,3,388,236),USE(?Sheet1),FONT(,,,FONT:regular,CHARSET:ANSI)
                         TAB('General'),USE(?Tab1)
                           PROMPT('Rates Option:'),AT(10,33),USE(?LO:Rates_Option:Prompt),TRN
                           LIST,AT(78,33,130,9),USE(LO:Rates_Option),DROP(15),FROM('Rates|#1|Additional Rates|#2|C' & |
  'ontainer Park Rates|#3|Container Park Discount Minimiums|#4')
                           GROUP,AT(10,73,128,10),USE(?Group_Rate_Change)
                             PROMPT('Rate Change:'),AT(10,73),USE(?Rate_Change:Prompt),TRN
                             LIST,AT(78,73,60,10),USE(LO:Rate_Change),DROP(5),FROM('Increase|#0|Decrease|#1'),MSG('Change the' & |
  ' Rate Up / Down'),TIP('Change the Rate Up / Down')
                           END
                           CHECK(' &Round Rate to Cents'),AT(258,73),USE(LO:Round_To_Cents),MSG('Round to Cents'),TIP('Round to C' & |
  'ents<0DH,0AH>Effective on Rates & Container Park Rates'),TRN
                           STRING(' Effective From '),AT(15,142,,10),USE(?String1:3),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                           LINE,AT(77,145,309,0),USE(?Line1:3),COLOR(COLOR:Black),LINEWIDTH(4)
                           PROMPT('Percentage:'),AT(10,86),USE(?Percentage:Prompt),TRN
                           ENTRY(@N7.2~%~),AT(78,86,60,10),USE(LO:Percentage),RIGHT(1),MSG('Percentage change'),TIP('Percentage change')
                           CHECK(' &Set Percent'),AT(258,86),USE(LO:Set_Percent),MSG('Set the percentage rate to t' & |
  'he specified percent'),TIP('Set the percentage rate to the specified percent'),TRN
                           GROUP,AT(10,105,141,10),USE(?Group_Min_Charges)
                             PROMPT('Minimium Charges:'),AT(10,105),USE(?LO:Minimium_Charges:Prompt),TRN
                             LIST,AT(78,105,73,9),USE(LO:Minimium_Charges),DROP(5),FROM('Adjust|#0|No Adjustment|#1')
                           END
                           GROUP,AT(178,105,161,10),USE(?Group_Mins)
                             PROMPT('Min. Charge Rounding:'),AT(179,105),USE(?LO:Round_To_Rands:Prompt),TRN
                             LIST,AT(258,105,73,9),USE(LO:Round_To_Rands),DROP(5),FROM('None|#0|Rands|#1|10 Rands|#2' & |
  '|100 Rands|#3|5 Rands|#4'),TIP('Effective on Rates, Additional Rates & Container Par' & |
  'k Discount Rates')
                           END
                           STRING('Note: Adjustments only work on existing rates.  Where no rate exists nothing wi' & |
  'll be done.'),AT(10,185,366,10),USE(?String4),TRN
                           LINE,AT(11,198,375,0),USE(?Line1:4),COLOR(COLOR:Black),LINEWIDTH(5)
                           STRING(' What to Adjust '),AT(15,17,,10),USE(?String1),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           LINE,AT(77,22,308,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Dev comment - fuel surcharges taken out of this list until new method is implemented.'), |
  AT(214,25,166,29),USE(?Prompt9),FONT(,,,FONT:regular+FONT:italic,CHARSET:ANSI),HIDE,TRN
                           GROUP,AT(10,121,133,10),USE(?Group_AdHoc)
                             PROMPT('Ad Hoc Rates:'),AT(10,121),USE(?Ad_Hoc:Prompt),TRN
                             LIST,AT(78,121,73,10),USE(LO:Ad_Hoc),DROP(5,100),FROM('Don''t Adjust|#0|Adjust (leaving' & |
  ' Ad Hoc)|#1|Adjust (change to non Ad Hoc)|#2')
                           END
                           PROMPT('Effective Date:'),AT(10,158),USE(?Effective_Date:Prompt),TRN
                           SPIN(@d5),AT(78,157,60,10),USE(LO:Effective_Date),RIGHT(1)
                           BUTTON('...'),AT(143,156,12,10),USE(?Calendar)
                           BUTTON('&Go'),AT(322,156,54,24),USE(?Ok:2),LEFT,ICON('WAOK.ICO'),TIP('Run update')
                           STRING(' How to Adjust '),AT(15,57,,10),USE(?String1:2),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           LINE,AT(79,59,306,0),USE(?Line1:2),COLOR(COLOR:Black),LINEWIDTH(3)
                           BUTTON('Estimate Revenue Change with New Rate'),AT(11,209,,14),USE(?Button_Estimate)
                           OPTION('Estimate Based On'),AT(182,209,206,28),USE(LO:Estimate_Based_On),BOXED,DISABLE,TRN
                             RADIO('Last 30 Days'),AT(199,222),USE(?LOC:Estimate_Based_On:Radio1),TRN,VALUE('0')
                             RADIO('Last 3 Months'),AT(262,222),USE(?LOC:Estimate_Based_On:Radio2),TRN,VALUE('1')
                             RADIO('Last Year'),AT(327,222),USE(?LOC:Estimate_Based_On:Radio3),TRN,VALUE('2')
                           END
                           PROMPT('Effective Time:'),AT(178,156),USE(?LO:Effective_Time:Prompt)
                           ENTRY(@T1),AT(232,157,53,10),USE(LO:Effective_Time),RIGHT,TIP('Note, the rate used on t' & |
  'he day will always be the one with the latest effective TIME regardless of the time ' & |
  'on the day that the DI is captured.')
                         END
                         TAB('Run'),USE(?Tab_Run),HIDE
                           PROGRESS,AT(47,94,300,11),USE(?Progress),RANGE(0,100)
                         END
                       END
                       BUTTON('&Cancel'),AT(342,242,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(342,2,49,12),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       PROMPT('Notes....'),AT(4,244,325,10),USE(?Prompt_Note),FONT(,,,FONT:bold,CHARSET:ANSI)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar1            CalendarClass
View_Rates      VIEW(__Rates)
    .


Rates_View      ViewManager






View_CPRates    VIEW(Clients_ContainerParkDiscounts)
    .



View_AddRates   VIEW(__RatesAdditionalCharges)
    .




View_ParkRates    VIEW(__RatesContainerPark)
    .



View_Fuel       VIEW(__RatesFuelSurcharge)
    .
R_Group     GROUP(CLI_CP:Record),PRE(RG)
        .


R_CARA_Group GROUP(CARA:Record),PRE(RGA)
        .


R_CP_Group     GROUP(CPRA:Record),PRE(RCPG)
        .



R_FS_Group      GROUP(FSRA:Record),PRE(R_FS)
    .

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Update_Rates                               ROUTINE
    CLEAR(LOC:Entry_Group)

    ! Get Next Client
    L_RG:Idx    += 1
    GET(p_Client_Tags.TagQueue, L_RG:Idx)
    IF ERRORCODE()
       L_RG:Complete    = TRUE
    ELSE
       ! Update their rates
       L_RG:CID         = p_Client_Tags.TagQueue.Ptr

       DO Standard_Rates

       db.debugout('[Rates_Modify]   Start Container_Park_Discounts - ' & L_RG:CID)
       DO Container_Park_Discounts

       db.debugout('[Rates_Modify]   Start Additional_Charges - ' & L_RG:CID)
       DO Additional_Charges

       db.debugout('[Rates_Modify]   Start Fuel_Surcharge - ' & L_RG:CID)
       DO Fuel_Surcharge

       L_RG:Count_Clients    += 1
    .
    EXIT




Standard_Rates                            ROUTINE
    CLEAR(LOC:Entry_Group)
    ! Rates|Additional Rates|Container Park Rates|Container Park Discount Minimiums|Fuel Surcharges
    !   1           2                   3                       4                           5
    IF LO:Rates_Option = 1
    !IF LO:Rates = TRUE
       Rates_View.Init(View_Rates, Relate:__Rates)
       Rates_View.AddSortOrder(RAT:FKey_CID)
       Rates_View.AppendOrder('RAT:CRTID, RAT:LTID, RAT:JID, RAT:CTID, RAT:ToMass, -RAT:Effective_DateAndTime')
       !Rates_View.AddRange(RAT:CID, L_RG:CID)
       Rates_View.SetFilter('RAT:CID = ' & L_RG:CID, '1')

       ! 05/06/2012 - add filter to check only earlier than effective date for previous rates
       Rates_View.SetFilter('SQL(a.Effective_DateAndTime <= ' & SQL_Get_DateT_G(LO:Effective_Date, LO:Effective_Time) & ')', '2')
      
       Rates_View.Reset()
       LOOP
          IF Rates_View.Next() ~= LEVEL:Benign
             BREAK
          .

          IF LO:Ad_Hoc <= 0 AND RAT:AdHoc = TRUE        ! No change to Ad Hoc
             L_RG:Count_Skipped += 1
             CYCLE
          .

          ! Find last Effective Rate for each RAT:CRTID, RAT:LTID, RAT:JID, RAT:CTID and RAT:Weight
          IF RAT:CRTID ~= L_EG:CRTID OR RAT:LTID ~= L_EG:LTID OR L_EG:JID ~= RAT:JID OR RAT:CTID ~= L_EG:CTID OR RAT:ToMass ~= L_EG:ToMass
             DO Adjustment

             ! Store the 1st entries (latest effective date) RID
             LOC:Entry_Group   :=: RAT:Record
          .
          IF L_RG:Complete > 0
             BREAK
          .

          ! Carry on
       .
       Rates_View.Kill()

       IF L_RG:Complete <= 1            ! Dont adjust on error, 2 or 3
          DO Adjustment
    .  .
    EXIT


Adjustment              ROUTINE
    IF L_EG:CRTID ~= 0                  ! If we have a rate to increase
       ! We are onto another Rate, as one of the above has changed - get our latest effective Rate
       ! record and create a new one with the Increase / Decrease and new effective date.
       CLEAR(A_RAT:Record)
       IF Access:RatesAlias.TryPrimeAutoInc() ~= LEVEL:Benign
          L_RG:Complete           = 2
       ELSE
          !A_RAT:RID
          A_RAT:CID               = L_RG:CID

          A_RAT:CRTID             = L_EG:CRTID

          A_RAT:JID               = L_EG:JID
          A_RAT:LTID              = L_EG:LTID
          A_RAT:CTID              = L_EG:CTID
          A_RAT:ToMass            = L_EG:ToMass

          IF LO:Rate_Change = 0
             A_RAT:RatePerKg            = L_EG:RatePerKg + L_EG:RatePerKg        * (LO:Percentage / 100)
          ELSE
             A_RAT:RatePerKg            = L_EG:RatePerKg - L_EG:RatePerKg        * (LO:Percentage / 100)
          .

          IF LO:Round_To_Cents = TRUE
             A_RAT:RatePerKg            = ROUND(A_RAT:RatePerKg, 0.01)
          .

          IF LO:Minimium_Charges = 0    ! 0 is adjust, 1 is no change
             IF LO:Rate_Change = 0
                A_RAT:MinimiumCharge    = L_EG:MinimiumCharge + L_EG:MinimiumCharge   * (LO:Percentage / 100)
             ELSE
                A_RAT:MinimiumCharge    = L_EG:MinimiumCharge - L_EG:MinimiumCharge   * (LO:Percentage / 100)
             .

             CASE LO:Round_To_Rands            ! None|Rands|10 Rands|100 Rands|5 Rands
             OF 0
             OF 1
                A_RAT:MinimiumCharge       = ROUND(A_RAT:MinimiumCharge, 1)
             OF 2
                A_RAT:MinimiumCharge       = ROUND(A_RAT:MinimiumCharge, 10)
             OF 3
                A_RAT:MinimiumCharge       = ROUND(A_RAT:MinimiumCharge, 100)
             OF 4
                IF ROUND(A_RAT:MinimiumCharge, 10) > A_RAT:MinimiumCharge              ! Up is fine
                   A_RAT:MinimiumCharge    = ROUND(A_RAT:MinimiumCharge, 10)
                ELSE
                   IF A_RAT:MinimiumCharge > (INT(A_RAT:MinimiumCharge / 10) * 10)     ! > 0 i.e. on way to 5 rand
                      A_RAT:MinimiumCharge = ROUND(A_RAT:MinimiumCharge, 10) + 5       ! Round down and add 5
          .  .  .  .

          A_RAT:Effective_Date    = LO:Effective_Date
          A_RAT:Effective_TIME    = LO:Effective_Time

          A_RAT:Added_Date        = TODAY()
          A_RAT:Added_Time        = CLOCK()

          IF LO:Ad_Hoc = 1                  ! Don't Adjust|Adjust (leaving Ad Hoc)|Adjust (change to non Ad Hoc)
             A_RAT:AdHoc          = L_EG:AdHoc
          ELSE
             A_RAT:AdHoc          = 0
          .

          A_RAT:RUBID             = L_RG:RUBID

          IF Access:RatesAlias.TryInsert() ~= LEVEL:Benign
             db.debugout('[Rates_Modify]  Failed insert - RID: ' & A_RAT:RID & ',  CID: ' & A_RAT:CID & ', Mass: ' & A_RAT:ToMass)

             Access:RatesAlias.CancelAutoInc()
             L_RG:Complete        = 3
          ELSE
             db.debugout('[Rates_Modify]  Inserted - RID: ' & A_RAT:RID & ',  CID: ' & A_RAT:CID & ', Mass: ' & A_RAT:ToMass)
             L_RG:Count          += 1
    .  .  .
    EXIT
Container_Park_Discounts            ROUTINE
    ! Rates|Additional Rates|Container Park Rates|Container Park Discount Minimiums|Fuel Surcharges
    !   1           2                   3                       4                           5
    IF LO:Rates_Option = 4
!    IF LO:ContainerParkDiscount = TRUE
       CLEAR(R_Group)

       Rates_View.Init(View_CPRates, Relate:Clients_ContainerParkDiscounts)
       Rates_View.AddSortOrder(CLI_CP:FKey_CID)
       Rates_View.AppendOrder('CLI_CP:FID, -CLI_CP:Effective_DateAndTime')
       !Rates_View.AddRange()
       Rates_View.SetFilter('CLI_CP:CID = ' & L_RG:CID,'1')

       ! 05/06/2012 - add filter to check only earlier than effective date for previous rates
       Rates_View.SetFilter('SQL(a.Effective_DateAndTime <= ' & SQL_Get_DateT_G(LO:Effective_Date, LO:Effective_Time) & ')', '2')

       Rates_View.Reset()
       LOOP
          IF Rates_View.Next() ~= LEVEL:Benign
             BREAK
          .

          ! Find last Effective Rate for each CLI_CP:FID
          IF CLI_CP:FID ~= RG:FID
             IF RG:CPDID ~= 0
                DO Add_CPD
             .

             ! Store the 1st entries (latest effective date)
             R_Group   :=: CLI_CP:Record
       .  .

       IF RG:CPDID ~= 0
          DO Add_CPD
       .

       Rates_View.Kill()
    .
    EXIT


Add_CPD             ROUTINE
    IF Access:Clients_ContainerParkDiscountsAlias.TryPrimeAutoInc() ~= LEVEL:Benign
    ELSE
       !A_CLI_CP:CPDID
       A_CLI_CP:CID                           = L_RG:CID
       A_CLI_CP:FID                           = RG:FID
       A_CLI_CP:ContainerParkRateDiscount     = RG:ContainerParkRateDiscount

       A_CLI_CP:ContainerParkMinimium         = RG:ContainerParkMinimium
       IF LO:Minimium_Charges > 0
          IF LO:Rate_Change = 0
             A_CLI_CP:ContainerParkMinimium   = RG:ContainerParkMinimium + RG:ContainerParkMinimium  * (LO:Percentage / 100)
          ELSE
             A_CLI_CP:ContainerParkMinimium   = RG:ContainerParkMinimium - RG:ContainerParkMinimium  * (LO:Percentage / 100)
          .

          CASE LO:Round_To_Rands            ! None|Rands|10 Rands|100 Rands|5 Rands
          OF 0
          OF 1
             A_CLI_CP:ContainerParkMinimium       = ROUND(A_CLI_CP:ContainerParkMinimium, 1)
          OF 2
             A_CLI_CP:ContainerParkMinimium       = ROUND(A_CLI_CP:ContainerParkMinimium, 10)
          OF 3
             A_CLI_CP:ContainerParkMinimium       = ROUND(A_CLI_CP:ContainerParkMinimium, 100)
          OF 4
             IF ROUND(A_CLI_CP:ContainerParkMinimium, 10) > A_CLI_CP:ContainerParkMinimium              ! Up is fine
                A_CLI_CP:ContainerParkMinimium    = ROUND(A_CLI_CP:ContainerParkMinimium, 10)
             ELSE
                IF A_CLI_CP:ContainerParkMinimium > (INT(A_CLI_CP:ContainerParkMinimium / 10) * 10)     ! > 0 i.e. on way to 5 rand
                   A_CLI_CP:ContainerParkMinimium = ROUND(A_CLI_CP:ContainerParkMinimium, 10) + 5       ! Round down and add 5
       .  .  .  .

       A_CLI_CP:Effective_Date                = LO:Effective_Date
       A_CLI_CP:Effective_TIME                = LO:Effective_Time          

       A_CLI_CP:RUBID                         = L_RG:RUBID

       IF Access:Clients_ContainerParkDiscountsAlias.TryInsert() = LEVEL:Benign
          L_RG:Count    += 1
       ELSE
          Access:Clients_ContainerParkDiscountsAlias.CancelAutoInc()
    .  .
    EXIT





Additional_Charges                        ROUTINE
    ! Rates|Additional Rates|Container Park Rates|Container Park Discount Minimiums|Fuel Surcharges
    !   1           2                   3                       4                           5
    IF LO:Rates_Option = 2
!    IF LO:AdditionalRates = TRUE
       CLEAR(R_CARA_Group)

       Rates_View.Init(View_AddRates, Relate:__RatesAdditionalCharges)
       Rates_View.AddSortOrder(CARA:FKey_CID)
       Rates_View.AppendOrder('CARA:ACCID,-CARA:Effective_DateAndTime')
       !Rates_View.AddRange(CARA:CID,)
       Rates_View.SetFilter('CARA:CID = ' & L_RG:CID, '1')

       ! 05/06/2012 - add filter to check only earlier than effective date for previous rates
       Rates_View.SetFilter('SQL(a.Effective_DateAndTime <= ' & SQL_Get_DateT_G(LO:Effective_Date, LO:Effective_Time) & ')', '2')
      
       Rates_View.Reset()
       LOOP
          IF Rates_View.Next() ~= LEVEL:Benign
             BREAK
          .

          IF CARA:ACCID ~= RGA:ACCID
             IF RGA:ACID ~= 0
                DO Add_CARA
             .
             R_CARA_Group :=: CARA:Record
       .  .
       IF RGA:ACID ~= 0
          DO Add_CARA
       .

       Rates_View.Kill()
    .
    EXIT


Add_CARA                    ROUTINE
    IF Access:RatesAdditionalChargesAlias.TryPrimeAutoInc() ~= LEVEL:Benign
    ELSE
       ! A_CARA:ACID
       A_CARA:ACCID           = RGA:ACCID
       A_CARA:CID             = L_RG:CID

       A_CARA:Charge          = RGA:Charge
       IF LO:Minimium_Charges > 0
          IF LO:Rate_Change = 0
             A_CARA:Charge    = RGA:Charge + RGA:Charge  * (LO:Percentage / 100)
          ELSE
             A_CARA:Charge    = RGA:Charge - RGA:Charge  * (LO:Percentage / 100)
          .

          CASE LO:Round_To_Rands            ! None|Rands|10 Rands|100 Rands|5 Rands
          OF 0
          OF 1
             A_CARA:Charge       = ROUND(A_CARA:Charge, 1)
          OF 2
             A_CARA:Charge       = ROUND(A_CARA:Charge, 10)
          OF 3
             A_CARA:Charge       = ROUND(A_CARA:Charge, 100)
          OF 4
             IF ROUND(A_CARA:Charge, 10) > A_CARA:Charge              ! Up is fine
                A_CARA:Charge    = ROUND(A_CARA:Charge, 10)
             ELSE
                IF A_CARA:Charge > (INT(A_CARA:Charge / 10) * 10)     ! > 0 i.e. on way to 5 rand
                   A_CARA:Charge = ROUND(A_CARA:Charge, 10) + 5       ! Round down and add 5
       .  .  .  .

       A_CARA:Effective_Date    = LO:Effective_Date
       A_CARA:Effective_TIME    = LO:Effective_Time
                
       A_CARA:RUBID             = L_RG:RUBID

       IF Access:RatesAdditionalChargesAlias.TryInsert() = LEVEL:Benign
          L_RG:Count           += 1
       ELSE
          Access:RatesAdditionalChargesAlias.CancelAutoInc()
    .  .
    EXIT
Container_Park                              ROUTINE
    ! Rates|Additional Rates|Container Park Rates|Container Park Discount Minimiums|Fuel Surcharges
    !   1           2                   3                       4                           5
    IF LO:Rates_Option = 3
!    IF LO:ContainerParkRates = TRUE
       db.debugout('[Rates_Modify - Timer]  About to do Container Parks...')
       CLEAR(R_CP_Group)

       Rates_View.Init(View_ParkRates, Relate:__RatesContainerPark)
       Rates_View.AddSortOrder(CPRA:FKey_FID)
       Rates_View.AppendOrder('CPRA:JID,CPRA:ToMass,-CPRA:Effective_DateAndTime')

       ! 05/06/2012 - add filter to check only earlier than effective date for previous rates
       Rates_View.SetFilter('SQL(a.Effective_DateAndTime <= ' & SQL_Get_DateT_G(LO:Effective_Date, LO:Effective_Time) & ')', '2')

       Rates_View.Reset()
       LOOP
          IF Rates_View.Next() ~= LEVEL:Benign
             BREAK
          .

          IF RCPG:FID ~= CPRA:FID OR RCPG:JID ~= CPRA:JID OR RCPG:ToMass ~= CPRA:ToMass
             IF RCPG:CPID ~= 0
          db.debugout('[Rates_Modify]   Container Park Rates - CPRA:ToMass: ' & CPRA:ToMass)
                DO Add_RCPG
             .
             R_CP_Group :=: CPRA:Record
       .  .

       IF RCPG:CPID ~= 0
          DO Add_RCPG
       .

       Rates_View.Kill()
    .
    EXIT



Add_RCPG                            ROUTINE
    IF Access:RatesContainerParkAlias.TryPrimeAutoInc() ~= LEVEL:Benign
       db.debugout('[Rates_Modify]   Failed to prime Container Park rate record - ' & CLIP(FILEERROR()))
    ELSE
       !A_CPRA:CPID
       A_CPRA:FID             = RCPG:FID
       A_CPRA:JID             = RCPG:JID
       A_CPRA:ToMass          = RCPG:ToMass

       IF LO:Rate_Change = 0
          A_CPRA:RatePerKg            = RCPG:RatePerKg + RCPG:RatePerKg        * (LO:Percentage / 100)
       ELSE
          A_CPRA:RatePerKg            = RCPG:RatePerKg - RCPG:RatePerKg        * (LO:Percentage / 100)
       .

       IF LO:Round_To_Cents = TRUE
          A_CPRA:RatePerKg            = ROUND(A_CPRA:RatePerKg, 0.01)
       .

       A_CPRA:Effective_Date  = LO:Effective_Date
       A_CPRA:Effective_TIME  = LO:Effective_Time
      
       A_CPRA:RUBID           = L_RG:RUBID

       IF Access:RatesContainerParkAlias.TryInsert() = LEVEL:Benign
          L_RG:Count         += 1          ! - these are extra?  3/6/12 no longer sure what this meant, so uncommented!..
       ELSE
          db.debugout('[Rates_Modify]   Failed to add Container Park rate record.')

          Access:RatesContainerParkAlias.CancelAutoInc()
    .  .
    EXIT
Fuel_Surcharge                             ROUTINE
    ! Rates|Additional Rates|Container Park Rates|Container Park Discount Minimiums|Fuel Surcharges
    !   1           2                   3                       4                           5
    IF LO:Rates_Option = 5
!    IF LO:Fuel_Surcharges = TRUE
       Rates_View.Init(View_Fuel, Relate:__RatesFuelSurcharge)
       Rates_View.AddSortOrder(FSRA:FKey_CID)
       Rates_View.AppendOrder('-FSRA:Effective_DateAndTime')
       !Rates_View.AddRange(FSRA:CID,)
       Rates_View.SetFilter('FSRA:CID = ' & L_RG:CID, '1')

       ! 05/06/2012 - add filter to check only earlier than effective date for previous rates
       Rates_View.SetFilter('SQL(a.Effective_DateAndTime <= ' & SQL_Get_DateT_G(LO:Effective_Date, LO:Effective_Time) & ')', '2')

       Rates_View.Reset()
       LOOP
          IF Rates_View.Next() ~= LEVEL:Benign
             BREAK
          .

          DO Add_FuelSurcharge
          BREAK
       .
       Rates_View.Kill()
    .
    EXIT


Add_FuelSurcharge           ROUTINE
    !R_FS_Group      GROUP(FSRA:Record),PRE(R_FS)

    R_FS_Group  :=: FSRA:Record

    IF Access:__RatesFuelSurcharge.TryPrimeAutoInc() ~= LEVEL:Benign
    ELSE
       ! FSRA:FCID
       FSRA:CID                     = L_RG:CID

       IF LO:Set_Percent = TRUE
          FSRA:FuelSurcharge        = LO:Percentage
       ELSE
          IF LO:Rate_Change = 0
             FSRA:FuelSurcharge     = R_FS:FuelSurcharge + R_FS:FuelSurcharge * (LO:Percentage / 100)
          ELSE
             FSRA:FuelSurcharge     = R_FS:FuelSurcharge - R_FS:FuelSurcharge * (LO:Percentage / 100)
       .  .

       FSRA:Effective_Date          = LO:Effective_Date
       FSRA:Effective_TIME          = LO:Effective_Time

       FSRA:RUBID                   = L_RG:RUBID

       IF Access:__RatesFuelSurcharge.TryInsert() = LEVEL:Benign
          L_RG:Count          += 1
       ELSE
          Access:__RatesFuelSurcharge.CancelAutoInc()
    .  .

    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Rates_Modify')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LO:Rates_Option:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
      BIND('RAT:CID',RAT:CID)
      BIND('RAT:Effective_DateAndTime',RAT:Effective_DateAndTime)
    
      BIND('CLI_CP:CID',CLI_CP:CID)
      BIND('CARA:CID',CARA:CID)
  
      BIND('FSRA:CID',FSRA:CID)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  Relate:Clients_ContainerParkDiscountsAlias.Open          ! File Clients_ContainerParkDiscountsAlias used by this procedure, so make sure it's RelationManager is open
  Relate:RatesAdditionalChargesAlias.Open                  ! File RatesAdditionalChargesAlias used by this procedure, so make sure it's RelationManager is open
  Relate:RatesAlias.Open                                   ! File RatesAlias used by this procedure, so make sure it's RelationManager is open
  Relate:RatesContainerParkAlias.Open                      ! File RatesContainerParkAlias used by this procedure, so make sure it's RelationManager is open
  Access:__Rates.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RateUpdates.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesAdditionalCharges.UseFile                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients_ContainerParkDiscounts.UseFile            ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesContainerPark.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesFuelSurcharge.UseFile                      ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Rates_Modify',QuickWindow)                 ! Restore window settings from non-volatile store
  LO:Effective_Time = DEFORMAT( SUB( FORMAT(CLOCK(), @T1),1,2 ) & ':00', @T1) ! remove any secs, mins
  
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
    Relate:Clients_ContainerParkDiscountsAlias.Close
    Relate:RatesAdditionalChargesAlias.Close
    Relate:RatesAlias.Close
    Relate:RatesContainerParkAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Rates_Modify',QuickWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LO:Rates_Option
          ! Rates|Additional Rates|Container Park Rates|Container Park Discount Minimiums|Fuel Surcharges
          !   1           2                   3                       4                           5
      
          IF LO:Rates_Option ~= 1                 !LO:Rates = FALSE
             DISABLE(?Group_AdHoc)
          ELSE
             ENABLE(?Group_AdHoc)
          .         
      
          IF INLIST(LO:Rates_Option, 1, 2, 4) <= 0
             DISABLE(?Group_Min_Charges)
             DISABLE(?Group_Mins)
          ELSE
             ENABLE(?Group_Min_Charges)
             POST(EVENT:Accepted, ?LO:Minimium_Charges)     ! Set whatever is based on setting
          .
      
          IF INLIST(LO:Rates_Option, 1, 3) <= 0
             DISABLE(?LO:Round_To_Cents)
          ELSE
             ENABLE(?LO:Round_To_Cents)
          .
      
          IF LO:Rates_Option = 5
             DISABLE(?Group_Rate_Change)
             ENABLE(?LO:Set_Percent)
      
             LO:Set_Percent   = TRUE
          ELSE
             ENABLE(?Group_Rate_Change)
             DISABLE(?LO:Set_Percent)
          .
      
          DISPLAY
    OF ?LO:Set_Percent
          IF LO:Set_Percent   = TRUE
             DISABLE(?Group_Rate_Change)
          ELSE
             ENABLE(?Group_Rate_Change)
          .
    OF ?LO:Minimium_Charges
          IF LO:Minimium_Charges = 0
             ENABLE(?Group_Mins)
          ELSE
             DISABLE(?Group_Mins)
          .
    OF ?Calendar
      ThisWindow.Update()
      Calendar1.SelectOnClose = True
      Calendar1.Ask('Select a Date',LO:Effective_Date)
      IF Calendar1.Response = RequestCompleted THEN
      LO:Effective_Date=Calendar1.SelectedDate
      DISPLAY(?LO:Effective_Date)
      END
      ThisWindow.Reset(True)
    OF ?Ok:2
      ThisWindow.Update()
          IF LO:No_Clients = 0
             MESSAGE('There are no Clients selected.', 'Rates Update', ICON:Exclamation)
             CYCLE
          .
          IF LO:Effective_Date = 0
             MESSAGE('Please specify a new effective date.', 'Rates Update', ICON:Exclamation)
             SELECT(?LO:Effective_Date)
             CYCLE
          .
          IF LO:Effective_Date < TODAY()
             CASE MESSAGE('Are you sure you want to specify an effective date that is earlier than today?', 'Rates Update', |
                   ICON:Question, BUTTON:No+BUTTON:YES, BUTTON:NO)
             OF BUTTON:No 
                SELECT(?LO:Effective_Date)
                CYCLE
          .  .
      
          IF LO:Percentage = 0.0
             ! ?
          .
      
      
          L_RG:Idx            = 0
          L_RG:Loops          = 10
          L_RG:Progress       = 0
          L_RG:Complete       = FALSE
          
          L_RG:Count          = 0
          L_RG:Count_Clients  = 0
          L_RG:Count_Skipped  = 0
      
      
          CLEAR(RATU:Record)
          IF Access:__RateUpdates.TryPrimeAutoInc() ~= LEVEL:Benign
             MESSAGE('Rates Bulk Update could not be added (primed).', 'Rates Update', ICON:Hand)
             CYCLE
          ELSE
             !RATU:RUBID
             RATU:UID                 = GLO:UID
             RATU:Effective_Date      = LO:Effective_Date
             RATU:Effective_Time      = LO:Effective_Time
              
             RATU:RunDate             = TODAY()
             RATU:RunTime             = CLOCK()
      
             RATU:IncreaseDecrease    = LO:Rate_Change            ! Increase|Decrease
             RATU:Percentage          = LO:Percentage
             RATU:MinimiumCharge      = LO:Minimium_Charges
             RATU:AdHoc               = LO:Ad_Hoc
      
             RATU:Rates_Option        = LO:Rates_Option
             RATU:Set_Percent         = LO:Set_Percent
      
             RATU:Round_To_Cents      = LO:Round_To_Cents
             RATU:Round_To_Rands      = LO:Round_To_Rands
      
             IF Access:__RateUpdates.TryInsert() ~= LEVEL:Benign
                MESSAGE('Rates Bulk Update could not be added.||Update will not run.', 'Rates Update', ICON:Hand)
                CYCLE
             ELSE
                ! Added record
                L_RG:RUBID            = RATU:RUBID
          .  .
      
      
          QuickWindow{PROP:Timer}     = L_RG:Timer
      
          ?Progress{PROP:RangeHigh}   = RECORDS(Clients)
      
          UNHIDE(?Tab_Run)
          HIDE(?Tab1)
      
          DISABLE(?Ok:2)
      
          SETCURSOR(CURSOR:Wait)
    OF ?Button_Estimate
      ThisWindow.Update()
           MESSAGE('Not implemented yet.||It would be interesting to know though before making the change.||' & |
              'Email me: ivan@infosolutions.co.za', 'Rates Modification', ICON:Asterisk)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LO:Minimium_Charges
          IF LO:Minimium_Charges = 0
             ENABLE(?Group_Mins)
          ELSE
             DISABLE(?Group_Mins)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseDown
      CYCLE
    OF EVENT:OpenWindow
          QuickWindow{PROP:Timer}      = 0
          LO:No_Clients   = p_Client_Tags.NumberTagged()
          ?Prompt_Note{PROP:Text} = 'This will affect ' & p_Client_Tags.NumberTagged() & ' clients.'
          IF LO:Minimium_Charges = 0
             DISABLE(?Group_Mins)
          ELSE
             ENABLE(?Group_Mins)
          .
          POST(EVENT:Accepted, ?LO:Rates_Option)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
          QuickWindow{PROP:Timer}  = 0
      
          L_RG:Time   = CLOCK()
          LOOP L_RG:Loops TIMES
             IF (CLOCK() - L_RG:Time) > 100
                L_RG:Loops    -= L_RG:Loops * 0.1
                BREAK
             .
      
             L_RG:Progress    += 1
      
             DO Update_Rates
      
             IF L_RG:Complete > 0
                BREAK
          .  .
      
          ?Progress{PROP:Progress} = L_RG:Progress
      
          IF (CLOCK() - L_RG:Time) < 75
             L_RG:Loops    += L_RG:Loops * 0.2
          .
      
          IF L_RG:Complete > 0
             DO Container_Park
      
             ! Should we re-get the record here?
             RATU:ClientsUpdated  = L_RG:Count_Clients
             RATU:RatesAdded      = L_RG:Count
             RATU:CompletedStatus = L_RG:Complete
             IF Access:__RateUpdates.TryUpdate() ~= LEVEL:Benign
                MESSAGE('There was a problem updating the Rate Batch record.', 'Update Rates', ICON:Hand)
             .
      
             IF L_RG:Complete > 1
                MESSAGE('There was a problem updating rates.  Code: ' & L_RG:Complete, 'Update Rates', ICON:Hand)
             .
      
             MESSAGE('The Rates of ' & L_RG:Count_Clients & ' Clients have been updated.||A total of ' & L_RG:Count & ' Rate records have been added.||' & L_RG:Count_Skipped & ' Rates were skipped (Ad Hoc).', 'Update Rates', ICON:Asterisk)
      
             UNHIDE(?Tab1)
             SELECT(?Tab1)
         !    ENABLE(?OK:2)
             ?Cancel{PROP:Text} = '&Close'
      
             HIDE(?Tab_Run)
      
             SETCURSOR()
          ELSE
             QuickWindow{PROP:Timer}  = L_RG:Timer
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Check_ClientsRateTypes PROCEDURE  (p:CID, p:LTID)          ! Declare Procedure
LOC:CRTID            ULONG                                 ! Client Rate Type ID
LOC:Count            LONG                                  ! 
Tek_Failed_File     STRING(100)

View_CRT            VIEW(ClientsRateTypes)
    .


CRT_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsRateTypes.Open()
     .
     Access:ClientsRateTypes.UseFile()
    ! (p:CID, p:LTID)       , p:Option)
    ! (ULONG, ULONG),ULONG
    !   p:Option
    !       0 - count unique uses of Load Type on Client Rate Types, ie no. of Client Rate Types using passed Load Type

    PUSHBIND
    BIND('CRT:LTID', CRT:LTID)

    CRT_View.Init(View_CRT, Relate:ClientsRateTypes)
    CRT_View.AddSortOrder(CRT:FKey_CID)
    CRT_View.AddRange(CRT:CID, p:CID)
    CRT_View.SetFilter('CRT:LTID=' & p:LTID)

    CRT_View.Reset()

    LOOP
       IF CRT_View.Next() ~= LEVEL:Benign
          BREAK
       .

       ! All Client and LTID
       IF LOC:CRTID ~= CRT:CRTID
          LOC:Count    += 1
       .

       LOC:CRTID        = CRT:CRTID
    .
    CRT_View.Kill()

    IF LOC:Count ~= 1
       CLEAR(LOC:CRTID)
    .

    UNBIND('CRT:LTID')
    POPBIND
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:CRTID)

    Exit

CloseFiles     Routine
    Relate:ClientsRateTypes.Close()
    Exit
