

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS019.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Address          PROCEDURE  (p:AID, p:Type, p:Details) ! Declare Procedure
R_Return_Group       GROUP,PRE(R)                          ! 
Suburb               STRING(50)                            ! Suburb
PostalCode           STRING(10)                            ! 
Country              STRING(50)                            ! Country
Province             STRING(35)                            ! Province
City                 STRING(35)                            ! City
Found                BYTE                                  ! 
                     END                                   ! 
L:Delim              CSTRING(5)                            ! 
R:Details            STRING(250)                           ! 
L:List               STRING(100)                           ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Addresses.Open()
     .
     Access:Addresses.UseFile()
    ! Get_Address
    ! (p:AID, p:Type, p:Details)
    ! (ULONG, BYTE=0, BYTE=0),STRING
    ! p:Type
    !   0 - Comma delimited string
    !   1 - Block
    !   2 - Comma delimited string - including empty Line 1 & 2 where they are empty
    !   3 - Phone Nos. (comma delim)
    ! p:Details
    !   0 - all
    !   1 - without City & Province
    !
    ! Returns
    !   Address Name, Line1 (if), Line2 (if), Post Code, City, Province
    !   or
    !   Address Name, Line1 (if), Line2 (if), Post Code
    !   or
    !   Address Name, Line1 (if), Line2 (if), <missing info string>
    !   or
    !   Phone1, Phone2 OR Phone1 OR Phone 2

    L:Delim   = ', '       ! added here 8/10/13 as not set by default

    ADD:AID              = p:AID
    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
!       IF ADD:Client = TRUE AND ADD:Delivery = FALSE
!          MESSAGE('The Collection address specified is a Client address and not a Delivery address.||Please select another address.', 'Collection Address', ICON:Exclamation)

       IF p:Type = 3         ! Phone nos. 
          Add_to_List(ADD:PhoneNo, R:Details, L:Delim)          
          Add_to_List(ADD:PhoneNo2, R:Details, L:Delim)          
       ELSE  
          R_Return_Group    = Get_Suburb_Details(ADD:SUID)

          EXECUTE (p:Type + 1)       
             L:Delim   = ', '       
             L:Delim   = '<10>'
          .

          R:Details      = CLIP(ADD:AddressName)
          IF CLIP(ADD:Line1) ~= '' OR p:Type = 2
             R:Details   = CLIP(R:Details) & L:Delim & CLIP(ADD:Line1)
          .
          IF CLIP(ADD:Line2) ~= '' OR p:Type = 2
             R:Details   = CLIP(R:Details) & L:Delim & CLIP(ADD:Line2)
          .
          IF R:Found = TRUE        ! assigned by called in return group
             EXECUTE p:Details + 1
                R:Details   = CLIP(R:Details) & L:Delim & CLIP(R:PostalCode) & L:Delim & CLIP(R:City) & L:Delim & CLIP(R:Province)
                R:Details   = CLIP(R:Details) & L:Delim & CLIP(R:PostalCode)
             .
          ELSE
             R:Details   = CLIP(R:Details) & L:Delim & '(Suburb info. missing - SUID: ' & ADD:SUID & ',  AID: ' & p:AID & ')'
    .  .  .
!         old
!    ! (p:AID, p:Type)
!    ! (ULONG,BYTE=0),STRING
!    ! p:Type
!    !   0 - Comma delimited string
!    !   1 - Block
!    !   2 - Comma delimited string - including empty Line 1 & 2 where they are empty
!    !
!    ! Returns
!    !   Address Name, Line1 (if), Line2 (if), Post Code, City, Province
!    !   or
!    !   Address Name, Line1 (if), Line2 (if), <missing info string>
!
!    ADD:AID              = p:AID
!    IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
!!       IF ADD:Client = TRUE AND ADD:Delivery = FALSE
!!          MESSAGE('The Collection address specified is a Client address and not a Delivery address.||Please select another address.', 'Collection Address', ICON:Exclamation)
!
!       R_Return_Group    = Get_Suburb_Details(ADD:SUID)
!
!       CASE p:Type + 1
!       OF 1
!          L:Delim   = ', '
!       OF 2
!          L:Delim   = '<10>'
!       .
!
!
!       R:Details      = CLIP(ADD:AddressName)
!       IF CLIP(ADD:Line1) ~= '' OR p:Type = 2
!          R:Details   = CLIP(R:Details) & L:Delim & CLIP(ADD:Line1) & L:Delim
!       .
!       IF CLIP(ADD:Line2) ~= '' OR p:Type = 2
!          R:Details   = CLIP(R:Details) & CLIP(ADD:Line2) & L:Delim
!       .
!       IF R:Found = TRUE
!          R:Details   = CLIP(R:Details) & CLIP(R:PostalCode) & L:Delim & CLIP(R:City) & L:Delim & CLIP(R:Province)
!       ELSE
!          R:Details   = CLIP(R:Details) & L:Delim & '(Suburb info. missing - SUID: ' & ADD:SUID & ',  AID: ' & p:AID & ')'
!    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(R:Details)

    Exit

CloseFiles     Routine
    Relate:Addresses.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Count_Recs           PROCEDURE  (p:Table, p:Where)         ! Declare Procedure
LOC:Count            LONG                                  ! 
_SQL    SQLQueryClass

  CODE
    ! (p:Table, p:Where)
    ! (STRING, <STRING>),LONG

    _SQL.Init(GLO:DBOwner, '_SQLTemp2')
    IF OMITTED(2)
       _SQL.PropSQL('SELECT COUNT(*) FROM ' & CLIP(p:Table))
    ELSE
       _SQL.PropSQL('SELECT COUNT(*) FROM ' & CLIP(p:Table) & ' WHERE ' & CLIP(p:Where))
    .

    _SQL.Next_Q()

    LOC:Count   = _SQL.Get_El(1)

    RETURN(LOC:Count)
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Client_Invoice_History PROCEDURE  (p:CID, p:Date, p:Option, p_Bal_Group) ! Declare Procedure
LOC:Statement_Info   GROUP,PRE(L_SI)                       ! 
Days90               DECIMAL(10,2)                         ! 90 Days
Days60               DECIMAL(10,2)                         ! 60 Days
Days30               DECIMAL(10,2)                         ! 30 Days
Current              DECIMAL(10,2)                         ! Current
Total                DECIMAL(10,2)                         ! Total
                     END                                   ! 
LOC:Return           LONG                                  ! 
LOC:SQL_Str          STRING(1000)                          ! 
LOC:Date             DATE                                  ! 
LOC:Count            LONG                                  ! 
Tek_Failed_File     STRING(100)

sql_        SQLQueryClass

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
     Access:ClientsAlias.UseFile()
     Access:_Invoice.UseFile()
    ! (p:CID, p:Date, p:Option, p_Bal_Group)
    ! (ULONG, LOND=0, BYTE=0, *STRING),LONG,PROC
    !
    !   p:Option
    !       0   - Total
    !       1   - VAT
    !       2   - Freight
    !       3   - Additional
    !       4   - Fuel
    !       5   - Doc
    !       6   - Insurance
    !
    ! Note: 90 days is only 90 days not all before that as well
    !       Total is all time total
    !
    ! Gets group of data, Current, 30 days, 60 days, 90 days, total

    SETCURSOR(CURSOR:WAIT)
    CLEAR(LOC:Statement_Info)
    sql_.Init(GLO:DBOwner, '_SQLTemp2')

    A_CLI:CID               = p:CID
    IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) ~= LEVEL:Benign
       LOC:Return           = -1
    ELSE
       CLEAR(LOC:SQL_Str)
       IF p:Date = 0
          p:Date    = TODAY()
       .

       LOC:Date     = p:Date

       LOOP 5 TIMES
          LOC:Count    += 1
          LOC:SQL_Str   = 'CID = ' & p:CID

          Add_to_List('Total > 0.0', LOC:SQL_Str, ' AND ')

          IF LOC:Count > 4
             ! Total
          ELSE
             ! Note next day used for query due to time
             Add_to_List('InvoiceDateAndTime < '  & SQL_Get_DateT_G(LOC:Date + 1,,1) , LOC:SQL_Str, ' AND ')
             Add_to_List('InvoiceDateAndTime >= ' & SQL_Get_DateT_G(LOC:Date - 30,,1), LOC:SQL_Str, ' AND ')
          .

          LOC:SQL_Str   = 'SELECT SUM(Total), SUM(VAT), SUM(FreightCharge), SUM(AdditionalCharge), SUM(FuelSurcharge), ' & |
                            'SUM(Documentation), SUM(Insurance)'                                                         & |
                            'FROM _Invoice WHERE ' & CLIP(LOC:SQL_Str)

          sql_.PropSQL(CLIP(LOC:SQL_Str))
          IF sql_.Next_Q() > 0
             EXECUTE LOC:Count
                L_SI:Current    = sql_.Get_El(p:Option + 1)
                L_SI:Days30     = sql_.Get_El(p:Option + 1)
                L_SI:Days60     = sql_.Get_El(p:Option + 1)
                L_SI:Days90     = sql_.Get_El(p:Option + 1)
                L_SI:Total      = sql_.Get_El(p:Option + 1)
             .
          ELSE
             ! No records for the period
          .

          LOC:Date     -= 30
    .  .

    p_Bal_Group         = LOC:Statement_Info
    SETCURSOR()
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:ClientsAlias.Close()
    Relate:_Invoice.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Client_Stats     PROCEDURE  (p:Type_ID, p:Entity_ID)   ! Declare Procedure
LOC:Report_Group     GROUP,PRE(L_RG)                       ! 
T_Year               DECIMAL(8)                            ! Tonnage for a year
T_6Months            DECIMAL(8)                            ! 
T_AvgYear            DECIMAL(8)                            ! 
Income_Year          DECIMAL(8)                            ! 
Income_6Months       DECIMAL(8)                            ! 
Income_AvgYear       DECIMAL(8)                            ! 
Tonnage_Rank_Year    LONG                                  ! Last year
Income_Rank_Year     LONG                                  ! 
Average_Rate_Rank_Year LONG                                ! 
Tonnage_Rank_All     LONG                                  ! 
Income_Rank_All      LONG                                  ! 
Average_Rate_Rank_All LONG                                 ! 
Rate_Average_Year    DECIMAL(7,2)                          ! 
Rate_Average_All     DECIMAL(7,2)                          ! 
                     END                                   ! 
Tek_Failed_File     STRING(100)

sql_        SQLQueryClass
T_Stats         CLASS       !,TYPE

CL:SelectType       BYTE

CL:WorkingType_ID   SHORT

CL:Entity_ID        ULONG

CL_Table            CSTRING(255)

Update_             PROCEDURE(SHORT p_Type_ID, ULONG p_Entity_ID=0, LONG p:FromDate=0, LONG p:ToDate=0)
Get_Entity_         PROCEDURE(ULONG p_Entity_ID, SHORT p_Type_ID),STRING

_Remove             PROCEDURE(SHORT p_Type_ID, ULONG p_Entity_ID=0)

_Set_Entity         PROCEDURE(ULONG p_Entity_ID=0, SHORT p_Type_ID=0)
_Set_DateRange      PROCEDURE(LONG p:FromDate=0, LONG p:ToDate=0)
_Set_Select         PROCEDURE(),STRING
_Set_Conditions     PROCEDURE()
                .



    ! Tonnage   - year, 6 months, avg / year,       Rank Year, Rank All, Rank Avg All
    ! Income    - year, 6 months, avg / year,       Rank Year, Rank All, Rank Avg All

    ! CL:WorkingType_ID                         CL:SelectType
    !   1   - Tonnage - Year                    1
    !   2   - Tonnage - 6 Months                1
    !   3   - Tonnage - Avg. / Year             2
    !   4   - Tonnage - Rank Year               3
    !   5   - Tonnage - Rank All                3
    !   6   - Tonnage - Rank Avg All            4
    !
    !   7   - Income - Year                     5
    !   8   - Income - 6 Months                 5
    !   9   - Income - Avg. / Year              6
    !   10  - Income - Rank Year                7
    !   11  - Income - Rank All                 7
    !   12  - Income - Rank Avg All             8
_Where          CLASS

Clause      STRING(255)
Pos         LONG
LastPos     LONG

Ref         &_Where

Add         PROCEDURE(STRING p_Clause, LONG p_Pos=0),LONG,PROC      ! User should never supply the 2nd parameter
Get         PROCEDURE(LONG p_Pos),STRING
Clear_      PROCEDURE()

Build       PROCEDURE(LONG p_Pos=1, <STRING p_Operator>),STRING

GetClause   PROCEDURE(LONG p_Pos),STRING
BuildClause PROCEDURE(LONG p_Pos=1, <STRING p_Operator>),STRING

Destructor  PROCEDURE()
    .




  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Statistics.Open()
     .
     Access:ClientsAlias.UseFile()
     Access:_Invoice.UseFile()
     Access:Statistics.UseFile()
    SETCURSOR(CURSOR:WAIT)
    sql_.Init(GLO:DBOwner, '_SQLTemp2')

    ! SHORT p:Type_ID=0, ULONG p:Entity_ID=0

    ! Tonnage / Income  - year, 6 months, avg / year,       Rank Year, Rank All, Rank Avg All
    ! p:Type_ID
    !   1   - Tonnage - Year
    !   2   - Tonnage - 6 Months
    !   3   - Tonnage - Avg. / Year
    !   4   - Tonnage - Rank Year
    !   5   - Tonnage - Rank All
    !   6   - Tonnage - Rank Avg All
    !
    !   7   - Income - Year
    !   8   - Income - 6 Months
    !   9   - Income - Avg. / Year
    !   10  - Income - Rank Year
    !   11  - Income - Rank All
    !   12  - Income - Rank Avg All
    !
    !   13  - Rate - Average Rank - Year        9
    !   14  - Rate - Average Rank - All         9
    !
    !   15  - Rate - Average  - Year            10
    !   16  - Rate - Average  - All             10


    ! p:Entity_ID = 0      ! Refresh all stats
    ! p:Type_ID = 0

    ! SHORT p:Type_ID, ULONG p:Entity_ID=0, DATE p:FromDate=0, DATE p:ToDate=0

    IF p:Type_ID = 0
       T_Stats.Update_(1, p:Entity_ID, TODAY()-364, TODAY())
       T_Stats.Update_(2, p:Entity_ID, TODAY()-(364/2), TODAY())
       T_Stats.Update_(3, p:Entity_ID, TODAY()-364, TODAY())
       T_Stats.Update_(4, p:Entity_ID, TODAY()-364, TODAY())
       T_Stats.Update_(5, p:Entity_ID)

       T_Stats.Update_(7, p:Entity_ID, TODAY()-364, TODAY())
       T_Stats.Update_(8, p:Entity_ID, TODAY()-(364/2), TODAY())
       T_Stats.Update_(9, p:Entity_ID, TODAY()-364, TODAY())
       T_Stats.Update_(10, p:Entity_ID, TODAY()-364, TODAY())
       T_Stats.Update_(11, p:Entity_ID)

       T_Stats.Update_(13, p:Entity_ID, TODAY()-364, TODAY())
       T_Stats.Update_(14, p:Entity_ID)

       T_Stats.Update_(15, p:Entity_ID, TODAY()-364, TODAY())
       T_Stats.Update_(16, p:Entity_ID)
    ELSE
       From_#      = 0
       To_#        = 0

       CASE p:Type_ID
       OF 1 OROF 3 OROF 4 OROF 7 OROF 9 OROF 10 OROF 13 OROF 15
          From_#   = TODAY()-364
          To_#     = TODAY()
       OF 2 OROF 8
          From_#   = TODAY()-(364/2)
          To_#     = TODAY()
       .

       T_Stats.Update_(p:Type_ID, p:Entity_ID, From_#, To_#)
    .

    IF p:Entity_ID ~= 0
       ! p:Type_ID
       !   1   - Tonnage - Year
       !   2   - Tonnage - 6 Months
       !   3   - Tonnage - Avg. / Year
       !   4   - Tonnage - Rank Year
       !   5   - Tonnage - Rank All
       !   6   - Tonnage - Rank Avg All
       !
       !   7   - Income - Year
       !   8   - Income - 6 Months
       !   9   - Income - Avg. / Year
       !   10  - Income - Rank Year
       !   11  - Income - Rank All
       !   12  - Income - Rank Avg All
       !
       !   13  - Rate - Average Rank - Year        9
       !   14  - Rate - Average Rank - All         9
       !
       !   15  - Rate - Average  - Year            10
       !   16  - Rate - Average  - All             10

       ! Populate info for requested entity

       L_RG:T_Year                      = T_Stats.Get_Entity_(p:Entity_ID, 1)
       L_RG:T_6Months                   = T_Stats.Get_Entity_(p:Entity_ID, 2)
       L_RG:T_AvgYear                   = T_Stats.Get_Entity_(p:Entity_ID, 3)

       L_RG:Income_Year                 = T_Stats.Get_Entity_(p:Entity_ID, 7)
       L_RG:Income_6Months              = T_Stats.Get_Entity_(p:Entity_ID, 8)
       L_RG:Income_AvgYear              = T_Stats.Get_Entity_(p:Entity_ID, 9)

       L_RG:Tonnage_Rank_Year           = T_Stats.Get_Entity_(p:Entity_ID, 4)
       L_RG:Tonnage_Rank_All            = T_Stats.Get_Entity_(p:Entity_ID, 5)

       L_RG:Income_Rank_Year            = T_Stats.Get_Entity_(p:Entity_ID, 10)
       L_RG:Income_Rank_All             = T_Stats.Get_Entity_(p:Entity_ID, 11)

       L_RG:Average_Rate_Rank_Year      = T_Stats.Get_Entity_(p:Entity_ID, 13)
       L_RG:Average_Rate_Rank_All       = T_Stats.Get_Entity_(p:Entity_ID, 14)

       L_RG:Rate_Average_Year           = T_Stats.Get_Entity_(p:Entity_ID, 15)
       L_RG:Rate_Average_All            = T_Stats.Get_Entity_(p:Entity_ID, 16)
    .

    SETCURSOR()
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Report_Group)

    Exit

CloseFiles     Routine
    Relate:ClientsAlias.Close()
    Relate:_Invoice.Close()
    Relate:Statistics.Close()
    Exit
T_Stats.Update_             PROCEDURE(SHORT p_Type_ID, ULONG p_Entity_ID=0, LONG p:FromDate=0, LONG p:ToDate=0)
    ! Tonnage   - year, 6 months, avg / year,       Rank Year, Rank All, Rank Avg All
    ! Income    - year, 6 months, avg / year,       Rank Year, Rank All, Rank Avg All

    CODE
    ! p_Type_ID                                 CL:SelectType
    !   1   - Tonnage - Year                    1
    !   2   - Tonnage - 6 Months                1
    !   3   - Tonnage - Avg. / Year             2
    !   4   - Tonnage - Rank Year               3
    !   5   - Tonnage - Rank All                3
    !   6   - Tonnage - Rank Avg All            -
    !
    !   7   - Income - Year                     5
    !   8   - Income - 6 Months                 5
    !   9   - Income - Avg. / Year              6
    !   10  - Income - Rank Year                7
    !   11  - Income - Rank All                 7
    !   12  - Income - Rank Avg All             -
    !
    !   13  - Rate - Average Rank - Year        9
    !   14  - Rate - Average Rank - All         9
    !
    !   15  - Rate - Average  - Year            10
    !   16  - Rate - Average  - All             10

    SELF._Set_Entity(p_Entity_ID, p_Type_ID)

    SELF._Remove(p_Type_ID, p_Entity_ID)

    SELF._Set_DateRange(p:FromDate, p:ToDate)

    SELF._Set_Conditions()

    ! Result of select would be single client record or all
    sql_.PropSQL(SELF._Set_Select())
    LOOP
       IF sql_.Next_Q() <= 0
          ! No records for the query
          BREAK
       .

       CLEAR(STAT:Record)
       IF Access:Statistics.PrimeAutoInc() = LEVEL:Benign
          !STAT:Stats_ID
          STAT:Entity_ID    = sql_.Get_El(1)   ! Entity ID
          STAT:Type_ID      = p_Type_ID
          
          STAT:Statistic    = sql_.Get_El(2)   ! Data

          STAT:UpdateDate   = TODAY()
          STAT:UpdateTime   = CLOCK()

          IF Access:Statistics.TryInsert() ~= LEVEL:Benign
             Access:Statistics.CancelAutoInc()
    .  .  .

    RETURN
! ----------------------------------------------------------------------
T_Stats._Set_Entity         PROCEDURE(ULONG p_Entity_ID=0, SHORT p_Type_ID=0)
    CODE
    IF p_Type_ID ~= 0
       SELF.CL:WorkingType_ID    = p_Type_ID
    .

    ! Tonnage   - year, 6 months, avg / year,       Rank Year, Rank All, Rank Avg All
    ! Income    - year, 6 months, avg / year,       Rank Year, Rank All, Rank Avg All

    ! p_Type_ID                                 CL:SelectType
    !   1   - Tonnage - Year                    1
    !   2   - Tonnage - 6 Months                1
    !   3   - Tonnage - Avg. / Year             2
    !   4   - Tonnage - Rank Year               3
    !   5   - Tonnage - Rank All                3
    !   6   - Tonnage - Rank Avg All            -
    !
    !   7   - Income - Year                     5
    !   8   - Income - 6 Months                 5
    !   9   - Income - Avg. / Year              6
    !   10  - Income - Rank Year                7
    !   11  - Income - Rank All                 7
    !   12  - Income - Rank Avg All             -
    !
    !   13  - Rate - Average Rank - Year        9
    !   14  - Rate - Average Rank - All         9
    !
    !   15  - Rate - Average  - Year            10
    !   16  - Rate - Average  - All             10

    CASE SELF.CL:WorkingType_ID
    OF 1 TO 2
       SELF.CL:SelectType    = 1
    OF 3
       SELF.CL:SelectType    = 2
    OF 4 TO 5
       SELF.CL:SelectType    = 3
    OF 6
       SELF.CL:SelectType    = 4
    OF 7 TO 8
       SELF.CL:SelectType    = 5
    OF 9
       SELF.CL:SelectType    = 6
    OF 10 TO 11
       SELF.CL:SelectType    = 7
    OF 12
       SELF.CL:SelectType    = 8
    OF 13 TO 14
       SELF.CL:SelectType    = 9
    OF 15 TO 16
       SELF.CL:SelectType    = 10
    .

    _Where.Clear_()

    CASE SELF.CL:WorkingType_ID
    OF 1 TO 16
       IF p_Entity_ID = 0
          _Where.Add('')
       ELSE
          _Where.Add('CID = ' & p_Entity_ID)
       .
    ELSE
       MESSAGE('CL:WorkingType_ID "' & SELF.CL:WorkingType_ID & '" not implemented', 'Get_Client_Stats', ICON:Hand)
    .
    SELF.CL:Entity_ID    = p_Entity_ID

    IF SELF.CL:SelectType <= 16
       SELF.CL_Table   = '_Invoice'
    .

    RETURN
T_Stats._Set_DateRange        PROCEDURE(LONG p:FromDate=0, LONG p:ToDate=0)
l_Clause        STRING(101)
    CODE
    IF p:FromDate ~= 0
       ! (STRING, *STRING, STRING, BYTE=0, <STRING>)
       Add_to_List('InvoiceDateAndTime >= ' & SQL_Get_DateT_G(p:FromDate,,1), l_Clause, ' AND ')
    .
    IF p:ToDate ~= 0
       Add_to_List('InvoiceDateAndTime < ' & SQL_Get_DateT_G(p:ToDate + 1,,1), l_Clause, ' AND ')
    .

    _Where.Add(CLIP(l_Clause))
    RETURN
T_Stats._Set_Conditions     PROCEDURE()
    CODE
    ! p_Type_ID                                 CL:SelectType
    !   1   - Tonnage - Year                    1
    !   2   - Tonnage - 6 Months                1
    !   3   - Tonnage - Avg. / Year             2
    !   4   - Tonnage - Rank Year               3
    !   5   - Tonnage - Rank All                3
    !   6   - Tonnage - Rank Avg All            -
    !
    !   7   - Income - Year                     5
    !   8   - Income - 6 Months                 5
    !   9   - Income - Avg. / Year              6
    !   10  - Income - Rank Year                7
    !   11  - Income - Rank All                 7
    !   12  - Income - Rank Avg All             -
    !
    !   13  - Rate - Average Rank - Year        9
    !   14  - Rate - Average Rank - All         9
    !
    !   15  - Rate - Average  - Year            10
    !   16  - Rate - Average  - All             10

    IF SELF.CL:SelectType <= 10
       IF SELF.CL:SelectType <= 4               ! Weight
          _Where.Add('Total > 0.0')             ! Exclude credited invoices
       ELSIF SELF.CL:SelectType = 9 OR SELF.CL:SelectType = 10
          _Where.Add('Total > 0.0 AND Weight > 0.0')             ! Exclude credits, 0 weight invoices
       ELSE                                     ! Total
       .

       CASE SELF.CL:SelectType
       OF 1 OROF 5
       OF 2 OROF 6
       OF 3 OROF 7
       OF 4 OROF 8
    .  .
    RETURN
T_Stats._Set_Select          PROCEDURE()         !,STRING
l_Str       STRING(1255)
l_SumCol    CSTRING(101)
    CODE
    ! Tonnage   - year, 6 months, avg / year,       Rank Year, Rank All, Rank Avg All
    ! Income    - year, 6 months, avg / year,       Rank Year, Rank All, Rank Avg All

    ! p_Type_ID                                 CL:SelectType
    !   1   - Tonnage - Year                    1
    !   2   - Tonnage - 6 Months                1
    !   3   - Tonnage - Avg. / Year             2
    !   4   - Tonnage - Rank Year               3
    !   5   - Tonnage - Rank All                3
    !   6   - Tonnage - Rank Avg All            -
    !
    !   7   - Income - Year                     5
    !   8   - Income - 6 Months                 5
    !   9   - Income - Avg. / Year              6
    !   10  - Income - Rank Year                7
    !   11  - Income - Rank All                 7
    !   12  - Income - Rank Avg All             -
    !
    !   13  - Rate - Average Rank - Year        9
    !   14  - Rate - Average Rank - All         9
    !
    !   15  - Rate - Average  - Year            10
    !   16  - Rate - Average  - All             10

    CASE SELF.CL:SelectType
    OF 1 TO 4
       l_SumCol = 'Weight'
    OF 5 TO 8
       l_SumCol = 'Total'
    OF 9 TO 10
       l_SumCol = 'Total / Weight'
    .

    CASE SELF.CL:SelectType
    OF 1 OROF 5
       l_Str    = 'SELECT CID, SUM(' & l_SumCol & ') FROM _Invoice WHERE ' & _Where.Build() & ' GROUP BY CID'
    OF 2 OROF 6
       l_Str    = 'SELECT CID, AVG(' & l_SumCol & ') FROM _Invoice WHERE ' & _Where.Build() & ' GROUP BY CID'
    OF 3 OROF 7
       l_Str    = 'SELECT x.CID, x.[Rank] FROM ' & |
                  ' (SELECT     CID, SUM(' & l_SumCol & ') AS [Sum], ' & |
                  '        RANK() OVER (ORDER BY SUM(' & l_SumCol & ') desc) AS [Rank] ' & |
                  ' FROM         _Invoice' & |
                  ' ' & _Where.GetClause(2) & |
                  ' GROUP BY CID) AS x' & |
                  ' WHERE x.' & _Where.Get(1)
    OF 4 OROF 8
       l_Str    = ''        ! Not implemented
    OF 9
       l_Str    = 'SELECT x.CID, x.[Rank], x.[Average Rate] FROM ' & |
                  ' (SELECT     CID, AVG(' & l_SumCol & ') AS [Average Rate], ' & |
                  '        RANK() OVER (ORDER BY AVG(' & l_SumCol & ') desc) AS [Rank] ' & |
                  ' FROM         _Invoice' & |
                  ' ' & _Where.BuildClause(2) & |
                  ' GROUP BY CID) AS x' & |
                  ' WHERE x.' & _Where.Get(1)
    OF 10
       l_Str    = 'SELECT x.CID, x.[Average Rate] FROM ' & |
                  ' ( SELECT CID, AVG(TOTAL / WEIGHT) AS [Average Rate]' & |
                  ' FROM         _Invoice' & |
                  ' ' & _Where.BuildClause(2) & |
                  ' GROUP BY CID) AS x' & |
                  ' WHERE x.' & _Where.Get(1)
    .

    RETURN(CLIP(l_Str))


!   SELECT x.CID, x.[Rank] FROM
!    (
!    SELECT     CID, AVG(Total / Weight) AS [Avg Rate], 
!            RANK() OVER (ORDER BY AVG(Total / Weight) desc) AS [Rank]
!    FROM         _Invoice
!    WHERE WEIGHT > 0.0 AND TOTAL > 0.0
!
!    GROUP BY CID
!    ) AS x
!    WHERE x.CID = 330


!    SELECT x.CID, x.[Rank] FROM
!    (
!    SELECT     CID, SUM(TOTAL) AS Total, 
!            RANK() OVER (ORDER BY SUM(TOTAL) desc) AS [Rank]
!    FROM         _Invoice
!    WHERE <over what data for all entities>
!    GROUP BY CID
!    ) AS x
!    WHERE x.CID = 330


    !p:SQL_Str     = 'SELECT SUM(Total), SUM(VAT), SUM(FreightCharge), SUM(AdditionalCharge), SUM(FuelSurcharge), ' & |
    !                  'SUM(Documentation), SUM(Insurance)'                                                         & |

T_Stats._Remove             PROCEDURE(SHORT p_Type_ID, ULONG p_Entity_ID=0)
    CODE
    IF p_Entity_ID = 0
       sql_.PropSQL('DELETE FROM ' & Statistics{PROP:Name} & ' WHERE Type_ID = ' & p_Type_ID)
    ELSE
       sql_.PropSQL('DELETE FROM ' & Statistics{PROP:Name} & ' WHERE Type_ID = ' & p_Type_ID & ' AND Entity_ID = ' & p_Entity_ID)
    .
    RETURN
T_Stats.Get_Entity_         PROCEDURE(ULONG p_Entity_ID, SHORT p_Type_ID)   !,STRING
L_Str   STRING(20)
    CODE
    CLEAR(STAT:Record)
    STAT:Entity_ID  = p_Entity_ID
    STAT:Type_ID    = p_Type_ID

    IF Access:Statistics.TryFetch(STAT:SKey_Entity_Type) = LEVEL:Benign
       L_Str        = STAT:Statistic
    .
    RETURN(L_Str)
! ----------------------------------------------------------------------
_Where.Add                      PROCEDURE(STRING p_Clause, LONG p_Pos=0)
L_Pos       LONG
    CODE
    IF SELF.Pos = 0                     ! This is a new node
       SELF.Clause      = CLIP(p_Clause)
       SELF.Pos         = p_Pos + 1
       L_Pos            = SELF.Pos
    ELSE                                ! Already allocated, pass on
       IF SELF.Ref &= NULL              ! Create a new node
          SELF.Ref     &= NEW(_Where)
       .

       L_Pos            = SELF.Ref.Add(p_Clause, SELF.Pos)
    .

    SELF.LastPos        = L_Pos
    RETURN(L_Pos)                       ! Created at node position


_Where.Build       PROCEDURE(LONG p_Pos=1, <STRING p_Operator>)      !,STRING
l_Operator  STRING(10)
    CODE
!    MESSAGE('pos: ' & SELF.Pos)

    IF SELF.Ref &= NULL
       RETURN(CLIP(SELF.Clause))
    ELSE
       IF OMITTED(3)
          l_Operator  = 'AND'
       ELSE
          l_Operator  = CLIP(p_Operator) & ' '
       .

       IF SELF.Clause = '' OR p_Pos > SELF.Pos
          RETURN( CLIP(SELF.Ref.Build(p_Pos, l_Operator)) )
       ELSE
          IF SELF.Ref.Build(p_Pos, l_Operator) ~= ''
             RETURN( CLIP(CLIP(SELF.Clause) & ' ' & CLIP(l_Operator) & ' ' & SELF.Ref.Build(p_Pos, l_Operator)) )
          ELSE
             RETURN( CLIP(SELF.Clause) )
    .  .  .


!_Where.Build       PROCEDURE()      !,STRING
!l_Pos       LONG
!l_Where     CSTRING(5001)
!    CODE
!    l_Pos   = SELF.LastPos + 1
!    LOOP
!       l_Pos    -= 1
!       IF l_Pos <= 0
!          BREAK
!       .
!       l_Where  = l_Where & ' AND ' & SELF.Get(l_Pos)
!    .
!    l_Where     = SUB(l_Where, 6, LEN(l_Where))         ! Remove AND in front
!
!    RETURN(l_Where)


_Where.Get         PROCEDURE(LONG p_Pos)        !,STRING
    CODE
    IF SELF.Pos = p_Pos
       RETURN(CLIP(SELF.Clause))
    ELSE
       RETURN(SELF.Ref.Get(p_Pos))
    .


_Where.Destructor   PROCEDURE()
    CODE
    IF SELF.Ref &= NULL              ! Create a new node
    ELSE
       DISPOSE(SELF.Ref)
    .
    RETURN


_Where.Clear_        PROCEDURE()
    CODE
    CLEAR(SELF.Clause)
    CLEAR(SELF.Pos)
    CLEAR(SELF.LastPos)

    DISPOSE(SELF.Ref)
    RETURN


_Where.GetClause     PROCEDURE(LONG p_Pos)        !,STRING
L_Clause        STRING(1001)
    CODE
    L_Clause    = SELF.Get(p_Pos)
    IF CLIP(L_Clause) ~= ''
       L_Clause = 'WHERE ' & L_Clause
    .

    RETURN(CLIP(L_Clause))


_Where.BuildClause   PROCEDURE(LONG p_Pos=1, <STRING p_Operator>)      !,STRING
L_Clause        STRING(1001)
    CODE
    IF OMITTED(3)
       L_Clause = SELF.Build(p_Pos)
    ELSE
       L_Clause = SELF.Build(p_Pos, p_Operator)
    .
    IF CLIP(L_Clause) ~= ''
       L_Clause = 'WHERE ' & L_Clause
    .

    RETURN(CLIP(L_Clause))
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Client_Search    PROCEDURE  (p:ClientName)             ! Declare Procedure
LOC:Replace          CSTRING(20)                           ! 

  CODE
    ! (p:ClientName)
    ! (STRING),STRING

    ! Strip out all characters we dont want

    LOC:Replace     = '.'

    ! (STRING, *CSTRING, STRING, BYTE=0, BYTE=0, BYTE=0),STRING
    ! (p:Str, p:Chars, p:Rep, p:Occurances, p:Method, p:Quiet)
    RETURN( Replace_Chars(p:ClientName, LOC:Replace, '',,1,1) )
