

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('FUNTRNIS001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Procedure not yet defined
!!! </summary>
Main_holder PROCEDURE !Procedure not yet defined
  CODE
  GlobalErrors.ThrowMessage(Msg:ProcedureToDo,'Main_holder') ! This procedure acts as a place holder for a procedure yet to be defined
  SETKEYCODE(0)
  GlobalResponse = RequestCancelled                        ! Request cancelled is the implied action
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_City_Details     PROCEDURE  (p:CIID)                   ! Declare Procedure
L_Return_Group       GROUP,PRE(L_RG)                       ! 
Country              STRING(50)                            ! Country
Province             STRING(35)                            ! Province
City                 STRING(35)                            ! City
                     END                                   ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Add_Cities.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Add_Countries.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Add_Provinces.Open()
     .
     Access:Add_Cities.UseFile()
     Access:Add_Countries.UseFile()
     Access:Add_Provinces.UseFile()
    CITI:CIID               = p:CIID
    IF Access:Add_Cities.TryFetch(CITI:PKey_CIID) = LEVEL:Benign
       L_RG:City            = CITI:City
       PROV:PRID            = CITI:PRID
       IF Access:Add_Provinces.TryFetch(PROV:PKey_PRID) = LEVEL:Benign
          COUN:COID         = PROV:COID
          L_RG:Province     = PROV:Province
          IF Access:Add_Countries.TryFetch(COUN:PKey_COID) = LEVEL:Benign
             L_RG:Country   = COUN:Country
    .  .  .


!    RETURN(L_Return_Group)
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(L_Return_Group)

    Exit

CloseFiles     Routine
    Relate:Add_Cities.Close()
    Relate:Add_Countries.Close()
    Relate:Add_Provinces.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Suburb_Details   PROCEDURE  (p:SUID)                   ! Declare Procedure
L_Return_Group       GROUP,PRE(L_RG)                       ! 
Suburb               STRING(50)                            ! Suburb
PostalCode           STRING(10)                            ! 
Country              STRING(50)                            ! Country
Province             STRING(35)                            ! Province
City                 STRING(35)                            ! City
Found                BYTE                                  ! 
                     END                                   ! 
L_Get_Group          GROUP,PRE(L_GG)                       ! 
Country              STRING(50)                            ! Country
Province             STRING(35)                            ! Province
City                 STRING(35)                            ! City
                     END                                   ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Add_Suburbs.Open()
     .
     Access:Add_Suburbs.UseFile()
    CLEAR(L_Return_Group)

    SUBU:SUID               =  p:SUID
    IF Access:Add_Suburbs.TryFetch(SUBU:PKey_SUID) = LEVEL:Benign
       L_Get_Group          =  Get_City_Details(SUBU:CIID)

       L_Return_Group      :=: L_Get_Group

       L_RG:Suburb          = SUBU:Suburb
       L_RG:PostalCode      = SUBU:PostalCode

       L_RG:Found           = TRUE
    .


!    RETURN(L_Return_Group)
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(L_Return_Group)

    Exit

CloseFiles     Routine
    Relate:Add_Suburbs.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Window_Duplicate_Client_Rates PROCEDURE (p:CID, p:RID)

LOC:Screen_Vars      GROUP,PRE(L_SV)                       ! 
From_Journey         STRING(70)                            ! Description
From_JID             ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
From_LoadType        STRING(80)                            ! Load Type
From_LoadTypeID      ULONG                                 ! Load Type ID
To_Journey           STRING(70)                            ! Description
To_JID               ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
To_LoadType          STRING(80)                            ! Load Type
To_LoadTypeID        ULONG                                 ! Load Type ID
                     END                                   ! 
LOC:Loop_Group       GROUP,PRE(L_LG)                       ! 
CID                  ULONG                                 ! Client ID
JID                  ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
LTID                 ULONG                                 ! Load Type ID
                     END                                   ! 
LOC:Rate_Q           QUEUE,PRE(L_RQ)                       ! 
ToMass               DECIMAL(9)                            ! Up to this mass in Kgs
RatePerKg            DECIMAL(8,2)                          ! Rate per Kg
MinimiumCharge       DECIMAL(10,2)                         ! 
Effective_Date       DATE                                  ! Effective from date
CTID                 ULONG                                 ! Container Type ID
Effective_TIME       TIME                                  ! Not used!
                     END                                   ! 
LOC:Idx              ULONG                                 ! 
LOC:Success          ULONG                                 ! 
LOC:Use_Eff_Date     LONG                                  ! 
FDB3::View:FileDrop  VIEW(JourneysAlias)
                       PROJECT(A_JOU:Journey)
                       PROJECT(A_JOU:JID)
                     END
FDB6::View:FileDrop  VIEW(ClientsRateTypes)
                       PROJECT(CRT:ClientRateType)
                       PROJECT(CRT:CRTID)
                     END
Queue:FileDrop       QUEUE                            !
A_JOU:Journey          LIKE(A_JOU:Journey)            !List box control field - type derived from field
A_JOU:JID              LIKE(A_JOU:JID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:1     QUEUE                            !
CRT:ClientRateType     LIKE(CRT:ClientRateType)       !List box control field - type derived from field
CRT:CRTID              LIKE(CRT:CRTID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Duplicate Rate(s)'),AT(,,260,224),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY, |
  IMM,MDI,HLP('Window_Duplicate_Client_Rates'),SYSTEM
                       SHEET,AT(3,2,254,204),USE(?Sheet1)
                         TAB('Add'),USE(?Tab1)
                           PROMPT('Duplicate Journey:'),AT(7,20),USE(?From_Journey:Prompt),TRN
                           ENTRY(@s70),AT(75,20,175,10),USE(L_SV:From_Journey),COLOR(00E9E9E9h),MSG('Description'),READONLY, |
  SKIP,TIP('Description')
                           PROMPT('Load Type:'),AT(7,34),USE(?From_LoadType:Prompt),TRN
                           ENTRY(@s80),AT(75,34,175,10),USE(L_SV:From_LoadType),COLOR(00E9E9E9h),MSG('Load Type'),READONLY, |
  SKIP,TIP('Load Type')
                           LIST,AT(7,48,244,98),USE(?List2),VSCROLL,FORMAT('50R(1)|M~To Mass~L(2)@n-12.0@50R(1)|M~' & |
  'Rate Per Kg~L(2)@n-11.2@60R(1)|M~Minimium Charge~L(2)@n-14.2@50R(1)|M~Effective Date~L(2)@d5@'), |
  FROM(LOC:Rate_Q)
                           PROMPT('This will duplicate all the rates for the above Journey to the To Journey below.'), |
  AT(7,150,244,20),USE(?Prompt3),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           PROMPT('To Journey:'),AT(7,176),USE(?From_Journey:Prompt:2),TRN
                           LIST,AT(75,176,175,10),USE(L_SV:To_Journey),VSCROLL,DROP(15),FORMAT('280L(2)|M~Journey~@s70@'), |
  FROM(Queue:FileDrop)
                           PROMPT('To Load Type:'),AT(7,190),USE(?From_Journey:Prompt:3),TRN
                           LIST,AT(75,190,175,10),USE(CRT:ClientRateType),DROP(5),FORMAT('320L(2)|M~Load Type~@s80@'), |
  FROM(Queue:FileDrop:1),MSG('Load Type')
                         END
                       END
                       BUTTON('&OK'),AT(154,208,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(208,208,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB3                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB6                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

View_Rates          VIEW(__Rates)
    .

Rates_View          ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Duplicate_Rates                 ROUTINE
    FREE(LOC:Rate_Q)

    PUSHBIND
    BIND('RAT:JID', RAT:JID)
    BIND('RAT:LTID', RAT:LTID)

    Rates_View.Init(View_Rates, Relate:__Rates)
    Rates_View.AddSortOrder(RAT:FKey_CID)
    Rates_View.AppendOrder('RAT:JID,RAT:LTID,-RAT:Effective_Date,RAT:ToMass')
    Rates_View.AddRange(RAT:CID, p:CID)
    Rates_View.SetFilter('RAT:JID = ' & L_SV:From_JID & ' AND RAT:LTID = ' & L_SV:From_LoadTypeID)

    Rates_View.Reset()
    LOOP
       IF Rates_View.Next() ~= LEVEL:Benign
          BREAK
       .

       IF LOC:Use_Eff_Date = 0
          LOC:Use_Eff_Date  = RAT:Effective_Date       ! 1st date less then our effective date, i.e. our effective date
       .
       IF RAT:Effective_Date ~= LOC:Use_Eff_Date       ! Outside our effective date now
          CLEAR(RAT:Record)
          BREAK
       .

       LOC:Rate_Q    :=: RAT:Record
       ADD(LOC:Rate_Q)
    .
    Rates_View.Kill()
    POPBIND
    EXIT



Add_Rates                   ROUTINE
    LOC:Idx = 0
    LOOP
       LOC:Idx  += 1
       GET(LOC:Rate_Q, LOC:Idx)
       IF ERRORCODE()
          BREAK
       .

       IF Access:__Rates.PrimeRecord() ~= LEVEL:Benign
          CYCLE
       .

       RAT:Record   :=: LOC:Rate_Q
       RAT:JID       = L_SV:To_JID
       RAT:LTID      = L_SV:To_LoadTypeID
       RAT:CID       = p:CID

       IF Access:__Rates.TryInsert() ~= LEVEL:Benign
          Access:__Rates.CancelAutoInc()
       ELSE
          LOC:Success    += 1
    .  .
    EXIT
!       Not used
!Duplicate_Rates                 ROUTINE
!    FREE(LOC:Rate_Q)
!
!    CASE p:Type
!    OF 0                    ! Consolidated
!       CLEAR(CSRA:Record,-1)
!       CSRA:CID     = p:CID
!       CSRA:JID     = L_SV:From_JID
!       CLEAR(CSRA:Effective_Date,1)
!
!       SET(CSRA:CKey_CID_JID_EffDate_ToMass, CSRA:CKey_CID_JID_EffDate_ToMass)
!    OF 1                    ! Breakbulk
!       CLEAR(BBRA:Record, -1)
!       BBRA:CID     = p:CID
!       BBRA:JID     = L_SV:From_JID
!       CLEAR(BBRA:Effective_Date,1)
!
!       SET(BBRA:CKey_CID_JID_EffDate_ToMass, BBRA:CKey_CID_JID_EffDate_ToMass)
!    OF 2                    ! Containers
!       CLEAR(CORA:Record, -1)
!       CORA:CID     = p:CID
!       CORA:JID     = L_SV:From_JID
!       CLEAR(CORA:Effective_Date,1)
!
!       SET(CORA:CKey_CID_JID_CTID_EffDate_ToMass, CORA:CKey_CID_JID_CTID_EffDate_ToMass)
!    .
!
!    LOOP
!       CASE p:Type
!       OF 0                    ! Consolidated
!          IF Access:__RatesConsolidated.TryNext() ~= LEVEL:Benign
!             BREAK
!          .
!          LOC:Loop_Group    :=: CSRA:Record
!       OF 1                    ! Breakbulk
!          IF Access:__RatesBreakBulk.TryNext() ~= LEVEL:Benign
!             BREAK
!          .
!          LOC:Loop_Group    :=: BBRA:Record
!       OF 2                    ! Containers
!          IF Access:__RatesContainer.TryNext() ~= LEVEL:Benign
!             BREAK
!          .
!          LOC:Loop_Group    :=: CORA:Record
!       .
!
!       IF L_LG:JID ~= L_SV:From_JID OR L_LG:CID ~= p:CID
!          BREAK
!       .
!
!       CASE p:Type
!       OF 0                    ! Consolidated
!          LOC:Rate_Q    :=: CSRA:Record
!       OF 1                    ! Breakbulk
!          LOC:Rate_Q    :=: BBRA:Record
!       OF 2                    ! Containers
!          LOC:Rate_Q    :=: CORA:Record
!       .
!       ADD(LOC:Rate_Q)
!    .
!    EXIT
!
!
!
!Add_Rates                   ROUTINE
!    LOC:Idx = 0
!    LOOP
!       LOC:Idx  += 1
!       GET(LOC:Rate_Q, LOC:Idx)
!       IF ERRORCODE()
!          BREAK
!       .
!
!       CASE p:Type
!       OF 0                    ! Consolidated
!          Access:__RatesConsolidated.PrimeRecord()
!          CSRA:Record   :=: LOC:Rate_Q
!          CSRA:JID       = L_SV:To_JID
!          CSRA:CID       = p:CID
!
!          IF Access:__RatesConsolidated.TryInsert() ~= LEVEL:Benign
!             Access:__RatesConsolidated.CancelAutoInc()
!          ELSE
!             LOC:Success    += 1
!          .
!       OF 1                    ! Breakbulk
!          Access:__RatesBreakBulk.TryPrimeAutoInc()
!          BBRA:Record   :=: LOC:Rate_Q
!          BBRA:JID       = L_SV:To_JID
!          BBRA:CID       = p:CID
!
!          IF Access:__RatesBreakBulk.TryInsert() ~= LEVEL:Benign
!             Access:__RatesBreakBulk.CancelAutoInc()
!          ELSE
!             LOC:Success    += 1
!          .
!       OF 2                    ! Containers
!          Access:__RatesContainer.TryPrimeAutoInc()
!          CORA:Record   :=: LOC:Rate_Q
!          CORA:JID       = L_SV:To_JID
!          CORA:CID       = p:CID
!
!          IF Access:__RatesContainer.TryInsert() ~= LEVEL:Benign
!             Access:__RatesContainer.CancelAutoInc()
!          ELSE
!             LOC:Success    += 1
!    .  .  .
!    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Window_Duplicate_Client_Rates')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?From_Journey:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:ClientsRateTypes.Open                             ! File ClientsRateTypes used by this procedure, so make sure it's RelationManager is open
  Relate:JourneysAlias.Open                                ! File JourneysAlias used by this procedure, so make sure it's RelationManager is open
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__Rates.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Window_Duplicate_Client_Rates',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      ! (p:CID, p:RID)
  
      RAT:RID                 = p:RID
      IF Access:__Rates.TryFetch(RAT:PKey_CRID) = LEVEL:Benign
         L_SV:From_JID        = RAT:JID
         L_SV:From_LoadTypeID = RAT:LTID
      .
  
      JOU:JID                 = L_SV:From_JID
      IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
         L_SV:From_Journey    = JOU:Journey
      .
  
  
  
  
  
      CRT:CRTID               = L_SV:From_LoadTypeID
      IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
         L_SV:From_LoadType   = CRT:ClientRateType
      .
  
  
      CRT:CRTID               = L_SV:From_LoadTypeID
      IF Access:ClientsRateTypes.TryFetch(CRT:PKey_CRTID) = LEVEL:Benign
         L_SV:To_LoadType     = CRT:ClientRateType
         L_SV:To_LoadTypeID   = CRT:CRTID
      .
  FDB3.Init(?L_SV:To_Journey,Queue:FileDrop.ViewPosition,FDB3::View:FileDrop,Queue:FileDrop,Relate:JourneysAlias,ThisWindow)
  FDB3.Q &= Queue:FileDrop
  FDB3.AddSortOrder(A_JOU:Key_Journey)
  FDB3.AddField(A_JOU:Journey,FDB3.Q.A_JOU:Journey) !List box control field - type derived from field
  FDB3.AddField(A_JOU:JID,FDB3.Q.A_JOU:JID) !Primary key field - type derived from field
  FDB3.AddUpdateField(A_JOU:JID,L_SV:To_JID)
  ThisWindow.AddItem(FDB3.WindowComponent)
  FDB3.DefaultFill = 0
  FDB6.Init(?CRT:ClientRateType,Queue:FileDrop:1.ViewPosition,FDB6::View:FileDrop,Queue:FileDrop:1,Relate:ClientsRateTypes,ThisWindow)
  FDB6.Q &= Queue:FileDrop:1
  FDB6.AddSortOrder(CRT:Key_ClientRateType)
  FDB6.AddField(CRT:ClientRateType,FDB6.Q.CRT:ClientRateType) !List box control field - type derived from field
  FDB6.AddField(CRT:CRTID,FDB6.Q.CRT:CRTID) !Primary key field - type derived from field
  FDB6.AddUpdateField(CRT:CRTID,L_SV:To_LoadTypeID)
  ThisWindow.AddItem(FDB6.WindowComponent)
  FDB6.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ClientsRateTypes.Close
    Relate:JourneysAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Window_Duplicate_Client_Rates',QuickWindow) ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Ok
      ThisWindow.Update()
          IF L_SV:To_JID = 0
             MESSAGE('Please Select a To Journey.', 'To Journey', ICON:Hand)
             SELECT(?L_SV:To_Journey)
          ELSE
             DO Add_Rates
             MESSAGE('Added ' & LOC:Success & ' rate records.', 'Rates Added', ICON:Asterisk)
             POST(EVENT:CloseWindow)
          .
    OF ?Cancel
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          DO Duplicate_Rates
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! Based on Client (Journey optional)
!!! </summary>
Get_Clients_CP_Related PROCEDURE  (p:CID, p:JID, p:Mode)   ! Declare Procedure
LOC:Found            LONG                                  ! 
LOC:JID              ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
LOC:Idx              ULONG                                 ! 
LOC:CT_Rates_Q       QUEUE,PRE(L_CQ),STATIC                ! static
CID                  ULONG                                 ! Client ID
CPID                 ULONG                                 ! Container Park Rate ID
FID                  ULONG                                 ! Floor ID
JID                  ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
Count                ULONG                                 ! 
                     END                                   ! 
LOC:CT_Clients_Q     QUEUE,PRE(L_CSQ),STATIC               ! static
CID                  ULONG                                 ! Client ID
                     END                                   ! 
LOC:Cache_Stats      GROUP,PRE(L_CS),STATIC                ! static
Caching_No           ULONG                                 ! 
Last_Cache_Out       LONG                                  ! 
Hits                 ULONG                                 ! 
Requests             ULONG                                 ! 
Clients              ULONG                                 ! 
Freed                ULONG                                 ! Q freed times
                     END                                   ! 
Tek_Failed_File     STRING(100)

Cli_CP_Rate_View        VIEW(Clients_ContainerParkDiscounts)
    PROJECT(CLI_CP:CID)
       JOIN(CPRA:FKey_FID, CLI_CP:FID)
       PROJECT(CPRA:CPID, CPRA:FID, CPRA:JID)
    .  .


Cli_CP_View             ViewManager


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Clients_ContainerParkDiscounts.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:__RatesContainerPark.Open()
     .
     Access:Clients_ContainerParkDiscounts.UseFile()
     Access:__RatesContainerPark.UseFile()
    ! (p:CID, p:JID, p:Mode)
    !   p:Mode  -   0 - just load Q
    !               1 - cache
    !               2 - cache, return no. of distinct Journeys
    !
    !   cache will now hold up to about 100 records (whatever no is loaded by last client starting < 100)

    IF p:Mode = 0
       DO Load_Q
    ELSE
       L_CS:Requests    += 1

       ! We are going to use a queue - which we will keep up to 100 records
       IF RECORDS(LOC:CT_Rates_Q) > 100 OR RECORDS(LOC:CT_Clients_Q) > 100 OR L_CS:Caching_No ~= GLO:Rates_Caching_No
          FREE(LOC:CT_Rates_Q)              ! Reload the Q
          FREE(LOC:CT_Clients_Q)
          L_CS:Freed    += 1

          IF L_CS:Caching_No ~= GLO:Rates_Caching_No
             CLEAR(LOC:Cache_Stats)
             L_CS:Caching_No     = GLO:Rates_Caching_No
       .  .

       L_CSQ:CID    = p:CID
       GET(LOC:CT_Clients_Q, L_CSQ:CID)
       IF ERRORCODE()
!    db.debugout('loading queue - JID: ' & p:JID)
          DO Load_Q
          L_CS:Clients += 1

          L_CSQ:CID     = p:CID
          ADD(LOC:CT_Clients_Q, L_CSQ:CID)
       ELSE
          L_CS:Hits    += 1
       .

       IF p:JID = 0
          ! No JID then if we have any records
          IF p:Mode = 2
             SORT(LOC:CT_Rates_Q, L_CQ:CID, L_CQ:JID)
             L_CQ:CID   = p:CID
             GET(LOC:CT_Rates_Q, L_CQ:CID)
             IF ~ERRORCODE()
                LOC:Idx = POINTER(LOC:CT_Rates_Q)
                IF LOC:Idx > 0
                   LOC:Idx -= 1
                   LOOP
                      LOC:Idx += 1
                      GET(LOC:CT_Rates_Q, LOC:Idx)
                      IF ERRORCODE() OR L_CQ:CID ~= p:CID
                         BREAK
                      .
                      IF LOC:JID = L_CQ:JID
                         CYCLE
                      .
                      LOC:JID       = L_CQ:JID

                      LOC:Found    += 1
             .  .  .
          ELSE
             L_CQ:CID  = p:CID
             GET(LOC:CT_Rates_Q, L_CQ:CID)
             IF ~ERRORCODE()
   !    db.debugout('Client found')
                LOC:Found  = TRUE
          .  .
       ELSE
!    db.debugout('Journey looking - JID: ' & p:JID)
          L_CQ:CID  = p:CID
          L_CQ:JID  = p:JID
          GET(LOC:CT_Rates_Q, L_CQ:CID, L_CQ:JID)
          IF ~ERRORCODE()
!    db.debugout('Journey found - JID: ' & p:JID)
             LOC:Found  = TRUE
       .  .

       IF (L_CS:Requests % 100) = 0 AND ABS(CLOCK() - L_CS:Last_Cache_Out) > 200     ! 2 seconds
          ! Every 100 requests output the cache info
          ! Output Cache info   - (p:Text, p:Heading, p:Name)
          START(Add_Log_h,, 'Cached info - Requests: ' & L_CS:Requests & ',  Hits: ' & L_CS:Hits & ',  Clients: ' & L_CS:Clients & ',  Q Freed: ' & L_CS:Freed & ',  Cur recs: ' & RECORDS(LOC:CT_Rates_Q), 'Get_Clients_CP_Related' ,'Cache')

          L_CS:Last_Cache_Out   = CLOCK()
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Found)

    Exit

CloseFiles     Routine
    Relate:Clients_ContainerParkDiscounts.Close()
    Relate:__RatesContainerPark.Close()
    Exit
Load_Q                  ROUTINE
    PUSHBIND
    Cli_CP_View.Init(Cli_CP_Rate_View, Relate:Clients_ContainerParkDiscounts)
    Cli_CP_View.AddSortOrder(CLI_CP:FKey_CID)
    Cli_CP_View.AppendOrder('CPRA:FID, CPRA:JID, -CPRA:Effective_Date, CPRA:ToMass')
    Cli_CP_View.AddRange(CLI_CP:CID, p:CID)

    IF p:Mode = 0                   ! Then use the JID restriction
       IF p:JID ~= 0
          BIND('CPRA:JID', CPRA:JID)
          Cli_CP_View.SetFilter('CPRA:JID=' & p:JID)
    .  .

    Cli_CP_View.Reset()

!    IF p:JID = 0
!       Cli_CP_Rate_View{PROP:SQL}  = 'CALL dbo.GetFirstClient(' & p:CID & '[IN])'
!    ELSE
!       Cli_CP_Rate_View{PROP:SQL}  = 'CALL dbo.GetFirstClient(' & p:CID & ',' & p:JID & ')'
!    .
!    IF ERRORCODE()
!       db.debugout('Error: ' & CLIP(Error()) & '||File Err: ' & FILEERROR())
!    .
    LOOP
       IF Cli_CP_View.Next() ~= LEVEL:Benign
          BREAK
       .

       IF p:Mode = 0
          LOC:Found         = 1
          BREAK
       ELSE
          L_CQ:CID          = p:CID
          L_CQ:FID          = CPRA:FID
          GET(LOC:CT_Rates_Q, L_CQ:CID, L_CQ:FID)
          IF ERRORCODE()
             ! Store 1 JID entry
             L_CQ:CID       = p:CID
             L_CQ:CPID      = CLI_CP:CID
             L_CQ:FID       = CPRA:FID
             L_CQ:JID       = CPRA:JID
             L_CQ:Count     = 1
             ADD(LOC:CT_Rates_Q)
          ELSE
             L_CQ:Count    += 1
             PUT(LOC:CT_Rates_Q)
    .  .  .

    Cli_CP_View.Kill()
    POPBIND
    EXIT
