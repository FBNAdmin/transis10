

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS018.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Add_Reminder         PROCEDURE  (p:RemDate, p:RemTime, p:RemOption, p:RemUID, p:RemUGID, p:Notes, p:Ref, p:Ref_Check, p:RemType, p:ID) ! Declare Procedure
LOC:Result           LONG                                  ! 
LOC:Dont_Add         BYTE                                  ! 
Tek_Failed_File     STRING(100)

Rem             VIEW(RemindersAlias)
    !PROJECT()
    .


Rem_Mgr         ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:RemindersAlias.Open()
     .
     Access:RemindersAlias.UseFile()
    ! (p:RemDate, p:RemTime, p:RemOption, p:RemUID, p:RemUGID, p:Notes, p:Ref, p:Ref_Check, p:RemType, p:ID)
    ! (LONG, LONG, BYTE=0, LONG, LONG, STRING, STRING, BYTE=0, BYTE=0, ULONG=0), LONG, PROC
    !   1     2      3      4     5       6      7      8       9       10
    ! p:RemDate
    ! p:RemTime
    ! p:RemOption       - All, Group & User, User, Group
    ! p:RemUID
    ! p:RemUGID
    ! p:Notes
    ! p:Ref
    ! p:Ref_Check       0 - Off
    !                   1 - Check for an Active entry of this ref type, dont add if found
    !                   2 - Check for an entry of this ref type, dont add if found

    DO Check_Ref

    IF LOC:Dont_Add = TRUE
       LOC:Result              = -3
    ELSE
       IF Access:RemindersAlias.TryPrimeAutoInc() = LEVEL:Benign
          !A_REM:RID
          A_REM:ReminderType    = p:RemType
          A_REM:ID              = p:ID              ! Link to a record (for special Reminder types eg Client)
          A_REM:Active          = TRUE
          A_REM:Popup           = TRUE

          A_REM:ReminderDate    = p:RemDate
          A_REM:ReminderTime    = p:RemTime

          A_REM:RemindOption    = p:RemOption
          A_REM:UID             = p:RemUID
          A_REM:UGID            = p:RemUGID

          A_REM:Notes           = p:Notes

          A_REM:Created_UID     = GLO:UID

          A_REM:Created_Date    = TODAY()
          A_REM:Created_Time    = CLOCK()

          A_REM:Reference       = p:Ref

          A_REM:SystemGenerated = 1

          IF Access:RemindersAlias.TryInsert() = LEVEL:Benign
             LOC:Result        = 1
          ELSE
             LOC:Result        = -2
             Access:RemindersAlias.CancelAutoInc()
          .
       ELSE
          LOC:Result           = -1
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:RemindersAlias.Close()
    Exit
Check_Ref               ROUTINE
    ! p:Ref_Check       0 - Off
    !                   1 - Check for an Active entry of this ref type, dont add if found
    !                   2 - Check for an entry of this ref type, dont add if found

    IF p:Ref_Check > 0
       Rem_Mgr.Init(Rem, Relate:RemindersAlias)
       Rem_Mgr.AddsortOrder(A_REM:SKey_Reference)
       Rem_Mgr.AddRange(A_REM:Reference, p:Ref)

       Rem_Mgr.Reset()
       LOOP
          IF Rem_Mgr.Next() ~= LEVEL:Benign
             BREAK
          .

          IF p:Ref_Check = 2
             LOC:Dont_Add       = TRUE
             BREAK
          .

          IF p:Ref_Check = 1
             IF A_REM:Active = TRUE
                LOC:Dont_Add    = TRUE
                BREAK
       .  .  .

       Rem_Mgr.Kill()
    .
    EXIT
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_UserGroup_Info   PROCEDURE  (p:Info, p:Value, p:UGID, p:Group) ! Declare Procedure
LOC:Result           LONG                                  ! 
LOC:Got              BYTE                                  ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:UserGroups.Open()
     .
     Access:UserGroups.UseFile()
    ! (p:Info, p:Value, p:UGID, p:Group)
    ! (BYTE, *STRING, <ULONG>, <STRING>),LONG

    IF OMITTED(4) AND OMITTED(3)
       MESSAGE('No suitable parameters were passed.||UGID or Group required.', 'Get_UserGroup_Info', ICON:Hand)
    ELSIF OMITTED(4)
       USEG:UGID        = p:UGID
       IF Access:UserGroups.TryFetch(USEG:PKey_UGID) = LEVEL:Benign
          LOC:Got       = TRUE
       .
    ELSE
       USEG:GroupName   = CLIP(LEFT(p:Group))
       IF Access:UserGroups.TryFetch(USEG:Key_GroupName) = LEVEL:Benign
          LOC:Got       = TRUE
    .  .


    IF LOC:Got = TRUE
       EXECUTE p:Info
          p:Value   = CLIP(USEG:GroupName)                  ! 1
          p:Value   = USEG:UGID                             ! 2
       ELSE
          MESSAGE('Option does not exist.||Option (Info): ' & p:Info, 'Get_UserGroup_Info', ICON:Hand)
       .
    ELSE
       LOC:Result   = -1
    .


!    db.debugout('[Get_UserGroup_Info]  p:Info: ' & p:Info & ', p:Value: ' & CLIP(p:Value) & |
!                ', p:Group: ' & CLIP(p:Group) & ', LOC:Result: ' & LOC:Result)
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:UserGroups.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! uses DeliveryItemAlias
!!! </summary>
Get_DelItems_Info    PROCEDURE  (p:DIID, p:Option)         ! Declare Procedure
LOC:CTID             ULONG                                 ! Container Type ID
LOC:Return           STRING(255)                           ! 
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Commodities.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:PackagingTypes.Open()
     .
     Access:DeliveryItemAlias.UseFile()
     Access:Commodities.UseFile()
     Access:PackagingTypes.UseFile()
    ! (p:DIID, p:Option)
    ! (ULONG, BYTE), STRING
    ! p:Option      1 - Commodity
    !               2 - Packaging
    !               3 - Units
    !               4 - Weight

    A_DELI:DIID  = p:DIID
    IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) = LEVEL:Benign
       CASE p:Option
       OF 1
          COM:CMID          = A_DELI:CMID
          IF Access:Commodities.TryFetch(COM:PKey_CMID) = LEVEL:Benign
             LOC:Return     = COM:Commodity
          .
       OF 2
          PACK:PTID         = A_DELI:PTID
          IF Access:PackagingTypes.TryFetch(PACK:PKey_PTID) = LEVEL:Benign
             LOC:Return     = PACK:Packaging
          .
       OF 3
          LOC:Return  = A_DELI:Units
       OF 4
          LOC:Return  = A_DELI:Weight
            
       ELSE
          MESSAGE('Invalid option: ' & p:Option, 'Get_DelItems_Info', ICON:Hand)
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles
       Return(CLIP(LOC:Return))
    

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:DeliveryItemAlias.Close()
    Relate:Commodities.Close()
    Relate:PackagingTypes.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Month_End_Day    PROCEDURE  (p:Date)                   ! Declare Procedure
LOC:Month_End_Day    BYTE                                  ! 

  CODE
    ! Get_Month_End_Day
    ! (LONG=0),BYTE
    ! (p:Date)


    IF p:Date > 0
       ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip)
       ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>),STRING
       !   1       2       3           4           5
       LOC:Month_End_Day    = Get_Setting('Month End Day - ' & FORMAT(MONTH(p:Date),'@n02'), 0)
    .

    IF LOC:Month_End_Day = 0
       LOC:Month_End_Day    = Get_Setting('Month End Day - Default', 1, '25', '@n_3', 'The month end day to use for aging (should be less than 29).' & |
                                                '<13,10>You can add month ends for specific months by specifying "Month End Day - 01" for January etc.')
    .

    RETURN(LOC:Month_End_Day)
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Month_End_Date   PROCEDURE  (p:Date, p:MonthEndDay)    ! Declare Procedure
LOC:MonthEndDate     DATE                                  ! 
LOC:Run_Date_Mth_End BYTE                                  ! 
LOC:Next_Mth_Mth_End BYTE                                  ! 

  CODE
    ! Get_Month_End_Date
    ! (p:Date, p:MonthEndDay)
    ! (LONG, BYTE=0),DATE

    IF p:MonthEndDay = 0
       LOC:Run_Date_Mth_End        = Get_Month_End_Day(p:Date)

       IF LOC:Run_Date_Mth_End >= DAY(p:Date)                  ! This day is still coming this month
          LOC:MonthEndDate         = DATE(MONTH(p:Date), LOC:Run_Date_Mth_End, YEAR(p:Date))
       ELSE                                                    ! This day has past, so we need to use the next month end date
          LOC:Next_Mth_Mth_End     = Get_Month_End_Day( DATE( MONTH(p:Date) + 1, 1, YEAR(p:Date) ) )  ! Get Mth end day using 1st day of next month

          LOC:MonthEndDate         = DATE(MONTH(p:Date) + 1, LOC:Next_Mth_Mth_End, YEAR(p:Date))
       .
    ELSE
       ! Month end date is - using the run date and the MonthEndDay, eg. 12 May run date = 25 May month end date
       IF p:MonthEndDay >= DAY(p:Date)                         ! This day is still coming this month
          LOC:MonthEndDate         = DATE(MONTH(p:Date), p:MonthEndDay, YEAR(p:Date))
       ELSE
          LOC:MonthEndDate         = DATE(MONTH(p:Date) + 1, p:MonthEndDay, YEAR(p:Date))
    .  .


    RETURN(LOC:MonthEndDate)
