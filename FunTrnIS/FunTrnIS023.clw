

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('FUNTRNIS023.INC'),ONCE        !Local module procedure declarations
                     END


FileName   CSTRING(256)
  include('Wordmrg.inc')

TMergeFieldsQueue Queue,Type
FieldName           CString(128)
Level               LONG
FieldValue          CString(2048)
                  End

!!! Reports !!!
WORDREPORT_FuelSurcharge    EQUATE(1)
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
MergeWordReport      PROCEDURE  (*GROUP pValues,LONG pReportId) ! Declare Procedure
MsWord               Class(WordMerge)
FillMergeFieldsQueue    Procedure,Virtual
                     End

Window WINDOW('Merging'),AT(,,239,30),FONT('MS Sans Serif',8,,FONT:regular),CENTER,GRAY,DOUBLE
       PANEL,AT(2,2,235,27),USE(?Panel1),BEVEL(-1,1)
       PROMPT('Merging...'),AT(32,11,195,10),USE(?Status)
     END

FieldValue CString(256)

TemplateName CString(FILE:MaxFileName)
DocName      CString(FILE:MaxFileName)
QFields      TMergeFieldsQueue

  CODE
    Open(Window)
    Display
    ?Status{Prop:Text} = 'Open template...'
    MSWord.Init()
    MSWord.WordVisible = False
    Access:WordTemplates.Open()
    Access:WordTemplates.UseFile()
    WTP:ReportId = pReportId
    SET(WTP:By_ReportId,WTP:By_ReportId)
    LOOP
        IF Access:WordTemplates.TryNext()<>Level:Benign THEN break.
        IF WTP:ReportId <> pReportId THEN break.
        IF WTP:IsCurrent
              TemplateName = GetTempFile(clip(WTP:TemplateName),'dot')
              BlobToFile(WTP:Storage,TemplateName)
              break
        END
    END
    Access:WordTemplates.Close()
    IF TemplateName=''
        Message('Template for report "'&GetWordReportName(pReportId)&'" is not set. || You should create new report under MS Word Report Templates section in Setup menu.',,ICON:Asterisk)
        RETURN ''
    END
    DocName     = TemplateName[1 : LEN(TemplateName)-1 ] &'c'

    MSWord.OpenTemplate(TemplateName)
   
    !MSWord.AutoClose = 1
    ?Status{Prop:Text} = 'Merging...'

    MSWord.ProcessMerge(DocName)
    !MSWord.WordVisible = true
    !stop(1)
    MSWord.Kill()
    ?Status{Prop:Text} = 'Saving...'
    REMOVE(TemplateName)
    RETURN DocName

MsWord.FillmergeFieldsQueue procedure
ptrValue        ANY
strValue        CString(128)
GroupFieldName  CString(128)
 Code
    GetFieldsToMerge(QFields,pReportId)
    LOOP i#=1 TO RECORDS(QFields)
        GET(QFields,i#)
        GroupFieldName = WHO(pValues,i#)
        IF GroupFieldName<>'' THEN
            ptrValue &= WHAT(pValues,i#)
            strValue = ptrValue
        ELSE
            strValue = ''
        END
        !stop(QFields.FieldName&'--------'&GroupFieldName)
        Self.AddFieldPair(QFields.FieldName,strValue)
    END
!!! <summary>
!!! Generated from procedure template - Browse
!!! </summary>
BrowseWordTemplates PROCEDURE 

ItemStr              CString(2048)
FileActive           BYTE(False)
ExportFile FILE,DRIVER('ASCII'),NAME(Filename),PRE(EF),CREATE
RECORD     RECORD,PRE(Rec)
Line        STRING(255)
           END
          END
      !end popup
StatusArchived       BYTE                                  !
SelectedReportId     LONG                                  !
QReport              TStringIdQueue
QMerge               TMergeFieldsQueue

BRW1::View:Browse    VIEW(WordTemplates)
                       PROJECT(WTP:TemplateName)
                       PROJECT(WTP:IsCurrent)
                       PROJECT(WTP:TemplateID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
WTP:TemplateName       LIKE(WTP:TemplateName)         !List box control field - type derived from field
WTP:TemplateName_Style LONG                           !Field style
WTP:IsCurrent          LIKE(WTP:IsCurrent)            !Browse hot field - type derived from field
WTP:TemplateID         LIKE(WTP:TemplateID)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
Window               WINDOW('MS Word Report Templates'),AT(,,461,286),FONT('Tahoma',8,COLOR:Black,FONT:regular, |
  CHARSET:ANSI),RESIZE,CENTER,ICON('empty.ico'),GRAY,MDI,SYSTEM,IMM
                       GROUP,AT(2,0,457,21),USE(?GroupToolbox)
                         BUTTON,AT(2,3,16,15),USE(?Insert),ICON('insert.ico'),SKIP,TIP('New Mail Template')
                         BUTTON,AT(20,3,16,15),USE(?Change),ICON('EDIT.ICO'),DISABLE,SKIP,TIP('Edit Mail Template')
                         BUTTON,AT(38,3,16,15),USE(?Delete),ICON('delete.ico'),DISABLE,SKIP,TIP('Delete Mail Template')
                         BUTTON,AT(57,3,16,15),USE(?Refresh),ICON('Refresh.ico'),TIP('Refresh template list')
                         LIST,AT(346,4,107,11),USE(StatusArchived),DROP(10),FROM('All|#0|Active|#1|Archived|#2')
                         STRING('Show:'),AT(322,5),USE(?String1)
                       END
                       LIST,AT(2,25,173,78),USE(?ReportList),VSCROLL,FORMAT('52L(2)|MIT~Report~@S255@'),FROM(QReport)
                       LIST,AT(180,26,278,240),USE(?List),VSCROLL,FORMAT('155L(2)|MY~Template~@S255@'),FROM(Queue:Browse), |
  IMM,MSG('Browsing Records')
                       BUTTON('&Insert'),AT(184,40,74,12),USE(?PopInsert),HIDE
                       BUTTON('&Change'),AT(184,52,73,12),USE(?PopChange),HIDE
                       BUTTON('&Delete'),AT(184,66,73,12),USE(?PopDelete),HIDE
                       LIST,AT(3,109,172,155),USE(?MergeList),VSCROLL,ALRT(MouseRight),FORMAT('52L(2)|MT~Field' & |
  's Available for Merge~@s127@'),FROM(QMerge)
                       BUTTON('&Select'),AT(366,270,45,14),USE(?Select)
                       BUTTON('Close'),AT(414,270,45,14),USE(?CancelButton)
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyFilter            PROCEDURE(),DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
  !------------------------------------
  !Style for ?List
  !------------------------------------
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseWordTemplates')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Insert
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  Relate:WordTemplates.Open                       ! File WordTemplates used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:WordTemplates,SELF) ! Initialize the browse manager
  SELF.Open(Window)                               ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse
  BRW1.FileLoaded = 1                             ! This is a 'file loaded' browse
  BRW1.AddSortOrder(,WTP:By_TemplateName)         ! Add the sort order for WTP:By_TemplateName for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,WTP:TemplateName,,BRW1) ! Initialize the browse locator using  using key: WTP:By_TemplateName , WTP:TemplateName
  BRW1.AddResetField(StatusArchived)              ! Apply the reset field
  BRW1.AddField(WTP:TemplateName,BRW1.Q.WTP:TemplateName) ! Field WTP:TemplateName is a hot field or requires assignment from browse
  BRW1.AddField(WTP:IsCurrent,BRW1.Q.WTP:IsCurrent) ! Field WTP:IsCurrent is a hot field or requires assignment from browse
  BRW1.AddField(WTP:TemplateID,BRW1.Q.WTP:TemplateID) ! Field WTP:TemplateID is a hot field or requires assignment from browse
  Window{PROP:MinWidth} = 300                     ! Restrict the minimum window width
  Window{PROP:MinHeight} = 200                    ! Restrict the minimum window height
  Resizer.Init(AppStrategy:Surface)               ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  BRW1.AskProcedure = 1
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('FunTrnIS','BrowseWordTemplates',1,?List,1,BRW1::PopupTextExt,Queue:Browse,2,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\FunTrnIS.INI')
  SELF.SetAlerts()
  FillQReports(QReport)
  ?ReportList{Prop:Selected} = 1
  ?List{PROPSTYLE:FontStyle,1} = Font:Bold
  POST(EVENT:NewSelection,?ReportList)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:WordTemplates.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    UpdateWordTemplate(SelectedReportId)
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Refresh
        BRW1.ResetSort(True)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Insert
      ThisWindow.Update()
      Post(EVENT:Accepted,?PopInsert)
    OF ?Change
      ThisWindow.Update()
      Post(EVENT:Accepted,?PopChange)
    OF ?Delete
      ThisWindow.Update()
      Post(EVENT:Accepted,?PopDelete)
    OF ?CancelButton
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?MergeList
    CASE EVENT()
    OF EVENT:AlertKey
      if Keycode() = MOUSERIGHT
      
         ItemStr = 'Export to ...|-|Copy to clipboard'!ItemStr&'|'&Clip(QItems.Name)
            ans# = Popup(ItemStr)
         if ans# > 0 then
            if ans# = 1 then
               IF FILEDIALOG('Choose File to Save',FileName,'*.csv',FILE:Save+FILE:KeepDir+FILE:NoError+FILE:LongName)
                  Create(ExportFile)
                  OPEN(ExportFile)
                  c#=Records(QMerge)
                  j#=0
                  loop
                     if j#>c# then break .
                     j#=j#+1
                     Get(QMerge,j#)
                     if QMerge.Level=2
                        Rec:Line=QMerge.FieldName
                        ADD(ExportFile)
                     end
                  end
                  close(ExportFile)
               end
           end
            if ans# = 2 then
               Get(QMerge,Choice(?MergeList))
               if Qmerge.Level>1 then SetClipBoard(Qmerge.FieldName).
            end
         else
            Return -1
         End
      
      end
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?ReportList
        Get(QReport,Choice(?ReportList))
        SelectedReportID = QReport.ID
        Post(EVENT:Accepted,?Refresh)
        GetFieldsToMerge(QMerge,SelectedReportID)
    END
  ReturnValue = PARENT.TakeNewSelection()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyFilter PROCEDURE

FilterString    CSTRING(1024)
  CODE
  PARENT.ApplyFilter
    FilterString = Self.View{prop:filter}
    IF StatusArchived
        FilterString = ConString(FilterString,'SQL((A.Archived='&StatusArchived-1&'))',' AND ')
    END
    FilterString = ConString(FilterString,'SQL((A.ReportId='&SelectedReportId&'))',' AND ')
  
    Self.View{prop:filter} = FilterString


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?PopInsert
    SELF.ChangeControl=?PopChange
    SELF.DeleteControl=?PopDelete
  END


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  SELF.Q.WTP:TemplateName_Style = 0 ! 
    IF SELF.Q.WTP:IsCurrent
        SELF.Q.WTP:TemplateName_Style = 1
    END


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END


BRW1.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
  ?Insert{Prop:Disable} = ?PopInsert{Prop:Disable}
  ?Change{Prop:Disable} = ?PopChange{Prop:Disable}
  ?Delete{Prop:Disable} = ?PopDelete{Prop:Disable}


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?GroupToolbox, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?GroupToolbox
  SELF.SetStrategy(?MergeList, Resize:FixLeft+Resize:FixTop, Resize:LockWidth+Resize:ConstantBottom) ! Override strategy for ?MergeList
  SELF.SetStrategy(?List, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom) ! Override strategy for ?List
  SELF.SetStrategy(?CancelButton, Resize:FixRight+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?CancelButton
  SELF.SetStrategy(?Select, Resize:FixRight+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Select

!!! <summary>
!!! Generated from procedure template - Form
!!! </summary>
UpdateWordTemplate PROCEDURE (LONG pReportId)

TempDocFileName      CString(256)
ActionMessage        CSTRING(40)                           !
ReportName           CSTRING(256)                          !
Window               WINDOW('Update Template'),AT(,,260,132),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  DOUBLE,CENTER,ICON('empty.ico'),GRAY,MDI,SYSTEM,IMM
                       GROUP,AT(4,0,253,110),USE(?Group1),BOXED
                         PROMPT('Report:'),AT(12,12,57,10),USE(?Prompt2)
                         ENTRY(@s255),AT(84,10,164,11),USE(ReportName),REQ
                         PROMPT('Description'),AT(10,58),USE(?Prompt3)
                         TEXT,AT(10,68,240,21),USE(WTP:Description),BOXED
                         CHECK('Archived'),AT(208,96),USE(WTP:Archived)
                         CHECK('Currently in use'),AT(10,94),USE(WTP:IsCurrent)
                       END
                       PROMPT('Template name:'),AT(10,28,57,10),USE(?MAI:TemplateName:Prompt)
                       ENTRY(@S255),AT(84,26,164,11),USE(WTP:TemplateName),REQ
                       BUTTON('Select file'),AT(86,42,82,14),USE(?Button3),TIP('Select MS Word file as template')
                       BUTTON(' Edit template'),AT(172,42,76,14),USE(?ButtonEdit),TIP('Edit template in MS Word')
                       BUTTON('Cancel'),AT(212,114,45,14),USE(?CancelButton)
                       BUTTON('OK'),AT(164,114,45,14),USE(?OK),DEFAULT,REQ
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
Update                 PROCEDURE(),DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Window{PROP:Text} = ActionMessage                        ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateWordTemplate')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:WordTemplates)
  Relate:WordTemplates.Open                                ! File WordTemplates used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:WordTemplates
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(Window)                                        ! Open window
  if WTP:Storage{prop:size} = 0
   if ?ButtonEdit{prop:enabled} then disable(?ButtonEdit).
  else
   if ?ButtonEdit{prop:enabled} = false then enable(?ButtonEdit).
  end
  ReportName = GetWordReportName(WTP:ReportId)
  
  Do DefineListboxStyle
  INIMgr.Fetch('UpdateWordTemplate',Window)                ! Restore window settings from non-volatile store
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:WordTemplates.Close
  END
  IF SELF.Opened
    INIMgr.Update('UpdateWordTemplate',Window)             ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  WTP:TemplateName = GetWordReportName(pReportId)
  WTP:ReportID = pReportId
    WTP:Storage{prop:size}=0
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?CancelButton
       post(event:closewindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button3
      ThisWindow.Update()
       if Filedialog('Select Word document as a mail template',TempDocFileName,'MS Word files|*.doc;*.dot',File:longname+File:keepdir)
          ans# = Filetoblob(TempDocFileName,WTP:Storage)
          Enable(?ButtonEdit)
          Update
          !ThisWindow.Update
       End
    OF ?ButtonEdit
      ThisWindow.Update()
       ShowDocumentInBlob(WTP:Storage,1)
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeCompleted()
    IF SELF.Response = RequestCompleted   AND WTP:IsCurrent
        RunSQLRequest('UPDATE WordTemplates SET IsCurrent=0 WHERE ReportId='&WTP:ReportId&' AND TemplateID<>'&WTP:TemplateId)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.Update PROCEDURE

  CODE
  PARENT.Update
  if WTP:Storage{prop:size} = 0
   if ?ButtonEdit{prop:enabled} then disable(?ButtonEdit).
  else
   if ?ButtonEdit{prop:enabled} = false then enable(?ButtonEdit).
  end

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
EditWordDocument     PROCEDURE  (String pDocumentFileName) ! Declare Procedure
ProgrWindow WINDOW('MS Word running...'),AT(,,163,30),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:CYRILLIC), |
         CENTER,TIMER(100),GRAY,DOUBLE
       PANEL,AT(2,1,159,27),USE(?Panel),BEVEL(-1,1)
       STRING('Editing of MS Word document...'),AT(7,11,148,10),USE(?StatusLine),CENTER
     END
Result                       Long
WinWordPath                  String(255)
CommandLine                  CString(256)

dwCreateFlags                Long
StartupInfo                  GROUP(STARTUP_INFO).
ProcessInformation           GROUP(WIN_PROCESS_INFORMATION).
BlankStr                     CString(11)
FoundOnce                    Byte(False)
WordWindowClosed             Byte(False)
hWnd                         Long

  CODE
  if ~Exists(pDocumentFileName) then
     Message('File to edit does not exist!|File: '&pDocumentFileName,,Icon:Hand)
     Return
  End
  !Result=GetRegistryItem(HKEY_LOCAL_MACHINE,'SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\Winword.exe','',WinWordpath)
  WinWordpath = GETREG(REG_LOCAL_MACHINE, 'SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\Winword.exe')
  if WinWordpath <> '' then
     ! unpack blob
     CommandLine = clip(WinWordpath) & ' "' & clip(pDocumentFileName)&'"'
     StartupInfo.dwFlags     = STARTF_USESHOWWINDOW
     StartupInfo.wShowWindow = SW_SHOWMAXIMIZED
     ans# = CreateProcess(0,CommandLine,0,0,1,dwCreateFlags,0,0,address(StartupInfo),address(ProcessInformation))
     if ans# = 0 then Return.
     CloseHandle(ProcessInformation.hProcess)
     CloseHandle(ProcessInformation.hThread)
     !WaitForInputIdle(ProcessInformation.hProcess,INFINITE)
     Open(ProgrWindow)
     Accept
        Case Event()
        of EVENT:Timer
           Rename(pDocumentFileName,pDocumentFileName)
           if ErrorCode() ~= 5 then
              if FoundOnce then Break.
           else
              FoundOnce = True
           End
        End
     End
     Close(ProgrWindow)
  else
     Message('Make sure that MS word installed!',,Icon:Hand)
  End

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ShowDocumentInBlob   PROCEDURE  (*BLOB pBlob,Byte SaveAfterEdit) ! Declare Procedure
TempDocFileName CString(256)

  CODE
  TempDocFileName = GetTempFile('Show','doc')
  ans# = BlobToFile(pBlob,TempDocFileName)
  EditWordDocument(TempDocFileName)
  if SaveAfterEdit then
     ans# = FileToBlob(TempDocFileName,pBlob)
  End
  Remove(TempDocFileName)
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
GetFieldsToMerge     PROCEDURE  (*TMergeFieldsQueue retQFields,LONG pReportId) ! Declare Procedure

  CODE
    free(retQFields)
    CASE pReportId
    OF WORDREPORT_FuelSurcharge
        clear(retQFields)
        retQFields.FieldName  = 'Fuel Surcharge Fields'
        retQFields.Level      = 1
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'Client_Address'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'Run_date'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'Client_No'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'Client_fax_No'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'FuelBaseRate_1stRecord'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'FuelBaseRate_As_of_ClientsFuelCostEffectiveDate'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'ClientsFuelSurcharge_At_RunForEffectiveDate'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'ClientsFuelSurcharge_EffectiveDate'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'FuelCostBaseDate'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'FuelCost_At_FuelCostBaseDate'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'FuelCost_At_Run_Date'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'CPLChange'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'CPLChangePercent'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'FuelCostBaseDate_Month_Year'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'BaseRate'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'BaseIncrease'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'BaseIncreasePercent'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'BaseChange'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'OtherRate'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'OtherChange'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'TotalChange'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'RevisedFuelBasePercent'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'RevisedOtherPercent'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'RevisedTotalPercent'
        retQFields.Level      = 2
        ADD(retQFields)

        clear(retQFields)
        retQFields.FieldName  = 'FuelSurcharge'
        retQFields.Level      = 2
        ADD(retQFields)
    END
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
GetWordReportName    PROCEDURE  (LONG pReportId)           ! Declare Procedure

  CODE
    CASE pReportId
    OF WORDREPORT_FuelSurcharge
        RETURN 'Fuel Surcharge'
    END
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
FillQReports         PROCEDURE  (*TStringIdQueue rQReport) ! Declare Procedure

  CODE
    free(rQReport)
    rQReport.Id  = WORDREPORT_FuelSurcharge
    rQReport.Str = GetWordReportName(rQReport.Id)
    ADD(rQReport)
