

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetEmail.inc'),ONCE

                     MAP
                       INCLUDE('FUNTRNIS026.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Client_Invoice_Totals PROCEDURE  (p:CID, p:Date, p:Option, p_Bal_Group) ! Declare Procedure
LOC:Statement_Info   GROUP,PRE(L_SI)                       !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
                     END                                   !
LOC:Return           LONG                                  !
LOC:SQL_Str          STRING(1000)                          !
LOC:Date             DATE                                  !
LOC:Count            LONG                                  !
Tek_Failed_File     STRING(100)

sql_        SQLQueryClass

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
     Access:ClientsAlias.UseFile()
     Access:_Invoice.UseFile()
    ! (p:CID, p:Date, p:Option, p_Bal_Group)
    ! (ULONG, LOND=0, BYTE=0, *STRING),LONG,PROC
    !
    !   p:Option
    !       0   - Total
    !       1   - VAT
    !       2   - Freight
    !       3   - Additional
    !       4   - Fuel
    !       5   - Doc
    !       6   - Insurance
    !
    ! Note: 90 days is only 90 days not all before that as well
    !       Total is all time total
    !
    ! Gets group of data, Current, 30 days, 60 days, 90 days, total

    SETCURSOR(CURSOR:WAIT)
    CLEAR(LOC:Statement_Info)
    sql_.Init(GLO:DBOwner, '_SQLTemp2')

    A_CLI:CID               = p:CID
    IF Access:ClientsAlias.TryFetch(A_CLI:PKey_CID) ~= LEVEL:Benign
       LOC:Return           = -1
    ELSE
       CLEAR(LOC:SQL_Str)
       IF p:Date = 0
          p:Date    = TODAY()
       .

       LOC:Date     = p:Date

       LOOP 5 TIMES
          LOC:Count    += 1
          LOC:SQL_Str   = 'CID = ' & p:CID

          Add_to_List('Total > 0.0', LOC:SQL_Str, ' AND ')

          IF LOC:Count > 4
             ! Total
          ELSE
             ! Note next day used for query due to time
             Add_to_List('InvoiceDateAndTime < '  & SQL_Get_DateT_G(LOC:Date + 1,,1) , LOC:SQL_Str, ' AND ')
             Add_to_List('InvoiceDateAndTime >= ' & SQL_Get_DateT_G(LOC:Date - 30,,1), LOC:SQL_Str, ' AND ')
          .

          LOC:SQL_Str   = 'SELECT SUM(Total), SUM(VAT), SUM(FreightCharge), SUM(AdditionalCharge), SUM(FuelSurcharge), ' & |
                            'SUM(Documentation), SUM(Insurance)'                                                         & |
                            'FROM _Invoice WHERE ' & CLIP(LOC:SQL_Str)

          sql_.PropSQL(CLIP(LOC:SQL_Str))
          IF sql_.Next_Q() > 0
             EXECUTE LOC:Count
                L_SI:Current    = sql_.Get_El(p:Option + 1)
                L_SI:Days30     = sql_.Get_El(p:Option + 1)
                L_SI:Days60     = sql_.Get_El(p:Option + 1)
                L_SI:Days90     = sql_.Get_El(p:Option + 1)
                L_SI:Total      = sql_.Get_El(p:Option + 1)
             .
          ELSE
             ! No records for the period
          .

          LOC:Date     -= 30
    .  .

    p_Bal_Group         = LOC:Statement_Info
    SETCURSOR()
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:ClientsAlias.Close()
    Relate:_Invoice.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Gen_Statement_230211 PROCEDURE  (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output) ! Declare Procedure
LOC:Result           LONG                                  !
LOC:MonthEndDate     LONG                                  !
LOC:Run_Date_Mth_End BYTE                                  !
LOC:Next_Mth_Mth_End BYTE                                  !
LOC:Statement_Info   GROUP,PRE(L_SI)                       !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
                     END                                   !
LOC:Credited         DECIMAL(11,2)                         !
LOC:Amount           DECIMAL(10,2)                         !
LOC:CreditNotes_Added_Q QUEUE,PRE(L_CQ)                    !
IID                  ULONG                                 !Invoice Number
                     END                                   !
LOC:Stats_Group      GROUP,PRE(L_SG)                       !
No_Invs              LONG                                  !
TimeIn               LONG                                  !
                     END                                   !
LOC:Skip_Statement   BYTE                                  !
Tek_Failed_File     STRING(100)

Inv_View            VIEW(_Invoice)
    PROJECT(INV:IID, INV:BID, INV:CID, INV:DINo, INV:InvoiceDate, INV:InvoiceTime, INV:Total, INV:DID, INV:Status, INV:StatusUpToDate, INV:ClientNo)
    .

View_Inv            ViewManager



InvA_View            VIEW(InvoiceAlias)
    PROJECT(A_INV:IID, A_INV:BID, A_INV:CID, A_INV:DINo, A_INV:InvoiceDate, A_INV:InvoiceTime, A_INV:Total, A_INV:DID, A_INV:Status)
    .

View_InvA            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAllocation.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Statements.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_StatementItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceAlias.Open()
     .
     Access:_Invoice.UseFile()
     Access:ClientsPaymentsAllocation.UseFile()
     Access:_Statements.UseFile()
     Access:_StatementItems.UseFile()
     Access:InvoiceAlias.UseFile()
    PUSHBIND
    BIND('INV:InvoiceDate', INV:InvoiceDate)
    BIND('INV:InvoiceTime', INV:InvoiceTime)
    BIND('INV:InvoiceDateAndTime', INV:InvoiceDateAndTime)
    BIND('INV:Status', INV:Status)
    BIND('INV:CR_IID', INV:CR_IID)
    BIND('A_INV:InvoiceDate', A_INV:InvoiceDate)
    BIND('A_INV:InvoiceTime', A_INV:InvoiceTime)
    BIND('A_INV:InvoiceDateAndTime', A_INV:InvoiceDateAndTime)
    BIND('A_INV:Status', A_INV:Status)
    
    ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:OutPut)
    !     1       2      3        4       5          6            7               8            9             10
    ! (ULONG, ULONG, LONG, LONG, <*DECIMAL>, BYTE=0, <*STRING>, BYTE=0, BYTE=0, BYTE=0),LONG
    !   p:Date  - Run / Effective date
    !   p:Run_Type
    !       0   -               ! Client & Statement Gen.
    !       1   - 
    !       2   -
    !
    !   p:OutPut
    !       0   - none
    !       1   - All
    !       2   - Current
    !       3   - 30 Days
    !       4   - 60 Days
    !       5   - 90 Days
    !
    ! We should run the invoice maintenance function before running the statements... maybe not though, see code

    L_SG:TimeIn                 = CLOCK()

    ! If p:MonthEndDay is provided it is taken to be the forthcoming month end otherwise settings are used
    ! Get_Month_End_Date
    ! (p:Date, p:MonthEndDay)
    ! (LONG, BYTE=0),DATE
    LOC:MonthEndDate            = Get_Month_End_Date(p:Date, p:MonthEndDay)

!    db.debugout('[Gen_Statement]   p:MonthEndDay: ' & p:MonthEndDay & ',   Date: ' & FORMAT(LOC:MonthEndDate,@d6))
!    db.debugout('[Gen_Statement]  In - CID: ' & p:CID & ',  Date: ' & FORMAT(p:Date,@d5))
    IF p:Dont_Add = TRUE
       DO Get_Details
       IF ~OMITTED(7)
          p:Statement_Info      = LOC:Statement_Info
       .
    ELSE
       ! Create Statement
       CLEAR(STA:Record)
       IF Access:_Statements.TryPrimeAutoInc() = LEVEL:Benign
          !STA:STID            auto inc
          STA:STRID             = p:STRID
          STA:StatementDate     = p:Date
          STA:StatementTime     = p:Time

          STA:CID               = p:CID
          STA:BID               = GLO:BranchID

          DO Get_Details
          STA:Record           :=: LOC:Statement_Info

          IF STA:Total = 0.0
             ! No statement when nothing to state.
             IF Access:_Statements.CancelAutoInc() ~= LEVEL:Benign
             .
          ELSE
             IF Access:_Statements.TryInsert() ~= LEVEL:Benign
                ! hmm ??
             ELSE
                LOC:Result      = TRUE
          .  .
       ELSE
          ! hmmm?
    .  .

    IF ~OMITTED(5)
       p:Owing                  = L_SI:Total
    .

    ! Update Client - updates Client Record with these balances
    Upd_Client_Balances(p:CID, p:Date, LOC:Statement_Info)


    IF CLOCK() - L_SG:TimeIn > 100
       db.debugout('[Gen_Statement]  No. Invs: ' & L_SG:No_Invs & ',   CID: ' & p:CID        & |
                    ',  Time > 1 sec: ' & CLOCK() - L_SG:TimeIn)
    .
    UNBIND('INV:InvoiceDate')
    UNBIND('INV:InvoiceTime')
    UNBIND('INV:InvoiceDateAndTime')
    UNBIND('INV:Status')
    UNBIND('INV:CR_IID')
    UNBIND('A_INV:InvoiceDate')
    UNBIND('A_INV:InvoiceTime')
    UNBIND('A_INV:InvoiceDateAndTime')
    UNBIND('A_INV:Status')
    POPBIND
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:_Invoice.Close()
    Relate:ClientsPaymentsAllocation.Close()
    Relate:_Statements.Close()
    Relate:_StatementItems.Close()
    Relate:InvoiceAlias.Close()
    Exit
Get_Details                           ROUTINE
!    My_SQL.Init(GLO:DBOwner,'_TempSQL')
!
!    IF My_SQL.PropSQL('SELECT IID, BID, CID, DINo, InvoiceDate, InvoiceTime, Total, DID, Status, StatusUpToDate, ClientNo' & |
!                    ' FROM _Invoice WHERE CID = ' & p:CID & ' AND InvoiceDate < ' & SQL_Get_DateT_G(p:Date + 1) & |
!                    ' AND AND Status < 4 ORDER BY InvoiceDate') < 0
!       MESSAGE('Error on SQL.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Gen_Statement', ICON:Hand)
!    .
!
    L_SG:No_Invs        = 0

    View_Inv.Init(Inv_View, Relate:_Invoice)
    View_Inv.AddSortOrder(INV:FKey_CID)
    View_Inv.AddRange(INV:CID, p:CID)

    View_Inv.AppendOrder('INV:InvoiceDate')

    ! Assume they only want current, 2 is fully paid
    ! Note: use of tomorrow as date, with no time specified.

    !       0           1               2              3                 4               5              6         7
    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown, Bad Debt, Over Paid
    ! Fully paid invoices are excluded from the set because they cannot impact on the balance owing
    ! 8 May, added conditions from OR onwards
    !      , note Bad Debts are not dealt with here.....???

    View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND (INV:Status < 4 OR (INV:Status = 5 AND INV:CR_IID = 0) OR INV:Status = 7)')
    !IF p:Dont_Add = TRUE
    !   Add a filter for Credit notes (2) as they cannot impact on an Age analysis run
    !.

    View_Inv.Reset()
    IF ERRORCODE()
       MESSAGE('Error on View.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Gen_Statement', ICON:Hand)
    .
    LOOP
       IF View_Inv.Next() ~= LEVEL:Benign
          BREAK
       .
       L_SG:No_Invs += 1


       ! 8 May 08 - With new condition above we need condition here to avoid showing shown
       LOC:Skip_Statement       = FALSE
       IF INV:Status = 5 OR INV:Status = 7
          ! Then we dont want to do
          LOC:Skip_Statement    = TRUE
       .

       ! Here we only have the invoices less than the date specified.
       ! and not fully paid.
       ! For credit notes, if they are appearing on their 1st statement, then set them to having been shown
       IF p:Run_Type < 2 AND p:Dont_Add = FALSE          ! Client & Statement Gen.
          Upd_Invoice_Paid_Status(INV:IID, 1)            ! Update Fully Paid - Credit to Fully Paid and Credit Note to Credit Note - Shown
       ELSE
          IF INV:StatusUpToDate = FALSE                  ! We know we need to check the Invoice Status
             Upd_Invoice_Paid_Status(INV:IID)
       .  .

       IF p:Dont_Add = FALSE            ! Add to Statement
          L_CQ:IID       = INV:IID
          GET(LOC:CreditNotes_Added_Q, L_CQ:IID)
          IF ~ERRORCODE()               ! Already have shown Credit note with respective invoice
             CYCLE
          .

          IF LOC:Skip_Statement = FALSE
             DO Add_to_Statement
          .
       ELSE
          IF INV:Status = 2 OR INV:Status = 5                             ! If this is a Credit Note, then no amount
             STAI:Amount    = 0.0
          ELSE
             STAI:Debit     = INV:Total                                   ! Invoice total

             ! Check that Invoice has no Credit Notes for it
             LOC:Credited   = Get_Inv_Credited( INV:IID, p:Date )         ! Less any Credit Notes

             STAI:Debit    += LOC:Credited

             STAI:Credit    = Get_ClientsPay_Alloc_Amt( INV:IID, 0, p:Date )      ! Payment total for this Invoice

             STAI:Amount    = STAI:Debit - STAI:Credit
          .

          LOC:Amount        = STAI:Amount
       .

!    db.debugout('[Gen_Statement]  CID: ' & p:CID & ',  INV:IID: ' & INV:IID & ',  INV:CID: ' & INV:CID & ',  INV:DID: ' & INV:DID & ',  INV:Total: ' & INV:Total)
       CASE Months_Between(LOC:MonthEndDate, INV:InvoiceDate)
       OF 0
          L_SI:Current   += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 2                       ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             Add_Log(',' & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, 'Current', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       OF 1
          L_SI:Days30    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 3
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '30 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       OF 2
          L_SI:Days60    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 4
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '60 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       ELSE
          L_SI:Days90    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 5
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '90 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
    .  .

    View_Inv.Kill()

    L_SI:Total            = L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current
    EXIT
Add_to_Statement                ROUTINE
    IF Access:_StatementItems.TryPrimeAutoInc() = LEVEL:Benign
       ! STAI:STIID
       STAI:STID            = STA:STID

       STAI:InvoiceDate     = INV:InvoiceDate
       STAI:IID             = INV:IID
       STAI:DID             = INV:DID
       STAI:DINo            = INV:DINo

       ! p:Option
       !   0.  Invoice Paid
       !   1.  Clients Payments Allocation total
       STAI:Debit           = INV:Total

       ! When Credit
       LOC:Credited         = Get_Inv_Credited( INV:IID )
       STAI:Debit          += LOC:Credited               ! Less any Credit

       STAI:Credit          = Get_ClientsPay_Alloc_Amt( INV:IID, 0 )

       !    0               1               2           3                   4               5
       ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
       IF INV:Status = 2 OR INV:Status = 5          ! Credit notes
          STAI:Amount       = 0.0

          ! FBN wants the "Credit" amount to be in the Credit column and a + amount
          STAI:Credit       = INV:Total
          STAI:Debit       -= INV:Total
       ELSE
          STAI:Amount       = STAI:Debit - STAI:Credit
       .

       LOC:Amount           = STAI:Amount

       IF Access:_StatementItems.TryInsert() = LEVEL:Benign
          IF INV:Status = 2 OR INV:Status = 5      !LOC:Credited ~= 0.0
             ! Add the credit note in here and add it to the Q so that it is not added here again
             L_CQ:IID       = INV:IID

             DO Credit_Notes_for_IID

             ADD(LOC:CreditNotes_Added_Q)
    .  .  .
    EXIT
Credit_Notes_for_IID            ROUTINE
    View_InvA.Init(InvA_View, Relate:InvoiceAlias)
    View_InvA.AddSortOrder(A_INV:Key_CR_IID)
    View_InvA.AppendOrder('A_INV:InvoiceDate')
    View_InvA.AddRange(A_INV:CR_IID, L_CQ:IID)

    !       0           1               2              3                 4               5
    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
    View_InvA.SetFilter('A_INV:InvoiceDate < ' & p:Date + 1 & ' AND A_INV:Status < 4')

    View_InvA.Reset()
    LOOP
       IF View_InvA.Next() ~= LEVEL:Benign
          BREAK
       .

       ! Show it then after the invoice
       IF Access:_StatementItems.TryPrimeAutoInc() = LEVEL:Benign
          ! STAI:STIID
          STAI:STID            = STA:STID

          STAI:InvoiceDate     = A_INV:InvoiceDate
          STAI:IID             = A_INV:IID
          STAI:DID             = A_INV:DID
          STAI:DINo            = A_INV:DINo

          ! p:Option
          !   0.  Invoice Paid
          !   1.  Clients Payments Allocation total
          STAI:Debit           = A_INV:Total

          ! When Credit
          LOC:Credited         = Get_Inv_Credited( A_INV:IID )                      ! --------- should not be for credit notes
          STAI:Debit          += LOC:Credited               ! Less any Credit

          STAI:Credit          = Get_ClientsPay_Alloc_Amt( A_INV:IID, 0 )           ! --------- should not be for credit notes

          !    0               1               2           3                   4               5
          ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
          IF A_INV:Status = 2                                                       ! --------- should only be
             STAI:Amount       = 0.0

             ! FBN wants the "Credit" amount to be in the Credit column
             STAI:Credit      -= A_INV:Total
             STAI:Debit       -= A_INV:Total
          .

    db.debugout('[Gen_Statement]  Adding Credit note - STAI:STIID: ' & STAI:STIID)

          IF Access:_StatementItems.TryInsert() = LEVEL:Benign
    .  .  .

    View_InvA.Kill()
    EXIT
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
GetSQLValue          PROCEDURE  (String pRequest,<*File pFile>,<*? pField>) ! Declare Procedure
RetVal CString(256)
FilesOpened     BYTE(0)

  CODE
  if Omitted(pFile) then
     Do OpenFiles
     _SQLTemp{Prop:SQL} = pRequest
     if ~SQLError(_SQLTemp) then
        Next(_SQLTemp)
        if ~ErrorCode() then
           RetVal = Clip(_SQ:S1)
           Do CLoseFiles
           Return RetVal
        else
           Case ErrorCode()
           of 33
           else
              SetClipboard(_SQLTemp{Prop:SQL})
              Message('Error occured during Next statement on SQLFile, SQL request:||'&_SQLTemp{Prop:SQL}&'||'&Error()&'||SQL request has been copied into clipboard!', 'TransIS - Error', Icon:Hand)
           End
        End
     End
     Do CloseFiles
  else
     if ~Omitted(pField) then
        pFile{Prop:SQL} = pRequest
        if ~SQLError(pFile) then
           Next(pFile)
           if ~ErrorCode() then
              Return pField
           else
              Case ErrorCode()
              of 33
              else
                 SetClipboard(pFile{Prop:SQL})
                 Message('Error occured during Next statement on '&pFile{Prop:Name}&', SQL request:||'&pFile{Prop:SQL}&'||'&Error()&'||SQL request has been copied into clipboard!', 'TransIS - Error', Icon:Hand)
              End
           End
        End
     else
        Message('Wrong using of GetSQLValue function!', 'TransIS - Error', Icon:Hand)
     End
  End
  Return ''
!--------------------------------------
OpenFiles  ROUTINE
  Access:_SQLTemp.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:_SQLTemp.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:_SQLTemp.Close
     FilesOpened = False
  END
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Client_Load_Types PROCEDURE  (p_CID, p_LTID)           ! Declare Procedure
LOC:Count            ULONG                                 !
FilesOpened     BYTE(0)

  CODE
    DO OpenFiles
    
    SET(LoadTypes2Alias)
    LOOP
      NEXT(LoadTypes2Alias)
      IF ERRORCODE()
         BREAK         
      .
      
      ! Consolidated|Container Park|Container|Full Load|Empty Container|Local Delivery
      !      0           1              2         3           4               5
      IF A_LOAD2:LoadOption = 1
         IF Get_Clients_CP_Related(p_CID) = 0
            CYCLE
         .
      ELSE
         IF Get_Clients_Related(p_CID, A_LOAD2:LTID, 1) = 0
            CYCLE
      .  .  
      
      LOC:Count    += 1
      IF LOC:Count = 1
         p_LTID     = A_LOAD2:LTID
   .  .
   
   DO CloseFiles
   
   RETURN(LOC:Count)
!--------------------------------------
OpenFiles  ROUTINE
  Access:LoadTypes2Alias.Open                              ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LoadTypes2Alias.UseFile                           ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LoadTypes2Alias.Close
     FilesOpened = False
  END
!!! <summary>
!!! Generated from procedure template - Source
!!! (p:CID, p:Type, p:Option)
!!! </summary>
Get_Client_Emails    PROCEDURE  (p:CID, p:Type, p:Option)  ! Declare Procedure
LOC:Return           STRING(1001)                          !
LOC:AddAddress       BYTE                                  !
LOC:Address          STRING(255)                           !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:EmailAddresses.Open()
     .
     Access:EmailAddresses.UseFile()
    ! p:Type   -  Default 0, RateLetter 1, Operations 2
    ! p:Option -  address 0, name <address> 1
    ! (ULONG, BYTE=0, BYTE=0)

    CLEAR(EMAI:Record, -1)
    EMAI:CID = p:CID
    SET(EMAI:FKey_CID, EMAI:FKey_CID)
    LOOP
      IF Access:EmailAddresses.TryNext() ~= Level:Benign
        BREAK
      .
      IF EMAI:CID <> p:CID
        BREAK
      .
      
      LOC:AddAddress  = FALSE
      EXECUTE p:Type + 1
        LOC:AddAddress = EMAI:DefaultAddress 
        LOC:AddAddress = EMAI:RateLetter
        LOC:AddAddress = EMAI:Operations
      .

      IF LOC:AddAddress <> FALSE
        LOC:Address = EMAI:EmailAddress
        IF p:Option = 1
          LOC:Address = CLIP(EMAI:EmailName) & ' <' & CLIP(LOC:Address) & '>'
        .
        
!        message('here  LOC:Address ' & LOC:Address)   
        
        Add_to_List(LOC:Address, LOC:Return, ';')
      .
    .

      
!     message('p:Type: ' & p:Type & ',  retu: ' & LOC:Return)
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:EmailAddresses.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
!!! </summary>
Gen_Statement_old_June12 PROCEDURE  (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output) ! Declare Procedure
LOC:Result           LONG                                  !
LOC:MonthEndDate     LONG                                  !
LOC:Run_Date_Mth_End BYTE                                  !
LOC:Next_Mth_Mth_End BYTE                                  !
LOC:Statement_Info   GROUP,PRE(L_SI)                       !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
                     END                                   !
LOC:Credited         DECIMAL(11,2)                         !
LOC:Amount           DECIMAL(10,2)                         !
LOC:CreditNotes_Added_Q QUEUE,PRE(L_CQ)                    !
IID                  ULONG                                 !Invoice Number
                     END                                   !
LOC:Stats_Group      GROUP,PRE(L_SG)                       !
No_Invs              LONG                                  !
TimeIn               LONG                                  !
                     END                                   !
LOC:Skip_Statement   BYTE                                  !
Tek_Failed_File     STRING(100)

Inv_View            VIEW(_Invoice)
    PROJECT(INV:IID, INV:BID, INV:CID, INV:DINo, INV:InvoiceDate, INV:InvoiceTime, INV:Total, INV:DID, INV:Status, INV:StatusUpToDate, INV:ClientNo, INV:CR_IID)
    .

View_Inv            ViewManager



InvA_View            VIEW(InvoiceAlias)
    PROJECT(A_INV:IID, A_INV:BID, A_INV:CID, A_INV:DINo, A_INV:InvoiceDate, A_INV:InvoiceTime, A_INV:Total, A_INV:DID, A_INV:Status, A_INV:CR_IID)
    .

View_InvA            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAllocation.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Statements.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_StatementItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceAlias.Open()
     .
     Access:_Invoice.UseFile()
     Access:ClientsPaymentsAllocation.UseFile()
     Access:_Statements.UseFile()
     Access:_StatementItems.UseFile()
     Access:InvoiceAlias.UseFile()
    PUSHBIND
    BIND('INV:InvoiceDate', INV:InvoiceDate)
    BIND('INV:InvoiceTime', INV:InvoiceTime)
    BIND('INV:InvoiceDateAndTime', INV:InvoiceDateAndTime)
    BIND('INV:Status', INV:Status)
    BIND('INV:CR_IID', INV:CR_IID)
    BIND('A_INV:InvoiceDate', A_INV:InvoiceDate)
    BIND('A_INV:InvoiceTime', A_INV:InvoiceTime)
    BIND('A_INV:InvoiceDateAndTime', A_INV:InvoiceDateAndTime)
    BIND('A_INV:Status', A_INV:Status)
    
    ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:OutPut)
    !     1       2      3        4       5          6            7               8            9             10
    ! (ULONG, ULONG, LONG, LONG, <*DECIMAL>, BYTE=0, <*STRING>, BYTE=0, BYTE=0, BYTE=0),LONG
    !   p:Date  - Run / Effective date
    !   p:Run_Type
    !       0   -               ! Client & Statement Gen.
    !       1   - 
    !       2   -
    !
    !   p:OutPut
    !       0   - none
    !       1   - All
    !       2   - Current
    !       3   - 30 Days
    !       4   - 60 Days
    !       5   - 90 Days
    !
    ! We should run the invoice maintenance function before running the statements... maybe not though, see code

    L_SG:TimeIn                 = CLOCK()

    ! If p:MonthEndDay is provided it is taken to be the forthcoming month end otherwise settings are used
    ! Get_Month_End_Date
    ! (p:Date, p:MonthEndDay)
    ! (LONG, BYTE=0),DATE
    LOC:MonthEndDate            = Get_Month_End_Date(p:Date, p:MonthEndDay)

!    db.debugout('[Gen_Statement]   p:MonthEndDay: ' & p:MonthEndDay & ',   Date: ' & FORMAT(LOC:MonthEndDate,@d6))
!    db.debugout('[Gen_Statement]  In - CID: ' & p:CID & ',  Date: ' & FORMAT(p:Date,@d5))
    IF p:Dont_Add = TRUE
       DO Get_Details
       IF ~OMITTED(7)
          p:Statement_Info      = LOC:Statement_Info
       .
    ELSE
       ! Create Statement
       CLEAR(STA:Record)
       IF Access:_Statements.TryPrimeAutoInc() = LEVEL:Benign
          !STA:STID            auto inc
          STA:STRID             = p:STRID
          STA:StatementDate     = p:Date
          STA:StatementTime     = p:Time

          STA:CID               = p:CID
          STA:BID               = GLO:BranchID

          DO Get_Details
          STA:Record           :=: LOC:Statement_Info

          IF STA:Total = 0.0
             ! No statement when nothing to state.
             IF Access:_Statements.CancelAutoInc() ~= LEVEL:Benign
             .
          ELSE
             IF Access:_Statements.TryInsert() ~= LEVEL:Benign
                ! hmm ??
             ELSE
                LOC:Result      = TRUE
          .  .
       ELSE
          ! hmmm?
    .  .

    IF ~OMITTED(5)
       p:Owing                  = L_SI:Total
    .

    ! Update Client - updates Client Record with these balances
    Upd_Client_Balances(p:CID, p:Date, LOC:Statement_Info)


    IF CLOCK() - L_SG:TimeIn > 100
       db.debugout('[Gen_Statement]  No. Invs: ' & L_SG:No_Invs & ',   CID: ' & p:CID        & |
                    ',  Time > 1 sec: ' & CLOCK() - L_SG:TimeIn)
    .
    UNBIND('INV:InvoiceDate')
    UNBIND('INV:InvoiceTime')
    UNBIND('INV:InvoiceDateAndTime')
    UNBIND('INV:Status')
    UNBIND('INV:CR_IID')
    UNBIND('A_INV:InvoiceDate')
    UNBIND('A_INV:InvoiceTime')
    UNBIND('A_INV:InvoiceDateAndTime')
    UNBIND('A_INV:Status')
    POPBIND
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:_Invoice.Close()
    Relate:ClientsPaymentsAllocation.Close()
    Relate:_Statements.Close()
    Relate:_StatementItems.Close()
    Relate:InvoiceAlias.Close()
    Exit
Get_Details                           ROUTINE
!    My_SQL.Init(GLO:DBOwner,'_TempSQL')
!
!    IF My_SQL.PropSQL('SELECT IID, BID, CID, DINo, InvoiceDate, InvoiceTime, Total, DID, Status, StatusUpToDate, ClientNo' & |
!                    ' FROM _Invoice WHERE CID = ' & p:CID & ' AND InvoiceDate < ' & SQL_Get_DateT_G(p:Date + 1) & |
!                    ' AND AND Status < 4 ORDER BY InvoiceDate') < 0
!       MESSAGE('Error on SQL.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Gen_Statement', ICON:Hand)
!    .
!
    L_SG:No_Invs        = 0

    View_Inv.Init(Inv_View, Relate:_Invoice)
    View_Inv.AddSortOrder(INV:FKey_CID)
    View_Inv.AddRange(INV:CID, p:CID)

    View_Inv.AppendOrder('INV:InvoiceDate')

    ! Assume they only want current, 2 is fully paid
    ! Note: use of tomorrow as date, with no time specified.

    !       0           1               2              3                 4               5              6         7
    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown, Bad Debt, Over Paid
    ! Fully paid invoices are excluded from the set because they cannot impact on the balance owing
    ! 8 May, added conditions from OR onwards
    !      , note Bad Debts are not dealt with here.....???

    !View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND (INV:Status < 4 OR (INV:Status = 5 AND INV:CR_IID = 0) OR INV:Status = 7)')

    !25.02.2011 by Victor. Client said: 
    ! "on every statement it should show the invoice and all credit notes against it - until the invoice is fully paid - 
    ! then it shows once more and never again", so
    ! 1) Include fully paid invoices in the view. They be excluded later depending on whether they appeared on Statements
    ! 2) Exclude credit notes, they will be calculated separately for each invoice listed    
    !View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND ( INV:CR_IID = 0 AND INV:Status < 5 AND INV:Status NOT= 2) OR INV:Status = 7 )')
    View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND (INV:CR_IID = 0 AND INV:Status < 4 AND INV:Status ~= 2 OR (INV:CR_IID = 0 AND INV:Status = 7))')


    !IF p:Dont_Add = TRUE ! Victor: old functionality - ignore
    !   Add a filter for Credit notes (2) as they cannot impact on an Age analysis run
    !.
   
    View_Inv.Reset()
    IF ERRORCODE()
       MESSAGE('Error on View.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Gen_Statement', ICON:Hand)
    .
    LOOP
       IF View_Inv.Next() ~= LEVEL:Benign
          BREAK
       .
       If INV:InvoiceDate >= p:Date + 1  Or INV:CR_IID ~= 0 Or (INV:Status > 5 AND INV:Status ~= 7) Or INV:Status = 2 Then
          Cycle
       End
       LOC:Amount = 0
       If INV:Status = 4 ! fully paid (added 25.02.2011)
          If p:Dont_Add = FALSE
             !If Check_IID_On_Statement(INV:IID) ~= 0 Then
                ! -1 = Error condition, found Statement Item but not Statement
                ! -2 = Error condition, found Statement but not Statement Run
                !  0 = No Statement found
                !  1 = Found on Statement but not Client Statement
                !  2 = Found on Client Statement
             !   Cycle
             !End
             Access:_SQLTemp.Open
             Access:_SQLTemp.UseFile
             if RunSQLRequest('Select Count(*) From _StatementItems Where IID = '&INV:IID,_SQLTemp)                
                If Access:_SQLTemp.TryNext() = Level:Benign THEN
                   cnt# = _SQ:S1
                   If cnt# Then
                      Cycle
                   End
                Else                   
                   Cycle !Could not get count
                End
             End
             Access:_SQLTemp.Close             
          Else
             !exclude fully paid invoices as before
             Cycle
          End
       End

       L_SG:No_Invs += 1


       ! 8 May 08 - With new condition above we need condition here to avoid showing shown
       LOC:Skip_Statement       = FALSE
       IF INV:Status = 5 OR INV:Status = 7
          ! Then we dont want to do
          LOC:Skip_Statement    = TRUE
       .
       
       ! Here we only have the invoices less than the date specified.
       ! and not fully paid (or fully paid not shown - Added by Victor)
       ! For credit notes, if they are appearing on their 1st statement, then set them to having been shown
       IF p:Run_Type < 2 AND p:Dont_Add = FALSE          ! Client & Statement Gen.
          Upd_Invoice_Paid_Status(INV:IID, 1)            ! Update Fully Paid - Credit to Fully Paid and Credit Note to Credit Note - Shown
       ELSE
          IF INV:StatusUpToDate = FALSE                  ! We know we need to check the Invoice Status
             Upd_Invoice_Paid_Status(INV:IID)
       .  .

       IF p:Dont_Add = FALSE            ! Add to Statement
          L_CQ:IID       = INV:IID
          GET(LOC:CreditNotes_Added_Q, L_CQ:IID)
          IF ~ERRORCODE()               ! Already have shown Credit note with respective invoice
             CYCLE
          .
          
          IF LOC:Skip_Statement = FALSE
             DO Add_to_Statement
          .
       ELSE
          IF INV:Status = 2 OR INV:Status = 5                             ! If this is a Credit Note, then no amount
             ! should not be within this branch since 25.02.2011
             !STAI:Amount    = 0.0 ! change of 17.03.2011 by Victor
          ELSE
             STAI:Debit     = INV:Total                                   ! Invoice total

             !LOC:Credited   = Get_Inv_Credited( INV:IID, p:Date )         ! Less any Credit Notes

             !STAI:Debit    += LOC:Credited ! remarked by Victor
             !Do not accoount Credit Notes for Debit anymore MIT:35-1826 (25.02.2011)
             ! Before 17.03.2010 there was:
             !credit note doesn't affect amount so we don't account it for credit at first
             ! with requirement of 14.03.2011 it should be accounted in total, so move calculating amount later

             STAI:Credit    = Get_ClientsPay_Alloc_Amt( INV:IID, 0, p:Date )      ! Payment total for this Invoice

             !STAI:Amount    = STAI:Debit - STAI:Credit

             ! In case of adding statement credit notes are added to credit as + amount, need to account for credit here
             ! (according to new requirement of 25.02.2011 it shoud include all notes of the invoice, so ommit date)
             LOC:Credited   = Get_Inv_Credited( INV:IID )
             STAI:Credit   -= LOC:Credited
             STAI:Amount    = STAI:Debit - STAI:Credit
          .

          LOC:Amount        = STAI:Amount
       .       
!    db.debugout('[Gen_Statement]  CID: ' & p:CID & ',  INV:IID: ' & INV:IID & ',  INV:CID: ' & INV:CID & ',  INV:DID: ' & INV:DID & ',  INV:Total: ' & INV:Total)
       CASE Months_Between(LOC:MonthEndDate, INV:InvoiceDate)
       OF 0
          L_SI:Current   += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 2                       ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             Add_Log(',' & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, 'Current', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       OF 1
          L_SI:Days30    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 3
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '30 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       OF 2
          L_SI:Days60    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 4
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '60 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       ELSE
          L_SI:Days90    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 5
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '90 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
    .  .

    View_Inv.Kill()

    L_SI:Total            = L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current
    EXIT
Add_to_Statement                ROUTINE
    IF Access:_StatementItems.TryPrimeAutoInc() = LEVEL:Benign
       ! STAI:STIID
       STAI:STID            = STA:STID

       STAI:InvoiceDate     = INV:InvoiceDate
       STAI:IID             = INV:IID
       STAI:DID             = INV:DID
       STAI:DINo            = INV:DINo

       ! p:Option
       !   0.  Invoice Paid
       !   1.  Clients Payments Allocation total
       STAI:Debit           = INV:Total

       ! When Credit
       LOC:Credited         = Get_Inv_Credited( INV:IID )
       !STAI:Debit          += LOC:Credited               ! Less any Credit
       !Do not accoount Credit Notes for Debit anymore MIT:35-1826 (25.02.2011) by Victor

       STAI:Credit          = Get_ClientsPay_Alloc_Amt( INV:IID, 0 )

       !    0               1               2           3                   4               5
       ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
       IF INV:Status = 2 OR INV:Status = 5          ! Credit notes
          ! normally should not be here as we have excluded credit notes since 25.02.2011
          !STAI:Amount       = 0.0 !changed 17.03.2011 by Victor

          ! FBN wants the "Credit" amount to be in the Credit column and a + amount
          STAI:Credit       = - INV:Total
          STAI:Debit       -= INV:Total
       ELSE
          !STAI:Amount       = STAI:Debit - STAI:Credit
       END
       STAI:Amount       = STAI:Debit - STAI:Credit ! moved from Else 17.03.2011

       LOC:Amount           = STAI:Amount
       
       IF Access:_StatementItems.TryInsert() = LEVEL:Benign          
          DO Credit_Notes_for_IID ! Add all credit notes of the respective invoice if any (and add them to the Q so that it is not added here again)

    .  .
    EXIT
    
Credit_Notes_for_IID            ROUTINE
    View_InvA.Init(InvA_View, Relate:InvoiceAlias)
    View_InvA.AddSortOrder(A_INV:Key_CR_IID)
    View_InvA.AppendOrder('A_INV:InvoiceDate')
    View_InvA.AddRange(A_INV:CR_IID, INV:IID) ! NOT L_CQ:IID, L_CQ:IID is a credit note IID

    !       0           1               2              3                 4               5
    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
    !View_InvA.SetFilter('A_INV:InvoiceDate < ' & p:Date + 1 & ' AND A_INV:Status < 4')
    !Do not accoounted Credit Notes for Debit anymore MIT:35-1826 (25.02.2011) by Victor

    View_InvA.Reset()
    LOOP
       IF View_InvA.Next() ~= LEVEL:Benign
          BREAK
        .        

       ! Show it then after the invoice
       IF Access:_StatementItems.TryPrimeAutoInc() = LEVEL:Benign
          ! STAI:STIID
          STAI:STID            = STA:STID

          STAI:InvoiceDate     = A_INV:InvoiceDate
          STAI:IID             = A_INV:IID
          STAI:DID             = A_INV:DID
          STAI:DINo            = A_INV:DINo

          ! p:Option
          !   0.  Invoice Paid
          !   1.  Clients Payments Allocation total
          STAI:Debit           = A_INV:Total

          ! When Credit
          LOC:Credited         = Get_Inv_Credited( A_INV:IID )                      ! --------- should not be for credit notes
          !STAI:Debit          += LOC:Credited               ! Less any Credit
          !Do not accoounted Credit Notes for Debit anymore MIT:35-1826 (25.02.2011) by Victor

          STAI:Credit          = Get_ClientsPay_Alloc_Amt( A_INV:IID, 0 )           ! --------- should not be for credit notes
          !    0               1               2           3                   4               5
          ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
          !IF A_INV:Status = 2 ! unconditionally since 25.02.2011                    ! --------- should only be
             !STAI:Amount       = 0.0 !changed 17.03.2011

             ! FBN wants the "Credit" amount to be in the Credit column
             STAI:Credit      -= A_INV:Total
             STAI:Debit       -= A_INV:Total
             STAI:Amount       = STAI:Debit - STAI:Credit !calculate Amount for credit note (since 17.03.2011)
          !.
    db.debugout('[Gen_Statement]  Adding Credit note - STAI:STIID: ' & STAI:STIID)

          IF Access:_StatementItems.TryInsert() = LEVEL:Benign             
             LOC:Amount    += STAI:Amount              !include the Anount in Total (since 17.03.2011)
             L_CQ:IID       = STAI:IID
             ADD(LOC:CreditNotes_Added_Q)             

             ! Ivan - as we are not setting Credit Note - Shown above anymore, because Credit notes are filtered out before they
             !        get to that code, we must now do that here
             IF p:Run_Type < 2 AND p:Dont_Add = FALSE AND A_INV:Status = 2    ! Client & Statement Gen.
                Upd_Invoice_Paid_Status(A_INV:IID, 1)            ! Update Fully Paid - Credit to Fully Paid and Credit Note to Credit Note - Shown

!    Message('[Gen_Statement]  Setting shown status - A_INV:IID: ' & A_INV:IID)
             .                
          End
       End
    End

    View_InvA.Kill()
    EXIT
!!! <summary>
!!! Generated from procedure template - Source
!!! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
!!! </summary>
Gen_Statement_new_but_not_right_yet PROCEDURE  (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output) ! Declare Procedure
LOC:Result           LONG                                  !
LOC:MonthEndDate     LONG                                  !
LOC:Run_Date_Mth_End BYTE                                  !
LOC:Next_Mth_Mth_End BYTE                                  !
LOC:Statement_Info   GROUP,PRE(L_SI)                       !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
                     END                                   !
LOC:Credited         DECIMAL(11,2)                         !
LOC:Amount           DECIMAL(10,2)                         !
LOC:CreditNotes_Added_Q QUEUE,PRE(L_CQ)                    !
IID                  ULONG                                 !Invoice Number
                     END                                   !
LOC:Stats_Group      GROUP,PRE(L_SG)                       !
No_Invs              LONG                                  !
TimeIn               LONG                                  !
                     END                                   !
LOC:Skip_Statement   BYTE                                  !
Tek_Failed_File     STRING(100)

Inv_View            VIEW(_Invoice)
    PROJECT(INV:IID, INV:BID, INV:CID, INV:DINo, INV:InvoiceDate, INV:InvoiceTime, INV:Total, INV:DID, INV:Status, INV:StatusUpToDate, INV:ClientNo, INV:CR_IID)
    .

View_Inv            ViewManager



InvA_View            VIEW(InvoiceAlias)
    PROJECT(A_INV:IID, A_INV:BID, A_INV:CID, A_INV:DINo, A_INV:InvoiceDate, A_INV:InvoiceTime, A_INV:Total, A_INV:DID, A_INV:Status, A_INV:CR_IID)
    .

View_InvA            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAllocation.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Statements.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_StatementItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceAlias.Open()
     .
     Access:_Invoice.UseFile()
     Access:ClientsPaymentsAllocation.UseFile()
     Access:_Statements.UseFile()
     Access:_StatementItems.UseFile()
     Access:InvoiceAlias.UseFile()
    PUSHBIND
    BIND('INV:InvoiceDate', INV:InvoiceDate)
    BIND('INV:InvoiceTime', INV:InvoiceTime)
    BIND('INV:InvoiceDateAndTime', INV:InvoiceDateAndTime)
    BIND('INV:Status', INV:Status)
    BIND('INV:CR_IID', INV:CR_IID)
    BIND('A_INV:InvoiceDate', A_INV:InvoiceDate)
    BIND('A_INV:InvoiceTime', A_INV:InvoiceTime)
    BIND('A_INV:InvoiceDateAndTime', A_INV:InvoiceDateAndTime)
    BIND('A_INV:Status', A_INV:Status)
    
    ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:OutPut)
    !     1       2      3        4       5          6            7               8            9             10
    ! (ULONG, ULONG, LONG, LONG, <*DECIMAL>, BYTE=0, <*STRING>, BYTE=0, BYTE=0, BYTE=0),LONG
    !   p:Date  - Run / Effective date
    !   p:Run_Type
    !       0   -               ! Client & Statement Gen.
    !       1   - 
    !       2   -
    !
    !   p:OutPut
    !       0   - none
    !       1   - All
    !       2   - Current
    !       3   - 30 Days
    !       4   - 60 Days
    !       5   - 90 Days
    !
    ! We should run the invoice maintenance function before running the statements... maybe not though, see code

    L_SG:TimeIn                 = CLOCK()

    ! If p:MonthEndDay is provided it is taken to be the forthcoming month end otherwise settings are used
    ! Get_Month_End_Date
    ! (p:Date, p:MonthEndDay)
    ! (LONG, BYTE=0),DATE
    LOC:MonthEndDate            = Get_Month_End_Date(p:Date, p:MonthEndDay)

!    db.debugout('[Gen_Statement]   p:MonthEndDay: ' & p:MonthEndDay & ',   Date: ' & FORMAT(LOC:MonthEndDate,@d6))
!    db.debugout('[Gen_Statement]  In - CID: ' & p:CID & ',  Date: ' & FORMAT(p:Date,@d5))
    IF p:Dont_Add = TRUE
       DO Get_Details
       IF ~OMITTED(7)
          p:Statement_Info      = LOC:Statement_Info
       .
    ELSE
       ! Create Statement
       CLEAR(STA:Record)
       IF Access:_Statements.TryPrimeAutoInc() = LEVEL:Benign
          !STA:STID            auto inc
          STA:STRID             = p:STRID
          STA:StatementDate     = p:Date
          STA:StatementTime     = p:Time

          STA:CID               = p:CID
          STA:BID               = GLO:BranchID

          DO Get_Details
          STA:Record           :=: LOC:Statement_Info

          IF STA:Total = 0.0
             ! No statement when nothing to state.
             IF Access:_Statements.CancelAutoInc() ~= LEVEL:Benign
             .
          ELSE
             IF Access:_Statements.TryInsert() ~= LEVEL:Benign
                ! hmm ??
             ELSE
                LOC:Result      = TRUE
          .  .
       ELSE
          ! hmmm?
    .  .

    IF ~OMITTED(5)
       p:Owing                  = L_SI:Total
    .

    ! Update Client - updates Client Record with these balances
    Upd_Client_Balances(p:CID, p:Date, LOC:Statement_Info)


    IF CLOCK() - L_SG:TimeIn > 100
       db.debugout('[Gen_Statement]  No. Invs: ' & L_SG:No_Invs & ',   CID: ' & p:CID        & |
                    ',  Time > 1 sec: ' & CLOCK() - L_SG:TimeIn)
    .
    UNBIND('INV:InvoiceDate')
    UNBIND('INV:InvoiceTime')
    UNBIND('INV:InvoiceDateAndTime')
    UNBIND('INV:Status')
    UNBIND('INV:CR_IID')
    UNBIND('A_INV:InvoiceDate')
    UNBIND('A_INV:InvoiceTime')
    UNBIND('A_INV:InvoiceDateAndTime')
    UNBIND('A_INV:Status')
    POPBIND
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:_Invoice.Close()
    Relate:ClientsPaymentsAllocation.Close()
    Relate:_Statements.Close()
    Relate:_StatementItems.Close()
    Relate:InvoiceAlias.Close()
    Exit
Get_Details                           ROUTINE
!    My_SQL.Init(GLO:DBOwner,'_TempSQL')
!
!    IF My_SQL.PropSQL('SELECT IID, BID, CID, DINo, InvoiceDate, InvoiceTime, Total, DID, Status, StatusUpToDate, ClientNo' & |
!                    ' FROM _Invoice WHERE CID = ' & p:CID & ' AND InvoiceDate < ' & SQL_Get_DateT_G(p:Date + 1) & |
!                    ' AND AND Status < 4 ORDER BY InvoiceDate') < 0
!       MESSAGE('Error on SQL.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Gen_Statement', ICON:Hand)
!    .
!
    L_SG:No_Invs        = 0

    View_Inv.Init(Inv_View, Relate:_Invoice)
    View_Inv.AddSortOrder(INV:FKey_CID)
    View_Inv.AddRange(INV:CID, p:CID)

    View_Inv.AppendOrder('INV:InvoiceDate')

    ! Assume they only want current, 2 is fully paid
    ! Note: use of tomorrow as date, with no time specified.

    !       0           1               2              3                 4               5              6         7
    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown, Bad Debt, Over Paid
    ! Fully paid invoices are excluded from the set because they cannot impact on the balance owing
    ! 8 May, added conditions from OR onwards
    !      , note Bad Debts are not dealt with here.....???

    !View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND (INV:Status < 4 OR (INV:Status = 5 AND INV:CR_IID = 0) OR INV:Status = 7)')

    !25.02.2011 by Victor. Client said: 
    ! "on every statement it should show the invoice and all credit notes against it - until the invoice is fully paid - 
    ! then it shows once more and never again", so
    ! 1) Include fully paid invoices in the view. They be excluded later depending on whether they appeared on Statements
    ! 2) Exclude credit notes, they will be calculated separately for each invoice listed    
    !View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND ( INV:CR_IID = 0 AND INV:Status < 5 AND INV:Status NOT= 2) OR INV:Status = 7 )')
    !View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND (INV:CR_IID = 0 AND INV:Status < 4 AND INV:Status ~= 2 OR (INV:CR_IID = 0 AND INV:Status = 7))')

    !27.02.12 by Ivan, including fully paid will include lots of select results we dont need.
    ! Looking at the filter condition it is no longer logical.  
    ! It used to say, if < fullpaid OR (creditnoteshown AND no credited inv) OR Over Paid
    ! now it says   , if no credited inv AND < fullypaid AND (not credit note OR (no credited inv AND Over Paid))
    !    in this case even if it was no credited inv AND was Over Paid, this True would be overridden by the < fullpaid requirement
  
    !    however if "a credit note" then if "not credit of inv" AND "is over paid" then this part would be TRUE   = a not yet shown credit note???
    !    if "not a credit note" then rest ignored
  
  
  !  View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND INV:CR_IID = 0 ' & | 
  !                          'AND INV:Status < 4 AND (INV:Status ~= 2 OR (INV:Status = 7))')
  
  ! STOPPED - logic is wrong but not exactly sure what should happen
  
    IF p:Dont_Add = TRUE       ! then we dont need to include Status 4 invoices   
      INV:Status = 4
    .
  
    !17.02.12  
       
    !IF p:Dont_Add = TRUE ! Victor: old functionality - ignore
    !   Add a filter for Credit notes (2) as they cannot impact on an Age analysis run
    !.
   
    View_Inv.Reset()
    IF ERRORCODE()
       MESSAGE('Error on View.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Gen_Statement', ICON:Hand)
    .
    LOOP
       IF View_Inv.Next() ~= LEVEL:Benign
          BREAK
       .
       If INV:InvoiceDate >= p:Date + 1  Or INV:CR_IID ~= 0 Or (INV:Status > 5 AND INV:Status ~= 7) Or INV:Status = 2 Then
          Cycle
       End
       LOC:Amount = 0
       If INV:Status = 4 ! fully paid (added 25.02.2011)
          If p:Dont_Add = FALSE
             !If Check_IID_On_Statement(INV:IID) ~= 0 Then
                ! -1 = Error condition, found Statement Item but not Statement
                ! -2 = Error condition, found Statement but not Statement Run
                !  0 = No Statement found
                !  1 = Found on Statement but not Client Statement
                !  2 = Found on Client Statement
             !   Cycle
             !End
             Access:_SQLTemp.Open
             Access:_SQLTemp.UseFile
             if RunSQLRequest('Select Count(*) From _StatementItems Where IID = '&INV:IID,_SQLTemp)                
                If Access:_SQLTemp.TryNext() = Level:Benign THEN
                   cnt# = _SQ:S1
                   If cnt# Then
                      Cycle
                   End
                Else                   
                   Cycle !Could not get count
                End
             End
             Access:_SQLTemp.Close             
          Else
             !exclude fully paid invoices as before
             Cycle
          End
       End

       L_SG:No_Invs += 1


       ! 8 May 08 - With new condition above we need condition here to avoid showing shown
       LOC:Skip_Statement       = FALSE
       IF INV:Status = 5 OR INV:Status = 7
          ! Then we dont want to do
          LOC:Skip_Statement    = TRUE
       .
       
       ! Here we only have the invoices less than the date specified.
       ! and not fully paid (or fully paid not shown - Added by Victor)
       ! For credit notes, if they are appearing on their 1st statement, then set them to having been shown
       IF p:Run_Type < 2 AND p:Dont_Add = FALSE          ! Client & Statement Gen.
          Upd_Invoice_Paid_Status(INV:IID, 1)            ! Update Fully Paid - Credit to Fully Paid and Credit Note to Credit Note - Shown
       ELSE
          IF INV:StatusUpToDate = FALSE                  ! We know we need to check the Invoice Status
             Upd_Invoice_Paid_Status(INV:IID)
       .  .

       IF p:Dont_Add = FALSE            ! Add to Statement
          L_CQ:IID       = INV:IID
          GET(LOC:CreditNotes_Added_Q, L_CQ:IID)
          IF ~ERRORCODE()               ! Already have shown Credit note with respective invoice
             CYCLE
          .
          
          IF LOC:Skip_Statement = FALSE
             DO Add_to_Statement
          .
       ELSE
          IF INV:Status = 2 OR INV:Status = 5                             ! If this is a Credit Note, then no amount
             ! should not be within this branch since 25.02.2011
             !STAI:Amount    = 0.0 ! change of 17.03.2011 by Victor
          ELSE
             STAI:Debit     = INV:Total                                   ! Invoice total

             !LOC:Credited   = Get_Inv_Credited( INV:IID, p:Date )         ! Less any Credit Notes

             !STAI:Debit    += LOC:Credited ! remarked by Victor
             !Do not accoount Credit Notes for Debit anymore MIT:35-1826 (25.02.2011)
             ! Before 17.03.2010 there was:
             !credit note doesn't affect amount so we don't account it for credit at first
             ! with requirement of 14.03.2011 it should be accounted in total, so move calculating amount later

             STAI:Credit    = Get_ClientsPay_Alloc_Amt( INV:IID, 0, p:Date )      ! Payment total for this Invoice

             !STAI:Amount    = STAI:Debit - STAI:Credit

             ! In case of adding statement credit notes are added to credit as + amount, need to account for credit here
             ! (according to new requirement of 25.02.2011 it shoud include all notes of the invoice, so ommit date)
             LOC:Credited   = Get_Inv_Credited( INV:IID )
             STAI:Credit   -= LOC:Credited
             STAI:Amount    = STAI:Debit - STAI:Credit
          .

          LOC:Amount        = STAI:Amount
       .       
!    db.debugout('[Gen_Statement]  CID: ' & p:CID & ',  INV:IID: ' & INV:IID & ',  INV:CID: ' & INV:CID & ',  INV:DID: ' & INV:DID & ',  INV:Total: ' & INV:Total)
       CASE Months_Between(LOC:MonthEndDate, INV:InvoiceDate)
       OF 0
          L_SI:Current   += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 2                       ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             Add_Log(',' & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, 'Current', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       OF 1
          L_SI:Days30    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 3
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '30 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       OF 2
          L_SI:Days60    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 4
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '60 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       ELSE
          L_SI:Days90    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 5
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '90 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
    .  .

    View_Inv.Kill()

    L_SI:Total            = L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current
    EXIT
Add_to_Statement                ROUTINE
    IF Access:_StatementItems.TryPrimeAutoInc() = LEVEL:Benign
       ! STAI:STIID
       STAI:STID            = STA:STID

       STAI:InvoiceDate     = INV:InvoiceDate
       STAI:IID             = INV:IID
       STAI:DID             = INV:DID
       STAI:DINo            = INV:DINo

       ! p:Option
       !   0.  Invoice Paid
       !   1.  Clients Payments Allocation total
       STAI:Debit           = INV:Total

       ! When Credit
       LOC:Credited         = Get_Inv_Credited( INV:IID )
       !STAI:Debit          += LOC:Credited               ! Less any Credit
       !Do not accoount Credit Notes for Debit anymore MIT:35-1826 (25.02.2011) by Victor

       STAI:Credit          = Get_ClientsPay_Alloc_Amt( INV:IID, 0 )

       !    0               1               2           3                   4               5
       ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
       IF INV:Status = 2 OR INV:Status = 5          ! Credit notes
          ! normally should not be here as we have excluded credit notes since 25.02.2011
          !STAI:Amount       = 0.0 !changed 17.03.2011 by Victor

          ! FBN wants the "Credit" amount to be in the Credit column and a + amount
          STAI:Credit       = - INV:Total
          STAI:Debit       -= INV:Total
       ELSE
          !STAI:Amount       = STAI:Debit - STAI:Credit
       END
       STAI:Amount       = STAI:Debit - STAI:Credit ! moved from Else 17.03.2011

       LOC:Amount           = STAI:Amount
       
       IF Access:_StatementItems.TryInsert() = LEVEL:Benign          
          DO Credit_Notes_for_IID ! Add all credit notes of the respective invoice if any (and add them to the Q so that it is not added here again)

    .  .
    EXIT
    
Credit_Notes_for_IID            ROUTINE
    View_InvA.Init(InvA_View, Relate:InvoiceAlias)
    View_InvA.AddSortOrder(A_INV:Key_CR_IID)
    View_InvA.AppendOrder('A_INV:InvoiceDate')
    View_InvA.AddRange(A_INV:CR_IID, INV:IID) ! NOT L_CQ:IID, L_CQ:IID is a credit note IID

    !       0           1               2              3                 4               5
    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
    !View_InvA.SetFilter('A_INV:InvoiceDate < ' & p:Date + 1 & ' AND A_INV:Status < 4')
    !Do not accoounted Credit Notes for Debit anymore MIT:35-1826 (25.02.2011) by Victor

    View_InvA.Reset()
    LOOP
       IF View_InvA.Next() ~= LEVEL:Benign
          BREAK
        .        

       ! Show it then after the invoice
       IF Access:_StatementItems.TryPrimeAutoInc() = LEVEL:Benign
          ! STAI:STIID
          STAI:STID            = STA:STID

          STAI:InvoiceDate     = A_INV:InvoiceDate
          STAI:IID             = A_INV:IID
          STAI:DID             = A_INV:DID
          STAI:DINo            = A_INV:DINo

          ! p:Option
          !   0.  Invoice Paid
          !   1.  Clients Payments Allocation total
          STAI:Debit           = A_INV:Total

          ! When Credit
          LOC:Credited         = Get_Inv_Credited( A_INV:IID )                      ! --------- should not be for credit notes
          !STAI:Debit          += LOC:Credited               ! Less any Credit
          !Do not accoounted Credit Notes for Debit anymore MIT:35-1826 (25.02.2011) by Victor

          STAI:Credit          = Get_ClientsPay_Alloc_Amt( A_INV:IID, 0 )           ! --------- should not be for credit notes
          !    0               1               2           3                   4               5
          ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
          !IF A_INV:Status = 2 ! unconditionally since 25.02.2011                    ! --------- should only be
             !STAI:Amount       = 0.0 !changed 17.03.2011

             ! FBN wants the "Credit" amount to be in the Credit column
             STAI:Credit      -= A_INV:Total
             STAI:Debit       -= A_INV:Total
             STAI:Amount       = STAI:Debit - STAI:Credit !calculate Amount for credit note (since 17.03.2011)
          !.
    db.debugout('[Gen_Statement]  Adding Credit note - STAI:STIID: ' & STAI:STIID)

          IF Access:_StatementItems.TryInsert() = LEVEL:Benign             
             LOC:Amount    += STAI:Amount              !include the Anount in Total (since 17.03.2011)
             L_CQ:IID       = STAI:IID
             ADD(LOC:CreditNotes_Added_Q)             

             ! Ivan - as we are not setting Credit Note - Shown above anymore, because Credit notes are filtered out before they
             !        get to that code, we must now do that here
             IF p:Run_Type < 2 AND p:Dont_Add = FALSE AND A_INV:Status = 2    ! Client & Statement Gen.
                Upd_Invoice_Paid_Status(A_INV:IID, 1)            ! Update Fully Paid - Credit to Fully Paid and Credit Note to Credit Note - Shown

!    Message('[Gen_Statement]  Setting shown status - A_INV:IID: ' & A_INV:IID)
             .                
          End
       End
    End

    View_InvA.Kill()
    EXIT
!!! <summary>
!!! Generated from procedure template - Window
!!! (p_Subject, p_Text, p_EmailAccount, p_HTML, p_Server, p_AuthUser, p_AuthPassword, p_From, p_To, p_CC, p_BCC, p_Helo, p_Attach, p_Port)
!!! </summary>
Send_Email PROCEDURE (STRING p_Subject,STRING p_Text,BYTE p_EmailAccount,<STRING p_HTML>,<STRING p_Server>,<STRING p_AuthUser>,<STRING p_AuthPassword>,<STRING p_From>,<STRING p_To>,<STRING p_CC>,<STRING p_BCC>,<STRING p_Helo>,<STRING p_Attach>,<ULONG p_Port>)

LOC:Progress         LONG                                  !
LOC:Result           STRING(500)                           !
QuickWindow          WINDOW('Send Email'),AT(,,183,81),FONT('MS Sans Serif',8,,FONT:regular),CENTER,GRAY,IMM,MDI, |
  HLP('Send_Email'),TIMER(50)
                       PROGRESS,AT(39,44,105,8),USE(?Progress1),RANGE(0,100)
                       BUTTON('&Cancel'),AT(130,63,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       STRING('String1'),AT(9,9,158),USE(?STRING1)
                       STRING('String1'),AT(9,23,158,10),USE(?STRING2)
                     END

MDISyncro MDISynchronization
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Local Data Classes
ThisEmail            CLASS(NetEmailSend)                   ! Generated by NetTalk Extension (Class Definition)
ErrorTrap              PROCEDURE(string errorStr,string functionName),DERIVED
MessageSent            PROCEDURE(),DERIVED

                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:Result)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Prepare_and_Send    ROUTINE
  DATA
r:name    STRING(6)

  CODE
    
    ! Send_Email
    IF p_EmailAccount = 0

       !            12             13


       ! (STRING p_Subject, STRING p_Text, BYTE p_EmailAccount=3, <STRING p_HTML>, <STRING p_Server>,
       !            1               2               3                       4               5
       ! <STRING p_AuthUser>, <STRING p_AuthPassword>, <STRING p_From>, <STRING p_To>, <STRING p_CC>, <STRING p_BCC>,
       !            6                       7                       8           9               10              11
       ! <STRING p_Helo>, <STRING p_Attach>, <ULONG p_Port>),LONG
       !            12             13           14
       !
       ! (STRING, STRING, BYTE=3, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>, <STRING>),LONG

       IF ~OMITTED(5)
          ThisEmail.Server      = p_Server
       .
       IF ~OMITTED(6)
          ThisEmail.AuthUser    = p_AuthUser
       .
       IF ~OMITTED(7)
          ThisEmail.AuthPassword = p_AuthPassword
       .
       IF ~OMITTED(8)
          ThisEmail.From        = p_From
       .
       IF ~OMITTED(9)
          ThisEmail.ToList      = p_To
          ?STRING1{PROP:Text} = 'To: ' & CLIP(p_To)
       .
       IF ~OMITTED(10)
          ThisEmail.CCList      = p_CC
       .
       IF ~OMITTED(11)
          ThisEmail.BCCList     = p_BCC
       .
       IF ~OMITTED(12)
          ThisEmail.Helo        = p_Helo
       .
       IF ~OMITTED(13)
          ThisEmail.AttachmentList  = p_Attach
       .
       IF ~OMITTED(14)
          ThisEmail.port        = p_Port
       .
    ELSE      
       ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip)
       ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>),STRING
       !   1       2       3           4           5

       ! ThisEmail.Server         = Get_Section_from_Settings(p_EmailAccount, 'Server', 'BC Domain Register SMTP Server')
       r:name = 'Email-'
      
       ThisEmail.Server         = Get_Setting(r:name & 'Server', 1, 'mail.fbn-transport.co.za',, 'FBN Mailserver')
       ThisEmail.port           = Get_Setting(r:name & 'Port', 1, '25',, 'FBN Mailserver port')
      
       ThisEmail.AuthUser       = Get_Setting(r:name & 'Account', 1, 'operations@fbn-transport.co.za', 'Email Account')
       ThisEmail.AuthPassword   = Get_Setting(r:name & 'Password', 1, '', 'Email Account Password')
       
       ThisEmail.From           = Get_Setting(r:name & 'From', 1, 'operations@fbn-transport.co.za', 'From Email address')
!       ThisEmail.ToList         = Get_Setting(r:name & 'To', 'BC Domain Register Co ZA address')
       ThisEmail.CCList         = Get_Setting(r:name & 'CC', 1,'', 'CC Email address')
       ThisEmail.BCCList        = Get_Setting(r:name & 'BCC', 1,'', 'BCC Email address')

       ThisEmail.Helo           = Get_Setting(r:name & 'Helo', 1,'', 'Helo string to pass server')
    .

    ThisEmail.Subject           = CLIP(p_Subject)
    ?STRING2{PROP:Text} = 'Subject: ' & CLIP(p_Subject)

    IF OMITTED(4)
       ThisEmail.SetRequiredMessageSize (0, |
                                         LEN(CLIP(p_Text)), 0)
    ELSE
       ThisEmail.SetRequiredMessageSize (0, |
                                         LEN(CLIP(p_Text)), LEN(CLIP(p_HTML)))
    .

    IF ThisEmail.Error = 0                                      ! Check for error
       ThisEmail.MessageText        = CLIP(p_Text)

       IF ~OMITTED(4)
          ThisEmail.MessageHtml     = CLIP(p_HTML)
       .

       ThisEmail.SendMail(NET:EMailMadeFromPartsMode)           ! Put email in queue and start sending it
    ELSE
       MESSAGE('Error creating email to send.||Error: ' & ThisEmail.Error, 'Email Error', ICON:Hand)
    .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Send_Email')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  SELF.Open(QuickWindow)                                   ! Open window
                                               ! Generated by NetTalk Extension (Start)
  ThisEmail.init()
  if ThisEmail.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  Do DefineListboxStyle
  !ProcedureTemplate = Window
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ThisEmail.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF MDISyncro.TakeEvent() THEN CYCLE.
    ThisEmail.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ?STRING1{PROP:Text} = ''
      ?STRING2{PROP:Text} = ''
          ?Progress1{PROP:Progress}   = 0
          ?Progress1{PROP:RangeHigh}  = 100
    OF EVENT:Timer
            IF LOC:Progress = 0
               LOC:Progress    = 10
            ELSE
               LOC:Progress    = LOC:Progress * 1.1
            .
            IF LOC:Progress >= 100
               LOC:Progress    = 0
            .
            ?Progress1{PROP:Progress}   = LOC:Progress
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          DO Prepare_and_Send
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


ThisEmail.ErrorTrap PROCEDURE(string errorStr,string functionName)


  CODE
      LOC:Result  = CLIP(errorStr) & ',  ' & SELF._ErrorStatus & ',' & SELF._AfterErrorAction
      POST(EVENT:CloseWindow)
      
      
      IF FALSE  
  PARENT.ErrorTrap(errorStr,functionName)
      .


ThisEmail.MessageSent PROCEDURE


  CODE
  PARENT.MessageSent
  LOC:Result  = ''
  POST(EVENT:CloseWindow)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_InvTotal_Weight  PROCEDURE  (p:IID, p:Inv_Tot, p:Inv_Weight) ! Declare Procedure
Tek_Failed_File     STRING(100)

sql_        SQLQueryClass

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_SQLTemp.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
     Access:_SQLTemp.UseFile()
     Access:_Invoice.UseFile()
  ! (p:IID, p:Inv_Tot, p:Inv_Weight)
  ! (STRING p:IID)
  
  ! Volumetric weight, we can do this in the query itself.... todo - later *********************


  !sql_.PropSQL('SELECT Total, Weight, VolumetricWeight
  sql_.Init(GLO:DBOwner, '_SQLTemp2')
    
  sql_.PropSQL('SELECT SUM(Total), SUM(Weight) FROM _Invoice WHERE IID IN (' & CLIP(p:IID) & ')')
  

!  'DISTINCT            LTRIM(DATEPART([year], INVOICEDATEANDTIME)) + <39>/<39> + LTRIM(DATEPART([month], INVOICEDATEANDTIME)) + ' & |
!                '<39>/<39> + LTRIM(DATEPART([day], INVOICEDATEANDTIME)) ' & |
!                'FROM _Invoice WHERE InvoiceDateAndTime >= ' & |
!               SQL_Get_DateT_G(LO:From_Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(LO:To_Date + 1,,1))

  IF sql_.Next_Q() >= 0
    ! Get the values... set them to parameters and pass the result back    
    p:Inv_Tot     = sql_.Get_El(1)
    p:Inv_Weight  = sql_.Get_El(2)
    
    ! message('Total 1: ' & sql_.Get_El(1))
    ! message('Weight 2: ' & sql_.Get_El(2))
  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:_SQLTemp.Close()
    Relate:_Invoice.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Delivery_TripSheets PROCEDURE  (p_DID)                 ! Declare Procedure
LOC:TRID_List        STRING(500)                           !
LOC_Q_TRID           QUEUE,PRE(L_)                         !
TRID                 ULONG                                 !Tripsheet ID
                     END                                   !
Tek_Failed_File     STRING(100)

TS_View   VIEW(TripSheetDeliveriesAlias)
    PROJECT(A_TRDI:TRID,A_TRDI:DIID,A_TRDI:UnitsLoaded)

    JOIN(A_TRI:PKey_TID, A_TRDI:TRID)
      PROJECT(A_TRI:VCID)
    .
    JOIN(A_DELI:PKey_DIID, A_TRDI:DIID)
      PROJECT(A_DELI:DID,A_DELI:ItemNo)
      JOIN(A_DEL:PKey_DID, A_DELI:DID)
        PROJECT(A_DEL:DINo)
      .      
    .    
.

  
TS_       ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:VehicleComposition.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetsAlias.Open()
     .
     Access:DeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
     Access:VehicleComposition.UseFile()
     Access:TripSheetDeliveriesAlias.UseFile()
     Access:TripSheetsAlias.UseFile()
    PUSHBIND()
    BIND('A_TRI:TRID ',A_TRI:TRID )

    TS_.Init(TS_View, Relate:DeliveryItemAlias)
    TS_.AddSortOrder(A_DELI:FKey_DID_ItemNo)
    TS_.AddRange(A_DELI:DID, p_DID)
    TS_.AppendOrder('A_TRI:DepartDate')

    TS_.SetFilter('A_TRI:TRID <> 0')                 ! Not interested when no TRID present

    TS_.Reset()
    IF ERRORCODE()
       MESSAGE('Error on Open View - Tripsheet View.||Error: ' & ERROR() & '|F Error: ' & FILEERROR(), 'Get_Delivery_TripSheets', ICON:Hand)
    ELSE
       LOOP
          IF TS_.Next() ~= LEVEL:Benign
             BREAK          
          .
          
          L_:TRID   = A_TRI:TRID 
          GET(LOC_Q_TRID, +L_:TRID)
          IF ERRORCODE()
            ADD(LOC_Q_TRID)
            Add_to_List(L_:TRID, LOC:TRID_List, ',')            
          .          
        .
    .

    UNBIND('A_TRI:TRID')
    POPBIND()

    ! a list of TRIDs in order of departure
    !RETURN(LOC:TRID_List) - in template
    
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:TRID_List)

    Exit

CloseFiles     Routine
    Relate:DeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Relate:VehicleComposition.Close()
    Relate:TripSheetDeliveriesAlias.Close()
    Relate:TripSheetsAlias.Close()
    Exit
