

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('FUNTRNIS009.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Delivery Item un-loaded units
!!! </summary>
Get_TripDelItemUnDel_Units PROCEDURE  (p:DIID, p:Option)   ! Declare Procedure
LOC:Units            ULONG                                 !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
     Access:DeliveryItemAlias.UseFile()
    !   p:Option    (from Get_TripItem_Del_Units)
    !       0. Units not = Delivered Units
    !       1. Units not on trip sheets or rejected = Delivered & Un Delivered on incomplete Trips
    !       2. Units not = Delivered & Rejected
    !       3. Units not on tripsheet or rejected = Delivered & Rejected & Un Delivered on incomplete Trips

    A_DELI:DIID     = p:DIID
    IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) = LEVEL:Benign
       LOC:Units    = A_DELI:Units - Get_TripItem_Del_Units(p:DIID, p:Option)
!       db.debugout('p:DIID: ' & p:DIID & ',  LOC:Units: ' & LOC:Units & ',  A_DELI:Units: ' & A_DELI:Units & ',  TripU: ' & Get_TripItem_Del_Units(p:DIID, p:Option))
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Units)

    Exit

CloseFiles     Routine
    Relate:DeliveryItemAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_DelMan_State     PROCEDURE  (p:DID, p:MID, p:DIIDs, p:Option) ! Declare Procedure
LOC:State_High       BYTE                                  !State of this manifest
LOC:MID_Listed       QUEUE,PRE(L_ML)                       !
MID                  ULONG                                 !Manifest ID
                     END                                   !
LOC:New_State        BYTE                                  !
Tek_Failed_File     STRING(100)

Items_View       VIEW(DeliveryItemAlias)
    PROJECT(A_DELI:DIID, A_DELI:DID, A_DELI:ItemNo)
       JOIN(A_MALD:FKey_DIID, A_DELI:DIID)
       PROJECT(A_MALD:MLID, A_MALD:DIID)
          JOIN(A_MAL:PKey_MLID, A_MALD:MLID)
          PROJECT(A_MAL:MLID, A_MAL:MID)
             JOIN(A_MAN:PKey_MID, A_MAL:MID)
             PROJECT(A_MAN:MID, A_MAN:BID, A_MAN:State)
    .  .  .  .
    

Item_VM         ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestAlias.Open()
     .
     Access:DeliveryItemAlias.UseFile()
     Access:ManifestLoadDeliveriesAlias.UseFile()
     Access:ManifestLoadAlias.UseFile()
     Access:ManifestAlias.UseFile()
    ! (p:DID, p:MID, p:DIIDs, p:Option)
    ! (ULONG, <*STRING>, <*STRING>, BYTE=0),BYTE
    !   when provided p:MID is a list of MIDs that are at the returned high state
    !   when provided p:DIIDs is a list of DIIDs that are at the returned high state
    !
    ! p:Option
    !   0   - Get highest state (original functionality)
    !   1   - Get lowest state

    BIND('A_MAL:MID',A_MAL:MID)

    Item_VM.Init(Items_View, Relate:DeliveryItemAlias)
    Item_VM.AddSortOrder(A_DELI:FKey_DID_ItemNo)
    Item_VM.AddRange(A_DELI:DID, p:DID)
    !Item_VM.AppendOrder('A_DELI:DID, A_DELI:ItemNo')

    IF p:Option = 0
       Item_VM.SetFilter('A_MAL:MID <> 0')                 ! Not interested when no MID present
    ELSE
       LOC:State_High    = 99
    .

    Item_VM.Reset()
    IF ERRORCODE()
       MESSAGE('Error on Open View - Items_View.||Error: ' & ERROR() & '|F Error: ' & FILEERROR(), 'Get_DelMan_State', ICON:Hand)
    ELSE
       LOOP
          IF Item_VM.Next() ~= LEVEL:Benign
             BREAK
          .
          IF p:Option = 1
             IF A_MAL:MID = 0               ! Not sure how this would be possible?
                LOC:State_High  = 0
                BREAK
          .  .

          LOC:New_State     = 0
          IF p:Option = 0          ! Getting the highest manifest states state
             IF LOC:State_High < A_MAN:State
                LOC:State_High  = A_MAN:State
                LOC:New_State   = TRUE
             .
          ELSE      ! p:Option = 1
             IF LOC:State_High > A_MAN:State
                LOC:State_High  = A_MAN:State
                LOC:New_State   = TRUE
          .  .

          IF LOC:New_State = TRUE
             ! Clear old high DIID lists then
             IF ~OMITTED(3)
                p:DIIDs         = ''
             .

             IF ~OMITTED(2)
                FREE(LOC:MID_Listed)
                p:MID           = ''
          .  .


          ! If we are on highest/lowest state record the DIIDs
          IF LOC:State_High = A_MAN:State
             IF ~OMITTED(2)
                L_ML:MID        = A_MAN:MID
                GET(LOC:MID_Listed, L_ML:MID)
                IF ERRORCODE()
                   IF CLIP(p:MID) = ''
                      p:MID        = A_MAN:MID
                   ELSE
                      p:MID        = CLIP(p:MID) & ',' & A_MAN:MID
                   .

                   L_ML:MID        = A_MAN:MID
                   ADD(LOC:MID_Listed)
             .  .

             IF ~OMITTED(3)
                IF CLIP(p:DIIDs) = ''
                   p:DIIDs      = A_DELI:DIID
                ELSE
                   p:DIIDs      = CLIP(p:DIIDs) & ',' & A_DELI:DIID
    .  .  .  .  .

    IF LOC:State_High = 99          ! We found no records so set state to 0
       LOC:State_High    = 0
    .

    Item_VM.Kill()
    UNBIND('A_MAL:MID')
    
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:State_High)

    Exit

CloseFiles     Routine
    Relate:DeliveryItemAlias.Close()
    Relate:ManifestLoadDeliveriesAlias.Close()
    Relate:ManifestLoadAlias.Close()
    Relate:ManifestAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_TripDelItems_Incomplete PROCEDURE  (p:TRID, p:DIIDs)   ! Declare Procedure
LOC:Ulong            ULONG                                 !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetDeliveriesAlias.Open()
     .
     Access:TripSheetDeliveriesAlias.UseFile()
    ! (p:TRID, p:DIIDs)

    CLEAR(TRDI:Record, -1)
    A_TRDI:TRID          = p:TRID
    SET(A_TRDI:FKey_TRID, A_TRDI:FKey_TRID)
    LOOP
       IF Access:TripSheetDeliveriesAlias.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF A_TRDI:TRID ~= p:TRID
          BREAK
       .
       IF A_TRDI:DIID = 0                             ! Empty Trip Delivery
          CYCLE
       .


       IF A_TRDI:DeliveredDate = 0 OR (A_TRDI:UnitsDelivered + A_TRDI:UnitsNotAccepted) ~= A_TRDI:UnitsLoaded
          LOC:Ulong    += 1

          IF CLIP(p:DIIDs) = ''
             p:DIIDs    = CLIP(A_TRDI:DIID)
          ELSE
             p:DIIDs    = CLIP(p:DIIDs) & ',' & CLIP(A_TRDI:DIID)
    .  .  .

    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Ulong)

    Exit

CloseFiles     Routine
    Relate:TripSheetDeliveriesAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Ask_Date_Range PROCEDURE (p:From, p:To, p:Option, p:Heading)

LOC:Options          GROUP,PRE(LO)                         !
From_Date            DATE                                  !
To_Date              DATE                                  !
                     END                                   !
LOC:Day1             STRING(20)                            !
LOC:Day2             STRING(20)                            !
QuickWindow          WINDOW('Specify Dates'),AT(,,213,160),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MDI,HLP('Print_Invoices_Window'),SYSTEM
                       SHEET,AT(4,4,205,135),USE(?Sheet1)
                         TAB('Options'),USE(?Tab1)
                           STRING(@s20),AT(145,24,60,10),USE(LOC:Day1),TRN
                           PROMPT('From Date:'),AT(13,24),USE(?From_Date:Prompt),TRN
                           SPIN(@d5b),AT(57,24,65,10),USE(LO:From_Date),RIGHT(1)
                           BUTTON('...'),AT(126,24,12,10),USE(?Calendar)
                           GROUP,AT(13,40,192,10),USE(?Group_To_Date)
                             PROMPT('To Date:'),AT(13,40),USE(?To_Date:Prompt),TRN
                             SPIN(@d5b),AT(57,40,65,10),USE(LO:To_Date),RIGHT(1)
                             BUTTON('...'),AT(126,40,12,10),USE(?Calendar:2)
                             STRING(@s20),AT(145,40,60,10),USE(LOC:Day2),TRN
                           END
                           PROMPT(''),AT(13,86,189,30),USE(?Prompt_Err),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                         END
                       END
                       BUTTON('&OK'),AT(108,142,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(160,142,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(8,142,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:Options)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Check_Parms             ROUTINE
    IF ~OMITTED(4)
       QuickWindow{PROP:Text}   = p:Heading
    .
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Ask_Date_Range')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Day1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Ask_Date_Range',QuickWindow)               ! Restore window settings from non-volatile store
      !(p:From, p:To)
      IF p:From ~= 0
         LO:From_Date     = p:From
      ELSE
         LO:From_Date     = GETINI('Get_Date_Range', 'From_Date', , GLO:Local_INI)
      .
  
      IF p:To ~= 0
         LO:To_Date       = p:To
      ELSE
         LO:To_Date       = GETINI('Get_Date_Range', 'To_Date', , GLO:Local_INI)
      .
  
      LOC:Day1    = Week_Day(LO:From_Date)
      LOC:Day2    = Week_Day(LO:To_Date)
  
      IF p:Option = 1
         HIDE(?Group_To_Date)
      .
      DO Check_Parms
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('Ask_Date_Range',QuickWindow)            ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Ok
          IF p:Option ~= 1
             ! Check that To is After From
             IF LO:From_Date > LO:To_Date
                ?Prompt_Err{PROP:Text}        = 'The From Date cannot be later than the To Date.'
      
                QuickWindow{PROP:AcceptAll}   = FALSE
                SELECT(?LO:From_Date)
                CYCLE
          .  .
          PUTINI('Get_Date_Range', 'From_Date', LO:From_Date, GLO:Local_INI)
          PUTINI('Get_Date_Range', 'To_Date', LO:To_Date, GLO:Local_INI)
       POST(EVENT:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LO:From_Date
          LOC:Day1    = Week_Day(LO:From_Date)
          LOC:Day2    = Week_Day(LO:To_Date)
          DISPLAY
    OF ?Calendar
      ThisWindow.Update()
          LOC:Day1    = Week_Day(LO:From_Date)
          LOC:Day2    = Week_Day(LO:To_Date)
          DISPLAY
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',LO:From_Date)
      IF Calendar5.Response = RequestCompleted THEN
      LO:From_Date=Calendar5.SelectedDate
      DISPLAY(?LO:From_Date)
      END
      ThisWindow.Reset(True)
    OF ?LO:To_Date
          LOC:Day1    = Week_Day(LO:From_Date)
          LOC:Day2    = Week_Day(LO:To_Date)
          DISPLAY
    OF ?Calendar:2
      ThisWindow.Update()
          LOC:Day1    = Week_Day(LO:From_Date)
          LOC:Day2    = Week_Day(LO:To_Date)
          DISPLAY
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',LO:To_Date)
      IF Calendar6.Response = RequestCompleted THEN
      LO:To_Date=Calendar6.SelectedDate
      DISPLAY(?LO:To_Date)
      END
      ThisWindow.Reset(True)
    OF ?Cancel
      ThisWindow.Update()
          CLEAR(LOC:Options)
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LO:From_Date
          LOC:Day1    = Week_Day(LO:From_Date)
          LOC:Day2    = Week_Day(LO:To_Date)
          DISPLAY
    OF ?LO:To_Date
          LOC:Day1    = Week_Day(LO:From_Date)
          LOC:Day2    = Week_Day(LO:To_Date)
          DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Ask_Rates_Check PROCEDURE 

LOC:Options          GROUP,PRE(L_O)                        !
Run_Option           BYTE(1)                               !Show rate variance or Show summary of all DI's without validating rates
Date_Option          BYTE                                  !Use the entry Created on date or the DI date
From_Date            LONG                                  !
To_Date              LONG                                  !
Show_Setup_Rates     BYTE                                  !Show setup rates
Show_When_DI_Rate    BYTE                                  !Show rates that are higher, lower or either with regard to the specified rates
IncludeNoRates       BYTE                                  !
IncludeZeroRates     BYTE                                  !
                     END                                   !
LOC:Day_From         STRING(10)                            !
LOC:Day_To           STRING(10)                            !
QuickWindow          WINDOW('Delivery Rates Check'),AT(,,282,228),FONT('Tahoma',8,,FONT:regular),RESIZE,ALRT(EscKey), |
  CENTER,GRAY,IMM,MDI,HLP('Ask_Rates_Check')
                       SHEET,AT(4,2,273,205),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           PROMPT('Run Option:'),AT(13,22),USE(?L_O:Run_Option:Prompt),TRN
                           LIST,AT(86,22,70,10),USE(L_O:Run_Option),DROP(5),FROM('Show All|#0|Show Variance|#1'),TIP('Show rate ' & |
  'variance or Show summary of all DI''s without validating rates')
                           PROMPT('Date Option:'),AT(13,42),USE(?Date_Option:Prompt),TRN
                           LIST,AT(86,42,70,10),USE(L_O:Date_Option),DROP(5),FROM('DI Date|#0|Created Date|#1|All Enties|#2'), |
  MSG('Use the entry Created on date or the DI date'),TIP('Use the entry Created on dat' & |
  'e or the DI date')
                           GROUP,AT(13,66,248,26),USE(?Group_DateGroup)
                             PROMPT('From Date:'),AT(13,66),USE(?From_Date:Prompt),TRN
                             SPIN(@d5b),AT(86,66,70,10),USE(L_O:From_Date),RIGHT(1)
                             BUTTON('...'),AT(178,65,12,10),USE(?Calendar)
                             STRING(@s10),AT(201,65),USE(LOC:Day_From),TRN
                             PROMPT('To Date:'),AT(13,82),USE(?To_Date:Prompt),TRN
                             SPIN(@d5b),AT(86,82,70,10),USE(L_O:To_Date),RIGHT(1)
                             BUTTON('...'),AT(178,81,12,10),USE(?Calendar:2)
                             STRING(@s10),AT(201,81),USE(LOC:Day_To),TRN
                           END
                           GROUP,AT(62,98,185,71),USE(?Group_Criteria)
                             CHECK(' Show Setup Rates'),AT(85,154),USE(L_O:Show_Setup_Rates),MSG('Show setup rates'),TIP('Show setup rates'), |
  TRN
                             PROMPT('Show When DI Rate:'),AT(13,104),USE(?L_O:Show_When_DI_Rate:Prompt:2)
                             COMBO(@n3),AT(86,105,,9),USE(L_O:Show_When_DI_Rate,,?L_O:Show_When_DI_Rate:3),RIGHT,OVR,DROP(5, |
  100),FROM('Higher and Lower|#0|Higher|#1|Lower|#2'),MSG('Show rates that are higher, ' & |
  'lower or either with regard to the specified rates'),TIP('Show rates that are higher' & |
  ', lower or either with regard to the specified rates')
                             CHECK(' Include No Rates'),AT(85,123),USE(L_O:IncludeNoRates),TIP('Check this on to inc' & |
  'lude DIs where no Rate was found')
                             CHECK(' Include Zero Rates'),AT(85,137),USE(L_O:IncludeZeroRates),TIP('Include records ' & |
  'where Client Rates are zero')
                           END
                           PROMPT('Setup rates are used when no rate exists for the client on the DI'),AT(85,173,163, |
  24),USE(?Prompt_SetupRates),TRN
                         END
                       END
                       BUTTON('&OK'),AT(173,209,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(226,209,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       BUTTON('&Help'),AT(3,210,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

Calendar5            CalendarClass
Calendar6            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:Options)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Ask_Rates_Check')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?L_O:Run_Option:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Clients.Open                                      ! File Clients used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Ask_Rates_Check',QuickWindow)              ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      !CLI:CID     = Get_Setup_Info(3)
      CLI:CID     = Get_Branch_Info(, 4)
      IF Access:Clients.TryFetch(CLI:PKey_CID) = LEVEL:Benign
         ?Prompt_SetupRates{PROP:Text}    = CLIP(?Prompt_SetupRates{PROP:Text}) & ' (these are defined under "client" ' & |
                                            CLIP(CLI:ClientName) & ')'
      ELSE
         ?Prompt_SetupRates{PROP:Text}    = 'No Setup rates are defined.'
      .
      L_O:Show_Setup_Rates    = GETINI('Ask_Rates_Check', 'Show_Setup_Rates', 0, GLO:Local_INI)
  
      L_O:Show_When_DI_Rate   = GETINI('Ask_Rates_Check', 'Show_When_DI_Rate', 1, GLO:Local_INI)
      ! Set the date range to the day before
      L_O:To_Date         = TODAY()
  
      Day_#   = TODAY() % 7
      CASE Day_#
      OF 6          ! Sat
         L_O:From_Date    = TODAY() - 1
      OF 0          ! Sun
         L_O:From_Date    = TODAY() - 2
      OF 1          ! Mon
         L_O:From_Date    = TODAY() - 3
         L_O:To_Date      = TODAY() - 1
      ELSE
         L_O:From_Date    = TODAY() - 1
         L_O:To_Date      = TODAY() - 1
      .
  
      
      LOC:Day_To    = Week_Day(L_O:To_Date)
      LOC:Day_From  = Week_Day(L_O:From_Date)
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Clients.Close
  END
  IF SELF.Opened
    INIMgr.Update('Ask_Rates_Check',QuickWindow)           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?L_O:Run_Option
          IF L_O:Run_Option = 0
             DISABLE(?Group_Criteria)
          ELSE
             ENABLE(?Group_Criteria)
          .
    OF ?L_O:Date_Option
          ! DI, Created, All
          IF L_O:Date_Option = 2
             DISABLE(?Group_DateGroup)
          ELSE
             ENABLE(?Group_DateGroup)
          .
    OF ?L_O:From_Date
          LOC:Day_From    = Week_Day(L_O:From_Date)
          DISPLAY(?L_O:From_Date)
    OF ?Calendar
      ThisWindow.Update()
          LOC:Day_From    = Week_Day(L_O:From_Date)
          DISPLAY(?L_O:From_Date)
      Calendar5.SelectOnClose = True
      Calendar5.Ask('Select a Date',L_O:From_Date)
      IF Calendar5.Response = RequestCompleted THEN
      L_O:From_Date=Calendar5.SelectedDate
      DISPLAY(?L_O:From_Date)
      END
      ThisWindow.Reset(True)
    OF ?L_O:To_Date
          LOC:Day_To    = Week_Day(L_O:To_Date)
          DISPLAY(?L_O:To_Date)
    OF ?Calendar:2
      ThisWindow.Update()
          LOC:Day_To    = Week_Day(L_O:To_Date)
          DISPLAY(?L_O:To_Date)
      Calendar6.SelectOnClose = True
      Calendar6.Ask('Select a Date',L_O:To_Date)
      IF Calendar6.Response = RequestCompleted THEN
      L_O:To_Date=Calendar6.SelectedDate
      DISPLAY(?L_O:To_Date)
      END
      ThisWindow.Reset(True)
    OF ?Ok
      ThisWindow.Update()
          PUTINI('Ask_Rates_Check', 'Show_Setup_Rates', L_O:Show_Setup_Rates, GLO:Local_INI)
      
          PUTINI('Ask_Rates_Check', 'Show_When_DI_Rate', L_O:Show_When_DI_Rate, GLO:Local_INI)
       POST(EVENT:CloseWindow)
    OF ?Cancel
      ThisWindow.Update()
          L_O:From_Date   = -1
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_O:Run_Option
          IF L_O:Run_Option = 0
             DISABLE(?Group_Criteria)
          ELSE
             ENABLE(?Group_Criteria)
          .
    OF ?L_O:Date_Option
          ! DI, Created, All
          IF L_O:Date_Option = 2
             DISABLE(?Group_DateGroup)
          ELSE
             ENABLE(?Group_DateGroup)
          .
    OF ?L_O:From_Date
          LOC:Day_From    = Week_Day(L_O:From_Date)
          DISPLAY(?L_O:From_Date)
    OF ?L_O:To_Date
          LOC:Day_To    = Week_Day(L_O:To_Date)
          DISPLAY(?L_O:To_Date)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
          L_O:From_Date   = -1
          POST(EVENT:CloseWindow)
    OF EVENT:CloseDown
          L_O:From_Date   = -1
          POST(EVENT:CloseWindow)
    OF EVENT:OpenWindow
          ! DI, Created, All
          IF L_O:Date_Option = 2
             DISABLE(?Group_DateGroup)
          ELSE
             ENABLE(?Group_DateGroup)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

