

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS025.INC'),ONCE        !Local module procedure declarations
                     END


PDFPrinterName      EQUATE('WinIT PDF Printer')
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
PrintDoc             PROCEDURE  (String DocName, string strPrinterName) ! Declare Procedure
cstrSaveActivePrinter  CSTRING(256)
WordObject           Long
ObjectStr            CString(128)
StatusWindow WINDOW('Printing...'),AT(,,274,25),FONT('MS Sans Serif',8,,FONT:regular),CENTER,GRAY, |
         DOUBLE
       PANEL,AT(1,1,271,22),USE(?Panel11),BEVEL(-1,1)
       PROMPT('Connecting to MS Word...'),AT(7,8,261,9),USE(?Status)
     END

  CODE
         if Clip(DocName) = '' then Return Level:Fatal.
         Open(StatusWindow)
         Display
         WordObject = Create(0,Create:OLE)          
         WordObject{prop:create}='Word.Application'
         if WordObject{'Application.Name'} = 'No ole automation interface'
            Message('No ole automation interface!|It seems that Microsoft Word is not Installed!',,Icon:Hand)
            Destroy(WordObject)
            Close(StatusWindow)
            Remove(clip(DocName))
            Return Level:Fatal
         End
         WordObject{'Application.Visible'} = False
         ?Status{Prop:Text} = 'Printing...'
         WordObject{'Application.Documents.Open("' & Clip(DocName) & '")'}
         cstrSaveActivePrinter = WordObject{'Application.ActivePrinter'}
         IF CLIP(strPrinterName) <> ''
           WordObject{'Application.ActivePrinter'} = '"' & CLIP(strPrinterName) & '"'
         END
         WordObject{'Application.ActiveDocument.PrintOut(0)'}
         WordObject{'Application.ActivePrinter'} = cstrSaveActivePrinter
         ?Status{Prop:Text} = 'Done! Unloading...'
         ObjectStr = WordObject{prop:object}
         WordObject{'Application.Visible'} = False
         WordObject{'Application.Quit(0)'}
         WordObject{Prop:Release}          = ObjectStr
         WordObject{Prop:Deactivate}       = True
         Destroy(WordObject)
         Close(StatusWindow)
         Return Level:Benign
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
PreparePDFPrinter    PROCEDURE  (STRING strPDFFileName,String pTitle) ! Declare Procedure

  CODE
 ! Without Save As dialog
   PUTREG(REG_CURRENT_USER, 'Software\'&PDFPrinterName, 'Title',  CLIP(pTitle))
   PUTREG(REG_CURRENT_USER, 'Software\'&PDFPrinterName, 'BypassSaveAs', '1')
   PUTREG(REG_CURRENT_USER, 'Software\'&PDFPrinterName, 'OutputFile',   CLIP(strPDFFileName))
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
RestorePDFPrinter    PROCEDURE                             ! Declare Procedure

  CODE
 ! Restore
   PUTREG(REG_CURRENT_USER, 'Software\'&PDFPrinterName, 'Title',  '')
   PUTREG(REG_CURRENT_USER, 'Software\'&PDFPrinterName, 'BypassSaveAs', '0')
   PUTREG(REG_CURRENT_USER, 'Software\'&PDFPrinterName, 'OutputFile',   'CW_DEFAULT.pdf')
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
WaitForFile          PROCEDURE  (STRING strTitle, STRING strFileName, LONG MaxTime) ! Declare Procedure
!ConvertProcess  BasicProcessType
lTimeIn  LONG

  CODE
  SetCursor(CURSOR:Wait)
  lTimeIn = CLOCK()
  !ConvertProcess.Init(1, CLIP(strTitle),,,,,1, 50, 0)
  LOOP
    IF EXISTS(CLIP(strFileName)) THEN
        lTimeIn=CLOCK()
        loop while (CLOCK()-lTimeIn)<300.
        BREAK
    end
    IF (CLOCK() - lTimeIn) > MaxTime THEN BREAK.

    M# = CLOCK() - lTimeIn
    !IF ((M#/300)-INT(M#/300))=0
    !  if ~ConvertProcess.LoopStep() then break.
    !END
  END
  SetCursor()
  lTimeIn = CLOCK()
  !ConvertProcess.Kill()
  IF EXISTS(CLIP(strFileName))
    RETURN Level:Benign
  ELSE
    RETURN Level:Fatal
  END
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
FileToPDF            PROCEDURE  (STRING strFileName, STRING strPDFFileName,String pTitle) ! Declare Procedure
cstrExt  CSTRING(256)
RetVal  LONG

  CODE
 RetVal = Level:Benign
 PreparePDFPrinter(strPDFFileName, pTitle)

 dot#=INSTRING('.',CLIP(strFileName),-1,LEN(CLIP(strFileName)))
 cstrExt = strFileName[dot#+1 : LEN(CLIP(strFileName))]
 CASE UPPER(cstrExt)
 OF 'DOC' OROF 'RTF'
   RetVal = PrintDoc(CLIP(strFileName),   PDFPrinterName)
 !OF 'XLS'
 !    RetVal = PrintExcel(CLIP(strFileName), 'WinIT PDF Printer', strPDFFileName, pTitle)
 ELSE
   RetVal = Level:Fatal
 END
 ! Wait for printing
   IF RetVal = Level:Benign
     IF WaitForFile('Merging...', CLIP(strPDFFileName), 1 * 60 * 100) <> Level:Benign! 1 min
       MESSAGE('File ' & CLIP(strFileName) & ' can not been included!', 'Error', Icon:Asterisk)
     END
   END
 RestorePDFPrinter()

 RETURN RetVal
