

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS017.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Audit_ManageProfit   PROCEDURE  (p:IID, p:DID, p:DINo, p:MID, p:TIN, p:Tran_Date, p:Amount, p:Option, p:BID, p:Broking, p:Info) ! Declare Procedure
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Audit_ManagementProfit.Open()
     .
     Access:Audit_ManagementProfit.UseFile()
    ! (p:IID, p:DID, p:DINo, p:MID, p:TIN, p:Tran_Date, p:Amount, p:Option, p:BID, p:Broking, p:Info)
    ! (ULONG=0, ULONG=0, ULONG=0, ULONG=0, ULONG=0, LONG, *DECIMAL, BYTE, LONG, LONG, <STRING>)
    !   1           2       3       4         5     6       7       8       9    10     11

    IF Access:Audit_ManagementProfit.PrimeRecord() = LEVEL:Benign
       MPA:IID          = p:IID
       MPA:DID          = p:DID
       MPA:DINo         = p:DINo
       MPA:MID          = p:MID
       MPA:TIN          = p:TIN
       MPA:Tran_Date    = p:Tran_Date
       MPA:Amount       = p:Amount
       MPA:BID          = p:BID
       MPA:Broking      = p:Broking

       MPA:Source       = p:Option          ! Standard|Summary|None

       IF ~OMITTED(11)
          MPA:Info      = p:Info
       .

       IF Access:Audit_ManagementProfit.TryInsert() = LEVEL:Benign
          !
       ELSE
          MESSAGE('p:IID: ' & p:IID & ',  MPA:Source: ' & MPA:Source)
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:Audit_ManagementProfit.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_DelMan_State_orig PROCEDURE  (p:DID, p:MID, p:DIIDs)   ! Declare Procedure
LOC:State_High       BYTE                                  !State of this manifest
LOC:MID_Listed       QUEUE,PRE(L_ML)                       !
MID                  ULONG                                 !Manifest ID
                     END                                   !
Tek_Failed_File     STRING(100)

Items_View       VIEW(DeliveryItemAlias)
    PROJECT(A_DELI:DIID, A_DELI:DID, A_DELI:ItemNo)
       JOIN(A_MALD:FKey_DIID, A_DELI:DIID)
       PROJECT(A_MALD:MLID, A_MALD:DIID)
          JOIN(A_MAL:PKey_MLID, A_MALD:MLID)
          PROJECT(A_MAL:MLID, A_MAL:MID)
             JOIN(A_MAN:PKey_MID, A_MAL:MID)
             PROJECT(A_MAN:MID, A_MAN:BID, A_MAN:State)
    .  .  .  .
    

Item_VM         ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestAlias.Open()
     .
     Access:DeliveryItemAlias.UseFile()
     Access:ManifestLoadDeliveriesAlias.UseFile()
     Access:ManifestLoadAlias.UseFile()
     Access:ManifestAlias.UseFile()
    ! (p:DID, p:MID, p:DIIDs)
    ! (ULONG, <*STRING>, <*STRING>),BYTE
    !   when provided p:MID is a list of MIDs that are at the returned high state
    !   when provided p:DIIDs is a list of DIIDs that are at the returned high state

    BIND('A_MAL:MID',A_MAL:MID)

    Item_VM.Init(Items_View, Relate:DeliveryItemAlias)
    Item_VM.AddSortOrder(A_DELI:FKey_DID_ItemNo)
    Item_VM.AddRange(A_DELI:DID, p:DID)
    !Item_VM.AppendOrder('A_DELI:DID, A_DELI:ItemNo')
    Item_VM.SetFilter('A_MAL:MID <> 0')                 ! Not interested when no MID present

    Item_VM.Reset()
    IF ERRORCODE()
       MESSAGE('Error on Open View - Items_View.||Error: ' & ERROR() & '|F Error: ' & FILEERROR(), 'Get_DelMan_State', ICON:Hand)
    ELSE
       LOOP
          IF Item_VM.Next() ~= LEVEL:Benign
             BREAK
          .
          !IF A_MAL:MID = 0                      
          !   CYCLE
          !.

          ! Getting the highest manifest states state
          IF LOC:State_High < A_MAN:State
             LOC:State_High     = A_MAN:State

             ! Clear old high DIID lists then
             IF ~OMITTED(3)
                p:DIIDs         = ''
             .

             IF ~OMITTED(2)
                FREE(LOC:MID_Listed)
                p:MID           = ''
          .  .

          ! If we are on highest state record the DIIDs
          IF LOC:State_High = A_MAN:State
             IF ~OMITTED(2)
                L_ML:MID        = A_MAN:MID
                GET(LOC:MID_Listed, L_ML:MID)
                IF ERRORCODE()
                   IF CLIP(p:MID) = ''
                      p:MID        = A_MAN:MID
                   ELSE
                      p:MID        = CLIP(p:MID) & ',' & A_MAN:MID
                   .

                   L_ML:MID        = A_MAN:MID
                   ADD(LOC:MID_Listed)
             .  .

             IF ~OMITTED(3)
                IF CLIP(p:DIIDs) = ''
                   p:DIIDs      = A_DELI:DIID
                ELSE
                   p:DIIDs      = CLIP(p:DIIDs) & ',' & A_DELI:DIID
    .  .  .  .  .

    Item_VM.Kill()
    UNBIND('A_MAL:MID')
    
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:State_High)

    Exit

CloseFiles     Routine
    Relate:DeliveryItemAlias.Close()
    Relate:ManifestLoadDeliveriesAlias.Close()
    Relate:ManifestLoadAlias.Close()
    Relate:ManifestAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! ** global **   needs work....
!!! </summary>
Get_User_Access      PROCEDURE  (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned) ! Declare Procedure
LOC:Return           LONG                                  !
LOC:Found            BYTE                                  !
LOC:Found_Area       BYTE                                  !
LOC:ASID             ULONG                                 !
Tek_Failed_File     STRING(100)

View_UA         VIEW(UsersAccesses)
       JOIN(APP:PKey_ASID, USAC:ASID)
    .  .

View_UAE         VIEW(UsersAccessesExtra)
       JOIN(APPSE:PKey_ASEID, USACE:ASEID)
    .  .


UA_View         ViewManager




View_UGA        VIEW(UsersGroups)
       JOIN(UGAC:FKey_UGID, USEBG:UGID)
          JOIN(APP:PKey_ASID, UGAC:ASID)
    .  .  .

View_UGAE        VIEW(UsersGroups)
       JOIN(UGACE:FKey_UGID, USEBG:UGID)
          JOIN(APPSE:PKey_ASEID, UGACE:ASEID)
    .  .  .


UG_View         ViewManager



  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:UsersAccesses.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ApplicationSections.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Application_Section_Usage.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ApplicationSections_Extras.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:UsersAccessesExtra.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:UsersGroups.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:UserGroupAccesses.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:UserGroupAccessesExtra.Open()
     .
     Access:UsersAccesses.UseFile()
     Access:ApplicationSections.UseFile()
     Access:Application_Section_Usage.UseFile()
     Access:ApplicationSections_Extras.UseFile()
     Access:UsersAccessesExtra.UseFile()
     Access:UsersGroups.UseFile()
     Access:UserGroupAccesses.UseFile()
     Access:UserGroupAccessesExtra.UseFile()
    start_#  = CLOCK()
    db.debugout('[Get_User_Access]  Start - App: ' & CLIP(p:AppSection) & ',  UID: ' & p:UID & ',  Proc: ' & CLIP(p:Procedure))

    PUSHBIND()
    BIND('APP:ApplicationSection', APP:ApplicationSection)
    BIND('APPSE:ExtraSection', APPSE:ExtraSection)
    ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned)
    ! (ULONG, STRING      , STRING     , <STRING>       , BYTE=0             , <*BYTE>                 ),LONG
    !   1       2               3               4               5                   6
    ! Returns
    !   0   - Allow (default)
    !   1   - Disable
    !   2   - Hide
    !   100 - Allow                 - Default action
    !   101 - Disallow              - Default action
    !
    !   Application Sections are added if not found
    !   Application Section Usage is logged         - not, only if not found user level????
    !
    ! p:Default_Action
    !   - applies to 1st Add only

    ! No Group or User actions present then default action taken
    !   Group action present then applied
    !      User action present then applied, overriding Group
    ! Check order, User specific, Group specific, default

    ! -----  Check App. Section Default  -----
    DO Check_App_Section                          ! Create, set default
    ! -----  Check App. Extra Section Default  -----
    DO Check_Extra_Section                        ! Create, set default, if Extra Section present


    ! -----  Check User actions  -----
    DO Check_User_App_Section

    IF LOC:Found = FALSE OR LOC:Return ~= 0      ! Not found, or we got a negative for it
       DO Check_User_Extra_Sections              ! Extra can override App Sections

       IF LOC:Found = FALSE                      ! Not found
          DO Check_UserGroups_Extra_Sections     ! User Group Extra can override UserGroups (NOTE: 13.10.14, these used to be other way around)

          IF LOC:Found = FALSE            
             DO Check_UserGroups                 ! App Sections (NOTE: 13.10.14, these used to be other way around)
    .  .  .

    ! 25/02/14, we are recording when application sections were used, not sure why.
    ! Removing this table from replication set now.  So data will be branch indepenant
    DO Upd_Section_Usage
            
            
! if p:AppSection = 'Manifests' AND ~OMITTED(4)
!    MESSAGE('2 return: ' & LOC:Return & '    found: ' & LOC:Found & '    found area@ ' & LOC:Found_Area, p:AppSection & ' - ' & p:Extra_Section)
! .
            
    UNBIND('APP:ApplicationSection')
    UNBIND('APPSE:ExtraSection')
    POPBIND()
                                                                                                          
    IF ~OMITTED(6)
       ! If user proc wants this status then return is, Found indicates actual rule, not default
       IF LOC:Found = 0
          p:DefaultAction_Returned  = 1
    .  .

    db.debugout('[Get_User_Access]  Complete - App: ' & CLIP(p:AppSection) & ',  UID: ' & p:UID & ',  Found_Area: ' & LOC:Found_Area & ',  Return: ' & LOC:Return & ',  Proc: ' & CLIP(p:Procedure) & ', Extra: ' & CLIP(p:Extra_Section) & ',  Time: ' & CLOCK() - start_#)
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:UsersAccesses.Close()
    Relate:ApplicationSections.Close()
    Relate:Application_Section_Usage.Close()
    Relate:ApplicationSections_Extras.Close()
    Relate:UsersAccessesExtra.Close()
    Relate:UsersGroups.Close()
    Relate:UserGroupAccesses.Close()
    Relate:UserGroupAccessesExtra.Close()
    Exit
Check_App_Section                     ROUTINE
    APP:ApplicationSection   = p:AppSection
    IF Access:ApplicationSections.TryFetch(APP:Key_ApplicationSection) = LEVEL:Benign
       LOC:ASID = APP:ASID   
    ELSE  
       IF Access:ApplicationSections.TryPrimeAutoInc() = LEVEL:Benign
          !APP:ASID
          APP:ApplicationSection     = p:AppSection
          APP:DefaultAction          = p:SetDefault_Action
          IF Access:ApplicationSections.TryInsert() = LEVEL:Benign
             LOC:ASID = APP:ASID
          ELSE
             Access:ApplicationSections.CancelAutoInc()
             CLEAR(APP:RECORD)
    .  .  .

    LOC:Return          = APP:DefaultAction
    EXIT
Check_Extra_Section                   ROUTINE
    IF ~OMITTED(4)                      ! Extra Section
       APPSE:ASID           = LOC:ASID
       APPSE:ExtraSection   = p:Extra_Section

       IF CLIP(APPSE:ExtraSection) ~= CLIP(p:Extra_Section)
          MESSAGE('The application extra section specified cannot be used.|Please report this to the developer.||Extra Section: ' & CLIP(p:Extra_Section) & '|APPSE:ExtraSection: ' & |
                        CLIP(APPSE:ExtraSection), 'Get_User_Access - Get_User_Section', ICON:Hand)
       .

       IF Access:ApplicationSections_Extras.TryFetch(APPSE:SKey_ASID_ExtraSection) ~= LEVEL:Benign
          LOOP
             CLEAR(APPSE:Record)
             IF Access:ApplicationSections_Extras.TryPrimeAutoInc() = LEVEL:Benign
                ! APPSE:ASEID
                APPSE:ASID             = LOC:ASID
                APPSE:ExtraSection     = p:Extra_Section
                APPSE:DefaultAction    = p:SetDefault_Action
                IF Access:ApplicationSections_Extras.Insert() ~= LEVEL:Benign
                   !MESSAGE('APPSE:ASEID: ' & APPSE:ASEID & '|APPSE:ASID: ' & APPSE:ASID & '|APPSE:ExtraSection: ' & CLIP(APPSE:ExtraSection) & '|APPSE:DefaultAction: ' & CLIP(APPSE:DefaultAction), 'Failed to Insert')
                   Access:ApplicationSections_Extras.CancelAutoInc()
                .
             ELSE
                ! Prime failed, probably bad record
                CLEAR(APPSE:Record)
                APPSE:ASID     = 0
                IF Access:ApplicationSections_Extras.TryFetch(APPSE:FKey_ASID) = LEVEL:Benign
                   ! Probably a problem record... remove
                   IF Relate:ApplicationSections_Extras.Delete(0) = LEVEL:Benign
                      CYCLE
             .  .  .
             BREAK
       .  .

      LOC:Return           = APPSE:DefaultAction
    .
    EXIT
    ! -------------------------------------------------------------------------------------
Check_User_App_Section           ROUTINE
    UA_View.Init(View_UA, Relate:UsersAccesses)
    UA_View.AddSortOrder(USAC:FKey_UID)
    !UA_View.AppendOrder()
    UA_View.AddRange(USAC:UID, p:UID)
    UA_View.SetFilter('APP:ApplicationSection = <39>' & CLIP(p:AppSection) & '<39>')

    UA_View.Reset()
    LOOP
       IF UA_View.Next() ~= LEVEL:Benign
          BREAK
       .

       LOC:Return       = USAC:AccessOption     ! Allow, View, Disallow
       LOC:Found        = TRUE
       LOC:Found_Area   = 1

       BREAK
    .
    UA_View.Kill()
    EXIT
Check_User_Extra_Sections       ROUTINE
    IF ~OMITTED(4)                      ! Extra Section
       UA_View.Init(View_UAE, Relate:UsersAccessesExtra)
       UA_View.AddSortOrder(USACE:FKey_UID)
       !UA_View.AppendOrder()
       UA_View.AddRange(USACE:UID, p:UID)
       UA_View.SetFilter('APPSE:ExtraSection = <39>' & CLIP(p:Extra_Section) & '<39>')

       UA_View.Reset()
       LOOP
          IF UA_View.Next() ~= LEVEL:Benign
             BREAK
          .

          LOC:Return       = USACE:AccessOption     ! Allow, View, Disallow
          LOC:Found        = TRUE
          LOC:Found_Area   = 3

          BREAK
       .
       UA_View.Kill()
    .
    EXIT
    ! -------------------------------------------------------------------------------------
Check_UserGroups                      ROUTINE
    !View_UGA        VIEW(UsersGroups)
    !       JOIN(UGAC:FKey_UGID, USEBG:UGID)
    !          JOIN(APP:PKey_ASID, UGAC:ASID)
    !    .  .  .

    UG_View.Init(View_UGA, Relate:UsersGroups)
    UG_View.AddSortOrder(USEBG:Key_UID)
    UG_View.AddRange(USEBG:UID, p:UID)
    UG_View.SetFilter('APP:ApplicationSection = <39>' & CLIP(p:AppSection) & '<39>')

    UG_View.Reset()
    LOOP
       IF UG_View.Next() ~= LEVEL:Benign
          BREAK
       .

       ! We have this Application Section for this User in a Group
       LOC:Return       = UGAC:AccessOption         ! Allow, View, Disallow
       LOC:Found        = TRUE
       LOC:Found_Area   = 2

       BREAK
    .
    UG_View.Kill()
    EXIT
Check_UserGroups_Extra_Sections         ROUTINE
    IF ~OMITTED(4)                      ! Extra Section
       UG_View.Init(View_UGAE, Relate:UsersGroups)
       UG_View.AddSortOrder(USEBG:Key_UID)
       !UG_View.AppendOrder()
       UG_View.AddRange(USEBG:UID, p:UID)
       UG_View.SetFilter('APPSE:ExtraSection = <39>' & CLIP(p:Extra_Section) & '<39>')

       UG_View.Reset()
       LOOP
          IF UG_View.Next() ~= LEVEL:Benign
             BREAK
          .

          LOC:Return       = UGACE:AccessOption     ! Allow, View, Disallow
          LOC:Found        = TRUE
          LOC:Found_Area   = 4

          BREAK
       .
       UG_View.Kill()
    .
    EXIT
    ! -------------------------------------------------------------------------------------
Upd_Section_Usage                     ROUTINE
    APPSU:ASID                  = LOC:ASID
    APPSU:ProcedureName         = p:Procedure
    IF Access:Application_Section_Usage.Fetch(APPSU:FKey_ASID_ProcedureName) = LEVEL:Benign
       ! Ok then
       APPSU:LastCalledDate  = TODAY()
       APPSU:LastCalledTime  = CLOCK()
       IF Access:Application_Section_Usage.TryUpdate() = LEVEL:Benign
       .
    ELSE
       IF LOC:ASID = 0
          MESSAGE('Application Section ID not set.|Procedure:' & CLIP(p:Procedure) & '||ASID = 0', 'Get_User_Access', ICON:Exclamation)
       ELSE
          IF Access:Application_Section_Usage.PrimeRecord() ~= LEVEL:Benign
          ELSE
             !APPSU:ASUID
             APPSU:ASID            = LOC:ASID
             APPSU:ProcedureName   = p:Procedure
             APPSU:LastCalledDate  = TODAY()
             APPSU:LastCalledTime  = CLOCK()
             IF Access:Application_Section_Usage.Insert() ~= LEVEL:Benign
                MESSAGE('Application Section Usage not added, Insert failed.||Procedure:' & CLIP(p:Procedure) & '|ASID: ' & LOC:ASID, 'Get_User_Access', ICON:Exclamation)
                Access:Application_Section_Usage.CancelAutoInc()
    .  .  .  .

    EXIT
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_TruckTrailer_Info PROCEDURE  (p:TTID, p:Type, p:Effective_Date, p:LicenseExpiryDate) ! Declare Procedure
LOC:Return           STRING(255)                           !
LOC:Expiry_Days      LONG                                  !
LOC:Msg_Type         BYTE                                  !
LOC:Rem_Zero         BYTE                                  !
LOC:Got_Truck        BYTE                                  !
LOC:LicenseExpiryDate LONG                                 !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:VehicleMakeModel.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TruckTrailerAlias.Open()
     .
     Access:VehicleMakeModel.UseFile()
     Access:TruckTrailerAlias.UseFile()
   ! (p:TTID, p:Type, p:Effective_Date, p:LicenseExpiryDate)
   ! (ULONG, BYTE=0, LONG=0, <*LONG>),STRING
   !
   ! p:Type    0 - Expiry - Message if less than Settings expiry, otherwise nothing
   !           1 - Expiry - CSV info if less than Settings expiry, otherwise nothing
   !           2 - Expiry - Message
   !           3 - Expiry - CSV info
   !           4 - LicenseInfo
   !           5 - Make & Model
   !           6 - Registration
   !           7 - Capacity
   !
   ! p:Effective_Date
   !           0 - Today
   !           x - Passed date
   !
   ! [Usage]

   CASE p:Type
   OF 0 OROF 2
      LOC:Msg_Type = 1
   OF 1 OROF 3
      LOC:Msg_Type = 2       
   .

   CLEAR(A_TRU:Record)
   A_TRU:TTID           = p:TTID
   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
      LOC:Got_Truck     = TRUE  
   .
   
   IF p:Type < 5
      IF p:Effective_Date = 0
         p:Effective_Date = TODAY()
      .

      LOC:Return          = '<Not found>'         ! TTID not found
      LOC:LicenseExpiryDate = -1

      ! (p:Setting, p:Create)
      LOC:Expiry_Days     = DEFORMAT(Get_Setting('Vehicle License Expiry - Notice Days', TRUE, '30', '@n_5', 'No of days within which to remind users of License Expiry.'))
      IF UPPER(CLIP(Get_Setting('Vehicle License Expiry - Remind Blank License Dates', TRUE, 'No', '@s3', 'Yes/No'))) ~= 'NO'
         LOC:Rem_Zero     = TRUE
      .

      IF LOC:Got_Truck = TRUE
         LOC:Return           = ''
         LOC:LicenseExpiryDate  = A_TRU:LicenseExpiryDate

         IF LOC:Rem_Zero = TRUE OR A_TRU:LicenseExpiryDate ~= 0
            CASE p:Type
            OF 0 OROF 1
               IF LOC:Expiry_Days > A_TRU:LicenseExpiryDate - p:Effective_Date
                  DO Exp_Message
               .
            OF 2 OROF 3
               DO Exp_Message
            OF 4
               LOC:Return    = A_TRU:LicenseInfo
            ELSE
               MESSAGE('Option it not supported.||Option (Type): ' & p:Type, 'Get_TruckTrailer_Info', ICON:Hand)
      .  .  .
   ELSE
      !           5 - Make & Model
      !           6 - Registration
      !           7 - Capacity
      LOC:Return  = ''               

      CASE p:Type         
      OF 5
         VMM:VMMID      = A_TRU:VMMID
         IF Access:VehicleMakeModel.TryFetch(VMM:PKey_VMMID) = LEVEL:Benign
            LOC:Return  = VMM:MakeModel
         .
      OF 6
         LOC:Return     = A_TRU:Registration
      OF 7
         LOC:Return     = A_TRU:Capacity         
   .  .
            
   IF ~OMITTED(4)
      p:LicenseExpiryDate = LOC:LicenseExpiryDate     
   .
            
         
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles
    RETURN(CLIP(LOC:Return))

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:VehicleMakeModel.Close()
    Relate:TruckTrailerAlias.Close()
    Exit
Exp_Message                             ROUTINE
    EXECUTE LOC:Msg_Type
       LOC:Return   = CLIP(A_TRU:Registration) & ' - License expires in ' & A_TRU:LicenseExpiryDate - p:Effective_Date  & ' days on the ' & FORMAT(A_TRU:LicenseExpiryDate,@d5)
       LOC:Return   = CLIP(A_TRU:Registration) & ',' & A_TRU:LicenseExpiryDate - p:Effective_Date & ',' & FORMAT(A_TRU:LicenseExpiryDate,@d5)
    .

    EXIT
!!! <summary>
!!! Generated from procedure template - Source
!!! (p:Setting, p:CreateUpdate, p:DefUpdValue, p:Picture, p:Tip)
!!! </summary>
Get_Setting          PROCEDURE  (p:Setting, p:CreateUpdate, p:DefUpdValue, p:Picture, p:Tip) ! Declare Procedure

  CODE
    ! Get_Setting
    ! was (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip)
    ! (p:Setting, p:CreateUpdate, p:DefUpdValue, p:Picture, p:Tip)
    ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>),STRING
    !   1       2       3           4           5
    ! 
    ! p:CreateUpdate
    !   1 - create only if none
    !   2 - create if none or update if exists

    SETI:Setting            = p:Setting
    IF Access:Settings.TryFetch(SETI:Key_Setting) = LEVEL:Benign
       ! SETI:Value - returned always
       IF p:CreateUpdate = 2
          IF ~OMITTED(3)
             SETI:Value     = p:DefUpdValue
          .
          IF ~OMITTED(4)
             SETI:Picture   = p:Picture
          .
          IF ~OMITTED(5)
             SETI:Tip       = p:Tip
          .

          IF Access:Settings.TryUpdate() ~= LEVEL:Benign
             CLEAR(SETI:Record)
       .  .
    ELSIF p:CreateUpdate > 0
       CLEAR(SETI:Record)
       SETI:Setting         = p:Setting

       IF Access:Settings.TryPrimeAutoInc() = LEVEL:Benign
          SETI:Setting      = p:Setting

          IF ~OMITTED(3)
             SETI:Value     = p:DefUpdValue
          .
          IF ~OMITTED(4)
             SETI:Picture   = p:Picture
          .
          IF ~OMITTED(5)
             SETI:Tip       = p:Tip
          .

          IF Access:Settings.TryInsert() ~= LEVEL:Benign
             Access:Settings.CancelAutoInc()
             CLEAR(SETI:Record)
          .
       ELSE
          CLEAR(SETI:Record)
       .
    ELSE
       CLEAR(SETI:Record)
    .

    RETURN(SETI:Value)
