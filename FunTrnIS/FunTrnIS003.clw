

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('FUNTRNIS003.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_ManLoadItems_Info PROCEDURE  (p:MLID, p:Option, p:DID, p:DIID) ! Declare Procedure
LOC:Returned         STRING(50)                            !
LOC:Decimal          DECIMAL(20,6)                         !
LOC:Idx              LONG                                  !
LOC:Deliveries_Q     QUEUE,PRE(L_DQ)                       !
DID                  ULONG                                 !Delivery ID
                     END                                   !
Tek_Failed_File     STRING(100)

ManLoad_View            VIEW(ManifestLoadDeliveriesAlias)
       JOIN(A_DELI:PKey_DIID, A_MALD:DIID)
       PROJECT(A_DELI:DIID, A_DELI:DID, A_DELI:Weight, A_DELI:Units)
    .  .





View_ManLoad            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Deliveries.Open()
     .
     Access:DeliveryItemAlias.UseFile()
     Access:ManifestLoadDeliveriesAlias.UseFile()
     Access:Deliveries.UseFile()
    ! (p:MLID, p:Option, p:DID, p:DIID)
    ! (ULONG, BYTE=0, <ULONG>, <ULONG>), STRING
    ! p:Option
    !   0. Weight (real - not volumetric)
    !   1. Units
    !       if p:DID is provided then the result is limited to the DID passed
    !       if p:DIID is provided then results are limited to the DIID passed
    !   2. No. of Deliveries on the Manifest Load
    !   3. Total Deliveries charge on the Manifest Load

    PUSHBIND
    IF ~OMITTED(4)
       BIND('A_DELI:DIID',A_DELI:DIID)
    .

    View_ManLoad.Init(ManLoad_View, Relate:ManifestLoadDeliveriesAlias)
    View_ManLoad.AddSortOrder(A_MALD:FSKey_MLID_DIID)
    View_ManLoad.AddRange(A_MALD:MLID, p:MLID)

    !View_ManLoad.AppendOrder()
    IF ~OMITTED(4)
       View_ManLoad.SetFilter('A_DELI:DIID=' & p:DIID)
    .

    View_ManLoad.Reset()
    IF ERRORCODE()
       MESSAGE('Error on view.|Error: ' & ERROR(), 'Get_ManLoadItems_Info', ICON:Hand)
    .
    LOOP
       IF View_ManLoad.Next() ~= LEVEL:Benign
          BREAK
       .

       CASE p:Option
       OF 0
          IF A_DELI:DIID = 0
             CLEAR(A_DELI:Record)
             MESSAGE('DeliveryItems (alias) could not be fetched.||A_MALD:DIID: ' & A_MALD:DIID & '|A_MALD:MLID: ' & A_MALD:MLID, 'Get_ManItems_Weight', ICON:Hand)
          .

          IF OMITTED(3)
             LOC:Decimal    += A_DELI:Weight           * (A_MALD:UnitsLoaded / A_DELI:Units)
          ELSE
             IF A_DELI:DID = p:DID
                LOC:Decimal += A_DELI:Weight           * (A_MALD:UnitsLoaded / A_DELI:Units)
          .  .
       OF 1
          IF OMITTED(3)
             LOC:Decimal    += A_MALD:UnitsLoaded
          ELSE
             IF A_DELI:DIID = 0
                CLEAR(A_DELI:Record)
                MESSAGE('DeliveryItems (alias) could not be fetched.||A_MALD:DIID: ' & A_MALD:DIID & '|A_MALD:MLID: ' & A_MALD:MLID, 'Get_ManItems_Weight', ICON:Hand)
             .

             IF A_DELI:DID = p:DID
                LOC:Decimal += A_MALD:UnitsLoaded        ! A_DELI:Units previously?
          .  .

!   db.debugout('[Get_ManLoadItems_Info]  Decimal: ' & LOC:Decimal & ',  MLID: ' & p:MLID & ',  DID: ' & p:DID & ',  Option: ' & p:Option & ',  A_MALD:UnitsLoaded: ' & A_MALD:UnitsLoaded)

       OF 2 OROF 3
          L_DQ:DID      = A_DELI:DID
          GET(LOC:Deliveries_Q, L_DQ:DID)
          IF ERRORCODE()
             L_DQ:DID   = A_DELI:DID
             ADD(LOC:Deliveries_Q)
          .
       ELSE
          MESSAGE('Unknown Option: ' & p:Option, 'Get_ManItems_Info', ICON:Hand)
    .  .

    View_ManLoad.Kill()
    POPBIND

    CASE p:Option
    OF 2
       LOC:Decimal  = RECORDS(LOC:Deliveries_Q)
       FREE(LOC:Deliveries_Q)
    OF 3
       LOC:Idx      = 0
       LOOP
          LOC:Idx  += 1
          GET(LOC:Deliveries_Q, LOC:Idx)
          IF ERRORCODE()
             BREAK
          .

          DEL:DID           = L_DQ:DID
          IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
             LOC:Decimal   += (DEL:DocumentCharge + DEL:FuelSurcharge + DEL:TollCharge + DEL:Charge + DEL:AdditionalCharge) * 1 + (DEL:VATRate / 100)
    .  .  .


!   db.debugout('[Get_ManLoadItems_Info]  Complete - Decimal: ' & LOC:Decimal & ',  MLID: ' & p:MLID & ',  DID: ' & p:DID & ',  Option: ' & p:Option)

    LOC:Returned        = LOC:Decimal
    
!               old
!    ! (p:MLID, p:Option, p:DID)
!    ! (ULONG, BYTE=0, <ULONG>),ULONG
!    ! p:Option
!    !   0. Weight (real - not volumetric)
!    !   1. Units
!    !       if p:DID is provided then the result is limited to the DID passed
!    
!    View_ManLoad.Init(ManLoad_View, Relate:ManifestLoadDeliveriesAlias)
!    View_ManLoad.AddSortOrder(A_MALD:FSKey_MLID_DIID)
!    !View_ManLoad.AppendOrder()
!    View_ManLoad.AddRange(A_MALD:MLID, p:MLID)
!    !View_ManLoad.SetFilter()
!
!    View_ManLoad.Reset()
!    LOOP
!       IF View_ManLoad.Next() ~= LEVEL:Benign
!          BREAK
!       .
!
!       CASE p:Option
!       OF 0
!          A_DELI:DIID  = A_MALD:DIID
!          IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) ~= LEVEL:Benign
!             MESSAGE('DeliveryItems (alias) could not be fetched.||A_MALD:DIID: ' & A_MALD:DIID & '|A_MALD:MLID: ' & A_MALD:MLID, 'Get_ManItems_Weight', ICON:Hand)
!          .
!
!          IF OMITTED(3)
!!             IF A_DELI:Weight >= A_DELI:VolumetricWeight
!                LOC:Weight_As_Ulong   += A_DELI:Weight              * (A_MALD:UnitsLoaded / A_DELI:Units)  * 100
!!             ELSE
!!                LOC:Weight_As_Ulong   += A_DELI:VolumetricWeight    * (A_MALD:UnitsLoaded / A_DELI:Units)  * 100
!!             .
!          ELSE
!             IF A_DELI:DID = p:DID
!!                IF A_DELI:Weight >= A_DELI:VolumetricWeight
!                   LOC:Weight_As_Ulong   += A_DELI:Weight           * (A_MALD:UnitsLoaded / A_DELI:Units)  * 100
!!                ELSE
!!                   LOC:Weight_As_Ulong   += A_DELI:VolumetricWeight * (A_MALD:UnitsLoaded / A_DELI:Units)  * 100
!          .  .  !.
!       OF 1
!          IF OMITTED(3)
!             LOC:Weight_As_Ulong   += A_MALD:UnitsLoaded
!          ELSE
!             A_DELI:DIID            = A_MALD:DIID
!             IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) ~= LEVEL:Benign
!                CLEAR(A_DELI:Record)
!                MESSAGE('DeliveryItems (alias) could not be fetched.||A_MALD:DIID: ' & A_MALD:DIID & '|A_MALD:MLID: ' & A_MALD:MLID, 'Get_ManItems_Weight', ICON:Hand)
!             .
!             IF A_DELI:DID = p:DID
!                LOC:Weight_As_Ulong    += A_MALD:UnitsLoaded        ! A_DELI:Units previously?
!          .  .
!       ELSE
!          MESSAGE('Unknown Option: ' & p:Option, 'Get_ManItems_Info', ICON:Hand)
!    .  .
!
!    View_ManLoad.Kill()
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Returned)

    Exit

CloseFiles     Routine
    Relate:DeliveryItemAlias.Close()
    Relate:ManifestLoadDeliveriesAlias.Close()
    Relate:Deliveries.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Ask_ManifestLoad PROCEDURE (p:MID)

LOC:Remaining        DECIMAL(6)                            !In Kgs
LOC:Selected         ULONG                                 !Manifest Load ID
LOC:MID              ULONG                                 !Manifest ID
BRW1::View:Browse    VIEW(ManifestLoad)
                       PROJECT(MAL:MLID)
                       PROJECT(MAL:MID)
                       PROJECT(MAL:TTID)
                       JOIN(TRU:PKey_TTID,MAL:TTID)
                         PROJECT(TRU:Registration)
                         PROJECT(TRU:Capacity)
                         PROJECT(TRU:TTID)
                         PROJECT(TRU:VMMID)
                         JOIN(VMM:PKey_VMMID,TRU:VMMID)
                           PROJECT(VMM:MakeModel)
                           PROJECT(VMM:VMMID)
                         END
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
VMM:MakeModel          LIKE(VMM:MakeModel)            !List box control field - type derived from field
TRU:Registration       LIKE(TRU:Registration)         !List box control field - type derived from field
TRU:Capacity           LIKE(TRU:Capacity)             !List box control field - type derived from field
LOC:Remaining          LIKE(LOC:Remaining)            !List box control field - type derived from local data
MAL:MLID               LIKE(MAL:MLID)                 !Primary key field - type derived from field
MAL:MID                LIKE(MAL:MID)                  !Browse key field - type derived from field
TRU:TTID               LIKE(TRU:TTID)                 !Related join file key field - type derived from field
VMM:VMMID              LIKE(VMM:VMMID)                !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
Window               WINDOW('Manifest Loads'),AT(,,221,162),FONT('Tahoma',8,,FONT:regular),DOUBLE,GRAY,MDI,SYSTEM
                       PROMPT('There are no other vehicles to Load.  Please select one of the Vehicles below w' & |
  'hich already has some items loaded.'),AT(8,2,205,30),USE(?Prompt2),FONT(,,COLOR:Black,FONT:bold, |
  CHARSET:ANSI),TRN
                       STRING(@n_10),AT(177,36,41,10),USE(LOC:MID),RIGHT(1)
                       SHEET,AT(4,34,214,109),USE(?Sheet1)
                         TAB('Manifest Load Trucks && Trailers'),USE(?Tab1)
                           BUTTON('&Select'),AT(161,124,,14),USE(?Select),LEFT,ICON('WASELECT.ICO'),FLAT
                         END
                       END
                       LIST,AT(8,50,205,70),USE(?List),VSCROLL,FORMAT('66L(2)|M~Make & Model~@s35@46L(2)|M~Reg' & |
  'istration~@s20@38D(2)|M~Capacity~L@n-8.0@38D(2)|M~Loaded~L@n-8.0@'),FROM(Queue:Browse), |
  IMM,MSG('Browsing Records')
                       PROMPT(''),AT(4,148,153,10),USE(?Prompt_VehComp)
                       BUTTON('&Close'),AT(165,146,,14),USE(?CancelButton),LEFT,ICON('WACLOSE.ICO'),FLAT
                     END

BRW1::LastSortOrder       BYTE
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop
  RETURN(LOC:Selected)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Ask_ManifestLoad')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Remaining',LOC:Remaining)             ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  Relate:Manifest.Open                            ! File Manifest used by this procedure, so make sure it's RelationManager is open
  Access:VehicleComposition.UseFile               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:ManifestLoad,SELF) ! Initialize the browse manager
  SELF.Open(Window)                               ! Open window
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,MAL:FKey_MID)                ! Add the sort order for MAL:FKey_MID for sort order 1
  BRW1.AddRange(MAL:MID,LOC:MID)                  ! Add single value range limit for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,MAL:MID,1,BRW1)       ! Initialize the browse locator using  using key: MAL:FKey_MID , MAL:MID
  BRW1.AddField(VMM:MakeModel,BRW1.Q.VMM:MakeModel) ! Field VMM:MakeModel is a hot field or requires assignment from browse
  BRW1.AddField(TRU:Registration,BRW1.Q.TRU:Registration) ! Field TRU:Registration is a hot field or requires assignment from browse
  BRW1.AddField(TRU:Capacity,BRW1.Q.TRU:Capacity) ! Field TRU:Capacity is a hot field or requires assignment from browse
  BRW1.AddField(LOC:Remaining,BRW1.Q.LOC:Remaining) ! Field LOC:Remaining is a hot field or requires assignment from browse
  BRW1.AddField(MAL:MLID,BRW1.Q.MAL:MLID)         ! Field MAL:MLID is a hot field or requires assignment from browse
  BRW1.AddField(MAL:MID,BRW1.Q.MAL:MID)           ! Field MAL:MID is a hot field or requires assignment from browse
  BRW1.AddField(TRU:TTID,BRW1.Q.TRU:TTID)         ! Field TRU:TTID is a hot field or requires assignment from browse
  BRW1.AddField(VMM:VMMID,BRW1.Q.VMM:VMMID)       ! Field VMM:VMMID is a hot field or requires assignment from browse
  INIMgr.Fetch('Ask_ManifestLoad',Window)         ! Restore window settings from non-volatile store
      LOC:MID         = p:MID
  
      MAN:MID         = p:MID
      IF Access:Manifest.TryFetch(MAN:PKey_MID) = LEVEL:Benign
         VCO:VCID     = MAN:VCID
         IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
            ?Prompt_VehComp{PROP:Text}    = 'Composition: ' & VCO:CompositionName
      .  .
  BRW1.AddToolbarTarget(Toolbar)                  ! Browse accepts toolbar control
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('FunTrnIS','Ask_ManifestLoad',1,?List,1,BRW1::PopupTextExt,Queue:Browse,4,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\FunTrnIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Manifest.Close
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Ask_ManifestLoad',Window)               ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Select
      BRW1.UpdateViewRecord()
          LOC:Selected    = MAL:MLID
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CancelButton
      ThisWindow.Update()
          CLEAR(LOC:Selected)
      
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)


BRW1.SetQueueRecord PROCEDURE

  CODE
      LOC:Remaining   = Get_ManLoadItems_Info(MAL:MLID)
  PARENT.SetQueueRecord
  


BRW1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
     ELSE
     END
  END

!!! <summary>
!!! Generated from procedure template - Source
!!! uses Get_Delivery_ManIDs
!!! </summary>
Check_Delivery_MID   PROCEDURE  (p:DID, p:MID)             ! Declare Procedure
LOC:MIDs             STRING(200)                           !
LOC:MID              ULONG                                 !
LOC:DIID             ULONG                                 !
LOC:Return           BYTE                                  !

  CODE
    ! (p:DID, p:MID)
    ! Could / Should we change this into a SQL query rather?

    !   0. Return number of different MIDs
    !   x. Return this number MID
    IF Get_Delivery_ManIDs(p:DID, 0, LOC:MIDs, 1) > 0
       ! Check through LOC:MIDs for the p:MID
       ! If not omitted - p:MAN_DIID_List
       !       Load it with MID,DIID,MID,DIID - which is the MIDs and the 1st DIID on each

       LOOP
          IF CLIP(LOC:MIDs) = ''
             BREAK
          .

          LOC:MID  = Get_1st_Element_From_Delim_Str(LOC:MIDs, ',', TRUE)
!          LOC:DIID = Get_1st_Element_From_Delim_Str(LOC:MIDs, ',', TRUE)       - new option 1 allows for only MIDs to be returned

          IF LOC:MID = p:MID
             LOC:Return = 1
             BREAK
    .  .  .


    RETURN(LOC:Return)
!!! <summary>
!!! Generated from procedure template - Source
!!! Delivery Item Un-manifested units
!!! </summary>
Get_DelItemUnMan_Units PROCEDURE  (p:DIID)                 ! Declare Procedure
LOC:Units            ULONG                                 !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
     Access:DeliveryItemAlias.UseFile()
    ! (p:DIID)

    A_DELI:DIID       = p:DIID
    IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) = LEVEL:Benign
!    message('A_DELI:Units: ' & A_DELI:Units & '||Get_ManLoadItem_Units(p:DIID): ' & Get_ManLoadItem_Units(p:DIID))
       LOC:Units    = A_DELI:Units - Get_ManLoadItem_Units(p:DIID)
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Units)

    Exit

CloseFiles     Routine
    Relate:DeliveryItemAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! note: Ulong returned - divide by 100;  also can do single item
!!! </summary>
Get_DelMan_Weight    PROCEDURE  (p:DID, p:DIID)            ! Declare Procedure
LOC:Weight_As_Ulong  ULONG                                 !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
     Access:DeliveryItemAlias.UseFile()
    ! (p:DID, p:DIID)
    ! (ULONG, <ULONG>),ULONG
    ! Create view?

    CLEAR(A_DELI:Record, -1)
    A_DELI:DID        = p:DID
    SET(A_DELI:FKey_DID_ItemNo, A_DELI:FKey_DID_ItemNo)
    LOOP
       IF Access:DeliveryItemAlias.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF A_DELI:DID ~= p:DID
          BREAK
       .

       IF ~OMITTED(3)
          IF MALD:DIID ~= p:DIID
             CYCLE
       .  .

       ! We must use real weight not Volumetric!!!  Volumetric is only for rates

!       IF A_DELI:Weight >= A_DELI:VolumetricWeight
          LOC:Weight_As_Ulong   += A_DELI:Weight           * (Get_ManLoadItem_Units(A_DELI:DIID) / A_DELI:Units)  * 100
!       ELSE
!          LOC:Weight_As_Ulong   += A_DELI:VolumetricWeight * (Get_ManLoadItem_Units(A_DELI:DIID) / A_DELI:Units)  * 100
!       .

       IF ~OMITTED(3)
          BREAK
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Weight_As_Ulong)

    Exit

CloseFiles     Routine
    Relate:DeliveryItemAlias.Close()
    Exit
