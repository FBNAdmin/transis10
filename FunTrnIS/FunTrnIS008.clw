

   MEMBER('FunTrIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('FUNTRNIS008.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output)
!!! </summary>
Gen_Statement        PROCEDURE  (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:Output) ! Declare Procedure
LOC:Result           LONG                                  !
LOC:MonthEndDate     LONG                                  !
LOC:Run_Date_Mth_End BYTE                                  !
LOC:Next_Mth_Mth_End BYTE                                  !
LOC:Statement_Info   GROUP,PRE(L_SI)                       !
Days90               DECIMAL(10,2)                         !90 Days
Days60               DECIMAL(10,2)                         !60 Days
Days30               DECIMAL(10,2)                         !30 Days
Current              DECIMAL(10,2)                         !Current
Total                DECIMAL(10,2)                         !Total
                     END                                   !
LOC:Credited         DECIMAL(11,2)                         !
LOC:Amount           DECIMAL(10,2)                         !
LOC:CreditNotes_Added_Q QUEUE,PRE(L_CQ)                    !
IID                  ULONG                                 !Invoice Number
                     END                                   !
LOC:Stats_Group      GROUP,PRE(L_SG)                       !
No_Invs              LONG                                  !
TimeIn               LONG                                  !
                     END                                   !
LOC:Skip_Statement   BYTE                                  !
LOC:Output_Audit     BYTE                                  !
LOC:Statement_Items_Added ULONG                            !
Tek_Failed_File     STRING(100)

Inv_View            VIEW(_Invoice)
    PROJECT(INV:IID, INV:BID, INV:CID, INV:DINo, INV:InvoiceDate, INV:InvoiceTime, INV:Total, INV:DID, INV:Status, INV:StatusUpToDate, INV:ClientNo, INV:CR_IID)
    .

View_Inv            ViewManager



InvA_View            VIEW(InvoiceAlias)
    PROJECT(A_INV:IID, A_INV:BID, A_INV:CID, A_INV:DINo, A_INV:InvoiceDate, A_INV:InvoiceTime, A_INV:Total, A_INV:DID, A_INV:Status, A_INV:CR_IID)
    .

View_InvA            ViewManager
Measures             IKB_Stats_class

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Invoice.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAllocation.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Statements.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_StatementItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceAlias.Open()
     .
     Access:_Invoice.UseFile()
     Access:ClientsPaymentsAllocation.UseFile()
     Access:_Statements.UseFile()
     Access:_StatementItems.UseFile()
     Access:InvoiceAlias.UseFile()
    PUSHBIND
    BIND('INV:InvoiceDate', INV:InvoiceDate)
    BIND('INV:InvoiceTime', INV:InvoiceTime)
    BIND('INV:InvoiceDateAndTime', INV:InvoiceDateAndTime)
    BIND('INV:Status', INV:Status)
    BIND('INV:CR_IID', INV:CR_IID)
    BIND('A_INV:InvoiceDate', A_INV:InvoiceDate)
    BIND('A_INV:InvoiceTime', A_INV:InvoiceTime)
    BIND('A_INV:InvoiceDateAndTime', A_INV:InvoiceDateAndTime)
    BIND('A_INV:Status', A_INV:Status)
    
    ! (p:STRID, p:CID, p:Date, p:Time, p:Owing, p:Dont_Add, p:Statement_Info, p:Run_Type, p:MonthEndDay, p:OutPut)
    !     1       2      3        4       5          6            7               8            9             10
    ! (ULONG, ULONG, LONG, LONG, <*DECIMAL>, BYTE=0, <*STRING>, BYTE=0, BYTE=0, BYTE=0),LONG
    !   p:Date  - Run / Effective date
    !   p:Run_Type
    !       0   -               ! Client & Statement Gen.
    !       1   - 
    !       2   -
    !
    !   p:OutPut
    !       0   - none
    !       1   - All
    !       2   - Current
    !       3   - 30 Days
    !       4   - 60 Days
    !       5   - 90 Days
    !
    ! We should run the invoice maintenance function before running the statements... maybe not though, see code
    ! check if we have an ini setting then - 16 Oct 2012 - allows this to be set in ini thenq
    LOC:OutPut_Audit  = GETINI('Gen_Statement', 'OutPut_Audit', 0, GLO:Global_INI)
    !MESSAGE('GLO:Global_INI: ' & CLIP(GLO:Global_INI) & '    LOC:OutPut_Audit: ' & LOC:OutPut_Audit)
    IF LOC:OutPut_Audit = 10
       IF p:OutPut = 0
          p:OutPut = 1
    .  .

    L_SG:TimeIn                 = CLOCK()

    ! If p:MonthEndDay is provided it is taken to be the forthcoming month end otherwise settings are used
    ! Get_Month_End_Date
    ! (p:Date, p:MonthEndDay)
    ! (LONG, BYTE=0),DATE
    LOC:MonthEndDate            = Get_Month_End_Date(p:Date, p:MonthEndDay)

!    db.debugout('[Gen_Statement]   p:MonthEndDay: ' & p:MonthEndDay & ',   Date: ' & FORMAT(LOC:MonthEndDate,@d6))
!    db.debugout('[Gen_Statement]  In - CID: ' & p:CID & ',  Date: ' & FORMAT(p:Date,@d5))
    IF p:Dont_Add = TRUE
       DO Get_Details
       IF ~OMITTED(7)
          p:Statement_Info      = LOC:Statement_Info
       .
    ELSE
       ! Create Statement
       CLEAR(STA:Record)
       IF Access:_Statements.TryPrimeAutoInc() = LEVEL:Benign
          !STA:STID            auto inc
          STA:STRID             = p:STRID
          STA:StatementDate     = p:Date
          STA:StatementTime     = p:Time

          STA:CID               = p:CID
          STA:BID               = GLO:BranchID

          DO Get_Details
          STA:Record           :=: LOC:Statement_Info

          !IF STA:Total = 0.0
          IF LOC:Statement_Items_Added <= 0
             ! No statement when nothing to state.
             IF Access:_Statements.CancelAutoInc() ~= LEVEL:Benign
             .
          ELSE
             IF Access:_Statements.TryInsert() ~= LEVEL:Benign
                ! hmm ??
             ELSE
                LOC:Result      = TRUE
          .  .
       ELSE
          ! hmmm?
    .  .

    IF ~OMITTED(5)
       p:Owing                  = L_SI:Total
    .

    ! Update Client - updates Client Record with these balances
   Measures._Start('Upd_Client_Balances')
    Upd_Client_Balances(p:CID, p:Date, LOC:Statement_Info)
   Measures._Stop('Upd_Client_Balances')

    IF CLOCK() - L_SG:TimeIn > 100
       db.debugout('[Gen_Statement]  No. Invs: ' & L_SG:No_Invs & ',   CID: ' & p:CID        & |
                    ',  Time > 1 sec: ' & (CLOCK() - L_SG:TimeIn) / 100)
    .
   
   db.Debugout('Gen_Statement - Stats: ' & Measures._Stats())
   Add_Log(Measures._Stats(), 'Stats @ ' & FORMAT(CLOCK(), @t4), 'Stats - Gen_Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
            
            
            
            
    UNBIND('INV:InvoiceDate')
    UNBIND('INV:InvoiceTime')
    UNBIND('INV:InvoiceDateAndTime')
    UNBIND('INV:Status')
    UNBIND('INV:CR_IID')
    UNBIND('A_INV:InvoiceDate')
    UNBIND('A_INV:InvoiceTime')
    UNBIND('A_INV:InvoiceDateAndTime')
    UNBIND('A_INV:Status')
    POPBIND
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:_Invoice.Close()
    Relate:ClientsPaymentsAllocation.Close()
    Relate:_Statements.Close()
    Relate:_StatementItems.Close()
    Relate:InvoiceAlias.Close()
    Exit
Get_Details                            ROUTINE
DATA
r_propsql      BYTE(0)

r_stats  GROUP,PRE(_rs)
Upd_Invoice_Paid_Status_time  ULONG
Get_ClientsPay_Alloc_Amt_time ULONG
Get_Inv_Credited_time ULONG
Add_Log_time ULONG
.

CODE
!    My_SQL.Init(GLO:DBOwner,'_TempSQL')
!
!    IF My_SQL.PropSQL('SELECT IID, BID, CID, DINo, InvoiceDate, InvoiceTime, Total, DID, Status, StatusUpToDate, ClientNo' & |
!                    ' FROM _Invoice WHERE CID = ' & p:CID & ' AND InvoiceDate < ' & SQL_Get_DateT_G(p:Date + 1) & |
!                    ' AND AND Status < 4 ORDER BY InvoiceDate') < 0
!       MESSAGE('Error on SQL.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Gen_Statement', ICON:Hand)
!    .
!
    L_SG:No_Invs        = 0

    View_Inv.Init(Inv_View, Relate:_Invoice)
    View_Inv.AddSortOrder(INV:FKey_CID)
    View_Inv.AddRange(INV:CID, p:CID)

    View_Inv.AppendOrder('INV:InvoiceDate')

    ! Assume they only want current, 2 is fully paid
    ! Note: use of tomorrow as date, with no time specified.

    !       0           1               2              3                 4               5              6         7
    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown, Bad Debt, Over Paid
    ! Fully paid invoices are excluded from the set because they cannot impact on the balance owing
    ! 8 May, added conditions from OR onwards
    !      , note Bad Debts are not dealt with here.....???

    !View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND (INV:Status < 4 OR (INV:Status = 5 AND INV:CR_IID = 0) OR INV:Status = 7)')

    !25.02.2011 by Victor. Client said: 
    ! "on every statement it should show the invoice and all credit notes against it - until the invoice is fully paid - 
    ! then it shows once more and never again", so
    ! 1) Include fully paid invoices in the view. They be excluded later depending on whether they appeared on Statements
    ! 2) Exclude credit notes, they will be calculated separately for each invoice listed    
    !View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND ( INV:CR_IID = 0 AND INV:Status < 5 AND INV:Status NOT= 2) OR INV:Status = 7 )')
    View_Inv.SetFilter('INV:InvoiceDate < ' & p:Date + 1 & ' AND (INV:CR_IID = 0 AND INV:Status < 4 AND INV:Status ~= 2 OR (INV:CR_IID = 0 AND INV:Status = 7))', '1')

    IF p:Dont_Add = TRUE      ! 27.06.12 - Ivan - then we dont want to select fully paid as we ignore later
      View_Inv.SetFilter('INV:Status ~= 4', '2')
    .
  
    !IF p:Dont_Add = TRUE ! Victor: old functionality - ignore
    !   Add a filter for Credit notes (2) as they cannot impact on an Age analysis run
    !.
   
    View_Inv.Reset()
    IF ERRORCODE()
       MESSAGE('Error on View.||Error: ' & ERROR() & '|File Er.: ' & FILEERROR(), 'Gen_Statement', ICON:Hand)
    .
    LOOP
       IF View_Inv.Next() ~= LEVEL:Benign
          IF LOC:OutPut_Audit = 10 AND r_propsql = 0      ! 16 Oct 2012
             Add_Log('Prop SQL _invoice: ' & _Invoice{PROP:SQL}, 'SQL @ ' & FORMAT(CLOCK(), @t4), 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
             r_propsql = 1
          .
          BREAK
       .      
       IF LOC:OutPut_Audit = 10 AND r_propsql = 0      ! 16 Oct 2012
          Add_Log('Prop SQL _invoice: ' & _Invoice{PROP:SQL}, 'SQL @ ' & FORMAT(CLOCK(), @t4), 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          r_propsql = 1
       .

       If INV:InvoiceDate >= p:Date + 1  Or INV:CR_IID ~= 0 Or (INV:Status > 5 AND INV:Status ~= 7) Or INV:Status = 2 Then       ! INV:Status = 6 is included here, and any new ones above 7
          Cycle
       End
       LOC:Amount = 0
       If INV:Status = 4 ! fully paid (added 25.02.2011)
          If p:Dont_Add = FALSE
             !If Check_IID_On_Statement(INV:IID) ~= 0 Then
                ! -1 = Error condition, found Statement Item but not Statement
                ! -2 = Error condition, found Statement but not Statement Run
                !  0 = No Statement found
                !  1 = Found on Statement but not Client Statement
                !  2 = Found on Client Statement
             !   Cycle
             !End
             Access:_SQLTemp.Open
             Access:_SQLTemp.UseFile
             if RunSQLRequest('Select Count(*) From _StatementItems Where IID = '&INV:IID,_SQLTemp)                
                If Access:_SQLTemp.TryNext() = Level:Benign THEN
                   cnt# = _SQ:S1
                   If cnt# Then
                      Cycle
                   End
                Else                   
                   Cycle !Could not get count
                End
             End
             Access:_SQLTemp.Close             
          Else
             !exclude fully paid invoices as before
             Cycle
          End
       End

       L_SG:No_Invs += 1


       ! 8 May 08 - With new condition above we need condition here to avoid showing shown
       LOC:Skip_Statement       = FALSE
       IF INV:Status = 5 OR INV:Status = 7
          ! Then we dont want to do
          LOC:Skip_Statement    = TRUE
       .
       
       ! Here we only have the invoices less than the date specified.
       ! and not fully paid (or fully paid not shown - Added by Victor)
       ! For credit notes, if they are appearing on their 1st statement, then set them to having been shown
       IF p:Run_Type < 2 AND p:Dont_Add = FALSE          ! Client & Statement Gen.
          Upd_Invoice_Paid_Status(INV:IID, 1)            ! Update Fully Paid - Credit to Fully Paid and Credit Note to Credit Note - Shown
       ELSE
          IF INV:StatusUpToDate = FALSE                  ! We know we need to check the Invoice Status
             Measures._Start('Upd_Invoice_Paid_Status')
               
             Upd_Invoice_Paid_Status(INV:IID)
               
             Measures._Stop('Upd_Invoice_Paid_Status')
       .  .

       IF p:Dont_Add = FALSE            ! Add to Statement
          L_CQ:IID       = INV:IID
          GET(LOC:CreditNotes_Added_Q, L_CQ:IID)
          IF ~ERRORCODE()               ! Already have shown Credit note with respective invoice
             CYCLE
          .
          
          IF LOC:Skip_Statement = FALSE
             DO Add_to_Statement
          .
       ELSE
          IF INV:Status = 2 OR INV:Status = 5                             ! If this is a Credit Note, then no amount
             ! should not be within this branch since 25.02.2011
             !STAI:Amount    = 0.0 ! change of 17.03.2011 by Victor
          ELSE
             STAI:Debit     = INV:Total                                   ! Invoice total

             !LOC:Credited   = Get_Inv_Credited( INV:IID, p:Date )         ! Less any Credit Notes

             !STAI:Debit    += LOC:Credited ! remarked by Victor
             !Do not accoount Credit Notes for Debit anymore MIT:35-1826 (25.02.2011)
             ! Before 17.03.2010 there was:
             !credit note doesn't affect amount so we don't account it for credit at first
             ! with requirement of 14.03.2011 it should be accounted in total, so move calculating amount later

             Measures._Start('Get_ClientsPay_Alloc_Amt')
             STAI:Credit    = Get_ClientsPay_Alloc_Amt( INV:IID, 0, p:Date )      ! Payment total for this Invoice
             Measures._Stop('Get_ClientsPay_Alloc_Amt')

             !STAI:Amount    = STAI:Debit - STAI:Credit

             ! In case of adding statement credit notes are added to credit as + amount, need to account for credit here
             ! (according to new requirement of 25.02.2011 it shoud include all notes of the invoice, so ommit date)
             Measures._Start('Get_Inv_Credited')
             LOC:Credited   = Get_Inv_Credited( INV:IID )
             Measures._Stop('Get_Inv_Credited')
               
             STAI:Credit   -= LOC:Credited
             STAI:Amount    = STAI:Debit - STAI:Credit
          .

          LOC:Amount        = STAI:Amount
       .       
!    db.debugout('[Gen_Statement]  CID: ' & p:CID & ',  INV:IID: ' & INV:IID & ',  INV:CID: ' & INV:CID & ',  INV:DID: ' & INV:DID & ',  INV:Total: ' & INV:Total)
       Measures._Start('Months_Between')
       CASE Months_Between(LOC:MonthEndDate, INV:InvoiceDate)
       OF 0
          L_SI:Current   += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 2                       ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)
             Add_Log(',' & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, 'Current', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       OF 1
          L_SI:Days30    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 3
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '30 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       OF 2
          L_SI:Days60    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 4
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '60 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       ELSE
          L_SI:Days90    += LOC:Amount

          IF p:OutPut = 1 OR p:OutPut = 5
             Add_Log(','  & INV:ClientNo & ',' & INV:IID & ',' & FORMAT(INV:InvoiceDate,@d18) & ',' & INV:Total & ',' & LOC:Credited & ',' & STAI:Credit, '90 Days', 'Gen Statement - ' & FORMAT(TODAY(), @d7) & '.csv',,,FALSE)
          .
       .
            
       Measures._Stop('Months_Between')
           
    .  

    View_Inv.Kill()

    L_SI:Total            = L_SI:Days90 + L_SI:Days60 + L_SI:Days30 + L_SI:Current
    EXIT
Add_to_Statement                ROUTINE
    IF Access:_StatementItems.TryPrimeAutoInc() = LEVEL:Benign
       ! STAI:STIID
       STAI:STID            = STA:STID

       STAI:InvoiceDate     = INV:InvoiceDate
       STAI:IID             = INV:IID
       STAI:DID             = INV:DID
       STAI:DINo            = INV:DINo

       ! p:Option
       !   0.  Invoice Paid
       !   1.  Clients Payments Allocation total
       STAI:Debit           = INV:Total

       ! When Credit
       LOC:Credited         = Get_Inv_Credited( INV:IID )
       !STAI:Debit          += LOC:Credited               ! Less any Credit
       !Do not accoount Credit Notes for Debit anymore MIT:35-1826 (25.02.2011) by Victor

       STAI:Credit          = Get_ClientsPay_Alloc_Amt( INV:IID, 0 )

       !    0               1               2           3                   4               5
       ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
       IF INV:Status = 2 OR INV:Status = 5          ! Credit notes
          ! normally should not be here as we have excluded credit notes since 25.02.2011
          !STAI:Amount       = 0.0 !changed 17.03.2011 by Victor

          ! FBN wants the "Credit" amount to be in the Credit column and a + amount
          STAI:Credit       = - INV:Total
          STAI:Debit       -= INV:Total
       ELSE
          !STAI:Amount       = STAI:Debit - STAI:Credit
       END
       STAI:Amount       = STAI:Debit - STAI:Credit ! moved from Else 17.03.2011

       LOC:Amount           = STAI:Amount
       
       IF Access:_StatementItems.TryInsert() = LEVEL:Benign          
          LOC:Statement_Items_Added += 1
             
          DO Credit_Notes_for_IID ! Add all credit notes of the respective invoice if any (and add them to the Q so that it is not added here again)

    .  .
    EXIT
    
Credit_Notes_for_IID            ROUTINE
    View_InvA.Init(InvA_View, Relate:InvoiceAlias)
    View_InvA.AddSortOrder(A_INV:Key_CR_IID)
    View_InvA.AppendOrder('A_INV:InvoiceDate')
    View_InvA.AddRange(A_INV:CR_IID, INV:IID) ! NOT L_CQ:IID, L_CQ:IID is a credit note IID

    !       0           1               2              3                 4               5
    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
    !View_InvA.SetFilter('A_INV:InvoiceDate < ' & p:Date + 1 & ' AND A_INV:Status < 4')
    !Do not accoounted Credit Notes for Debit anymore MIT:35-1826 (25.02.2011) by Victor

    View_InvA.Reset()
    LOOP
       IF View_InvA.Next() ~= LEVEL:Benign
          BREAK
        .        

       ! Show it then after the invoice
       IF Access:_StatementItems.TryPrimeAutoInc() = LEVEL:Benign
          ! STAI:STIID
          STAI:STID            = STA:STID

          STAI:InvoiceDate     = A_INV:InvoiceDate
          STAI:IID             = A_INV:IID
          STAI:DID             = A_INV:DID
          STAI:DINo            = A_INV:DINo

          ! p:Option
          !   0.  Invoice Paid
          !   1.  Clients Payments Allocation total
          STAI:Debit           = A_INV:Total

          ! When Credit
          LOC:Credited         = Get_Inv_Credited( A_INV:IID )                      ! --------- should not be for credit notes
          !STAI:Debit          += LOC:Credited               ! Less any Credit
          !Do not accoounted Credit Notes for Debit anymore MIT:35-1826 (25.02.2011) by Victor

          STAI:Credit          = Get_ClientsPay_Alloc_Amt( A_INV:IID, 0 )           ! --------- should not be for credit notes
          !    0               1               2           3                   4               5
          ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
          !IF A_INV:Status = 2 ! unconditionally since 25.02.2011                    ! --------- should only be
             !STAI:Amount       = 0.0 !changed 17.03.2011

             ! FBN wants the "Credit" amount to be in the Credit column
             STAI:Credit      -= A_INV:Total
             STAI:Debit       -= A_INV:Total
             STAI:Amount       = STAI:Debit - STAI:Credit !calculate Amount for credit note (since 17.03.2011)
          !.
    db.debugout('[Gen_Statement]  Adding Credit note - STAI:STIID: ' & STAI:STIID)

          IF Access:_StatementItems.TryInsert() = LEVEL:Benign             
             LOC:Statement_Items_Added += 1
             
             LOC:Amount    += STAI:Amount              !include the Anount in Total (since 17.03.2011)
             L_CQ:IID       = STAI:IID
             ADD(LOC:CreditNotes_Added_Q)             

             ! Ivan - as we are not setting Credit Note - Shown above anymore, because Credit notes are filtered out before they
             !        get to that code, we must now do that here
             IF p:Run_Type < 2 AND p:Dont_Add = FALSE AND A_INV:Status = 2    ! Client & Statement Gen.
                Upd_Invoice_Paid_Status(A_INV:IID, 1)            ! Update Fully Paid - Credit to Fully Paid and Credit Note to Credit Note - Shown

!    Message('[Gen_Statement]  Setting shown status - A_INV:IID: ' & A_INV:IID)
             .                
          End
       End
    End

    View_InvA.Kill()
    EXIT
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Check_StatementItems PROCEDURE  (p:IID)                    ! Declare Procedure
LOC:Client_Statement STRING(100)                           !
Tek_Failed_File     STRING(100)

View_State              VIEW(_StatementItems)
    !PROJECT()
       JOIN(STA:PKey_STID, STAI:STID)
       PROJECT(STA:STID, STA:STRID)
          JOIN(STAR:PKey_STRID, STA:STRID)
          PROJECT(STAR:RunDescription, STAR:RunDate, STAR:Type)
    .  .  .


State_View              ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_StatementItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Statements.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Statement_Runs.Open()
     .
     Access:_StatementItems.UseFile()
     Access:_Statements.UseFile()
     Access:_Statement_Runs.UseFile()
    ! (p:IID)
    ! Check if this invoice has been on a Client Statement yet

    PUSHBIND
    BIND('STAR:Type', STAR:Type)

    State_View.Init(View_State, Relate:_StatementItems)
    State_View.AddSortOrder(STAI:FKey_IID)
    State_View.AppendOrder('STAI:InvoiceDate,STAI:InvoiceTime,STAI:STID,STAI:STIID')
    State_View.AddRange(STAI:IID, p:IID)
    State_View.SetFilter('STAR:Type=0')

    State_View.Reset()

    IF State_View.Next() = LEVEL:Benign
       LOC:Client_Statement     = STA:STID
    .

    State_View.Kill()

    POPBIND

    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Client_Statement)

    Exit

CloseFiles     Routine
    Relate:_StatementItems.Close()
    Relate:_Statements.Close()
    Relate:_Statement_Runs.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! note: Ulong returned - divide by 100;  also can do single item
!!! </summary>
Get_DelTripDel_Weight PROCEDURE  (p:DID, p:DIID)           ! Declare Procedure
LOC:Weight_As_Ulong  ULONG                                 !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
     Access:DeliveryItemAlias.UseFile()
    ! (p:DID, p:DIID)
    ! (ULONG, <ULONG>),ULONG
    ! Create view?

    CLEAR(A_DELI:Record, -1)
    A_DELI:DID        = p:DID
    SET(A_DELI:FKey_DID_ItemNo, A_DELI:FKey_DID_ItemNo)
    LOOP
       IF Access:DeliveryItemAlias.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF A_DELI:DID ~= p:DID
          BREAK
       .

       IF ~OMITTED(3)
          IF MALD:DIID ~= p:DIID
             CYCLE
       .  .

       !       0. Delivered Units
       !       1. Delivered & Un Delivered on incomplete Trips
       !       2. Delivered & Rejected
       !       3. Delivered & Rejected & Un Delivered on incomplete Trips
       LOC:Weight_As_Ulong   += A_DELI:Weight           * (Get_TripItem_Del_Units(A_DELI:DIID, 3) / A_DELI:Units)  * 100

       IF ~OMITTED(3)
          BREAK
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Weight_As_Ulong)

    Exit

CloseFiles     Routine
    Relate:DeliveryItemAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! note: Ulong returned - divide by 100 for weight
!!! </summary>
Get_TripDelItems_Info PROCEDURE  (p:TRID, p:Option, p:DID, p:TDID, p:DIID, p:Del_List) ! Declare Procedure
LOC:Weight_As_Ulong  ULONG                                 !
LOC:DelDates_Q       QUEUE,PRE(L_DQ)                       !Unique Q
Date                 DATE                                  !
                     END                                   !
Tek_Failed_File     STRING(100)

View_TS         VIEW(TripSheetDeliveriesAlias)
    .

View_TS_Del     VIEW(TripSheetDeliveriesAlias)
    PROJECT(A_TRDI:DIID, A_TRDI:TRID, A_TRDI:DeliveredDate, A_TRDI:DeliveredTime)
       JOIN(A_DELI:PKey_DIID, A_TRDI:DIID)
       PROJECT(A_DELI:ItemNo)
       .
       JOIN(A_TRI:PKey_TID, A_TRDI:TRID)
       PROJECT(A_TRI:ReturnedDate,A_TRI:ReturnedTime)
    .  .



TS_View         ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetsAlias.Open()
     .
     Access:DeliveryItemAlias.UseFile()
     Access:TripSheetDeliveriesAlias.UseFile()
     Access:TripSheetsAlias.UseFile()
    ! (p:TRID, p:Option, p:DID, p:TDID, p:DIID, p:Del_List)
    ! (<ULONG>, BYTE=0, <ULONG>, <ULONG>, <ULONG>, <*STRING>),ULONG
    !   1         2        3        4       5           6
    ! p:Del_List
    !   Either list of Dates & Times or TRID's
    ! p:Option
    ! TRID passed
    !   0. Weight (real weight - not volumetric)
    !   1. Units
    !       if p:DID is provided then the result is limited to the DID passed
    !       if p:TDID is provided then exclude this from weight
    !   2. List of TS-Delivery delivered Dates, no of entries returned - for DIID or TRID!
    ! DID passed only       (no of entries returned )
    !   3. List of TS item delivered Dates & Times   (format "@d5 - @t1, @d5 - @t1)
    !           was - List of TS delivered Dates & Times
    !   4. List of TS TRIDs
    !
    !   Note - if option 3 or 4 used but param 3 ommitted then this will cause a GPF in current form!

    IF (p:Option = 3 OR p:Option = 4)
       BIND('A_DELI:DID',A_DELI:DID)

       TS_View.Init(View_TS_Del, Relate:TripSheetDeliveriesAlias)
       TS_View.AddSortOrder(A_TRDI:PKey_TDID)
       TS_View.AppendOrder('A_DELI:ItemNo')
       !TS_View.AddRange(A_DELI:DID, p:DID)

       IF ~OMITTED(3)
          TS_View.SetFilter('A_DELI:DID = ' & p:DID)
       ELSE
          MESSAGE('Option used is ' & p:Option & '||But p:DID was omitted.', 'Get_TripDelItems_Info', ICON:Hand)
       .
    ELSE
       TS_View.Init(View_TS, Relate:TripSheetDeliveriesAlias)

       IF ~OMITTED(1)
          TS_View.AddSortOrder(A_TRDI:FKey_TRID)
          TS_View.AddRange(A_TRDI:TRID, p:TRID)
       ELSIF ~OMITTED(5)
          TS_View.AddSortOrder(A_TRDI:FKey_DIID)
          TS_View.AddRange(A_TRDI:DIID, p:DIID)
    .  .


    !TS_View.AppendOrder()
    !TS_View.SetFilter()

    TS_View.Reset()
    LOOP
       IF TS_View.Next() ~= LEVEL:Benign
          BREAK
       .

       IF A_TRDI:DIID = 0                             ! Empty Trip Delivery
          CYCLE
       .
       IF ~OMITTED(4)
          IF A_TRDI:TDID = p:TDID
             CYCLE
       .  .

       CASE p:Option
       OF 0
          A_DELI:DIID  = A_TRDI:DIID
          IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) ~= LEVEL:Benign
             MESSAGE('DeliveryItems (alias) could not be fetched.||TRDI:DIID: ' & A_TRDI:DIID, 'Get_TripDelItems_Info', ICON:Hand)
          .

          IF OMITTED(3)
!             IF A_DELI:Weight >= A_DELI:VolumetricWeight
                LOC:Weight_As_Ulong   += A_DELI:Weight           * (A_TRDI:UnitsLoaded / A_DELI:Units)  * 100
!             ELSE
!                LOC:Weight_As_Ulong   += A_DELI:VolumetricWeight * (A_TRDI:UnitsLoaded / A_DELI:Units)  * 100
!             .
          ELSE
             IF A_DELI:DID = p:DID
!                IF A_DELI:Weight >= A_DELI:VolumetricWeight
                   LOC:Weight_As_Ulong   += A_DELI:Weight           * (A_TRDI:UnitsLoaded / A_DELI:Units)  * 100
!                ELSE
!                   LOC:Weight_As_Ulong   += A_DELI:VolumetricWeight * (A_TRDI:UnitsLoaded / A_DELI:Units)  * 100
          .  .  !.
       OF 1
          IF OMITTED(3)
             LOC:Weight_As_Ulong   += A_TRDI:UnitsLoaded
          ELSE
             A_DELI:DIID            = A_TRDI:DIID
             IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) ~= LEVEL:Benign
                CLEAR(A_DELI:Record)
                MESSAGE('DeliveryItems (alias) could not be fetched.||TRDI:DIID: ' & A_TRDI:DIID, 'Get_TripDelItems_Info', ICON:Hand)
             .
             IF A_DELI:DID = p:DID
                LOC:Weight_As_Ulong    += A_DELI:Units
          .  .
       OF 2
          L_DQ:Date                     = A_TRDI:DeliveredDate
          GET(LOC:DelDates_Q, L_DQ:Date)
          IF ~ERRORCODE()
             CYCLE
          .

          L_DQ:Date                     = A_TRDI:DeliveredDate
          ADD(LOC:DelDates_Q, +L_DQ:Date)

          IF ~OMITTED(6)
             ! (p:Add, p:List, p:Delim, p:Option, p:Prefix)
             ! (STRING, *STRING, STRING, BYTE=0, <STRING>)

             Add_to_List(FORMAT(A_TRDI:DeliveredDate,@d5), p:Del_List, ', ')
          .

          LOC:Weight_As_Ulong          += 1
       OF 3 OROF 4
          L_DQ:Date                     = A_TRDI:TRID
          GET(LOC:DelDates_Q, L_DQ:Date)
          IF ~ERRORCODE()
             CYCLE
          .
                                          
          L_DQ:Date                     = A_TRDI:TRID
          ADD(LOC:DelDates_Q, +L_DQ:Date)

          IF ~OMITTED(6)
             IF p:Option = 3
                Add_to_List(FORMAT(A_TRDI:DeliveredDate, @d5) & ' - ' & FORMAT(A_TRDI:DeliveredTime, @t1), p:Del_List, ', ')
             ELSE
                Add_to_List(A_TRDI:TRID, p:Del_List, ', ')
          .  .

          LOC:Weight_As_Ulong          += 1
       ELSE
          MESSAGE('Unknown Option: ' & p:Option, 'Get_TripDelItems_Info', ICON:Hand)
    .  .

    TS_View.Kill()


!    message('here - did: ' & p:DID & ',  no.: ' & LOC:Weight_As_Ulong & ',  p:Del_List: ' & CLIP(p:Del_List) & |
!            ',  p:Option: ' & p:Option)

    IF p:Option = 3 OR p:Option = 4
       UNBIND('A_DELI:DID')
    .
!               old
!    ! (p:TRID, p:Option, p:DID, p:TDID)
!    ! (ULONG, BYTE=0, <ULONG>, <ULONG>),ULONG
!    ! p:Option
!    !   0. Weight (real weight - not volumetric)
!    !   1. Units
!    !       if p:DID is provided then the result is limited to the DID passed
!    !       if p:TDID is provided then exclude this from weight
!
!!View_TS         VIEW(TripSheetDeliveriesAlias)
!
!
!!TS_View         ViewManager
!
!
!    CLEAR(A_TRDI:Record, -1)
!    A_TRDI:TRID          = p:TRID
!    SET(A_TRDI:FKey_TRID, A_TRDI:FKey_TRID)
!    LOOP
!       IF Access:TripSheetDeliveriesAlias.TryNext() ~= LEVEL:Benign
!          BREAK
!       .
!       IF A_TRDI:TRID ~= p:TRID
!          BREAK
!       .
!       IF A_TRDI:DIID = 0                             ! Empty Trip Delivery
!          CYCLE
!       .
!       IF ~OMITTED(4)
!          IF A_TRDI:TDID = p:TDID
!             CYCLE
!       .  .
!
!       CASE p:Option
!       OF 0
!          A_DELI:DIID  = A_TRDI:DIID
!          IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) ~= LEVEL:Benign
!             MESSAGE('DeliveryItems (alias) could not be fetched.||TRDI:DIID: ' & A_TRDI:DIID, 'Get_TripDelItems_Info', ICON:Hand)
!          .
!
!          IF OMITTED(3)
!!             IF A_DELI:Weight >= A_DELI:VolumetricWeight
!                LOC:Weight_As_Ulong   += A_DELI:Weight           * (A_TRDI:UnitsLoaded / A_DELI:Units)  * 100
!!             ELSE
!!                LOC:Weight_As_Ulong   += A_DELI:VolumetricWeight * (A_TRDI:UnitsLoaded / A_DELI:Units)  * 100
!!             .
!          ELSE
!             IF A_DELI:DID = p:DID
!!                IF A_DELI:Weight >= A_DELI:VolumetricWeight
!                   LOC:Weight_As_Ulong   += A_DELI:Weight           * (A_TRDI:UnitsLoaded / A_DELI:Units)  * 100
!!                ELSE
!!                   LOC:Weight_As_Ulong   += A_DELI:VolumetricWeight * (A_TRDI:UnitsLoaded / A_DELI:Units)  * 100
!          .  .  !.
!       OF 1
!          IF OMITTED(3)
!             LOC:Weight_As_Ulong   += A_TRDI:UnitsLoaded
!          ELSE
!             A_DELI:DIID            = A_TRDI:DIID
!             IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) ~= LEVEL:Benign
!                CLEAR(A_DELI:Record)
!                MESSAGE('DeliveryItems (alias) could not be fetched.||TRDI:DIID: ' & A_TRDI:DIID, 'Get_TripDelItems_Info', ICON:Hand)
!             .
!             IF A_DELI:DID = p:DID
!                LOC:Weight_As_Ulong    += A_DELI:Units
!          .  .
!       ELSE
!          MESSAGE('Unknown Option: ' & p:Option, 'Get_TripDelItems_Info', ICON:Hand)
!    .  .
!
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Weight_As_Ulong)

    Exit

CloseFiles     Routine
    Relate:DeliveryItemAlias.Close()
    Relate:TripSheetDeliveriesAlias.Close()
    Relate:TripSheetsAlias.Close()
    Exit
