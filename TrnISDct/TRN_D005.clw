

   MEMBER('TRANSIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('TRN_D005.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
Update_DBStructure PROCEDURE (p_Qry)

LOC:Update_Commands_Q QUEUE,PRE(L_UQ)                      !
No                   ULONG                                 !
Command              STRING(255)                           !
                     END                                   !
LOC:Results          STRING(1000)                          !
LOC:Idx              ULONG                                 !
LOC:Complete         BYTE                                  !
QuickWindow          WINDOW('Update Database Structure'),AT(,,260,160),FONT('MS Sans Serif',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('Update_DBStructure'),TIMER(100)
                       SHEET,AT(3,3,253,137),USE(?Sheet1)
                         TAB('General'),USE(?Tab1)
                           TEXT,AT(7,20,245,116),USE(LOC:Results),VSCROLL,BOXED,COLOR(00E9E9E9h),READONLY,SKIP
                         END
                       END
                       BUTTON('&Close'),AT(207,142,49,14),USE(?Ok),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation')
                       BUTTON('&Help'),AT(3,142,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_DBStructure')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Results
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_DBStructure',QuickWindow)           ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_DBStructure',QuickWindow)        ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          IF CLIP(p_Qry) = ''
             L_UQ:No                 = 1
             IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\TransIS.INI') = '0'
                L_UQ:Command         = 'ALTER TABLE Reminders ADD Created_UID INT'
                ADD(LOC:Update_Commands_Q)
             .
      
             L_UQ:No                 = 2
             IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\TransIS.INI') = '0'
                L_UQ:Command        = 'ALTER TABLE Reminders ADD Created_DateTime DateTime NULL'
                ADD(LOC:Update_Commands_Q)
             .
      
      
             ! 8 March 06
             L_UQ:No                 = 3
             IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\TransIS.INI') = '0'
                L_UQ:Command        = 'UPDATE __Rates  SET CTID = 0 WHERE (CTID <> 0) AND (LTID NOT IN ' & |
                                      '(SELECT LTID FROM loadtypes2 WHERE loadoption = 2))'
                ADD(LOC:Update_Commands_Q)
             .
      
             L_UQ:No                 = 4
             IF GETINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '0', '.\TransIS.INI') = '0'
                L_UQ:Command        = 'UPDATE __Rates  SET LTID = (SELECT LTID FROM clientsratetypes ' & |
                                      'WHERE clientsratetypes.CRTID = __Rates.CRTID)'
                ADD(LOC:Update_Commands_Q)
             .
          ELSE
             L_UQ:No                 = 0
             L_UQ:Command            = CLIP(p_Qry)
             ADD(LOC:Update_Commands_Q)
          .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
          QuickWindow{PROP:Timer} = 0
      
          LOC:Idx += 1
          GET(LOC:Update_Commands_Q, LOC:Idx)
          IF ERRORCODE()
             Add_to_List('---', LOC:Results, '<13,10>',,,TRUE)
             Add_to_List('Complete.', LOC:Results, '<13,10>',,,TRUE)
          ELSE
             _SQLTemp{PROP:SQL}  = CLIP(L_UQ:Command)
             IF ERRORCODE()
                ! (p:Add, p:List, p:Delim, p:Option, p:Prefix, p:Beginning)
                Add_to_List('Error on DB update: "' & CLIP(L_UQ:Command) & '"', LOC:Results, '<13,10>',,,TRUE)
                Add_to_List('---', LOC:Results, '<13,10>',,,TRUE)
             ELSE
                Add_to_List('DB updated: "' & CLIP(L_UQ:Command) & '"', LOC:Results, '<13,10>',,,TRUE)
                Add_to_List('---', LOC:Results, '<13,10>',,,TRUE)
      
                PUTINI('Update_DBStructure', 'CommandNo_' & L_UQ:No, '1', '.\TransIS.INI')
             .
      
             QuickWindow{PROP:Timer} = 100
          .
      
          DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_ClientsPay_Alloc_Amt_old PROCEDURE  (p:ID, p:Option)   ! Declare Procedure
LOC:Paid_Dec         DECIMAL(15,2)                         !
LOC:Return           STRING(30)                            !
Tek_Failed_File     STRING(100)

CliPay_View         VIEW(ClientsPaymentsAllocationAlias)
    PROJECT(A_CLIPA:CPAID, A_CLIPA:CPID, A_CLIPA:AllocationNo, A_CLIPA:IID, A_CLIPA:Amount)
    .



CP_View             ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAllocationAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAlias.Open()
     .
     Access:ClientsPaymentsAllocationAlias.UseFile()
     Access:ClientsPaymentsAlias.UseFile()
    ! p:Option
    !   0.  Invoice Paid
    !   1.  Clients Payments Allocation total

!          db.debugout('[Get_ClientsPay_Alloc_Amt]  Option: ' & p:Option & ',  ID: ' & p:ID)

    IF p:Option > 1
       MESSAGE('The option does not exist: ' & p:Option, 'Get_ClientsPay_Alloc_Amt', ICON:Hand)
    ELSE
       CP_View.Init(CliPay_View, Relate:ClientsPaymentsAllocationAlias)

       CASE p:Option
       OF 0
          CP_View.AddSortOrder(A_CLIPA:Fkey_IID)
          CP_View.AppendOrder('A_CLIPA:CPID,A_CLIPA:AllocationNo')
          CP_View.AddRange(A_CLIPA:IID, p:ID)
       OF 1                    
          CP_View.AddSortOrder(A_CLIPA:FKey_CPID_AllocationNo)
          CP_View.AddRange(A_CLIPA:CPID, p:ID)
       .

       CP_View.Reset()
       LOOP
          IF CP_View.Next() ~= LEVEL:Benign
             BREAK
          .

          LOC:Paid_Dec     += A_CLIPA:Amount
       .


       ! We need to check if these payments have been credited...
       IF p:Option = 1
          A_CLIP:CPID_Reversal  = p:ID
          IF Access:ClientsPaymentsAlias.TryFetch(A_CLIP:Key_CPIDReversal) = LEVEL:Benign
             ! This CPID has been reversed.. add these up too
             CP_View.AddSortOrder(A_CLIPA:FKey_CPID_AllocationNo)
             CP_View.AppendOrder('A_CLIPA:AllocationNo')
             CP_View.AddRange(A_CLIPA:CPID, A_CLIP:CPID)

             CP_View.SetSort(2)

             CP_View.Reset()
             LOOP
                IF CP_View.Next() ~= LEVEL:Benign
                   BREAK
                .

                LOC:Paid_Dec     += A_CLIPA:Amount
       .  .  .

       CP_View.Kill()
    .


    LOC:Return  = LOC:Paid_Dec
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:ClientsPaymentsAllocationAlias.Close()
    Relate:ClientsPaymentsAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Inv_Credited_old PROCEDURE  (p:IID)                    ! Declare Procedure
LOC:Total            DECIMAL(15,2)                         !
LOC:Return           STRING(30)                            !
Tek_Failed_File     STRING(100)

View_Inv            VIEW(InvoiceAlias)
    PROJECT(A_INV:IID,A_INV:Total)
    .


Inv_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceAlias.Open()
     .
     Access:InvoiceAlias.UseFile()
    ! (p:IID)

    Inv_View.Init(View_Inv, Relate:InvoiceAlias)
    Inv_View.AddSortOrder(A_INV:Key_CR_IID)

    !Inv_View.AppendOrder()
    Inv_View.AddRange(A_INV:CR_IID, p:IID)
    !Inv_View.SetFilter()

    Inv_View.Reset()

    LOOP
       IF Inv_View.Next() ~= LEVEL:Benign
          BREAK
       .

       LOC:Total   += A_INV:Total
    .

    Inv_View.Kill()


    LOC:Return      = LOC:Total
    
    
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:InvoiceAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Rep_GetNextID        PROCEDURE  (FileManager FM, Key PrimaryKey, *? RecordID) ! Declare Procedure
LOC:Group            GROUP,PRE(LG)                         !
RangeFrom            ULONG                                 !
RangeTo              ULONG                                 !
RepCID               ULONG                                 !
                     END                                   !
NextId               LONG                                  !
SpecialCase_ReplicationTable BYTE,STATIC                   !
LOC:Exit             BYTE                                  !
Tek_Failed_File     STRING(100)

LastID Long(0)

TableName  CString(61)
FieldName  CString(61)
SQLRequest CString(256)
FileState  UShort
MyC     CLASS

Get_Range   PROCEDURE()
Get_Max     PROCEDURE()
Upd_Last    PROCEDURE(),LONG

    .
MyLock  CriticalSection


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ReplicationIDControl.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_SQLTemp.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ReplicationTableIDs.Open()
     .
     Access:ReplicationIDControl.UseFile()
     Access:_SQLTemp.UseFile()
     Access:ReplicationTableIDs.UseFile()
       ! (FileManager, Key, *?),ULong
       ! (FileManager FM, Key PrimaryKey, *? RecordID)

!    db.debugout('Rep_GetNextID - in - GLO:ReplicatedDatabaseID: ' & GLO:ReplicatedDatabaseID)

    ! Get the applicable range
    IF GLO:ReplicatedDatabaseID ~= 0                ! On mode
       TableName   = FM.GetName()                   ! Get table name

!       db.debugout('[Rep_GetNextID]  Table: ' & CLIP(TableName))

       IF CLIP(UPPER(TableName)) = UPPER('dbo.ReplicationTableIDs')
          MyLock.Wait()
          IF SpecialCase_ReplicationTable = FALSE
             SpecialCase_ReplicationTable = TRUE
          ELSE
             LOC:Exit   = TRUE
          .
          MyLock.Release()
       .

       IF LOC:Exit = FALSE
          ! Get available range and Last used ID
          MyC.Get_Range()

          IF LG:RangeTo = 0                           ! We have NO range
             MESSAGE('There is no Range specified for Replication Database ID: ' & GLO:ReplicatedDatabaseID,'Replication ID Error',ICON:Hand)
          ELSE
             MyC.Get_Max()

             IF NextID = -1
                ! ??? send admin email?
                MESSAGE('Could not allocate an ID, please try again.','Replication ID Error',ICON:Hand)
             ELSIF NextID > LG:RangeTo OR NextID = 0
                NextID = -2
                ! ??? send admin email?
                MESSAGE('The Range specified for Replication Database ID: ' & GLO:ReplicatedDatabaseID & ' has been exceeded.','Replication ID Error',ICON:Hand)
             .

             IF NextID > 0
                RecordID   = NextID
             ELSE
                RecordID   = -1
    .  .  .  .

    !Message('RecordID='&RecordID,'Debug Message',,,,2) ! Victor - March 03, 2011

    IF CLIP(UPPER(TableName)) = UPPER('dbo.ReplicationTableIDs') AND LOC:Exit = FALSE
       MyLock.Wait()
       SpecialCase_ReplicationTable = FALSE
       MyLock.Release()
    .
            
!    db.debugout('[Rep_GetNextID]  -end- Table: ' & CLIP(TableName) & '   RecordID: ' & RecordID)
!    original
!
!    ! (FileManager, Key, *?, String),ULong
!    ! (FileManager FM, Key PrimaryKey, *? RecordID, String EssenceType)
!
!    FileState   = FM.SaveBuffer()
!
!
!    cf#         = PrimaryKey{PROP:Field,1}      ! 1st field of primary key
!    FieldName   = FM.File{PROP:Label,cf#}       ! Field name for this field
!    l#          = Len(Clip(FieldName))          ! Length of this field name
!    p#          = Instring(':',FieldName,1,1)   ! Prefix end position
!    FieldName   = FieldName[p# + 1 : l#]        ! Extract actual field name (external label??)
!
!    TableName   = FM.GetName()                  ! Get table name
!
!    ! Why > 0?
!    SQLRequest  = 'SELECT Max(' & FieldName & ') FROM ' & TableName & ' WHERE ' & FieldName & ' >0'
!
!    NextID      = 1
!
!  !  message(NextID)
!
!    Access:SQL_File.Open()
!    Access:SQL_File.UseFile()
!
!  !  lock(SQL_FILE)
!  !  EMPTY(SQL_FILE)
!
!    RecordID    = 0
!
!    SQL_File{Prop:SQL} = SQLRequest
!    Access:SQL_File.Next()
!    IF ~ERRORCODE()
!       RecordID = SQL:Col_1
!       IF RecordID > 0 THEN NextID = RecordID + 1.
!    .
!
!    Access:SQL_File.Close()
!  !  message('out - ' & NextID)
!
!    ! Compare new ID with the pool
!    Access:LocalIDPool.Open()
!    Access:LocalIDPool.UseFile()
!    LIP:EssenceType = EssenceType
!    ans#            = Access:LocalIDPool.Fetch(LIP:By_EssenceType)
!    LastID = LIP:LastID
!    IF NextID <= LastID
!       NextID = LastID + 1
!    .
!    ! Updating the pool
!    LIP:EssenceType = EssenceType
!    LIP:LastID      = NextID
!    IF ans# = Level:Benign then
!       Access:LocalIDPool.Update()
!    ELSE
!       Access:LocalIDPool.Insert()
!    .
!    Access:LocalIDPool.Close()
!
!    FM.RestoreBuffer(FileState)
!
!    RETURN NextId
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(NextId)

    Exit

CloseFiles     Routine
    Relate:ReplicationIDControl.Close()
    Relate:_SQLTemp.Close()
    Relate:ReplicationTableIDs.Close()
    Exit
MyC.Get_Range   PROCEDURE()
    CODE
    TableName   = FM.GetName()                  ! Get table name

    ! Get available range and Last used ID

    _SQLTemp{PROP:SQL}  = 'SELECT RangeFrom, RangeTo '  & |
                 'FROM ReplicationIDControl '           & |
                 'WHERE ReplicatedDatabaseID = '        & GLO:ReplicatedDatabaseID & ' ' & |
                 'ORDER BY RepCID DESC'

    IF Access:_SQLTemp.Next() = LEVEL:Benign
       LG:RangeFrom = _SQ:S1
       LG:RangeTo   = _SQ:S2
    .

    _SQLTemp{PROP:SQL}  = 'SELECT LastID '      & |
                 'FROM ReplicationTableIDs '    & |
                 'WHERE TableName = <39>'       & TableName & '<39> '   & |
                 'AND ReplicatedDatabaseID = '  & GLO:ReplicatedDatabaseID
    IF Access:_SQLTemp.Next() = LEVEL:Benign
       LastID       = _SQ:S1
    .
    RETURN


MyC.Get_Max     PROCEDURE()

RecID       LONG
    CODE
    FileState   = FM.SaveBuffer()

    cf#         = PrimaryKey{PROP:Field,1}      ! 1st field of primary key
    FieldName   = FM.File{PROP:Label,cf#}       ! Field name for this field
    l#          = LEN(CLIP(FieldName))          ! Length of this field name
    p#          = INSTRING(':',FieldName,1,1)   ! Prefix end position
    FieldName   = FieldName[p# + 1 : l#]        ! Extract actual field name (external label??)

    LOOP 3 TIMES
       IF LG:RangeFrom < LastID
          LG:RangeFrom  = LastID
       .

       SQLRequest  = 'SELECT Max(' & FieldName & ') FROM ' & TableName & ' ' & |
                    'WHERE ' & FieldName & ' >= ' & LG:RangeFrom & ' AND ' & FieldName & ' <= ' & LG:RangeTo

       NextID      = 1
       RecID       = 0

       _SQLTemp{Prop:SQL} = SQLRequest
       IF Access:_SQLTemp.Next() = LEVEL:Benign
!       IF ~ERRORCODE()
          RecID     = _SQ:S1
          IF RecID > 0
             NextID = RecID + 1
       .  .

       IF NextID = 1
          NextID    = LG:RangeFrom
          IF NextID = 0
            db.debugout('[Rep_GetNextID]  RangeFrom is 0!!!!!!!!!!!!!!!!!!!')
       .  .

    ! TEst code!   this is to purposefully make it fail

    db.debugout('[Rep_GetNextID]  NextID: ' & NextID & ',  LastID: ' & LastID)

!    _SQLTemp{Prop:SQL} = 'UPDATE ReplicationTableIDs SET LastID = ' & NextID & ' WHERE TableName = <39>' & TableName & |
!                            '<39> AND LastID = ' & LastID

       IF SELF.Upd_Last() >= 0
          BREAK
       .
       NextID       = -1
    .

    FM.RestoreBuffer(FileState)
    RETURN



MyC.Upd_Last        PROCEDURE()
L:Ret   LONG
    CODE
    ! Check that the Table is in the table
    REPT:ReplicatedDatabaseID   = GLO:ReplicatedDatabaseID
    REPT:TableName              = TableName
    IF Access:ReplicationTableIDs.TryFetch(REPT:Key_ReplicationDatabaseIDTableName) ~= LEVEL:Benign
       CLEAR(REPT:Record)
       IF Access:ReplicationTableIDs.PrimeRecord() = LEVEL:Benign
          REPT:ReplicatedDatabaseID = GLO:ReplicatedDatabaseID
          REPT:TableName            = TableName
          IF Access:ReplicationTableIDs.Insert() = LEVEL:Benign
    .  .  .


    _SQLTemp{Prop:SQL} = 'SELECT LastID FROM ReplicationTableIDs WHERE TableName = <39>' & TableName & |
                            '<39> AND LastID = ' & LastID & ' AND ReplicatedDatabaseID = '  & GLO:ReplicatedDatabaseID
    IF Access:_SQLTemp.Next() ~= LEVEL:Benign
       L:Ret        = -1                        ! Already taken
    ELSE
       _SQLTemp{Prop:SQL} = 'UPDATE ReplicationTableIDs SET LastID = ' & NextID & ' WHERE TableName = <39>' & TableName & |
                               '<39> AND LastID = ' & LastID & ' AND ReplicatedDatabaseID = '  & GLO:ReplicatedDatabaseID
       IF ERRORCODE()
          L:Ret     = -2
       ELSE
          _SQLTemp{Prop:SQL} = 'SELECT LastID FROM ReplicationTableIDs WHERE TableName = <39>' & TableName & '<39> AND ReplicatedDatabaseID = '  & GLO:ReplicatedDatabaseID
          IF Access:_SQLTemp.Next() ~= LEVEL:Benign
             L:Ret  = -3
          ELSIF DEFORMAT(_SQ:S1) ~= NextID
             L:Ret  = -4
    .  .  .
    RETURN(L:Ret)



!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Check_Replication    PROCEDURE                             ! Declare Procedure
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ReplicationIDControl.Open()
     .
     Access:ReplicationIDControl.UseFile()
    IF GLO:ReplicatedDatabaseID = 0
       IF ~ERRORCODE()
          IF RECORDS(ReplicationIDControl) > 0
             MESSAGE('There are Replication ID Control Records in this database but there is no Replication ID set for your TransIS client.||If you are accessing a replicated database please immediately set a Replication ID under Setup pull down menu and My Settings.', 'Replication Check', ICON:Exclamation)
       .  .
    .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:ReplicationIDControl.Close()
    Exit
