

   MEMBER('TRANSIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('TRN_D002.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_Delivery_ManIDs  PROCEDURE  (p:DID, p:No, p:MAN_DIID_List, p:List_Option, p:DIID) ! Declare Procedure
LOC:Loaded           ULONG                                 ! Number of units
LOC:Man_IDs_Q        QUEUE,PRE(L_MQ)                       ! 
MID                  ULONG                                 ! Manifest ID
DIID                 ULONG                                 ! Delivery Item ID
                     END                                   ! 
LOC:Idx              LONG                                  ! 
Tek_Failed_File     STRING(100)

Items_View       VIEW(DeliveryItemAlias)                ! Delivery Items - Alias
    PROJECT(A_DELI:DIID, A_DELI:DID, A_DELI:ItemNo)
       JOIN(A_MALD:FKey_DIID, A_DELI:DIID)              ! Manifest Load Deliveries - Alias
       PROJECT(A_MALD:MLID, A_MALD:DIID)
          JOIN(A_MAL:PKey_MLID, A_MALD:MLID)            ! Manifest Load - Alias
          PROJECT(A_MAL:MLID, A_MAL:MID)
    .  .  .
    


View_Items      ViewManager




!ManLoad_View       VIEW(ManifestLoadAlias)
!    PROJECT(A_MAL:MLID, A_MAL:MID)
!       JOIN(A_MALD:FSKey_MLID_DIID, A_MAL:MLID)
!       PROJECT(A_MALD:MLID, A_MALD:DIID)
!          JOIN(A_DELI:PKey_DIID, A_MALD:DIID)
!          PROJECT(A_DELI:DIID, A_DELI:DID, A_DELI:ItemNo)
!    .  .  .

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadAlias.Open()
     .
     Access:ManifestLoadDeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
     Access:ManifestLoadAlias.UseFile()
    ! (p:DID, p:No, p:MAN_DIID_List, p:List_Option, p:DIID)     , LOC:Loaded
    ! (ULONG, SHORT, <*STRING>, BYTE=0, ULONG=0),ULONG, PROC
    ! Look through all the Items for the Delivery and enumerate the MAN:MIDs into a Q
    ! p:No
    !   0. Return number of different MIDs (for this DID)
    !   x. Return this number MID - return the nth MID (for this DID)
    !
    ! p:List_Option
    !   0. Return list with MID, DIID, MID, DIID
    !   x. Return list with MID, MID
    !
    ! If not omitted - p:MAN_DIID_List
    !       Load it with MID,DIID,MID,DIID - which is the MIDs and the 1st DIID on each
    !
    ! A delivery item can be loaded on different trailers - i.e. different ManLoads
    ! Item can appear on more than one ManifestLoad but a single ManifestLoad can only have 1 instance of the Item
    !
    ! If p:DIID provided then only get for this DIID

    PUSHBIND

    FREE(LOC:Man_IDs_Q)

    BIND('A_MAL:MID', A_MAL:MID)

    View_Items.Init(Items_View, Relate:DeliveryItemAlias)
    View_Items.AddSortOrder(A_DELI:FKey_DID_ItemNo)
    !View_Items.AppendOrder('A_DELI:DID, A_DELI:ItemNo')    - redundant
    View_Items.AddRange(A_DELI:DID, p:DID)
    View_Items.SetFilter('A_MAL:MID~=0','1')                ! Not interested when no MID present, should also be no Man.Load.Del

    IF p:DIID ~= 0
       BIND('A_MALD:DIID', A_MALD:DIID)
       View_Items.SetFilter('A_MALD:DIID=' & p:DIID,'2')
    .

    View_Items.Reset()

    LOOP
       IF View_Items.Next() ~= LEVEL:Benign
          BREAK
       .

       L_MQ:MID       = A_MAL:MID
       GET(LOC:Man_IDs_Q, L_MQ:MID)
       IF ERRORCODE()
          ! Record this MID and the 1st DIID on it
          L_MQ:MID    = A_MAL:MID
          L_MQ:DIID   = A_DELI:DIID

          ADD(LOC:Man_IDs_Q, L_MQ:MID)
    .  .

    View_Items.Kill()



    ! Get return field or count
    IF p:No = 0
       LOC:Loaded       = RECORDS(LOC:Man_IDs_Q)
    ELSE
       GET(LOC:Man_IDs_Q, p:No)
       IF ~ERRORCODE()
          LOC:Loaded    = L_MQ:MID
    .  .


    ! Accumulate IDs into optional passed list
    IF ~OMITTED(3)
       LOC:Idx      = 0
       LOOP
          LOC:Idx  += 1
          GET(LOC:Man_IDs_Q, LOC:Idx)
          IF ERRORCODE()
             BREAK
          .
          IF p:List_Option = 0
             IF CLIP(p:MAN_DIID_List) = ''
                p:MAN_DIID_List    = L_MQ:MID & ',' & L_MQ:DIID
             ELSE
                p:MAN_DIID_List    = CLIP(p:MAN_DIID_List) & ',' & L_MQ:MID & ',' & L_MQ:DIID
             .
          ELSE
             IF CLIP(p:MAN_DIID_List) = ''
                p:MAN_DIID_List    = L_MQ:MID
             ELSE
                p:MAN_DIID_List    = CLIP(p:MAN_DIID_List) & ',' & L_MQ:MID
    .  .  .  .

    UNBIND('A_MAL:MID')
    IF p:DIID ~= 0
       UNBIND('A_MALD:DIID')
    .
    POPBIND
!   old 2
!    ! (p:DID, p:No, p:MAN_DIID_List, p:List_Option)
!    ! Look through all the Items for the Delivery and enumerate the MAN:MIDs into a Q
!    ! p:No
!    !   0. Return number of different MIDs (for this DID)
!    !   x. Return this number MID - return the nth MID (for this DID)
!    !
!    ! p:List_Option
!    !   0. Return list with MID, DIID, MID, DIID
!    !   x. Return list with MID, MID
!    !
!    ! If not omitted - p:MAN_DIID_List
!    !       Load it with MID,DIID,MID,DIID - which is the MIDs and the 1st DIID on each
!    !
!    ! A delivery item can be loaded on different trailers - i.e. different ManLoads
!    ! Item can appear on more than one ManifestLoad but a single ManifestLoad can only have 1 instance of the Item
!    PUSHBIND
!
!    FREE(LOC:Man_IDs_Q)     ! No caching at the moment
!    SET(A_DELI:FKey_DID_ItemNo)             !, A_DELI:FKey_DID_ItemNo)
!
!    BIND('A_DELI:DID',A_DELI:DID)
!    Items_View{PROP:Filter} = 'A_DELI:DID = ' & p:DID
!    Items_View{PROP:Order}  = 'A_DELI:DID, A_DELI:ItemNo'
!    OPEN(Items_View)
!    SET(Items_View)
!    IF ERRORCODE()
!       MESSAGE('Error on Open View - Items_View.||Error: ' & ERROR() & '|F Error: ' & FILEERROR(), 'Get_Delivery_ManIDs', ICON:Hand)
!    ELSE
!       LOOP
!          NEXT(Items_View)
!          IF ERRORCODE()
!!    message('Next view error: ' & ERROR() & '||F Err: ' & FILEERROR() & '||Recs Q: ' & RECORDS(LOC:Man_IDs_Q))
!             BREAK
!          .
!          IF A_MAL:MID = 0                      ! Not interested when no MID present
!             CYCLE
!          .
!
!          L_MQ:DID       = p:DID
!          L_MQ:MID       = A_MAL:MID
!          GET(LOC:Man_IDs_Q, L_MQ:MID, L_MQ:DID)
!          IF ERRORCODE()
!             ! Record this MID and the 1st DIID on it
!             L_MQ:DID    = p:DID
!             L_MQ:MID    = A_MAL:MID
!
!             L_MQ:DIID   = A_DELI:DIID
!             ADD(LOC:Man_IDs_Q, L_MQ:DID, L_MQ:MID)
!       .  .
!       CLOSE(Items_View)
!    .
!
!
!    ! NOTE: We can only have 1 DID due to Filter condition above - so no need for Offset
!    ! Find our offset for this DID
!!       L_MQ:DID        = p:DID
!!       GET(LOC:Man_IDs_Q, L_MQ:DID)
!!       IF ~ERRORCODE()
!!          LOC:Off_Set  = POINTER(LOC:Man_IDs_Q) - 1
!!       .
!
!    ! Get return field or count
!    IF p:No = 0
!       ! Count matching entries - start at 1st one
!       !LOC:Idx          = LOC:Off_Set
!       LOC:Idx          = 0
!       LOOP
!          LOC:Idx      += 1
!          GET(LOC:Man_IDs_Q, LOC:Idx)
!          IF ERRORCODE()
!             BREAK
!          .
!          IF L_MQ:DID ~= p:DID                              ! This is redundant - impossible to have more than 1 DID
!             BREAK                                          ! Our filter condition excludes this posibility
!          .
!
!          LOC:Loaded   += 1                                 !           = RECORDS(LOC:Man_IDs_Q)
!       .
!    ELSE
!       GET(LOC:Man_IDs_Q, p:No)     ! + LOC:Off_Set)
!       IF ~ERRORCODE() AND L_MQ:DID = p:DID
!          LOC:Loaded    = L_MQ:MID
!    .  .
!
!
!    ! Accumulate IDs into optional passed list
!    IF ~OMITTED(3)
!       LOC:Idx      = 0
!       LOOP
!          LOC:Idx  += 1
!          GET(LOC:Man_IDs_Q, LOC:Idx)
!          IF ERRORCODE()
!             BREAK
!          .
!          IF p:List_Option = 0
!             IF CLIP(p:MAN_DIID_List) = ''
!                p:MAN_DIID_List    = L_MQ:MID & ',' & L_MQ:DIID
!             ELSE
!                p:MAN_DIID_List    = CLIP(p:MAN_DIID_List) & ',' & L_MQ:MID & ',' & L_MQ:DIID
!             .
!          ELSE
!             IF CLIP(p:MAN_DIID_List) = ''
!                p:MAN_DIID_List    = L_MQ:MID
!             ELSE
!                p:MAN_DIID_List    = CLIP(p:MAN_DIID_List) & ',' & L_MQ:MID
!    .  .  .  .
!
!    POPBIND












!   old
!    ! (p:DID, p:No, p:MAN_DIID_List, p:List_Option)
!    ! Look through all the Items for the Delivery and enumerate the MAN:MIDs into a Q
!    ! p:No
!    !   0. Return number of different MIDs
!    !   x. Return this number MID
!    !
!    ! If not omitted - p:MAN_DIID_List
!    !       Load it with MID,DIID,MID,DIID - which is the MIDs and the 1st DIID on each
!    !
!    ! A delivery item can be loaded on different trailers - i.e. different ManLoads
!    ! Item can appear on more than one ManifestLoad but a single ManifestLoad can only have 1 instance of the Item
!    PUSHBIND
!
!    FREE(LOC:Man_IDs_Q)     ! No caching at the moment
!    SET(A_DELI:FKey_DID_ItemNo)             !, A_DELI:FKey_DID_ItemNo)
!
!    BIND('A_DELI:DID',A_DELI:DID)
!    Items_View{PROP:Filter} = 'A_DELI:DID = ' & p:DID
!    Items_View{PROP:Order}  = 'A_DELI:DID, A_DELI:ItemNo'
!    OPEN(Items_View)
!    SET(Items_View)
!    IF ERRORCODE()
!       MESSAGE('Error on Open View - Items_View.||Error: ' & ERROR() & '|F Error: ' & FILEERROR(), 'Get_Delivery_ManIDs', ICON:Hand)
!    ELSE
!       LOOP
!          NEXT(Items_View)
!          IF ERRORCODE()
!!    message('Next view error: ' & ERROR() & '||F Err: ' & FILEERROR() & '||Recs Q: ' & RECORDS(LOC:Man_IDs_Q))
!             BREAK
!          .
!          IF A_MAL:MID = 0                      ! Not interested when no MID present
!             CYCLE
!          .
!
!          L_MQ:DID       = p:DID
!          L_MQ:MID       = A_MAL:MID
!          GET(LOC:Man_IDs_Q, L_MQ:MID, L_MQ:DID)
!          IF ERRORCODE()
!             ! Record this MID and the 1st DIID on it
!             L_MQ:DID    = p:DID
!             L_MQ:MID    = A_MAL:MID
!
!             L_MQ:DIID   = A_DELI:DIID
!             ADD(LOC:Man_IDs_Q, L_MQ:DID, L_MQ:MID)
!       .  .
!       CLOSE(Items_View)
!    .
!
!
!    ! Find our offset for this DID
!    L_MQ:DID        = p:DID
!    GET(LOC:Man_IDs_Q, L_MQ:DID)
!    IF ~ERRORCODE()
!       LOC:Off_Set  = POINTER(LOC:Man_IDs_Q) - 1
!    .
!
!
!    ! Get return field or count
!    IF p:No = 0
!       ! Count matching entries - start at 1st one
!       LOC:Idx          = LOC:Off_Set
!       LOOP
!          LOC:Idx      += 1
!          GET(LOC:Man_IDs_Q, LOC:Idx)
!          IF ERRORCODE()
!             BREAK
!          .
!          IF L_MQ:DID ~= p:DID                              ! This is redundant - impossible to have more than 1 DID
!             BREAK                                          ! Our filter condition excludes this posibility
!          .
!
!          LOC:Loaded   += 1                                 !           = RECORDS(LOC:Man_IDs_Q)
!       .
!    ELSE
!       GET(LOC:Man_IDs_Q, p:No + LOC:Off_Set)
!       IF ~ERRORCODE() AND L_MQ:DID = p:DID
!          LOC:Loaded    = L_MQ:MID
!    .  .
!
!
!    ! Accumulate IDs into optional passed list
!    IF ~OMITTED(3)
!       LOC:Idx      = 0
!       LOOP
!          LOC:Idx  += 1
!          GET(LOC:Man_IDs_Q, LOC:Idx)
!          IF ERRORCODE()
!             BREAK
!          .
!          IF p:List_Option = 0
!             IF CLIP(p:MAN_DIID_List) = ''
!                p:MAN_DIID_List    = L_MQ:MID & ',' & L_MQ:DIID
!             ELSE
!                p:MAN_DIID_List    = CLIP(p:MAN_DIID_List) & ',' & L_MQ:MID & ',' & L_MQ:DIID
!             .
!          ELSE
!             IF CLIP(p:MAN_DIID_List) = ''
!                p:MAN_DIID_List    = L_MQ:MID
!             ELSE
!                p:MAN_DIID_List    = CLIP(p:MAN_DIID_List) & ',' & L_MQ:MID
!    .  .  .  .
!
!    POPBIND














!   old
!    ! (p:DID, p:No, p:MAN_DIID_List)
!    ! Look through all the Items for the Delivery and enumerate the MAN:MIDs into a Q
!    ! p:No
!    !   0. Return number of different MIDs
!    !   x. Return this number MID
!    !
!    ! If not omitted - p:MAN_DIID_List
!    !       Load it with MID,DIID,MID,DIID - which is the MIDs and the 1st DIID on each
!    !
!    ! A delivery item can be loaded on different trailers - i.e. different ManLoads
!    ! Item can appear on more than one ManifestLoad but a single ManifestLoad can only have 1 instance of the Item
!
!
!    maybe cache this based on a global
!
!
!    CLEAR(A_DELI:Record,-1)
!    A_DELI:DID    = p:DID
!    SET(A_DELI:FKey_DID_ItemNo, A_DELI:FKey_DID_ItemNo)
!    LOOP
!       IF Access:DeliveryItemAlias.TryNext() ~= LEVEL:Benign
!          BREAK
!       .
!       IF A_DELI:DID ~= p:DID
!          BREAK
!       .
!
!       ! Look through the ManLoads its on
!       CLEAR(A_MALD:Record,-1)
!       A_MALD:DIID          = A_DELI:DID
!       SET(A_MALD:FKey_DIID, A_MALD:FKey_DIID)
!       LOOP
!          IF Access:ManifestLoadDeliveriesAlias.TryNext() ~= LEVEL:Benign
!             BREAK
!          .
!          IF A_MALD:DIID ~= A_DELI:DID
!             BREAK
!          .
!
!          MAL:MLID          = A_MALD:MLID
!          IF Access:ManifestLoad.TryFetch(MAL:PKey_MLID) = LEVEL:Benign
!!             L_MQ:DID       = p:DID
!             L_MQ:MID       = MAL:MID
!             GET(LOC:Man_IDs_Q, L_MQ:MID)
!             IF ERRORCODE()
!                ! Record this MID and the 1st DIID on it
!                L_MQ:DID    = p:DID
!                L_MQ:MID    = MAL:MID
!
!                L_MQ:DIID   = A_DELI:DIID
!                ADD(LOC:Man_IDs_Q)
!    .  .  .  .
!
!
!    IF p:No = 0
!       LOC:Loaded           = RECORDS(LOC:Man_IDs_Q)
!    ELSE
!       GET(LOC:Man_IDs_Q, p:No)
!       IF ~ERRORCODE()
!          LOC:Loaded        = L_MQ:MID
!    .  .
!
!
!    IF ~OMITTED(3)
!       LOC:Idx      = 0
!       LOOP
!          LOC:Idx  += 1
!          GET(LOC:Man_IDs_Q, LOC:Idx)
!!          message('LOC:Idx: ' & LOC:Idx)
!          IF ERRORCODE()
!             BREAK
!          .
!          IF CLIP(p:MAN_DIID_List) = ''
!             p:MAN_DIID_List    = L_MQ:MID & ',' & L_MQ:DIID
!          ELSE
!             p:MAN_DIID_List    = CLIP(p:MAN_DIID_List) & ',' & L_MQ:MID & ',' & L_MQ:DIID
!    .  .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Loaded)

    Exit

CloseFiles     Routine
    Relate:ManifestLoadDeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Relate:ManifestLoadAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! record not returned
!!! </summary>
Get_InvTransporter_Credited PROCEDURE  (p:TIN, p:Option, p:TIN_to_Ignore, p:Type, p:ToDate) ! Declare Procedure
LOC:Total            DECIMAL(15,2)                         ! 
LOC:Buffer_ID        USHORT                                ! 
Tek_Failed_File     STRING(100)

View_Inv            VIEW(InvoiceTransporterAlias)
    PROJECT(A_INT:TIN, A_INT:Cost, A_INT:VAT)
    .


Inv_View            ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceTransporterAlias.Open()
     .
     Access:InvoiceTransporterAlias.UseFile()
    ! (p:TIN, p:Option, p:TIN_to_Ignore, p:Type, p:ToDate)
    ! Accumulate the Total Credits / Additionals for an Invoice
    !
    ! p:Options
    !   0.  All
    !   1.  Credits
    !   2.  Debits
    !
    ! p:TIN_to_Ignore
    !   If present then ignore this TIN in calculation

    LOC:Buffer_ID   = Access:InvoiceTransporterAlias.SaveBuffer()
    IF p:ToDate ~= 0
       BIND('A_INT:InvoiceDate',A_INT:InvoiceDate)
    .

    Inv_View.Init(View_Inv, Relate:InvoiceTransporterAlias)
    Inv_View.AddSortOrder(A_INT:Key_CR_TIN)

    !Inv_View.AppendOrder()
    Inv_View.AddRange(A_INT:CR_TIN, p:TIN)
    IF p:ToDate ~= 0
       Inv_View.SetFilter('A_INT:InvoiceDate < ' & p:ToDate + 1)
    .

    Inv_View.Reset()
    LOOP
       IF Inv_View.Next() ~= LEVEL:Benign
          BREAK
       .

       IF p:TIN_to_Ignore ~= 0
          IF p:TIN_to_Ignore = A_INT:TIN
             CYCLE
       .  .

       CASE p:Option
       OF 1                 ! Only Credits
          IF A_INT:Cost >= 0.0
             CYCLE
          .
       OF 2                 ! Only Debits
          IF A_INT:Cost <= 0.0
             CYCLE
       .  .

       IF p:Type = 1
          LOC:Total    += 1
       ELSE
          LOC:Total    += A_INT:Cost + A_INT:VAT
    .  .

    Inv_View.Kill()

    IF p:ToDate ~= 0
       UNBIND('A_INT:InvoiceDate')
    .
    Access:InvoiceTransporterAlias.RestoreBuffer(LOC:Buffer_ID)
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Total)

    Exit

CloseFiles     Routine
    Relate:InvoiceTransporterAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_TransportersPay_Alloc_Amt PROCEDURE  (p:ID, p:Option, p:ToDate) ! Declare Procedure
LOC:Paid_Dec         DECIMAL(15,2)                         ! 
LOC:Return           STRING(30)                            ! 
CliPay_View         VIEW(TransporterPaymentsAllocationsAlias)
    PROJECT(A_TRAPA:TRPAID, A_TRAPA:TPID, A_TRAPA:AllocationNo, A_TRAPA:TIN, A_TRAPA:Amount)
    .



CP_View             ViewManager

  CODE
    ! (p:ID, p:Option, p:ToDate)
    ! Get the total Payments for an Transporter Invoice or a Transporter Payment
    !
    ! p:Option
    !   0.  Invoice Paid
    !   1.  Clients Payments Allocation total

!          db.debugout('[Get_ClientsPay_Alloc_Amt]  Option: ' & p:Option & ',  ID: ' & p:ID)

    Relate:TransporterPaymentsAlias.Open()
    Relate:TransporterPaymentsAllocationsAlias.Open()

    Access:TransporterPaymentsAlias.UseFile()
    Access:TransporterPaymentsAllocationsAlias.UseFile()

    IF p:Option > 1
       MESSAGE('The option does not exist: ' & p:Option, 'Get_TransportersPay_Alloc_Amt', ICON:Hand)
    ELSE
       IF p:ToDate ~= 0
          BIND('A_TRAP:DateMade', A_TRAP:DateMade)
       .

       CP_View.Init(CliPay_View, Relate:TransporterPaymentsAllocationsAlias)

       CASE p:Option
       OF 0
          CP_View.AddSortOrder(A_TRAPA:FKey_TIN)
          CP_View.AppendOrder('A_TRAPA:TPID,A_TRAPA:AllocationNo')
          CP_View.AddRange(A_TRAPA:TIN, p:ID)
       OF 1                    
          CP_View.AddSortOrder(A_TRAPA:FKey_TPID_AllocationNo)
          CP_View.AddRange(A_TRAPA:TPID, p:ID)
       .

       IF p:ToDate ~= 0
          CP_View.SetFilter('A_TRAP:DateMade < ' & p:ToDate + 1)
       .

       CP_View.Reset()
       LOOP
          IF CP_View.Next() ~= LEVEL:Benign
             BREAK
          .

          LOC:Paid_Dec     += A_TRAPA:Amount
       .


       ! We need to check if these payments have been credited...
       IF p:Option = 1
          A_TRAP:TPID_Reversal    = p:ID
          IF Access:TransporterPaymentsAlias.TryFetch(A_TRAP:SKey_TPID_Reversal) = LEVEL:Benign
             ! This CPID has been reversed.. add these up too
             CP_View.AddSortOrder(A_TRAPA:FKey_TPID_AllocationNo)
             CP_View.AppendOrder('A_TRAPA:AllocationNo')
             CP_View.AddRange(A_TRAPA:TPID, A_TRAP:TPID)

             CP_View.SetSort(2)

             CP_View.Reset()
             LOOP
                IF CP_View.Next() ~= LEVEL:Benign
                   BREAK
                .

                LOC:Paid_Dec     += A_TRAPA:Amount
       .  .  .

       CP_View.Kill()

       IF p:ToDate ~= 0
          UNBIND('A_TRAP:DateMade')
    .  .

    LOC:Return  = LOC:Paid_Dec

    Relate:TransporterPaymentsAlias.Close()
    Relate:TransporterPaymentsAllocationsAlias.Close()
    RETURN(LOC:Return)

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Add_Log_h            PROCEDURE  (p1,p2,p3)                 ! Declare Procedure

  CODE
    Add_Log(p1,p2,p3)
    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_ManLoadItem_Units PROCEDURE  (p:DIID)                  ! Declare Procedure
LOC:Loaded           USHORT                                ! Number of units
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_SQLTemp.Open()
     .
     Access:ManifestLoadDeliveriesAlias.UseFile()
     Access:_SQLTemp.UseFile()
    ! (p:DIID)
    ! A delivery item can be loaded on different trailers
    ! Item can appear on more than one ManifestLoad but a single ManifestLoad can only have 1 instance of the Item

    _SQLTemp{PROP:SQL}  = 'SELECT SUM(UnitsLoaded) FROM ManifestLoadDeliveries WHERE DIID = ' & p:DIID
    IF ERRORCODE()
       MESSAGE('SQL ERROR: ' & FILEERROR(), 'Get_ManLoadItem_Units', ICON:Hand)
    ELSE
       NEXT(_SQLTemp)
       IF ~ERRORCODE()
          LOC:Loaded    = _SQ:S1
    .  .



!    CLEAR(A_MALD:Record,-1)
!    A_MALD:DIID          = p:DIID
!    SET(A_MALD:FKey_DIID, A_MALD:FKey_DIID)
!    LOOP
!       IF Access:ManifestLoadDeliveriesAlias.TryNext() ~= LEVEL:Benign
!          BREAK
!       .
!       IF A_MALD:DIID ~= p:DIID
!          BREAK
!       .
!
!       LOC:Loaded       += A_MALD:UnitsLoaded
!    .

    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Loaded)

    Exit

CloseFiles     Routine
    Relate:ManifestLoadDeliveriesAlias.Close()
    Relate:_SQLTemp.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! Returns delivered units of completed trips
!!! </summary>
Get_TripItem_Del_Units PROCEDURE  (p:DIID, p:Option)       ! Declare Procedure
LOC:Loaded           USHORT                                ! Number of units
LOC:ToAdd            LONG                                  ! 
Tek_Failed_File     STRING(100)

View_TD         VIEW(TripSheetDeliveriesAlias)
    PROJECT(A_TRDI:DIID, A_TRDI:UnitsDelivered, A_TRDI:UnitsNotAccepted, A_TRDI:UnitsLoaded)
    .

View_TD2        VIEW(TripSheetDeliveriesAlias)
    PROJECT(A_TRDI:DIID, A_TRDI:UnitsDelivered, A_TRDI:UnitsNotAccepted, A_TRDI:TRID, A_TRDI:UnitsLoaded)
       JOIN(A_TRI:PKey_TID, A_TRDI:TRID)
       PROJECT(A_TRI:TRID,A_TRI:State)
    .  .

Trip_View       ViewManager





    

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetsAlias.Open()
     .
     Access:TripSheetDeliveriesAlias.UseFile()
     Access:TripSheetsAlias.UseFile()
    ! (p:DIID, p:Option)
    !   p:Option
    !       0. Delivered Units
    !       1. (Delivered & Un Delivered) on incomplete Trips
    !       2. Delivered & Rejected
    !       3. (Delivered & Rejected & Un Delivered) on incomplete Trips
    !       4. Delivered & Rejected & Un Delivered on ALL Trips
    ! A delivery item can be loaded on different trailers
    ! Item can appear on more than one ManifestLoad but a single ManifestLoad can only have 1 instance of the Item

!    db.debugout('[Get_TripItem_Del_Units]  -----------  In  -----------   p:DIID: ' & p:DIID & ',   p:Option: ' & p:Option)

    IF p:Option = 1 OR p:Option = 3 OR p:Option = 4
       PUSHBIND()
       BIND('A_TRI:State',A_TRI:State)
       BIND('A_TRDI:UnitsDelivered',A_TRDI:UnitsDelivered)
       BIND('A_TRDI:UnitsNotAccepted',A_TRDI:UnitsNotAccepted)
    .

    IF p:Option = 1 OR p:Option = 3 OR p:Option = 4
       Trip_View.Init(View_TD2, Relate:TripSheetDeliveriesAlias)
    ELSE
       Trip_View.Init(View_TD, Relate:TripSheetDeliveriesAlias)
    .
    Trip_View.AddSortOrder(A_TRDI:FKey_DIID)
    Trip_View.AppendOrder('A_TRDI:TDID')
    Trip_View.AddRange(A_TRDI:DIID,p:DIID)

    IF p:Option = 1 OR p:Option = 3
       ! Loading, Loaded, On Route, Transferred, Finalised
       !    1       2         3          4            5
!       Trip_View.SetFilter('A_TRI:State < 4 AND A_TRDI:UnitsDelivered = 0 AND A_TRDI:UnitsNotAccepted = 0')
       Trip_View.SetFilter('A_TRI:State < 4')       ! 27 Feb 07
     .

    Trip_View.Reset()
!    IF ERRORCODE()
!       MESSAGE('Error on Reset: ' & ERROR())
!    .
    LOOP
       IF Trip_View.Next() ~= LEVEL:Benign
          BREAK
       .

   db.debugout('[Get_TripItem_Del_Units]  p:DIID: ' & p:DIID & ',   A_TRDI:UnitsDelivered: ' & A_TRDI:UnitsDelivered & ',   A_TRDI:UnitsNotAccepted: ' & A_TRDI:UnitsNotAccepted & ',   A_TRDI:UnitsLoaded: ' & A_TRDI:UnitsLoaded & ',   p:Option: ' & p:Option)

       IF p:Option < 2
          LOC:ToAdd     = A_TRDI:UnitsDelivered
       ELSE
          LOC:ToAdd     = A_TRDI:UnitsDelivered + A_TRDI:UnitsNotAccepted
       .

       LOC:Loaded      += LOC:ToAdd

       ! Only add in UnitsLoaded when no delivered units and state is not finalised
       IF p:Option = 1 OR p:Option = 3 OR p:Option = 4
          ! add in A_TRDI:UnitsLoaded less any that have already been rejected or delivered
          LOC:Loaded   += (A_TRDI:UnitsLoaded - (A_TRDI:UnitsDelivered + A_TRDI:UnitsNotAccepted))
    .  .
    Trip_View.Kill()

    IF p:Option = 1 OR p:Option = 3 OR p:Option = 4
       UNBIND('A_TRI:State')
       UNBIND('A_TRDI:UnitsDelivered')
       UNBIND('A_TRDI:UnitsNotAccepted')
       POPBIND()
    .

!    db.debugout('[Get_TripItem_Del_Units]  -----------  Out  -----------   LOC:Loaded: ' & LOC:Loaded)
    !   p:Option
    !       0. Delivered Units
    !       1. Delivered & Un Delivered on incomplete Trips
    !       2. Delivered & Rejected
    !       3. Delivered & Rejected & Un Delivered on incomplete Trips
    !       4. Delivered & Rejected & Un Delivered on ALL Trips

!               old
!    ! (p:DIID, p:Option)
!    !   p:Option
!    !       0. Delivered Units
!    !       1. Delivered & Un Delivered on incomplete Trips
!    !       2. Delivered & Rejected
!    !       3. Delivered & Rejected & Un Delivered on incomplete Trips
!    ! A delivery item can be loaded on different trailers
!    ! Item can appear on more than one ManifestLoad but a single ManifestLoad can only have 1 instance of the Item
!
!    CLEAR(A_TRDI:Record,-1)
!    A_TRDI:DIID          = p:DIID
!    SET(A_TRDI:FKey_DIID, A_TRDI:FKey_DIID)
!    LOOP
!       IF Access:TripSheetDeliveriesAlias.TryNext() ~= LEVEL:Benign
!          BREAK
!       .
!       IF A_TRDI:DIID ~= p:DIID
!          BREAK
!       .
!
!       IF p:Option < 2
!          LOC:Loaded       += A_TRDI:UnitsDelivered
!       ELSE
!          LOC:Loaded       += A_TRDI:UnitsDelivered + A_TRDI:UnitsNotAccepted
!       .
!
!       ! Only add in UnitsLoaded when no delivered units
!       IF A_TRDI:UnitsDelivered = 0 AND (p:Option = 1 OR p:Option = 3)
!          A_TRI:TRID        = A_TRDI:TRID
!          IF Access:TripSheetsAlias.TryFetch(A_TRI:PKey_TID) = LEVEL:Benign
!             IF A_TRI:State ~= 4                ! Not finalised
!                ! add in A_TRDI:UnitsLoaded
!                LOC:Loaded += A_TRDI:UnitsLoaded
!    .  .  .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Loaded)

    Exit

CloseFiles     Routine
    Relate:TripSheetDeliveriesAlias.Close()
    Relate:TripSheetsAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt, p:Header) - global
!!! </summary>
Add_Log              PROCEDURE  (STRING p:Text,STRING p:Heading,<STRING p:Name>,BYTE p:Overwrite,BYTE p:Name_Opt,BYTE p:Rollover,BYTE p:Date_Time_Opt,<STRING p:Header>) ! Declare Procedure
LOC:Date             LONG,STATIC                           ! 
LOC:Name             STRING(255)                           ! 
LOC:No               LONG                                  ! 
LOC:Extension        STRING(4)                             ! 
LOC:Name_Static      STRING('TrIS_Def {247}'),STATIC       ! 
LOC:Debug            BYTE                                  ! On or Off`
LOC:NewFile          BYTE                                  ! 
Log_File        FILE,DRIVER('ASCII'),CREATE,PRE(LF),THREAD
Rec         RECORD
Text            STRING(2000)
            .   .

  CODE
    ! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt, p:Header)
    ! (STRING, STRING   , <STRING>, BYTE=0     , BYTE=0    , BYTE=1    , BYTE=1       , <STRING>  )
    !    1        2           3        4           5           6              7           8
    ! p:Name_Opt
    !   0.  - None
    !   1.  - Set static name to p:Name
    !   2.  - Use static name
    ! p:Rollover
    !   0.  - No rollover of file allowed
    !   1.  - Rollover allowed (default)
    ! p:Date_Time_Opt
    !   0.  - Off
    !   1.  - On (default)
    ! p:Header
    !       Used if creating new file and present 
    
    LOC:Debug   = 0

    IF OMITTED(3) OR CLIP(p:Name) = ''
       LOC:Name         = 'TransIS'
    ELSE
       LOC:Name         = CLIP(p:Name)
    .
   
    EXECUTE p:Name_Opt
       LOC:Name_Static  = LOC:Name                  ! p:Name - cant use this as it may be blank, Name will contain it
       LOC:Name         = LOC:Name_Static           ! LOC:Name_Static could be empty, we will default it
    .

    LOC:Extension       = '.log'

    len_#       = LEN(CLIP(LOC:Name))
    dot_pos_#   = INSTRING('.', LOC:Name, -1, len_#)
    IF len_# - dot_pos_# <= 3
       ! Extension is specified!
       LOC:Extension        = SUB(LOC:Name, dot_pos_#, 4)
       LOC:Name             = SUB(LOC:Name, 1, dot_pos_# - 1)
    .

    Log_File{PROP:Name}     = CLIP(LOC:Name) & LOC:Extension
    IF EXISTS(CLIP(Log_File{PROP:Name})) <= 0 OR p:Overwrite = TRUE
       LOC:Date             = 0
       CREATE(Log_File)
       IF ERRORCODE()
          IF OMITTED(3) OR CLIP(p:Name) = ''
             MESSAGE('Failed to create file.||File: ' & CLIP(Log_File{PROP:Name}) & '||Error (' & ERRORCODE() & '): ' & ERROR(), 'Add_Log', ICON:Hand)
          ELSE
             MESSAGE('Failed to create file.||File: ' & CLIP(Log_File{PROP:Name}) & '|(p:Name was: ' & CLIP(p:Name) & ')||Error: ' & ERROR(), 'Add_Log', ICON:Hand)
          .
       ELSE      
          LOC:NewFile = TRUE
    .  .  
         
    LOOP 10 TIMES
       LOC:No   += 1

       SHARE(Log_File)
       IF ERRORCODE()
          IF LOC:No = 1
             ! What now? Try new name
             Log_File{PROP:Name}    = CLIP(LOC:Name) & '-' & LOC:No & LOC:Extension

             IF EXISTS(CLIP(Log_File{PROP:Name})) <= 0
                LOC:Date            = 0
                CREATE(Log_File)
                IF ERRORCODE()
                     MESSAGE('Failed to create file.||File: ' & CLIP(Log_File{PROP:Name}) & '||Error: ' & ERROR(), 'Add_Log', ICON:Hand)
                  ELSE
                     LOC:NewFile = TRUE
             .  .
             CYCLE
          ELSE
             MESSAGE('Failed to create / open log file.||Error: ' & ERROR() & '||File: ' & CLIP(Log_File{PROP:Name}), 'Log File Error', ICON:Hand)
             BREAK
       .  .

       IF p:Rollover = TRUE AND BYTES(Log_File) > 250000
          LOC:Date              = 0

          ! Create a new file: -1 -2 etc
          CLOSE(Log_File)

          Log_File{PROP:Name}   = CLIP(LOC:Name) & '-' & LOC:No & LOC:Extension

          IF EXISTS(CLIP(Log_File{PROP:Name})) <= 0
             LOC:Date           = 0
             CREATE(Log_File)
             IF ERRORCODE()
                  MESSAGE('Failed to create next file.||File: ' & CLIP(Log_File{PROP:Name}) & '||Error: ' & ERROR(), 'Add_Log', ICON:Hand)
               ELSE
                  LOC:NewFile = TRUE
          .  .
          CYCLE
       .

       IF LOC:NewFile AND ~OMITTED(8)    ! Add the header then 
          LF:Text = p:Header
          ADD(Log_File)
       .   

       ! If we get here we have an open file less than 250000 bytes
       IF p:Date_Time_Opt = TRUE
          IF LOC:Date ~= TODAY()
             LOC:Date      = TODAY()
             LF:Text       = '[--' & FORMAT(LOC:Date, @d6) & '--]'
             ADD(Log_File)
          .
               
          LF:Text          = FORMAT(CLOCK(), @t4) & ',[' & CLIP(p:Heading) & '],' & CLIP(p:Text)
       ELSE
          LF:Text          = CLIP(p:Text)
       .

       ADD(Log_File)
!       IF ERRORCODE()
!          MESSAGE('Failed to add to file.||File: ' & CLIP(Log_File{PROP:Name}) & '||Error: ' & ERROR(), 'Add_Log', ICON:Hand)
!       .

       IF LOC:Debug = 1
          db.debugout('[Add_Log]  Error: ' & CLIP(ERROR()) & ',  Name: ' & Log_File{PROP:Name})
       .

       CLOSE(Log_File)
       BREAK
    .

    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! ** global **   ** see thread event limitation notes inside ***
!!! </summary>
Thread_Send_Procs_Events PROCEDURE  (p:Procedure, p:Event) ! Declare Procedure
LOC:Idx              ULONG                                 ! 
LOC:Thread_Q         QUEUE,PRE(L_TQ)                       ! 
Thread               LONG                                  ! 
                     END                                   ! 
LOC:Setup_Group      GROUP,PRE(L_SG),STATIC                ! static
Checked              BYTE                                  ! 
Thread_Send_Proc_Events BYTE                               ! 
                     END                                   ! 

  CODE
    IF L_SG:Checked = FALSE
       L_SG:Checked                 = TRUE
       L_SG:Thread_Send_Proc_Events = GETINI('Setup', 'Thread_Send_Proc_Events', 1, GLO:Local_INI)
    .

    db.debugout('[Thread_Send_Procs_Events]  Start - Proc: ' & CLIP(p:Procedure) & ',  Event: ' & p:Event & ',  Thread_Send_Proc_Events: ' & L_SG:Thread_Send_Proc_Events & ',  Q Recs: ' & RECORDS(GLO:Thread_Q) & '    - Clock: ' & CLOCK())

    IF L_SG:Thread_Send_Proc_Events > 0
       ! (p:Procedure, p:Event)
       ! Check through the Q for threads of this Procedure and post events when found

       ! Of course window events are only received by the top most window of a thread

       LOC:Idx     = 0
       LOOP
          LOC:Idx += 1
          GET(GLO:Thread_Q, LOC:Idx)
          IF ERRORCODE()
             BREAK
          .

          L_TQ:Thread  = GL_TQ:Thread
          GET(LOC:Thread_Q, L_TQ:Thread)
          IF ~ERRORCODE()
             CYCLE
          .

    db.debugout('[Thread_Send_Procs_Events]  GL_TQ:ProcedureName: ' & CLIP(GL_TQ:ProcedureName) & ' = p:Procedure: ' & p:Procedure & ',   Idx: ' & LOC:Idx)

          IF CLIP(UPPER(GL_TQ:ProcedureName)) = CLIP(UPPER(p:Procedure))
             L_TQ:Thread  = GL_TQ:Thread
             ADD(LOC:Thread_Q)

    db.debugout('[Thread_Send_Procs_Events]  Posting for p:Procedure: ' & p:Procedure & '  on thread: ' & GL_TQ:Thread & ',   Idx: ' & LOC:Idx)

             POST(p:Event,, GL_TQ:Thread)
    .  .  .
!!! <summary>
!!! Generated from procedure template - Source
!!! ** global **   
!!! </summary>
Thread_Add_Del_Check_Global PROCEDURE  (p:Procedure, p:Thread, p:Option) ! Declare Procedure
L:Idx                ULONG                                 ! 
LOC:Return           LONG                                  ! 

  CODE
    ! (p:Procedure, p:Thread, p:Option)
    ! p:Option
    !   0. - Add
    !   1. - Delete last instance
    !   2. - Check and return true / false
    !   3. - Check and return no. of instances
    db.debugout('[Thread_Add_Del_Check_Global]  Start - Proc: ' & CLIP(p:Procedure) & ',  Thread: ' & p:Thread & ',  Option: ' & p:Option & '   - Clock: ' & CLOCK())


    IF p:Option = 0
       GL_TQ:ProcedureName     = UPPER(p:Procedure)
       GL_TQ:Thread            = p:Thread
       GL_TQ:Q_ID              = RECORDS(GLO:Thread_Q)
       ADD(GLO:Thread_Q)

       SORT(GLO:Thread_Q, GL_TQ:ProcedureName, GL_TQ:Thread, -GL_TQ:Q_ID)
    ELSIF p:Option = 1
       ! Look for last instance for proc, thread
       L:Idx    = 0
       LOOP
          L:Idx += 1
          GET(GLO:Thread_Q, L:Idx)
          IF ERRORCODE()
             BREAK
          .
          IF GL_TQ:ProcedureName ~= UPPER(p:Procedure) OR GL_TQ:Thread ~= p:Thread
             CYCLE
          .

          DELETE(GLO:Thread_Q)
          BREAK
       .
    ELSIF p:Option = 2
       GL_TQ:ProcedureName      = UPPER(p:Procedure)
       GL_TQ:Thread             = p:Thread

       IF p:Thread = 0
          GET(GLO:Thread_Q, GL_TQ:ProcedureName)
       ELSE
          GET(GLO:Thread_Q, GL_TQ:ProcedureName, GL_TQ:Thread)
       .

       IF ~ERRORCODE()
!       message('found with GL_TQ:ProcedureName: ' & CLIP(GL_TQ:ProcedureName) & ', Thread: ' & GL_TQ:Thread & |
!            ', GL_TQ:Q_ID: ' & GL_TQ:Q_ID)

          LOC:Return            = 1
       .
    ELSE
       L:Idx    = 0
       LOOP
          L:Idx += 1
          GET(GLO:Thread_Q, L:Idx)
          IF ERRORCODE()
             BREAK
          .
          IF CLIP(GL_TQ:ProcedureName) ~= CLIP(UPPER(p:Procedure))
             CYCLE
          .

          LOC:Return   += 1
    .  .

    RETURN(LOC:Return)
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Main_dct             PROCEDURE                             ! Declare Procedure

  CODE
