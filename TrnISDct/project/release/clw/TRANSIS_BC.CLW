  MEMBER('TRANSIS.CLW')

  PRAGMA('define(init_priority=>3)')

  MAP
    MODULE('TRANSIS_BC0.CLW')
TRANSIS_BC0:DctInit             PROCEDURE()
TRANSIS_BC0:DctKill             PROCEDURE()
TRANSIS_BC0:FilesInit           PROCEDURE()
    END
    MODULE('TRANSIS_BC1.CLW')
TRANSIS_BC1:DctInit             PROCEDURE()
TRANSIS_BC1:DctKill             PROCEDURE()
TRANSIS_BC1:FilesInit           PROCEDURE()
    END
    MODULE('TRANSIS_BC2.CLW')
TRANSIS_BC2:DctInit             PROCEDURE()
TRANSIS_BC2:DctKill             PROCEDURE()
TRANSIS_BC2:FilesInit           PROCEDURE()
    END
    MODULE('TRANSIS_BC3.CLW')
TRANSIS_BC3:DctInit             PROCEDURE()
TRANSIS_BC3:DctKill             PROCEDURE()
TRANSIS_BC3:FilesInit           PROCEDURE()
    END
    MODULE('TRANSIS_BC4.CLW')
TRANSIS_BC4:DctInit             PROCEDURE()
TRANSIS_BC4:DctKill             PROCEDURE()
TRANSIS_BC4:FilesInit           PROCEDURE()
    END
    MODULE('TRANSIS_BC5.CLW')
TRANSIS_BC5:DctInit             PROCEDURE()
TRANSIS_BC5:DctKill             PROCEDURE()
TRANSIS_BC5:FilesInit           PROCEDURE()
    END
    MODULE('TRANSIS_BC6.CLW')
TRANSIS_BC6:DctInit             PROCEDURE()
TRANSIS_BC6:DctKill             PROCEDURE()
TRANSIS_BC6:FilesInit           PROCEDURE()
    END
    MODULE('TRANSIS_BC7.CLW')
TRANSIS_BC7:DctInit             PROCEDURE()
TRANSIS_BC7:DctKill             PROCEDURE()
TRANSIS_BC7:FilesInit           PROCEDURE()
    END
  END

DLLInit              DllInitializer                          !This object is used to initialize the dll, it is defined in the main program module

DctInit PROCEDURE()
  CODE
  TRANSIS_BC0:DctInit
  TRANSIS_BC1:DctInit
  TRANSIS_BC2:DctInit
  TRANSIS_BC3:DctInit
  TRANSIS_BC4:DctInit
  TRANSIS_BC5:DctInit
  TRANSIS_BC6:DctInit
  TRANSIS_BC7:DctInit
  TRANSIS_BC0:FilesInit
  TRANSIS_BC1:FilesInit
  TRANSIS_BC2:FilesInit
  TRANSIS_BC3:FilesInit
  TRANSIS_BC4:FilesInit
  TRANSIS_BC5:FilesInit
  TRANSIS_BC6:FilesInit
  TRANSIS_BC7:FilesInit


DctKill PROCEDURE()
  CODE
  TRANSIS_BC0:DctKill
  TRANSIS_BC1:DctKill
  TRANSIS_BC2:DctKill
  TRANSIS_BC3:DctKill
  TRANSIS_BC4:DctKill
  TRANSIS_BC5:DctKill
  TRANSIS_BC6:DctKill
  TRANSIS_BC7:DctKill

