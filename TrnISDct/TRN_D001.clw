

   MEMBER('TRANSIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('TRN_D001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Upd_Invoice_Paid_Status PROCEDURE  (ULONG p:IID,BYTE p:Option,BYTE p:CreditChanged) ! Declare Procedure
LOC:Total_Paid       DECIMAL(15,2)                         !
LOC:Total_Credit     DECIMAL(15,2)                         !
LOC:Status           BYTE                                  !used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown
LOC:Buffer           USHORT                                !

  CODE
    ! (ULONG, BYTE=0, BYTE=0),BYTE,PROC
    ! (p:IID, p:Option, p:CreditChanged)
    !   p:Option
    !       - 1 is for Client Statement runs
    !       - x is other
    !
    !   - For Invoice update Paid to Fully Paid - Credit or Fully Paid
    !   - For Credit Note, update Shown status
    !     
    !   A Status < 4 will cause the invoice to show on the next Statement generated.
    !   If an invoice is status 4 then 3 should only be set again if there have been changes to the credits applied.
    !   p:CreditChanged is used to indicate this condition.
    !
    !              0               1           2               3                  4                5               6                 7
    !          No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note (shown), Bad Debt (shown), Over Paid
    !
    ! Returns Invoice Status (possibly updated)

    db.debugout('[Upd_Invoice_Paid_Status]  IID: ' & p:IID & ',  Option: ' & p:Option & '   - Clock: ' & CLOCK())

    IF p:IID > 0
       Relate:_Invoice.Open()
       Access:_Invoice.UseFile()

       LOC:Buffer   = Access:_Invoice.SaveBuffer()

       INV:IID      = p:IID
       IF Access:_Invoice.TryFetch(INV:PKey_IID) = LEVEL:Benign
          IF INV:Total < 0.0                                      ! Credit notes are less - i.e. < 0.0  (as are Bad Debts now)
             IF p:Option = 1
                IF INV:BadDebt = TRUE
                   INV:Status  = 6
                ELSE
                   INV:Status  = 5
                .
             ELSE
                ! Check this Credit Note is on a Clients statement
                !   -1 = Error condition, found Statement Item but not Statement
                !   -2 = Error condition, found Statement but not Statement Run
                !    0 = No Statement found
                !    1 = Found on Statement but not Client Statement
                !    2 = Found on Client Statement

                IF Check_IID_On_Statement(p:IID) = 2                       ! On a Client Statement
                   IF INV:BadDebt = TRUE
                      INV:Status  = 6
                   ELSE
                      INV:Status  = 5
                   .
                ELSE
                   IF INV:BadDebt = TRUE                                   ! Added 25.03.14 - We don't have an option for BadDebt (not shown)
                      INV:Status  = 6
                   ELSE                        
                      INV:Status  = 2                                         
             .  .  .                                                        
          ELSE                                                             ! Invoice
             LOC:Total_Paid    = Get_ClientsPay_Alloc_Amt( p:IID, 0 )

   !    db.debugout('[Upd_Invoice_Paid_Status]  after')

             ! Check that Invoice has no Credit Notes for it
             LOC:Total_Credit  = Get_Inv_Credited( p:IID )                 ! Reduce owing by Credit Notes
             LOC:Total_Paid   += - (LOC:Total_Credit)

             !     0               1           2               3                  4                5               6                 7
             ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note (shown), Bad Debt (shown), Over Paid
             IF INV:Total < LOC:Total_Paid
                INV:Status     = 7
             ELSIF INV:Total = LOC:Total_Paid
                ! Only change Fully Paid invoices with Credits to Fully Paid if Option is 1 - ie on Client Statement gen.
                IF INV:Status = 4 AND p:CreditChanged = TRUE
                   IF p:Option ~= 1 AND LOC:Total_Credit ~= 0.0
                      INV:Status  = 3
                   .
                ELSE
                   INV:Status  = 4
                .
             ELSIF LOC:Total_Paid ~= 0.0
                INV:Status     = 1
             ELSE
                INV:Status     = 0
          .  .

          INV:StatusUpToDate   = TRUE

          IF Access:_Invoice.TryUpdate() = LEVEL:Benign
       .  .

       LOC:Status  = INV:Status

       Access:_Invoice.RestoreBuffer(LOC:Buffer)

       Relate:_Invoice.Close()
    .

    RETURN(LOC:Status)
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Set_Connection PROCEDURE 

LOC:Connection_Group GROUP,PRE(L_CG)                       !
Database             STRING(100)                           !Database
User                 STRING(100)                           !User
Password             STRING(100)                           !
Server               STRING(100)                           !
DBOwner              STRING(255)                           !
Trusted_Connection   BYTE                                  !Use Trusted Connection
BusyHandling         BYTE                                  !
                     END                                   !
Window               WINDOW('TransIS - Set Database Connection Info.'),AT(,,310,243),FONT('MS Sans Serif',8,,FONT:regular), |
  DOUBLE,GRAY
                       SHEET,AT(4,4,301,214),USE(?Sheet1)
                         TAB('Connection'),USE(?Tab1)
                           PROMPT('Database:'),AT(10,21),USE(?Database:Prompt),TRN
                           ENTRY(@s100),AT(67,21,126,10),USE(L_CG:Database),MSG('Database'),TIP('Database')
                           PROMPT('User:'),AT(10,36),USE(?User:Prompt),TRN
                           ENTRY(@s100),AT(67,36,126,10),USE(L_CG:User),MSG('User'),TIP('User')
                           PROMPT('Password:'),AT(10,50),USE(?Password:Prompt),TRN
                           ENTRY(@s100),AT(67,50,126,10),USE(L_CG:Password)
                           PROMPT('Server:'),AT(10,64),USE(?Server:Prompt),TRN
                           ENTRY(@s100),AT(67,64,126,10),USE(L_CG:Server)
                           BUTTON('&Build'),AT(67,78,48,14),USE(?Button_Build)
                           CHECK(' &Trusted Connection'),AT(67,168),USE(L_CG:Trusted_Connection),MSG('Use Trusted ' & |
  'Connection'),TIP('Use Trusted Connection'),TRN
                           PROMPT('Busy Handling:'),AT(10,202),USE(?L_CG:BusyHandling:Prompt),TRN
                           LIST,AT(67,202,231,10),USE(L_CG:BusyHandling),DROP(5),FROM('1 - Do Nothing (dont use)|#' & |
  '1|2 - One Connection Per Thread (recommended)|#2|3 - Retry on Busy (default)|#3|4 - ' & |
  'Connection Locking|#4')
                           PROMPT('DB Owner:'),AT(10,100),USE(?L_CG:DBOwner:Prompt),TRN
                           TEXT,AT(67,100,231,62),USE(L_CG:DBOwner),VSCROLL,BOXED
                         END
                       END
                       PROMPT('Path'),AT(4,221,187,21),USE(?Prompt_Path),TRN
                       BUTTON('&OK'),AT(196,226,,14),USE(?OkButton),LEFT,ICON('waok.ico'),DEFAULT,FLAT
                       BUTTON('&Cancel'),AT(252,226,,14),USE(?CancelButton),LEFT,ICON('wacancel.ico'),FLAT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Set_Connection')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Database:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
      ?Prompt_Path{PROP:Text} = PATH()
  Do DefineListboxStyle
  INIMgr.Fetch('Set_Connection',Window)                    ! Restore window settings from non-volatile store
      L_CG:DBOwner    = GETINI('Setup', 'DBOwner', 'TransIS;UID=sa;PWD=sa;DATABASE=TransIS', GLO:Global_INI)
  
      L_CG:Server     = Get_1st_Element_From_Delim_Str(L_CG:DBOwner, ';', TRUE)
  
      L_CG:User       = Get_1st_Element_From_Delim_Str(L_CG:DBOwner, ';', TRUE)
      L_CG:User       = LEFT(SUB(L_CG:User, 5, LEN(CLIP(L_CG:User))))
  
      L_CG:Password   = Get_1st_Element_From_Delim_Str(L_CG:DBOwner, ';', TRUE)
      L_CG:Password   = LEFT(SUB(L_CG:Password, 5, LEN(CLIP(L_CG:Password))))
  
      L_CG:Database   = Get_1st_Element_From_Delim_Str(L_CG:DBOwner, ';', TRUE)
      L_CG:Database   = LEFT(SUB(L_CG:Database, 10, LEN(CLIP(L_CG:Database))))
  
  
      L_CG:DBOwner    = GETINI('Setup', 'DBOwner', 'TransIS;UID=sa;PWD=sa;DATABASE=TransIS', GLO:Global_INI)
  
      L_CG:Trusted_Connection = GETINI('Setup', 'Trusted_Connection', '1', GLO:Global_INI)
  
  
      L_CG:BusyHandling   = GETINI('Setup','BusyHandling','3','.\TransIS.INI')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('Set_Connection',Window)                 ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_Build
      ThisWindow.Update()
          L_CG:DBOwner        = CLIP(L_CG:Server) & ';UID=' & CLIP(L_CG:User) & ';PWD=' & CLIP(L_CG:Password) & ';DATABASE=' & CLIP(L_CG:Database)
          DISPLAY(?L_CG:DBOwner)
    OF ?OkButton
      ThisWindow.Update()
      !    IF LOC:Test_Database = TRUE
      !       CASE MESSAGE('You have checked the Test Database option.||If the specified database is not for TEST ONLY purposes please un-check this.||Are you sure?', 'Confirm Test Database', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
      !       OF BUTTON:No
      !          Window{PROP:AcceptAll}   = 0
      !          CYCLE
      !    .  .
      !
      !    PUTINI('Setup', 'Test_Database', LOC:Test_Database, '.\TransIS.INI')
      
          PUTINI('Setup', 'DBOwner', CLIP(L_CG:DBOwner), GLO:Global_INI)
          PUTINI('Setup', 'Trusted_Connection', L_CG:Trusted_Connection, GLO:Global_INI)
      
      
      
          PUTINI('Setup', 'BusyHandling', L_CG:BusyHandling, '.\TransIS.INI')
       POST(EVENT:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update()
       POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Source
!!! ** global **  on trigger
!!! </summary>
Upd_Delivery_Man_Status PROCEDURE  (p:DID, p:DIID, p:Option) ! Declare Procedure
LOC:DID              ULONG                                 !Delivery ID
LOC:No_MIDs          ULONG                                 !
LOC:No_Unloaded_Items ULONG                                !
LOC:Manifested       BYTE                                  !Manifested status - this has maintenance function - Not Manifested, Partially Manifested, Partially Manifested Multiple, Fully Manifested, Fully Manifested Multiple
LOC:User_Text        STRING(255)                           !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
     Access:DeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
    db.debugout('[Upd_Delivery_Man_Status]  DID: ' & p:DID & ',  DIID: ' & p:DIID & ',  Option: ' & p:Option & '   - Clock: ' & CLOCK())

    ! (p:DID, p:DIID, p:Option)
    ! p:Option
    !       0 - silent
    !       1 - comment on change
    ! If DIID passed than it is used to get the DID, this is the only use of p:DIID
    ! Maintains A_DEL:Manifested status


    IF OMITTED(1) OR p:DID = 0
       IF OMITTED(2) OR p:DIID = 0
          MESSAGE('No DID or DIID provided.', 'Upd_Delivery_Man_Status', ICON:Hand)
       ELSE
          A_DELI:DIID   = p:DIID
          IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) = LEVEL:Benign
             LOC:DID    = A_DELI:DID
       .  .
    ELSE
       LOC:DID  = p:DID
    .



    A_DEL:DID   = LOC:DID
    IF Access:DeliveriesAlias.TryFetch(A_DEL:PKey_DID) = LEVEL:Benign
       ! Check through the Delivery Items and ManifestLoadDeliveries and Manifests to see what manifests exists

       !   0. Return number of different MIDs      ! (p:DID, p:No, p:MAN_DIID_List, p:List_Option, p:DIID)
       LOC:No_MIDs              = Get_Delivery_ManIDs(LOC:DID, 0)

       !   1. Total Items not loaded on the DID    ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache)
       LOC:No_Unloaded_Items    = Get_DelItem_s_Totals(LOC:DID, 1,,, TRUE)



       ! A_DEL:Manifested can assume any of:
       ! Not Manifested, Partially Manifested, Partially Manifested Multiple, Fully Manifested, Fully Manifested Multiple
       !        0               1                           2                        3                     4
       CASE LOC:No_Unloaded_Items
       OF 0
          ! All Items loaded
          IF LOC:No_MIDs = 1
             LOC:Manifested       = 3
          ELSIF LOC:No_MIDs > 1
             LOC:Manifested       = 4
          ELSE                              ! No MIDs is 0
             ! We have no items unloaded but also no MIDs....  this would seem to be an error, or the DID has no items yet
             ! We should check to see if the DID has items and report an error if so ??????
             LOC:Manifested       = 0
          .
       ELSE
          ! Not all Items loaded
          IF LOC:No_MIDs = 1
             LOC:Manifested       = 1
          ELSIF LOC:No_MIDs > 1
             LOC:Manifested       = 2
          ELSE                              ! No MIDs is 0
             ! We know we have Items, LOC:No_Unloaded_Items > 0, but they may all not be loaded yet
             LOC:Manifested       = 0
       .  .

       IF p:Option = 1
          IF A_DEL:Manifested ~= LOC:Manifested
             EXECUTE LOC:Manifested + 1
                LOC:User_Text   = 'No Items are loaded.'
                LOC:User_Text   = 'Not all Items are loaded.'
                LOC:User_Text   = 'Not all Items are loaded, but loaded Items are on more than one Manifest.'
                LOC:User_Text   = 'All Items are loaded on one Manifest.'
                LOC:User_Text   = 'All Items are loaded on more than one Manifest.'
             .
             MESSAGE('DI No. ' & A_DEL:DINo & ' Manifested status has been changed to: || ' & CLIP(LOC:User_Text), 'Update Delivery Manifested Status', ICON:Asterisk)
       .  .

       A_DEL:Manifested         = LOC:Manifested

       IF Access:DeliveriesAlias.TryUpdate() ~= LEVEL:Benign               ! (p:Text, p:Heading, p:Name)
          Add_Log('Unable to update the Deliveries (G error: ' & Access:DeliveriesAlias.GetError() & ') (alias) with Manifested status: ' & A_DEL:Manifested, '[Upd_Delivery_Man_Status]')
       .
    ELSE
       MESSAGE('Could not fetch the delivery record!||DID: ' & LOC:DID & '||G Err: ' & Access:DeliveriesAlias.GetError(), 'Upd_Delivery_Man_Status', ICON:Hand)
    .

    
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Manifested)

    Exit

CloseFiles     Routine
    Relate:DeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! on trigger
!!! </summary>
Upd_Delivery_Del_Status PROCEDURE  (p:DID, p:DIID, p:Silent, p:NoCache) ! Declare Procedure
LOC:No_Unloaded_Items LONG                                 !
LOC:DID              ULONG                                 !Delivery ID
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
     Access:DeliveriesAlias.UseFile()
     Access:DeliveryItemAlias.UseFile()
    db.debugout('[Upd_Delivery_Del_Status]  DID: ' & p:DID & '   - Clock: ' & CLOCK())

    ! (p:DID, p:DIID, p:Silent, p:NoCache)
    ! (<ULONG>, <ULONG>, BYTE=0, BYTE=0), BYTE, PROC
    ! If DIID passed than it is used to get the DID, this is the only use of p:DIID
    ! Maintains A_DEL:Delivered status
    !
    ! p:NoCache     - There is no caching in this procedure but this flag is passed to called procedures to stop
    !                 caching at the next level

    IF OMITTED(1) OR p:DID = 0
       IF OMITTED(2) OR p:DIID = 0
          IF p:Silent = 0
             MESSAGE('No DID or DIID provided.', 'Upd_Delivery_Del_Status', ICON:Hand)
          .
       ELSE
          A_DELI:DIID   = p:DIID
          IF Access:DeliveryItemAlias.TryFetch(A_DELI:PKey_DIID) = LEVEL:Benign
             LOC:DID    = A_DELI:DID
       .  .
    ELSE
       LOC:DID  = p:DID
    .


    A_DEL:DID   = LOC:DID
    IF Access:DeliveriesAlias.TryFetch(A_DEL:PKey_DID) = LEVEL:Benign
       ! Special case, manually set to delivered
       IF A_DEL:Delivered ~= 3
          ! Check through the Delivery Items and TripSheetDeliveries to see what has been delivered

          ! p:Option
          !   0. Weight of all DID items - Volumetric considered
          !   1. Total Items not loaded on the DID    - Manifest
          !   2. Total Items loaded on the DID        - Manifest
          !   3. Total Items on the DID
          !   4. Total Items not loaded on the DID    - Tripsheet
          !   5. Total Items loaded on the DID        - Tripsheet
          !   6. Weight of all DID items - real weight
          !   7. Total Items not delivered on the DID - Tripsheet

          ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache)
          ! (ULONG, BYTE, <ULONG>, <*STRING>, BYTE=0),ULONG
          LOC:No_Unloaded_Items    = Get_DelItem_s_Totals(LOC:DID, 7, , , p:NoCache)      !4)  changed 8 Nov 06

      !MESSAGE('No_Unloaded_Items: ' & LOC:No_Unloaded_Items & '||DID: ' & LOC:DID, 'Upd_Delivery_Del_Status', ICON:Hand)

    db.debugout('[Upd_Delivery_Del_Status]  1 LOC:No_Unloaded_Items: ' & LOC:No_Unloaded_Items & ',  A_DEL:Delivered: ' & A_DEL:Delivered & '   - Clock: ' & CLOCK())

          ! A_DEL:Delivered can assume any of: Not Delivered, Partially Delivered, Delivered, Delivered (manual)
          CASE LOC:No_Unloaded_Items
          OF 0
             ! All Items loaded
             A_DEL:Delivered       = 2
          ELSE
             ! Not all Items loaded
             IF Get_DelItem_s_Totals(LOC:DID, 5, , , p:NoCache) > 0           !LOC:No_Unloaded_Items, taken out 8 Nov 06
                A_DEL:Delivered    = 1
             ELSE
                A_DEL:Delivered    = 0
          .  .

    db.debugout('[Upd_Delivery_Del_Status]  2 LOC:No_Unloaded_Items: ' & LOC:No_Unloaded_Items & ',  A_DEL:Delivered: ' & A_DEL:Delivered & '   - Clock: ' & CLOCK())

          IF Access:DeliveriesAlias.TryUpdate() ~= LEVEL:Benign            ! (p:Text, p:Heading, p:Name)
             Add_Log('Unable to update the Deliveries (G error: ' & Access:DeliveriesAlias.GetError() & ') (alias) with Delivered status: ' & A_DEL:Delivered, '[Upd_Delivery_Del_Status]')
       .  .
    ELSE
       IF p:Silent = 0
          MESSAGE('Could not fetch the delivery record!||DID: ' & LOC:DID & '||G Err: ' & Access:DeliveriesAlias.GetError(), 'Upd_Delivery_Del_Status', ICON:Hand)
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(A_DEL:Delivered)

    Exit

CloseFiles     Routine
    Relate:DeliveriesAlias.Close()
    Relate:DeliveryItemAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Upd_InvoiceTransporter_Paid_Status PROCEDURE  (p:TIN)      ! Declare Procedure
LOC:Total_Paid       DECIMAL(15,2)                         !
LOC:Total_Credit     DECIMAL(15,2)                         !
LOC:Status           BYTE                                  !used for filtering - No Payments, Partially Paid, Credit Note, Fully Paid
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_InvoiceTransporter.Open()
     .
     Access:_InvoiceTransporter.UseFile()
    ! (p:TIN)
    IF p:TIN > 0
       INT:TIN                 = p:TIN
       IF Access:_InvoiceTransporter.TryFetch(INT:PKey_TIN) = LEVEL:Benign
          IF INT:Cost < 0.0                                                ! Credit notes are less
             IF INT:Status ~= 2
                INT:Status = 2                                             ! Status is not correct, set Credit
             .
          ELSE                                                             ! Invoice
             LOC:Total_Paid    = Get_TransportersPay_Alloc_Amt( p:TIN, 0 )

             ! Check that Invoice has no Credit Notes for it
             ! p:Options
             !   0.  All
             !   1.  Credits
             !   2.  Debits
             LOC:Total_Credit  = Get_InvTransporter_Credited( p:TIN, 1 )                 ! Reduce owing by Credit Notes

       db.debugout('[Upd_Inv.Trans._Paid_Status]  TIN: ' & p:TIN & ',  INT:Cost: ' & INT:Cost & ',  LOC:Total_Paid: ' & LOC:Total_Paid & ',  LOC:Total_Credit: ' & LOC:Total_Credit & ',  INT:Status: ' & INT:Status)

             LOC:Total_Paid   += - (LOC:Total_Credit)

             ! Reget the record, in case a different buffer has been loaded from functions called
             INT:TIN                 = p:TIN
             IF Access:_InvoiceTransporter.TryFetch(INT:PKey_TIN) = LEVEL:Benign
             .

             !     0               1           2               3
             ! No Payments, Partially Paid, Credit Note, Fully Paid
             IF (INT:Cost + INT:VAT) <= LOC:Total_Paid
                INT:Status     = 3
             ELSIF LOC:Total_Paid ~= 0.0
                INT:Status     = 1
             ELSE
                INT:Status     = 0
          .  .

          INT:StatusUpToDate   = TRUE

          IF Access:_InvoiceTransporter.TryUpdate() = LEVEL:Benign
    .  .  .

    LOC:Status  = INT:Status

       DO CloseFiles
       Return(LOC:Status)

    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Status)

    Exit

CloseFiles     Routine
    Relate:_InvoiceTransporter.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! (p:CPID, p:Amt_Allocated)
!!! </summary>
Upd_ClientPayments_Status PROCEDURE  (p:CPID, p:Amt_Allocated) ! Declare Procedure
LOC:Amount           DECIMAL(10,2)                         !Amount of payment
LOC:Return           LONG                                  !
Tek_Failed_File     STRING(100)

View_CPA        VIEW(ClientsPaymentsAllocationAlias)
    PROJECT(A_CLIPA:Amount)
    .


CPA_View        ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAllocationAlias.Open()
     .
     Access:ClientsPaymentsAlias.UseFile()
     Access:ClientsPaymentsAllocationAlias.UseFile()
    ! (p:CPID, p:Amt_Allocated)
    ! (ULONG, <*DECIMAL>),LONG,PROC
    A_CLIP:CPID             = p:CPID
    IF Access:ClientsPaymentsAlias.TryFetch(A_CLIP:PKey_CPID) ~= LEVEL:Benign
       LOC:Return           = -1
    ELSE
       ! Not Allocated, Partial Allocation, Fully Allocated

       CPA_View.Init(View_CPA, Relate:ClientsPaymentsAllocationAlias)
       CPA_View.AddSortOrder(A_CLIPA:FKey_CPID_AllocationNo)
       CPA_View.AddRange(A_CLIPA:CPID, A_CLIP:CPID)
       CPA_View.Reset()
       LOOP
          IF CPA_View.Next() ~= LEVEL:Benign
             BREAK
          .

          LOC:Amount       += A_CLIPA:Amount
       .
       CPA_View.Kill()

       A_CLIP:Status        = 0
       IF LOC:Amount >= A_CLIP:Amount
          A_CLIP:Status     = 2
       ELSIF LOC:Amount > 0
          A_CLIP:Status     = 1
       .

       A_CLIP:StatusUpToDate    = TRUE
       IF Access:ClientsPaymentsAlias.TryUpdate() = LEVEL:Benign
       .

       IF LOC:Amount > A_CLIP:Amount          ! Complain!
          MESSAGE('Allocated amount exceeds Payment amount!||Client Payment ID - CPID: ' & A_CLIP:CPID & '|Client ID - CID: ' & A_CLIP:CID & |
                    '|Date Payment Made: ' & FORMAT(A_CLIP:DateMade, @d5) & '|Notes: ' & CLIP(A_CLIP:Notes) |
                    , 'Upd_ClientPayments_Status', ICON:Hand)
!                    '|Date Payment Made: ' & FORMAT(A_CLIP:DateMade, @d5) & '|Notes: ' & CLIP(A_CLIP:Notes) & '|Type: ' & CHOOSE(A_CLIP:Type,'','')|
       .

       LOC:Return           = A_CLIP:Status

       IF ~OMITTED(2)
          p:Amt_Allocated   = LOC:Amount
    .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:ClientsPaymentsAlias.Close()
    Relate:ClientsPaymentsAllocationAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Check_IID_On_Statement PROCEDURE  (p:IID)                  ! Declare Procedure
LOC:Result           LONG                                  !
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_StatementItems.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Statements.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_Statement_Runs.Open()
     .
     Access:_StatementItems.UseFile()
     Access:_Statements.UseFile()
     Access:_Statement_Runs.UseFile()
    ! (p:IID)
    ! LOC:Result
    !   -1 = Error condition, found Statement Item but not Statement
    !   -2 = Error condition, found Statement but not Statement Run
    !    0 = No Statement found
    !    1 = Found on Statement but not Client Statement
    !    2 = Found on Client Statement

    ! No Payments, Partially Paid, Credit Note, Fully Paid - Credit, Fully Paid, Credit Note Shown

    STAI:IID                = p:IID
    IF Access:_StatementItems.TryFetch(STAI:FKey_IID) = LEVEL:Benign
       ! Get the Statement
       STA:STID             = STAI:STID
       IF Access:_Statements.TryFetch(STA:PKey_STID) = LEVEL:Benign
          ! Get the Statement Run to check type of Statement
          STAR:STRID        = STA:STRID
          IF Access:_Statement_Runs.TryFetch(STAR:PKey_STRID)  = LEVEL:Benign
             IF STAR:Type < 2               ! On a Client Statement
                LOC:Result  = 2
             ELSE
                LOC:Result  = 1
             .
          ELSE
             LOC:Result     = -2
          .
       ELSE
          LOC:Result        = -1
       .
    ELSE
       LOC:Result           = 0
    .

    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Result)

    Exit

CloseFiles     Routine
    Relate:_StatementItems.Close()
    Relate:_Statements.Close()
    Relate:_Statement_Runs.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Get_ClientsPay_Alloc_Amt PROCEDURE  (p:ID, p:Option, p:ToDate) ! Declare Procedure
LOC:Paid_Dec         DECIMAL(15,2)                         !
LOC:Return           STRING(30)                            !
LOC:Where            STRING(255)                           !
Tek_Failed_File     STRING(100)

My_SQL    SQLQueryClass

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAllocationAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ClientsPaymentsAlias.Open()
     .
     Access:ClientsPaymentsAllocationAlias.UseFile()
     Access:ClientsPaymentsAlias.UseFile()
    ! (p:ID, p:Option, p:ToDate)
    ! p:Option
    !   0.  Invoice Paid
    !   1.  Clients Payments Allocation total

!          db.debugout('[Get_ClientsPay_Alloc_Amt]  Option: ' & p:Option & ',  ID: ' & p:ID)

    IF p:Option > 1
       MESSAGE('The option does not exist: ' & p:Option, 'Get_ClientsPay_Alloc_Amt', ICON:Hand)
    ELSE
       My_SQL.Init(GLO:DBOwner, '_SQLTemp2')

       EXECUTE p:Option + 1
          LOC:Where = 'IID = ' & p:ID
          LOC:Where = 'CPID = ' & p:ID
       .

       IF p:ToDate ~= 0
          ! (p:Add, p:List, p:Delim, p:Option, p:Prefix) - (STRING, *STRING, STRING, BYTE=0, <STRING>)
          Add_to_List('AllocationDateAndTime < ' & SQL_Get_DateT_G(p:ToDate + 1,,1), LOC:Where, ' AND ')
       .

       My_SQL.PropSQL('SELECT SUM(Amount) FROM ClientsPaymentsAllocation WHERE ' & CLIP(LOC:Where))

       IF My_SQL.Next_Q() > 0
          LOC:Paid_Dec  = My_SQL.Data_G.F1
       .

       ! We need to check if these payments have been credited...
       IF p:Option = 1
          A_CLIP:CPID_Reversal  = p:ID
          IF Access:ClientsPaymentsAlias.TryFetch(A_CLIP:Key_CPIDReversal) = LEVEL:Benign
             My_SQL.PropSQL('SELECT SUM(Amount) FROM ClientsPaymentsAllocation WHERE CPID = ' & A_CLIP:CPID)

             ! This CPID has been reversed.. add these up too
             IF My_SQL.Next_Q() > 0
                LOC:Paid_Dec  += My_SQL.Data_G.F1
    .  .  .  .


    LOC:Return  = LOC:Paid_Dec
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:ClientsPaymentsAllocationAlias.Close()
    Relate:ClientsPaymentsAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! **SQL Query Class used
!!! </summary>
Get_Inv_Credited     PROCEDURE  (p:IID, p:ToDate)          ! Declare Procedure
LOC:Total            DECIMAL(15,2)                         !
LOC:Return           STRING(30)                            !
Tek_Failed_File     STRING(100)

My_SQL    SQLQueryClass

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:InvoiceAlias.Open()
     .
     Access:InvoiceAlias.UseFile()
    ! (p:IID, p:ToDate)

    My_SQL.Init(GLO:DBOwner, '_SQLTemp2')

    IF p:ToDate ~= 0
       My_SQL.PropSQL('SELECT SUM(Total) FROM _Invoice WHERE CR_IID = ' & p:IID & ' AND InvoiceDateAndTime < ' & |
                    SQL_Get_DateT_G(p:ToDate + 1,,1))
    ELSE
       My_SQL.PropSQL('SELECT SUM(Total) FROM _Invoice WHERE CR_IID = ' & p:IID)
    .

    IF My_SQL.Next_Q() > 0
       LOC:Total    = My_SQL.Data_G.F1
    .


    LOC:Return      = LOC:Total
    
    
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Return)

    Exit

CloseFiles     Routine
    Relate:InvoiceAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! note: Ulong returned - divide by 100 for weight   Manifest & Tripsheets also used
!!! </summary>
Get_DelItem_s_Totals PROCEDURE  (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache, p:Weight_Option) ! Declare Procedure
LOC:Weight_As_Ulong  ULONG                                 !
LOC:Vol_Weight_As_Ulong ULONG                              !
LOC:Loaded_Units     ULONG                                 !
LOC:Cached           BYTE                                  !
LOC:Statics          GROUP,PRE(L_SG),STATIC                !static
Logout_At            LONG                                  !
Logout_At_Get        LONG                                  !
Hits_Tot             ULONG                                 !
Req_Tot              ULONG                                 !
Freed                ULONG                                 !
                     END                                   !
LOC:Cache_Queue      QUEUE,PRE(L_CQ),STATIC                !static
DID                  ULONG                                 !Delivery ID
DIID                 ULONG                                 !Delivery Item ID
Date                 LONG                                  !
Time                 LONG                                  !
Cached_1             BYTE                                  !
Cached_2             BYTE                                  !
Cached_4             BYTE                                  !
Cached_5             BYTE                                  !
Cached_7             BYTE                                  !
Volumetric_Weight    ULONG                                 !
Total_Items_Not_Loaded ULONG                               !
Total_Items_Loaded   ULONG                                 !
Total_Items          ULONG                                 !
Total_Items_Not_Loaded_Tripsheet ULONG                     !
Total_Items_Loaded_Tripsheet ULONG                         !
Total_Items_Not_Delivered_Tripsheet ULONG                  !
Weight_of_all_Items  ULONG                                 !
Missed_Times_Tot     ULONG                                 !Totals missed times
Missed_Times_less_5_secs ULONG                             !Missed times with less than 5 sec. window
Vol_Weight_of_all_Items ULONG                              !
                     END                                   !
LOC:Group_Cache      GROUP,PRE(L_GC)                       !
Volumetric_Weight    ULONG                                 !
Total_Items          ULONG                                 !
Weight_of_all_Items  ULONG                                 !
Vol_Weight_of_all_Items ULONG                              !
                     END                                   !
LOC:Option_7_DelDates STRING(150)                          !
Tek_Failed_File     STRING(100)

View_DelItems           VIEW(DeliveryItemAlias)
    PROJECT(A_DELI:DID, A_DELI:DIID, A_DELI:Weight, A_DELI:VolumetricWeight, A_DELI:Units)
    .


DelItems_View           ViewManager

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveryItemAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:_SQLTemp.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadDeliveries.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:DeliveriesAlias.Open()
     .
     Access:DeliveryItemAlias.UseFile()
     Access:_SQLTemp.UseFile()
     Access:ManifestLoadDeliveries.UseFile()
     Access:DeliveriesAlias.UseFile()
    ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache, p:Weight_Option)
    ! (ULONG, BYTE, <ULONG>, <*STRING>, BYTE=0, BYTE=0),ULONG
    !
    ! p:Option
    !   0. Weight of all DID items - Volumetric considered
    !   1. Total Items not loaded on the DID    - Manifest
    !   2. Total Items loaded on the DID        - Manifest
    !   3. Total Items on the DID
    !   4. Total Items not loaded on the DID    - Tripsheet
    !   5. Total Items loaded on the DID        - Tripsheet
    !   6. Weight of all DID items - real / actual weight  (weight * 100)
    !   7. Total Items not delivered on the DID - Tripsheet     (similar to 4)
    !
    ! p:DIID is passed to check a single DIID
    ! p:Items_DIID_List is passed to return a list of DIIDs for the given Option
    !
    ! Cached entries are renewed every 1.5 seconds
    !
    ! p:Weight_Option
    !   0. Highest weight overall, highest total volumetric or total actual for all items  (default)
    !   1. Highest weight per item  (original)

    L_SG:Req_Tot   += 1

!    db.debugout('[Get_DelItem_s_Totals]  -------------  In  --------------    Option: ' & p:Option & ',  p:DID: ' & p:DID)


    DO Check_Cached

    IF LOC:Cached = FALSE
       DO Get_File_Data

!       IF p:Option = 4 OR p:Option = 5 OR p:Option = 7
!          db.debugout('[Get_DelItem_s_Totals]  Cache Q: ' & RECORDS(LOC:Cache_Queue) & ',  Hits_Tot: ' & L_SG:Hits_Tot & ',  Req_Tot: ' & L_SG:Req_Tot & ',  Freed: ' & L_SG:Freed  & ',   DID: ' & p:DID & ',   Option: ' & p:Option)
!       .
    ELSE                                        ! Cached
       DO Get_Cached

!       IF p:Option = 4 OR p:Option = 5 OR p:Option = 7
!          db.debugout('[Get_DelItem_s_Totals]  Cached - DID: ' & p:DID & ',  Option: ' & p:Option & ',  Weight_As_Ulong: ' & LOC:Weight_As_Ulong)
!       .
    .

!          IF p:DID = 26277
!             MESSAGE('Return: ' & LOC:Weight_As_Ulong, 'p:DID: ' & p:DID)
!          .

!    db.debugout('[Get_DelItem_s_Totals]  -------------  Out  --------------    Return: ' & LOC:Weight_As_Ulong)
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles
    !    db.debugout('[Get_DelItem_s_Totals]  Result: ' & LOC:Weight_As_Ulong & ',    p:Option: ' & p:Option & ',  p:DID: ' & p:DID)
    

       Return(LOC:Weight_As_Ulong)

    Exit

CloseFiles     Routine
    Relate:DeliveryItemAlias.Close()
    Relate:_SQLTemp.Close()
    Relate:ManifestLoadDeliveries.Close()
    Relate:DeliveriesAlias.Close()
    Exit
Get_File_Data                           ROUTINE
    ! p:Option
    !   0. Weight of all DID items - Volumetric considered
    !   1. Total Items not loaded on the DID    - Manifest
    !   2. Total Items loaded on the DID        - Manifest
    !   3. Total Items on the DID
    !   4. Total Items not loaded on the DID    - Tripsheet
    !   5. Total Items loaded on the DID        - Tripsheet
    !   6. Weight of all DID items - real / actual weight
    !   7. Total Items not delivered on the DID - Tripsheet     (similar to 4)
   IF p:Option = 1 AND FALSE
       _SQLTemp{PROP:SQL}  = 'SELECT SUM(di.Units), SUM(mld.UnitsLoaded) FROM DeliveryItems AS di JOIN ManifestLoadDeliveries AS mld ON mld.DIID = di.DIID WHERE DID = ' & p:DID
      
!       _SQLTemp{PROP:SQL}  = 'SELECT SUM(UnitsLoaded) FROM ManifestLoadDeliveries WHERE DIID = ' & p:DIID
       IF ERRORCODE()
          MESSAGE('SQL ERROR: ' & FILEERROR() & ' (' & ERROR() & ')', 'Get_DelItem_s_Totals', ICON:Hand)
       ELSE
          NEXT(_SQLTemp)
          IF ~ERRORCODE()
             LOC:Weight_As_Ulong    = _SQ:S1 - _SQ:S2
             db.debugout('[Get_DelItem_s_Totals]  _SQ:S1 - _SQ:S2:  ' & CLIP(_SQ:S1) & ' - ' & CLIP(_SQ:S2) &   '    = ' & LOC:Weight_As_Ulong)
       .  .

!      LOC:Weight_As_Ulong
      EXIT
   .
   

    IF ~OMITTED(3)                           ! Single Item
       PUSHBIND()
       BIND('A_DELI:DIID', A_DELI:DIID)
    .

    DelItems_View.Init(View_DelItems, Relate:DeliveryItemAlias)
    DelItems_View.AddSortOrder(A_DELI:FKey_DID_ItemNo)
    !DelItems_View.AppendOrder()
    DelItems_View.AddRange(A_DELI:DID, p:DID)

    IF ~OMITTED(3)                           ! Single Item
       DelItems_View.SetFilter('A_DELI:DIID = ' & p:DIID)
    .

    CLEAR(LOC:Group_Cache)

    DelItems_View.Reset()
    LOOP
       IF DelItems_View.Next() ~= LEVEL:Benign
          BREAK
       .

       CASE p:Option
       OF 0
          ! This variable is now set below - it was being counted twice anyway - 06/02/10
!          IF p:Weight_Option = 1
!             ! Volumetric Weight is used for rates
!             IF A_DELI:Weight < A_DELI:VolumetricWeight
!                LOC:Weight_As_Ulong       += A_DELI:VolumetricWeight * 100
!             ELSE
!                LOC:Weight_As_Ulong       += A_DELI:Weight           * 100
!             .
!          ELSE
!             ???
!          .
       OF 1 OROF 4 OROF 7                                 ! These ones require look ups
          LOC:Weight_As_Ulong          += A_DELI:Units

          IF p:Option = 1
             LOC:Loaded_Units           = Get_ManLoadItem_Units(A_DELI:DIID)
          ELSIF p:Option = 4
             !      0. Delivered Units
             !      1. Delivered & Un Delivered on incomplete Trips
             !      2. Delivered & Rejected
             !      3. Delivered & Rejected & Un Delivered on incomplete Trips
             !      4. Total Items not loaded on the DID    - Tripsheet
             !      5. Total Items loaded on the DID        - Tripsheet
             !      6. Weight of all DID items - real weight

             ! Get_TripItem_Del_Units   - (p:DIID, p:Option)
             !   p:Option
             !       0. Delivered Units
             !       1. Delivered & Un Delivered on incomplete Trips
             !       2. Delivered & Rejected
             !       3. Delivered & Rejected & Un Delivered on incomplete Trips
             !       4. Delivered & Rejected & Un Delivered on ALL Trips
             LOC:Loaded_Units           = Get_TripItem_Del_Units(A_DELI:DIID, 4)
          ELSIF p:Option = 7
             LOC:Loaded_Units           = Get_TripItem_Del_Units(A_DELI:DIID, 2)    ! changed to 2 - Feb 27 07
          .

          ! An old bug allowed items to be loaded on more than one tripsheet leading to a possibility of
          ! this going negative.  Cludge added..  6 March 07

          IF LOC:Weight_As_Ulong - LOC:Loaded_Units < 0
             db.debugout('[Get_DelItem_s_Totals]  *** DI loaded more than no of items *** - Loaded: ' & LOC:Loaded_Units & ',   Item Units: ' & A_DELI:Units & ',   Result: ' & LOC:Weight_As_Ulong & ',    p:DID: ' & p:DID)
             LOC:Weight_As_Ulong           = 0
          ELSE
             LOC:Weight_As_Ulong          -= LOC:Loaded_Units
          .


!          IF p:DID = 26277
!    db.debugout('[Get_DelItem_s_Totals]  Loaded: ' & LOC:Loaded_Units & ',   Item Units: ' & A_DELI:Units & ',   Result: ' & LOC:Weight_As_Ulong & ',    p:DID: ' & p:DID)
!          .

          IF ~OMITTED(4)
             IF LOC:Loaded_Units ~= A_DELI:Units
                IF CLIP(p:Items_DIID_List) = ''
                   p:Items_DIID_List    = A_DELI:DIID
                ELSE
                   p:Items_DIID_List    = CLIP(p:Items_DIID_List) & ',' & A_DELI:DIID
          .  .  .
       OF 2 OROF 5                          ! These ones require look ups
          IF p:Option = 2
             LOC:Loaded_Units           = Get_ManLoadItem_Units(A_DELI:DIID)
          ELSE                        
             LOC:Loaded_Units           = Get_TripItem_Del_Units(A_DELI:DIID, 3)
          .

          LOC:Weight_As_Ulong          += LOC:Loaded_Units
       OF 3
          LOC:Weight_As_Ulong          += A_DELI:Units
       OF 6
          LOC:Weight_As_Ulong          += A_DELI:Weight           * 100
       . 

       ! Always save these 3 - as always available
       IF A_DELI:Weight < A_DELI:VolumetricWeight
          L_GC:Volumetric_Weight       += A_DELI:VolumetricWeight * 100
       ELSE
          L_GC:Volumetric_Weight       += A_DELI:Weight           * 100
       .
       L_GC:Total_Items                += A_DELI:Units
       L_GC:Weight_of_all_Items        += A_DELI:Weight           * 100
       L_GC:Vol_Weight_of_all_Items    += A_DELI:VolumetricWeight * 100


       ! Collect DIID list if not omitted - and not option 1 or 4 which have a special condition
       IF ~OMITTED(4) AND p:Option ~= 1 AND p:Option ~= 4 AND p:Option ~= 7
          IF CLIP(p:Items_DIID_List) = ''
             p:Items_DIID_List          = A_DELI:DIID
          ELSE
             p:Items_DIID_List          = CLIP(p:Items_DIID_List) & ',' & A_DELI:DIID
       .  .


       IF ~OMITTED(3)                        ! Single Item found so exit
           BREAK
    .  .

    DelItems_View.Kill()
    IF ~OMITTED(3)                           ! Single Item
       UNBIND('A_DELI:DIID')
       POPBIND()
    .


    CASE p:Option
    OF 0 OROF 6         ! If we are looking for weights...
       ! 05/02/10
       ! Change to default charging weight, now greater of total of Actual or Volumetric
       ! No need to change cache as it will have correct charge weight cached
       IF p:Weight_Option = 1
          LOC:Weight_As_Ulong      = L_GC:Volumetric_Weight
       ELSE
          IF L_GC:Weight_of_all_Items > L_GC:Vol_Weight_of_all_Items
             LOC:Weight_As_Ulong   = L_GC:Weight_of_all_Items
          ELSE
             LOC:Weight_As_Ulong   = L_GC:Vol_Weight_of_all_Items
    .  .  .

    DO Update_Cache
    EXIT
! ----------------------------------------------------------------------
Check_Cached                         ROUTINE
    IF RECORDS(LOC:Cache_Queue) > 5000
       FREE(LOC:Cache_Queue)
       L_SG:Freed  += 1
    .

    CLEAR(LOC:Cache_Queue)
    IF p:No_Cache = FALSE
       L_CQ:DID     = p:DID

       IF ~OMITTED(3)
          L_CQ:DIID = p:DIID
          GET(LOC:Cache_Queue, L_CQ:DID, L_CQ:DIID)
       ELSE
          GET(LOC:Cache_Queue, L_CQ:DID)
       .
       IF ERRORCODE()
          CLEAR(LOC:Cache_Queue)
          p:No_Cache    = TRUE
    .  .

    IF p:No_Cache = TRUE OR |
            ((L_CQ:Cached_1 = FALSE AND p:Option = 1) OR (L_CQ:Cached_2 = FALSE AND p:Option = 2) OR |
             (L_CQ:Cached_4 = FALSE AND p:Option = 4) OR (L_CQ:Cached_5 = FALSE AND p:Option = 5) OR |
             (L_CQ:Cached_7 = FALSE AND p:Option = 7))
       ! Not cached
    ELSE
       IF L_CQ:Date = TODAY() AND (CLOCK() - L_CQ:Time) <= 500        ! 5 a second
          LOC:Cached    = TRUE
       ELSE
!      db.debugout('[Get_DelItem_s_Totals]  cached,  L_CQ:DID: ' & L_CQ:DID & ',  p: ' & p:DID & ' - too long?   ' & (CLOCK() - L_CQ:Time))
          L_CQ:Missed_Times_Tot += 1
          IF (CLOCK() - L_CQ:Time) <= 500
             L_CQ:Missed_Times_less_5_secs  += 1
          .
          L_CQ:Time     = CLOCK()

          PUT(LOC:Cache_Queue)

          IF ABS(CLOCK() - L_SG:Logout_At) > 100        ! Output every 1 secs
             L_SG:Logout_At     = CLOCK()

             START(Add_Log_h,, 'Option: ' & p:Option & '   Cached but > than 5 secs ago - DID: ' & p:DID & ',  Time loaded: ' & FORMAT(L_CQ:Time,@t4) & ',  secs. since: ' & (CLOCK() - L_CQ:Time) / 100 & ',   Missed Tot: ' & L_CQ:Missed_Times_Tot & ', Missed less 5 secs: ' & L_CQ:Missed_Times_less_5_secs, 'Get_DelItem_s_Totals', 'Cache')
    .  .  .

    IF L_SG:Logout_At = 0
       L_SG:Logout_At     = CLOCK()
       L_SG:Logout_At_Get = CLOCK()
    .
    EXIT
Update_Cache                          ROUTINE
    ! Update cache  -  Get any old entry
    L_CQ:DID         = p:DID
    IF ~OMITTED(3)
       L_CQ:DIID     = p:DIID
       GET(LOC:Cache_Queue, L_CQ:DID, L_CQ:DIID)
    ELSE
       GET(LOC:Cache_Queue, L_CQ:DID)
    .

    IF ERRORCODE()
       CLEAR(LOC:Cache_Queue)
    ELSE
       LOC:Cached = TRUE
    .

    CASE p:Option
    OF 1 OROF 4 OROF 7
       IF p:Option = 1
          L_CQ:Total_Items_Not_Loaded        = LOC:Weight_As_Ulong
          L_CQ:Cached_1                      = TRUE
       ELSIF p:Option = 4
          L_CQ:Total_Items_Not_Loaded_Tripsheet = LOC:Weight_As_Ulong
          L_CQ:Cached_4                      = TRUE
       ELSIF p:Option = 7
          L_CQ:Total_Items_Not_Delivered_Tripsheet = LOC:Weight_As_Ulong
          L_CQ:Cached_7                      = TRUE
       .
    OF 2 OROF 5
       IF p:Option = 2
          L_CQ:Total_Items_Loaded            = LOC:Weight_As_Ulong
          L_CQ:Cached_2                      = TRUE
       ELSE
          L_CQ:Total_Items_Loaded_Tripsheet  = LOC:Weight_As_Ulong
          L_CQ:Cached_5                      = TRUE
    .  .

    L_CQ:Volumetric_Weight          = L_GC:Volumetric_Weight
    L_CQ:Total_Items                = L_GC:Total_Items
    L_CQ:Weight_of_all_Items        = L_GC:Weight_of_all_Items
    L_CQ:Vol_Weight_of_all_Items    = L_GC:Vol_Weight_of_all_Items

    L_CQ:DID         = p:DID
    L_CQ:Date        = TODAY()
    L_CQ:Time        = CLOCK()

    IF ~OMITTED(3)                           ! Single Item
       L_CQ:DIID     = p:DIID
    .

    IF LOC:Cached = TRUE
       PUT(LOC:Cache_Queue)
       IF ERRORCODE()
          db.debugout('[Get_DelItem_s_Totals]  Error on Queue entry put?')
       .
    ELSE
       ADD(LOC:Cache_Queue, +L_CQ:DID, +L_CQ:DIID)
    .


!    db.debugout('[Get_DelItem_s_Totals]  Cache Update  - L_CQ:DID: ' & L_CQ:DID & ',   L_CQ:DIID: ' & L_CQ:DIID)
    EXIT
Get_Cached                              ROUTINE
    CASE p:Option
    OF 0
       LOC:Weight_As_Ulong       = L_CQ:Volumetric_Weight
    OF 1 OROF 4 OROF 7
       IF p:Option = 1
          LOC:Weight_As_Ulong    = L_CQ:Total_Items_Not_Loaded
       ELSIF p:Option = 4
          LOC:Weight_As_Ulong    = L_CQ:Total_Items_Not_Loaded_Tripsheet
       ELSIF p:Option = 7
          LOC:Weight_As_Ulong    = L_CQ:Total_Items_Not_Delivered_Tripsheet
       .
    OF 2 OROF 5
       IF p:Option = 2
          LOC:Weight_As_Ulong    = L_CQ:Total_Items_Loaded
       ELSE
          LOC:Weight_As_Ulong    = L_CQ:Total_Items_Loaded_Tripsheet
       .
    OF 3
       LOC:Weight_As_Ulong       = L_CQ:Total_Items
    OF 6
       LOC:Weight_As_Ulong       = L_CQ:Weight_of_all_Items
    .

    L_SG:Hits_Tot               += 1

    ! IF logging - (p:Text, p:Heading, p:Name)
    IF ABS(CLOCK() - L_SG:Logout_At_Get) > 200        ! Output every 2 secs
       L_SG:Logout_At_Get = CLOCK()
       START(Add_Log_h,, 'Option: ' & p:Option & ',  Cache hit - DID: ' & p:DID & ',  Time loaded: ' & FORMAT(L_CQ:Time,@t4) & ' - secs. since: ' & (CLOCK() - L_CQ:Time) / 100 & ',  Cache: ' & RECORDS(LOC:Cache_Queue) & ',  Freed: ' & L_SG:Freed & ',  Reqs: ' & L_SG:Req_Tot & ', Hits: ' & L_SG:Hits_Tot & ' ' & INT((L_SG:Hits_Tot / L_SG:Req_Tot) * 100), 'Get_DelItem_s_Totals', 'Cache')
    .
    EXIT
