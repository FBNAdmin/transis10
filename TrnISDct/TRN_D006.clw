

   MEMBER('TRANSIS.CLW')                                   ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('TRN_D006.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Profit_Audit PROCEDURE 

CurrentTab           STRING(80)                            !
LOC:Dates_Q          QUEUE,PRE(L_DQ)                       !
Date                 DATE                                  !
                     END                                   !
LOC:Options          GROUP,PRE(LOC)                        !
Selected_Date        DATE                                  !
Selected_Date_To     DATE                                  !
BranchName           STRING(35)                            !Branch Name
BID                  ULONG                                 !Branch ID
                     END                                   !
LOC:Date_Group       GROUP,PRE(L_DG)                       !
Date                 DATE                                  !
Time                 TIME                                  !
                     END                                   !
Q_Source1            QUEUE,PRE(SRC1)                       !Queue declaration for browse/combo box using ?Browse:1
MPA:MPID             LIKE(MPA:MPID)                        !List box control field - type derived from field
MPA:Tran_Date        LIKE(MPA:Tran_Date)                   !List box control field - type derived from field
MPA:IID              LIKE(MPA:IID)                         !List box control field - type derived from field
MPA:TIN              LIKE(MPA:TIN)                         !List box control field - type derived from field
MPA:DID              LIKE(MPA:DID)                         !List box control field - type derived from field
MPA:DINo             LIKE(MPA:DINo)                        !List box control field - type derived from field
MPA:MID              LIKE(MPA:MID)                         !List box control field - type derived from field
MPA:BID              LIKE(MPA:BID)                         !List box control field - type derived from field
MPA:Broking          LIKE(MPA:Broking)                     !List box control field - type derived from field
MPA:Amount           LIKE(MPA:Amount)                      !List box control field - type derived from field
MPA:Source           LIKE(MPA:Source)                      !List box control field - type derived from field
MPA:Info             LIKE(MPA:Info)                        !List box control field - type derived from field
Mark                 BYTE                                  !Entry's marked status
ViewPosition         STRING(1024)                          !Entry's view position
                     END                                   !
Q_Source2            QUEUE,PRE(SRC2)                       !Queue declaration for browse/combo box using ?Browse:1
MPA:MPID             LIKE(MPA:MPID)                        !List box control field - type derived from field
MPA:Tran_Date        LIKE(MPA:Tran_Date)                   !List box control field - type derived from field
MPA:IID              LIKE(MPA:IID)                         !List box control field - type derived from field
MPA:TIN              LIKE(MPA:TIN)                         !List box control field - type derived from field
MPA:DID              LIKE(MPA:DID)                         !List box control field - type derived from field
MPA:DINo             LIKE(MPA:DINo)                        !List box control field - type derived from field
MPA:MID              LIKE(MPA:MID)                         !List box control field - type derived from field
MPA:BID              LIKE(MPA:BID)                         !List box control field - type derived from field
MPA:Broking          LIKE(MPA:Broking)                     !List box control field - type derived from field
MPA:Amount           LIKE(MPA:Amount)                      !List box control field - type derived from field
MPA:Source           LIKE(MPA:Source)                      !List box control field - type derived from field
MPA:Info             LIKE(MPA:Info)                        !List box control field - type derived from field
Mark                 BYTE                                  !Entry's marked status
ViewPosition         STRING(1024)                          !Entry's view position
                     END                                   !
BRW1::View:Browse    VIEW(Audit_ManagementProfit)
                       PROJECT(MPA:MPID)
                       PROJECT(MPA:Tran_Date)
                       PROJECT(MPA:IID)
                       PROJECT(MPA:TIN)
                       PROJECT(MPA:DID)
                       PROJECT(MPA:DINo)
                       PROJECT(MPA:MID)
                       PROJECT(MPA:BID)
                       PROJECT(MPA:Broking)
                       PROJECT(MPA:Amount)
                       PROJECT(MPA:Source)
                       PROJECT(MPA:Info)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
MPA:MPID               LIKE(MPA:MPID)                 !List box control field - type derived from local data
MPA:Tran_Date          LIKE(MPA:Tran_Date)            !List box control field - type derived from local data
MPA:IID                LIKE(MPA:IID)                  !List box control field - type derived from local data
MPA:TIN                LIKE(MPA:TIN)                  !List box control field - type derived from local data
MPA:DID                LIKE(MPA:DID)                  !List box control field - type derived from local data
MPA:DINo               LIKE(MPA:DINo)                 !List box control field - type derived from local data
MPA:MID                LIKE(MPA:MID)                  !List box control field - type derived from local data
MPA:BID                LIKE(MPA:BID)                  !List box control field - type derived from local data
MPA:Broking            LIKE(MPA:Broking)              !List box control field - type derived from local data
MPA:Amount             LIKE(MPA:Amount)               !List box control field - type derived from local data
MPA:Source             LIKE(MPA:Source)               !List box control field - type derived from local data
MPA:Info               LIKE(MPA:Info)                 !List box control field - type derived from local data
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(Audit_ManagementProfit_Alias)
                       PROJECT(MPA_A:MPID)
                       PROJECT(MPA_A:Tran_Date)
                       PROJECT(MPA_A:IID)
                       PROJECT(MPA_A:TIN)
                       PROJECT(MPA_A:DID)
                       PROJECT(MPA_A:DINo)
                       PROJECT(MPA_A:MID)
                       PROJECT(MPA_A:BID)
                       PROJECT(MPA_A:Broking)
                       PROJECT(MPA_A:Amount)
                       PROJECT(MPA_A:Source)
                       PROJECT(MPA_A:Info)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
MPA_A:MPID             LIKE(MPA_A:MPID)               !List box control field - type derived from field
MPA_A:Tran_Date        LIKE(MPA_A:Tran_Date)          !List box control field - type derived from field
MPA_A:IID              LIKE(MPA_A:IID)                !List box control field - type derived from field
MPA_A:TIN              LIKE(MPA_A:TIN)                !List box control field - type derived from field
MPA_A:DID              LIKE(MPA_A:DID)                !List box control field - type derived from field
MPA_A:DINo             LIKE(MPA_A:DINo)               !List box control field - type derived from field
MPA_A:MID              LIKE(MPA_A:MID)                !List box control field - type derived from field
MPA_A:BID              LIKE(MPA_A:BID)                !List box control field - type derived from field
MPA_A:Broking          LIKE(MPA_A:Broking)            !List box control field - type derived from field
MPA_A:Amount           LIKE(MPA_A:Amount)             !List box control field - type derived from field
MPA_A:Source           LIKE(MPA_A:Source)             !List box control field - type derived from field
MPA_A:Info             LIKE(MPA_A:Info)               !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB2::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?LOC:BranchName
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW1::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW1::PopupChoice    SIGNED                       ! Popup current choice
BRW1::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW1::PopupChoiceExec BYTE(0)                     ! Popup executed
BRW6::FormatManager  ListFormatManagerClass,THREAD ! LFM object
BRW6::PopupTextExt   STRING(1024)                 ! Extended popup text
BRW6::PopupChoice    SIGNED                       ! Popup current choice
BRW6::PopupChoiceOn  BYTE(1)                      ! Popup on/off choice
BRW6::PopupChoiceExec BYTE(0)                     ! Popup executed
QuickWindow          WINDOW('Browse the Audit Management Profit file'),AT(,,443,328),FONT('MS Sans Serif',8,,FONT:regular), |
  RESIZE,CENTER,GRAY,IMM,MAX,MDI,HLP('Browse_Profit_Audit'),SYSTEM
                       GROUP,AT(6,2,433,14),USE(?Group_Top)
                         PROMPT('Date:'),AT(6,4),USE(?Prompt1),TRN
                         LIST,AT(26,4,71,10),USE(LOC:Selected_Date),VSCROLL,DROP(10),FORMAT('32R(2)|M~Date~L@d5b@'), |
  FROM(LOC:Dates_Q)
                         PROMPT('Branch:'),AT(110,4),USE(?Prompt1:2)
                         LIST,AT(138,4,71,10),USE(LOC:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch Name~@s35@'), |
  FROM(Queue:FileDrop)
                         BUTTON,AT(292,2,34,14),USE(?Button_Refresh),KEY(AltR),ICON('RefreshI.ico')
                         BUTTON('Remove Same Inv / Amts'),AT(342,2,,14),USE(?Button_Rem)
                       END
                       SHEET,AT(4,19,436,287),USE(?Sheet1)
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT(''),AT(76,22,365,10),USE(?Prompt_Note)
                           LIST,AT(6,36,433,118),USE(?Browse_Source1),HVSCROLL,FORMAT('36R(2)|M~MPID~C(0)@n_10@46R' & |
  '(2)|M~Tran. Date~C(0)@d6@38R(2)|M~IIN~C(0)@n_10@38R(2)|M~TIN~C(0)@n_10@36R(2)|M~DID~' & |
  'C(0)@n_10@36R(2)|M~DI No.~C(0)@n_10@36R(2)|M~MID~C(0)@n_10@36R(2)|M~BID~C(0)@n_10@32' & |
  'R(2)|M~Broking~C(0)@n3@40R(1)|M~Amount~C(0)@n-14.2@28R(2)|M~Source~C(0)@n3@80L(2)|M~' & |
  'Info~C(0)@s50@'),FROM(Q_Source1),MSG('Browsing the Audit_ManagementProfit file')
                           LIST,AT(6,165,433,118),USE(?Browse_Source2),HVSCROLL,FORMAT('36R(2)|M~MPID~C(0)@n_10@46' & |
  'R(2)|M~Tran. Date~C(0)@d6@38R(2)|M~IIN~C(0)@n_10@38R(2)|M~TIN~C(0)@n_10@36R(2)|M~DID' & |
  '~C(0)@n_10@36R(2)|M~DI No.~C(0)@n_10@36R(2)|M~MID~C(0)@n_10@36R(2)|M~BID~C(0)@n_10@3' & |
  '2R(2)|M~Broking~C(0)@n3@40R(1)|M~Amount~C(0)@n-14.2@28R(2)|M~Source~C(0)@n3@80L(2)|M' & |
  '~Info~C(0)@s50@'),FROM(Q_Source2),MSG('Browsing the Audit_ManagementProfit file')
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           LIST,AT(6,38,433,118),USE(?Browse:1),HVSCROLL,FORMAT('36R(2)|M~MPID~C(0)@n_10@46R(2)|M~' & |
  'Tran. Date~C(0)@d6@38R(2)|M~IIN~C(0)@n_10@38R(2)|M~TIN~C(0)@n_10@36R(2)|M~DID~C(0)@n' & |
  '_10@36R(2)|M~DI No.~C(0)@n_10@36R(2)|M~MID~C(0)@n_10@36R(2)|M~BID~C(0)@n_10@32R(2)|M' & |
  '~Broking~C(0)@n3@40R(1)|M~Amount~C(0)@n-14.2@28R(2)|M~Source~C(0)@n3@80L(2)|M~Info~C(0)@s50@'), |
  FROM(Queue:Browse:1),IMM,MSG('Browsing the Audit_ManagementProfit file')
                           LIST,AT(6,188,433,104),USE(?List),VSCROLL,FORMAT('36R(2)|M~MPID~C(0)@n_10@46R(2)|M~Tran' & |
  '. Date~C(0)@d6@38R(2)|M~IIN~C(0)@n_10@38R(2)|M~TIN~C(0)@n_10@36R(2)|M~DID~C(0)@n_10@' & |
  '36R(2)|M~DI No.~C(0)@n_10@36R(2)|M~MID~C(0)@n_10@36R(2)|M~BID~C(0)@n_10@32R(2)|M~Bro' & |
  'king~C(0)@n3@40R(1)|M~Amount~C(0)@n-14.2@28R(2)|M~Source~C(0)@n3@80L(2)|M~Info~C(0)@s50@'), |
  FROM(Queue:Browse),IMM,MSG('Browsing Records')
                         END
                       END
                       BUTTON('&Close'),AT(387,312,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       BUTTON('&Help'),AT(4,312,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

BRW1::LastSortOrder       BYTE
BRW6::LastSortOrder       BYTE
BRW1::SortHeader  CLASS(SortHeaderClassType) !Declare SortHeader Class
QueueResorted          PROCEDURE(STRING pString),VIRTUAL
                  END
BRW6::SortHeader  CLASS(SortHeaderClassType) !Declare SortHeader Class
QueueResorted          PROCEDURE(STRING pString),VIRTUAL
                  END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW_1                CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW_2                CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB2                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END

sql_        SQLQueryClass


Process_Class       CLASS,TYPE

Get_Dates       PROCEDURE()
Remove_Inv      PROCEDURE()
Load_Qs         PROCEDURE(ULONG p_BID=0)

Load_Q          PROCEDURE(BYTE p_SourceNo, ULONG p_BID)

    .




p_c                 Process_Class
File_View           VIEW(Audit_ManagementProfit)
                       PROJECT(MPA:MPID)
                       PROJECT(MPA:Tran_Date)
                       PROJECT(MPA:IID)
                       PROJECT(MPA:TIN)
                       PROJECT(MPA:DID)
                       PROJECT(MPA:DINo)
                       PROJECT(MPA:MID)
                       PROJECT(MPA:BID)
                       PROJECT(MPA:Broking)
                       PROJECT(MPA:Amount)
                       PROJECT(MPA:Source)
                       PROJECT(MPA:Info)
                     END


View_File       ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()               ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Profit_Audit')
  SELF.Request = GlobalRequest                    ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                     ! Set this windows ErrorManager to the global ErrorManager
  BIND('LOC:Selected_Date',LOC:Selected_Date)     ! Added by: BrowseBox(ABC)
  BIND('LOC:Selected_Date_To',LOC:Selected_Date_To) ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                            ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)        ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)        ! Add the close control to the window manger
  END
  Relate:Audit_ManagementProfit.Open              ! File Audit_ManagementProfit used by this procedure, so make sure it's RelationManager is open
  Relate:Audit_ManagementProfit_Alias.Open        ! File Audit_ManagementProfit_Alias used by this procedure, so make sure it's RelationManager is open
  Relate:Branches.SetOpenRelated()
  Relate:Branches.Open                            ! File Branches used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW_1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Audit_ManagementProfit,SELF) ! Initialize the browse manager
  BRW_2.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:Audit_ManagementProfit_Alias,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                          ! Open window
  Do DefineListboxStyle
  BRW_1.Q &= Queue:Browse:1
  BRW_1.FileLoaded = 1                            ! This is a 'file loaded' browse
  BRW_1.AddSortOrder(,)                           ! Add the sort order for  for sort order 1
  BRW_1.AppendOrder('+MPA:MPID')                  ! Append an additional sort order
  BRW_1.SetFilter('((MPA:Source = 1) AND ((0 >= LOC:Selected_Date) OR (MPA:Tran_Date >= LOC:Selected_Date AND MPA:Tran_Date << LOC:Selected_Date_To)))') ! Apply filter expression to browse
  BRW_1.AddField(MPA:MPID,BRW_1.Q.MPA:MPID)       ! Field MPA:MPID is a hot field or requires assignment from browse
  BRW_1.AddField(MPA:Tran_Date,BRW_1.Q.MPA:Tran_Date) ! Field MPA:Tran_Date is a hot field or requires assignment from browse
  BRW_1.AddField(MPA:IID,BRW_1.Q.MPA:IID)         ! Field MPA:IID is a hot field or requires assignment from browse
  BRW_1.AddField(MPA:TIN,BRW_1.Q.MPA:TIN)         ! Field MPA:TIN is a hot field or requires assignment from browse
  BRW_1.AddField(MPA:DID,BRW_1.Q.MPA:DID)         ! Field MPA:DID is a hot field or requires assignment from browse
  BRW_1.AddField(MPA:DINo,BRW_1.Q.MPA:DINo)       ! Field MPA:DINo is a hot field or requires assignment from browse
  BRW_1.AddField(MPA:MID,BRW_1.Q.MPA:MID)         ! Field MPA:MID is a hot field or requires assignment from browse
  BRW_1.AddField(MPA:BID,BRW_1.Q.MPA:BID)         ! Field MPA:BID is a hot field or requires assignment from browse
  BRW_1.AddField(MPA:Broking,BRW_1.Q.MPA:Broking) ! Field MPA:Broking is a hot field or requires assignment from browse
  BRW_1.AddField(MPA:Amount,BRW_1.Q.MPA:Amount)   ! Field MPA:Amount is a hot field or requires assignment from browse
  BRW_1.AddField(MPA:Source,BRW_1.Q.MPA:Source)   ! Field MPA:Source is a hot field or requires assignment from browse
  BRW_1.AddField(MPA:Info,BRW_1.Q.MPA:Info)       ! Field MPA:Info is a hot field or requires assignment from browse
  BRW_2.Q &= Queue:Browse
  BRW_2.AddSortOrder(,)                           ! Add the sort order for  for sort order 1
  BRW_2.AddField(MPA_A:MPID,BRW_2.Q.MPA_A:MPID)   ! Field MPA_A:MPID is a hot field or requires assignment from browse
  BRW_2.AddField(MPA_A:Tran_Date,BRW_2.Q.MPA_A:Tran_Date) ! Field MPA_A:Tran_Date is a hot field or requires assignment from browse
  BRW_2.AddField(MPA_A:IID,BRW_2.Q.MPA_A:IID)     ! Field MPA_A:IID is a hot field or requires assignment from browse
  BRW_2.AddField(MPA_A:TIN,BRW_2.Q.MPA_A:TIN)     ! Field MPA_A:TIN is a hot field or requires assignment from browse
  BRW_2.AddField(MPA_A:DID,BRW_2.Q.MPA_A:DID)     ! Field MPA_A:DID is a hot field or requires assignment from browse
  BRW_2.AddField(MPA_A:DINo,BRW_2.Q.MPA_A:DINo)   ! Field MPA_A:DINo is a hot field or requires assignment from browse
  BRW_2.AddField(MPA_A:MID,BRW_2.Q.MPA_A:MID)     ! Field MPA_A:MID is a hot field or requires assignment from browse
  BRW_2.AddField(MPA_A:BID,BRW_2.Q.MPA_A:BID)     ! Field MPA_A:BID is a hot field or requires assignment from browse
  BRW_2.AddField(MPA_A:Broking,BRW_2.Q.MPA_A:Broking) ! Field MPA_A:Broking is a hot field or requires assignment from browse
  BRW_2.AddField(MPA_A:Amount,BRW_2.Q.MPA_A:Amount) ! Field MPA_A:Amount is a hot field or requires assignment from browse
  BRW_2.AddField(MPA_A:Source,BRW_2.Q.MPA_A:Source) ! Field MPA_A:Source is a hot field or requires assignment from browse
  BRW_2.AddField(MPA_A:Info,BRW_2.Q.MPA_A:Info)   ! Field MPA_A:Info is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize) ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                           ! Add resizer to window manager
  !    BRW_2.SetFilter('(MPA_A:Source = 2) AND ((0 >= LOC:Selected_Date) OR (MPA_A:Tran_Date >= LOC:Selected_Date AND MPA_A:Tran_Date << LOC:Selected_Date_To))')
      ! Apply filter expression to browse
  
  INIMgr.Fetch('Browse_Profit_Audit',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                  ! Reset required after window size altered by INI manager
      sql_.Init(GLO:DBOwner, '_SQLTemp2')
  
      p_c.Get_Dates()
  FDB2.Init(?LOC:BranchName,Queue:FileDrop.ViewPosition,FDB2::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB2.Q &= Queue:FileDrop
  FDB2.AddSortOrder(BRA:Key_BranchName)
  FDB2.AddField(BRA:BranchName,FDB2.Q.BRA:BranchName) !List box control field - type derived from field
  FDB2.AddField(BRA:BID,FDB2.Q.BRA:BID) !Primary key field - type derived from field
  FDB2.AddUpdateField(BRA:BID,LOC:BID)
  ThisWindow.AddItem(FDB2.WindowComponent)
  FDB2.DefaultFill = 0
  BRW_2.AddToolbarTarget(Toolbar)                 ! Browse accepts toolbar control
  BRW_2.ToolbarItem.HelpButton = ?Help
  BRW1::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW1::FormatManager.Init('TRNISDCT','Browse_Profit_Audit',1,?Browse:1,1,BRW1::PopupTextExt,Queue:Browse:1,12,LFM_CFile,LFM_CFile.Record)
  BRW1::FormatManager.BindInterface(,,,'.\TRNISDCT.INI')
  BRW6::FormatManager.SaveFormat = True
  ! List Format Manager initialization
  BRW6::FormatManager.Init('TRNISDCT','Browse_Profit_Audit',1,?List,6,BRW6::PopupTextExt,Queue:Browse,12,LFM_CFile,LFM_CFile.Record)
  BRW6::FormatManager.BindInterface(,,,'.\TRNISDCT.INI')
  SELF.SetAlerts()
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW1::SortHeader.Init(Queue:Browse:1,?Browse:1,'','',BRW1::View:Browse,MPA:PKey_MPID)
  BRW1::SortHeader.UseSortColors = False
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW6::SortHeader.Init(Queue:Browse,?List,'','',BRW6::View:Browse,MPA_A:PKey_MPID)
  BRW6::SortHeader.UseSortColors = False
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Audit_ManagementProfit.Close
    Relate:Audit_ManagementProfit_Alias.Close
    Relate:Branches.Close
  !Kill the Sort Header
  BRW1::SortHeader.Kill()
  !Kill the Sort Header
  BRW6::SortHeader.Kill()
  END
  ! List Format Manager destructor
  BRW1::FormatManager.Kill() 
  ! List Format Manager destructor
  BRW6::FormatManager.Kill() 
  IF SELF.Opened
    INIMgr.Update('Browse_Profit_Audit',QuickWindow)       ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW1::SortHeader.SetAlerts()
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW6::SortHeader.SetAlerts()


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LOC:Selected_Date
          LOC:Selected_Date_To    = LOC:Selected_Date + 1
    OF ?Button_Refresh
      ThisWindow.Update()
          p_c.Load_Qs(LOC:BID)
    OF ?Button_Rem
      ThisWindow.Update()
          p_c.Remove_Inv()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Take Sort Headers Events
  IF BRW1::SortHeader.TakeEvents()
     RETURN Level:Notify
  END
  !Take Sort Headers Events
  IF BRW6::SortHeader.TakeEvents()
     RETURN Level:Notify
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LOC:Selected_Date
          LOC:Selected_Date_To    = LOC:Selected_Date + 1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          p_c.Load_Qs()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Process_Class.Remove_Inv       PROCEDURE()

Idx         LONG

    CODE
    Idx     = RECORDS(Q_Source1) + 1
    LOOP
       Idx -= 1
       GET(Q_Source1, Idx)
       IF ERRORCODE()
          BREAK
       .

       IF SRC1:MPA:IID ~= 0
          SRC2:MPA:IID      = SRC1:MPA:IID
          SRC2:MPA:Info     = SRC1:MPA:Info

          GET(Q_Source2, SRC2:MPA:IID)
       ELSE
          SRC2:MPA:TIN      = SRC1:MPA:TIN
          SRC2:MPA:Info     = SRC1:MPA:Info

          GET(Q_Source2, SRC2:MPA:TIN)
       .

       IF ERRORCODE()
          CYCLE
       .

       IF SRC1:MPA:Amount = SRC2:MPA:Amount
          DELETE(Q_Source1)
          DELETE(Q_Source2)
    .  .


    ?Prompt_Note{PROP:Text} = 'Source 1: ' & RECORDS(Q_Source1) & ',  Source 2: ' & RECORDS(Q_Source2)
    DISPLAY
    RETURN
Process_Class.Get_Dates          PROCEDURE()
    CODE
    FREE(LOC:Dates_Q)
    L_DQ:Date   = -1
    ADD(LOC:Dates_Q)

    sql_.PropSQL('SELECT Tran_DateTime FROM Audit_ManagementProfit GROUP BY Tran_DateTime')
!                SQL_Get_DateT_G(LO:From_Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(LO:To_Date + 1,,1))
    LOOP
       IF sql_.Next_Q() <= 0
          BREAK
       .


       LOC:Date_Group       = SQL_Ret_DateT_G(sql_.Get_El(1))
       L_DQ:Date            = L_DG:Date

!    db.debugout('L_DQ:Date: ' & L_DQ:Date & ',   el1: ' & sql_.Get_El(1))

       ADD(LOC:Dates_Q)
    .

    RETURN
Process_Class.Load_Qs             PROCEDURE(ULONG p_BID=0)
    CODE
    SELF.Load_Q(1, p_BID)
    SELF.Load_Q(2, p_BID)
    RETURN


Process_Class.Load_Q            PROCEDURE(BYTE p_SourceNo, ULONG p_BID)
    CODE
    EXECUTE p_SourceNo
       FREE(Q_Source1)
       FREE(Q_Source2)
    .

    View_File.Init(File_View, Relate:Audit_ManagementProfit)
    View_File.AddSortOrder()
    View_File.SetFilter('MPA:Source = ' & p_SourceNo,'1')
    IF p_BID ~= 0
       View_File.SetFilter('MPA:BID = ' & p_BID,'2')
    .

    IF LOC:Selected_Date <= 0
    ELSE
       View_File.SetFilter('(MPA:Tran_Date >= LOC:Selected_Date AND MPA:Tran_Date << LOC:Selected_Date_To)','Date')
    .

!                SQL_Get_DateT_G(LO:From_Date,,1) & ' AND InvoiceDateAndTime < ' & SQL_Get_DateT_G(LO:To_Date + 1,,1))
    View_File.Reset()
    LOOP
       IF View_File.Next() ~= LEVEL:Benign
          BREAK
       .

!       EXECUTE p_SourceNo
!          Q_Source1 :=: MPA:Record
!          Q_Source2 :=: MPA:Record
!       .

!          Q_Source1 :=: MPA:Record

!        message('MPA:MPID: ' & MPA:MPID)
       CASE p_SourceNo
       OF 1
          Q_Source1.MPA:MPID        = MPA:MPID
          Q_Source1.MPA:Tran_Date   = MPA:Tran_Date
          Q_Source1.MPA:IID         = MPA:IID
          Q_Source1.MPA:TIN         = MPA:TIN
          Q_Source1.MPA:DID         = MPA:DID
          Q_Source1.MPA:DINo        = MPA:DINo
          Q_Source1.MPA:MID         = MPA:MID
          Q_Source1.MPA:BID         = MPA:BID
          Q_Source1.MPA:Broking     = MPA:Broking
          Q_Source1.MPA:Amount      = MPA:Amount
          Q_Source1.MPA:Source      = MPA:Source
          Q_Source1.MPA:Info        = MPA:Info
       OF 2
          Q_Source2.MPA:MPID        = MPA:MPID
          Q_Source2.MPA:Tran_Date   = MPA:Tran_Date
          Q_Source2.MPA:IID         = MPA:IID
          Q_Source2.MPA:TIN         = MPA:TIN
          Q_Source2.MPA:DID         = MPA:DID
          Q_Source2.MPA:DINo        = MPA:DINo
          Q_Source2.MPA:MID         = MPA:MID
          Q_Source2.MPA:BID         = MPA:BID
          Q_Source2.MPA:Broking     = MPA:Broking
          Q_Source2.MPA:Amount      = MPA:Amount
          Q_Source2.MPA:Source      = MPA:Source
          Q_Source2.MPA:Info        = MPA:Info
       .

       EXECUTE p_SourceNo
          ADD(Q_Source1)
          ADD(Q_Source2)
    .  .

    EXECUTE p_SourceNo
       SORT(Q_Source1, Q_Source1.MPA:IID, Q_Source1.MPA:TIN, SRC1:MPA:Info)
       SORT(Q_Source2, Q_Source2.MPA:IID, Q_Source2.MPA:TIN, SRC2:MPA:Info)
    .

    ?Prompt_Note{PROP:Text} = 'Source 1: ' & RECORDS(Q_Source1) & ',  Source 2: ' & RECORDS(Q_Source2)
    RETURN

BRW_1.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW1::LastSortOrder<>NewOrder THEN
     BRW1::SortHeader.ClearSort()
  END
  IF BRW1::LastSortOrder <> NewOrder THEN
     BRW1::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW1::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_1.TakeNewSelection PROCEDURE

  CODE
  IF BRW1::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW1::PopupTextExt = ''
        BRW1::PopupChoiceExec = True
        BRW1::FormatManager.MakePopup(BRW1::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW1::PopupTextExt = '|-|' & CLIP(BRW1::PopupTextExt)
        END
        BRW1::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW1::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW1::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW1::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW1::PopupChoiceOn AND BRW1::PopupChoiceExec THEN
     BRW1::PopupChoiceExec = False
     BRW1::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW1::PopupTextExt)
     BRW1::SortHeader.RestoreHeaderText()
     BRW_1.RestoreSort()
     IF BRW1::FormatManager.DispatchChoice(BRW1::PopupChoice)
        BRW1::SortHeader.ResetSort()
     ELSE
        BRW1::SortHeader.SortQueue()
     END
  END


BRW_2.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW6::LastSortOrder<>NewOrder THEN
     BRW6::SortHeader.ClearSort()
  END
  IF BRW6::LastSortOrder <> NewOrder THEN
     BRW6::FormatManager.SetCurrentFormat(CHOOSE(NewOrder>0,2,NewOrder+2),'SortOrder'&CHOOSE(NewOrder>0,1,NewOrder+1))
  END
  BRW6::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_2.TakeNewSelection PROCEDURE

  CODE
  IF BRW6::PopupChoiceOn THEN
     IF KEYCODE() = MouseRightUp
        BRW6::PopupTextExt = ''
        BRW6::PopupChoiceExec = True
        BRW6::FormatManager.MakePopup(BRW6::PopupTextExt)
        IF SELF.Popup.GetItems() THEN
           BRW6::PopupTextExt = '|-|' & CLIP(BRW6::PopupTextExt)
        END
        BRW6::FormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
        SELF.Popup.AddMenu(CLIP(BRW6::PopupTextExt),SELF.Popup.GetItems()+1)
        BRW6::FormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
     ELSE
        BRW6::PopupChoiceExec = False
     END
  END
  PARENT.TakeNewSelection
  IF BRW6::PopupChoiceOn AND BRW6::PopupChoiceExec THEN
     BRW6::PopupChoiceExec = False
     BRW6::PopupChoice = SELF.Popup.GetLastNumberSelection()
     SELF.Popup.DeleteMenu(BRW6::PopupTextExt)
     BRW6::SortHeader.RestoreHeaderText()
     BRW_2.RestoreSort()
     IF BRW6::FormatManager.DispatchChoice(BRW6::PopupChoice)
        BRW6::SortHeader.ResetSort()
     ELSE
        BRW6::SortHeader.SortQueue()
     END
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Top, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Top
  SELF.SetStrategy(?Browse:1, Resize:LockYPos, Resize:ConstantBottomCenter) ! Override strategy for ?Browse:1
  SELF.SetStrategy(?List, Resize:FixYCenter, Resize:ConstantBottom) ! Override strategy for ?List


FDB2.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      ! Add an All
      Queue:FileDrop.BRA:BranchName      = 'All'
      GET(Queue:FileDrop, Queue:FileDrop.BRA:BranchName)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop)
         Queue:FileDrop.BRA:BranchName      = 'All'
         Queue:FileDrop.BRA:BID             = 0
         ADD(Queue:FileDrop)
      .
  RETURN ReturnValue

BRW1::SortHeader.QueueResorted       PROCEDURE(STRING pString)
  CODE
    IF pString = ''
       BRW_1.RestoreSort()
       BRW_1.ResetSort(True)
    ELSE
       BRW_1.ReplaceSort(pString)
    END
BRW6::SortHeader.QueueResorted       PROCEDURE(STRING pString)
  CODE
    IF pString = ''
       BRW_2.RestoreSort()
       BRW_2.ResetSort(True)
    ELSE
       BRW_2.ReplaceSort(pString)
    END
!!! <summary>
!!! Generated from procedure template - Source
!!! In MS SQL 2008 executes the passed SQL if the passed table doesn't exists
!!! </summary>
CreateFile           PROCEDURE  (String pTable,String pCreateStatement) ! Declare Procedure

  CODE
  If Access:_Statement_Run_Desc.Open() = Level:Benign Then     
     If Access:_Statement_Run_Desc.UseFile() = Level:Benign Then        
        _Statement_Run_Desc{Prop:SQL} = 'SELECT 0, SERVERPROPERTY(''productversion'')'
        If ~FileErrorCode() Then           
           Next(_Statement_Run_Desc)
           If ~ErrorCode() Then              
              v# = STDES:RunDescription[1 : InString('.',STDES:RunDescription,1,1)-1]              
              If v# >= 10 Then
                 _Statement_Run_Desc{Prop:SQL} = 'SELECT COUNT(*) FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].['&pTable&']'') AND type in (N''U'')'                 
                 If ~FileErrorCode() Then
                    Next(_Statement_Run_Desc)
                    If ~ErrorCode() Then
                       If ~STDES:SRDID Then
                          _Statement_Run_Desc{PROP:SQL} = pCreateStatement                          
                       End
                    End
                 End
              End
           End
        End
     End
     Access:_Statement_Run_Desc.Close
  End
