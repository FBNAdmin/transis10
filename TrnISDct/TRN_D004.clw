

   MEMBER('TRANSIS.CLW')                                   ! This is a MEMBER module

                     MAP
                       INCLUDE('TRN_D004.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Returns delivered units of completed trips
!!! </summary>
Get_TripItem_Del_Units PROCEDURE  (p:DIID, p:Option)       ! Declare Procedure
LOC:Loaded           USHORT                                !Number of units
LOC:ToAdd            LONG                                  !
Tek_Failed_File     STRING(100)

View_TD         VIEW(TripSheetDeliveriesAlias)
    PROJECT(A_TRDI:DIID, A_TRDI:UnitsDelivered, A_TRDI:UnitsNotAccepted, A_TRDI:UnitsLoaded)
    .

View_TD2        VIEW(TripSheetDeliveriesAlias)
    PROJECT(A_TRDI:DIID, A_TRDI:UnitsDelivered, A_TRDI:UnitsNotAccepted, A_TRDI:TRID, A_TRDI:UnitsLoaded)
       JOIN(A_TRI:PKey_TID, A_TRDI:TRID)
       PROJECT(A_TRI:TRID,A_TRI:State)
    .  .

Trip_View       ViewManager





    

  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetDeliveriesAlias.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:TripSheetsAlias.Open()
     .
     Access:TripSheetDeliveriesAlias.UseFile()
     Access:TripSheetsAlias.UseFile()
    ! (p:DIID, p:Option)
    !   p:Option
    !       0. Delivered Units
    !       1. (Delivered & Un Delivered) on incomplete Trips
    !       2. Delivered & Rejected
    !       3. (Delivered & Rejected & Un Delivered) on incomplete Trips
    !       4. Delivered & Rejected & Un Delivered on ALL Trips
    ! A delivery item can be loaded on different trailers
    ! Item can appear on more than one ManifestLoad but a single ManifestLoad can only have 1 instance of the Item

!    db.debugout('[Get_TripItem_Del_Units]  -----------  In  -----------   p:DIID: ' & p:DIID & ',   p:Option: ' & p:Option)

    IF p:Option = 1 OR p:Option = 3 OR p:Option = 4
       PUSHBIND()
       BIND('A_TRI:State',A_TRI:State)
       BIND('A_TRDI:UnitsDelivered',A_TRDI:UnitsDelivered)
       BIND('A_TRDI:UnitsNotAccepted',A_TRDI:UnitsNotAccepted)
    .

    IF p:Option = 1 OR p:Option = 3 OR p:Option = 4
       Trip_View.Init(View_TD2, Relate:TripSheetDeliveriesAlias)
    ELSE
       Trip_View.Init(View_TD, Relate:TripSheetDeliveriesAlias)
    .
    Trip_View.AddSortOrder(A_TRDI:FKey_DIID)
    Trip_View.AppendOrder('A_TRDI:TDID')
    Trip_View.AddRange(A_TRDI:DIID,p:DIID)

    IF p:Option = 1 OR p:Option = 3
       ! Loading, Loaded, On Route, Transferred, Finalised
       !    1       2         3          4            5
!       Trip_View.SetFilter('A_TRI:State < 4 AND A_TRDI:UnitsDelivered = 0 AND A_TRDI:UnitsNotAccepted = 0')
       Trip_View.SetFilter('A_TRI:State < 4')       ! 27 Feb 07
     .

    Trip_View.Reset()
!    IF ERRORCODE()
!       MESSAGE('Error on Reset: ' & ERROR())
!    .
    LOOP
       IF Trip_View.Next() ~= LEVEL:Benign
          BREAK
       .

   db.debugout('[Get_TripItem_Del_Units]  p:DIID: ' & p:DIID & ',   A_TRDI:UnitsDelivered: ' & A_TRDI:UnitsDelivered & ',   A_TRDI:UnitsNotAccepted: ' & A_TRDI:UnitsNotAccepted & ',   A_TRDI:UnitsLoaded: ' & A_TRDI:UnitsLoaded & ',   p:Option: ' & p:Option)

       IF p:Option < 2
          LOC:ToAdd     = A_TRDI:UnitsDelivered
       ELSE
          LOC:ToAdd     = A_TRDI:UnitsDelivered + A_TRDI:UnitsNotAccepted
       .

       LOC:Loaded      += LOC:ToAdd

       ! Only add in UnitsLoaded when no delivered units and state is not finalised
       IF p:Option = 1 OR p:Option = 3 OR p:Option = 4
          ! add in A_TRDI:UnitsLoaded less any that have already been rejected or delivered
          LOC:Loaded   += (A_TRDI:UnitsLoaded - (A_TRDI:UnitsDelivered + A_TRDI:UnitsNotAccepted))
    .  .
    Trip_View.Kill()

    IF p:Option = 1 OR p:Option = 3 OR p:Option = 4
       UNBIND('A_TRI:State')
       UNBIND('A_TRDI:UnitsDelivered')
       UNBIND('A_TRDI:UnitsNotAccepted')
       POPBIND()
    .

!    db.debugout('[Get_TripItem_Del_Units]  -----------  Out  -----------   LOC:Loaded: ' & LOC:Loaded)
    !   p:Option
    !       0. Delivered Units
    !       1. Delivered & Un Delivered on incomplete Trips
    !       2. Delivered & Rejected
    !       3. Delivered & Rejected & Un Delivered on incomplete Trips
    !       4. Delivered & Rejected & Un Delivered on ALL Trips

!               old
!    ! (p:DIID, p:Option)
!    !   p:Option
!    !       0. Delivered Units
!    !       1. Delivered & Un Delivered on incomplete Trips
!    !       2. Delivered & Rejected
!    !       3. Delivered & Rejected & Un Delivered on incomplete Trips
!    ! A delivery item can be loaded on different trailers
!    ! Item can appear on more than one ManifestLoad but a single ManifestLoad can only have 1 instance of the Item
!
!    CLEAR(A_TRDI:Record,-1)
!    A_TRDI:DIID          = p:DIID
!    SET(A_TRDI:FKey_DIID, A_TRDI:FKey_DIID)
!    LOOP
!       IF Access:TripSheetDeliveriesAlias.TryNext() ~= LEVEL:Benign
!          BREAK
!       .
!       IF A_TRDI:DIID ~= p:DIID
!          BREAK
!       .
!
!       IF p:Option < 2
!          LOC:Loaded       += A_TRDI:UnitsDelivered
!       ELSE
!          LOC:Loaded       += A_TRDI:UnitsDelivered + A_TRDI:UnitsNotAccepted
!       .
!
!       ! Only add in UnitsLoaded when no delivered units
!       IF A_TRDI:UnitsDelivered = 0 AND (p:Option = 1 OR p:Option = 3)
!          A_TRI:TRID        = A_TRDI:TRID
!          IF Access:TripSheetsAlias.TryFetch(A_TRI:PKey_TID) = LEVEL:Benign
!             IF A_TRI:State ~= 4                ! Not finalised
!                ! add in A_TRDI:UnitsLoaded
!                LOC:Loaded += A_TRDI:UnitsLoaded
!    .  .  .  .
    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return(LOC:Loaded)

    Exit

CloseFiles     Routine
    Relate:TripSheetDeliveriesAlias.Close()
    Relate:TripSheetsAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover)  -  Global
!!! </summary>
Add_Log              PROCEDURE  (p:Text, p:Heading, p:Name, p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt) ! Declare Procedure
LOC:Date             LONG,STATIC                           !
LOC:Name             STRING(255)                           !
LOC:No               LONG                                  !
LOC:Extension        STRING(4)                             !
LOC:Name_Static      STRING('TrIS_Def {247}'),STATIC       !
LOC:Debug            BYTE                                  !On or Off`
Log_File        FILE,DRIVER('ASCII'),CREATE,PRE(LF),THREAD
Rec         RECORD
Text            STRING(2000)
            .   .

  CODE
    ! (p:Text, p:Heading, p:Name  , p:Overwrite, p:Name_Opt, p:Rollover, p:Date_Time_Opt)
    ! (STRING, STRING   , <STRING>, BYTE=0     , BYTE=0    , BYTE=1    , BYTE=1         )
    ! p:Name_Opt
    !   0.  - None
    !   1.  - Set static name to p:Name
    !   2.  - Use static name
    ! p:Rollover
    !   0.  - No rollover of file allowed
    !   1.  - Rollover allowed (default)
    ! p:Date_Time_Opt
    !   0.  - Off
    !   1.  - On (default)

    LOC:Debug   = 0

    IF OMITTED(3) OR CLIP(p:Name) = ''
       LOC:Name         = 'TransIS'
    ELSE
       LOC:Name         = CLIP(p:Name)
    .

    EXECUTE p:Name_Opt
       LOC:Name_Static  = LOC:Name                  ! p:Name - cant use this as it may be blank, Name will contain it
       LOC:Name         = LOC:Name_Static           ! LOC:Name_Static could be empty, we will default it
    .

    LOC:Extension       = '.log'

    len_#       = LEN(CLIP(LOC:Name))
    dot_pos_#   = INSTRING('.', LOC:Name, -1, len_#)
    IF len_# - dot_pos_# <= 3
       ! Extension is specified!
       LOC:Extension        = SUB(LOC:Name, dot_pos_#, 4)
       LOC:Name             = SUB(LOC:Name, 1, dot_pos_# - 1)
    .

    Log_File{PROP:Name}     = CLIP(LOC:Name) & LOC:Extension
    IF EXISTS(CLIP(Log_File{PROP:Name})) <= 0 OR p:Overwrite = TRUE
       LOC:Date             = 0
       CREATE(Log_File)
       IF ERRORCODE()
          IF OMITTED(3) OR CLIP(p:Name) = ''
             MESSAGE('Failed to create file.||File: ' & CLIP(Log_File{PROP:Name}) & '||Error (' & ERRORCODE() & '): ' & ERROR(), 'Add_Log', ICON:Hand)
          ELSE
             MESSAGE('Failed to create file.||File: ' & CLIP(Log_File{PROP:Name}) & '|(p:Name was: ' & CLIP(p:Name) & ')||Error: ' & ERROR(), 'Add_Log', ICON:Hand)
    .  .  .

    LOOP 10 TIMES
       LOC:No   += 1

       SHARE(Log_File)
       IF ERRORCODE()
          IF LOC:No = 1
             ! What now? Try new name
             Log_File{PROP:Name}    = CLIP(LOC:Name) & '-' & LOC:No & LOC:Extension

             IF EXISTS(CLIP(Log_File{PROP:Name})) <= 0
                LOC:Date            = 0
                CREATE(Log_File)
                IF ERRORCODE()
                   MESSAGE('Failed to create file.||File: ' & CLIP(Log_File{PROP:Name}) & '||Error: ' & ERROR(), 'Add_Log', ICON:Hand)
             .  .
             CYCLE
          ELSE
             MESSAGE('Failed to create / open log file.||Error: ' & ERROR() & '||File: ' & CLIP(Log_File{PROP:Name}), 'Log File Error', ICON:Hand)
             BREAK
       .  .

       IF p:Rollover = TRUE AND BYTES(Log_File) > 250000
          LOC:Date              = 0

          ! Create a new file: -1 -2 etc
          CLOSE(Log_File)

          Log_File{PROP:Name}   = CLIP(LOC:Name) & '-' & LOC:No & LOC:Extension

          IF EXISTS(CLIP(Log_File{PROP:Name})) <= 0
             LOC:Date           = 0
             CREATE(Log_File)
             IF ERRORCODE()
                MESSAGE('Failed to create next file.||File: ' & CLIP(Log_File{PROP:Name}) & '||Error: ' & ERROR(), 'Add_Log', ICON:Hand)
          .  .
          CYCLE
       .

       ! If we get here we have an open file less than 250000 bytes
       IF p:Date_Time_Opt = TRUE
          IF LOC:Date ~= TODAY()
             LOC:Date      = TODAY()
             LF:Text       = '[--' & FORMAT(LOC:Date, @d6) & '--]'
             ADD(Log_File)
          .

          LF:Text          = FORMAT(CLOCK(), @t4) & ',[' & CLIP(p:Heading) & '],' & CLIP(p:Text)
       ELSE
          LF:Text          = CLIP(p:Text)
       .

       ADD(Log_File)
!       IF ERRORCODE()
!          MESSAGE('Failed to add to file.||File: ' & CLIP(Log_File{PROP:Name}) & '||Error: ' & ERROR(), 'Add_Log', ICON:Hand)
!       .

       IF LOC:Debug = 1
          db.debugout('[Add_Log]  Error: ' & CLIP(ERROR()) & ',  Name: ' & Log_File{PROP:Name})
       .

       CLOSE(Log_File)
       BREAK
    .

    RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! ** global **   ** see thread event limitation notes inside ***
!!! </summary>
Thread_Send_Procs_Events PROCEDURE  (p:Procedure, p:Event) ! Declare Procedure
LOC:Idx              ULONG                                 !
LOC:Thread_Q         QUEUE,PRE(L_TQ)                       !
Thread               LONG                                  !
                     END                                   !
LOC:Setup_Group      GROUP,PRE(L_SG),STATIC                !static
Checked              BYTE                                  !
Thread_Send_Proc_Events BYTE                               !
                     END                                   !

  CODE
    IF L_SG:Checked = FALSE
       L_SG:Checked                 = TRUE
       L_SG:Thread_Send_Proc_Events = GETINI('Setup', 'Thread_Send_Proc_Events', 1, GLO:Local_INI)
    .

    db.debugout('[Thread_Send_Procs_Events]  Start - Proc: ' & CLIP(p:Procedure) & ',  Event: ' & p:Event & ',  Thread_Send_Proc_Events: ' & L_SG:Thread_Send_Proc_Events & ',  Q Recs: ' & RECORDS(GLO:Thread_Q) & '    - Clock: ' & CLOCK())

    IF L_SG:Thread_Send_Proc_Events > 0
       ! (p:Procedure, p:Event)
       ! Check through the Q for threads of this Procedure and post events when found

       ! Of course window events are only received by the top most window of a thread

       LOC:Idx     = 0
       LOOP
          LOC:Idx += 1
          GET(GLO:Thread_Q, LOC:Idx)
          IF ERRORCODE()
             BREAK
          .

          L_TQ:Thread  = GL_TQ:Thread
          GET(LOC:Thread_Q, L_TQ:Thread)
          IF ~ERRORCODE()
             CYCLE
          .

    db.debugout('[Thread_Send_Procs_Events]  GL_TQ:ProcedureName: ' & CLIP(GL_TQ:ProcedureName) & ' = p:Procedure: ' & p:Procedure & ',   Idx: ' & LOC:Idx)

          IF CLIP(UPPER(GL_TQ:ProcedureName)) = CLIP(UPPER(p:Procedure))
             L_TQ:Thread  = GL_TQ:Thread
             ADD(LOC:Thread_Q)

    db.debugout('[Thread_Send_Procs_Events]  Posting for p:Procedure: ' & p:Procedure & '  on thread: ' & GL_TQ:Thread & ',   Idx: ' & LOC:Idx)

             POST(p:Event,, GL_TQ:Thread)
    .  .  .
!!! <summary>
!!! Generated from procedure template - Source
!!! ** global **   
!!! </summary>
Thread_Add_Del_Check_Global PROCEDURE  (p:Procedure, p:Thread, p:Option) ! Declare Procedure
L:Idx                ULONG                                 !
LOC:Return           LONG                                  !

  CODE
    ! (p:Procedure, p:Thread, p:Option)
    ! p:Option
    !   0. - Add
    !   1. - Delete last instance
    !   2. - Check and return true / false
    !   3. - Check and return no. of instances
    db.debugout('[Thread_Add_Del_Check_Global]  Start - Proc: ' & CLIP(p:Procedure) & ',  Thread: ' & p:Thread & ',  Option: ' & p:Option & '   - Clock: ' & CLOCK())


    IF p:Option = 0
       GL_TQ:ProcedureName     = UPPER(p:Procedure)
       GL_TQ:Thread            = p:Thread
       GL_TQ:Q_ID              = RECORDS(GLO:Thread_Q)
       ADD(GLO:Thread_Q)

       SORT(GLO:Thread_Q, GL_TQ:ProcedureName, GL_TQ:Thread, -GL_TQ:Q_ID)
    ELSIF p:Option = 1
       ! Look for last instance for proc, thread
       L:Idx    = 0
       LOOP
          L:Idx += 1
          GET(GLO:Thread_Q, L:Idx)
          IF ERRORCODE()
             BREAK
          .
          IF GL_TQ:ProcedureName ~= UPPER(p:Procedure) OR GL_TQ:Thread ~= p:Thread
             CYCLE
          .

          DELETE(GLO:Thread_Q)
          BREAK
       .
    ELSIF p:Option = 2
       GL_TQ:ProcedureName      = UPPER(p:Procedure)
       GL_TQ:Thread             = p:Thread

       IF p:Thread = 0
          GET(GLO:Thread_Q, GL_TQ:ProcedureName)
       ELSE
          GET(GLO:Thread_Q, GL_TQ:ProcedureName, GL_TQ:Thread)
       .

       IF ~ERRORCODE()
!       message('found with GL_TQ:ProcedureName: ' & CLIP(GL_TQ:ProcedureName) & ', Thread: ' & GL_TQ:Thread & |
!            ', GL_TQ:Q_ID: ' & GL_TQ:Q_ID)

          LOC:Return            = 1
       .
    ELSE
       L:Idx    = 0
       LOOP
          L:Idx += 1
          GET(GLO:Thread_Q, L:Idx)
          IF ERRORCODE()
             BREAK
          .
          IF CLIP(GL_TQ:ProcedureName) ~= CLIP(UPPER(p:Procedure))
             CYCLE
          .

          LOC:Return   += 1
    .  .

    RETURN(LOC:Return)
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Main_dct             PROCEDURE                             ! Declare Procedure

  CODE
