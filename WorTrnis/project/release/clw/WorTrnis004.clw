

   MEMBER('WorTrnis.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('BRWEXT.INC'),ONCE

                     MAP
                       INCLUDE('WORTRNIS004.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Manifest_24_04_14 PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Start            LONG                                  ! 
LOC:Screen_Group     GROUP,PRE(L_SG)                       ! 
Journey              STRING(70)                            ! Description
TransporterName      STRING(35)                            ! Transporters Name
CompositionName      STRING(35)                            ! 
Driver               STRING(35)                            ! Driver - Note these only apply to Horse & Combined types
MakeModels           STRING(200)                           ! Make & Model
Registrations        STRING(120)                           ! 
Licensing            STRING(200)                           ! Licensing details
Capacity             DECIMAL(6)                            ! In Kgs
No_Vehicles          LONG                                  ! Number of vehicles in combination
Tot_Weight           DECIMAL(8,2)                          ! In kg's
VC_Capacity          DECIMAL(7)                            ! Total capacity not to exceed this amount
User_Told_VC_Weight_Exceeded BYTE                          ! 
Original_VehComp_Tip STRING(200)                           ! 
No_DIs               SHORT                                 ! No. of DI's loaded on this Manifest
ChargesVAT           BYTE                                  ! This Transporter charges / pays VAT
VCID                 ULONG                                 ! Vehicle Composition ID
Notifications        BYTE                                  ! Turn On/Off notifications that will be sent based on the progress of the Manifest
                     END                                   ! 
LOC:Item_Description STRING(200)                           ! 
L_BG:Vehicle_Desc    STRING(100)                           ! 
LOC:PreviousCosts    QUEUE,PRE(L_PCQ)                      ! Previous Costs for this Transporter and Journey
TransporterName      STRING(50)                            ! Transporters Name
Rate                 DECIMAL(10,4)                         ! Rate for DI (debtors journey rate possibly adjusted) - Charge will be Rate multiplied by the total weight in kgs.
Cost                 DECIMAL(9,2)                          ! Cost for this leg (from Transporter)
DepartDate           DATE                                  ! Depated Date
Display              BYTE                                  ! 
                     END                                   ! 
L_SV:Prev_Cost       STRING(100)                           ! 
LOC:Load_Item_Warned_ID ULONG                              ! user has been warned of problem items - 1st ID warned of, used to check when done
LOC:Load_Item_Warned BYTE                                  ! user has been warned of problem items
LOC:Last_State       BYTE                                  ! State of this manifest
LOC:Rate_Group       GROUP,PRE(L_RG)                       ! 
Setup                BYTE                                  ! 
Setup_VCID           ULONG                                 ! Vehicle Composition ID
MinimiumLoad         DECIMAL(8)                            ! In Kgs
BaseCharge           DECIMAL(9,2)                          ! 
PerRate              DECIMAL(10,4)                         ! Above Minimium Load
TRID                 ULONG                                 ! Transporter Rate ID
                     END                                   ! 
LOC:Manifest_State_Group GROUP,PRE(L_MSG)                  ! 
No_Unloaded          ULONG                                 ! 
Unloaded_DIID_List   STRING(200)                           ! 
Item_List            STRING(500)                           ! 
No_MIDs              ULONG                                 ! That a DID is on
                     END                                   ! 
LOC:Prompts_Group    GROUP,PRE(L_PG)                       ! Prompt the user for these or not
Print_Manifest       BYTE                                  ! 
Create_Invoices      BYTE                                  ! 
Print_Delivery_Notes BYTE                                  ! 
Print_Invoices       BYTE                                  ! 
State_Progression    BYTE                                  ! 
                     END                                   ! 
LOC:Last_DID         ULONG                                 ! Delivery ID
LOC:Items_List_Group GROUP,PRE()                           ! 
L_IL:Weight          DECIMAL(8,2)                          ! In kg's
L_IL:Weight_Vol      DECIMAL(8,2)                          ! Volumetric weight
L_IL:Total_Charge    DECIMAL(11,2)                         ! Charge for the DI- Excludes VAT, Docs, Fuel, Insurance and Additional Charges charges
L_IL:2nd_Transporter STRING(35)                            ! Transporters Name
L_IL:2nd_TransPrice  DECIMAL(9,2)                          ! Cost for this leg (from Transporter)
L_IL:2nd_Trans_Area  STRING(50)                            ! Suburb
L_IL:2nd_Leg_TIDs    STRING(50)                            ! 
                     END                                   ! 
LOC:Manifest_Date_Orig DATE                                ! compared with date on OK, if differs for Manifest which has invoice then offer to update Manifest Invoice
LOC:Change_Trans_Rate GROUP,PRE(L_TRG)                     ! Get new rate if these change
TID                  ULONG                                 ! Transporter ID
JID                  ULONG                                 ! Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
VCID                 ULONG                                 ! Vehicle Composition ID
                     END                                   ! 
LOC:Transporter_Invoices_Browse GROUP,PRE()                ! 
L_TIG:Total          DECIMAL(12,2)                         ! 
                     END                                   ! 
LOC:Allow_Change_DI  BYTE                                  ! for unloaded manifests - certain users - set in setup
LOC:Truck_Group      GROUP,PRE(L_TG)                       ! 
CompositionName      STRING(35)                            ! 
VCID                 ULONG                                 ! Vehicle Composition ID
FreightLiner         STRING(30)                            ! 
FreightLinerTTID     ULONG                                 ! Track or Trailer ID
Trailer              STRING(30)                            ! 
TrailerTTID          ULONG                                 ! Track or Trailer ID
SuperLink            STRING(30)                            ! 
SuperLinkTTID        ULONG                                 ! Track or Trailer ID
Capacity             DECIMAL(8)                            ! 
                     END                                   ! 
LOC:Temps            GROUP,PRE(L_TMPG)                     ! 
Broking              BYTE(1)                               ! This is a broking transporter
                     END                                   ! 
BRW4::View:Browse    VIEW(ManifestLoad)
                       PROJECT(MAL:MLID)
                       PROJECT(MAL:MID)
                       PROJECT(MAL:TTID)
                       JOIN(TRU:PKey_TTID,MAL:TTID)
                         PROJECT(TRU:Registration)
                         PROJECT(TRU:Capacity)
                         PROJECT(TRU:TTID)
                       END
                       JOIN(MALD:FSKey_MLID_DIID,MAL:MLID)
                         PROJECT(MALD:UnitsLoaded)
                         PROJECT(MALD:DIID)
                         PROJECT(MALD:MLDID)
                         PROJECT(MALD:MLID)
                         JOIN(DELI:PKey_DIID,MALD:DIID)
                           PROJECT(DELI:ItemNo)
                           PROJECT(DELI:Units)
                           PROJECT(DELI:Type)
                           PROJECT(DELI:ContainerNo)
                           PROJECT(DELI:Weight)
                           PROJECT(DELI:DIID)
                           PROJECT(DELI:DID)
                           PROJECT(DELI:PTID)
                           PROJECT(DELI:CMID)
                           JOIN(DEL:PKey_DID,DELI:DID)
                             PROJECT(DEL:DINo)
                             PROJECT(DEL:DID)
                             PROJECT(DEL:CID)
                             JOIN(CLI:PKey_CID,DEL:CID)
                               PROJECT(CLI:ClientName)
                               PROJECT(CLI:CID)
                             END
                           END
                           JOIN(PACK:PKey_PTID,DELI:PTID)
                             PROJECT(PACK:Packaging)
                             PROJECT(PACK:PTID)
                           END
                           JOIN(COM:PKey_CMID,DELI:CMID)
                             PROJECT(COM:Commodity)
                             PROJECT(COM:CMID)
                           END
                         END
                       END
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?Browse:4
L_BG:Vehicle_Desc      LIKE(L_BG:Vehicle_Desc)        !List box control field - type derived from local data
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
MALD:UnitsLoaded       LIKE(MALD:UnitsLoaded)         !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
LOC:Item_Description   LIKE(LOC:Item_Description)     !List box control field - type derived from local data
L_IL:Weight            LIKE(L_IL:Weight)              !List box control field - type derived from local data
L_IL:2nd_Transporter   LIKE(L_IL:2nd_Transporter)     !List box control field - type derived from local data
L_IL:2nd_TransPrice    LIKE(L_IL:2nd_TransPrice)      !List box control field - type derived from local data
L_IL:2nd_Trans_Area    LIKE(L_IL:2nd_Trans_Area)      !List box control field - type derived from local data
L_IL:Total_Charge      LIKE(L_IL:Total_Charge)        !List box control field - type derived from local data
L_IL:Weight_Vol        LIKE(L_IL:Weight_Vol)          !List box control field - type derived from local data
DEL:DID                LIKE(DEL:DID)                  !List box control field - type derived from field
MALD:DIID              LIKE(MALD:DIID)                !List box control field - type derived from field
MALD:MLDID             LIKE(MALD:MLDID)               !List box control field - type derived from field
MALD:MLID              LIKE(MALD:MLID)                !List box control field - type derived from field
DELI:Units             LIKE(DELI:Units)               !Browse hot field - type derived from field
DELI:Type              LIKE(DELI:Type)                !Browse hot field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !Browse hot field - type derived from field
DELI:Weight            LIKE(DELI:Weight)              !Browse hot field - type derived from field
PACK:Packaging         LIKE(PACK:Packaging)           !Browse hot field - type derived from field
TRU:Registration       LIKE(TRU:Registration)         !Browse hot field - type derived from field
TRU:Capacity           LIKE(TRU:Capacity)             !Browse hot field - type derived from field
MAL:MLID               LIKE(MAL:MLID)                 !Primary key field - type derived from field
MAL:MID                LIKE(MAL:MID)                  !Browse key field - type derived from field
MAL:TTID               LIKE(MAL:TTID)                 !Browse key field - type derived from field
TRU:TTID               LIKE(TRU:TTID)                 !Related join file key field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Related join file key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Related join file key field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(_InvoiceTransporter)
                       PROJECT(INT:TIN)
                       PROJECT(INT:Cost)
                       PROJECT(INT:VAT)
                       PROJECT(INT:VATRate)
                       PROJECT(INT:DINo)
                       PROJECT(INT:Leg)
                       PROJECT(INT:Broking)
                       PROJECT(INT:Manifest)
                       PROJECT(INT:ExtraInv)
                       PROJECT(INT:Rate)
                       PROJECT(INT:CreatedDate)
                       PROJECT(INT:CreatedTime)
                       PROJECT(INT:MID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
INT:TIN                LIKE(INT:TIN)                  !List box control field - type derived from field
INT:Cost               LIKE(INT:Cost)                 !List box control field - type derived from field
INT:VAT                LIKE(INT:VAT)                  !List box control field - type derived from field
L_TIG:Total            LIKE(L_TIG:Total)              !List box control field - type derived from local data
INT:VATRate            LIKE(INT:VATRate)              !List box control field - type derived from field
INT:DINo               LIKE(INT:DINo)                 !List box control field - type derived from field
INT:Leg                LIKE(INT:Leg)                  !List box control field - type derived from field
INT:Broking            LIKE(INT:Broking)              !List box control field - type derived from field
INT:Broking_Icon       LONG                           !Entry's icon ID
INT:Manifest           LIKE(INT:Manifest)             !List box control field - type derived from field
INT:Manifest_Icon      LONG                           !Entry's icon ID
INT:ExtraInv           LIKE(INT:ExtraInv)             !List box control field - type derived from field
INT:ExtraInv_Icon      LONG                           !Entry's icon ID
INT:Rate               LIKE(INT:Rate)                 !List box control field - type derived from field
INT:CreatedDate        LIKE(INT:CreatedDate)          !List box control field - type derived from field
INT:CreatedTime        LIKE(INT:CreatedTime)          !List box control field - type derived from field
INT:MID                LIKE(INT:MID)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW3::View:Browse    VIEW(TransporterPaymentsAllocations)
                       PROJECT(TRAPA:AllocationNo)
                       PROJECT(TRAPA:AllocationDate)
                       PROJECT(TRAPA:AllocationTime)
                       PROJECT(TRAPA:Amount)
                       PROJECT(TRAPA:Comment)
                       PROJECT(TRAPA:TRPAID)
                       PROJECT(TRAPA:MID)
                       PROJECT(TRAPA:TPID)
                       JOIN(TRAP:PKey_TPID,TRAPA:TPID)
                         PROJECT(TRAP:TPID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
TRAP:TPID              LIKE(TRAP:TPID)                !List box control field - type derived from field
TRAPA:AllocationNo     LIKE(TRAPA:AllocationNo)       !List box control field - type derived from field
TRAPA:AllocationDate   LIKE(TRAPA:AllocationDate)     !List box control field - type derived from field
TRAPA:AllocationTime   LIKE(TRAPA:AllocationTime)     !List box control field - type derived from field
TRAPA:Amount           LIKE(TRAPA:Amount)             !List box control field - type derived from field
TRAPA:Comment          LIKE(TRAPA:Comment)            !List box control field - type derived from field
TRAPA:TRPAID           LIKE(TRAPA:TRPAID)             !Primary key field - type derived from field
TRAPA:MID              LIKE(TRAPA:MID)                !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::MAN:Record  LIKE(MAN:RECORD),THREAD
QuickWindow          WINDOW('Form Manifest'),AT(,,502,319),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER,GRAY,IMM, |
  MAX,MDI,HLP('UpdateManifest'),SYSTEM
                       SHEET,AT(4,4,495,294),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           GROUP,AT(239,82,251,10),USE(?Group_LoadedWeight)
                             PROMPT('of'),AT(239,82,250),USE(?PROMPT_TotWeight),RIGHT
                           END
                           GROUP,AT(9,20,485,56),USE(?Group_All_Top)
                             GROUP,AT(9,22,483,11),USE(?Group_Trans)
                               PROMPT('Transporter:'),AT(9,22),USE(?TransporterName:Prompt),TRN
                               BUTTON('...'),AT(50,22,12,10),USE(?CallLookup_Transporter)
                               ENTRY(@s35),AT(66,22,100,10),USE(L_SG:TransporterName),MSG('Transporters Name'),REQ,TIP('Transporters Name')
                               PROMPT('Composition:'),AT(275,22),USE(?CompositionName:Prompt),TRN
                               BUTTON('...'),AT(319,22,12,10),USE(?CallLookup_Composition)
                             END
                             ENTRY(@s35),AT(335,22,145,10),USE(L_SG:CompositionName),TIP('Vehicle Composition<0DH,0AH>' & |
  'Horse & Trailer(s)')
                             BUTTON('|'),AT(170,22,6,10),USE(?Button_ChangeTransporter),TIP('Change the Transporter')
                             BUTTON('|'),AT(485,22,6,10),USE(?Button_ChangeComposition),TIP('Change the Composition')
                             PROMPT('Journey:'),AT(9,37),USE(?Journey:Prompt),TRN
                             BUTTON('...'),AT(50,37,12,10),USE(?CallLookup_Journey)
                             ENTRY(@s70),AT(66,37,173,10),USE(L_SG:Journey),MSG('Description'),REQ,TIP('Description')
                             PROMPT('Rate:'),AT(9,52),USE(?MAN:Rate:Prompt),TRN
                             ENTRY(@n-13.4),AT(66,52,57,10),USE(MAN:Rate),RIGHT(1),COLOR(00E9E9E9h),MSG('Rate per Kg'), |
  TIP('Rate per Kg')
                             GROUP,AT(275,66,206,10),USE(?Group1),DISABLE
                               PROMPT('Previous Rates:'),AT(275,66),USE(?DELL:Cost:Prompt:2),TRN
                               LIST,AT(335,66,145,10),USE(L_SV:Prev_Cost),VSCROLL,COLOR(00E9E9E9h),DROP(15,200),FORMAT('100L(2)|M~' & |
  'Transporter Name~@s50@28R(2)|M~Rate~L(2)@n-11.2@48R(1)|M~Cost~L(2)@n-13.2@40D(2)|M~D' & |
  'epart Date~L(2)@d6@'),FROM(LOC:PreviousCosts),TIP('Only Transferred Manifests are us' & |
  'ed to generate this list')
                             END
                             PROMPT('Cost:'),AT(9,66),USE(?MAN:Cost:Prompt),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                             ENTRY(@n-14.2),AT(66,66,57,10),USE(MAN:Cost),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI),RIGHT(1), |
  TIP('Excluding VAT')
                             BUTTON('Specify Rate'),AT(130,52,,10),USE(?Button_Adjust),TIP('Specify a cost - this wi' & |
  'll then adjust the rate to match')
                             BUTTON('Check Rate'),AT(186,52,53,10),USE(?Button_Check_Rate),TIP('Check the rate again' & |
  'st the transporter rates')
                             CHECK(' Broking'),AT(436,52),USE(MAN:Broking),LEFT,MSG('Broking Manifest'),TIP('Broking Manifest'), |
  TRN
                             PROMPT('Driver:'),AT(275,39),USE(?Driver:Prompt),TRN
                             BUTTON('...'),AT(319,38,12,10),USE(?CallLookup_Driver)
                             ENTRY(@s35),AT(335,38,145,10),USE(L_SG:Driver),MSG('Driver'),READONLY,SKIP,TIP('Driver')
                             STRING(@n_10),AT(334,51),USE(L_SG:VCID),RIGHT(1),HIDE
                             PROMPT('VCID - hidden:'),AT(278,51),USE(?L_SG:VCID:Prompt),HIDE
                           END
                           SHEET,AT(9,80,485,216),USE(?Sheet2)
                             TAB('Manifest Items'),USE(?Tab_Items)
                               GROUP,AT(103,278,130,16),USE(?Group_Bottom_DIs)
                                 PROMPT('No. DI''s:'),AT(155,281),USE(?No_DIs:Prompt),TRN
                                 STRING(@n5),AT(187,281),USE(L_SG:No_DIs),RIGHT(1),TRN
                               END
                               LIST,AT(13,100,476,174),USE(?Browse:4),HVSCROLL,FORMAT('66L(2)|FM~Vehicle Description~L' & |
  '(1)@s100@38R(2)|FM~DI No.~L(1)@n_10@20R(2)|M~Item (DI)~L(1)@n6@32R(2)|M~Units Loaded' & |
  '~L(1)@n6@60L(2)|M~Client~L(1)@s35@50L(2)|M~Commodity~L(1)@s35@62L(2)|M~Pack. / Conta' & |
  'iner~L(1)@s200@44R(2)|M~Weight Loaded Units~L(1)@n-11.1@70L(2)|M~2nd Transporter~L(1' & |
  ')@s35@47R(1)|M~2nd Trans. Cost~L(1)@n-13.2@70L(1)|M~2nd Trans. Suburb~@s50@46R(2)|M~' & |
  'Total Charge~L(1)@n-15.2@44R(2)|M~Vol. Weight (not done)~L(1)@n-11.2@30R(2)|M~DID~L(' & |
  '1)@n_10@30R(2)|M~DIID~L(1)@n_10@30R(2)|M~MLDID~L(1)@n_10@30R(2)|M~MLID~L(1)@n_10@'),FROM(Queue:Browse:4), |
  IMM,MSG('Browsing the ManifestLoad file')
                               BUTTON('C&apture Items'),AT(13,278,,14),USE(?Button_Save),LEFT,ICON('SAVE.ICO'),FLAT,TIP('Press to s' & |
  'tart capturing Delivery Items')
                               BUTTON('Add &Load'),AT(335,278,,14),USE(?Insert_AddLoad),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                               BUTTON('&Change Loaded'),AT(404,278,,14),USE(?Change_Loaded),LEFT,ICON('WACHANGE.ICO'),FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                               BUTTON('Change DI'),AT(110,281,22,10),USE(?Button_ChangeDI),HIDE
                             END
                             TAB('Prints && Additionals'),USE(?Tab_Extra)
                               GROUP,AT(14,95,346,143),USE(?Group_Extra_Top_Left)
                                 BUTTON('Print Delivery Notes'),AT(69,99,75,14),USE(?Button_Print_Delivery_Notes)
                                 BUTTON('Print Invoice (s)'),AT(149,99,75,14),USE(?Button_Print_Invoice)
                                 BUTTON('Print Manifest'),AT(229,99,75,14),USE(?Button_Print_Manifest)
                                 PROMPT('Created Date:'),AT(17,149),USE(?MAN:CreatedDate:Prompt),TRN
                                 ENTRY(@t7),AT(73,183,57,10),USE(MAN:DepartTime),RIGHT(1),MSG('Departed Time'),TIP('Departed Time')
                                 ENTRY(@d6),AT(73,169,57,10),USE(MAN:DepartDate),RIGHT(1),MSG('Depated Date'),TIP('Depated Date')
                                 BUTTON('Change'),AT(137,149,,10),USE(?Button_ChangeManDate),TIP('Change the Manifest date')
                                 ENTRY(@n-7.2),AT(73,127,57,10),USE(MAN:VATRate),RIGHT(1),MSG('VAT rate'),TIP('VAT rate')
                                 ENTRY(@d6),AT(73,149,59,10),USE(MAN:CreatedDate),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Created Date'), |
  READONLY,SKIP,TIP('Created Date')
                                 PROMPT('VAT Rate:'),AT(17,127),USE(?MAN:VATRate:Prompt),TRN
                                 PROMPT('Depart Date:'),AT(17,169),USE(?MAN:DepartDate:Prompt),TRN
                                 PROMPT('Depart Time:'),AT(17,183),USE(?MAN:DepartTime:Prompt),TRN
                                 PROMPT('ETA Date:'),AT(17,201),USE(?MAN:ETADate:Prompt),TRN
                                 ENTRY(@d6),AT(73,202,57,10),USE(MAN:ETADate),RIGHT(1),MSG('Estimated arrival date'),TIP('Estimated ' & |
  'arrival date')
                                 PROMPT('ETA Time:'),AT(17,215),USE(?MAN:ETATime:Prompt),TRN
                                 ENTRY(@t7),AT(73,216,57,10),USE(MAN:ETATime),RIGHT(1),MSG('Estimated arrival time'),TIP('Estimated ' & |
  'arrival time')
                               END
                             END
                             TAB('Trucks && ..'),USE(?TAB_Truck)
                               GROUP('Group placement'),AT(9,95,482,84),USE(?GROUP_Place_Trucks),TRN
                                 BUTTON('Change Vehicles'),AT(149,99,75,14),USE(?Button_Change_Vehicles)
                                 BUTTON('View Composition'),AT(69,99,75,14),USE(?Button_View_Composition)
                                 ENTRY(@s200),AT(69,118,289,10),USE(L_SG:MakeModels),COLOR(00E9E9E9h),FLAT,MSG('Make & Model'), |
  READONLY,SKIP,TIP('Make & Model')
                                 ENTRY(@s120),AT(69,130,289,10),USE(L_SG:Registrations),COLOR(00E9E9E9h),FLAT,READONLY,SKIP
                                 PROMPT('Licensing:'),AT(13,142),USE(?Licensing:Prompt),TRN
                                 ENTRY(@s200),AT(69,142,289,10),USE(L_SG:Licensing),COLOR(00E9E9E9h),FLAT,MSG('Licensing details'), |
  READONLY,SKIP,TIP('Licensing details')
                                 ENTRY(@n10.0),AT(69,156,57,10),USE(L_SG:VC_Capacity),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Total capa' & |
  'city not to exceed this amount'),READONLY,SKIP,TIP('Total capacity not to exceed thi' & |
  's amount<0DH,0AH>Composition Total Capacity')
                                 STRING('Weight exceeds Veh. Comp. max weight'),AT(69,170,289,10),USE(?String_Max_Weight),FONT(, |
  ,COLOR:Red,FONT:bold,CHARSET:ANSI),HIDE
                                 PROMPT('Makes && Models:'),AT(13,118),USE(?MakeModel:Prompt),TRN
                                 PROMPT('Registrations:'),AT(13,130),USE(?Registration:Prompt),TRN
                                 PROMPT('T/T Capacity:'),AT(246,156),USE(?Capacity:Prompt),TRN
                                 ENTRY(@n-8.0),AT(302,156,55,10),USE(L_SG:Capacity),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('In Kgs'), |
  READONLY,SKIP,TIP('Truck / Trailer total Capacity in Kgs')
                                 PROMPT('No. Vehicles:'),AT(246,106),USE(?No_Vehicles:Prompt),TRN
                                 ENTRY(@n-5),AT(302,106,55,10),USE(L_SG:No_Vehicles),RIGHT(1),COLOR(00E9E9E9h),FLAT,MSG('Number of ' & |
  'vehicles in combination'),READONLY,SKIP,TIP('Number of vehicles in combination')
                                 PROMPT('Capacity:'),AT(13,156),USE(?VC_Capacity:Prompt),TRN
                               END
                               GROUP,AT(9,183,482,106),USE(?GROUP_Trucks_Change),HIDE
                                 PANEL,AT(11,187,350,104),USE(?PANEL_Truck_Change),BEVEL(1),FILL(00E1E4FFh)
                                 PROMPT('New Composition:'),AT(17,191),USE(?L_TG:CompositionName:Prompt),TRN
                                 ENTRY(@s35),AT(81,192,148,10),USE(L_TG:CompositionName),REQ
                                 PROMPT('Truck:'),AT(17,210),USE(?L_TG:FreightLiner:Prompt),TRN
                                 BUTTON('...'),AT(65,209,12,11),USE(?CallLookup_FrightLiner),HIDE
                                 ENTRY(@s30),AT(81,210,126,10),USE(L_TG:FreightLiner),DISABLE
                                 BUTTON('X'),AT(213,209,9,11),USE(?BUTTON_ClearFreightLiner),HIDE
                                 BUTTON('...'),AT(65,224,12,11),USE(?CallLookup_Trailer)
                                 PROMPT('Trailer:'),AT(17,224),USE(?L_TG:Trailer:Prompt),TRN
                                 ENTRY(@s30),AT(81,224,126,10),USE(L_TG:Trailer)
                                 BUTTON('X'),AT(213,224,9,11),USE(?BUTTON_ClearTrailer)
                                 PROMPT('Super Link:'),AT(17,238),USE(?L_TG:SuperLink:Prompt),TRN
                                 BUTTON('...'),AT(65,237,12,11),USE(?CallLookup_SuperLink)
                                 ENTRY(@s30),AT(81,238,126,10),USE(L_TG:SuperLink)
                                 BUTTON('X'),AT(213,237,9,11),USE(?BUTTON_ClearSuperLink)
                                 BUTTON('Apply Changes'),AT(81,274,65),USE(?BUTTON_Apply_VCO_Changes),FONT(,,,FONT:regular)
                                 BUTTON('Cancel Changes'),AT(149,274,65,14),USE(?BUTTON_Cancel_VCO_Changes),FONT(,,,FONT:regular)
                                 PROMPT(''),AT(229,210,125),USE(?PROMPT_Freightliner)
                                 PROMPT(''),AT(229,225,125),USE(?PROMPT_Trailer)
                                 PROMPT(''),AT(229,239,125),USE(?PROMPT_SuperLink)
                                 LINE,AT(13,254,199,0),USE(?LINE2),COLOR(COLOR:Black)
                                 PROMPT('New Capacity:'),AT(17,258),USE(?L_TG:Capacity:Prompt),TRN
                                 ENTRY(@n-11.0),AT(147,257,60,10),USE(L_TG:Capacity),RIGHT(1),COLOR(00F5F5F5h),READONLY
                               END
                             END
                           END
                         END
                         TAB('&2) Manifest Invoice / Payments'),USE(?Tab5)
                           GROUP,AT(9,20,487,220),USE(?Group_Inv_Pay)
                             PROMPT('Transporter Invoices'),AT(9,22),USE(?Prompt24),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                             LIST,AT(9,34,482,126),USE(?List),VSCROLL,FORMAT('36R(1)|M~TIN~C(0)@n_10@46R(1)|M~Cost~C' & |
  '(0)@n-14.2@46R(1)|M~VAT~C(0)@n-14.2@46R(1)|M~Total~C(0)@n-17.2@[38R(1)|M~Rate~C(0)@n' & |
  '-7.2@](27)|M~VAT~[36R(1)|M~DI No.~C(0)@n_10b@20R(1)|M~Leg~C(0)@n5@]|M~Extra Leg~30R(' & |
  '1)|MI~Broking~C(0)@p p@32R(1)|MI~Manifest~C(0)@p p@32R(1)|MI~Extra Inv.~C(0)@p p@42R' & |
  '(1)|M~Rate~C(0)@n-13.4@[38R(1)|M~Date~C(0)@d5b@38R(1)|M~Created Time~L(2)@t7@](-13)|' & |
  'M~Created~'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                             LINE,AT(11,164,481,-1),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                             PROMPT('Transporter Payment Allocations'),AT(9,170),USE(?Prompt24:2),FONT(,,,FONT:bold,CHARSET:ANSI), |
  TRN
                             LIST,AT(9,183,482,110),USE(?List:2),HVSCROLL,FORMAT('40R(1)|M~TPID~L(2)@n_10@38R(1)|M~A' & |
  'llocation No~L(2)@n_5@[40R(1)|M~Date~L(2)@d5b@36R(1)|M~Time~L(2)@t7@]|M~Allocation~5' & |
  '6R(1)|M~Amount~L(2)@n-14.2@100L(1)|M~Comment~L(2)@s255@'),FROM(Queue:Browse:1),IMM,MSG('Browsing Records')
                           END
                         END
                       END
                       BUTTON,AT(4,301,,14),USE(?Button_Prev),LEFT,ICON('PRIORPG.ICO'),FLAT,SKIP
                       BUTTON('Transferred'),AT(28,301,,14),USE(?Button_Next),LEFT,ICON('NEXTPG.ICO'),FLAT,SKIP
                       PROMPT('Load Complete'),AT(100,305,109,10),USE(?Prompt_Change),FONT(,,COLOR:Red,FONT:bold, |
  CHARSET:ANSI)
                       BUTTON('&Print Details'),AT(338,301,49,14),USE(?PrintDetails),FLAT
                       BUTTON('&OK'),AT(393,301,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(447,301,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(155,2,17,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       GROUP,AT(251,6,245,10),USE(?Group_AboveTabTop),TRN
                         PROMPT('BID :'),AT(383,6),USE(?MAN:BID:Prompt),TRN
                         STRING(@n_3),AT(403,6,,10),USE(MAN:BID),RIGHT(1),TRN
                         PROMPT('MID:'),AT(435,6),USE(?MAN:MID:Prompt),TRN
                         STRING(@n_7),AT(454,6),USE(MAN:MID),RIGHT(1),TRN
                         STRING(@n_7),AT(333,6),USE(MAN:VCID),RIGHT(1)
                         PROMPT('VCID:'),AT(313,6),USE(?MAN:VCID:Prompt)
                       END
                     END

BRW4::LastSortOrder       BYTE
BRW4::SortHeader  CLASS(SortHeaderClassType) !Declare SortHeader Class
QueueResorted          PROCEDURE(STRING pString),VIRTUAL
                  END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW_ManLoads         CLASS(BrowseClass)                    ! Browse using ?Browse:4
Q                      &Queue:Browse:4                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE NewOrder,BYTE Force),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW2                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                      ! Default Locator
BRW3                 CLASS(BrowseClass)                    ! Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
                     END

BRW3::Sort0:Locator  StepLocatorClass                      ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! ============ **********   READ ME   ********** ============ 
!
!  3 March 2014
!  The logic and construction here are bad now.  Too many modifications over too long a period
!
!  As of now a manifest can have a Vehicle composition changed on it, and this can create a new VCO record
!  Which it will do upon saving the form, if the VCO has been changed and applied.
! 
!  Older code checks any change in the VCO and asks the user if they are happy depending on if the new VCO
!  can fit all the cargo on it.
!  This code uses L_SG:CompositionName and L_SG:VCID, and only if the user accepts the change does MAN:VCID get 
!  updated.
!
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
View_Chg            VIEW(ManifestAlias)
    PROJECT(A_MAN:MID, A_MAN:TID, A_MAN:Cost, A_MAN:JID, A_MAN:DepartDate, A_MAN:Rate, A_MAN:State)
       JOIN(A_TRA:PKey_TID, A_MAN:TID)
       PROJECT(A_TRA:TransporterName)
    .  .


Chg_View            ViewManager
    MAP
Transporter_Rates                PROCEDURE(BYTE p:NotQuite=0),STRING,PROC

Apply_Vehicle_Composition        PROCEDURE(LONG p_Opt)

Set_TruckTrailer                 PROCEDURE(<STRING p_CompositionName>, ULONG p_VCID=0)    ! Expects L_TG TTIDs set

Change_Veh_Comp                  PROCEDURE(ULONG p_New_VCID, ULONG p_Old_VCID, BYTE p:Override=0),LONG
Move_VehComp_Cargo               PROCEDURE(ULONG p_New_VCID, ULONG p_Old_VCID),LONG

Veh_Composition_New              PROCEDURE(ULONG p_NewVCOSelected=0)            ! Uses Composition record to populate TTIDs
    .
State_Class         CLASS,TYPE

c_TID           LIKE(TRA:TID)

New_TID         PROCEDURE(ULONG p:TID),LONG

    .


SC      State_Class

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Set_Composition_New                 ROUTINE              ! from Manifest TTID info,   sets L_SG group data
   ! Set the info about the composition (see additionals tab) - no bus logic here
   LOC:Start  = CLOCK()

   ! Setup the Truck change fields
   CLEAR(LOC:Truck_Group)
   L_TG:FreightLinerTTID   = MAN:TTID_FreightLiner
   L_TG:TrailerTTID        = MAN:TTID_Trailer
   L_TG:SuperLinkTTID      = MAN:TTID_SuperLink             
   Set_TruckTrailer(L_SG:CompositionName, MAN:VCID)

   
   ! Get_TruckTrailer_Info    4 - LicenseInfo, 5 - Make & Model, 6 - Registration, 7 - Capacity
   L_SG:MakeModels     = ''
   Add_to_List(Get_TruckTrailer_Info(MAN:TTID_FreightLiner, 5), L_SG:MakeModels, ', ')
   Add_to_List(Get_TruckTrailer_Info(MAN:TTID_Trailer, 5), L_SG:MakeModels, ', ')
   Add_to_List(Get_TruckTrailer_Info(MAN:TTID_SuperLink, 5), L_SG:MakeModels, ', ')
   
   L_SG:Registrations  = ''
   Add_to_List(Get_TruckTrailer_Info(MAN:TTID_FreightLiner, 6), L_SG:Registrations, ', ') 
   Add_to_List(Get_TruckTrailer_Info(MAN:TTID_Trailer, 6), L_SG:Registrations, ', ') 
   Add_to_List(Get_TruckTrailer_Info(MAN:TTID_SuperLink, 6), L_SG:Registrations, ', ') 
      
   !L_SG:Licensing        = Get_VehComp_Info(MAN:VCID, 11)
   L_SG:Licensing      = ''
   Add_to_List(Get_TruckTrailer_Info(MAN:TTID_FreightLiner, 4), L_SG:Licensing, ', ')
   Add_to_List(Get_TruckTrailer_Info(MAN:TTID_Trailer, 4), L_SG:Licensing, ', ')
   Add_to_List(Get_TruckTrailer_Info(MAN:TTID_SuperLink, 4), L_SG:Licensing, ', ')
         
   ! Calc the total Capacity     
   ! TODO In theory a composition can be less than the sum - asking Yvonne how to handle
   L_SG:Capacity       = Get_TruckTrailer_Info(MAN:TTID_FreightLiner, 7) + Get_TruckTrailer_Info(MAN:TTID_Trailer, 7) + | 
                              + Get_TruckTrailer_Info(MAN:TTID_SuperLink, 7)
   IF MAN:VCID = 0         
      L_SG:VC_Capacity  = L_SG:Capacity
   .
   
   L_SG:No_Vehicles    = 0
   IF MAN:TTID_FreightLiner <> 0 THEN L_SG:No_Vehicles    = 1 .
   IF MAN:TTID_Trailer <> 0      THEN L_SG:No_Vehicles   += 1 .
   IF MAN:TTID_SuperLink <> 0    THEN L_SG:No_Vehicles   += 1 .

   !DO Veh_Licensing_Reminders     TODO - removing for now!  26/02/14

   DRI:DRID            = MAN:DRID
   IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
      L_SG:Driver      = CLIP(DRI:FirstName) & ' ' & DRI:Surname
   .
     
   DISPLAY
   IF CLOCK() - LOC:Start > 0
      db.debugout('[Update_Manifest - Set_Composition_New]  time taken: ' & CLOCK() - LOC:Start)
   .
   EXIT
!Set_TruckTrailer                          ******* moved to proc
!Set_TruckTrailer                          ROUTINE                 ! should come after screen group loaded
!   CLEAR(LOC:Truck_Group)
!   
!   L_TG:FreightLinerTTID   = MAN:TTID_FreightLiner
!   L_TG:TrailerTTID        = MAN:TTID_SuperLink
!   L_TG:SuperLinkTTID      = MAN:TTID_Trailer
!   
!   L_TG:CompositionName    = L_SG:CompositionName           ! Assumes screen group loaded already
!   L_TG:VCID               = MAN:VCID
!   
!   
!   A_TRU:TTID              = L_TG:FreightLinerTTID
!   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = Level:Benign
!      L_TG:FreightLiner    = A_TRU:Registration
!      
!      ?PROMPT_Freightliner{PROP:Text} = A_TRU:Capacity & 'kgs, ' & Get_TruckTrailer_Info(L_TG:FreightLinerTTID, 5)
!   .
!   
!   A_TRU:TTID              = MAN:TTID_Trailer
!   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = Level:Benign
!      L_TG:Trailer         = A_TRU:Registration
!      ?PROMPT_SuperLink{PROP:Text} = A_TRU:Capacity & 'kgs, ' & Get_TruckTrailer_Info(L_TG:SuperLinkTTID, 5)
!   .
!      
!   A_TRU:TTID              = MAN:TTID_SuperLink
!   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = Level:Benign
!      L_TG:SuperLink       = A_TRU:Registration
!      ?PROMPT_Trailer{PROP:Text} = A_TRU:Capacity & 'kgs, ' & Get_TruckTrailer_Info(L_TG:TrailerTTID, 5)
!   .
!   
!   EXIT
!
!   ! (p:TTID, p:Type, p:Effective_Date, p:LicenseExpiryDate)
!   ! (ULONG, BYTE=0, LONG=0, <*LONG>),STRING
!   !
!   ! p:Type    0 - Expiry - Message if less than Settings expiry, otherwise nothing
!   !           1 - Expiry - CSV info if less than Settings expiry, otherwise nothing
!   !           2 - Expiry - Message
!   !           3 - Expiry - CSV info
!   !           4 - LicenseInfo
!   !           5 - Make & Model
!   !           6 - Registration
!   !           7 - Capacity
!   !
!   ! p:Effective_Date
!   !           0 - Today
!   !           x - Passed date
!   !
!   ! [Usage]
!Veh_Composition_Changed    ROUTINE             *********** not used
!    TODO - done the above SETTING of vehichle now need to check on changed
!   
!    LOC:Start  = CLOCK()
!
!    VCO:VCID                = MAN:VCID
!    IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
!       IF CLIP(L_SG:Original_VehComp_Tip) = ''
!          L_SG:Original_VehComp_Tip         = L_SG:CompositionName{PROP:Tip}
!       .
!       L_SG:CompositionName{PROP:Tip}       = CLIP(L_SG:Original_VehComp_Tip) & '<13,10><13,10>' & CLIP(L_SG:CompositionName)
!
!       L_SG:VC_Capacity     = VCO:Capacity
!
!       ! 16 Feb 14 - set new Manifest based vehicle info, only when new one selected by the user
!         MAN:TTID_FreightLiner   = VCO:TTID0
!         MAN:TTID_Trailer        = VCO:TTID1
!         MAN:TTID_SuperLink      = VCO:TTID2   
!         
!       TRU:TTID             = VCO:TTID0
!       IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
!          IF MAN:DRID = 0
!             MAN:DRID   = TRU:DRID
!          .
!
!          DRI:DRID          = TRU:DRID
!          IF Access:Drivers.TryFetch(DRI:PKey_DRID) ~= LEVEL:Benign
!             CLEAR(DRI:Record)
!          .
!
!          IF MAN:DRID ~= TRU:DRID AND MAN:State < 2             ! Loading|#0|Loaded|#1|On Route|#2|Transferred|
!             CASE MESSAGE('The Manifest has a different Driver to the Truck driver specified.|Manifest Driver: ' & CLIP(L_SG:Driver) & |
!                          '|Truck Driver: ' & CLIP(CLIP(DRI:FirstName) & ' ' & DRI:Surname) & |
!                          ' ||Would you like to change the Driver to the Truck driver now?', 'Driver', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
!             OF BUTTON:Yes
!                MAN:DRID   = TRU:DRID
!          .  .
!
!!          L_SG:Driver       = TRU:Driver
!!          L_SG:Assistant    = TRU:Assistant
!
!          L_SG:MakeModels       = Get_VehComp_Info(MAN:VCID, 3)
!          L_SG:Registrations    = Get_VehComp_Info(MAN:VCID, 2)
!          L_SG:Licensing        = Get_VehComp_Info(MAN:VCID, 11)
!
!          L_SG:Capacity         = Get_VehComp_Info(MAN:VCID, 1)
!          L_SG:No_Vehicles      = Get_VehComp_Info(MAN:VCID, 7)
!
!          DO Veh_Licensing_Reminders
!
!          DRI:DRID              = MAN:DRID
!          IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
!             L_SG:Driver        = CLIP(DRI:FirstName) & ' ' & DRI:Surname
!          .
!          DISPLAY
!       .
!    ELSE
!       ENABLE(?Button_Save)
!       DISABLE(?Insert:5)
!       DISABLE(?Change:5)
!       !DISABLE(?Delete:5)
!    .
!    IF CLOCK() - LOC:Start > 0
!       db.debugout('[Update_Manifest - Veh_Composition]  time taken: ' & CLOCK() - LOC:Start)
!    .
!    EXIT
Veh_Licensing_Reminders   ROUTINE
    DATA
Idx             LONG

R:TTID          LIKE(TRU:TTID)
R:Msg           STRING(255)
R:Reminders     BYTE
R:User          STRING(255)
R:Group         STRING(255)
R:Value         STRING(20)
R:License_Exp   LONG

R:Rem_Zero      BYTE

Rem_Group       GROUP(),PRE(R)
RemDate             DATE
RemTime             TIME
RemUID              LIKE(USE:UID)
RemUGID             LIKE(USE:UID)
RemNotes            LIKE(REM:Notes)
RemRef              LIKE(REM:Reference)
                .

    CODE
    ! Check and Setup Reminders
    IF UPPER(CLIP(Get_Setting('Vehicle License Expiry - Activate Reminder', TRUE, 'Yes', '@s3', 'Yes/No'))) ~= 'NO'
       R:Reminders  = TRUE
    .
    IF R:Reminders = TRUE
       IF UPPER(CLIP(Get_Setting('Vehicle License Expiry - Remind Blank License Dates', TRUE, 'No', '@s3', 'Yes/No'))) ~= 'NO'
          R:Rem_Zero    = TRUE
       .

       R:RemDate        = TODAY()
       R:RemTime        = DEFORMAT('11:00:00',@t4)

       R:User           = Get_Setting('Vehicle License Expiry - User', TRUE,,, 'The Users Login for which reminders will be added.')
       IF CLIP(R:User) ~= ''
          ! (p:Info, p:Value, p:UID, p:Login)
          ! (BYTE, *STRING, <ULONG>, <STRING>),LONG
          IF Get_User_Info(7, R:Value,, R:User) >= 0
             R:RemUID   = DEFORMAT(R:Value)
          ELSE
             MESSAGE('The User specified in the Settings for Vehicle Expiry reminders is not valid.||User: ' & R:User, 'Vehicle Licensing', ICON:Exclamation)
       .  .

       R:Group          = Get_Setting('Vehicle License Expiry - User Group', TRUE,'Admin','', 'The User Group for which reminders will be added.')
       IF CLIP(R:Group) ~= ''
          IF Get_UserGroup_Info(2, R:Value,, R:Group) >= 0
             R:RemUGID  = DEFORMAT(R:Value)
          ELSE
             MESSAGE('The User Group specified in the Settings for Vehicle Expiry reminders is not valid.||User Group: ' & R:Group, 'Vehicle Licensing', ICON:Exclamation)
       .  .

       IF R:RemUID = 0 AND R:RemUGID = 0
          R:Reminders   = FALSE
          MESSAGE('If License Reminders are required they will not be created.||There is no suitable User or User Group to assign them to.', 'Vehicle Licensing', ICON:Exclamation)
    .  .

    ! Loop through the Trucks and check for Licensing
    Idx         = 0
    LOOP 4 TIMES
       Idx     += 1

       ! (p:VCID, p:Option, p:Truck, p:Additional)
       ! (ULONG, BYTE, BYTE=0, BYTE=0),STRING
       ! p:Option  1.  Capacity                            of all
       !           6.  Truck / Trailer ID (TTID)           requires p:Truck
       !
       ! p:Truck
       !           - Truck no. in combination, 1 to 4
       !R:TTID   = Get_VehComp_Info(MAN:VCID, 6, Idx - 1)        ! 0 is 1st vehicle
       R:TTID = 0
       EXECUTE Idx
         R:TTID = L_TG:FreightLinerTTID                     
         R:TTID = L_TG:TrailerTTID
         R:TTID = L_TG:SuperLinkTTID       
       .
         
       IF R:TTID ~= 0
          ! Get_TruckTrailer_Info
          ! (ULONG, BYTE=0, LONG=0, <*LONG>),STRING
          ! (p:TTID, p:Type, p:Effective_Date, p:LicenseExpiryDate)
          !
          ! p:Type    0 - Message if less than Settings expiry, otherwise nothing
          !           1 - CSV info if less than Settings expiry, otherwise nothing
          !           2 - Message
          !           3 - CSV info

          R:Msg     = Get_TruckTrailer_Info(R:TTID, 0,, R:License_Exp)
          IF CLIP(R:Msg) ~= ''
             MESSAGE(CLIP(R:Msg), 'Vehicle Licensing', ICON:Exclamation)

             IF R:Reminders = TRUE AND (R:Rem_Zero = TRUE OR R:License_Exp > 0)
                R:RemRef        = 'Vehicle-TTID:' & R:TTID
                R:RemNotes      = R:Msg

                ! (p:RemDate, p:RemTime, p:RemOption, p:RemUID, p:RemUGID, p:Notes, p:Ref, p:Ref_Check, p:RemType, p:ID)
                ! (LONG, LONG, BYTE=0, LONG, LONG, STRING, STRING, BYTE=0, BYTE=0), LONG, PROC
                !   1     2      3      4     5       6      7      8       9
                ! p:Ref_Check       0 - Off
                !                   1 - Check for an Active entry of this ref type, dont add if found
                !                   2 - Check for an entry of this ref type, dont add if found
                ! p:RemType         0 - General, 1 - Client, 2 - Truck/Trailer

                Add_Reminder(R:RemDate, R:RemTime,, R:RemUID, R:RemUGID, R:RemNotes, R:RemRef, 1, 2, R:TTID)
    .  .  .  .
    EXIT
Load_Charges_Q                 ROUTINE
    DATA
L:FoundPos      LONG
L:Idx           ULONG
L:Top           BYTE(1)


    CODE
    LOC:Start  = CLOCK()
    FREE(LOC:PreviousCosts)

    IF MAN:JID ~= 0
       !BIND('A_MAN:JID',A_MAN:JID)             - binds in Bind of Proc!
       !BIND('A_MAN:State',A_MAN:State)

       Chg_View.Init(View_Chg, Relate:ManifestAlias)
       Chg_View.AddSortOrder(A_MAN:FKey_JID)
       Chg_View.AppendOrder('A_MAN:MID,-A_MAN:CreatedDate')
       Chg_View.AddRange(A_MAN:JID, MAN:JID)
       Chg_View.SetFilter('A_MAN:MID ~= ' & MAN:MID)

       ! Loading|Loaded|On Route|Transferred
       Chg_View.Reset()
       LOOP
          IF Chg_View.Next() ~= LEVEL:Benign
             BREAK
          .
          L:Idx              += 1
          IF RECORDS(LOC:PreviousCosts) > 10
             BREAK
          .

!    message('A_MAN:JID: ' & A_MAN:JID & '||A_MAN:MID: ' & A_MAN:MID, 'MAN:MID: ' & MAN:MID)

          ! Dont load duplicates of the same Transporter and Cost
          L_PCQ:TransporterName     = A_TRA:TransporterName
          L_PCQ:Cost                = A_MAN:Cost
          GET(LOC:PreviousCosts, +L_PCQ:TransporterName, +L_PCQ:Cost)
          IF ERRORCODE()
             CLEAR(LOC:PreviousCosts)
             L_PCQ:TransporterName  = A_TRA:TransporterName
             L_PCQ:Rate             = A_MAN:Rate
             L_PCQ:Cost             = A_MAN:Cost
             L_PCQ:DepartDate       = A_MAN:DepartDate
             ADD(LOC:PreviousCosts, +L_PCQ:TransporterName, +L_PCQ:Cost)
       .  .
       Chg_View.Kill()

       db.debugout('[Update_Manifests - Load_Charges_Q]   Manifests: ' & L:Idx)

       SORT(LOC:PreviousCosts, +L_PCQ:TransporterName, -L_PCQ:DepartDate)

       ! Now move all this Manifest Transporter entries to the top
       L:Top    = TRUE
       L:Idx    = 0
       LOOP
          L:Idx += 1
          GET(LOC:PreviousCosts, L:Idx)
          IF ERRORCODE()
             BREAK
          .

          IF L_PCQ:TransporterName ~= L_SG:TransporterName
             L:Top  = FALSE
          ELSIF L:Top = FALSE                       ! We know names are same too
             ! Move this entry to the top
             ADD(LOC:PreviousCosts, 1)
             GET(LOC:PreviousCosts, L:Idx+1)        ! Pushed down one
             DELETE(LOC:PreviousCosts)
             !L:Idx  -= 1                            ! Go back one - no - we have added and deleted
       .  .

       IF RECORDS(LOC:PreviousCosts) > 0
          ?L_SV:Prev_Cost{PROP:Background} = 0C8FFFFH
          ENABLE(?Group1)

          ! Add display Q entry
          GET(LOC:PreviousCosts, 1)
          L_SV:Prev_Cost        = 'Rate ' & CLIP(LEFT(FORMAT(L_PCQ:Rate,@n12.4))) & ' - ' & FORMAT(L_PCQ:DepartDate, @d5) & ' - ' & CLIP(L_PCQ:TransporterName)

          L_PCQ:TransporterName = L_SV:Prev_Cost
          L_PCQ:Display         = TRUE
          ADD(LOC:PreviousCosts)

          DISPLAY(?L_SV:Prev_Cost)
       ELSE
          ?L_SV:Prev_Cost{PROP:Background} = 0E9E9E9H
          DISABLE(?Group1)
    .  .

    IF CLOCK() - LOC:Start > 0
       db.debugout('[Update_Manifest - Load_Charges_Q]  time taken: ' & CLOCK() - LOC:Start)
    .
    EXIT
Load_State                          ROUTINE
    LOC:Start = CLOCK()

    ! Loading|#0|Loaded|#1|On Route|#2|Transferred|#3
    CASE MAN:State
    OF 0                ! Loading|#0|
       ?Prompt_Change{PROP:Text}        = ''

       ?Button_Next{PROP:Text}          = '&Loading'
       ?Button_Next{PROP:Tip}           = 'Status is Loading - set Status to Loaded'



       ! Set specific User permisions - edit DI from Un-loaded Manifest

       ! (p:UID, p:AppSection, p:Procedure, p:Extra)
       ! Returns
       !   1   - Disable
       !   2   - Hide
       !   100 - Allow
       !   101 - Disallow

       LOC:Allow_Change_DI      = 0
       User_Access_#            = Get_User_Access(GLO:UID, 'Manifests', 'Update_Manifest', 'DI-Edit')
       IF User_Access_# = 0 OR User_Access_# = 100
          LOC:Allow_Change_DI   = 1
       .
    OF 1                ! Loaded|#1|
       ?Prompt_Change{PROP:Text}        = 'Load Complete'

       ?Button_Next{PROP:Text}          = '&Loaded'
       ?Button_Next{PROP:Tip}           = 'Status is Loaded - set Status to On Route'
    OF 2                ! On Route|#2|
       ! Nothing more can be done - set view mode
       ?Prompt_Change{PROP:Text}        = 'No Changes'

       ?Button_Next{PROP:Text}          = 'On &Route'
       ?Button_Next{PROP:Tip}           = 'Status is On Route - set Status to Transferred'
    OF 3                ! Transferred|#3
       ?Prompt_Change{PROP:Text}        = 'No Changes'

       ?Button_Next{PROP:Text}          = '&Transferred'
       ?Button_Next{PROP:Tip}           = 'Status is Transferred - this stage is Final'
    .

    DO Load_State_Buttons

    DO Load_State_Options

    IF LOC:Last_State >= 2
       ! When in these states update form is in view mode only - so manual update here
       IF Access:Manifest.Update() = LEVEL:Benign
       .

       ! If new state would allow edits then tell user how to do it
       IF MAN:State < 2
          MESSAGE('This state allows the Manifest to be edited.||To edit the Manifest please click OK and then re-load the Manifest.', 'Manifest State Change', ICON:Exclamation)
          POST(EVENT:CloseWindow)
       .
    ELSIF LOC:Last_State >= 1
       ! Save here as they have now made items editable....
       ! ??? I think any state change should be saved immediately???
       IF Access:Manifest.Update() = LEVEL:Benign
    .  .

    IF CLOCK() - LOC:Start > 0
       db.debugout('[Update_Manifest - Load_State]  time taken: ' & CLOCK() - LOC:Start)
    .

    LOC:Last_State  = MAN:State
    EXIT



Load_State_Buttons                  ROUTINE
    IF MAN:State = 0
       DISABLE(?Button_Prev)
    .
    IF MAN:State = 3
       DISABLE(?Button_Next)
    .

    IF MAN:State < 3
       IF ?Button_Next{PROP:Enabled} = FALSE
          ENABLE(?Button_Next)
    .  .

    IF MAN:State > 0
       IF ?Button_Prev{PROP:Enabled} = FALSE
          ENABLE(?Button_Prev)
    .  .

    IF ?Button_Save{PROP:Enabled} = FALSE AND LOC:Last_State < 2
       IF MAN:State > 0
          DISABLE(?Insert_AddLoad)
          DISABLE(?Change_Loaded)
          !DISABLE(?Delete:5)

   !       BRW2.InsertControl   = 0
   !       BRW2.ChangeControl   = 0
   !       BRW2.DeleteControl   = 0
       ELSE
          ENABLE(?Insert_AddLoad)
          ENABLE(?Change_Loaded)
          !ENABLE(?Delete:5)

   !       BRW2.InsertControl   = ?Insert:5
   !       BRW2.ChangeControl   = ?Change:5
   !       BRW2.DeleteControl   = ?Delete:5
       .
    ELSIF ?Button_Save{PROP:Enabled} = TRUE
       DISABLE(?Insert_AddLoad)
       DISABLE(?Change_Loaded)
       !DISABLE(?Delete:5)
    .
    EXIT



Load_State_Options              ROUTINE
    IF MAN:State > 0
       ?MAN:Cost{PROP:ReadOnly}         = TRUE
       ?MAN:VATRate{PROP:ReadOnly}      = TRUE

       ?MAN:Cost{PROP:Background}       = 0E9E9E9H
       ?MAN:VATRate{PROP:Background}    = 0E9E9E9H
    ELSIF ?MAN:Cost{PROP:ReadOnly} = TRUE
       ?MAN:Cost{PROP:ReadOnly}         = FALSE
       ?MAN:VATRate{PROP:ReadOnly}      = FALSE

       ?MAN:Cost{PROP:Background}       = -1
       ?MAN:VATRate{PROP:Background}    = -1
    .

    EXIT
Rate_Calc                            ROUTINE                        ! Calculates the rate from the weight
       LOC:Start  = CLOCK()

!    MESSAGE('Manifest weight: ' & Get_ManLoad_Info(MAN:MID, 0) / 100, 'MAN:MID: ' & MAN:MID)
    DO Get_Tot_Weight

    IF L_SG:Tot_Weight ~= 0.0
       MAN:Rate     = MAN:Cost / L_SG:Tot_Weight
    .
    DISPLAY(?MAN:Rate)

    IF CLOCK() - LOC:Start > 0
       db.debugout('[Update_Manifest - Rate_Calc]  time taken: ' & CLOCK() - LOC:Start)
    .
    EXIT
Cost_Rate_Calc                   ROUTINE
       LOC:Start  = CLOCK()

    DO Get_Tot_Weight

    MAN:Cost                = MAN:Rate * L_SG:Tot_Weight

    !?Prompt_Cost{PROP:Text} = ''
    DO Rate_Calc
    DISPLAY

    IF CLOCK() - LOC:Start > 0
       db.debugout('[Update_Manifest - Cost_Rate_Calc]  time taken: ' & CLOCK() - LOC:Start)
    .
    EXIT
Get_Tot_Weight                   ROUTINE
DATA
p_PerLoaded    DECIMAL(12.2)

CODE
   LOC:Start  = CLOCK()

   L_SG:Tot_Weight               = Get_ManLoad_Info(MAN:MID, 0)          !/ 100
   p_PerLoaded                   = (L_SG:Tot_Weight / L_SG:VC_Capacity) * 100
   ?PROMPT_TotWeight{PROP:Text}  = 'Loaded Weight: ' & FORMAT(L_SG:Tot_Weight,@n10) & '  ( ' & CLIP(LEFT(FORMAT(p_PerLoaded,@n4) & '% of capacity: ' & LEFT(FORMAT(L_SG:VC_Capacity,@n10)))) & ' )'

   IF L_SG:Tot_Weight > L_SG:VC_Capacity
      IF L_SG:User_Told_VC_Weight_Exceeded = FALSE
         L_SG:User_Told_VC_Weight_Exceeded = TRUE
         MESSAGE('The Manifested weight exceeds the total allowed weight for this Vehicle Composition!||Please remove some items from the vehicle(s).', 'Update Manifest', ICON:Exclamation)
      .

      UNHIDE(?String_Max_Weight)
      ?PROMPT_TotWeight{PROP:FontColor}     = COLOR:Red
      ?PROMPT_TotWeight{PROP:FontStyle}     = FONT:bold
      !?Tot_Weight:Prompt{PROP:FontColor}   = COLOR:Red

      ?String_Max_Weight{PROP:Text}        = 'Weigtht exceeds Veh. Comp. max weight by ' & L_SG:Tot_Weight - L_SG:Capacity
   ELSIF p_PerLoaded > 90.0
      ?PROMPT_TotWeight{PROP:FontColor}     = COLOR:Orange
      ?PROMPT_TotWeight{PROP:FontStyle}     = FONT:bold
         
   ELSE      
      ?PROMPT_TotWeight{PROP:FontColor}     = -1
      ?PROMPT_TotWeight{PROP:FontStyle}     = -1
      !?Tot_Weight:Prompt{PROP:FontColor}   = -1
      HIDE(?String_Max_Weight)
      L_SG:User_Told_VC_Weight_Exceeded    = FALSE
   .

   IF CLOCK() - LOC:Start > 0
      db.debugout('[Update_Manifest - Get_Tot_Weight]  time taken: ' & CLOCK() - LOC:Start)
   .
   EXIT
! ----------------------------------------------------------------------------------------
Print_Delivery_Note             ROUTINE
    BRW_ManLoads.UpdateViewRecord()
    IF DELI:DID ~= 0
       Print_Cont(DELI:DID, 2, 1)
    ELSE
       MESSAGE('No DID available to print.', 'Update_Manifest', ICON:Hand)
    .
    EXIT
Print_Invoice_for_Delivery   ROUTINE
    BRW_ManLoads.UpdateViewRecord()
    IF DELI:DID ~= 0
       INV:DID  = DELI:DID
       IF Access:_Invoice.TryFetch(INV:FKey_DID) = LEVEL:Benign
          IF EVENT() = EVENT:User+1
             Print_Cont(INV:IID, 0)
          ELSE
             Print_Invoices(0, INV:IID)
          .
       ELSE
          MESSAGE('There is no invoice for DI No.: ' & DEL:DINo,'Print Invoice', ICON:Exclamation)
       .
    ELSE
       MESSAGE('No DID available to print.', 'Update_Manifest', ICON:Hand)
    .
    EXIT
! ----------------------------------------------------------------------------------------
Transporter_Rate                ROUTINE                             ! Called on change
    DATA
R:Result        DECIMAL(20,2)

    CODE
!       db.debugout('[]   Transporter_Rate - MAN:TID: ' & MAN:TID & ', MAN:JID: ' & MAN:JID & ', MAN:VCID: ' & MAN:VCID)
    ! Loading|#0|Loaded|#1|On Route|#2|Transferred|#3
    IF MAN:State < 2        ! Not On Route - can change values
       IF MAN:TID ~= 0 AND MAN:JID ~= 0 AND MAN:VCID ~= 0

!  ---- section moved
!          ! If the truck has changed
!          IF L_TRG:TID ~= MAN:TID
!             IF MAN:TID = 0
!                MAN:Broking         = L_TMPG:Broking
!             ELSE
!                IF MAN:Broking ~= L_TMPG:Broking
!                   IF L_TMPG:Broking = TRUE
!                      R:Msg         = 'New Transporter is Broking, set Broking on Manifest now?'
!                   ELSE
!                      R:Msg         = 'New Transporter is NOT Broking, set NO Broking on Manifest now?'
!                   .
!
!                   CASE MESSAGE('The Transporter has been changed.||' & CLIP(R:Msg), 'Manifest Transporter', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
!                   OF BUTTON:Yes
!                      MAN:Broking   = L_TMPG:Broking
!          .  .  .  .

          ! If the truck has changed
          IF L_TRG:TID ~= MAN:TID OR L_TRG:JID ~= MAN:JID OR L_TRG:VCID ~= MAN:VCID
             R:Result     = Transporter_Rates()

      !       db.debugout('[]   Transporter Rate - R:Result: ' & R:Result)

             IF R:Result < 0.0
                ! Problem
             ELSIF R:Result = 0.0
                ! No base charge - uses rate
             ELSE
                MAN:Cost  = R:Result
                DISPLAY(?MAN:Cost)
             .


             IF L_TRG:TID ~= MAN:TID
                IF L_SG:ChargesVAT = FALSE                     ! Check VAT
                   MAN:VATRate  = 0.0
                ELSIF MAN:VATRate = 0.0
                   MAN:VATRate  = Get_Setup_Info(4)
                .
                DISPLAY(?MAN:VATRate)
             .

             LOC:Change_Trans_Rate   :=: MAN:Record
    .  .  .
    EXIT
! ----------------------------------------------------------------------------------------
Transporter_Set                  ROUTINE
    DATA
R:Msg           STRING(200)

    CODE
    CASE SC.New_TID(MAN:TID)        ! Transport changed, check Broking flag
    OF 1
       ! If no Transporter before, set to the new selected Transporter
       MAN:Broking         = L_TMPG:Broking        ! L_TMPG:Broking set when user selects new Transporter
       DISPLAY
    OF 2
       IF MAN:Broking ~= L_TMPG:Broking
          IF L_TMPG:Broking = TRUE
             R:Msg         = 'New Transporter is Broking, set Broking on Manifest now?'
          ELSE
             R:Msg         = 'New Transporter is NOT Broking, set NO Broking on Manifest now?'
          .

          CASE MESSAGE('The Transporter has been changed.||' & CLIP(R:Msg), 'Manifest Transporter', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
          OF BUTTON:Yes
             MAN:Broking   = L_TMPG:Broking
             DISPLAY
    .  .  .
    

    DO Transporter_Rate

    DO Load_Charges_Q
    EXIT
Driver_Check                     ROUTINE
    ! Load horse and driver info and if exists set other info
    TRU:TTID                = MAN:TTID_FreightLiner            !VCO:TTID0
    IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
       IF MAN:DRID = 0
          MAN:DRID      = TRU:DRID
       .
   
       DRI:DRID         = TRU:DRID        !MAN:DRID
       IF Access:Drivers.TryFetch(DRI:PKey_DRID) ~= LEVEL:Benign
          CLEAR(DRI:Record)
       .
       IF MAN:DRID ~= TRU:DRID AND MAN:State < 2             ! Loading|#0|Loaded|#1|On Route|#2|Transferred|
          CASE MESSAGE('The Manifest has a different Driver to the Truck driver specified.|' & |
                        'Manifest Driver: ' & CLIP(L_SG:Driver) & '  (ID: ' & MAN:DRID & ')' & |
                       '|Truck Driver: ' & CLIP(CLIP(DRI:FirstName) & ' ' & DRI:Surname) & '  (ID: ' & TRU:DRID & ')' & |
                       ' ||Would you like to change the Driver to the Truck driver now?', 'Driver', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
          OF BUTTON:Yes
             MAN:DRID         = TRU:DRID
             L_SG:Driver      = CLIP(DRI:FirstName) & ' ' & DRI:Surname
    .  .  .

!          L_SG:Driver       = TRU:Driver
!          L_SG:Assistant    = TRU:Assistant

    EXIT
         
               
! ----------------------------------------------------------------------------------------
Button_Next                      ROUTINE
DATA
R_Exit   BYTE(0)

CODE
   ! Loading|#0|Loaded|#1|On Route|#2|Transferred|
   IF MAN:State = 0                                    ! Was Loading
      IF MAN:Cost <= 0.0
         CASE MESSAGE('You have not specified a Cost for this manifest.|Invoices will be generated if you continue.||Would you like to specify a Cost now before proceeding to Loaded?', 'Manifest Cost', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
         OF BUTTON:Yes
            SELECT(?MAN:Cost)
            R_Exit  = TRUE
         .
      ELSE
         IF DEFORMAT(Transporter_Rates()) < 0
            CASE MESSAGE('Transporter Rates are not satisfied.|Invoices will be generated if you continue.||Would you like to proceeding to Loaded?', 'Manifest Rate', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
            OF BUTTON:No
               SELECT(?MAN:Cost)
               R_Exit  = TRUE
      .  .  .
   ELSIF MAN:State = 3
      MESSAGE('The Manifest is in the final state - Transferred', 'Manifest State', ICON:Asterisk)
      R_Exit  = TRUE
   .

   IF R_Exit = FALSE
      MAN:State   += 1
      IF MAN:State > 3
         MAN:State = 3
      .

      IF MAN:State = 1                                        ! Loaded
         ! Check that all the Deliveries Items are manifested on this Manifest unless allow multi.
         ! If not do not allow state change.
         L_MSG:Unloaded_DIID_List = ''
                                                              ! (p:MID, p:Items_DIID_List)
         L_MSG:No_Unloaded        = Get_ManDelItems_UnLoaded(MAN:MID, L_MSG:Unloaded_DIID_List)
         IF L_MSG:No_Unloaded > 0
            ! Not all Items Manifested - list of unmanifested DIIDs is in L_MSG:Unloaded_DIID_List
            ! Collect details to show the user....
            L_MSG:Item_List       = ''
            LOOP
               IF CLIP(L_MSG:Unloaded_DIID_List) = ''
                  BREAK
               .

               DELI:DIID          = Get_1st_Element_From_Delim_Str(L_MSG:Unloaded_DIID_List, ',', TRUE)
               IF Access:DeliveryItems.TryFetch(DELI:PKey_DIID) = LEVEL:Benign
                  DEL:DID         = DELI:DID
                  IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
                     IF CLIP(L_MSG:Item_List) = ''
                        L_MSG:Item_List  = 'DI No.: ' & DEL:DINo & ',  Item No.: ' & DELI:ItemNo & ', DI Date: ' & FORMAT(DEL:DIDate,@d6) & ' (' & DELI:DID & ')'
                     ELSE
                        L_MSG:Item_List  = CLIP(L_MSG:Item_List) & '|' & 'DI No.: ' & DEL:DINo & ',  Item No.: ' & DELI:ItemNo & ', DI Date: ' & FORMAT(DEL:DIDate,@d6) & ' (' & DELI:DID & ')'
                  .  .
               ELSE
                  MESSAGE('Problem getting DIID.', 'Update_Manifest', ICON:Hand)
            .  .

   !  message('Unloaded_DIID_List: ' & CLIP(L_MSG:Unloaded_DIID_List) & '|No_Unloaded: ' & L_MSG:No_Unloaded)
            MAN:State = 0
            MESSAGE('There are Items for a Delivery on this Manifest that have not been loaded here.||Items:|' & CLIP(L_MSG:Item_List), 'Manifest Not Complete', ICON:Exclamation)
         ELSE
            ! Are they all loaded on this MID?
            L_MSG:Unloaded_DIID_List     = ''
            !  L_MSG:Unloaded_DIID_List will return with MID,DIID,MID,DIID....

            L_MSG:No_MIDs                = Get_Manifest_Del_MIDs(MAN:MID, L_MSG:Unloaded_DIID_List)

   !  message('Unloaded_DIID_List: ' & CLIP(L_MSG:Unloaded_DIID_List) & '|No_MIDs: ' & L_MSG:No_MIDs)

            IF L_MSG:No_MIDs > 1
               ! Collect details to show the user....
               L_MSG:Item_List           = ''
               LOOP
                  IF CLIP(L_MSG:Unloaded_DIID_List) = ''
                     BREAK
                  .

                  A_MAN:MID              = Get_1st_Element_From_Delim_Str(L_MSG:Unloaded_DIID_List, ',', TRUE)
                  IF CLIP(L_MSG:Item_List) = ''
                     L_MSG:Item_List     = 'Manifest: ' & A_MAN:MID
                  ELSE
                     L_MSG:Item_List     = CLIP(L_MSG:Item_List) & '|Manifest: ' & A_MAN:MID
                  .
                  DELI:DIID              = Get_1st_Element_From_Delim_Str(L_MSG:Unloaded_DIID_List, ',', TRUE)
                  IF Access:DeliveryItems.TryFetch(DELI:PKey_DIID) = LEVEL:Benign
                     DEL:DID             = DELI:DID
                     IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
                        L_MSG:Item_List  = CLIP(L_MSG:Item_List) & ' - ' & 'DI No.: ' & DEL:DINo & ',  Item No.: ' & DELI:ItemNo & ',  DI Date: ' & FORMAT(DEL:DIDate,@d6)
                     .
                  ELSE
                     MESSAGE('Problem getting DIID (Manifest)', 'Update Manifest', ICON:Hand)
               .  .

               MAN:State      = 0
               MESSAGE('There are Items for Deliveries on this Manifest which appear on other Manifests!||' & CLIP(L_MSG:Item_List), 'Manifest Incorrect', ICON:Hand)
         .  .

         IF MAN:State = 1
            ! Loaded state has been confirmed here.
            IF L_PG:Print_Manifest = FALSE
               Print_Manifest_Loading(MAN:MID)
            ELSE
               CASE MESSAGE('Would you like to print the Manifest now?', 'Manifest Loaded State', ICON:Question, '1 Copy|2|3', 1)
               OF 1
                  Print_Manifest_Loading(MAN:MID)
               OF 2
                  Print_Manifest_Loading(MAN:MID)
                  Print_Manifest_Loading(MAN:MID)
               OF 3
                  Print_Manifest_Loading(MAN:MID)
                  Print_Manifest_Loading(MAN:MID)
                  Print_Manifest_Loading(MAN:MID)
            .  .

            ! Ask the user if we should invoice the Manifest & Deliveries now.
            Button_Yes_#          = FALSE
            IF L_PG:Create_Invoices = FALSE
               Button_Yes_#       = TRUE
            ELSE
               CASE MESSAGE('Would you like to create the Invoices for the Manifest & Deliveries on this Manifest now?||This will update the Manifest record, any changes you have made will be saved.', 'Manifest Loaded State', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
               OF BUTTON:Yes
                  Button_Yes_#    = TRUE
               ELSE
                  MAN:State       = 0         ! Dont advance state otherwise
            .  .

            IF Button_Yes_# = TRUE
               ! We have to save the current Manifest so that we can use it to generate Invoices
               ! But we want the old state on it in case the invoices dont generate properly
               ! in which case the state will have to remain as is, so save with old state
               ! then set back to new state.
               MAN:State          = 0
               IF Access:Manifest.Update() = LEVEL:Benign
               .
               MAN:State          = 1

               IF MAN:CreatedDate = 0
                    MAN:CreatedDate    = TODAY()
                    MAN:CreatedTime    = CLOCK()        
                  Res_#              = Gen_Man_Invs(MAN:MID, MAN:CreatedDate)
               ELSIF MAN:CreatedDate ~= TODAY()
                  CASE MESSAGE('The Manifest Created Date is not Today, would you like to generate invoices with the Manifest date or Today?', 'Invoice Generation', ICON:Question, 'Manifest|Today', 1)
                  OF 1
                     Res_#   = Gen_Man_Invs(MAN:MID, MAN:CreatedDate)
                  OF 2
                     Res_#   = Gen_Man_Invs(MAN:MID, TODAY())
                  .
               ELSE
                  Res_#      = Gen_Man_Invs(MAN:MID, MAN:CreatedDate)
               .

               IF Res_# < 0
                  MAN:State  = 0
                  MESSAGE('There were some errors on creating Invoices for this Manifest (MID: ' & MAN:MID & ').||The Status of this Invoice cannot be changed until these error are resolved.', 'Generated Manifest Invoices', ICON:Exclamation)
               ELSE
                  IF Access:Manifest.Update() = LEVEL:Benign
                  .

                  IF Res_# ~= 0
                     CASE MESSAGE('There were some errors on creating Invoices for this Manifest (MID: ' & MAN:MID & ').||Do you want to print the Delivery Notes now?', 'Generated Manifest Invoices', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No)
                     OF BUTTON:Yes
                        !Print_DN_POD(MAN:MID,,1)          ! Print all for this MID less any not invoiced
                        Print_DN_POD_Cont(MAN:MID)
                     .
                  ELSE
                     IF L_PG:Print_Delivery_Notes = FALSE
                        !Print_DN_POD(MAN:MID,,1)          ! Print all for this MID less any not invoiced - should be none here
                        Print_DN_POD_Cont(MAN:MID)
                     ELSE
                        CASE MESSAGE('Invoices created for Manifest (MID: ' & MAN:MID & ').||Do you want to print the Delivery Notes now?', 'Generated Manifest Invoices', ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                        OF BUTTON:Yes
                           !Print_DN_POD(MAN:MID,,1)       ! Print all for this MID less any not invoiced - should be none here
                           Print_DN_POD_Cont(MAN:MID)
                  .  .  .

                  IF Res_# < 0
                  ELSE
                     !   Print_Man_Invs
                     ! (p:MID, p:Type, p:Option, p:Preview)
                     ! (ULONG, BYTE, BYTE=0, BYTE=1),LONG,PROC
                     !   p:Option
                     !       0 - COD invoices
                     !       1 - All
                     !   p:Type
                     !       0 - Continuous
                     !       1 - Page printer

                     ! L_PG:Print_Invoices = Prompt to print invoices
                     IF L_PG:Print_Invoices = FALSE          ! (p:MID, p:Type, p:Option, p:Preview)
                        Print_Man_Invs(MAN:MID, 0, 0, 0)       ! No preview on auto print - COD invoices only
                     ELSE
                        CASE MESSAGE('Would you like to print the Invoices for this Manifest now?', 'Print Manifest Invoices', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                        OF BUTTON:Yes
                           Print_Man_Invs(MAN:MID, 0)        ! COD invoices only (3rd parm def. 0)
      .  .  .  .  .  .  .



      IF MAN:State = 2                                    ! On Route
         ! Check Departure and ETA
         IF MAN:DepartDate = 0 OR MAN:ETADate = 0
            CASE MESSAGE('No Departure and/or ETA Date has been specified.||Would you like to specify today (' & FORMAT(TODAY(),@d5) & ') as the Departure and tomorrow as the ETA?||Select No to manually set the Dates.', |
                         'Manifest State', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
            OF BUTTON:No
               SELECT(?MAN:DepartDate)
               MAN:State       = 1
            OF BUTTON:Yes
               MAN:DepartDate  = TODAY()
               MAN:DepartTime  = CLOCK()
               MAN:ETADate     = TODAY() + 1
               MAN:ETATime     = CLOCK()
         .  .

         IF MAN:State = 2                                        ! On Route
            CASE MESSAGE('Are you sure you want to finalise the loading and change the Manifest Status to On Route?', 'Manifest State', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
            OF BUTTON:YES
              IF L_SG:Notifications = TRUE   
                 ! 27 Sep 13 - send emails for DIs that have email addresses on them......
                 ThisWindow.Update()                
                 Manifest_Emails_Setup(MAN:MID,11)                        ! send emails now
                 ThisWindow.Reset()
              .                     
            OF BUTTON:No
               MAN:State = 1
      .  .  .


      IF MAN:State = 3                                        ! Transferred
         ! Ask user if we should change the Floors on all these deliveries to the To Address Floor
         ! if one exists or the General Delivered Floor if no To Address Floor (offer choice of To Floor)
         CASE MESSAGE('Are you sure you want to finalise the Manifest and set Status Transferred?||Note: The Manifest will be saved if you proceed.', 'Manifest State', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
         OF BUTTON:No
            MAN:State = 2
         .

         IF MAN:State = 3                                        ! Transferred
            To_Floor_#   = Ask_MoveFloor(MAN:MID)
            IF To_Floor_# <= 0
               ! No to floor specified.
               CASE MESSAGE('You have not specified a Floor to move the Deliveries to.||Would you still like to set status on this Manifest to Transferred?','To Floor', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
               OF BUTTON:No
                  MAN:State = 2
               .
            ELSE
               ! Update Manifest status first, then move items (there is a check in move that all are in state 3)
               IF Access:Manifest.Update() = LEVEL:Benign
               .
               IF Move_ManDeliveries(MAN:MID, To_Floor_#) < 0
                  MESSAGE('Not all applicable Deliveries were updated to the new Floor.', 'Floor Updates', ICON:Hand)
            .  .

            IF L_SG:Notifications = TRUE   
               ! 27 Sep 13 - send emails for DIs that have email addresses on them......
               ThisWindow.Update()                
               Manifest_Emails_Setup(MAN:MID,12)                         ! send emails now - replies on DIs being moved to a new Floor to get branch            
               ThisWindow.Reset()                
      .  .  .

      IF MAN:State < 3 AND LOC:Last_State ~= MAN:State                          ! If indeed we did change states, then automate
         IF L_PG:State_Progression = 1
            POST(EVENT:Accepted, ?Button_Next)
         ELSIF L_PG:State_Progression = 2
            CASE MESSAGE('State has been advanced.||Would you like to advance the state of this Manifest again?', 'Prompt for State Progression', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
            OF BUTTON:Yes
               POST(EVENT:Accepted, ?Button_Next)
      .  .  .

      DO Load_State
   .
   EXIT
         
Button_Prev                      ROUTINE
   ! Loading|#0|Loaded|#1|On Route|#2|Transferred|

   IF MAN:State >= 2
      IF MAN:State = 2
         MESSAGE('State is On Route.||Normally you should not change the status back once On Route status has been reached.', 'Manifest State', ICON:Hand)
      ELSE
         MESSAGE('State is Transferred.||You should not change the status back once Transferred status has been reached.', 'Manifest State', ICON:Hand)
   .  .

   MAN:State   -= 1

   CASE MAN:State
   OF 0                ! Loading|#0|
      ?Button_Prev{PROP:Tip}           = 'Status is Loading - this stage is Earliest'
   OF 1                ! Loaded|#1|
      ?Button_Prev{PROP:Tip}           = 'Status is Loaded - set Status to Loading (record saved immediately)'
   OF 2                ! On Route|#2|
      ?Button_Prev{PROP:Tip}           = 'Status is On Route - set Status to Loaded (record saved immediately)'
   OF 3                ! Transferred|#3
      ?Button_Prev{PROP:Tip}           = 'Status is Transferred - set status to On Route'
   ELSE
      MAN:State                        = 0
   .

   DO Load_State
   EXIT
         

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Manifest Record '
  OF InsertRecord
    ActionMessage = 'Manifest Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Manifest Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Manifest_24_04_14')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PROMPT_TotWeight
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_BG:Vehicle_Desc',L_BG:Vehicle_Desc)              ! Added by: BrowseBox(ABC)
  BIND('LOC:Item_Description',LOC:Item_Description)        ! Added by: BrowseBox(ABC)
  BIND('L_IL:Weight',L_IL:Weight)                          ! Added by: BrowseBox(ABC)
  BIND('L_IL:2nd_Transporter',L_IL:2nd_Transporter)        ! Added by: BrowseBox(ABC)
  BIND('L_IL:2nd_TransPrice',L_IL:2nd_TransPrice)          ! Added by: BrowseBox(ABC)
  BIND('L_IL:2nd_Trans_Area',L_IL:2nd_Trans_Area)          ! Added by: BrowseBox(ABC)
  BIND('L_IL:Total_Charge',L_IL:Total_Charge)              ! Added by: BrowseBox(ABC)
  BIND('L_IL:Weight_Vol',L_IL:Weight_Vol)                  ! Added by: BrowseBox(ABC)
  BIND('L_TIG:Total',L_TIG:Total)                          ! Added by: BrowseBox(ABC)
      BIND('A_MAN:MID',A_MAN:MID)                 ! For our view
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(MAN:Record,History::MAN:Record)
  SELF.AddHistoryField(?MAN:Rate,9)
  SELF.AddHistoryField(?MAN:Cost,8)
  SELF.AddHistoryField(?MAN:Broking,25)
  SELF.AddHistoryField(?MAN:DepartTime,19)
  SELF.AddHistoryField(?MAN:DepartDate,18)
  SELF.AddHistoryField(?MAN:VATRate,10)
  SELF.AddHistoryField(?MAN:CreatedDate,14)
  SELF.AddHistoryField(?MAN:ETADate,22)
  SELF.AddHistoryField(?MAN:ETATime,23)
  SELF.AddHistoryField(?MAN:BID,2)
  SELF.AddHistoryField(?MAN:MID,1)
  SELF.AddHistoryField(?MAN:VCID,4)
  SELF.AddUpdateFile(Access:Manifest)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Relate:InvoiceAlias.Open                                 ! File InvoiceAlias used by this procedure, so make sure it's RelationManager is open
  Relate:ManifestAlias.Open                                ! File ManifestAlias used by this procedure, so make sure it's RelationManager is open
  Relate:TransporterAlias.Open                             ! File TransporterAlias used by this procedure, so make sure it's RelationManager is open
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleComposition.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailer.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleMakeModel.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:__RatesTransporter.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Drivers.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailerAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:Manifest
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW_ManLoads.Init(?Browse:4,Queue:Browse:4.ViewPosition,BRW4::View:Browse,Queue:Browse:4,Relate:ManifestLoad,SELF) ! Initialize the browse manager
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:_InvoiceTransporter,SELF) ! Initialize the browse manager
  BRW3.Init(?List:2,Queue:Browse:1.ViewPosition,BRW3::View:Browse,Queue:Browse:1,Relate:TransporterPaymentsAllocations,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
      DO Load_State
      IF MAN:State >= 2               ! On Route
         ThisWindow.Request   = ViewRecord
      .
  
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?CallLookup_Transporter)
    ?L_SG:TransporterName{PROP:ReadOnly} = True
    DISABLE(?CallLookup_Composition)
    ?L_SG:CompositionName{PROP:ReadOnly} = True
    DISABLE(?Button_ChangeTransporter)
    DISABLE(?Button_ChangeComposition)
    DISABLE(?CallLookup_Journey)
    ?L_SG:Journey{PROP:ReadOnly} = True
    ?MAN:Rate{PROP:ReadOnly} = True
    DISABLE(?L_SV:Prev_Cost)
    ?MAN:Cost{PROP:ReadOnly} = True
    DISABLE(?Button_Adjust)
    DISABLE(?Button_Check_Rate)
    DISABLE(?CallLookup_Driver)
    ?L_SG:Driver{PROP:ReadOnly} = True
    DISABLE(?Button_Save)
    DISABLE(?Insert_AddLoad)
    DISABLE(?Change_Loaded)
    DISABLE(?Button_ChangeDI)
    DISABLE(?Button_Print_Delivery_Notes)
    DISABLE(?Button_Print_Invoice)
    DISABLE(?Button_Print_Manifest)
    ?MAN:DepartTime{PROP:ReadOnly} = True
    DISABLE(?Button_ChangeManDate)
    ?MAN:VATRate{PROP:ReadOnly} = True
    ?MAN:CreatedDate{PROP:ReadOnly} = True
    ?MAN:ETATime{PROP:ReadOnly} = True
    DISABLE(?Button_Change_Vehicles)
    DISABLE(?Button_View_Composition)
    ?L_SG:MakeModels{PROP:ReadOnly} = True
    ?L_SG:Registrations{PROP:ReadOnly} = True
    ?L_SG:Licensing{PROP:ReadOnly} = True
    ?L_SG:VC_Capacity{PROP:ReadOnly} = True
    ?L_SG:No_Vehicles{PROP:ReadOnly} = True
    ?L_TG:CompositionName{PROP:ReadOnly} = True
    DISABLE(?CallLookup_FrightLiner)
    ?L_TG:FreightLiner{PROP:ReadOnly} = True
    DISABLE(?BUTTON_ClearFreightLiner)
    DISABLE(?CallLookup_Trailer)
    ?L_TG:Trailer{PROP:ReadOnly} = True
    DISABLE(?BUTTON_ClearTrailer)
    DISABLE(?CallLookup_SuperLink)
    ?L_TG:SuperLink{PROP:ReadOnly} = True
    DISABLE(?BUTTON_ClearSuperLink)
    DISABLE(?BUTTON_Apply_VCO_Changes)
    DISABLE(?BUTTON_Cancel_VCO_Changes)
    ?L_TG:Capacity{PROP:ReadOnly} = True
    DISABLE(?Button_Prev)
    DISABLE(?Button_Next)
    DISABLE(?PrintDetails)
  END
      IF MAN:State >= 2               ! On Route
         ENABLE(?Button_Next)
         ENABLE(?Button_Prev)
      .
  
      ?Prompt_Change{PROP:Text}    = ''
  
      LOC:Last_State  = MAN:State
      ENABLE(?Button_Print_Delivery_Notes)
      ENABLE(?Button_Print_Invoice)
      ENABLE(?Button_Print_Manifest)
  
      ENABLE(?Button_ChangeManDate)
      
      Enable(?PrintDetails)
  BRW_ManLoads.Q &= Queue:Browse:4
  BRW_ManLoads.FileLoaded = 1                              ! This is a 'file loaded' browse
  BRW_ManLoads.AddSortOrder(,MAL:SKey_MID_TTID)            ! Add the sort order for MAL:SKey_MID_TTID for sort order 1
  BRW_ManLoads.AddRange(MAL:MID,MAN:MID)                   ! Add single value range limit for sort order 1
  BRW_ManLoads.AppendOrder('+MALD:MLDID')                  ! Append an additional sort order
  BRW_ManLoads.AddField(L_BG:Vehicle_Desc,BRW_ManLoads.Q.L_BG:Vehicle_Desc) ! Field L_BG:Vehicle_Desc is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(DEL:DINo,BRW_ManLoads.Q.DEL:DINo)  ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(DELI:ItemNo,BRW_ManLoads.Q.DELI:ItemNo) ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(MALD:UnitsLoaded,BRW_ManLoads.Q.MALD:UnitsLoaded) ! Field MALD:UnitsLoaded is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(CLI:ClientName,BRW_ManLoads.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(COM:Commodity,BRW_ManLoads.Q.COM:Commodity) ! Field COM:Commodity is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(LOC:Item_Description,BRW_ManLoads.Q.LOC:Item_Description) ! Field LOC:Item_Description is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(L_IL:Weight,BRW_ManLoads.Q.L_IL:Weight) ! Field L_IL:Weight is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(L_IL:2nd_Transporter,BRW_ManLoads.Q.L_IL:2nd_Transporter) ! Field L_IL:2nd_Transporter is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(L_IL:2nd_TransPrice,BRW_ManLoads.Q.L_IL:2nd_TransPrice) ! Field L_IL:2nd_TransPrice is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(L_IL:2nd_Trans_Area,BRW_ManLoads.Q.L_IL:2nd_Trans_Area) ! Field L_IL:2nd_Trans_Area is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(L_IL:Total_Charge,BRW_ManLoads.Q.L_IL:Total_Charge) ! Field L_IL:Total_Charge is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(L_IL:Weight_Vol,BRW_ManLoads.Q.L_IL:Weight_Vol) ! Field L_IL:Weight_Vol is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(DEL:DID,BRW_ManLoads.Q.DEL:DID)    ! Field DEL:DID is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(MALD:DIID,BRW_ManLoads.Q.MALD:DIID) ! Field MALD:DIID is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(MALD:MLDID,BRW_ManLoads.Q.MALD:MLDID) ! Field MALD:MLDID is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(MALD:MLID,BRW_ManLoads.Q.MALD:MLID) ! Field MALD:MLID is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(DELI:Units,BRW_ManLoads.Q.DELI:Units) ! Field DELI:Units is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(DELI:Type,BRW_ManLoads.Q.DELI:Type) ! Field DELI:Type is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(DELI:ContainerNo,BRW_ManLoads.Q.DELI:ContainerNo) ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(DELI:Weight,BRW_ManLoads.Q.DELI:Weight) ! Field DELI:Weight is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(PACK:Packaging,BRW_ManLoads.Q.PACK:Packaging) ! Field PACK:Packaging is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(TRU:Registration,BRW_ManLoads.Q.TRU:Registration) ! Field TRU:Registration is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(TRU:Capacity,BRW_ManLoads.Q.TRU:Capacity) ! Field TRU:Capacity is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(MAL:MLID,BRW_ManLoads.Q.MAL:MLID)  ! Field MAL:MLID is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(MAL:MID,BRW_ManLoads.Q.MAL:MID)    ! Field MAL:MID is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(MAL:TTID,BRW_ManLoads.Q.MAL:TTID)  ! Field MAL:TTID is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(TRU:TTID,BRW_ManLoads.Q.TRU:TTID)  ! Field TRU:TTID is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(DELI:DIID,BRW_ManLoads.Q.DELI:DIID) ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(CLI:CID,BRW_ManLoads.Q.CLI:CID)    ! Field CLI:CID is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(PACK:PTID,BRW_ManLoads.Q.PACK:PTID) ! Field PACK:PTID is a hot field or requires assignment from browse
  BRW_ManLoads.AddField(COM:CMID,BRW_ManLoads.Q.COM:CMID)  ! Field COM:CMID is a hot field or requires assignment from browse
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,INT:Fkey_MID)                         ! Add the sort order for INT:Fkey_MID for sort order 1
  BRW2.AddRange(INT:MID,MAN:MID)                           ! Add single value range limit for sort order 1
  BRW2.AddLocator(BRW2::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW2::Sort0:Locator.Init(,INT:MID,1,BRW2)                ! Initialize the browse locator using  using key: INT:Fkey_MID , INT:MID
  BRW2.AppendOrder('+INT:CreatedDate,+INT:TIN')            ! Append an additional sort order
  ?List{PROP:IconList,1} = '~checkoffdim.ico'
  ?List{PROP:IconList,2} = '~checkon.ico'
  BRW2.AddField(INT:TIN,BRW2.Q.INT:TIN)                    ! Field INT:TIN is a hot field or requires assignment from browse
  BRW2.AddField(INT:Cost,BRW2.Q.INT:Cost)                  ! Field INT:Cost is a hot field or requires assignment from browse
  BRW2.AddField(INT:VAT,BRW2.Q.INT:VAT)                    ! Field INT:VAT is a hot field or requires assignment from browse
  BRW2.AddField(L_TIG:Total,BRW2.Q.L_TIG:Total)            ! Field L_TIG:Total is a hot field or requires assignment from browse
  BRW2.AddField(INT:VATRate,BRW2.Q.INT:VATRate)            ! Field INT:VATRate is a hot field or requires assignment from browse
  BRW2.AddField(INT:DINo,BRW2.Q.INT:DINo)                  ! Field INT:DINo is a hot field or requires assignment from browse
  BRW2.AddField(INT:Leg,BRW2.Q.INT:Leg)                    ! Field INT:Leg is a hot field or requires assignment from browse
  BRW2.AddField(INT:Broking,BRW2.Q.INT:Broking)            ! Field INT:Broking is a hot field or requires assignment from browse
  BRW2.AddField(INT:Manifest,BRW2.Q.INT:Manifest)          ! Field INT:Manifest is a hot field or requires assignment from browse
  BRW2.AddField(INT:ExtraInv,BRW2.Q.INT:ExtraInv)          ! Field INT:ExtraInv is a hot field or requires assignment from browse
  BRW2.AddField(INT:Rate,BRW2.Q.INT:Rate)                  ! Field INT:Rate is a hot field or requires assignment from browse
  BRW2.AddField(INT:CreatedDate,BRW2.Q.INT:CreatedDate)    ! Field INT:CreatedDate is a hot field or requires assignment from browse
  BRW2.AddField(INT:CreatedTime,BRW2.Q.INT:CreatedTime)    ! Field INT:CreatedTime is a hot field or requires assignment from browse
  BRW2.AddField(INT:MID,BRW2.Q.INT:MID)                    ! Field INT:MID is a hot field or requires assignment from browse
  BRW3.Q &= Queue:Browse:1
  BRW3.AddSortOrder(,TRAPA:FKey_MID)                       ! Add the sort order for TRAPA:FKey_MID for sort order 1
  BRW3.AddRange(TRAPA:MID,MAN:MID)                         ! Add single value range limit for sort order 1
  BRW3.AddLocator(BRW3::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW3::Sort0:Locator.Init(,TRAPA:MID,1,BRW3)              ! Initialize the browse locator using  using key: TRAPA:FKey_MID , TRAPA:MID
  BRW3.AppendOrder('+TRAPA:AllocationDate,+TRAPA:TRPAID')  ! Append an additional sort order
  BRW3.AddField(TRAP:TPID,BRW3.Q.TRAP:TPID)                ! Field TRAP:TPID is a hot field or requires assignment from browse
  BRW3.AddField(TRAPA:AllocationNo,BRW3.Q.TRAPA:AllocationNo) ! Field TRAPA:AllocationNo is a hot field or requires assignment from browse
  BRW3.AddField(TRAPA:AllocationDate,BRW3.Q.TRAPA:AllocationDate) ! Field TRAPA:AllocationDate is a hot field or requires assignment from browse
  BRW3.AddField(TRAPA:AllocationTime,BRW3.Q.TRAPA:AllocationTime) ! Field TRAPA:AllocationTime is a hot field or requires assignment from browse
  BRW3.AddField(TRAPA:Amount,BRW3.Q.TRAPA:Amount)          ! Field TRAPA:Amount is a hot field or requires assignment from browse
  BRW3.AddField(TRAPA:Comment,BRW3.Q.TRAPA:Comment)        ! Field TRAPA:Comment is a hot field or requires assignment from browse
  BRW3.AddField(TRAPA:TRPAID,BRW3.Q.TRAPA:TRPAID)          ! Field TRAPA:TRPAID is a hot field or requires assignment from browse
  BRW3.AddField(TRAPA:MID,BRW3.Q.TRAPA:MID)                ! Field TRAPA:MID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  L_SG:Notifications   = GETINI('Delivery', 'NotificationsOn', '1', GLO:Global_INI)
      IF SELF.Request = InsertRecord
         MAN:JID          = GETINI('Manifest', 'Default_Manifest_JID14', 1, GLO:Local_INI)     ! changed ini to 14 so users all get new default!
           
         DISABLE(?Insert_AddLoad)
  
         MAN:TID          = GETINI('Manifest', 'Default_Manifest_TransID14', 2, GLO:Local_INI)
         ! Check their VAT rate?  Just set default
         MAN:VATRate      = Get_Setup_Info(4)
      ELSE
         L_TRG:TID    = MAN:TID
         L_TRG:JID    = MAN:JID
         L_TRG:VCID   = MAN:VCID
  
         DO Load_Charges_Q
         DISABLE(?Button_Save)
  
         LOC:Start    = CLOCK()
         IF Get_ManLoad_Info(MAN:MID, 1) > 0
            DISABLE(?Group_Trans)
  
  !          ENABLE(?L_SG:TransporterName)
  !          ?L_SG:TransporterName{PROP:ReadOnly}    = TRUE
  !          ENABLE(?L_SG:CompositionName)                     
            ?L_SG:CompositionName{PROP:ReadOnly}    = TRUE     ! Taken out of group because at end and easier then doing all
         .
  
         IF CLOCK() - LOC:Start > 0
            db.debugout('[Update_Manifest - Init]  time taken: ' & CLOCK() - LOC:Start)
      .  .
      TRA:TID                 = MAN:TID
      IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
         L_SG:TransporterName = TRA:TransporterName
         L_SG:ChargesVAT      = TRA:ChargesVAT
  
      .
     VCO:VCID                = MAN:VCID
     L_SG:VCID               = MAN:VCID
     IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
        L_SG:CompositionName = VCO:CompositionName
     ELSIF MAN:VCID <> 0 
        L_SG:CompositionName = '< Comp. not found for: ' & MAN:VCID & ' > '
     .
  
     Veh_Composition_New()
     !DO Set_Composition_New  
  
  
     JOU:JID                 = MAN:JID
     IF Access:Journeys.TryFetch(JOU:PKey_JID) = LEVEL:Benign
        L_SG:Journey         = JOU:Journey
     .
  
      DO Get_Tot_Weight
  
      DO Load_State
      L_PG:Print_Manifest         = GETINI('Manifest_Prompts', 'Print_Manifest', 0, GLO:Local_INI)
      L_PG:Create_Invoices        = GETINI('Manifest_Prompts', 'Create_Invoices', 0, GLO:Local_INI)
      L_PG:Print_Delivery_Notes   = GETINI('Manifest_Prompts', 'Print_Delivery_Notes', 0, GLO:Local_INI)
      L_PG:Print_Invoices         = GETINI('Manifest_Prompts', 'Print_Invoices', 0, GLO:Local_INI)
  
      L_PG:State_Progression      = GETINI('Manifest_Prompts', 'State_Progression', 2, GLO:Local_INI)
  BRW_ManLoads.AskProcedure = 8                            ! Will call: Update_ManifestLoad_h(MAN:VCID)
      BRW_ManLoads.Popup.AddItem('Print Del. Note','PopupPrintDN')
      BRW_ManLoads.Popup.AddItemEvent('PopupPrintDN',EVENT:User)
      BRW_ManLoads.Popup.AddItem('Print Invoice','PopupPrintInv')
      BRW_ManLoads.Popup.AddItemEvent('PopupPrintInv',EVENT:User+1)
      BRW_ManLoads.Popup.AddItem('Print Invoice (Lazer)','PopupPrintInvL')
      BRW_ManLoads.Popup.AddItemEvent('PopupPrintInvL',EVENT:User+2)
      BRW_ManLoads.Popup.AddItem('-','SeparatorPrintOpt')
  
  BRW2.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW2.ToolbarItem.HelpButton = ?Help
  BRW3.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  BRW3.ToolbarItem.HelpButton = ?Help
      LOC:Manifest_Date_Orig  = MAN:CreatedDate
  SELF.SetAlerts()
      BRW_ManLoads.Popup.AddItem('-','SeparatorChangeDI') ! BrowseAddPopupAction (ABC Free)
      BRW_ManLoads.Popup.AddItemMimic('Popup_ChangeDI',?Button_ChangeDI) ! BrowseAddPopupAction (ABC Free)
  
      BRW_ManLoads.Popup.AddItem('-','SeparatorManEmails') ! BrowseAddPopupAction (ABC Free)
      BRW_ManLoads.Popup.AddItem('Manifest Emails','PopupManEmail') ! BrowseAddPopupAction (ABC Free)        
      BRW_ManLoads.Popup.AddItemEvent('PopupManEmail',EVENT:User+3)
  
     SC.New_TID(MAN:TID)     ! needs to be primed on open for Update
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW4::SortHeader.Init(Queue:Browse:4,?Browse:4,'','',BRW4::View:Browse,MAL:PKey_MLID)
  BRW4::SortHeader.UseSortColors = False
  BRW4::SortHeader.SetStorageSettings('.\WorTrnis.INI','Update_Manifest_24_04_14')
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
      !IF SELF.Request = InsertRecord AND GlobalResponse = RequestCancelled
      !   START(Process_Deliveries)
      !.
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
    Relate:InvoiceAlias.Close
    Relate:ManifestAlias.Close
    Relate:TransporterAlias.Close
  !Kill the Sort Header
  BRW4::SortHeader.Kill()
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  MAN:BID = GLO:BranchID
  PARENT.PrimeFields


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  BRW_ManLoads.Popup.SetItemEnable('Popup_ChangeDI',CHOOSE(NOT(LOC:Allow_Change_DI = 0))) ! BrowseAddPopupAction (ABC Free)
  
  
  PARENT.Reset(Force)
      DO Load_State_Buttons


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Transporter
      Select_VehicleComposition
      Browse_Journeys
      Browse_Drivers('','')
      Browse_TruckTrailer
      Browse_TruckTrailer
      Browse_TruckTrailer
      Update_ManifestLoad_h(MAN:VCID)
    END
    ReturnValue = GlobalResponse
  END
      DO Rate_Calc
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  !Initialize the Sort Header using the Browse Queue and Browse Control
  BRW4::SortHeader.SetAlerts()


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?CallLookup_Composition
          ! Load Transporter record
          TRA:TID = MAN:TID
          IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
          .
    OF ?Button_ChangeDI
          ! Get the DI record
          GET(Queue:Browse:4,CHOICE(?Browse:4))
          IF ERRORCODE()
             CYCLE
          .
      
      
          DEL:DID     = Queue:Browse:4.DEL:DID
          IF Access:Deliveries.TryFetch(DEL:PKey_DID) ~= LEVEL:Benign
             CYCLE
          .
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup_Transporter
      ThisWindow.Update()
      TRA:TransporterName = L_SG:TransporterName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SG:TransporterName = TRA:TransporterName
        MAN:TID = TRA:TID
        L_SG:ChargesVAT = TRA:ChargesVAT
        L_TMPG:Broking = TRA:Broking
      END
      ThisWindow.Reset(1)
          DO Transporter_Set
    OF ?L_SG:TransporterName
      IF L_SG:TransporterName OR ?L_SG:TransporterName{PROP:Req}
        TRA:TransporterName = L_SG:TransporterName
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SG:TransporterName = TRA:TransporterName
            MAN:TID = TRA:TID
            L_SG:ChargesVAT = TRA:ChargesVAT
            L_TMPG:Broking = TRA:Broking
          ELSE
            CLEAR(MAN:TID)
            CLEAR(L_SG:ChargesVAT)
            CLEAR(L_TMPG:Broking)
            SELECT(?L_SG:TransporterName)
            CYCLE
          END
        ELSE
          MAN:TID = TRA:TID
          L_SG:ChargesVAT = TRA:ChargesVAT
          L_TMPG:Broking = TRA:Broking
        END
      END
      ThisWindow.Reset()
          DO Transporter_Set
    OF ?CallLookup_Composition
      ThisWindow.Update()
      VCO:CompositionName = L_SG:CompositionName
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_SG:CompositionName = VCO:CompositionName
        L_SG:VCID = VCO:VCID
        MAN:TTID_FreightLiner = VCO:TTID0
        MAN:TTID_Trailer = VCO:TTID1
        MAN:TTID_SuperLink = VCO:TTID2
      END
      ThisWindow.Reset(1)
          POST(EVENT:Accepted, ?L_SG:CompositionName)
      !    DO Transporter_Rate
      !    DO Veh_Composition
    OF ?L_SG:CompositionName
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_SG:CompositionName OR ?L_SG:CompositionName{PROP:Req}
          VCO:CompositionName = L_SG:CompositionName
          IF Access:VehicleComposition.TryFetch(VCO:Key_Name)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              L_SG:CompositionName = VCO:CompositionName
              L_SG:VCID = VCO:VCID
              MAN:TTID_FreightLiner = VCO:TTID0
              MAN:TTID_Trailer = VCO:TTID1
              MAN:TTID_SuperLink = VCO:TTID2
            ELSE
              CLEAR(L_SG:VCID)
              CLEAR(MAN:TTID_FreightLiner)
              CLEAR(MAN:TTID_Trailer)
              CLEAR(MAN:TTID_SuperLink)
              SELECT(?L_SG:CompositionName)
              CYCLE
            END
          ELSE
            L_SG:VCID = VCO:VCID
            MAN:TTID_FreightLiner = VCO:TTID0
            MAN:TTID_Trailer = VCO:TTID1
            MAN:TTID_SuperLink = VCO:TTID2
          END
        END
      END
      ThisWindow.Reset()
          IF Get_ManLoad_Info(MAN:MID, 1) > 0 AND MAN:VCID ~= L_SG:VCID
             ! We have some items, and have changed the Vehicle composition, check that the new comp. can handle all cargo on similarily split
             ! vehicles, if so then allocate cargo to new vehicles and allow change of VCID
      
             Done_#  = FALSE
             IF Change_Veh_Comp(L_SG:VCID, MAN:VCID) < 0                 ! Re-load old composition
                CASE MESSAGE('Selected replacement Vehicle Composition does not match the current Vehicle Composition in capacity per Tuck/Trailer.' & |
                            '||Would you like to override this warning?||If you do some trucks/trailers may be overloaded.' |
                            ,'Composition Not Changed',ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
                  OF BUTTON:Yes
                   !Change_Veh_Comp                  PROCEDURE(ULONG p_New_VCID, ULONG p_Old_VCID, BYTE p:Override=0),LONG
                   IF Change_Veh_Comp(L_SG:VCID, MAN:VCID, 1) < 0                 ! Re-load old composition
                   .
                   Done_#   = TRUE
                .
             ELSE
                Done_#      = TRUE
             .
                
             IF Done_# = FALSE
                VCO:VCID  = MAN:VCID
                IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
                    L_SG:CompositionName   = VCO:CompositionName
                    L_SG:VCID              = MAN:VCID
                .
             ELSE
                MAN:VCID  = L_SG:VCID
                Veh_Composition_New(TRUE)
                IF Access:Manifest.Update() = LEVEL:Benign
                   BRW_ManLoads.ResetQueue(1)
             .  .
          ELSE
             MAN:VCID     = L_SG:VCID  
             Veh_Composition_New(TRUE)
          .
          DO Transporter_Rate
          IF CLIP(L_SG:CompositionName) = ''
             CLEAR(MAN:VCID)
             Veh_Composition_New(TRUE)
          .
          DO Load_Charges_Q
    OF ?Button_ChangeTransporter
      ThisWindow.Update()
          ! Enable the Transporter to allow changes
          ENABLE(?Group_Trans)
      
      
      !    PROMPT('Transporter:'),AT(8,22),USE(?TransporterName:Prompt),#ORIG(?TransporterName:Prompt)
      !    BUTTON('...'),AT(50,22,12,10),USE(?CallLookup_Transporter),#SEQ(9),#ORIG(?CallLookup)
      !    ENTRY(@s35),AT(66,22,85,10),USE(L_SG:TransporterName),MSG('Transporters Name'),TIP('Transporters Name'), |
      !        REQ,#ORIG(L_SG:TransporterName)
      
          DISABLE(?CompositionName:Prompt)
          DISABLE(?CallLookup_Composition)
          DISABLE(?L_SG:CompositionName)
      
      
          ENABLE(?L_SG:TransporterName)
          ENABLE(?CallLookup_Transporter)
          ENABLE(?TransporterName:Prompt)
    OF ?Button_ChangeComposition
      ThisWindow.Update()
          ! Enable the Transporter to allow changes
          ENABLE(?Group_Trans)
      
          DISABLE(?L_SG:TransporterName)
          DISABLE(?CallLookup_Transporter)
          DISABLE(?TransporterName:Prompt)
      
      
          ENABLE(?CompositionName:Prompt)
          ENABLE(?CallLookup_Composition)
          ENABLE(?L_SG:CompositionName)
    OF ?CallLookup_Journey
      ThisWindow.Update()
      JOU:Journey = L_SG:Journey
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        L_SG:Journey = JOU:Journey
        MAN:JID = JOU:JID
      END
      ThisWindow.Reset(1)
          DO Transporter_Rate
          DO Load_Charges_Q
    OF ?L_SG:Journey
      IF L_SG:Journey OR ?L_SG:Journey{PROP:Req}
        JOU:Journey = L_SG:Journey
        IF Access:Journeys.TryFetch(JOU:Key_Journey)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            L_SG:Journey = JOU:Journey
            MAN:JID = JOU:JID
          ELSE
            CLEAR(MAN:JID)
            SELECT(?L_SG:Journey)
            CYCLE
          END
        ELSE
          MAN:JID = JOU:JID
        END
      END
      ThisWindow.Reset()
          DO Transporter_Rate
          DO Load_Charges_Q
    OF ?MAN:Rate
          IF QuickWindow{PROP:AcceptAll} = FALSE
             DO Cost_Rate_Calc
          .
    OF ?MAN:Cost
          DO Rate_Calc
    OF ?Button_Adjust
      ThisWindow.Update()
          ?MAN:Rate{PROP:Readonly}    = FALSE
          ?MAN:Rate{PROP:Skip}        = FALSE
          ?MAN:Rate{PROP:Background}  = -1
    OF ?Button_Check_Rate
      ThisWindow.Update()
          Transporter_Rates(TRUE)         ! Not quiet mode
    OF ?MAN:Broking
      IF QuickWindow{PROP:AcceptAll}  = FALSE
         ! then user clicked this, write log
         Add_Log('User: ' & GLO:Login & ' set Broking to ' & MAN:Broking & ' on Manifest: ' & MAN:MID,'Manifest form updated','Audit_Transporter_Invoice.log')
      .
    OF ?CallLookup_Driver
      ThisWindow.Update()
      DRI:FirstNameSurname = L_SG:Driver
      IF SELF.Run(4,SelectRecord) = RequestCompleted
        L_SG:Driver = DRI:FirstNameSurname
        MAN:DRID = DRI:DRID
      END
      ThisWindow.Reset(1)
          DRI:DRID          = MAN:DRID
          IF Access:Drivers.TryFetch(DRI:PKey_DRID) = LEVEL:Benign
             L_SG:Driver    = CLIP(DRI:FirstName) & ' ' & DRI:Surname
             DISPLAY(?L_SG:Driver)
          .
    OF ?L_SG:Driver
      IF NOT QuickWindow{PROP:AcceptAll}
        IF L_SG:Driver OR ?L_SG:Driver{PROP:Req}
          DRI:FirstNameSurname = L_SG:Driver
          IF Access:Drivers.TryFetch(DRI:SKey_FirstNameSurname)
            IF SELF.Run(4,SelectRecord) = RequestCompleted
              L_SG:Driver = DRI:FirstNameSurname
              MAN:DRID = DRI:DRID
            ELSE
              CLEAR(MAN:DRID)
              SELECT(?L_SG:Driver)
              CYCLE
            END
          ELSE
            MAN:DRID = DRI:DRID
          END
        END
      END
      ThisWindow.Reset()
    OF ?Button_Save
      ThisWindow.Update()
          IF Access:Manifest.Update() = LEVEL:Benign
             IF MAN:VCID = 0
                MESSAGE('Please select a Vehicle Composition.', 'Capture Items', ICON:Exclamation)
                SELECT(?L_SG:CompositionName)
             ELSE
                ENABLE(?Insert_AddLoad)
                DISABLE(?Button_Save)
      
                POST(EVENT:Accepted, ?Insert_AddLoad)
          .  .
    OF ?Insert_AddLoad
      ThisWindow.Update()
          IF MAN:VCID = 0
             MESSAGE('Please select a Vehicle composition for this Manifest.', 'Manifest', ICON:Exclamation)
             SELECT(?L_SG:CompositionName)
             CYCLE
          .
    OF ?Button_ChangeDI
      ThisWindow.Update()
      GlobalRequest = ChangeRecord
      Update_Deliveries(1)
      ThisWindow.Reset
    OF ?Button_Print_Delivery_Notes
      ThisWindow.Update()
          ThisWindow.Update
      
          CASE POPUP('Print Delivery Notes Continuous|Print Delivery Notes Laser')
          OF 1
             Print_DN_POD_Cont(MAN:MID)
          OF 2
             Print_DN_POD(MAN:MID)
          .
      
          ThisWindow.Reset
      
    OF ?Button_Print_Invoice
      ThisWindow.Update()
          ThisWindow.Update
      
          CASE POPUP('Print Invoices Continuous|Print Invoices Laser|Print COD Invoices Continuous')
          OF 1
             Print_Man_Invs(MAN:MID, 0, 1)
          OF 2
             Print_Man_Invs(MAN:MID, 1, 1)
          OF 3
             Print_Man_Invs(MAN:MID, 0)
          .
      
          ThisWindow.Reset
      
      
          
    OF ?Button_Print_Manifest
      ThisWindow.Update()
          ThisWindow.Update
      
          CASE POPUP('Print Manifest Loading|Print Manifest')
          OF 1
             Print_Manifest_Loading(MAN:MID)
          OF 2
             Print_Manifest(MAN:MID)
          .
      
          ThisWindow.Reset
      
    OF ?Button_ChangeManDate
      ThisWindow.Update()
          ?MAN:CreatedDate{PROP:ReadOnly}     = FALSE
          ?MAN:CreatedDate{PROP:Flat}         = FALSE
          ?MAN:CreatedDate{PROP:Background}   = -1
          DISPLAY
      
    OF ?Button_Change_Vehicles
      ThisWindow.Update()
         UNHIDE(?GROUP_Trucks_Change)
         SELECT(?L_TG:FreightLiner)
      
         DISABLE(?OK)
         DISABLE(?Tab_Items)
         DISABLE(?Tab_Extra)
    OF ?Button_View_Composition
      ThisWindow.Update()
      IF MAN:VCID <> 0
         ThisWindow.Update()
         
         VCO:VCID = MAN:VCID
         IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = Level:Benign
            GlobalRequest = ViewRecord
            Update_VehicleComposition()
         .
         
         ThisWindow.Reset
      .
               
    OF ?CallLookup_FrightLiner
      ThisWindow.Update()
      TRU:Registration = L_TG:FreightLiner
      IF SELF.Run(5,SelectRecord) = RequestCompleted
        L_TG:FreightLiner = TRU:Registration
        L_TG:FreightLinerTTID = TRU:TTID
      END
      ThisWindow.Reset(1)
      Set_TruckTrailer()
    OF ?L_TG:FreightLiner
      IF L_TG:FreightLiner OR ?L_TG:FreightLiner{PROP:Req}
        TRU:Registration = L_TG:FreightLiner
        IF Access:TruckTrailer.TryFetch(TRU:Key_Registration)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            L_TG:FreightLiner = TRU:Registration
            L_TG:FreightLinerTTID = TRU:TTID
          ELSE
            CLEAR(L_TG:FreightLinerTTID)
            SELECT(?L_TG:FreightLiner)
            CYCLE
          END
        ELSE
          L_TG:FreightLinerTTID = TRU:TTID
        END
      END
      ThisWindow.Reset()
      Set_TruckTrailer()
    OF ?BUTTON_ClearFreightLiner
      ThisWindow.Update()
      CLEAR(L_TG:FreightLiner)
      CLEAR(L_TG:FreightLinerTTID)
      Set_TruckTrailer()
    OF ?CallLookup_Trailer
      ThisWindow.Update()
      TRU:Registration = L_TG:Trailer
      IF SELF.Run(5,SelectRecord) = RequestCompleted
        L_TG:Trailer = TRU:Registration
        L_TG:TrailerTTID = TRU:TTID
      END
      ThisWindow.Reset(1)
      Set_TruckTrailer()
    OF ?L_TG:Trailer
      IF L_TG:Trailer OR ?L_TG:Trailer{PROP:Req}
        TRU:Registration = L_TG:Trailer
        IF Access:TruckTrailer.TryFetch(TRU:Key_Registration)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            L_TG:Trailer = TRU:Registration
            L_TG:TrailerTTID = TRU:TTID
          ELSE
            CLEAR(L_TG:TrailerTTID)
            SELECT(?L_TG:Trailer)
            CYCLE
          END
        ELSE
          L_TG:TrailerTTID = TRU:TTID
        END
      END
      ThisWindow.Reset()
      Set_TruckTrailer()
    OF ?BUTTON_ClearTrailer
      ThisWindow.Update()
      CLEAR(L_TG:Trailer)
      CLEAR(L_TG:TrailerTTID)
      Set_TruckTrailer()
    OF ?CallLookup_SuperLink
      ThisWindow.Update()
      TRU:Registration = L_TG:SuperLink
      IF SELF.Run(5,SelectRecord) = RequestCompleted
        L_TG:SuperLink = TRU:Registration
        L_TG:SuperLinkTTID = TRU:TTID
      END
      ThisWindow.Reset(1)
      Set_TruckTrailer()
    OF ?L_TG:SuperLink
      IF L_TG:SuperLink OR ?L_TG:SuperLink{PROP:Req}
        TRU:Registration = L_TG:SuperLink
        IF Access:TruckTrailer.TryFetch(TRU:Key_Registration)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            L_TG:SuperLink = TRU:Registration
            L_TG:SuperLinkTTID = TRU:TTID
          ELSE
            CLEAR(L_TG:SuperLinkTTID)
            SELECT(?L_TG:SuperLink)
            CYCLE
          END
        ELSE
          L_TG:SuperLinkTTID = TRU:TTID
        END
      END
      ThisWindow.Reset()
      Set_TruckTrailer()
    OF ?BUTTON_ClearSuperLink
      ThisWindow.Update()
      CLEAR(L_TG:SuperLink)
      CLEAR(L_TG:SuperLinkTTID)
      Set_TruckTrailer()
    OF ?BUTTON_Apply_VCO_Changes
      ThisWindow.Update()
      Apply_Vehicle_Composition(TRUE)
    OF ?BUTTON_Cancel_VCO_Changes
      ThisWindow.Update()
      Apply_Vehicle_Composition(FALSE)
    OF ?Button_Prev
      ThisWindow.Update()
      DO Button_Prev
    OF ?Button_Next
      ThisWindow.Update()
      DO Button_Next
    OF ?PrintDetails
      ThisWindow.Update()
      Print_Manifest_Invoices(MAN:MID)
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
          IF LOC:Manifest_Date_Orig ~= MAN:CreatedDate
             LOC:Manifest_Date_Orig   = MAN:CreatedDate
      
             Updated_#                = FALSE
             IF ThisWindow.Request = ViewRecord
                IF Access:Manifest.TryFetch(MAN:PKey_MID) = LEVEL:Benign
                   MAN:CreatedDate    = LOC:Manifest_Date_Orig
                   !MAN:CreatedTime
      
                   IF Access:Manifest.Update() = LEVEL:Benign
                      Updated_#       = TRUE
                .  .
             ELSE
                Updated_#             = TRUE
             .
      
             IF Updated_# = TRUE
                ! Check for an Invoice - offer to update it if present
                ! Loop through Transporter Invoices for this Manifest
                CLEAR(INT:Record)
                INT:MID       = MAN:MID
                SET(INT:Fkey_MID, INT:Fkey_MID)
                LOOP
                   IF Access:_InvoiceTransporter.TryNext() ~= LEVEL:Benign
                      BREAK
                   .
                   IF INT:MID ~= MAN:MID
                      BREAK
                   .
      
                   IF INT:Manifest = TRUE OR (INT:ExtraInv = FALSE AND INT:CR_TIN = 0)
                      IF INT:CreatedDate ~= MAN:CreatedDate
                         CASE MESSAGE('The Manifest date has been changed.||A Transporter Invoice exists for this Manifest - TIN: ' & INT:TIN & '.  Would you like to update the date on this Invoice date as well?', 'Transporter Invoice', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
                         OF BUTTON:Yes
                            INT:CreatedDate     = MAN:CreatedDate
      
                            IF Access:_InvoiceTransporter.Update() ~= LEVEL:Benign
                .  .  .  .  .
      
                CASE MESSAGE('Would you like to also change the Invoice Date on all Debtor Invoices generated from this Manifest to be the new date - ' & FORMAT(MAN:CreatedDate, @d6b), 'Client Invoices', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                OF BUTTON:Yes
                   CLEAR(A_INV:Record)
                   A_INV:MID  = MAN:MID
                   SET(A_INV:FKey_MID, A_INV:FKey_MID)
                   LOOP
                      IF Access:InvoiceAlias.TryNext() ~= LEVEL:Benign
                         BREAK
                      .
                      IF A_INV:MID ~= MAN:MID
                         BREAK
                      .
      
                      IF A_INV:InvoiceDate = MAN:CreatedDate
                         A_INV:InvoiceDate   = MAN:CreatedDate
                         IF Access:InvoiceAlias.TryUpdate() ~= LEVEL:Benign
          .  .  .  .  .  .
      !! If we have no Veh Composition then we create a new one if we have Manifest TTIDs
      !IF MAN:VCID = 0
      !   IF MAN:TTID_FreightLiner <> 0
      !      ! Create new Veh comp.
      !      ! (p_TID, p_CompositionName, p_FreightLinerTTID, p_TrailerTTID, p_SuperLinkTTID, p_Capicity)
      !
      !      
      !!      ThisWindow.Update()
      !      MAN:VCID    = Add_Vehicle_Composition(MAN:TID, L_TG:CompositionName, L_TG:FreightLinerTTID, L_TG:TrailerTTID, L_TG:SuperLinkTTID, L_TG:Capacity)
      !!      ThisWindow.Reset()
      !      IF MAN:VCID ~= 0
      !         L_SG:CompositionName = L_TG:CompositionName
      !      .   
      !      
      !      db.Debugout('[Manifest]  MAN:VCID: ' & MAN:VCID & '         L_TG:CompositionName: ' & L_TG:CompositionName)
      !      
      !      IF MAN:VCID = 0
      !         QuickWindow{PROP:AcceptAll} = FALSE
      !         MESSAGE('Could not create a new Vehicle Composition.','Vehicle Composition',ICON:Hand)
      !         CYCLE
      !.  .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ! If we have no Veh Composition then we create a new one if we have Manifest TTIDs
  IF MAN:VCID = 0
     IF MAN:TTID_FreightLiner <> 0
        ! Create new Veh comp.
        ! (p_TID, p_CompositionName, p_FreightLinerTTID, p_TrailerTTID, p_SuperLinkTTID, p_Capicity)
  
        
  !      ThisWindow.Update()
        MAN:VCID    = Add_Vehicle_Composition(MAN:TID, L_TG:CompositionName, L_TG:FreightLinerTTID, L_TG:TrailerTTID, L_TG:SuperLinkTTID, L_TG:Capacity)
  !      ThisWindow.Reset()
        IF MAN:VCID ~= 0
           L_SG:CompositionName = L_TG:CompositionName
        .   
        
        db.Debugout('[Manifest]  MAN:VCID: ' & MAN:VCID & '         L_TG:CompositionName: ' & L_TG:CompositionName)
        
        IF MAN:VCID = 0
           QuickWindow{PROP:AcceptAll} = FALSE
           MESSAGE('Could not create a new Vehicle Composition.||Check that the New name is unique.','Vehicle Composition',ICON:Hand)
           
           SELECT(?TAB_Truck)
           POST(EVENT:Accepted, ?Button_Change_Vehicles)
           SELECT(?L_TG:CompositionName)
           CYCLE
  .  .  .
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      CASE EVENT()
      OF EVENT:User                               !0600h+3
         DO Print_Delivery_Note
      OF EVENT:User+1 OROF EVENT:User+2           !0600h+4
        DO Print_Invoice_for_Delivery
      OF EVENT:User+3
        Manifest_Emails_Setup(MAN:MID)                
      .
  
  !Take Sort Headers Events
  IF BRW4::SortHeader.TakeEvents()
     RETURN Level:Notify
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_SV:Prev_Cost
          GET(LOC:PreviousCosts, CHOICE(?L_SV:Prev_Cost))
          IF ~ERRORCODE()
             IF L_PCQ:Display = TRUE
                MESSAGE('The last entry is the Display entry.||Please use an entry above this one.', 'Previous Rates', ICON:Hand)
             ELSE
                IF MAN:Cost ~= 0.0
                   IF MAN:Rate ~= L_PCQ:Rate
                      CASE MESSAGE('Would you like to update the Manifest Cost specified (' & CLIP(LEFT(FORMAT(MAN:Cost,@n-12.2))) & ') with the selected rated cost (' & CLIP(LEFT(FORMAT(L_PCQ:Rate * L_SG:Tot_Weight,@n-12.2))) & ')?', 'Update Manifest Cost & Rate', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                      OF BUTTON:Yes
                         MAN:Cost     = L_PCQ:Rate * L_SG:Tot_Weight
                         MAN:Rate     = L_PCQ:Rate
                         DISPLAY
                   .  .
                ELSE
                   MAN:Cost           = L_PCQ:Rate * L_SG:Tot_Weight
                   MAN:Rate           = L_PCQ:Rate
                   DISPLAY
                .
      
                L_SV:Prev_Cost        = 'Rate ' & CLIP(LEFT(FORMAT(L_PCQ:Rate,@n12.2))) & ' - ' & FORMAT(L_PCQ:DepartDate, @d5) & ' - ' & CLIP(L_PCQ:TransporterName)
      
                L_PCQ:Display         = TRUE
                GET(LOC:PreviousCosts, L_PCQ:Display)
                IF ~ERRORCODE() AND L_PCQ:Display = TRUE
                   DELETE(LOC:PreviousCosts)
                .
      
                L_PCQ:Display         = TRUE
                L_PCQ:TransporterName = L_SV:Prev_Cost
                ADD(LOC:PreviousCosts)
      
                SELECT(?L_SV:Prev_Cost, RECORDS(LOC:PreviousCosts))
      
                DO Rate_Calc
             .
          ELSE
      !        message('error')
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          ! Set User accesses...
      
          User_Access_#   = Get_User_Access(GLO:UID, 'Manifests', 'Update_Manifest')       ! (p:UID, p:AppSection, p:Procedure)
      
          !   1   - Disable
          !   2   - Hide
          !   100 - Allow
          !   101 - Disallow
      
          IF User_Access_# = 0 OR User_Access_# = 100
             UNHIDE(?Button_ChangeTransporter)
          ELSE
             HIDE(?Button_ChangeTransporter)
          .
      
      
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW_ManLoads.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert_AddLoad
    SELF.ChangeControl=?Change_Loaded
  END


BRW_ManLoads.ResetFromView PROCEDURE

L_SG:No_DIs:Cnt      LONG                                  ! Count variable for browse totals
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:ManifestLoad.SetQuickScan(1)
  SELF.Reset
  IF SELF.UseMRP
     IF SELF.View{PROP:IPRequestCount} = 0
          SELF.View{PROP:IPRequestCount} = 60
     END
  END
  LOOP
    IF SELF.UseMRP
       IF SELF.View{PROP:IPRequestCount} = 0
            SELF.View{PROP:IPRequestCount} = 60
       END
    END
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      SETCURSOR()
      RETURN
    END
    SELF.SetQueueRecord
    IF (LOC:Last_DID ~= DEL:DID)
      L_SG:No_DIs:Cnt += 1
    END
    LOC:Last_DID    = DEL:DID
  
      !db.debugout('[Update_Manifest]  reset from view...')
  END
  SELF.View{PROP:IPRequestCount} = 0
  L_SG:No_DIs = L_SG:No_DIs:Cnt
  PARENT.ResetFromView
  Relate:ManifestLoad.SetQuickScan(0)
  SETCURSOR()


BRW_ManLoads.SetQueueRecord PROCEDURE

  CODE
      ! DELI:Units
      ! DELI:Type
      ! DELI:ContainerNo
      ! PACK:Packaging
      ! TRU:Registration
      ! VMM:MakeModel
      ! VMM:Capacity
  
      CASE DELI:Type
      OF 0                                ! Container
         LOC:Item_Description     = DELI:ContainerNo
      OF 1                                ! Loose
         LOC:Item_Description     = CLIP(PACK:Packaging)  & ' (total: units ' & DELI:Units & ', weight: ' & FORMAT(DELI:Weight, @n9.2) & ')'
      .
  
      L_IL:Weight                 = MALD:UnitsLoaded * (DELI:Weight / DELI:Units)
     
  
      ! ikb why are these not populated????  18 April
      !L_IL:Weight_Vol             =
      !L_IL:Total_Charge           =
  
      ! Returns comma delim TID list, need to look up transporters
      ! p:Option          0. = Number
      !                   1. = Total Cost
      !                   2. = Total Cost inc.
      !                   3. = TIDs
  
      CLEAR(L_IL:2nd_Transporter)
      CLEAR(L_IL:2nd_TransPrice)
      CLEAR(L_IL:2nd_Trans_Area)
  
      L_IL:2nd_Leg_TIDs           = Get_DelLegs_Info(DELI:DID, 3)
      LOOP
         IF CLIP(L_IL:2nd_Leg_TIDs) = ''
            BREAK
         .
  
         A_TRA:TID                = Get_1st_Element_From_Delim_Str(L_IL:2nd_Leg_TIDs, ',', TRUE)
  
         IF A_TRA:TID = 0         ! Allow for blank entries in comma list, like 1st entry
            CYCLE
         .
         IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) ~= LEVEL:Benign
            CLEAR(A_TRA:Record)
         .
  
         ! (p:Add, p:List, p:Delim, p:Option, p:Prefix)
         Add_To_List(A_TRA:TransporterName, L_IL:2nd_Transporter, ',')
      .
  
      L_IL:2nd_TransPrice         = Get_DelLegs_Info(DELI:DID, 2)
      !L_IL:2nd_Trans_Area         =
  
  
  
      !L_BG:Vehicle_Desc           = CLIP(VMM:MakeModel) & ' - ' & CLIP(TRU:Registration)  & ', Capacity: ' &  VMM:Capacity
      L_BG:Vehicle_Desc           = CLIP(TRU:Registration)  & ', Capacity: ' &  TRU:Capacity
  
  PARENT.SetQueueRecord
  


BRW_ManLoads.SetSort PROCEDURE(BYTE NewOrder,BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(NewOrder,Force)
  IF BRW4::LastSortOrder<>NewOrder THEN
     BRW4::SortHeader.ClearSort()
  END
  IF BRW4::LastSortOrder=0 THEN
     BRW4::LastSortOrder=NewOrder
     BRW4::SortHeader.LoadSort()
  END
  BRW4::LastSortOrder=NewOrder
  RETURN ReturnValue


BRW_ManLoads.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
      IF ReturnValue = Record:OK
         ! Check for problem records...
         IF LOC:Load_Item_Warned = FALSE AND MALD:MLDID = 0
            IF LOC:Load_Item_Warned_ID = MAL:MLID
               ! Same one, stop warnings
               LOC:Load_Item_Warned  = TRUE
            ELSE
               IF LOC:Load_Item_Warned_ID = 0
                  LOC:Load_Item_Warned_ID   = MAL:MLID
               .
  
               ! This record is not valid, offer to remove it if in Loading state only.
               IF MAN:State = 0
                  Add_Log('Manifest Items record appears to be incorrect - MAL:MLID: ' & MAL:MLID & '   MAL:MID: ' & MAL:MID,'Update_Manifest')
                  CASE MESSAGE('A Manifest Items record appears to be incorrect. It contains no Manifest Load Item ID.||Would you like to remove this entry now?', 'Manifest Items', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                  OF BUTTON:Yes
                     IF Access:ManifestLoad.Fetch(MAL:PKey_MLID) ~= LEVEL:Benign
                        MESSAGE('Could not fetch the Manifest Load record to remove it.', 'Manifest Load', ICON:Exclamation)
                     ELSE
                        IF Access:ManifestLoad.DeleteRecord(0) = LEVEL:Benign
                           Add_Log('Incorrect Man Item deleted - MAL:MLID: ' & MAL:MLID & '   MAL:MID: ' & MAL:MID,'Update_Manifest')        
                           ReturnValue    = Record:Filtered
                  .  .  .
               ELSE
                  Add_Log('Manifest Items record appears to be incorrect, BUT Manifest not in Loading state!!! - MAL:MLID: ' & MAL:MLID & '   MAL:MID: ' & MAL:MID,'Update_Manifest')
                  MESSAGE('A Manifest Items record appears to be incorrect. It contains no Manifest Load Item ID.||This Manifest is not in Loading State and cannot have this record removed.  You should seek assistance with this.', 'Manifest Items', ICON:Hand)
      .  .  .  .
  BRW4::RecordStatus=ReturnValue
  RETURN ReturnValue


BRW2.SetQueueRecord PROCEDURE

  CODE
      L_TIG:Total         = INT:Cost + INT:VAT
  
  PARENT.SetQueueRecord
  
  IF (INT:Broking = 1)
    SELF.Q.INT:Broking_Icon = 2                            ! Set icon from icon list
  ELSE
    SELF.Q.INT:Broking_Icon = 1                            ! Set icon from icon list
  END
  IF (INT:Manifest = 1)
    SELF.Q.INT:Manifest_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.INT:Manifest_Icon = 1                           ! Set icon from icon list
  END
  IF (INT:ExtraInv = 1)
    SELF.Q.INT:ExtraInv_Icon = 2                           ! Set icon from icon list
  ELSE
    SELF.Q.INT:ExtraInv_Icon = 1                           ! Set icon from icon list
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Prompt_Change, Resize:FixLeft+Resize:FixBottom, Resize:LockHeight) ! Override strategy for ?Prompt_Change
  SELF.SetStrategy(?Group_All_Top, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_All_Top
  SELF.SetStrategy(?Group_LoadedWeight, Resize:FixRight+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_LoadedWeight
  SELF.SetStrategy(?Group_Extra_Top_Left, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Extra_Top_Left
  SELF.SetStrategy(?Group_Bottom_DIs, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Bottom_DIs
  SELF.SetStrategy(?Group_Inv_Pay, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_Inv_Pay
  SELF.SetStrategy(?GROUP_Place_Trucks, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?GROUP_Place_Trucks
  SELF.SetStrategy(?GROUP_Trucks_Change, Resize:LockXPos+Resize:LockYPos, Resize:LockSize) ! Override strategy for ?GROUP_Trucks_Change
  SELF.SetStrategy(?Group_AboveTabTop, Resize:FixRight+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Group_AboveTabTop

Transporter_Rates               PROCEDURE(BYTE p:NotQuite)             !,LONG
R:Result        LONG
R:Result_Str    STRING(100)

R:BaseChg       STRING(200)
R:MinimiumLoad  STRING(200)
R:RatePer       STRING(200)

R:RateAbove     LIKE(MAN:Rate)          !DECIMAL(12.2)

R:Msg           STRING(1000)

R_Rate_G        GROUP,PRE(R)
PerRate            LIKE(MAN:Rate)
                .

R:Trans_J_VC    BYTE,STATIC             ! 1st itteration where Transporter, Journey and Veh. Comp. is set

    CODE
    LOC:Start  = clock()

    IF R:Trans_J_VC = FALSE AND (MAN:TID ~= 0 AND MAN:JID ~= 0 AND MAN:VCID ~= 0) AND p:NotQuite = FALSE
       R:Trans_J_VC = 2                 ! 1st fulled in all and quiet mode....
    .

    DO Get_Tot_Weight

    CLEAR(LOC:Rate_Group)
                                        ! (p:TID, p:JID, p:VCID, p:Setup, p:Setup_VCID, p:MinimiumLoad, p:BaseCharge, p:TRID, p:Local_Rate, p:Eff_Date)
    R_Rate_G            = Get_Transporter_Rate(MAN:TID, MAN:JID, MAN:VCID, L_RG:Setup, L_RG:Setup_VCID, L_RG:MinimiumLoad, L_RG:BaseCharge, L_RG:TRID,, MAN:CreatedDate)
    L_RG:PerRate        = R:PerRate

    IF L_RG:TRID > 0
       ! As we dont know what to do with the transporter rates well just warn the user
       IF MAN:Cost < L_RG:BaseCharge
          R:BaseChg     = 'Cost is less than Base Charge of ' & CLIP(LEFT(FORMAT(L_RG:BaseCharge,@n-12.2))) & '.'
          R:Msg         = CLIP(R:BaseChg)
       .
       IF L_SG:Tot_Weight < L_RG:MinimiumLoad
          R:MinimiumLoad = 'Load weight is less than Minimium Load of ' & CLIP(LEFT(FORMAT(L_RG:MinimiumLoad,@n-12.2))) & '.'
          IF CLIP(R:Msg) = ''
             R:Msg      = CLIP(R:MinimiumLoad)
          ELSE
             R:Msg      = CLIP(R:Msg) & '||' & CLIP(R:MinimiumLoad)
          .
       ELSE
          R:RateAbove   = MAN:Cost / (L_SG:Tot_Weight - L_RG:MinimiumLoad)
          IF R:RateAbove < L_RG:PerRate
             R:RatePer  = 'Maifest rate per kg (' & CLIP(LEFT(FORMAT(R:RateAbove,@n-12.2))) & ') above Minimum Load (' & CLIP(LEFT(FORMAT(L_RG:MinimiumLoad,@n-12.2))) & ') is less than specified Rate Per kg above of ' & CLIP(LEFT(FORMAT(L_RG:PerRate,@n-12.2))) & '.'

             IF CLIP(R:Msg) = ''
                R:Msg   = CLIP(R:RatePer)
             ELSE
                R:Msg   = CLIP(R:Msg) & '||' & CLIP(R:RatePer)
       .  .  .

       IF CLIP(R:Msg) ~= '' AND R:Trans_J_VC ~= 2
          IF MAN:Cost < L_RG:BaseCharge
             CASE MESSAGE('Transporter Rates are not satisfied.||' & CLIP(R:Msg) & '||Would you like to set the Cost to the Base Rate now?', 'Transporter Rates', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:Yes)
             OF BUTTON:Yes
                MAN:Cost    = L_RG:BaseCharge
             .
          ELSE
             IF L_RG:Setup = TRUE
                MESSAGE('Transporter Rates are not satisfied based on Setup Rate.||' & CLIP(R:Msg), 'Transporter Rates', ICON:Exclamation)
                R:Result    = -2
             ELSE
                MESSAGE('Transporter Rates are not satisfied.||' & CLIP(R:Msg), 'Transporter Rates', ICON:Exclamation)
                R:Result    = -3
       .  .  .
    ELSIF R:Trans_J_VC ~= 2
       R:Result         = -1
       MESSAGE('We have no applicable Transporter Rate specified.', 'Transporter Rates', ICON:Exclamation)
    .

    DISPLAY

    IF CLOCK() - LOC:Start > 0
       db.debugout('[Update_Manifest - Transporter_Rates]  time taken: ' & CLOCK() - LOC:Start)
    .


    R:Result_Str    = R:Result


    IF R:Trans_J_VC = 2
       R:Trans_J_VC     = TRUE
       IF R:Result < 0
       ELSE
          IF R:PerRate = 0.0
             R:Result_Str   = L_RG:BaseCharge   ! Always return base charge?
          .
          IF MAN:Rate = 0.0
             MAN:Rate       = R:PerRate
    .  .  .
    RETURN(R:Result_Str)
Change_Veh_Comp             PROCEDURE(ULONG p_New_VCID, ULONG p_Old_VCID, BYTE p:Override=0)         !,LONG
L_Return    LONG

L_TTID0     LIKE(VCO:TTID0)
L_TTID1     LIKE(VCO:TTID1)
L_TTID2     LIKE(VCO:TTID2)
L_TTID3     LIKE(VCO:TTID3)

L_Switch    BYTE

   CODE
   ! Check that the new composition at least matches the old one - unless overridden
   ! MAN:VCID ~= L_SG:VCID

   ! Load new VC info.     
   VCO:VCID    = p_New_VCID                       !MAN:VCID
   IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
      L_TTID0  = VCO:TTID0
      L_TTID1  = VCO:TTID1
      L_TTID2  = VCO:TTID2
      L_TTID3  = VCO:TTID3
   .

   VCO:VCID    = p_Old_VCID                       !L_SG:VCID           ! 16/feb/14 - replaced 
   IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign    .

   ! Check trailers
   TRU:TTID    = L_TTID0
   IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign    .
   A_TRU:TTID  = VCO:TTID0
   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign    .
   IF TRU:Capacity ~= A_TRU:Capacity
      L_Return -= 1
   .

   TRU:TTID    = L_TTID1
   IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign    .
   A_TRU:TTID  = VCO:TTID1
   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign    .
   IF TRU:Capacity ~= A_TRU:Capacity
      L_Return -= 1
   .

   TRU:TTID    = L_TTID2
   IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign    .
   A_TRU:TTID  = VCO:TTID2
   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign    .
   IF TRU:Capacity ~= A_TRU:Capacity
      L_Return -= 1
   .

   ! No idea what a 3rd trailer could be?   Checked my testing DB 16 feb 14, none exist in it.
   TRU:TTID    = L_TTID3
   IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign    .
   A_TRU:TTID  = VCO:TTID3
   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign    .
   IF TRU:Capacity ~= A_TRU:Capacity
      L_Return -= 1
   .


   ! Switch if ok
   IF L_Return >= 0 OR p:Override = TRUE
      Move_VehComp_Cargo(p_New_VCID, p_Old_VCID)         !,LONG
   .
   RETURN(L_Return)
Move_VehComp_Cargo             PROCEDURE(ULONG p_New_VCID, ULONG p_Old_VCID)         !,LONG

! Returns 0 if all well, 1 if some cargo was moved to a different positioned truck, because there weren't matching nos of trucks      
L_Return    LONG(0)

L_TTID0     LIKE(VCO:TTID0)
L_TTID1     LIKE(VCO:TTID1)
L_TTID2     LIKE(VCO:TTID2)
L_TTID3     LIKE(VCO:TTID3)

L_LastTTID  LIKE(VCO:TTID3)
         
L_Switch    BYTE

L_UnknownTTID  LIKE(VCO:TTID2)

CODE
   ! Expects MAN:VCID to be the new one now

   db.Debugout('Move -  new p_New_VCID: ' &  p_New_VCID & '     Old p_Old_VCID: ' & p_Old_VCID)
   
   ! Load new VC info. into locals
   VCO:VCID    = p_New_VCID
   IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
      L_TTID0  = VCO:TTID0
      L_TTID1  = VCO:TTID1
      L_TTID2  = VCO:TTID2
      L_TTID3  = VCO:TTID3
   .

   ! Load old VC info
   VCO:VCID    = p_Old_VCID                       !L_SG:VCID           ! 16/feb/14 - replaced 
   IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign    .

   ! Loops through all Manifest Loads
   CLEAR(MAL:Record,-1)
   MAL:MID  = MAN:MID
   SET(MAL:FKey_MID,MAL:FKey_MID)
   LOOP
      IF Access:ManifestLoad.TryNext() ~= LEVEL:Benign
         BREAK
      .
      IF MAL:MID ~= MAN:MID
         BREAK
      .
      
      db.Debugout('Move -  old MAL:TTID ' &  MAL:TTID   & '          VCO:TTID0-2: ' & VCO:TTID0 & ', ' & VCO:TTID1 & ', ' & VCO:TTID2)
      
      L_Switch   = TRUE

      CASE MAL:TTID
      OF VCO:TTID0                                 ! If this is our 1st veh. from the old composition used here then
         MAL:TTID   = L_TTID0                      ! Replace with our first new vehicle from the new composition
      OF VCO:TTID1
         MAL:TTID   = L_TTID1
      OF VCO:TTID2
         MAL:TTID   = L_TTID2
      OF VCO:TTID3
         MAL:TTID   = L_TTID3
      ELSE
         L_UnknownTTID    = MAL:TTID
         L_Switch  = FALSE
      .

      IF L_Switch = TRUE
         db.Debugout('Move -  new MAL:TTID ' &  MAL:TTID)
         
         IF MAL:TTID <> 0
            L_LastTTID  = MAL:TTID
            L_Return    = 1
         .
            
         ! Potentially we have extra vehicles on the original Manifest, which we don't have a match for on the new VCO
         ! In such cases the IDs will be 0, if they are then use the previous veh. ID
         IF MAL:TTID = 0
            MAL:TTID    = L_LastTTID
         .  

         IF Access:ManifestLoad.TryUpdate() ~= LEVEL:Benign
   .  .  .  

   IF L_UnknownTTID <> 0
      MESSAGE('There is some cargo that is not on a Truck/Trailer from the previous Vehicle Composition and hence wont be moved to a new Truck/Trailer.' | 
         & '||You can remove this cargo and re-load it onto a suitable Truck/Trailer using the "Change Loaded" button.||A Truck/Trailer involved is ID ' & L_UnknownTTID, |
            'Move Cargo', ICON:Hand)
   .
         
   RETURN(L_Return)
!Change_Veh_Comp             **********************  changed from this 6 March 14
!Change_Veh_Comp             PROCEDURE(BYTE p:Override=0)         !,LONG
!L_Return    LONG
!
!L_TTID0     LIKE(VCO:TTID0)
!L_TTID1     LIKE(VCO:TTID1)
!L_TTID2     LIKE(VCO:TTID2)
!L_TTID3     LIKE(VCO:TTID3)
!
!L_Switch    BYTE
!
!    CODE
!    ! Check that the new composition at least matches the old one - unless overridden
!    ! MAN:VCID ~= L_SG:VCID
!
!    ! Load new VC info.     
!    VCO:VCID    = MAN:VCID
!    IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
!       L_TTID0  = VCO:TTID0
!       L_TTID1  = VCO:TTID1
!       L_TTID2  = VCO:TTID2
!       L_TTID3  = VCO:TTID3
!    .
!
!!    VCO:VCID    = L_SG:VCID           ! 16/feb/14 - replaced 
!!    IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
!!    .
!
!    ! Check trailers
!    TRU:TTID    = L_TTID0
!    IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
!    .
!    A_TRU:TTID  = L_TG:FreightLinerTTID         !VCO:TTID0
!    IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!    .
!    IF TRU:Capacity ~= A_TRU:Capacity
!       L_Return -= 1
!    .
!
!    TRU:TTID    = L_TTID1
!    IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
!    .
!    A_TRU:TTID  = L_TG:TrailerTTID              !VCO:TTID1
!    IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!    .
!    IF TRU:Capacity ~= A_TRU:Capacity
!       L_Return -= 1
!    .
!
!    TRU:TTID    = L_TTID2
!    IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
!    .
!    A_TRU:TTID  = L_TG:SuperLinkTTID            !VCO:TTID2
!    IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!    .
!    IF TRU:Capacity ~= A_TRU:Capacity
!       L_Return -= 1
!    .
!
!    ! No idea what a 3rd trailer could be?   Checked my testing DB 16 feb 14, none exist in it.
!    TRU:TTID    = L_TTID3
!    IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
!    .
!    A_TRU:TTID  = VCO:TTID3
!    IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = LEVEL:Benign
!    .
!    IF TRU:Capacity ~= A_TRU:Capacity
!       L_Return -= 1
!    .
!
!
!
!    ! Switch if ok
!    IF L_Return >= 0 OR p:Override = TRUE
!       CLEAR(MAL:Record,-1)
!       MAL:MID  = MAN:MID
!       SET(MAL:FKey_MID,MAL:FKey_MID)
!       LOOP
!          IF Access:ManifestLoad.TryNext() ~= LEVEL:Benign
!             BREAK
!          .
!          IF MAL:MID ~= MAN:MID
!             BREAK
!          .
!
!          L_Switch  = FALSE
!
!          IF MAL:TTID = L_TTID0
!             MAL:TTID   = L_TG:FreightLinerTTID
!             L_Switch   = TRUE
!          ELSIF MAL:TTID = L_TTID1
!             MAL:TTID   = L_TG:TrailerTTID
!             L_Switch   = TRUE
!          ELSIF MAL:TTID = L_TTID2
!             MAL:TTID   = L_TG:SuperLinkTTID
!             L_Switch   = TRUE
!          ELSIF MAL:TTID = L_TTID3
!             MAL:TTID   = 0        !????? VCO:TTID3   16 feb 14, change means we dont have any option for this
!             L_Switch   = TRUE
!          .
!
!          IF L_Switch = TRUE
!             ! If user wants to override non-matched vechicles then we need to make sure we are not saving a 0 truck ID
!             IF p:Override = TRUE
!                IF MAL:TTID = 0
!                   IF L_TG:TrailerTTID ~= 0                 !VCO:TTID1 ~= 0
!                      MAL:TTID = L_TG:TrailerTTID           !VCO:TTID1
!                   ELSE
!                      MAL:TTID = L_TG:FreightLinerTTID      !!VCO:TTID0
!             .  .  .
!
!             IF Access:ManifestLoad.TryUpdate() ~= LEVEL:Benign
!    .  .  .  .
!    RETURN(L_Return)
State_Class.New_TID                 PROCEDURE(ULONG p:TID)      !,LONG

Ret     LONG

    CODE
    IF p:TID ~= SELF.c_TID
       IF SELF.c_TID = 0
          Ret       = 1
       ELSE
          Ret       = 2
       .
       SELF.c_TID     = p:TID
    .
    RETURN(Ret)
! ----------------------------------------------------------------------------------------
Apply_Vehicle_Composition        PROCEDURE(LONG p_Opt)
! Opt is Apply 1 or Cancel 0

Msg_           STRING(150)
R_Old_VCID     ULONG

CODE
   R_Old_VCID     = L_SG:VCID
   ! TODO
   ! Here  apply changes made on Truck Trailer to actual composition info used here, on additionals tab.
   ! And save the new composition?
   ! Check weights of loaded stuff is

   IF MAN:TTID_FreightLiner = L_TG:FreightLinerTTID AND MAN:TTID_Trailer = L_TG:TrailerTTID     |         
                  AND MAN:TTID_SuperLink = L_TG:SuperLinkTTID
      ! If we haven't changed any vehicles then Apply should do nothing.  Not even change the composition name

      ! Do nothing
   ELSE
      IF p_Opt = TRUE
         !TODO - check that capactiy of new VCO would be ok, warn user if NOT
         !if all ok, confirm with user that new VCO will be created now, and advise of any cargo that will
         !be moved from old Trucks
         !Then if all ok, create new VCO and move stuff using procedure used before         
         IF Get_ManLoad_Info(MAN:MID, 1) > 0          ! We have cargo loaded
            IF L_TG:Capacity < L_SG:Capacity 
               Msg_  = '||The Capacity of the selected replacement Vehicles is lower than the original.  If you go ahead some vehicles may be overloaded.'
         .  .         
            
         CASE MESSAGE('This will create a New Composition named ' & CLIP(L_TG:CompositionName) & '.' & CLIP(Msg_) & | 
                     '|||Would you like to go ahead?' ,'Composition Change',ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
         OF BUTTON:Yes
            !L_SG:VC_Capacity        = VCO:Capacity       ! TODO - capacity... 
            ! Set the Manifest & local TT info
            !L_SG:VCID               = 0                   ! the one we loaded is no longer applicable, BUT we use it as OLD VCO 
            !MAN:VCID                = 0
            !L_SG:CompositionName    = L_TG:CompositionName
            
            ! If we have no Veh Composition then we create a new one if we have Manifest TTIDs
            IF MAN:TTID_FreightLiner <> 0
               ! Create new Veh comp.
               ! (p_TID, p_CompositionName, p_FreightLinerTTID, p_TrailerTTID, p_SuperLinkTTID, p_Capicity)

         !      ThisWindow.Update()

               
               IF L_TG:FreightLinerTTID = 0
                  L_TG:FreightLinerTTID   = L_TG:TrailerTTID
                  L_TG:TrailerTTID        = 0
               .
               IF L_TG:TrailerTTID = 0
                  L_TG:TrailerTTID        = L_TG:SuperLinkTTID
                  L_TG:SuperLinkTTID      = 0
               .
                  
               MAN:VCID    = Add_Vehicle_Composition(MAN:TID, L_TG:CompositionName, L_TG:FreightLinerTTID, L_TG:TrailerTTID, L_TG:SuperLinkTTID, L_TG:Capacity)
               db.Debugout('[Manifest]  MAN:VCID: ' & MAN:VCID & '         L_TG:CompositionName: ' & L_TG:CompositionName)

         !      ThisWindow.Reset()
               IF MAN:VCID > 0
                  MAN:TTID_FreightLiner   = L_TG:FreightLinerTTID
                  MAN:TTID_Trailer        = L_TG:TrailerTTID
                  MAN:TTID_SuperLink      = L_TG:SuperLinkTTID

                  L_SG:CompositionName    = L_TG:CompositionName
                  L_SG:VCID               = MAN:VCID

                  ! Move cargo to new trucks...                  
                  Move_VehComp_Cargo(MAN:VCID, R_Old_VCID)         !,LONG

                  DO Driver_Check            

                  DO Set_Composition_New           ! Also sets Driver fields, so must come after change to this above

                  !DO Veh_Licensing_Reminders     TODO - removing for now!  26/02/14            
               ELSE                                !IF MAN:VCID <= 0
                  MAN:VCID                = L_SG:VCID
                  
                  QuickWindow{PROP:AcceptAll} = FALSE
                  MESSAGE('Could not create a new Vehicle Composition.||Check that the New name is unique.','Vehicle Composition',ICON:Hand)
                  
                  SELECT(?TAB_Truck)
                  POST(EVENT:Accepted, ?Button_Change_Vehicles)
                  SELECT(?L_TG:CompositionName)                  
            .  .               
         OF BUTTON:NO
            p_Opt    = FALSE
      .  .
            
      IF p_Opt = FALSE
         CLEAR(LOC:Truck_Group)
         L_TG:FreightLinerTTID      = MAN:TTID_FreightLiner
         L_TG:TrailerTTID           = MAN:TTID_Trailer  
         L_TG:SuperLinkTTID         = MAN:TTID_SuperLink          
         Set_TruckTrailer(L_SG:CompositionName, L_SG:VCID)
   .  .
      
   ! Re-enable / hide 
   HIDE(?GROUP_Trucks_Change)
   ENABLE(?OK)
   ENABLE(?Tab_Items)
   ENABLE(?Tab_Extra)
      
   DO Get_Tot_Weight
      
   DISPLAY
   RETURN
Set_TruckTrailer                          PROCEDURE(<STRING p_CompositionName>, ULONG p_VCID=0)    ! Expects L_TG TTIDs set
!   L_TG:FreightLinerTTID   = MAN:TTID_FreightLiner
!   L_TG:TrailerTTID        = MAN:TTID_Trailer
!   L_TG:SuperLinkTTID      = MAN:TTID_SuperLink

CODE
   CLEAR(L_TG:Capacity)
   ?PROMPT_Freightliner{PROP:Text}  = ''
   ?PROMPT_SuperLink{PROP:Text}     = ''
   ?PROMPT_Trailer{PROP:Text}       = ''
   
   IF OMITTED(1) OR p_VCID = 0
      ! We need to construct one then, std is 20 chars transporter + reg no. of horse, but do we want std?  We do it later
   ELSE      
      L_TG:CompositionName = p_CompositionName           ! Assumes screen group loaded already
   .   
   L_TG:VCID               = p_VCID
   
   db.Debugout('Set_TruckTrailer  - L_TG:VCID: ' & L_TG:VCID) 
      
   A_TRU:TTID              = L_TG:FreightLinerTTID
   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = Level:Benign
      L_TG:FreightLiner    = A_TRU:Registration
      L_TG:Capacity        = A_TRU:Capacity
      ?PROMPT_Freightliner{PROP:Text}  = A_TRU:Capacity & ' kgs, ' & Get_TruckTrailer_Info(L_TG:FreightLinerTTID, 5)

      IF OMITTED(1) OR p_VCID = 0
         ! We need to construct one then, std is 20 chars transporter + reg no. of horse, but do we want std?
         A_TRA:TID         = A_TRU:TID
         IF Access:TransporterAlias.TryFetch(A_TRA:PKey_TID) = Level:Benign
            L_TG:CompositionName  = 'm.' & CLIP(A_TRA:TransporterName) & ' - ' & A_TRU:Registration
   .  .  .
   
   A_TRU:TTID              = L_TG:TrailerTTID        
   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = Level:Benign
      L_TG:Trailer         = A_TRU:Registration
      L_TG:Capacity       += A_TRU:Capacity
      ?PROMPT_Trailer{PROP:Text}       = A_TRU:Capacity & ' kgs, ' & Get_TruckTrailer_Info(L_TG:TrailerTTID, 5)
   .
      
   A_TRU:TTID              = L_TG:SuperLinkTTID
   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = Level:Benign
      L_TG:SuperLink       = A_TRU:Registration
      L_TG:Capacity       += A_TRU:Capacity
      ?PROMPT_SuperLink{PROP:Text}     = A_TRU:Capacity & ' kgs, ' & Get_TruckTrailer_Info(L_TG:SuperLinkTTID, 5)
   .
   
   ! (p:TTID, p:Type, p:Effective_Date, p:LicenseExpiryDate)
   ! (ULONG, BYTE=0, LONG=0, <*LONG>),STRING
   !
   ! p:Type    0 - Expiry - Message if less than Settings expiry, otherwise nothing
   !           1 - Expiry - CSV info if less than Settings expiry, otherwise nothing
   !           2 - Expiry - Message
   !           3 - Expiry - CSV info
   !           4 - LicenseInfo
   !           5 - Make & Model
   !           6 - Registration
   !           7 - Capacity
   !
   ! p:Effective_Date
   !           0 - Today
   !           x - Passed date
   !
   ! [Usage]
   DISPLAY         
   RETURN
   
Veh_Composition_New             PROCEDURE(ULONG p_NewVCOSelected=0)            ! Uses Composition record to populate TTIDs
   CODE
   ! When setting up info for a Vehicle composition, so on entry to form or on new one selected
   LOC:Start  = CLOCK()

   ! Implementing Rules:   
   ! If we have a new VCO then set all local TT fields to this new VCO
   ! If VCO same as known, checked here, then make no change
   
   ! L_SG:CompositionName is the screen field for the composition the user selects, we want to show something here

   db.Debugout('L_TG:VCID <> MAN:VCID:  ' & L_TG:VCID & ' <> ' & MAN:VCID   & '        L_SG:VCID: ' & L_SG:VCID & '  p_NewVCOSelected: ' & p_NewVCOSelected)
   
   IF (L_TG:VCID <> MAN:VCID) OR p_NewVCOSelected = TRUE                     ! It's zero or new one selected
      VCO:VCID                   = MAN:VCID
      IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = LEVEL:Benign
         IF CLIP(L_SG:Original_VehComp_Tip) = ''
            L_SG:Original_VehComp_Tip         = L_SG:CompositionName{PROP:Tip}
         .
         L_SG:CompositionName{PROP:Tip}       = CLIP(L_SG:Original_VehComp_Tip) & '<13,10><13,10>' & CLIP(L_SG:CompositionName)         

         L_SG:VC_Capacity        = VCO:Capacity       ! TODO - capacity... 
            
         ! Set the Manifest & local TT info
         MAN:TTID_FreightLiner   = VCO:TTID0
         MAN:TTID_Trailer        = VCO:TTID1
         MAN:TTID_SuperLink      = VCO:TTID2
         
         IF p_NewVCOSelected = TRUE            ! We have selected a new Composition, we will set the Driver from it.
            A_TRU:TTID  = MAN:TTID_FreightLiner
            IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = Level:Benign
               MAN:DRID    = A_TRU:DRID
         .  .
   db.Debugout('A_TRU:DRID: ' & A_TRU:DRID)
         
         DO Set_Composition_New        

         DO Driver_Check            
           
         !DO Veh_Licensing_Reminders     TODO - removing for now!  26/02/14   *******************************************
         DISPLAY
       ELSE
          ENABLE(?Button_Save)
          DISABLE(?Insert_AddLoad)
          DISABLE(?Change_Loaded)
          !DISABLE(?Delete:5)
   .  .
      
   IF CLOCK() - LOC:Start > 0
      db.debugout('[Update_Manifest - Veh_Composition_New]  time taken: ' & CLOCK() - LOC:Start)
   .
   RETURN
BRW4::SortHeader.QueueResorted       PROCEDURE(STRING pString)
  CODE
    IF pString = ''
       BRW_ManLoads.RestoreSort()
       BRW_ManLoads.ResetSort(True)
    ELSE
       BRW_ManLoads.ReplaceSort(pString)
    END
