

   MEMBER('WorTrnis.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('WORTRNIS006.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! -----   ***    Tag Class
!!! </summary>
Update_ManifestLoad PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:DI_No_Locator    ULONG                                 ! Delivery Instruction Number
LOC:Trucks_Trailers_Q QUEUE,PRE(L_TTQ)                     ! 
Description          STRING(100)                           ! 
Capacity             DECIMAL(6)                            ! In Kgs
Remaining_Capacity   DECIMAL(6)                            ! In Kgs
Loaded_Capacity      DECIMAL(6)                            ! In Kgs
TTID                 ULONG                                 ! Track or Trailer ID
                     END                                   ! 
LOC:Screen_Vars      GROUP,PRE(L_SV)                       ! 
Reg_Make_Model       STRING(100)                           ! Registration & Make / Model
Capacity             DECIMAL(6)                            ! In Kgs
No_Items             USHORT                                ! Number of units
Reload_ManItems      ULONG                                 ! 
Reload_Deliveries    ULONG                                 ! 
Last_DIID_Set        ULONG                                 ! 
VC_Capacity          DECIMAL(7)                            ! Total capacity not to exceed this amount
Floor                STRING(35)                            ! Floor Name
                     END                                   ! 
LOC:Item_Desc        STRING(150)                           ! 
LOC:Config_Vars      GROUP,PRE(L_CV)                       ! 
FID                  ULONG                                 ! Floor ID
DID                  ULONG                                 ! Delivery ID
DIID                 ULONG                                 ! Delivery Item ID
DID_Loaded_Del       ULONG                                 ! DID of selected loaded Delivery Item
                     END                                   ! 
L_DG:Terms           STRING(20)                            ! Terms
L_MG:Manifested_Item_Desc STRING(150)                      ! 
L_MG:Manifested_Weight DECIMAL(8,2)                        ! In kg's
L_IG:Item_Desc       STRING(150)                           ! 
L_IG:Item_Weight     DECIMAL(8,2)                          ! In kg's
L_IG:Item_Units      USHORT                                ! Number of units
Result               LONG                                  ! 
LOC:Inserted_Changed BYTE                                  ! Were Items Inserted or Changed
LOC:Cancelled        BYTE                                  ! 
LOC:ManLoadDel_Q     QUEUE,PRE(L_MLD)                      ! 
DID                  ULONG                                 ! Delivery ID
                     END                                   ! 
L_TG:Transporter_Group GROUP,PRE(L_TG)                     ! 
VCID                 ULONG                                 ! Vehicle Composition ID
TID                  ULONG                                 ! Transporter ID
Broking              BYTE(1)                               ! This is a broking transporter
DID                  ULONG                                 ! Delivery ID
                     END                                   ! 
LOC:Deliveries_Q     QUEUE,PRE(L_DQ)                       ! For performance pre load here and then use this in browse
DID                  ULONG                                 ! Delivery ID
                     END                                   ! 
BRW2::View:Browse    VIEW(ManifestLoadDeliveries)
                       PROJECT(MALD:UnitsLoaded)
                       PROJECT(MALD:DIID)
                       PROJECT(MALD:MLDID)
                       PROJECT(MALD:MLID)
                       JOIN(DELI:PKey_DIID,MALD:DIID)
                         PROJECT(DELI:ItemNo)
                         PROJECT(DELI:VolumetricWeight)
                         PROJECT(DELI:VolumetricRatio)
                         PROJECT(DELI:ContainerNo)
                         PROJECT(DELI:Weight)
                         PROJECT(DELI:Units)
                         PROJECT(DELI:DIID)
                         PROJECT(DELI:PTID)
                         PROJECT(DELI:DID)
                         PROJECT(DELI:CMID)
                         JOIN(PACK:PKey_PTID,DELI:PTID)
                           PROJECT(PACK:Packaging)
                           PROJECT(PACK:PTID)
                         END
                         JOIN(DEL:PKey_DID,DELI:DID)
                           PROJECT(DEL:DINo)
                           PROJECT(DEL:DID)
                           PROJECT(DEL:CID)
                           JOIN(CLI:PKey_CID,DEL:CID)
                             PROJECT(CLI:ClientName)
                             PROJECT(CLI:CID)
                           END
                         END
                         JOIN(COM:PKey_CMID,DELI:CMID)
                           PROJECT(COM:Commodity)
                           PROJECT(COM:CMID)
                         END
                       END
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse_Loaded_Del_Items
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
L_MG:Manifested_Item_Desc LIKE(L_MG:Manifested_Item_Desc) !List box control field - type derived from local data
MALD:UnitsLoaded       LIKE(MALD:UnitsLoaded)         !List box control field - type derived from field
L_MG:Manifested_Weight LIKE(L_MG:Manifested_Weight)   !List box control field - type derived from local data
DELI:VolumetricWeight  LIKE(DELI:VolumetricWeight)    !List box control field - type derived from field
DELI:VolumetricRatio   LIKE(DELI:VolumetricRatio)     !List box control field - type derived from field
MALD:DIID              LIKE(MALD:DIID)                !List box control field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !Browse hot field - type derived from field
DELI:Weight            LIKE(DELI:Weight)              !Browse hot field - type derived from field
DELI:Units             LIKE(DELI:Units)               !Browse hot field - type derived from field
PACK:Packaging         LIKE(PACK:Packaging)           !Browse hot field - type derived from field
MALD:MLDID             LIKE(MALD:MLDID)               !Primary key field - type derived from field
MALD:MLID              LIKE(MALD:MLID)                !Browse key field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Related join file key field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Related join file key field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !Related join file key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(Deliveries)
                       PROJECT(DEL:DINo)
                       PROJECT(DEL:ClientReference)
                       PROJECT(DEL:Insure)
                       PROJECT(DEL:DIDate)
                       PROJECT(DEL:DID)
                       PROJECT(DEL:Manifested)
                       PROJECT(DEL:MultipleManifestsAllowed)
                       PROJECT(DEL:FID)
                       PROJECT(DEL:SID)
                       PROJECT(DEL:CRTID)
                       PROJECT(DEL:JID)
                       PROJECT(DEL:CID)
                       JOIN(SERI:PKey_SID,DEL:SID)
                         PROJECT(SERI:ServiceRequirement)
                         PROJECT(SERI:SID)
                       END
                       JOIN(CRT:PKey_CRTID,DEL:CRTID)
                         PROJECT(CRT:ClientRateType)
                         PROJECT(CRT:CRTID)
                       END
                       JOIN(JOU:PKey_JID,DEL:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:JID)
                       END
                       JOIN(CLI:PKey_CID,DEL:CID)
                         PROJECT(CLI:ClientName)
                         PROJECT(CLI:CID)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List_Deliveries
DEL:DINo               LIKE(DEL:DINo)                 !List box control field - type derived from field
DEL:DINo_Style         LONG                           !Field style
CLI:ClientName         LIKE(CLI:ClientName)           !List box control field - type derived from field
CLI:ClientName_Style   LONG                           !Field style
DEL:ClientReference    LIKE(DEL:ClientReference)      !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
SERI:ServiceRequirement LIKE(SERI:ServiceRequirement) !List box control field - type derived from field
CRT:ClientRateType     LIKE(CRT:ClientRateType)       !List box control field - type derived from field
L_DG:Terms             LIKE(L_DG:Terms)               !List box control field - type derived from local data
DEL:Insure             LIKE(DEL:Insure)               !List box control field - type derived from field
DEL:Insure_Icon        LONG                           !Entry's icon ID
DEL:DIDate             LIKE(DEL:DIDate)               !List box control field - type derived from field
DEL:DID                LIKE(DEL:DID)                  !List box control field - type derived from field
DEL:Manifested         LIKE(DEL:Manifested)           !Browse hot field - type derived from field
DEL:MultipleManifestsAllowed LIKE(DEL:MultipleManifestsAllowed) !Browse hot field - type derived from field
DEL:FID                LIKE(DEL:FID)                  !Browse hot field - type derived from field
SERI:SID               LIKE(SERI:SID)                 !Related join file key field - type derived from field
CRT:CRTID              LIKE(CRT:CRTID)                !Related join file key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
CLI:CID                LIKE(CLI:CID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB10::View:FileDrop VIEW(Floors)
                       PROJECT(FLO:Floor)
                       PROJECT(FLO:FBNFloor)
                       PROJECT(FLO:FID)
                     END
FDB11::View:FileDrop VIEW(DeliveryItems)
                       PROJECT(DELI:Units)
                       PROJECT(DELI:ItemNo)
                       PROJECT(DELI:Type)
                       PROJECT(DELI:ContainerNo)
                       PROJECT(DELI:VolumetricWeight)
                       PROJECT(DELI:Weight)
                       PROJECT(DELI:DIID)
                       PROJECT(DELI:PTID)
                       PROJECT(DELI:CMID)
                       JOIN(PACK:PKey_PTID,DELI:PTID)
                         PROJECT(PACK:Packaging)
                         PROJECT(PACK:PTID)
                       END
                       JOIN(COM:PKey_CMID,DELI:CMID)
                         PROJECT(COM:Commodity)
                         PROJECT(COM:CMID)
                       END
                     END
Queue:FileDrop:1     QUEUE                            !
FLO:Floor              LIKE(FLO:Floor)                !List box control field - type derived from field
FLO:FBNFloor           LIKE(FLO:FBNFloor)             !List box control field - type derived from field
FLO:FBNFloor_Icon      LONG                           !Entry's icon ID
FLO:FID                LIKE(FLO:FID)                  !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDrop:2     QUEUE                            !
COM:Commodity          LIKE(COM:Commodity)            !List box control field - type derived from field
L_IG:Item_Desc         LIKE(L_IG:Item_Desc)           !List box control field - type derived from local data
L_IG:Item_Units        LIKE(L_IG:Item_Units)          !List box control field - type derived from local data
L_IG:Item_Weight       LIKE(L_IG:Item_Weight)         !List box control field - type derived from local data
DELI:Units             LIKE(DELI:Units)               !List box control field - type derived from field
DELI:ItemNo            LIKE(DELI:ItemNo)              !List box control field - type derived from field
DELI:Type              LIKE(DELI:Type)                !Browse hot field - type derived from field
DELI:ContainerNo       LIKE(DELI:ContainerNo)         !Browse hot field - type derived from field
PACK:Packaging         LIKE(PACK:Packaging)           !Browse hot field - type derived from field
DELI:VolumetricWeight  LIKE(DELI:VolumetricWeight)    !Browse hot field - type derived from field
DELI:Weight            LIKE(DELI:Weight)              !Browse hot field - type derived from field
DEL:DINo               LIKE(DEL:DINo)                 !Browse hot field - type derived from field
PACK:PTID              LIKE(PACK:PTID)                !Browse hot field - type derived from field
COM:CMID               LIKE(COM:CMID)                 !Browse hot field - type derived from field
DELI:DIID              LIKE(DELI:DIID)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::MAL:Record  LIKE(MAL:RECORD),THREAD
QuickWindow          WINDOW('Form Manifest Load'),AT(,,451,324),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,IMM,MDI,HLP('UpdateManifestLoad'),SYSTEM
                       SHEET,AT(4,4,443,301),USE(?CurrentTab),WIZARD
                         TAB('&1) General'),USE(?Tab2)
                           GROUP,AT(8,160,435,26),USE(?Group_LoadItems),TRN
                             PROMPT('Delivery Items:'),AT(8,160),USE(?Prompt3:3),TRN
                             LIST,AT(84,160,131,10),USE(L_IG:Item_Desc),HVSCROLL,DROP(15,300),FORMAT('60L(2)|M~Commo' & |
  'dity~@s35@76L(2)|M~Pack. / Container No.~@s150@36R(2)|M~Units Left~L@n6@44R(2)|M~Wei' & |
  'ght Left~L@n-11.2@40R(2)|M~Total Units~L@n6@24R(2)|M~Item No.~L@n6@'),FROM(Queue:FileDrop:2)
                             PROMPT('Units to Load:'),AT(320,160),USE(?L_SV:No_Items:Prompt),TRN
                             SPIN(@n6),AT(390,160,50,10),USE(L_SV:No_Items),RIGHT(1),HVSCROLL,MSG('Number of units'),STEP(1), |
  TIP('Number of units')
                             BUTTON('Load All Items'),AT(240,174,59,12),USE(?Button_AllItems),TIP('On the selected delivery')
                             BUTTON('Load Max'),AT(310,174,59,12),USE(?Button_AllItems:2),TIP('On the selected deliv' & |
  'ery load maximium number of items')
                             BUTTON('Load Items'),AT(380,174,59,12),USE(?Button_ThisItem),TIP('Load the item selecte' & |
  'd in Delivery Items with the Units to Load shown')
                           END
                           PROMPT('Capacity:'),AT(307,10),USE(?Capacity:Prompt),TRN
                           ENTRY(@n-8.0),AT(392,10,50,10),USE(L_SV:Capacity),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                           PROMPT('Remaining Capacity:'),AT(307,24,80),USE(?Remaining_Capacity:Prompt),TRN
                           ENTRY(@n-8.0),AT(392,24,50,10),USE(L_TTQ:Remaining_Capacity),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                           LIST,AT(84,24,125,10),USE(L_SV:Floor),FONT(,,,FONT:bold,CHARSET:ANSI),VSCROLL,DROP(15),FORMAT('80L(2)|M~F' & |
  'loor~@s35@12L(2)|MI~FBN Floor~@p p@'),FROM(Queue:FileDrop:1)
                           LIST,AT(8,50,434,104),USE(?List_Deliveries),HVSCROLL,ALRT(CtrlA),ALRT(CtrlU),FORMAT('40R(2)|MY~' & |
  'DI No.~L@n_10@60L(2)|MY~Client~@s35@50L(2)|M~Client Ref.~@s30@70L(2)|M~Journey~@s70@' & |
  '46L(2)|M~Service Requirement~@s35@70L(2)|M~Load Type~@s35@50L(2)|M~Terms~@s20@26L(2)' & |
  '|MI~Insure~@p p@46R(2)|M~DI Date~L@d6@30R(2)|M~DID~L@n_10@'),FROM(Queue:Browse),IMM,MSG('Browsing Records')
                           PROMPT('Locate - DI No.:'),AT(8,38,73,10),USE(?LOC:DI_No_Locator:Prompt),COLOR(00D5FFFFh)
                           ENTRY(@n_10),AT(84,38,69,10),USE(LOC:DI_No_Locator),RIGHT(1),COLOR(00D5FFFFh),MSG('Delivery I' & |
  'nstruction Number'),TIP('Delivery Instruction Number')
                           STRING(''),AT(193,38,249,10),USE(?String_Tagged),RIGHT(1)
                           PROMPT('Deliveries on Floor:'),AT(8,24),USE(?Prompt3),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                           BUTTON('Load Tagged'),AT(8,174,59,12),USE(?Button_Tagged),DISABLE,TIP('On the selected delivery')
                           LINE,AT(8,190,434,0),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Loaded Delivery Items:'),AT(8,194),USE(?Prompt9),FONT(,,,FONT:bold,CHARSET:ANSI),TRN
                           PROMPT('Truck / Trailer:'),AT(8,10),USE(?Reg_Make_Model:Prompt:2),TRN
                           LIST,AT(84,10,125,10),USE(L_SV:Reg_Make_Model),VSCROLL,DROP(15,170),FORMAT('120L(2)|M~D' & |
  'escription~@s100@32D(2)|M~Capacity~L@n-8.0@32D(2)|M~Remaining~L@n-8.0@32D(2)|M~Loade' & |
  'd~L@n-8.0@40R(2)|M~TTID~L@n_10@'),FROM(LOC:Trucks_Trailers_Q)
                           LIST,AT(8,206,434,80),USE(?Browse_Loaded_Del_Items),HVSCROLL,FORMAT('35R(2)|M~DI No.~L(' & |
  '1)@n_10@19R(2)|M~Item No.~L(1)@n6@60L(2)|M~Client~L(1)@s35@60L(2)|M~Commodity~L(1)@s' & |
  '35@74L(2)|M~Pack. / Container No.~L(1)@s150@26R(2)|M~Units Loaded~L(1)@n6@44R(2)|M~W' & |
  'eight~L(1)@n-11.2@44R(2)|M~Volumetric Weight~L(1)@n-11.2@44R(2)|M~Volumetric Ratio~L' & |
  '(1)@n-11.2@40R(2)|M~DIID~L(1)@n_10@'),FROM(Queue:Browse:2),IMM,MSG('Browsing the Mai' & |
  'nfestLoadDeliveries file')
                           BUTTON('&Insert'),AT(288,288,49,14),USE(?Insert:3),LEFT,ICON('WAINSERT.ICO'),DISABLE,FLAT, |
  HIDE,MSG('Insert a Record'),TIP('Insert a Record')
                           BUTTON('&Change'),AT(342,288,49,14),USE(?Change:3),LEFT,ICON('WACHANGE.ICO'),FLAT,MSG('Change the Record'), |
  TIP('Change the Record')
                           BUTTON('&Delete'),AT(394,288,49,14),USE(?Delete:3),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Remove the DI Item from this Manifest')
                         END
                       END
                       BUTTON('&OK'),AT(346,308,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(398,308,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(12,290,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                       PROMPT('TTID:'),AT(4,310),USE(?MAL:TTID:Prompt)
                       STRING(@n_10),AT(17,310),USE(MAL:TTID),RIGHT(1),TRN
                       PROMPT('MID:'),AT(73,310),USE(?MAL:MID:Prompt)
                       STRING(@n_10),AT(84,310),USE(MAL:MID),RIGHT(1),TRN
                       BUTTON('Hidden - Control'),AT(270,308,,14),USE(?Button_Hidden_Control),HIDE
                       PROMPT('MLID:'),AT(141,310),USE(?MAL:MLID:Prompt)
                       STRING(@n_10),AT(157,310),USE(MAL:MLID),RIGHT(1),TRN
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW_Manifested       CLASS(BrowseClass)                    ! Browse using ?Browse_Loaded_Del_Items
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW_Deliveries       CLASS(BrowseClass)                    ! Browse using ?List_Deliveries
Q                      &Queue:Browse                  !Reference to browse queue
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator IncrementalLocatorClass               ! Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB10                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
SetQueueRecord         PROCEDURE(),DERIVED
                     END

FDB11                CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop:2              !Reference to display queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(SIGNED Field),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

View_ML         VIEW(ManifestLoadDeliveriesAlias)
       JOIN(A_DELI:PKey_DIID, A_MALD:DIID)
       PROJECT(A_DELI:DIID, A_DELI:DID)
    .  .

ML_View         ViewManager
    MAP
Load_All                   PROCEDURE(ULONG p_DID),LONG,PROC
Check_Deliveries_Q         PROCEDURE(ULONG p:DID),BYTE
    .
Fields_Type_Q   QUEUE,TYPE
F         &LONG
    .


Tags_Class              CLASS(shpTagClass)

Style_F_Q       &Fields_Type_Q
List_Box        ANY                 ! I dont get why this doesn't need to be a reference
Text_Note       ANY
List_Q          &QUEUE
Brw             &BrowseClass
Brw_ID          ANY

Action_Ctrl     ANY
Action_Event    LONG


Set_Count_Action_Ctrl              PROCEDURE(? p_Action_Ctrl, LONG p_Event)

Do_Keycode      PROCEDURE()
Do_All_Events   PROCEDURE(LONG p_ID)

Style_Setup     PROCEDURE()
Style_Entry     PROCEDURE(LONG p_ID)

Tag_Toggle_Rec  PROCEDURE(LONG p_ID)
Tag_All         PROCEDURE()     ! Specific to this proc code here!!

Un_Tag_All      PROCEDURE()

Add_Style_Field PROCEDURE(*LONG p_field)

Init            PROCEDURE(? p_List, QUEUE p_Q, ? p_Text, *BrowseClass p_Brw, *? p_B)

Construct       PROCEDURE()
Destruct        PROCEDURE()

Screen_Out      PROCEDURE()

Proc_Code       PROCEDURE()     ! this code is specific to this procedure
    .



p_Tags      Tags_Class
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Result)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
  !------------------------------------
  !Style for ?List_Deliveries
  !------------------------------------
!---------------------------------------------------------------------------
Load_Trucks_Trailers                ROUTINE
    DATA
R:Idx       LONG

    CODE
    ! Load the Manifest
          db.debugout('[Update_ManifestLoad - Load_Trucks_Trailers]   MAL:MID: ' & MAL:MID)

    MAN:MID     = MAL:MID
    IF Access:Manifest.TryFetch(MAN:PKey_MID) = LEVEL:Benign
       L_TG:TID     = MAN:TID
       TRA:TID      = L_TG:TID
       IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
          L_TG:Broking  = TRA:Broking
       ELSE
          db.debugout('[Update_ManifestLoad - Check_Delivery_Service]   FAILED FETCH TRANSPORTER')
       .


       !L_SV:VC_Capacity        = Get_VehComp_Info(MAN:VCID, 8)      ! VC capacity - NOTE: !!!!!! ??? not used anywhere yet
       L_TG:VCID    = MAN:VCID

       LOOP 4 TIMES
          R:Idx               += 1

          CLEAR(LOC:Trucks_Trailers_Q)
          L_TTQ:TTID           = Get_VehComp_Info(MAN:VCID, 6, R:Idx - 1)

          IF ThisWindow.Request = InsertRecord
             ! Check that this truck / trailer is not being used already on this MID
             A_MAL:MID         = MAL:MID
             A_MAL:TTID        = L_TTQ:TTID
             IF Access:ManifestLoadAlias.TryFetch(A_MAL:SKey_MID_TTID) = LEVEL:Benign
                CYCLE
          .  .

          L_TTQ:Description    = Get_VehComp_Info(MAN:VCID, 4, R:Idx - 1, 5)
          L_TTQ:Capacity       = Get_VehComp_Info(MAN:VCID, 5, R:Idx - 1)

          IF CLIP(L_TTQ:Description) = ''
             BREAK
          .

          IF L_TTQ:Capacity > 0
             ADD(LOC:Trucks_Trailers_Q)
    .  .  .
    EXIT
Remaining_Capacity               ROUTINE
DATA
L_RemainPer    DECIMAL(10.2)

CODE
  
   L_TTQ:Loaded_Capacity      = Get_ManLoadItems_Info(MAL:MLID, 0)
   !Get_ManTruck_Weight(MAL:MID, MAL:TTID) / 100
   

   L_TTQ:Remaining_Capacity   = L_TTQ:Capacity - L_TTQ:Loaded_Capacity
   L_RemainPer                = (L_TTQ:Remaining_Capacity / L_TTQ:Capacity)*100
   ?Remaining_Capacity:Prompt{PROP:Text} = 'Remaining Capacity ' & CLIP(LEFT(FORMAT(L_RemainPer,@n-4))) & '% :'
  
   IF L_RemainPer < 0.0
      ?L_TTQ:Remaining_Capacity{PROP:FontColor} = COLOR:Red
      ?L_TTQ:Remaining_Capacity{PROP:FontStyle} = FONT:bold
   ELSIF L_RemainPer < 10.0
      ?L_TTQ:Remaining_Capacity{PROP:FontColor} = COLOR:Orange
      ?L_TTQ:Remaining_Capacity{PROP:FontStyle} = FONT:bold
   ELSE
      ?L_TTQ:Remaining_Capacity{PROP:FontColor} = -1
      ?L_TTQ:Remaining_Capacity{PROP:FontStyle} = -1
   .
   
   DISPLAY(?L_TTQ:Remaining_Capacity)
   EXIT
! -------------------------------------------------------------------------------------------------------
Load_All_Items                          ROUTINE
    q_pos_#         = CHOICE(?List_Deliveries)

    IF Load_All(L_CV:DID) > 0
       GET(Queue:Browse, q_pos_#)
       DEL:DID      = Queue:Browse.DEL:DID
       IF Access:Deliveries.TryFetch(DEL:PKey_DID)
       .
       BRW_Deliveries.ResetFromBuffer()

       BRW_Deliveries.UpdateViewRecord()
       L_CV:DID     = DEL:DID
    .
    EXIT
!       Load_All_Items_old                          ROUTINE
!    DATA
!
!R:Needed        LIKE(L_TTQ:Remaining_Capacity)
!R:LessAlreadyOn LIKE(L_TTQ:Remaining_Capacity)
!R:LoadedUnits   ULONG
!R:Continue      BYTE
!
!    CODE
!    q_pos_#         = CHOICE(?List_Deliveries)
!
!    R:Continue      = TRUE
!
!    ! Check we have capacity
!    ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache)
!    R:Needed        = Get_DelItem_s_Totals(L_CV:DID, 6,,, TRUE) / 100      ! Total weight for Del Items
!
!    ! Less weight already loaded for this DID on any Manifest!
!!    R:LessAlreadyOn = Get_DelMan_Weight(L_CV:DID, MAL:MLID) / 100   ! Manifested weight on this MLID (for this Truck / Trailer)
!    R:LessAlreadyOn = Get_DelMan_Weight(L_CV:DID) / 100
!    R:Needed       -= R:LessAlreadyOn
!
!    IF R:Needed <= L_TTQ:Remaining_Capacity
!    ELSE
!       CASE MESSAGE('The capacity of this Truck / Trailer would be exceeded if you loaded: ' & CLIP(LEFT(FORMAT(R:Needed,@n12.2))) |
!               & ' which is the total weight of all the unloaded items on the selected delivery.||Do you want to continue to overload this vehicle?' |
!                , 'Truck / Trailer', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
!       OF BUTTON:Yes
!       ELSE
!          R:Continue    = FALSE
!    .  .
!
!    IF R:Continue = TRUE
!       DO Check_Delivery_Service
!
!       ! Update the Delivery Manifest ID
!
!       ! Loop through the Delivery Items
!       CLEAR(DELI:Record, -1)
!       DELI:DID     = L_CV:DID
!       SET(DELI:FKey_DID_ItemNo, DELI:FKey_DID_ItemNo)
!       LOOP
!          IF Access:DeliveryItems.TryNext() ~= LEVEL:Benign
!             BREAK
!          .
!          IF DELI:DID ~= L_CV:DID
!             BREAK
!          .
!
!          R:LoadedUnits             = Get_ManLoadItem_Units(DELI:DIID)
!          IF DELI:Units - R:LoadedUnits > 0
!             ! If previously loaded on this MAL:MLID we need to add to that entry
!             MALD:MLID                 = MAL:MLID
!             MALD:DIID                 = DELI:DIID
!             IF Access:ManifestLoadDeliveries.TryFetch(MALD:FSKey_MLID_DIID) = LEVEL:Benign
!             !message('MALD:UnitsLoaded: ' & MALD:UnitsLoaded & '||R:LoadedUnits: ' & R:LoadedUnits & '||DELI:Units: ' & DELI:Units)
!                ! Add the total Units on Item less already loaded total = total remaining
!                MALD:UnitsLoaded      += DELI:Units - R:LoadedUnits
!                IF Access:ManifestLoadDeliveries.TryUpdate() ~= LEVEL:Benign
!                   CLEAR(MALD:Record)
!                .
!             ELSE
!                IF Access:ManifestLoadDeliveries.TryPrimeAutoInc() = LEVEL:Benign
!                   !MALD:MLDID          =
!                   MALD:MLID           = MAL:MLID
!                   MALD:DIID           = DELI:DIID
!                   MALD:UnitsLoaded    = DELI:Units - R:LoadedUnits
!                   IF Access:ManifestLoadDeliveries.TryInsert() ~= LEVEL:Benign
!                      Access:ManifestLoadDeliveries.CancelAutoInc()
!                      CLEAR(MALD:Record)
!       .  .  .  .  .
!
!       LOC:Inserted_Changed    = TRUE
!       !Upd_Delivery_Man_Status(L_CV:DID)
!
!       DO Remaining_Capacity
!
!       L_SV:Reload_ManItems   += 1
!       L_SV:Reload_Deliveries += 1
!
!       L_SV:Last_DIID_Set      = 0
!   !       FDB11.ResetQueue(1)
!    .
!
!    GET(Queue:Browse, q_pos_#)
!    DEL:DID     = Queue:Browse.DEL:DID
!    IF Access:Deliveries.TryFetch(DEL:PKey_DID)
!    .
!    BRW_Deliveries.ResetFromBuffer()
!    EXIT
Load_This_Item                         ROUTINE
    DATA

R:Needed        LIKE(L_TTQ:Remaining_Capacity)
R:LessAlreadyOn LIKE(L_TTQ:Remaining_Capacity)
R:Continue      BYTE

    CODE
    IF L_SV:No_Items <= 0
       MESSAGE('You have 0 specified in the Units to Load field.', 'Load This Item', ICON:Asterisk)
    ELSE
       q_pos_#      = CHOICE(?List_Deliveries)

       R:Continue   = TRUE

       ! Check we have capacity
       DELI:DIID    = L_CV:DIID
       IF Access:DeliveryItems.TryFetch(DELI:PKey_DIID) ~= LEVEL:Benign
          MESSAGE('Could not fetch the Delivery Item.||DIID: ' & L_CV:DIID, 'Load This Item', ICON:Hand)
       ELSE
          R:Needed          = L_SV:No_Items * (DELI:Weight / DELI:Units)

          ! Weight for Del Item
          !R:Needed          = Get_DelItem_s_Totals(L_CV:DID, 0, L_CV:DIID) / 100

          ! Less weight already loaded for this DIID
          ! Manifested weight for this DIID on any Manifest
          !R:LessAlreadyOn = Get_DelMan_Weight(L_CV:DID, L_CV:DIID) / 100
          !R:Needed       -= R:LessAlreadyOn
          IF R:Needed <= L_TTQ:Remaining_Capacity
          ELSE
             CASE MESSAGE('The capacity of this Truck / Trailer would be exceeded if you loaded: ' & CLIP(LEFT(FORMAT(R:Needed,@n12.2))) |
                           & ' which is the total weight of all the unloaded items on the selected delivery.||Do you want to continue to overload this vehicle?' |
                            , 'Truck / Trailer', ICON:Hand, BUTTON:Yes+BUTTON:NO, BUTTON:NO)
             OF BUTTON:Yes
             ELSE
                R:Continue    = FALSE
          .  .


          IF R:Continue = TRUE
             DO Check_Delivery_Service

             ! Update the Delivery Manifest ID
             ! Loop through the Delivery Items

             ! If previously loaded on this MAL:MLID we need to add to that entry
             MALD:MLID             = MAL:MLID
             MALD:DIID             = DELI:DIID
             IF Access:ManifestLoadDeliveries.TryFetch(MALD:FSKey_MLID_DIID) = LEVEL:Benign
                MALD:UnitsLoaded  += L_SV:No_Items
                IF Access:ManifestLoadDeliveries.TryUpdate() ~= LEVEL:Benign
                .
             ELSE
                IF Access:ManifestLoadDeliveries.TryPrimeAutoInc() = LEVEL:Benign
                   !MALD:MLDID        =
                   MALD:MLID          = MAL:MLID
                   MALD:DIID          = DELI:DIID
                   MALD:UnitsLoaded   = L_SV:No_Items
                   IF Access:ManifestLoadDeliveries.TryInsert() ~= LEVEL:Benign
                      Access:ManifestLoadDeliveries.CancelAutoInc()
             .  .  .

             LOC:Inserted_Changed    = TRUE
             !Upd_Delivery_Man_Status(L_CV:DID)

             DO Remaining_Capacity

             L_SV:Reload_ManItems   += 1
             L_SV:Reload_Deliveries += 1
             L_SV:Last_DIID_Set      = 0
             FDB11.ResetQueue(1)
       .  .

       GET(Queue:Browse, q_pos_#)
       DEL:DID     = Queue:Browse.DEL:DID
       IF Access:Deliveries.TryFetch(DEL:PKey_DID)
       .
       BRW_Deliveries.ResetFromBuffer()

       BRW_Deliveries.UpdateViewRecord()
       L_CV:DID     = DEL:DID
    .
    EXIT
Load_Max_Items                       ROUTINE
    DATA

R:Needed        LIKE(L_TTQ:Remaining_Capacity)
R:CanLoad       LIKE(L_TTQ:Remaining_Capacity)
R:Load          LIKE(L_TTQ:Remaining_Capacity)
R:LoadedUnits   ULONG

    CODE
    q_pos_# = CHOICE(?List_Deliveries)

    ! Loop through the Delivery Items
    CLEAR(DELI:Record, -1)
    DELI:DID     = L_CV:DID
    SET(DELI:FKey_DID_ItemNo, DELI:FKey_DID_ItemNo)
    LOOP
       IF Access:DeliveryItems.TryNext() ~= LEVEL:Benign
          BREAK
       .
       IF DELI:DID ~= L_CV:DID
          BREAK
       .

       ! If we have no weight then allow all to be loaded
       IF DELI:Weight = 0.0
          R:CanLoad      = DELI:Units           ! Then we call load them all!  They weigh nothing...
       ELSE
          R:CanLoad      = L_TTQ:Remaining_Capacity / (DELI:Weight / DELI:Units)
       .

       R:LoadedUnits     = Get_ManLoadItem_Units(DELI:DIID)
       IF (DELI:Units - R:LoadedUnits) <= 0
          CYCLE
       .
       
       IF R:CanLoad > (DELI:Units - R:LoadedUnits)
          R:Load         = DELI:Units - R:LoadedUnits
       ELSE
          R:Load         = R:CanLoad
       .

       R:Needed          = DELI:Weight * (R:Load / DELI:Units)
       IF R:Needed <= L_TTQ:Remaining_Capacity AND R:Load > 0
          DO Check_Delivery_Service

          ! If previously loaded on this MAL:MLID we need to add to that entry
          MALD:MLID                 = MAL:MLID
          MALD:DIID                 = DELI:DIID
          IF Access:ManifestLoadDeliveries.TryFetch(MALD:FSKey_MLID_DIID) = LEVEL:Benign
          !message('MALD:UnitsLoaded: ' & MALD:UnitsLoaded & '||R:LoadedUnits: ' & R:LoadedUnits & '||DELI:Units: ' & DELI:Units)
             ! Add the total Units on Item less already loaded total = total remaining
             MALD:UnitsLoaded      += R:Load
             IF Access:ManifestLoadDeliveries.TryUpdate() ~= LEVEL:Benign
                CLEAR(MALD:Record)
             .
          ELSE
             IF Access:ManifestLoadDeliveries.TryPrimeAutoInc() = LEVEL:Benign
                !MALD:MLDID          =
                MALD:MLID           = MAL:MLID
                MALD:DIID           = DELI:DIID
                MALD:UnitsLoaded    = R:Load
                IF Access:ManifestLoadDeliveries.TryInsert() ~= LEVEL:Benign
                   Access:ManifestLoadDeliveries.CancelAutoInc()
                   CLEAR(MALD:Record)
          .  .  .
       ELSE
          MESSAGE('Cannot load any of the selected Deliveries Items onto this Load.', 'Load Max Items', ICON:Exclamation)
       .
       DO Remaining_Capacity
    .

    LOC:Inserted_Changed    = TRUE

    L_SV:Reload_ManItems   += 1
    L_SV:Reload_Deliveries += 1

    L_SV:Last_DIID_Set      = 0
    !Upd_Delivery_Man_Status(L_CV:DID)
    FDB11.ResetQueue(1)


    SELECT(?List_Deliveries, q_pos_#)

    !BRW_Deliveries.                   from current??

    GET(Queue:Browse, q_pos_#)
    DEL:DID     = Queue:Browse.DEL:DID
    IF Access:Deliveries.TryFetch(DEL:PKey_DID)
    .
!    db.debugout('DID: ' & DEL:DID)

    BRW_Deliveries.ResetFromBuffer()

    BRW_Deliveries.UpdateViewRecord()
    L_CV:DID     = DEL:DID
    EXIT
! -------------------------------------------------------------------------------------------------------
Check_Delivery_Service           ROUTINE
    ! Check if the Delivery option matches the Broking option

!    db.debugout('[Update_ManifestLoad - Check_Delivery_Service]   L_TG:DID: ' & L_TG:DID & ',   L_CV:DID: ' & L_CV:DID)

    IF L_TG:DID = L_CV:DID
       ! We have already dealt with this delivery
    ELSE
       L_TG:DID     = L_CV:DID

       DEL:DID      = L_CV:DID
       IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
          SERI:SID  = DEL:SID
          IF Access:ServiceRequirements.TryFetch(SERI:PKey_SID) ~= LEVEL:Benign
             db.debugout('[Update_ManifestLoad - Check_Delivery_Service]   FAILED FETCH SERVICE REQUIREMENTS')
          ELSE
!    db.debugout('[Update_ManifestLoad - Check_Delivery_Service]   SERI:Broking: ' & SERI:Broking & ',   L_TG:Broking: ' & L_TG:Broking)

             IF SERI:Broking = L_TG:Broking
                ! ok
             ELSE
                ! Ask the user to select a new Service Type for this delivery
                GlobalRequest       = SelectRecord
                Browse_ServiceRequirements(DEL:DINo, DEL:SID)
                IF GlobalResponse = RequestCompleted
                   ! Change the Service Requirement on the DI
                   DEL:SID          = SERI:SID
                   IF Access:Deliveries.Update() = LEVEL:Benign
    .  .  .  .  .  .

    EXIT
! -------------------------------------------------------------------------------------------------------
Pre_Load_DeliveryQ               ROUTINE
   FREE(LOC:Deliveries_Q)
   _SQLTemp{PROP:SQL}  = 'select src.did from '                                  & |
      '(select sum(di.Units) as u, sum(mld.UnitsLoaded) as ul, d.fid, d.did '    & | 
      ' from Deliveries as d	join DeliveryItems AS di 		ON d.DID = di.did	'  & | 
      ' left     JOIN ManifestLoadDeliveries AS mld 			ON mld.DIID = di.DIID ' & |
      ' where d.Manifested >= 0 AND d.Manifested <= 2 '                          & | 
      ' group by d.did, d.FID  )  src '                                          & |
      ' where (src.ul is null OR (src.u - src.ul) > 0) AND src.FID = ' & L_CV:FID
  
   IF ERRORCODE()
      MESSAGE('SQL ERROR: ' & FILEERROR() & ' (' & ERROR() & ')', 'Update_ManifestLoad', ICON:Hand)
   ELSE
      LOOP
         NEXT(_SQLTemp)
         IF ERRORCODE()
            BREAK
         .
         
         L_DQ:DID = _SQ:S1
         ADD(LOC:Deliveries_Q)
   .  .
      
   L_SV:Reload_Deliveries += 1
   db.debugout('-------- L_CV:FID: ' & L_CV:FID & '      records: ' & RECORDS(LOC:Deliveries_Q) & '        L_SV:Reload_Deliveries: ' & L_SV:Reload_Deliveries)
   EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Manifest Load Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Manifest Load Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_ManifestLoad')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt3:3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_CV:FID',L_CV:FID)                                ! Added by: BrowseBox(ABC)
  BIND('L_MG:Manifested_Item_Desc',L_MG:Manifested_Item_Desc) ! Added by: BrowseBox(ABC)
  BIND('L_MG:Manifested_Weight',L_MG:Manifested_Weight)    ! Added by: BrowseBox(ABC)
  BIND('L_DG:Terms',L_DG:Terms)                            ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(MAL:Record,History::MAL:Record)
  SELF.AddHistoryField(?MAL:TTID,3)
  SELF.AddHistoryField(?MAL:MID,2)
  SELF.AddHistoryField(?MAL:MLID,1)
  SELF.AddUpdateFile(Access:ManifestLoad)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:Deliveries.Open                                   ! File Deliveries used by this procedure, so make sure it's RelationManager is open
  Relate:ManifestLoadAlias.Open                            ! File ManifestLoadAlias used by this procedure, so make sure it's RelationManager is open
  Relate:ManifestLoadDeliveriesAlias.Open                  ! File ManifestLoadDeliveriesAlias used by this procedure, so make sure it's RelationManager is open
  Relate:_SQLTemp.Open                                     ! File _SQLTemp used by this procedure, so make sure it's RelationManager is open
  Access:ManifestLoad.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItemAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailer.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ManifestLoad
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
      DO Load_Trucks_Trailers
  
      IF RECORDS(LOC:Trucks_Trailers_Q) <= 0
         IF SELF.Request = InsertRecord
            MESSAGE('No vehicles are available.', 'Update Manifest Load', ICON:Hand)
         ELSE
            MESSAGE('No vehicles are available.||There seems to be a problem.', 'Update Manifest Load', ICON:Hand)
         .
         Result    = -1
         RETURN(LEVEL:Notify)
      ELSIF SELF.Request = InsertRecord     !AND RECORDS(LOC:Trucks_Trailers_Q) = 1
         ! Load it - first one regardless
         GET(LOC:Trucks_Trailers_Q,1)
         IF ~ERRORCODE()
            MAL:TTID             = L_TTQ:TTID
            L_SV:Reg_Make_Model  = L_TTQ:Description
            L_SV:Capacity        = L_TTQ:Capacity
      .  .
  
  
  
      db.debugout('[Update_ManifestLoad]  Truck recs: ' & RECORDS(LOC:Trucks_Trailers_Q))
  BRW_Manifested.Init(?Browse_Loaded_Del_Items,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:ManifestLoadDeliveries,SELF) ! Initialize the browse manager
  BRW_Deliveries.Init(?List_Deliveries,Queue:Browse.ViewPosition,BRW12::View:Browse,Queue:Browse,Relate:Deliveries,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?L_IG:Item_Desc)
    DISABLE(?Button_AllItems)
    DISABLE(?Button_AllItems:2)
    DISABLE(?Button_ThisItem)
    DISABLE(?L_SV:Floor)
    ?LOC:DI_No_Locator{PROP:ReadOnly} = True
    DISABLE(?Button_Tagged)
    ?L_SV:Reg_Make_Model{PROP:ReadOnly} = True
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    DISABLE(?Button_Hidden_Control)
  END
  BRW_Manifested.Q &= Queue:Browse:2
  BRW_Manifested.FileLoaded = 1                            ! This is a 'file loaded' browse
  BRW_Manifested.AddSortOrder(,MALD:FSKey_MLID_DIID)       ! Add the sort order for MALD:FSKey_MLID_DIID for sort order 1
  BRW_Manifested.AddRange(MALD:MLID,Relate:ManifestLoadDeliveries,Relate:ManifestLoad) ! Add file relationship range limit for sort order 1
  BRW_Manifested.AddResetField(L_SV:Reload_ManItems)       ! Apply the reset field
  BRW_Manifested.AddField(DEL:DINo,BRW_Manifested.Q.DEL:DINo) ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW_Manifested.AddField(DELI:ItemNo,BRW_Manifested.Q.DELI:ItemNo) ! Field DELI:ItemNo is a hot field or requires assignment from browse
  BRW_Manifested.AddField(CLI:ClientName,BRW_Manifested.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW_Manifested.AddField(COM:Commodity,BRW_Manifested.Q.COM:Commodity) ! Field COM:Commodity is a hot field or requires assignment from browse
  BRW_Manifested.AddField(L_MG:Manifested_Item_Desc,BRW_Manifested.Q.L_MG:Manifested_Item_Desc) ! Field L_MG:Manifested_Item_Desc is a hot field or requires assignment from browse
  BRW_Manifested.AddField(MALD:UnitsLoaded,BRW_Manifested.Q.MALD:UnitsLoaded) ! Field MALD:UnitsLoaded is a hot field or requires assignment from browse
  BRW_Manifested.AddField(L_MG:Manifested_Weight,BRW_Manifested.Q.L_MG:Manifested_Weight) ! Field L_MG:Manifested_Weight is a hot field or requires assignment from browse
  BRW_Manifested.AddField(DELI:VolumetricWeight,BRW_Manifested.Q.DELI:VolumetricWeight) ! Field DELI:VolumetricWeight is a hot field or requires assignment from browse
  BRW_Manifested.AddField(DELI:VolumetricRatio,BRW_Manifested.Q.DELI:VolumetricRatio) ! Field DELI:VolumetricRatio is a hot field or requires assignment from browse
  BRW_Manifested.AddField(MALD:DIID,BRW_Manifested.Q.MALD:DIID) ! Field MALD:DIID is a hot field or requires assignment from browse
  BRW_Manifested.AddField(DELI:ContainerNo,BRW_Manifested.Q.DELI:ContainerNo) ! Field DELI:ContainerNo is a hot field or requires assignment from browse
  BRW_Manifested.AddField(DELI:Weight,BRW_Manifested.Q.DELI:Weight) ! Field DELI:Weight is a hot field or requires assignment from browse
  BRW_Manifested.AddField(DELI:Units,BRW_Manifested.Q.DELI:Units) ! Field DELI:Units is a hot field or requires assignment from browse
  BRW_Manifested.AddField(PACK:Packaging,BRW_Manifested.Q.PACK:Packaging) ! Field PACK:Packaging is a hot field or requires assignment from browse
  BRW_Manifested.AddField(MALD:MLDID,BRW_Manifested.Q.MALD:MLDID) ! Field MALD:MLDID is a hot field or requires assignment from browse
  BRW_Manifested.AddField(MALD:MLID,BRW_Manifested.Q.MALD:MLID) ! Field MALD:MLID is a hot field or requires assignment from browse
  BRW_Manifested.AddField(DELI:DIID,BRW_Manifested.Q.DELI:DIID) ! Field DELI:DIID is a hot field or requires assignment from browse
  BRW_Manifested.AddField(PACK:PTID,BRW_Manifested.Q.PACK:PTID) ! Field PACK:PTID is a hot field or requires assignment from browse
  BRW_Manifested.AddField(DEL:DID,BRW_Manifested.Q.DEL:DID) ! Field DEL:DID is a hot field or requires assignment from browse
  BRW_Manifested.AddField(CLI:CID,BRW_Manifested.Q.CLI:CID) ! Field CLI:CID is a hot field or requires assignment from browse
  BRW_Manifested.AddField(COM:CMID,BRW_Manifested.Q.COM:CMID) ! Field COM:CMID is a hot field or requires assignment from browse
  BRW_Deliveries.Q &= Queue:Browse
  BRW_Deliveries.FileLoaded = 1                            ! This is a 'file loaded' browse
  BRW_Deliveries.AddSortOrder(,DEL:Key_DINo)               ! Add the sort order for DEL:Key_DINo for sort order 1
  BRW_Deliveries.AddLocator(BRW12::Sort0:Locator)          ! Browse has a locator for sort order 1
  BRW12::Sort0:Locator.Init(?LOC:DI_No_Locator,DEL:DINo,1,BRW_Deliveries) ! Initialize the browse locator using ?LOC:DI_No_Locator using key: DEL:Key_DINo , DEL:DINo
  BRW_Deliveries.SetFilter('(DEL:Manifested >= 0 AND DEL:Manifested <<= 2 AND DEL:FID = L_CV:FID)') ! Apply filter expression to browse
  BRW_Deliveries.AddResetField(L_SV:Reload_Deliveries)     ! Apply the reset field
  ?List_Deliveries{PROP:IconList,1} = '~checkoffdim.ico'
  ?List_Deliveries{PROP:IconList,2} = '~checkon.ico'
  BRW_Deliveries.AddField(DEL:DINo,BRW_Deliveries.Q.DEL:DINo) ! Field DEL:DINo is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(CLI:ClientName,BRW_Deliveries.Q.CLI:ClientName) ! Field CLI:ClientName is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:ClientReference,BRW_Deliveries.Q.DEL:ClientReference) ! Field DEL:ClientReference is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(JOU:Journey,BRW_Deliveries.Q.JOU:Journey) ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(SERI:ServiceRequirement,BRW_Deliveries.Q.SERI:ServiceRequirement) ! Field SERI:ServiceRequirement is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(CRT:ClientRateType,BRW_Deliveries.Q.CRT:ClientRateType) ! Field CRT:ClientRateType is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(L_DG:Terms,BRW_Deliveries.Q.L_DG:Terms) ! Field L_DG:Terms is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:Insure,BRW_Deliveries.Q.DEL:Insure) ! Field DEL:Insure is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:DIDate,BRW_Deliveries.Q.DEL:DIDate) ! Field DEL:DIDate is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:DID,BRW_Deliveries.Q.DEL:DID) ! Field DEL:DID is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:Manifested,BRW_Deliveries.Q.DEL:Manifested) ! Field DEL:Manifested is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:MultipleManifestsAllowed,BRW_Deliveries.Q.DEL:MultipleManifestsAllowed) ! Field DEL:MultipleManifestsAllowed is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(DEL:FID,BRW_Deliveries.Q.DEL:FID) ! Field DEL:FID is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(SERI:SID,BRW_Deliveries.Q.SERI:SID) ! Field SERI:SID is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(CRT:CRTID,BRW_Deliveries.Q.CRT:CRTID) ! Field CRT:CRTID is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(JOU:JID,BRW_Deliveries.Q.JOU:JID) ! Field JOU:JID is a hot field or requires assignment from browse
  BRW_Deliveries.AddField(CLI:CID,BRW_Deliveries.Q.CLI:CID) ! Field CLI:CID is a hot field or requires assignment from browse
  QuickWindow{PROP:MinWidth} = QuickWindow{PROP:Width}     ! Restrict the minimum window width to the 'design time' width
  QuickWindow{PROP:MinHeight} = 324                        ! Restrict the minimum window height
  QuickWindow{PROP:MaxWidth} = QuickWindow{PROP:Width}     ! Restrict the maximum window width to the 'design time' width
  QuickWindow{PROP:MaxHeight} = 324                        ! Restrict the maximum window height
  Resizer.Init(AppStrategy:Surface)                        ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_ManifestLoad',QuickWindow)          ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      L_TTQ:TTID              = MAL:TTID
      GET(LOC:Trucks_Trailers_Q, +L_TTQ:TTID)
      IF ~ERRORCODE()
         L_SV:Reg_Make_Model  = L_TTQ:Description
         L_SV:Capacity        = L_TTQ:Capacity
      ELSE
         ! TID has probably changed, Truck/Trailer is not in Q
         TRU:TTID             = MAL:TTID
         IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
            ?L_SV:Reg_Make_Model{PROP:Tip}   = 'Registration: ' & TRU:Registration
         ELSE
            ?L_SV:Reg_Make_Model{PROP:Tip}   = '<unknown>'
      .  .
      IF SELF.Request ~= InsertRecord
         DISABLE(?L_SV:Reg_Make_Model)
      .
      ! Set the floor
      FLO:FID             = Get_Branch_Info(GLO:BranchID, 3)
      IF Access:Floors.TryFetch(FLO:PKey_FID) = LEVEL:Benign
         L_SV:Floor       = FLO:Floor
         L_CV:FID         = FLO:FID
      .
  BRW_Manifested.AskProcedure = 1                          ! Will call: Update_MainfestLoadDeliveries
  FDB10.Init(?L_SV:Floor,Queue:FileDrop:1.ViewPosition,FDB10::View:FileDrop,Queue:FileDrop:1,Relate:Floors,ThisWindow)
  FDB10.Q &= Queue:FileDrop:1
  FDB10.AddSortOrder(FLO:Key_Floor)
  FDB10.AddField(FLO:Floor,FDB10.Q.FLO:Floor) !List box control field - type derived from field
  FDB10.AddField(FLO:FBNFloor,FDB10.Q.FLO:FBNFloor) !List box control field - type derived from field
  FDB10.AddField(FLO:FID,FDB10.Q.FLO:FID) !Primary key field - type derived from field
  FDB10.AddUpdateField(FLO:FID,L_CV:FID)
  ThisWindow.AddItem(FDB10.WindowComponent)
  ?L_SV:Floor{PROP:IconList,1} = '~checkoffdim.ico'
  ?L_SV:Floor{PROP:IconList,2} = '~checkon.ico'
  FDB11.Init(?L_IG:Item_Desc,Queue:FileDrop:2.ViewPosition,FDB11::View:FileDrop,Queue:FileDrop:2,Relate:DeliveryItems,ThisWindow)
  FDB11.Q &= Queue:FileDrop:2
  FDB11.AddSortOrder(DELI:FKey_DID_ItemNo)
  FDB11.AddRange(DELI:DID,L_CV:DID)
  FDB11.AddField(COM:Commodity,FDB11.Q.COM:Commodity) !List box control field - type derived from field
  FDB11.AddField(L_IG:Item_Desc,FDB11.Q.L_IG:Item_Desc) !List box control field - type derived from local data
  FDB11.AddField(L_IG:Item_Units,FDB11.Q.L_IG:Item_Units) !List box control field - type derived from local data
  FDB11.AddField(L_IG:Item_Weight,FDB11.Q.L_IG:Item_Weight) !List box control field - type derived from local data
  FDB11.AddField(DELI:Units,FDB11.Q.DELI:Units) !List box control field - type derived from field
  FDB11.AddField(DELI:ItemNo,FDB11.Q.DELI:ItemNo) !List box control field - type derived from field
  FDB11.AddField(DELI:Type,FDB11.Q.DELI:Type) !Browse hot field - type derived from field
  FDB11.AddField(DELI:ContainerNo,FDB11.Q.DELI:ContainerNo) !Browse hot field - type derived from field
  FDB11.AddField(PACK:Packaging,FDB11.Q.PACK:Packaging) !Browse hot field - type derived from field
  FDB11.AddField(DELI:VolumetricWeight,FDB11.Q.DELI:VolumetricWeight) !Browse hot field - type derived from field
  FDB11.AddField(DELI:Weight,FDB11.Q.DELI:Weight) !Browse hot field - type derived from field
  FDB11.AddField(DEL:DINo,FDB11.Q.DEL:DINo) !Browse hot field - type derived from field
  FDB11.AddField(PACK:PTID,FDB11.Q.PACK:PTID) !Browse hot field - type derived from field
  FDB11.AddField(COM:CMID,FDB11.Q.COM:CMID) !Browse hot field - type derived from field
  FDB11.AddField(DELI:DIID,FDB11.Q.DELI:DIID) !Primary key field - type derived from field
  FDB11.AddUpdateField(DELI:DIID,L_CV:DIID)
  ThisWindow.AddItem(FDB11.WindowComponent)
  SELF.SetAlerts()
  DO Pre_Load_DeliveryQ            ! Requires FID to be set
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Deliveries.Close
    Relate:ManifestLoadAlias.Close
    Relate:ManifestLoadDeliveriesAlias.Close
    Relate:_SQLTemp.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_ManifestLoad',QuickWindow)       ! Save window data to non-volatile store
  END
      IF LOC:Inserted_Changed = TRUE AND LOC:Cancelled = TRUE
         IF RECORDS(LOC:ManLoadDel_Q) > 0
            LOOP I_# = 1 TO RECORDS(LOC:ManLoadDel_Q)
               GET(LOC:ManLoadDel_Q, I_#)
               IF ERRORCODE()
                  BREAK
               .
  
               Process_Deliveries(L_MLD:DID)
      .  .  .
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  MAL:MID = MAN:MID
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
     CLEAR(L_CV:DID_Loaded_Del)
     IF MALD:DIID = 0
        MESSAGE('DIID on Update call is 0', 'Update_ManifestLoad', ICON:Hand)
     ELSE
        DELI:DIID   = MALD:DIID
        IF Access:DeliveryItems.TryFetch(DELI:PKey_DIID) = LEVEL:Benign
           IF DELI:DID = 0
              MESSAGE('Delivery Item has DID 0.||DIID is: ' & MALD:DIID, 'Update_ManifestLoad', ICON:Hand)
           ELSE
              L_CV:DID_Loaded_Del  = DELI:DID
           .
        ELSE
           MESSAGE('Delivery could not be fetched on Update call.||DIID is: ' & MALD:DIID, 'Update_ManifestLoad', ICON:Hand)
     .  .
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_MainfestLoadDeliveries
    ReturnValue = GlobalResponse
  END
     IF Request = DeleteRecord
        ! Check and Remove any MID from the Delivery if there are no items from this Delivery loaded on this MID now
        IF Get_ManLoad_Info(MAL:MID, 1, L_CV:DID_Loaded_Del) = 0
                        ! (p:DID, p:MID, p:Reset)
           !Set_DeliveryMID(L_CV:DID_Loaded_Del, MAL:MID, 1)
           !Upd_Delivery_Man_Status(L_CV:DID_Loaded_Del)
        .
     ELSIF Request = ChangeRecord
        IF ReturnValue = RequestCompleted
           LOC:Inserted_Changed   = TRUE
        .
  
        IF MALD:UnitsLoaded = 0
           IF Access:ManifestLoadDeliveries.DeleteRecord(0) = LEVEL:Benign
              IF Get_ManLoad_Info(MAL:MID, 1, L_CV:DID_Loaded_Del) = 0
                 !Set_DeliveryMID(L_CV:DID_Loaded_Del, MAL:MID, 1)
                 !Upd_Delivery_Man_Status(L_CV:DID_Loaded_Del)
        .  .  .
     ELSIF Request = InsertRecord
        LOC:Inserted_Changed      = TRUE
     .
  
  
  
     L_SV:Reload_Deliveries  += 1
     L_SV:Reload_ManItems    += 1
  
    db.debugout('[Update_ManifestLoad]  L_SV:Reload_Deliveries: ' & L_SV:Reload_Deliveries)
  
     FDB11.ResetQueue(1)
  
     DO Remaining_Capacity
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button_AllItems
      ThisWindow.Update()
          DO Load_All_Items
    OF ?Button_AllItems:2
      ThisWindow.Update()
          DO Load_Max_Items
    OF ?Button_ThisItem
      ThisWindow.Update()
          DO Load_This_Item
    OF ?L_SV:Floor
         DO Pre_Load_DeliveryQ            ! Requires FID to be set
    OF ?Button_Tagged
      ThisWindow.Update()
          p_Tags.Proc_Code()
          
    OF ?OK
      ThisWindow.Update()
          IF L_TTQ:Remaining_Capacity < 0.0
             CASE MESSAGE('The Truck / Trailer''s capacity is exceeded by: ' & FORMAT(L_TTQ:Remaining_Capacity, @n12.2) & '||Do you want to remove items?', 'Manifest Load - Capacity', ICON:Question,|
                          BUTTON:Yes+BUTTON:No,BUTTON:Yes)
             OF BUTTON:Yes
                QuickWindow{PROP:AcceptAll}    = FALSE
                SELECT(?)
                CYCLE
          .  .
      
          CLEAR(L_SV:Capacity)
          CLEAR(L_TTQ:TTID)
      
          IF CHOICE(?L_SV:Reg_Make_Model) <= 0
             GET(LOC:Trucks_Trailers_Q, 1)
          ELSE
             GET(LOC:Trucks_Trailers_Q, CHOICE(?L_SV:Reg_Make_Model))
          .
      
          IF ~ERRORCODE()
             L_SV:Capacity    = L_TTQ:Capacity
      
             MAL:TTID         = L_TTQ:TTID
             DISPLAY
          .
      
      
          FREE(LOC:ManLoadDel_Q)
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    OF ?Cancel
      ThisWindow.Update()
          LOC:Cancelled   = TRUE
      
      
          ! Get a list of DIDs, run updates for them...
          FREE(LOC:ManLoadDel_Q)
          ML_View.Init(View_ML, Relate:ManifestLoadDeliveriesAlias)
          ML_View.AddSortOrder(A_MALD:FSKey_MLID_DIID)
          ML_View.AppendOrder('A_MALD:MLDID')
          ML_View.AddRange(A_MALD:MLID, MAL:MLID)
          !ML_View.SetFilter()
          ML_View.Reset()
          LOOP
             IF ML_View.Next() ~= LEVEL:Benign
                BREAK
             .
      
             IF A_DELI:DID ~= 0
                L_MLD:DID    = A_DELI:DID
                ADD(LOC:ManLoadDel_Q)
          .  .
      
          ML_View.Kill()
    OF ?Button_Hidden_Control
      ThisWindow.Update()
          IF p_Tags.NumberTagged() <= 0
             ENABLE(?Group_LoadItems)
             DISABLE(?Button_Tagged)
          ELSE
             DISABLE(?Group_LoadItems)
             ENABLE(?Button_Tagged)
          .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List_Deliveries
    CASE EVENT()
    OF EVENT:AlertKey
          p_Tags.Do_Keycode()
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List_Deliveries
    BRW_Deliveries.UpdateViewRecord()
        p_Tags.Do_All_Events(DEL:DID)
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_IG:Item_Desc
          GET(Queue:FileDrop:2, CHOICE(?L_IG:Item_Desc))
          IF ~ERRORCODE()
             L_SV:No_Items                     = Queue:FileDrop:2.L_IG:Item_Units
             ?L_SV:No_Items{PROP:RangeHigh}    = Queue:FileDrop:2.L_IG:Item_Units
             DISPLAY(?L_SV:No_Items)
          .
    OF ?L_SV:Floor
          p_Tags.Un_Tag_All()
    OF ?List_Deliveries
      BRW_Deliveries.UpdateViewRecord()
          L_CV:DID    = DEL:DID
    OF ?L_SV:Reg_Make_Model
          CLEAR(L_SV:Capacity)
          CLEAR(L_TTQ:TTID)
          GET(LOC:Trucks_Trailers_Q, CHOICE(?L_SV:Reg_Make_Model))
          IF ~ERRORCODE()
             L_SV:Capacity    = L_TTQ:Capacity
      
             MAL:TTID         = L_TTQ:TTID
             DISPLAY
          .
          DO Remaining_Capacity
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all Selected events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?L_SV:Reg_Make_Model
          ! Check if we have items loaded, if we do confirm change of trailer....
          IF RECORDS(Queue:Browse:2) > 0
             CASE MESSAGE('You have loaded items on this trailer already.||Are you sure you want to change which trailer these items are loaded on?||If you want to load unloaded items on another trailer please click OK and then on the Manifest screen click Add Load.', 'Change Trailer', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:No)
             OF BUTTON:No
                SELECT(?List_Deliveries)
          .  .
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          p_Tags.Init(?List_Deliveries, Queue:Browse, ?String_Tagged, BRW_Deliveries, Queue:Browse.DEL:DID)
      
          p_Tags.Set_Count_Action_Ctrl(?Button_Hidden_Control, EVENT:Accepted)
      
          p_Tags.Add_Style_Field(Queue:Browse.DEL:DINo_Style)
          p_Tags.Add_Style_Field(Queue:Browse.CLI:ClientName_Style)
      
      
          
          DO Remaining_Capacity
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Load_All                                    PROCEDURE(ULONG p_DID)     !,LONG

R:Continue      LONG
R:Needed        LIKE(L_TTQ:Remaining_Capacity)
R:LessAlreadyOn LIKE(L_TTQ:Remaining_Capacity)
R:LoadedUnits   ULONG

    CODE
    R:Continue      = TRUE

    ! Check we have capacity
    ! (p:DID, p:Option, p:DIID, p:Items_DIID_List, p:No_Cache)
    R:Needed        = Get_DelItem_s_Totals(p_DID, 6,,, TRUE) / 100      ! Total weight for Del Items

    ! Less weight already loaded for this DID on any Manifest!
!    R:LessAlreadyOn = Get_DelMan_Weight(p_DID, MAL:MLID) / 100   ! Manifested weight on this MLID (for this Truck / Trailer)
    R:LessAlreadyOn = Get_DelMan_Weight(p_DID) / 100
    R:Needed       -= R:LessAlreadyOn

    IF R:Needed <= L_TTQ:Remaining_Capacity
    ELSE
       CASE MESSAGE('The capacity of this Truck / Trailer would be exceeded if you loaded: ' & CLIP(LEFT(FORMAT(R:Needed,@n12.2))) |
               & ' which is the total weight of all the unloaded items on the selected delivery.||Do you want to continue to overload this vehicle?' |
                , 'Truck / Trailer', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
       OF BUTTON:Yes
       ELSE
          R:Continue    = FALSE
    .  .

    IF R:Continue = TRUE
       DO Check_Delivery_Service

       ! Update the Delivery Manifest ID

       ! Loop through the Delivery Items
       CLEAR(DELI:Record, -1)
       DELI:DID     = p_DID
       SET(DELI:FKey_DID_ItemNo, DELI:FKey_DID_ItemNo)
       LOOP
          IF Access:DeliveryItems.TryNext() ~= LEVEL:Benign
             BREAK
          .
          IF DELI:DID ~= p_DID
             BREAK
          .

          R:LoadedUnits             = Get_ManLoadItem_Units(DELI:DIID)
          IF DELI:Units - R:LoadedUnits > 0
             ! If previously loaded on this MAL:MLID we need to add to that entry
             MALD:MLID                 = MAL:MLID
             MALD:DIID                 = DELI:DIID
             IF Access:ManifestLoadDeliveries.TryFetch(MALD:FSKey_MLID_DIID) = LEVEL:Benign
             !message('MALD:UnitsLoaded: ' & MALD:UnitsLoaded & '||R:LoadedUnits: ' & R:LoadedUnits & '||DELI:Units: ' & DELI:Units)
                ! Add the total Units on Item less already loaded total = total remaining
                MALD:UnitsLoaded      += DELI:Units - R:LoadedUnits
                IF Access:ManifestLoadDeliveries.TryUpdate() ~= LEVEL:Benign
                   CLEAR(MALD:Record)
                .
             ELSE
                IF Access:ManifestLoadDeliveries.TryPrimeAutoInc() = LEVEL:Benign
                   !MALD:MLDID          =
                   MALD:MLID           = MAL:MLID
                   MALD:DIID           = DELI:DIID
                   MALD:UnitsLoaded    = DELI:Units - R:LoadedUnits
                   IF Access:ManifestLoadDeliveries.TryInsert() ~= LEVEL:Benign
                      Access:ManifestLoadDeliveries.CancelAutoInc()
                      CLEAR(MALD:Record)
       .  .  .  .  .

       LOC:Inserted_Changed    = TRUE
       !Upd_Delivery_Man_Status(p_DID)

       DO Remaining_Capacity

       L_SV:Reload_ManItems   += 1
       L_SV:Reload_Deliveries += 1

       L_SV:Last_DIID_Set      = 0
    .

    RETURN(R:Continue)
Tags_Class.Proc_Code                    PROCEDURE()         ! Specific to this proc
Loaded  LONG

    CODE
    Loaded  = 0
    LOOP
       GET(SELF.TagQueue, 1)
       IF ERRORCODE()
          BREAK
       .

       IF Load_All(SELF.TagQueue.Ptr) > 0
          Loaded    += 1
          SELF.ClearTag(SELF.TagQueue.Ptr)
       ELSE
          BREAK
    .  .

    IF Loaded > 0
       GET(Queue:Browse, q_pos_#)
       DEL:DID     = Queue:Browse.DEL:DID
       IF Access:Deliveries.TryFetch(DEL:PKey_DID)
       .
       BRW_Deliveries.ResetFromBuffer()
       SELF.Screen_Out()
    .
    RETURN

Tags_Class.Init                         PROCEDURE(? p_List, QUEUE p_Q, ? p_Text, *BrowseClass p_Brw, *? p_B)
    CODE
    SELF.List_Q    &= p_Q

    SELF.List_Box   = p_List

    SELF.Text_Note  = p_Text

    SELF.Brw       &= p_Brw

    SELF.Brw_ID    &= p_B

    SELF.Style_Setup()
    RETURN

Tags_Class.Set_Count_Action_Ctrl              PROCEDURE(? p_Action_Ctrl, LONG p_Event)
    CODE
    SELF.Action_Ctrl     = p_Action_Ctrl
    SELF.Action_Event    = p_Event
    RETURN

Tags_Class.Tag_Toggle_Rec              PROCEDURE(LONG p_ID)
    CODE
    IF SELF.IsTagged(p_ID) = TRUE
       SELF.ClearTag(p_ID)
    ELSE
       SELF.MakeTag(p_ID)
    .

    ! Highlighted record... this is not what we want in all cases
    GET(SELF.List_Q, CHOICE(SELF.List_Box))
    IF ~ERRORCODE()
       SELF.Style_Entry(p_ID)
       PUT(SELF.List_Q)                    
    .
    SELF.Screen_Out()
    RETURN

Tags_Class.Tag_All                     PROCEDURE()          ! Specific to this proc code here!!
Idx     LONG

View_Dels   VIEW(Deliveries)
    PROJECT(DEL:DID)
    .

Man_Dels    ViewManager

    CODE
    ! Tag All in the current sort order...

    ! DEL:Manifested >= 0 AND DEL:Manifested <= 2 AND DEL:FID = L_CV:FID

    Man_Dels.Init(View_Dels, Relate:Deliveries)
    Man_Dels.AddSortOrder(DEL:Key_DINo)
    Man_Dels.SetFilter('DEL:Manifested >= 0 AND DEL:Manifested <= 2 AND DEL:FID = L_CV:FID')

    Man_Dels.Reset()

    LOOP
       IF Man_Dels.Next() ~= LEVEL:Benign
          BREAK
       .

       SELF.MakeTag(DEL:DID)
    .

    Man_Dels.Kill()

    ! Now update all shown records with correct style
    Idx = 0
    LOOP
       Idx  += 1
       GET(SELF.List_Q, Idx)
       IF ERRORCODE()
          BREAK
       .

       SELF.Style_Entry(SELF.Brw_ID)
       PUT(SELF.List_Q)                    
    .

    SELF.Screen_Out()
    RETURN

Tags_Class.Un_Tag_All                  PROCEDURE()
    CODE
    SELF.ClearAllTags()
    SELF.Brw.ResetFromBuffer()
    SELF.Screen_Out()
    RETURN


Tags_Class.Do_Keycode       PROCEDURE()
    CODE
    CASE KEYCODE()
    OF CtrlA
       SELF.Tag_All()
    OF CtrlU
       SELF.Un_Tag_All()
    .
    RETURN


Tags_Class.Do_All_Events    PROCEDURE(LONG p_ID)
    CODE
    CASE EVENT()
    OF EVENT:Accepted
       !EVENT:MouseDown
!       BRW1.UpdateViewRecord()

       IF KeyCode() = ShiftMouseLeft
          ! Tag all records between this one and last tagged record
          IF CHOICE(?CurrentTab) = 1                   ! -----------------------------
!             SELF.Tag_From_To()
          .
       ELSIF KeyCode() = CtrlMouseLeft
           SELF.Tag_Toggle_Rec(p_ID)
       ELSIF KeyCode() ~= MouseRight AND KeyCode() ~= CtrlMouseRight        !AND LOC:Last_Tagged_Rec_Id ~= 0
!          InBox_Tags.ClearAllTags()                ! Clear all tags when no shift left mouse
!          LOC:Last_Tagged_Rec_Id        = 0

!          BRW1.ResetFromBuffer()
    .  .
    RETURN


Tags_Class.Style_Setup                 PROCEDURE()
    CODE
    ! Style:Normal
!    ?list1{PROPSTYLE:FontStyle, 1}     = FONT:Regular
!    ?list1{PROPSTYLE:FontStyle, 1}     = FONT:Bold

    ! Style:Header
    SELF.List_Box{PROPSTYLE:TextColor, 1}     = COLOR:HIGHLIGHTTEXT
    SELF.List_Box{PROPSTYLE:BackColor, 1}     = COLOR:HIGHLIGHT
    SELF.List_Box{PROPSTYLE:TextSelected, 1}  = COLOR:White
    SELF.List_Box{PROPSTYLE:BackSelected, 1}  = COLOR:Blue
    RETURN


Tags_Class.Style_Entry                 PROCEDURE(LONG p_ID)
Idx     LONG
Style   LONG
    CODE
    IF SELF.IsTagged(p_ID) = TRUE
       Style        = 1
    ELSE
       Style        = 0
    .

    Idx = 0
    LOOP
       Idx  += 1
       GET(SELF.Style_F_Q, Idx)
       IF ERRORCODE()
          BREAK
       .
       SELF.Style_F_Q.F    = Style
    .
    RETURN


Tags_Class.Add_Style_Field              PROCEDURE(*LONG p_field)
    CODE
    SELF.Style_F_Q.F      &= p_field
    ADD(SELF.Style_F_Q)
    RETURN

Tags_Class.Screen_Out                   PROCEDURE()
    CODE
    SELF.Text_Note{PROP:Text}   = 'Tagged: ' & SELF.NumberTagged()
    IF SELF.Action_Ctrl &= NULL
    ELSE
       POST(SELF.Action_Event, SELF.Action_Ctrl)
    .
    RETURN

Tags_Class.Construct       PROCEDURE()
    CODE
    SELF.Style_F_Q  &= NEW(Fields_Type_Q)
    RETURN

Tags_Class.Destruct        PROCEDURE()
    CODE
    DISPOSE(SELF.Style_F_Q)
    RETURN


!  Controls
!!    ?List_Deliveries           5
!    ?String_Tagged      3
!    ?CurrentTab         1
!
!  Views & Files
!    View_Man            1
!    Man_View            4
!
!    Relate:Manifest     1
!
!    Queue:Browse      6
!
!  Objects
!    BRW_Manifests       2



Check_Deliveries_Q               PROCEDURE(ULONG p:DID)      !,BYTE
L:Res    BYTE(0)
CODE
   L_DQ:DID = p:DID
   GET(LOC:Deliveries_Q, L_DQ:DID)
   IF ~ERRORCODE()
      L:Res = TRUE
   .
!   db.debugout('[Update_ManifestLoad] Check_Deliveries_Q - p:DID: ' & p:DID & '     Pass: ' & L:Res)
   RETURN(L:Res)

BRW_Manifested.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW_Manifested.SetQueueRecord PROCEDURE

  CODE
      ! Hot fields:
      !   DELI:Type
      !   DELI:ContainerNo
      !   PACK:Packaging
      !   DELI:VolumetricWeight
      !   DELI:Weight
  
      CLEAR(L_MG:Manifested_Item_Desc)
  
      CASE DELI:Type
      OF 0                                ! Container
         L_MG:Manifested_Item_Desc    = DELI:ContainerNo
      OF 1                                ! Loose
         L_MG:Manifested_Item_Desc    = MALD:UnitsLoaded & ' * ' & PACK:Packaging
      .
  
  !    IF DELI:Weight < DELI:VolumetricWeight
  !       L_MG:Manifested_Weight       = DELI:VolumetricWeight * (MALD:UnitsLoaded / DELI:Units)
  !    ELSE
      L_MG:Manifested_Weight          = DELI:Weight           * (MALD:UnitsLoaded / DELI:Units)
  !    .
  PARENT.SetQueueRecord
  


BRW_Deliveries.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  PARENT.ResetQueue(ResetMode)
      IF RECORDS(Queue:Browse) <= 0
         CLEAR(L_CV:DID)
  
         IF ?Button_AllItems{PROP:Disable} = FALSE
            DISABLE(?Button_AllItems)
            DISABLE(?Button_AllItems:2)
            DISABLE(?Button_ThisItem)
         .
      ELSE
         IF ?Button_AllItems{PROP:Disable} = TRUE
            ENABLE(?Button_AllItems)
            ENABLE(?Button_AllItems:2)
            ENABLE(?Button_ThisItem)
      .  .


BRW_Deliveries.SetQueueRecord PROCEDURE

  CODE
      !       nothing
      ! Pre Paid|COD Delivery|COD Pickup|Account
      !EXECUTE DEL:Terms
      !   L_DG:Terms       = 'COD Delivery'
      !   L_DG:Terms       = 'COD Pickup'
      !   L_DG:Terms       = 'Account'
      !ELSE
      !   L_DG:Terms       = 'Pre Paid'
      !.
  PARENT.SetQueueRecord
  
  SELF.Q.DEL:DINo_Style = 0 ! 
  SELF.Q.CLI:ClientName_Style = 0 ! 
  IF (DEL:Insure = 1)
    SELF.Q.DEL:Insure_Icon = 2                             ! Set icon from icon list
  ELSE
    SELF.Q.DEL:Insure_Icon = 1                             ! Set icon from icon list
  END
      p_Tags.Style_Entry(DEL:DID)
  


BRW_Deliveries.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
LOC:Filtered        BYTE
  CODE
      ! Show DEL:Manifested 1 if Multiple allowed  OR  if this is MID on Loaded Items
  !   start_# = CLOCK()
  
      IF DEL:Manifested = 1
         db.debugout('[Update_ManifestLoad]  Validate Deliveries - DEL:DID ' & DEL:DID & '     DEL:MultipleManifestsAllowed: ' & DEL:MultipleManifestsAllowed)
         IF DEL:MultipleManifestsAllowed = TRUE OR Check_Delivery_MID(DEL:DID, MAL:MID) > 0
            ! Show
         ELSE
  !       MESSAGE('DEL:DID: ' & DEL:DID & '||Check_Delivery_MID: ' & Check_Delivery_MID(DEL:DID, MAL:MID))
            ReturnValue   = Record:Filtered
            LOC:Filtered  = TRUE
      .  .
  
  !    db.debugout('[Update_ManifestLoad]  Validate Deliveries - Stage 1 - DEL:DID ' & DEL:DID & '   Filtered: ' & LOC:Filtered & '   yes#: ' & yes# & '    Time: ' & CLOCK() - start_#)
  !    start_# = CLOCK()
           
      IF LOC:Filtered = FALSE
         ! Check we have some Un-Manifested Items
  !       IF Get_DelItem_s_Totals(DEL:DID, 1,,,TRUE) = 0
         IF Check_Deliveries_Q(DEL:DID) <= 0
  !       MESSAGE('DEL:DID: ' & DEL:DID & '||Get_DelItem_s_Totals: ' & Get_DelItem_s_Totals(DEL:DID, 1))
            ReturnValue      = Record:Filtered
         ELSE
  
  ReturnValue = PARENT.ValidateRecord()
      .  .
  
  !    db.debugout('[Update_ManifestLoad]  Validate Deliveries - Stage 2 - DEL:DID ' & DEL:DID & '   Time: ' & CLOCK() - start_#)
  BRW12::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


FDB10.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  
  IF (FLO:FBNFloor = 1)
    SELF.Q.FLO:FBNFloor_Icon = 2
  ELSE
    SELF.Q.FLO:FBNFloor_Icon = 1
  END


FDB11.SetQueueRecord PROCEDURE

  CODE
      ! Hot fields:
      !   DELI:Type
      !   DELI:ContainerNo
      !   PACK:Packaging
      !   DELI:VolumetricWeight
      !   DELI:Weight
      !   DELI:Units
      !   DEL:DINo
  
      L_IG:Item_Units             = DELI:Units - Get_ManLoadItem_Units(DELI:DIID)
  
      CASE DELI:Type
      OF 0                                ! Container
         L_IG:Item_Desc           = DELI:ContainerNo
      OF 1                                ! Loose
         L_IG:Item_Desc           = L_IG:Item_Units & ' * ' & PACK:Packaging
      .
  
  
      ! We dont care about volumetric weight here
  
  !    IF DELI:Weight < DELI:VolumetricWeight
  !       L_IG:Item_Weight         = DELI:VolumetricWeight * (L_IG:Item_Units / DELI:Units)
  !    ELSE
       L_IG:Item_Weight         = DELI:Weight           * (L_IG:Item_Units / DELI:Units)
  !    .
  PARENT.SetQueueRecord
  


FDB11.TakeNewSelection PROCEDURE(SIGNED Field)

  CODE
  PARENT.TakeNewSelection(Field)
      IF L_SV:Last_DIID_Set ~= L_CV:DIID
         L_SV:Last_DIID_Set                   = L_CV:DIID
         GET(Queue:FileDrop:2, CHOICE(?L_IG:Item_Desc))
         IF ~ERRORCODE()
            L_SV:No_Items                     = Queue:FileDrop:2.L_IG:Item_Units
            ?L_SV:No_Items{PROP:RangeHigh}    = Queue:FileDrop:2.L_IG:Item_Units
            DISPLAY(?L_SV:No_Items)
      .  .


FDB11.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !    message('DIID: ' & DELI:DIID & '||Items: ' & Get_DelItemUnMan_Units(DELI:DIID))
  
      db.debugout('[Update_ManifestLoad]  Validate Delivery Items - DELI:DIID: ' & DELI:DIID)
      IF Get_DelItemUnMan_Units(DELI:DIID) = 0
         ReturnValue    = Record:Filtered
      ELSE
  ReturnValue = PARENT.ValidateRecord()
      .
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Delivery_PODs_moved_to_manapp PROCEDURE  (ULONG p:DID)     ! Declare Procedure
LOC:DIID             ULONG                                 ! Delivery Item ID
LOC:FID              ULONG                                 ! Floor ID
LOC:Del_Group        GROUP,PRE(L_DG)                       ! 
Date                 LONG                                  ! 
Time                 LONG                                  ! 
                     END                                   ! 
View_TripDels   VIEW(TripSheetDeliveriesAlias)
                     PROJECT(A_TRDI:TDID, A_TRDI:DIID)
                     JOIN(A_DELI:PKey_DIID, A_TRDI:DIID)
                        JOIN(A_DEL:PKey_DID, A_DELI:DID)
                           PROJECT(A_DEL:DID)  
                        .
                     .
                  .

TripDelsView    ViewManager

  CODE
!   ! Loop through the Deliveries
!   PUSHBIND()
!   BIND('DEL:DID',DEL:DID)
!
!   TripDelsView.Init(View_TripDels, Relate:TripSheetDeliveriesAlias)
!   TripDelsView.AddSortOrder(A_TRDI:FKey_TRID)
!   TripDelsView.AppendOrder('A_TRDI:TDID')            !A_TRDI:DIID')
!   !TripDelsView.AddRange(A_TRDI:TRID, TRI:TRID)
!
!   TripDelsView.SetFilter('A_DEL:DID = ' & p:DID)
!
!   TripDelsView.Reset()
!   LOOP !UNTIL L_Cancel_Item = TRUE
!      IF TripDelsView.Next() ~= LEVEL:Benign
!         BREAK
!      .
!
!      LOC:DIID      = A_TRDI:DIID
!
!      TRDI:TDID     = A_TRDI:TDID
!      IF Access:TripSheetDeliveries.TryFetch(TRDI:PKey_TDID) ~= LEVEL:Benign
!         MESSAGE('Could not fetch the Trip Sheet Delivery: ' & A_TRDI:TDID, 'Trip Sheet POD', ICON:Hand)
!         CYCLE
!      .
!
!      GlobalRequest     = ChangeRecord
!      Update_TripSheetDeliveries(LOC:Del_Group)
!      IF GlobalResponse = RequestCancelled
!         ! Do what?
!         CASE MESSAGE('Would you like to stop updating the Trip Sheet Deliveries?', 'Trip Sheet POD', ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
!         OF BUTTON:Yes
!            BREAK
!         .
!      ELSE
!         ! Change the Floor that the entire DI is on....
!         ! Or should this be done only when all have been moved?  In theory
!         ! they could go to different floors too...
!         DEL:DID        = A_DEL:DID
!         IF Access:Deliveries.TryFetch(DEL:PKey_DID) = LEVEL:Benign
!            DEL:FID     = LOC:FID                  !TODO - set the floor to delivered???????????????????
!            IF Access:Deliveries.TryUpdate() = LEVEL:Benign
!   .  .  .  . 
!               
!   UNBIND('A_DEL:DID')
!   POPBIND()
